﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.Web.Resource;
using MTData.Web.ViewData;

using Ninject;

namespace MTData.Web.Controllers
{
    public class UserController : Controller
    {

        UserViewData _objUserViewData;
        readonly ILogger _logger = new Logger();
        private readonly IUserService _userService;
        private readonly ICommonService _commonService;
        private readonly IFleetService _fleetService;
        private StringBuilder _logMessage;
        private StringBuilder _logTracking;

        public UserController([Named("UserService")] IUserService userService, [Named("FleetService")] IFleetService fleetService, [Named("CommonService")] ICommonService commonService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _userService = userService;
            _commonService = commonService;
            _logMessage = logMessage;
            _logTracking = logTracking;
            _fleetService = fleetService;
        }

        /// <summary>
        /// Purpose             :   Create the new users
        /// Function Name       :   Create
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 4) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                int userId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.UserID));
                int logOnByUserId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserId));
                int logOnByUserType = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserType));

                _logMessage.Append("using object intilizer to GetUserRoles by user id= " + userId + " and convert to viewdatalist and get CountryList, StateList and CityList");

                _objUserViewData = new UserViewData { UserRoles = _userService.GetUserRoles(userId).ToViewDataList() };
                _objUserViewData = new UserViewData
                {
                    UserRoles = _userService.GetUserRoles(userId).ToViewDataList(),
                    CountryList = CountryList(),
                    UserTypeList = UserTypeList(),

                    fk_Country = "AU"
                };

                _logMessage.Append("User Roles  CountryList, StateList and CityList list populate successfully");

                int merchantId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.fk_MerchantID));

                _logMessage.Append("calling FleetList method of _userService by merchant Id= " + merchantId + " and convert to viewdatalist");

                _objUserViewData.fleetlist = _userService.FleetList(merchantId).ToViewDataList();
                if (logOnByUserType == 5)
                {
                    List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                    List<FleetViewData> fleetViewResult = new List<FleetViewData>();
                    foreach(FleetViewData fleetViewData in  _objUserViewData.fleetlist)
                    {
                        if (fleetListAccessed.Contains(fleetViewData.FleetID))
                            fleetViewResult.Add(fleetViewData);
                    }

                    _objUserViewData.fleetlist = fleetViewResult.AsEnumerable();
                    ViewBag.IsCorporate = true;
                }
                else
                {
                    ViewBag.IsCorporate = false;
                }

                _logMessage.Append("ToViewDataList method executed successfully");
                _logMessage.Append("calling GetUsersFleet method of _userService by user Id= " + userId + " and convert to viewdatalist");

                _objUserViewData.userFleetList = _userService.GetUsersFleet(userId).ToViewDataList();

                _logMessage.Append("ToViewDataList method executed successfully");

                return View(_objUserViewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   to display the users
        /// Function Name       :  index
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/30/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 4)
                return RedirectToAction("index", "Member");

            return View();
        }

        /// <summary>
        /// Purpose             :   Creating Users
        /// Function Name       :   Create
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/31/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userView"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(UserViewData userView)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 4)
                return RedirectToAction("index", "Member");
            
            try
            {
                if (!string.IsNullOrEmpty(userView.fleetListId))
                    userView.fleetListId = userView.fleetListId.Replace("multiselect-all,", string.Empty);

                if (userView.fk_UserTypeID == 4)
                {
                    if (userView.UserRoleIdList == null)
                        ModelState.AddModelError("UserRoleIdList", "Please select a role");
                    
                    if (String.IsNullOrEmpty(userView.fleetListId))
                        ModelState.AddModelError("fleetListId", "Please select a fleet");
                }

                int logOnByUserId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserId));
                int logOnByUserType = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserType));
                
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                
                int userId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.UserID));
                
                _objUserViewData = new UserViewData { UserRoles = _userService.GetUserRoles(userId).ToViewDataList() };
                
                int merchantId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.fk_MerchantID));
                
                _logMessage.Append("using object intilizer to GetUserRoles by user id= " + userId + " and convert to viewdatalist and get CountryList, StateList and CityList and GetUsersFleet by user Id= " + userId);
                
                _objUserViewData = new UserViewData
                {
                    UserRoles = _userService.GetUserRoles(userId).ToViewDataList(),
                    CountryList = CountryList(),
                    fk_Country = "AU",
                    fleetlist = _userService.FleetList(merchantId).ToViewDataList(),
                    userFleetList = _userService.GetUsersFleet(userId).ToViewDataList()
                };
                
                _logMessage.Append("User Roles, fleet, CountryList, StateList and CityList list populate successfully");
                
                if (!ModelState.IsValid)
                {
                    userView.CountryList = CountryList();
                    userView.fk_Country = "AU";
                    userView.fleetlist = _userService.FleetList(merchantId).ToViewDataList();

                    if (logOnByUserType == 5)
                    {
                        List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                        List<FleetViewData> fleetViewResult = new List<FleetViewData>();
                        foreach (FleetViewData fleetViewData in userView.fleetlist)
                        {
                            if (fleetListAccessed.Contains(fleetViewData.FleetID))
                            {
                                fleetViewResult.Add(fleetViewData);
                            }
                        }
                        
                        userView.fleetlist = fleetViewResult.AsEnumerable();
                        ViewBag.IsCorporate = true;
                    }
                    else
                    {
                        ViewBag.IsCorporate = false;
                    }
                    
                    userView.UserRoles = _userService.GetUserRoles(userId).ToViewDataList();
                    userView.UserTypeList = UserTypeList();
                    
                    if (userView.UserRoleIdList != null)
                    {
                        var roles = string.Join(", ", userView.UserRoleIdList);
                        userView.RolesAssigned = roles;
                    }
                    
                    if (userView.UserRoleIdList == null)
                        userView.RolesAssigned = "0".ToString();
                    
                    return View(userView);
                }

                var res = _commonService.IsExist(userView.Email);
                if (res)
                {
                    userView.CountryList = CountryList();
                    userView.fk_Country = "AU";
                    userView.fleetlist = _userService.FleetList(merchantId).ToViewDataList();
                    if (logOnByUserType == 5)
                    {
                        List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                        List<FleetViewData> fleetViewResult = new List<FleetViewData>();
                        foreach (FleetViewData fleetViewData in userView.fleetlist)
                        {
                            if (fleetListAccessed.Contains(fleetViewData.FleetID))
                            {
                                fleetViewResult.Add(fleetViewData);
                            }
                        }
                        
                        userView.fleetlist = fleetViewResult.AsEnumerable();
                        ViewBag.IsCorporate = true;
                    }
                    else
                    {
                        ViewBag.IsCorporate = false;
                    }
                    
                    userView.UserRoles = _userService.GetUserRoles(userId).ToViewDataList();
                    var roles = string.Join(", ", userView.UserRoleIdList);
                    userView.RolesAssigned = roles;
                    userView.UserTypeList = UserTypeList();
                    ViewBag.ExitUser = "Account for '" + userView.Email + "' already exists please contact to your administrator";
                    
                    return View(userView);
                }
                
                var filename = System.Web.HttpContext.Current.Server.MapPath("~/HtmlTemplate/confirmMail.html");
                var myFile = new StreamReader(filename);
                string msgfile = myFile.ReadToEnd();
                var msgBody = new StringBuilder(msgfile);
                
                string cMail = userView.Email;
                string cName = ((SessionDto)Session["User"]).SessionMerchant.Company;                
                string cPCode = Membership.GeneratePassword(10, 0);
                
                _objUserViewData = new UserViewData
                {
                    UserRoles = _userService.GetUserRoles(userId).ToViewDataList()
                };
                
                userView.PCode = cPCode.Hash();
                UserDto userDto = userView.ToDTO();
                userDto.fk_MerchantID = (((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                userDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                userDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                
                if (userDto.fk_UserTypeID == 3)
                {
                    userDto.RolesAssigned = string.Join(", ", userView.merchantRoles);
                    userDto.fleetListId = string.Join(", ", userView.FleetAssigned);
                }
                
                if (userDto.fk_UserTypeID == 4)
                {
                    userDto.RolesAssigned = string.Join(", ", userView.UserRoleIdList);
                    userDto.fleetListId = string.Join(", ", userView.fleetListId);
                }
                
                userDto.UserID = userId;
                
                _logMessage.Append(
                    "Calling Add method of _userService class takes userDto, userDto properties Frist Name= " +
                    userDto.FName + ", Last Name= " + userDto.LName + ", Phone= " + userDto.Phone + ", Email= " +
                    userDto.Email + ", Country= " + userDto.fk_Country + ", State= " + userDto.fk_State +
                    ", City= " + userDto.fk_City + ", Address= " + userDto.Address + ", RolesAssigned= " +
                    userDto.RolesAssigned + "+");
                
                bool results = _userService.Add(userDto);
                
                _logMessage.Append("Calling SendMail method of sendEmail class takes cMail, msgBody, cName thats are =" + cMail + ", " + msgBody + " and " + cName);
                
                string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                
                _logTracking.Append("User " + userName + " create user" + userDto.Email + " at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                
                try
                {
                    _logMessage.Append("Mail Sending begin and reading file ~/HtmlTemplate/password.html");
                    using (myFile)
                    {
                        string cuserName = userView.FName;
                        string appUrl = System.Configuration.ConfigurationManager.AppSettings["ApplicationURL"];
                        msgBody.Replace("[##cMail##]", cMail);
                        msgBody.Replace("[##cPassword##]", cPCode);
                        msgBody.Replace("[##cName##]", cName);
                        msgBody.Replace("[##cuserName##]", cuserName);
                        msgBody.Replace("[##AppURL##]", appUrl);
                        
                        var sendEmail = new EmailUtil();
                        
                        _logMessage.Append("calling SendMail method of utility class");
                        
                        sendEmail.SendMail(cMail, msgBody.ToString(), cName, "Registered User");
                        
                        if (results)
                            _logMessage.Append("Add method of adminUserService class executed successfully");
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), ex);
                }
                return RedirectToAction("index", "User");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   Updating Users
        /// Function Name       :   Update
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   1/10/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Update(int userId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 4)
                return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                
                int logOnByUserId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserId));
                int merchantId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.fk_MerchantID));
                int logOnByUserType = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserType));
                
                UserDto userdto = _userService.GetUser(userId);
                if (userdto == null)
                {
                    Session.Abandon();
                    return RedirectToAction("index", "Member");
                }

                UserViewData userData = userdto.ToViewData();
                var dto = (SessionDto)Session["User"];
                if (dto != null)
                {
                    if (merchantId != userdto.fk_MerchantID)
                    {
                        Session.Abandon();
                        return RedirectToAction("index", "Member");
                    }

                    int user = Convert.ToInt32((dto.SessionUser.UserID));
                    userData.UserRoles = _userService.GetUserRoles(user).ToViewDataList();
                    userData.fleetlist = _userService.FleetList(merchantId).ToViewDataList();

                    if (logOnByUserType == 5)
                    {
                        List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                        List<FleetViewData> fleetViewResult = new List<FleetViewData>();
                        foreach (FleetViewData fleetViewData in userData.fleetlist)
                        {
                            if (fleetListAccessed.Contains(fleetViewData.FleetID))
                            {
                                fleetViewResult.Add(fleetViewData);
                            }
                        }

                        userData.fleetlist = fleetViewResult.AsEnumerable();
                        ViewBag.IsCorporate = true;
                    }
                    else
                    {
                        ViewBag.IsCorporate = false;
                    }
                }
                
                userData.userFleetList = _userService.GetUsersFleet(userId).ToViewDataList();               
                userData.UserTypeList = UserTypeList();
                ViewData.Model = userData;
                
                _logMessage.Append("calling GetCountryList of _commonService to populate country dropdown");
                
                var countryDtoList = _commonService.GetCountryList();
                
                _logMessage.Append("GetCountryList of _commonService executed successfully");
                _logMessage.Append("calling ToCountryViewDataList method to map CountryViewData to CountryCodeDto and retrun CountryViewData");
                
                userData.CountryList = countryDtoList.ToCountryViewDataList();
                
                _logMessage.Append("ToCountryViewDataList method executed successfully");
                
                return View(userData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   Updating Users Details
        /// Function Name       :   Update
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   1/10/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="viewData"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(UserViewData viewData, int userId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 4)
                return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                 _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                
                int user = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.UserID));
                int logOnByUserId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserId));
                int merchantId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.fk_MerchantID));
                int logOnByUserType = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserType));
                
                _logMessage.Append("using object intilizer to GetUserRoles by user id= " + userId + " and convert to viewdatalist and get CountryList, StateList and CityList");

                _objUserViewData = new UserViewData
                {
                    fleetlist = _userService.FleetList(merchantId).ToViewDataList(),
                    userFleetList = _userService.GetUsersFleet(userId).ToViewDataList(),
                    UserRoles = _userService.GetUserRoles(user).ToViewDataList(),
                    CountryList = CountryList(),
                };

                if (!ModelState.IsValid)
                {
                    viewData.CountryList = CountryList();
                    viewData.fk_Country = "AU";
                    viewData.fleetlist = _userService.FleetList(merchantId).ToViewDataList();
                    if (logOnByUserType == 5)
                    {
                        List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                        List<FleetViewData> fleetViewResult = new List<FleetViewData>();
                        foreach (FleetViewData fleetViewData in  viewData.fleetlist)
                        {
                            if (fleetListAccessed.Contains(fleetViewData.FleetID))
                            {
                                fleetViewResult.Add(fleetViewData);
                            }
                        }
                        
                        viewData.fleetlist = fleetViewResult.AsEnumerable();
                        ViewBag.IsCorporate = true;
                    }
                    else
                    {
                        ViewBag.IsCorporate = false;
                    }          
                    
                    viewData.userFleetList = _userService.GetUsersFleet(userId).ToViewDataList();
                    viewData.UserRoles = _userService.GetUserRoles(user).ToViewDataList();
                    
                    if (viewData.UserRoleIdList != null)
                    {
                        var role = string.Join(", ", viewData.UserRoleIdList);
                        viewData.RolesAssigned = role;
                    }
                    
                    viewData.UserTypeList = UserTypeList();
                    
                    return View(viewData);
                }

                _logMessage.Append("calling ToDTO method to map UserViewData to UserDto and retrun UserViewData");
                
                UserDto userDto = viewData.ToDTO();
                
                _logMessage.Append("ToDTO method executed successfully");
                
                if (userDto.fk_UserTypeID == 3)
                {
                    userDto.RolesAssigned = string.Join(", ", viewData.merchantRoles);
                    userDto.fleetListId = string.Join(", ", viewData.FleetAssigned);
                }
                
                if (userDto.fk_UserTypeID == 4)
                {
                    if (viewData.UserRoleIdList != null && viewData.fleetListId != null)
                    {
                        userDto.RolesAssigned = string.Join(", ", viewData.UserRoleIdList);
                        userDto.fleetListId = string.Join(", ", viewData.fleetListId);
                    }
                }

                _logMessage.Append("Calling Update method of _userService class takes userDto, userDto properties Frist Name= " + userDto.FName + ", Last Name= " + userDto.LName + ", Phone= " + userDto.Phone + ", Email= " + userDto.Email + ", Country= " + userDto.fk_Country + ", State= " + userDto.fk_State + ", City= " + userDto.fk_City + ", Address= " + userDto.Address + ", RolesAssigned= " + userDto.RolesAssigned + "userId= " + userId);
                
                bool results = _userService.Update(userDto, userId);
                ViewBag.Message = results ? "User Updated Successfully" : "Internal Server Error";
                if (results)
                {
                    _logMessage.Append("Update method executed successfully");
                    
                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;

                    _logTracking.Append("User " + userName + " update user" + userDto.Email + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                }

                return RedirectToAction("index", "User");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// Purpose             :   to get city list
        /// Function Name       :   CountryList
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   1/13/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
       
        [NonAction]
        public IEnumerable<CountryViewData> CountryList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetCountryList method of _commonService class to get countries");
                
                var countryDtoList = _commonService.GetCountryList();
                
                _logMessage.Append("GetCountryList method of _commonService executed successfully");
                _logMessage.Append("calling ToCountryViewDataList method to map CountryViewData to CountryCodeDto and retrun CountryViewData");
                
                var countryViewDataList = countryDtoList.ToCountryViewDataList();
                
                _logMessage.Append("ToCountryViewDataList method executed successfully");
                _logMessage.Append("converting countryViewDataList list into objcountrylist");
                
                List<CountryViewData> objcountrylist = countryViewDataList.ToList();
                
                _logMessage.Append("convert countryViewDataList into objcountrylist successful");
                
                var li = new List<SelectListItem> { new SelectListItem { Text = "Please select a Country", Value = "0" } };

                li.AddRange(objcountrylist.Select(item => new SelectListItem { Text = item.Title, Value = Convert.ToString(item.CountryCodesID) }));

                _logMessage.Append("returning country list");

                return objcountrylist;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return null;
        }

        /// <summary>
        /// Purpose             :  page indexing to display the list of merchant
        /// Function Name       :   MerchantUserListByFiter
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   1/23/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult MerchantUserListByFiter(string name = null, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetAdminUserListByFilter method of adminUserService class to get Users List");
                
                int adminUserCount;
                name = name.Trim();
                int merchantId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.fk_MerchantID));
                int logOnByUserId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserId));
                int logOnByUserType = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserType));

                FilterSearchParametersDto filterSearch = new FilterSearchParametersDto()
                {
                    Name = name,
                    StartIndex = jtStartIndex,
                    PageSize = jtPageSize,
                    Sorting = jtSorting,
                    MerchantId = merchantId
                };

                var merchantUser = _userService.UserListByFilter(filterSearch,logOnByUserId,logOnByUserType);          
                adminUserCount = _userService.UserList(merchantId,name,logOnByUserId,logOnByUserType).Count();
                if (string.IsNullOrEmpty(name))
                {
                    _logMessage.Append("Calling UserList method of _userService class to count fee");
                    
                    adminUserCount = _userService.UserList(merchantId,name, logOnByUserId, logOnByUserType).Count();
                    
                    _logMessage.Append("UserList method of _userService executed successfully result = " + adminUserCount);
                    
                    return Json(new { Result = "OK", Records = merchantUser, TotalRecordCount = adminUserCount });
                }

                var userDtos = merchantUser as UserDto[] ?? merchantUser.ToArray();

                return Json(new { Result = "OK", Records = userDtos, TotalRecordCount = adminUserCount });
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
                return Json(new { Result = "ERROR", exception.Message });
            }
        }

        /// <summary>
        /// Purpose             :   To find out user is locked or not 
        /// Function Name       :   Islocked
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   1/24/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Islocked(int id, bool status)
        {
            _commonService.IsLockedMember(id, status);
            
            string userName = ((SessionDto)Session["User"]).SessionUser.Email;
            
            _logTracking.Append("User " + userName + " locked user at " + System.DateTime.Now.ToUniversalTime());
            _logger.LogInfoMessage(_logTracking.ToString());

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Purpose             :  Apply page indexing to diplay the delete screen
        /// Function Name       :  Delete
        /// Created By          :  Umesh Kumar
        /// Created On          :  1/29/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(int userId)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                try
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    _logMessage.Append("Calling DeleteUser method of _userService class takes userId= " + userId);

                    _userService.DeleteUser(userId);

                    _logMessage.Append("DeleteUser method of _userService class executed successfully");

                    Thread.Sleep(50);

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;

                    _logTracking.Append("User " + userName + " delete user ID" + userId + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return Json(new { Result = "OK" });
                }
                catch (Exception exception)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), exception);
                }
            }

            return Json(new { Result = "No" });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAddress()
        {
            if (Session["User"] != null)
            {
                string country = ((SessionDto)Session["User"]).SessionUser.fk_Country;
                string state = ((SessionDto)Session["User"]).SessionUser.fk_State;
                string city = ((SessionDto)Session["User"]).SessionUser.fk_City;
                string merchantAddress = ((SessionDto)Session["User"]).SessionUser.Address;
                var result = new { Country = country, State = state, City = city, Address = merchantAddress };
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            return Json("");
        }

        public static IEnumerable<UserTypeViewData> UserTypeList()
        {
            List<UserTypeViewData> typeList = new List<UserTypeViewData>
            {
                new UserTypeViewData {UsetTypeId = 3, UserType = "Merchant admin"},
                new UserTypeViewData {UsetTypeId = 4, UserType = "Merchant user"},
            };
            
            return typeList;
        }

        /// <summary>
        /// Purpose             : for changing status for first login
        /// Function Name       :  ChangeStatus
        /// Created By          :  Asif Shafeeque
        /// Created On          :  8/17/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="UserId"></param>
        [HttpPost]
        public void ChangeStatus(int UserId)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                try
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    _logMessage.Append("Calling changeStatus method of _userService class takes userId= " + UserId);
                    
                    _userService.ChangeStatus(UserId);
                    
                    _logMessage.Append("changeStatus method of _userService class executed successfully");
                    
                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;

                    _logTracking.Append("User " + userName + " change status of login of user ID" + UserId + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                }
                catch (Exception exception)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), exception);
                }
            }
        }

    }
}