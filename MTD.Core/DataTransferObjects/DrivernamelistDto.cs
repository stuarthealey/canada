﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
   public class DrivernamelistDto
    {
        public string DriverNo { get; set; }
        public string FName { get; set; }
    }
}
