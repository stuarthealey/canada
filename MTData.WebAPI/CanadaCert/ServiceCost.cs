﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CanadaCert
{
    public class ServiceCost
    {
        public string ServiceID { get; set; }
        public string TransactionTimeMs { get; set; }
    }
}