﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
    public class MTDSemiIntigratedTxnDto
    {
        public int Id { get; set; }
        public string CMessageNumber { get; set; }
        public Nullable<decimal> CAmount1 { get; set; }
        public Nullable<decimal> CAmount2 { get; set; }
        public Nullable<decimal> CAmount3 { get; set; }
        public Nullable<decimal> CAmount4 { get; set; }
        public string CAmount1Label { get; set; }
        public string CAmount2Label { get; set; }
        public string CAmount3Label { get; set; }
        public string CAmount4Label { get; set; }
        public string CTransactionType { get; set; }
        public string CReference { get; set; }
        public string CTransactionID { get; set; }
        public string cAuthCode { get; set; }
        public string cOfferPWCB { get; set; }
        public string cTransactionStatus { get; set; }
        public string CEntryMethod { get; set; }
        public string CReceiptNumber { get; set; }
        public string CAcquirerMerchantID { get; set; }
        public string CDateTime { get; set; }
        public string CCurrency { get; set; }
        public string CCardSchemeName { get; set; }
        public string CPAN { get; set; }
        public string CExpiryDate { get; set; }
        public string CGEMSReceipt { get; set; }
        public string CAuthorizationCode { get; set; }
        public string cAcquirerResponseCode { get; set; }
        public string CMerchantName { get; set; }
        public string CMerchantAddress1 { get; set; }
        public string CMerchantAddress2 { get; set; }
        public string CAID { get; set; }
        public string CPANSequenceNum { get; set; }
        public string CStartDate { get; set; }
        public string CTerminalIdentity { get; set; }
        public Nullable<decimal> CTransactionAmount { get; set; }
        public string CHostMessage { get; set; }
        public string CGratuityAmount { get; set; }
        public string CCashAmount { get; set; }
        public string CTotalTransactionAmount { get; set; }
        public string CICCApplicationFileName { get; set; }
        public string CICCApplicationPreferredName { get; set; }
        public string CCVM { get; set; }
        public Nullable<decimal> CDCCAmount { get; set; }
        public Nullable<decimal> CDonationAmount { get; set; }
        public Nullable<decimal> CRedeemedAmount { get; set; }
        public string DCCCurrency { get; set; }
        public string CFXRateApplied { get; set; }
        public string CIsDCCTxn { get; set; }
        public string CIsLoyaltyTxn { get; set; }
        public string CFXExponentApplied { get; set; }
        public string CDCCCurrencyExponent { get; set; }
        public string RequestXml { get; set; }
        public string ResponseXml { get; set; }
        public Nullable<decimal> Flagfall { get; set; }
        public Nullable<decimal> Fare { get; set; }
        public Nullable<decimal> Extras { get; set; }
        public Nullable<decimal> Total { get; set; }
    }
}