﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Models
{
    public class LogoutResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }

    }
}