﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CanadaCert
{
    public class Bit63Fields
    {
        public string Table14Response { get; set; }
        public string Table49Response { get; set; }
        public string TableVIResponse { get; set; }
        public string TableMCResponse { get; set; }
        public string TableDSResponse { get; set; }
        public string TableSPResponse { get; set; }
    }
}