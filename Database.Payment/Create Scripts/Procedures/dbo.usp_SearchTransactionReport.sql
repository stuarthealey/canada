IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SearchTransactionReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_SearchTransactionReport]
GO

CREATE PROCEDURE dbo.usp_SearchTransactionReport
-- =============================================
-- Author:		
-- Create date: 
-- Description:	Search Transaction report
-- =============================================
--	S.Healey	2018-01-25		- Added First\Last card number details.
--
(
	@TransId INT = NULL, -- Pass Null in case no trans ID
	@VehicleId VARCHAR(50) = NULL,
	@DriverId NVARCHAR(50) = NULL,
	@DateFrom DATETIME = NULL,
	@DateTo DATETIME = NULL,
	@CardType NVARCHAR(20) = NULL,
	@EntryMode NVARCHAR(20) = NULL, 
	@AuthorizeCode NVARCHAR(20) = NULL,
	@PaymentType NVARCHAR(20) = NULL,
	@TransType NVARCHAR(20) = NULL, 
	@fk_MerchantID INT = NULL,
	@ExpDate VARCHAR(50) = NULL,
	@MinAmount MONEY = NULL,
	@MaxAmount MONEY = NULL,
	@FirstFourDigits VARCHAR(10) = NULL,
	@LastFourDigits VARCHAR(10) = NULL,
	@Approval NVARCHAR(20) = NULL,
	@JobNumber VARCHAR(15) = NULL,
	@fk_FleetId NVARCHAR(20) = NULL,
	@IsVarified BIT = NULL,
	@RCTerminalId  NVARCHAR(20) = NULL,
	@SerialNo NVARCHAR(50) = NULL
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	-- If we know the Transaction ID, find by just that.
	IF (ISNULL(@TransId, 0) > 0)
	BEGIN
		SELECT	Id, 
				MerchantId, 
				VehicleNo, 
				DriverNo, 
				JobNumber, 
				Stan, 
				TransRefNo, 
				TransOrderNo, 
				PaymentType, 
				TxnType, 
				LocalDateTime, 
				TransmissionDateTime, 
				FareValue, 
				Tip, 
				Surcharge, 
				Fee, 
				Taxes, 
				Toll, 
				Amount, 
				Currency, 
				ExpiryDate, 
				CardType, 
				Industry, 
				EntryMode, 
				ResponseCode, 
				AddRespData, 
				AuthId, 
				AthNtwId, 
				RequestXml, 
				ResponseXml,  
				SourceId, 
				TransNote, 
				CardToken, 
				IsCompleted, 
				IsRefunded, 
				IsVoided, 
				CreatedBy, 
				TxnDate, 
				fk_FleetId, 
				TechFee, 
				FleetFee, 
				IsVarified,
				TerminalId,
				SerialNo, 
				IsDownloaded,
				FirstFourDigits, 
				LastFourDigits

		FROM dbo.MTDTransaction T

		WHERE T.Id = @TransId
	END
	ELSE
	BEGIN
		SELECT	Id, 
				MerchantId, 
				VehicleNo, 
				DriverNo, 
				JobNumber, 
				Stan, 
				TransRefNo, 
				TransOrderNo, 
				PaymentType, 
				TxnType, 
				LocalDateTime, 
				TransmissionDateTime, 
				FareValue, 
				Tip, 
				Surcharge, 
				Fee, 
				Taxes, 
				Toll, 
				Amount, 
				Currency, 
				ExpiryDate, 
				CardType, 
				Industry, 
				EntryMode, 
				ResponseCode, 
				AddRespData, 
				AuthId, 
				AthNtwId, 
				RequestXml, 
				ResponseXml,  
				SourceId, 
				TransNote, 
				CardToken, 
				IsCompleted, 
				IsRefunded, 
				IsVoided, 
				CreatedBy, 
				TxnDate, 
				fk_FleetId, 
				TechFee, 
				FleetFee, 
				IsVarified,
				TerminalId,
				SerialNo, 
				IsDownloaded,
				FirstFourDigits, 
				LastFourDigits

		FROM dbo.MTDTransaction T

		WHERE (T.TxnDate >= @DateFrom OR @DateFrom IS NULL)
		  AND (T.TxnDate <= @DateTo OR @DateTo IS NULL)
		  AND (T.VehicleNo = @VehicleId OR COALESCE(@VehicleId, '') = '')
		  AND (T.DriverNo = @DriverId OR COALESCE(@DriverId, '') = '')
		  AND (T.CardType = @CardType OR COALESCE(@CardType, '') = '')
		  AND (T.EntryMode = @EntryMode OR COALESCE(@EntryMode, '') = '')
		  AND (T.AuthId = @AuthorizeCode OR COALESCE(@AuthorizeCode, '') = '')
		  AND (T.Industry = @PaymentType OR COALESCE(@PaymentType, '') = '')
		  AND (T.TxnType = @TransType OR COALESCE(@TransType, '') = '')
		  AND (T.MerchantId = @fk_MerchantID OR COALESCE(@fk_MerchantID,0) = 0)
		  AND (T.ExpiryDate = @ExpDate OR COALESCE(@ExpDate, '') = '')
		  AND (T.Amount >=  @MinAmount OR COALESCE(@MinAmount,0) = 0)
		  AND (T.Amount <=  @MaxAmount OR COALESCE(@MaxAmount,0) = 0)
		  AND (T.FirstFourDigits = @FirstFourDigits OR COALESCE(@FirstFourDigits, '') = '')
		  AND (T.LastFourDigits = @LastFourDigits OR COALESCE(@LastFourDigits, '') = '')
		  AND (T.AddRespData = @Approval OR COALESCE(@Approval, '') = '')
		  AND (T.JobNumber = @JobNumber OR COALESCE(@JobNumber, '') = '')
		  AND (T.fk_FleetId = @fk_FleetId OR COALESCE(@fk_FleetId, '') = '')
		  AND (T.IsVarified = @IsVarified OR @IsVarified IS NULL)
		  AND (T.TerminalId = @RCTerminalId OR  COALESCE(@RCTerminalId, '') = '')
		  AND (T.SerialNo = (SELECT TOP 1 MacAdd FROM dbo.Terminal WHERE SerialNo = @SerialNo AND IsActive = 1) OR COALESCE(@SerialNo, '') = '')
	END
END
GO
