﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
    public class MtdtxnSattlementUKDto
    {
        public int Id { get; set; }
        public string Revision { get; set; }
        public int? MerchantIndex { get; set; }
        public int? settlementResult { get; set; }
        public string AcquirerName { get; set; }
        public string MerchantAddress1 { get; set; }
        public string MerchantAddress2 { get; set; }
        public string AcquirerMerchantID { get; set; }
        public string TerminalIdentity { get; set; }
        public string HostMessage { get; set; }
        public short? FirstMessageNumber { get; set; }
        public short? LastMessageNumber { get; set; }
        public string DateTime { get; set; }
        public string NumScheme { get; set; }
        public int? QuantityDebits { get; set; }
        public decimal? ValueDebits { get; set; }
        public int? QuantityCredits { get; set; }
        public decimal? ValueCredits { get; set; }
        public int? QuantityCash { get; set; }
        public Nullable<decimal> ValueCash { get; set; }
        public int? QuantityClessCredits { get; set; }
        public decimal? ValueClessCredits { get; set; }
        public int? QuantityClessDebits { get; set; }
        public decimal? ValueClessDebits { get; set; }
        public string CardSchemeName { get; set; }
        public string RequestXml { get; set; }
    }
}
