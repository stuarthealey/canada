﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.Web.ViewData
{
    public class Header
    {
        public string DeviceId { get; set; }
        public string DriverNumber { get; set; }
        public string RequestId { get; set; }
        public string Token { get; set; }
        public string DriverId { get; set; }
    }
}