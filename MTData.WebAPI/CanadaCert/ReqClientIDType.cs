﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CanadaCert
{
    [Serializable]
    public class ReqClientIDType
    {
        public string DID { get; set; }
        public string App { get; set; }
        public string Auth { get; set; }
        public string ClientRef { get; set; }
    }
}