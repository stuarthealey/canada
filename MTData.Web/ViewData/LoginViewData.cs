﻿using System.ComponentModel.DataAnnotations;

namespace MTData.Web.ViewData
{
    public class LoginViewData : BaseViewData
    {
       // [Required(ErrorMessage = "*")]
        public string UserName { get; set; }
      // [Required(ErrorMessage = "*")]
        public string PCode { get; set; }
        public string ConfirmPCode { get; set; }

    }
}