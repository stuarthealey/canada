﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MTData.WebAPI.CanadaCommon;
namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table36
    {
        #region Private Constants and Variables

        #region Constants
        private const string tblId = "|33|36";
        private const string version = "|31";
        private const string customerServiceNo = "|38|38|38|32|39|39|34|33|39|31";//888-299-4391
        private  string orderNo = "|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20";
        private string ecommUrl = "|68|74|74|70|73|3a|2f|2f|70|61|79|6d|65|6e|74|73|2e|6d|74|64|61|74|61|2e|75|73|2f|20|20|20|20|20"; // Non-VI:-"https://payments.mtdata.us/2020202020";For VI:-|68|74|74|70|73|3a|2f|2f|6d|74|2e|75|73  |77|77|77|2e|6d|74|64|61|74|61|2e|75|73
        #endregion

        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'33 36' Table 36
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        public string Version
        {
            get { return version; }
        }

        public string CustomerServiceNo
        {
            get { return customerServiceNo; }
        }

        public string OrderNo
        {
            get { return orderNo; }
            set { orderNo = value; }
        }

        public string EcommUrl
        {
            get { return ecommUrl; }
            set { ecommUrl = value; }
        }
        
        #endregion
    }
}