﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Data.Data
{
    public class DownloadParameters
    {
        public string UserId { get; set; }
        public string PCode { get; set; }
        public Nullable<System.DateTime> DateFrom { get; set; }
        public Nullable<System.DateTime> DateTo { get; set; }
        public string EntryMode { get; set; }
        public Nullable<int> Fk_MerchantID { get; set; }
        public string City { get; set; }
        public string VehicleId { get; set; }
        public string DriverId { get; set; }
        public Nullable<int> Fk_FleetId { get; set; }
        public string SerialNo { get; set; }
        public string TransType { get; set; }
        public string DownloadStatus { get; set; }
        public Nullable<int> BatchNumber { get; set; }
        public Nullable<bool> RetrieveSinceBatch { get; set; }
    }
}
