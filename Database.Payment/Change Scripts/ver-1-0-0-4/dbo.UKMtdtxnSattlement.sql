IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UKMtdtxnSattlement]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[UKMtdtxnSattlement](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[Revision] [nvarchar](3) NULL,
		[MerchantIndex] [int] NULL,
		[settlementResult] [int] NULL,
		[AcquirerName] [nvarchar](24) NULL,
		[MerchantAddress1] [nvarchar](24) NULL,
		[MerchantAddress2] [nvarchar](24) NULL,
		[AcquirerMerchantID] [nvarchar](24) NULL,
		[TerminalIdentity] [nvarchar](8) NULL,
		[HostMessage] [nvarchar](32) NULL,
		[FirstMessageNumber] [smallint] NULL,
		[LastMessageNumber] [smallint] NULL,
		[DateTime] [nvarchar](15) NULL,
		[NumScheme] [nchar](90) NULL,
		[QuantityDebits] [int] NULL,
		[ValueDebits] [money] NULL,
		[QuantityCredits] [int] NULL,
		[ValueCredits] [money] NULL,
		[QuantityCash] [int] NULL,
		[ValueCash] [money] NULL,
		[QuantityClessCredits] [int] NULL,
		[ValueClessCredits] [money] NULL,
		[QuantityClessDebits] [int] NULL,
		[ValueClessDebits] [money] NULL,
		[CardSchemeName] [nvarchar](25) NULL,
		[RequestXml] [xml] NULL,
	PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
