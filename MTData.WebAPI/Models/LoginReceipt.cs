﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class LoginReceipt
    {
        [DataMember]
        public List<ReceiptInfo> ReceiptField { get; set; }
    }
}