﻿using MTD.Core.DataTransferObjects;

namespace MTData.Web.ViewData
{
    public static class ViewDataExtention
    {
        public static MerchantDto CreateMerchantDto(MerchantViewData merchantViewData)
        {
            MerchantDto merchandDto = new MerchantDto
            {

                Company = merchantViewData.Company,
                IsActive = merchantViewData.IsActive,
                // fk_SurID = merchantViewData.fk_SurID,
                CreatedBy = merchantViewData.CreatedBy,
                ModifiedBy = merchantViewData.ModifiedBy,
                CreatedDate = merchantViewData.CreatedDate,
                ModifiedDate = merchantViewData.ModifiedDate,

                SetupFee = merchantViewData.SetupFee,
                MonthlyFee = merchantViewData.MonthlyFee,
                IsAllowSale = merchantViewData.IsAllowSale,
                IsAllowPreauth = merchantViewData.IsAllowPreauth,
                IsAllowCapture = merchantViewData.IsAllowCapture,
                IsAllowRefund = merchantViewData.IsAllowRefund,
                IsAllowVoid = merchantViewData.IsAllowVoid,
            };
            return merchandDto;
        }

        public static MerchantViewData CreateMerchantViewData(MerchantDto merchantViewData)
        {
            MerchantViewData merchandViewData = new MerchantViewData
            {

                Company = merchantViewData.Company,
                IsActive = merchantViewData.IsActive,
                CreatedBy = merchantViewData.CreatedBy,
                ModifiedBy = merchantViewData.ModifiedBy,
                CreatedDate = merchantViewData.CreatedDate,
                ModifiedDate = merchantViewData.ModifiedDate,

                SetupFee = merchantViewData.SetupFee,
                MonthlyFee = merchantViewData.MonthlyFee,
                IsAllowSale = merchantViewData.IsAllowSale,
                IsAllowPreauth = merchantViewData.IsAllowPreauth,
                IsAllowCapture = merchantViewData.IsAllowCapture,
                IsAllowRefund = merchantViewData.IsAllowRefund,
                IsAllowVoid = merchantViewData.IsAllowVoid,
            };
            return merchandViewData;
        }

        public static LoginDto CreateLoginDto(LoginViewData loginViewData)
        {
            LoginDto loginDto = new LoginDto
            {
                UserName = loginViewData.UserName,
                PCode = loginViewData.PCode,
                ConfirmPCode = loginViewData.ConfirmPCode,
            };
            return loginDto;
        }

        public static UserDto CreateAdminUserDto(UserViewData userView)
        {
            UserDto user = new UserDto
            {

                FName = userView.FName,
                LName = userView.LName,
                Email = userView.Email,
                Phone = userView.Phone,
                fk_Country = userView.fk_Country,
                fk_State = userView.fk_State,
                fk_City = userView.fk_City,
                Address = userView.Address,
                PCode = userView.PCode,
                IsActive = userView.IsActive,
                IsLockedOut = userView.IsLockedOut,
                ModifiedBy = userView.ModifiedBy,

            };
            return user;
        }

    }
}