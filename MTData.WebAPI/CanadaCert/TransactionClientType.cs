﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CanadaCert
{
        [Serializable]
        public class TransactionClientType
        {
            public string ServiceID { get; set; }
            public string Payload { get; set; }
        }
 
}