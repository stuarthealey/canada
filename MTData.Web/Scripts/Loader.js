﻿
//Function to show loader
function Showloader() {
    $('.popup-loading-bg').fadeIn(300);
    $('body').append('<div id="mask"></div>');
    $('#mask').fadeIn(300);
}

//function to close loader
function Closeloader() {
    $(this).delay(500).queue(function () {

        $('#mask , .popup-loading-bg').fadeOut(300, function () {
            $('#mask').remove();
        });
        $(this).dequeue();
    });
}
