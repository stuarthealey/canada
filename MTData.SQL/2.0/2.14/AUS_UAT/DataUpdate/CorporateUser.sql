begin tran abc
declare @tempMer table(id int identity(1,1),  UserId int )
insert @tempMer
select distinct fk_UserID from Recipient  where fk_UserTypeID in(5) and fk_MerchantID is null and fk_UserID is not null
select * from @tempMer
declare @maxCount int,@min int=0,@merId int, @UserType int, @userID int
select @maxCount=count(*) from @tempMer
while (@min <= @maxCount)
BEGIN
	select  @userID=UserId
	from @tempMer where id=@min
	if not exists(select 1 from Recipient where fk_UserID=@userID and fk_UserTypeID=5 and fk_ReportID in(7))
	insert into Recipient values(null,@userID,1,'admin@mtdata.com',GETDATE(),5,7)
	if not exists(select 1 from Recipient where fk_UserID=@userID and fk_UserTypeID=5 and fk_ReportID in(8))
	insert into Recipient values(null,@userID,1,'admin@mtdata.com',GETDATE(),5,8)
	set @min=@min+1
END
 commit transaction abc
--rollback transaction abc
 --select * from Recipient
 GO