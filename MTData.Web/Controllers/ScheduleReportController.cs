﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Text;
using System.Globalization;
using System.Reflection;
using System.Collections.Generic;
using System.Web.Security;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.Web.Resource;
using MTData.Web.ViewData;

namespace MTData.Web.Controllers
{
    public class ScheduleReportController : Controller
    {
        private readonly ILogger _logger = new Logger();
        private readonly IScheduleService _scheduleService;
        private readonly ICommonService _commonService;
        private StringBuilder _logMessage;
        private StringBuilder _logTracking;

        public ScheduleReportController([Named("CommonService")] ICommonService commonService,
            [Named("ScheduleService")] IScheduleService scheduleService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _scheduleService = scheduleService;
            _commonService = commonService;
            _logMessage = logMessage;
            _logTracking = logTracking;
        }

        /// <summary>
        ///  Purpose            : To display the schedule report details
        /// Function Name       :   Display
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/28/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Display()
        {
            if ((SessionDto)Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                return View();
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            : To schedule reports
        /// Function Name       :  Create
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/29/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var recipientDto = _commonService.GetRecipientListDistinct();
                foreach (var rec in recipientDto)
                {
                    rec.IsRecipient = true;
                }

                var recipient = recipientDto.ToMerchantViewDataSelectList();
                var adminRecipientDto = _commonService.GetAdminRecipientListDistinct();
                foreach (var recipnt in adminRecipientDto)
                {
                    recipnt.IsRecipient = true;
                }

                var adminRecipient = adminRecipientDto.ToMerchantViewDataSelectList();
                ScheduleReportViewData scheduleReport = new ScheduleReportViewData
                {
                    FrequencyList = FrequencyList(),
                    TimeZones = TimeZoneList(),
                    DayList = WeekDayList(),
                    MonthDayList = MonthDayList(),
                    ReportList = ReportList(),
                    Frequency = Convert.ToByte(1),
                    AdminList = adminRecipient,
                    MerchantList = recipient
                };

                _logMessage.Append("calling FrequencyList method of same class");
                
                scheduleReport.FrequencyTyeList = FrequencyTypeList();
                
                _logMessage.Append("FrequencyList methods executed successfully");

                scheduleReport.DateFrom = DateTime.Now.Date;

                return View(scheduleReport);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            : To schedule reports
        /// Function Name       :  Create
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/29/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="scheduleViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(ScheduleReportViewData scheduleViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("index", "Member");

            try
            {
                if ((scheduleViewData.Frequency == 2) && (scheduleViewData.WeekDay == null))
                    ModelState.AddModelError("WeekDay", MtdataResource.Please_select_week_day);

                if ((scheduleViewData.Frequency == 3) && (scheduleViewData.MonthDay == null))
                    ModelState.AddModelError("MonthDay", MtdataResource.Please_select_month_day);

                if (scheduleViewData.DateFrom > scheduleViewData.DateTo)
                    ModelState.AddModelError("DateFrom", MtdataResource.Date_from_validation);

                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (ModelState.IsValid)
                {
                    scheduleViewData.DisplayStartTime = scheduleViewData.StartTime;
                    DateTime dateTime = (DateTime)(DateTime.Now.Date + scheduleViewData.StartTime);
                    string timeZoneID = scheduleViewData.TimeZone;
                    TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneID);
                    DateTime dat = TimeZoneInfo.ConvertTime(dateTime, timeZone);
                    string strTime = dat.TimeOfDay.ToString();
                    TimeSpan ts = TimeSpan.Parse(strTime.Substring(0, 8));
                    scheduleViewData.StartTime = ts;
                    ScheduleReportViewData scheduleView = ClearDay(scheduleViewData);

                    _logMessage.Append("calling ToScheduleDto method to map ScheduleReportDto to ScheduleReportViewData and return ScheduleReportDto");

                    var scheduleDto = scheduleView.ToScheduleDto();

                    _logMessage.Append("ToScheduleDto method executed successfully");

                    scheduleDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                    _logMessage.Append("calling Add method of  _scheduleService takes scheduleDto parameters are " +
                                       scheduleDto.WeekDay + ", " + scheduleDto.ScheduleID + ", " + scheduleDto.MonthDay +
                                       ", " + scheduleDto + ", " + scheduleDto.ModifiedDate + ", " +
                                       scheduleDto.IsCreated + ", " + scheduleDto.Frequency + ", " + scheduleDto.DateTo +
                                       ", " + scheduleDto.CreatedDate);

                    RecipientDto rd = new RecipientDto();
                    AdminRecipientListDto arl = new AdminRecipientListDto();
                    arl.AdminList = scheduleView.AdminList.ToDtoList();
                    bool resultAdmin = _scheduleService.UpdateAdminRecipient(arl);

                    // Update Merchant
                    rd.MerchantList = scheduleView.MerchantList.ToMerchantListDto();
                    bool result = _scheduleService.UpdateRecipient(rd);
                    _scheduleService.Add(scheduleDto);
                    _logMessage.Append("Add method of  _scheduleService executed successfully");

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + " add schedule report Id" + scheduleDto.fk_ReportID + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return RedirectToAction("Display");
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            _logMessage.Append("calling FrequencyList,  FrequencyTypeListmethod,TimeZoneList method of same class");

            scheduleViewData.FrequencyList = FrequencyList();
            scheduleViewData.FrequencyTyeList = FrequencyTypeList();
            scheduleViewData.TimeZones = TimeZoneList();
            scheduleViewData.ReportList = ReportList();
            scheduleViewData.DayList = WeekDayList();
            scheduleViewData.MonthDayList = MonthDayList();

            foreach (var item in scheduleViewData.AdminList)
            {
                item.ModifiedBy = item.Email;
            }

            foreach (var itemMerchant in scheduleViewData.MerchantList)
            {
                itemMerchant.ModifiedBy = itemMerchant.Email;
            }

            _logMessage.Append("FrequencyList,  FrequencyTypeListmethod,TimeZoneList methods executed successfully");

            return View(scheduleViewData);
        }

        /// <summary>
        ///  Purpose            : To update the Schedule Report Details
        /// Function Name       :   Update
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/30/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Update(int scheduleId, int reportId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                _logMessage.Append("calling GetScheduleDto method _scheduleService");

                var scheduleDto = _scheduleService.GetScheduleDto(scheduleId);

                _logMessage.Append("GetScheduleDto method executed successfully");
                _logMessage.Append("calling ToScheduleViewData method to map ScheduleReportViewData to ScheduleReportDto and retrun ScheduleReportViewData");

                var scheduleReport = scheduleDto.ToScheduleViewData();

                _logMessage.Append("ToScheduleViewData method executed successfully");
                _logMessage.Append("calling FrequencyList,  FrequencyTypeListmethod,TimeZoneList method of same class");

                scheduleReport.FrequencyList = FrequencyList();
                scheduleReport.FrequencyTyeList = FrequencyTypeList();
                scheduleReport.TimeZones = TimeZoneList();
                scheduleReport.DayList = WeekDayList();
                scheduleReport.MonthDayList = MonthDayList();
                scheduleReport.ReportList = ReportListReadOnly();
                scheduleReport.StartTime = scheduleReport.DisplayStartTime;
                var recipientDto = _commonService.GetRecipientList(reportId, 2);

                _logMessage.Append("GetRecipientList of  _commonService executed successfully");
                _logMessage.Append("calling ToMerchantViewDataSelectList method to map MerchantListViewData to MerchantListDto and retrun MerchantListViewData");

                var recipient = recipientDto.ToMerchantViewDataSelectList();

                _logMessage.Append("ToMerchantViewDataSelectList method executed successfully");

                scheduleReport.MerchantList = recipient;

                _logMessage.Append("FrequencyList,  FrequencyTypeListmethod,TimeZoneList methods executed successfully");

                var adminRecipientDto = _commonService.GetRecipientList(reportId, 1);

                _logMessage.Append("GetRecipientList of  _commonService executed successfully");
                _logMessage.Append("calling ToMerchantViewDataSelectList method to map MerchantListViewData to MerchantListDto and retrun MerchantListViewData");

                var adminRecipient = adminRecipientDto.ToMerchantViewDataSelectList();

                scheduleReport.AdminList = adminRecipient;

                return View(scheduleReport);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            : to update the Schedule report details
        /// Function Name       :  Update
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/31/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="scheduleView"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(ScheduleReportViewData scheduleView)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("index", "Member");

            try
            {
                if (scheduleView.Frequency == 0)
                    ModelState.AddModelError("Frequency", MtdataResource.Please_select_frequency);

                if ((scheduleView.Frequency == 2) && (scheduleView.WeekDay == null))
                    ModelState.AddModelError("WeekDay", MtdataResource.Please_select_week_day);

                if ((scheduleView.Frequency == 3) && (scheduleView.MonthDay == null))
                    ModelState.AddModelError("MonthDay", MtdataResource.Please_select_month_day);

                if (scheduleView.DateFrom > scheduleView.DateTo)
                    ModelState.AddModelError("DateFrom", MtdataResource.Date_from_validation);

                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (ModelState.IsValid)
                {
                    scheduleView.DisplayStartTime = scheduleView.StartTime;
                    DateTime dateTime = (DateTime)(DateTime.Now.Date + scheduleView.StartTime);
                    string timeZoneID = scheduleView.TimeZone;
                    TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneID);
                    DateTime dat = TimeZoneInfo.ConvertTime(dateTime, timeZone);
                    string strTime = dat.TimeOfDay.ToString();
                    TimeSpan ts = TimeSpan.Parse(strTime.Substring(0, 8));
                    scheduleView.StartTime = ts;
                    ScheduleReportViewData scheduleViewData = ClearDay(scheduleView);

                    _logMessage.Append("calling ToScheduleDto method to map ScheduleReportDto to ScheduleReportViewData and retrun ScheduleReportDto");

                    ScheduleReportDto scheduleDto = scheduleViewData.ToScheduleDto();

                    _logMessage.Append("ToScheduleDto method executed successfully");

                    scheduleDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                    _logMessage.Append("calling Update method of  _scheduleService takes scheduleDto parameters are " +
                                       scheduleDto.WeekDay + ", " + scheduleDto.ScheduleID + ", " + scheduleDto.MonthDay +
                                       ", " + scheduleDto + ", " + scheduleDto.ModifiedDate + ", " +
                                       scheduleDto.IsCreated + ", " + scheduleDto.Frequency + ", " + scheduleDto.DateTo +
                                       ", " + scheduleDto.CreatedDate);

                    // Update Admin
                    RecipientDto rd = new RecipientDto();
                    AdminRecipientListDto arl = new AdminRecipientListDto();
                    arl.AdminList = scheduleView.AdminList.ToDtoList();
                    bool resultAdmin = _scheduleService.UpdateAdminRecipient(arl);
            
                    // Update Merchant
                    rd.MerchantList = scheduleView.MerchantList.ToMerchantListDto();
                    bool result = _scheduleService.UpdateRecipient(rd);
                    _scheduleService.Update(scheduleDto);

                    if (result)
                    {
                        ViewBag.Message = MtdataResource.Updated_successfully;

                        _logMessage.Append("Update method of  _scheduleService executed successfully");

                        string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                        _logTracking.Append("User " + userName + " update schedule Id" + scheduleDto.ScheduleID + " at " + System.DateTime.Now.ToUniversalTime());
                        _logger.LogInfoMessage(_logTracking.ToString());
                    }

                    return RedirectToAction("Display");
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            _logMessage.Append("calling FrequencyList,  FrequencyTypeListmethod,TimeZoneList method of same class");

            scheduleView.FrequencyList = FrequencyList();
            scheduleView.FrequencyTyeList = FrequencyTypeList();
            scheduleView.TimeZones = TimeZoneList();
            scheduleView.DayList = WeekDayList();
            scheduleView.MonthDayList = MonthDayList();
            scheduleView.ReportList = ReportListReadOnly();
            
            foreach (var item in scheduleView.AdminList)
            {
                item.ModifiedBy = item.Email;
            }
            
            foreach (var itemMerchant in scheduleView.MerchantList)
            {
                itemMerchant.ModifiedBy = itemMerchant.Email;
            }
            
            _logMessage.Append("FrequencyList,  FrequencyTypeListmethod,TimeZoneList methods executed successfully");

            return View(scheduleView);
        }

        /// <summary>
        ///  Purpose            : To updated Recipients
        /// Function Name       :   UpdateRecipient
        /// Created By          :   Naveen Kumar
        /// Created On          :   2/2/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UpdateRecipient()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("calling GetRecipientList of  _commonService");

                var recipientDto = _commonService.GetRecipientList();
                
                _logMessage.Append("GetRecipientList of  _commonService executed successfully");
                _logMessage.Append("calling ToMerchantViewDataSelectList method to map MerchantListViewData to MerchantListDto and retrun MerchantListViewData");

                var recipient = recipientDto.ToMerchantViewDataSelectList();

                _logMessage.Append("ToMerchantViewDataSelectList method executed successfully");

                RecipientViewData recipientView = new RecipientViewData { MerchantList = recipient };

                return View(recipientView);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            : To Update Recipients
        /// Function Name       :   UpdateRecipient
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/3/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="recipient"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateRecipient(RecipientViewData recipient)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("calling ToRecipientDto method to map RecipientDto to RecipientViewData and retrun RecipientDto");

                var recipientDto = recipient.ToRecipientDto();

                _logMessage.Append("ToRecipientDto method executed successfully");

                recipientDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                bool result = _scheduleService.UpdateRecipient(recipientDto);
                if (result)
                {
                    ViewBag.Message = MtdataResource.Recipient_list_updated_successfully;

                    _logMessage.Append("calling GetRecipientList of  _commonService");

                    var recipientListDto = _commonService.GetRecipientList();
                    
                    _logMessage.Append("GetRecipientList of  _commonService executed successfully");
                    _logMessage.Append("calling ToMerchantViewDataSelectList method to map MerchantListViewData to MerchantListDto and retrun MerchantListViewData");

                    var recipientList = recipientListDto.ToMerchantViewDataSelectList();

                    _logMessage.Append("ToMerchantViewDataSelectList method executed successfully");

                    RecipientViewData recipientView = new RecipientViewData { MerchantList = recipientList };

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + " update recepient at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                    
                    return View(recipientView);
                }

                return RedirectToAction("UpdateRecipient");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            : To get the frequency list
        /// Function Name       :   FrequencyList
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/3/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<FrequencyListViewData> FrequencyList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("creating frequencyList with assigned FrequencyListViewData ");

                List<FrequencyListViewData> frequencyList = new List<FrequencyListViewData>
                {
                    new FrequencyListViewData{FrequencyId=0,Name="--Select Frequency--"},
                    new FrequencyListViewData {FrequencyId = 1, Name = "Daily"},
                    new FrequencyListViewData {FrequencyId = 2, Name = "Weekly"},
                    new FrequencyListViewData {FrequencyId = 3, Name = "Monthly"}
                };

                _logMessage.Append("returning frequencyList");

                return frequencyList;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

        /// <summary>
        ///  Purpose            : To get the list of types of frequency 
        /// Function Name       :   FrequencyTypeList
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/4/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<FrequencyTypeViewData> FrequencyTypeList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                
                List<FrequencyTypeViewData> frequencyList = new List<FrequencyTypeViewData>();
                return frequencyList;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

        /// <summary>
        ///  Purpose            : to get the list of time zone
        /// Function Name       :   TimeZoneList
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/4/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public List<TimeZoneViewData> TimeZoneList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("geting GetSystemTimeZones from TimeZoneInfo class and assign it into TimeZoneViewData and creating a list into timeZone");

                var timeZone = TimeZoneInfo.GetSystemTimeZones()
                        .Select(z => new TimeZoneViewData { TimeZoneName = z.DisplayName, TimeZoneTime = z.Id })
                        .ToList();

                _logMessage.Append("list of timeZone created successfully");
                _logMessage.Append("returing list of timezones");

                return timeZone;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

        /// <summary>
        ///  Purpose            : to find the frequency 
        /// Function Name       :   FindFrequency
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/5/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="scheduleViewdata"></param>
        [NonAction]
        public void FindFrequency(ScheduleReportDto scheduleDto)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("assinging frequency to scheduleViewdata");

                switch (scheduleDto.Frequency)
                {
                    case 1:
                        scheduleDto.FrequencyName = "Daily";
                        break;
                    case 2:
                        scheduleDto.FrequencyName = "Weekly";
                        break;
                    case 3:
                        scheduleDto.FrequencyName = "Monthly";
                        break;
                    case 4:
                        scheduleDto.FrequencyName = "Yearly";
                        break;
                }

                _logMessage.Append("frequencies assigned to scheduleViewdata");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
        }

        /// <summary>
        ///  Purpose            : to get the list of week day
        /// Function Name       :   WeekDayList
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/5/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<WeekDayViewData> WeekDayList()
        {
            List<WeekDayViewData> dayList = new List<WeekDayViewData>
            {
                new WeekDayViewData {DayID = 1, DayName = "Sunday"},
                new WeekDayViewData {DayID = 2, DayName = "Monday"},
                new WeekDayViewData {DayID = 4, DayName = "Tuesday"},
                new WeekDayViewData {DayID = 8, DayName = "Wednesday"},
                new WeekDayViewData {DayID = 16, DayName = "Thursday"},
                new WeekDayViewData {DayID = 32, DayName = "Friday"},
                new WeekDayViewData {DayID = 64, DayName = "Saturday"}
            };

            return dayList;
        }

        /// <summary>
        ///  Purpose            : to get the list of month days
        /// Function Name       :   MonthDayList
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/5/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public static IEnumerable<MonthDayViewData> MonthDayList()
        {
            List<MonthDayViewData> monthDayList = new List<MonthDayViewData>();
            for (int index = 1; index <= 31; index++)
            {
                monthDayList.Add(new MonthDayViewData { MonthID = index, MonthName = index.ToString(CultureInfo.InvariantCulture) });
            }

            return monthDayList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<ReportListViewData> ReportList()
        {
            var reportList = _scheduleService.ReportList();
            var reportViewData = reportList.ToReportList();

            return reportViewData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<ReportListViewData> ReportListReadOnly()
        {
            var reportList = _scheduleService.ReportListReadOnly();
            var reportViewData = reportList.ToReportList();

            return reportViewData;
        }

        /// <summary>
        ///  Purpose            : to clear week day and month day
        /// Function Name       :   ClearDay
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/6/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns></returns>
        [NonAction]
        public static ScheduleReportViewData ClearDay(ScheduleReportViewData schedule)
        {
            if (schedule.Frequency == 1)
            {
                schedule.WeekDay = null;
                schedule.MonthDay = null;
            }

            if (schedule.Frequency == 2)
                schedule.MonthDay = null;

            if (schedule.Frequency == 3)
                schedule.WeekDay = null;

            return schedule;
        }

        /// <summary>
        ///  Purpose            : To find week day
        /// Function Name       :   FindWeekDay
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/6/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="scheduleViewdata"></param>
        [NonAction]
        public static void FindWeekDay(ScheduleReportDto scheduleDto)
        {
            switch (scheduleDto.WeekDay)
            {
                case 1:
                    scheduleDto.WeekName = "Sunday";
                    break;
                case 2:
                    scheduleDto.WeekName = "Monday";
                    break;
                case 4:
                    scheduleDto.WeekName = "Tuesday";
                    break;
                case 8:
                    scheduleDto.WeekName = "Wednesday";
                    break;
                case 16:
                    scheduleDto.WeekName = "Thursday";
                    break;
                case 32:
                    scheduleDto.WeekName = "Friday";
                    break;
                case 64:
                    scheduleDto.WeekName = "Saturday";
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult ScheduleReportListByFiter(string name = null, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling ScheduleReportListByFiter method of adminUserService class to get Admin Users List");

                int adminUserCount;
                name = name.Trim();
                var scheduleReport = _scheduleService.ScheduleReportListByFilter(name, jtStartIndex, jtPageSize, jtSorting);

                foreach (var sc in scheduleReport)
                {
                    sc.StartTime = sc.DisplayStartTime;
                    TimeSpan ts = (TimeSpan)sc.StartTime;
                    int hours = ts.Hours;
                    double min = ts.Minutes;
                    string str = hours.ToString() + ':' + min.ToString("00");
                    sc.TimeInString = str;
                    TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(sc.TimeZone);
                    sc.TimeZone = timeZone.ToString();
                    FindFrequency(sc);

                    if (sc.Frequency == 2)
                        FindWeekDay(sc);
                }

                _logMessage.Append("GetAdminUserListByFilter of adminUserService executed successfully");

                if (string.IsNullOrEmpty(name))
                {
                    _logMessage.Append("Calling AdminUserListCount method of adminUserService class to count adminUsers");

                    adminUserCount = scheduleReport.ToList().Count;

                    _logMessage.Append("AdminUserListCount method of adminUserService executed successfully result = " + adminUserCount);

                    return Json(new { Result = "OK", Records = scheduleReport, TotalRecordCount = adminUserCount });
                }

                var userDtos = scheduleReport as ScheduleReportDto[] ?? scheduleReport.ToArray();

                adminUserCount = userDtos.ToList().Count();

                return Json(new { Result = "OK", Records = userDtos, TotalRecordCount = adminUserCount });
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public JsonResult Delete(int ScheduleID)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return Json(new { Result = "No", Message = "Not Authorized" });
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling Delete method of scheduleService class takes ScheduleID= " + ScheduleID);

                _scheduleService.Delete(ScheduleID);

                _logMessage.Append("Delete method of scheduleService class executed successfully");

                string userName = ((SessionDto)Session["User"]).SessionUser.Email;

                _logTracking.Append("User " + userName + " delete schedule Id " + ScheduleID + " at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());

                return Json(new { Result = "OK" });
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return Json(new { Result = "No" });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public JsonResult GetNotification(ScheduleReportViewData schedule)
        {
            try
            {
                DateTime dateTime = (DateTime)(schedule.DateFrom + schedule.StartTime);
                string timeZoneID = schedule.TimeZone;
                TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneID);
                DateTime startTime = (DateTime)(schedule.DateFrom + schedule.StartTime);
                DateTime currentTime = DateTime.Now;
                currentTime = TimeZoneInfo.ConvertTime(currentTime, timeZone);
                int result = DateTime.Compare(currentTime, startTime);
                string message = string.Empty;
                string setfrequency = string.Empty.ToString();
                string dateto = ((DateTime)schedule.DateTo).ToString("MM/dd/yyyy");
                string containDatefrom = schedule.DateFrom.ToString();
                string containDateTo = schedule.DateTo.ToString();
                DateTime nextDay = DateTime.Now;
                DayOfWeek next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), "Sunday", true);
                string day = string.Empty;

                switch (schedule.Frequency)
                {
                    case 1:
                        if (result > 0)//past
                        {
                            if (currentTime.TimeOfDay < (TimeSpan)schedule.StartTime)
                            {
                                setfrequency = "every day";
                                nextDay = currentTime;
                                containDatefrom = nextDay.AddDays(-((int)schedule.FrequencyType)).ToString("MM/dd/yyyy");
                                containDateTo = nextDay.AddDays(Convert.ToDouble(-1)).ToString("MM/dd/yyyy");
                            }

                            if (currentTime.TimeOfDay > (TimeSpan)schedule.StartTime)
                            {
                                setfrequency = "every day";
                                nextDay = currentTime.AddDays(1);
                                containDatefrom = nextDay.AddDays(-((int)schedule.FrequencyType)).ToString("MM/dd/yyyy");
                                containDateTo = nextDay.AddDays(-1).ToString("MM/dd/yyyy");
                            }
                        }

                        if (result <= 0)//future
                        {
                            if ((currentTime.Date == startTime.Date))
                            {
                                if (currentTime.TimeOfDay < (TimeSpan)schedule.StartTime)
                                {
                                    setfrequency = "every day";
                                    nextDay = startTime;
                                    containDatefrom = startTime.AddDays(-((int)schedule.FrequencyType)).ToString("MM/dd/yyyy");
                                    containDateTo = nextDay.AddDays(-1).ToString("MM/dd/yyyy");
                                }

                                if (currentTime.TimeOfDay > (TimeSpan)schedule.StartTime)
                                {
                                    setfrequency = "every day";
                                    nextDay = startTime.AddDays(1);
                                    containDatefrom = startTime.AddDays(-((int)schedule.FrequencyType)).ToString("MM/dd/yyyy");
                                    containDateTo = nextDay.AddDays(-1).ToString("MM/dd/yyyy");
                                }
                            }

                            if ((currentTime.Date < startTime.Date))
                            {
                                setfrequency = "every day";
                                nextDay = startTime;
                                containDatefrom = nextDay.AddDays(-((int)schedule.FrequencyType)).ToString("MM/dd/yyyy");
                                containDateTo = nextDay.AddDays(-1).ToString("MM/dd/yyyy");
                            }
                        }
                        break;

                    case 2:
                        if (result > 0)//past
                        {
                            if (currentTime.TimeOfDay > (TimeSpan)schedule.StartTime)
                            {
                                setfrequency = "every week";
                                day = schedule.WeekName;
                                next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), day, true);
                                currentTime = currentTime.AddDays(1);
                                nextDay = GetNextWeekday(Convert.ToDateTime(currentTime.Date), next);
                                DateTime lastSaturday = nextDay.AddDays(-((int)nextDay.DayOfWeek) - 1);
                                DateTime lastSunday = lastSaturday.AddDays(-6);
                                containDatefrom = lastSunday.AddDays(-Convert.ToDouble(7 * (schedule.FrequencyType - 1))).ToString("MM/dd/yyyy");
                                if (schedule.FrequencyType == 1)
                                    containDatefrom = lastSunday.ToString("MM/dd/yyyy");
                                containDateTo = lastSaturday.ToString("MM/dd/yyyy");
                            }
                            else
                            {
                                setfrequency = "every week";
                                day = schedule.WeekName;
                                next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), day, true);
                                nextDay = GetNextWeekday(Convert.ToDateTime(currentTime.Date), next);
                                DateTime lastSaturday = nextDay.AddDays(-((int)nextDay.DayOfWeek) - 1);
                                DateTime lastSunday = lastSaturday.AddDays(-6);
                                containDatefrom = lastSunday.AddDays(-Convert.ToDouble(7 * (schedule.FrequencyType - 1))).ToString("MM/dd/yyyy");
                                if (schedule.FrequencyType == 1)
                                    containDatefrom = lastSunday.ToString("MM/dd/yyyy");
                                containDateTo = lastSaturday.ToString("MM/dd/yyyy");
                            }
                        }

                        if (result <= 0)//future
                        {
                            if ((currentTime.Date == startTime.Date))
                            {
                                if (currentTime.TimeOfDay > (TimeSpan)schedule.StartTime)
                                {
                                    setfrequency = "every week";
                                    day = schedule.WeekName;
                                    next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), day, true);
                                    currentTime = currentTime.AddDays(1);
                                    nextDay = GetNextWeekday(Convert.ToDateTime(currentTime.Date), next);
                                    DateTime lastSaturday = nextDay.AddDays(-((int)nextDay.DayOfWeek) - 1);
                                    DateTime lastSunday = lastSaturday.AddDays(-6);
                                    containDatefrom = lastSunday.AddDays(-Convert.ToDouble(7 * (schedule.FrequencyType - 1))).ToString("MM/dd/yyyy");
                                    if (schedule.FrequencyType == 1)
                                        containDatefrom = lastSunday.ToString("MM/dd/yyyy");
                                    containDateTo = lastSaturday.ToString("MM/dd/yyyy");
                                }
                                else
                                {
                                    if (currentTime.TimeOfDay < (TimeSpan)schedule.StartTime && currentTime.Date == startTime.Date)
                                    {
                                        setfrequency = "every week";
                                        day = schedule.WeekName;
                                        next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), day, true);
                                        nextDay = startTime;
                                        DateTime lastSaturday = nextDay.AddDays(-((int)nextDay.DayOfWeek) - 1);
                                        DateTime lastSunday = lastSaturday.AddDays(-6);
                                        containDatefrom = lastSunday.AddDays(-Convert.ToDouble(7 * (schedule.FrequencyType - 1))).ToString("MM/dd/yyyy");
                                        if (schedule.FrequencyType == 1)
                                            containDatefrom = lastSunday.ToString("MM/dd/yyyy");
                                        containDateTo = lastSaturday.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        setfrequency = "every week";
                                        day = schedule.WeekName;
                                        next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), day, true);
                                        nextDay = GetNextWeekday(Convert.ToDateTime(currentTime.Date), next);
                                        DateTime lastSaturday = nextDay.AddDays(-((int)nextDay.DayOfWeek) - 1);
                                        DateTime lastSunday = lastSaturday.AddDays(-6);
                                        containDatefrom = lastSunday.AddDays(-Convert.ToDouble(7 * (schedule.FrequencyType - 1))).ToString("MM/dd/yyyy");
                                        if (schedule.FrequencyType == 1)
                                            containDatefrom = lastSunday.ToString("MM/dd/yyyy");
                                        containDateTo = lastSaturday.ToString("MM/dd/yyyy");
                                    }
                                }
                            }

                            if ((currentTime.Date < startTime.Date))
                            {
                                setfrequency = "every week";
                                day = schedule.WeekName;
                                next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), day, true);
                                nextDay = GetNextWeekday(Convert.ToDateTime(startTime.Date), next);
                                DateTime lastSaturday = nextDay.AddDays(-((int)nextDay.DayOfWeek) - 1);
                                DateTime lastSunday = lastSaturday.AddDays(-6);
                                containDatefrom = lastSunday.AddDays(-Convert.ToDouble(7 * (schedule.FrequencyType - 1))).ToString("MM/dd/yyyy");

                                if (schedule.FrequencyType == 1)
                                    containDatefrom = lastSunday.ToString("MM/dd/yyyy");

                                containDateTo = lastSaturday.ToString("MM/dd/yyyy");
                            }
                        }

                        break;

                    case 3:
                        setfrequency = "every month";
                        int days = (int)schedule.MonthDay;
                        int totalDays = NoOFDays(currentTime);
                        int monthFrequency = (int)schedule.FrequencyType;

                        if (result > 0)//past
                        {
                            if (currentTime.TimeOfDay > (TimeSpan)schedule.StartTime)
                            {
                                if (days <= totalDays)
                                {
                                    if (days > currentTime.Day)
                                    {
                                        nextDay = dateTime;
                                        nextDay = new DateTime(currentTime.Year, currentTime.Month, days) + (TimeSpan)schedule.StartTime;
                                        DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - 1, 1);
                                        containDatefrom = new DateTime(nextDay.Year, nextDay.Month - monthFrequency, 1).ToString("MM/dd/yyyy");
                                        int lastMonthDays = NoOFDays(lastMonth.Date);
                                        containDateTo = new DateTime(nextDay.Year, nextDay.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        nextDay = dateTime;
                                        nextDay = new DateTime(currentTime.Year, currentTime.Month + 1, days) + (TimeSpan)schedule.StartTime;
                                        DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - 1, 1);
                                        containDatefrom = new DateTime(nextDay.Year, nextDay.Month - monthFrequency, 1).ToString("MM/dd/yyyy");
                                        int lastMonthDays = NoOFDays(lastMonth.Date);
                                        containDateTo = new DateTime(nextDay.Year, nextDay.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                                    }
                                }
                            }
                            else// go for next month////
                            {
                                nextDay = dateTime;
                                nextDay = new DateTime(currentTime.Year, currentTime.Month + 1, days) + (TimeSpan)schedule.StartTime;
                                DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - monthFrequency, 1);
                                containDatefrom = new DateTime(nextDay.Year, nextDay.Month - 1, 1).ToString("MM/dd/yyyy");
                                int lastMonthDays = NoOFDays(lastMonth.Date);
                                containDateTo = new DateTime(nextDay.Year, nextDay.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                            }
                        }

                        if (result <= 0)//future
                        {
                            if (days == startTime.Day && currentTime.TimeOfDay < (TimeSpan)schedule.StartTime)
                            {
                                if (days <= totalDays)
                                {
                                    if (days > currentTime.Day)
                                    {
                                        nextDay = dateTime;
                                        nextDay = new DateTime(startTime.Year, currentTime.Month, days) + (TimeSpan)schedule.StartTime;
                                        DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - 1, 1);
                                        containDatefrom = new DateTime(nextDay.Year, startTime.Month - monthFrequency, 1).ToString("MM/dd/yyyy");
                                        int lastMonthDays = NoOFDays(lastMonth.Date);
                                        containDateTo = new DateTime(nextDay.Year, nextDay.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        nextDay = dateTime;
                                        nextDay = startTime;
                                        DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - 1, 1);
                                        containDatefrom = new DateTime(nextDay.Year, startTime.Month - monthFrequency, 1).ToString("MM/dd/yyyy");
                                        int lastMonthDays = NoOFDays(lastMonth.Date);
                                        containDateTo = new DateTime(nextDay.Year, startTime.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                                    }
                                }
                            }
                            else if (days > startTime.Day)
                            {
                                nextDay = dateTime;
                                nextDay = new DateTime(currentTime.Year, startTime.Month, days) + (TimeSpan)schedule.StartTime;
                                DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - 1, 1);
                                containDatefrom = new DateTime(startTime.Year, startTime.Month - monthFrequency, 1).ToString("MM/dd/yyyy");
                                int lastMonthDays = NoOFDays(lastMonth.Date);
                                containDateTo = new DateTime(nextDay.Year, nextDay.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                            }
                            else// go for next month
                            {
                                nextDay = dateTime;
                                nextDay = new DateTime(currentTime.Year, startTime.Month + 1, days) + (TimeSpan)schedule.StartTime;
                                DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - 1, 1);
                                containDatefrom = new DateTime(startTime.Year, startTime.Month - monthFrequency, 1).ToString("MM/dd/yyyy");
                                int lastMonthDays = NoOFDays(lastMonth.Date);
                                containDateTo = new DateTime(nextDay.Year, nextDay.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                            }
                        }
                        break;
                }
                
                nextDay = nextDay.Date + ((TimeSpan)schedule.StartTime);
                string nextDate = nextDay.ToString();
                DateTime datTo = (DateTime)schedule.DateTo;
                
                if (nextDay > datTo)
                {
                    message = " Next Report can not be scheduled on" + nextDate + " as schedule Date to - " + dateto + " is earliear than next scheduled date. ";
                    return Json(message, JsonRequestBehavior.AllowGet);
                }
                
                message = " * Report will be scheduled " + setfrequency + " between " + schedule.DateFrom + " to " + dateto + " 11:59 PM";
                message += ". </br>Next report will be scheduled for  " + nextDate + " and will contain data between " + containDatefrom + " 12 :00 AM to  " + containDateTo + " 11:59 PM";
                message += " based on " + timeZone + " Time Zone.";
                
                return Json(message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), e);
            }

            return Json(null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="start"></param>
        /// <param name="day"></param>
        /// <returns></returns>
        public static DateTime GetNextWeekday(DateTime start, DayOfWeek day)
        {
            int daysToAdd = ((int)day - (int)start.DayOfWeek + 7) % 7;
            return start.AddDays(daysToAdd);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static int NoOFDays(DateTime date)
        {
            int month = date.Month;
            int year = date.Year;
            int daysInMonth = DateTime.DaysInMonth(year, month);
            return daysInMonth;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CorporateReport()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                int corporateUserId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.UserID);
                var recipientDto = _commonService.GetCorporateUserList(corporateUserId);
                var recipient = recipientDto.ToMerchantViewDataSelectList();

                ScheduleReportViewData scheduleReport = new ScheduleReportViewData
                {
                    FrequencyList = FrequencyList(),
                    TimeZones = TimeZoneList(),
                    DayList = WeekDayList(),
                    MonthDayList = MonthDayList(),
                    ReportList = CorporateReportList(corporateUserId),
                    Frequency = Convert.ToByte(1),
                    MerchantList = recipient
                };

                _logMessage.Append("calling FrequencyList method of same class");

                scheduleReport.FrequencyTyeList = FrequencyTypeList();

                _logMessage.Append("FrequencyList methods executed successfully");

                scheduleReport.DateFrom = DateTime.Now.Date;

                return View(scheduleReport);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scheduleViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CorporateReport(ScheduleReportViewData scheduleViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("index", "Member");
            
            int corporateUserId = ((SessionDto)Session["User"]).SessionUser.UserID;
            string modifiedBy = ((SessionDto)Session["User"]).SessionUser.Email;
            
            try
            {
                if ((scheduleViewData.Frequency == 2) && (scheduleViewData.WeekDay == null))
                {
                    ModelState.AddModelError("WeekDay", MtdataResource.Please_select_week_day);
                }

                if ((scheduleViewData.Frequency == 3) && (scheduleViewData.MonthDay == null))
                {
                    ModelState.AddModelError("MonthDay", MtdataResource.Please_select_month_day);
                }
                
                if (scheduleViewData.DateFrom > scheduleViewData.DateTo)
                {
                    ModelState.AddModelError("DateFrom", MtdataResource.Date_from_validation);
                }

                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (ModelState.IsValid)
                {
                    scheduleViewData.DisplayStartTime = scheduleViewData.StartTime;
                    DateTime dateTime = (DateTime)(DateTime.Now.Date + scheduleViewData.StartTime);
                    string timeZoneID = scheduleViewData.TimeZone;
                    TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneID);
                    DateTime dat = TimeZoneInfo.ConvertTime(dateTime, timeZone);
                    string strTime = dat.TimeOfDay.ToString();
                    TimeSpan ts = TimeSpan.Parse(strTime.Substring(0, 8));
                    scheduleViewData.StartTime = ts;
                    ScheduleReportViewData scheduleView = ClearDay(scheduleViewData);

                    _logMessage.Append("calling ToScheduleDto method to map ScheduleReportDto to ScheduleReportViewData and return ScheduleReportDto");

                    var scheduleDto = scheduleView.ToScheduleDto();

                    _logMessage.Append("ToScheduleDto method executed successfully");

                    scheduleDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                    
                    _logMessage.Append("calling Add method of  _scheduleService takes scheduleDto parameters are " +
                                       scheduleDto.WeekDay + ", " + scheduleDto.ScheduleID + ", " + scheduleDto.MonthDay +
                                       ", " + scheduleDto + ", " + scheduleDto.ModifiedDate + ", " +
                                       scheduleDto.IsCreated + ", " + scheduleDto.Frequency + ", " + scheduleDto.DateTo +
                                       ", " + scheduleDto.CreatedDate);

                    RecipientDto recipientDto = new RecipientDto();
                    recipientDto.ReportId = (int)scheduleViewData.fk_ReportID;
                    recipientDto.MerchantList = scheduleViewData.MerchantList.ToMerchantListDto();
                    ScheduleReportViewData scheduleViewReport = new ScheduleReportViewData();
                    scheduleViewReport = scheduleViewData;
                    scheduleViewReport.StartTime = scheduleViewData.DisplayStartTime;
                    scheduleDto.NextScheduleDateTime = GetNotify(scheduleViewReport).Date + scheduleViewData.DisplayStartTime;
                    _scheduleService.UpdateCorporateUserRecipient(recipientDto, corporateUserId, modifiedBy);
                    scheduleDto.CorporateUserId = corporateUserId;
                    _scheduleService.AddCorporateUser(scheduleDto);

                    _logMessage.Append("Add method of  _scheduleService executed successfully");

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + " add schedule report Id" + scheduleDto.fk_ReportID + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return RedirectToAction("CorporateReportDisplay");
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            _logMessage.Append("calling FrequencyList,  FrequencyTypeListmethod,TimeZoneList method of same class");

            scheduleViewData.FrequencyList = FrequencyList();
            scheduleViewData.FrequencyTyeList = FrequencyTypeList();
            scheduleViewData.TimeZones = TimeZoneList();
            scheduleViewData.ReportList = CorporateReportList(corporateUserId);
            scheduleViewData.DayList = WeekDayList();
            scheduleViewData.MonthDayList = MonthDayList();
            
            var rec = _commonService.GetCorporateUserList(corporateUserId);
            scheduleViewData.MerchantList = rec.ToMerchantViewDataSelectList();

            _logMessage.Append("FrequencyList,  FrequencyTypeListmethod,TimeZoneList methods executed successfully");
            
            return View(scheduleViewData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scheduleId"></param>
        /// <param name="reportId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UpdateCorporateReport(int scheduleId, int reportId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                int CorporateUserId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.UserID);

                ScheduleReportViewData scheduleReport = _scheduleService.GetScheduleDto(scheduleId).ToScheduleViewData();
                scheduleReport.FrequencyList = FrequencyList();
                scheduleReport.FrequencyTyeList = FrequencyTypeList();
                scheduleReport.TimeZones = TimeZoneList();
                scheduleReport.DayList = WeekDayList();
                scheduleReport.MonthDayList = MonthDayList();
                scheduleReport.ReportList = ReportListReadOnly();
                scheduleReport.MerchantList = _commonService.GetUpdatedCorporateUserList(reportId, CorporateUserId).ToMerchantViewDataSelectList();
                
                _logMessage.Append("calling FrequencyList method of same class");

                scheduleReport.FrequencyTyeList = FrequencyTypeList();

                _logMessage.Append("FrequencyList methods executed successfully");

                scheduleReport.DateFrom = DateTime.Now.Date;
                scheduleReport.StartTime = scheduleReport.DisplayStartTime;

                return View(scheduleReport);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        [HttpPost]
        public ActionResult UpdateCorporateReport(ScheduleReportViewData scheduleViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("index", "Member");

            int corporateUserId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.UserID);
            string modifiedBy = ((SessionDto)Session["User"]).SessionUser.Email;
            try
            {
                if ((scheduleViewData.Frequency == 2) && (scheduleViewData.WeekDay == null))
                {
                    ModelState.AddModelError("WeekDay", MtdataResource.Please_select_week_day);
                }

                if ((scheduleViewData.Frequency == 3) && (scheduleViewData.MonthDay == null))
                {
                    ModelState.AddModelError("MonthDay", MtdataResource.Please_select_month_day);
                }

                if (scheduleViewData.DateFrom > scheduleViewData.DateTo)
                    ModelState.AddModelError("DateFrom", MtdataResource.Date_from_validation);

                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (ModelState.IsValid)
                {
                    scheduleViewData.DisplayStartTime = scheduleViewData.StartTime;
                    DateTime dateTime = (DateTime)(DateTime.Now.Date + scheduleViewData.StartTime);
                    string timeZoneID = scheduleViewData.TimeZone;
                    TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneID);
                    DateTime dat = TimeZoneInfo.ConvertTime(dateTime, timeZone);
                    string strTime = dat.TimeOfDay.ToString();
                    TimeSpan ts = TimeSpan.Parse(strTime.Substring(0, 8));
                    scheduleViewData.StartTime = ts;
                    ScheduleReportViewData scheduleView = ClearDay(scheduleViewData);

                    _logMessage.Append("calling ToScheduleDto method to map ScheduleReportDto to ScheduleReportViewData and return ScheduleReportDto");
                    
                    var scheduleDto = scheduleView.ToScheduleDto();
                    
                    _logMessage.Append("ToScheduleDto method executed successfully");

                    scheduleDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                    _logMessage.Append("calling Add method of  _scheduleService takes scheduleDto parameters are " +
                                       scheduleDto.WeekDay + ", " + scheduleDto.ScheduleID + ", " + scheduleDto.MonthDay +
                                       ", " + scheduleDto + ", " + scheduleDto.ModifiedDate + ", " +
                                       scheduleDto.IsCreated + ", " + scheduleDto.Frequency + ", " + scheduleDto.DateTo +
                                       ", " + scheduleDto.CreatedDate);

                    RecipientDto recipientDto = new RecipientDto();
                    recipientDto.ReportId = (int)scheduleViewData.fk_ReportID;
                    recipientDto.MerchantList = scheduleViewData.MerchantList.ToMerchantListDto();
                    ScheduleReportViewData scheduleViewReport = new ScheduleReportViewData();
                    scheduleViewReport = scheduleViewData;
                    scheduleViewReport.StartTime = scheduleViewData.DisplayStartTime;
                    scheduleDto.NextScheduleDateTime = GetNotify(scheduleViewReport).Date + scheduleViewData.DisplayStartTime;
                    _scheduleService.UpdateCorporateUserRecipient(recipientDto, corporateUserId, modifiedBy);
                    scheduleDto.CorporateUserId = corporateUserId;
                    _scheduleService.UpdateCorporate(scheduleDto);

                    _logMessage.Append("Add method of  _scheduleService executed successfully");
                    
                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + " add schedule report Id" + scheduleDto.fk_ReportID + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                    
                    return RedirectToAction("CorporateReportDisplay");
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            _logMessage.Append("calling FrequencyList,  FrequencyTypeListmethod,TimeZoneList method of same class");

            scheduleViewData.FrequencyList = FrequencyList();
            scheduleViewData.FrequencyTyeList = FrequencyTypeList();
            scheduleViewData.TimeZones = TimeZoneList();
            scheduleViewData.ReportList = ReportListReadOnly();
            scheduleViewData.DayList = WeekDayList();
            scheduleViewData.MonthDayList = MonthDayList();

            _logMessage.Append("FrequencyList,  FrequencyTypeListmethod,TimeZoneList methods executed successfully");
            
            return View(scheduleViewData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns></returns>
        [NonAction]
        public DateTime GetNotify(ScheduleReportViewData schedule)
        {
            DateTime nextDay = DateTime.Now;
            try
            {
                DateTime dateTime = (DateTime)(schedule.DateFrom + schedule.StartTime);
                string timeZoneID = schedule.TimeZone;
                TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneID);
                DateTime startTime = (DateTime)(schedule.DateFrom + schedule.StartTime);
                DateTime currentTime = DateTime.Now;
                currentTime = TimeZoneInfo.ConvertTime(currentTime, timeZone);
                int result = DateTime.Compare(currentTime, startTime);
                string setfrequency = string.Empty.ToString();
                string containDatefrom = schedule.DateFrom.ToString();
                string containDateTo = schedule.DateTo.ToString();
                DayOfWeek next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), "Sunday", true);
                string day = string.Empty;

                switch (schedule.Frequency)
                {
                    case 1:
                        if (result > 0)//past
                        {
                            if (currentTime.TimeOfDay < (TimeSpan)schedule.StartTime)
                            {
                                setfrequency = "every day";
                                nextDay = currentTime;
                                containDatefrom = nextDay.AddDays(-((int)schedule.FrequencyType)).ToString("MM/dd/yyyy");
                                containDateTo = nextDay.AddDays(Convert.ToDouble(-1)).ToString("MM/dd/yyyy");
                            }
                            if (currentTime.TimeOfDay > (TimeSpan)schedule.StartTime)
                            {
                                setfrequency = "every day";
                                nextDay = currentTime.AddDays(1);
                                containDatefrom = nextDay.AddDays(-((int)schedule.FrequencyType)).ToString("MM/dd/yyyy");
                                containDateTo = nextDay.AddDays(-1).ToString("MM/dd/yyyy");
                            }
                        }
                        
                        if (result <= 0)//future
                        {
                            if ((currentTime.Date == startTime.Date))
                            {
                                if (currentTime.TimeOfDay < (TimeSpan)schedule.StartTime)
                                {
                                    setfrequency = "every day";
                                    nextDay = startTime;
                                    containDatefrom = startTime.AddDays(-((int)schedule.FrequencyType)).ToString("MM/dd/yyyy");
                                    containDateTo = nextDay.AddDays(-1).ToString("MM/dd/yyyy");
                                }
                                
                                if (currentTime.TimeOfDay > (TimeSpan)schedule.StartTime)
                                {
                                    setfrequency = "every day";
                                    nextDay = startTime.AddDays(1);
                                    containDatefrom = startTime.AddDays(-((int)schedule.FrequencyType)).ToString("MM/dd/yyyy");
                                    containDateTo = nextDay.AddDays(-1).ToString("MM/dd/yyyy");
                                }
                            }

                            if ((currentTime.Date < startTime.Date))
                            {
                                setfrequency = "every day";
                                nextDay = startTime;
                                containDatefrom = nextDay.AddDays(-((int)schedule.FrequencyType)).ToString("MM/dd/yyyy");
                                containDateTo = nextDay.AddDays(-1).ToString("MM/dd/yyyy");
                            }
                        }

                        break;

                    case 2:
                        if (result > 0)//past
                        {
                            if (currentTime.TimeOfDay > (TimeSpan)schedule.StartTime)
                            {
                                setfrequency = "every week";
                                day = schedule.WeekName;
                                next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), day, true);
                                currentTime = currentTime.AddDays(1);
                                nextDay = GetNextWeekday(Convert.ToDateTime(currentTime.Date), next);
                                DateTime lastSaturday = nextDay.AddDays(-((int)nextDay.DayOfWeek) - 1);
                                DateTime lastSunday = lastSaturday.AddDays(-6);
                                containDatefrom = lastSunday.AddDays(-Convert.ToDouble(7 * (schedule.FrequencyType - 1))).ToString("MM/dd/yyyy");
                                if (schedule.FrequencyType == 1)
                                    containDatefrom = lastSunday.ToString("MM/dd/yyyy");
                                containDateTo = lastSaturday.ToString("MM/dd/yyyy");
                            }
                            else
                            {
                                setfrequency = "every week";
                                day = schedule.WeekName;
                                next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), day, true);
                                nextDay = GetNextWeekday(Convert.ToDateTime(currentTime.Date), next);
                                DateTime lastSaturday = nextDay.AddDays(-((int)nextDay.DayOfWeek) - 1);
                                DateTime lastSunday = lastSaturday.AddDays(-6);
                                containDatefrom = lastSunday.AddDays(-Convert.ToDouble(7 * (schedule.FrequencyType - 1))).ToString("MM/dd/yyyy");
                                if (schedule.FrequencyType == 1)
                                    containDatefrom = lastSunday.ToString("MM/dd/yyyy");
                                containDateTo = lastSaturday.ToString("MM/dd/yyyy");
                            }
                        }

                        if (result <= 0)//future
                        {
                            if ((currentTime.Date == startTime.Date))
                            {
                                if (currentTime.TimeOfDay > (TimeSpan)schedule.StartTime)
                                {
                                    setfrequency = "every week";
                                    day = schedule.WeekName;
                                    next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), day, true);
                                    currentTime = currentTime.AddDays(1);
                                    nextDay = GetNextWeekday(Convert.ToDateTime(currentTime.Date), next);
                                    DateTime lastSaturday = nextDay.AddDays(-((int)nextDay.DayOfWeek) - 1);
                                    DateTime lastSunday = lastSaturday.AddDays(-6);
                                    containDatefrom = lastSunday.AddDays(-Convert.ToDouble(7 * (schedule.FrequencyType - 1))).ToString("MM/dd/yyyy");
                                    if (schedule.FrequencyType == 1)
                                        containDatefrom = lastSunday.ToString("MM/dd/yyyy");
                                    containDateTo = lastSaturday.ToString("MM/dd/yyyy");
                                }
                                else
                                {
                                    if (currentTime.TimeOfDay < (TimeSpan)schedule.StartTime && currentTime.Date == startTime.Date)
                                    {
                                        setfrequency = "every week";
                                        day = schedule.WeekName;
                                        next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), day, true);
                                        nextDay = startTime;
                                        DateTime lastSaturday = nextDay.AddDays(-((int)nextDay.DayOfWeek) - 1);
                                        DateTime lastSunday = lastSaturday.AddDays(-6);
                                        containDatefrom = lastSunday.AddDays(-Convert.ToDouble(7 * (schedule.FrequencyType - 1))).ToString("MM/dd/yyyy");
                                        if (schedule.FrequencyType == 1)
                                            containDatefrom = lastSunday.ToString("MM/dd/yyyy");
                                        containDateTo = lastSaturday.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        setfrequency = "every week";
                                        day = schedule.WeekName;
                                        next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), day, true);
                                        nextDay = GetNextWeekday(Convert.ToDateTime(currentTime.Date), next);
                                        DateTime lastSaturday = nextDay.AddDays(-((int)nextDay.DayOfWeek) - 1);
                                        DateTime lastSunday = lastSaturday.AddDays(-6);
                                        containDatefrom = lastSunday.AddDays(-Convert.ToDouble(7 * (schedule.FrequencyType - 1))).ToString("MM/dd/yyyy");
                                        if (schedule.FrequencyType == 1)
                                            containDatefrom = lastSunday.ToString("MM/dd/yyyy");
                                        containDateTo = lastSaturday.ToString("MM/dd/yyyy");
                                    }
                                }
                            }

                            if ((currentTime.Date < startTime.Date))
                            {
                                setfrequency = "every week";
                                day = schedule.WeekName;
                                next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), day, true);
                                nextDay = GetNextWeekday(Convert.ToDateTime(startTime.Date), next);
                                DateTime lastSaturday = nextDay.AddDays(-((int)nextDay.DayOfWeek) - 1);
                                DateTime lastSunday = lastSaturday.AddDays(-6);
                                containDatefrom = lastSunday.AddDays(-Convert.ToDouble(7 * (schedule.FrequencyType - 1))).ToString("MM/dd/yyyy");
                                if (schedule.FrequencyType == 1)
                                    containDatefrom = lastSunday.ToString("MM/dd/yyyy");
                                containDateTo = lastSaturday.ToString("MM/dd/yyyy");
                            }
                        }

                        break;

                    case 3:
                        setfrequency = "every month";
                        int days = (int)schedule.MonthDay;
                        int totalDays = NoOFDays(currentTime);
                        int monthFrequency = (int)schedule.FrequencyType;

                        if (result > 0)//past
                        {
                            if (currentTime.TimeOfDay > (TimeSpan)schedule.StartTime)
                            {
                                if (days <= totalDays)
                                {
                                    if (days > currentTime.Day)
                                    {
                                        nextDay = dateTime;
                                        nextDay = new DateTime(currentTime.Year, currentTime.Month, days) + (TimeSpan)schedule.StartTime;
                                        DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - 1, 1);
                                        containDatefrom = new DateTime(nextDay.Year, nextDay.Month - monthFrequency, 1).ToString("MM/dd/yyyy");
                                        int lastMonthDays = NoOFDays(lastMonth.Date);
                                        containDateTo = new DateTime(nextDay.Year, nextDay.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        nextDay = dateTime;
                                        nextDay = new DateTime(currentTime.Year, currentTime.Month + 1, days) + (TimeSpan)schedule.StartTime;
                                        DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - 1, 1);
                                        containDatefrom = new DateTime(nextDay.Year, nextDay.Month - monthFrequency, 1).ToString("MM/dd/yyyy");
                                        int lastMonthDays = NoOFDays(lastMonth.Date);
                                        containDateTo = new DateTime(nextDay.Year, nextDay.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                                    }
                                }
                            }
                            else// go for next month////
                            {
                                nextDay = dateTime;
                                nextDay = new DateTime(currentTime.Year, currentTime.Month + 1, days) + (TimeSpan)schedule.StartTime;
                                DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - monthFrequency, 1);
                                containDatefrom = new DateTime(nextDay.Year, nextDay.Month - 1, 1).ToString("MM/dd/yyyy");
                                int lastMonthDays = NoOFDays(lastMonth.Date);
                                containDateTo = new DateTime(nextDay.Year, nextDay.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                            }
                        }

                        if (result <= 0)//future
                        {
                            if (days == startTime.Day && currentTime.TimeOfDay < (TimeSpan)schedule.StartTime)
                            {
                                if (days <= totalDays)
                                {
                                    if (days > currentTime.Day)
                                    {
                                        nextDay = dateTime;
                                        nextDay = new DateTime(startTime.Year, currentTime.Month, days) + (TimeSpan)schedule.StartTime;
                                        DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - 1, 1);
                                        containDatefrom = new DateTime(nextDay.Year, startTime.Month - monthFrequency, 1).ToString("MM/dd/yyyy");
                                        int lastMonthDays = NoOFDays(lastMonth.Date);
                                        containDateTo = new DateTime(nextDay.Year, nextDay.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        nextDay = dateTime;
                                        nextDay = startTime;
                                        DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - 1, 1);
                                        containDatefrom = new DateTime(nextDay.Year, startTime.Month - monthFrequency, 1).ToString("MM/dd/yyyy");
                                        int lastMonthDays = NoOFDays(lastMonth.Date);
                                        containDateTo = new DateTime(nextDay.Year, startTime.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                                    }
                                }
                            }
                            else if (days > startTime.Day)
                            {
                                nextDay = dateTime;
                                nextDay = new DateTime(currentTime.Year, startTime.Month, days) + (TimeSpan)schedule.StartTime;
                                DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - 1, 1);
                                containDatefrom = new DateTime(startTime.Year, startTime.Month - monthFrequency, 1).ToString("MM/dd/yyyy");
                                int lastMonthDays = NoOFDays(lastMonth.Date);
                                containDateTo = new DateTime(nextDay.Year, nextDay.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                            }
                            else// go for next month
                            {
                                nextDay = dateTime;
                                nextDay = new DateTime(currentTime.Year, startTime.Month + 1, days) + (TimeSpan)schedule.StartTime;
                                DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - 1, 1);
                                containDatefrom = new DateTime(startTime.Year, startTime.Month - monthFrequency, 1).ToString("MM/dd/yyyy");
                                int lastMonthDays = NoOFDays(lastMonth.Date);
                                containDateTo = new DateTime(nextDay.Year, nextDay.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                            }
                        }
                        break;
                }

                nextDay = nextDay.Date;
                return nextDay;
            }
            catch (Exception e)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), e);
            }

            return nextDay;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="corporateUserId"></param>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<ReportListViewData> CorporateReportList(int corporateUserId)
        {
            var reportList = _scheduleService.CorporateReportList(corporateUserId);
            var reportViewData = reportList.ToReportList();
            return reportViewData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CorporateReportDisplay()
        {
            if ((SessionDto)Session["User"] == null) return RedirectToAction("Index", "Member");
            int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || userType != 5) return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                return View();
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult CorporateReportListByFiter(string name = null, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling ScheduleReportListByFiter method of adminUserService class to get Admin Users List");

                int adminUserCount;
                name = name.Trim();
                int corporateUserId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.UserID));
                
                FilterSearchParametersDto filterSearch = new FilterSearchParametersDto()
                {
                    Name = name,
                    StartIndex = jtStartIndex,
                    PageSize = jtPageSize,
                    Sorting = jtSorting,
                    CorporateId = corporateUserId
                };

                var scheduleReport = _scheduleService.ScheduleReportListByFilter(filterSearch, jtSorting);

                foreach (var sc in scheduleReport)
                {
                    sc.StartTime = sc.DisplayStartTime;
                    TimeSpan ts = (TimeSpan)sc.StartTime;
                    int hours = ts.Hours;
                    double min = ts.Minutes;
                    string str = hours.ToString() + ':' + min.ToString("00");
                    sc.TimeInString = str;
                    TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(sc.TimeZone);
                    sc.TimeZone = timeZone.ToString();
                    FindFrequency(sc);
                    if (sc.Frequency == 2)
                        FindWeekDay(sc);
                }

                _logMessage.Append("GetAdminUserListByFilter of adminUserService executed successfully");

                if (string.IsNullOrEmpty(name))
                {
                    _logMessage.Append("Calling AdminUserListCount method of adminUserService class to count adminUsers");

                    adminUserCount = scheduleReport.ToList().Count;

                    _logMessage.Append("AdminUserListCount method of adminUserService executed successfully result = " + adminUserCount);

                    return Json(new { Result = "OK", Records = scheduleReport, TotalRecordCount = adminUserCount });
                }

                var userDtos = scheduleReport as ScheduleReportDto[] ?? scheduleReport.ToArray();
                adminUserCount = userDtos.ToList().Count();

                return Json(new { Result = "OK", Records = userDtos, TotalRecordCount = adminUserCount });
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ScheduleID"></param>
        /// <returns></returns>
        public JsonResult DeleteCorporateReport(int ScheduleID)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return Json(new { Result = "No", Message = "Not Authorized" });
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling Delete method of scheduleService class takes ScheduleID= " + ScheduleID);

                _scheduleService.DeleteCorporate(ScheduleID);

                _logMessage.Append("Delete method of scheduleService class executed successfully");
                
                string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                _logTracking.Append("User " + userName + " delete schedule Id " + ScheduleID + " at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                
                return Json(new { Result = "OK" });
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return Json(new { Result = "No" });
        }

        public JsonResult WeeklyReportRecipients(int reportId)
        {
            ScheduleReportViewData scheduleReport = new ScheduleReportViewData();
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var adminRecipientDto = _commonService.WeeklyReportRecipients(reportId);
                var adminRecipient = adminRecipientDto.ToMerchantViewDataSelectList();

                scheduleReport.AdminList = adminRecipient;
                scheduleReport.IsCreated = true;

                return Json(scheduleReport, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return Json(scheduleReport, JsonRequestBehavior.AllowGet);
        }

        public ActionResult WeeklyRptRecipient()
        {
            ScheduleReportViewData scheduleReport = new ScheduleReportViewData();
            scheduleReport.ReportList = WeeklyReports();
            return View(scheduleReport);
        }

        [HttpPost]
        public ActionResult WeeklyRptRecipient(ScheduleReportViewData scheduleViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("index", "Member");

            try
            {    
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));                   
                _logMessage.Append("calling ToScheduleDto method to map ScheduleReportDto to ScheduleReportViewData and return ScheduleReportDto");

                var scheduleDto = scheduleViewData.ToScheduleDto();

                _logMessage.Append("ToScheduleDto method executed successfully");

                scheduleDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                
                _logMessage.Append("calling Add method of  _scheduleService takes scheduleDto parameters are " +
                                    scheduleDto.WeekDay + ", " + scheduleDto.ScheduleID + ", " + scheduleDto.MonthDay +
                                    ", " + scheduleDto + ", " + scheduleDto.ModifiedDate + ", " +
                                    scheduleDto.IsCreated + ", " + scheduleDto.Frequency + ", " + scheduleDto.DateTo +
                                    ", " + scheduleDto.CreatedDate);

                AdminRecipientListDto arl = new AdminRecipientListDto();
                arl.AdminList = scheduleViewData.AdminList.ToDtoList();
                
                foreach (var item in arl.AdminList)
                {
                    item.fk_ReportID = Convert.ToInt32(scheduleViewData.fk_ReportID);
                }

                bool resultAdmin = _scheduleService.UpdateAdminRecipient(arl);
                ViewBag.Msg="Data Saved Successfully";
                ViewBag.IsSetDropDown = 1;
                scheduleViewData.ReportList = WeeklyReports();

                return View(scheduleViewData);             
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            scheduleViewData.ReportList = WeeklyReports();
            return View(scheduleViewData);
        }

        [NonAction]
        public List<ReportListViewData> WeeklyReports()
        {
            return new List<ReportListViewData> { 
                new ReportListViewData { ReportId = 0, Name = "--Select Report--", DisplayName = "--Select Report--" },
                new ReportListViewData { ReportId = 27, Name = "Weekly Summary Count Report", DisplayName = "Weekly Summary Count Report" },
                                                  new ReportListViewData { ReportId = 28, Name = "Weekly Summary Dollar Volume Report", DisplayName = "Weekly Summary Dollar Volume Report" },
                                                  new ReportListViewData { ReportId = 29, Name = "Monthly Graph Count Volume Report", DisplayName = "Monthly Graph Count Volume Report" },
                                                  new ReportListViewData { ReportId = 30, Name = "Monthly Graph Dollar Volume Report", DisplayName = "Monthly Graph Dollar Volume Report" },
            };
        }

    }

}
