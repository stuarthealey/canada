
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_AdminExceptionReportScheduler]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_AdminExceptionReportScheduler]
GO

/****** Object:  StoredProcedure [dbo].[usp_AdminExceptionReportScheduler]    Script Date: 11/2/2015 10:59:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <12-06-2015>
-- Description:	<Prepare and Send exception reports to admin users>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AdminExceptionReportScheduler]
AS
BEGIN TRY
 SET NOCOUNT ON;
 SET ANSI_WARNINGS OFF
 DECLARE @Res INT,
 @UserId INT,
 @To VARCHAR(50),
 @Body NVARCHAR(MAX),
 @Error VARCHAR(200),
 @MailId INT,
 @Valid INT,
 @MsgXml NVARCHAR(40),
 @XmlData NVARCHAR(MAX),
 @UserName VARCHAR(50),
 @ReportType INT,
 @RepType VARCHAR(50),
 @RepHeading VARCHAR(200),
 @FDate DATETIME,
 @TDate DATETIME
 --Declaring cursor for fetching admin recipients for exception report
 DECLARE Queue_Cursor CURSOR FOR 
 SELECT u.UserID,u.Email FROM Users u INNER JOIN Recipient r ON u.UserID=r.fk_UserId WHERE r.IsRecipient=1 
 AND r.fk_UserTypeId=1 AND  r.fk_ReportId=3  AND u.IsActive=1 AND u.fk_UserTypeID=1 
 OPEN Queue_Cursor
 FETCH NEXT FROM Queue_Cursor INTO @UserId,@To
 WHILE @@FETCH_STATUS = 0   
 BEGIN   
 SELECT @UserName=FName FROM Users WHERE UserID=@UserId
--Exceute store procedure AdminExceptionReport to get exception data in xml format
 EXEC usp_AdminExceptionReport @XmlData OUTPUT,@ReportType OUTPUT,@FDate OUTPUT,@TDate OUTPUT
 SET  @RepType=CASE @ReportType WHEN 1 THEN 'Daily Exception Report' WHEN 2 THEN 'Weekly Exception Report' ELSE 'Monthly Exception Report' END
 SET @RepHeading=@RepType+' '+'(Dated from'+' '+ CONVERT(VARCHAR(11),@FDate,106)+' '+'to'+' '+CONVERT(VARCHAR(11),@TDate,106)+')'
 IF(@XmlData IS NULL or  @XmlData='') BEGIN
 SET @Valid=0
 INSERT INTO MailMessage(RepUserId,[Status],SendTo,Body,RepUserTypeId,[Date],ReportType) VALUES(@UserId,-1,@To,'No records in database',1,GETDATE(),'AdminExceptionReport')
 END
 ELSE BEGIN
 SET @Valid=1
 END
 IF(@Valid=1) BEGIN
 SET @MsgXml=CAST((select concat('Dear',' ',@UserName,',') FOR XML PATH('p'), ELEMENTS ) AS NVARCHAR(MAX))
--Set data in html format
 SET @Body ='<html><style>td{text-align:left;font-family:verdana;font-size:12px}p{font-family:verdana;font-size:13px}
 span{font-family:verdana;font-size:13px}</style>
 <body><p>Listed below is the exception report containing fleet exception details corresponding to merchants.</p>
 <H5 style="color:grey;text-align:center;font-size:11px;font-family:verdana">'+@RepHeading+'</H5>
 <table border = 1 cellpadding="0" cellspacing="0" style="background-color:white;border-collapse:collapse"> <tr>
 <th style="color:grey;font-size:13px;width:600px;text-align:left">Merchant</th>
 <th style="color:grey;font-size:13px;width:600px;text-align:left">Fleet</th>
 <th style="color:grey;font-size:13px;width:600px;text-align:left">Exp Fleet Trans</th>
 <th style="color:grey;font-size:13px;width:500px;text-align:left">Act Fleet Trans</th>
 <th style="color:grey;font-size:13px;width:200px;text-align:left">Exp Car</th>
 <th style="color:grey;font-size:13px;width:200px;left-align:center">Act Car</th>
 <th style="color:grey;font-size:13px;width:300px;text-align:left">Exp  Value</th>
 <th style="color:grey;font-size:13px;width:300px;text-align:left">Act Trans Value</th>
  <th style="color:grey;font-size:13px;width:800px;text-align:left">Exp Case</th></tr>'    
 SET @Body = @MsgXml+@Body + @XmlData +'</table><br/><br/><span>Thanks & Regards,<br/><br/>MTData Developments Pty Ltd.
 <br/>www.mtdata.com<span><h4 style="color:grey;font-size:12px;font-family:verdana">MTData-All rights reserved</h4>
 <p style="font-size:11px;color:black">Note:This is an automated message. Do not Reply or Reply All to this message as 
 replies are not being accepted.</p></body></html>'
 INSERT INTO MailMessage(RepUserId,[Status],SendTo,Body,RepUserTypeId,[Date],ReportType) VALUES(@UserId,0,@To,@Body,1,GETDATE(),'AdminExceptionReport')
 SET @MailId=@@IDENTITY
 EXEC @Res=msdb.dbo.sp_send_dbmail
 @profile_name = 'MTData',
 @recipients =@To,
 @body =@Body,
 @subject = 'Admin Exception Report',
 @body_format ='HTML'
 IF (@Res != 0)
 BEGIN
   SET @Error =@@ERROR
   UPDATE MailMessage set Error=@Error,[Date]=GETDATE() WHERE MailId=@MailId
 END;
 ELSE
 BEGIN
   UPDATE MailMessage set [Status]=1,[Date]=GETDATE() where MailId=@MailId
 END
END
FETCH NEXT FROM Queue_Cursor INTO @UserId,@To
END   
CLOSE Queue_Cursor   
DEALLOCATE Queue_Cursor
END TRY
--use catch to log error in the table  ProcErrorHandler
BEGIN CATCH
       INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
       SELECT ERROR_NUMBER()
	         ,ERROR_SEVERITY()
			 ,ERROR_STATE()
			 ,ERROR_PROCEDURE()
			 ,ERROR_LINE()
			 ,ERROR_MESSAGE()
			 ,SUSER_SNAME()
			 ,HOST_NAME()
			 ,GETDATE()
END CATCH

GO

