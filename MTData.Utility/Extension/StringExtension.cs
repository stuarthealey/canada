﻿using System;
using System.Configuration;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Runtime.Serialization.Formatters.Soap;

namespace MTData.Utility
{    
    public static class StringExtension
    {
        public const int SaltByteSize = 24;
        public const int HashByteSize = 24;
        public const int Iterations = 400000;
        public const int IterationIndex = 0;
        public const int SaltIndex = 1;
        public const int PkdfIndex = 2;
        public const string yy = "20";

        /// <summary>
        /// Purpose             :   Creates hash of the password with salt
        /// Function Name       :   Hash
        /// Created By          :   
        /// Created On          :   01/08/2015
        /// Modification Made   :   ****************************
        /// Modified On         :   ****************************
        /// </summary>
        /// <param name="password">The password to hash.</param>
        /// <returns>The hash of the password.</returns>
        public static string Hash(this string password)
        {
            RNGCryptoServiceProvider cryptoService = new RNGCryptoServiceProvider();
            byte[] salt = new byte[SaltByteSize];
            cryptoService.GetBytes(salt);
            byte[] hash = HashBytes(password, salt, Iterations, HashByteSize);

            string hashPassword = Iterations + ":" + Convert.ToBase64String(salt) + ":" + Convert.ToBase64String(hash);

            cryptoService.Dispose();

            return hashPassword;
        }

        /// <summary>
        /// Purpose             :   Returns SHA1 hash
        /// Function Name       :   HashBytes
        /// Created By          :   
        /// Created On          :   01/08/2015
        /// Modification Made   :   ****************************
        /// Modified On         :   ****************************
        /// </summary>
        /// <param name="password"></param>
        /// <param name="salt"></param>
        /// <param name="iterations"></param>
        /// <param name="outputBytes"></param>
        /// <returns>A hash of the password.</returns>
        private static byte[] HashBytes(string password, byte[] salt, int iterations, int outputBytes)
        {
            PasswordDeriveBytes deriveBytes = new PasswordDeriveBytes(password, salt);
            deriveBytes.IterationCount = iterations;
            byte[] resultBytes = deriveBytes.GetBytes(outputBytes);
            deriveBytes.Dispose();

            return resultBytes;
        }

        /// <summary>
        /// Purpose             :   Validates a password with the given hash password
        /// Function Name       :   ValidatePassCode
        /// Created By          :   
        /// Created On          :   01/08/2015
        /// Modification Made   :   ****************************
        /// Modified On         :   ****************************
        /// </summary>
        /// <param name="password">The password to check.</param>
        /// <param name="correctHash">A hash of the correct password.</param>
        /// <returns>True or False</returns>
        public static bool ValidatePassCode(string password, string correctHash)
        {
            char[] delimiter = { ':' };
            if (correctHash != null)
            {
                string[] split = correctHash.Split(delimiter);
                int iterations = Int32.Parse(split[IterationIndex]);
                byte[] salt = Convert.FromBase64String(split[SaltIndex]);
                byte[] hash = Convert.FromBase64String(split[PkdfIndex]);
                byte[] testHash = HashBytes(password, salt, iterations, hash.Length);

                return SlowEquals(hash, testHash);
            }

            return false;
        }

        /// <summary>
        /// Purpose             :   To prevent from timing attacks
        /// Function Name       :   SlowEquals
        /// Created By          :  
        /// Created On          :   01/08/2015
        /// Modification Made   :   ****************************
        /// Modified On         :   ****************************
        /// </summary>
        /// <param name="byteArrayHash">The first byte array.</param>
        /// <param name="byteArrayTestHash">The second byte array.</param>
        /// <returns>True or False</returns>
        private static bool SlowEquals(byte[] byteArrayHash, byte[] byteArrayTestHash)
        {
            uint diffResult = (uint)byteArrayHash.Length ^ (uint)byteArrayTestHash.Length;
            for (int index = 0; index < byteArrayHash.Length && index < byteArrayTestHash.Length; index++)
            {
                diffResult |= (uint)(byteArrayHash[index] ^ byteArrayTestHash[index]);
            }

            return diffResult == 0;
        }

        /// <summary>
        /// Purpose             :   Random string generation
        /// Function Name       :   CreateRandomString
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   06/6/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   06/6/2015 "MM/DD/YYYY"   
        /// </summary>
        /// <param name="minLegth"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static string CreateRandomString(int maxSize)
        {
            char[] chars = new char[62];
            chars = "jfd2309au4085aUAa34D@vb#$5n6a#$AfS4!4a5DFKa343!yIa5!sx#$dp9897n#JK!*Ufy%ghAlKt8M75&".ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[maxSize];
                crypto.GetNonZeroBytes(data);
            }

            StringBuilder result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }

            return result.ToString();
        }

        /// <summary>
        /// Purpose             :   Encrypt information
        /// Function Name       :   Encrypt
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   06/6/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   06/6/2015 "MM/DD/YYYY"  
        /// </summary>
        /// <param name="strEncrypt"></param>
        /// <returns></returns>
        public static string Encrypt(this string strEncrypt)
        {
            if (!string.IsNullOrEmpty(strEncrypt))
            {
                byte[] encDataByte = Encoding.UTF8.GetBytes(strEncrypt);
                string encodedData = Convert.ToBase64String(encDataByte);
                return encodedData;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Purpose             :   Decrypt information
        /// Function Name       :   Decrypt
        /// Created By          :   Salil Gupta
        /// Created On          :   06/6/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   06/6/2015 "MM/DD/YYYY"  
        /// </summary>
        /// <param name="strDecrypt"></param>
        /// <returns></returns>
        public static string Decrypt(this string strDecrypt)
        {
            if (!string.IsNullOrEmpty(strDecrypt))
            {
                UTF8Encoding encoder = new UTF8Encoding();
                Decoder utf8Decode = encoder.GetDecoder();
                byte[] todecodeByte = Convert.FromBase64String(strDecrypt);
                int charCount = utf8Decode.GetCharCount(todecodeByte, 0, todecodeByte.Length);
                char[] decodedChar = new char[charCount];
                utf8Decode.GetChars(todecodeByte, 0, todecodeByte.Length, decodedChar, 0);

                string result = new String(decodedChar);
                return result;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Purpose             :   Extracting [0]FirstFour, [1]LastFour, [2]DD, [3]MM
        /// Function Name       :   GetTrackOneData
        /// Created By          :   Sunil Singh
        /// Created On          :   06/6/2015
        /// Modification Made   :   ****************************
        /// Modified On         :   06/6/2015 "MM/DD/YYYY"    
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static CardInfo GetCardNumberData(string param)
        {
            CardInfo result = new CardInfo();

            //PAY-13 As the card number can have a leading ';' character, we need to strip that.
            if (param.StartsWith(";"))
                param = param.TrimStart(';');

            if (param.Length > 8)
            {
                if (param.Contains("="))
                {
                    //string data = param.Substring(1, param.Length - 1);
                    string data = param.Substring(0, param.Length - 1);     //Don't trim the leading character, as we have already done that by dropping the ';'

                    string cardNum = data.Substring(0, data.IndexOf('='));

                    string yy = data.Substring(data.IndexOf('=') + 1, 2);
                    string mm = data.Substring(data.IndexOf('=') + 3, 2);

                    string serviceCode = data.Substring(data.IndexOf('=') + 5, 3);

                    string firstFour = cardNum.Substring(0, 4);
                    string lastFour = cardNum.Substring(cardNum.Length - 4, 4);

                    result.First4 = firstFour;
                    result.Last4 = lastFour;
                    result.CardNumber = cardNum;
                    result.YY = yy;
                    result.MM = mm;
                    result.ServiceCode = serviceCode;
                }
                else
                {
                    string firstFour = param.Substring(0, 4);
                    string lastFour = param.Substring(param.Length - 4, 4);

                    result.First4 = firstFour;
                    result.Last4 = lastFour;
                }
            }
            else
            {
                result.ErrorMessage = "Invalid card number. Length was less than 8 digit.";
            }

            return result;
        }

        /// <summary>
        /// Purpose             :   Extracting Card holder name
        /// Function Name       :   GetTrackOneData
        /// Created By          :   Sunil Singh
        /// Created On          :   06/6/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   06/6/2015 "MM/DD/YYYY"   
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetTrackOneData(string value)
        {
            try
            {
                string response = string.Empty;
                if (!string.IsNullOrEmpty(value))
                {
                    string[] part = null;
                    if (!string.IsNullOrEmpty(value) && value.Contains("^"))
                    {
                        char[] delimiter = { '^' };
                        part = value.Split(delimiter);
                    }

                    if (part != null && part.Length > 1)
                        response = part[1];
                }

                return response;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   Extracting Encrypt Block and KeyId
        /// Function Name       :   GetTrackThreeData
        /// Created By          :   Sunil Singh
        /// Created On          :   06/6/2015
        /// Modification Made   :   ****************************
        /// Modidified On       :   06/6/2015 "MM/DD/YYYY"  
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string[] GetTrackThreeData(string value)
        {
            try
            {
                string[] part = null;
                if (!string.IsNullOrEmpty(value) && value.Contains(":"))
                {
                    char[] delimiter = { ':' };
                    part = value.Split(delimiter);
                }
                return part;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Purpose             :   Converting object to SOAP
        /// Function Name       :   ObjectToSoap
        /// Created By          :   Sunil Singh
        /// Created On          :   06/6/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   06/6/2015 "MM/DD/YYYY"   
        /// </summary>
        /// <param name="Object"></param>
        /// <returns></returns>
        public static string ObjectToSoap(object Object)
        {
            if (Object.IsNull())
                throw new ArgumentException("Object can not be null");

            try
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    SoapFormatter serializer = new SoapFormatter();
                    serializer.Serialize(stream, Object);
                    stream.Flush();
                    string resp = Encoding.UTF8.GetString(stream.GetBuffer(), 0, (int)stream.Position);
                    resp = resp.Replace("&#60;", "<").Replace("&#62;", ">").Replace("&#34;", "\"");

                    return resp;
                }
            }
            catch { throw; }
        }

        /// <summary>
        /// Purpose             :   Check Null for object
        /// Function Name       :   IsNull
        /// Created By          :   Sunil Singh
        /// Created On          :   06/6/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   06/6/2015 "MM/DD/YYYY"  
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool IsNull(this object obj)
        {
            return obj == null;
        }

        /// <summary>
        /// Purpose             :   masking carnumber
        /// Function Name       :   MaskCardNumber
        /// Created By          :   Sunil Singh
        /// Created On          :   06/6/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   06/6/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="cardNumber"></param>
        /// <returns></returns>
        public static string MaskCardNumber(this string cardNumber)
        {
            if (!string.IsNullOrEmpty(cardNumber) && cardNumber.Length >= 12 && cardNumber.Length <= 19)
            {
                var firstSixDigits = cardNumber.Substring(0, 6);
                var lastFourDigits = cardNumber.Substring(cardNumber.Length - 4, 4);
                var maskedDigit = new string('0', cardNumber.Length - 10);

                return firstSixDigits + maskedDigit + lastFourDigits;
            }
            else
            {
                var defaultCard = new string('0', 16);
                return defaultCard;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static string First(this string value, int count)
        {
            string result = null;
            if (value != null)
            {
                count = Math.Max(0, count);
                result = value.Substring(0, Math.Min(count, value.Length));
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static string Last(this string value, int count)
        {
            string result = null;
            if (value != null)
            {
                count = Math.Max(0, count);
                result = value.Substring(Math.Max(0, value.Length - count), Math.Min(value.Length, count));
            }

            return result;
        }

        /// <summary>
        /// Purpose             :   Get Pin and Serial number from Pin data. PIn serail data is 36 digit first 16 digits are PIN and last 20 digits are KCN 
        /// Function Name       :   GetPinSerial
        /// Created By          :   Salil Gupta
        /// Created On          :   06/25/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   06/25/2015 "MM/DD/YYYY"  
        /// </summary>
        /// <param name="pinData"></param>
        /// <returns></returns>
        public static string[] GetPinSerial(string pinData)
        {
            string[] result = new string[2];
            if (pinData.Length == 36)
            {
                string debitPin = pinData.Substring(0, 16);
                string serialNumber = pinData.Substring(36 - 20);
                result[0] = debitPin;
                result[1] = serialNumber;
            }

            return result;
        }

        /// <summary>
        /// Purpose             :   Remove CDATA expression from transaction response.
        /// Function Name       :   HandleCdata
        /// Created By          :   Sunil Singh
        /// Created On          :   07/25/2015
        /// Modification Made   :   *************
        /// Modified On       :   "MM/DD/YYYY"  
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string HandleCdata(this string data)
        {
            if (!string.IsNullOrEmpty(data))
            {
                if (data.Contains("CDATA"))
                {
                    int lastIndex = data.IndexOf("CDATA");
                    data = data.Substring(0, lastIndex - 3) + "</RejectResponse></GMF>";
                }
            }

            return data;
        }

        /// <summary>
        /// Purpose             :   To display a message if specified XML element is missing.
        /// Function Name       :   MissingElementMessage
        /// Created By          :   Sunil Singh
        /// Created On          :   08-Sep-2015
        /// Modification Made   :   *************
        /// Modified On       :   "MM/DD/YYYY"  
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static string MissingElementMessage(this string element)
        {
            if (!string.IsNullOrEmpty(element))
                element = "Element " + element + " is missing from request xml data";

            return element;
        }

        /// <summary>
        /// Purpose             :   To display a message if specified XML element is missing.
        /// Function Name       :   MissingElementMessage
        /// Created By          :   Sunil Singh
        /// Created On          :   08-Sep-2015
        /// Modification Made   :   *************
        /// Modified On       :   "MM/DD/YYYY"  
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static string InvalidFormat(this string element)
        {
            if (!string.IsNullOrEmpty(element))
                element = "Element " + element + " has invalid data fromat";

            return element;
        }

        /// <summary>
        /// Purpose             :   To make TerminalID of length 8 by prifixing 0.
        /// Function Name       :   FormatTerminalId
        /// Created By          :   Sunil Singh
        /// Created On          :   09-Sep-2015
        /// Modification Made   :   *************
        /// Modified On       :   "MM/DD/YYYY"  
        /// </summary>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        public static string FormatTerminalId(this string terminalId)
        {
            if (!string.IsNullOrEmpty(terminalId))
            {
                int len = terminalId.Length;
                string prefix = new string('0', 8 - len);

                terminalId = prefix + terminalId;
            }

            return terminalId;
        }

        /// <summary>
        /// Purpose             :   To genererate token
        /// Function Name       :   GenerateToken
        /// Created By          :   
        /// Created On          :   09-Sep-2015
        /// Modification Made   :   *************
        /// Modified On       :   "MM/DD/YYYY"  
        /// </summary>
        /// <param name="driverNo"></param>
        ///  <param name="driverPin"></param>
        /// <returns></returns>
        public static string GenerateToken(string driverNo, string driverPin)
        {
            Guid g = Guid.NewGuid();
            string guidString = Convert.ToBase64String(g.ToByteArray());
            guidString = guidString.Replace("=", "");
            guidString = guidString.Replace("+", "");

            string encryptedDriver = EncryptDriver(driverNo);
            string encryptedPin = EncryptDriver(driverPin);
            string token = guidString + "^" + encryptedDriver + ":" + encryptedPin;

            return token;
        }

        /// <summary>
        /// Purpose             :   To encrypt driver data
        /// Function Name       :   EncryptDriver
        /// Created By          :  
        /// Created On          :   09-Sep-2015
        /// Modification Made   :   *************
        /// Modified On       :   "MM/DD/YYYY"  
        /// </summary>
        /// <param name="driverData"></param>
        /// <returns></returns>
        public static string EncryptDriver(string driverData)
        {
            string encryptionKey = ConfigurationManager.AppSettings["EncDecryptDriverKey"];
            byte[] clearBytes = Encoding.Unicode.GetBytes(driverData);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes rfcDeriveBytes = new Rfc2898DeriveBytes(encryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = rfcDeriveBytes.GetBytes(32);
                encryptor.IV = rfcDeriveBytes.GetBytes(16);
                rfcDeriveBytes.Dispose();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(clearBytes, 0, clearBytes.Length);
                        cryptoStream.Close();
                    }

                    driverData = Convert.ToBase64String(memoryStream.ToArray());
                }
            }

            return driverData;
        }

        /// <summary>
        /// Purpose             :   To decrypt driver data
        /// Function Name       :   DecryptDriver
        /// Created By          :   
        /// Created On          :   09-Sep-2015
        /// Modification Made   :   *************
        /// Modified On       :   "MM/DD/YYYY"  
        /// </summary>
        /// <param name="cipherText"></param>
        /// <returns></returns>
        public static string DecryptDriver(string cipherText)
        {
            string decryptionKey = ConfigurationManager.AppSettings["EncDecryptDriverKey"];
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes rfc2898DeriveBytes = new Rfc2898DeriveBytes(decryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = rfc2898DeriveBytes.GetBytes(32);
                encryptor.IV = rfc2898DeriveBytes.GetBytes(16);
                rfc2898DeriveBytes.Dispose();
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(cipherBytes, 0, cipherBytes.Length);
                        cryptoStream.Close();
                    }

                    cipherText = Encoding.Unicode.GetString(memoryStream.ToArray());
                }
            }

            return cipherText;
        }

        public static string DateFormat(string mmyy)
        {
            string yyyymm = string.Empty;
            if (mmyy.Length == 4)
            {
                string month = mmyy.Substring(0, 2);
                string year = mmyy.Substring(2, 2);
                yyyymm = yy + year + month;
                yyyymm = Encrypt(yyyymm);
            }

            return yyyymm;
        }
    }
}
