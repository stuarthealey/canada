﻿using System;
using System.Configuration;
using System.Globalization;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web.Configuration;
using System.Xml;
using System.Security.Cryptography;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.WebAPI.Common;
using MTData.WebAPI.Models;
using MTData.WebAPI.Resources;
using MTData.WebAPI.CanadaCert;
using MTData.WebAPI.CanadaCommon;

namespace MTData.WebAPI.Controllers
{
    public class BookerAppTokenController : TransactionRequestController
    {
        #region (Constructor Loading)

        private readonly ITransactionService _transactionService;
        private readonly ITerminalService _terminalService;
        private readonly IMerchantService _merchantService;
        private readonly ICommonService _commonService;

        private LocalVariables objLocalVars = new LocalVariables();
        private readonly StringBuilder _logMessage;
        private StringBuilder _logTracking;
        readonly ILogger _logger = new Logger();

        public BookerAppTokenController(
            [Named("TransactionService")] ITransactionService transactionService,
            [Named("MerchantService")] IMerchantService merchantService,
            [Named("TerminalService")] ITerminalService terminalService,
            [Named("CommonService")] ICommonService commonService,
            StringBuilder logMessage, StringBuilder logTracking)
            : base(terminalService, logMessage)
        {
            _merchantService = merchantService;
            _transactionService = transactionService;
            _terminalService = terminalService;
            _logMessage = logMessage;
            _commonService = commonService;
            _logTracking = logTracking;
        }

        fDMerchantDto _fdMerchantobj;
        DatawireDto _datawireObj;

		#endregion

		#region (Create RCCI)

		/// <summary>
		/// Purpose             :   Creating booker app token 
		/// Function Name       :   BookerAppToken
		/// Created By          :   Salil Gupta
		/// Created On          :   07/04/2015
		/// Modification Made   :   Refactored and added Logging, tracing, error handling
		/// Modified By         :   Sunil Singh
		/// Modified At         :   03-Sep-2015
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		public HttpResponseMessage CreateRCCI(HttpRequestMessage request)
        {
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                _logMessage.AppendLine("BookerApp CreateToken process start:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");

			CommonFunctions.WrapSoapArgs soapArgs = new CommonFunctions.WrapSoapArgs()
			{
				soapType = CommonFunctions.SoapWrapType.None,
				soapResponseNode = "CreateRCCIResponse",
				responseNamespace = "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService",
				soapResultNode = "CreateRCCIResult",
				serialisedNodeName = "CreateRCCIResp",

				requestNodeName = "request",
				documentElementName = "CreateRCCIReq"
			};

			/*Token request in xml format */
			CreateRCCIResp tokenResponse = new CreateRCCIResp();
            TransactionRequest tr = new TransactionRequest();
			var resp = new XmlDocument();
			try
			{
                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    _logMessage.AppendLine("[BApp.CreateRCCI] Loading request xml");

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);
                				
				// Check to see if this is still wrapped in the envelope..
				string createRCCIRequest = xmlDoc.InnerXml;
				soapArgs.soapType = CommonFunctions.GetSoapType(xmlDoc);

				if (soapArgs.soapType != CommonFunctions.SoapWrapType.None)
					createRCCIRequest = CommonFunctions.UnwrapSoap(createRCCIRequest, soapArgs);

                //zzz Debug output of request, so we can see what comes in, and how to mimic from test app.
                // Log the received XML, in case we lose everything after this because of an exception.
                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    _logger.LogInfoMessage(string.Format("CreateRCCI() request:{0};RCCIreq:{1};XML:{2};", request.ToString(), createRCCIRequest, xmlDoc.OuterXml));

                // Loading request
                _logMessage.AppendLine("[BApp.CreateRCCI] loading request xml into CreateRCCIReq object");

                var createRCCIReq = (CreateRCCIReq)CommonFunctions.XmlToObject(createRCCIRequest, typeof(CreateRCCIReq));

                // Initializing local variables.
                _logMessage.AppendLine("[BApp.CreateRCCI] Init variables from request xml");

                SetLocalVarFromXMLRequest(createRCCIReq);

                //zzz
                //PAY-12 extract the AVS details from the request, if they exist.
                //createRCCIReq.AVSDetails.Unit
                if (createRCCIReq.AVSDetails != null)
                {
                    _logMessage.AppendLine(string.Format("[BApp.CreateRCCI] AVS details. Unit:{0};Number:{1};Street:{2};Line1:{3};Line2:{4};Suburb:{5};State:{6};Postcode:{7};",
                        createRCCIReq.AVSDetails.Unit, createRCCIReq.AVSDetails.StreetNumber, createRCCIReq.AVSDetails.Street, createRCCIReq.AVSDetails.AddressLine1, 
                        createRCCIReq.AVSDetails.AddressLine2, createRCCIReq.AVSDetails.Suburb, createRCCIReq.AVSDetails.State, createRCCIReq.AVSDetails.Postcode));
                }
                else
                {
                    _logMessage.AppendLine(string.Format("[BApp.CreateRCCI] No Customer AVS details provided."));
                }

                // FirstDataDetails
                // Retreiving firstDataDetails for TokenType
                _logMessage.AppendLine(string.Format("[BApp.CreateRCCI] GetFirstDataDetails() DeviceId:{0};FleetID:{1};RequestType:{2};", objLocalVars.DeviceId, Convert.ToInt32(objLocalVars.FleetId), objLocalVars.RequestType));

                var firstDataDetails = _commonService.GetFirstDataDetails(objLocalVars.DeviceId, (string)null, (string)null, Convert.ToInt32(objLocalVars.FleetId), objLocalVars.RequestType);
                if (firstDataDetails.IsNull())
                {
                    _logMessage.AppendLine("[BApp.CreateRCCI] Failed GetFirstDataDetails()");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    tokenResponse = CommonFunctions.CreateRCCIFailedResp(tr, PaymentAPIResources.InvalidTerminal);
                    resp.InnerXml = CommonFunctions.CreateXml(tokenResponse);

                    return new HttpResponseMessage() { Content = new StringContent(CommonFunctions.WrapSoap(resp, soapArgs), Encoding.UTF8, "application/soap+xml") };
                }
                else
                {
                    _logMessage.AppendLine(string.Format("[BApp.CreateRCCI] FirstData TokenType:{0};", firstDataDetails.TokenType));
                }

                // Terminal
                // Getting terminal detail based on DeviceId
                _logMessage.AppendLine(string.Format("[BApp.CreateRCCI] GetTerminalDto() DeviceID:{0};", objLocalVars.DeviceId));

                var terminal = _terminalService.GetTerminalDto(objLocalVars.DeviceId);
                if (terminal.IsNull())
                {
                    _logMessage.AppendLine("[BApp.CreateRCCI] Failed GetTerminalDto()");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    tokenResponse = CommonFunctions.CreateRCCIFailedResp(tr, PaymentAPIResources.InvalidTerminal);
                    resp.InnerXml = CommonFunctions.CreateXml(tokenResponse);
					
					return new HttpResponseMessage() { Content = new StringContent(CommonFunctions.WrapSoap(resp, soapArgs), Encoding.UTF8, "application/soap+xml") };
                }
                else
                {
                    _logMessage.AppendLine(string.Format("[BApp.CreateRCCI] Terminal MerchantID:{0};DatawireID:{1};SerialNo:{2};", terminal.fk_MerchantID, terminal.DatawireId, terminal.SerialNo));
                }

                // _fdMerchantobj
                // Retriving merchant data from database
                _logMessage.AppendLine(string.Format("[BApp.CreateRCCI] GetFDMerchant() MerchantID:{0};", terminal.fk_MerchantID));
                _fdMerchantobj = _merchantService.GetFDMerchant(terminal.fk_MerchantID);

                if (_fdMerchantobj.IsNull())
                {
                    _logMessage.AppendLine("[BApp.CreateRCCI] Failed GetFDMerchant()");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    tokenResponse = CommonFunctions.CreateRCCIFailedResp(tr, PaymentAPIResources.FdMerchantNotExist);
                    resp.InnerXml = CommonFunctions.CreateXml(tokenResponse);
                    return new HttpResponseMessage() { Content = new StringContent(CommonFunctions.WrapSoap(resp, soapArgs), Encoding.UTF8, "application/soap+xml") };
                }
                else
                {
                    _logMessage.AppendLine(string.Format("[BApp.CreateRCCI] FDMerchant FDMerchantID:{0};ProjectID:{1};GroupID:{2};App:{3};ServiceID:{4};ServiceURL:{5};", _fdMerchantobj.FDMerchantID, _fdMerchantobj.ProjectId, _fdMerchantobj.GroupId, _fdMerchantobj.App, _fdMerchantobj.ServiceId, _fdMerchantobj.ServiceUrl));
                }

                // _datawireObj
                /* Generate Client Ref Number in the format <STAN>|<TPPID>, right justified and left padded with "0" */
                _logMessage.AppendLine(string.Format("[BApp.CreateRCCI] GetDataWireDetails() MerchantID:{0};DatawireID:{1};", terminal.fk_MerchantID, terminal.DatawireId));

                _datawireObj = _merchantService.GetDataWireDetails(terminal.fk_MerchantID, Convert.ToInt32(terminal.DatawireId));

                if (_datawireObj.IsNull())
                {
                    _logMessage.AppendLine("[BApp.CreateRCCI] Failed GetDataWireDetails()");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    tokenResponse = CommonFunctions.CreateRCCIFailedResp(tr, PaymentAPIResources.DatawireDetailsNull);
                    resp.InnerXml = CommonFunctions.CreateXml(tokenResponse);

                    return new HttpResponseMessage() { Content = new StringContent(CommonFunctions.WrapSoap(resp, soapArgs), Encoding.UTF8, "application/soap+xml") };
                }
                else
                {
                    _logMessage.AppendLine(string.Format("[BApp.CreateRCCI] Datawire RCTerminalId:{0};DID:{1};", _datawireObj.RCTerminalId, _datawireObj.DID));
                }

                _logMessage.AppendLine("[BApp.CreateRCCI] Preparing transaction request.");

                // Initializing instance of TransactionRequest
                // Generate transaction data

                tr.FdMerchantId = Convert.ToString(_fdMerchantobj.FDMerchantID); //MTData
                tr.MtdMerchantId = terminal.fk_MerchantID;
                tr.SerialNumber = terminal.SerialNo;
                tr.TerminalId = _datawireObj.RCTerminalId;
                tr.PaymentType = objLocalVars.PayType;
                tr.CardNumber = objLocalVars.Ccno;
                tr.ExpiryDate = objLocalVars.ExpiryDate;
                tr.IndustryType = objLocalVars.IndustryType;
                tr.TppId = _fdMerchantobj.ProjectId;

                //zzzzzz
                //2018-05-23 Set GroupId to be blank, from advice from Datawire.
                // tr.GroupId = Convert.ToString(_fdMerchantobj.GroupId);
                tr.GroupId = string.Empty;

                //zzzzzz
                //2018-05-24 Set DID to be blank, from advice from Datawire.
                tr.Did = _datawireObj.DID;
                // tr.Did = string.Empty;  // Appears that we HAVE to have the DID here to actually authenticate the connection\request.  Try and clear later at the transaction stage.

                tr.App = _fdMerchantobj.App;
                tr.ServiceId = Convert.ToString(_fdMerchantobj.ServiceId);
                tr.ServiceUrl = _fdMerchantobj.ServiceUrl;
                tr.CrdHldrName = objLocalVars.Name;
                tr.LocalDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                tr.TrnmsnDateTime = DateTime.UtcNow.ToString("yyyyMMddHHmmss");
                tr.CrdHldrPhone = objLocalVars.Phone;

                tr.CreatedBy = SigmaStaticValue.BookerAppToken;

                tr.TokenType = firstDataDetails.TokenType;
                tr.CustomerId = objLocalVars.CustomerId;
                tr.EncryptMethod = objLocalVars.EncryptMethod;
                tr.KeyVersion = objLocalVars.KeyVersion;

                tr.IsTransArmor = _merchantService.IsTransArmor(tr.MtdMerchantId);      //PAY-13 Get the Merchant UseTransArmor flag as well.

                CardTokenDto cardTokenDto = null;

                // Prepare and send booker app token only request for canada
                if (WebConfigurationManager.AppSettings["Project"] == PaymentAPIResources.CanadaProject)
                {
                    tr.IndustryType = SigmaStaticValue.BookerAppToken;
                    tr.TransactionType = SigmaStaticValue.TokenOnly;
                    tr.CurrencyCode = PaymentAPIResources.Cur_Cn;

                    // Changes for card registration
                    tr.CardCvv = objLocalVars.CardCvv;
                    tr.ZipCode = objLocalVars.ZipCode;

                    int transactionId;
                    CnBookerAppTokenPayment cnBookerAppTokenPayment = new CnBookerAppTokenPayment(_transactionService, _merchantService, _terminalService, _logMessage);

                    Iso8583Response iso8583Response = cnBookerAppTokenPayment.CnBookerAppTransaction(tr, createRCCIReq, out transactionId, out cardTokenDto);

                    _logMessage.AppendLine(string.Format("[BApp.CreateRCCI] TokenType. tr.TokenType:{0};FDPrivateUsageData63:{1};", tr.TokenType, iso8583Response.FDPrivateUsageData63));

                    //PAY-12 Populate the AVS details of the cardToken object, if it is not NULL
                    if (!object.Equals(cardTokenDto, null))
                    {
                        cardTokenDto.Unit = objLocalVars.AVSUnit;
                        cardTokenDto.StreetNumber = objLocalVars.AVSStreetNumber;
                        cardTokenDto.Street = objLocalVars.AVSStreet;
                        cardTokenDto.AddressLine1 = objLocalVars.AVSAddressLine1;
                        cardTokenDto.AddressLine2 = objLocalVars.AVSAddressLine2;
                        cardTokenDto.Suburb = objLocalVars.AVSSuburb;
                        cardTokenDto.State = objLocalVars.AVSState;
                        cardTokenDto.Postcode = objLocalVars.AVSPostcode;
                    }

                    if (!object.Equals(iso8583Response, null))
                    {
                        tokenResponse = CommonFunctions.CanadaTokenResponse(iso8583Response, tr);

                        _logMessage.AppendLine(string.Format("[BApp.CreateRCCI] ISO8583 ResponseCode:{0};ReturnCode:{1};TokenResponse:{2};AdditionalData:{3};Token:{4};Token.Code:{5};Token.Error:{6}",
                                iso8583Response.ResponseCode, iso8583Response.ReturnCode, tokenResponse.Result.Code, iso8583Response.AdditionalResponseData, iso8583Response.Token, tokenResponse.Result.Code, tokenResponse.Result.ErrorDescription));
                    }
                    else
                    {
                        tokenResponse = CommonFunctions.CanadaFailedTokenResponse(tr);

                        _logMessage.AppendLine(string.Format("[BApp.CreateRCCI] ISO8583 ResponseCode NULL;"));
                    }
                }
                else   // US\other regions payment processing.
                {
                    #region US
                    _logMessage.AppendLine("[BApp.CreateRCCI] Creating US Trans Request.");

                    CreateTransactionRequest(tr);

                    // Get XML Data
                    string xmlSerializedTransReq = GetXmlData();

                    // Save Request into DB
                    // Save transaction into MTDTransaction Table
                    _logMessage.AppendLine("[BApp.CreateRCCI] storing transaction request into database.");

                    MTDTransactionDto transactionDto = CommonFunctions.XmlToTransactionDto(tr);
                    int transId = _transactionService.Add(transactionDto);

                    _terminalService.UpdateStan(transactionDto.Stan, transactionDto.TransRefNo, tr.TerminalId, tr.MtdMerchantId);
                    string clientRef = GetClientRef(_fdMerchantobj.ProjectId);

                    // Save masked card data into database.
                    _logMessage.AppendLine("[BApp.CreateRCCI] managing card token.");
                    cardTokenDto = CommonFunctions.XmlToCardTokenDto(tr);
                    cardTokenDto.Id = _transactionService.ManageCardToken(cardTokenDto);

                    /* Send data using SOAP protocol to Datawire */
                    _logMessage.AppendLine("[BApp.CreateRCCI] sending transaction request ot FirstData.");

                    string xmlSerializedTransResp = new SoapHandler().SendMessage(xmlSerializedTransReq, clientRef, tr);

                    // Update response of transaction
                    _logMessage.AppendLine("BApp.CreateRCCI] updating transaction response into database and send time out transaction if null response is returned from first data");
                    MTDTransactionDto responsetransactionDto;
                    if (!string.IsNullOrEmpty(xmlSerializedTransResp))
                    {
                        responsetransactionDto = CommonFunctions.XmlResponseToTransactionDto(xmlSerializedTransResp, false, tr.PaymentType.ToString(), tr.IndustryType);
                        _transactionService.Update(responsetransactionDto, transId, tr.RapidConnectAuthId);
                    }
                    else
                    {
                        // Sending Timeout Transaction
                        TimeoutTransaction objTimeout = new TimeoutTransaction(_transactionService, _terminalService, _merchantService, _logMessage);
                        xmlSerializedTransResp = objTimeout.TimeOut(tr, transId, clientRef, xmlSerializedTransResp);

                        if (!String.IsNullOrEmpty(xmlSerializedTransResp))
                        {
                            responsetransactionDto = CommonFunctions.XmlResponseToTransactionDto(xmlSerializedTransResp, false, tr.PaymentType.ToString(), tr.IndustryType);
                            _transactionService.Update(responsetransactionDto, transId, tr.RapidConnectAuthId);
                        }
                        else
                        {
                            _logMessage.AppendLine("[BApp.CreateRCCI] Creating US SOAP response ");

                            tokenResponse = CommonFunctions.CreateRCCIFailedResp(new TransactionRequest(), PaymentAPIResources.TimeOut);

                            resp.InnerXml = CommonFunctions.CreateXml(tokenResponse);

                            _logMessage.AppendLine("[BApp.CreateRCCI] Process complete US.");
                            _logger.LogInfoMessage(_logMessage.ToString());

                            return new HttpResponseMessage() { Content = new StringContent(CommonFunctions.WrapSoap(resp, soapArgs), Encoding.UTF8, "application/soap+xml") };
                        }
                    }

                    // Update Card Token response into database.
                    _logMessage.AppendLine("[BApp.CreateRCCI] updating token into database.");
                    if (!string.IsNullOrEmpty(xmlSerializedTransResp) && !string.IsNullOrEmpty(responsetransactionDto.CardToken))
                    {
                        cardTokenDto.Token = responsetransactionDto.CardToken;
                        _transactionService.ManageCardToken(cardTokenDto);
                    }

                    _logMessage.AppendLine("[BApp.CreateRCCI] Binding transaction request.");
                    if (!string.IsNullOrEmpty(xmlSerializedTransResp))
                        tokenResponse = CommonFunctions.CreateRCCIResp(tr, xmlSerializedTransResp);

                    #endregion
                }

                // Output the full Card Token object values now, for debug purposes.
                _logMessage.AppendLine(string.Format("[BApp.CreateRCCI] CardToken:{0};", cardTokenDto.ToString()));

                // POD : return rcci is a combination of CH:<customerID>:<cardToken.Token>
                if (!string.IsNullOrWhiteSpace(tokenResponse.RCCI.Value))
                {
                    tokenResponse.RCCI.Value = string.Format("CH:{0}:{1}", cardTokenDto.CustomerId ?? "", tokenResponse.RCCI.Value);
                    _logMessage.AppendLine(string.Format("[BApp.CreateRCCI] Token RCCI Value:{0};", tokenResponse.RCCI.Value));
                }
                else
                {
                    _logMessage.AppendLine(string.Format("[BApp.CreateRCCI] Token RCCI Value is blank."));
                }

                // Create SOAP response
                _logMessage.AppendLine("[BApp.CreateRCCI] Create SOAP response");

                resp.InnerXml = CommonFunctions.CreateXml(tokenResponse);

                _logMessage.AppendLine(string.Format("[BApp.CreateRCCI] ResponseXML:{0};", resp.InnerXml));
                _logMessage.AppendLine("[BApp.CreateRCCI] Process complete.");
                _logger.LogInfoMessage(_logMessage.ToString());

                return new HttpResponseMessage() { Content = new StringContent(CommonFunctions.WrapSoap(resp, soapArgs), Encoding.UTF8, "application/soap+xml") };
            }
            catch (CryptographicException ex)
            {
                tokenResponse = CommonFunctions.CreateRCCIFailedResp(new TransactionRequest(), PaymentAPIResources.CryptoError);

                resp.InnerXml = CommonFunctions.CreateXml(tokenResponse);

                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                return new HttpResponseMessage() { Content = new StringContent(CommonFunctions.WrapSoap(resp, soapArgs), Encoding.UTF8, "application/soap+xml") };
            }
            catch (Exception ex)
            {
                tokenResponse = CommonFunctions.CreateRCCIFailedResp(new TransactionRequest(), PaymentAPIResources.Exception);

                resp.InnerXml = CommonFunctions.CreateXml(tokenResponse);

                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                return new HttpResponseMessage() { Content = new StringContent(CommonFunctions.WrapSoap(resp, soapArgs), Encoding.UTF8, "application/soap+xml") };
            }
        }

        /// <summary>       
        /// Purpose             :   Initializign local variables.
        /// Function Name       :   SetLocalVarFromXMLRequest
        /// Created By          :   Sunil Singh
        /// Created On          :   03-Sep-2015
        /// Modification Made   :   
        /// Modified By         :   
        /// Modified At         :  
        /// </summary>
        /// <param name="argElements"></param>
        /// <param name="ns2"></param>
        private void SetLocalVarFromXMLRequest(CreateRCCIReq req)
        {
            try
            {
				string deviceLookup = "BookerAppDeviceId";

				if (WebConfigurationManager.AppSettings["Project"] == PaymentAPIResources.CanadaProject)
					deviceLookup = "CnBookerAppDeviceId";

				// Check to see if there is a specific one for the MGID specified..
				// POD : To test differences with tokenisation for additional merchants.
				string deviceID = null;
				if (req.MGID != null && !string.IsNullOrWhiteSpace(req.MGID.Value))
					deviceID = WebConfigurationManager.AppSettings[string.Format("{0}_{1}", deviceLookup, req.MGID.Value)];

				if (string.IsNullOrWhiteSpace(deviceID))
					deviceID = WebConfigurationManager.AppSettings[deviceLookup];

				objLocalVars.DeviceId = deviceID;

                objLocalVars.PayType = (PymtTypeType)Enum.Parse(typeof(PymtTypeType), "Credit");
                objLocalVars.IndustryType = PaymentAPIResources.BookerAppIndustryType;

                // Loading CustomerDetails
                objLocalVars.Name = req.CustomerDetails.FirstName + " " + req.CustomerDetails.Surname;
                objLocalVars.Phone = req.CustomerDetails.ContactPhone;
                objLocalVars.CustomerId = req.CustomerDetails.CustomerId.Value;

                // Loading FullCreditCardDetails
                string yr = DateTime.Now.ToString("yyyy");

                // Loading Encryption node                
                int encMthd = req.Encryption.Method;
                int encKyVrsn = req.Encryption.KeyVersion;
                string encTokn = req.Encryption.Token;

                objLocalVars.EncryptMethod = encMthd;
                objLocalVars.KeyVersion = encKyVrsn;

                if (encMthd != 0)
                {
                    string keyVrsn = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(encKyVrsn).ToLower();
                    AESEncryptionLogic AESLogic = new AESEncryptionLogic(keyVrsn, false);

                    // Decrypting credit card details
                    string expMth;
                    AESLogic.TryDecrypt(req.FullCreditCardDetails.ExpiryMonth, out expMth);

                    string expYr;
                    AESLogic.TryDecrypt(req.FullCreditCardDetails.ExpiryYear, out expYr);

                    string pan;
                    AESLogic.TryDecrypt(req.FullCreditCardDetails.PAN, out pan);

                    objLocalVars.Ccno = pan;
                    objLocalVars.ExpiryDate = yr.Substring(0, 2) + expYr + expMth;
                }
                else
                {
                    objLocalVars.Ccno = req.FullCreditCardDetails.PAN;
                    objLocalVars.ExpiryDate = yr.Substring(0, 2) + req.FullCreditCardDetails.ExpiryYear + req.FullCreditCardDetails.ExpiryMonth;
                }

                // Changes for card registration
                //zzzzzz objLocalVars.CardCvv = req.FullCreditCardDetails.CVV2;
                objLocalVars.ZipCode = req.CustomerDetails.ZipCode;

                //PAY-12 Extract the AVS details from the request
                if (req.AVSDetails != null)
                {
                    objLocalVars.AVSUnit = req.AVSDetails.Unit;
                    objLocalVars.AVSStreetNumber = req.AVSDetails.StreetNumber;
                    objLocalVars.AVSStreet = req.AVSDetails.Street;
                    objLocalVars.AVSAddressLine1 = req.AVSDetails.AddressLine1;
                    objLocalVars.AVSAddressLine2 = req.AVSDetails.AddressLine2;
                    objLocalVars.AVSSuburb = req.AVSDetails.Suburb;
                    objLocalVars.AVSState = req.AVSDetails.State;
                    objLocalVars.AVSPostcode = req.AVSDetails.Postcode;
                }
                else
                {
                    req.AVSDetails = new CustomerAVSDetails();  // At least get the class existing.
                }

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                {
                    _logger.LogInfoMessage(string.Format("BookerAppTokenController.SetLocalVarFromXML() Req:{0};AVS Unit:{1};StreetNumber:{2};Street:{3};Suburb:{4};Postcode:{5};", req.ToString(),
                                            objLocalVars.AVSUnit, objLocalVars.AVSStreetNumber, objLocalVars.AVSStreet, objLocalVars.AVSSuburb, objLocalVars.AVSPostcode));
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(string.Format("BookerAppTokenController.SetLocalVarFromXML() Req:{0};Ex:{1};", req.ToString(), ex.ToString()), ex);

                throw;
            }
        }

        #endregion

        #region Validate RCCI

        /// <summary>
        /// Purpose             :   To validate a card token for BookerApp 
        /// Function Name       :   ValidateRCCI
        /// Created By          :   Sunil Singh
        /// Created On          :   19-Jan-2016
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public HttpResponseMessage ValidateRCCI(HttpRequestMessage request)
        {
            _logMessage.AppendLine("BookerApp CreateToken process start:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");

            /*Token request in xml format */
            ValidateRCCIResp valdtResp = new ValidateRCCIResp();

            try
            {
                var resp = new XmlDocument();

                _logMessage.AppendLine("[BookerApp > ValidateToken] Loading request xml");
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                // Loading request
                _logMessage.AppendLine("[BookerApp > ValidateToken] loading request xml into CreateRCCIReq object");
                var validateRCCIReq = (ValidateRCCIReq)CommonFunctions.XmlToObject(xmlDoc.InnerXml, typeof(ValidateRCCIReq));

                // Initializing local variables.
                _logMessage.AppendLine("[BookerApp > ValidateToken] Setting local variables from RequestXML");
                objLocalVars.CustomerId = validateRCCIReq.CustomerId.Value;
                objLocalVars.Rcci = validateRCCIReq.RCCI.Value;

                _logTracking.Append("BookerApp ValidateToken process start. CustomerId: " + objLocalVars.CustomerId + " RCCI=" + objLocalVars.Rcci + " at " + System.DateTime.Now.ToUniversalTime());

                _logMessage.AppendLine("[BookerApp > ValidateToken] Validating CustomerId & RCCI for null or empty");
                if (string.IsNullOrEmpty(objLocalVars.CustomerId) || string.IsNullOrEmpty(objLocalVars.Rcci))
                {
                    _logMessage.AppendLine("[BookerApp > ValidateToken] CustomerId or RCCI was empty");
                    _logger.LogInfoMessage(_logMessage.ToString());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    var cardStatus = new CreditCardStatus() { IsValid = false, StatusDescription = PaymentAPIResources.EmptyRCCI };
                    valdtResp.CreditCardStatus = cardStatus;
                    resp.InnerXml = CommonFunctions.CreateXml(valdtResp);
                    return new HttpResponseMessage() { Content = new StringContent(resp.InnerXml, Encoding.UTF8, "application/soap+xml") };
                }

                // Fetch CardToken data from DB
                _logMessage.AppendLine("[BookerApp > ValidateToken] Fetching CardToken data from DB");
                var cardTokenData = _transactionService.GetCardToken(objLocalVars.CustomerId, objLocalVars.Rcci);
                if (cardTokenData.IsNull())
                {
                    _logMessage.AppendLine("[BookerApp > ValidateToken] cardTokenData was null");
                    _logger.LogInfoMessage(_logMessage.ToString());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    var cardStatus = new CreditCardStatus() { IsValid = false, StatusDescription = PaymentAPIResources.InvalidRCCI };
                    valdtResp.CreditCardStatus = cardStatus;
                    resp.InnerXml = CommonFunctions.CreateXml(valdtResp);

                    return new HttpResponseMessage() { Content = new StringContent(resp.InnerXml, Encoding.UTF8, "application/soap+xml") };
                }
                else
                {
                    _logMessage.AppendLine("[BookerApp > ValidateToken] CardToken data found, preparing response.");
                    valdtResp = ValidateRCCIResult(cardTokenData);
                    _logMessage.AppendLine("[BookerApp > ValidateToken] CardToken data response prepared. Process complete.");

                    _logger.LogInfoMessage(_logTracking.ToString());
                    _logger.LogInfoMessage(_logMessage.ToString());

                    resp.InnerXml = CommonFunctions.CreateXml(valdtResp);

                    return new HttpResponseMessage() { Content = new StringContent(resp.InnerXml, Encoding.UTF8, "application/soap+xml") };
                }
            }
            catch (CryptographicException ex)
            {
                var cardStatus = new CreditCardStatus() { IsValid = false, StatusDescription = PaymentAPIResources.CryptoError };
                valdtResp.CreditCardStatus = cardStatus;
                var resp = CommonFunctions.CreateXml(valdtResp);

                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                return new HttpResponseMessage() { Content = new StringContent(resp, Encoding.UTF8, "application/soap+xml") };
            }
            catch (Exception ex)
            {
                var cardStatus = new CreditCardStatus() { IsValid = false, StatusDescription = PaymentAPIResources.Exception };
                valdtResp.CreditCardStatus = cardStatus;
                var resp = CommonFunctions.CreateXml(valdtResp);

                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                return new HttpResponseMessage() { Content = new StringContent(resp, Encoding.UTF8, "application/soap+xml") };
            }
        }

        /// <summary>
        /// Creating ValidateRCCI success response
        /// </summary>
        /// <param name="crdTkn"></param>
        /// <returns></returns>
        private ValidateRCCIResp ValidateRCCIResult(CardTokenDto crdTkn)
        {
            _logMessage.AppendLine("[BookerApp > ValidateRCCIResult] initializing TruncatedCreditCardDetails.");
            var resp = new ValidateRCCIResp();
            var crdStts = new CreditCardStatus();
            var tCrd = new TruncatedCreditCardDetails();

            _logMessage.AppendLine("[BookerApp > ValidateRCCIResult] Setting CardType.");
            tCrd.CardType = crdTkn.CardType;

            _logMessage.AppendLine("[BookerApp > ValidateRCCIResult] Setting truncated PAN.");
            var trPan = crdTkn.CardNumber;
            if (!string.IsNullOrEmpty(trPan))
            {
                int len = trPan.Length - 4;
                trPan = new string('*', len) + trPan.Substring(len, 4);
                tCrd.TruncatedPAN = trPan;
            }

            _logMessage.AppendLine("[BookerApp > ValidateRCCIResult] Setting card expiry.");
            var crdExpr = crdTkn.ExpiryDate.Decrypt();
            if (!string.IsNullOrEmpty(crdExpr) && crdExpr.Length >= 6)
            {
                _logMessage.AppendLine("[BookerApp > ValidateRCCIResult] Validating card expiry.");
                int crdExpry = Convert.ToInt32(crdExpr);
                int curntDt = Convert.ToInt32(DateTime.Now.ToString("yyyyMM"));
                if (crdExpry > curntDt)
                {
                    crdStts.IsValid = true;
                    crdStts.StatusDescription = SigmaStaticValue.Registered;
                    //crdStts.StatusDescription = "Registered";
                }
                else
                {
                    crdStts.IsValid = false;
                    crdStts.StatusDescription = SigmaStaticValue.Expired;
                    //crdStts.StatusDescription = "Expired";
                }

                tCrd.ExpiryMonth = crdExpr.Substring(4, 2);
                tCrd.ExpiryYear = crdExpr.Substring(2, 2);
            }

            resp.CreditCardStatus = crdStts;
            resp.TruncatedCreditCardDetails = tCrd;

            return resp;
        } 

        #endregion
    }
}
