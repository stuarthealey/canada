﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using MTData.Web.Resource;

namespace MTData.Web.ViewData
{
    public class UserViewData
    {
        public int UserID { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof (MtdataResource), ErrorMessageResourceName = "Up_to_50_characters_are_allowed")]
        [Required(ErrorMessageResourceType = typeof (MtdataResource), ErrorMessageResourceName = "Please_enter_the_first_name")]
        [RegularExpression(@"^[A-Za-z 0-9]+$", ErrorMessage = "First name  is not valid")]
        [Display(Name = "First name")]
        public string FName { get; set; }

        [Required(ErrorMessageResourceType = typeof (MtdataResource), ErrorMessageResourceName = "Please_enter_the_last_name")]
        [Display(Name = "Last name")]
        [StringLength(50, ErrorMessageResourceType = typeof (MtdataResource), ErrorMessageResourceName = "Up_to_50_characters_are_allowed")]
        [RegularExpression(@"^[A-Za-z 0-9]+$", ErrorMessage = "Last name  is not valid")]
        public string LName { get; set; }

        [Required(ErrorMessageResourceType = typeof (MtdataResource), ErrorMessageResourceName = "Please_enter_the_e_mail_address")]
        [StringLength(50, ErrorMessageResourceType = typeof(MtdataResource), ErrorMessageResourceName = "Up_to_50_characters_are_allowed")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Email is not valid")]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof (MtdataResource), ErrorMessageResourceName = "Please_enter_the_phone_number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Phone  is not valid")]
        [MinLength(6,ErrorMessage = "Phone  must be minimum length of 6")]
        [Display(Name = "Phone")]
        public string Phone { get; set; }

        [Required(ErrorMessageResourceType = typeof (MtdataResource), ErrorMessageResourceName = "Please_select_the_country")]
        [Display(Name = "Country")]
        public string fk_Country { get; set; }

        [Display(Name = "State")]
        public string fk_State { get; set; }

        [Display(Name = "City")]
        public string fk_City { get; set; }
        
        [Required(ErrorMessage = "Please enter the address")]
        [StringLength(250, ErrorMessageResourceType = typeof(MtdataResource), ErrorMessageResourceName = "Address_Range")]
        public string Address { get; set; }

        public string PCode { get; set; }
        public string ConfirmPCode { get; set; }
        public int fk_UserTypeID { get; set; }
        public Nullable<int> fk_MerchantID { get; set; }
        public bool IsLockedOut { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string RolesAssigned { get; set; }
        public IEnumerable<RoleViewData> UserRoles { get; set; }
        public IEnumerable<UserFleetViewData> userFleetList{ get; set; }
        public IEnumerable<FleetViewData> fleetlist { get; set; }
        public List<int> UserRoleIdList { get; set; }
        public List<int> merchantRoles { get; set; }
        public List<int> FleetAssigned { get; set; }
        public IEnumerable<CountryViewData>  CountryList { get; set; }
        public IEnumerable<StateViewData> StateList { get; set; }
        public IEnumerable<CityViewData> CityList { get; set; }
        public IEnumerable<UserTypeViewData> UserTypeList { get; set; }
        public IEnumerable<AllMerchantsViewData> MerchantList { get; set; }
        public IEnumerable<AllMerchantsViewData> UserMerchantList { get; set; } 
        public string fleetListId { get; set; }
        public string MerchantListId { get; set; }
        public string GridCity { get; set; }
        public string GridState { get; set; }
        public string CountryName { get; set; }
        public bool? IsMerAdd { get; set; }
        public bool IsChkMerAdd { get; set; }
        public Nullable<bool> IsFirstLogin { get; set; }
        public int LogOnByUserType { get; set; }
        public int LogOnByUserId { get; set; }
        public Nullable<int> ParentUserID { get; set; }
        public string ErrorMessage { get; set; }

        public Nullable<bool> UseTransArmor { get; set; }       //PAY-13
    }
}