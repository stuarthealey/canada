﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MTData.Web.ViewData
{
    public class TaxRateOther
    {
        //[Required]
        [Range(typeof(decimal), "0", "100")]
        [Display(Name = "Federal Tax Rate")]
        public Nullable<decimal> FederalTaxRate { get; set; }
        public Nullable<decimal> FederalTaxMax { get; set; }

        //[Required]
        [Range(typeof(decimal), "0", "100")]
        [Display(Name = "State Tax Rate")]
        public Nullable<decimal> StateTaxRate { get; set; }
        public Nullable<decimal> StateTaxMax { get; set; }

        //[Required]
        [Display(Name = "Tip low")]
        public Nullable<decimal> TipPerLow { get; set; }
        
        //[Required]
        [Display(Name = "Tip medium")]
        public Nullable<decimal> TipPerMedium { get; set; }
        
        //[Required]
        [Display(Name = "Tip high")]
        public Nullable<decimal> TipPerHigh { get; set; }
        public bool IsActive { get; set; }

        //[Required]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Disclaimer")]
        public string Disclaimer { get; set; }

        //[Required]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Terms and Conditions")]
        public string TermCondition { get; set; }

        public string RolesAssigned { get; set; }
        public IEnumerable<RoleViewData> UserRoles { get; set; }
        public List<int> UserRoleIdList { get; set; }
        public TCViewData TCondition { get; set; }
        public int isPersonalUpdate { get; set; }
        public bool isTaxInclusive { get; set; }
        public string DisclaimerPlainText { get; set; }
        public string TermConditionPlainText { get; set; }
        public bool IsJobNoRequired { get; set; }
    }
}