﻿using MTD.Core.DataTransferObjects;
using MTD.Core.Factory.Interface;
using MTD.Data.Data;

namespace MTD.Core.Factory
{
    public class MerchantFactory : IMerchantFactory
    {
        public Merchant CreateMerchant(MerchantDto merchantDto)
        {
            Merchant merchant = new Merchant
            {
                MerchantID = merchantDto.MerchantID,
                Company = merchantDto.Company,
                SetupFee = merchantDto.SetupFee,
                MonthlyFee = merchantDto.MonthlyFee,
                Disclaimer = merchantDto.Disclaimer,
                TermCondition = merchantDto.TermCondition,
                StateTaxRate = merchantDto.StateTaxRate,
                FederalTaxRate = merchantDto.FederalTaxRate,
                TipPerLow = merchantDto.TipPerLow,
                TipPerMedium = merchantDto.TipPerMedium,
                TipPerHigh = merchantDto.TipPerHigh,
                IsActive = merchantDto.IsActive,
                CreatedBy = merchantDto.CreatedBy,
                CreatedDate = merchantDto.CreatedDate,
                ModifiedBy = merchantDto.ModifiedBy,
                ModifiedDate = merchantDto.ModifiedDate,
                IsAllowSale = merchantDto.IsAllowSale,
                IsAllowPreauth = merchantDto.IsAllowPreauth,
                IsAllowCapture = merchantDto.IsAllowCapture,
                IsAllowRefund = merchantDto.IsAllowRefund,
                IsAllowVoid = merchantDto.IsAllowVoid,
                UseTransArmor = merchantDto.UseTransArmor   //PAY-13
            };
            return merchant;
        }

        public ManageReciept ToManageReciept(ManageRecieptDto recieptDto)
        {
            ManageReciept manageRec = new ManageReciept
            {
                FleetID = recieptDto.FleetID,
                fk_MerchantID = recieptDto.fk_MerchantID,
                FleetName = recieptDto.FleetName,
                Description = recieptDto.Description,
                ReceiptID = recieptDto.ReceiptID,
                fk_FleetID = recieptDto.fk_FleetID,
                IsFleet = recieptDto.IsFleet,
                IsVehicle = recieptDto.IsVehicle,
                IsDriver = recieptDto.IsDriver,
                IsDriverTaxNo = recieptDto.IsDriverTaxNo,
                IsCompName = recieptDto.IsCompName,
                IsCompAdd = recieptDto.IsCompAdd,
                IsComPhone = recieptDto.IsComPhone,
                IsPickAdd = recieptDto.IsPickAdd,
                IsDestAdd = recieptDto.IsDestAdd,
                IsFlagFall = recieptDto.IsFlagFall,
                IsFare = recieptDto.IsFare,
                IsExtra = recieptDto.IsExtra,
                IsTolls = recieptDto.IsTolls,
                IsTip = recieptDto.IsTip,
                IsStateTaxPer = recieptDto.IsStateTaxPer,
                IsStateTaxAmt = recieptDto.IsStateTaxAmt,
                IsFederalTaxPer = recieptDto.IsFederalTaxPer,
                IsFederalTaxAmt = recieptDto.IsFederalTaxAmt,
                IsSurPer = recieptDto.IsSurPer,
                IsSurAmt = recieptDto.IsSurAmt,
                IsSubTotal = recieptDto.IsSubTotal,
                IsTotal = recieptDto.IsTotal,
                IsActive = recieptDto.IsActive,
                CreatedBy = recieptDto.CreatedBy,
                CreatedDate = recieptDto.CreatedDate,
                ModifiedBy = recieptDto.ModifiedBy,
                ModifiedDate = recieptDto.ModifiedDate,
                IsTermsAndCondition = recieptDto.IsTermsAndCondition,
                IsDisclaimer = recieptDto.IsDisclaimer,
                IsCompanyTaxNo = recieptDto.IsCompanyTaxNo,

                IsFleetPos = recieptDto.IsFleetPos,
                IsVehiclePos = recieptDto.IsVehiclePos,
                IsDriverPos = recieptDto.IsDriverPos,
                IsDriverTaxNoPos = recieptDto.IsDriverTaxNoPos,
                IsCompNamePos = recieptDto.IsCompNamePos,
                IsCompPhonePos = recieptDto.IsCompPhonePos,
                IsPickAddPos = recieptDto.IsPickAddPos,
                IsDestAddPos = recieptDto.IsDestAddPos,
                IsFlagFallPos = recieptDto.IsFlagFallPos,
                IsFarePos = recieptDto.IsFarePos,
                ISExtraPos = recieptDto.ISExtraPos,
                IsTollsPos = recieptDto.IsTollsPos,
                IsTipPos = recieptDto.IsTipPos,
                IsStateTaxPerPos = recieptDto.IsStateTaxPerPos,
                IsFederalTaxPerPos = recieptDto.IsFederalTaxPerPos,
                IsFederalTaxAmtPos = recieptDto.IsFederalTaxAmtPos,
                IsSurPerPos = recieptDto.IsSurPerPos,
                IsSurAmtPos = recieptDto.IsSurAmtPos,
                IsSubTotalPos = recieptDto.IsSubTotalPos,
                IsTotalpos = recieptDto.IsTotalpos,
                IsTermsConditionsPos = recieptDto.IsTermsConditionsPos,
                IsDisclaimerPos = recieptDto.IsDisclaimerPos,
                IsCompanyTaxNoPos = recieptDto.IsCompanyTaxNoPos,
                IsCompAddPos = recieptDto.IsCompAddPos,
                IsStateTaxAmtPos = recieptDto.IsStateTaxAmtPos
            };
            return manageRec;
        }

        public MerchantUser ToMerchantUser(MerchantUserDto merchanUserDto)
        {
            MerchantUser merchantUser = new MerchantUser
            {
                MerchantID = merchanUserDto.MerchantID,
                Company = merchanUserDto.Company,
                SetupFee = merchanUserDto.SetupFee,
                MonthlyFee = merchanUserDto.MonthlyFee,
                Disclaimer = merchanUserDto.Disclaimer,
                TermCondition = merchanUserDto.TermCondition,
                StateTaxRate = merchanUserDto.StateTaxRate,
                FederalTaxRate = merchanUserDto.FederalTaxRate,
                TipPerLow = merchanUserDto.TipPerLow,
                TipPerMedium = merchanUserDto.TipPerMedium,
                TipPerHigh = merchanUserDto.TipPerHigh,
                IsActive = merchanUserDto.IsActive,
                CreatedBy = merchanUserDto.CreatedBy,
                CreatedDate = merchanUserDto.CreatedDate,
                ModifiedBy = merchanUserDto.ModifiedBy,
                ModifiedDate = merchanUserDto.ModifiedDate,
                IsAllowSale = merchanUserDto.IsAllowSale,
                IsAllowPreauth = merchanUserDto.IsAllowPreauth,
                IsAllowCapture = merchanUserDto.IsAllowCapture,
                IsAllowRefund = merchanUserDto.IsAllowRefund,
                IsAllowVoid = merchanUserDto.IsAllowVoid,
                Country = merchanUserDto.Country,
                FName = merchanUserDto.FName,
                LName = merchanUserDto.LName,
                Email = merchanUserDto.Email,
                Phone = merchanUserDto.Phone,
                Address = merchanUserDto.Address,
                fk_MerchantID = merchanUserDto.fk_MerchantID,
                UserID = merchanUserDto.UserID,
                City = merchanUserDto.City,
                State = merchanUserDto.State,
                StateTaxMax = merchanUserDto.StateTaxMax,
                FederalTaxMax = merchanUserDto.FederalTaxMax,
                PCode = merchanUserDto.PCode,
                RolesAssigned = merchanUserDto.RolesAssigned,
                IsLockedOut = merchanUserDto.IsLockedOut,
                CompanyTaxNumber = merchanUserDto.CompanyTaxNumber,
                Fk_FDId = merchanUserDto.Fk_FDId,
                GropuId = merchanUserDto.GropuId,
                IsTaxInclusive = merchanUserDto.IsTaxInclusive,
                RapidConnect = merchanUserDto.RapidConnect,
                TokenType = merchanUserDto.TokenType,
                MCCode = merchanUserDto.MCCode,
                EncryptionKey = merchanUserDto.EncryptionKey,
                DisclaimerPlainText = merchanUserDto.DisclaimerPlainText,
                TermConditionPlainText = merchanUserDto.TermConditionPlainText,
                IsJobNoRequired=merchanUserDto.IsJobNoRequired,
                ProjectId=merchanUserDto.ProjectId,
                EcommProjectId=merchanUserDto.EcommProjectId,
                ProjectType = merchanUserDto.ProjectType,
                TimeZone = merchanUserDto.TimeZone,
                ZipCode = merchanUserDto.ZipCode,
                UseTransArmor = merchanUserDto.UseTransArmor    //PAY-13
            };
            return merchantUser;
        }

        public ReceiptMaster ToReceiptMaster(ReceiptMasterDto recieptDto)
        {
            ReceiptMaster manageRec = new ReceiptMaster
            {
                FieldID = recieptDto.FieldID,
                FieldName = recieptDto.FieldName,
                IsActive = recieptDto.IsActive,
                CreatedDate = recieptDto.CreatedDate,
                CreatedBy = recieptDto.CreatedBy,
                ModifiedDate = recieptDto.ModifiedDate,
                ModifiedBy = recieptDto.ModifiedBy,
                RecieptCol = recieptDto.RecieptCol
            };

            return manageRec;
        }
    }
}
