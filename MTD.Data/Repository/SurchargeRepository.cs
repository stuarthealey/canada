﻿using System;
using System.Collections.Generic;
using System.Linq;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Data.Repository
{
    public class SurchargeRepository : BaseRepository, ISurchargeRepository
    {

        public SurchargeRepository(IMtDataDevlopmentsEntities dbMtData)
            : base(dbMtData)
        {

        }

        /// <summary>
        /// Purpose             :   To add surcharge
        /// Function Name       :   Add
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   02/20/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="surcharge"></param>
        /// <returns></returns>
        public bool Add(Surcharge surcharge)
        {
            try
            {
                bool isActive = DbMtData.Surcharges.Any(m => m.MerchantId == surcharge.MerchantId && m.FleetID == surcharge.FleetID && m.IsActive);
                if (isActive)
                {
                    return true;
                }

                bool isExist = DbMtData.Gateways.Any(m => m.Name == "First Data");
                if (isExist)
                {
                    var gateway = DbMtData.Gateways.FirstOrDefault(m => m.Name == "First Data");
                    surcharge.fk_GatewayID = gateway.GatewayID;
                }
                surcharge.IsActive = true;
                surcharge.CreatedDate = DateTime.Now;
                Fleet fleetDb = DbMtData.Fleets.Where(x => x.FleetID == surcharge.FleetID).FirstOrDefault();
                fleetDb.TechFeeType = surcharge.TechFeeType;
                fleetDb.TechFeeFixed = surcharge.TechFeeFixed;
                fleetDb.TechFeePer = surcharge.TechFeePer;
                fleetDb.TechFeeMaxCap = surcharge.TechFeeMaxCap;
                DbMtData.Surcharges.Add(surcharge);
                DbMtData.CommitChanges();
            }
            catch (Exception)
            {
                throw;
            }
            return false;

        }

        /// <summary>
        /// Purpose             :   To get surcharge
        /// Function Name       :   GetSurcharge
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   02/20/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="surchargeId"></param>
        /// <returns></returns>
        public Surcharge GetSurcharge(int surchargeId)
        {
            Surcharge surchargeDb = DbMtData.Surcharges.Find(surchargeId);
            string gatewayName = (from m in DbMtData.Gateways where m.GatewayID == surchargeDb.fk_GatewayID select m.Name).FirstOrDefault();
            surchargeDb.Name = gatewayName;
            Fleet fleetDb = DbMtData.Fleets.Where(x => x.FleetID == surchargeDb.FleetID).FirstOrDefault();
            surchargeDb.TechFeeType = fleetDb.TechFeeType;
            surchargeDb.TechFeeFixed=fleetDb.TechFeeFixed;
            surchargeDb.TechFeePer=fleetDb.TechFeePer;
            surchargeDb.TechFeeMaxCap = fleetDb.TechFeeMaxCap;
            return surchargeDb;
        }

        /// <summary>
        /// Purpose             :   To update surcharge
        /// Function Name       :   Update
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   02/20/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="surcharge"></param>
        /// <param name="surchargeId"></param>
        /// <returns></returns>
        public bool Update(Surcharge surcharge, int surchargeId)
        {
            try
            {
                    Surcharge surchargeDb = DbMtData.Surcharges.Find(surchargeId);
                    bool isExist = DbMtData.Gateways.Any(m => m.Name == "First Data");
                    if (isExist)
                    {
                        var gateway = DbMtData.Gateways.FirstOrDefault(m => m.Name == "First Data");
                        surcharge.fk_GatewayID = gateway.GatewayID;
                    }
                    surchargeDb.SurchargeType = surcharge.SurchargeType;
                    surchargeDb.SurchargeFixed = surcharge.SurchargeFixed;
                    surchargeDb.SurchargePer = surcharge.SurchargePer;
                    surchargeDb.SurMaxCap = surcharge.SurMaxCap;
                    surchargeDb.SurMaxCap = surcharge.SurMaxCap;
                    surchargeDb.MerchantId = surcharge.MerchantId;
                    surchargeDb.BookingFeeFixed = surcharge.BookingFeeFixed;
                    surchargeDb.BookingFeePer = surcharge.BookingFeePer;
                    surchargeDb.BookingFeeType = surcharge.BookingFeeType;
                    surchargeDb.BookingFeeMaxCap = surcharge.BookingFeeMaxCap;
                    Fleet fleetDb = DbMtData.Fleets.Where(x => x.FleetID == surcharge.FleetID).FirstOrDefault();
                    fleetDb.TechFeeType = surcharge.TechFeeType;
                    fleetDb.TechFeeFixed = surcharge.TechFeeFixed;
                    fleetDb.TechFeePer = surcharge.TechFeePer;
                    fleetDb.TechFeeMaxCap = surcharge.TechFeeMaxCap;
                    surchargeDb.ModifiedDate = DateTime.Now;
                    surchargeDb.ModifiedBy = surcharge.ModifiedBy;
            }
            catch (Exception)
            {
                throw;
            }

            DbMtData.CommitChanges();
            return false;
        }

        /// <summary>
        /// Purpose             :   To delete surcharge
        /// Function Name       :   DeleteSurcharge
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   02/20/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"        
        ///  </summary>
        /// <param name="surchargeId"></param>
        public void DeleteSurcharge(int surchargeId)
        {
            Surcharge surchargeDb = DbMtData.Surcharges.Find(surchargeId);
            surchargeDb.IsActive = false;
            DbMtData.CommitChanges();
        }

        /// <summary>
        /// Purpose             :   To get list of surcharge
        /// Function Name       :   SurchargeList
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   02/20/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"    
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Surcharge> SurchargeList()
        {
            try
            {
                IEnumerable<Surcharge> surchargeList = (from m in DbMtData.Surcharges
                                                        join l in DbMtData.Fleets on m.FleetID equals l.FleetID
                                                        join n in DbMtData.Gateways on m.fk_GatewayID
                                                            equals n.GatewayID
                                                        join k in DbMtData.Merchants on m.MerchantId
                                       equals k.MerchantID
                                                        where m.IsActive
                                                        select
                                                             new
                                                             {
                                                                 m.SurID,
                                                                 m.SurchargeFixed,
                                                                 m.SurchargePer,
                                                                 m.SurMaxCap,
                                                                 n.Name,
                                                                 m.fk_GatewayID,
                                                                 m.MerchantId,
                                                                 m.SurchargeType,
                                                                 m.BookingFeeFixed,
                                                                 m.BookingFeeMaxCap,
                                                                 m.BookingFeePer,
                                                                 m.BookingFeeType,
                                                                 l
                                                                 .FleetName
                                                             }).AsEnumerable().Select
                (objNew => new Surcharge
                {
                    SurID = objNew.SurID,
                    SurchargeFixed = objNew.SurchargeFixed,
                    SurchargePer = objNew.SurchargePer,
                    SurMaxCap = objNew.SurMaxCap,
                    Name = objNew.Name,
                    MerchantId = objNew.MerchantId,
                    fk_GatewayID = objNew.fk_GatewayID,
                    BookingFeeType = objNew.BookingFeeType,
                    BookingFeePer = objNew.BookingFeePer,
                    BookingFeeFixed = objNew.BookingFeeFixed,
                    BookingFeeMaxCap = objNew.BookingFeeMaxCap,
                    SurchargeType = objNew.SurchargeType,
                    FleetName = objNew.FleetName

                }).ToList();

                foreach (Surcharge sur in surchargeList)
                {
                    switch (sur.SurchargeType)
                    {
                        case 1:
                            {
                                sur.SurType = "Fixed";
                                break;
                            }
                        case 2:
                            {
                                sur.SurType = "Percentage";
                                break;
                            }
                        case 3:
                            {
                                sur.SurType = "Both (Fixed and Percentage)";
                                break;
                            }
                    }
                    switch (sur.BookingFeeType)
                    {
                        case 1:
                            {
                                sur.FeeType = "Fixed";
                                break;
                            }
                        case 2:
                            {
                                sur.FeeType = "Percentage";
                                break;
                            }
                        case 3:
                            {
                                sur.FeeType = "Both (Fixed and Percentage)";
                                break;
                            }
                    }
                }
                return surchargeList;
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To find surcharge
        /// Function Name       :   FindSurcharge
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   02/20/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"     
        /// </summary>
        /// <param name="gatewayId"></param>
        /// <returns></returns>
        public bool FindSurcharge(int gatewayId)
        {
            return DbMtData.Surcharges.Any(x => x.fk_GatewayID == gatewayId);
        }

        /// <summary>
        /// Purpose             :   To get list of  surcharge by filter
        /// Function Name       :   SurchargeListByFiter
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   02/20/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"      
        /// </summary>
        /// <param name="fsParametes"></param>
        /// <returns></returns>
        public IEnumerable<Surcharge> SurchargeListByFiter(FilterSearchParameters fsParametes)
        {
            try
            {
                IEnumerable<Surcharge> surchargeList = (from m in DbMtData.Surcharges
                                                        join l in DbMtData.Fleets on m.FleetID equals l.FleetID
                                                        join n in DbMtData.Gateways on m.fk_GatewayID
                                                            equals n.GatewayID
                                                        join k in DbMtData.Merchants on m.MerchantId
                                                            equals k.MerchantID
                                                        where m.IsActive
                                                        select
                                                             new
                                                             {
                                                                 m.SurID,
                                                                 //m.CCPrefix,
                                                                 m.SurchargeFixed,
                                                                 m.SurchargePer,
                                                                 m.SurMaxCap,
                                                                 n.Name,
                                                                 m.fk_GatewayID,
                                                                 m.MerchantId,
                                                                 m.SurchargeType,
                                                                 m.BookingFeeType,
                                                                 m.BookingFeeFixed,
                                                                 m.BookingFeePer,
                                                                 m.BookingFeeMaxCap,
                                                                 //m.IsDefault,
                                                                 l.FleetName
                                                             }).AsEnumerable().Select
               (objNew => new Surcharge
               {
                   SurID = objNew.SurID,
                   //CCPrefix = objNew.CCPrefix,
                   SurchargeFixed = objNew.SurchargeFixed,
                   SurchargePer = objNew.SurchargePer,
                   SurMaxCap = objNew.SurMaxCap,
                   Name = objNew.Name,
                   MerchantId = objNew.MerchantId,
                   fk_GatewayID = objNew.fk_GatewayID,
                   SurchargeType = objNew.SurchargeType,
                   BookingFeeType = objNew.BookingFeeType,
                   BookingFeeFixed = objNew.BookingFeeFixed,
                   BookingFeePer = objNew.BookingFeePer,
                   BookingFeeMaxCap = objNew.BookingFeeMaxCap,
                   //IsDefault = objNew.IsDefault,
                   FleetName = objNew.FleetName
               }).Where(x => x.MerchantId == fsParametes.MerchantId);

                if (!string.IsNullOrEmpty(fsParametes.Name))
                {
                    surchargeList = surchargeList.Where(p => p.FleetName.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(fsParametes.Sorting))
                {
                    surchargeList = SortedSurcharge(fsParametes.Sorting, surchargeList);
                }
                return fsParametes.PageSize > 0
                          ? surchargeList.Skip(fsParametes.StartIndex).Take(fsParametes.PageSize).ToList().Where(x => x.MerchantId == fsParametes.MerchantId) //Paging
                          : surchargeList.ToList().Where(x => x.MerchantId == fsParametes.MerchantId); //No paging
            }
            catch (Exception)
            {

                throw;
            }


        }

        /// <summary>
        /// Purpose             :   To get receipt by fleet Id
        /// Function Name       :   Surcharge By MerchantID
        /// Created By          :   Sunil Singh
        /// Created On          :   06/04/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"   
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<SurchargeList> SurchargeListByMerchantId(int merchantId)
        {

            var surchargeDb = (from sr in DbMtData.Surcharges
                               where sr.MerchantId == merchantId
                               select new SurchargeList
                               {
                                   //CCPrefix = sr.CCPrefix,
                                   //IsDefault = sr.IsDefault,
                                   SurchargeType = sr.SurchargeType,
                                   SurchargeFixed = sr.SurchargeFixed,
                                   SurchargePer = sr.SurchargePer,
                                   SurMaxCap = sr.SurMaxCap,
                                   BookingFeeType = sr.BookingFeeType,
                                   BookingFeeFixed = sr.BookingFeeFixed,
                                   BookingFeePer = sr.BookingFeePer,
                                   BookingFeeMaxCap = sr.BookingFeeMaxCap
                               }).ToList();

            var singleSur = surchargeDb.SingleOrDefault(o => o.IsDefault == true);
            if (surchargeDb.Count > 0)
            {
                if (singleSur == null)
                {
                    List<SurchargeList> sList = surchargeDb;
                    DefaultSetting defaultDbSurcharge = DbMtData.DefaultSettings.Find(2);
                    SurchargeList sl = new SurchargeList();
                    if (defaultDbSurcharge != null)
                    {
                        sl.SurchargeType = defaultDbSurcharge.SurchargeType;
                        sl.SurMaxCap = defaultDbSurcharge.SurchargeMaxCap;
                        sl.SurchargePer = defaultDbSurcharge.SurchargePer;
                        sl.SurchargeFixed = defaultDbSurcharge.SurchargeFixed;
                        sl.IsDefault = true;
                    }
                    else
                    {
                        sl.SurchargeType = 1;
                        sl.SurchargeFixed = 0;
                        sl.SurchargePer = 1;
                        sl.SurMaxCap = 0;
                        sl.IsDefault = true;
                    }

                    DefaultSetting defaultDbFee = DbMtData.DefaultSettings.Find(3);
                    if (defaultDbFee != null)
                    {
                        sl.BookingFeeType = defaultDbFee.BookingFeeType;
                        sl.BookingFeeFixed = defaultDbFee.BookingFeeFixed;
                        sl.BookingFeePer = defaultDbFee.BookingFeePer;
                        sl.BookingFeeMaxCap = defaultDbFee.BookingMaxCap;
                        sl.IsDefault = true;
                    }
                    else
                    {
                        sl.BookingFeeType = 1;
                        sl.BookingFeeFixed = 0;
                        sl.BookingFeePer = 1;
                        sl.BookingFeeMaxCap = 0;
                        sl.IsDefault = true;
                    }
                    sList.Add(sl);
                    return sList;
                }
            }

            if (surchargeDb.Count == 0)
            {
                List<SurchargeList> sList = new List<SurchargeList>();
                DefaultSetting defaultDbSurcharge = DbMtData.DefaultSettings.Find(2);
                SurchargeList sl = new SurchargeList();
                if (defaultDbSurcharge != null)
                {
                    sl.SurchargeType = defaultDbSurcharge.SurchargeType;
                    sl.SurMaxCap = defaultDbSurcharge.SurchargeMaxCap;
                    sl.SurchargePer = defaultDbSurcharge.SurchargePer;
                    sl.SurchargeFixed = defaultDbSurcharge.SurchargeFixed;
                    sl.IsDefault = true;
                }
                else
                {
                    sl.SurchargeType = 1;
                    sl.SurchargeFixed = 0;
                    sl.SurchargePer = 1;
                    sl.SurMaxCap = 0;
                    sl.IsDefault = true;
                }

                DefaultSetting defaultDbFee = DbMtData.DefaultSettings.Find(3);
                if (defaultDbFee != null)
                {
                    sl.BookingFeeType = defaultDbFee.BookingFeeType;
                    sl.BookingFeeFixed = defaultDbFee.BookingFeeFixed;
                    sl.BookingFeePer = defaultDbFee.BookingFeePer;
                    sl.BookingFeeMaxCap = defaultDbFee.BookingMaxCap;
                    sl.IsDefault = true;
                }
                else
                {
                    sl.BookingFeeType = 1;
                    sl.BookingFeeFixed = 0;
                    sl.BookingFeePer = 1;
                    sl.BookingFeeMaxCap = 0;
                    sl.IsDefault = true;
                }

                sList.Add(sl);
                return sList;
            }
            return surchargeDb;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<Surcharge> SortedSurcharge(string sorting, IEnumerable<Surcharge> surchargeList)
        {
            switch (sorting)
            {
                case "Name ASC":
                    surchargeList = surchargeList.OrderBy(p => p.Name);
                    break;
                case "Name DESC":
                    surchargeList = surchargeList.OrderByDescending(p => p.Name);
                    break;
                case "FleetName ASC":
                    surchargeList = surchargeList.OrderBy(p => p.FleetName);
                    break;
                case "FleetName DESC":
                    surchargeList = surchargeList.OrderByDescending(p => p.FleetName);
                    break;
                default:
                    surchargeList = surchargeList.OrderBy(p => p.Name); //Default!
                    break;
            }
            return surchargeList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fsParametes"></param>
        /// <returns></returns>
        public int SurchargeListCount(FilterSearchParameters fsParametes)
        {
            try
            {
                IEnumerable<Surcharge> surchargeList = (from m in DbMtData.Surcharges
                                                        join l in DbMtData.Fleets on m.FleetID equals l.FleetID
                                                        join n in DbMtData.Gateways on m.fk_GatewayID
                                                            equals n.GatewayID
                                                        join k in DbMtData.Merchants on m.MerchantId
                                                            equals k.MerchantID
                                                        where m.IsActive
                                                        select
                                                             new
                                                             {
                                                                 m.SurID,
                                                                 //m.CCPrefix,
                                                                 m.SurchargeFixed,
                                                                 m.SurchargePer,
                                                                 m.SurMaxCap,
                                                                 n.Name,
                                                                 m.fk_GatewayID,
                                                                 m.MerchantId,
                                                                 m.SurchargeType,
                                                                 m.BookingFeeType,
                                                                 m.BookingFeeFixed,
                                                                 m.BookingFeePer,
                                                                 m.BookingFeeMaxCap,
                                                                 //m.IsDefault,
                                                                 l.FleetName
                                                             }).AsEnumerable().Select
               (objNew => new Surcharge
               {
                   SurID = objNew.SurID,
                   //CCPrefix = objNew.CCPrefix,
                   SurchargeFixed = objNew.SurchargeFixed,
                   SurchargePer = objNew.SurchargePer,
                   SurMaxCap = objNew.SurMaxCap,
                   Name = objNew.Name,
                   MerchantId = objNew.MerchantId,
                   fk_GatewayID = objNew.fk_GatewayID,
                   SurchargeType = objNew.SurchargeType,
                   BookingFeeType = objNew.BookingFeeType,
                   BookingFeeFixed = objNew.BookingFeeFixed,
                   BookingFeePer = objNew.BookingFeePer,
                   BookingFeeMaxCap = objNew.BookingFeeMaxCap,
                   //IsDefault = objNew.IsDefault,
                   FleetName = objNew.FleetName
               }).Where(x => x.MerchantId == fsParametes.MerchantId);

                if (!string.IsNullOrEmpty(fsParametes.Name))
                {
                    surchargeList = surchargeList.Where(p => p.FleetName.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                return surchargeList.Count();
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}