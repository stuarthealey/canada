﻿using System.Web.Mvc;
using MTData.Web.Interface;

namespace MTData.Web
{
    public static class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new CustomActionFilter());
        }
        
    }
}
