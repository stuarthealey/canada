﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
    public class AllMerchantsDto
    {
        public string Company { get; set; }
        public int MerchantID { get; set; }
    }

}
