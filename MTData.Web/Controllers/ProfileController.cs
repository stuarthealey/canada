﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Security;
using System.IO;
using System.Web;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Web.Resource;
using MTData.Web.ViewData;
using MTData.Utility;

namespace MTData.Web.Controllers
{
    public class ProfileController : Controller
    {
        readonly ILogger _logger = new Logger();
        private readonly IAdminUserService _adminUserService;
        private readonly IUserService _userService;
        private readonly IMemberService _loginService;
        private readonly ICommonService _commonService;
        private StringBuilder _logMessage;
        private StringBuilder _logTracking;

        public ProfileController([Named("AdminUserService")] IAdminUserService adminUserService, [Named("LoginService")] IMemberService loginService, [Named("UserService")] IUserService userService, [Named("CommonService")] ICommonService commonService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _adminUserService = adminUserService;
            _loginService = loginService;
            _userService = userService;
            _commonService = commonService;
            _logMessage = logMessage;
            _logTracking = logTracking;
        }

        /// <summary>
        ///  Purpose            :to display the profile
        /// Function Name       :Index
        /// Created By          : Umesh Kumar
        /// Created On          : 12/15/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            var userDtoList = _adminUserService.UserProfiles();
            var viewDataList = userDtoList.ToAdminViewDataList();

            return View(viewDataList);
        }

        /// <summary>
        ///  Purpose            :to  edit profile
        /// Function Name       :Edit
        /// Created By          : Umesh Kumar
        /// Created On          : 12/16/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="adminUserId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Edit(int adminUserId)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            CountryList();
            var adminUserDto = _adminUserService.GetAdminUserDto(adminUserId);
            var user = adminUserDto.ToViewAdminData();
            return View(user);
        }

        /// <summary>
        ///  Purpose            :to retrieve the edit profile
        /// Function Name       :Edit
        /// Created By          : Umesh Kumar
        /// Created On          : 12/16/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userView"></param>
        /// <param name="adminUserId"></param>
        /// <returns></returns>
        public ActionResult Edit(UserViewData userView, int adminUserId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {

                try
                {
                    CountryList();
                    if ((string.IsNullOrEmpty(userView.FName)))
                    {
                        ModelState.AddModelError("FName", MtdataResource.Please_Enter_First_Name);
                        return View();
                    }
                    if ((string.IsNullOrEmpty(userView.LName)))
                    {
                        ModelState.AddModelError("LName", MtdataResource.Please_Enter_Last_Name);
                        return View();
                    }
                    if ((string.IsNullOrEmpty(userView.Email)))
                    {
                        ModelState.AddModelError("Email", MtdataResource.Please_Enter_Email);
                        return View();
                    }
                    const string emailRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                              @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                              @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                    var re = new Regex(emailRegex);
                    if (!re.IsMatch(userView.Email))
                    {
                        ModelState.AddModelError("Email", MtdataResource.Email_is_not_valid);
                        return View();
                    }
                    if ((string.IsNullOrEmpty(userView.Address)))
                    {
                        ModelState.AddModelError("Address", MtdataResource.Please_Enter_Address);
                        return View();
                    }
                    userView.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.FName);
                    UserDto adminUserDto = ViewDataExtention.CreateAdminUserDto(userView);
                    bool result = _adminUserService.UpdateProfile(adminUserDto, adminUserId);
                    ViewBag.Message = result ? "User Updated" : "Internal server Error";
                    return View();
                }
                catch (Exception exception)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), exception);
                }
            }
            return RedirectToAction("index", "Member");
        }

        /// <summary>
        ///  Purpose            :to fetch the change the password
        /// Function Name       :ChangePassword
        /// Created By          : Umesh Kumar
        /// Created On          : 12/17/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ChangePassword()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            return View();
        }

        /// <summary>
        ///  Purpose            :to change the password
        /// Function Name       :ChangePassword
        /// Created By          : Umesh Kumar
        /// Created On          : 12/17/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="loginView"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ChangePassword(LoginViewData loginView)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

            try
            {
                bool isValid = true;
                if (string.IsNullOrEmpty(loginView.UserName) && string.IsNullOrEmpty(loginView.PCode) && string.IsNullOrEmpty(loginView.ConfirmPCode))
                {
                    ModelState.AddModelError("UserName", MtdataResource.ProfileController_ChangePassword_Please_enter_old_password);
                    ModelState.AddModelError("PCode", MtdataResource.ProfileController_ChangePassword_Please_enter_new_password);
                    ModelState.AddModelError("ConfirmPCode", MtdataResource.ProfileController_ChangePassword_Please_re_enter_new_password);

                    return View(loginView);
                }

                if (string.IsNullOrEmpty(loginView.UserName))
                {
                    ModelState.AddModelError("UserName", MtdataResource.ProfileController_ChangePassword_Please_enter_old_password);
                    isValid = false;
                }

                if (string.IsNullOrEmpty(loginView.PCode))
                {
                    ModelState.AddModelError("PCode", MtdataResource.ProfileController_ChangePassword_Please_enter_new_password);
                    isValid = false;
                }

                if (string.IsNullOrEmpty(loginView.ConfirmPCode))
                {
                    ModelState.AddModelError("ConfirmPCode", MtdataResource.ProfileController_ChangePassword_Please_re_enter_new_password);
                    isValid = false;
                }

                if (isValid)
                {
                    string olderPCode = loginView.UserName;
                    string confirmpass = loginView.ConfirmPCode;
                    if (confirmpass == loginView.PCode)
                    {
                        loginView.PCode = loginView.PCode;
                        loginView.UserName = Convert.ToString(((SessionDto)Session["User"]).SessionUser.UserID);
                        loginView.ConfirmPCode = olderPCode;

                        LoginDto loginDto = ViewDataExtention.CreateLoginDto(loginView);

                        int result = _loginService.UpdatePassword(loginDto);
                        if (result == 2)
                        {
                            ViewBag.Message = MtdataResource.Old_Password_Incorrect;
                            return View(loginView);
                        }

                        if (result == 3)
                        {
                            ViewBag.Message = MtdataResource.NewPassword_Match_OldPassword;
                            return View(loginView);
                        }

                        if (result == 4)
                        {
                            try
                            {
                                _logMessage.Append("Mail Sending begin and reading file ~/HtmlTemplate/password.html");

                                var filename = System.Web.HttpContext.Current.Server.MapPath("~/HtmlTemplate/password.html");
                                using (var myFile = new StreamReader(filename))
                                {
                                    string msgfile = myFile.ReadToEnd();
                                    var msgBody = new StringBuilder(msgfile);
                                    string cMail = ((SessionDto)Session["User"]).SessionUser.Email;
                                    string cName = ((SessionDto)Session["User"]).SessionMerchant.Company;
                                    string cuserName = ((SessionDto)Session["User"]).SessionUser.FName;
                                    string appUrl = System.Configuration.ConfigurationManager.AppSettings["ApplicationURL"];
                                    msgBody.Replace("[##email##]", cMail);
                                    msgBody.Replace("[##password##]", loginView.PCode);
                                    msgBody.Replace("[##UserName##]", cuserName);
                                    msgBody.Replace("[##AppURL##]", appUrl);

                                    _logMessage.Append("calling SendMail method of utility class");

                                    var sendEmail = new EmailUtil();
                                    sendEmail.SendMail(cMail, msgBody.ToString(), cName, "Reset Password");
                                    ViewBag.Message = MtdataResource.Password_updated_successfully;

                                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;

                                    _logTracking.Append("User " + userName + " update password for user" + loginDto.UserName + " at " + System.DateTime.Now.ToUniversalTime());
                                    _logger.LogInfoMessage(_logTracking.ToString());
                                }
                            }
                            catch (Exception ex)
                            {
                                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                            }

                            ViewBag.Message = MtdataResource.Password_updated_successfully;
                            Session.Abandon();
                            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                            return View();
                        }
                       
                        return View(loginView);
                    }

                    ModelState.AddModelError("ConfirmPCode", MtdataResource.Password_Mismatch);

                    return View(loginView);
                }

                return View();
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return RedirectToAction("index", "Member");
        }

        /// <summary>
        ///  Purpose            :to get the users
        /// Function Name       :Users
        /// Created By          : Umesh Kumar
        /// Created On          : 12/18/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <returns></returns>
        public ActionResult Users()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            int user = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.UserID);
            var userDtoList = _userService.UserList(user, string.Empty,0,0);
            var userViewDatList = userDtoList.ToViewDataList();

            return View(userViewDatList);
        }

        /// <summary>
        ///  Purpose            :to display the users
        /// Function Name       :Display
        /// Created By          : Umesh Kumar
        /// Created On          : 12/19/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Display()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                UserDto userDto = _userService.GetUser(userId);
                UserViewData userData = userDto.ToViewData();
                return View(userData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        ///  Purpose            :to update the users
        /// Function Name       :Update
        /// Created By          : Umesh Kumar
        /// Created On          : 12/20/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Update()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                UserDto userDto = _userService.GetUser(userId);
                UserViewData userData = userDto.ToViewData();
                var countryDtoList = _commonService.GetCountryList();

                _logMessage.Append("calling ToCountryViewDataList method to map CountryViewData to CountryCodeDto and retrun CountryViewData");

                userData.CountryList = countryDtoList.ToCountryViewDataList();

                _logMessage.Append("ToCountryViewDataList method executed successfully");

                return View(userData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            :to retrieve the updated users
        /// Function Name       :Update
        /// Created By          : Umesh Kumar
        /// Created On          : 12/22/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="viewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(UserViewData viewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                if (ModelState.IsValid)
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    _logMessage.Append("calling ToDTO method to map UserViewData to UserDto and retrun UserViewData");

                    UserDto userDto = viewData.ToDTO();

                    _logMessage.Append("ToDTO method executed successfully");

                    userDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.FName);

                    _logMessage.Append("Calling UpdateUserProfile method of _userService class takes MerchantUserDto, MerchantUserDto properties Frist Name= " + userDto.FName + ", Last Name= " + userDto.LName + ", Phone= " + userDto.Phone + ", Email= " + userDto.Email + ", Country= " + userDto.fk_Country + ", State= " + userDto.fk_State + ", City= " + userDto.fk_City + ", Address= " + userDto.Address + ", RolesAssigned= " + userDto.RolesAssigned + "userid= " + viewData.UserID + " ");

                    bool result = _userService.UpdateUserProfile(userDto, viewData.UserID);
                    if (result)
                    {
                        ViewBag.Message = MtdataResource.Email_already_exist;
                        viewData.CountryList = CountryList();
                        return View(viewData);
                    }

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;

                    _logTracking.Append("User " + userName + " update profile for user" + userDto.Email + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                    _logMessage.Append("UpdateUserProfile method of _userService class executed successfully");

                    int length = viewData.FName.Length;
                    if (length > 11)
                    {
                        string nameNow = viewData.FName.Substring(0, 9) + "...";
                        ((SessionDto)Session["User"]).SessionUser.FName = nameNow;
                    }
                    else
                    {
                        ((SessionDto)Session["User"]).SessionUser.FName = viewData.FName;
                    }

                    return RedirectToAction("Display");
                }

            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            var countryDtoList = _commonService.GetCountryList();

            _logMessage.Append("calling ToCountryViewDataList method to map CountryViewData to CountryCodeDto and retrun CountryViewData");

            viewData.CountryList = countryDtoList.ToCountryViewDataList();

            _logMessage.Append("ToCountryViewDataList method executed successfully");

            return View(viewData);
        }

        /// <summary>
        ///  Purpose            :to get the list of country
        /// Function Name       :CountryList
        /// Created By          : Umesh Kumar
        /// Created On          : 12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<CountryViewData> CountryList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("calling GetCountryList method of _commonService");

                var countryDtoList = _commonService.GetCountryList();

                _logMessage.Append("GetCountryList method of _commonService executed successfully");
                _logMessage.Append("calling ToCountryViewDataList method to map CountryViewData to CountryCodeDto and retrun CountryViewData");

                var countryViewDataList = countryDtoList.ToCountryViewDataList();

                _logMessage.Append("ToCountryViewDataList method executed successfully");
                _logMessage.Append("converting countryViewDataList into list<CountryViewData>");

                List<CountryViewData> objcountrylist = countryViewDataList.ToList();

                _logMessage.Append("conversion countryViewDataList into list<CountryViewData> completed");

                var li = new List<SelectListItem> { new SelectListItem { Text = MtdataResource.Please_Select_a_Country, Value = "0" } };
                li.AddRange(objcountrylist.Select(item => new SelectListItem { Text = item.Title, Value = Convert.ToString(item.CountryCodesID) }));

                _logMessage.Append("returning list as objcountrylist");

                return objcountrylist;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

    }
}