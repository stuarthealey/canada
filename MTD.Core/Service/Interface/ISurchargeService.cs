﻿using System.Collections.Generic;
using MTD.Core.DataTransferObjects;

namespace MTD.Core.Service.Interface
{
    public interface ISurchargeService
    {
        bool Add(SurchargeDto surchargeDto);
        
        bool Update(SurchargeDto surchargeDto, int surchargeId);
        
        IEnumerable<SurchargeDto> GetSurchargeDtoList();
        
        SurchargeDto GetSurchargeDto(int surchargeId);
        
        void DeleteSurcharge(int vehicleId);
        
        bool FindSurcharge(int gatewayId);

        IEnumerable<SurchargeDto> SurchargeListByFiter(FilterSearchParametersDto searchParameters);  

        IEnumerable<SurchargeDto> SurchargeByMerchantId(int merchantId);
        int SurchargeListCount(FilterSearchParametersDto fsParametes);
    }
}
