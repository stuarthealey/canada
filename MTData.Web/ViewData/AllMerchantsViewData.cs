﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.Web.ViewData
{
    public class AllMerchantsViewData
    {
        public string Company { get; set; }
        public int MerchantID { get; set; }
    }
}