﻿
namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table14AdditionalDiscoverData
    {
        #region Private Constants and Variables

        #region Constants
        private const string tblId = "|31|34";
        private const string sixSpaces = "|20|20|20|20|20|20";
        private const string twelveZeros = "|30|30|30|30|30|30|30|30|30|30|30|30";
        #endregion


        #region Variables
        private string defaultIndicator = "|58"; //X=58 in Hex
        private string defaultTranID = "|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20";
        private string defaultAuthAmount = "|30|30|30|30|30|30|30|30|30|30|30|30";
        #endregion

        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'31 34' Table 36
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        /// <summary>
        /// Indicator of Table
        /// default X in Table 14 indicator
        /// </summary>
        public string DiscoverIndicator
        {
            get { return defaultIndicator; }
            set { defaultIndicator = value; }
        }

        /// <summary>
        /// Transaction ID
        /// default 15 spaces in Transaction ID
        /// </summary>
        public string TransactionID
        {
            get { return defaultTranID; }
            set { defaultTranID = value; }
        }

        /// <summary>
        /// Filler. contains 6 Spaces(1 space= X'20') in Request 
        /// </summary>
        public string Filler6Space
        {
            get { return sixSpaces; }
        }

        /// <summary>
        /// Filler. contains 12 zero(1 space= X'30') in Request 
        /// </summary>
        public string Filler12Zeros
        {
            get { return twelveZeros; }
        }

        /// <summary>
        /// Total Auth Amount
        /// default 12 Zeros(1 space= X'30') in Request 
        /// </summary>
        public string TotalAuthAmount
        {
            get { return defaultAuthAmount; }
            set { defaultAuthAmount = value; }
        }

        #endregion
    }
}