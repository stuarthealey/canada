﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Models
{
    public partial class CreateRCCIResponse 
    {
        public CreateRCCIResult CreateRCCIResult { get; set; }
    }

    public partial class BaseResponse
    {
        public ResultStatus Result { get; set; }
    }

    public partial class CreateRCCIResult : BaseResponse
    {
        public CreditCardStatus CreditCardStatus { get; set; }
        public RCCI RCCI { get; set; }
        public TruncatedCreditCardDetails TruncatedCreditCardDetails { get; set; }
    }

    public partial class CreditCardStatus
    {
        public bool IsValid { get; set; }
        public string StatusDescription { get; set; }
    }

    public partial class RCCI
    {
        public string Value { get; set; }
    }

    public partial class TruncatedCreditCardDetails
    {
        public string TruncatedPAN { get; set; }
        public string ExpiryMonth { get; set; }
        public string ExpiryYear { get; set; }
        public string CardType { get; set; }
    }
    
    public partial class MGID
    {
        public string Value { get; set; }
    }
  
    public partial class ResultStatus
    {
        public ResultCode Code { get; set; }
        public string ErrorDescription { get; set; }
    }
  
    public enum ResultCode
    {
        Success,
        InvalidRCCI,
        InvalidCreditCard,
        Error,
    }

    public partial class ValidateRCCIResponse : BaseResponse
    {
        public CreditCardStatus CreditCardStatus { get; set; }
        public TruncatedCreditCardDetails TruncatedCreditCardDetails { get; set; }
    }
 
    public partial class FullCreditCardDetails
    {
        public string PAN { get; set; }
        public string ExpiryMonth { get; set; }
        public string ExpiryYear { get; set; }
        public string NameOnCard { get; set; }
        public string CVV2 { get; set; }
    }
   
    public partial class Encryption
    {
        public int Method { get; set; }
        public int KeyVersion { get; set; }
        public string Token { get; set; }
    }
   
    public partial class CustomerId
    {
        public string Value { get; set; }
    }
   
    public partial class CustomerDetails
    {
        public CustomerId CustomerId { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
    }

}