﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.Web.ViewData;

namespace MTData.Web.Controllers
{
    public class SubAdminController : Controller
    {
        private StringBuilder _logMessage;
        private StringBuilder _logTracking;
        private readonly ISubAdminService _subAdminService;
        private readonly IMerchantService _merchantService;
        private readonly IUserService _userService;
        private readonly ICommonService _commonService;
        readonly ILogger _logger = new Logger();
        UserViewData _objUserViewData;

        public SubAdminController([Named("MerchantService")] IMerchantService merchantService, [Named("UserService")] IUserService userService, [Named("SubAdminService")] ISubAdminService subAdminService, [Named("CommonService")] ICommonService commonService, StringBuilder logTracking, StringBuilder logMessage)
        {
            _subAdminService = subAdminService;
            _commonService = commonService;
            _userService = userService;
            _logTracking = logTracking;
            _logMessage = logMessage;
            _merchantService = merchantService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || Session["User"] == null)
                return RedirectToAction("Index", "Member");

            int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
            IsFirstLoginViewData obj = new IsFirstLoginViewData();

            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Session["User"] == null || userType == 2 || userType == 3 || userType == 4)
                return RedirectToAction("Index", "Member");

            if (Session["AllInfo"] != null)
                Session.Remove("AllInfo");

            if (!(bool)Session["Islogged"])
            {
                obj.IsFirstLogin = (bool)((SessionDto)Session["User"]).SessionUser.IsFirstLogin;
                obj.UserTypeId = Convert.ToString(((SessionDto)Session["User"]).SessionUser.fk_UserTypeID);
                Session.Remove("Islogged");
                Session["Islogged"] = true;
                obj.UserId = ((SessionDto)Session["User"]).SessionUser.UserID;

                return View(obj);
            }

            obj.UserId = ((SessionDto)Session["User"]).SessionUser.UserID;
            obj.UserTypeId = Convert.ToString(((SessionDto)Session["User"]).SessionUser.fk_UserTypeID);
            return View(obj);
        }

        /// <summary>
        /// Purpose             :   To get interface for creating user
        /// Function Name       :   Create
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   10/27/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            int userType = 0;
            if (Session["User"] != null)
                userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;

            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || userType == 2 || userType == 3 || userType == 4) return RedirectToAction("index", "Member");

            try
            {
                int userId = ((SessionDto)Session["User"]).SessionUser.UserID;

                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("using object intilizer to GetUserRoles and convert to viewdatalist and get CountryList, StateList and CityList");

                _objUserViewData = new UserViewData
                {
                    CountryList = CountryList(),
                    MerchantList = _subAdminService.MerchantList().ToViewDataList(),
                    fk_Country = "AU"
                };

                if (userType == 5)
                {
                    _objUserViewData.MerchantList = _subAdminService.GetUserMerchant(userId).ToViewDataList();
                }

                _logMessage.Append(" CountryList, StateList and CityList list populate successfully");
                _logMessage.Append("ToViewDataList method executed successfully");
                _objUserViewData.fk_UserTypeID = userType;

                return View(_objUserViewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   Creating Users
        /// Function Name       :   Create
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   10/27/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userViewdata"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(UserViewData userViewdata)
        {
            if (Session["User"] == null)
                return RedirectToAction("Index", "Member");

            int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
            int userId = ((SessionDto)Session["User"]).SessionUser.UserID;

            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || userType == 2 || userType == 3 || userType == 4)
                return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                
                if (!string.IsNullOrEmpty(userViewdata.fleetListId))
                    userViewdata.fleetListId = userViewdata.fleetListId.Replace("multiselect-all,", string.Empty);
                
                if (!string.IsNullOrEmpty(userViewdata.MerchantListId))
                    userViewdata.MerchantListId = userViewdata.MerchantListId.Replace("multiselect-all,", string.Empty);

                if ((String.IsNullOrEmpty(userViewdata.fleetListId)) || userViewdata.fleetListId == "multiselect-all" || userViewdata.fleetListId == "-1")
                    ModelState.AddModelError("fleetListId", "Please select a Fleet");

                if ((String.IsNullOrEmpty(userViewdata.MerchantListId)) || userViewdata.MerchantListId == "multiselect-all" || userViewdata.MerchantListId == "-1")
                    ModelState.AddModelError("MerchantListId", "Please select a Merchant");

                userViewdata.CreatedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                string cPCode = Membership.GeneratePassword(10, 0);
                userViewdata.PCode = cPCode.Hash();

                userViewdata.UserRoles = _merchantService.GetUserRoles().ToViewDataList();
                userViewdata.CountryList = CountryList();
                userViewdata.MerchantList = _subAdminService.MerchantList().ToViewDataList();

                if (userType == 5)
                    userViewdata.MerchantList = _subAdminService.GetUserMerchant(userId).ToViewDataList();

                userViewdata.ParentUserID = userId;

                if (!string.IsNullOrEmpty(userViewdata.MerchantListId))
                    userViewdata.fleetlist = _subAdminService.GetFleet(userViewdata.MerchantListId, userType, userId).ToViewDataList();
                else
                    userViewdata.fleetlist = _subAdminService.GetFleet("-1", userType, userId).ToViewDataList();

                if (ModelState.IsValid)
                {
                    var res = _commonService.IsExist(userViewdata.Email);
                    if (!res)
                    {
                        userViewdata.fk_UserTypeID = userType;
                        var response = _subAdminService.Add(userViewdata.ToDTO());

                        string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                        _logTracking.Append("User " + userViewdata.Email + " Sub Admin user created  by" + userName + " at " + System.DateTime.Now.ToUniversalTime());
                        _logger.LogInfoMessage(_logTracking.ToString());

                        if (response)
                        {
                            try
                            {
                                _logMessage.Append("Mail Sending begin and reading file ~/HtmlTemplate/confirmMail.html");

                                var filename = System.Web.HttpContext.Current.Server.MapPath("~/HtmlTemplate/confirmMail.html");
                                using (var myFile = new System.IO.StreamReader(filename))
                                {
                                    string msgfile = myFile.ReadToEnd();
                                    var msgBody = new StringBuilder(msgfile);
                                    myFile.Close();

                                    string cMail = userViewdata.Email;
                                    string cName = "MTI Admin";

                                    string cuserName = userViewdata.FName;
                                    string appUrl = System.Configuration.ConfigurationManager.AppSettings["ApplicationURL"];

                                    msgBody.Replace("[##cMail##]", cMail);
                                    msgBody.Replace("[##cName##]", cName);
                                    msgBody.Replace("[##cPassword##]", cPCode);
                                    msgBody.Replace("[##AppURL##]", appUrl);
                                    msgBody.Replace("[##cuserName##]", cuserName);

                                    var sendEmail = new EmailUtil();

                                    _logMessage.Append("calling SendMail method of utility class");
                                    sendEmail.SendMail(cMail, msgBody.ToString(), cName, "Registered Sub Admin");
                                }
                            }
                            catch (Exception ex)
                            {
                                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                            }
                        }

                        return RedirectToAction("index", "SubAdmin");
                    }

                    ViewBag.ExitUser = "Account for '" + userViewdata.Email + "' already exists please contact to your administrator";

                    return View(userViewdata);
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View(userViewdata);
        }

        /// <summary>
        /// Purpose             :   To get interface for updating user
        /// Function Name       :   Update
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   10/27/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Update(int userId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;

            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || userType == 2 || userType == 3 || userType == 4)
                return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("using object intilizer to GetUserRoles and convert to viewdatalist and get CountryList, StateList and CityList");

                UserViewData userViewData = new UserViewData();
                userViewData = _subAdminService.GetUser(userId).ToViewData();

                userViewData.CountryList = CountryList();
                userViewData.MerchantList = _subAdminService.MerchantList().ToViewDataList();
                userViewData.UserMerchantList = _subAdminService.GetUserMerchant(userId).ToViewDataList();
                userViewData.userFleetList = _userService.GetUsersFleet(userId).ToViewDataList();
                userViewData.fk_Country = "AU";

                string merchantListId = "-1";
                foreach (var item in userViewData.UserMerchantList)
                {
                    merchantListId += "," + item.MerchantID;
                }

                if (userType == 5)
                    userViewData.MerchantList = _subAdminService.GetCorpUserMerchant(userId).ToViewDataList();

                userViewData.fleetlist = _subAdminService.GetFleet(merchantListId, userType, userId).ToViewDataList();

                _logMessage.Append("ToViewDataList method executed successfully");

                userViewData.fk_UserTypeID = userType;

                return View(userViewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             : apply page indexing on merchant list
        /// Function Name       : ViewMerchant
        /// Created By          : umesh
        /// Created On          :2/5/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult SubAdminListByFiter(string name = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetMerchantCountByFilter method of _merchantService class to count Merchant");

                int merchantUserListCount;
                name = name.Trim();
                int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                PageSizeSearchViewData pSizeSearch = new PageSizeSearchViewData()
                {
                    Name = name,
                    JtStartIndex = jtStartIndex,
                    JtPageSize = jtPageSize,
                    JtSorting = jtSorting,
                    UserId = userId,
                    UserType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID
                };

                PageSizeSearchDto pSizeSearchDto = pSizeSearch.ToPageSizeSearch();
                var subAdminUser = _subAdminService.GetSusbAdminByFilter(pSizeSearchDto);
                if (string.IsNullOrEmpty(name))
                {
                    _logMessage.Append("Calling MerchantList method of _merchantService class to count Merchants");

                    merchantUserListCount = _subAdminService.SubAdminList(pSizeSearchDto).ToList().Count;

                    _logMessage.Append("MerchantList method of _merchantService executed successfully result = " + merchantUserListCount);

                    return Json(new { Result = "OK", Records = subAdminUser, TotalRecordCount = merchantUserListCount });
                }

                var subAdminUserDtos = subAdminUser as UserDto[] ?? subAdminUser.ToArray();
                merchantUserListCount = _subAdminService.SubAdminList(pSizeSearchDto).ToList().Count;

                _logMessage.Append("GetMerchantByFilter method of _merchantService executed successfully");

                return Json(new { Result = "OK", Records = subAdminUserDtos, TotalRecordCount = merchantUserListCount });
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteSubAdmin(int userId)
        {
            if (Session["User"] != null)
            {
                try
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    _logMessage.Append("Calling DeleteMerchant method of MarchnatService class takes UserId= " + userId);

                    _subAdminService.Remove(userId); //delete merchant by id

                    _logMessage.Append("DeleteMerchant method of MarchnatService class executed successfully");
                    _logMessage.Append("Merchant id " + userId + " was deleted by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return Json(new { Result = "OK" });
                }
                catch (Exception exception)
                {
                    _logger.LogError(exception.ToString());
                }
            }

            return Json(new { Result = "No" });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Fleet()
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || Session["User"] == null)
                return RedirectToAction("Index", "Member");

            IsFirstLoginViewData isFirst = new IsFirstLoginViewData();
            return View(isFirst);
        }

        /// <summary>
        /// Purpose             :   Updating Users
        /// Function Name       :   Update
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   10/27/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userViewdata"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(UserViewData userViewdata, int userId)
        {
            int userType = 0;
            if (Session["User"] != null)
            if (Session["User"] == null) return RedirectToAction("Index", "Member");

            userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;

            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || userType == 2 || userType == 3 || userType == 4) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (!string.IsNullOrEmpty(userViewdata.fleetListId))
                    userViewdata.fleetListId = userViewdata.fleetListId.Replace("multiselect-all,", string.Empty);
                
                if (!string.IsNullOrEmpty(userViewdata.MerchantListId))
                    userViewdata.MerchantListId = userViewdata.MerchantListId.Replace("multiselect-all,", string.Empty);

                if ((String.IsNullOrEmpty(userViewdata.fleetListId)) || userViewdata.fleetListId == "multiselect-all" || userViewdata.fleetListId == "-1")
                    ModelState.AddModelError("fleetListId", "Please select a fleet");

                if ((String.IsNullOrEmpty(userViewdata.MerchantListId)) || userViewdata.MerchantListId == "multiselect-all" || userViewdata.MerchantListId == "-1")
                    ModelState.AddModelError("MerchantListId", "Please select a Merchant");

                userViewdata.ModifiedBy = ((SessionDto)Session["User"]).SessionUser.Email;

                userViewdata.UserRoles = _merchantService.GetUserRoles().ToViewDataList();
                userViewdata.CountryList = CountryList();
                userViewdata.MerchantList = _subAdminService.MerchantList().ToViewDataList();
                userViewdata.UserMerchantList = _subAdminService.GetUserMerchant(userId).ToViewDataList();
                userViewdata.userFleetList = _userService.GetUsersFleet(userId).ToViewDataList();
                
                if (!string.IsNullOrEmpty(userViewdata.MerchantListId))
                    userViewdata.fleetlist = _subAdminService.GetFleet(userViewdata.MerchantListId, userType, userId).ToViewDataList();
                else
                    userViewdata.fleetlist = _subAdminService.GetFleet("-1", userType, userId).ToViewDataList();

                if (userType == 5)
                    userViewdata.MerchantList = _subAdminService.GetCorpUserMerchant(userId).ToViewDataList();

                if (ModelState.IsValid)
                {
                    var response = _subAdminService.Update(userViewdata.ToDTO(), userId);
                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userViewdata.Email + " Sub Admin user updated  by" + userName + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    if (response=="OK")
                        return RedirectToAction("index");
                    else
                    {
                        TempData["ErrorMessage"] = response;
                        return View(userViewdata);
                    }
                }

                return View(userViewdata);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View(userViewdata);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<CountryViewData> CountryList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetCountryList method of _commonService class to get countries");

                var countryDtoList = _commonService.GetCountryList();

                _logMessage.Append("GetCountryList method of _commonService executed successfully");
                _logMessage.Append("calling ToCountryViewDataList method to map CountryViewData to CountryCodeDto and retrun CountryViewData");

                var countryViewDataList = countryDtoList.ToCountryViewDataList();

                _logMessage.Append("ToCountryViewDataList method executed successfully");
                _logMessage.Append("converting countryViewDataList list into objcountrylist");

                List<CountryViewData> objcountrylist = countryViewDataList.ToList();

                _logMessage.Append("convert countryViewDataList into objcountrylist successful");

                var li = new List<SelectListItem> { new SelectListItem { Text = "Please select a Country", Value = "0" } };
                li.AddRange(objcountrylist.Select(item => new SelectListItem { Text = item.Title, Value = Convert.ToString(item.CountryCodesID) }));
                
                _logMessage.Append("returning country list");

                return objcountrylist;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :   To get fleet of different merchants
        /// Function Name       :   GetFleet
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   10/27/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantListId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetFleet(string merchantListId)
        {
            try
            {
                int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                var fleetList = _subAdminService.GetFleet(merchantListId, userType, userId);

                return Json(fleetList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return null;
        }

    }
}