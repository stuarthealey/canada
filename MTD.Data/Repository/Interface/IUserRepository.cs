﻿using MTD.Data.Data;
using System.Collections.Generic;

namespace MTD.Data.Repository.Interface
{
    public interface IUserRepository
    {
        bool Add(User user);
        IEnumerable<Role> GetUsersRole(int userId);
        IEnumerable<User> UserList(int userId, string name, int logOnByUserId, int logOnByUserType);
        bool Update(User user, int userId);
        bool UpdateUserProfile(User user, int userId);
        User GetUserUpdate(int userId, int merchantId);
        User GetUser(int userId, int val);
        User GetUser(int userId);
        void DeleteUser(int userId);
        IEnumerable<Fleet> FleetList(int merchantId);
        IEnumerable<UserFleet> GetUsersFleet(int userId);
        IEnumerable<Fleet> UserFleetLists(int userId);
        IEnumerable<User> UserListByFilter(FilterSearchParameters searchParameters, int logOnByUserId, int logOnByUserType);
        void ChangeStatus(int UserId);
        IEnumerable<FleetList> UserFleetList(int UserId);
        IEnumerable<Fleet> PayerFleetList(int PayerId, int merchantId);
        List<int> GetAccesibleUserFleet(List<int> fleets);
        IEnumerable<UserFleet> GetUsersPayerFleet(int userId, int PayerId);
    }
}
