﻿using System.Collections.Generic;
using MTD.Data.Data;
namespace MTD.Data.Repository.Interface
{
    public interface IScheduleRepository
    {
        void Update(ScheduleReport scheduleReport);
        ScheduleReport GetScheduleReport(int scheduleId);
        void Add(ScheduleReport scheduleReport);
        bool UpdateRecipient(RecipientMaster recipient);
        bool UpdateAdminRecipient(AdminRecipientList adminRecipientList);
        IEnumerable<ScheduleReport> ScheduleReportList(); 
        IEnumerable<ScheduleReport> ScheduleReportListByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting);
        IEnumerable<ScheduleReport> ScheduleReportListByFilter(FilterSearchParameters filterSearchParameters, string jtSorting);
        IEnumerable<Report> ReportList();
        IEnumerable<Report> ReportList(int merchantId);
        IEnumerable<Report> ReportListReadOnly();
        void  Delete(int ScheduleID);
        void UpdateCorporateUserRecipient(RecipientMaster recipient,int corporateUserId,string modifiedBy);
        void AddCorporateUser(ScheduleReport scheduleReport);
        void UpdateCorporate(ScheduleReport scheduleReport);
        IEnumerable<Report> CorporateReportList(int corporateId);
        void DeleteCorporate(int ScheduleID);
        IEnumerable<Report> ReportList(int to, int from);
        IEnumerable<usp_GetAdminWeeklySummaryCount_Result> WeeklySummary(FilterSearchParameters filterSearchParameters);
        int WeeklySummaryCount(FilterSearchParameters filterSearchParameters); 
        IEnumerable<usp_GetAdminWeeklyDollarVolume_Result> WeeklyDollarVolumeReport(FilterSearchParameters filterSearchParameters);
        int WeeklyDollarVolumeReportCount(FilterSearchParameters filterSearchParameters);
    }
}
