﻿using System;

namespace MTD.Core.DataTransferObjects
{
   public class DriverTransactionDto
    {
        public int TransID { get; set; }
        public string VehicleNo { get; set; }
        public string DriverNo { get; set; }
        public int fk_GatewayID { get; set; }
        public string GatewayReturnCode { get; set; }
        public string GatewayReturnMsg { get; set; }
        public Nullable<DateTime> TransDate { get; set; }
        public Nullable<int> TransReturncode { get; set; }
        public decimal TransAmount { get; set; }
        public string TransType { get; set; }
        public Nullable<int> TransRefID { get; set; }
        public string TransMode { get; set; }
        public string TransCardHolderName { get; set; }
        public string TransEmail { get; set; }
        public string TransPhone { get; set; }
        public string TransAddress { get; set; }
        public string TransCity { get; set; }
        public string TransState { get; set; }
        public string TransZip { get; set; }
        public string TransCountry { get; set; }
        public string TransCCNo { get; set; }
        public string TransCCType { get; set; }
        public string TransGatewayTnxID { get; set; }
        public string TransAuthCode { get; set; }
        public string TransReturnMsg { get; set; }
        public string Currency { get; set; }
        public Nullable<decimal> SurAmt { get; set; }
        public Nullable<decimal> TaxAmt { get; set; }
        public Nullable<decimal> BookingAmt { get; set; }
        public Nullable<decimal> Fare { get; set; }
        public Nullable<decimal> Toll { get; set; }
        public Nullable<decimal> Tip { get; set; }
        public Nullable<decimal> FlagFall { get; set; }
        public Nullable<DateTime> StartDateTime { get; set; }
        public Nullable<DateTime> EndDateTime { get; set; }
        public string PickAdd { get; set; }
        public string DestAdd { get; set; }
        public string TransNote { get; set; }
        public Nullable<decimal> Extras { get; set; }
        public Nullable<double> EndLat { get; set; }
        public Nullable<double> StartLat { get; set; }
        public Nullable<double> StartLong { get; set; }
        public Nullable<double> EndLong { get; set; }
        public Nullable<bool> Trans_IsMoto { get; set; }
        public string ResponseXML { get; set; }
        public string PNRef { get; set; }
        public string TransCCExpDate { get; set; }
        public string BatchNumber { get; set; }
        public Nullable<int> fk_MerchantID { get; set; }
    }
}
