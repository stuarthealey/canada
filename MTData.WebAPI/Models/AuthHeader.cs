﻿using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class AuthHeader
    {
        [DataMember]
        public string Status { get; set; }

        private string deviceID;

        [DataMember]
        public string DeviceId
        {
            get { return this.deviceID ?? "null"; }
            set { this.deviceID = value; }
        }
             [DataMember]
        public int TransID { get; set; }

       

        private string message { get; set; }

        [DataMember]
        public string Message
        {
            get { return this.message ?? "null"; }
            set { this.message = value; }
        }
    }
}