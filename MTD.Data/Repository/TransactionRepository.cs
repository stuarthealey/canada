﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;

using MTD.Data.Data;
using MTD.Data.Repository.Interface;
using MTData.Utility;

namespace MTD.Data.Repository
{
    public class TransactionRepository : BaseRepository, ITransactionRepository
    {
        private const string RefundMessage = "Refund";
        private const string VoidMessage = "Void";
        private const string CompletionMessage = "Completion";
        private const string CardTypeInterac = "Interac";

        public TransactionRepository(IMtDataDevlopmentsEntities dbMtData) : base(dbMtData)
        {
        }

        /// <summary>
        /// Purpose             :   To create new transaction
        /// Function Name       :   Create
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   12/23/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public int Create(MTDTransaction transaction)
        {
            try
            {
                DbMtData.MTDTransactions.Add(transaction);
                DbMtData.CommitChanges();
                int id = transaction.Id;
                return id;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To manage card token
        /// Function Name       :   ManageCardToken
        /// Created By          :   Sunil Singh
        /// Created On          :   06/26/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="cardToken"></param>
        /// <returns></returns>
        public int ManageCardToken(CardToken cardToken)
        {
            try
            {
                var cardNumber = cardToken.CardNumber;
				var customerId = cardToken.CustomerId;

				//	POD : Do not replace a token unless the customerid matches. as otherwise it is only the last 4 masked digits is used as the key.
                CardToken token = DbMtData.CardTokens.FirstOrDefault(o => o.CardNumber == cardNumber && o.CustomerId == customerId);

                if (token != null)
                {
                    token.Token = cardToken.Token;
                    token.CardNumber = cardToken.CardNumber;
                    token.CardType = cardToken.CardType;
                    token.CustomerId = cardToken.CustomerId;
                    token.ExpiryDate = cardToken.ExpiryDate;
                    token.EncryptMethod = cardToken.EncryptMethod;
                    token.KeyVersion = cardToken.KeyVersion;

                    //PAY-12 Get additional AVS data
                    token.Unit = cardToken.Unit;
                    token.StreetNumber = cardToken.StreetNumber;
                    token.Street = cardToken.Street;
                    token.AddressLine1 = cardToken.AddressLine1;
                    token.AddressLine2 = cardToken.AddressLine2;
                    token.Suburb = cardToken.Suburb;
                    token.State = cardToken.State;
                    token.Postcode = cardToken.Postcode;

                    DbMtData.CommitChanges();

                    return token.Id;
                }
                else
                {
                    DbMtData.CardTokens.Add(cardToken);
                    DbMtData.CommitChanges();

                    return cardToken.Id;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        ///  Purpose            :   To update the response of the transaction into database
        /// Function Name       :   Update
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/2/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="trx"></param>
        /// <param name="transId"></param>
        /// <param name="rapidConnectAuthId"></param>
        public void Update(MTDTransaction trx, int transId, string rapidConnectAuthId)
        {
            try
            {
                MTDTransaction trxUpdate = DbMtData.MTDTransactions.FirstOrDefault(x => x.Id == transId);
                if (trxUpdate != null)
                {
                    trxUpdate.ResponseCode = trx.ResponseCode;
                    trxUpdate.AddRespData = trx.AddRespData;
                    trxUpdate.CardLvlResult = trx.CardLvlResult;
                    trxUpdate.SrcReasonCode = trx.SrcReasonCode;
                    trxUpdate.AuthId = trx.AuthId;
                    trxUpdate.AthNtwId = trx.AthNtwId;
                    trxUpdate.ResponseXml = trx.ResponseXml;
                    trxUpdate.CardToken = trx.CardToken;
                    trxUpdate.LocalDateTime = trx.LocalDateTime;
                    trxUpdate.TransmissionDateTime = trx.TransmissionDateTime;
                    trxUpdate.TxnDate = trx.TxnDate;
                    trxUpdate.GatewayTxnId = trx.GatewayTxnId;

                    //D#6108 Need to also update the Amount when the response was Approved Lesser Amount.
                    if (trx.ResponseCode == "002")
                    {
                        trxUpdate.Amount = trx.Amount;
                    }

                    if (!string.IsNullOrEmpty(rapidConnectAuthId) && trx.ResponseCode == "000")
                    {
                        int previousTrxId = Convert.ToInt32(rapidConnectAuthId);
                        MTDTransaction previousTrxresponse = DbMtData.MTDTransactions.FirstOrDefault(x => x.Id == previousTrxId);
                    
                        if (previousTrxresponse != null)
                        {
                            if (trx.TxnType == "Completion")
                                previousTrxresponse.IsCompleted = true;
                            else if (trx.TxnType == "Void")
                                previousTrxresponse.IsVoided = true;
                            else if (trx.TxnType == "Refund")
                                previousTrxresponse.IsRefunded = true;
                        }
                    }
                }

                DbMtData.CommitChanges();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To update the response of the transaction into database
        /// Function Name       :   Update
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/2/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="transId"></param>
        /// <returns></returns>
        public MTDTransaction GetTransaction(int transId)
        {
            try
            {
                var transaction = DbMtData.MTDTransactions.FirstOrDefault(d => d.Id == transId);
                transaction.SerialNo = (from vh in DbMtData.Vehicles
                                        join trm in DbMtData.Terminals
                                            on vh.fk_TerminalID equals trm.TerminalID
                                        where vh.VehicleNumber == transaction.VehicleNo && vh.fk_FleetID == transaction.fk_FleetId && trm.IsActive
                                        select trm.MacAdd).FirstOrDefault();
                return transaction;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To get the transaction 
        /// Function Name       :   GetTransaction
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/2/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transId"></param>
        /// <param name="mid"></param>
        /// <returns></returns>
        public MTDTransaction GetTransaction(int transId, int? mid)
        {
            try
            {
                var transaction = DbMtData.MTDTransactions.FirstOrDefault(d => d.Id == transId && d.MerchantId == mid);//DbMtData.Transactions.Where(d => d.TransID == transID && d => d.fk == transID).FirstOrDefault();
                return transaction;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To get the transaction 
        /// Function Name       :   GetTransaction
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/2/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transId"></param>
        /// <param name="mid"></param>
        /// <returns></returns>
        public MTDTransaction GetTransaction(int userType, int userId, int merchantId, int transId, string project)
        {
            try
            {
                dynamic transaction;

                //if (project == "CN")
                //    transaction = DbMtData.MTDTransactions.FirstOrDefault(d => d.Id == transId && d.TxnType == "Authorization" && d.ResponseCode == "00" && d.IsRefunded == false && d.IsVoided == false);
                //else
                //    transaction = DbMtData.MTDTransactions.FirstOrDefault(d => d.Id == transId && d.TxnType == "Authorization" && d.ResponseCode == "000" && d.IsRefunded == false && d.IsVoided == false);

                if (project == "CN")
                    transaction = DbMtData.MTDTransactions.FirstOrDefault(d => d.Id == transId && d.TxnType == "Authorization" && d.ResponseCode == "00" && d.IsRefunded == false && d.IsVoided == false && d.IsCompleted == false);
                else
                    transaction = DbMtData.MTDTransactions.FirstOrDefault(d => d.Id == transId && d.TxnType == "Authorization" && d.ResponseCode == "000" && d.IsRefunded == false && d.IsVoided == false && d.IsCompleted == false);

                IEnumerable<Fleet> fleetLi = new List<Fleet>();
                IEnumerable<UserFleet> fleetList = new List<UserFleet>();
                
                if (userType == 2 || userType == 3)
                {
                    fleetLi = (from fleet in DbMtData.Fleets
                               where fleet.fk_MerchantID == merchantId && fleet.IsActive
                               select new { fleet }).Distinct().AsEnumerable().Select(objNew =>
                                                    new Fleet { FleetID = objNew.fleet.FleetID }).ToList();
                }
                
                if (userType == 4)
                {
                    fleetLi = (from fleet in DbMtData.Fleets
                               join ufleet in DbMtData.UserFleets on fleet.FleetID equals ufleet.fk_FleetID
                               where fleet.fk_MerchantID == merchantId && ufleet.fk_UserID == userId && fleet.IsActive
                               select new { fleet }).Distinct().AsEnumerable().Select(objNew =>
                                                   new Fleet { FleetID = objNew.fleet.FleetID }).ToList();
                }
                
                if (userType == 5)
                {
                    IEnumerable<Fleet> merchantFleets = MerchantFleetList(merchantId);
                    IEnumerable<UserFleet> accessedFleet = GetUsersFleet(userId);
                    merchantFleets = merchantFleets.Where(x => accessedFleet.Select(y => y.fk_FleetID).Contains((int)x.FleetID)).ToList();
                    bool isFound = merchantFleets.Select(x => x.FleetID).Contains((int)transaction.fk_FleetId);
                    
                    if (isFound)
                        return transaction;
                }

                if (transaction != null)
                {
                    bool isFound = fleetLi.Select(x => x.FleetID).Contains((int)transaction.fk_FleetId);
                    if (isFound)
                        return transaction;
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To get the total no. of transaction 
        /// Function Name       :   GetTransactionCount
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/2/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public int GetTransactionCount(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_SearchTransactionReport_Result> query = DbMtData.usp_SearchTransactionReport(tvd.Id, 
                    tvd.VehicleNo, 
                    tvd.DriverNo, 
                    tvd.startDate, 
                    tvd.endDate, 
                    tvd.CardType, 
                    tvd.EntryMode, 
                    tvd.AuthId, 
                    tvd.Industry, 
                    tvd.TxnType, 
                    tvd.MerchantId, 
                    tvd.ExpiryDate, 
                    tvd.MinAmount, 
                    tvd.MaxAmount, 
                    tvd.FirstFourDigits, 
                    tvd.LastFourDigits, 
                    tvd.AddRespData, 
                    tvd.JobNumber, 
                    Convert.ToString(tvd.fk_FleetId), 
                    tvd.IsVarified, 
                    Convert.ToString(tvd.TerminalId), 
                    tvd.SerialNo);

                return query.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To get the transaction 
        /// Function Name       :   GetTransaction
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/5/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public IEnumerable<usp_SearchTransactionReport_Result> GetTransaction(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_SearchTransactionReport_Result> query = DbMtData.usp_SearchTransactionReport(tvd.Id, tvd.VehicleNo, tvd.DriverNo, tvd.startDate, tvd.endDate, tvd.CardType, tvd.EntryMode, tvd.AuthId, tvd.Industry, tvd.TxnType, tvd.MerchantId, tvd.ExpiryDate, tvd.MinAmount, tvd.MaxAmount, tvd.FirstFourDigits, tvd.LastFourDigits, tvd.AddRespData, tvd.JobNumber, Convert.ToString(tvd.fk_FleetId), tvd.IsVarified, Convert.ToString(tvd.TerminalId), tvd.SerialNo);

                if (!string.IsNullOrEmpty(tvd.Sorting))
                {
                    query = SortedTransactionReport(tvd.Sorting, query);
                    return query.Skip(tvd.PageNumber).Take(tvd.PageSize);
                }
                return query.OrderByDescending(x => x.TxnDate).Skip(tvd.PageNumber).Take(tvd.PageSize);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To get driver
        /// Function Name       :   GetDriver
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/6/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetDriversTransaction_Result> GetDriver(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_GetDriversTransaction_Result> query = DbMtData.usp_GetDriversTransaction(tvd.MerchantId, tvd.FleetId, tvd.VehicleNo, tvd.DriverNo, tvd.startDate, tvd.endDate);

                if (!string.IsNullOrEmpty(tvd.Sorting))
                {
                    query = SortedGetDriver(tvd.Sorting, query);
                }
                return query.Skip(tvd.PageNumber).Take(tvd.PageSize);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To get technology summary report
        /// Function Name       :   GetTechnologySummary
        /// Created By          :   Naveen Kumar
        /// Created On          :   10/6/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public IEnumerable<usp_TechnologySummaryReport_Result> GetTechnologySummary(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_TechnologySummaryReport_Result> query = DbMtData.usp_TechnologySummaryReport(tvd.MerchantId, tvd.FleetId, tvd.startDate, tvd.endDate);
                if (!string.IsNullOrEmpty(tvd.Sorting))
                {
                    query = SortedGetTechSummary(tvd.Sorting, query);
                }
                return query.Skip(tvd.PageNumber).Take(tvd.PageSize).ToList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To get technology detail report
        /// Function Name       :   GetTechnologyDetail
        /// Created By          :   Naveen Kumar
        /// Created On          :   10/6/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public IEnumerable<usp_TechnologyDetailReport_Result> GetTechnologyDetail(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_TechnologyDetailReport_Result> query = DbMtData.usp_TechnologyDetailReport(tvd.MerchantId, tvd.FleetId, tvd.VehicleNo, tvd.DriverNo, tvd.startDate, tvd.endDate, tvd.CardType);
                if (!string.IsNullOrEmpty(tvd.Sorting))
                {
                    query = SortedGetTechDetail(tvd.Sorting, query);
                }
                return query.Skip(tvd.PageNumber).Take(tvd.PageSize);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To get the total no. of drivers
        /// Function Name       :   GetDriverCount
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/7/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public int GetDriverCount(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_GetDriversTransaction_Result> getDriversTransaction = DbMtData.usp_GetDriversTransaction(tvd.MerchantId, tvd.FleetId, tvd.VehicleNo, tvd.DriverNo, tvd.startDate, tvd.endDate);
                return getDriversTransaction.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To get the total no. of technology fee records
        /// Function Name       :   GetTechFeeCount
        /// Created By          :   naveen Kumar
        /// Created On          :   10/6/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public int GetTechFeeCount(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_TechnologySummaryReport_Result> query = DbMtData.usp_TechnologySummaryReport(tvd.MerchantId, tvd.FleetId, tvd.startDate, tvd.endDate);
                return query.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To get the total no. of technology fee records
        /// Function Name       :   GetTechFeeCount
        /// Created By          :   naveen Kumar
        /// Created On          :   10/6/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public int GetTechFeeDetailCount(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_TechnologyDetailReport_Result> query = DbMtData.usp_TechnologyDetailReport(tvd.MerchantId, tvd.FleetId, tvd.VehicleNo, tvd.DriverNo, tvd.startDate, tvd.endDate, tvd.CardType);
                return query.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To check whether driver exist or not
        /// Function Name       :   DriverVhecileExist
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/8/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverNo"></param>
        /// <param name="vehicleNo"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public bool DriverVhecileExist(string driverNo, string vehicleNo, int merchantId)
        {
            try
            {
                IQueryable<int> merchantFleets = from res in DbMtData.Fleets.Where(x => x.fk_MerchantID == merchantId) select res.FleetID;
                
                IQueryable<string> merchantDrivers = from item in DbMtData.Drivers
                                                     where merchantFleets.Contains(item.fk_FleetID)
                                                     select item.DriverNo;
                
                IQueryable<string> merchantVehicle = from item in DbMtData.Vehicles
                                                     where merchantFleets.Contains(item.fk_FleetID)
                                                     select item.VehicleNumber;
                
                if (merchantDrivers.Contains(driverNo) && merchantVehicle.Contains(vehicleNo))
                    return true;
            }
            catch
            {
                throw;
            }

            return false;
        }

        /// <summary>
        ///  Purpose            :   To get merchant id
        /// Function Name       :   GetMerchantId
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/12/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverNo"></param>
        /// <param name="vehicleNo"></param>
        /// <returns></returns>
        public int GetMerchantId(string driverNo, string vehicleNo)
        {
            try
            {
                int driverFleetId = 0;
                int vehicleFleetId = 0;
                int merchantbyIdByDriver = 0;
                int merchantbyIdByVehicle = 0;
                var firstvehicleFleetId = (from q in DbMtData.Vehicles where q.VehicleNumber == vehicleNo select q).FirstOrDefault();
                if (firstvehicleFleetId != null)
                {
                    vehicleFleetId = firstvehicleFleetId.fk_FleetID;
                }

                var firstdriverFleetId = (from q in DbMtData.Drivers where q.DriverNo == driverNo select q).FirstOrDefault();
                if (firstdriverFleetId != null)
                {
                    driverFleetId = firstdriverFleetId.fk_FleetID;
                }

                var firstMerchantIdByDriver = (from q in DbMtData.Fleets where q.FleetID == driverFleetId select q).FirstOrDefault();
                if (firstMerchantIdByDriver != null)
                {
                    merchantbyIdByDriver = firstMerchantIdByDriver.fk_MerchantID;
                }
                var firstMerchantIdByVehicle = (from q in DbMtData.Fleets where q.FleetID == vehicleFleetId select q).FirstOrDefault();
                if (firstMerchantIdByVehicle != null)
                {
                    merchantbyIdByVehicle = firstMerchantIdByVehicle.fk_MerchantID;
                }

                if (merchantbyIdByDriver > 0 && merchantbyIdByVehicle > 0 &&
                    merchantbyIdByVehicle == merchantbyIdByDriver)
                {
                    return merchantbyIdByDriver;
                }
            }
            catch
            {
                throw;
            }

            return 0;
        }

        /// <summary>
        ///  Purpose            :   To add transaction into database
        /// Function Name       :   Add
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/12/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public int Add(MTDTransaction transaction)
        {
            try
            {
                transaction.TxnDate = DateTime.UtcNow;
                transaction.IsCompleted = false;
                transaction.IsRefunded = false;
                transaction.IsVoided = false;
                transaction.IsVarified = false;
                transaction.IsDownloaded = false;
                DbMtData.MTDTransactions.Add(transaction);
                DbMtData.CommitChanges();
                return transaction.Id;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To update the response of the transaction into database
        /// Function Name       :   Update
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/2/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="trx"></param>
        public void Update(MTDTransaction trx)
        {
            if (trx.TransRefNo != null)
            {
                try
                {
                    MTDTransaction trxUpdate = DbMtData.MTDTransactions.FirstOrDefault(x => x.TransRefNo == trx.TransRefNo);
                    if (trxUpdate != null)
                    {
                        trxUpdate.ResponseCode = trx.ResponseCode;
                        trxUpdate.AddRespData = trx.AddRespData;
                        trxUpdate.CardLvlResult = trx.CardLvlResult;
                        trxUpdate.SrcReasonCode = trx.SrcReasonCode;
                        trxUpdate.GatewayTxnId = trx.GatewayTxnId;
                        trxUpdate.AuthId = trx.AuthId;
                        trxUpdate.AthNtwId = trx.AthNtwId;
                        trxUpdate.ResponseXml = trx.ResponseXml;
                    }

                    DbMtData.CommitChanges();
                }
                catch
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vechileNumber"></param>
        /// <returns></returns>
        public string GetDriverDevice(string vechileNumber, int fleetId)
        {
            Vehicle vehicle = DbMtData.Vehicles.FirstOrDefault(x => x.VehicleNumber == vechileNumber && x.fk_FleetID == fleetId && x.IsActive);
            if (vehicle != null)
            {
                Terminal termainl = DbMtData.Terminals.FirstOrDefault(x => x.TerminalID == vehicle.fk_TerminalID && x.IsActive);
                if (termainl != null)
                    return termainl.MacAdd;
            }

            return string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public TxnTaxSurcharges GetTaxSurchages(int merchantId)
        {
            var merchant = DbMtData.Merchants.FirstOrDefault(x => x.MerchantID == merchantId);
            TxnTaxSurcharges txnSurcharge = new TxnTaxSurcharges();
            if (merchant != null)
            {

                txnSurcharge.StateTaxRate = merchant.StateTaxRate;
                txnSurcharge.FederalTaxRate = merchant.FederalTaxRate;
                txnSurcharge.TipPerLow = merchant.TipPerLow;
                txnSurcharge.TipPerMedium = merchant.TipPerMedium;
                txnSurcharge.TipPerHigh = merchant.TipPerHigh;

                if (merchant.IsTaxInclusive != null) txnSurcharge.IsTaxInclusive = (bool)merchant.IsTaxInclusive;
                var surharge = DbMtData.Surcharges.FirstOrDefault(x => x.MerchantId == merchantId && x.IsActive);
                if (surharge != null)
                {
                    txnSurcharge.SurchargeType = surharge.SurchargeType;
                    txnSurcharge.SurMaxCap = surharge.SurMaxCap;
                    txnSurcharge.SurchargePer = surharge.SurchargePer;
                    txnSurcharge.SurchargeFixed = surharge.SurchargeFixed;
                }
            }

            return txnSurcharge;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="driverNo"></param>
        /// <returns></returns>
        public TxnTaxSurcharges GetTaxSurchages(int merchantId, int driverNo)
        {
            var merchant = DbMtData.Merchants.FirstOrDefault(x => x.MerchantID == merchantId && x.IsActive);
            TxnTaxSurcharges txnSurcharge = new TxnTaxSurcharges();
            if (merchant != null)
            {
                bool surchargeFound = DbMtData.Surcharges.Any(a => a.MerchantId == merchantId && a.IsActive && a.FleetID == driverNo);
                if (surchargeFound)
                {
                    var surharge = DbMtData.Surcharges.FirstOrDefault(a => a.MerchantId == merchantId && a.IsActive && a.FleetID == driverNo);
                    txnSurcharge.SurchargeType = surharge.SurchargeType;
                    txnSurcharge.SurMaxCap = surharge.SurMaxCap;
                    txnSurcharge.SurchargePer = surharge.SurchargePer;
                    txnSurcharge.SurchargeFixed = surharge.SurchargeFixed;

                    txnSurcharge.FeeType = surharge.BookingFeeType;
                    txnSurcharge.FeeFixed = surharge.BookingFeeFixed;
                    txnSurcharge.FeePer = surharge.BookingFeePer;
                    txnSurcharge.FeeMaxCap = surharge.BookingFeeMaxCap;
                    return txnSurcharge;
                }
                else
                {
                    DefaultSetting defaultDbSurchargeFee = DbMtData.DefaultSettings.Find(2);
                    if (defaultDbSurchargeFee != null)
                    {
                        txnSurcharge.SurchargeType = defaultDbSurchargeFee.SurchargeType;
                        txnSurcharge.SurMaxCap = defaultDbSurchargeFee.SurchargeMaxCap;
                        txnSurcharge.SurchargePer = defaultDbSurchargeFee.SurchargePer;
                        txnSurcharge.SurchargeFixed = defaultDbSurchargeFee.SurchargeFixed;
                        txnSurcharge.FeeType = defaultDbSurchargeFee.BookingFeeType;
                        txnSurcharge.FeeFixed = defaultDbSurchargeFee.BookingFeeFixed;
                        txnSurcharge.FeePer = defaultDbSurchargeFee.BookingFeePer;
                        txnSurcharge.FeeMaxCap = defaultDbSurchargeFee.BookingMaxCap;
                    }
                    else
                    {
                        txnSurcharge.SurchargeType = 0;
                        txnSurcharge.SurchargeFixed = 0;
                        txnSurcharge.SurchargePer = 1;
                        txnSurcharge.SurMaxCap = 0;
                        txnSurcharge.FeeType = 0;
                        txnSurcharge.FeeFixed = 0;
                        txnSurcharge.FeePer = 1;
                        txnSurcharge.FeeMaxCap = 0;
                    }
                }

                return txnSurcharge;
            }

            return txnSurcharge;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mid"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetDrivers_Result> GetDrivers(int mid)
        {
            IEnumerable<usp_GetDrivers_Result> driverList = DbMtData.usp_GetDrivers(mid);
            var newList = driverList.OrderBy(x => x.DriverNo).ToList();
            return newList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mid"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetVehicles_Result> GetVehicle(int mid)
        {
            IEnumerable<usp_GetVehicles_Result> vehileList = DbMtData.usp_GetVehicles(mid);
            var newList = vehileList.OrderBy(x => x.VehicleNumber).ToList();
            return newList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public TxnTaxSurcharges GetTaxSurchages()
        {
            DefaultSetting defaultDbSurcharge = DbMtData.DefaultSettings.Find(2);
            TxnTaxSurcharges txnSurcharge = new TxnTaxSurcharges();
            if (defaultDbSurcharge != null)
            {
                txnSurcharge.SurchargeType = defaultDbSurcharge.SurchargeType;
                txnSurcharge.SurMaxCap = defaultDbSurcharge.SurchargeMaxCap;
                txnSurcharge.SurchargePer = defaultDbSurcharge.SurchargePer;
                txnSurcharge.SurchargeFixed = defaultDbSurcharge.SurchargeFixed;
            }
            else
            {
                txnSurcharge.SurchargeType = 0;
                txnSurcharge.SurchargeFixed = 0;
                txnSurcharge.SurchargePer = 1;
                txnSurcharge.SurMaxCap = 0;
            }

            DefaultSetting defaultDbFee = DbMtData.DefaultSettings.Find(3);
            if (defaultDbFee != null)
            {
                txnSurcharge.FeeType = defaultDbFee.BookingFeeType;
                txnSurcharge.FeeFixed = defaultDbFee.BookingFeeFixed;
                txnSurcharge.FeePer = defaultDbFee.BookingFeePer;
                txnSurcharge.FeeMaxCap = defaultDbFee.BookingMaxCap;
            }
            else
            {
                txnSurcharge.FeeType = 0;
                txnSurcharge.FeeFixed = 0;
                txnSurcharge.FeePer = 1;
                txnSurcharge.FeeMaxCap = 0;
            }

            return txnSurcharge;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<usp_SearchTransactionReport_Result> SortedTransactionReport(string sorting, IEnumerable<usp_SearchTransactionReport_Result> query)
        {
            switch (sorting)
            {
                case "VehicleNo ASC":
                    query = query.OrderBy(p => p.VehicleNo);
                    break;
                case "VehicleNo DESC":
                    query = query.OrderByDescending(p => p.VehicleNo);
                    break;
                case "DriverNo ASC":
                    query = query.OrderBy(p => p.DriverNo);
                    break;
                case "DriverNo DESC":
                    query = query.OrderByDescending(p => p.DriverNo);
                    break;
                case "CardType ASC":
                    query = query.OrderBy(p => p.CardType);
                    break;
                case "CardType DESC":
                    query = query.OrderByDescending(p => p.CardType);
                    break;
                case "Amount ASC":
                    query = query.OrderBy(p => p.Amount);
                    break;
                case "Amount DESC":
                    query = query.OrderByDescending(p => p.Amount);
                    break;
                case "TxnType ASC":
                    query = query.OrderBy(p => p.TxnType);
                    break;
                case "TxnType DESC":
                    query = query.OrderByDescending(p => p.TxnType);
                    break;
                case "StringDateTime ASC":
                    query = query.OrderBy(p => p.TxnDate);
                    break;
                case "StringDateTime DESC":
                    query = query.OrderByDescending(p => p.TxnDate);
                    break;
                case "PaymentType ASC":
                    query = query.OrderBy(p => p.PaymentType);
                    break;
                case "PaymentType DESC":
                    query = query.OrderByDescending(p => p.PaymentType);
                    break;
                case "AddRespData ASC":
                    query = query.OrderBy(p => p.AddRespData);
                    break;
                case "AddRespData DESC":
                    query = query.OrderByDescending(p => p.AddRespData);
                    break;
            }

            return query;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<usp_GetDriversTransaction_Result> SortedGetDriver(string sorting, IEnumerable<usp_GetDriversTransaction_Result> query)
        {
            switch (sorting)
            {
                case "DriverName ASC":
                    query = query.OrderBy(p => p.DriverName);
                    break;
                case "DriverName DESC":
                    query = query.OrderByDescending(p => p.DriverName);
                    break;
                case "DriverNo ASC":
                    query = query.OrderBy(p => p.DriverNo);
                    break;
                case "DriverNo DESC":
                    query = query.OrderByDescending(p => p.DriverNo);
                    break;
                case "VehicleNo ASC":
                    query = query.OrderBy(p => p.VehicleNo);
                    break;
                case "VehicleNo DESC":
                    query = query.OrderByDescending(p => p.VehicleNo);
                    break;
                case "Fleet ASC":
                    query = query.OrderBy(p => p.Fleet);
                    break;
                case "Fleet DESC":
                    query = query.OrderByDescending(p => p.Fleet);
                    break;
                case "NumberOfKeyed ASC":
                    query = query.OrderBy(p => p.NumberOfKeyed);
                    break;
                case "NumberOfKeyed DESC":
                    query = query.OrderByDescending(p => p.NumberOfKeyed);
                    break;
                case "KeyedSaleAmount ASC":
                    query = query.OrderBy(p => p.KeyedSaleAmount);
                    break;
                case "KeyedSaleAmount DESC":
                    query = query.OrderByDescending(p => p.KeyedSaleAmount);
                    break;
                case "NumberOfSwiped ASC":
                    query = query.OrderBy(p => p.NumberOfSwiped);
                    break;
                case "NumberOfSwiped DESC":
                    query = query.OrderByDescending(p => p.NumberOfSwiped);
                    break;
                case "SwipedSaleAmount ASC":
                    query = query.OrderBy(p => p.SwipedSaleAmount);
                    break;
                case "SwipedSaleAmount DESC":
                    query = query.OrderByDescending(p => p.SwipedSaleAmount);
                    break;
                case "TotalNumberOfSale ASC":
                    query = query.OrderBy(p => p.TotalNumberOfSale);
                    break;
                case "TotalNumberOfSale DESC":
                    query = query.OrderByDescending(p => p.TotalNumberOfSale);
                    break;
                case "TotalSaleAmount ASC":
                    query = query.OrderBy(p => p.TotalSaleAmount);
                    break;
                case "TotalSaleAmount DESC":
                    query = query.OrderByDescending(p => p.TotalSaleAmount);
                    break;
            }

            return query;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<usp_TechnologySummaryReport_Result> SortedGetTechSummary(string sorting, IEnumerable<usp_TechnologySummaryReport_Result> query)
        {
            switch (sorting)
            {
                case "FleetName ASC":
                    query = query.OrderBy(p => p.FleetName);
                    break;
                case "FleetName DESC":
                    query = query.OrderByDescending(p => p.FleetName);
                    break;
                case "CountVehicle ASC":
                    query = query.OrderBy(p => p.CountVehicle);
                    break;
                case "CountVehicle DESC":
                    query = query.OrderByDescending(p => p.CountVehicle);
                    break;
                case "AutomaticTrans ASC":
                    query = query.OrderBy(p => p.AutomaticTrans);
                    break;
                case "AutomaticTrans DESC":
                    query = query.OrderByDescending(p => p.AutomaticTrans);
                    break;
                case "ManualTrans ASC":
                    query = query.OrderBy(p => p.ManualTrans);
                    break;
                case "ManualTrans DESC":
                    query = query.OrderByDescending(p => p.ManualTrans);
                    break;
                case "CountRefund ASC":
                    query = query.OrderBy(p => p.CountRefund);
                    break;
                case "CountRefund DESC":
                    query = query.OrderByDescending(p => p.CountRefund);
                    break;
                case "CountVoid ASC":
                    query = query.OrderBy(p => p.CountVoid);
                    break;
                case "CountVoid DESC":
                    query = query.OrderByDescending(p => p.CountVoid);
                    break;
                case "TotalTrans ASC":
                    query = query.OrderBy(p => p.TotalTrans);
                    break;
                case "TotalTrans DESC":
                    query = query.OrderByDescending(p => p.TotalTrans);
                    break;
                case "TechFee ASC":
                    query = query.OrderBy(p => p.TechFee);
                    break;
                case "TechFee DESC":
                    query = query.OrderByDescending(p => p.TechFee);
                    break;
                case "FleetFee ASC":
                    query = query.OrderBy(p => p.FleetFee);
                    break;
                case "FleetFee DESC":
                    query = query.OrderByDescending(p => p.FleetFee);
                    break;
                case "Surcharge ASC":
                    query = query.OrderBy(p => p.Surcharge);
                    break;
                case "Surcharge DESC":
                    query = query.OrderByDescending(p => p.Surcharge);
                    break;
                case "Booking_Fee ASC":
                    query = query.OrderBy(p => p.Booking_Fee);
                    break;
                case "Booking_Fee DESC":
                    query = query.OrderByDescending(p => p.Booking_Fee);
                    break;
                case "TotalAmt ASC":
                    query = query.OrderBy(p => p.TotalAmt);
                    break;
                case "TotalAmt DESC":
                    query = query.OrderByDescending(p => p.TotalAmt);
                    break;
            }

            return query;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<usp_TechnologyDetailReport_Result> SortedGetTechDetail(string sorting, IEnumerable<usp_TechnologyDetailReport_Result> query)
        {
            switch (sorting)
            {
                case "TransId ASC":
                    query = query.OrderBy(p => p.TransId);
                    break;
                case "TransId DESC":
                    query = query.OrderByDescending(p => p.TransId);
                    break;
                case "FleetName ASC":
                    query = query.OrderBy(p => p.FleetName);
                    break;
                case "FleetName DESC":
                    query = query.OrderByDescending(p => p.FleetName);
                    break;
                case "TxnDate ASC":
                    query = query.OrderBy(p => p.TxnDate);
                    break;
                case "TxnDate DESC":
                    query = query.OrderByDescending(p => p.TxnDate);
                    break;
                case "VehicleNo ASC":
                    query = query.OrderBy(p => p.VehicleNo);
                    break;
                case "VehicleNo DESC":
                    query = query.OrderByDescending(p => p.VehicleNo);
                    break;
                case "DriverNo ASC":
                    query = query.OrderBy(p => p.DriverNo);
                    break;
                case "DriverNo DESC":
                    query = query.OrderByDescending(p => p.DriverNo);
                    break;
                case "TxnType ASC":
                    query = query.OrderBy(p => p.TxnType);
                    break;
                case "TxnType DESC":
                    query = query.OrderByDescending(p => p.TxnType);
                    break;
                case "Amount ASC":
                    query = query.OrderBy(p => p.Amount);
                    break;
                case "Amount DESC":
                    query = query.OrderByDescending(p => p.Amount);
                    break;
                case "CardType ASC":
                    query = query.OrderBy(p => p.CardType);
                    break;
                case "CardType DESC":
                    query = query.OrderByDescending(p => p.CardType);
                    break;
                case "TechFee ASC":
                    query = query.OrderBy(p => p.TechFee);
                    break;
                case "TechFee DESC":
                    query = query.OrderByDescending(p => p.TechFee);
                    break;
                case "FleetFee ASC":
                    query = query.OrderBy(p => p.FleetFee);
                    break;
                case "FleetFee DESC":
                    query = query.OrderByDescending(p => p.FleetFee);
                    break;
                case "Surcharge ASC":
                    query = query.OrderBy(p => p.Surcharge);
                    break;
                case "Surcharge DESC":
                    query = query.OrderByDescending(p => p.Surcharge);
                    break;
                case "Fee ASC":
                    query = query.OrderBy(p => p.Booking_Fee);
                    break;
                case "Fee DESC":
                    query = query.OrderByDescending(p => p.Booking_Fee);
                    break;
                case "AuthId ASC":
                    query = query.OrderBy(p => p.AuthId);
                    break;
                case "AuthId DESC":
                    query = query.OrderByDescending(p => p.AuthId);
                    break;
            }

            return query;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="txnId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool Varified(int txnId, bool status)
        {
            try
            {
                MTDTransaction mtTxn = DbMtData.MTDTransactions.Where(x => x.Id == txnId).FirstOrDefault();
                mtTxn.IsVarified = status;
                return Convert.ToBoolean(DbMtData.CommitChanges());
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ManyTxnVerified"></param>
        /// <returns></returns>
        public bool VarifiedMany(ManyTxnClass[] ManyTxnVerified)
        {
            try
            {
                foreach (ManyTxnClass mtc in ManyTxnVerified)
                {
                    MTDTransaction mtTxn = DbMtData.MTDTransactions.Where(x => x.Id == mtc.Id).FirstOrDefault();
                    mtTxn.IsVarified = true;
                }
                return Convert.ToBoolean(DbMtData.CommitChanges());
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<usp_GetPayrollSAGETransaction_Result> SortedGetPayrollSage(string sorting, IEnumerable<usp_GetPayrollSAGETransaction_Result> query)
        {
            switch (sorting)
            {
                case "SageAccount ASC":
                    query = query.OrderBy(p => p.SageAccount);
                    break;
                case "TransactionAmount DESC":
                    query = query.OrderByDescending(p => p.TransactionAmount);
                    break;
            }

            return query;
        }

        /// <summary>
        ///  Purpose            :   To Get payroll For Sage
        /// Function Name       :   GetpayrollForSage
        /// Created By          :  Shishir Saunakia
        /// Created On          :   1/6/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetPayrollSAGETransaction_Result> GetpayrollSage(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_GetPayrollSAGETransaction_Result> query = DbMtData.usp_GetPayrollSAGETransaction(tvd.MerchantId, tvd.FleetId, tvd.PayerType, tvd.DriverNo, tvd.startDate, tvd.endDate, tvd.NetworkType);

                if (!string.IsNullOrEmpty(tvd.Sorting))
                {
                    query = SortedGetPayrollSage(tvd.Sorting, query);
                }
                return query.Skip(tvd.PageNumber).Take(tvd.PageSize).ToList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To get the total no. of PayrollSage
        /// Function Name       :   GetPayrollSageCount
        /// Created By          :   Shishir Saunakia
        /// Created On          :   10/06/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public int GetPayrollSageCount(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_GetPayrollSAGETransaction_Result> getPayrollSageTransaction = DbMtData.usp_GetPayrollSAGETransaction(tvd.MerchantId, tvd.FleetId, tvd.PayerType, tvd.DriverNo, tvd.startDate, tvd.endDate, tvd.NetworkType);
                return getPayrollSageTransaction.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<usp_GetPayrollPaymentTransaction_Result> SortedGetPayrollPayment(string sorting, IEnumerable<usp_GetPayrollPaymentTransaction_Result> query)
        {
            switch (sorting)
            {
                case "Name ASC":
                    query = query.OrderBy(p => p.Name);
                    break;
                case "Date DESC":
                    query = query.OrderByDescending(p => p.Date);
                    break;
            }

            return query;
        }

        /// <summary>
        ///  Purpose            :   To Get payroll For Payment
        /// Function Name       :   GetpayrollForPayment
        /// Created By          :  Shishir Saunakia
        /// Created On          :   10/07/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetPayrollPaymentTransaction_Result> GetpayrollPayment(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_GetPayrollPaymentTransaction_Result> query = DbMtData.usp_GetPayrollPaymentTransaction(tvd.MerchantId, tvd.FleetId, tvd.PayerType, tvd.DriverNo, tvd.startDate, tvd.endDate, tvd.NetworkType);

                if (!string.IsNullOrEmpty(tvd.Sorting))
                {
                    query = SortedGetPayrollPayment(tvd.Sorting, query);
                }
                return query.Skip(tvd.PageNumber).Take(tvd.PageSize).ToList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To get the total no. of PayrollPayment
        /// Function Name       :   GetPayrollPaymentCount
        /// Created By          :   Shishir Saunakia
        /// Created On          :   10/07/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public int GetPayrollPaymentCount(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_GetPayrollPaymentTransaction_Result> getPayrollPayTransaction = DbMtData.usp_GetPayrollPaymentTransaction(tvd.MerchantId, tvd.FleetId, tvd.PayerType, tvd.DriverNo, tvd.startDate, tvd.endDate, tvd.NetworkType);
                return getPayrollPayTransaction.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public Fleet GetFleetTechFee(int fleetId)
        {
            Fleet fleet = DbMtData.Fleets.Where(x => x.FleetID == fleetId && x.IsActive).FirstOrDefault();
            return fleet;
        }

        /// <summary>
        /// Purpose             :   to get the list of fleet
        /// Function Name       :   FleetList
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/25/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<Fleet> MerchantFleetList(int merchantId)
        {
            try
            {
                IEnumerable<Fleet> fleetList =
                    (from fleet in DbMtData.Fleets where fleet.fk_MerchantID == merchantId && fleet.IsActive select new { fleet.FleetID, fleet.FleetName }).AsEnumerable()
                        .Select(objNew =>
                            new Fleet
                            {
                                FleetID = objNew.FleetID,
                                FleetName = objNew.FleetName

                            }).ToList().OrderBy(x => x.FleetName);

                return fleetList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   to get the list of users's fleet
        /// Function Name       :   GetUsersFleet
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/22/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<UserFleet> GetUsersFleet(int userId)
        {
            try
            {
                IEnumerable<UserFleet> getFleet =
                    (from fleet in DbMtData.UserFleets
                     where fleet.fk_UserID == userId

                     select new { fleet.fk_FleetID, fleet.fk_UserID }).AsEnumerable()
                        .Select(objNew =>
                            new UserFleet
                            {
                                fk_FleetID = objNew.fk_FleetID,
                                fk_UserID = objNew.fk_UserID

                            }).ToList();
                return getFleet;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To get the transaction Data To TC Cashiering
        /// Function Name       :   GetTransaction
        /// Created By          :   Umesh Kumar
        /// Created On          :   11/18/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetTransactionData_Result> DownloadTransactionData(DownloadParameters dParam)
        {
            try
            {
                IEnumerable<usp_GetTransactionData_Result> query = DbMtData.usp_GetTransactionData(dParam.Fk_MerchantID, dParam.DateFrom, dParam.DateTo, dParam.TransType, dParam.DownloadStatus, dParam.VehicleId, dParam.DriverId, dParam.Fk_FleetId, dParam.EntryMode, dParam.BatchNumber, dParam.RetrieveSinceBatch);
                return query.OrderByDescending(x => x.TxnDate);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose            :   To Validate DownloadRequest. Returns 0-If Uid & Pwd is incorrect, 1- if pass, 2- If merchantId is not active
        /// Function Name       :   ValidateDownloadRequest
        /// Created By          :   Umesh Kumar
        /// Created On          :   11/23/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="uId"></param>
        /// <param name="pCode"></param>
        /// <returns></returns>
        public int ValidateDownloadRequest(string uId, string pCode, int mCode)
        {
            try
            {
                var isActiveMer = DbMtData.Merchants.Any(x => x.MerchantID == mCode && x.IsActive);
                if (isActiveMer)
                {
                    string passCode = (from m in DbMtData.ExternalUsers
                                      where m.UserName == uId && m.MerchantId == mCode
                                      select m.PCode).FirstOrDefault();

                    if (StringExtension.ValidatePassCode(pCode, passCode))
                        return 1;
                    else
                        return 0;
                }
                else
                    return 2;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public bool JobNumberRequired(int merchantId)
        {
            var merchant = DbMtData.Merchants.Where(x => x.MerchantID == merchantId && x.IsActive).FirstOrDefault();
            return Convert.ToBoolean(merchant.IsJobNoRequired);
        }

        /// <summary>
        /// Purpose             :   To fetch card token detail
        /// Function Name       :   GetCardToken
        /// Created By          :   Sunil Singh
        /// Created On          :   19-Jan-2016
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="cardToken"></param>
        /// <returns></returns>
        public CardToken GetCardToken(string customerId, string cardToken)
        {
            try
            {
                var cardTokenData = DbMtData.CardTokens.FirstOrDefault(o => o.Token == cardToken && o.CustomerId == customerId);
                return cardTokenData;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To fetch card token detail
        /// Function Name       :   GetCardToken
        /// Created By          :   Sunil Singh
        /// Created On          :   19-Jan-2016
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="cardToken"></param>
        /// <returns></returns>
        //public CardToken GetCardToken(string cardToken)
        //{
        //    try
        //    {
        //        var cardTokenData = DbMtData.CardTokens.FirstOrDefault(o => o.Token == cardToken);
        //        return cardTokenData;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public int SaveCapKey(CapKey capK)
        {
            try
            {
                CapKey cpKey = DbMtData.CapKeys.FirstOrDefault(o => o.IsNew == true && o.IsContactLess == capK.IsContactLess);
                if (cpKey != null)
                {
                    UpdateCapKeyStatus((bool)capK.IsContactLess);
                    var filerVersn = cpKey.FileVersion;
                    capK.FileVersion = (filerVersn == null) ? 0 : filerVersn + 0.1m;
                }
                else
                    capK.FileVersion = 1;

                capK.IsNew = true;

                DbMtData.CapKeys.Add(capK);
                DbMtData.CommitChanges();

                return capK.Id;
            }
            catch
            {
                throw;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public void UpdateCapKeyStatus(bool isContactless)
        {
            try
            {
                IQueryable<CapKey> capKeys = DbMtData.CapKeys.Where(x => x.IsContactLess == isContactless);

                foreach (var cpk in capKeys)
                {
                    cpk.IsNew = false;
                }

                DbMtData.CommitChanges();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public CapKey GetCapKey()
        {
            try
            {
                CapKey cpKey = DbMtData.CapKeys.FirstOrDefault(o => o.IsNew == true);
                return cpKey;
            }
            catch
            {
                throw;
            }
        }            

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public int GetMerchantId(int fleetId)
        {
            try
            {
                int fleetMerchant = DbMtData.Fleets.Where(x => x.FleetID == fleetId).FirstOrDefault().fk_MerchantID;
                return fleetMerchant;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetAdminSummaryReport_Result> GetadminSummary(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo)
        {
            try
            {
                IEnumerable<usp_GetAdminSummaryReport_Result> adminSummary = DbMtData.usp_GetAdminSummaryReport(dateFrom, dateTo);
                return adminSummary;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetCardTypeReport_Result> AdminCardypeReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo)
        {
            try
            {
                var adminCardType = DbMtData.usp_GetCardTypeReport(dateFrom, dateTo);
                return adminCardType;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetAdminExceptionReport_Result> AdminExceptionReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo)
        {
            IEnumerable<usp_GetAdminExceptionReport_Result> adminException = DbMtData.usp_GetAdminExceptionReport(dateFrom, dateTo);
            return adminException;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetTotalTransByVehicleReport_Result> TransByVehicleReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo)
        {
            try
            {
                IEnumerable<usp_GetTotalTransByVehicleReport_Result> vechicleRep = DbMtData.usp_GetTotalTransByVehicleReport(dateFrom, dateTo);
                return vechicleRep;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchnatId"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetDetailReport_Result> GetDetailsReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int merchnatId)
        {
            try
            {
                IEnumerable<usp_GetDetailReport_Result> detailsRep = DbMtData.usp_GetDetailReport(dateFrom, dateTo, merchnatId);
                return detailsRep;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetMerchantSummaryReport_Result> MerchantSummaryReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int merchantId)
        {
            try
            {
                IEnumerable<usp_GetMerchantSummaryReport_Result> detailsRep = DbMtData.usp_GetMerchantSummaryReport(dateFrom, dateTo, merchantId);
                return detailsRep;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetMerchantCardTypeReport_Result> GetMerchantCardTypeReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int merchantId)
        {
            try
            {
                IEnumerable<usp_GetMerchantCardTypeReport_Result> detailsRep = DbMtData.usp_GetMerchantCardTypeReport(dateFrom, dateTo, merchantId);
                return detailsRep;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetMerchantExceptionReport_Result> GetMerchantExceptionReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int merchantId)
        {
            try
            {
                IEnumerable<usp_GetMerchantExceptionReport_Result> getMerchantSummary = DbMtData.usp_GetMerchantExceptionReport(dateFrom, dateTo, merchantId);
                return getMerchantSummary;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetMerchantUserExceptionReport_Result> GetMerchantUserExceptionReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int? mUserId)
        {
            try
            {
                IEnumerable<usp_GetMerchantUserExceptionReport_Result> getmuExecp = DbMtData.usp_GetMerchantUserExceptionReport(dateFrom, dateTo, mUserId);
                return getmuExecp;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetMerchantTotalTransByVehicleReport_Result> GetMerchantTotalTransByVehicle(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int merchantId)
        {
            try
            {
                IEnumerable<usp_GetMerchantTotalTransByVehicleReport_Result> getMerchantTotal = DbMtData.usp_GetMerchantTotalTransByVehicleReport(dateFrom, dateTo, merchantId);
                return getMerchantTotal;
            }
            catch
            {
                throw;
            }
        }

        //On Demand Merchant Users
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantId"></param>
        /// <param name="merchantUserId"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetMerchantUserCardTypeReport_Result> GetMerchantUserCardTypeReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantId, Nullable<int> merchantUserId)
        {
            try
            {
                IEnumerable<usp_GetMerchantUserCardTypeReport_Result> getMerchantUserCrd = DbMtData.usp_GetMerchantUserCardTypeReport(dateFrom, dateTo, merchantId, merchantUserId);
                return getMerchantUserCrd;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantId"></param>
        /// <param name="merchantUserId"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetMerchantUserDetailReport_Result> GetMerchantUserDetailReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantId, Nullable<int> merchantUserId)
        {
            try
            {
                IEnumerable<usp_GetMerchantUserDetailReport_Result> getMerchantUserDetail = DbMtData.usp_GetMerchantUserDetailReport(dateFrom, dateTo, merchantId, merchantUserId);
                return getMerchantUserDetail;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantId"></param>
        /// <param name="merchantUserId"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetMerchantUserSummaryReport_Result> GetMerchantUserSummaryReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantId, Nullable<int> merchantUserId)
        {
            try
            {
                IEnumerable<usp_GetMerchantUserSummaryReport_Result> getMerchantUserSummary = DbMtData.usp_GetMerchantUserSummaryReport(dateFrom, dateTo, merchantId, merchantUserId);
                return getMerchantUserSummary;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantId"></param>
        /// <param name="merchantUserId"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetMerchantUserTotalTransByVehicleReport_Result> GetMerchantUserTotalTransByVehicleReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantId, Nullable<int> merchantUserId)
        {
            try
            {
                IEnumerable<usp_GetMerchantUserTotalTransByVehicleReport_Result> getMerchantUserttv = DbMtData.usp_GetMerchantUserTotalTransByVehicleReport(dateFrom, dateTo, merchantId, merchantUserId);
                return getMerchantUserttv;
            }
            catch
            {
                throw;
            }
        }

        //On Demand Corporate Users
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantUserId"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetCorpUserCardTypeReport_Result> GetCorpUserCardTypeReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantUserId)
        {
            try
            {
                IEnumerable<usp_GetCorpUserCardTypeReport_Result> getCorptUserCrd = DbMtData.usp_GetCorpUserCardTypeReport(dateFrom, dateTo, merchantUserId);
                return getCorptUserCrd;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantUserId"></param>
        /// <returns></returns>
        public IEnumerable<usp_CorpUserGetTotalTransByVehicleReport_Result> CorpUserGetTotalTransByVehicleReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantUserId)
        {
            try
            {
                IEnumerable<usp_CorpUserGetTotalTransByVehicleReport_Result> getCorpUserttv = DbMtData.usp_CorpUserGetTotalTransByVehicleReport(dateFrom, dateTo, merchantUserId);
                return getCorpUserttv;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetCorpUserExceptionReport_Result> GetCorporateExceptionReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int merchantId)
        {
            try
            {
                IEnumerable<usp_GetCorpUserExceptionReport_Result> getMerchantSummary = DbMtData.usp_GetCorpUserExceptionReport(dateFrom, dateTo, merchantId);
                return getMerchantSummary;
            }
            catch
            {
                throw;
            }
        }

        //On Demand Run Admin
        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRun"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public int AdminExceptionReportScheduler(bool? isRun, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo)
        {
            try
            {
                int getMerchantTotal = DbMtData.usp_AdminExceptionReportScheduler(isRun, dateFrom, dateTo);
                return getMerchantTotal;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRun"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public int TotalTransByVehicleScheduler(bool? isRun, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo)
        {
            try
            {
                int getMerchantTotal = DbMtData.usp_TotalTransByVehicleScheduler(isRun, dateFrom, dateTo);
                return getMerchantTotal;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRun"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public int CardTypeReportScheduler(bool? isRun, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo)
        {
            try
            {
                ObjectResult<int?> getMerchantTotal = DbMtData.usp_CardTypeReportScheduler(isRun, dateFrom, dateTo);
                return 1;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRun"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public int AdminSummaryReportScheduler(bool? isRun, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo)
        {
            try
            {
                ObjectResult<int?> getMerchantTotal = DbMtData.usp_AdminSummaryReportScheduler(isRun, dateFrom, dateTo);
                return -1;
            }
            catch
            {
                throw;
            }
        }

        //On Demand Run Merchant
        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRun"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public int MerchantSummaryReportScheduler(bool? isRun, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int? merchantId)
        {
            try
            {
                int getMerchantTotal = DbMtData.usp_MerchantSummaryReportScheduler(isRun, dateFrom, dateTo, merchantId);
                return getMerchantTotal;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRun"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public int MerchantTotalTransByVehicleScheduler(bool? isRun, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int? merchantId)
        {
            try
            {
                int getMerchantTotal = DbMtData.usp_MerchantTotalTransByVehicleScheduler(isRun, dateFrom, dateTo, merchantId);
                return getMerchantTotal;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRun"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public int MerchantExceptionReportScheduler(bool? isRun, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo)
        {
            try
            {
                int getMerchantTotal = DbMtData.usp_MerchantExceptionReportScheduler(isRun, dateFrom, dateTo);
                return getMerchantTotal;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRun"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public int DetailReportScheduler(bool? isRun, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int? merchantId)
        {
            try
            {
                int getMerchantTotal = DbMtData.usp_DetailReportScheduler(isRun, dateFrom, dateTo, merchantId);
                return getMerchantTotal;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRun"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public int MerchantCardTypeReportScheduler(bool? isRun, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int? merchantId)
        {
            try
            {
                int getMerchantTotal = DbMtData.usp_MerchantCardTypeReportScheduler(isRun, dateFrom, dateTo, merchantId);
                return getMerchantTotal;
            }
            catch
            {
                throw;
            }
        }

        //On Demand Run Merchant User
        /// <summary>
        /// 
        /// </summary>
        /// <param name="isOnDemandReport"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="parentMerchantId"></param>
        /// <param name="merchantUserId"></param>
        /// <returns></returns>
        public int MerchantUserCardTypeReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> parentMerchantId, Nullable<int> merchantUserId)
        {
            try
            {
                int result = DbMtData.usp_MerchantUserCardTypeReportScheduler(isOnDemandReport, fromDate, toDate, parentMerchantId, merchantUserId);
                return result;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isOnDemandReport"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="parentMerchantId"></param>
        /// <param name="merchantUserId"></param>
        /// <returns></returns>
        public int MerchantUserDetailReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> parentMerchantId, Nullable<int> merchantUserId)
        {
            int result = DbMtData.usp_MerchantUserDetailReportScheduler(isOnDemandReport, fromDate, toDate, parentMerchantId, merchantUserId);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isOnDemandReport"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="parentMerchantId"></param>
        /// <param name="merchantUserId"></param>
        /// <returns></returns>
        public int MerchantUserTotalTransByVehicleScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> parentMerchantId, Nullable<int> merchantUserId)
        {
            int result = DbMtData.usp_MerchantUserTotalTransByVehicleScheduler(isOnDemandReport, fromDate, toDate, parentMerchantId, merchantUserId);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isOnDemandReport"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="parentMerchantId"></param>
        /// <param name="merchantUserId"></param>
        /// <returns></returns>
        public int MerchantUserSummaryReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> parentMerchantId, Nullable<int> merchantUserId)
        {
            int result = DbMtData.usp_MerchantUserSummaryReportScheduler(isOnDemandReport, fromDate, toDate, parentMerchantId, merchantUserId);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isOnDemandReport"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="parentMerchantId"></param>
        /// <param name="merchantUserId"></param>
        /// <returns></returns>
        public int MerchantUserExceptionReportScheduler(Nullable<bool> isOnDemandReport, DateTime? fromDate, DateTime? toDate, int? merchantUserId)
        {
            int result = DbMtData.usp_MerchantUserExceptionReportScheduler(isOnDemandReport, fromDate, toDate, merchantUserId);
            return result;
        }

        //Corporate User RUN
        /// <summary>
        /// 
        /// </summary>
        /// <param name="isOnDemandReport"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="corpUserId"></param>
        /// <returns></returns>
        public int CorpUserTotalTransByVehicleScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> corpUserId)
        {
            int result = DbMtData.usp_CorpUserTotalTransByVehicleScheduler(isOnDemandReport, fromDate, toDate, corpUserId);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isOnDemandReport"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="corpUserId"></param>
        /// <returns></returns>
        public int CorpUserCardTypeReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> corpUserId)
        {
            ObjectResult<int?> result = DbMtData.usp_CorpUserCardTypeReportScheduler(isOnDemandReport, fromDate, toDate, corpUserId);
            return -1;
        }

        public void CnUpdate(MTDTransaction mtdTransaction, int tId)
        {
            int sourceId;
            var transData = DbMtData.MTDTransactions.Find(tId);

            transData.AddRespData = mtdTransaction.AddRespData;
            transData.ResponseCode = mtdTransaction.ResponseCode;
            transData.AuthId = mtdTransaction.AuthId;
            transData.ResponseXml = mtdTransaction.ResponseXml;
            transData.ResponseBit63 = mtdTransaction.ResponseBit63;

            //transData.SourceId = mtdTransaction.SourceId;
            if (!string.IsNullOrEmpty(mtdTransaction.SourceId))
            {
                transData.SourceId = mtdTransaction.SourceId;
            }

            if (int.TryParse(transData.SourceId, out sourceId))
            {
                var sourceTransaction = DbMtData.MTDTransactions.Find(sourceId);
                if (!object.Equals(sourceTransaction, null))
                {
                    switch (mtdTransaction.TxnType)
                    {
                        case VoidMessage:
                            sourceTransaction.IsVoided = true;
                            break;
                        case RefundMessage:
                            sourceTransaction.IsRefunded = true;
                            break;
                        case CompletionMessage:
                            sourceTransaction.IsCompleted = true;
                            break;
                    }
                }
            }

            DbMtData.CommitChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetTransactionUrl()
        {
            string activeUrl = (from m in DbMtData.ServiceDiscoveryProviders where m.IsServiceDiscovery == false && m.IsSetActive == true select m.Url).FirstOrDefault();
            return activeUrl;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isOnDemandReport"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="corpUserId"></param>
        /// <returns></returns>
        public int CorpUserExceptionReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> corpUserId)
        {
            int result = DbMtData.usp_CorpUserExceptionReportScheduler(isOnDemandReport, fromDate, toDate, corpUserId);
            return result;
        }

        public string GetRefNumber()
        {
            var transRef = (from m in DbMtData.Datawires select m.RefNumber).ToList().Max();
            return Convert.ToString(transRef);
        }

        public string GetFastestUrl(string failOverUrl, List<string> urlList)
        {
            double transTime = Convert.ToDouble((from m in DbMtData.ServiceDiscoveryProviders where m.IsServiceDiscovery == false && m.Url == failOverUrl select m.TransactionTime).FirstOrDefault());

            var data = (from n in DbMtData.ServiceDiscoveryProviders
                        where n.IsServiceDiscovery == false
                        select new { UrlData = n }).AsEnumerable().Select(obj => new ServiceProviderDbData
                                {
                                    HostId = obj.UrlData.HostId,
                                    Url = obj.UrlData.Url,
                                    IsSetActive = obj.UrlData.IsSetActive,
                                    IsServiceDiscovery = obj.UrlData.IsServiceDiscovery,
                                    TransactionTime = Convert.ToDouble(obj.UrlData.TransactionTime)
                                }).ToList();

            if (failOverUrl == null || urlList == null)
            {
                return null;
            }
            else
            {
                var activeUrlData = (from m in data where m.TransactionTime >= transTime && m.Url != failOverUrl && urlList.Contains(m.Url) orderby m.TransactionTime ascending select m).FirstOrDefault();
                if (activeUrlData != null)
                {
                    var setNewActiveUrl = (from m in DbMtData.ServiceDiscoveryProviders where m.Url == activeUrlData.Url && m.IsServiceDiscovery == false select m).FirstOrDefault();
                    setNewActiveUrl.IsSetActive = true;
                }
                
                var notActiveUrlData = (from m in DbMtData.ServiceDiscoveryProviders where m.IsServiceDiscovery == false && m.Url == failOverUrl select m).FirstOrDefault();
                notActiveUrlData.IsSetActive = false;
                DbMtData.CommitChanges();

                if (activeUrlData != null)
                    return activeUrlData.Url;
                else
                    return null;
            }
        }

        public bool IsClsCapKey(int terminalId)
        {
            bool? isCls = (from m in DbMtData.Terminals
                           where m.TerminalID == terminalId
                           select m.IsContactLess).FirstOrDefault();

            return (isCls == null) ? false : true;
        }

        /// <summary>
        /// Get transaction details based on stan no.
        /// </summary>
        /// <param name="mid"></param>
        /// <param name="tid"></param>
        /// <param name="stan"></param>
        /// <returns></returns>
        public MTDTransaction GetTransaction(int? mid, string tid, string stan)
        {
            try
            {
                var transaction = DbMtData.MTDTransactions.Where(t => t.Stan == stan && t.MerchantId == mid && t.TerminalId == tid).OrderByDescending(t => t.Id).FirstOrDefault();
                return transaction;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get transaction id based on request id
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public string GetTransactionId(string requestId)
        {
            string transId = string.Empty;
            try
            {
                var trans = DbMtData.MTDTransactions.Where(t => t.RequestId == requestId).FirstOrDefault();
                if (trans != null)
                    transId = trans.Id.ToString();

                return transId;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// To get transaction to decide mac verification failed or not
        /// </summary>
        /// <param name="transId"></param>
        /// <returns></returns>
        public bool IsMacVerificationFailed(string transId)
        {
            try
            {
                var trans = DbMtData.MTDTransactions.Where(t => t.SourceId == transId).OrderByDescending(t => t.Id).FirstOrDefault();

                if (trans != null && trans.ResponseCode == "00")
                    return true;
                else
                    return false;
            }
            catch
            {
                throw;
            }
        }

    }

}


