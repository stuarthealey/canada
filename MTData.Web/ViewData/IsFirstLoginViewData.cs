﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.Web.ViewData
{
    public class IsFirstLoginViewData
    {
        public bool? IsFirstLogin { get; set; }
        public string UserTypeId { get; set; }
        public int UserId { get; set; }
    }
}