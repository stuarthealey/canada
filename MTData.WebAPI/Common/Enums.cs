﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Common
{
    public enum EmailField
    {
        CompanyName,
        CompanyPhone,
        CompanyAddress,
        CompanyTaxNumber,
        Driver,
        DriverTaxNumber,
        Extra,
        Fare,
        FederalTaxAmount,
        FederalTaxPercentage,
        Flagfall,
        Fleet,
        PickAddress,
        StateTaxAmount,
        StateTaxPercentage,
        Subtotal,
        SurchargeAmount,
        SurchargePercentage,
        TermsandConditions,
        Tip,
        Tolls,
        Total,
        Vehicle,
        Disclaimer,
        DestinationAddress,
        CreditCardNumber,
        PlateNumber
    };

    public enum PaymentCardType
    {
        Credit,
        Debit
    };

    public enum PaymentCard
    {
        Amex,
        Diners,
        Discover,
        JCB,
        MasterCard,
        Visa
    };

    public enum PaymentTransactionType
    {
        Authorization,
        Completion,
        Sale,
        Refund,
        Void,
        FileDownload,
        Timeout
    };

    public enum PaymentIndustryType
    {
        Ecommerce,
        Retail,
        TokenOnly,
        TAKey,
        CAPKUpdate,
        CAPKDownload,
        BookerAppPayment
    };

    public enum PaymentEntryMode
    {
        Keyed,
        Swiped,
        FSwiped,
        EmvContact,
        EmvContactless,
        MSRContactless
    };

    public enum RequestXMLType
    {
        LoginRequest,
        DatawireRequest,
        MailRequest,
        PaymentRequest
    }

    public enum EmvTagType
    {
        AuthorizationRequestEMVTags = 1
    }

    public enum UserTypes
    {
        Admin = 1,
        Merchant = 2,
        MerchantAdmin = 3,
        MerchantUser = 4
    }

    public enum PayType
    {
        PayDriver=1,
        PayOwner=2,
        PayNetwork=3
    };

    public enum FeeType
    {
        Fixed = 1,
        Percentage = 2,
        Both = 3
    };

    public enum DataWireRequestType
    {
        Registration = 1,
        Activation = 2,
        Ping = 3,
        SimpleTransaction = 4,
        InitiateSession = 5,
        SessionTransaction = 6,
        TerminateSession = 7,
        None = 0
    }

    public enum SegmentType
    {
        StandardDataSegment = 100,
        ProductDataSegment = 102,
        EbtDataSegment = 103,
        FleetDataSegment = 101,
        VariableInformationDataSegment = 111,
        TotalsDataSegment = 990,
        None = 0
    }

    public enum MessageTypeDW
    {
        Pointer,
        TotalFieldSeparator,
        SegmentLength,
        SegmentType,
        StartFunction,
        EndFunction,
        CategoryHeader,
        CardLabel,
        CardTypeTotalCount,
        CardTypeTotalAmount,
        ApprovedTotalsResponseHeader,
        Hash,
        Space,
        Equal,
        DeclinedTotalsResponseHeader,
        DataSection1Header,
        MessageFormatVersion,
        Noofsegment,
        ATLMessage,
        ResponseHeader,
        AdditionalSegmentHeader,
        AdditionalInformationIndicator,
        AdditionalInformationLength,
        AdditionalInformation,
        DataWireIdNotFound,
        UserAgent,
        ServiceCost,
        TransactionTimeMs,
        PayloadMessage,
        Retry,
        TransactionCancel,
        FleetCardType,
        Status,
        SessionContext,
        ReturnCode,
        URL,
        RespClientId,
        Did,
        ServiceProvider,
        MaxTransactionsInPackage,
        ServiceIdText,
        PayloadText,
        NotFound,
        NoTicketProvisioned,
        ok,
        ApplicationId,
        ServiceId,
        ClientRefId,
        AccessDenied,
        MerchantRegistered,
        RegistrationCancel,
        RegistrationSuccess,
        RegistrationFailed,
        AuthenticationError,
        StatusSuccess,
        StatusOk,
        UnreachableUrlResponse,
        EbtMessageHeader,
        EbtMessageFooter,
        FleetMessageHeader,
        FleetMessageFooter,
        ProductCodeMessageHeader,
        ProductCodeMessageFooter,
        ServiceLevel,
        NoofProducts,
        ProductCode,
        UnitofMeasure,
        Quantity,
        ProductDataFieldDelimiter,
        ProductDataFieldDelimiterValue,
        UnitPrice,
        ProductAmount,
        ExceptMaxProductsOverLimit,
        ExceptSimilarProduct,
        StandardDataMessageHeader,
        StandardDataMessageFooter,
        TotalsDataMessageHeader,
        TotalsDataMessageFooter,
        VariableInformationMessageHeader,
        VariableInformationMessageFooter,
        VarianleIndicator,
        VariableLength,
        VariableInformation,
        Exception,
        InvalidBuypassId,
        StatusCodeAuthenticationError,
        StatusCodeUnknownServiceid,
        StatusCodeWrongSessionContext,
        StatusCodeAccessDenied,
        StatusCodeFailed,
        StatusCodeRetry,
        StatusCodeTimeOut,
        StatusCodeXmlError,
        StatusCodeOtherError,
        StatusCode008,
        StatusCode200,
        StatusCode201,
        StatusCode202,
        StatusCode203,
        StatusCode204,
        StatusCode205,
        StatusCode206,
        StatusCode405,
        StatusCode505,
        LabelInfo,
        LabelDebug,
        LabelError,
        TimeStamp,
        ReceiptMessage,
        ReplaceResponse,
        NoResponse,
        PinUse,
        RequestInValid,
        notfound,
        authenticationerror,
        retry,
        accessdenied
    }

}
