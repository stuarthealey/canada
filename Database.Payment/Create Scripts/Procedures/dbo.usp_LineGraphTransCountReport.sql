IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LineGraphTransCountReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LineGraphTransCountReport]
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <05-07-2016>
-- Description:	<To get the data for line graph trans count volume report>
-- =============================================
CREATE PROCEDURE [dbo].[usp_LineGraphTransCountReport]
(
	@UserId INT
)
AS
BEGIN TRY
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	DECLARE @DateFromThisWeek DATETIME,
			@DateToThisWeek DATETIME,
			@DateFromWeek1 DATETIME,
			@DateToWeek1 DATETIME,
			@DateFromWeek2 DATETIME,
			@DateToWeek2 DATETIME,
			@DateFromWeek3 DATETIME,
			@DateToWeek3 DATETIME,
			@DateFromWeek4 DATETIME,
			@DateToWeek4 DATETIME,
			@DateFromWeek5 DATETIME,
			@DateToWeek5 DATETIME,
			@DateFromWeek6 DATETIME,
			@DateToWeek6 DATETIME,
			@DateFromWeek7 DATETIME,
			@DateToWeek7 DATETIME,
			@DateFromWeek8 DATETIME,
			@DateToWeek8 DATETIME,
			@DateFromWeek9 DATETIME,
			@DateToWeek9 DATETIME,
			@DateFromWeek10 DATETIME,
			@DateToWeek10 DATETIME,
			@DateFromWeek11 DATETIME,
			@DateToWeek11 DATETIME,
			@DateFromWeek12 DATETIME,
			@DateToWeek12 DATETIME,
			@UserType INT,
			@MerchantId INT

  SELECT  @DateFromThisWeek = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,0,GETDATE())), 0) - 1,
          @DateToThisWeek = GETDATE(),
          @DateFromWeek1 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK, - 1,GETDATE())), 0) - 1,
          @DateToWeek1 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK, - 1,GETDATE())), 7) - 1,
          @DateFromWeek2 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-2,GETDATE())), 0) - 1,
          @DateToWeek2 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-2,GETDATE())), 7) - 1,
          @DateFromWeek3 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-3,GETDATE())), 0) - 1,
          @DateToWeek3 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-3,GETDATE())), 7) - 1,
          @DateFromWeek4 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-4,GETDATE())), 0) - 1,
          @DateToWeek4 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-4,GETDATE())), 7) - 1,
          @DateFromWeek5 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-5,GETDATE())), 0) - 1,
          @DateToWeek5 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-5,GETDATE())), 7) - 1,
		  @DateFromWeek6 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-6,GETDATE())), 0) - 1,
          @DateToWeek6 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-6,GETDATE())), 7) - 1,
		  @DateFromWeek7 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-7,GETDATE())), 0) - 1,
          @DateToWeek7 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-7,GETDATE())), 7) - 1,
		  @DateFromWeek8 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-8,GETDATE())), 0) - 1,
          @DateToWeek8 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-8 ,GETDATE())), 7) - 1,
		  @DateFromWeek9 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-9,GETDATE())), 0) - 1,
          @DateToWeek9 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-9,GETDATE())), 7) - 1,
		  @DateFromWeek10 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-10,GETDATE())), 0) - 1,
          @DateToWeek10 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-10,GETDATE())), 7) - 1,
		  @DateFromWeek11 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-11,GETDATE())), 0) - 1,
          @DateToWeek11 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-11,GETDATE())), 7) - 1,
		  @DateFromWeek12 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-12,GETDATE())), 0) - 1,
          @DateToWeek12 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-12,GETDATE())), 7)-1

	DECLARE @DataTable TABLE (
		Id INT,
		FleetId INT,
		Txndate DATETIME
		)

	DECLARE @FleetList TABLE (FleetListId INT)
	SELECT @UserType = fk_UserTypeId, @MerchantId = fk_MerchantId FROM dbo.Users WHERE UserId = @UserId

	IF (@UserType = 2)
	BEGIN
		INSERT INTO @FleetList
		SELECT FleetID FROM dbo.Fleet WHERE IsActive = 1 AND fk_MerchantId = @MerchantId
	END
	ELSE
	BEGIN
		INSERT INTO @FleetList
		SELECT DISTINCT fk_FleetID FROM dbo.UserFleet WHERE fk_UserID = @UserId AND IsActive = 1
	END

	INSERT INTO @DataTable
	SELECT Id,fk_FleetId,TxnDate FROM MTDTransaction WHERE ResponseCode IN ('00', '000', '002') AND fk_FleetId IN (SELECT FleetListId FROM @FleetList) AND TxnType IN ('Sale', 'Completion') AND IsRefunded = 0 AND IsVoided = 0

	SELECT  ROW_NUMBER() OVER (ORDER BY f.FleetName) AS 'Sr.No',
			COALESCE(f.FleetName,'')  AS 'FleetName',
			(SELECT COALESCE(COUNT(Id),0) FROM @DataTable dr WHERE dr.FleetId = f.FleetID AND dr.Txndate >= @DateFromThisWeek AND dr.Txndate < @DateToThisWeek) AS 'TransCountThisWeek',
			(SELECT COALESCE(COUNT(Id),0) FROM @DataTable dr WHERE dr.FleetId = f.FleetID AND dr.Txndate >= @DateFromWeek1 AND dr.Txndate < @DateToWeek1) AS 'TransCountOneWeekAgo',
			(SELECT COALESCE(COUNT(Id),0) FROM @DataTable dr WHERE dr.FleetId = f.FleetID AND dr.Txndate >= @DateFromWeek2 AND dr.Txndate < @DateToWeek2) AS 'TransCountTwoWeeksAgo',
			(SELECT COALESCE(COUNT(Id),0) FROM @DataTable dr WHERE dr.FleetId = f.FleetID AND dr.Txndate >= @DateFromWeek3 AND dr.Txndate < @DateToWeek3) AS 'TransCountThreeWeeksAgo',
			(SELECT COALESCE(COUNT(Id),0) FROM @DataTable dr WHERE dr.FleetId = f.FleetID AND dr.Txndate >= @DateFromWeek4 AND dr.Txndate < @DateToWeek4) AS 'TransCountFourWeeksAgo',
			(SELECT COALESCE(COUNT(Id),0) FROM @DataTable dr WHERE dr.FleetId = f.FleetID AND dr.Txndate >= @DateFromWeek5 AND dr.Txndate < @DateToWeek5) AS 'TransCountFiveWeeksAgo',
			(SELECT COALESCE(COUNT(Id),0) FROM @DataTable dr WHERE dr.FleetId = f.FleetID AND dr.Txndate >= @DateFromWeek6 AND dr.Txndate < @DateToWeek6) AS 'TransCountSixWeeksAgo',
			(SELECT COALESCE(COUNT(Id),0) FROM @DataTable dr WHERE dr.FleetId = f.FleetID AND dr.Txndate >= @DateFromWeek7 AND dr.Txndate < @DateToWeek7) AS 'TransCountSevenWeeksAgo',
			(SELECT COALESCE(COUNT(Id),0) FROM @DataTable dr WHERE dr.FleetId = f.FleetID AND dr.Txndate >= @DateFromWeek8 AND dr.Txndate < @DateToWeek8) AS 'TransCountEightWeeksAgo',
			(SELECT COALESCE(COUNT(Id),0) FROM @DataTable dr WHERE dr.FleetId = f.FleetID AND dr.Txndate >= @DateFromWeek9 AND dr.Txndate < @DateToWeek9) AS 'TransCountNineWeeksAgo',
			(SELECT COALESCE(COUNT(Id),0) FROM @DataTable dr WHERE dr.FleetId = f.FleetID AND dr.Txndate >= @DateFromWeek10 AND dr.Txndate < @DateToWeek10) AS 'TransCountTenWeeksAgo',
			(SELECT COALESCE(COUNT(Id),0) FROM @DataTable dr WHERE dr.FleetId = f.FleetID AND dr.Txndate >= @DateFromWeek11 AND dr.Txndate < @DateToWeek11) AS 'TransCountElevenWeeksAgo',
			(SELECT COALESCE(COUNT(Id),0) FROM @DataTable dr WHERE dr.FleetId = f.FleetID AND dr.Txndate >= @DateFromWeek12 AND dr.Txndate < @DateToWeek12) AS 'TransCountTwelveWeeksAgo'
    FROM dbo.MTDTransaction m 
		INNER JOIN dbo.Fleet f ON m.fk_FleetId = f.FleetID
	WHERE m.fk_FleetId IN (SELECT FleetListId FROM @FleetList)
	  AND m.ResponseCode IN ('00', '000', '002') AND m.TxnType IN ('Sale', 'Completion') AND m.IsRefunded = 0 AND m.IsVoided = 0
	  AND TxnDate >= @DateFromWeek12 
	  AND TxnDate < @DateToThisWeek
    GROUP BY f.FleetName, f.FleetID
	ORDER BY f.FleetName		
END TRY
--use catch to log error with details in ProcErrorHandler table
BEGIN CATCH
      INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
      SELECT ERROR_NUMBER()
	        ,ERROR_SEVERITY()
			,ERROR_STATE()
			,ERROR_PROCEDURE()
			,ERROR_LINE()
			,ERROR_MESSAGE()
			,SUSER_SNAME()
			,HOST_NAME()
			,GETDATE()
END CATCH
GO