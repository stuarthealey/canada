﻿using System;
using System.Collections.Generic;
using System.Linq;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Data.Repository
{
    public class BookingFeeRepository : BaseRepository, IBookingFeeRepository
    { 
        public BookingFeeRepository(IMtDataDevlopmentsEntities dbMtData)
            : base(dbMtData)
        {

        }

        /// <summary>
        /// Purpose             :   To add booking fee
        /// Function Name       :   Add
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   12/23/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fee"></param>
        public void Add(BookingFee fee)
        {
            try
            {
            fee.IsActive = true;
            fee.CreatedDate = DateTime.Now;
            DbMtData.BookingFees.Add(fee);
            DbMtData.CommitChanges();
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get fee
        /// Function Name       :   GetFee
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   12/23/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="feeId"></param>
        /// <returns></returns>
        public BookingFee GetFee(int feeId)
        {
            try
            {
            BookingFee bookingFee = DbMtData.BookingFees.Find(feeId);
            string gatewayName = (from m in DbMtData.Gateways where m.GatewayID == bookingFee.fk_GatewayID select m.Name).FirstOrDefault();
            bookingFee.Name = gatewayName;
            return bookingFee;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To update booking fee
        /// Function Name       :   Update
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   12/23/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fee"></param>
        /// <param name="feeId"></param>
        public void Update(BookingFee fee, int feeId)
        {
            try
            {
                BookingFee feeDb = DbMtData.BookingFees.Find(feeId);
                feeDb.BookingFeeFixed = fee.BookingFeeFixed;
                feeDb.BookingFeeType = fee.BookingFeeType;
                feeDb.BookingFeeMaxCap = fee.BookingFeeMaxCap;
                feeDb.BookingFeePer = fee.BookingFeePer;
                feeDb.fk_GatewayID = fee.fk_GatewayID;
                feeDb.fk_MerchantID = fee.fk_MerchantID;
                feeDb.ModifiedDate = DateTime.Now;
                feeDb.ModifiedBy = fee.ModifiedBy;
            }
            catch
            {
                throw;
            }

            DbMtData.CommitChanges();
        }

        /// <summary>
        /// Purpose             :   To delete fee
        /// Function Name       :   DeleteFee
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   12/24/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="feeId"></param>
        public void DeleteFee(int feeId)
        {
            BookingFee bookingFeeDb = DbMtData.BookingFees.Find(feeId);
            bookingFeeDb.IsActive = false;
            DbMtData.CommitChanges();
        }

        /// <summary>
        /// Purpose             :   To get list of fee
        /// Function Name       :   FeeList
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   12/24/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<BookingFee> FeeList()
        {
            IEnumerable<BookingFee> feeList = (from m in DbMtData.BookingFees
                                               join n in DbMtData.Merchants on m.fk_MerchantID equals n.MerchantID
                                               join k in DbMtData.Gateways on m.fk_GatewayID equals k.GatewayID
                                               where m.IsActive == true
                                               select new { m.BookingFeePer, m.BookingFeeFixed, m.BookingFeeID, m.BookingFeeType, m.BookingFeeMaxCap, n.Company, m.fk_MerchantID, k.Name })
                .AsEnumerable() .Select( objNew =>  new BookingFee
                        {
                            BookingFeeFixed = objNew.BookingFeeFixed,
                            BookingFeePer = objNew.BookingFeePer,
                            BookingFeeID = objNew.BookingFeeID,
                            BookingFeeMaxCap = objNew.BookingFeeMaxCap,
                            BookingFeeType = objNew.BookingFeeType,
                            Company = objNew.Company,
                            fk_MerchantID = objNew.fk_MerchantID,
                            Name = objNew.Name
                        })
                .ToList();

            foreach (BookingFee fee in feeList)
            {
                if (fee.BookingFeeType == 0)
                {
                    fee.Type = "Fixed";
                }
                else if (fee.BookingFeeType == 1)
                {
                    fee.Type = "Percentage";
                }
                else if (fee.BookingFeeType == 2)
                {
                    fee.Type = "Both (Fixed and Percentage)";
                }
            }
            return feeList;
        }

        /// <summary>
        /// Purpose             :   To display list of fee
        /// Function Name       :   FeeListByFiter
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   12/24/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <param name="sorting"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<BookingFee> FeeListByFiter(FilterSearchParameters fsParametes)
        {
            IEnumerable<BookingFee> feeList = (from m in DbMtData.BookingFees
                                               join n in DbMtData.Merchants on m.fk_MerchantID equals n.MerchantID
                                               join k in DbMtData.Gateways on m.fk_GatewayID equals k.GatewayID
                                               where m.IsActive == true
                                               select new { m.BookingFeePer, m.BookingFeeFixed, m.BookingFeeID, m.BookingFeeType, m.BookingFeeMaxCap, n.Company, m.fk_MerchantID, k.Name })
                .AsEnumerable()
                .Select(
                    objNew =>
                        new BookingFee
                        {
                            BookingFeeFixed = objNew.BookingFeeFixed,
                            BookingFeePer = objNew.BookingFeePer,
                            BookingFeeID = objNew.BookingFeeID,
                            BookingFeeMaxCap = objNew.BookingFeeMaxCap,
                            BookingFeeType = objNew.BookingFeeType,
                            Company = objNew.Company,
                            fk_MerchantID = objNew.fk_MerchantID,
                            Name = objNew.Name
                        })
                .ToList().Where(x => x.fk_MerchantID ==fsParametes.MerchantId);
            foreach (BookingFee fee in feeList)
            {
                fee.Type = fee.BookingFeeType == 0 ? "Fixed" : "Percentage";
            }
            if (!string.IsNullOrEmpty(fsParametes.Name))
            {
                feeList = feeList.Where(p => p.Name.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0);
            }

            if (!string.IsNullOrEmpty(fsParametes.Sorting))
            {
                switch (fsParametes.Sorting)
                {
                    case "Company ASC":
                        feeList = feeList.OrderBy(p => p.Company);
                        break;
                    case "Company DESC":
                        feeList = feeList.OrderByDescending(p => p.Company);
                        break;
                    default:
                        feeList = feeList.OrderBy(p => p.Company);
                        break;
                }
            }
            return fsParametes.PageSize > 0
                       ? feeList.Skip(fsParametes.StartIndex).Take(fsParametes.PageSize).ToList() //Paging
                       : feeList.ToList();
        }

    }
}