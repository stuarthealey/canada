IF OBJECT_ID('dbo.ServiceDiscoveryProvider', 'U') IS NULL
BEGIN
	CREATE TABLE dbo.ServiceDiscoveryProvider (
		HostId INT IDENTITY(1,1) NOT NULL,
		[Url] NVARCHAR(200) NULL,
		IsSetActive BIT NOT NULL,
		IsServiceDiscovery BIT NOT NULL,
		TransactionTime NVARCHAR(40) NULL,
	 CONSTRAINT [PK_ServiceDiscoveryProvider] PRIMARY KEY CLUSTERED 
	(
		HostID ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	PRINT 'Table ServiceDiscoveryProvider created.'
END
ELSE
BEGIN
	PRINT 'Table ServiceDiscoveryProvider already exists.'
END
