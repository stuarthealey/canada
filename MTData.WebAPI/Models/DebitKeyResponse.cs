﻿using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class DebitKeyResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string Message { get; set; }

        private string _macData;
        [DataMember(EmitDefaultValue = false)]
        public string MacData
        {
            get { return string.IsNullOrEmpty(this._macData) ? string.Empty : this._macData; }
            set { this._macData = value; }
        }

        private string _checkDigit;
        [DataMember(EmitDefaultValue = false)]
        public string CheckDigit
        {
            get { return string.IsNullOrEmpty(this._checkDigit) ? string.Empty : this._checkDigit; }
            set { this._checkDigit = value; }
        }

        private string _tMac;
        [DataMember(EmitDefaultValue = false)]
        public string TMAC
        {
            get { return string.IsNullOrEmpty(this._tMac) ? string.Empty : this._tMac; }
            set { this._tMac = value; }
        }

        private string _tKpe;
        [DataMember(EmitDefaultValue = false)]
        public string TKPE
        {
            get { return string.IsNullOrEmpty(this._tKpe) ? string.Empty : this._tKpe; }
            set { this._tKpe = value; }
        }

        private string _tKme;
        [DataMember(EmitDefaultValue = false)]
        public string TKME
        {
            get { return string.IsNullOrEmpty(this._tKme) ? string.Empty : this._tKme; }
            set { this._tKme = value; }
        }
               
        [DataMember(EmitDefaultValue = false)]
        public string RCTerminal { get; set; }
    }
}