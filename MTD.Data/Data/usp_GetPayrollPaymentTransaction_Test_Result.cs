//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MTD.Data.Data
{
    using System;
    
    public partial class usp_GetPayrollPaymentTransaction_Test_Result
    {
        public Nullable<System.DateTime> Date { get; set; }
        public string NAME { get; set; }
        public string BankName { get; set; }
        public string BSB { get; set; }
        public string AccountNumber { get; set; }
        public Nullable<decimal> AmountPayable { get; set; }
    }
}
