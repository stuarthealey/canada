﻿using System;

namespace MTD.Core.DataTransferObjects
{
  public   class DefaultTaxDto
    {
        public int DefaultID { get; set; }
        public Nullable<decimal> StateTaxRate { get; set; }
        public Nullable<decimal> FederalStateRate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
    }
}
