﻿using MTD.Data.Data;
using System.Collections.Generic;

namespace MTD.Data.Repository.Interface
{
   public interface IReceiptRepository
    {
       IEnumerable<ReceiptFormat> ReceiptList(int merchantId, string name, int logOnByUserId, int logOnByUserType);
       
       IEnumerable<fleetReceiptTemp> GetReceiptFleet(int fleetId);
       
       IEnumerable<Fleet> GetUserFleet(int merchantId);      
       
       void DeleteReceipt(int receiptId);

       void DeleteReceipt(int receiptId, int merchantId); 

       IEnumerable<ReceiptList> ReceiptByfleetId(int fleetId);
       
       IEnumerable<ReceiptList> ReceiptToShowByfleetId(int fleetId);
       
       bool Update(ReceiptFormat fleetDto, int fleetId);
       
       void DeleteReceiptField(int fieldId);
       
       IEnumerable<ReceiptMaster> GetReceiptFields();
       
       void AddReciept(ReceiptFormat reciept);
       
       void AddRecieptField(string field, ReceiptMaster receiptMaster);
       
       bool IsExistFleet(int fleetId);
       
       bool UpdateReceiptMaster(ReceiptMaster receiptMaster);
       
       bool IsExistFleetForUpdate(int fleetId, int receiptId);
       
       bool IsFieldExist(int fieldId);

       IEnumerable<ReceiptFormat> ReceiptListByFilter(FilterSearchParameters searchParameters, int logOnByUserId, int logOnByUserType);
       
       IEnumerable<Fleet> GetFleetList(int merchantId);
    
   }
}
