﻿using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class ResponseHeader
    {
        [DataMember]
        public string Status { get; set; }

        private string _msg = string.Empty;
        [DataMember]
        public string Message
        {
            get { return string.IsNullOrEmpty(_msg) ? string.Empty : _msg; }
            set { _msg = value; }
        }
    }
}