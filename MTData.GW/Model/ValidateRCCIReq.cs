﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MTData.WCFGW.Model
{
	[DataContract(Namespace = "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService")]
	public class ValidateRCCIReq
	{
		[DataMember(Order = 0)]
		public GPSLocation GPSLocation { get; set; }

		[DataMember(Order = 1)]
		public ValueNode CustomerId { get; set; }

		[DataMember(Order = 2)]
		public ValueNode RCCI { get; set; }
	}
}