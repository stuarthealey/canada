﻿using System;

namespace MTD.Core.DataTransferObjects
{
   public  class MerchantGatewayDto
    {
        public int MerchantGatewayID { get; set; }
        public Nullable<int> fk_MerchantID { get; set; }
        public int fk_GatewayID { get; set; }
        public string UserName { get; set; }
        public string PCode { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDefault { get; set; }
        public Nullable<bool> IsMtdataGateway { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
    }
}
