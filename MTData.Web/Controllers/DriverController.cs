﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using System.Data;
using System.Text.RegularExpressions;
using System.Web;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.Web.Resource;
using MTData.Web.ViewData;

namespace MTData.Web.Controllers
{
    public class DriverController : Controller
    {
        readonly ILogger _logger = new Logger();
        private readonly IDriverService _driverService;
        private readonly ICommonService _commonService;
        private StringBuilder _logMessage;
        private readonly IUserService _userService;
        private readonly IScheduleService _scheduleService;
        private readonly IFleetService _fleetService;
        private StringBuilder _logTracking;
        public const int SALT_BYTE_SIZE = 24;
        public const int HASH_BYTE_SIZE = 24;
        public const int PBKDF2_ITERATIONS = 40000;
        public const int ITERATION_INDEX = 0;
        public const int SALT_INDEX = 1;
        public const int PBKDF2_INDEX = 2;
        public string pinHash = string.Empty;

        public DriverController([Named("ScheduleService")] IScheduleService scheduleService, 
                                [Named("UserService")] IUserService userService, 
                                [Named("DriverService")] IDriverService driverService, 
                                [Named("CommonService")] ICommonService commonService, 
                                [Named("FleetService")] IFleetService fleetService, 
                                StringBuilder logMessage, StringBuilder logTracking)
        {
            _commonService = commonService;
            _driverService = driverService;
            _logMessage = logMessage;
            _userService = userService;
            _scheduleService = scheduleService;
            _logTracking = logTracking;
            _fleetService = fleetService;
        }

        /// <summary>
        /// Purpose             :   To display the list of drivers
        /// Function Name       :   Index
        /// Created By          :   Umesh Kumar
        /// Created On          :   07/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");
            return View();
        }

        /// <summary>
        /// Purpose             :   To export list in excel file
        /// Function Name       :   Index
        /// Created By          :   Umesh Kumar
        /// Created On          :   01/28/2016
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fName"></param>
        /// <param name="driverNo"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(string fName, string driverNo)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetDeriverListByFilter method of _driverService class to get driver List");

                int listCount;
                var dto = (SessionDto)Session["User"];
                int merchantId = Convert.ToInt32(dto.SessionUser.fk_MerchantID);
                int logOnByUserId = Convert.ToInt32(dto.SessionUser.LogOnByUserId);
                int logOnByUserType = Convert.ToInt32(dto.SessionUser.LogOnByUserType);
                int userId = dto.SessionUser.UserID;
                int userType = dto.SessionUser.fk_UserTypeID;
                var userfleet = _userService.GetUsersFleet(userId);//2
                FilterSearchParametersDto filterSearch = new FilterSearchParametersDto();

                filterSearch.Name = fName;
                filterSearch.DriverNo = driverNo;
                filterSearch.StartIndex = 0;
                filterSearch.PageSize = 0;
                filterSearch.Sorting = string.Empty;
                filterSearch.MerchantId = merchantId;
                filterSearch.UserId = userId;
                filterSearch.UserType = userType;
                filterSearch.userFleet = userfleet;
                filterSearch.LogOnByUserType = logOnByUserType;
                filterSearch.LogOnByUserId = logOnByUserId;
                listCount = _driverService.GetDriverDtoList(filterSearch).Count();

                if (listCount > 1)
                {
                    filterSearch.PageSize = listCount;
                    var viewDataList = _driverService.GetDeriverListByFilter(filterSearch);//1

                    using (StringWriter output = new StringWriter())
                    {
                        output.WriteLine("\"Name\",\"Phone\",\"Email\",\"FleetName\",\"DriverNo\",\"DriverTaxNo\",\" Address\"");

                        foreach (DriverDto item in viewDataList)
                        {
                            output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\"", item.FName, item.Phone, item.Email, item.FleetName, item.DriverNo, item.DriverTaxNo, item.Address);
                        }

                        _logTracking.Append("Transaction Report Excel file Downloaded by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                        _logger.LogInfoMessage(_logTracking.ToString());

                        return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Driver.csv");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   To create the driver
        /// Function Name       :   Create
        /// Created By          :   Umesh Kumar
        /// Created On          :   08/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            if (((SessionDto)Session["User"]) != null)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                    return RedirectToAction("index", "Member");

                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("using object intilizer to populate CountryList, StateList, CityList and FleetList");

                var dto = (SessionDto)Session["User"];
                int userId = dto.SessionUser.UserID;
                int merchantId = Convert.ToInt32(dto.SessionUser.fk_MerchantID);
                int userType = dto.SessionUser.fk_UserTypeID;
                int logOnByUserId = Convert.ToInt32(dto.SessionUser.LogOnByUserId);
                int logOnByUserType = Convert.ToInt32(dto.SessionUser.LogOnByUserType);
                var viewData = new DriverViewData();
                
                viewData.CountryList = CountryList();
                viewData.Fk_Country = "AU";

                if (userType == 2 || userType == 3)
                    viewData.FleetList = FleetList();

                if (userType == 4)
                    viewData.FleetList = _userService.UserFleetList(userId).ToViewDataList();

                if (logOnByUserType == 5)
                {
                    List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                    List<FleetListViewData> fleetViewResult = viewData.FleetList.Where(m => fleetListAccessed.Contains((int)m.FleetID)).Select(m => m).Distinct().ToList();
                    viewData.FleetList = fleetViewResult.AsEnumerable();
                }

                _logMessage.Append("returing to view create");
                viewData.DriverNo = null;
                viewData.PIN = null;

                return View(viewData);
            }

            return RedirectToAction("Index", "Member");
        }

        /// <summary>
        /// Purpose             :   To create the driver
        /// Function Name       :   Create
        /// Created By          :   Umesh Kumar
        /// Created On          :   08/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(DriverViewData driverViewData, HttpPostedFileBase uploadFile)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("Index", "Member");
            try
            {
                int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                int logOnByUserId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserId));
                int logOnByUserType = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserType));
                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                
                Regex regexNum = new Regex(@"^[0-9]+$");

                if (string.IsNullOrEmpty(driverViewData.PIN))
                    ModelState.AddModelError("PIN", MtdataResource.DriverController_Create_Please_enter_pin);

                if (!string.IsNullOrEmpty(driverViewData.PIN))
                {
                    if (driverViewData.PIN.Length < 4)
                        ModelState.AddModelError("PIN", MtdataResource.PIN_Min_Length);
                }

                if (!string.IsNullOrEmpty(driverViewData.PIN))
                {
                    if (driverViewData.PIN.Length > 10)
                        ModelState.AddModelError("PIN", MtdataResource.PIN_Max_Length);
                }

                if (!string.IsNullOrEmpty(driverViewData.PIN))
                {
                    Match matchNum = regexNum.Match(driverViewData.PIN);
                    if (!matchNum.Success)
                        ModelState.AddModelError("PIN", MtdataResource.InvalidPIN);
                }

                if (string.IsNullOrEmpty(driverViewData.ConfirmPIN))
                    ModelState.AddModelError("ConfirmPIN", MtdataResource.DriverController_Create_Please_enter_confirm_pin);

                driverViewData.CountryList = CountryList();
                driverViewData.FleetList = FleetList();
                
                if (userType == 4)
                    driverViewData.FleetList = _userService.UserFleetList(userId).ToViewDataList();
                
                if (logOnByUserType == 5)
                {
                    List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                    List<FleetListViewData> fleetViewResult = driverViewData.FleetList.Where(m => fleetListAccessed.Contains((int)m.FleetID)).Select(m => m).Distinct().ToList();
                    driverViewData.FleetList = fleetViewResult.AsEnumerable();
                }
                
                if (uploadFile != null)
                {
                    if (!IsValidExcel(uploadFile))
                    {
                        driverViewData.IsDriverError = true;
                        ViewBag.UploadMessage = MtdataResource.invalid_Excel_Fromat;
                        List<DriverViewData> db = new List<DriverViewData>();
                        driverViewData.driverListViewData = db;
                        ModelState.Clear();
                        return View(driverViewData);
                    }
                }

                if (uploadFile != null)
                {
                    _logMessage.Append("Driver scv file " + uploadFile.FileName + " was uploaded by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                    _logMessage.Append("Try to get device list from Excel file.");
                    
                    List<DriverViewData> lstDriverViewData = UploadExcelFile(uploadFile);
                    
                    _logMessage.Append("Get Device list from Excel file successfully.");
                    _logMessage.Append("Calling Add method of service _terminalService takes terminalDto parameters list.");
                    
                    if (lstDriverViewData != null && lstDriverViewData.Count() > 0)
                    {
                        List<DriverViewData> objDriverExcelUploadViewData = _driverService.AddDriverList(lstDriverViewData.ToDriverDtoList(), merchantId, userId, userType).ToDriverViewDataList();
                        _logMessage.Append("Get list of unauthorize device from uploaded excel file.");

                        driverViewData.IsDriverError = true;
                        driverViewData.driverListViewData = objDriverExcelUploadViewData;
                        
                        if (lstDriverViewData != null && objDriverExcelUploadViewData != null)
                        {
                            int uplodedDriver = lstDriverViewData.Count() - objDriverExcelUploadViewData.Count();

                            if (uplodedDriver > 0)
                                ViewBag.UploadMessage = uplodedDriver + " driver has been created from selected file.";
                            else
                                ViewBag.UploadMessage = MtdataResource.No_Driver_from_File;
                        }
                    }
                    else
                    {
                        driverViewData.IsDriverError = true;
                        List<DriverViewData> db = new List<DriverViewData>();
                        driverViewData.driverListViewData = db;
                        ViewBag.UploadMessage = MtdataResource.No_recordFound;
                    }

                    ModelState.Remove("FName");
                    ModelState.Remove("Email");
                    ModelState.Remove("LName");
                    ModelState.Remove("Phone");
                    ModelState.Remove("DriverNo");
                    ModelState.Remove("PIN");
                    ModelState.Remove("ConfirmPIN");
                    ModelState.Remove("DriverTaxNo");
                    ModelState.Remove("fk_FleetID");
                    ModelState.Remove("Fk_Country");
                    ModelState.Remove("Address");
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        if (String.IsNullOrEmpty(driverViewData.PIN))
                        {
                            ModelState.AddModelError("PIN", MtdataResource.Please_enter_PIN);
                            driverViewData.PIN = null;

                            return View(driverViewData);
                        }

                        string pCode = driverViewData.PIN.Hash();

                        _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                        _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                        _logMessage.Append("calling Hash method to encrypt the Driver pin");

                        string cPCode = driverViewData.PIN;
                        if (!string.IsNullOrEmpty(driverViewData.Account))
                            driverViewData.Account = StringExtension.EncryptDriver(driverViewData.Account);
                        
                        if (!string.IsNullOrEmpty(driverViewData.SageAccount))
                            driverViewData.SageAccount = StringExtension.EncryptDriver(driverViewData.SageAccount);
                        
                        driverViewData.PIN = pCode;
                        _logMessage.Append("calling ToDriverDto method to map DriverDto to DriverViewData and retrun DriverDto");
                        
                        var driverDto = driverViewData.ToDriverDto();
                        _logMessage.Append("ToDriverDto method executed successfully");
                        
                        var sessionDto = (SessionDto)Session["User"];
                        if (sessionDto != null)
                            driverDto.CreatedBy = (sessionDto.SessionUser.Email);
                        
                        _logMessage.Append("Calling Add method of service _driverService takes parameter First Name =" + driverViewData.FName + ",Last Name = " + driverViewData.LName + ", Phone= " + driverViewData.Phone + ", Email ID= " + driverViewData.Email + ", Driver Number = " + driverViewData.DriverNo + ", PIN = " + driverViewData.PIN + ", Driver tax Number = " + driverViewData.DriverNo + ", Fleet = " + driverViewData.fk_FleetID + ", Country = " + driverViewData.Fk_Country + ", State = " + driverViewData.Fk_State + ", City = " + driverViewData.Fk_City + ", Address = " + driverViewData.Address);

                        if (!string.IsNullOrEmpty(driverViewData.Email))
                        {
                            bool isExistEmail = _driverService.IsEmailExist(driverViewData.Email, driverViewData.fk_FleetID, driverViewData.DriverID);
                            if (isExistEmail)
                            {
                                ViewBag.Message = MtdataResource.Email_Exist_in_same_fleet;
                                driverViewData.PIN = null;
                                return View(driverViewData);
                            }
                        }

                        bool isExist = _driverService.Add(driverDto);
                        if (isExist)
                        {
                            ViewBag.Message = MtdataResource.Driver_Already_Exists;
                            driverViewData.PIN = null;
                            return View(driverViewData);
                        }

                        if (!string.IsNullOrEmpty(driverViewData.Email))
                        {
                            try
                            {
                                _logMessage.Append("Mail Sending begin and reading file ~/HtmlTemplate/DriverMail.html");
                                var filename = System.Web.HttpContext.Current.Server.MapPath("~/HtmlTemplate/DriverMail.html");
                                using (var myFile = new StreamReader(filename))
                                {
                                    string msgfile = myFile.ReadToEnd();
                                    var msgBody = new StringBuilder(msgfile);
                                    string cMail = driverViewData.Email;
                                    string driverNo = driverViewData.DriverNo;
                                    string cName = (((SessionDto)Session["User"]).SessionMerchant.Company);
                                    string cuserName = driverViewData.FName;
                                    msgBody.Replace("[##DriverNo##]", driverNo);
                                    msgBody.Replace("[##cName##]", cName);
                                    msgBody.Replace("[##cPassword##]", cPCode);
                                    msgBody.Replace("[##cuserName##]", cuserName);

                                    _logMessage.Append("calling SendMail method of utility class");

                                    var sendEmail = new EmailUtil();
                                    sendEmail.SendMail(cMail, msgBody.ToString(), cName, "Registered User");
                                }
                            }
                            catch (Exception ex)
                            {
                                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                            }
                        }

                        _logTracking.Append("Driver " + driverViewData.DriverNo + " was created by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                        _logger.LogInfoMessage(_logTracking.ToString());
                        _logMessage.Append("Add method of service _driverService executed successfully");

                        return Redirect("Index");
                    }

                    _logMessage.Append("populating CountryList, StateList, CityList and FleetList");
                    _logMessage.Append("population of CountryList, StateList, CityList and FleetList success");

                    return View(driverViewData);
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
                ModelState.Clear();
            }
            return View(driverViewData);
        }

        /// <summary>
        /// Purpose             :   To update the driver
        /// Function Name       :   Update
        /// Created By          :   Umesh Kumar
        /// Created On          :   09/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverId"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public ActionResult Update(int driverId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                var dto = (SessionDto)Session["User"];
                int merchantId = Convert.ToInt32(dto.SessionUser.fk_MerchantID);
                int logOnByUserId = Convert.ToInt32(dto.SessionUser.LogOnByUserId);
                int logOnByUserType = Convert.ToInt32(dto.SessionUser.LogOnByUserType);

                _logMessage.Append("calling GetDriverDto method of _driverService takes parameter driverId= " + driverId);
                
                var driverDto = _driverService.GetDriverDto(driverId, merchantId);
                if (driverDto == null)
                {
                    Session.Remove("User");
                    return RedirectToAction("index", "Member");
                }

                int userId = dto.SessionUser.UserID;
                int userType = dto.SessionUser.fk_UserTypeID;
                
                _logMessage.Append("GetDriverDto method executed successfully");
                _logMessage.Append("calling ToDriverViewData method to map DriverViewData to DriverDto and retrun DriverViewData");
                
                var driver = driverDto.ToDriverViewData();

                if (!string.IsNullOrEmpty(driver.Account))
                    driver.Account = StringExtension.DecryptDriver(driver.Account);
                
                if (!string.IsNullOrEmpty(driver.SageAccount))
                    driver.SageAccount = StringExtension.DecryptDriver(driver.SageAccount);
                
                _logMessage.Append("ToDriverViewData method executed successfully");
                _logMessage.Append("calling GetCountryList of _commonService to populate country dropdown");
                
                var countryDtoList = _commonService.GetCountryList();
                
                _logMessage.Append("GetCountryList of _commonService executed successfully");
                _logMessage.Append("calling ToCountryViewDataList method to map CountryViewData to CountryCodeDto and retrun CountryViewData");
                
                driver.CountryList = countryDtoList.ToCountryViewDataList();
                
                _logMessage.Append("ToCountryViewDataList method executed successfully");
                
                driver.FleetList = FleetList();
                
                if (userType == 2 || userType == 3)
                    driver.FleetList = FleetList();
                
                if (userType == 4)
                    driver.FleetList = _userService.UserFleetList(userId).ToViewDataList();
                
                if (logOnByUserType == 5)
                {
                    List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                    List<FleetListViewData> fleetViewResult = driver.FleetList.Where(m => fleetListAccessed.Contains((int)m.FleetID)).Select(m => m).Distinct().ToList();
                    driver.FleetList = fleetViewResult.AsEnumerable();
                }

                _logMessage.Append("FleetList method to populate FleetList");

                return View(driver);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   To update the driver
        /// Function Name       :   Update
        /// Created By          :   Umesh Kumar
        /// Created On          :   09/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverViewData"></param>
        /// <param name="driverId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(DriverViewData driverViewData, int driverId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");
            
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                int logOnByUserId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserId));
                int logOnByUserType = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserType));
                
                _logMessage.Append("populating CountryList, StateList, CityList and FleetList");
                driverViewData.CountryList = CountryList();
                driverViewData.FleetList = FleetList();
                
                if (userType == 4)
                    driverViewData.FleetList = _userService.UserFleetList(userId).ToViewDataList();
                
                if (logOnByUserType == 5)
                {
                    List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                    List<FleetListViewData> fleetViewResult = driverViewData.FleetList.Where(m => fleetListAccessed.Contains((int)m.FleetID)).Select(m => m).Distinct().ToList();
                    driverViewData.FleetList = fleetViewResult.AsEnumerable();
                }

                _logMessage.Append("population of CountryList, StateList, CityList and FleetList success");
                
                if (ModelState.IsValid)
                {
                    _logMessage.Append("calling ToDriverDto method to map DriverViewData to DriverDto and retrun DriverViewData");

                    DriverDto driverDto = driverViewData.ToDriverDto();

                    _logMessage.Append("ToDriverDto method executed successfully");
                    driverDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                    
                    _logMessage.Append("Calling Update method of service _driverService takes parameter First Name =" +
                                       driverViewData.FName + ",Last Name = " + driverViewData.LName + ", Phone= " +
                                       driverViewData.Phone + ", Email ID= " + driverViewData.Email +
                                       ", Driver Number = " + driverViewData.DriverNo + ", PIN = " + driverViewData.PIN +
                                       ", Driver tax Number = " + driverViewData.DriverNo + ", Fleet = " +
                                       driverViewData.fk_FleetID + ", Country = " + driverViewData.Fk_Country +
                                       ", State = " + driverViewData.Fk_State + ", City = " + driverViewData.Fk_City +
                                       ", Address = " + driverViewData.Address + ", and driver id= " + driverId);
                    
                    if (!string.IsNullOrEmpty(driverDto.Email))
                    {
                        bool isEmailExist = _driverService.IsEmailExist(driverDto.Email, driverDto.fk_FleetID, driverDto.DriverID);
                        if (isEmailExist)
                        {
                            ViewBag.Message = MtdataResource.Email_Exist_in_same_fleet;
                            driverViewData.PIN = null;
                            return View(driverViewData);
                        }
                    }

                    if (!string.IsNullOrEmpty(driverDto.Account))
                        driverDto.Account = StringExtension.EncryptDriver(driverDto.Account);
                    
                    if (!string.IsNullOrEmpty(driverDto.SageAccount))
                        driverDto.SageAccount = StringExtension.EncryptDriver(driverDto.SageAccount);
                    
                    if (_driverService.Update(driverDto, driverId))
                    {
                        ViewBag.Message = MtdataResource.Driver_Already_Exists;
                        driverViewData.PIN = null;

                        return View(driverViewData);
                    }

                    _logTracking.Append("Driver " + driverViewData.DriverNo + " was updated by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                    _logMessage.Append("Update method of service _driverService executed successfully");
                    
                    return Redirect("Index");
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            driverViewData.PIN = null;
            return View(driverViewData);
        }

        /// <summary>
        /// Purpose             :   To get the list of countries
        /// Function Name       :   CountryList
        /// Created By          :   Umesh Kumar
        /// Created On          :   13/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<CountryViewData> CountryList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetCountryList method of _commonService class to get countries");

                var countryDtoList = _commonService.GetCountryList();

                _logMessage.Append("GetCountryList method of _commonService executed successfully");
                _logMessage.Append("calling ToCountryViewDataList method to map CountryViewData to CountryCodeDto and retrun CountryViewData");
                
                var countryViewDataList = countryDtoList.ToCountryViewDataList();
                
                _logMessage.Append("ToCountryViewDataList method executed successfully");
                _logMessage.Append("converting countryViewDataList list into objcountrylist");
                
                List<CountryViewData> objcountrylist = countryViewDataList.ToList();
                
                _logMessage.Append("convert countryViewDataList into objcountrylist successful");
                
                var li = new List<SelectListItem> { new SelectListItem { Text = MtdataResource.Please_Select_a_Country, Value = "0" } };
                li.AddRange(objcountrylist.Select(item => new SelectListItem { Text = item.Title, Value = Convert.ToString(item.CountryCodesID) }));
                
                _logMessage.Append("returning country list");
                
                return objcountrylist;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :   To get the list of fleets
        /// Function Name       :   FleetList
        /// Created By          :   Umesh Kumar
        /// Created On          :   16/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<FleetListViewData> FleetList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetGatewayCurrency method of _gatewayService");

                _logMessage.Append("Calling GetFleetList method of _commonService class to get countries");
                
                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);                
                var fleetDtoList = _commonService.GetFleetList(merchantId);
                
                _logMessage.Append("GetFleetList method of _commonService executed successfully");
                _logMessage.Append("calling ToFleetViewDataSelectList method to map FleetListViewData to FleetListDto and retrun FleetListViewData");
                
                var fleetViewDataList = fleetDtoList.ToFleetViewDataSelectList();
                
                _logMessage.Append("ToCountryViewDataList method executed successfully");

                return fleetViewDataList;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :   To filter the list of drivers
        /// Function Name       :   DriverListByFiter
        /// Created By          :   Umesh Kumar
        /// Created On          :   17/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param> 
        /// <returns></returns>
        public JsonResult DriverListByFiter(string name = null, string driverNo = null, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetDeriverListByFilter method of _driverService class to get driver List");

                int listCount;
                var dto = (SessionDto)Session["User"];
                int merchantId = Convert.ToInt32(dto.SessionUser.fk_MerchantID);
                int logOnByUserId = Convert.ToInt32(dto.SessionUser.LogOnByUserId);
                int logOnByUserType = Convert.ToInt32(dto.SessionUser.LogOnByUserType);
                int userId = dto.SessionUser.UserID;
                int userType = dto.SessionUser.fk_UserTypeID;
                var userfleet = _userService.GetUsersFleet(userId);//2
                
                FilterSearchParametersDto filterSearch = new FilterSearchParametersDto()
                {
                    Name = name,
                    DriverNo = driverNo,
                    StartIndex = jtStartIndex,
                    PageSize = jtPageSize,
                    Sorting = jtSorting,
                    MerchantId = merchantId,
                    UserId = userId,
                    UserType = userType,
                    userFleet = userfleet,
                    LogOnByUserType = logOnByUserType,
                    LogOnByUserId = logOnByUserId
                };

                var viewDataList = _driverService.GetDeriverListByFilter(filterSearch);//1
                listCount = _driverService.GetDriverDtoList(filterSearch).Count();
                if (userType == 4)
                    listCount = _driverService.UserDriverList(filterSearch).Count();
                
                if (string.IsNullOrEmpty(name))
                {
                    _logMessage.Append("Calling UserList method of _driverService class to count fee");
                    _logMessage.Append("UserList method of _driverService executed successfully result = " + listCount);
                    return Json(new { Result = "OK", Records = viewDataList, TotalRecordCount = listCount });
                }

                var driverDtos = viewDataList as DriverDto[] ?? viewDataList.ToArray();

                return Json(new { Result = "OK", Records = driverDtos, TotalRecordCount = listCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        /// Purpose             :   To delete the driver
        /// Function Name       :   Delete
        /// Created By          :   Umesh Kumar
        /// Created On          :   18/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(int driverId)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                try
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    _logMessage.Append("Calling DeleteDriver method of _driverService class takes driverId= " + driverId);

                    _driverService.DeleteDriver(driverId);

                    _logMessage.Append("DeleteVehicle method of _vehicleService class executed successfully");
                    _logTracking.Append("Driver ID" + driverId + " was deleted by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return Json(new { Result = "OK" });
                }
                catch (Exception exception)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), exception);
                }
            }

            return Json(new { Result = "No" });
        }

        /// <summary>
        /// Purpose             :   To delete the driver
        /// Function Name       :   Delete
        /// Created By          :   Umesh Kumar
        /// Created On          :   18/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverId"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public List<DriverViewData> UploadExcelFile(HttpPostedFileBase uploadFile)
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            List<DriverViewData> objDriverViewData = new List<DriverViewData>();
            
            try
            {
                if (uploadFile.ContentLength > 0)
                {
                    if (!Directory.Exists(Server.MapPath("../Uploads")))
                        Directory.CreateDirectory(Server.MapPath("../Uploads"));
                    
                    string filePath = Path.Combine(HttpContext.Server.MapPath("../Uploads"), Path.GetFileName(uploadFile.FileName));
                    uploadFile.SaveAs(filePath);

                    string line = string.Empty;
                    string[] strArray = null;

                    // work out where we should split on comma, but not in a sentance
                    Regex r = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

                    //Set the filename in to our stream
                    StreamReader sr = new StreamReader(filePath);

                    //Read the first line and split the string at , with our regular express in to an array
                    line = sr.ReadLine();
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (!string.IsNullOrEmpty(line))
                            strArray = r.Split(line);       //For each item in the new split array, dynamically builds our Data columns. Save us having to worry about it.

                        DriverViewData Obj = new DriverViewData();
                        Obj.FName = strArray[0].Trim();
                        Obj.LName = strArray[1].Trim();
                        Obj.Phone = strArray[2].Trim();
                        Obj.Email = strArray[3].Trim();
                        Obj.DriverNo = strArray[4].Trim();
                        Obj.PIN = strArray[5].Trim(); ;
                        Obj.DriverTaxNo = strArray[6].Trim();
                        Obj.SageAccount = strArray[7].Trim();
                        Obj.BankName = strArray[8].Trim();
                        Obj.AccountName = strArray[9].Trim();
                        Obj.BSB = strArray[10].Trim();
                        Obj.Account = strArray[11].Trim();

                        Obj.FleetName = strArray[12].Trim();
                        Obj.Fk_Country = strArray[13].Trim();
                        Obj.Fk_State = strArray[14].Trim();
                        Obj.Fk_City = strArray[15].Trim();
                        Obj.Address = strArray[16].Trim();

                        Obj.ConfirmPIN = strArray[5].Trim();
                        Obj.CreatedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                        Obj.ModifiedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                        objDriverViewData.Add(Obj);
                    }

                    sr.Dispose();
                }

                return objDriverViewData;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return null;
            }
        }

        /// <summary>
        /// Process the file supplied and process the CSV to a dynamic datatable
        /// </summary>
        /// <param name="fileName">String</param>
        /// <returns>DataTable</returns>
        private DataTable ProcessCSV(string fileName)
        {
            DataTable dt = new DataTable();
            try
            {
                //Set up our variables 
                string line = string.Empty;
                string[] strArray = null;
                DataRow row;

                // work out where we should split on comma, but not in a sentance
                Regex r = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

                //Set the filename in to our stream
                StreamReader sr = new StreamReader(fileName);

                //Read the first line and split the string at , with our regular express in to an array
                line = sr.ReadLine();
                if (!string.IsNullOrEmpty(line))
                    strArray = r.Split(line);       //For each item in the new split array, dynamically builds our Data columns. Save us having to worry about it.
                
                Array.ForEach(strArray, s => dt.Columns.Add(new DataColumn()));

                //Read each line in the CVS file until it's empty
                while ((line = sr.ReadLine()) != null)
                {
                    row = dt.NewRow();

                    //add our current value to our data row
                    row.ItemArray = r.Split(line);
                    dt.Rows.Add(row);
                }

                //Tidy Streameader up
                sr.Dispose();

                //return a the new DataTable
                return dt;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return dt;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driverId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ResetPin(int driverId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                
                _logMessage.Append("calling GetDriverDto method of _driverService takes parameter driverId= " + driverId);

                var driverDto = _driverService.GetDriverDto(driverId, merchantId);
                if (driverDto == null)
                {
                    Session.Abandon();
                    return RedirectToAction("index", "Member");
                }

                _logMessage.Append("GetDriverDto method executed successfully");
                _logMessage.Append("calling ToDriverViewData method to map DriverViewData to DriverDto and retrun DriverViewData");

                var driver = driverDto.ToDriverViewData();
                driver.PIN = string.Empty;

                return View(driver);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driverViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ResetPin(DriverViewData driverViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                Regex regexNum = new Regex(@"^[0-9]+$");
                bool isValid = true;

                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);

                if (!string.IsNullOrEmpty(driverViewData.PIN))
                {
                    if (driverViewData.PIN.Length < 4)
                    {
                        ModelState.AddModelError("PIN", MtdataResource.PIN_Min_Length);
                        isValid = false;
                    }
                }

                if (!string.IsNullOrEmpty(driverViewData.PIN))
                {
                    if (driverViewData.PIN.Length > 10)
                    {
                        ModelState.AddModelError("PIN", MtdataResource.PIN_Max_Length);
                        isValid = false;
                    }
                }

                if (!string.IsNullOrEmpty(driverViewData.PIN))
                {
                    Match matchNum = regexNum.Match(driverViewData.PIN);
                    if (!matchNum.Success)
                    {
                        ModelState.AddModelError("PIN", MtdataResource.InvalidPIN);
                        isValid = false;
                    }
                }

                if (string.IsNullOrEmpty(driverViewData.PIN))
                {
                    ModelState.AddModelError("PIN", MtdataResource.DriverController_Create_Please_enter_pin);
                    isValid = false;
                }

                if (string.IsNullOrEmpty(driverViewData.ConfirmPIN))
                {
                    ModelState.AddModelError("ConfirmPIN", MtdataResource.DriverController_Create_Please_enter_confirm_pin);
                    isValid = false;
                }
                
                if (driverViewData.PIN != driverViewData.ConfirmPIN)
                {
                    ModelState.AddModelError("ConfirmPIN", MtdataResource.Pin_Mismatch);
                    isValid = false;
                }
                
                if (isValid)
                {
                    string Pin = driverViewData.PIN.Hash();
                    bool result = _driverService.ResetPin(driverViewData.DriverID, Pin);
                    if (result)
                    {
                        if (driverViewData.Email != null)
                        {
                            try
                            {
                                _logMessage.Append("calling SendMail method of utility class for reset driver PIN");
                                SendEmail(driverViewData);
                            }
                            catch (Exception ex)
                            {
                                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                            }

                            _logTracking.Append("Driver ID" + driverViewData.DriverID + " Pin Reset by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                            _logger.LogInfoMessage(_logTracking.ToString());
                        }

                        ViewBag.Message = MtdataResource.PIN_Updated_Successfully;
                    }
                    else
                    {
                        ViewBag.Error = MtdataResource.invalid_info_to_reset_pin;
                    }

                    return View(driverViewData);
                }

                return View(driverViewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driverViewData"></param>
        private void SendEmail(DriverViewData driverViewData)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                var filename = System.Web.HttpContext.Current.Server.MapPath("~/HtmlTemplate/DriverPin.html");
                var myFile = new System.IO.StreamReader(filename);
                string msgfile = myFile.ReadToEnd();
                var msgBody = new StringBuilder(msgfile);
                
                myFile.Close();
                
                string cMail = driverViewData.Email;
                string cName = driverViewData.FName;
                string cPin = driverViewData.PIN;
                string cDriverNo = driverViewData.DriverNo;
                
                msgBody.Replace("[##UserName##]", cName);
                msgBody.Replace("[##email##]", cMail);
                msgBody.Replace("[##pin##]", cPin);
                msgBody.Replace("[##DriverNo##]", cDriverNo);

                _logMessage.Append("calling SendMail method of utility class");

                var sendEmail = new EmailUtil();
                sendEmail.SendMail(cMail, msgBody.ToString(), cName, "PIN Reset");
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                throw;
            }
        }

        /// <summary>
        /// Method for download excel file format for uploading driver list
        /// </summary>
        /// <param name="fileName">String</param>
        /// <returns>DataTable</returns>
        public FileResult Download(string file)
        {
            string CurrentFileName = (from fls in GetFiles()
                                      where fls.FileName == file
                                      select fls.FilePath).First();

            string contentType = string.Empty;

            if (CurrentFileName.Contains(".pdf"))
                contentType = "application/pdf";
            else if (CurrentFileName.Contains(".docx"))
                contentType = "application/docx";
            else if (CurrentFileName.Contains(".csv"))
                contentType = "application/csv";

            return File(CurrentFileName, contentType, file);
        }

        /// <summary>
        /// Get All file list
        /// </summary>
        /// <param name="fileName">String</param>
        /// <returns>DataTable</returns>
        public List<DownloadFileViewData> GetFiles()
        {
            List<DownloadFileViewData> lstFiles = new List<DownloadFileViewData>();
            DirectoryInfo dirInfo = new DirectoryInfo(Server.MapPath("../Assets/TemplateFiles"));

            int i = 0;
            foreach (var item in dirInfo.GetFiles())
            {
                lstFiles.Add(new DownloadFileViewData()
                {
                    FileId = i + 1,
                    FileName = item.Name,
                    FilePath = dirInfo.FullName + @"\" + item.Name
                });
                i = i + 1;
            }

            return lstFiles;
        }

        /// <summary>
        /// Purpose             :   To get list of fleets of merchant
        /// Function Name       :   GetDriverRec
        /// Created By          :   Asif Shafeeque
        /// Created On          :   07/10/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetDriverRec()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID > 3)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                var dto = (SessionDto)Session["User"];
                int userType = dto.SessionUser.fk_UserTypeID;
                int merchantId = Convert.ToInt32(dto.SessionUser.fk_MerchantID);

                DriverRecListViewData driverObj = new DriverRecListViewData();
                
                _logMessage.Append("fetching fleet list and map to view data");
                
                int logOnByUserId = dto.SessionUser.LogOnByUserId;
                int loggedbyUserType = dto.SessionUser.LogOnByUserType;
                
                if (loggedbyUserType == 5) // For 5
                {
                    driverObj.fleetlist = _userService.FleetList(merchantId).ToViewDataList();
                    IEnumerable<UserFleetDto> accessedFleet = _userService.GetUsersFleet(logOnByUserId);
                    driverObj.fleetlist = driverObj.fleetlist.Where(x => accessedFleet.Select(y => y.fk_FleetID).Contains((int)x.FleetID)).ToList();
                    driverObj.ModifiedBy = dto.SessionUser.Email;
                    driverObj.loggedUser = Convert.ToString(dto.SessionUser.fk_UserTypeID);

                    return View(driverObj);
                }
                else
                {
                    if (userType == 2 || userType == 3)
                    {
                        driverObj.fleetlist = _userService.FleetList(merchantId).ToViewDataList();
                        driverObj.ModifiedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                        driverObj.loggedUser = Convert.ToString(((SessionDto)Session["User"]).SessionUser.fk_UserTypeID);

                        return View(driverObj);
                    }
                    
                    driverObj.fleetlist = _driverService.AllFleets().ToViewDataList();
                    driverObj.ModifiedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                    driverObj.loggedUser = Convert.ToString(((SessionDto)Session["User"]).SessionUser.fk_UserTypeID);
                    
                    return View(driverObj);
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   To get list of fleets of merchant and driver and user list
        /// Function Name       :   GetDriverRecipient
        /// Created By          :   Asif Shafeeque
        /// Created On          :   07/10/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetDriverRecipient(int fleetId)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                DriverRecListViewData driverObj = new DriverRecListViewData();

                _logMessage.Append("fetching driver recipient and map to view data");
                
                driverObj.UserList = _driverService.GetDriverReceipent(merchantId, fleetId).ToDriverViewDataList();
                driverObj.ModifiedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                
                return Json(driverObj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return Json(null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetAllDriverRecipient()
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                DriverRecListViewData driverObj = new DriverRecListViewData();
                _logMessage.Append("fetching driver recipient and map to view data");

                int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                
                if (userType == 1)
                    driverObj.UserList = _driverService.GetDriverReceipent().ToDriverViewDataList().Distinct();
                
                if (userType == 2 || userType == 3)
                    driverObj.UserList = _driverService.GetMerchantDriverRecipient(merchantId).ToDriverViewDataList().Distinct();
                
                driverObj.ModifiedBy = ((SessionDto)Session["User"]).SessionUser.Email;

                return Json(driverObj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return Json(null);
        }

        /// <summary>
        /// Purpose             :   To save and update driver and user list
        /// Function Name       :   GetDriverRec
        /// Created By          :   Asif Shafeeque
        /// Created On          :   07/10/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="DriverRecipientViewdata"></param>
        /// <returns></returns>
        [HttpPost]
        public string GetDriverRec(DriverRecListViewData DriverRecipientViewdata)
        {
            if (Session["User"] == null) return string.Empty;
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null && ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 2 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 3)
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                bool response = false;
                try
                {
                    DriverRecListDto obj = new DriverRecListDto();
                    obj = DriverRecipientViewdata.toDto();
                    
                    int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                    int loggeUser = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                    
                    IEnumerable<DriverRecipientDto> DriverRecObj = DriverRecipientViewdata.UserList.TorecipientDtoList();
                    
                    int fleetId = Convert.ToInt32(DriverRecipientViewdata.fk_FleetId);
                    int userType = Convert.ToInt32(DriverRecipientViewdata.UserType);
                    
                    if (loggeUser == 2 || loggeUser == 3)
                        response = _driverService.MerchantDriverRecipient(obj, fleetId, userType, merchantId);
                    
                    if (loggeUser == 1)
                        response = _driverService.UpdateDriverRecipient(obj, fleetId, userType, merchantId);
                    
                    string Msg = string.Empty;
                    if (response)
                        Msg = "Saved successfully";

                    return Msg;
                }
                catch (Exception exception)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), exception);
                }
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetScheduleReport()
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                ScheduleReportViewData scheduleReport = new ScheduleReportViewData
                {
                    TimeZones = TimeZoneList(),
                    DayList = WeekDayList(),
                    MonthDayList = MonthDayList(),
                    ReportList = _driverService.DriverReportList().ToReportList(),
                };

                return View(scheduleReport);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetScheduleReport(ScheduleReportViewData scheduleViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                scheduleViewData.CreatedBy = Convert.ToString(((SessionDto)Session["User"]).SessionUser.Email);
                scheduleViewData.ModifiedBy = Convert.ToString(((SessionDto)Session["User"]).SessionUser.Email);

                bool IsValid = true;
                
                if (scheduleViewData.fk_ReportID == 4)
                    scheduleViewData.Frequency = 1;
                
                if (scheduleViewData.fk_ReportID == 5)
                    scheduleViewData.Frequency = 2;
                
                if (scheduleViewData.fk_ReportID == 6)
                    scheduleViewData.Frequency = 3;

                if (scheduleViewData.Frequency == 2)
                {
                    if (scheduleViewData.WeekDay == null)
                    {
                        ModelState.AddModelError("WeekDay", MtdataResource.Please_select_week_day);
                        IsValid = false;
                    }
                }

                if (scheduleViewData.Frequency == 3)
                {
                    if (scheduleViewData.MonthDay == null)
                    {
                        ModelState.AddModelError("MonthDay", MtdataResource.Please_select_month_day);
                        IsValid = false;
                    }
                }
                
                if (scheduleViewData.DateFrom > scheduleViewData.DateTo)
                {
                    ModelState.AddModelError("DateFrom", MtdataResource.Date_from_validation);
                    IsValid = false;
                }

                if (scheduleViewData.TimeZone == null)
                {
                    ModelState.AddModelError("TimeZone", MtdataResource.Please_select_a_timezone);
                    IsValid = false;
                }

                if (scheduleViewData.DateFrom == null)
                {
                    ModelState.AddModelError("DateFrom", MtdataResource.Please_enter_date_from);
                    IsValid = false;
                }

                if (scheduleViewData.DateTo == null)
                {
                    ModelState.AddModelError("DateTo", MtdataResource.Please_enter_date_to);
                    IsValid = false;
                }

                if (scheduleViewData.StartTime == null)
                {
                    ModelState.AddModelError("StartTime", MtdataResource.Please_enter_the_start_time);
                    IsValid = false;
                }

                scheduleViewData.FrequencyType = 1;
                if (IsValid)
                {
                    var scheduleDto = scheduleViewData.ToScheduleDto();
                    _driverService.ScheduleDriverReport(scheduleDto);
                    _logMessage.Append("ScheduleDriverReport of _driverService executed successfully");

                    return Redirect("ScheduleIndex");
                }

                scheduleViewData.TimeZones = TimeZoneList();
                scheduleViewData.DayList = WeekDayList();
                scheduleViewData.MonthDayList = MonthDayList();
                scheduleViewData.ReportList = _driverService.DriverReportList().ToReportList();
                
                _logMessage.Append("DayList, MonthDayList, TimeZoneList methods executed successfully");
                
                return View(scheduleViewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            : to get the list of time zone
        /// Function Name       :   TimeZoneList
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/4/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public List<TimeZoneViewData> TimeZoneList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("geting GetSystemTimeZones from TimeZoneInfo class and assign it into TimeZoneViewData and creating a list into timeZone");

                var timeZone = TimeZoneInfo.GetSystemTimeZones()
                        .Select(z => new TimeZoneViewData { TimeZoneName = z.DisplayName, TimeZoneTime = z.Id })
                        .ToList();

                _logMessage.Append("list of timeZone created successfully");
                _logMessage.Append("returing list of timezones");

                return timeZone;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

        /// <summary>
        ///  Purpose            : to get the list of week day
        /// Function Name       :   WeekDayList
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/5/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<WeekDayViewData> WeekDayList()
        {
            List<WeekDayViewData> dayList = new List<WeekDayViewData>
            {
                new WeekDayViewData {DayID = 1, DayName = "Sunday"},
                new WeekDayViewData {DayID = 2, DayName = "Monday"},
                new WeekDayViewData {DayID = 4, DayName = "Tuesday"},
                new WeekDayViewData {DayID = 8, DayName = "Wednesday"},
                new WeekDayViewData {DayID = 16, DayName = "Thursday"},
                new WeekDayViewData {DayID = 32, DayName = "Friday"},
                new WeekDayViewData {DayID = 64, DayName = "Saturday"}
            };

            return dayList;
        }

        /// <summary>
        ///  Purpose            : to get the list of month days
        /// Function Name       :   MonthDayList
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/5/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public static IEnumerable<MonthDayViewData> MonthDayList()
        {
            List<MonthDayViewData> monthDayList = new List<MonthDayViewData>();
            for (int index = 1; index <= 31; index++)
            {
                monthDayList.Add(new MonthDayViewData { MonthID = index, MonthName = index.ToString(CultureInfo.InvariantCulture) });
            }

            return monthDayList;
        }

        /// <summary>
        ///  Purpose            : to get list of schedule driver report
        /// Function Name       :   ScheduleReportListByFiter
        /// Created By          :  Asf Shafeeque
        /// Created On          :  7/27/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult ScheduleReportListByFiter(string name = null, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling ScheduleReportListByFiter method of adminUserService class to get Admin Users List");

                name = name.Trim();
                
                int adminUserCount;

                var scheduleReport = _driverService.ScheduleReportListByFilter(name, jtStartIndex, jtPageSize, jtSorting);
                foreach (var sc in scheduleReport)
                {
                    TimeSpan ts = (TimeSpan)sc.StartTime;
                    int hours = ts.Hours;
                    int min = ts.Minutes;
                    string str = hours.ToString() + ':' + min.ToString();
                    sc.TimeInString = str;

                    TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(sc.TimeZone);
                    sc.TimeZone = timeZone.ToString();
                    FindFrequency(sc);
                    
                    if (sc.Frequency == 2)
                        FindWeekDay(sc);
                }

                _logMessage.Append("GetAdminUserListByFilter of adminUserService executed successfully");

                if (string.IsNullOrEmpty(name))
                {
                    _logMessage.Append("Calling AdminUserListCount method of adminUserService class to count adminUsers");
                
                    adminUserCount = _driverService.ScheduleDriverReportList().ToList().Count;
                    
                    _logMessage.Append("AdminUserListCount method of adminUserService executed successfully result = " + adminUserCount);
                    
                    return Json(new { Result = "OK", Records = scheduleReport, TotalRecordCount = adminUserCount });
                }

                var userDtos = scheduleReport as ScheduleReportDto[] ?? scheduleReport.ToArray();
                
                adminUserCount = userDtos.ToList().Count();
                
                return Json(new { Result = "OK", Records = userDtos, TotalRecordCount = adminUserCount });
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        ///  Purpose            : to get the list of time zone
        /// Function Name       :   TimeZoneList
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/4/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public void FindFrequency(ScheduleReportDto scheduleDto)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("assinging frequency to scheduleViewdata");

                switch (scheduleDto.Frequency)
                {
                    case 1:
                        scheduleDto.FrequencyName = "Daily";
                        break;
                    case 2:
                        scheduleDto.FrequencyName = "Weekly";
                        break;
                    case 3:
                        scheduleDto.FrequencyName = "Monthly";
                        break;
                    case 4:
                        scheduleDto.FrequencyName = "Yearly";
                        break;
                }

                _logMessage.Append("frequencies assigned to scheduleViewdata");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
        }

        /// <summary>
        ///  Purpose            : to get the list of time zone
        /// Function Name       :   TimeZoneList
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/4/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public static void FindWeekDay(ScheduleReportDto scheduleDto)
        {
            switch (scheduleDto.WeekDay)
            {
                case 1:
                    scheduleDto.WeekName = "Sunday";
                    break;
                case 2:
                    scheduleDto.WeekName = "Monday";
                    break;
                case 4:
                    scheduleDto.WeekName = "Tuesday";
                    break;
                case 8:
                    scheduleDto.WeekName = "Wednesday";
                    break;
                case 16:
                    scheduleDto.WeekName = "Thursday";
                    break;
                case 32:
                    scheduleDto.WeekName = "Friday";
                    break;
                case 64:
                    scheduleDto.WeekName = "Saturday";
                    break;
            }
        }

        /// <summary>
        /// Purpose            : to list of get schedule driver report
        /// Function Name       :   ScheduleIndex
        /// Created By          :  Asf Shafeeque
        /// Created On          :  7/27/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ScheduleIndex()
        {
            if ((SessionDto)Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                return View();
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            : to get schedule driver report
        /// Function Name       :   ScheduleUpdate
        /// Created By          :  Asf Shafeeque
        /// Created On          :  7/27/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="scheduleId"></param>
        /// <param name="reportId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ScheduleUpdate(int scheduleId, int reportId)
        {
            if ((SessionDto)Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                _logMessage.Append("calling GetScheduleDto method _scheduleService");
                
                var scheduleDto = _driverService.GetScheduleDto(scheduleId);
                
                _logMessage.Append("GetScheduleDto method executed successfully");

                _logMessage.Append("calling ToScheduleViewData method to map ScheduleReportViewData to ScheduleReportDto and retrun ScheduleReportViewData");
                
                var scheduleReport = scheduleDto.ToScheduleViewData();
                
                _logMessage.Append("ToScheduleViewData method executed successfully");
                _logMessage.Append("calling FrequencyList,  FrequencyTypeListmethod,TimeZoneList method of same class");
                
                scheduleReport.TimeZones = TimeZoneList();
                scheduleReport.DayList = WeekDayList();
                scheduleReport.MonthDayList = MonthDayList();
                scheduleReport.ReportList = _scheduleService.ReportListReadOnly().ToReportList();

                return View(scheduleReport);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            : to schedule driver report
        /// Function Name       :   ScheduleUpdate
        /// Created By          :  Asf Shafeeque
        /// Created On          :  7/27/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="scheduleViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ScheduleUpdate(ScheduleReportViewData scheduleViewData)
        {
            if ((SessionDto)Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                scheduleViewData.CreatedBy = Convert.ToString(((SessionDto)Session["User"]).SessionUser.Email);
                scheduleViewData.ModifiedBy = Convert.ToString(((SessionDto)Session["User"]).SessionUser.Email);
                
                bool IsValid = true;
                
                if (scheduleViewData.fk_ReportID == 4)
                    scheduleViewData.Frequency = 1;
                
                if (scheduleViewData.fk_ReportID == 5)
                    scheduleViewData.Frequency = 2;
                
                if (scheduleViewData.fk_ReportID == 6)
                    scheduleViewData.Frequency = 3;

                if (scheduleViewData.Frequency == 2)
                {
                    if (scheduleViewData.WeekDay == null)
                    {
                        ModelState.AddModelError("WeekDay", MtdataResource.Please_select_week_day);
                        IsValid = false;
                    }
                }

                if (scheduleViewData.Frequency == 3)
                {
                    if (scheduleViewData.MonthDay == null)
                    {
                        ModelState.AddModelError("MonthDay", MtdataResource.Please_select_month_day);
                        IsValid = false;
                    }
                }

                if (scheduleViewData.DateFrom > scheduleViewData.DateTo)
                {
                    ModelState.AddModelError("DateFrom", MtdataResource.Date_from_validation);
                    IsValid = false;
                }

                if (scheduleViewData.TimeZone == null)
                {
                    ModelState.AddModelError("TimeZone", MtdataResource.Please_select_a_timezone);
                    IsValid = false;
                }

                if (scheduleViewData.DateFrom == null)
                {
                    ModelState.AddModelError("DateFrom", MtdataResource.Please_enter_date_from);
                    IsValid = false;
                }

                if (scheduleViewData.DateTo == null)
                {
                    ModelState.AddModelError("DateTo", MtdataResource.Please_enter_date_to);
                    IsValid = false;
                }

                if (scheduleViewData.StartTime == null)
                {
                    ModelState.AddModelError("StartTime", MtdataResource.Please_enter_the_start_time);
                    IsValid = false;
                }

                scheduleViewData.FrequencyType = 1;

                if (IsValid)
                {
                    var scheduleDto = scheduleViewData.ToScheduleDto();
                    _driverService.ScheduleDriverReportUpdate(scheduleDto);
                    _logMessage.Append("ScheduleDriverReport of  _driverService executed successfully");
                    _logTracking.Append("schedule Report Id" + scheduleViewData.fk_ReportID + " updated by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                    return Redirect("ScheduleIndex");
                }

                scheduleViewData.TimeZones = TimeZoneList();
                scheduleViewData.DayList = WeekDayList();
                scheduleViewData.MonthDayList = MonthDayList();
                scheduleViewData.ReportList = _scheduleService.ReportListReadOnly().ToReportList();
                _logMessage.Append("DayList,  MonthDayList,TimeZoneList methods executed successfully");

                return View(scheduleViewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uploadFile"></param>
        /// <returns></returns>
        private bool IsValidExcel(HttpPostedFileBase uploadFile)
        {
            string line = string.Empty;
            string filePath = string.Empty;

            if (uploadFile.ContentLength > 0)
            {
                if (!Directory.Exists(Server.MapPath("../Uploads")))
                    Directory.CreateDirectory(Server.MapPath("../Uploads"));

                filePath = Path.Combine(HttpContext.Server.MapPath("../Uploads"),
                Path.GetFileName(uploadFile.FileName));
                uploadFile.SaveAs(filePath);
            }

            //Set the filename in to our stream
            StreamReader sr = new StreamReader(filePath);
            string driverExcel = "FirstName,LastName,Phone,Email,DriverNumber,PIN,DriverTaxNumber,Sage Account,Bank Name,Account Name,BSB,Account,Fleet,Country,State,City,Address";

            //Read the first line 
            line = sr.ReadLine();
            sr.Dispose();

            if (!string.IsNullOrEmpty(line))
            {
                if (driverExcel != line)
                    return false;
                else
                    return true;
            }

            return false;
        }
    }
}
