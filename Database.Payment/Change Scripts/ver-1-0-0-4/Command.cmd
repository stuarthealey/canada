@echo off
REM: Command File Created by Microsoft Visual Database Tools
REM: Authentication type: Windows NT
REM: Usage: CommandFilename [Server] [Database]

if '%1' == '' goto usage
if '%2' == '' goto usage

if '%1' == '/?' goto usage
if '%1' == '-?' goto usage
if '%1' == '?' goto usage
if '%1' == '/help' goto usage

REM: ====================================
REM: Ordered scripts
REM: ====================================

echo.
echo "dbo.CardToken.sql"
osql -S %1 -d %2 -E -b -i "dbo.CardToken.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.CarOwners.sql"
osql -S %1 -d %2 -E -b -i "dbo.CarOwners.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.DefaultSetting.sql"
osql -S %1 -d %2 -E -b -i "dbo.DefaultSetting.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.Driver.sql"
osql -S %1 -d %2 -E -b -i "dbo.Driver.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.ExternalUser.sql"
osql -S %1 -d %2 -E -b -i "dbo.ExternalUser.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.FDMerchant.sql"
osql -S %1 -d %2 -E -b -i "dbo.FDMerchant.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.Fleet.sql"
osql -S %1 -d %2 -E -b -i "dbo.Fleet.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.Merchant.sql"
osql -S %1 -d %2 -E -b -i "dbo.Merchant.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.MTDSemiIntigratedTxn.sql"
osql -S %1 -d %2 -E -b -i "dbo.MTDSemiIntigratedTxn.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.MTDTransaction.sql"
osql -S %1 -d %2 -E -b -i "dbo.MTDTransaction.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.MtdtxnSattlementUK.sql"
osql -S %1 -d %2 -E -b -i "dbo.MtdtxnSattlementUK.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.MtdtxnSemiUK.sql"
osql -S %1 -d %2 -E -b -i "dbo.MtdtxnSemiUK.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.Report.sql"
osql -S %1 -d %2 -E -b -i "dbo.Report.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.Roles.sql"
osql -S %1 -d %2 -E -b -i "dbo.Roles.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.ScheduleReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.ScheduleReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.Surcharge.sql"
osql -S %1 -d %2 -E -b -i "dbo.Surcharge.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.TAKey.sql"
osql -S %1 -d %2 -E -b -i "dbo.TAKey.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.Terminal.sql"
osql -S %1 -d %2 -E -b -i "dbo.Terminal.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.TransactionCountSlab.sql"
osql -S %1 -d %2 -E -b -i "dbo.TransactionCountSlab.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.UKMTDTransaction.sql"
osql -S %1 -d %2 -E -b -i "dbo.UKMTDTransaction.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.UKMtdtxnSattlement.sql"
osql -S %1 -d %2 -E -b -i "dbo.UKMtdtxnSattlement.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.UserFleet.sql"
osql -S %1 -d %2 -E -b -i "dbo.UserFleet.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.UserMerchant.sql"
osql -S %1 -d %2 -E -b -i "dbo.UserMerchant.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.Users.sql"
osql -S %1 -d %2 -E -b -i "dbo.Users.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.UserType.sql"
osql -S %1 -d %2 -E -b -i "dbo.UserType.sql"
if %ERRORLEVEL% NEQ 0 goto errors

REM: ====================================
REM: Reference Data Scripts
REM: ====================================

echo.
echo "dbo.ReferenceDataChanges.sql"
osql -S %1 -d %2 -E -b -i "dbo.ReferenceDataChanges.sql"
if %ERRORLEVEL% NEQ 0 goto errors

goto finish

REM: How to use screen
:usage
echo.
echo Usage: MyScript Server Database
echo Server: the name of the target SQL Server
echo Database: the name of the target database
echo.
echo Example: MyScript.cmd MainServer MainDatabase
echo.
echo.
goto done

REM: error handler
:errors
echo.
echo WARNING! Error(s) were detected!
echo --------------------------------
echo Please evaluate the situation and, if needed,
echo restart this command file. You may need to
echo supply command parameters when executing
echo this command file.
echo.
pause
goto done

REM: finished execution
:finish
echo.
echo Script execution is complete!
:done
@echo on