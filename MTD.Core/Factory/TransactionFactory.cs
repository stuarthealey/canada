﻿using MTD.Core.Factory.Interface;
using MTD.Core.DataTransferObjects;
using MTD.Data.Data;

namespace MTD.Core.Factory
{
    class TransactionFactory : ITransactionFactory
    {
        public MTDTransaction CreateTransaction(TransactionDto transactionDto)
        {
            MTDTransaction transaction = new MTDTransaction
            {
                //TransID = transactionDto.TransID,
                //VehicleNumber = transactionDto.VehicleNumber,
                //DriverNo = transactionDto.DriverNo,
                //fk_GatewayID = transactionDto.fk_GatewayID,
                //GatewayReturnCode = transactionDto.GatewayReturnCode,
                //GatewayReturnMsg = transactionDto.GatewayReturnMsg,
                //TransDate = transactionDto.TransDate,
                //TransReturncode = transactionDto.TransReturncode,
                //TransAmount = transactionDto.TransAmount,
                //TransType = transactionDto.TransType,
                //TransRefID = transactionDto.TransRefID,       
                //TransMode = transactionDto.TransMode,
                //TransCardHolderName = transactionDto.TransCardHolderName,
                //TransEmail = transactionDto.TransEmail,
                //TransPhone = transactionDto.TransPhone,
                //TransAddress = transactionDto.TransAddress,
                //TransCity = transactionDto.TransCity,
                //TransState = transactionDto.TransState,
                //TransZip = transactionDto.TransZip,
                //TransCountry = transactionDto.TransCountry,
                //TransCCNo = transactionDto.TransCCNo,
                //TransCCExpDate = transactionDto.TransCCExpDate,
                //TransCCType = transactionDto.TransCCType,
                //TransGatewayTnxID = transactionDto.TransGatewayTnxID,
                //TransAuthCode = transactionDto.TransAuthCode,
                //TransReturnMsg = transactionDto.TransReturnMsg,               
                //Currency = transactionDto.Currency,
                //SurAmt = transactionDto.SurAmt,
                //TaxAmt = transactionDto.TaxAmt,
                //BookingAmt = transactionDto.BookingAmt,
                //PNRef=transactionDto.PNRef,
                //BatchNumber = transactionDto.BatchNumber,
                //ResponseXML = transactionDto.ResponseXML

            };
            return transaction;
        }
    }
}