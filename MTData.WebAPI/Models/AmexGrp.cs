﻿namespace MTData.WebAPI.Models
{
    public class AmexGrp
    {
        public string GatewayTxnId { get; set; }
        public string CardToken { get; set; }
        public string AmExTranID { get; set; }
        public string AmExPOSData { get; set; }
    }
}   