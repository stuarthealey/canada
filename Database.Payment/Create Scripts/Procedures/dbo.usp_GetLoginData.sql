IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetLoginData]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_GetLoginData]
GO

CREATE PROCEDURE dbo.usp_GetLoginData
(
	@DeviceId VARCHAR(100),
	@DriverId VARCHAR(50),
	@UserType INT
)
AS
BEGIN
	DECLARE @Error NVARCHAR(MAX),
			@TerminalID INT,
			@fk_MerchantID INT,
			@fleetId INT,
			@VehicleNumber VARCHAR(50),
			@PIN NVARCHAR(300),
			@DriverNo NVARCHAR(50),
			@DriverTaxNo NVARCHAR(50),

			--Fleet
			@fK_CurrencyCodeID INT,
			@FleetName NVARCHAR(100),
			@IsCardSwiped BIT,
			@IsContactless BIT,
			@IsChipAndPin BIT,
			@IsShowTip BIT,
			@CurrencyCode CHAR(3),

			--Merchant
			@CompanyTaxNumber NVARCHAR(20),
			@Company NVARCHAR(100),
			@TipPerLow DECIMAL(4, 2),
			@TipPerMedium DECIMAL(4, 2),
			@TipPerHigh DECIMAL(4, 2),
			@FederalTaxRate DECIMAL(4, 2),
			@StateTaxRate DECIMAL(4, 2),
			@Disclaimer NVARCHAR(MAX),
			@TermCondition NVARCHAR(MAX),
			@IsTaxInclusive BIT,

			--User
			@Address NVARCHAR(250),
			@fk_City VARCHAR(50),
			@fk_State VARCHAR(50),
			@fk_Country NVARCHAR(40),
			@Phone NVARCHAR(20),
			@fleet_MechantId INT,

			@FileVersion DECIMAL(10, 2) = 0,
			@UpdateRequired CHAR(3),
			@IsClsCap BIT,

			@TAKeyFileVersion DECIMAL(10, 2) = 0,
			@TAKeyUpdateRequired CHAR(3)

	SET @Error = ''

	IF EXISTS (SELECT 1 FROM dbo.Terminal WHERE MacAdd = @DeviceId AND IsActive = 1)
	BEGIN
		SELECT	@TerminalID = TerminalID, 
				@fk_MerchantID = fk_MerchantID, 
				@FileVersion = FileVersion,
				@IsClsCap = ISNULL(IsContactLess, 0)
		FROM dbo.Terminal 
		WHERE MacAdd = @DeviceId 
		  AND IsActive = 1

		EXEC dbo.usp_CheckFileVersion @IsClsCap = @IsClsCap, @CurrentVersion = @FileVersion, @UpdateRequired = @UpdateRequired OUT

		IF EXISTS (SELECT 1 FROM dbo.Terminal WHERE MacAdd = @DeviceId AND IsActive = 1)
		BEGIN
			SELECT	@TerminalID = TerminalID, 
					@fk_MerchantID = fk_MerchantID, 
					@TAKeyFileVersion = TAKeyFileVersion 
			FROM dbo.Terminal 
			WHERE MacAdd = @DeviceId 
			  AND IsActive = 1

			EXEC dbo.usp_CheckTAKeyFileVersion @TAKeyFileVersion, @TAKeyUpdateRequired OUT

			IF EXISTS (SELECT 1 FROM dbo.Vehicle WHERE fk_TerminalID = @TerminalID AND IsActive = 1)
			BEGIN
				SELECT	@fleetId = fk_FleetID,
						@VehicleNumber = VehicleNumber 
				FROM dbo.Vehicle 
				WHERE fk_TerminalID = @TerminalID 
				  AND IsActive = 1

				IF EXISTS (SELECT 1 FROM dbo.Driver WHERE DriverNo = @DriverId AND fk_FleetID = @fleetId AND IsActive = 1)
				BEGIN
					SELECT	@PIN = PIN, 
							@DriverNo = DriverNo, 
							@DriverTaxNo = DriverTaxNo 
					FROM dbo.Driver 
					WHERE DriverNo = @DriverId 
					  AND fk_FleetID = @fleetId 
					  AND IsActive = 1

					IF EXISTS (SELECT 1 FROM dbo.Fleet WHERE FleetID = @fleetId AND IsActive = 1)
					BEGIN
						SELECT	@FleetName = FleetName,
								@IsCardSwiped = IsCardSwiped,
								@IsContactless = @IsClsCap, --to get contactless value from terminal table (IsContactless)
								@IsChipAndPin = IsChipAndPin,
								@IsShowTip = IsShowTip,
								@fK_CurrencyCodeID = fK_CurrencyCodeID,
								@fleet_MechantId = fk_MerchantID
						FROM dbo.Fleet 
						WHERE FleetID = @fleetId 
						  AND IsActive = 1

						IF (@fleet_MechantId <> @fk_MerchantID)
						BEGIN
							SET @Error = 'Invalid driver configuration, please contact support. (1)'
						END
						ELSE
						BEGIN
							SELECT @CurrencyCode = CurrencyCode 
							FROM dbo.Currency 
							WHERE CurrencyCodeID = @fK_CurrencyCodeID	
				
							IF EXISTS (SELECT 1 FROM dbo.Merchant WHERE MerchantID = @fk_MerchantID AND IsActive = 1)
							BEGIN
								SELECT	@CompanyTaxNumber = CompanyTaxNumber,
										@Company = Company,
										@TipPerLow = TipPerLow,
										@TipPerMedium = TipPerMedium,
										@TipPerHigh = TipPerHigh,
										@FederalTaxRate = FederalTaxRate,
										@StateTaxRate = StateTaxRate,
										@Disclaimer = DisclaimerPlainText,
										@TermCondition = TermConditionPlainText,
										@IsTaxInclusive = IsTaxInclusive
								FROM dbo.Merchant 
								WHERE MerchantID = @fk_MerchantID 
								  AND IsActive = 1							

								IF EXISTS (SELECT 1 FROM dbo.Users WHERE fk_MerchantID = @fk_MerchantID AND fk_UserTypeID = @UserType AND IsActive = 1)
								BEGIN
									SELECT	@Address = [Address],
											@fk_City = fk_City,
											@fk_State = fk_State,
											@fk_Country = (SELECT TOP 1 Title FROM dbo.CountryCodes CC INNER JOIN dbo.Users US ON CC.CountryCodesID = US.fk_Country),
											@Phone = Phone
									FROM dbo.Users 
									WHERE fk_MerchantID = @fk_MerchantID 
									  AND fk_UserTypeID = @UserType 
									  AND IsActive = 1
								END
								ELSE
								BEGIN
									SET @Error = 'Invalid driver configuration, please contact support. (2)'
								END
							END
							ELSE
							BEGIN
								SET @Error = 'Invalid driver configuration, please contact support. (3)'
							END
						END		
					END
					ELSE
					BEGIN
						SET @Error = 'Invalid driver configuration, please contact support. (4)'
					END
				END
				ELSE
				BEGIN
					SET @Error = 'Invalid driver configuration, please contact support. (5)'
				END
			END
			ELSE
			BEGIN
				SET @Error = 'Invalid driver configuration, please contact support. (6)'
			END
		END
		ELSE
		BEGIN
			SET @Error = 'Invalid terminal configuration, please contact support. (1)'
		END

	END
	ELSE
	BEGIN
		SET @Error = 'Invalid terminal configuration, please contact support. (2)'
	END

	-- Output data.
	---------------
	SELECT	@Error AS 'Error',
			@TerminalID AS 'TerminalID',
			@fk_MerchantID AS 'fk_MerchantID',
			@fleetId AS 'fleetId',
			@VehicleNumber AS 'VehicleNumber',
			@PIN AS 'PIN',
			@DriverNo AS 'DriverNo',
			@DriverTaxNo AS 'DriverTaxNo',
			@FleetName AS 'FleetName',
			@IsCardSwiped AS 'IsCardSwiped',
			@IsContactless AS 'IsContactless',
			@IsChipAndPin AS 'IsChipAndPin',
			@IsShowTip AS 'IsShowTip',
			@CurrencyCode AS 'CurrencyCode',
			@CompanyTaxNumber AS 'CompanyTaxNumber',
			@Company AS 'Company',
			@TipPerLow AS 'TipPerLow',
			@TipPerMedium AS 'TipPerMedium',
			@TipPerHigh AS 'TipPerHigh',
			@FederalTaxRate AS 'FederalTaxRate',
			@StateTaxRate AS 'StateTaxRate',
			@Disclaimer AS 'DisclaimerPlainText',
			@TermCondition AS 'TermConditionPlainText',
			@IsTaxInclusive AS 'IsTaxInclusive',
			@Address AS 'Address',
			@fk_City AS 'fk_City',
			@fk_State AS 'fk_State',
			@fk_Country AS 'fk_Country',
			@Phone AS 'Phone',
			@UpdateRequired AS 'UpdateRequired',
			@TAKeyUpdateRequired AS 'TAKeyUpdateRequired'

END
GO
