IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CorporateCardTypeReport]') AND type IN (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_CorporateCardTypeReport]
GO

CREATE PROCEDURE dbo.usp_CorporateCardTypeReport
-- =============================================
-- Author:		<??>
-- Create date: <>
-- Description:	<Corporate Card Type report>
-- =============================================
-- S.Healey		2016-11-10		- Performance optimisation with use of indexes and improved queries.
(
	@IsOnDemandReport BIT,
	@FromDate DATETIME = NULL,
	@ToDate DATETIME = NULL,
	@CorpUserId INT = NULL,
	@XmlData NVARCHAR(MAX) OUT,
	@ReportType INT OUT,
	@FDate DATE OUT,
	@TDate DATE OUT,
	@TotalNoVisa INT OUT,
	@TotalNoMasterCard INT OUT,
	@TotalNoAmex INT OUT,
	@TotalNoDiscover INT OUT,
	@TotalNoJcb INT OUT,
	@TotalNoDiner INT OUT,
	@TotalVisa NVARCHAR(30) OUT,
	@TotalMasterCard NVARCHAR(30) OUT,
	@TotalAmex NVARCHAR(30) OUT,
	@TotalDiscover NVARCHAR(30) OUT,
	@TotalJcb NVARCHAR(30) OUT,
	@TotalDiner NVARCHAR(30) OUT,
	@NextScheduleDateTime DATETIME OUT
)
AS
BEGIN
	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SET NOCOUNT ON
		SET ANSI_WARNINGS OFF

		DECLARE @Frequency INT,
				@FrequencyType INT,
				@DateFrom DATE,
				@DateTo DATE,
				@ScheduleId INT,
				@ParentCorporateUserId INT

		DECLARE @InnerTable TABLE (
			Id INT, 
			Amount MONEY, 
			fk_FleetId INT, 
			ResponseCode VARCHAR(25),
			IsRefunded BIT,
			IsVoided BIT,
			CardType NVARCHAR(25),
			TxnType NVARCHAR(20)
			)

		IF EXISTS (SELECT 1 FROM dbo.Users WHERE UserID = @CorpUserId AND ParentUserID IS NULL AND IsActive = 1)
		BEGIN
			SET @ParentCorporateUserId = @CorpUserId	
		END
		ELSE
		BEGIN
			SELECT @ParentCorporateUserId = ParentUserID FROM dbo.Users WHERE ParentUserID IS NOT NULL AND IsActive = 1
		END

		SELECT  @ScheduleId = ScheduleID,
				@Frequency = Frequency,
				@FrequencyType = FrequencyType,
				@NextScheduleDateTime = NextScheduleDateTime 
		FROM dbo.ScheduleReport 
		WHERE fk_ReportId = 8 
		  AND IsCreated = 1 
		  AND fk_MerchantId IS NULL 
		  AND CorporateUserId = @ParentCorporateUserId

		IF (@IsOnDemandReport = 1)
		BEGIN
			SET @DateFrom = @FromDate
			SET @DateTo = @ToDate
		END
		ELSE
		BEGIN
			IF (@Frequency = 1)
			BEGIN
				SET @DateFrom = GETDATE() - @FrequencyType
				SET @DateTo = GETDATE()
				SET @NextScheduleDateTime = @NextScheduleDateTime + @FrequencyType 
			END
			ELSE IF (@Frequency = 2)
			BEGIN
				SET @DateFrom = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK, -@FrequencyType, GETDATE())), 0) -1
				SET @DateTo = DATEADD(WEEK, DATEDIFF(WEEK, 0, GETDATE()), 0) - 1
				SET @NextScheduleDateTime = DATEADD(WK, @FrequencyType, @NextScheduleDateTime)
			END
			ELSE IF (@Frequency = 3)
			BEGIN
				SET @DateFrom = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH, -@FrequencyType, GETDATE())), 0)
				SET @DateTo = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) 
				SET @NextScheduleDateTime = DATEADD(MONTH, @FrequencyType, @NextScheduleDateTime)   
			END   
			ELSE
			BEGIN
				RETURN 1
			END 
		END  

		--insert data into table variable InnerTable for reuse*/                         
		INSERT INTO @InnerTable(Id, CardType, Amount, fk_FleetId, TxnType, IsRefunded, IsVoided)                   
		SELECT Id, CardType, Amount, fk_FleetId, TxnType, IsRefunded, IsVoided
		FROM dbo.MTDTransaction 
		WHERE TxnDate >= @DateFrom 
		  AND TxnDate < @DateTo
		  AND ResponseCode IN ('000', '00', '002')   
		  AND fk_FleetId IN (SELECT DISTINCT fk_FleetId FROM dbo.UserFleet WHERE fk_UserTypeId = 5 AND fk_UserID = @CorpUserId AND IsActive = 1)

		SET @XmlData = CAST((SELECT ROW_NUMBER() OVER (ORDER BY trans.Company) AS 'td','',
								   COALESCE(trans.Company, '') AS 'td','',
								   COALESCE(trans.FleetName, '') AS 'td','',
								   (SELECT COALESCE(COUNT(it.Id), 0) FROM @InnerTable it WHERE it.fk_FleetId = trans.FleetID AND it.CardType = 'Visa' AND it.TxnType IN ('Sale', 'Completion')) AS 'td','',
								   (SELECT CONCAT('$',CAST(COALESCE(SUM(it.Amount), 0) AS DECIMAL(18, 2))) FROM @InnerTable it WHERE it.fk_FleetId = trans.FleetID AND it.CardType = 'Visa' AND it.TxnType IN ('Sale', 'Completion') AND it.IsRefunded = 0 AND IsVoided=0) AS 'td','',
								   (SELECT COALESCE(COUNT(it.Id), 0) FROM @InnerTable it WHERE it.fk_FleetId = trans.FleetID AND it.CardType = 'MasterCard' AND it.TxnType IN ('Sale', 'Completion')) AS 'td','',
								   (SELECT CONCAT('$',CAST(COALESCE(SUM(it.Amount), 0) AS DECIMAL(18, 2)))  FROM @InnerTable it WHERE it.fk_FleetId = trans.FleetID AND it.CardType = 'MasterCard' AND it.TxnType IN ('Sale', 'Completion') AND it.IsRefunded = 0 AND IsVoided=0) AS 'td','',
								   (SELECT COALESCE(COUNT(it.Id), 0) FROM @InnerTable it WHERE  it.fk_FleetId = trans.FleetID AND it.CardType = 'Amex' AND it.TxnType IN ('Sale', 'Completion')) AS 'td','',
								   (SELECT CONCAT('$',CAST(COALESCE(SUM(it.Amount), 0) AS DECIMAL(18, 2)))  FROM @InnerTable it WHERE it.fk_FleetId = trans.FleetID AND it.CardType = 'Amex' AND it.TxnType IN ('Sale', 'Completion') AND it.IsRefunded = 0 AND IsVoided=0) AS 'td','',
								   (SELECT COALESCE(COUNT(it.Id), 0) FROM @InnerTable it WHERE it.fk_FleetId = trans.FleetID AND it.CardType = 'Discover' AND it.TxnType IN ('Sale', 'Completion')) AS 'td','',
								   (SELECT CONCAT('$',CAST(COALESCE(SUM(it.Amount), 0) AS DECIMAL(18, 2)))  FROM @InnerTable it WHERE it.fk_FleetId = trans.FleetID AND it.CardType = 'Discover' AND it.TxnType IN ('Sale', 'Completion') AND it.IsRefunded = 0 AND IsVoided=0) AS 'td','',
								   (SELECT COALESCE(COUNT(it.Id), 0) FROM @InnerTable it WHERE it.fk_FleetId = trans.FleetID AND it.CardType = 'JCB' AND it.TxnType IN ('Sale', 'Completion')) AS 'td','',
								   (SELECT CONCAT('$',CAST(COALESCE(SUM(it.Amount), 0) AS DECIMAL(18, 2)))  FROM @InnerTable it WHERE it.fk_FleetId = trans.FleetID AND it.CardType = 'JCB' AND it.TxnType IN ('Sale', 'Completion') AND it.IsRefunded = 0 AND IsVoided=0) AS 'td','',
								   (SELECT COALESCE(COUNT(it.Id), 0) FROM @InnerTable it WHERE it.fk_FleetId = trans.FleetID AND it.CardType = 'DinersClub' AND it.TxnType IN ('Sale', 'Completion')) AS 'td','',
								   (SELECT CONCAT('$',CAST(COALESCE(SUM(it.Amount), 0) AS DECIMAL(18, 2)))  FROM @InnerTable it WHERE it.fk_FleetId = trans.FleetID AND it.CardType = 'DinersClub' AND it.TxnType IN ('Sale', 'Completion') AND it.IsRefunded = 0 AND IsVoided=0) AS 'td',''
							  
							  FROM ( SELECT mtd.FleetID, mtd.FleetName, mtd.Company, m.ResponseCode, m.TxnDate, m.CardType, m.TxnType, m.Id, m.Amount 
									FROM dbo.MTDTransaction m
										INNER JOIN ( SELECT fl.FleetID, fl.FleetName, mer.Company 
													FROM dbo.Fleet fl
														INNER JOIN ( SELECT MerchantID, Company FROM dbo.Merchant) mer ON fl.fk_MerchantID = mer.MerchantID
												) mtd ON m.fk_FleetId = mtd.FleetID
									) trans
							WHERE trans.TxnDate >= @DateFrom
							  AND trans.TxnDate < @DateTo    
							  AND trans.ResponseCode IN ('000', '00', '002')   
							  AND trans.FleetID IN (SELECT DISTINCT fk_FleetId FROM dbo.UserFleet WHERE fk_UserTypeId = 5 AND fk_UserID = @CorpUserId AND IsActive = 1)                         
							GROUP BY trans.FleetID, trans.FleetName, trans.Company 
							ORDER BY trans.Company 
							FOR XML PATH('tr'), ELEMENTS ) AS NVARCHAR(MAX))

		SET @TotalNoVisa = (SELECT COALESCE(COUNT(Id), 0) FROM @InnerTable ot WHERE ot.TxnType IN ('Sale', 'Completion') AND ot.CardType = 'Visa' GROUP BY ot.CardType)
		SET @TotalNoMasterCard = (SELECT COALESCE(COUNT(Id), 0) FROM @InnerTable ot WHERE ot.TxnType IN ('Sale', 'Completion') AND ot.CardType = 'MasterCard' GROUP BY ot.CardType)
		SET @TotalNoAmex = (SELECT COALESCE(COUNT(Id), 0) FROM @InnerTable ot WHERE ot.TxnType IN ('Sale', 'Completion') AND ot.CardType = 'Amex' GROUP BY ot.CardType)
		SET @TotalNoDiscover = (SELECT COALESCE(COUNT(Id), 0) FROM @InnerTable ot WHERE ot.TxnType IN ('Sale', 'Completion') AND ot.CardType = 'Discover' GROUP BY ot.CardType)
		SET @TotalNoJcb = (SELECT COALESCE(COUNT(Id), 0) FROM @InnerTable ot WHERE ot.TxnType IN ('Sale', 'Completion') AND ot.CardType = 'JCB' GROUP BY ot.CardType)
		SET @TotalNoDiner = (SELECT COALESCE(COUNT(Id), 0) FROM @InnerTable ot WHERE ot.TxnType IN ('Sale', 'Completion') AND ot.CardType = 'DinersClub' GROUP BY ot.CardType)	  
		SET @TotalVisa = (SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(18, 2)) FROM @InnerTable ot WHERE ot.TxnType IN ('Sale', 'Completion') AND ot.IsRefunded = 0 AND ot.IsVoided = 0 AND ot.CardType = 'Visa' GROUP BY ot.CardType)
		SET @TotalMasterCard = (SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(18, 2)) FROM @InnerTable ot WHERE ot.TxnType IN ('Sale', 'Completion') AND ot.IsRefunded = 0 AND ot.IsVoided = 0 AND ot.CardType = 'MasterCard' GROUP BY ot.CardType)
		SET @TotalAmex = (SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(18, 2)) FROM @InnerTable ot WHERE ot.TxnType IN ('Sale', 'Completion') AND ot.IsRefunded = 0 AND ot.IsVoided = 0 AND ot.CardType = 'Amex' GROUP BY ot.CardType)
		SET @TotalDiscover = (SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(18, 2)) FROM @InnerTable ot WHERE ot.TxnType IN ('Sale', 'Completion') AND ot.IsRefunded = 0 AND ot.IsVoided = 0 AND ot.CardType = 'Discover' GROUP BY ot.CardType)
		SET @TotalJcb = (SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(18, 2)) FROM @InnerTable ot WHERE ot.TxnType IN ('Sale', 'Completion') AND ot.IsRefunded = 0 AND ot.IsVoided = 0 AND ot.CardType = 'JCB' GROUP BY ot.CardType)
		SET @TotalDiner = (SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(18, 2)) FROM @InnerTable ot WHERE ot.TxnType IN ('Sale', 'Completion') AND ot.IsRefunded = 0 AND ot.IsVoided = 0 AND ot.CardType = 'DinersClub' GROUP BY ot.CardType)    

		SET @FDate = @DateFrom
		SET @TDate = DATEADD(DAY, -1, @DateTo)
		SET @ReportType = @Frequency 
	END TRY 
	BEGIN CATCH
		INSERT INTO dbo.ProcErrorHandler(ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, UserName, HostName, [TimeStamp])
		SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
	END CATCH
END
GO