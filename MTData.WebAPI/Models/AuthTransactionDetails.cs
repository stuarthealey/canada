﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    [XmlSerializerFormat]

    public class AuthTransactionDetails
    {

        private string _transactionID;

        [DataMember]
        public string TransactionID
        {
            get { return string.IsNullOrEmpty(this._transactionID) ? string.Empty : this._transactionID; }
            set { this._transactionID = value; }
        }


        private string _driverNumber;

        [DataMember]
        public string DriverNumber
        {
            get { return string.IsNullOrEmpty(this._driverNumber) ? string.Empty : this._driverNumber; }
            set { this._driverNumber = value; }
        }

        private string _transactionResponse;

        [DataMember]
        public string TransactionResponse
        {
            get { return string.IsNullOrEmpty(this._transactionResponse) ? string.Empty : this._transactionResponse; }
            set { this._transactionResponse = value; }
        }


        private string _cardFirstFourDigits;

        [DataMember]
        public string CardFirstFourDigits
        {
            get { return string.IsNullOrEmpty(this._cardFirstFourDigits) ? string.Empty : this._cardFirstFourDigits; }
            set { this._cardFirstFourDigits = value; }
        }

        private string _cardLastFourDigits;

        [DataMember]
        public string CardLastFourDigits
        {
            get { return string.IsNullOrEmpty(this._cardLastFourDigits) ? string.Empty : this._cardLastFourDigits; }
            set { this._cardLastFourDigits = value; }
        }


        private string _vehicleNo;

        [DataMember]
        public string VehicleNo
        {
            get { return string.IsNullOrEmpty(this._vehicleNo) ? string.Empty : this._vehicleNo; }
            set { this._vehicleNo = value; }
        }


        private string _amount;

        [DataMember]
        public string Amount
        {
            get { return string.IsNullOrEmpty(this._amount) ? string.Empty : this._amount; }
            set { this._amount = value; }
        }

        private string _authID;

        [DataMember]
        public string AuthID
        {
            get { return string.IsNullOrEmpty(this._authID) ? string.Empty : this._authID; }
            set { this._authID = value; }
        }

        private string _entryMode;

        [DataMember]
        public string EntryMode
        {
            get { return string.IsNullOrEmpty(this._entryMode) ? string.Empty : this._entryMode; }
            set { this._entryMode = value; }
        }


        private string _transactionType;

        [DataMember]
        public string TransactionType
        {
            get { return string.IsNullOrEmpty(this._transactionType) ? string.Empty : this._transactionType; }
            set { this._transactionType = value; }
        }


        private string _paymentType;

        [DataMember]
        public string PaymentType
        {
            get { return string.IsNullOrEmpty(this._paymentType) ? string.Empty : this._paymentType; }
            set { this._paymentType = value; }
        }


        private string _transactionDate;

        [DataMember]
        public string TransactionDate
        {
            get { return string.IsNullOrEmpty(this._transactionDate) ? string.Empty : this._transactionDate; }
            set { this._transactionDate = value; }
        }



        private string _cardType;

        [DataMember]
        public string CardType
        {
            get { return string.IsNullOrEmpty(this._cardType) ? string.Empty : this._cardType; }
            set { this._cardType = value; }
        }


       

     
    }
}