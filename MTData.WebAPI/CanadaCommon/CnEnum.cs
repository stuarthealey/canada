﻿using System.Runtime.Serialization;

namespace MTData.WebAPI.CanadaCommon
{
    public enum DataType
    {
        Numeric,
        Alphanumeric,
        None,
        Binary
    }

    public enum FormatType
    {
        BCD,
        ASCII,
        HD,
        HB
    }

    public enum LengthType
    {
        Fixed,
        Variable
    }

    public enum IndustryType
    {
        None = 0,
        Ecommerce = 1,
        Retail = 2,
        EMV = 3,
        BookerAppToken = 4,
        BookerAppPayment = 5,
        KeyUpdateRequest = 6,
        CAPKDownload = 7,
        CAPKUpdate = 8,
        NewKeyRequest = 9
    }

    public enum CardIssuer
    {
        None,
        Visa,
        Discover,
        Amex,
        JCB,
        MasterCard,
        DCI,
        Diners,
        NonePinOnly,
        Interac
    }

    public enum CardType
    {
        [EnumMember]
        None = 0,
        [EnumMember]
        Credit = 1,
        [EnumMember]
        Debit = 2      
    }

    public enum CurrencyCode
    {
        US,
        CAD
    }

    public enum TransactionProcessed
    {
        [EnumMember]
        None = 0,
        [EnumMember]
        Sale = 1,
        [EnumMember]
        Void = 2,
        [EnumMember]
        Refund = 3,
        [EnumMember]
        BillPayment = 4,
        [EnumMember]
        Referral = 5,
        [EnumMember]
        PartialReversal = 6,
        [EnumMember]
        PartialRefund = 7,
        [EnumMember]
        Authorization = 8,
        [EnumMember]
        Completion = 9
    }

    /// <summary>
    /// Describes different bill payment types.
    /// </summary>
    public enum BillPaymentType
    {
        [EnumMember]
        None = 0,
        [EnumMember]
        Single = 1,
        [EnumMember]
        Recurring = 2,
        [EnumMember]
        Installment = 3,
        [EnumMember]
        Deferred = 4
    }

    /// <summary>
    /// Describes different bill payment modes.
    /// </summary>
    public enum BillPaymentMode
    {
        [EnumMember]
        None = 0,
        [EnumMember]
        Initial = 1,
        [EnumMember]
        Subsequent = 2
    }

    public enum TransactionType
    {
        [EnumMember]
        None = 0,
        [EnumMember]
        Sale = 1,
        [EnumMember]
        Void = 2,
        [EnumMember]
        Refund = 3,
        [EnumMember]
        Reversal = 4,
        [EnumMember]
        PartialReversal = 5,
        [EnumMember]
        PartialRefund = 6,
        [EnumMember]
        Authorization = 7,
        [EnumMember]
        Completion = 8,
        [EnumMember]
        TimeOut = 9
    }

    public enum ReversalType
    {
        Void = 1,
        Refund = 3,
        None = 0
    }

    public enum EntryMode
    {
        Keyed,
        Swiped,
        FSwiped,
        EmvContact,
        EmvContactless,
        MSRContactless
    }

    public enum MessageType
    {
        CreditSale,
        CreditAuthorization,
        CreditCompletion,
        CreditRefund,
        CreditVoid,
        DebitSale,
        DebitAuthorization,
        DebitCompletion,
        DebitRefund,
        DebitVoid,
        CreditReversal,
        DebitReversal,
        CreditPartialReversal,
        CreditSubsequentRecurring,
        CreditSubsequentInstallment,
        CreditInitialRecurring,
        CreditInitialInstallment,
        CreditTokenOnly,
        KeyUpdateRequest,
        CreditEMVReversal,
        CreditFallbackSale,
        CreditFallbackReversal,
        CapKeyDownload,
        NewKeyRequest
    }

    public enum RequestType
    {
        Android,
        Dispatch
    }

    public enum AccountType
    {
        Checking,
        Saving
    }
}