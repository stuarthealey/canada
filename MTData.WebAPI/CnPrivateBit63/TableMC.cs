﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class TableMC
    {
        #region Private Constants and Variables
        #region Constants
        private const string tblID = "|4D|43";
        #endregion
        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'4D 43' in Request
        /// </summary>
        public string TableID
        {
            get { return tblID; }
        }
        #endregion
    }
}