IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (7))
	INSERT INTO dbo.Report VALUES ('Total Transactions By Vehicle','Total Transactions By Vehicle',0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (8))
	INSERT INTO dbo.Report VALUES ('Card Type Report','Card Type Report',0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (9))
	INSERT INTO dbo.Report VALUES ('Admin Summary Report','Admin Summary Report', 0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (10))
	INSERT INTO dbo.Report VALUES ('Admin Exception Report','Admin Exception Report', 0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (11))
	INSERT INTO dbo.Report VALUES ('Admin Card Type Report','Admin Card Type Report', 0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (12))
	INSERT INTO dbo.Report VALUES ('Admin Total Transactions By Vehicle Report','Admin Total Transactions By Vehicle Report', 0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (13))
	INSERT INTO dbo.Report VALUES ('Merchant Summary Report','Merchant Summary Report', 0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (14))
	INSERT INTO dbo.Report VALUES ('Merchant Detail Report','Merchant Detail Report', 0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (15))
	INSERT INTO dbo.Report VALUES ('Merchant Exception Report','Merchant Exception Report', 0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (16))
	INSERT INTO dbo.Report VALUES ('Merchant Total Transactions By Vehicle Report','Merchant Total Transactions By Vehicle Report', 0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (17))
	INSERT INTO dbo.Report VALUES ('Merchant Card Type Report','Merchant Card Type Report',0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (18))
	INSERT INTO dbo.Report VALUES ('Merchant User Card Type Report','Merchant User Card Type Report',0)
GO

IF EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (18))
	DELETE FROM dbo.Report WHERE ReportID IN (18)
Go

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (19))
	INSERT INTO dbo.Report VALUES ('Merchant User Card Type Report','Merchant User Card Type Report',0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (20))
	INSERT INTO dbo.Report VALUES ('Merchant User Summary Report','Merchant User Summary Report',0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (21))
	INSERT INTO dbo.Report VALUES ('Merchant User Detail Report','Merchant User Detail Report',0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (22))
	INSERT INTO dbo.Report VALUES ('Merchant User Exception Report','Merchant User Exception Report',0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (23))
	INSERT INTO dbo.Report VALUES ('Merchant User Total Transactions By Vehicle Report','Merchant User Total Transactions By Vehicle Report',0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (24))
	INSERT INTO dbo.Report VALUES ('Exception Report','Exception Report',0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (25))
	INSERT INTO dbo.Report VALUES ('Card Type Report','Card Type Report',0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE ReportID IN (26))
	INSERT INTO dbo.Report VALUES ('Total Transactions By Vehicle Report','Total Transactions By Vehicle Report',0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE Name='AdminWeeklySummaryCount')
	INSERT INTO dbo.Report VALUES ('AdminWeeklySummaryCount','AdminWeeklySummaryCount',0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE Name='AdminWeeklyDollarVolume')
	INSERT INTO dbo.Report VALUES ('AdminWeeklyDollarVolume','AdminWeeklyDollarVolume',0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE Name='MonthlyGraphCountReport')
	INSERT INTO dbo.Report VALUES ('MonthlyGraphCountReport','MonthlyGraphCountReport',0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE Name='MonthlyGraphDollarVolumeReport')
	INSERT INTO dbo.Report VALUES ('MonthlyGraphDollarVolumeReport','MonthlyGraphDollarVolumeReport',0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE Name='LineGraphCountVolumeReport')
	INSERT INTO dbo.Report VALUES ('LineGraphCountVolumeReport','LineGraphCountVolumeReport',0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.Report WHERE Name='LineGraphDollarVolumeReport')
	INSERT INTO dbo.Report VALUES ('LineGraphDollarVolumeReport','LineGraphDollarVolumeReport',0)
GO
