﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MTData.Web.ViewData
{
    public class TaxViewData
    {
        public int MerchantID { get; set; }
        public Nullable<int> fk_UserID { get; set; }
        [Required(ErrorMessage = "Please enter the state tax rate")]
        [Display(Name = "State tax rate")]
        public Nullable<decimal> StateTaxRate { get; set; }
        [Required(ErrorMessage = "Please enter the federal tax rate")]
        [Display(Name = "Federal Tax Rate")]
        public Nullable<decimal> FederalTaxRate { get; set; }
        public IEnumerable<MerchantListViewData> MerchantList { get; set; }
        [Display(Name = "Merchant")]
        public string Company { get; set; }
        public string MerchantName { get; set; }
        public bool IsTaxInclusive { get; set; }
        public string DisclaimerPlainText { get; set; }
        public string TermConditionPlainText { get; set; }

        public bool UseTransArmor { get; set; }     //PAY-13
    }
}