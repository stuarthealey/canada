﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table54
    {
        #region Private Constants and Variables
        #region Constants
        private const string tblId = "|35|34";
        #endregion
        #region Variables
        private string defaultIndicator = "1";//1 Indicates that Deferred Billing was used at the Point of Service.
        #endregion
        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'35 34' in Request
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        /// <summary>
        /// Indicator indicates that Deferred Billing was used at the Point of Service
        /// </summary>
        public string Indicator
        {
            get { return defaultIndicator; }
            set { defaultIndicator = value; }
        }
        #endregion
    }
}