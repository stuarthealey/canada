﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Data.Data
{
    public class ServiceProviderDbData
    {
        public int HostId { get; set; }
        public string Url { get; set; }
        public bool IsSetActive { get; set; }
        public bool IsServiceDiscovery { get; set; }
        public double TransactionTime { get; set; }
    }

}
