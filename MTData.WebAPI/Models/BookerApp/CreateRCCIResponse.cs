﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MTData.WebAPI.Models
{
	public class CreateRCCIResp
	{
		public ResultStatus Result { get; set; }

		public ValueNode RCCI { get; set; }

		public CreditCardStatus CreditCardStatus { get; set; }

		public TruncatedCreditCardDetails TruncatedCreditCardDetails { get; set; }
	}

	public class CreditCardStatus
	{
		public bool IsValid { get; set; }

		public string StatusDescription { get; set; }
	}

    public partial class TruncatedCreditCardDetails
	{
		public string TruncatedPAN { get; set; }

		public string ExpiryMonth { get; set; }

		public string ExpiryYear { get; set; }

		public string CardType { get; set; }
	}

	public enum ResultCode
	{
		Success,
		InvalidRCCI,
		InvalidCreditCard,
		Error,
	}

    public partial class ResultStatus
	{
		public ResultCode Code { get; set; }

		public string ErrorDescription { get; set; }
	}
}

