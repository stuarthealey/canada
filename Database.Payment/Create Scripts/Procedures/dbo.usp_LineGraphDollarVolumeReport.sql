IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LineGraphDollarVolumeReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_LineGraphDollarVolumeReport]
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <05-07-2016>
-- Description:	<To get the data for line graph dollar volume report>
-- =============================================
CREATE PROCEDURE [dbo].[usp_LineGraphDollarVolumeReport]
(
	@UserId INT
)
AS
BEGIN TRY
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
	SET ANSI_WARNINGS OFF

	DECLARE @DateFromThisWeek DATETIME,
			@DateToThisWeek DATETIME,
			@DateFromWeek1 DATETIME,
			@DateToWeek1 DATETIME,
			@DateFromWeek2 DATETIME,
			@DateToWeek2 DATETIME,
			@DateFromWeek3 DATETIME,
			@DateToWeek3 DATETIME,
			@DateFromWeek4 DATETIME,
			@DateToWeek4 DATETIME,
			@DateFromWeek5 DATETIME,
			@DateToWeek5 DATETIME,
			@DateFromWeek6 DATETIME,
			@DateToWeek6 DATETIME,
			@DateFromWeek7 DATETIME,
			@DateToWeek7 DATETIME,
			@DateFromWeek8 DATETIME,
			@DateToWeek8 DATETIME,
			@DateFromWeek9 DATETIME,
			@DateToWeek9 DATETIME,
			@DateFromWeek10 DATETIME,
			@DateToWeek10 DATETIME,
			@DateFromWeek11 DATETIME,
			@DateToWeek11 DATETIME,
			@DateFromWeek12 DATETIME,
			@DateToWeek12 DATETIME,
			@UserType INT,
			@MerchantId INT

  SELECT  @DateFromThisWeek = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,0,GETDATE())), 0) - 1,
          @DateToThisWeek = GETDATE(),
          @DateFromWeek1 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK, - 1,GETDATE())), 0) - 1,
          @DateToWeek1 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK, - 1,GETDATE())), 7) - 1,
          @DateFromWeek2 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-2,GETDATE())), 0) - 1,
          @DateToWeek2 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-2,GETDATE())), 7) - 1,
          @DateFromWeek3 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-3,GETDATE())), 0) - 1,
          @DateToWeek3 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-3,GETDATE())), 7) - 1,
          @DateFromWeek4 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-4,GETDATE())), 0) - 1,
          @DateToWeek4 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-4,GETDATE())), 7) - 1,
          @DateFromWeek5 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-5,GETDATE())), 0) - 1,
          @DateToWeek5 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-5,GETDATE())), 7) - 1,
		  @DateFromWeek6 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-6,GETDATE())), 0) - 1,
          @DateToWeek6 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-6,GETDATE())), 7) - 1,
		  @DateFromWeek7 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-7,GETDATE())), 0) - 1,
          @DateToWeek7 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-7,GETDATE())), 7) - 1,
		  @DateFromWeek8 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-8,GETDATE())), 0) - 1,
          @DateToWeek8 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-8 ,GETDATE())), 7) - 1,
		  @DateFromWeek9 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-9,GETDATE())), 0) - 1,
          @DateToWeek9 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-9,GETDATE())), 7) - 1,
		  @DateFromWeek10 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-10,GETDATE())), 0) - 1,
          @DateToWeek10 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-10,GETDATE())), 7) - 1,
		  @DateFromWeek11 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-11,GETDATE())), 0) - 1,
          @DateToWeek11 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-11,GETDATE())), 7) - 1,
		  @DateFromWeek12 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-12,GETDATE())), 0) - 1,
          @DateToWeek12 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-12,GETDATE())), 7) - 1

	DECLARE @DataTable TABLE (
		Id INT,
		FleetId INT,
		Txndate DATETIME,
		Amount DECIMAL(18,2)
		)

   DECLARE @FleetList TABLE (FleetId INT)

   SELECT @UserType = fk_UserTypeId, @MerchantId = fk_MerchantId FROM dbo.Users WHERE UserId = @UserId
   
	IF (@UserType = 2)
	BEGIN
		INSERT INTO @FleetList
		SELECT FleetID FROM dbo.Fleet WHERE IsActive = 1 AND fk_MerchantId = @MerchantId
	END
	ELSE
	BEGIN
		INSERT INTO @FleetList
		SELECT DISTINCT fk_FleetID FROM dbo.UserFleet WHERE fk_UserID = @UserId AND IsActive = 1
	END

	INSERT INTO @DataTable
	SELECT Id, fk_FleetId, TxnDate, Amount FROM dbo.MTDTransaction WHERE ResponseCode IN ('00', '000', '002') AND TxnType IN ('Sale', 'Completion') AND IsRefunded = 0 AND IsVoided = 0 AND fk_FleetId IN (SELECT FleetId FROM @FleetList)

	SELECT  ROW_NUMBER() OVER (ORDER BY f.FleetName) AS 'Sr.No',
			COALESCE(f.FleetName,'')  AS 'FleetName',
			(SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromThisWeek AND Txndate < @DateToThisWeek) AS 'TransAmtThisWeek',
			(SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromWeek1 AND Txndate < @DateToWeek1) AS 'TransAmtOneWeekAgo',
			(SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromWeek2 AND Txndate < @DateToWeek2) AS 'TransAmtTwoWeeksAgo',
			(SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromWeek3 AND Txndate < @DateToWeek3) AS 'TransAmtThreeWeeksAgo',
			(SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromWeek4 AND Txndate < @DateToWeek4) AS 'TransAmtFourWeeksAgo',
			(SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromWeek5 AND Txndate < @DateToWeek5) AS 'TransAmtFiveWeeksAgo',
			(SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromWeek6 AND Txndate < @DateToWeek6) AS 'TransAmtSixWeeksAgo',
			(SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromWeek7 AND Txndate < @DateToWeek7) AS 'TransAmtSevenWeeksAgo',
			(SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromWeek8 AND Txndate < @DateToWeek8) AS 'TransAmtEightWeeksAgo',
			(SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromWeek9 AND Txndate < @DateToWeek9) AS 'TransAmtNineWeeksAgo',
			(SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromWeek10 AND Txndate < @DateToWeek10) AS 'TransAmtTenWeeksAgo',
			(SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromWeek11 AND Txndate < @DateToWeek11) AS 'TransAmtElevenWeeksAgo',	
			(SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromWeek12 AND Txndate < @DateToWeek12) AS 'TransAmtTwelveWeeksAgo'
    FROM dbo.MTDTransaction m 
		INNER JOIN dbo.Fleet f ON m.fk_FleetId = f.FleetID
	WHERE m.fk_FleetId IN (SELECT FleetId FROM @FleetList)
	  AND m.ResponseCode IN ('00', '000', '002') AND m.TxnType IN ('Sale', 'Completion') AND m.IsRefunded = 0 AND m.IsVoided = 0
	  AND TxnDate >= @DateFromWeek12 
	  AND TxnDate < @DateToThisWeek
    GROUP BY f.FleetName, f.FleetID
	ORDER BY f.FleetName
END TRY
--use catch to log error with details in ProcErrorHandler table
BEGIN CATCH
	INSERT INTO ProcErrorHandler(ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, UserName, HostName, [TimeStamp])
	SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
END CATCH
GO