IF OBJECTPROPERTY(OBJECT_ID('DeleteTerminal'), 'IsTrigger') = 1
	DROP TRIGGER DeleteTerminal
GO

CREATE TRIGGER [dbo].[DeleteTerminal] ON  [dbo].[Merchant] 
	FOR UPDATE
AS
	DECLARE @merchantId INT
	DECLARE @isDeleted BIT
	BEGIN
		IF UPDATE(IsActive)
			set @merchantId = (SELECT MerchantID FROM Inserted i)

		SET @isDeleted = (SELECT IsActive FROM Inserted i)
		
		IF (@isDeleted = 0)
			UPDATE Terminal SET IsActive=0 WHERE fk_MerchantID=@merchantId
	END
	RETURN
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Merchant' AND COLUMN_NAME = 'DisclaimerPlainText')
	ALTER TABLE Merchant ADD  DisclaimerPlainText NVARCHAR(max) null
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Merchant' AND COLUMN_NAME = 'TermConditionPlainText')
	ALTER TABLE Merchant ADD  TermConditionPlainText NVARCHAR(max) null
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Merchant' AND COLUMN_NAME = 'IsJobNoRequired')
	ALTER TABLE Merchant ADD IsJobNoRequired BIT NULL
GO
