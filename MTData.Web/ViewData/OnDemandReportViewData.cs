﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using MTData.Web.Resource;

namespace MTData.Web.ViewData
{
    public class OnDemandReportViewData
    {
        [Required(ErrorMessageResourceType = typeof(MtdataResource), ErrorMessageResourceName = "Please_enter_date_from")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime DateFrom { get; set; }

        [Required(ErrorMessageResourceType = typeof(MtdataResource), ErrorMessageResourceName = "Please_enter_date_to")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime? DateTo { get; set; }
        
        public int? fk_MerchantId { get; set; }
        public bool IsReport { get; set; }
        
        [Required(ErrorMessageResourceType = typeof(MtdataResource), ErrorMessageResourceName = "Please_select_a_report_name")]
        public Nullable<int> fk_ReportID { get; set; }
        
        public IEnumerable<ReportListViewData> ReportList { get; set; }
        public IEnumerable<MerchantListViewData> MerchantList { get; set; }

        public IEnumerable<GetCardTypeReportViewData> GetCardTypeReport { get; set; }
        public IEnumerable<GetAdminSummaryReportViewData> GetAdminSummaryReport { get; set; }

        public IEnumerable<GetAdminExceptionReportViewData> GetAdminExceptionReport { get; set; }
        public IEnumerable<GetTotalTransByVehicleReportViewData> AdminTotalTransactionsByVehicle { get; set; }

        public IEnumerable<GetMerchantSummaryReportViewData> MerchantSummaryReport { get; set; }
        public IEnumerable<GetDetailReportViewData> DetailReport { get; set; }

        public IEnumerable<GetMerchantExceptionReportViewData> MerchantExceptionReport { get; set; }
        public IEnumerable<GetMerchantTotalTransByVehicleReportViewData> MerchantTotalTransReport { get; set; }

        public IEnumerable<GetMerchantCardTypeReportViewData> GetMerchantCardTypeReport { get; set; }
    }
}