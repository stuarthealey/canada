﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
    public class WeeklyDollarVolumeDto
    {
        public Nullable<long> Sr_No { get; set; }
        public string FleetName { get; set; }
        public string TransAmtThisWeek { get; set; }
        public string TransAmtWeekOne { get; set; }
        public string TransAmtWeekTwo { get; set; }
        public string TransAmtWeekThree { get; set; }
        public string TransAmtWeekFour { get; set; }
        public string TransAmtWeekFive { get; set; }
        public string TransAmtWeekSix { get; set; }
    }
}
