﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MTD.Core.Service.Interface;
using MTData.Web.ViewData;
using Ninject;
using System.Text;
using System.Reflection;
using System.Globalization;
using MTData.Utility;
using System.Text.RegularExpressions;

namespace MTData.Web.Controllers
{
    public class CountryStateController : Controller
    {
        readonly ILogger _logger = new Logger();
        private readonly ICommonService _commonService;
        private StringBuilder _logMessage;

        public CountryStateController([Named("CommonService")] ICommonService commonService, StringBuilder logMessage)
        {
            _commonService = commonService;
            _logMessage = logMessage;
        }
        /// <summary>
        /// Purpose             :   To get state list
        /// Function Name       :   GetStateList
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   4/23/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="mtch"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetStateList(string countryId, string mtch)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                            MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetStateList method of _commonService class to get state list for Australia");
                var stateDtoList = _commonService.GetStateList(countryId, mtch);
                _logMessage.Append("GetStateList method of _commonService executed successfully");
                _logMessage.Append("converting ToStateViewDataList list into stateList");
                var stateList = stateDtoList.ToStateViewDataList().OrderBy(s => s.Name).ToList();
                _logMessage.Append("convert stateDtoList into stateList successful");
                _logMessage.Append("returning state list");
                List<string> list = new List<string>();
                foreach (StateViewData str in stateList)
                {
                    list.Add(str.Name);
                }
                var states = list.Take(10).Distinct();
                return Json(states, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :   To get city list
        /// Function Name       :   GetCityList
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   4/25/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="mtch"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetCityList(string mtch)
        {
            try
           {
               if (!Regex.IsMatch(mtch, @"[A-Za-z0-9\-]+"))
               {
                   throw new ArgumentException("Invalid format.");
               }
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                              MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetCountryList method of _commonService class to get countries");
                _logMessage.Append("Calling GetCityList method of _commonService class to get city list for state");
                var cityDtoList = _commonService.GetCityList(mtch);
                _logMessage.Append("GetCityList method of _commonService executed successfully");
                _logMessage.Append("converting cityDtoList list into stateList");
                var cityViewDataList = cityDtoList.ToCityViewDataList().OrderBy(s => s.CityName).ToList();
                _logMessage.Append("convert ToCityViewDataList into cityViewDataList successful");
                _logMessage.Append("returning city list");
                List<string> list = new List<string>();
                foreach (CityViewData city in cityViewDataList)
                {
                    list.Add(city.CityName);
                }
                var cities = list.Take(10).Distinct();
                return Json(cities, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetCarsDrivers(string id)
        {
            if (String.IsNullOrEmpty(id)) return Json(null, JsonRequestBehavior.AllowGet);
            CarDriverViewData ob = new CarDriverViewData();
            ob.CarList = _commonService.GetCars(Convert.ToInt32(id)).ToViewDataList();
            ob.DriverList = _commonService.GetDrivers(Convert.ToInt32(id)).ToViewDataList();
            return Json(ob, JsonRequestBehavior.AllowGet);
        }

    }
}