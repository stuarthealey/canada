﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MTData.Web.ViewData
{
    public class FleetViewData
    {
      
        public int FleetID { get; set; }
        public int fk_MerchantID { get; set; }
        [StringLength(40, ErrorMessage = "Up to 40 characters are allowed")]
        [Required(ErrorMessage = "Please enter the fleet's name")]
        [RegularExpression(@"^[A-Za-z 0-9]+$", ErrorMessage = "Fleet name  is not valid")]
        [Display(Name = "Fleet name")]
        public string FleetName { get; set; }
        [StringLength(240)]
        [Required(ErrorMessage = "Please enter the fleet's description")]
        [Display(Name = "Fleet description")]
        public string Description { get; set; }
        public string CurrencyCode { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<decimal> FleetTransaction { get; set; }
        public Nullable<decimal> CarTransaction { get; set; }
        public Nullable<decimal> TransactionValue { get; set; }
        public Nullable<int> fK_CurrencyCodeID { get; set; }
        public string currencyWithSymbol { get; set; }
        public IEnumerable<CurrencyViewData> currencyList { get; set; }
        public Nullable<decimal> FleetFee { get; set; }
        public bool IsShowTip { get; set; }
        [Required(ErrorMessage = "Please select pay type")]
        public Nullable<byte> PayType { get; set; }
        public IEnumerable<FleetViewData> fleetlist { get; set; }
        public Nullable<byte> TechFeeType { get; set; }
        public Nullable<decimal> TechFeeFixed { get; set; }
        public Nullable<decimal> TechFeePer { get; set; }
        public Nullable<decimal> TechFeeMaxCap { get; set; }

        public Nullable<byte> FleetFeeType { get; set; }
        public Nullable<decimal> FleetFeeFixed { get; set; }
        public Nullable<decimal> FleetFeePer { get; set; }
        public Nullable<decimal> FleetFeeMaxCap { get; set; }

        public Nullable<int> DispatchFleetID { get; set; }
        public string BookingChannel { get; set; }
        public string UserName { get; set; }
        public string PCode { get; set; }
        [Url(ErrorMessage = "Please enter valid url")]
        public string URL { get; set; }
        public string Token { get; set; }
    }
}