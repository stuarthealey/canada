IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Schedule]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_Schedule]
GO

CREATE PROCEDURE dbo.usp_Schedule
-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <01-05-2015>
-- Description:	<Update the SysSchedules table to schedule the reports>
-- =============================================
(
   @schedule_id INT= NULL,
   @name NVARCHAR(128) = NULL,
   @new_name NVARCHAR(128) = NULL,
   @enabled TINYINT = NULL,
   @freq_type INT = NULL,
   @freq_interval INT = NULL,
   @freq_subday_type INT = NULL,
   @freq_subday_interval INT = NULL,
   @freq_relative_interval INT = NULL,
   @freq_recurrence_factor INT = NULL,
   @active_start_date INT = NULL,
   @active_end_date INT = NULL,
   @active_start_time INT = NULL,
   @active_end_time INT = NULL,
   @owner_login_name NVARCHAR(128) = NULL,
   @automatic_post BIT = 1
)
AS 
BEGIN
	DECLARE @res INT

	EXEC msdb.dbo.sp_update_schedule @schedule_id = @schedule_id,
					@name = @name,
					@new_name = @new_name,
					@enabled = @enabled,
					@freq_type = @freq_type,
					@freq_interval = @freq_interval,
					@freq_subday_type = @freq_subday_type,
					@freq_subday_interval = @freq_subday_interval,
					@freq_relative_interval = @freq_relative_interval,
					@freq_recurrence_factor = @freq_recurrence_factor,
					@active_start_date = @active_start_date,
					@active_end_date = @active_end_date,
					@active_start_time = @active_start_time,
					@active_end_time = @active_end_time,
					@owner_login_name = @owner_login_name,
					@automatic_post = @automatic_post

	SELECT @res
END
GO
