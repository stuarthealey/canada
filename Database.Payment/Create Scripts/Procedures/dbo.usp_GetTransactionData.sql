IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetTransactionData]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_GetTransactionData]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.usp_GetTransactionData
--
--	Get Transaction data
--
--	Written by:     Chetu
--	Date            Mar-2016
--	Change Control
--	======================================================--
--	S.Healey	2016-09-08	- D#5517 Performance improvements and adding indexes.
--	S.Healey	2016-12-14	- Added SET QUOTED_IDENTIFIER ON option in stored procedure script to ensure execution works.
--	S.Healey	2017-03-09	-T#6724 Performance improvements.
--
(	
	@fk_MerchantID INT,
	@DateFrom DATETIME,
	@DateTo DATETIME = NULL,
	@TransType NVARCHAR(20) = NULL, 
	@DownloadStatus CHAR(3) = NULL,
	@VehicleId VARCHAR(50) = NULL,
	@DriverId NVARCHAR(50) = NULL,
	@fk_fleetId INT = NULL,
	@EntryMode NVARCHAR(50) = NULL,
	@BatchNumber INT = NULL,
	@RetrieveSinceBatch BIT = 0
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON

	DECLARE @Sale VARCHAR(50) = NULL, 
			@Completion VARCHAR(50) = NULL, 
			@Refund VARCHAR(50) = NULL, 
			@IsDownloaded CHAR(1),
			@swipe VARCHAR(50) = NULL, 
			@fswipe VARCHAR(50) = NULL,
			@CurrentBatchNumber INT

	---Managing transaction type
	IF (UPPER(@TransType) = 'APPROVED')
	BEGIN
		SET @Sale = 'Sale'
		SET @Completion = 'Completion'
	END
	ELSE IF (UPPER(@TransType) = 'REFUND')
	BEGIN
		SET @Refund = 'Refund'
	END
	ELSE IF (UPPER(@TransType) = 'ALL' OR @TransType = '' OR @TransType IS NULL)
	BEGIN
		SET @Sale = 'Sale'
		SET @Completion = 'Completion'
		SET @Refund = 'Refund'
	END

	---Managing EntryMode
	IF (UPPER(@EntryMode) = 'SWIPE')
	BEGIN
		SET @swipe = 'swiped'
		SET @fswipe = 'fswiped'
	END
	
	-- Managing DownloadStatus
	SELECT @IsDownloaded = CASE
								WHEN UPPER(@DownloadStatus) = 'NEW' THEN '0'
								WHEN UPPER(@DownloadStatus) = 'OLD' THEN '1'
								WHEN UPPER(@DownloadStatus) = 'ALL' OR @DownloadStatus = '' OR @DownloadStatus IS NULL THEN NULL
							END

	PRINT '@IsDownloaded = "' + ISNULL(@IsDownloaded, '-') + '"'

	-- Preparing @DateTo if last date not supplied
	SET @DateTo = ISNULL(@DateTo, CAST(GETDATE() AS DATETIME))

	-- Preparing next BatchNumber to update
	SELECT @CurrentBatchNumber = MAX(BatchNumber) FROM dbo.MTDTransaction
	SET @CurrentBatchNumber = ISNULL(@CurrentBatchNumber, 0) + 1

	--T#6724 Check if any additional filter criteria have been provided. 
	-- If not then we can do a minimum record search.
	DECLARE @MinimumChecks BIT
	SET @MinimumChecks = 1

	IF (ISNULL(@VehicleId, '') <> ''
		OR ISNULL(@DriverId, '') <> ''
		OR @DateFrom IS NOT NULL
		OR ISNULL(@fk_FleetID, 0) <> 0)
	BEGIN
		SET @MinimumChecks = 0
	END

	PRINT 'MinimumChecks = "' + CAST(@MinimumChecks AS NVARCHAR) + '"'

	IF (@BatchNumber IS NULL)
	BEGIN
		IF (UPPER(@DownloadStatus) = 'NEW')
		BEGIN
			SELECT	ID, TerminalID, DriverNo, VehicleNo, JobNumber, CrdHldrName, CrdHldrPhone, CrdHldrCity, CrdHldrState, CrdHldrZip,
					CrdHldrAddress, TransRefNo, PaymentType, TxnType, FareValue, Tip, Surcharge, Fee, Taxes, Toll, Amount, ExpiryDate, CardType,
					Industry, EntryMode, ResponseCode, AddRespData, AuthId, SourceId, TransNote, IsCompleted, IsRefunded, PickAddress,
					DestinationAddress, TxnDate, FirstFourDigits, LastFourDigits, FlagFall, Extras, GatewayTxnId, fk_FleetId, RequestId,
					IsVoided, IsDownloaded, TransSeqNo, BatchNumber
			FROM dbo.MTDTransaction
			WHERE MerchantId = @fk_MerchantID
			  AND ResponseCode IN ('000', '00', '002') 
			  AND (VehicleNo = @VehicleId OR COALESCE(@VehicleId,'') = '')
			  AND (DriverNo = @DriverId OR COALESCE(@DriverId,'') = '')
			  AND (TxnDate >= @DateFrom OR @DateFrom IS NULL)
			  AND (TxnDate <= @DateTo)	-- This is ALWAYS set above, so no need to check NULL
			  AND (TxnType = @Sale OR TxnType = @Completion OR TxnType = @Refund)
			  AND (fk_FleetId = @fk_fleetId OR COALESCE(@fk_fleetId, 0) = 0)
			  AND (EntryMode = @swipe OR EntryMode = @fswipe OR EntryMode = @EntryMode OR COALESCE(@EntryMode,'') = '')
			  AND (IsDownloaded = @IsDownloaded OR @IsDownloaded IS NULL)

			UPDATE dbo.MTDTransaction 
			SET IsDownloaded = 1, 
				BatchNumber = @CurrentBatchNumber
			WHERE MerchantId = @fk_MerchantID
			  AND ResponseCode IN ('000', '00', '002') 
			  AND (VehicleNo = @VehicleId OR COALESCE(@VehicleId, '') = '')
			  AND (DriverNo = @DriverId OR COALESCE(@DriverId, '') = '')
			  AND (TxnDate >= @DateFrom OR @DateFrom IS NULL)
			  AND (TxnDate <= @DateTo)	-- This is ALWAYS set above, so no need to check NULL
			  AND (TxnType = @Sale OR TxnType = @Completion OR TxnType = @Refund)
			  AND (fk_FleetId = @fk_fleetId OR COALESCE(@fk_fleetId, 0) = 0)
			  AND (EntryMode = @swipe OR EntryMode = @fswipe OR EntryMode = @EntryMode OR COALESCE(@EntryMode,'') = '')
			  AND (IsDownloaded = @IsDownloaded OR @IsDownloaded IS NULL)
		END
		ELSE
		BEGIN
			SELECT	ID, TerminalID, DriverNo, VehicleNo, JobNumber, CrdHldrName, CrdHldrPhone, CrdHldrCity, CrdHldrState, CrdHldrZip,
					CrdHldrAddress, TransRefNo, PaymentType, TxnType, FareValue, Tip, Surcharge, Fee, Taxes, Toll, Amount, ExpiryDate, CardType,
					Industry, EntryMode, ResponseCode, AddRespData, AuthId, SourceId, TransNote, IsCompleted, IsRefunded, PickAddress,
					DestinationAddress, TxnDate, FirstFourDigits, LastFourDigits, FlagFall, Extras, GatewayTxnId, fk_FleetId, RequestId,
					IsVoided, IsDownloaded, TransSeqNo, BatchNumber
			FROM dbo.MTDTransaction
			WHERE MerchantId = @fk_MerchantID
			  AND ResponseCode IN ('000', '00', '002')  
			  AND (VehicleNo = @VehicleId OR COALESCE(@VehicleId,'') = '')
			  AND (DriverNo = @DriverId OR COALESCE(@DriverId,'') = '')
			  AND (TxnDate >= @DateFrom OR @DateFrom IS NULL)
			  AND (TxnDate <= @DateTo)	-- This is ALWAYS set above, so no need to check NULL
			  AND (TxnType = @Sale OR TxnType = @Completion OR TxnType = @Refund)
			  AND (fk_FleetId = @fk_fleetId OR COALESCE(@fk_fleetId, 0) = 0)
			  AND (EntryMode = @swipe OR EntryMode = @fswipe OR EntryMode = @EntryMode OR COALESCE(@EntryMode,'') = '')
			  AND (IsDownloaded = @IsDownloaded OR @IsDownloaded IS NULL)
		END
	END
	ELSE IF ((@BatchNumber IS NOT NULL) AND (@RetrieveSinceBatch = 0))
	BEGIN
		SELECT	ID,TerminalID, DriverNo, VehicleNo, JobNumber, CrdHldrName, CrdHldrPhone, CrdHldrCity, CrdHldrState, CrdHldrZip,
				CrdHldrAddress, TransRefNo, PaymentType, TxnType, FareValue, Tip, Surcharge, Fee, Taxes, Toll, Amount, ExpiryDate, CardType,
				Industry, EntryMode, ResponseCode, AddRespData, AuthId, SourceId, TransNote, IsCompleted, IsRefunded, PickAddress,
				DestinationAddress, TxnDate, FirstFourDigits, LastFourDigits, FlagFall, Extras, GatewayTxnId, fk_FleetId, RequestId,
				IsVoided, IsDownloaded, TransSeqNo, BatchNumber
		FROM dbo.MTDTransaction
		WHERE BatchNumber = @BatchNumber
	END	
	ELSE IF ((@BatchNumber IS NOT NULL) AND (@RetrieveSinceBatch = 1))
	BEGIN
		IF (@MinimumChecks = 1)
		BEGIN
			UPDATE dbo.MTDTransaction 
			SET IsDownloaded = 1, 
				BatchNumber = @CurrentBatchNumber
			WHERE MerchantId = @fk_MerchantID
			  AND ResponseCode IN ('000', '00', '002')  
			  --AND (TxnDate <= @DateTo)	-- This is ALWAYS set above, so no need to check NULL
			  AND (TxnType = @Sale OR TxnType = @Completion OR TxnType = @Refund)
			  AND (EntryMode = @swipe OR EntryMode = @fswipe OR EntryMode = @EntryMode OR COALESCE(@EntryMode,'') = '')
			  AND (IsDownloaded = @IsDownloaded OR @IsDownloaded IS NULL)
			  AND BatchNumber IS NULL

			-- Selecting all transactions that are part of [BatchNumber > Supplied batch number (i.e. @BatchNumber)]
			SELECT	ID, TerminalID, DriverNo, VehicleNo, JobNumber, CrdHldrName, CrdHldrPhone, CrdHldrCity, CrdHldrState, CrdHldrZip,
					CrdHldrAddress, TransRefNo, PaymentType, TxnType, FareValue, Tip, Surcharge, Fee, Taxes, Toll, Amount, ExpiryDate, CardType,
					Industry, EntryMode, ResponseCode, AddRespData, AuthId, SourceId, TransNote, IsCompleted, IsRefunded, PickAddress,
					DestinationAddress, TxnDate, FirstFourDigits, LastFourDigits, FlagFall, Extras, GatewayTxnId, fk_FleetId, RequestId,
					IsVoided, IsDownloaded, TransSeqNo, BatchNumber
			FROM dbo.MTDTransaction
			WHERE MerchantId = @fk_MerchantID
			  AND ResponseCode IN ('000', '00', '002')
			  --AND (TxnDate <= @DateTo)	-- This is ALWAYS set above, so no need to check NULL
			  AND (TxnType = @Sale OR TxnType = @Completion OR TxnType = @Refund)
			  AND (EntryMode = @swipe OR EntryMode = @fswipe OR EntryMode = @EntryMode OR COALESCE(@EntryMode,'') = '')
			  AND (IsDownloaded = @IsDownloaded OR @IsDownloaded IS NULL)
			  AND BatchNumber > @BatchNumber
		END
		ELSE
		BEGIN
			-- Assign new BatchNumber to all all unbatched transactions that are part of [BatchNumber > Supplied batch number (i.e. @BatchNumber)]
			UPDATE dbo.MTDTransaction 
			SET IsDownloaded = 1, 
				BatchNumber = @CurrentBatchNumber
			WHERE MerchantId = @fk_MerchantID
			  AND ResponseCode IN ('000', '00', '002')  
			  AND (VehicleNo = @VehicleId OR COALESCE(@VehicleId,'') = '')
			  AND (DriverNo = @DriverId OR COALESCE(@DriverId,'') = '')
			  AND (TxnDate >= @DateFrom OR @DateFrom IS NULL)
			  AND (TxnDate <= @DateTo)	-- This is ALWAYS set above, so no need to check NULL
			  AND (fk_FleetId = @fk_fleetId OR COALESCE(@fk_fleetId, 0) = 0)
			  AND (TxnType = @Sale OR TxnType = @Completion OR TxnType = @Refund)
			  AND (EntryMode = @swipe OR EntryMode = @fswipe OR EntryMode = @EntryMode OR COALESCE(@EntryMode,'') = '')
			  AND (IsDownloaded = @IsDownloaded OR @IsDownloaded IS NULL)
			  AND BatchNumber IS NULL

			-- Selecting all transactions that are part of [BatchNumber > Supplied batch number (i.e. @BatchNumber)]
			SELECT	ID, TerminalID, DriverNo, VehicleNo, JobNumber, CrdHldrName, CrdHldrPhone, CrdHldrCity, CrdHldrState, CrdHldrZip,
					CrdHldrAddress, TransRefNo, PaymentType, TxnType, FareValue, Tip, Surcharge, Fee, Taxes, Toll, Amount, ExpiryDate, CardType,
					Industry, EntryMode, ResponseCode, AddRespData, AuthId, SourceId, TransNote, IsCompleted, IsRefunded, PickAddress,
					DestinationAddress, TxnDate, FirstFourDigits, LastFourDigits, FlagFall, Extras, GatewayTxnId, fk_FleetId, RequestId,
					IsVoided, IsDownloaded, TransSeqNo, BatchNumber
			FROM dbo.MTDTransaction
			WHERE MerchantId = @fk_MerchantID
			  AND ResponseCode IN ('000', '00', '002')  			
			  AND (VehicleNo = @VehicleId OR COALESCE(@VehicleId,'') = '')
			  AND (DriverNo = @DriverId OR COALESCE(@DriverId,'') = '')
			  AND (TxnDate >= @DateFrom OR @DateFrom IS NULL)
			  AND (TxnDate <= @DateTo)	-- This is ALWAYS set above, so no need to check NULL
			  AND (fk_FleetId = @fk_fleetId OR COALESCE(@fk_fleetId, 0) = 0)
			  AND (TxnType = @Sale OR TxnType = @Completion OR TxnType = @Refund)
			  AND (EntryMode = @swipe OR EntryMode = @fswipe OR EntryMode = @EntryMode OR COALESCE(@EntryMode,'') = '')
			  AND (IsDownloaded = @IsDownloaded OR @IsDownloaded IS NULL)
			  AND BatchNumber > @BatchNumber
		END
	END

	SET NOCOUNT OFF
END
GO
