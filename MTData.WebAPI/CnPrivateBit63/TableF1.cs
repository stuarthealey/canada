﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class TableF1
    {
        private const string tblId = "|46|31";
        private string TableVersion = "001";
        private string DownloadFile = "EMV2KEY|20|20|20";
        private string FileCreationDate = "00000000000000";
        private string FileSize = "00000";//15593
        private string FileCRC = "|FB|2A";//|3E|30";//need to check this
        private string FunctionCode = "R";

        public string TableID
        {
            get { return tblId; }
        }

        public string Table_Version
        {
            get { return TableVersion; }
            set { TableVersion = value; }
        }

        public string Download_File
        {
            get { return DownloadFile; }
            set { TableVersion = value; }
        }

        public string File_CreationDate
        {
            get { return FileCreationDate; }
            set { FileCreationDate = value; }
        }

        public string File_Size
        {
            get { return FileSize; }
            set { FileSize = value; }
        }

        public string File_CRC
        {
            get { return FileCRC; }
            set { FileCRC = value; }
        }

        public string Function_Code
        {
            get { return FunctionCode; }
            set { FunctionCode = value; }
        }

    }
}