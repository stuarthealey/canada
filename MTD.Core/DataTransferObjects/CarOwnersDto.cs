﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
   public class CarOwnersDto
    {
        public int Id { get; set; }
        public string OwnerName { get; set; }
        public string Contact { get; set; }
        public string EmailAddress { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string Address { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string OwnerDetail { get; set; }
        public string FailedError { get; set; }
        public Nullable<int> fk_MerchantId { get; set; }
        public string ContactPerson { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string SageAccount { get; set; }
        public string BankName { get; set; }
        public string AccountName { get; set; }
        public string BSB { get; set; }
        public string Account { get; set; }
    }
}
