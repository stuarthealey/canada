﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
    public class GetMerchantExceptionReportDto
    {
        public string FleetName { get; set; }
        public string VehicleNo { get; set; }
        public string FareValue { get; set; }
        public string Tip { get; set; }
        public string Surcharge { get; set; }
        public string TechFee { get; set; }
        public string Amount { get; set; }
        public string Fee { get; set; }
        public string AmountOwing { get; set; }
    }
}
