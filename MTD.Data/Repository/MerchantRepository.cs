﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Transactions;

using MTD.Data.Data;
using MTD.Data.Repository.Interface;
using MTData.Utility;

namespace MTD.Data.Repository
{
    public class MerchantRepository : BaseRepository, IMerchantRepository
    {
        public MerchantRepository(IMtDataDevlopmentsEntities dbMtData) : base(dbMtData)
        {
        }

        /// <summary>
        /// Purpose             :   To add merchant
        /// Function Name       :   Add
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/28/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantUser"></param>
        /// <returns></returns>
        public bool Add(MerchantUser merchantUser)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    bool isExistEmail = DbMtData.Users.Any(m => m.Email == merchantUser.Email && m.IsActive);
                    bool isExistCompany = DbMtData.Merchants.Any(m => m.Company == merchantUser.Company && m.IsActive);
                    if (isExistEmail || isExistCompany)
                    {
                        return true;
                    }

                    var merchantDb = new Merchant
                    {
                        Company = merchantUser.Company,
                        StateTaxRate = merchantUser.StateTaxRate,
                        FederalTaxRate = merchantUser.FederalTaxRate,
                        TipPerHigh = merchantUser.TipPerHigh,
                        TipPerLow = merchantUser.TipPerLow,
                        TipPerMedium = merchantUser.TipPerMedium,
                        Disclaimer = merchantUser.Disclaimer,
                        TermCondition = merchantUser.TermCondition,
                        CreatedDate = DateTime.Now,
                        CompanyTaxNumber = merchantUser.CompanyTaxNumber,
                        IsActive = true,
                        IsTaxInclusive = merchantUser.IsTaxInclusive,
                        CreatedBy = merchantUser.CreatedBy,
                        Fk_FDId = merchantUser.Fk_FDId,
                        DisclaimerPlainText = merchantUser.DisclaimerPlainText,
                        TermConditionPlainText = merchantUser.TermConditionPlainText,
                        TimeZone = merchantUser.TimeZone,
                        ZipCode = merchantUser.ZipCode,
                        UseTransArmor = merchantUser.UseTransArmor      //PAY-13
                    };

                    DbMtData.Merchants.Add(merchantDb);

                    if (merchantUser.Fk_FDId > 0)
                    {
                        var fdMerchant = DbMtData.FDMerchants.Find(merchantUser.Fk_FDId);
                        fdMerchant.IsAssigned = true;
                        fdMerchant.IsActive = true;
                        fdMerchant.ModifiedBy = merchantUser.CreatedBy;
                        fdMerchant.ModifiedDate = DateTime.Now;
                        DbMtData.CommitChanges();
                    }

                    var data = new User
                    {
                        FName = merchantUser.FName,
                        LName = merchantUser.LName,
                        Email = merchantUser.Email,
                        Phone = merchantUser.Phone,
                        Address = merchantUser.Address,
                        fk_City = merchantUser.City,
                        fk_State = merchantUser.State,
                        fk_Country = merchantUser.Country,
                        CreatedDate = DateTime.Now,
                        fk_MerchantID = merchantDb.MerchantID,
                        CreatedBy = merchantUser.CreatedBy,
                        PCode = merchantUser.PCode,
                        IsLockedOut = false,
                        IsActive = true,
                        IsFirstLogin = true,
                        fk_UserTypeID = 2,
                        PCodeExpDate = DateTime.Now.AddDays(90)
                    };

                    DbMtData.Users.Add(data);
                    DbMtData.CommitChanges();
                    var report = DbMtData.Reports.Where(x => x.ReportId < 4).ToList();
                    report = report.Concat(DbMtData.Reports.Where(x => x.ReportId > 6 && x.ReportId < 9)).ToList();
                    Recipient[] recObj = new Recipient[report.Count()];
                    
                    int count = 0;
                    foreach (var item in report)
                    {
                        recObj[count] = new Recipient();
                        recObj[count].fk_MerchantID = merchantDb.MerchantID;
                        recObj[count].IsRecipient = true;
                        recObj[count].fk_UserID = null;
                        recObj[count].fk_UserTypeID = 2;
                        recObj[count].fk_ReportID = item.ReportId;
                        recObj[count].ModifiedBy = data.CreatedBy;
                        recObj[count].ModifiedDate = DateTime.Now;
                        DbMtData.Recipients.Add(recObj[count]);
                        count++;
                    }

                    DbMtData.CommitChanges();
                    var roleList = merchantUser.RolesAssigned.Split(',');
                    var roles = new UserRole[roleList.Length];
                    for (int i = 0; i < roleList.Length; i++)
                    {
                        roles[i] = new UserRole();
                        roles[i].fk_UserID = data.UserID;
                        roles[i].fk_RoleID = Convert.ToInt32(roleList[i]);
                        roles[i].IsActive = true;
                        roles[i].CreatedDate = DateTime.Now;
                        roles[i].CreatedBy = merchantUser.CreatedBy;
                        DbMtData.UserRoles.Add(roles[i]);
                    }

                    DbMtData.CommitChanges();        // commiting changes to database
                    transactionScope.Complete();
                    return false;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Purpose             :   To update merchant
        /// Function Name       :   Update
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/28/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantUser"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public int Update(MerchantUser merchantUser, int merchantId)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    bool isExist = DbMtData.Merchants.Any(m => m.Company == merchantUser.Company && m.IsActive && m.MerchantID != merchantId);
                    if (isExist)
                    {
                        return 1;
                    }

                    if (!string.IsNullOrEmpty(merchantUser.RapidConnect))
                    {
                        bool isFdMerchantExist = DbMtData.FDMerchants.Any(m => m.FDMerchantID == merchantUser.RapidConnect && m.FDId != merchantUser.Fk_FDId && (bool)m.IsActive);
                        if (isFdMerchantExist)
                        {
                            return 2;
                        }
                    }

                    var merchant = DbMtData.Merchants.FirstOrDefault(x => x.MerchantID == merchantId);
                    if (merchant != null)
                    {
                        int? fdId = merchant.Fk_FDId;
                        if (!string.IsNullOrEmpty(merchantUser.RapidConnect) || !string.IsNullOrEmpty(merchantUser.GropuId) || !string.IsNullOrEmpty(merchantUser.TokenType) || !string.IsNullOrEmpty(merchantUser.MCCode) || merchantUser.ProjectType!=null)
                        {
                            var fdMerchant = DbMtData.FDMerchants.Find(fdId);
                            if (fdMerchant == null)
                            {
                                FDMerchant fdAdd = new FDMerchant();
                                fdAdd.IsAssigned = true;
                                fdAdd.IsActive = true;
                                fdAdd.ModifiedBy = merchantUser.CreatedBy;
                                fdAdd.GroupId = merchantUser.GropuId;
                                fdAdd.FDMerchantID = merchantUser.RapidConnect;
                                fdAdd.TokenType = merchantUser.TokenType;
                                fdAdd.MCCode = merchantUser.MCCode;
                                fdAdd.EncryptionKey = merchantUser.EncryptionKey;
                                fdAdd.ModifiedDate = DateTime.Now;
                                fdAdd.ServiceId = ConfigurationManager.AppSettings["ServiceId"];
                                fdAdd.ServiceUrl = string.Empty;
                                fdAdd.App = ConfigurationManager.AppSettings["App"];
                                fdAdd.ProjectId = merchantUser.ProjectId;
                                fdAdd.EcommProjectId = merchantUser.EcommProjectId;
                                fdAdd.ProjectType = merchantUser.ProjectType;
                                fdAdd.CreatedBy = merchantUser.CreatedBy;
                                fdAdd.CreatedDate = DateTime.Now;
                                //fdAdd.UseTransArmor = merchantUser.UseTransArmor;   //PAY-13

                                DbMtData.FDMerchants.Add(fdAdd);
                                DbMtData.CommitChanges();

                                fdId = fdAdd.FDId;
                            }
                            else
                            {
                                fdMerchant.IsAssigned = true;
                                fdMerchant.IsActive = true;
                                fdMerchant.ProjectType = merchantUser.ProjectType;
                                fdMerchant.ProjectId = merchantUser.ProjectId;
                                fdMerchant.EcommProjectId = merchantUser.EcommProjectId;
                                fdMerchant.ModifiedBy = merchantUser.CreatedBy;
                                fdMerchant.GroupId = merchantUser.GropuId;
                                fdMerchant.FDMerchantID = merchantUser.RapidConnect;
                                fdMerchant.TokenType = merchantUser.TokenType;
                                fdMerchant.MCCode = merchantUser.MCCode;
                                fdMerchant.EncryptionKey = merchantUser.EncryptionKey;
                                fdMerchant.ModifiedDate = DateTime.Now;
                                //fdMerchant.UseTransArmor = merchantUser.UseTransArmor;      //PAY-13

                                DbMtData.CommitChanges();
                            }
                        }
                        else
                        {
                            var fdMerchant = DbMtData.FDMerchants.Find(fdId);
                            if (fdMerchant != null)
                            {
                                fdMerchant.IsAssigned = true;
                                fdMerchant.ModifiedBy = merchantUser.CreatedBy;
                                fdMerchant.GroupId = merchantUser.GropuId;
                                fdMerchant.FDMerchantID = merchantUser.RapidConnect;
                                fdMerchant.TokenType = merchantUser.TokenType;
                                fdMerchant.MCCode = merchantUser.MCCode;
                                fdMerchant.EcommProjectId = merchantUser.EcommProjectId;
                                fdMerchant.ProjectType = merchantUser.ProjectType;
                                fdMerchant.ProjectId = merchantUser.ProjectId;
                                fdMerchant.EncryptionKey = merchantUser.EncryptionKey;
                                fdMerchant.ModifiedDate = DateTime.Now;
                                //fdMerchant.UseTransArmor = merchantUser.UseTransArmor;      //PAY-13

                                DbMtData.CommitChanges();
                            }
                        }

                        Merchant merchantDb = DbMtData.Merchants.Find(merchantId);
                        merchantDb.Fk_FDId = fdId;
                        merchantDb.Company = merchantUser.Company;
                        merchantDb.StateTaxRate = merchantUser.StateTaxRate;
                        merchantDb.FederalTaxRate = merchantUser.FederalTaxRate;
                        merchantDb.TipPerLow = merchantUser.TipPerLow;
                        merchantDb.TipPerMedium = merchantUser.TipPerMedium;
                        merchantDb.TipPerHigh = merchantUser.TipPerHigh;
                        merchantDb.Disclaimer = merchantUser.Disclaimer;
                        merchantDb.TermCondition = merchantUser.TermCondition;
                        merchantDb.IsTaxInclusive = merchantUser.IsTaxInclusive;
                        merchantDb.CompanyTaxNumber = merchantUser.CompanyTaxNumber;
                        merchantDb.ModifiedBy = merchantUser.ModifiedBy;
                        merchantDb.IsJobNoRequired = merchantUser.IsJobNoRequired;
                        merchantDb.DisclaimerPlainText = merchantUser.DisclaimerPlainText;
                        merchantDb.TimeZone = merchantUser.TimeZone;
                        merchantDb.TermConditionPlainText = merchantUser.TermConditionPlainText;
                        merchantDb.ZipCode = merchantUser.ZipCode;
                        merchantDb.ModifiedDate = DateTime.Now;
                        merchantDb.UseTransArmor = merchantUser.UseTransArmor;  //PAY-13
                    }

                    DbMtData.CommitChanges();
                    var data = DbMtData.Users.Find(merchantUser.UserID); ;
                    if (data != null)
                    {
                        data.FName = merchantUser.FName;
                        data.LName = merchantUser.LName;
                        // data.Email = merchantUser.Email;
                        data.Phone = merchantUser.Phone;
                        data.Address = merchantUser.Address;
                        data.fk_City = merchantUser.City;
                        data.fk_State = merchantUser.State;
                        data.fk_Country = merchantUser.Country;
                        data.IsLockedOut = merchantUser.IsLockedOut;
                        data.ModifiedBy = merchantUser.ModifiedBy;
                        data.ModifiedDate = DateTime.Now;

                        DbMtData.CommitChanges();

                        var deleteRecords = from tempRecords in DbMtData.UserRoles
                                            where tempRecords.fk_UserID == data.UserID
                                            select tempRecords;

                        foreach (var item in deleteRecords)
                        {
                            DbMtData.UserRoles.Remove(item);
                        }

                        string[] roleList = merchantUser.RolesAssigned.Split(',');
                        var roles = new UserRole[roleList.Length];
                        for (int i = 0; i < roleList.Length; i++)
                        {
                            roles[i] = new UserRole();
                            roles[i].fk_UserID = data.UserID;
                            roles[i].fk_RoleID = Convert.ToInt32(roleList[i]);
                            roles[i].IsActive = true;
                            roles[i].CreatedDate = DateTime.Now;
                            roles[i].CreatedBy = merchantUser.CreatedBy;
                            DbMtData.UserRoles.Add(roles[i]);
                        }
                    }

                    DbMtData.CommitChanges();
                    transactionScope.Complete();

                    return 0;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Purpose             :   To get merchant
        /// Function Name       :   GetMerchant
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/28/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public MerchantUser GetMerchant(int merchantId, int userId)
        {
            try
            {
                var merchantLi = (from users in DbMtData.Users
                                  join merchants in DbMtData.Merchants on users.fk_MerchantID equals merchantId into result
                                  from resTemp in result
                                  where resTemp.MerchantID == merchantId && users.UserID == userId

                                  select new
                                  {
                                      resTemp.Company,
                                      users.FName,
                                      users.LName,
                                      users.Email,
                                      users.Address,
                                      users.fk_City,
                                      users.fk_State,
                                      users.fk_Country,
                                      users.Phone,
                                      users.IsActive,
                                      users.IsLockedOut,
                                      users.UserID,
                                      resTemp.MerchantID,
                                      resTemp.TimeZone,
                                      resTemp.StateTaxRate,
                                      resTemp.FederalTaxRate,
                                      resTemp.Disclaimer,
                                      resTemp.TermCondition,
                                      resTemp.TipPerHigh,
                                      resTemp.TipPerLow,
                                      resTemp.TipPerMedium,
                                      resTemp.CompanyTaxNumber,
                                      resTemp.Fk_FDId,
                                      resTemp.IsTaxInclusive,
                                      resTemp.IsJobNoRequired,
                                      resTemp.ZipCode,
                                      resTemp.UseTransArmor //PAY-13
                                  }).FirstOrDefault();

                var objNew = new MerchantUser();
                if (merchantLi == null) return objNew;

                objNew.Company = merchantLi.Company;
                objNew.FName = merchantLi.FName;
                objNew.LName = merchantLi.LName;
                objNew.Email = merchantLi.Email;
                objNew.Address = merchantLi.Address;
                objNew.TimeZone = merchantLi.TimeZone;
                objNew.City = merchantLi.fk_City;
                objNew.State = merchantLi.fk_State;
                objNew.Country = merchantLi.fk_Country;
                objNew.Phone = merchantLi.Phone;
                objNew.StateTaxRate = merchantLi.StateTaxRate;
                objNew.FederalTaxRate = merchantLi.FederalTaxRate;
                objNew.Disclaimer = merchantLi.Disclaimer;
                objNew.TermCondition = merchantLi.TermCondition;
                objNew.TipPerHigh = merchantLi.TipPerHigh;
                objNew.TipPerLow = merchantLi.TipPerLow;
                objNew.TipPerMedium = merchantLi.TipPerMedium;
                objNew.MerchantID = merchantLi.MerchantID;
                objNew.CompanyTaxNumber = merchantLi.CompanyTaxNumber;
                objNew.IsLockedOut = merchantLi.IsLockedOut;
                objNew.UserID = merchantLi.UserID;
                objNew.Fk_FDId = merchantLi.Fk_FDId;
                objNew.IsTaxInclusive = (bool)merchantLi.IsTaxInclusive;
                objNew.IsJobNoRequired = Convert.ToBoolean(merchantLi.IsJobNoRequired);
                objNew.ZipCode = merchantLi.ZipCode;
                objNew.UseTransArmor = merchantLi.UseTransArmor;        //PAY-13
                
                var role = (from roles in DbMtData.UserRoles where roles.fk_UserID == merchantLi.UserID select new { roles.fk_RoleID }).ToList();
                var temp = "";
                for (var i = 0; i < role.Count; i++)
                {
                    if (i == role.Count - 1)
                    {
                        temp = temp + role[i].fk_RoleID;
                    }
                    else
                    {
                        temp = temp + role[i].fk_RoleID + ",";
                    }
                }

                objNew.RolesAssigned = temp;
                return objNew;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To delete merchant
        /// Function Name       :   DeleteMerchant
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/28/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        public void DeleteMerchant(int merchantId)
        {
            try
            {
                var merchantDb = DbMtData.Merchants.Find(merchantId);
                merchantDb.IsActive = false;
                var objUser = (from item in DbMtData.Users.Where(x => x.fk_MerchantID == merchantId) select item).FirstOrDefault();
                objUser.IsActive = false;
                var fleets = (from m in DbMtData.Fleets where m.fk_MerchantID == merchantId && m.IsActive select m).ToList();
                foreach (Fleet fleet in fleets)
                {
                    fleet.IsActive = false;
                }
                if (merchantDb.Fk_FDId != null)
                {
                    var fdMerchant = DbMtData.FDMerchants.Find(merchantDb.Fk_FDId);
                    fdMerchant.IsAssigned = false;
                    fdMerchant.IsActive = false;
                }
                DbMtData.CommitChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get merchants
        /// Function Name       :   GetMerchants
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/28/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Merchant> GetMerchants()
        {
            return DbMtData.Merchants;
        }

        /// <summary>
        /// Purpose             :   To get list of merchants
        /// Function Name       :   MerchantList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/28/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MerchantUser> MerchantList(string name)
        {
            try
            {
                IEnumerable<MerchantUser> merchantLi = (from users in DbMtData.Users
                                                        join merchants in DbMtData.Merchants on users.fk_MerchantID equals merchants.MerchantID
                                                        join countries in DbMtData.CountryCodes on users.fk_Country equals countries.CountryCodesID
                                                        where merchants.IsActive && users.UserType.UserTypeID == 2
                                                        select new { Merchants = merchants, Users = users, CountryCodes = countries }).AsEnumerable().Select(objNew =>
                     new MerchantUser
                     {
                         Company = objNew.Merchants.Company,
                         FName = objNew.Users.FName,
                         LName = objNew.Users.LName,
                         Email = objNew.Users.Email,
                         Address = objNew.Users.Address,
                         City = objNew.Users.fk_City,
                         State = objNew.Users.fk_State,
                         Country = objNew.CountryCodes.Title,
                         Phone = objNew.Users.Phone,
                         IsActive = objNew.Users.IsActive,
                         IsLockedOut = objNew.Users.IsLockedOut,
                         MerchantID = objNew.Merchants.MerchantID,
                         UserID = objNew.Users.UserID
                     }).ToList();

                if (!string.IsNullOrEmpty(name))
                {
                    merchantLi = merchantLi.Where(p => p.Company.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }

                return merchantLi;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To add receipt
        /// Function Name       :   AddReciept
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/28/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="reciept"></param>
        public void AddReciept(ManageReciept reciept)
        {
            var objRec = new Receipt();
            try
            {
                objRec.fk_FleetID = reciept.fk_FleetID;
                objRec.IsFleet = reciept.IsFleet;
                objRec.IsVehicle = reciept.IsVehicle;
                objRec.IsDriver = reciept.IsDriver;
                objRec.IsDriverTaxNo = reciept.IsDriverTaxNo;
                objRec.IsCompName = reciept.IsCompName;
                objRec.IsCompAdd = reciept.IsCompAdd;
                objRec.IsComPhone = reciept.IsComPhone;
                objRec.IsPickAdd = reciept.IsPickAdd;
                objRec.IsDestAdd = reciept.IsDestAdd;
                objRec.IsFlagFall = reciept.IsFlagFall;
                objRec.IsFare = reciept.IsFare;
                objRec.IsExtra = reciept.IsExtra;
                objRec.IsTolls = reciept.IsTolls;
                objRec.IsTip = reciept.IsTip;
                objRec.IsStateTaxPer = reciept.IsStateTaxPer;
                objRec.IsStateTaxAmt = reciept.IsStateTaxAmt;
                objRec.IsFederalTaxPer = reciept.IsFederalTaxPer;
                objRec.IsFederalTaxAmt = reciept.IsFederalTaxAmt;
                objRec.IsSurPer = reciept.IsSurPer;
                objRec.IsSurAmt = reciept.IsSurAmt;
                objRec.IsSubTotal = reciept.IsSubTotal;
                objRec.IsTotal = reciept.IsTotal;
                objRec.IsActive = true;
                objRec.CreatedBy = reciept.CreatedBy;
                objRec.CreatedDate = DateTime.Now;
                objRec.ModifiedBy = reciept.ModifiedBy;
                objRec.ModifiedDate = reciept.ModifiedDate;
                objRec.IsTermsAndCondition = reciept.IsTermsAndCondition;
                objRec.IsDisclaimer = reciept.IsDisclaimer;
                objRec.IsCompanyTaxNo = reciept.IsCompanyTaxNo;
                DbMtData.Receipts.Add(objRec);
                DbMtData.CommitChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get roles of merchant
        /// Function Name       :   GetUsersRole
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/29/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Role> GetUsersRole()
        {
            try
            {
                IEnumerable<Role> userRoles =
                              (from role in DbMtData.Roles where role.fk_UserTypeID == null select new { role.RoleName, role.RoleID }).AsEnumerable()
                                  .Select(objNew =>
                                      new Role
                                      {
                                          RoleID = objNew.RoleID,
                                          RoleName = objNew.RoleName
                                      }).ToList();
                return userRoles;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get merchant as view
        /// Function Name       :   GetMerchantViewAs
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/29/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public MerchantUser GetMerchantViewAs(int merchantId, int userId)
        {
            try
            {
                var merchantLi = (from users in DbMtData.Users
                                  join countries in DbMtData.CountryCodes on users.fk_Country equals countries.CountryCodesID
                                  join merchants in DbMtData.Merchants on users.fk_MerchantID equals merchantId into result
                                  from resTemp in result
                                  where resTemp.MerchantID == merchantId && users.UserID == userId
                                  select new
                                  {
                                      resTemp.Company,
                                      users.FName,
                                      users.LName,
                                      users.Email,
                                      users.Address,
                                      countries.Title,
                                      users.fk_City,
                                      users.fk_State,
                                      users.Phone,
                                      users.IsActive,
                                      users.IsLockedOut,
                                      users.UserID,
                                      resTemp.MerchantID,
                                      resTemp.TimeZone,
                                      resTemp.ZipCode,
                                      resTemp.StateTaxRate,
                                      resTemp.FederalTaxRate,
                                      resTemp.Disclaimer,
                                      resTemp.TermCondition,
                                      resTemp.TipPerHigh,
                                      resTemp.TipPerLow,
                                      resTemp.TipPerMedium,
                                      resTemp.CompanyTaxNumber,
                                      resTemp.UseTransArmor //PAY-13
                                  }).FirstOrDefault();

                var objNew = new MerchantUser();

                if (merchantLi == null) return objNew;

                objNew.Company = merchantLi.Company;
                objNew.FName = merchantLi.FName;
                objNew.LName = merchantLi.LName;
                objNew.Email = merchantLi.Email;
                objNew.Address = merchantLi.Address;
                objNew.City = merchantLi.fk_City;
                objNew.State = merchantLi.fk_State;
                objNew.Country = merchantLi.Title;
                objNew.Phone = merchantLi.Phone;
                objNew.TimeZone = merchantLi.TimeZone;
                objNew.ZipCode = merchantLi.ZipCode;
                objNew.StateTaxRate = merchantLi.StateTaxRate;
                objNew.FederalTaxRate = merchantLi.FederalTaxRate;
                objNew.Disclaimer = merchantLi.Disclaimer;
                objNew.TermCondition = merchantLi.TermCondition;
                objNew.TipPerHigh = merchantLi.TipPerHigh;
                objNew.TipPerLow = merchantLi.TipPerLow;
                objNew.TipPerMedium = merchantLi.TipPerMedium;
                objNew.MerchantID = merchantLi.MerchantID;
                objNew.CompanyTaxNumber = merchantLi.CompanyTaxNumber;
                objNew.IsLockedOut = merchantLi.IsLockedOut;
                objNew.UserID = merchantLi.UserID;
                objNew.UseTransArmor = merchantLi.UseTransArmor;        //PAY-13

                var role = (from roles in DbMtData.UserRoles where roles.fk_UserID == merchantLi.UserID select new { roles.fk_RoleID }).ToList();
                var temp = "";
                for (var i = 0; i < role.Count; i++)
                {
                    if (i == role.Count - 1)
                    {
                        temp = temp + role[i].fk_RoleID;
                    }
                    else
                    {
                        temp = temp + role[i].fk_RoleID + ",";
                    }
                }

                objNew.RolesAssigned = temp;
                return objNew;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get merchant based on id
        /// Function Name       :   GetMerchant
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/29/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public Merchant GetMerchant(int merchantId)
        {
            var merchantDb = DbMtData.Merchants.FirstOrDefault(a => a.MerchantID.Equals(merchantId));
            return merchantDb;
        }

        /// <summary>
        /// Purpose             :   To get merchant list by filter
        /// Function Name       :   GetMerchantByFilter
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/29/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <param name="sorting"></param>
        /// <returns></returns>
        public IEnumerable<MerchantUser> GetMerchantByFilter(string name, int startIndex, int count, string sorting)
        {
            IEnumerable<MerchantUser> query = (from users in DbMtData.Users
                                               join merchants in DbMtData.Merchants on users.fk_MerchantID equals merchants.MerchantID
                                               join countries in DbMtData.CountryCodes on users.fk_Country equals countries.CountryCodesID
                                               where merchants.IsActive && users.UserType.UserTypeID == 2 && (bool)users.IsActive
                                               select new { Merchants = merchants, Users = users, CountryCodes = countries }).AsEnumerable().Select(objNew =>
                    new MerchantUser
                    {
                        City = objNew.Users.fk_City,
                        State = objNew.Users.fk_State,

                        Company = objNew.Merchants.Company,
                        LName = objNew.Users.FName + " " + objNew.Users.LName,
                        FName = objNew.Users.FName,
                        Email = objNew.Users.Email,
                        Address = objNew.Users.Address + ", " + objNew.Users.fk_City + ", " + objNew.Users.fk_State + ", " + objNew.CountryCodes.Title,
                        Country = objNew.CountryCodes.Title,
                        Phone = objNew.Users.Phone,
                        IsActive = objNew.Users.IsActive,
                        IsLockedOut = objNew.Users.IsLockedOut,
                        MerchantID = objNew.Merchants.MerchantID,
                        UserID = objNew.Users.UserID,
                        CreatedDate = objNew.Users.CreatedDate,
                        ZipCode = objNew.Merchants.ZipCode,
                        UseTransArmor = objNew.Merchants.UseTransArmor      //PAY-13
                    }).ToList();

            query = query.OrderBy(p => p.Company); //Default!

            //Filters
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(p => p.Company.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0);
            }

            if (!string.IsNullOrEmpty(sorting))
            {
                query = SortedMerchantUser(sorting, query);
            }
            
            List<MerchantUser> mUsers = new List<MerchantUser>();
            foreach (var mu in query.ToList())
            {
                mu.Address = mu.Address.Replace(", , ,", ", ");
                mu.Address = mu.Address.Replace(", ,", ", ");
                mUsers.Add(mu);
            }

            return count > 0
                        ? mUsers.Skip(startIndex).Take(count).ToList() //Paging
                        : mUsers.ToList(); //No paging
        }

        /// <summary>
        /// Purpose             :   To get FdMerchant
        /// Function Name       :   GetFDMerchant
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/29/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public FDMerchant GetFDMerchant(int merchantId)
        {
            try
            {
                var fdMerchant = (from fdmerchant in DbMtData.FDMerchants
                                  join merchant in DbMtData.Merchants on fdmerchant.FDId equals merchant.Fk_FDId into result
                                  from resTemp in result
                                  where resTemp.MerchantID == merchantId
                                  select fdmerchant).FirstOrDefault();

                return fdMerchant;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To check FdMerchant
        /// Function Name       :   FdMerchantExist
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/29/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="fDid"></param>
        /// <returns></returns>
        public FDMerchant FdMerchantExist(int fDid)
        {
            var fdMerchnat = DbMtData.FDMerchants.FirstOrDefault(x => x.FDId == fDid);
            return fdMerchnat;
        }

        /// <summary>
        /// Purpose             :   To get details of datawire
        /// Function Name       :   GetDataWireDetails
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/29/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="serialNo"></param>
        /// <returns></returns>
        public Datawire GetDataWireDetails(int merchantId, int Id)
        {
            try
            {
                Datawire obj = DbMtData.Datawires.FirstOrDefault(x => x.MerchantId == merchantId && x.ID == Id && x.IsActive && x.IsAssigned == true);
                return obj;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get details of terminal
        /// Function Name       :   TerminalDetails
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/29/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="serialNo"></param>
        /// <returns></returns>
        public Terminal TerminalDetails(string serialNo)
        {
            try
            {
                Terminal obj = DbMtData.Terminals.FirstOrDefault(x => x.SerialNo == serialNo && x.IsActive);
                return obj;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Delete FdMerchant
        /// Function Name       :   DeleteFdMerchant
        /// Created By          :   Naveen Kumar
        /// Created On          :   06/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fdMerchantId"></param>
        /// <returns></returns>
        public bool DeleteFdMerchant(int fdMerchantId)
        {
            try
            {
                bool? isAssigned = (from m in DbMtData.FDMerchants
                                    where m.FDId == fdMerchantId && m.IsActive == true
                                    select m.IsAssigned).FirstOrDefault();
                if (Convert.ToBoolean(isAssigned))
                {
                    return true;
                }
                var merchantDb = DbMtData.FDMerchants.Find(fdMerchantId);
                merchantDb.IsActive = false;
                DbMtData.CommitChanges();
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get FdMerchant Update
        /// Function Name       :   GetFdMerchantUpdate
        /// Created By          :   Naveen Kumar
        /// Created On          :   06/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fdMerchantId"></param>
        /// <returns></returns>
        public FDMerchant GetFdMerchantUpdate(int fdMerchantId)
        {
            var merchantDb = DbMtData.FDMerchants.FirstOrDefault(a => a.FDId.Equals(fdMerchantId) && a.IsActive == true);
            return merchantDb;
        }

        /// <summary>
        /// Purpose             :   To Update FdMerchant
        /// Function Name       :   UpdateFdMerchant
        /// Created By          :   Naveen Kumar
        /// Created On          :   06/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fdMerchant"></param>
        /// <param name="fdMerchantId"></param>
        /// <returns></returns>
        public void UpdateFdMerchant(FDMerchant fdMerchant, int fdMerchantId)
        {
            try
            {
                FDMerchant merchantDb = DbMtData.FDMerchants.Find(fdMerchantId);
                merchantDb.FDMerchantID = fdMerchant.FDMerchantID;
                merchantDb.GroupId = fdMerchant.GroupId;
                merchantDb.ServiceId = fdMerchant.ServiceId;
                merchantDb.ProjectId = fdMerchant.ProjectId;
                merchantDb.App = fdMerchant.App;
                merchantDb.ServiceUrl = fdMerchant.ServiceUrl;
                merchantDb.TokenType = fdMerchant.TokenType;
                merchantDb.MCCode = fdMerchant.MCCode;
                merchantDb.EncryptionKey = fdMerchant.EncryptionKey;
                merchantDb.ModifiedDate = DateTime.Now;
                merchantDb.ModifiedBy = fdMerchant.ModifiedBy;
                DbMtData.CommitChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get FdMerchant For Update
        /// Function Name       :   GetFdMerchantForUpdate
        /// Created By          :   Naveen Kumar
        /// Created On          :   06/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fdId"></param>
        /// <returns></returns>
        public FDMerchant GetFdMerchantForUpdate(int fdId)
        {
            var fdMerchant = DbMtData.FDMerchants.Find(fdId);
            return fdMerchant;
        }

        /// <summary>
        /// Purpose             :   To Get FDMerchants
        /// Function Name       :   GetFDMerchantList
        /// Created By          :   Naveen Kumar
        /// Created On          :   06/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<FDMerchant> GetFDMerchantList()
        {
            try
            {
                var fdMerchantList = from m in DbMtData.FDMerchants
                                     where m.IsActive == true
                                         && m.IsAssigned == false
                                     select m;

                return fdMerchantList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get FdMerchants
        /// Function Name       :   GetFdMerchantByFilter
        /// Created By          :   Naveen Kumar
        /// Created On          :   06/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <param name="sorting"></param>
        /// <returns></returns>
        public IEnumerable<FDMerchant> GetFdMerchantByFilter(string name, int startIndex, int count, string sorting)
        {
            IEnumerable<FDMerchant> query = from m in DbMtData.FDMerchants
                                            where m.IsActive == true
                                            select m;

            query = query.OrderBy(p => p.FDMerchantID);
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(p => p.FDMerchantID.ToString().IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0);
            }

            if (!string.IsNullOrEmpty(sorting))
            {
                switch (sorting)
                {
                    case "GroupId ASC":
                        query = query.OrderBy(p => p.GroupId);
                        break;
                    case "GroupId DESC":
                        query = query.OrderByDescending(p => p.GroupId);
                        break;
                    case "ProjectId ASC":
                        query = query.OrderBy(p => p.ProjectId);
                        break;
                    case "ProjectId DESC":
                        query = query.OrderByDescending(p => p.ProjectId);
                        break;
                    case "ServiceId ASC":
                        query = query.OrderBy(p => p.ServiceId);
                        break;
                    case "ServiceId DESC":
                        query = query.OrderByDescending(p => p.ServiceId);
                        break;
                    case "App ASC":
                        query = query.OrderBy(p => p.App);
                        break;
                    case "App DESC":
                        query = query.OrderByDescending(p => p.App);
                        break;
                    case "ServiceUrl ASC":
                        query = query.OrderBy(p => p.ServiceUrl);
                        break;
                    case "ServiceUrl DESC":
                        query = query.OrderByDescending(p => p.ServiceUrl);
                        break;
                    case "FDMerchantID ASC":
                        query = query.OrderBy(p => p.FDMerchantID);
                        break;
                    case "FDMerchantID DESC":
                        query = query.OrderByDescending(p => p.FDMerchantID);
                        break;
                }
            }

            return count > 0
                ? query.Skip(startIndex).Take(count).ToList() //Paging
                : query.ToList(); //No paging
        }

        /// <summary>
        /// Purpose             :   To Add FdMerchant
        /// Function Name       :   AddFdMerchant
        /// Created By          :   Naveen Kumar
        /// Created On          :   06/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fdMerchant"></param>
        /// <returns></returns>
        public bool FdMerchantAdd(FDMerchant fdMerchant)
        {
            try
            {
                bool isExist = DbMtData.FDMerchants.Any(m => m.FDMerchantID == fdMerchant.FDMerchantID && (bool)m.IsActive);
                if (isExist)
                {
                    return true;
                }

                fdMerchant.IsActive = true;
                fdMerchant.IsAssigned = false;
                fdMerchant.CreatedDate = DateTime.Now;
                DbMtData.FDMerchants.Add(fdMerchant); // saving fdmerchant to database
                DbMtData.CommitChanges(); // commiting changes to database
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Add FdMerchant
        /// Function Name       :   AddFdMerchant
        /// Created By          :   Naveen Kumar
        /// Created On          :   06/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fdMerchant"></param>
        /// <returns></returns>
        public int AddFdMerchant(FDMerchant fdMerchant)
        {
            try
            {
                bool IsExist = DbMtData.FDMerchants.Any(m => m.FDMerchantID == fdMerchant.FDMerchantID && (bool)m.IsActive);
                if (IsExist)
                {
                    return Convert.ToInt32(IsExist);
                }

                fdMerchant.IsActive = true;
                fdMerchant.IsAssigned = true;
                fdMerchant.CreatedDate = DateTime.Now;
                DbMtData.FDMerchants.Add(fdMerchant); // saving fdmerchant to database
                DbMtData.CommitChanges(); // commiting changes to database

                return fdMerchant.FDId;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Merchant Id
        /// Function Name       :   GetMerchantId
        /// Created By          :   Naveen Kumar
        /// Created On          :   06/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public int GetMerchantId(string email)
        {
            try
            {
                var user = DbMtData.Users.FirstOrDefault(m => m.Email == email);
                if (user != null)
                {
                    return (int)user.fk_MerchantID;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return 0;
        }

        /// <summary>
        /// Purpose             :   To Check Email Exist
        /// Function Name       :   EmailExist
        /// Created By          :   Umesh Kumar
        /// Created On          :   06/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool EmailExist(string email)
        {
            try
            {
                return DbMtData.Users.Any(m => m.Email == email && m.IsActive);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Check Company Exsit
        /// Function Name       :   CompanyExsit
        /// Created By          :   Umesh Kumar
        /// Created On          :   06/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public bool CompanyExsit(string company)
        {
            try
            {
                return DbMtData.Merchants.Any(m => m.Company == company && m.IsActive);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="company"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public bool CompanyExsit(string company, int merchantId)
        {
            try
            {
                return DbMtData.Merchants.Any(m => m.Company == company && m.IsActive && m.MerchantID != merchantId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update Password
        /// Function Name       :   UpdatePassword
        /// Created By          :   Umesh Kumar
        /// Created On          :   06/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public bool UpdatePassword(int userId, string newPassword)
        {
            try
            {
                bool found = DbMtData.Users.Any(m => m.UserID == userId && m.IsActive);
                if (found)
                {
                    var userDb = DbMtData.Users.FirstOrDefault(m => m.UserID == userId);
                    string[] arrayCode = new string[5];
                    arrayCode[0] = userDb.PCode;
                    arrayCode[1] = userDb.FirstPCode;
                    arrayCode[2] = userDb.SecondPCode;
                    arrayCode[3] = userDb.ThirdPCode;
                    arrayCode[4] = userDb.FourthPCode;
                    
                    for (int index = 0; index < 5; index++)
                    {
                        bool isSamePassCode = StringExtension.ValidatePassCode(newPassword, arrayCode[index]);
                        if (isSamePassCode)
                        {
                            return false;
                        }
                    }

                    newPassword = newPassword.Hash();
                    userDb.FourthPCode = userDb.ThirdPCode;
                    userDb.ThirdPCode = userDb.SecondPCode;
                    userDb.SecondPCode = userDb.FirstPCode;
                    userDb.FirstPCode = userDb.PCode;
                    userDb.PCode = newPassword;
                    userDb.PCodeExpDate = DateTime.Now.AddDays(90);
                    DbMtData.CommitChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return false;
        }

        /// <summary>
        /// Purpose             :   To Get Email
        /// Function Name       :   GetUserReceipent
        /// Created By          :   Umesh Kumar
        /// Created On          :   06/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetEmail(int userId)
        {
            try
            {
                bool found = DbMtData.Users.Any(m => m.UserID == userId);
                if (found)
                {
                    var users = DbMtData.Users.FirstOrDefault(m => m.UserID == userId);
                    if (users != null)
                    {
                        return users.Email;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return string.Empty;
        }

        /// <summary>
        /// Purpose             :   To Get User Receipent
        /// Function Name       :   GetUserReceipent
        /// Created By          :   Asif Shafeeque
        /// Created On          :   06/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="recipient"></param>
        /// <returns></returns>
        public IEnumerable<Recipient> GetUserReceipent(int merchantId)
        {
            try
            {
                var recipientList = (from m in DbMtData.Recipients join j in DbMtData.Users on m.fk_UserID equals j.UserID where m.fk_MerchantID == merchantId && (j.fk_UserTypeID == 3 || j.fk_UserTypeID == 4) && j.IsActive select new { m.fk_MerchantID, j.Email, j.FName, j.LName, m.fk_UserID }).Distinct().AsEnumerable().Select(objNew => new Recipient { fk_MerchantID = objNew.fk_MerchantID, fk_UserID = objNew.fk_UserID, Email = objNew.Email, FName = objNew.FName, LName = objNew.LName }).ToList();
                return recipientList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To update user receipents
        /// Function Name       :   UpdateUserRecipient
        /// Created By          :   Asif Shafeeque
        /// Created On          :   06/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="recipient"></param>
        /// <returns></returns>
        public bool UpdateUserRecipient(RecipientMaster recipient)
        {
            Recipient[] RecipientObj = new Recipient[recipient.MerchantList.Count()];
            try
            {
                int count = 0;
                foreach (var item in recipient.MerchantList)
                {
                    RecipientObj[count] = new Recipient();
                    RecipientObj[count] = (from m in DbMtData.Recipients where m.fk_MerchantID == item.fk_MerchantID && m.fk_UserID == item.fk_UserID && recipient.ReportId == m.fk_ReportID select m).FirstOrDefault();
                    if (RecipientObj[count] != null)
                    {
                        RecipientObj[count].fk_UserID = item.fk_UserID;
                        RecipientObj[count].fk_MerchantID = item.fk_MerchantID;
                        RecipientObj[count].IsRecipient = item.IsRecipient;
                        RecipientObj[count].ModifiedBy = item.ModifiedBy;
                        RecipientObj[count].ModifiedDate = DateTime.Now;
                        RecipientObj[count].fk_ReportID = recipient.ReportId;
                        DbMtData.CommitChanges();
                    }

                    count++;
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get ReportList
        /// Function Name       :   GetReportList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   06/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="recipient"></param>
        /// <returns></returns>
        public IEnumerable<Report> GetReportList()
        {
            try
            {
                var reportList = (from report in DbMtData.Reports where report.ReportId < 4 select new { report.ReportId, report.DisplayName, report.Name }).AsEnumerable()
                       .Select(objNew =>
                           new Report
                           {
                               ReportId = objNew.ReportId,
                               DisplayName = objNew.DisplayName,
                               Name = objNew.Name
                           }).ToList();

                return reportList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="reportId"></param>
        /// <returns></returns>
        public IEnumerable<Recipient> GetUserReceipent(int merchantId, int reportId)
        {
            try
            {
                var recipientList = (from m in DbMtData.Recipients join j in DbMtData.Users on m.fk_UserID equals j.UserID where m.fk_MerchantID == merchantId && m.fk_ReportID == reportId && (j.fk_UserTypeID == 3 || j.fk_UserTypeID == 4) && j.IsActive select new { m.fk_MerchantID, j.Email, j.FName, j.LName, m.fk_UserID, m.IsRecipient }).Distinct().AsEnumerable().Select(objNew => new Recipient { fk_MerchantID = objNew.fk_MerchantID, fk_UserID = objNew.fk_UserID, Email = objNew.Email, FName = objNew.FName, LName = objNew.LName, IsRecipient = objNew.IsRecipient }).ToList();
                return recipientList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<MerchantUser> SortedMerchantUser(string sorting, IEnumerable<MerchantUser> query)
        {
            switch (sorting)
            {
                case "Company ASC":
                    query = query.OrderBy(p => p.Company);
                    break;
                case "Company DESC":
                    query = query.OrderByDescending(p => p.Company);
                    break;
                case "LName ASC":
                    query = query.OrderBy(p => p.LName);
                    break;
                case "LName DESC":
                    query = query.OrderByDescending(p => p.LName);
                    break;
                case "Email ASC":
                    query = query.OrderBy(p => p.Email);
                    break;
                case "Email DESC":
                    query = query.OrderByDescending(p => p.Email);
                    break;
                case "CreatedDate ASC":
                    query = query.OrderBy(p => p.CreatedDate);
                    break;
                case "CreatedDate DESC":
                    query = query.OrderByDescending(p => p.CreatedDate);
                    break;
            }

            return query;
        }

        public bool UpdateTCPassword(int merchantId, string userName, string newPassword)
        {
            try
            {
                bool found = DbMtData.ExternalUsers.Any(m => m.MerchantId == merchantId);
                if (found)
                {
                    var userDb = DbMtData.ExternalUsers.FirstOrDefault(m => m.MerchantId == merchantId);
                    userDb.PCode = newPassword.Hash();
                    DbMtData.CommitChanges();
                    return true;
                }
                else
                {
                    ExternalUser extUser = new ExternalUser();
                    extUser.MerchantId = merchantId;
                    extUser.UserName = userName;
                    extUser.PCode = newPassword.Hash();
                    DbMtData.ExternalUsers.Add(extUser);
                    DbMtData.CommitChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string IsAllReadySet(int merchantId)
        {
            try
            {
                ExternalUser eUser = DbMtData.ExternalUsers.Where(m => m.MerchantId == merchantId).FirstOrDefault();
                if (eUser != null)
                    return eUser.UserName;
            }
            catch (Exception)
            {
                throw;
            }

            return string.Empty;
        }

        public bool IsUserAvailable(string userName)
        {
            return DbMtData.ExternalUsers.Any(m => m.UserName == userName);
        }

        public string GetMerchantZipCode(int merchantId)
        {
            return DbMtData.Merchants.Find(merchantId).ZipCode;
        }

        //PAY-13
        /// <summary>
        /// Get the IsTransArmor value from the Merchant.
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public bool IsTransArmor(int merchantId)
        {
            bool? useTA = DbMtData.Merchants.Find(merchantId).UseTransArmor;

            return (useTA.HasValue ? useTA.Value : false);
        }
    }
}
