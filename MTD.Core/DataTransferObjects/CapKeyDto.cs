﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
    public class CapKeyDto
    {
        public int Id { get; set; }
        public Nullable<System.DateTime> DownloadDate { get; set; }
        public string FileVersion { get; set; }
        public string XmlFile { get; set; }
        public string RawData { get; set; }
        public string RequestXML { get; set; }
        public string ResponseXML { get; set; }
        public Nullable<bool> IsNew { get; set; }
        public string FileCreationDate { get; set; }
        public string FileType { get; set; }
        public string FunCode { get; set; }
        public string FileSize { get; set; }
        public string FileCRC16 { get; set; }
        public Nullable<bool> IsContactLess { get; set; }
    }
}
