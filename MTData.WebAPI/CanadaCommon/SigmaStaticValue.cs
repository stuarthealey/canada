﻿namespace MTData.WebAPI.CanadaCommon
{
    public static class SigmaStaticValue
    {
        #region Declare constant for transaction message type

        /// <summary>
        /// Message Type for Credit Sale Request
        /// Value = "|01|00"
        /// </summary>
        public const string MessageType_ZeroOneZeroZero = "|01|00";

        /// <summary>
        ///Message Type for Credit Sale Response
        /// Value = "|01|10"
        /// </summary>
        public const string MessageType_ZeroOneOneZero = "|01|10";
        
        /// <summary>
        ///Message Type for Reversal Request
        /// Value = "|04|00"
        /// </summary>
        public const string MessageType_ZeroFourZeroZero = "|04|00";
        
        /// <summary>
        ///Message Type for Reversal Response
        /// Value = "|04|10"
        /// </summary>
        public const string MessageType_ZeroFourOneZero = "|04|10";
        
        /// <summary>
        ///Message Type for Debit Sale Request
        /// Value = "|02|00"
        /// </summary>
        public const string MessageType_ZeroTwoZeroZero = "|02|00";
        
        /// <summary>
        ///Message Type for Debit Sale Response
        /// Value = "|02|10"
        /// </summary>
        public const string MessageType_ZeroTwoOneZero = "|02|10";
        
        /// <summary>
        ///Message Type for Token only and Network Managment
        /// Value = "|08|00"
        /// </summary>
        public const string MessageType_TokenOnly = "|08|00";

        #endregion

        #region Merchant category code(MCC)

        /// <summary>
        /// Merchant Category Code for Retail. 
        /// Value="5999" 
        /// </summary>
        public const string MCCForRetail = "5999";

        /// <summary>
        /// Merchant Category Code for E-Com. 
        /// Value="5969" 
        /// </summary>
        public const string MCCForECom = "5969";

        /// <summary>
        /// Merchant Category Code for Direct Marketing. 
        /// Value="5968" 
        /// </summary>
        public const string MCCForDirectMarketing = "5968";

        /// <summary>
        /// Merchant Category Code for Restaurant. 
        /// Value="5812" 
        /// </summary>
        public const string MCCForRestaurant = "5812";

        #endregion

        #region Constant value for pos entry mode pin capability

        /// <summary>
        /// POS Entry Mode+ Pin Capability/Bitmap 22 for manual transaction.
        /// Value = "012" 
        /// "01":Manual/Key Entry and "2":Cannot accept PIN
        /// </summary>
        public const string POSEntryModePinCapabilityForManual = "010";

        /// <summary>
        /// POS Entry Mode+ Pin Capability/Bitmap 22 for manual transaction.
        /// Value = "012" 
        /// "01":Manual/Key Entry and "1":pin accepted.
        /// </summary>
        public const string POSEntryModePinCapabilityForManualRetail = "011";

        /// <summary>
        /// POS Entry Mode+ Pin Capability/Bitmap 22 for swiped transaction.
        /// Value = "902" 
        /// "90":Magnetic Stripe–CVV/CVC certified and "2":Cannot accept PIN
        /// </summary>
        public const string POSEntryModePinCapabilityForSwiped = "902";

        /// <summary>
        /// POS Entry Mode+ Pin Capability/Bitmap 22 for swiped transaction.
        /// Value = "901" 
        /// "90":Magnetic Stripe–CVV/CVC certified and "1":Can accept PIN
        /// </summary>
        public const string POSEntryModePinCapabilityForSwipedWithPin = "901";

        /// <summary>
        /// Pos Entry Mode+Pin Capability for MSR contactless.
        /// </summary>
        public const string POSEntryModePinCapabilityForMSRContactless = "911";

        public const string POSEntryModePinCapabilityForChipNPin = "051";

        public const string POSEntryModePinCapabilityForFallback = "801";

        #endregion

        #region Declare constant for processing code

        /// <summary>
        /// Set the const value of processing code for Credit Sale, Credit Reversal.
        /// Value = "000000"
        /// </summary>
        public const string ProcessingCodeForCreditSale = "000000";

        /// <summary>
        /// Set the const value of processing code for credit Refund, Referral and Partial Reversal.
        /// Value = "200000"
        /// </summary>
        public const string ProcessingCodeForCreditRefund = "200000";

        /// <summary>
        /// Set the const value of processing code for debit reversal.
        /// Value = "009000"
        /// </summary>
        public const string ProcessingCodeForDebitReversal = "009000";

        /// <summary>
        /// Set the const value of processing code for debit sale pin-less
        /// Value="509000"
        /// </summary>
        public const string ProcessingCodeForPinlessDebitSale = "509000";

        /// <summary>
        /// Set the const value of processing code for debit cashBack.
        /// Value = "099000"
        /// </summary>
        public const string ProcessingCodeForDebitCashBack = "099000";

        /// <summary>
        /// Set the const value of processing code for debit refund.
        /// Value = "209000"
        /// </summary>
        public const string ProcessingCodeForDebitRefund = "209000";

        /// <summary>
        /// Set the const value of processing code for recurring bill payment.
        /// Value = "500000"
        /// </summary>
        public const string ProcessingCodeBillPaymentNRecurring = "500000";

        /// <summary>
        /// Set the const value of processing code for pin debit sale.
        /// Value = "009000"
        /// </summary>
        public const string ProcessingCodeForPinDebitSale = "009000";

        /// <summary>
        /// Set the const value of processing code for credit cash back
        /// Value = "009000"
        /// </summary>
        public const string  ProcessingCodeForCreditCashBack= "090000";

        #endregion  

        #region Constant value for Pos condition code

        /// <summary>
        /// Pos Condition Code/Bit 25 for Normal Presentment (i.e., customer is present with card).
        /// Value = "00" 
        /// </summary>
        public const string PosConditionCodeZeroZero = "00";

        /// <summary>
        /// Pos Condition Code/Bit 25 In case of Visa card AVS transaction.
        /// Value = "71"        
        /// </summary>
        public const string PosConditionCodeVisaAvs = "71";

        /// <summary>
        /// Pos Condition Code/Bit 25 For AVS check transaction 
        /// Value = "52"
        /// </summary>
        public const string PosConditionCodeAVSCheck = "52";

        /// <summary>
        /// Pos Condition Code/Bit 25 For CVV check transaction. 
        /// Value = "51"
        /// </summary>
        public const string PosConditionCodeCVV = "51";

        /// <summary>
        /// Pos Condition Code/Bit 25 For E-Com  transaction.
        /// Value = "59"
        /// </summary>
        public const string PosConditionCodeFiveNine = "59";

        /// <summary>
        /// Pos Condition Code/Bit 25 For Recurring  transaction. 
        /// Value = "04"
        /// </summary>
        public const string PosConditionCodeRecurring = "04";

        /// <summary>
        /// Pos Condition Code/Bit 25 for Voice response Unit (VRU)/Telephone.
        /// Valid value for Debit Card PIN-Less Voice Response Unit or Telephone Transactions
        /// Value = "00" 
        /// </summary>
        public const string PosConditionCodeZeroSeven = "07";

        /// <summary>
        /// Pos Condition Code/Bit 25 For MailPhone order transaction. 
        /// Value = "08"
        /// </summary>
        public const string PosConditionCodeZeroEight = "08";
        
        #endregion

        #region Constant value for Table14 related data
        
        /// <summary>
        /// Get table 14 Id in Hex.
        /// </summary>
        public const string Table14IdInHex = "3134";

        /// <summary>
        /// Get table number 14.
        /// </summary>
        public const string TableNo14 = "14";
        
        #endregion

        public const string CurrencyCode = "124";

        /// <summary>
        /// Get the Value "00" for response code 00.
        /// </summary>
        public const string ResponseCodeZeroZero = "00";

        //zzz Bad hard-coded values!
        //public const string TerminalId = "01351646";
        //public const string MerchantId = "000082004330015";

        /// <summary>
        /// Get the Value "0" from Zero
        /// </summary>
        public const char Zero = '0';

        /// <summary>
        /// Get the Value "|" from pipe
        /// </summary>
        public const char Pipe = '|';

        /// <summary>
        /// Get the value "1C" from OneC.
        /// </summary>
        public const string OneC = "1C";

        /// <summary>
        /// Get the Value "=" from split.
        /// </summary>
        public const char SplitEquals = '=';

        /// <summary>
        /// Get the Value "|20" from PI20
        /// </summary>
        public const string PI20 = "|20";

        /// <summary>
        /// Get the Value "#" from Hash
        /// </summary>
        public const string Hash = "#";

        /// <summary>
        /// Get the Value "MMddHHmmss" from DateTimeFormatMMddHHmmss
        /// </summary>
        public const string DateTimeFormatMMddHHmmss = "MMddHHmmss";

        /// <summary>
        /// Get the Value "MMddHHmmss" from DateTimeFormatMMddHHmmss
        /// </summary>
        public const string DateTimeFormatMMdd = "MMdd";

        /// <summary>
        ///Get the Value "hhmmss" from TimeFormathhmmss.
        /// </summary>
        public const string TimeFormathhmmss = "hhmmss";

        /// <summary>
        /// Get the Value "01" for Aditional pos data.
        /// This is suggested by FirstData for Credit
        /// Sale/Full reversal and Pinless debit transactions.
        /// </summary>
        public const string AdditionalPossData = "00";

        /// <summary>
        /// Get the Value "42" for Aditional pos data.
        /// This is suggested \
        /// by FirstData for Credit for Retail industry.
        /// </summary>
        public const string AdditionalPossDataForRetailContact = "02";

        /// <summary>
        /// Get additional pos data for MSR contactless
        /// </summary>
        public const string AdditionalPossDataForMSRContactless = "77"; //"07";

        public const string AdditionalPossDataForRetail = "75";

        /// <summary>
        /// Get the Value "001" for First Data NII excluding Canadian Debit and Credit.
        /// Get the Value "047" for Canadian processing Debit and Credit.
        /// </summary>
        public const string NetworkInternationId = "047";

        public const string MerchantZipCode = "|09L4W 5A4  ";

        public const string StreetAddress = "|00|3199L4W 5A4  1 Westminster       ";

        /// <summary>
        /// Get the value Dot(.). 
        /// </summary>
        public const string Dot = ".";

        /// <summary>
        /// Get Single Space(" ").
        /// </summary>
        public const string SingleSpace = " ";

        /// <summary>
        /// Debit reversal default pin.
        /// </summary>
        public const string DebitReversalDefaultPin = "0000000000000000";

        #region Acquirer Reference Data

        public const string AcquirerRefDataForAuthOnly = "0";
        public const string AcquirerRefDataForAuthCapture = "1";
        public const string AcquirerRefDataForCaptureOnly = "2";

        #endregion

        public const string FourZeroes = "0000";

        public const string ThreeZeroes = "000";

        public const string AcquiringId = "000000000001";

        public const string ResponseCode = "ResponseCode";

        public const string UnknownResponseCode = "UnknownResponseCode";

        public const string NineSpaces = "         ";
        public const string TwentySpaces = "                    ";

        public const string BookerAppToken = "BookerAppToken";

        public const string BookerAppPayment = "BookerAppPayment";

        public const string TokenOnly = "TokenOnly";

        public const string NetworkInfoCodeForTokenOnly = "|08|87";

        //Changes done for EMV Contact certification of Canada

        public const string EncryptedPinDataForInterac = "FFFFFFFFFFFFFFFF";

        public const char OneF = 'F';

        public const char QuestionMark = '?';

        public const char SemiColon = ';';

        public const string KeyUpdateRequest = "KeyUpdateRequest";
        public const string NetworkInfoCodeForKeyUpdateRequest = "|08|42";

        #region For canada capkey
        
        public const string NetworkInfoCodeForCapKeyDownload = "|09|40";
        public const string CapkeyIndustry = "CAPKDownload";
        // public const string CapkeyDownload = "FileDownload";     //SH 2018-08-21 Unused constants, to be deleted later when confirmed not used.
        // public const string CapKeyMessage = "CapKeyDownload";    //SH 2018-08-21 Unused constants, to be deleted later when confirmed not used.

        #endregion

        public const string AdditionalPossDataForEMVContact = "75";
        public const string AdditionalPossDataForEMVContactless = "76";
        public const string POSEntryModePinCapabilityForEMVContactless = "071";
        public const string POSEntryModeForEMVFallback = "80";

        public const string NewKeyRequest = "NewKeyRequest";
        public const string NetworkInfoCodeForNewKeyRequest = "|08|11";

        public const string SaleSavingProcessingCode = "001000";
        public const string SaleCheckingProcessingCode = "002000";
        public const string VoidSavingProcessingCode = "220010";
        public const string VoidCheckingProcessingCode = "220020";

        public const string TableSDTagLN = "LN008";
        public const string Tag9F6E = "9F6E";

        public const string InteracAID = "A0000002771010";

        public const string Registered = "Registered";
        public const string Expired = "Expired";
        public const string FileDownload = "FileDownload";
        public const string NoUpdateAvailable = "No update available";
        public const string FileTransferSuccess = "File transfer successful";
        public const string Error = "Error";

        public const string ReversalReasonZeroZero = "00";
        public const string ReversalReasonTimeout = "01";
        public const string ReversalReasonMACVerification = "30";
        public const string ReversalReasonMACSynch = "31";
    }
}