﻿using System;
using System.Collections.Generic;
using System.Linq;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Data.Repository
{

    public class ReceiptRepository : BaseRepository, IReceiptRepository
    {
        public ReceiptRepository(IMtDataDevlopmentsEntities dbMtData)
            : base(dbMtData)
        {

        }

        /// <summary>
        /// Purpose             :   To get list receipts
        /// Function Name       :   ReceiptList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<ReceiptFormat> ReceiptList(int merchantId, string name, int logOnByUserId, int logOnByUserType)
        {

            IEnumerable<ReceiptFormat> list = (from m in DbMtData.FleetReceipts
                                               join n in DbMtData.Fleets on m.fk_FleetID equals n.FleetID
                                               where ((bool)m.IsActive && n.fk_MerchantID == merchantId)
                                               select new ReceiptFormat
                                               {
                                                   FleetID = n.FleetID,
                                                   FleetName = n.FleetName,

                                               }).Distinct().ToList();
            if (logOnByUserType == 5)
            {
                List<int> fleetListAccessed = GetAccesibleFleet(logOnByUserId,merchantId);
                List<ReceiptFormat> receiptViewResult = list.Where(m => fleetListAccessed.Contains((int)m.FleetID)).Select(m => m).Distinct().ToList();
                list = receiptViewResult.AsEnumerable();
            }
            if (!string.IsNullOrEmpty(name))
            {
                list = list.Where(p => p.FleetName.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
            }
            return list;
        }

        /// <summary>
        /// Purpose             :   To get  receipt fleet 
        /// Function Name       :   GetReceiptFleet
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public IEnumerable<fleetReceiptTemp> GetReceiptFleet(int fleetId)
        {

            try
            {
                if (fleetId == 0)
                {
                    bool isFound = DbMtData.FleetReceipts.Any(x => x.fk_FleetID == null);
                    if (isFound)
                    {
                        var list = (from m in DbMtData.FleetReceipts
                                    join k in DbMtData.ReceiptMasters on m.fk_FieldID equals k.FieldID
                                    where ((bool)m.IsActive && m.fk_FleetID == null)
                                    select new fleetReceiptTemp
                                        {
                                            FieldName = k.FieldName,
                                            FleetReceiptID = m.FleetReceiptID,
                                            fk_FieldID = m.fk_FieldID,
                                            fk_FieldText = m.fk_FieldText,
                                            fk_FleetID = m.fk_FleetID,
                                            Postion = m.Postion,
                                            IsShow = m.IsShow
                                        }).ToList();

                        return list;
                    }
                }
                else
                {
                    var fleetlist = (from m in DbMtData.FleetReceipts
                                     join n in DbMtData.Fleets on m.fk_FleetID equals n.FleetID
                                     join k in DbMtData.ReceiptMasters on m.fk_FieldID equals k.FieldID
                                     where ((bool)m.IsActive && m.fk_FleetID == fleetId)
                                     select new fleetReceiptTemp
                                     {
                                         FieldName = k.FieldName,
                                         FleetReceiptID = m.FleetReceiptID,
                                         fk_FieldID = m.fk_FieldID,
                                         fk_FieldText = m.fk_FieldText,
                                         fk_FleetID = m.fk_FleetID,
                                         Postion = m.Postion,
                                         IsShow = m.IsShow
                                     }).ToList();

                    return fleetlist;
                }


            }
            catch (Exception)
            {

                throw;
            }

            return null;

        }

        /// <summary>
        /// Purpose             :   To get fleets of user
        /// Function Name       :   GetUserFleet
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<Fleet> GetUserFleet(int merchantId)
        {
            IEnumerable<Fleet> fleetList;

            try
            {


                fleetList = (from fleet in DbMtData.Fleets where fleet.fk_MerchantID == merchantId && fleet.IsActive select new { fleet.FleetID, fleet.FleetName }).AsEnumerable()
                        .Select(objNew =>
                            new Fleet
                            {
                                FleetID = objNew.FleetID,
                                FleetName = objNew.FleetName

                            }).ToList();
                return fleetList;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To delete receipts
        /// Function Name       :   DeleteReceipt
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="fleetId"></param>
        public void DeleteReceipt(int fleetId)
        {
            try
            {
                Fleet fleetObj = DbMtData.Fleets.Find(fleetId);
                var receiptDb = DbMtData.FleetReceipts.Where(x => x.fk_FleetID == fleetId).ToList();
                receiptDb.ForEach(x => x.IsActive = false);
                fleetObj.IsReceipt = false;
                DbMtData.CommitChanges();
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To delete receipts
        /// Function Name       :   DeleteReceipt
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="fleetId"></param>
        public void DeleteReceipt(int fleetId, int merchantId)
        {
            try
            {
                Fleet fleetObj = DbMtData.Fleets.Where(x => x.FleetID == fleetId && x.fk_MerchantID == merchantId).FirstOrDefault();
                var receiptDb = DbMtData.FleetReceipts.Where(x => x.fk_FleetID == fleetId).ToList();
                receiptDb.ForEach(x => x.IsActive = false);
                fleetObj.IsReceipt = false;
                DbMtData.CommitChanges();
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To check field exist
        /// Function Name       :   IsFieldExist
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="fieldId"></param>
        /// <returns></returns>
        public bool IsFieldExist(int fieldId)
        {
            try
            {
                return DbMtData.FleetReceipts.Any(x => x.fk_FieldID == fieldId && (bool)x.IsActive);
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To delete receipt field
        /// Function Name       :   DeleteReceiptField
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"  
        /// </summary>
        /// <param name="fieldId"></param>
        public void DeleteReceiptField(int fieldId)
        {
            try
            {
                var receiptDb = DbMtData.ReceiptMasters.Find(fieldId);
                receiptDb.IsActive = false;
                DbMtData.CommitChanges();
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To add receipt
        /// Function Name       :   AddReciept
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"   
        /// </summary>
        /// <param name="reciept"></param>
        public void AddReciept(ReceiptFormat reciept)
        {
            FleetReceipt[] createObj = new FleetReceipt[reciept.recceiptMaster.Count()];
            Fleet fleetObj = DbMtData.Fleets.Find(Convert.ToInt32(reciept.fk_FleetID));
            var find = reciept.recceiptMaster.FirstOrDefault();
            try
            {
                if (reciept.fk_FleetID == 0)
                {
                    bool isFound = DbMtData.FleetReceipts.Any(x => x.fk_FleetID == null);
                    if (!isFound)
                    {
                        int count = 0;
                        foreach (var item in reciept.recceiptMaster)
                        {
                            createObj[count] = new FleetReceipt();
                            createObj[count].fk_FieldText = item.FieldName;
                            createObj[count].fk_FleetID = null;
                            createObj[count].fk_FieldID = item.FieldID;
                            createObj[count].Postion = item.Position;
                            createObj[count].CreatedDate = DateTime.Now;
                            createObj[count].ModifiedDate = DateTime.Now;
                            createObj[count].ModifiedBy = reciept.ModifiedBy;
                            createObj[count].CreatedBy = reciept.CreatedBy;
                            createObj[count].IsShow = item.IsShow;
                            createObj[count].IsActive = true;
                            DbMtData.FleetReceipts.Add(createObj[count]);
                            count++;
                        }

                    }

                }
                else if (find.fk_FieldText == null)
                {
                    int count = 0;
                    foreach (var item in reciept.recceiptMaster)
                    {
                        createObj[count] = new FleetReceipt();
                        createObj[count].fk_FieldText = item.FieldName;
                        createObj[count].fk_FleetID = reciept.fk_FleetID;
                        createObj[count].fk_FieldID = item.FieldID;
                        createObj[count].Postion = item.Position;
                        createObj[count].CreatedDate = DateTime.Now;
                        createObj[count].ModifiedDate = DateTime.Now;
                        createObj[count].ModifiedBy = reciept.ModifiedBy;
                        createObj[count].CreatedBy = reciept.CreatedBy;
                        createObj[count].IsShow = item.IsShow;
                        createObj[count].IsActive = true;
                        DbMtData.FleetReceipts.Add(createObj[count]);
                        count++;
                    }
                    fleetObj.IsReceipt = true;
                }
                else
                {
                    int count = 0;
                    foreach (var item in reciept.recceiptMaster)
                    {
                        createObj[count] = new FleetReceipt();
                        createObj[count].fk_FieldText = item.fk_FieldText;
                        createObj[count].fk_FleetID = reciept.fk_FleetID;
                        createObj[count].fk_FieldID = item.fk_FieldID;
                        createObj[count].Postion = item.Postion;
                        createObj[count].CreatedDate = DateTime.Now;
                        createObj[count].ModifiedDate = DateTime.Now;
                        createObj[count].ModifiedBy = reciept.ModifiedBy;
                        createObj[count].CreatedBy = reciept.CreatedBy;
                        createObj[count].IsShow = item.IsShow;
                        createObj[count].IsActive = true;
                        DbMtData.FleetReceipts.Add(createObj[count]);
                        count++;
                    }
                    fleetObj.IsReceipt = true;
                }

                DbMtData.CommitChanges();

            }
            catch (Exception)
            {

                throw;
            }



        }

        /// <summary>
        /// Purpose             :   To add receipt field
        /// Function Name       :   AddRecieptField
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"   
        /// </summary>
        /// <param name="field"></param>
        /// <param name="receiptMaster"></param>
        public void AddRecieptField(string field, ReceiptMaster receiptMaster)
        {
            var objRec = new ReceiptMaster();
            objRec.FieldName = field;
            objRec.CreatedBy = receiptMaster.CreatedBy;
            objRec.ModifiedBy = receiptMaster.ModifiedBy;
            objRec.CreatedDate = DateTime.Now;
            objRec.ModifiedDate = DateTime.Now;
            objRec.IsActive = true;
            DbMtData.ReceiptMasters.Add(objRec);
            DbMtData.CommitChanges();
        }

        /// <summary>
        /// Purpose             :   To check fleet exist
        /// Function Name       :   IsExistFleet
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"   
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public bool IsExistFleet(int fleetId)
        {
            try
            {

                return DbMtData.FleetReceipts.Any(x => x.fk_FleetID == fleetId && (bool)x.IsActive);

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To check fleet exist for update
        /// Function Name       :   IsExistFleetForUpdate
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"   
        /// </summary>
        /// <param name="fleetId"></param>
        /// <param name="receiptId"></param>
        /// <returns></returns>
        public bool IsExistFleetForUpdate(int fleetId, int receiptId)
        {
            try
            {
                FleetReceipt fleetReceipt = DbMtData.FleetReceipts.SingleOrDefault(x => x.FleetReceiptID == receiptId && (bool)x.IsActive);
                if (fleetReceipt != null)
                {
                    if (fleetId == fleetReceipt.fk_FleetID)
                    {
                        return false;
                    }
                    var result = DbMtData.FleetReceipts.Where(x => x.fk_FleetID == fleetId && (bool)x.IsActive).ToList();
                    if (result.Count != 0)
                    {
                        return true;
                    }
                    return false;
                }

            }
            catch (Exception)
            {
                throw;
            }
            return false;
        }

        /// <summary>
        /// Purpose             :   To update receipt
        /// Function Name       :   Update
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"   
        /// </summary>
        /// <param name="reciept"></param>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public bool Update(ReceiptFormat reciept, int fleetId)
        {
            try
            {
                if (fleetId == 0)
                {
                    bool IsFound = DbMtData.FleetReceipts.Any(x => x.fk_FleetID == null);
                    if (IsFound)
                    {
                        foreach (var item in reciept.fleetReceipt)
                        {
                            FleetReceipt updateObj = DbMtData.FleetReceipts.Find(Convert.ToInt32(item.FleetReceiptID));
                            if (updateObj != null)
                            {
                                updateObj.fk_FleetID = null;
                                updateObj.fk_FieldText = item.fk_FieldText;
                                updateObj.Postion = item.Postion;
                                updateObj.IsShow = item.IsShow;
                                updateObj.CreatedBy = reciept.CreatedBy;
                                updateObj.CreatedDate = DateTime.Now;
                                updateObj.ModifiedDate = DateTime.Now;
                                updateObj.ModifiedBy = reciept.ModifiedBy;
                                updateObj.IsActive = true;
                            }
                        }
                    }
                }
                else
                {
                    foreach (var item in reciept.fleetReceipt)
                    {
                        FleetReceipt updateObj = DbMtData.FleetReceipts.Find(Convert.ToInt32(item.FleetReceiptID));
                        if (updateObj != null)
                        {
                            updateObj.fk_FleetID = reciept.fk_FleetID;
                            updateObj.fk_FieldText = item.fk_FieldText;
                            updateObj.Postion = item.Postion;
                            updateObj.IsShow = item.IsShow;
                            updateObj.CreatedBy = reciept.CreatedBy;
                            updateObj.CreatedDate = DateTime.Now;
                            updateObj.ModifiedDate = DateTime.Now;
                            updateObj.ModifiedBy = reciept.ModifiedBy;
                            updateObj.IsActive = true;
                        }
                    }
                }

                return Convert.ToBoolean(DbMtData.CommitChanges());
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get receipt by fleet Id
        /// Function Name       :   ReceiptByfleetId
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   Madhuri Tanwar
        /// Modidified On       :   08/21/2015 
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public IEnumerable<ReceiptList> ReceiptByfleetId(int fleetId)
        {
            //var recieptDb = DbMtData.Receipts.FirstOrDefault(a => a.fk_FleetID.Equals(fleetId));
            List<ReceiptList> recieptDb;
            recieptDb = (from fr in DbMtData.FleetReceipts
                         join rm in DbMtData.ReceiptMasters on fr.fk_FieldID equals rm.FieldID
                         where fr.fk_FleetID == fleetId && (bool)fr.IsActive
                         select new ReceiptList
                         {
                             fk_FieldText = fr.fk_FieldText,
                             Postion = fr.Postion,
                             IsShow = fr.IsShow,
                             FieldName = rm.FieldName
                         }).ToList();

            if (recieptDb == null || recieptDb.Count.Equals(0))
            {
                recieptDb = (from fr in DbMtData.FleetReceipts
                             join rm in DbMtData.ReceiptMasters on fr.fk_FieldID equals rm.FieldID
                             where fr.fk_FleetID == null && (bool)fr.IsActive
                             select new ReceiptList
                             {
                                 fk_FieldText = fr.fk_FieldText,
                                 Postion = fr.Postion,
                                 IsShow = fr.IsShow,
                                 FieldName = rm.FieldName
                             }).ToList();
            }


            return recieptDb;
        }

        /// <summary>
        /// Purpose             :   To show receipt based on fleet Id
        /// Function Name       :   ReceiptToShowByfleetId
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"   
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public IEnumerable<ReceiptList> ReceiptToShowByfleetId(int fleetId)
        {
            //var recieptDb = DbMtData.Receipts.FirstOrDefault(a => a.fk_FleetID.Equals(fleetId));

            try
            {
                List<ReceiptList> list = new List<ReceiptList>();
                foreach (ReceiptList receiptList in (DbMtData.FleetReceipts.Join(DbMtData.ReceiptMasters, fr => fr.fk_FieldID, rm => rm.FieldID, (fr, rm) => new { fr, rm }).Where(@t => @t.fr.fk_FleetID == fleetId && (bool)@t.fr.IsActive && (bool)@t.fr.IsShow).OrderBy(@t => @t.fr.Postion).Select(@t => new ReceiptList
                {
                    fk_FieldText = @t.fr.fk_FieldText.Trim(),
                    Postion = @t.fr.Postion,
                    IsShow = @t.fr.IsShow,
                    FieldName = @t.rm.FieldName.Trim()
                })))
                    list.Add(receiptList);
                if (list.Count == 0)//For default receipt
                    foreach (ReceiptList receiptList in (DbMtData.FleetReceipts.Join(DbMtData.ReceiptMasters, fr => fr.fk_FieldID, rm => rm.FieldID, (fr, rm) => new { fr, rm }).Where(@t => @t.fr.fk_FleetID == null && (bool)@t.fr.IsActive && (bool)@t.fr.IsShow).OrderBy(@t => @t.fr.Postion).Select(@t => new ReceiptList
                    {
                        fk_FieldText = @t.fr.fk_FieldText.Trim(),
                        Postion = @t.fr.Postion,
                        IsShow = @t.fr.IsShow,
                        FieldName = @t.rm.FieldName.Trim()
                    })))
                        list.Add(receiptList);
                return list;
            }
            catch
            {
                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To get field of receipt
        /// Function Name       :   GetReceiptFields
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"    
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ReceiptMaster> GetReceiptFields()
        {
            try
            {
                IEnumerable<ReceiptMaster> recMaster = (from master in DbMtData.ReceiptMasters where (bool)master.IsActive select master).AsEnumerable()
                    .Select(objNew =>
                        new ReceiptMaster
                        {
                            FieldID = objNew.FieldID,
                            FieldName = objNew.FieldName

                        }).ToList();
                return recMaster;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To update master receipt
        /// Function Name       :   UpdateReceiptMaster
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/11/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"     
        /// </summary>
        /// <param name="receiptMaster"></param>
        /// <returns></returns>
        public bool UpdateReceiptMaster(ReceiptMaster receiptMaster)
        {

            int count = 0;
            try
            {
                for (int i = 0; i <= receiptMaster.RecieptCol.Count; i++ )
                {
                    var objRec = receiptMaster.RecieptCol[count];
                    var fields = objRec.Split(':');
                    var updateObj = DbMtData.ReceiptMasters.Find(Convert.ToInt32(fields[1]));
                    if (updateObj != null)
                    {
                        updateObj.FieldName = fields[0];
                        updateObj.CreatedBy = receiptMaster.CreatedBy;
                        updateObj.CreatedDate = DateTime.Now;
                        updateObj.ModifiedDate = DateTime.Now;
                        updateObj.ModifiedBy = receiptMaster.ModifiedBy;
                        updateObj.IsActive = true;
                        count++;
                    }

                }
                return Convert.ToBoolean(DbMtData.CommitChanges());
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get receipt list by filter
        /// Function Name       :   ReceiptListByFilter
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/11/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"     
        /// </summary>
        /// <param name="name"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <param name="sorting"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<ReceiptFormat> ReceiptListByFilter(FilterSearchParameters filterSearch, int logOnByUserId, int logOnByUserType)
        {
            IEnumerable<ReceiptFormat> list = (from m in DbMtData.FleetReceipts
                                               join n in DbMtData.Fleets on m.fk_FleetID equals n.FleetID
                                               where ((bool)m.IsActive && n.fk_MerchantID == filterSearch.MerchantId)
                                               select new ReceiptFormat
                                               {
                                                   FleetID = n.FleetID,
                                                   FleetName = n.FleetName,

                                               }).Distinct().ToList();
            if (logOnByUserType == 5)
            {
                List<int> fleetListAccessed = GetAccesibleFleet(logOnByUserId, filterSearch.MerchantId);
                List<ReceiptFormat> receiptViewResult = list.Where(m => fleetListAccessed.Contains((int)m.FleetID)).Select(m => m).Distinct().ToList();
                list = receiptViewResult.AsEnumerable();
            }
            if (!string.IsNullOrEmpty(filterSearch.Name))
            {
                list = list.Where(p => p.FleetName.IndexOf(filterSearch.Name, StringComparison.OrdinalIgnoreCase) >= 0);
            }
            if (!string.IsNullOrEmpty(filterSearch.Sorting))
            {
                list = SortedReceiptFormat(filterSearch.Sorting, list);
            }
            return filterSearch.PageSize > 0
                       ? list.Skip(filterSearch.StartIndex).Take(filterSearch.PageSize).ToList() //Paging
                       : list.ToList();
        }

        /// <summary>
        /// Purpose             :   To Get Fleet List
        /// Function Name       :   GetFleetList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/12/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"     
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Fleet> GetFleetList(int merchantId)
        {
            IEnumerable<Fleet> fleetList;

            try
            {


                fleetList = (from fleet in DbMtData.Fleets where fleet.fk_MerchantID == merchantId && fleet.IsActive && !(bool)fleet.IsReceipt select new { fleet.FleetID, fleet.FleetName }).AsEnumerable()
                        .Select(objNew =>
                            new Fleet
                            {
                                FleetID = objNew.FleetID,
                                FleetName = objNew.FleetName

                            }).ToList();
                return fleetList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        private IEnumerable<ReceiptFormat> SortedReceiptFormat(string sorting, IEnumerable<ReceiptFormat> list)
        {
            switch (sorting)
            {
                case "FleetName ASC":
                    list = list.OrderBy(p => p.FleetName);
                    break;
                case "FleetName DESC":
                    list = list.OrderByDescending(p => p.FleetName);
                    break;
                default:
                    list = list.OrderBy(p => p.FleetName); //Default!
                    break;
            }
            return list;
        }

        /// <summary>
        /// Purpose             :   to get the accessible fleets 
        /// Function Name       :   GetAccesibleFleet
        /// Created By          :   Naveen Kumar
        /// Created On          :   10/15/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public List<int> GetAccesibleFleet(int logOnByUserId, int merchantId)
        {
            List<int> accessibleFleets = (from m in DbMtData.UserFleets
                                          where m.fk_UserID == logOnByUserId && m.fk_MerchantId == merchantId
                                          && m.fk_UserTypeId == 5 && m.IsActive == true
                                          select (int)m.fk_FleetID).Distinct().ToList();
            return accessibleFleets;
        }

    }

}
