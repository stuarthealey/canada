﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MTData.Web.ViewData
{
    public class CarOwnerViewData
    {
        public int Id { get; set; }
        public string OwnerName { get; set; }
        [DataType(DataType.PhoneNumber)]
        [MinLength(6, ErrorMessage = "Contact Number  must be minimum length of 6")]
        public string Contact { get; set; }
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Email is not valid")]
        public string EmailAddress { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Address { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string OwnerDetail { get; set; }
        public bool IsCarOwnerError { get; set; }
        public string FailedError { get; set; }
        public Nullable<int> fk_MerchantId { get; set; }
        public string ContactPerson { get; set; }
        public string SageAccount { get; set; }
        public string BankName { get; set; }
        public string AccountName { get; set; }
        public string BSB { get; set; }
        public string Account { get; set; }
        public List<CarOwnerViewData> CarOwnerListViewData { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public IEnumerable<CountryViewData> CountryList { get; set; }
    }
}