﻿namespace MTD.Core.RapidConnect
{
    public class GetDidStatus
    {
        /// <summary>
        /// gets or sets the IsRegistered.
        /// </summary>
        public bool IsRegisterd { get; set; }

        /// <summary>
        /// gets or sets the DatawireID
        /// </summary>
        public string DatawireID { get; set; }
    }
}
