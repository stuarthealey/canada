﻿#region Used Namespace
using System;
using System.Net.Mail;
using System.IO;
#endregion

namespace MTData.Service.Models
{
    /// <summary>
    /// contains email sending logic
    /// </summary>
    public class EmailUtil
    {
        #region Global Variables
        string smtpHost;
        string smtpPort;
        string userName;
        string password;
        string fromAddress;
        string displayName;
        bool IsTestEmailID;
        bool IsEnableSsl;
        string subject;
        string path;

        SmtpClient smtpClient;
        #endregion

        #region Methods

        /// <summary>
        /// Constructor Used initialize member variable of class
        /// </summary>
        public EmailUtil()
        {
            //Setings for mail sending
            smtpHost = System.Configuration.ConfigurationManager.AppSettings["SmtpHost"];
            smtpPort = System.Configuration.ConfigurationManager.AppSettings["SmtpPort"];
            userName = System.Configuration.ConfigurationManager.AppSettings["SmtpUsername"];
            password = System.Configuration.ConfigurationManager.AppSettings["SmtpPassword"];
            IsTestEmailID = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsTestEmailID"]);
            IsEnableSsl = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsEnableSsl"]);
            subject = System.Configuration.ConfigurationManager.AppSettings["Subject"];
            fromAddress = System.Configuration.ConfigurationManager.AppSettings["FromAddress"];
            //set client credential and sent mail
            smtpClient = new SmtpClient();
            smtpClient.Host = smtpHost;
            smtpClient.Port = Convert.ToInt32(smtpPort);
            smtpClient.EnableSsl = IsEnableSsl;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(userName, password);
        }

        /// <summary>
        /// <summary>
        /// Purpose : contains email sending logic
        /// Function Name : SendMail
        /// Created By : Pankaj Singh
        /// Created On : 04/29/2011 "MM/DD/YYYY"
        /// Modificatios Made : ************************
        /// Modidified On : ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// </summary>
        /// <param name="emailId">Email ID</param>
        /// <param name="subject">Subject</param>
        /// <param name="mailDetails">Message Body</param>
        /// <returns></returns>
        public bool SendMail(string emailId, string mailDetails, string displayName)
        {
            MailMessage message = new MailMessage();

            this.displayName = displayName;

            try
            {

                //set message values
                AlternateView avHtml = AlternateView.CreateAlternateViewFromString(mailDetails, null, "text/html");
                message.From = new MailAddress(userName, displayName);
                message.To.Add(new MailAddress(emailId));
                message.Subject = subject;
                message.AlternateViews.Add(avHtml);
                smtpClient.Send(message);
                return true;
            }
            catch
            {
                //Log.Exception(ex);
                return false;
            }
        }

        #endregion
    }
}
