﻿using System.Collections.Generic;

using MTD.Core.DataTransferObjects;

namespace MTD.Core.Service.Interface
{
    public interface IUserService
    {
        bool Add(UserDto userDto);

        IEnumerable<RoleDto> GetUserRoles(int userId);

        IEnumerable<UserDto> UserList(int userId, string name, int logOnByUserId, int logOnByUserType);

        bool Update(UserDto userDto, int userId);

        bool UpdateUserProfile(UserDto userDto, int userId);

        IEnumerable<UserFleetDto> GetUsersFleet(int userId);

        UserDto GetUserUpdate(int userId, int merchantId);

        UserDto GetUser(int userId, int val);

        UserDto GetUser(int userId);

        void DeleteUser(int userId);

        IEnumerable<FleetDto> FleetList(int merchantId);

        IEnumerable<FleetDto> UserFleetLists(int userId);
        
        IEnumerable<UserDto> UserListByFilter(FilterSearchParametersDto filterSearch,int logOnByUserId,int logOnByUserType);

        void ChangeStatus(int UserId);

        IEnumerable<FleetListDto> UserFleetList(int UserId);

        IEnumerable<FleetDto> PayerFleetList(int PayerId, int merchantId);

        List<int> GetAccesibleUserFleet(List<int> fleets);

        IEnumerable<UserFleetDto> GetUsersPayerFleet(int userId, int PayerId);
    }
}
