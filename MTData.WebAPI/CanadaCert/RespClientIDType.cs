﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CanadaCert
{
    public class RespClientIDType
    {
        public string DID { get; set; }
        public string ClientRef { get; set; }
    }    
}