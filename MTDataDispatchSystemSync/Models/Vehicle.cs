﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTDataDispatchSystemSync.Models
{
    public class Vehicle
    {
        public int VehicleID { get; set; }
        public string CarNumber { get; set; }
        public string PrimaryFleet { get; set; }
        public string PrimaryCity { get; set; }
        public string Registration { get; set; }
        public string TaxiLicence { get; set; }
        public string ValidationReason { get; set; }
        public string InhibitedReason { get; set; }
        public string InsurancePolicy { get; set; }
        public string InsurancePolicyCompany { get; set; }
        public bool IsActive { get; set; }
        public int MaxPax { get; set; }
        public string VehicleMake { get; set; }
        public string VehicleModel { get; set; }
        public string VehicleMonth { get; set; }
        public string VehicleYear { get; set; }
        public string VehicleType { get; set; }
        public string FuelType { get; set; }
        public string Comments { get; set; }
        public string VIN { get; set; }
        public List<Lease> Lease { get; set; }
        public decimal PurchaseCost { get; set; }
        public int Odometer { get; set; }
        public int LastServiceAndOdometer { get; set; }
        public int NextServiceAndOdometer { get; set; }
        public decimal LabourCosts { get; set; }
        public decimal PartsCosts { get; set; }
        public decimal TotalCosts { get; set; }
        public List<Assets> Assets { get; set; }
        public List<Fleets> VehicleFleets { get; set; }
        public List<Condition> VehicleConditions { get; set; }
        public bool IsStandbyTaxi { get; set; }
        public bool LinkedVehicleID { get; set; }
        public string StandbyAction { get; set; }
        public List<ExpiryDateValueItem> ExpiryDateValueArray { get; set; } 

    }
}