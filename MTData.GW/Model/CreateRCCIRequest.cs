﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MTData.WCFGW.Model
{
	[DataContract(Namespace = "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService")]
	public class CreateRCCIReq
	{
		[DataMember(Order=0)]
		public GPSLocation GPSLocation { get; set; }

		[DataMember(Order = 1)]
		public ValueNode MGID { get; set; }

		[DataMember(Order = 2)]
		public CustomerDetails CustomerDetails { get; set; }

		[DataMember(Order = 3)]
		public EncryptionMethod Encryption { get; set; }

		[DataMember(Order = 4)]
		public CreditCardDetails FullCreditCardDetails { get; set; }

        [DataMember(Order = 5)]
        public CustomerAVSDetails AVSDetails { get; set; }
    }
    
    [DataContract(Namespace = "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService")]
	public class GPSLocation
	{
		[DataMember(Order = 0)]
		public decimal Latitude { get; set; }

		[DataMember(Order = 1)]
		public decimal Longitude { get; set; }

		[DataMember(Order = 2)]
		public decimal HorizontalAccuracyMeters { get; set; }
	}

	[DataContract(Namespace = "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService")]
	public class ValueNode
	{
		[DataMember]
		public string Value { get; set; }
	}

	[DataContract(Namespace = "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService")]
	public class CustomerDetails
	{
		[DataMember(Order = 0)]
		public ValueNode CustomerId { get; set; }

		[DataMember(Order = 1)]
		public string FirstName { get; set; }

		[DataMember(Order = 2)]
		public string Surname { get; set; }

		[DataMember(Order = 3)]
		public string ContactEmail { get; set; }

		[DataMember(Order = 4)]
		public string ContactPhone { get; set; }
	}

	[DataContract(Namespace = "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService")]
	public class EncryptionMethod
	{
		[DataMember(Order = 0)]
		public int Method { get; set; }

		[DataMember(Order = 1)]
		public int KeyVersion { get; set; }

		[DataMember(Order = 2)]
		public string Token { get; set; }
	}

	[DataContract(Namespace = "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService")]
	public class CreditCardDetails
	{
		[DataMember(Order = 0)]
		public string PAN { get; set; }

		[DataMember(Order = 1)]
		public string ExpiryMonth { get; set; }

		[DataMember(Order = 2)]
		public string ExpiryYear { get; set; }

		[DataMember(Order = 3)]
		public string NameOnCard { get; set; }

		[DataMember(Order = 4)]
		public string CVV2 { get; set; }
	}

    /// <summary>
    /// PAY-12 Address details for customer for RCCI
    /// </summary>
    [DataContract(Namespace = "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService")]
    public class CustomerAVSDetails
    {
        [DataMember(Order = 0)]
        public string Unit { get; set; }

        [DataMember(Order = 1)]
        public string StreetNumber { get; set; }

        [DataMember(Order = 2)]
        public string Street { get; set; }

        [DataMember(Order = 3)]
        public string AddressLine1 { get; set; }

        [DataMember(Order = 4)]
        public string AddressLine2 { get; set; }

        [DataMember(Order = 5)]
        public string Suburb { get; set; }

        [DataMember(Order = 6)]
        public string State { get; set; }

        [DataMember(Order = 7)]
        public string Postcode { get; set; }
    }
}
