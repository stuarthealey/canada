﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;


namespace MTData.Utility
{
   
    public class CryptLib
    {
        readonly UTF8Encoding _enc;
        readonly RijndaelManaged _rcipher;
        readonly byte[] _key;
        byte[] _pwd, _ivBytes;
        readonly byte[] _iv;
     
        private enum EncryptMode { Encrypt, Decrypt };

        static readonly char[] CharacterMatrixForRandomIvStringGeneration = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_'};
  
        /// <summary>
        /// Purpose             :   For generating random string
        /// Function Name       :   GenerateRandomIv
        /// Created By          :   
        /// Created On          :   06/6/2015
        /// Modification Made   :   ****************************
        /// Modidified On       :   06/6/20155 "MM/DD/YYYY"   
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        internal static string GenerateRandomIv(int length)
        {
            char[] iv = new char[length];
            byte[] randomBytes = new byte[length];

            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
                rng.GetBytes(randomBytes);

            for (int i = 0; i < iv.Length; i++)
            {
                int ptr = randomBytes[i] % CharacterMatrixForRandomIvStringGeneration.Length;
                iv[i] = CharacterMatrixForRandomIvStringGeneration[ptr];
            }

            return new string(iv);
        }

        /// <summary>
        /// 
        /// </summary>
        public CryptLib()
        {
            _enc = new UTF8Encoding();
            _rcipher = new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = 256,
                BlockSize = 128
            };
            _key = new byte[32];
            _iv = new byte[_rcipher.BlockSize / 8]; 
            _rcipher.Dispose();
            _ivBytes = new byte[16];
        }

        /// <summary>
        /// Purpose             :   For encrption and decryption
        /// Function Name       :   EncryptDecrypt
        /// Created By          :   
        /// Created On          :   06/6/2015
        /// Modification Made   :   ****************************
        /// Modidified On       :   06/6/20155 "MM/DD/YYYY"   
        /// </summary>
        /// <param name="inputText"></param>
        /// <param name="encryptionKey"></param>
        /// <param name="mode"></param>
        /// <param name="initVector"></param>
        /// <returns></returns>
        private String EncryptDecrypt(string inputText, string encryptionKey, EncryptMode mode, string initVector)
        {

            string _out = "";
            _pwd = Encoding.UTF8.GetBytes(encryptionKey);
            _ivBytes = Encoding.UTF8.GetBytes(initVector);

            int len = _pwd.Length;
            if (len > _key.Length)
            {
                len = _key.Length;
            }
            int ivLenth = _ivBytes.Length;
            if (ivLenth > _iv.Length)
            {
                ivLenth = _iv.Length;
            }

            Array.Copy(_pwd, _key, len);
            Array.Copy(_ivBytes, _iv, ivLenth);
            _rcipher.Key = _key;
            _rcipher.IV = _iv;

            if (mode.Equals(EncryptMode.Encrypt))
            {
            
                byte[] plainText = _rcipher.CreateEncryptor().TransformFinalBlock(_enc.GetBytes(inputText), 0, inputText.Length);
                _out = Convert.ToBase64String(plainText);
            }
            if (mode.Equals(EncryptMode.Decrypt))
            {
            
                byte[] plainText = _rcipher.CreateDecryptor().TransformFinalBlock(Convert.FromBase64String(inputText), 0, Convert.FromBase64String(inputText).Length);
                _out = _enc.GetString(plainText);
            }
            _rcipher.Dispose();
            return _out;
        }

        /// <summary>
        /// Purpose             :   For encrption 
        /// Function Name       :   Encrypt
        /// Created By          :   
        /// Created On          :   06/6/2015
        /// Modification Made   :   ****************************
        /// Modidified On       :   06/6/20155 "MM/DD/YYYY"   
        /// </summary>
        /// <param name="plainText"></param>
        /// <param name="key"></param>
        /// <param name="initVector"></param>
        /// <returns></returns>
        public string Encrypt(string plainText, string key, string initVector)
        {
            return EncryptDecrypt(plainText, key, EncryptMode.Encrypt, initVector);
        }

        /// <summary>
        /// Purpose             :   For decryption 
        /// Function Name       :   decrypt
        /// Created By          :   
        /// Created On          :   06/6/2015
        /// Modification Made   :   ****************************
        /// Modidified On       :   06/6/20155 "MM/DD/YYYY"   
        /// </summary>
        /// <param name="encryptedText"></param>
        /// <param name="key"></param>
        /// <param name="initVector"></param>
        /// <returns></returns>
        public string decrypt(string encryptedText, string key, string initVector)
        {
            return EncryptDecrypt(encryptedText, key, EncryptMode.Decrypt, initVector);
        }

        /// <summary>
        /// Purpose             :   For getting hash 
        /// Function Name       :   GetHashSha256
        /// Created By          :   
        /// Created On          :   06/6/2015
        /// Modification Made   :   ****************************
        /// Modidified On       :   06/6/20155 "MM/DD/YYYY"   
        /// </summary>
        /// <param name="text"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GetHashSha256(string text, int length)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            using (SHA256Managed hashstring = new SHA256Managed())
            {
                byte[] hash = hashstring.ComputeHash(bytes);
                string hashString = hash.Aggregate(string.Empty, (current, x) => current + String.Format("{0:x2}", x));
                if (length > hashString.Length)
                    return hashString;
                return hashString.Substring(0, length);
            }
        }

    }

}