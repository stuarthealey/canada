﻿using System;
using System.Linq;

using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Data.Repository
{
    public class DefaultRepository : BaseRepository, IDefaultRepository
    {
        public DefaultRepository(IMtDataDevlopmentsEntities dbMtData) : base(dbMtData)
        {
        }

        /// <summary>
        /// Purpose             :   To get default setting
        /// Function Name       :   GetDefault
        /// Created By          :   Salil Gupta
        /// Created On          :   12/30/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="defaultId"></param>
        /// <returns></returns>
        public DefaultSetting GetDefault(int defaultId)
        {
            try
            {
                DefaultSetting defaultDb = DbMtData.DefaultSettings.Find(defaultId);
                DefaultSetting defaultTip = DbMtData.DefaultSettings.Find(5);
                DefaultSetting defaultTechFee = DbMtData.DefaultSettings.Find(6);
                defaultDb.TipHigh = defaultTip.TipHigh;
                defaultDb.TipLow = defaultTip.TipLow;
                defaultDb.TipMedium = defaultTip.TipMedium;
                defaultDb.TechFeeFixed = defaultTechFee.TechFeeFixed;
                defaultDb.TechFeeMaxCap = defaultTechFee.TechFeeMaxCap;
                defaultDb.TechFeePer = defaultTechFee.TechFeePer;
                defaultDb.TechFeeType = defaultTechFee.TechFeeType;
                return defaultDb;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To update default setting
        /// Function Name       :   Update
        /// Created By          :   Salil Gupta
        /// Created On          :   12/30/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="defaultSetting"></param>
        /// <param name="defaultId"></param>
        public void Update(DefaultSetting defaultSetting, int defaultId)
        {
            try
            {
                DefaultSetting defaultDb = DbMtData.DefaultSettings.Find(defaultId);
                if (defaultId == 1)
                {
                    defaultDb.StateTaxRate = defaultSetting.StateTaxRate;
                    defaultDb.FederalStateRate = defaultSetting.FederalStateRate;
                }
                else
                {
                    if (defaultId == 2)
                    {
                        defaultDb.SurchargeFixed = defaultSetting.SurchargeFixed;
                        defaultDb.SurchargePer = defaultSetting.SurchargePer;
                        defaultDb.SurchargeMaxCap = defaultSetting.SurchargeMaxCap;
                        defaultDb.SurchargeType = defaultSetting.SurchargeType;
                        defaultDb.BookingFeeFixed = defaultSetting.BookingFeeFixed;
                        defaultDb.BookingFeePer = defaultSetting.BookingFeePer;
                        defaultDb.BookingMaxCap = defaultSetting.BookingMaxCap;
                        defaultDb.BookingFeeType = defaultSetting.BookingFeeType;
                    }
                    else if (defaultId == 4)
                    {
                        defaultDb.TermCondition = defaultSetting.TermCondition;
                        defaultDb.Disclaimer = defaultSetting.Disclaimer;
                    }
                    else if (defaultId == 5)
                    {
                        DefaultSetting defaulttipDb = DbMtData.DefaultSettings.Find(defaultId);
                        defaulttipDb.TipHigh = defaultSetting.TipHigh;
                        defaulttipDb.TipMedium = defaultSetting.TipMedium;
                        defaulttipDb.TipLow = defaultSetting.TipLow;
                    }
                    else if (defaultId == 6)
                    {
                        DefaultSetting defaulttipDb = DbMtData.DefaultSettings.Find(defaultId);
                        defaulttipDb.TechFeeType = defaultSetting.TechFeeType;
                        defaulttipDb.TechFeeFixed = defaultSetting.TechFeeFixed;
                        defaulttipDb.TechFeePer = defaultSetting.TechFeePer;
                        defaulttipDb.TechFeeMaxCap = defaultSetting.TechFeeMaxCap;
                    }
                    else if (defaultId == 7)
                    {
                        DefaultSetting defaulttipDb = DbMtData.DefaultSettings.Find(defaultId);
                        defaulttipDb.TransCountSlablt1 = defaultSetting.TransCountSlablt1;
                        defaulttipDb.TransCountSlablt2 = defaultSetting.TransCountSlablt2;
                        defaulttipDb.TransCountSlablt3 = defaultSetting.TransCountSlablt3;
                        defaulttipDb.TransCountSlabGt4 = defaultSetting.TransCountSlabGt4;
                        defaulttipDb.TransCountSlabGt2 = defaultSetting.TransCountSlabGt2;
                        defaulttipDb.TransCountSlabGt3 = defaultSetting.TransCountSlabGt3;

                    }
                }

                defaultDb.IsActive = true;
                defaultDb.ModifiedDate = DateTime.Now;
                defaultDb.ModifiedBy = defaultSetting.ModifiedBy;
            }
            catch (Exception)
            {
                throw;
            }

            DbMtData.CommitChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="defaultId"></param>
        /// <returns></returns>
        public TransactionCountSlab GetTransactionCountSlab(int merchantId)
        {
            try
            {
                TransactionCountSlab tcount = DbMtData.TransactionCountSlabs.Where(x=>x.Fk_MerchantId == merchantId).FirstOrDefault();
                return tcount;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="defaultId"></param>
        /// <returns></returns>
        public TransactionCountSlab GetTransactionCountSlabCorp(int userId)
        {
            try
            {
                TransactionCountSlab tcount = DbMtData.TransactionCountSlabs.Where(x => x.fk_CorporateUserId == userId).FirstOrDefault();
                return tcount;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(TransactionCountSlab slab, int merchantId)
        {
            try
            {
                bool isExist = DbMtData.TransactionCountSlabs.Any(x => x.Fk_MerchantId == merchantId);
                if(isExist){
                     TransactionCountSlab slabData=DbMtData.TransactionCountSlabs.Where(x=>x.Fk_MerchantId==merchantId).FirstOrDefault();
                    slabData.TransCountSlabGt2=slab.TransCountSlabGt2;
                    slabData.TransCountSlabGt3=slab.TransCountSlabGt3;
                    slabData.TransCountSlabGt4=slab.TransCountSlabGt4;
                    slabData.TransCountSlablt1=slab.TransCountSlablt2;
                    slabData.TransCountSlablt3=slab.TransCountSlablt3;
                    slabData.ModifiedDate=DateTime.Now;
                    slabData.ModifiedBy=slab.CreatedBy;
                    DbMtData.CommitChanges();
                }
                else{
                    slab.CreatedDate=DateTime.Now;
                    slab.Fk_MerchantId = merchantId;
                    DbMtData.TransactionCountSlabs.Add(slab);
                    DbMtData.CommitChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void UpdateCorp(TransactionCountSlab slab, int userId)
        {
            try
            {
                bool isExist = DbMtData.TransactionCountSlabs.Any(x => x.fk_CorporateUserId == userId);
                if (isExist)
                {
                    TransactionCountSlab slabData = DbMtData.TransactionCountSlabs.Where(x => x.fk_CorporateUserId == userId).FirstOrDefault();
                    slabData.TransCountSlabGt2 = slab.TransCountSlabGt2;
                    slabData.TransCountSlabGt3 = slab.TransCountSlabGt3;
                    slabData.TransCountSlabGt4 = slab.TransCountSlabGt4;
                    slabData.TransCountSlablt1 = slab.TransCountSlablt2;
                    slabData.TransCountSlablt3 = slab.TransCountSlablt3;
                    slabData.ModifiedDate = DateTime.Now;
                    slabData.ModifiedBy = slab.CreatedBy;
                    DbMtData.CommitChanges();
                }
                else
                {
                    slab.CreatedDate = DateTime.Now;
                    slab.fk_CorporateUserId = userId;
                    DbMtData.TransactionCountSlabs.Add(slab);
                    DbMtData.CommitChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
