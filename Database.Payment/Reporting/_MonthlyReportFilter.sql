IF NOT EXISTS (SELECT 1 FROM sys.tables WHERE name = '_MonthlyReportFilter')
BEGIN
	CREATE TABLE dbo._MonthlyReportFilter(
		[Type] NVARCHAR(5) NOT NULL,
		[Value] NVARCHAR(250) NOT NULL,
	CONSTRAINT [PK__MonthlyReportFilter] PRIMARY KEY CLUSTERED 
		([Type] ASC, [Value] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]

	PRINT '_MonthlyReportFilter table created.'
END
ELSE
BEGIN
	PRINT '_MonthlyReportFilter table already exists.'
END
GO