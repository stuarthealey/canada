
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetTransactionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetTransactionData]
GO

--exec [usp_GetTransactionData] 352,'2015-09-09 00:01:01','2015-09-16 00:01:01','','All',null,'510','253','EmvContact',2
Create proc [dbo].[usp_GetTransactionData]	
	@fk_MerchantID int,
	@DateFrom datetime,
	@DateTo datetime = NULL,
	@TransType nvarchar(20) = NULL, 
	@DownloadStatus Char(3) = NULL,
	@VehicleId varchar(50) = NULL,
	@DriverId nvarchar(50) = NULL,
	@fk_fleetId int = NULL,
	@EntryMode NVARCHAR(50)=NULL,
	@BatchNumber int = NULL,
	@RetrieveSinceBatch bit = 0
	AS
BEGIN
SET NOCOUNT ON	
	DECLARE @Sale varchar(50) = NULL, @Completion varchar(50) = NULL, @Refund varchar(50) = NULL, @IsDownloaded char(1),
			@swipe varchar(50) = NULL, @fswipe varchar(50) = NULL,@CurrentBatchNumber int


	---Managing transaction type
	IF(UPPER(@TransType) = 'APPROVED')
	BEGIN
		SET @Sale='Sale'
		SET @Completion='Completion'
	END
	ELSE IF(UPPER(@TransType) = 'REFUND')
		SET @Refund='Refund'
	ELSE IF(UPPER(@TransType) = 'ALL' OR @TransType='' OR @TransType IS NULL)
	BEGIN
		SET @Sale='Sale'
		SET @Completion='Completion'
		SET @Refund='Refund'
	END


	---Managing EntryMode
	IF(UPPER(@EntryMode) = 'SWIPE')
	BEGIN
		SET @swipe='swiped'
		SET @fswipe='fswiped'
	END

	
	-- Managing DownloadStatus
	SELECT @IsDownloaded = case WHEN UPPER(@DownloadStatus)='NEW' THEN '0' 
								WHEN UPPER(@DownloadStatus)='OLD' THEN '1' 
								WHEN UPPER(@DownloadStatus)='ALL' OR @DownloadStatus ='' OR @DownloadStatus IS NULL THEN  NULL END

	print @IsDownloaded
	
	-- Preparing @DateTo if last date not supplied
	SET @DateTo = IsNull(@DateTo, cast(getdate() as datetime))

	-- Preparing next BatchNumber to update
	SELECT @CurrentBatchNumber=MAX(BatchNumber) FROM MTDTransaction
	SET @CurrentBatchNumber = ISNULL(@CurrentBatchNumber,0)+1


	IF(@BatchNumber IS NULL)
	BEGIN

		IF(UPPER(@DownloadStatus)='NEW')
		BEGIN

				SELECT ID,TerminalID ,DriverNo,VehicleNo,JobNumber,CrdHldrName,CrdHldrPhone,CrdHldrCity,CrdHldrState,CrdHldrZip,
					CrdHldrAddress,TransRefNo,PaymentType,TxnType,FareValue,Tip,Surcharge,Fee,Taxes,Toll,Amount,ExpiryDate,CardType,
					Industry,EntryMode,ResponseCode,AddRespData ,AuthId,SourceId,TransNote,IsCompleted,IsRefunded,PickAddress,
					DestinationAddress,TxnDate,FirstFourDigits,LastFourDigits,FlagFall,Extras,GatewayTxnId,fk_FleetId,RequestId ,
					IsVoided,IsDownloaded,TransSeqNo,ISNULL(BatchNumber,@CurrentBatchNumber) 'BatchNumber'
				FROM [MTDTransaction]
				where 
				(VehicleNo = @VehicleId OR COALESCE(@VehicleId,'') = '')
				AND (DriverNo = @DriverId OR COALESCE(@DriverId,'') = '')
				AND (TxnDate >= @DateFrom OR @DateFrom IS NULL)
				AND (TxnDate <= @DateTo OR COALESCE(@DateTo,'')='')
				AND (TxnType = @Sale OR TxnType= @Completion OR TxnType= @Refund)
				AND (MerchantId = @fk_MerchantID OR COALESCE(@fk_MerchantID,0) = 0)
				AND (fk_FleetId = @fk_fleetId OR COALESCE(@fk_fleetId,0) = 0)
				AND (EntryMode = @swipe OR EntryMode= @fswipe OR EntryMode= @EntryMode OR COALESCE(@EntryMode,'') = '')
				AND (IsDownloaded = @IsDownloaded OR @IsDownloaded IS NULL)
				AND ResponseCode in ('000','002') 


				UPDATE [MTDTransaction] SET IsDownloaded=1,BatchNumber = @CurrentBatchNumber
				WHERE 
				(VehicleNo = @VehicleId OR COALESCE(@VehicleId,'') = '')
				AND (DriverNo = @DriverId OR COALESCE(@DriverId,'') = '')
				AND (TxnDate >= @DateFrom OR @DateFrom IS NULL)
				AND (TxnDate <= @DateTo OR COALESCE(@DateTo,'')='')
				AND (TxnType = @Sale OR TxnType= @Completion OR TxnType= @Refund)
				AND (MerchantId = @fk_MerchantID OR COALESCE(@fk_MerchantID,0) = 0)
				AND (fk_FleetId = @fk_fleetId OR COALESCE(@fk_fleetId,0) = 0)
				AND (EntryMode = @swipe OR EntryMode= @fswipe OR EntryMode= @EntryMode OR COALESCE(@EntryMode,'') = '')
				AND (IsDownloaded = @IsDownloaded OR @IsDownloaded IS NULL)
				AND ResponseCode in ('000','002') 
				
				OPTION (RECOMPILE)
		END
		ELSE
		BEGIN
				SELECT ID,TerminalID ,DriverNo,VehicleNo,JobNumber,CrdHldrName,CrdHldrPhone,CrdHldrCity,CrdHldrState,CrdHldrZip,
					CrdHldrAddress,TransRefNo,PaymentType,TxnType,FareValue,Tip,Surcharge,Fee,Taxes,Toll,Amount,ExpiryDate,CardType,
					Industry,EntryMode,ResponseCode,AddRespData ,AuthId,SourceId,TransNote,IsCompleted,IsRefunded,PickAddress,
					DestinationAddress,TxnDate,FirstFourDigits,LastFourDigits,FlagFall,Extras,GatewayTxnId,fk_FleetId,RequestId ,
					IsVoided,IsDownloaded,TransSeqNo, BatchNumber
				FROM [MTDTransaction]
				where 
				(VehicleNo = @VehicleId OR COALESCE(@VehicleId,'') = '')
				AND (DriverNo = @DriverId OR COALESCE(@DriverId,'') = '')
				AND (TxnDate >= @DateFrom OR @DateFrom IS NULL)
				AND (TxnDate <= @DateTo OR COALESCE(@DateTo,'')='')
				AND (TxnType = @Sale OR TxnType= @Completion OR TxnType= @Refund)
				AND (MerchantId = @fk_MerchantID OR COALESCE(@fk_MerchantID,0) = 0)
				AND (fk_FleetId = @fk_fleetId OR COALESCE(@fk_fleetId,0) = 0)
				AND (EntryMode = @swipe OR EntryMode= @fswipe OR EntryMode= @EntryMode OR COALESCE(@EntryMode,'') = '')
				AND (IsDownloaded = @IsDownloaded OR @IsDownloaded IS NULL)
				AND ResponseCode in ('000','002')  

				OPTION (RECOMPILE)
		END

	END
	ELSE IF((@BatchNumber IS NOT NULL) AND (@RetrieveSinceBatch=0))
	BEGIN
			SELECT ID,TerminalID ,DriverNo,VehicleNo,JobNumber,CrdHldrName,CrdHldrPhone,CrdHldrCity,CrdHldrState,CrdHldrZip,
				CrdHldrAddress,TransRefNo,PaymentType,TxnType,FareValue,Tip,Surcharge,Fee,Taxes,Toll,Amount,ExpiryDate,CardType,
				Industry,EntryMode,ResponseCode,AddRespData ,AuthId,SourceId,TransNote,IsCompleted,IsRefunded,PickAddress,
				DestinationAddress,TxnDate,FirstFourDigits,LastFourDigits,FlagFall,Extras,GatewayTxnId,fk_FleetId,RequestId ,
				IsVoided,IsDownloaded,TransSeqNo,BatchNumber
			FROM [MTDTransaction]
			WHERE BatchNumber = @BatchNumber

			OPTION (RECOMPILE)
	END	
	ELSE IF((@BatchNumber IS NOT NULL) AND (@RetrieveSinceBatch=1))
	BEGIN
	print 'a'
			--assign new BatchNumber to all all unbatched transactions that are part of [BatchNumber > Supplied batch number (i.e. @BatchNumber)]
			UPDATE [MTDTransaction] SET IsDownloaded=1,BatchNumber = @CurrentBatchNumber
				WHERE 
				(VehicleNo = @VehicleId OR COALESCE(@VehicleId,'') = '')
				AND (DriverNo = @DriverId OR COALESCE(@DriverId,'') = '')
				AND (TxnDate >= @DateFrom OR @DateFrom IS NULL)
				AND (TxnDate <= @DateTo OR COALESCE(@DateTo,'')='')
				AND (TxnType = @Sale OR TxnType= @Completion OR TxnType= @Refund)
				AND (MerchantId = @fk_MerchantID OR COALESCE(@fk_MerchantID,0) = 0)
				AND (fk_FleetId = @fk_fleetId OR COALESCE(@fk_fleetId,0) = 0)
				AND (EntryMode = @swipe OR EntryMode= @fswipe OR EntryMode= @EntryMode OR COALESCE(@EntryMode,'') = '')
				AND (IsDownloaded = @IsDownloaded OR @IsDownloaded IS NULL)
				AND ResponseCode in ('000','002')  
				AND (BatchNumber IS NULL)
				
			OPTION (RECOMPILE)

			print 'b'
			-- selecting all transactions that are part of [BatchNumber > Supplied batch number (i.e. @BatchNumber)]
			SELECT ID,TerminalID ,DriverNo,VehicleNo,JobNumber,CrdHldrName,CrdHldrPhone,CrdHldrCity,CrdHldrState,CrdHldrZip,
				CrdHldrAddress,TransRefNo,PaymentType,TxnType,FareValue,Tip,Surcharge,Fee,Taxes,Toll,Amount,ExpiryDate,CardType,
				Industry,EntryMode,ResponseCode,AddRespData ,AuthId,SourceId,TransNote,IsCompleted,IsRefunded,PickAddress,
				DestinationAddress,TxnDate,FirstFourDigits,LastFourDigits,FlagFall,Extras,GatewayTxnId,fk_FleetId,RequestId ,
				IsVoided,IsDownloaded,TransSeqNo,BatchNumber
			FROM [MTDTransaction]
			where 
			(VehicleNo = @VehicleId OR COALESCE(@VehicleId,'') = '')
			AND (DriverNo = @DriverId OR COALESCE(@DriverId,'') = '')
			AND (TxnDate >= @DateFrom OR @DateFrom IS NULL)
			AND (TxnDate <= @DateTo OR COALESCE(@DateTo,'')='')
			AND (TxnType = @Sale OR TxnType= @Completion OR TxnType= @Refund)
			AND (MerchantId = @fk_MerchantID OR COALESCE(@fk_MerchantID,0) = 0)
			AND (fk_FleetId = @fk_fleetId OR COALESCE(@fk_fleetId,0) = 0)
			AND (EntryMode = @swipe OR EntryMode= @fswipe OR EntryMode= @EntryMode OR COALESCE(@EntryMode,'') = '')
			AND (IsDownloaded = @IsDownloaded OR @IsDownloaded IS NULL)
			AND ResponseCode in ('000','002')  			
			AND (BatchNumber > @BatchNumber)

			OPTION (RECOMPILE)
	END	
	SET NOCOUNT OFF
END

GO

	
