IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DeviceLogin]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_DeviceLogin]
GO

CREATE PROCEDURE dbo.usp_DeviceLogin
(
	@SerialNumber NVARCHAR(50)
)
AS
BEGIN
     SELECT Email, [Password], fk_MerchantID, fk_FleetID 
	 FROM dbo.Users 
	 WHERE fk_MerchantID = (SELECT fk_MerchantID 
							FROM dbo.Fleet 
							WHERE FleetID = (	SELECT fk_FleetID 
												FROM dbo.Vehicle
												WHERE  VehicleID = (SELECT fk_VehicleID 
																	FROM dbo.VehicleTerminal
																	WHERE fk_TerminalID = (SELECT TerminalID 
																							FROM dbo.Terminal 
																							WHERE SerialNo = @SerialNumber)
																	)
												)
							)
END
GO
