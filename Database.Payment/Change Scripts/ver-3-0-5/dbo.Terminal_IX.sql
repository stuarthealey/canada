-- Create the IX_TerminalSerialNo index.
----------------------------------------
IF NOT EXISTS (SELECT * FROM sysindexes si INNER JOIN sysobjects so ON so.id = si.id WHERE si.name = 'IX_TerminalSerialNo' AND OBJECTPROPERTY(so.id, N'IsUserTable') = 1 AND so.name = 'Terminal')
BEGIN
	PRINT 'Creating Index IX_TerminalSerialNo on Terminal'

	CREATE NONCLUSTERED INDEX IX_TerminalSerialNo ON dbo.Terminal (SerialNo ASC, IsActive ASC)
		INCLUDE (MacAdd)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
END
ELSE
BEGIN
	PRINT 'Index IX_TerminalSerialNo on Terminal already exists'
END
GO
