﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
    public class FirstDataDetailsResultDto
    {
        public int TerminalID { get; set; }
        public string SerialNo { get; set; }
        public string DeviceName { get; set; }
        public string MacAdd { get; set; }
        public int fk_MerchantID { get; set; }
        public Nullable<bool> IsTerminalAssigned { get; set; }
        public Nullable<int> DatawireId { get; set; }
        public int FDId { get; set; }
        public string FDMerchantID { get; set; }
        public string GroupId { get; set; }
        public string serviceId { get; set; }
        public string ProjectId { get; set; }
        public string EcommProjectId { get; set; }
        public Nullable<byte> ProjectType { get; set; }
        public string App { get; set; }
        public Nullable<bool> IsAssigned { get; set; }
        public int ID { get; set; }
        public string DID { get; set; }
        public string DatewireXml { get; set; }
        public Nullable<int> MerchantId { get; set; }
        public string RCTerminalId { get; set; }
        public string Stan { get; set; }
        public string RefNumber { get; set; }
        public string TokenType { get; set; }
        public string MCCode { get; set; }
        public string EncryptionKey { get; set; }
    }
}
