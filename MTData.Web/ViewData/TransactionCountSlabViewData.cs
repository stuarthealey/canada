﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.Web.ViewData
{
    public class TransactionCountSlabViewData
    {
        public int SlabID { get; set; }
        public Nullable<int> Fk_MerchantId { get; set; }
        public Nullable<int> fk_CorporateUserId { get; set; }
        public Nullable<int> TransCountSlablt1 { get; set; }
        public Nullable<int> TransCountSlabGt2 { get; set; }
        public Nullable<int> TransCountSlablt2 { get; set; }
        public Nullable<int> TransCountSlabGt3 { get; set; }
        public Nullable<int> TransCountSlablt3 { get; set; }
        public Nullable<int> TransCountSlabGt4 { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}