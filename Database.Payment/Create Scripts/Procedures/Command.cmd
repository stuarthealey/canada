@echo off
REM: Command File Created by Microsoft Visual Database Tools
REM: Authentication type: Windows NT
REM: Usage: CommandFilename [Server] [Database]

if '%1' == '' goto usage
if '%2' == '' goto usage

if '%1' == '/?' goto usage
if '%1' == '-?' goto usage
if '%1' == '?' goto usage
if '%1' == '/help' goto usage

REM: ====================================
REM: Ordered scripts
REM: ====================================

echo.
echo "dbo.usp_AdminExceptionReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_AdminExceptionReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_AdminExceptionReportScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_AdminExceptionReportScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_AdminSummaryReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_AdminSummaryReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_AdminSummaryReportScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_AdminSummaryReportScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_CardTypeReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_CardTypeReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_CardTypeReportScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_CardTypeReportScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_CarOwnerSummaryReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_CarOwnerSummaryReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_CheckFileVersion.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_CheckFileVersion.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_CheckTAKeyFileVersion.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_CheckTAKeyFileVersion.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_CorporateDetailReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_CorporateDetailReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_CorporateDetailReportScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_CorporateDetailReportScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_CorporateSummaryReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_CorporateSummaryReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_CorporateSummaryReportScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_CorporateSummaryReportScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_CorpUserCardTypeReportScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_CorpUserCardTypeReportScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_CorpUserExceptionReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_CorpUserExceptionReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_CorpUserExceptionReportScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_CorpUserExceptionReportScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_CorpUserGetTotalTransByVehicleReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_CorpUserGetTotalTransByVehicleReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_CorpUserTotalTransByVehicle.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_CorpUserTotalTransByVehicle.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_CorpUserTotalTransByVehicleScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_CorpUserTotalTransByVehicleScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_Datawire.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_Datawire.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_DetailReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_DetailReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_DetailReportScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_DetailReportScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_DeviceLogin.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_DeviceLogin.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_DownloadData.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_DownloadData.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_DriverShiftReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_DriverShiftReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_DriverSummaryReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_DriverSummaryReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_FDMerchant.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_FDMerchant.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_FdTerminal.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_FdTerminal.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_FirstDataDetails.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_FirstDataDetails.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetAdminExceptionReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetAdminExceptionReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetAdminSummaryReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetAdminSummaryReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetCardTypeReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetCardTypeReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetDetailReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetDetailReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetDrivers.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetDrivers.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetDriversTransaction.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetDriversTransaction.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetFirstDataValue.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetFirstDataValue.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetLoginData.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetLoginData.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetMerchantExceptionReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetMerchantExceptionReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetMerchantSummaryReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetMerchantSummaryReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetMerchantTotalTransByVehicleReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetMerchantTotalTransByVehicleReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetPayrollPaymentTransaction.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetPayrollPaymentTransaction.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetPayrollSAGETransaction.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetPayrollSAGETransaction.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetTotalTransByVehicleReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetTotalTransByVehicleReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetTransactionData.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetTransactionData.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetUsersCredentials.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetUsersCredentials.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetVehicles.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetVehicles.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MerchantCardTypeReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MerchantCardTypeReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MerchantCardTypeReportScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MerchantCardTypeReportScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MerchantExceptionReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MerchantExceptionReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MerchantExceptionReportScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MerchantExceptionReportScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MerchantSummaryReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MerchantSummaryReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MerchantSummaryReportScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MerchantSummaryReportScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MerchantTotalTransByVehicle.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MerchantTotalTransByVehicle.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MerchantTotalTransByVehicleScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MerchantTotalTransByVehicleScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MerchantUserCardTypeReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MerchantUserCardTypeReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MerchantUserCardTypeReportScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MerchantUserCardTypeReportScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MerchantUserDetailReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MerchantUserDetailReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MerchantUserDetailReportScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MerchantUserDetailReportScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MerchantUserExceptionReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MerchantUserExceptionReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MerchantUserExceptionReportScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MerchantUserExceptionReportScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MerchantUserSummaryReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MerchantUserSummaryReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MerchantUserSummaryReportScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MerchantUserSummaryReportScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MerchantUserTotalTransByVehicle.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MerchantUserTotalTransByVehicle.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MerchantUserTotalTransByVehicleScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MerchantUserTotalTransByVehicleScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_OnDemandDriverReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_OnDemandDriverReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_PayDriverOwnerNetwork.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_PayDriverOwnerNetwork.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_PayDriverOwner.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_PayDriverOwner.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_PayNetwork.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_PayNetwork.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_PreAuthInsertResponse.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_PreAuthInsertResponse.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_Schedule.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_Schedule.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_ScheduleUnsent.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_ScheduleUnsent.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_SearchTransactionReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_SearchTransactionReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_TechnologyDetailReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_TechnologyDetailReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_TechnologySummaryReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_TechnologySummaryReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_TotalTransByVehicle.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_TotalTransByVehicle.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_TotalTransByVehicleScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_TotalTransByVehicleScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_ValidateEmail.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_ValidateEmail.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_Validation.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_Validation.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_CorporateCardTypeReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_CorporateCardTypeReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetMerchantCardTypeReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetMerchantCardTypeReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetCorpExceptionReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetCorpExceptionReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetCorporateUserDetailReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetCorporateUserDetailReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetCorporateUserSummaryReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetCorporateUserSummaryReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetCorpUserExceptionReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetCorpUserExceptionReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetCorpUserCardTypeReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetCorpUserCardTypeReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetMerchantUserCardTypeReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetMerchantUserCardTypeReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetMerchantUserTotalTransByVehicleReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetMerchantUserTotalTransByVehicleReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetMerchantUserSummaryReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetMerchantUserSummaryReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetMerchantUserExceptionReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetMerchantUserExceptionReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetMerchantUserDetailReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetMerchantUserDetailReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_AdminWeeklyDollarVolume.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_AdminWeeklyDollarVolume.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_AdminWeeklyDollarVolumeScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_AdminWeeklyDollarVolumeScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_AdminWeeklySummaryCount.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_AdminWeeklySummaryCount.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_AdminWeeklySummaryCountScheduler.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_AdminWeeklySummaryCountScheduler.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetAdminWeeklyDollarVolume.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetAdminWeeklyDollarVolume.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_GetAdminWeeklySummaryCount.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_GetAdminWeeklySummaryCount.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_LineGraphDollarVolumeReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_LineGraphDollarVolumeReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_LineGraphTransCountReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_LineGraphTransCountReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MonthlyGraphCountReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MonthlyGraphCountReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.usp_MonthlyGraphDollarVolumeReport.sql"
osql -S %1 -d %2 -E -b -i "dbo.usp_MonthlyGraphDollarVolumeReport.sql"
if %ERRORLEVEL% NEQ 0 goto errors

goto finish

REM: How to use screen
:usage
echo.
echo Usage: MyScript Server Database
echo Server: the name of the target SQL Server
echo Database: the name of the target database
echo.
echo Example: MyScript.cmd MainServer MainDatabase
echo.
echo.
goto done

REM: error handler
:errors
echo.
echo WARNING! Error(s) were detected!
echo --------------------------------
echo Please evaluate the situation and, if needed,
echo restart this command file. You may need to
echo supply command parameters when executing
echo this command file.
echo.
pause
goto done

REM: finished execution
:finish
echo.
echo Script execution is complete!
:done
@echo on