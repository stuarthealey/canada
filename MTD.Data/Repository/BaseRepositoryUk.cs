﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MTD.Data.Data;

namespace MTD.Data.Repository
{
    public class BaseRepositoryUk
    { 
        
        private IMTDataDevlopmentsUKEntities _dbMtDataUk;

        public BaseRepositoryUk(IMTDataDevlopmentsUKEntities dbMtDataUk)
        {
            _dbMtDataUk = dbMtDataUk;
        }

        public IMTDataDevlopmentsUKEntities DbMtDataUk
        {
            get
            {
                return _dbMtDataUk;
            }
        }
    }
}
