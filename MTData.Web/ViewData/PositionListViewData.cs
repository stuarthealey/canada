﻿using System;

namespace MTData.Web.ViewData
{
    public class PositionListViewData
    {
        public string Field { get; set; }
        public int? value { get; set; }
        public bool? isChecked { get; set; }
        public string  name { get; set; }
        public string Pos { get; set; }
        public int? fleetId { get; set; }
        public int? fieldId { get; set; }
        public int? FleetReceiptID { get; set; }
        public string FieldName { get; set; }
    }
    
}