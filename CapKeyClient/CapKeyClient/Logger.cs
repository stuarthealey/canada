﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;

namespace CapKeyClient
{
    public static class Logger
    {
        static string path = string.Empty;

        static Logger()
        {
            path = System.IO.Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);
            path = path + @"\Log";

            //using (EventLog eventLog = new EventLog("Application"))
            //{
            //    eventLog.Source = "Application";
            //    eventLog.WriteEntry(string.Format("CapKey Path:{0};", path), EventLogEntryType.Information, 101, 1);
            //}

            //Create directory 
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            //Create file path
            string fileName = DateTime.Now.ToString("yyyyMM") + ".txt";
            path = path + @"\" + fileName;
        }

        public static void WriteLog(string log)
        {
            //using (EventLog eventLog = new EventLog("Application"))
            //{
            //    eventLog.Source = "Application";
            //    eventLog.WriteEntry(string.Format("CapKey Path:{0};Log:{1};", path, log), EventLogEntryType.Information, 101, 1);
            //}

            if (!string.IsNullOrEmpty(log))
            {
                using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.WriteLine(log);
                    }
                }
            }
        }
    }
}
