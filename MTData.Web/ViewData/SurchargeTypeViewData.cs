﻿namespace MTData.Web.ViewData
{
    public class SurchargeTypeViewData
    {
        public int TypeID { get; set; }
        public string TypeName { get; set; }
    }
}