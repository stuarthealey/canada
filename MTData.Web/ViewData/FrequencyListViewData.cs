﻿namespace MTData.Web.ViewData
{
    public class FrequencyListViewData
    {
        public int FrequencyId { get; set; }
        public string Name { get; set; }
    }
}