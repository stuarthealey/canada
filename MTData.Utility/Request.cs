﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTData.Utility
{
   public static class Requests
    {
       public static class Retail
       {
           /* Request for  Swiped Entry Mode for Retail*/
               public static class Swiped
               {
                   public static string POSEntryMode = "901";
                   public static string POSCondCode = "00";
                   public static string TermCatCode = "01";
                   public static string TermEntryCapablt = "01";
                   public static string TermLocInd = "0";
                   public static string CardCaptCap = "1";
               }
               /* Request for Keyed  Entry Mode for Retail*/
               public static class Keyed
               {
                   public static string POSEntryMode = "011";
                   public static string POSCondCode = "00";
                   public static string TermCatCode = "01";
                   public static string TermEntryCapablt = "01";
                   public static string TermLocInd = "0";
                   public static string CardCaptCap = "1";
               }
              
       }

       public static class Ecommerce
       {
           /* Request for  Swiped Entry Mode for Ecommerce*/

           public static class Keyed
           {
               public static string POSEntryMode = "010";
               public static string POSCondCode = "59";
               public static string TermCatCode = "00";
               public static string TermEntryCapablt = "00";
               public static string TermLocInd = "1";
               public static string CardCaptCap = "0";
           }
          
       }
    }
}
