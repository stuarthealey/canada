﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MTData.Web.Resource;

namespace MTData.Web.ViewData
{
    public class SurchargeViewData
    {
        public int SurID { get; set; }
        [Required(ErrorMessageResourceType = typeof(MtdataResource), ErrorMessageResourceName = "Please_select_surcharge_type")]
        public Nullable<byte> SurchargeType { get; set; }
        
        public Nullable<decimal> SurchargeFixed { get; set; }
        public Nullable<decimal> SurchargePer { get; set; }
        public Nullable<decimal> SurMaxCap { get; set; }
        //public Nullable<decimal> TechFee { get; set; }
        [Required(ErrorMessageResourceType = typeof(MtdataResource), ErrorMessageResourceName = "Please_select_technology_FeeType")]
        public Nullable<byte> TechFeeType { get; set; }
        public Nullable<decimal> TechFeeFixed { get; set; }
        public Nullable<decimal> TechFeePer { get; set; }
        public Nullable<decimal> TechFeeMaxCap { get; set; }

        [Required(ErrorMessageResourceType = typeof(MtdataResource), ErrorMessageResourceName = "SurchargeViewData_TxnFee_Please_select_a_surcharge_type")]
        public Nullable<byte> BookingFeeType { get; set; }
        public Nullable<decimal> BookingFeeFixed { get; set; }
        public Nullable<decimal> BookingFeePer { get; set; }
        //[Required(ErrorMessageResourceType = typeof (MtdataResource), ErrorMessageResourceName = "SurchargeViewData_BookingFeeMaxCap_Please_enter_the_booking_fee_max_cap")]
        public Nullable<decimal> BookingFeeMaxCap { get; set; }
        public string CCType { get; set; }
         [Required(ErrorMessageResourceType = typeof(MtdataResource), ErrorMessageResourceName = "Required_Fleet_Name")]
        public int FleetID { get; set; }
        [Required(ErrorMessageResourceType = typeof (MtdataResource), ErrorMessageResourceName = "SurchargeViewData_fk_GatewayID_Please_select_a_gateway")]
        public int fk_GatewayID { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Nullable<decimal> SurchargePercentage { get; set; }
        //[Required(ErrorMessageResourceType = typeof (MtdataResource), ErrorMessageResourceName = "SurchargeViewData_Surcharge_Please_enter_a_surcharge")]
        public Nullable<decimal> Surcharge { get; set; }
        public string SurType { get; set; }
        public Nullable<decimal> FeePercentage { get; set; }
        //[Required(ErrorMessageResourceType = typeof (MtdataResource), ErrorMessageResourceName = "SurchargeViewData_BookingFee_Please_enter_the_booking_fee")]
        public Nullable<decimal> BookingFee { get; set; }
        public Nullable<decimal> TechFee { get; set; }
        public string FeeType { get; set; }
        public string MerchantName { get; set; }
        public string Company { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int MerchantId { get; set; }
        public string FleetName { get; set; }
        public IEnumerable<MerchantListViewData> MerchantList { get; set; }
        public IEnumerable<GatewayListViewData> GatewayList { get; set; }
        public IEnumerable<SurchargeTypeViewData> TypeList { get; set; }
        public IEnumerable<TechnologyFeeTypeViewData> TechnologyFeeList { get; set; }
        public Nullable<decimal> FleetFee { get; set; }
        public List<FleetListViewData> Fleets { get; set; }

        
    }
}