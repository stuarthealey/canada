﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table60
    {
        #region Private Constants and Variables
        #region Constants
        private const string tblId = "|36|30";
        #endregion

        #region Variables
        //For Visa, Master Card, Discover and Amex Electronic Commerce Indicator.
        private string defaultECommerceIndicator = "|30|37";//Default E-Commerce Indicator.
        #endregion
        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'36 30' Table 60
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        /// <summary>
        /// Gets or Sets the value of Bill Payment Type Indicator
        /// </summary>
        //public string BillPaymentTypeIndicator
        //{
        //    get { return defaultBillPaymentTypeIndicator; }
        //    set { defaultBillPaymentTypeIndicator = value; }
        //}

        /// <summary>
        /// Gets or Sets the value of ECommerce Indicator Type.
        /// </summary>
        public string ECommerceIndicator
        {
            get { return defaultECommerceIndicator; }
            set { defaultECommerceIndicator = value; }
        }

        /// <summary>
        /// Gets or Sets the value of Master Card ECommerce Indicator
        /// </summary>
        //public string MasterCardECommerceIndicator
        //{
        //    get { return defaultMCECommerceIndicator; }
        //    set { defaultMCECommerceIndicator = value; }
        //}

        /// <summary>
        /// Gets or Sets the value of Discover Ecommerce Indicator
        /// </summary>
        //public string DiscoverAndAMEXEcommerceIndicator
        //{
        //    get { return defaultDiscoverAndAMEXEcommerceIndicator; }
        //    set { defaultDiscoverAndAMEXEcommerceIndicator = value; }
        //}
        #endregion
    }
}