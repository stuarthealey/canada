﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class AuthResponse
    {


        [DataMember(EmitDefaultValue = false)]
        public AuthHeader Header { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public AuthTransactionDetails TransactionDetails  { get; set; }
        
    
    

    }
}