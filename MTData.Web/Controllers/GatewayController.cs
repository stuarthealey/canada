﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Web.Resource;
using MTData.Web.ViewData;
using Ninject;
using MTData.Utility;

namespace MTData.Web.Controllers
{
    public class GatewayController : Controller
    {
        readonly ILogger _logger = new Logger();
        private readonly IGatewayService _gatewayService;
        private readonly ISurchargeService _surchargeService;
        private StringBuilder _logMessage;
        private StringBuilder _logTracking;

        public GatewayController([Named("GatewayService")] IGatewayService gatewayService, [Named("SurchargeService")] ISurchargeService surchargeService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _gatewayService = gatewayService;
            _surchargeService = surchargeService;
            _logMessage = logMessage;
            _logTracking = logTracking;
        }

        /// <summary>
        /// Purpose             :   to display the gateway
        /// Function Name       :   Index
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   12/15/2014
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            return View();
        }

        /// <summary>
        /// Purpose             :   Creating gateway
        /// Function Name       :   Create
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   12/16/2104
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null && ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
            {
                var gatewayView = new GatewayViewData
                {
                    TimeZones = TimeZoneList(),
                    GatewayCurrencyList = GetGatewayCurrency(),
                    IsCancelSupported = true,
                    IsCaptureSupported = true,
                    IsPreauthSupported = true,
                    IsRefundSupported = true,
                    IsSaleSupported = true
                };
                _logMessage.Append("calling TimeZoneList method of same class");
                gatewayView.TimeZones = TimeZoneList();
                _logMessage.Append("TimeZoneList method Executed Successfully");
                _logMessage.Append("calling TimeZoneList method of same class");
                gatewayView.GatewayCurrencyList = GetGatewayCurrency();
                _logMessage.Append("GetGatewayCurrency method Executed Successfully");

                return View(gatewayView);
            }
            return RedirectToAction("index", "Member");
        }

        /// <summary>
        /// Purpose             :  Creating gateway
        /// Function Name       :   Create
        /// Created By          :   Madhuri Tanwar
        /// Created On          :    12/16/2104
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gatewayView"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(GatewayViewData gatewayView)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                       MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            if (!ModelState.IsValid)
            {
                _logMessage.Append("calling TimeZoneList method of same class");
                gatewayView.TimeZones = TimeZoneList();
                _logMessage.Append("TimeZoneList method Executed Successfully");
                _logMessage.Append("calling TimeZoneList method of same class");
                gatewayView.GatewayCurrencyList = GetGatewayCurrency();
                _logMessage.Append("GetGatewayCurrency method Executed Successfully");
                return View(gatewayView);
            }
            try
            {
                if (gatewayView.GatewayOffset ==MtdataResource.Select_timezone) //"Select Timezone")
                {
                    ModelState.AddModelError("GatewayOffset", MtdataResource.Please_Select_Gateway_Offset);
                    _logMessage.Append("calling TimeZoneList method of same class");
                    gatewayView.TimeZones = TimeZoneList();
                    _logMessage.Append("TimeZoneList method Executed Successfully");
                    _logMessage.Append("calling TimeZoneList method of same class");
                    gatewayView.GatewayCurrencyList = GetGatewayCurrency();
                    _logMessage.Append("GetGatewayCurrency method Executed Successfully");
                    return View(gatewayView);
                }
                _logMessage.Append("calling ToGatewayDto method to map GatewayViewData to GatewayDto and retrun GatewayViewData");
                GatewayDto gatewayDto = gatewayView.ToGatewayDto();
                _logMessage.Append("ToCountryViewDataList method executed successfully");
                _logMessage.Append("Calling Create method of _gatewayService class takes gatewayDto, gatewayDto properties UserName = " + gatewayView.UserName + ", Password = " + gatewayView.PCode + ", TimeZones= " + gatewayView.TimeZones + ", Name= " + gatewayView.Name + ", IsSaleSupported= " + gatewayView.IsSaleSupported + ", IsRefundSupported= " + gatewayView.IsRefundSupported + ", IsPreauthSupported= " + gatewayView.IsPreauthSupported + ", IsDefault= " + gatewayView.IsDefault + ", IsCaptureSupported= " + gatewayView.IsCaptureSupported + " IsCancelSupported= " + gatewayView.IsCancelSupported + " IsActive= " + "IsActive=" + gatewayView.IsActive + " GatewayUrl= " + gatewayView.GatewayUrl + ", GatewayOffset=" + gatewayView.GatewayOffset);
                bool result = _gatewayService.Create(gatewayDto);
                _logMessage.Append("Add Create of _gatewayService class executed successfully");
                if (result)
                {
                    _logTracking.Append("Gateway Name " + gatewayView.Name + " was Creadted by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                    return RedirectToAction("Index");
                }
                ViewBag.Message = MtdataResource.Gateway_Already_Exists; //"Internal Server Error";
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            gatewayView.TimeZones = TimeZoneList();
            gatewayView.GatewayCurrencyList = GetGatewayCurrency();
            return View(gatewayView);
        }

        /// <summary>
        /// Purpose             :  to edit the gateway
        /// Function Name       :   Edit
        /// Created By          :   Madhuri Tanwar
        /// Created On          :  12/20/2104
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gatewayId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Edit(int gatewayId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                                MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetSystemTimeZones method of TimeZoneInfo class of .net");

                GatewayDto gatewayDto = _gatewayService.GetGateway(gatewayId);
                GatewayViewData gatewayView = gatewayDto.ToGatewayViewData();
                gatewayView.TimeZones = TimeZoneList();
                gatewayView.GatewayCurrencyList = GetGatewayCurrency();
                return View(gatewayView);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// Purpose             :  to edit the gateway
        /// Function Name       :   Edit
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   12/20/2104
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gatewayView"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(GatewayViewData gatewayView)
        {
            try
            {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            if (Session["User"] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                       MethodBase.GetCurrentMethod().Name + ";");
            if (!ModelState.IsValid)
            {
                _logMessage.Append("calling TimeZoneList method of same class");
                gatewayView.TimeZones = TimeZoneList();
                _logMessage.Append("TimeZoneList method Executed Successfully");
                _logMessage.Append("calling TimeZoneList method of same class");
                gatewayView.GatewayCurrencyList = GetGatewayCurrency();
                _logMessage.Append("GetGatewayCurrency method Executed Successfully");
                return View(gatewayView);
            }
                if (gatewayView.GatewayOffset == MtdataResource.Select_timezone)    //"Select Timezone"
            {
                ModelState.AddModelError("GatewayOffset", MtdataResource.Please_Select_Gateway_Offset);
                gatewayView.TimeZones = TimeZoneList();
                gatewayView.GatewayCurrencyList = GetGatewayCurrency();
                return View(gatewayView);
            }
            _logMessage.Append("calling ToGatewayDto method to map GatewayViewData to GatewayDto and retrun GatewayViewData");
            GatewayDto gatewayDto = gatewayView.ToGatewayDto();
            _logMessage.Append("ToCountryViewDataList method executed successfully");
            _logMessage.Append("Calling Update method of _gatewayService class takes gatewayDto, gatewayDto properties UserName = " + gatewayView.UserName + ", Password = " + gatewayView.PCode + ", TimeZones= " + gatewayView.TimeZones + ", Name= " + gatewayView.Name + ", IsSaleSupported= " + gatewayView.IsSaleSupported + ", IsRefundSupported= " + gatewayView.IsRefundSupported + ", IsPreauthSupported= " + gatewayView.IsPreauthSupported + ", IsDefault= " + gatewayView.IsDefault + ", IsCaptureSupported= " + gatewayView.IsCaptureSupported + " IsCancelSupported= " + gatewayView.IsCancelSupported + " IsActive= " + "IsActive=" + gatewayView.IsActive + " GatewayUrl= " + gatewayView.GatewayUrl + ", GatewayOffset=" + gatewayView.GatewayOffset + ", GatewayId= " + gatewayView.GatewayID);
            bool result = _gatewayService.Update(gatewayDto);
            _logMessage.Append("Add Update of _gatewayService class executed successfully");
            if (result)
            {
                _logTracking.Append("Gateway Name " + gatewayView.Name + " was Updated by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                return RedirectToAction("Index");
            }
                ViewBag.Message = MtdataResource.Gateway_Already_Exists;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            gatewayView.TimeZones = TimeZoneList();
            gatewayView.GatewayCurrencyList = GetGatewayCurrency();
            return View(gatewayView);
            
        }

        /// <summary>
        /// Purpose             :  to get the list of time zone
        /// Function Name       :   TimeZoneList
        /// Created By          :   Madhuri Tanwar
        /// Created On          :  12/22/2104
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public List<TimeZoneViewData> TimeZoneList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                                MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetSystemTimeZones method of TimeZoneInfo class of .net");
                var timeZone = TimeZoneInfo.GetSystemTimeZones().Select(z => new TimeZoneViewData { TimeZoneName = z.DisplayName, TimeZoneTime = z.DisplayName }).ToList();
                var defaultValue = new TimeZoneViewData { TimeZoneTime = "Select Time Zone", TimeZoneName = "Select Time Zone" };
                _logMessage.Append("GetSystemTimeZones method of TimeZoneInfo class executed successfully");
                timeZone.Insert(0, defaultValue);
                return timeZone;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :  to get the list of  gateway currency 
        /// Function Name       :   GetGatewayCurrency
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   12/24/2014
        /// Modifications Made   :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<CurrencyViewData> GetGatewayCurrency()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                                MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetGatewayCurrency method of _gatewayService");
                var currencyDtoList = _gatewayService.GetGatewayCurrency();
                _logMessage.Append("GetGatewayCurrency method of _gatewayService executed successfully");
                _logMessage.Append("calling ToCurrencyViewDataList method to map CurrencyViewData to CurrencyDto and retrun CurrencyViewData");
                return currencyDtoList.ToCurrencyViewDataList();
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :  apply page indexing
        /// Function Name       :   GatewaytListByFiter
        /// Created By          :   Madhuri Tanwar
        /// Created On          :    4/2/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult GatewaytListByFiter(string name = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                               MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                int gatewayCount;
                var gateway = _gatewayService.GetGatewayByFilter(name, jtStartIndex, jtPageSize, jtSorting);
                _logMessage.Append("Calling GetMerchantByFilter method of _gatewayService class to get Gateways List");
                name = name.Trim();
                if (string.IsNullOrEmpty(name))
                {

                    _logMessage.Append("Calling GetGatewaysList method of _gatewayService class to count Gateways");
                    gatewayCount = _gatewayService.GatewaysListCount(name);
                    _logMessage.Append("GetGatewaysList method of _gatewayService executed successfully result = " + gatewayCount);
                    return Json(new { Result = "OK", Records = gateway, TotalRecordCount = gatewayCount });
                }
                var gatewayDtos = gateway as GatewayDto[] ?? gateway.ToArray();
                gatewayCount = _gatewayService.GatewaysListCount(name);
                _logMessage.Append("GetMerchantByFilter method of _gatewayService executed successfully");
                return Json(new { Result = "OK", Records = gatewayDtos, TotalRecordCount = gatewayCount });
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        /// Purpose             :  apply page indexing
        /// Function Name       :   Delete
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   4/2/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gatewayId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(int gatewayId)
        {

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                                MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling FindSurcharge method of _surchargeService class takes driverId= " + gatewayId);
                bool isExists = _surchargeService.FindSurcharge(gatewayId);
                if (isExists)
                {
                    _logMessage.Append("Can't delete the Gateway is in use");
                    ViewBag.Message = MtdataResource.Cannot_delete_gateway;  // "Can't delete the Gateway is in use";
                }
                else
                {
                    _logMessage.Append("Calling DeleteGateway method of _gatewayService class takes gatewayId= " + gatewayId);
                    _gatewayService.DeleteGateway(gatewayId);
                    _logMessage.Append("DeleteGateway method of _gatewayService executed successfully");
                    _logTracking.Append("Gateway Id " + gatewayId + " was Deleted by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                    ViewBag.Message = MtdataResource.Gateway_deleted_succesfully;  // "Gateway Deleted Successfully";
                }
               
                return Json(new { Result = "OK" });
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
                return Json(new { Result = "ERROR", exception.Message });
            }
        }

    }
}