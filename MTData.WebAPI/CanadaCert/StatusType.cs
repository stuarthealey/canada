﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CanadaCert
{
    public class StatusType
    {
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string StatusCode { get; set; }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value { get; set; }

    }
}