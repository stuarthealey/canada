begin tran abc
declare @tempMer table(id int identity(1,1),  UserId int )
insert @tempMer
 select distinct fk_MerchantID from Recipient where fk_UserTypeID  in(2) and fk_UserID is null --second
select * from @tempMer
declare @maxCount int,@min int=1,@merId int, @UserType int, @userID int
select @maxCount=count(*) from @tempMer
while (@min <= @maxCount)
BEGIN
	select  @userID=UserId  from @tempMer where id=@min
	if not exists(select 1 from Recipient where fk_MerchantID=@userID and fk_UserTypeID=2 and fk_ReportID in(7))
	insert into Recipient values(@userID,null,1,'admin@mtdata.com',GETDATE(),2,7)
	if not exists(select 1 from Recipient where fk_MerchantID=@userID and fk_UserTypeID=2 and fk_ReportID in(8))
	insert into Recipient values(@userID,null,1,'admin@mtdata.com',GETDATE(),2,8)
	set @min=@min+1
end
commit transaction abc
--rollback transaction abc
 --select * from Recipient
 GO