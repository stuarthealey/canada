IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MerchantUserDetailReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_MerchantUserDetailReport]
GO

CREATE PROCEDURE [dbo].[usp_MerchantUserDetailReport]
(
  @IsOnDemandReport BIT,
  @FromDate DATETIME=NULL,
  @ToDate DATETIME=NULL,
  @MerchantUserId INT,
  @ReportId INT,
  @XmlData NVARCHAR(MAX) OUT,
  @ReportType INT OUT,
  @FDate DATE OUT,
  @TDate DATE OUT
)
AS
BEGIN TRY
   SET ANSI_WARNINGS OFF
   SET NOCOUNT ON;
   DECLARE @NextScheduleDateTime DATETIME,
	       @ScheduleId INT
   DECLARE @Frequency INT,
           @FrequencyType INT,
           @DateFrom DATE,
           @DateTo DATE,
           @XmlTotal NVARCHAR(200),
           @CardType NVARCHAR(25),
           @InnerXml NVARCHAR(MAX),
		   @MerchantId INT
	 SELECT @MerchantId=fk_MerchantID FROM Users WHERE UserID=@MerchantUserId AND IsActive=1 AND fk_UserTypeID=4
     IF(@ReportId=2)
	 BEGIN
	 SELECT  @ScheduleId=ScheduleID,
	         @Frequency=Frequency,
             @FrequencyType=FrequencyType,
			 @NextScheduleDateTime=NextScheduleDateTime		 
     FROM ScheduleReport 
     WHERE fk_ReportId=2 AND IsCreated=1 AND fk_MerchantId IS NULL AND CorporateUserId IS NULL
	 END
	 IF(@ReportId IS NULL)
	 BEGIN
     SELECT  @ScheduleId=ScheduleID,
	         @Frequency=Frequency,
             @FrequencyType=FrequencyType,
			 @NextScheduleDateTime=NextScheduleDateTime		 
     FROM ScheduleReport 
     WHERE fk_ReportId=2 AND IsCreated=1 AND fk_MerchantId=@MerchantId
	 END
    --Set the dates in between transaction data to be prepared 
	IF(@IsOnDemandReport=1)
	  BEGIN
	   SET @DateFrom=@FromDate
	   SET @DateTo=@ToDate
	  END
	  ELSE
	   BEGIN
        IF(@Frequency=1) 
	    BEGIN       
          SET @DateFrom=GETDATE()-@FrequencyType
		  SET @DateTo=GETDATE()
		  SET @NextScheduleDateTime=@NextScheduleDateTime+@FrequencyType
        END
        ELSE IF(@Frequency=2) 
	    BEGIN
          SET @DateFrom = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-@FrequencyType,GETDATE())), 0)-1
          SET @DateTo= DATEADD(WEEK, DATEDIFF(WEEK, 0, GETDATE()), 0)-1
		  SET @NextScheduleDateTime=DATEADD(WK,@FrequencyType,@NextScheduleDateTime)
        END
       ELSE IF(@Frequency=3) 
	    BEGIN
         SET @DateFrom =DATEADD(MONTH, DATEDIFF(MONTH, 0,DATEADD(MONTH,-@FrequencyType,GETDATE())), 0)
         SET @DateTo=DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) 
	     SET @NextScheduleDateTime=DATEADD(MONTH,@FrequencyType,@NextScheduleDateTime)
       END  
	  ELSE
	  BEGIN
	   RETURN 1
	  END
	END
	SET @XmlData=''
    --Getting all the card types one by one 
    DECLARE @count INT = 1
    WHILE @count <=6 
    BEGIN
       IF(@count=1) 
       BEGIN
          SET @CardType='Amex'
       END
       ELSE IF(@count=2) 
       BEGIN
          SET @CardType='Discover'
       END
       ELSE IF(@count=3) 
       BEGIN
          SET @CardType='MasterCard'
       END
       ELSE IF(@count=4) 
       BEGIN
         SET @CardType='Visa'
       END 
	   ELSE IF(@count=5) 
       BEGIN
         SET @CardType='DinersClub'
       END  
       ELSE 
       BEGIN
         SET @CardType='JCB'
       END   
     --Prepare the transaction data in xml format corresponding to a that specific card type                      
     SET @InnerXml =ISNULL(CAST(( SELECT ISNULL(m.Company,'') AS 'td',''
                                                  ,CAST(GETDATE() AS DATE) AS 'td',''
												  ,t.Id AS 'td','',ISNULL(CAST(t.TxnDate AS DATE),'')  AS 'td',''
												  ,ISNULL(t.TxnType,'') AS 'td',''
												  ,ISNULL(t.CardType,'')  AS 'td',''
												  ,ISNULL(t.LastFourDigits,'')  AS 'td',''
												  ,ISNULL(t.AuthId,'') AS 'td',''
												  ,ISNULL(t.JobNumber,'') AS 'td',''
												  ,ISNULL(t.DriverNo,'') AS 'td' ,''												 
												  ,ISNULL(t.VehicleNo,'') AS 'td',''
												  ,CONCAT('$',CAST(ISNULL(t.TechFee,0.00) AS DECIMAL(16,2))) AS 'td',''
												  ,CONCAT('$',ISNULL(CAST(t.Surcharge AS DECIMAL(18,2)),0.00)) AS 'td',''
												  ,CONCAT('$',ISNULL(CAST(t.Amount AS DECIMAL(18,2)),0.00)) AS 'td',''
												  ,CONCAT('$',CAST(ISNULL(t.FleetFee,0.00)+ISNULL(t.Fee,0.00) AS DECIMAL(16,2))) AS 'td',''
												  ,CONCAT('$',CAST(ISNULL(t.Amount,0.00)-ISNULL(t.Fee ,0.00)-ISNULL(t.FleetFee ,0.00) AS DECIMAL(18,2))) AS 'td'								 
										     FROM MTDTransaction t 
										     INNER JOIN Vehicle v  ON t.VehicleNo=v.VehicleNumber 
                                             INNER JOIN Fleet f ON v.fk_FleetID=f.FleetID 
										     INNER JOIN Merchant m ON f.fk_MerchantID=m.MerchantID 
										     WHERE m.MerchantId=@MerchantID 
                                                   AND CAST(t.TxnDate AS DATE)>=@DateFrom 
												   AND CAST(t.TxnDate AS DATE) <@DateTo 
												   AND t.CardType=@CardType 
												   AND t.ResponseCode IN ('000','002') 
												   AND t.fk_FleetId=v.fk_FleetID
												   AND t.fk_FleetId IN (SELECT DISTINCT fk_FleetId FROM UserFleet WHERE fk_UserID=@MerchantUserId AND fk_MerchantId=@MerchantId AND IsActive=1 AND fk_UserTypeId=4)
                                             FOR XML PATH('tr'), ELEMENTS ) AS NVARCHAR(MAX)),'')
      SET  @XmlTotal= (SELECT ISNULL(SUM(tr.Amount),0.00)
	                   FROM dbo.MTDTransaction tr 
					   WHERE  tr.MerchantId=@MerchantID  
					          AND tr.TxnType IN ('Sale','Completion')
							  AND tr.IsRefunded=0 AND tr.IsVoided=0
							  AND tr.CardType=@CardType 
							  AND tr.ResponseCode IN ('000','002') 
							  AND CAST(tr.TxnDate AS DATE) >=@DateFrom 
		                      AND CAST(tr.TxnDate AS DATE) <@DateTo
							  AND tr.fk_FleetId IN (SELECT DISTINCT fk_FleetId FROM UserFleet WHERE fk_UserID=@MerchantUserId AND fk_MerchantId=@MerchantId AND IsActive=1 AND fk_UserTypeId=4))-
                       (SELECT ISNULL(SUM(tr.Fee),0)+ISNULL(SUM(tr.FleetFee),0)
	                   FROM dbo.MTDTransaction tr 
					   WHERE  tr.MerchantId=@MerchantID  
					          AND ((tr.TxnType='Sale' AND tr.IsRefunded=0 AND tr.IsVoided=0) 
							  OR (tr.TxnType='Authorization' AND tr.IsCompleted=1))
							  AND tr.CardType=@CardType 
							  AND tr.ResponseCode IN ('000','002') 
							  AND CAST(tr.TxnDate AS DATE) >=@DateFrom 
		                      AND CAST(tr.TxnDate AS DATE) <@DateTo
							  AND tr.fk_FleetId IN (SELECT DISTINCT fk_FleetId FROM UserFleet WHERE fk_UserID=@MerchantUserId AND fk_MerchantId=@MerchantId AND IsActive=1 AND fk_UserTypeId=4))							 
      IF(@InnerXml <>'' AND  @InnerXml IS NOT NULL) 
	  BEGIN
         SET @XmlTotal= '<tr><td colspan=16 style="text-align:center;background-color:blue">
	                         <div>'+CONCAT('Total for '+@CardType+''+' : ',CONCAT('$',CAST(@XmlTotal AS DECIMAL(18,2))))++' </div>
						</td></tr>'
      END
	  ELSE
      BEGIN
	    SET @XmlTotal=''
      END 
      SET @XmlData=@XmlData+@InnerXml+@XmlTotal
      SET @count = @count + 1  
    END
    SET @ReportType=@Frequency
    SET @FDate=@DateFrom
    SET @TDate= DATEADD(DAY,-1,@DateTo)
END TRY
--use catch to log error in table ProcErrorHandler
BEGIN CATCH
       INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
       SELECT ERROR_NUMBER()
	         ,ERROR_SEVERITY()
			 ,ERROR_STATE()
			 ,ERROR_PROCEDURE()
		     ,ERROR_LINE()
			 ,ERROR_MESSAGE()
			 ,SUSER_SNAME()
			 ,HOST_NAME()
			 ,GETDATE()
END CATCH
GO
