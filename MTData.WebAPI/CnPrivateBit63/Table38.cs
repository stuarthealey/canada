﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table38
    {
        #region Private Constants and Variables
        #region Constants
        private const string tblId = "|33|38";
        #endregion

        #region Variables

        private string reversalReasonCode = string.Empty;
        #endregion
        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'33s 38' Table 38
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        /// <summary>
        /// gets or sets the value of reversal reason code
        /// </summary>
        public string ReversalReasonCode
        {
            get { return reversalReasonCode; }
            set { reversalReasonCode = value; }
        }
        #endregion
    }
}