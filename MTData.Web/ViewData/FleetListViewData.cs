﻿namespace MTData.Web.ViewData
{
    public class FleetListViewData
    {
        public int FleetID { get; set; }
        public string FleetName { get; set; }
    }
}