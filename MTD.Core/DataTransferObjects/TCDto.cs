﻿namespace MTD.Core.DataTransferObjects
{
  public  class TCDto
    {
      
      public int DefaultID { get; set; }
      public string TermCondition { get; set; }
      public string Disclaimer { get; set; }
    }
}
