﻿using MTData.Web.Resource;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MTData.Web.ViewData
{
    public class PrefixViewData
    {
        public int PrefixID { get; set; }
        [Required(ErrorMessageResourceType = typeof(MtdataResource), ErrorMessageResourceName = "Please_enter_card_prefix")]
        public int? CCPrefix1 { get; set; }
        [Required(ErrorMessageResourceType = typeof(MtdataResource), ErrorMessageResourceName = "Please_select_gateway")]
        public int? fk_GatewayID { get; set; }
        public int? fk_MerchantId { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string Name { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public IEnumerable<GatewayListViewData> GatewayList { get; set; }
    }
}