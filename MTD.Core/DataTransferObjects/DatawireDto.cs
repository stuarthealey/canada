﻿using System;

namespace MTD.Core.DataTransferObjects
{
   public class DatawireDto
    {
        public int ID { get; set; }
        public string DID { get; set; }
        public string DatewireXml { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> IsAssigned { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> MerchantId { get; set; }
        public string URL { get; set; }
        //public string SerialNo { get; set; }
        public string RCTerminalId { get; set; }
       // ignore
        public string Stan { get; set; }
        public string RefNumber { get; set; }
        public DateTime? TransactionDate { get; set; }

    }
}
