﻿using System;
using System.Net.Http;
using System.Text;
using System.Xml;
using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.WebAPI.Models;
using Ninject;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using MTData.WebAPI.Resources;
using MTData.WebAPI.Common;
using System.Web.Configuration;
using MTData.WebAPI.Controllers;
using System.Globalization;

namespace MTData.WebAPI
{
    public class BookerAppToken : TransactionRequestController
    {
        #region (Constructor Loading)
        private LocalVariables objLocalVars = new LocalVariables();
        private readonly ITransactionService _transactionService;
        private static ITerminalService _terminalService;
        private readonly IMerchantService _merchantService;
        private readonly ICommonService _commonService;
        private StringBuilder _logMessage;
        private StringBuilder _logTracking;

        public BookerAppToken(
            [Named("TransactionService")] ITransactionService transactionService,
            [Named("TerminalService")] ITerminalService terminalService,
            [Named("MerchantService")] IMerchantService merchantService,
            [Named("CommonService")] ICommonService commonService,
            StringBuilder logMessage, StringBuilder logTracking)
            : base(terminalService, logMessage)
        {
            _transactionService = transactionService;
            _terminalService = terminalService;
            _merchantService = merchantService;
            _logMessage = logMessage;
            _logTracking = logTracking;
            _commonService = commonService;
        }
        #endregion

        /// <summary>
        /// Purpose             :   Creating booker app token 
        /// Function Name       :   BookerAppToken
        /// Created By          :   Salil Gupta
        /// Created On          :   07/04/2015
        /// Modification Made   :   Refactored and added Logging, tracing, error handling
        /// Modified By         :   Sunil Singh
        /// Modified At         :   03-Sep-2015
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public XmlDocument CreateToken(HttpRequestMessage request, fDMerchantDto _fdMerchantobj, DatawireDto _datawireObj, ILogger _logger)
        {
            _logMessage.AppendLine("BookerApp CreateToken process start:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");

            /*Token request in xml format */
            CreateRCCIResponse tokenResponse = new CreateRCCIResponse();
            TransactionRequest tr = new TransactionRequest();
            BookerAppTokenResp soapResp = new BookerAppTokenResp();
            try
            {
                XNamespace ns = "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService";
                var resp = new XmlDocument();

                _logMessage.AppendLine("[BookerApp > CreateToken] Loading request xml");
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                _logMessage.AppendLine("[BookerApp > CreateToken] Parsing request xml by specified namespace");
                var requestXML = XDocument.Parse(xmlDoc.InnerXml);

                //Initializing local variables.
                _logMessage.AppendLine("[BookerApp > CreateToken] initializing local variables from request xml");
                SetLocalVarFromXMLRequest(requestXML, ns);

                _logTracking.Append("BookerApp CreateToken process start. DeviceID: " + objLocalVars._deviceId + " at " + System.DateTime.Now.ToUniversalTime());


                #region firstDataDetails
                //Retreiving firstDataDetails for TokenType
                _logMessage.AppendLine("[BookerApp > CreateToken] _commonService.GetFirstDataDetails(objLocalVars._deviceId, (string)null, (string)null)");
                var firstDataDetails = _commonService.GetFirstDataDetails(objLocalVars._deviceId, (string)null, (string)null, Convert.ToInt32(objLocalVars.FleetId), objLocalVars._requestType);
                if (firstDataDetails.IsNull())
                {
                    _logMessage.AppendLine("[BookerApp > CreateToken] Failed executing _commonService.GetFirstDataDetails(objLocalVars._deviceId, (string)null, (string)null)");
                    _logger.LogInfoMessage(_logTracking.ToString());

                    tokenResponse = CommonFunctions.BookerAppFailedResponse(tr, PaymentAPIResources.InvalidTerminal);
                    return soapResp.ConstructDoc(tokenResponse.CreateRCCIResult);
                }
                #endregion

                #region terminal
                //Getting terminal detail based on DeviceId
                _logMessage.AppendLine("[BookerApp > CreateToken] executing _terminalService.GetTerminalDto(bookerAppDeviceId)");
                var terminal = _terminalService.GetTerminalDto(objLocalVars._deviceId);
                if (terminal.IsNull())
                {
                    _logMessage.AppendLine("[BookerApp > CreateToken] Failed executing _terminalService.GetTerminalDto(bookerAppDeviceId)");
                    _logger.LogInfoMessage(_logTracking.ToString());

                    tokenResponse = CommonFunctions.BookerAppFailedResponse(tr, PaymentAPIResources.InvalidTerminal);
                    return soapResp.ConstructDoc(tokenResponse.CreateRCCIResult);
                }
                #endregion

                #region _fdMerchantobj
                //Retriving merchant data from database
                _logMessage.AppendLine("[BookerApp > CreateToken] executing _merchantService.GetFDMerchant(terminal.fk_MerchantID)");
                _fdMerchantobj = _merchantService.GetFDMerchant(terminal.fk_MerchantID);
                if (_fdMerchantobj.IsNull())
                {
                    _logMessage.AppendLine("[BookerApp > CreateToken] Failed executing _merchantService.GetFDMerchant(terminal.fk_MerchantID)");
                    _logger.LogInfoMessage(_logTracking.ToString());

                    tokenResponse = CommonFunctions.BookerAppFailedResponse(tr, PaymentAPIResources.FdMerchantNotExist);
                    return soapResp.ConstructDoc(tokenResponse.CreateRCCIResult);
                }
                #endregion

                #region _datawireObj
                /* Generate Client Ref Number in the format <STAN>|<TPPID>, right justified and left padded with "0" */
                _logMessage.AppendLine("[BookerApp > CreateToken] executing _merchantService.GetDataWireDetails(terminal.fk_MerchantID, Convert.ToInt32(terminal.DatawireId)");
                _datawireObj = _merchantService.GetDataWireDetails(terminal.fk_MerchantID, Convert.ToInt32(terminal.DatawireId));
                if (_datawireObj.IsNull())
                {
                    _logMessage.AppendLine("[BookerApp > CreateToken] Failed to execute _merchantService.GetDataWireDetails(terminal.fk_MerchantID, Convert.ToInt32(terminal.DatawireId)");
                    _logger.LogInfoMessage(_logTracking.ToString());

                    tokenResponse = CommonFunctions.BookerAppFailedResponse(tr, PaymentAPIResources.DatawireDetailsNull);
                    return soapResp.ConstructDoc(tokenResponse.CreateRCCIResult);
                }
                #endregion

                #region Initializing instance of TransactionRequest
                //Generate transaction data
                tr.FdMerchantId = Convert.ToString(_fdMerchantobj.FDMerchantID); //MTData
                tr.MtdMerchantId = terminal.fk_MerchantID;
                tr.SerialNumber = terminal.SerialNo;
                tr.TerminalId = _datawireObj.RCTerminalId;
                tr.PaymentType = objLocalVars._paytype;
                tr.CardNumber = objLocalVars._ccno;
                tr.ExpiryDate = objLocalVars._expirydate;
                tr.IndustryType = objLocalVars._industryType;
                tr.TppId = _fdMerchantobj.ProjectId;
                tr.GroupId = Convert.ToString(_fdMerchantobj.GroupId);
                tr.Did = _datawireObj.DID;
                tr.App = _fdMerchantobj.App;
                tr.ServiceId = Convert.ToString(_fdMerchantobj.ServiceId);
                tr.ServiceUrl = _fdMerchantobj.ServiceUrl;
                tr.CrdHldrName = objLocalVars.Name;
                tr.LocalDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                tr.TrnmsnDateTime = DateTime.UtcNow.ToString("yyyyMMddHHmmss");
                tr.CrdHldrPhone = objLocalVars.Phone;
                tr.CreatedBy = "BookerAppToken";
                tr.TokenType = firstDataDetails.TokenType;

                CreateTransactionRequest(tr);
                #endregion

                //Get XML Data
                string xmlSerializedTransReq = GetXmlData();

                //Save transaction into MTDTransaction Table
                MTDTransactionDto transactionDto = CommonFunctions.XmlToTransactionDto(tr);
                int transId = _transactionService.Add(transactionDto);
                _terminalService.UpdateStan(transactionDto.Stan, transactionDto.TransRefNo, tr.TerminalId, tr.MtdMerchantId);
                string clientRef = GetClientRef(_fdMerchantobj.ProjectId);
                //end Save transaction into MTDTransaction Table


                //Save masked card data into database.
                CardTokenDto cardTokenDto = CommonFunctions.XmlToCardTokenDto(tr);
                cardTokenDto.Id = _transactionService.ManageCardToken(cardTokenDto);


                /* Send data using SOAP protocol to Datawire */
                string xmlSerializedTransResp = new SoapHandler().SendMessage(xmlSerializedTransReq, clientRef, tr);


                //update response of transaction
                MTDTransactionDto responsetransactionDto;
                if (string.IsNullOrEmpty(xmlSerializedTransResp))
                {
                    responsetransactionDto = CommonFunctions.XmlResponseToTransactionDto(
                        xmlSerializedTransResp, true, tr.PaymentType.ToString(), tr.IndustryType);
                    _transactionService.Update(responsetransactionDto, transId, tr.RapidConnectAuthId);
                }
                else
                {
                    responsetransactionDto = CommonFunctions.XmlResponseToTransactionDto(
                        xmlSerializedTransResp, false, tr.PaymentType.ToString(), tr.IndustryType);
                    _transactionService.Update(responsetransactionDto, transId, tr.RapidConnectAuthId);
                }


                //Update Card Token response into database.
                if (!string.IsNullOrEmpty(xmlSerializedTransResp))
                {
                    cardTokenDto.Token = responsetransactionDto.CardToken;
                    _transactionService.ManageCardToken(cardTokenDto);
                }

                //sending timeout
                if (string.IsNullOrEmpty(xmlSerializedTransResp))
                {
                    TimeoutTransaction objTimeout = new TimeoutTransaction(_transactionService, _terminalService, _merchantService, _logMessage);
                    xmlSerializedTransResp = objTimeout.TimeOut(tr, transId, clientRef, xmlSerializedTransResp);
                }

                if (!string.IsNullOrEmpty(xmlSerializedTransResp))
                    tokenResponse = CommonFunctions.TokenResponse(tr, xmlSerializedTransResp);

                //Create SOAP response
                var xmlResp = soapResp.ConstructDoc(tokenResponse.CreateRCCIResult);

                _logTracking.Append("BookerApp CreateToken process completed. DeviceID: " + objLocalVars._deviceId + " at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                _logger.LogInfoMessage(_logMessage.ToString());

                return xmlResp;
            }
            catch (Exception ex)
            {
                tokenResponse = CommonFunctions.BookerAppFailedResponse(tr, PaymentAPIResources.Exception);
                var xmlResp = soapResp.ConstructDoc(tokenResponse.CreateRCCIResult);

                _logger.LogInfoFatel(_logMessage.ToString(), ex);
                return soapResp.ConstructDoc(tokenResponse.CreateRCCIResult);
            }
        }

        /// <summary>       
        /// Purpose             :   Initializign local variables.
        /// Function Name       :   SetLocalVarFromXMLRequest
        /// Created By          :   Sunil Singh
        /// Created On          :   03-Sep-2015
        /// Modification Made   :   
        /// Modified By         :   
        /// Modified At         :  
        /// </summary>
        /// <param name="argElements"></param>
        /// <param name="ns2"></param>
        private void SetLocalVarFromXMLRequest(XDocument requestXML, XNamespace ns)
        {
            try
            {
                objLocalVars._deviceId = WebConfigurationManager.AppSettings["BookerAppDeviceId"];
                objLocalVars._paytype = (PymtTypeType)Enum.Parse(typeof(PymtTypeType), "Credit");
                objLocalVars._industryType = PaymentAPIResources.BookerAppIndustryType;

                //Loading CustomerDetails
                var custDetail = requestXML.Descendants(ns + "CustomerDetails").FirstOrDefault();
                string firstName = (string)custDetail.Element(ns + "FirstName");
                string surName = (string)custDetail.Element(ns + "Surname");
                objLocalVars.Name = firstName + " " + surName;
                objLocalVars.Phone = (string)custDetail.Element(ns + "ContactPhone");


                //Loading FullCreditCardDetails node
                var crdDtl = requestXML.Descendants(ns + "FullCreditCardDetails").FirstOrDefault();
                string expMth = (string)crdDtl.Element(ns + "ExpiryMonth");
                string expYr = (string)crdDtl.Element(ns + "ExpiryYear");
                string pan = (string)crdDtl.Element(ns + "PAN");

                //Loading Encryption node
                var encryption = requestXML.Descendants(ns + "Encryption").FirstOrDefault();
                string encMthd = (string)encryption.Element(ns + "Method");
                string encKyVrsn = (string)encryption.Element(ns + "KeyVersion");
                string encTkn = (string)encryption.Element(ns + "Token");

                int mnth = Convert.ToInt32(encKyVrsn);
                encTkn = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(mnth).ToLower();

                //Decrypting credit card details
                expMth = expMth.DecryptRijndael(encTkn);
                expYr = expYr.DecryptRijndael(encTkn);

                objLocalVars._ccno = pan.DecryptRijndael(encTkn);
                string yr = DateTime.Now.ToString("yyyy");
                objLocalVars._expirydate = yr.Substring(0, 2) + expYr + expMth;
            }
            catch
            {
                throw;
            }
        }

    }
}