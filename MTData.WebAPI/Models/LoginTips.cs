﻿using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class LoginTips
    {
        //[DataMember(EmitDefaultValue = false)]
        //public decimal? TipPercentageLow{get;set;}

        private decimal? tipPercentageLow;

        [DataMember]
        public decimal? TipPercentageLow
        {
            get { return this.tipPercentageLow ?? default(decimal); }
            set { this.tipPercentageLow = value; }
        }


        //[DataMember(EmitDefaultValue = false)]
        //public decimal? TipPercentageMedium{get;set;}

        private decimal? tipPercentageMedium;

        [DataMember]
        public decimal? TipPercentageMedium
        {
            get { return this.tipPercentageMedium ?? default(decimal); }
            set { this.tipPercentageMedium = value; }
        }
        
        //[DataMember(EmitDefaultValue = false)]
        //public decimal? TipPercentageHigh{get;set;}

        private decimal? tipPercentageHigh;

        [DataMember]
        public decimal? TipPercentageHigh
        {
            get { return this.tipPercentageHigh ?? default(decimal); }
            set { this.tipPercentageHigh = value; }
        }
        
    }
}