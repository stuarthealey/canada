﻿namespace MTData.Web.ViewData
{
    public class GatewayListViewData
    {
        public int GatewayID { get; set; }
        public string Name { get; set; }
    }
}