﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table56
    {
        #region "Private Constants and variables"

        #region Constants
        
        private const string tblId = "|35|36";
        
        #endregion
        
        #region "Public Properties"

        /// <summary>
        /// gets or sets the Table ID
        /// </summary>
        public string TableID { get { return tblId; } }

        /// <summary>
        /// gets or sets the Merchant Name
        /// </summary>
        public string MerchantName { get; set; }

        /// <summary>
        /// gets or sets the Merchant street address
        /// </summary>
        public string MerchantStreetAddress { get; set; }
        /// <summary>
        /// gets or sets the Merchant city
        /// </summary>
        public string MerchantCity { get; set; }

        /// <summary>
        /// gets or sets the Merchant state code numeric value.
        /// </summary>
        public string MerchantNumericStateCode { get; set; }

        /// <summary>
        /// gets or sets the Merchant state code Alpha value.
        /// </summary>
        public string MerchantAlphaStateCode { get; set; }

        /// <summary>
        /// gets or sets the merchant country code numeric value.
        /// </summary>
        public string MerchantNumericCountryCode { get; set; }

        /// <summary>
        /// gets or set the merchant country code alpha values.
        /// </summary>
        public string MerchantAlphaCountryCode { get; set; }

        /// <summary>
        /// gets or sets the merchant zip code.
        /// </summary>
        public string MerchantZipcode { get; set; }

        #endregion

        #endregion
    }
}