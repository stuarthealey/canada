﻿using System;
using MTD.Data.Data;

namespace MTD.Core.DataTransferObjects
{
    public class Receipt
    {
        public int ReceiptID { get; set; }
        public int fk_FleetID { get; set; }
        public bool IsFleet { get; set; }
        public bool IsVehicle { get; set; }
        public bool IsDriver { get; set; }
        public bool IsDriverTaxNo { get; set; }
        public bool IsCompName { get; set; }
        public bool IsCompAdd { get; set; }
        public bool IsComPhone { get; set; }
        public bool IsPickAdd { get; set; }
        public bool IsDestAdd { get; set; }
        public bool IsFlagFall { get; set; }
        public bool IsFare { get; set; }
        public bool IsExtra { get; set; }
        public bool IsTolls { get; set; }
        public bool IsTip { get; set; }
        public bool IsStateTaxPer { get; set; }
        public bool IsStateTaxAmt { get; set; }
        public bool IsFederalTaxPer { get; set; }
        public bool IsFederalTaxAmt { get; set; }
        public bool IsSurPer { get; set; }
        public bool IsSurAmt { get; set; }
        public bool IsSubTotal { get; set; }
        public bool IsTotal { get; set; }
        public bool IsMerName { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public virtual Fleet Fleet { get; set; }
    }
}