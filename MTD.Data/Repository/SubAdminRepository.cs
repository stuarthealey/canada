﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

using MTD.Data.Repository.Interface;
using MTD.Data.Data;

namespace MTD.Data.Repository
{
    public class SubAdminRepository : BaseRepository, ISubAdminRepository
    {
        public SubAdminRepository(IMtDataDevlopmentsEntities dbMtData) : base(dbMtData)
        {
        }

        /// <summary>
        /// Purpose             :   To get merchant list by filter
        /// Function Name       :   GetMerchantByFilter
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/29/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <param name="sorting"></param>
        /// <returns></returns>
        public IEnumerable<User> GetMerchantByFilter(PageSizeSearch pageSizeSearch)
        {
            IEnumerable<User> query = new List<User>();
            if (pageSizeSearch.UserType != 1)
            {
                query = (from users in DbMtData.Users
                         join countries in DbMtData.CountryCodes on users.fk_Country equals countries.CountryCodesID
                         where users.fk_UserTypeID == 5 && users.IsActive && users.ParentUserID == pageSizeSearch.UserId
                         select new { Users = users, CountryCodes = countries }).AsEnumerable().Select(objNew =>
                       new User
                       {
                           FName = objNew.Users.FName + " " + objNew.Users.LName,
                           Email = objNew.Users.Email,
                           Address = objNew.Users.Address + ", " + objNew.Users.fk_City + ", " + objNew.Users.fk_State + ", " + objNew.CountryCodes.Title,
                           Phone = objNew.Users.Phone,
                           IsActive = objNew.Users.IsActive,
                           IsLockedOut = objNew.Users.IsLockedOut,
                           UserID = objNew.Users.UserID,
                           fk_UserTypeID = objNew.Users.fk_UserTypeID,
                           CreatedDate = objNew.Users.CreatedDate
                       }).ToList();
            }
            else
            {
                query = (from users in DbMtData.Users
                         join countries in DbMtData.CountryCodes on users.fk_Country equals countries.CountryCodesID
                         where users.fk_UserTypeID == 5 && users.IsActive && users.ParentUserID == null
                         select new { Users = users, CountryCodes = countries }).AsEnumerable().Select(objNew =>
                      new User
                      {
                          FName = objNew.Users.FName + " " + objNew.Users.LName,
                          Email = objNew.Users.Email,
                          Address = objNew.Users.Address + ", " + objNew.Users.fk_City + ", " + objNew.Users.fk_State + ", " + objNew.CountryCodes.Title,
                          Phone = objNew.Users.Phone,
                          IsActive = objNew.Users.IsActive,
                          IsLockedOut = objNew.Users.IsLockedOut,
                          UserID = objNew.Users.UserID,
                          fk_UserTypeID = objNew.Users.fk_UserTypeID,
                          CreatedDate = objNew.Users.CreatedDate
                      }).ToList();
            }

            query = query.OrderBy(p => p.FName); //Default!
            
            if (!string.IsNullOrEmpty(pageSizeSearch.Name))
                query = query.Where(p => p.FName.IndexOf(pageSizeSearch.Name, StringComparison.OrdinalIgnoreCase) >= 0);
            
            if (!string.IsNullOrEmpty(pageSizeSearch.JtSorting))
                query = SortedMerchantUser(pageSizeSearch.JtSorting, query);
            
            List<User> mUsers = new List<User>();
            foreach (var mu in query)
            {
                string myMerchants = string.Empty;
                List<UserMerchant> userMerchents = DbMtData.UserMerchants.Where(x => x.fk_UserID == mu.UserID).ToList();
                List<Merchant> merchants = DbMtData.Merchants.Where(x => x.IsActive).ToList();
                string[] arry = merchants.Where(x => userMerchents.Select(y => y.fk_MerchantID).Contains(Convert.ToInt32(x.MerchantID))).Select(x => x.Company).ToArray();
                string myAllMerchant = string.Join(", ", arry);

                mu.MerchantListId = myAllMerchant;
                mu.Address = mu.Address.Replace(", , ,", ", ");
                mu.Address = mu.Address.Replace(", ,", ", ");
                mUsers.Add(mu);
            }

            return pageSizeSearch.JtPageSize > 0
                        ? mUsers.Skip(pageSizeSearch.JtStartIndex).Take(pageSizeSearch.JtPageSize).ToList() //Paging
                        : mUsers.ToList(); //No paging
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<User> SortedMerchantUser(string sorting, IEnumerable<User> query)
        {
            switch (sorting)
            {
                case "FName ASC":
                    query = query.OrderBy(p => p.FName);
                    break;
                case "FName DESC":
                    query = query.OrderByDescending(p => p.FName);
                    break;
                case "LName ASC":
                    query = query.OrderBy(p => p.LName);
                    break;
                case "LName DESC":
                    query = query.OrderByDescending(p => p.LName);
                    break;
                case "Email ASC":
                    query = query.OrderBy(p => p.Email);
                    break;
                case "Email DESC":
                    query = query.OrderByDescending(p => p.Email);
                    break;
                case "CreatedDate ASC":
                    query = query.OrderBy(p => p.CreatedDate);
                    break;
                case "CreatedDate DESC":
                    query = query.OrderByDescending(p => p.CreatedDate);
                    break;
            }

            return query;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<MerchantUser> SortedMyMerchantUser(string sorting, IEnumerable<MerchantUser> query)
        {
            switch (sorting)
            {
                case "Company ASC":
                    query = query.OrderBy(p => p.Company);
                    break;
                case "Company DESC":
                    query = query.OrderByDescending(p => p.Company);
                    break;
                case "LName ASC":
                    query = query.OrderBy(p => p.LName);
                    break;
                case "LName DESC":
                    query = query.OrderByDescending(p => p.LName);
                    break;
                case "Email ASC":
                    query = query.OrderBy(p => p.Email);
                    break;
                case "Email DESC":
                    query = query.OrderByDescending(p => p.Email);
                    break;
                case "CreatedDate ASC":
                    query = query.OrderBy(p => p.CreatedDate);
                    break;
                case "CreatedDate DESC":
                    query = query.OrderByDescending(p => p.CreatedDate);
                    break;
            }

            return query;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subAdminId"></param>
        /// <returns></returns>
        public bool Remove(int subAdminId)
        {
            try
            {
                var userDb = DbMtData.Users.Find(subAdminId);
                userDb.IsActive = false;
                
                var tempUser = DbMtData.UserMerchants.Where(x => x.fk_UserID == subAdminId).ToList();
                if (tempUser != null)
                    tempUser.ForEach(x => x.IsActive = false);
                
                var tempFleet = DbMtData.UserFleets.Where(x => x.fk_UserID == subAdminId).ToList();                
                if (tempFleet != null)
                    tempFleet.ForEach(x => x.IsActive = false);

                return Convert.ToBoolean(DbMtData.CommitChanges());
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get list of merchants
        /// Function Name       :   MerchantList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/28/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<User> SubAdminList(PageSizeSearch pageSizeSearch)
        {
            try
            {
                if (pageSizeSearch.UserType != 1)
                {
                    IEnumerable<User> merchantLi = (from users in DbMtData.Users
                                                    join countries in DbMtData.CountryCodes on users.fk_Country equals countries.CountryCodesID
                                                    where users.fk_UserTypeID == 5 && users.IsActive && users.ParentUserID == pageSizeSearch.UserId
                                                    select new { Users = users, CountryCodes = countries }).AsEnumerable().Select(objNew =>
                          new User
                          {

                              FName = objNew.Users.FName + " " + objNew.Users.LName,
                              Email = objNew.Users.Email,
                              Address = objNew.Users.Address + ", " + objNew.Users.fk_City + ", " + objNew.Users.fk_State + ", " + objNew.CountryCodes.Title,
                              Phone = objNew.Users.Phone,
                              IsActive = objNew.Users.IsActive,
                              IsLockedOut = objNew.Users.IsLockedOut,
                              UserID = objNew.Users.UserID,
                              fk_UserTypeID = objNew.Users.fk_UserTypeID,
                              CreatedDate = objNew.Users.CreatedDate
                          }).ToList();

                    if (!string.IsNullOrEmpty(pageSizeSearch.Name))
                        merchantLi = merchantLi.Where(p => p.FName.IndexOf(pageSizeSearch.Name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();

                    return merchantLi;
                }
                else
                {
                    IEnumerable<User> merchantLi = (from users in DbMtData.Users
                                                    join countries in DbMtData.CountryCodes on users.fk_Country equals countries.CountryCodesID
                                                    where users.fk_UserTypeID == 5 && users.IsActive && users.ParentUserID == null
                                                    select new { Users = users, CountryCodes = countries }).AsEnumerable().Select(objNew =>
                         new User
                         {

                             FName = objNew.Users.FName + " " + objNew.Users.LName,
                             Email = objNew.Users.Email,
                             Address = objNew.Users.Address + ", " + objNew.Users.fk_City + ", " + objNew.Users.fk_State + ", " + objNew.CountryCodes.Title,
                             Phone = objNew.Users.Phone,
                             IsActive = objNew.Users.IsActive,
                             IsLockedOut = objNew.Users.IsLockedOut,
                             UserID = objNew.Users.UserID,
                             fk_UserTypeID = objNew.Users.fk_UserTypeID,
                             CreatedDate = objNew.Users.CreatedDate
                         }).ToList();

                    if (!string.IsNullOrEmpty(pageSizeSearch.Name))
                        merchantLi = merchantLi.Where(p => p.FName.IndexOf(pageSizeSearch.Name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();

                    return merchantLi;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get merchant list by filter
        /// Function Name       :   GetMerchantByFilter
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/29/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <param name="sorting"></param>
        /// <returns></returns>
        public IEnumerable<MerchantUser> GetMyMerchantByFilter(PageSizeSearch pageSizeSearch)
        {
            try
            {
                IEnumerable<MerchantUser> query = (from users in DbMtData.Users
                                                   join merchants in DbMtData.Merchants on users.fk_MerchantID equals merchants.MerchantID
                                                   join countries in DbMtData.CountryCodes on users.fk_Country equals countries.CountryCodesID
                                                   where merchants.IsActive && users.UserType.UserTypeID == 2 && (bool)users.IsActive
                                                   select new { Merchants = merchants, Users = users, CountryCodes = countries }).AsEnumerable().Select(objNew =>
                  new MerchantUser
                  {
                      City = objNew.Users.fk_City,
                      State = objNew.Users.fk_State,

                      Company = objNew.Merchants.Company,
                      LName = objNew.Users.FName + " " + objNew.Users.LName,
                      FName = objNew.Users.FName,
                      Email = objNew.Users.Email,
                      Address = objNew.Users.Address + ", " + objNew.Users.fk_City + ", " + objNew.Users.fk_State + ", " + objNew.CountryCodes.Title,
                      Country = objNew.CountryCodes.Title,
                      Phone = objNew.Users.Phone,
                      IsActive = objNew.Users.IsActive,
                      IsLockedOut = objNew.Users.IsLockedOut,
                      MerchantID = objNew.Merchants.MerchantID,
                      UserID = objNew.Users.UserID,
                      CreatedDate = objNew.Users.CreatedDate
                  }).ToList();

                var merchantList = DbMtData.UserMerchants.ToList().Where(x => x.IsActive && x.fk_UserID == pageSizeSearch.UserId);
                query = query.Where(x => merchantList.Select(y => y.fk_MerchantID).Contains(x.MerchantID)).OrderBy(x => x.Company).ToList();

                //Filters
                if (!string.IsNullOrEmpty(pageSizeSearch.Name))
                    query = query.Where(p => p.Company.IndexOf(pageSizeSearch.Name, StringComparison.OrdinalIgnoreCase) >= 0);

                if (!string.IsNullOrEmpty(pageSizeSearch.JtSorting))
                    query = SortedMyMerchantUser(pageSizeSearch.JtSorting, query);

                List<MerchantUser> mUsers = new List<MerchantUser>();
                foreach (var mu in query)
                {
                    mu.Address = mu.Address.Replace(", , ,", ", ");
                    mu.Address = mu.Address.Replace(", ,", ", ");
                    mUsers.Add(mu);
                }

                return pageSizeSearch.JtPageSize > 0
                            ? mUsers.Skip(pageSizeSearch.JtStartIndex).Take(pageSizeSearch.JtPageSize).ToList() //Paging
                            : mUsers.ToList(); //No paging
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get list of merchants
        /// Function Name       :   MerchantList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/28/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MerchantUser> MyMerchantList(string name, int userId)
        {
            try
            {
                IEnumerable<MerchantUser> merchantLi = (from users in DbMtData.Users
                                                        join merchants in DbMtData.Merchants on users.fk_MerchantID equals merchants.MerchantID
                                                        join countries in DbMtData.CountryCodes on users.fk_Country equals countries.CountryCodesID
                                                        where merchants.IsActive && users.UserType.UserTypeID == 2
                                                        select new { Merchants = merchants, Users = users, CountryCodes = countries }).AsEnumerable().Select(objNew =>
                                                                 new MerchantUser
                                                                 {
                                                                     Company = objNew.Merchants.Company,
                                                                     FName = objNew.Users.FName,
                                                                     LName = objNew.Users.LName,
                                                                     Email = objNew.Users.Email,
                                                                     Address = objNew.Users.Address,
                                                                     City = objNew.Users.fk_City,
                                                                     State = objNew.Users.fk_State,
                                                                     Country = objNew.CountryCodes.Title,
                                                                     Phone = objNew.Users.Phone,
                                                                     IsActive = objNew.Users.IsActive,
                                                                     IsLockedOut = objNew.Users.IsLockedOut,
                                                                     MerchantID = objNew.Merchants.MerchantID,
                                                                     UserID = objNew.Users.UserID
                                                                 }).ToList();
                
                var merchantList = DbMtData.UserMerchants.ToList().Where(x => x.IsActive && x.fk_UserID == userId);
                merchantLi = merchantLi.Where(x => merchantList.Select(y => y.fk_MerchantID).Contains(x.MerchantID)).ToList();
                if (!string.IsNullOrEmpty(name))
                    merchantLi = merchantLi.Where(p => p.Company.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();

                return merchantLi;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get merchant list
        /// Function Name       :   MerchantList
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   10/27/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AllMerchants> MerchantList()
        {
            try
            {
                IEnumerable<AllMerchants> merchantLi = (from users in DbMtData.Users
                                                        join merchants in DbMtData.Merchants on users.fk_MerchantID equals merchants.MerchantID
                                                        where merchants.IsActive && users.UserType.UserTypeID == 2
                                                        select new { Merchants = merchants, Users = users }).AsEnumerable().Select(objNew =>
                                                                     new AllMerchants
                                                                     {
                                                                         Company = objNew.Merchants.Company,
                                                                         MerchantID = objNew.Merchants.MerchantID
                                                                     }).ToList().OrderBy(x => x.Company);

                return merchantLi;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get fleet on the basis of merchant
        /// Function Name       :   GetFleet
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   10/26/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantListId"></param>
        /// <returns></returns>
        public IEnumerable<Fleet> GetFleet(string merchantListId, int userType, int userId)
        {
            try
            {
                var merchantList = merchantListId.Split(',');
                List<MerchantListID> merchants = new List<MerchantListID>();

                if (!string.IsNullOrEmpty(merchantListId))
                {
                    for (int i = 0; i < merchantList.Length; i++)
                    {
                        if ("multiselect-all" != merchantList[i])
                        {
                            MerchantListID li = new MerchantListID { fk_MerchantID = Convert.ToInt32(merchantList[i]) };
                            merchants.Add(li);
                        }
                    }
                }

                var fleetList = DbMtData.Fleets.ToList().Where(x => (merchants.Select(y => y.fk_MerchantID).Contains(x.fk_MerchantID)) && x.IsActive);

                if (userType == 1)
                    return fleetList;

                var tempDb = DbMtData.UserFleets.Any(x => x.fk_UserID == userId && (bool)x.IsActive);
                if (userType == 5 && tempDb)
                {
                    IEnumerable<Fleet> getFleet = (from fleet in fleetList
                                                   join userfleet in DbMtData.UserFleets on fleet.FleetID equals userfleet.fk_FleetID
                                                   where userfleet.fk_UserID == userId && (bool)userfleet.IsActive

                                                   select new { fleet.FleetID, fleet.FleetName }).AsEnumerable().Select(objNew => new Fleet
                                                                    {
                                                                        FleetID = (int)objNew.FleetID,
                                                                        FleetName = objNew.FleetName
                                                                    }).ToList();

                    return getFleet;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Add user
        /// Function Name       :   Add
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   10/26/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool Add(User user)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    var userDb = new User();
                    userDb.FName = user.FName;
                    userDb.LName = user.LName;
                    userDb.Email = user.Email;
                    userDb.Phone = user.Phone;
                    userDb.fk_Country = user.fk_Country;
                    userDb.fk_State = user.fk_State;
                    userDb.fk_City = user.fk_City;
                    userDb.Address = user.Address;
                    userDb.PCode = user.PCode;
                    userDb.fk_MerchantID = user.fk_MerchantID;
                    userDb.fk_UserTypeID = 5;
                    userDb.IsLockedOut = user.IsLockedOut;
                    userDb.IsActive = Convert.ToBoolean(1);
                    userDb.CreatedBy = user.CreatedBy;
                    userDb.CreatedDate = DateTime.Now;
                    userDb.ModifiedBy = user.ModifiedBy;
                    userDb.ModifiedDate = DateTime.Now;
                    userDb.IsFirstLogin = true;
                    userDb.RolesAssigned = user.RolesAssigned;
                    userDb.PCodeExpDate = DateTime.Now.AddDays(90);

                    if (user.fk_UserTypeID == 1)
                        userDb.ParentUserID = null;
                    else
                        userDb.ParentUserID = user.ParentUserID;
                    
                    DbMtData.Users.Add(userDb);
                    DbMtData.CommitChanges();

                    var merchantList = user.MerchantListId.Split(',');
                    var userMerchantDb = new UserMerchant[merchantList.Length];
                    for (int i = 0; i < merchantList.Length; i++)
                    {
                        if ("multiselect-all" != merchantList[i])
                        {
                            userMerchantDb[i] = new UserMerchant
                            {
                                fk_UserID = userDb.UserID,
                                fk_MerchantID = Convert.ToInt32(merchantList[i]),
                                IsActive = true,
                                CreatedBy = userDb.CreatedBy,
                                CreatedDate = DateTime.Now
                            };

                            DbMtData.UserMerchants.Add(userMerchantDb[i]);
                        }
                    }

                    DbMtData.CommitChanges();

                    var fleetList = user.fleetListId.Split(',');
                    var userFleetDb = new UserFleet[fleetList.Length];
                    for (int i = 0; i < fleetList.Length; i++)
                    {
                        if ("multiselect-all" != fleetList[i] && fleetList[i] != "-1")
                        {
                            var merchantId = DbMtData.Fleets.Find(Convert.ToInt32(fleetList[i])).fk_MerchantID;
                            userFleetDb[i] = new UserFleet
                            {
                                fk_UserID = userDb.UserID,
                                fk_FleetID = Convert.ToInt32(fleetList[i]),
                                fk_MerchantId = merchantId,
                                fk_UserTypeId = 5,
                                IsActive = true
                            };

                            DbMtData.UserFleets.Add(userFleetDb[i]);
                        }
                    }

                    DbMtData.CommitChanges();
                    transactionScope.Complete();

                    return true;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AllMerchants> GetUserMerchant(int userId)
        {
            try
            {
                try
                {
                    IEnumerable<AllMerchants> getUserMerchant =
                        (from merchant in DbMtData.Merchants
                         join userMerchant in DbMtData.UserMerchants on merchant.MerchantID equals userMerchant.fk_MerchantID
                         where userMerchant.fk_UserID == userId

                         select new { userMerchant.fk_MerchantID, merchant.Company }).AsEnumerable()
                            .Select(objNew =>
                                new AllMerchants
                                {
                                    MerchantID = objNew.fk_MerchantID,
                                    Company = objNew.Company
                                }).ToList().OrderBy(x => x.Company);

                    return getUserMerchant;
                }
                catch (Exception)
                {
                    throw;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update user
        /// Function Name       :   Update
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   10/26/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="user"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string Update(User user, int userId)
        {
            string[] list = null;
            string[] olderFleetlist = null;
            string[] currentMerchantList = user.MerchantListId.Split(',');
            string[] currentMerchantFleetList = user.fleetListId.Split(',');
            List<int> usersId = (from u in DbMtData.Users.Where(x => x.ParentUserID == userId) select u.UserID).ToList();

            if (usersId.Count > 0)
            {
                string result = string.Empty;
                string olderFleets = string.Empty;
                foreach (int users in usersId)
                {
                    int?[] userFleets = (from u in DbMtData.UserFleets.Where(x => x.fk_UserID == users) select u.fk_FleetID).ToArray();
                    if (string.IsNullOrEmpty(olderFleets))
                        olderFleets = string.Join(",", userFleets);
                    else
                        olderFleets = olderFleets + "," + string.Join(",", userFleets);

                    int[] item = (from u in DbMtData.UserMerchants.Where(x => x.fk_UserID == users) select u.fk_MerchantID).ToArray();
                    if (string.IsNullOrEmpty(result))
                        result = string.Join(",", item);
                    else
                        result = result + "," + string.Join(",", item);
                }

                list = result.Split(',');
                olderFleetlist = olderFleets.Split(',');
            }

            if (list != null)
            {
                foreach (string oldUsersMerchants in list)
                {
                    if (!currentMerchantList.Contains(oldUsersMerchants))
                        return "Merchant's list can't be unassigned as this is getting used by corporate sub user's";
                }
            }
            
            if (olderFleetlist != null)
            {
                foreach (string oldUsersFleets in olderFleetlist)
                {
                    if (!currentMerchantFleetList.Contains(oldUsersFleets))
                        return "Fleet's list can't be unassigned as this is getting used by corporate sub user's";
                }
            }
            
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    var data = DbMtData.Users.Find(userId);
                    if (data != null)
                    {
                        data.FName = user.FName;
                        data.LName = user.LName;
                        data.Email = user.Email;
                        data.Phone = user.Phone;
                        data.Address = user.Address;
                        data.fk_City = user.fk_City;
                        data.fk_State = user.fk_State;
                        data.fk_Country = user.fk_Country;
                        data.RolesAssigned = user.RolesAssigned;
                        data.IsLockedOut = user.IsLockedOut;
                        DbMtData.CommitChanges();

                        var deleteRecords = from tempRecords in DbMtData.UserRoles
                                            where tempRecords.fk_UserID == data.UserID
                                            select tempRecords;
                        
                        foreach (var item in deleteRecords)
                        {
                            DbMtData.UserRoles.Remove(item);
                        }

                        var deleteuserMerchants = from tempRecords in DbMtData.UserMerchants
                                                  where tempRecords.fk_UserID == data.UserID
                                                  select tempRecords;
                        
                        foreach (var item in deleteuserMerchants)
                        {
                            DbMtData.UserMerchants.Remove(item);
                        }
                        
                        var merchantList = user.MerchantListId.Split(',');
                        var userMerchantDb = new UserMerchant[merchantList.Length];
                        
                        for (int i = 0; i < merchantList.Length; i++)
                        {
                            if ("multiselect-all" != merchantList[i])
                            {
                                userMerchantDb[i] = new UserMerchant
                                {
                                    fk_UserID = data.UserID,
                                    fk_MerchantID = Convert.ToInt32(merchantList[i]),
                                    IsActive = true,
                                    CreatedBy = data.CreatedBy,
                                    CreatedDate = DateTime.Now
                                };

                                DbMtData.UserMerchants.Add(userMerchantDb[i]);
                            }
                        }

                        DbMtData.CommitChanges();
                        var deleteFleets = from tempFleet in DbMtData.UserFleets
                                           where tempFleet.fk_UserID == data.UserID
                                           select tempFleet;
                        foreach (var item in deleteFleets)
                        {
                            DbMtData.UserFleets.Remove(item);
                        }
                        var fleetList = user.fleetListId.Split(',');
                        var userFleetDb = new UserFleet[fleetList.Length];
                        for (int i = 0; i < fleetList.Length; i++)
                        {
                            if ("multiselect-all" != fleetList[i] && fleetList[i] != "-1")
                            {
                                var merchantId = DbMtData.Fleets.Find(Convert.ToInt32(fleetList[i])).fk_MerchantID;
                                userFleetDb[i] = new UserFleet();
                                userFleetDb[i].fk_UserID = data.UserID;
                                userFleetDb[i].fk_FleetID = Convert.ToInt32(fleetList[i]);
                                userFleetDb[i].fk_MerchantId = merchantId;
                                userFleetDb[i].fk_UserTypeId = 5;
                                userFleetDb[i].IsActive = true;
                            }
                            DbMtData.UserFleets.Add(userFleetDb[i]);
                        }
                        DbMtData.CommitChanges();
                        transactionScope.Complete();
                        return "OK";
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return "OK";
        }

        /// <summary>
        /// Purpose             :   To GetUser 
        /// Function Name       :   GetUser
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   10/26/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public User GetUser(int userId)
        {
            try
            {
                User userLi = (from users in DbMtData.Users
                               where users.UserID == userId
                               select users).SingleOrDefault();

                if (userLi != null)
                {
                    var countryName = (from m in DbMtData.CountryCodes where m.CountryCodesID == userLi.fk_Country select m.Title).FirstOrDefault();
                    if (userLi != null)
                    {
                        userLi.CountryName = countryName;

                        var role = (from roles in DbMtData.UserRoles where roles.fk_UserID == userId select new { roles.fk_RoleID }).ToList();
                        string temp = string.Empty;

                        temp = string.Join(",", role.Select(x => x.fk_RoleID));
                        userLi.RolesAssigned = temp;
                        var users = (from userMerchant in DbMtData.UserMerchants where userMerchant.fk_UserID == userId select new { userMerchant.fk_MerchantID }).ToList();
                        string tempId = string.Empty;

                        tempId = string.Join(",", users.Select(x => x.fk_MerchantID));
                        userLi.MerchantListId = tempId;

                        var fleet = (from userFleet in DbMtData.UserFleets where userFleet.fk_UserID == userId select new { userFleet.fk_FleetID }).ToList();
                        string fleetId = string.Empty;

                        fleetId = string.Join(",", fleet.Select(x => x.fk_FleetID));
                        userLi.fleetListId = fleetId;
                        return userLi;
                    }
                }
            }
            catch
            {
                throw;
            }
            return null;
        }

        /// <summary>
        /// Purpose             :   to get the list of users's fleet
        /// Function Name       :   GetUsersFleet
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/22/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<UserFleet> GetUsersFleet(int userId)
        {
            try
            {
                IEnumerable<UserFleet> getFleet =
                    (from fleet in DbMtData.UserFleets
                     where fleet.fk_UserID == userId

                     select new { fleet.fk_FleetID, fleet.fk_UserID, fleet.Fleet.FleetName }).AsEnumerable()
                        .Select(objNew =>
                            new UserFleet
                            {
                                fk_FleetID = objNew.fk_FleetID,
                                fk_UserID = objNew.fk_UserID,
                                FleetName = objNew.FleetName
                            }).ToList().OrderBy(x => x.FleetName);

                return getFleet;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To get the total no. of transaction 
        /// Function Name       :   GetTransactionCount
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/2/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public int GetTransactionCount(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_SearchTransactionReport_Result> query = DbMtData.usp_SearchTransactionReport(tvd.Id, tvd.VehicleNo, tvd.DriverNo, tvd.startDate, tvd.endDate, tvd.CardType, tvd.EntryMode, tvd.AuthId, tvd.Industry, tvd.TxnType, tvd.MerchantId, tvd.ExpiryDate, tvd.MinAmount, tvd.MaxAmount, tvd.FirstFourDigits, tvd.LastFourDigits, tvd.AddRespData, tvd.JobNumber, Convert.ToString(tvd.fk_FleetId), tvd.IsVarified, Convert.ToString(tvd.TerminalId), tvd.SerialNo);
                IEnumerable<UserFleet> userFleets = GetUsersFleet(tvd.UserId);
                query = query.Where(x => x.fk_FleetId != null);
                int count = query.Where(x => userFleets.Select(y => y.fk_FleetID).Contains((int)x.fk_FleetId)).ToList().Count();
                return count;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To get the transaction 
        /// Function Name       :   GetTransaction
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/5/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public IEnumerable<usp_SearchTransactionReport_Result> GetTransaction(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_SearchTransactionReport_Result> query = DbMtData.usp_SearchTransactionReport(tvd.Id, tvd.VehicleNo, tvd.DriverNo, tvd.startDate, tvd.endDate, tvd.CardType, tvd.EntryMode, tvd.AuthId, tvd.Industry, tvd.TxnType, tvd.MerchantId, tvd.ExpiryDate, tvd.MinAmount, tvd.MaxAmount, tvd.FirstFourDigits, tvd.LastFourDigits, tvd.AddRespData, tvd.JobNumber, Convert.ToString(tvd.fk_FleetId), tvd.IsVarified, Convert.ToString(tvd.TerminalId), tvd.SerialNo);
                if (tvd.UserId != 0)
                {
                    IEnumerable<UserFleet> userFleets = GetUsersFleet(tvd.UserId);
                    query = query.Where(x => x.fk_FleetId != null);
                    query = query.Where(x => userFleets.Select(y => y.fk_FleetID).Contains((int)x.fk_FleetId)).ToList();
                }
                
                if (!string.IsNullOrEmpty(tvd.Sorting))
                {
                    query = SortedTransactionReport(tvd.Sorting, query);
                    return query.Skip(tvd.PageNumber).Take(tvd.PageSize);
                }

                return query.OrderByDescending(x => x.TxnDate).Skip(tvd.PageNumber).Take(tvd.PageSize);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<usp_SearchTransactionReport_Result> SortedTransactionReport(string sorting, IEnumerable<usp_SearchTransactionReport_Result> query)
        {
            switch (sorting)
            {
                case "VehicleNo ASC":
                    query = query.OrderBy(p => p.VehicleNo);
                    break;
                case "VehicleNo DESC":
                    query = query.OrderByDescending(p => p.VehicleNo);
                    break;
                case "DriverNo ASC":
                    query = query.OrderBy(p => p.DriverNo);
                    break;
                case "DriverNo DESC":
                    query = query.OrderByDescending(p => p.DriverNo);
                    break;
                case "CardType ASC":
                    query = query.OrderBy(p => p.CardType);
                    break;
                case "CardType DESC":
                    query = query.OrderByDescending(p => p.CardType);
                    break;
                case "Amount ASC":
                    query = query.OrderBy(p => p.Amount);
                    break;
                case "Amount DESC":
                    query = query.OrderByDescending(p => p.Amount);
                    break;
                case "TxnType ASC":
                    query = query.OrderBy(p => p.TxnType);
                    break;
                case "TxnType DESC":
                    query = query.OrderByDescending(p => p.TxnType);
                    break;
                case "StringDateTime ASC":
                    query = query.OrderBy(p => p.TxnDate);
                    break;
                case "StringDateTime DESC":
                    query = query.OrderByDescending(p => p.TxnDate);
                    break;
                case "PaymentType ASC":
                    query = query.OrderBy(p => p.PaymentType);
                    break;
                case "PaymentType DESC":
                    query = query.OrderByDescending(p => p.PaymentType);
                    break;
                case "AddRespData ASC":
                    query = query.OrderBy(p => p.AddRespData);
                    break;
                case "AddRespData DESC":
                    query = query.OrderByDescending(p => p.AddRespData);
                    break;
            }
            return query;
        }

        /// <summary>
        ///  Purpose            :   To get the total no. of drivers
        /// Function Name       :   GetDriverCount
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/7/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public int GetDriverCount(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_GetDriversTransaction_Result> getDriversTransaction = DbMtData.usp_GetDriversTransaction(tvd.MerchantId, tvd.FleetId, tvd.VehicleNo, tvd.DriverNo, tvd.startDate, tvd.endDate);
                IEnumerable<UserFleet> userFleets = GetUsersFleet(tvd.UserId);
                
                //zzz getDriversTransaction = getDriversTransaction.Where(x => x.FleetID != null);
                getDriversTransaction = getDriversTransaction.Where(x => x.FleetID > 0);
                
                int count = getDriversTransaction.Where(x => userFleets.Select(y => y.fk_FleetID).Contains((int)x.FleetID)).ToList().Count();
                
                return count;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To get driver
        /// Function Name       :   GetDriver
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/6/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetDriversTransaction_Result> GetDriver(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_GetDriversTransaction_Result> query = DbMtData.usp_GetDriversTransaction(tvd.MerchantId, tvd.FleetId, tvd.VehicleNo, tvd.DriverNo, tvd.startDate, tvd.endDate);
                if (tvd.UserId != 0)
                {
                    IEnumerable<UserFleet> userFleets = GetUsersFleet(tvd.UserId);
                    
                    //zzz query = query.Where(x => x.FleetID != null);
                    query = query.Where(x => x.FleetID > 0);

                    query = query.Where(x => userFleets.Select(y => y.fk_FleetID).Contains((int)x.FleetID)).ToList();
                }
                if (!string.IsNullOrEmpty(tvd.Sorting))
                {
                    query = SortedGetDriver(tvd.Sorting, query);
                }
                return query.Skip(tvd.PageNumber).Take(tvd.PageSize);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<usp_GetDriversTransaction_Result> SortedGetDriver(string sorting, IEnumerable<usp_GetDriversTransaction_Result> query)
        {
            switch (sorting)
            {
                case "DriverName ASC":
                    query = query.OrderBy(p => p.DriverName);
                    break;
                case "DriverName DESC":
                    query = query.OrderByDescending(p => p.DriverName);
                    break;
                case "DriverNo ASC":
                    query = query.OrderBy(p => p.DriverNo);
                    break;
                case "DriverNo DESC":
                    query = query.OrderByDescending(p => p.DriverNo);
                    break;
                case "VehicleNo ASC":
                    query = query.OrderBy(p => p.VehicleNo);
                    break;
                case "VehicleNo DESC":
                    query = query.OrderByDescending(p => p.VehicleNo);
                    break;
                case "Fleet ASC":
                    query = query.OrderBy(p => p.Fleet);
                    break;
                case "Fleet DESC":
                    query = query.OrderByDescending(p => p.Fleet);
                    break;
                case "NumberOfKeyed ASC":
                    query = query.OrderBy(p => p.NumberOfKeyed);
                    break;
                case "NumberOfKeyed DESC":
                    query = query.OrderByDescending(p => p.NumberOfKeyed);
                    break;
                case "KeyedSaleAmount ASC":
                    query = query.OrderBy(p => p.KeyedSaleAmount);
                    break;
                case "KeyedSaleAmount DESC":
                    query = query.OrderByDescending(p => p.KeyedSaleAmount);
                    break;
                case "NumberOfSwiped ASC":
                    query = query.OrderBy(p => p.NumberOfSwiped);
                    break;
                case "NumberOfSwiped DESC":
                    query = query.OrderByDescending(p => p.NumberOfSwiped);
                    break;
                case "SwipedSaleAmount ASC":
                    query = query.OrderBy(p => p.SwipedSaleAmount);
                    break;
                case "SwipedSaleAmount DESC":
                    query = query.OrderByDescending(p => p.SwipedSaleAmount);
                    break;
                case "TotalNumberOfSale ASC":
                    query = query.OrderBy(p => p.TotalNumberOfSale);
                    break;
                case "TotalNumberOfSale DESC":
                    query = query.OrderByDescending(p => p.TotalNumberOfSale);
                    break;
                case "TotalSaleAmount ASC":
                    query = query.OrderBy(p => p.TotalSaleAmount);
                    break;
                case "TotalSaleAmount DESC":
                    query = query.OrderByDescending(p => p.TotalSaleAmount);
                    break;
            }

            return query;
        }

        /// <summary>
        ///  Purpose            :   To get technology detail report
        /// Function Name       :   GetTechnologyDetail
        /// Created By          :   Naveen Kumar
        /// Created On          :   10/6/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public IEnumerable<usp_TechnologyDetailReport_Result> GetTechnologyDetail(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_TechnologyDetailReport_Result> query = DbMtData.usp_TechnologyDetailReport(tvd.MerchantId, tvd.FleetId, tvd.VehicleNo, tvd.DriverNo, tvd.startDate, tvd.endDate, tvd.CardType);
                if (tvd.UserId != 0)
                {
                    IEnumerable<UserFleet> userFleets = GetUsersFleet(tvd.UserId);
                    query = query.Where(x => x.fk_FleetId != null);
                    query = query.Where(x => userFleets.Select(y => y.fk_FleetID).Contains((int)x.fk_FleetId)).ToList();
                }
                if (!string.IsNullOrEmpty(tvd.Sorting))
                {
                    query = SortedGetTechDetail(tvd.Sorting, query);
                }
                return query.Skip(tvd.PageNumber).Take(tvd.PageSize);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To get the total no. of technology fee records
        /// Function Name       :   GetTechFeeCount
        /// Created By          :   naveen Kumar
        /// Created On          :   10/6/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public int GetTechFeeDetailCount(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_TechnologyDetailReport_Result> query = DbMtData.usp_TechnologyDetailReport(tvd.MerchantId, tvd.FleetId, tvd.VehicleNo, tvd.DriverNo, tvd.startDate, tvd.endDate, tvd.CardType);
                IEnumerable<UserFleet> userFleets = GetUsersFleet(tvd.UserId);
                query = query.Where(x => x.fk_FleetId != null);
                int count = query.Where(x => userFleets.Select(y => y.fk_FleetID).Contains((int)x.fk_FleetId)).ToList().Count();

                return count;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To get the total no. of technology fee records
        /// Function Name       :   GetTechFeeCount
        /// Created By          :   naveen Kumar
        /// Created On          :   10/6/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public int GetTechFeeCount(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_TechnologySummaryReport_Result> query = DbMtData.usp_TechnologySummaryReport(tvd.MerchantId, tvd.FleetId, tvd.startDate, tvd.endDate);
                IEnumerable<UserFleet> userFleets = GetUsersFleet(tvd.UserId);
                query = query.Where(x => x.fk_FleetId != null);
                int count = query.Where(x => userFleets.Select(y => y.fk_FleetID).Contains((int)x.fk_FleetId)).ToList().Count();
                return count;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :   To get technology summary report
        /// Function Name       :   GetTechnologySummary
        /// Created By          :   Naveen Kumar
        /// Created On          :   10/6/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        public IEnumerable<usp_TechnologySummaryReport_Result> GetTechnologySummary(MTDTransaction tvd)
        {
            try
            {
                IEnumerable<usp_TechnologySummaryReport_Result> query = DbMtData.usp_TechnologySummaryReport(tvd.MerchantId, tvd.FleetId, tvd.startDate, tvd.endDate);
                if (tvd.UserId != 0)
                {
                    IEnumerable<UserFleet> userFleets = GetUsersFleet(tvd.UserId);
                    query = query.Where(x => x.fk_FleetId != null);
                    query = query.Where(x => userFleets.Select(y => y.fk_FleetID).Contains((int)x.fk_FleetId)).ToList();
                }
                if (!string.IsNullOrEmpty(tvd.Sorting))
                {
                    query = SortedGetTechSummary(tvd.Sorting, query);
                }
                return query.Skip(tvd.PageNumber).Take(tvd.PageSize).ToList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<usp_TechnologyDetailReport_Result> SortedGetTechDetail(string sorting, IEnumerable<usp_TechnologyDetailReport_Result> query)
        {
            switch (sorting)
            {
                case "TransId ASC":
                    query = query.OrderBy(p => p.TransId);
                    break;
                case "TransId DESC":
                    query = query.OrderByDescending(p => p.TransId);
                    break;
                case "FleetName ASC":
                    query = query.OrderBy(p => p.FleetName);
                    break;
                case "FleetName DESC":
                    query = query.OrderByDescending(p => p.FleetName);
                    break;
                case "TxnDate ASC":
                    query = query.OrderBy(p => p.TxnDate);
                    break;
                case "TxnDate DESC":
                    query = query.OrderByDescending(p => p.TxnDate);
                    break;
                case "VehicleNo ASC":
                    query = query.OrderBy(p => p.VehicleNo);
                    break;
                case "VehicleNo DESC":
                    query = query.OrderByDescending(p => p.VehicleNo);
                    break;
                case "DriverNo ASC":
                    query = query.OrderBy(p => p.DriverNo);
                    break;
                case "DriverNo DESC":
                    query = query.OrderByDescending(p => p.DriverNo);
                    break;
                case "TxnType ASC":
                    query = query.OrderBy(p => p.TxnType);
                    break;
                case "TxnType DESC":
                    query = query.OrderByDescending(p => p.TxnType);
                    break;
                case "Amount ASC":
                    query = query.OrderBy(p => p.Amount);
                    break;
                case "Amount DESC":
                    query = query.OrderByDescending(p => p.Amount);
                    break;
                case "CardType ASC":
                    query = query.OrderBy(p => p.CardType);
                    break;
                case "CardType DESC":
                    query = query.OrderByDescending(p => p.CardType);
                    break;
                case "TechFee ASC":
                    query = query.OrderBy(p => p.TechFee);
                    break;
                case "TechFee DESC":
                    query = query.OrderByDescending(p => p.TechFee);
                    break;
                case "Surcharge ASC":
                    query = query.OrderBy(p => p.Surcharge);
                    break;
                case "Surcharge DESC":
                    query = query.OrderByDescending(p => p.Surcharge);
                    break;
                case "AuthId ASC":
                    query = query.OrderBy(p => p.AuthId);
                    break;
                case "AuthId DESC":
                    query = query.OrderByDescending(p => p.AuthId);
                    break;
            }

            return query;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<usp_TechnologySummaryReport_Result> SortedGetTechSummary(string sorting, IEnumerable<usp_TechnologySummaryReport_Result> query)
        {
            switch (sorting)
            {
                case "FleetName ASC":
                    query = query.OrderBy(p => p.FleetName);
                    break;
                case "FleetName DESC":
                    query = query.OrderByDescending(p => p.FleetName);
                    break;
                case "CountVehicle ASC":
                    query = query.OrderBy(p => p.CountVehicle);
                    break;
                case "CountVehicle DESC":
                    query = query.OrderByDescending(p => p.CountVehicle);
                    break;
                case "AutomaticTrans ASC":
                    query = query.OrderBy(p => p.AutomaticTrans);
                    break;
                case "AutomaticTrans DESC":
                    query = query.OrderByDescending(p => p.AutomaticTrans);
                    break;
                case "ManualTrans ASC":
                    query = query.OrderBy(p => p.ManualTrans);
                    break;
                case "ManualTrans DESC":
                    query = query.OrderByDescending(p => p.ManualTrans);
                    break;
                case "CountRefund ASC":
                    query = query.OrderBy(p => p.CountRefund);
                    break;
                case "CountRefund DESC":
                    query = query.OrderByDescending(p => p.CountRefund);
                    break;
                case "CountVoid ASC":
                    query = query.OrderBy(p => p.CountVoid);
                    break;
                case "CountVoid DESC":
                    query = query.OrderByDescending(p => p.CountVoid);
                    break;
                case "TotalTrans ASC":
                    query = query.OrderBy(p => p.TotalTrans);
                    break;
                case "TotalTrans DESC":
                    query = query.OrderByDescending(p => p.TotalTrans);
                    break;
                case "TechFee ASC":
                    query = query.OrderBy(p => p.TechFee);
                    break;
                case "TechFee DESC":
                    query = query.OrderByDescending(p => p.TechFee);
                    break;
                case "Surcharge ASC":
                    query = query.OrderBy(p => p.Surcharge);
                    break;
                case "Surcharge DESC":
                    query = query.OrderByDescending(p => p.Surcharge);
                    break;
                case "Booking_Fee ASC":
                    query = query.OrderBy(p => p.Booking_Fee);
                    break;
                case "Booking_Fee DESC":
                    query = query.OrderByDescending(p => p.Booking_Fee);
                    break;
                case "TotalAmt ASC":
                    query = query.OrderBy(p => p.TotalAmt);
                    break;
                case "TotalAmt DESC":
                    query = query.OrderByDescending(p => p.TotalAmt);
                    break;
            }

            return query;
        }

        /// <summary>
        /// 
        /// </summary>   
        /// <param name="fleetIdListId"></param>
        /// <returns></returns>
        public IEnumerable<DriverReportRecipient> GetUserDriverReceipent(string fleelistId)
        {
            try
            {
                var FleetList = fleelistId.Split(',');
                List<FleetListId> Fleets = new List<FleetListId>();
                if (!string.IsNullOrEmpty(fleelistId))
                {
                    for (int i = 0; i < FleetList.Length; i++)
                    {
                        if ("multiselect-all" != FleetList[i])
                        {
                            FleetListId Flt = new FleetListId { fk_FleetID = Convert.ToInt32(FleetList[i]) };
                            Fleets.Add(Flt);
                        }
                    }
                }

                var driverRecipient = (from drivers in DbMtData.Drivers
                                       join fleet in DbMtData.Fleets on drivers.fk_FleetID equals fleet.FleetID
                                       join merchant in DbMtData.Merchants on fleet.fk_MerchantID equals merchant.MerchantID
                                       join driverRec in DbMtData.DriverReportRecipients on
                                      drivers.DriverID equals driverRec.fk_DriverId
                                        into t
                                       from driverRec in t.DefaultIfEmpty()
                                       where (bool)drivers.IsActive
                                       //&&  drivers.fk_FleetID == fleetId 
                                       select new
                                       {
                                           drivers.DriverID,
                                           drivers.DriverNo,
                                           drivers.FName,
                                           drivers.LName,
                                           driverRec.IsRecipient,
                                           driverRec.ModifiedBy,
                                           driverRec.IsDaily,
                                           driverRec.IsWeekly,
                                           driverRec.IsMonthly,
                                           drivers.Email,
                                           drivers.fk_FleetID,
                                           merchant.Company,
                                           driverRec.IsOwnerRecipient
                                       }).Distinct().AsEnumerable().Select(objNew =>
                     new DriverReportRecipient
                     {
                         DriverID = objNew.DriverID,
                         DriverNo = objNew.DriverNo,
                         FName = objNew.FName,
                         LName = objNew.LName,
                         IsRecipient = objNew.IsRecipient,
                         ModifiedBy = objNew.ModifiedBy,
                         Email = objNew.Email,
                         Company = objNew.Company,
                         IsDaily = objNew.IsDaily,
                         IsWeekly = objNew.IsWeekly,
                         IsMonthly = objNew.IsMonthly,
                         fk_FleetId = objNew.fk_FleetID,
                         IsOwnerRecipient = objNew.IsOwnerRecipient
                     });

                driverRecipient = driverRecipient.ToList().Where(x => (Fleets.Select(y => y.fk_FleetID).Contains((int)x.fk_FleetId)));

                return driverRecipient.Distinct();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   to get the corporate users merchant
        /// Function Name       :   GetCorpUserMerchant
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   11/04/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<AllMerchants> GetCorpUserMerchant(int userId)
        {
            try
            {
                var parentId = DbMtData.Users.Find(userId).ParentUserID;
                IEnumerable<AllMerchants> getUserMerchant =
                       (from merchant in DbMtData.Merchants
                        join userMerchant in DbMtData.UserMerchants on merchant.MerchantID equals userMerchant.fk_MerchantID
                        where userMerchant.fk_UserID == parentId

                        select new { userMerchant.fk_MerchantID, merchant.Company }).AsEnumerable()
                           .Select(objNew =>
                               new AllMerchants
                               {
                                   MerchantID = objNew.fk_MerchantID,
                                   Company = objNew.Company

                               }).ToList().OrderBy(x => x.Company).ToList<AllMerchants>();

                var userMerchantList = GetUserMerchant(userId).ToList<AllMerchants>();
                var list = getUserMerchant.Cast<AllMerchants>().Union(userMerchantList.Cast<AllMerchants>(), new AllMerchantsComparer()).ToList<AllMerchants>();
                return list;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
