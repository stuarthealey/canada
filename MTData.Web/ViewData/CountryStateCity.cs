﻿using System.ComponentModel.DataAnnotations;

namespace MTData.Web.ViewData
{
    public class CountryStateCity
    {
        [Required(ErrorMessage = "Please Select a country")]
        public string Country { get; set; }
        [Required(ErrorMessage = "Please Select a state")]
        public string State { get; set; }
        [Required(ErrorMessage = "Please Select a city")]
        public string City { get; set; }
    }
}