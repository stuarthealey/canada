﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class TableFB
    {
        private const string tblId = "|46|42";
        private string TableVersion = "001";
        private string DownloadFile = "EMV2KEY|20|20|20";
        private string ReqFileBlock = "0001";
        private string ReqFileBlockSize = "256";
        private string ReqFileOffset = "0000000";
        private string FileBlockStatusCode = "";

        public string TableID
        {
            get { return tblId; }
        }

        public string Table_Version
        {
            get { return TableVersion; }
            set { TableVersion = value; }
        }

        public string Download_File
        {
            get { return DownloadFile; }
            set { DownloadFile = value; }
        }

        public string ReqFile_Block
        {
            get { return ReqFileBlock; }
            set { ReqFileBlock = value; }
        }

        public string Req_FileBlockSize
        {
            get { return ReqFileBlockSize; }
            set { ReqFileBlockSize = value; }
        }

        public string Req_FileOffset
        {
            get { return ReqFileOffset; }
            set { ReqFileOffset = value; }
        }

        public string FileBlock_StatusCode
        {
            get { return FileBlockStatusCode; }
            set { FileBlockStatusCode = value; }
        }
    }
}