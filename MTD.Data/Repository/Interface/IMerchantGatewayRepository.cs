﻿using System.Collections.Generic;
using MTD.Data.Data;

namespace MTD.Data.Repository.Interface
{
     public interface IMerchantGatewayRepository
    {
        bool Add(Merchant_Gateway merchantGateway);
        IEnumerable<Merchant_Gateway> MerchantGatewayList(int merchantId);
        bool Update(Merchant_Gateway merchantGateway, int merchantGatewayId);
        Merchant_Gateway GetMerchantGateway(int merchantGatewayId);
        void DeleteMerchantGateway(int merchantGatewayId);
        IEnumerable<Merchant_Gateway> MerchantGateListByFilter(FilterSearchParameters searchParameters);
    }
}
