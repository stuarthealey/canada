﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.Web.Resource;
using MTData.Web.ViewData;

namespace MTData.Web.Controllers
{
    public class CarOwnersController : Controller
    {
        readonly ILogger _logger = new Logger();
        private readonly ICarOwnersService _ownerService;
        private readonly ICommonService _commonService;
        private readonly IUserService _userService;
        private StringBuilder _logMessage;
        private StringBuilder _logTracking;

        public CarOwnersController([Named("UserService")] IUserService userService, [Named("CarOwnersService")] ICarOwnersService ownerService, [Named("CommonService")] ICommonService commonService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _commonService = commonService;
            _ownerService = ownerService;
            _logMessage = logMessage;
            _logTracking = logTracking;
            _userService = userService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            return View();
        }

        /// <summary>
        /// Purpose             :   To create the driver
        /// Function Name       :   Create
        /// Created By          :   Umesh Kumar
        /// Created On          :   08/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            try
            {
                if (((SessionDto)Session["User"]) != null)
                {
                    if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                        return RedirectToAction("index", "Member");

                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    _logMessage.Append("calling Country list to populate CountryList");

                    var viewData = new CarOwnerViewData();
                    viewData.CountryList = CountryList();
                    viewData.Country = "AU";
                    _logMessage.Append("returing to view create");
                    return View(viewData);
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return RedirectToAction("Index", "Member");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="carOwnerViewData"></param>
        /// <param name="uploadFile"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(CarOwnerViewData carOwnerViewData, HttpPostedFileBase uploadFile)
        {
            if (((SessionDto)Session["User"]) != null)
            {
                try
                {
                    if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                        return RedirectToAction("index", "Member");

                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                    bool isValid = true;
                    if (string.IsNullOrEmpty(carOwnerViewData.EmailAddress))
                    {
                        ModelState.AddModelError("EmailAddress", MtdataResource.Please_Enter_Email);
                        isValid = false;
                    }

                    if (!string.IsNullOrEmpty(carOwnerViewData.Contact))
                    {
                        if (carOwnerViewData.Contact.Length < 6)
                        {
                            isValid = false;
                            ModelState.AddModelError("Contact", MtdataResource.Contact_Min_Length);
                        }
                    }

                    if (string.IsNullOrEmpty(carOwnerViewData.Address))
                    {
                        isValid = false;
                        ModelState.AddModelError("Address", MtdataResource.Please_Enter_Address);
                    }

                    if (string.IsNullOrEmpty(carOwnerViewData.ContactPerson))
                    {
                        isValid = false;
                        ModelState.AddModelError("ContactPerson", MtdataResource.Contact_Person_required);
                    }

                    // Code for Excel Upload//
                    _logMessage.Append("begin upload from csv file");
                    if (uploadFile != null)
                    {
                        if (!IsValidExcel(uploadFile))
                        {
                            carOwnerViewData.IsCarOwnerError = true;
                            List<CarOwnerViewData> db = new List<CarOwnerViewData>();
                            carOwnerViewData.CarOwnerListViewData = db;
                            ViewBag.UploadMessage = MtdataResource.invalid_Excel_Fromat;
                            carOwnerViewData.CountryList = CountryList();
                            ModelState.Clear();
                            _logMessage.Append("Invalid Excel file uploaded");
                            return View(carOwnerViewData);
                        }

                        _logMessage.Append("Try to get car owner  list from Excel file.");
                        List<CarOwnerViewData> lstCarOwnerViewData = UploadExcelFile(uploadFile);
                        _logMessage.Append("Get Device list from Excel file successfully.");
                        _logMessage.Append("Calling Add method of service _terminalService takes terminalDto parameters list.");

                        if (lstCarOwnerViewData != null && lstCarOwnerViewData.Count() > 0)
                        {
                            List<CarOwnerViewData> objCarOwnerExcelUploadViewData = _ownerService.AddCarOwnewrList(lstCarOwnerViewData.ToDtoList()).ToCarOwnerList();

                            _logMessage.Append("Get list of unauthorize vehicle from uploaded excel file.");

                            carOwnerViewData.IsCarOwnerError = true;
                            carOwnerViewData.CarOwnerListViewData = objCarOwnerExcelUploadViewData;
                            if (lstCarOwnerViewData != null && objCarOwnerExcelUploadViewData != null)
                            {
                                int uplodedCarOwner = lstCarOwnerViewData.Count() - objCarOwnerExcelUploadViewData.Count();
                                if (uplodedCarOwner > 0)
                                {
                                    carOwnerViewData.CountryList = CountryList();
                                    ViewBag.UploadMessage = uplodedCarOwner + " Car owner has been created from selected file.";
                                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;

                                    _logTracking.Append("User " + userName + " add Car owner through excel file " + uploadFile.FileName + " at " + System.DateTime.Now.ToUniversalTime());
                                    _logger.LogInfoMessage(_logTracking.ToString());
                                }
                                else
                                {
                                    carOwnerViewData.CountryList = CountryList();
                                    ViewBag.UploadMessage = MtdataResource.No_record_created;
                                }
                            }
                        }
                        else
                        {
                            carOwnerViewData.IsCarOwnerError = true;
                            List<CarOwnerViewData> db = new List<CarOwnerViewData>();
                            carOwnerViewData.CarOwnerListViewData = db;
                            carOwnerViewData.CountryList = CountryList();
                            ViewBag.UploadMessage = MtdataResource.No_recordFound;
                        }

                        ModelState.Clear();
                    }
                    // End Code for Excel Upload
                    else
                    {
                        if (isValid && ModelState.IsValid)
                        {
                            var carownerDto = carOwnerViewData.ToCarOwner();
                            if (!string.IsNullOrEmpty(carownerDto.Account))
                                carownerDto.Account = StringExtension.EncryptDriver(carownerDto.Account);

                            if (!string.IsNullOrEmpty(carownerDto.SageAccount))
                                carownerDto.SageAccount = StringExtension.EncryptDriver(carownerDto.SageAccount);

                            carownerDto.fk_MerchantId = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;
                            carownerDto.CreatedBy = ((SessionDto)Session["User"]).SessionUser.Email;

                            if (_ownerService.Add(carownerDto))
                                return Redirect("Index");

                            carOwnerViewData.CountryList = CountryList();
                            ViewBag.Message = MtdataResource.Email_already_exist;
                        }
                    }

                    carOwnerViewData.CountryList = CountryList();

                    return View(carOwnerViewData);
                }
                catch (Exception ex)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), ex);
                }
            }

            return RedirectToAction("Index", "Member");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Update(int id)
        {
            if (((SessionDto)Session["User"]) != null)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                    return RedirectToAction("index", "Member");
                try
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    _logMessage.Append("calling countrylist method to  populate CountryList");

                    var viewData = new CarOwnerViewData();

                    _logMessage.Append("calling Read method of  owner Service with id " + id);
                    CarOwnersDto ownerDetails = _ownerService.Read(id);
                    viewData = ownerDetails.ToCarOwner();
                    
                    if (!string.IsNullOrEmpty(viewData.Account))
                        viewData.Account = StringExtension.DecryptDriver(viewData.Account);
                    
                    if (!string.IsNullOrEmpty(viewData.SageAccount))
                        viewData.SageAccount = StringExtension.DecryptDriver(viewData.SageAccount);
                    
                    _logMessage.Append("Read method of  owner Service executed successfully and take result in viewdata using ownerDetails.ToCarOwner()");
                    viewData.CountryList = CountryList();
                    _logMessage.Append("returing to view create");
                    
                    return View(viewData);
                }
                catch (Exception ex)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), ex);
                }
            }

            return RedirectToAction("Index", "Member");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="carOwner"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(CarOwnerViewData carOwner)
        {
            try
            {
                if (((SessionDto)Session["User"]) != null)
                {
                    if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                        return RedirectToAction("index", "Member");

                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                    bool isValid = true;
                    if (string.IsNullOrEmpty(carOwner.EmailAddress))
                    {
                        ModelState.AddModelError("EmailAddress", MtdataResource.Please_Enter_Email);
                        isValid = false;
                    }

                    if (!string.IsNullOrEmpty(carOwner.Contact))
                    {
                        if (carOwner.Contact.Length < 6)
                        {
                            isValid = false;
                            ModelState.AddModelError("Contact", MtdataResource.Contact_Min_Length);
                        }
                    }

                    if (string.IsNullOrEmpty(carOwner.Address))
                    {
                        isValid = false;
                        ModelState.AddModelError("Address", MtdataResource.Please_Enter_Address);
                    }

                    if (string.IsNullOrEmpty(carOwner.ContactPerson))
                    {
                        isValid = false;
                        ModelState.AddModelError("ContactPerson", MtdataResource.Contact_Person_required);
                    }

                    if (isValid && ModelState.IsValid)
                    {
                        carOwner.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                        carOwner.fk_MerchantId = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;
                        _logMessage.Append("calling ToCarOwner method return car owner dto");
                        
                        var CarOwnersDto = carOwner.ToCarOwner();
                        _logMessage.Append("ToCarOwner method executed successfully");
                        
                        if (!string.IsNullOrEmpty(CarOwnersDto.Account))
                            CarOwnersDto.Account = StringExtension.EncryptDriver(CarOwnersDto.Account);
                        
                        if (!string.IsNullOrEmpty(CarOwnersDto.SageAccount))
                            CarOwnersDto.SageAccount = StringExtension.EncryptDriver(CarOwnersDto.SageAccount);
                        
                        if (_ownerService.Update(CarOwnersDto))
                        {
                            _logMessage.Append("carowner updated successfully");
                            return Redirect("Index");
                        }

                        carOwner.CountryList = CountryList();
                        ViewBag.Message = MtdataResource.Email_already_exist;
                    }

                    carOwner.CountryList = CountryList();
                    return View(carOwner);
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return RedirectToAction("Index", "Member");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult OwnerListByFiter(string name = null, string ownerName="", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetDeriverListByFilter method of _driverService class to get driver List");

                int listCount;
                
                FilterSearchParametersDto filterSearch = new FilterSearchParametersDto()
                {
                    Name = name,
                    StartIndex = jtStartIndex,
                    PageSize = jtPageSize,
                    Sorting = jtSorting,
                    OwnerName = ownerName,
                    MerchantId = (int)((SessionDto)Session["User"]).SessionUser.fk_MerchantID
                };

                var viewDataList = _ownerService.GetOwnerListByFilter(filterSearch);//1
                listCount = _ownerService.GetOwnerCount(name, filterSearch.MerchantId, ownerName);
                
                if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(ownerName))
                {
                    _logMessage.Append("Calling UserList method of _driverService class to count fee");
                    _logMessage.Append("UserList method of _driverService executed successfully result = " + listCount);
                    return Json(new { Result = "OK", Records = viewDataList, TotalRecordCount = listCount });
                }

                var driverDtos = viewDataList as CarOwnersDto[] ?? viewDataList.ToArray();
                return Json(new { Result = "OK", Records = driverDtos, TotalRecordCount = listCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="OwnerId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(int Id)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                try
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    _logMessage.Append("Calling Remove method of _ownerService class takes Id= " + Id);

                    bool result = _ownerService.Remove(Id);
                    if (!result)
                    {
                        var msg = "can't be deleted as it is associated with car.";
                        return Json(new { Result = "No", Message = msg });
                    }

                    _logMessage.Append("Remove method of _ownerService class executed successfully");
                    _logTracking.Append("Owner ID" + Id + " was deleted by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return Json(new { Result = "OK" });
                }
                catch (Exception exception)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), exception);
                }
            }

            return Json(new { Result = "No" });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<CountryViewData> CountryList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetCountryList method of _commonService class to get countries");

                var countryDtoList = _commonService.GetCountryList();
                _logMessage.Append("GetCountryList method of _commonService executed successfully");
                _logMessage.Append("calling ToCountryViewDataList method to map CountryViewData to CountryCodeDto and retrun CountryViewData");
                
                var countryViewDataList = countryDtoList.ToCountryViewDataList();
                _logMessage.Append("ToCountryViewDataList method executed successfully");
                _logMessage.Append("converting countryViewDataList list into objcountrylist");
                
                List<CountryViewData> objcountrylist = countryViewDataList.ToList();
                
                _logMessage.Append("convert countryViewDataList into objcountrylist successful");
                
                var li = new List<SelectListItem> { new SelectListItem { Text = MtdataResource.Please_Select_a_Country, Value = "0" } };
                li.AddRange(objcountrylist.Select(item => new SelectListItem { Text = item.Title, Value = Convert.ToString(item.CountryCodesID) }));
                _logMessage.Append("returning country list");

                return objcountrylist;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uploadFile"></param>
        /// <returns></returns>
        private bool IsValidExcel(HttpPostedFileBase uploadFile)
        {
            string line = string.Empty;
            string filePath = string.Empty;

            if (uploadFile.ContentLength > 0)
            {
                if (!Directory.Exists(Server.MapPath("../Uploads")))
                    Directory.CreateDirectory(Server.MapPath("../Uploads"));

                filePath = Path.Combine(HttpContext.Server.MapPath("../Uploads"),
                Path.GetFileName(uploadFile.FileName));
                uploadFile.SaveAs(filePath);
            }
            
            //Set the filename in to our stream
            string CarOwnerExcel = "Owner Name,Contact Number,Contact Person,Email,Sage Account,Bank Name,Account Name,BSB,Account,Country,State,City,Address";

            using (StreamReader sr = new StreamReader(filePath))
            {
                //Read the first line 
                line = sr.ReadLine();
                sr.Dispose();
            }

            if (!string.IsNullOrEmpty(line))
            {
                if (CarOwnerExcel != line)
                    return false;
                else
                    return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uploadFile"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public List<CarOwnerViewData> UploadExcelFile(HttpPostedFileBase uploadFile)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            List<CarOwnerViewData> objCarownerDataView = new List<CarOwnerViewData>();
            try
            {
                if (uploadFile.ContentLength > 0)
                {
                    if (!Directory.Exists(Server.MapPath("../Uploads")))
                        Directory.CreateDirectory(Server.MapPath("../Uploads"));

                    string filePath = Path.Combine(HttpContext.Server.MapPath("../Uploads"), Path.GetFileName(uploadFile.FileName));
                    uploadFile.SaveAs(filePath);
                    string line = string.Empty;
                    string[] strArray = null;

                    // work out where we should split on comma, but not in a sentance
                    Regex r = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

                    //Set the filename in to our stream
                    StreamReader sr = new StreamReader(filePath);

                    //Read the first line and split the string at , with our regular express in to an array
                    line = sr.ReadLine();
                    
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (!string.IsNullOrEmpty(line))
                        {
                            strArray = r.Split(line);
                            //For each item in the new split array, dynamically builds our Data columns. Save us having to worry about it.
                        }

                        CarOwnerViewData Obj = new CarOwnerViewData();
                        Obj.OwnerName = strArray[0].Trim();
                        Obj.Contact = strArray[1].Trim();
                        Obj.ContactPerson = strArray[2].Trim();
                        Obj.EmailAddress = strArray[3].Trim();
                        Obj.SageAccount = strArray[4].Trim();
                        Obj.BankName = strArray[5].Trim();
                        Obj.AccountName = strArray[6].Trim();
                        Obj.BSB = strArray[7].Trim();
                        Obj.Account = strArray[8].Trim();
                        Obj.Country = strArray[9].Trim();
                        Obj.State = strArray[10].Trim();
                        Obj.City = strArray[11].Trim();
                        Obj.Address = strArray[12].Trim();                        

                        Obj.fk_MerchantId = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;
                        Obj.CreatedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                        Obj.ModifiedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                        objCarownerDataView.Add(Obj);
                    }

                    sr.Dispose();
                }

                return objCarownerDataView;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public FileResult Download(string file)
        {
            string CurrentFileName = (from fls in GetFiles()
                                      where fls.FileName == file
                                      select fls.FilePath).First();

            string contentType = string.Empty;

            if (CurrentFileName.Contains(".pdf"))
                contentType = "application/pdf";
            else if (CurrentFileName.Contains(".docx"))
                contentType = "application/docx";
            else if (CurrentFileName.Contains(".csv"))
                contentType = "application/csv";

            return File(CurrentFileName, contentType, file);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<DownloadFileViewData> GetFiles()
        {
            List<DownloadFileViewData> lstFiles = new List<DownloadFileViewData>();
            DirectoryInfo dirInfo = new DirectoryInfo(Server.MapPath("../Assets/TemplateFiles"));

            int i = 0;
            foreach (var item in dirInfo.GetFiles())
            {
                lstFiles.Add(new DownloadFileViewData()
                {

                    FileId = i + 1,
                    FileName = item.Name,
                    FilePath = dirInfo.FullName + @"\" + item.Name
                });
                i = i + 1;
            }

            return lstFiles;
        }

    }
}