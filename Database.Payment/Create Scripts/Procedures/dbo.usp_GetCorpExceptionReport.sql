IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetCorpExceptionReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetCorpExceptionReport]
GO
 
CREATE PROCEDURE [dbo].[usp_GetCorpExceptionReport]
(
  @DateFrom DATETIME,
  @DateTo DATETIME,
  @CorpUserId INT
)
AS
BEGIN TRY  
-- SET NOCOUNT ON added to prevent extra result sets from
     SET NOCOUNT ON;
     SET ANSI_WARNINGS OFF
	 DECLARE  @NextScheduleDateTime DATETIME,
	          @ScheduleId INT
     DECLARE  @FleetId INT,
              @ExpFleetTransaction INT,
              @ExpCarTransaction INT,
              @ExpTransactionValue DECIMAL(18,2),
              @ActFleetTransaction  INT,
              @ActCarTransaction INT,
              @ActTransactionValue DECIMAL(18,2),
              @Case VARCHAR(100)		
	 DECLARE @TableData TABLE
	 (
	   FleetName NVARCHAR(60),
       VehicleNo NVARCHAR(60),
	   FareValue  NVARCHAR(30),
	   Tip NVARCHAR(30),
	   Surcharge NVARCHAR(30),
	   TechFee NVARCHAR(30),
	   Amount NVARCHAR(30),
       Fee NVARCHAR(30),
	   AmountOwing NVARCHAR(30)
	 )	 

	 --Comment from here
	-- SELECT 'FleetName' AS 'FleetName', 'VehicleNo' AS 'VehicleNo', '$1.2' AS 'FareValue', '$1.2' AS 'Tip', '$1.2' AS 'Surcharge','$1.2' AS 'TechFee', '$1.2' AS 'Amount', '$1.2' AS 'Fee', '$1.2' AS 'AmountOwing'
	 --Comment till here

     --Declaring cursor for fetching fleets one by one corresponding to a particular merchant
     DECLARE Exp_Cursor CURSOR FOR 
     --SELECT FleetID FROM Fleet flt WHERE flt.fk_MerchantID=@MerchantId AND flt.IsActive=1
	 select fk_FleetId from UserFleet where fk_UserTypeId=5 and IsActive=1 and fk_UserID=@CorpUserId
     OPEN Exp_Cursor
     FETCH NEXT FROM Exp_Cursor INTO @FleetId
     WHILE @@FETCH_STATUS = 0   
     BEGIN   
     SET @Case=''
     SELECT COALESCE((SELECT Company FROM Merchant M WHERE M.MerchantID = T.MerchantId),'') AS Merchant
	       ,COALESCE((SELECT [FleetName] FROM Fleet F WHERE F.FleetID = T.FleetID),'') AS Fleet
           ,T.MerchantId
		   ,T.FleetID
		   ,T.VehicleNo
		   ,T.FareValue
		   ,T.Tip 
		   ,T.Surcharge 
		   ,T.Amount
		   ,T.AmountOwing
		   ,T.TechFee
		   ,At.ExpFleetTransaction
		   ,At.ExpCarTransaction
		   ,At.ExpTransactionValue
           ,AT.ActFleetTransaction
		   ,AT.ActCarTransaction
		   ,At.ActTransactionValue
		   ,T.Fee
		    INTO #Temp 
	 FROM
     (
	  SELECT COALESCE(tblSaleCompletion.MerchantId,0) AS 'MerchantId'
	       ,COALESCE((tblSaleCompletion.fk_FleetId),'') AS 'FleetID'
	       ,COALESCE(tblSaleCompletion.[VehicleNo],'') AS 'VehicleNo'
	       ,COALESCE(tblSaleCompletion.[FareValue],0) AS 'FareValue'
	       ,COALESCE(tblSaleCompletion.Tip,0) AS 'Tip'
	       ,COALESCE(tblSaleCompletion.Surcharge,0) AS 'Surcharge'
	       ,COALESCE(tblSaleCompletion.[Amount],0) AS 'Amount'
	       ,COALESCE(tblSaleCompletion.Fee,0)+COALESCE(tblSaleCompletion.FleetFee,0) AS 'Fee'
		   ,COALESCE(tblSaleCompletion.TechFee,0) AS 'TechFee'
	       ,COALESCE(tblSaleCompletion.Amount,0) - COALESCE(tblSaleCompletion.FleetFee,0)-COALESCE(tblSaleCompletion.Fee,0) AS 'AmountOwing'
	       ,COALESCE(tblSaleCompletion.TxnDate,'') AS 'TransDate'
	 FROM
	 (
	 SELECT 
		      MerchantId AS 'MerchantId'
			 ,fk_FleetId AS 'fk_FleetId'
			 ,VehicleNo AS 'VehicleNo'
			 ,FareValue AS 'FareValue'
			 ,Tip AS 'Tip'
			 ,Surcharge AS 'Surcharge'
			 ,Amount AS 'Amount'
			 ,Fee AS 'Fee'
			 ,TechFee AS 'TechFee'
			 ,TxnDate AS 'TxnDate'
			 ,FleetFee AS 'FleetFee'		                                                                                                                                  
	     FROM    dbo.MTDTransaction 								 													 								
		 WHERE   TxnType='Sale'
				 AND IsRefunded=0
			     AND IsVoided=0				    
		         AND ResponseCode IN ('000','002','00')
				 AND CAST(TxnDate AS DATE)>=@DateFrom 
		         AND CAST(TxnDate AS DATE)<@DateTo	
				 AND fk_FleetId in (select fk_FleetId from UserFleet where fk_UserTypeId=5 and IsActive=1 and fk_UserID=@CorpUserId)   
		  UNION ALL
		  SELECT  tblId.MerchantId,
		          tblId.fk_FleetId,
			      tblSrc.VehicleNo,
				  tblId.FareValue,
			      tblId.Tip,
				  tblId.Surcharge,
			      tblSrc.Amount,
				  tblId.Fee,
				  tblId.TechFee,
			      tblSrc.TxnDate,
				  tblId.FleetFee	                                                                
	      FROM dbo.MTDTransaction tblSrc 
	      INNER JOIN dbo.MTDTransaction tblId
		  ON tblSrc.SourceId=tblId.Id						 								
		  WHERE  tblSrc.TxnType='Completion'
		         AND tblSrc.ResponseCode IN ('000','002','00')
				 AND CAST(tblSrc.TxnDate AS DATE)>=@DateFrom 
		         AND CAST(tblSrc.TxnDate AS DATE)<@DateTo	
				 AND tblSrc.fk_FleetId in (select fk_FleetId from UserFleet where fk_UserTypeId=5 and IsActive=1 and fk_UserID=@CorpUserId)    
		) tblSaleCompletion	
     ) T
     INNER JOIN
     (
      SELECT Trans.MerchantId
	  ,Trans.FleetID
	  ,Fl.FleetTransaction ExpFleetTransaction,Fl.CarTransaction ExpCarTransaction,Fl.TransactionValue ExpTransactionValue
      ,Trans.FTransCount ActFleetTransaction,Trans.VTransCount ActCarTransaction,Trans.Amount ActTransactionValue FROM
     (
	  SELECT fk_MerchantID MerchantID, FleetID, FleetName, FleetTransaction, CarTransaction, TransactionValue  
	  FROM Fleet F WHERE  F.IsActive = 1 AND FleetId in (select fk_FleetId from UserFleet where fk_UserTypeId=5 and IsActive=1 and fk_UserID=@CorpUserId)
	  AND ( F.FleetTransaction IS NOT NULL OR  F.CarTransaction IS NOT NULL OR  F.TransactionValue IS NOT NULL) 
      ) Fl
     INNER JOIN
     (
	 SELECT V.MerchantId,V.FleetID,SUM(V.TransCount) FTransCount,Count(V.TransCount) VTransCount,SUM(V.FareValue) FareValue,SUM(V.Tip) AS 'Tip'
	 ,SUM(V.Surcharge) AS 'Surcharge',SUM(V.Amount) AS 'Amount',SUM(V.Fee) AS 'Fee',SUM(V.TechFee) AS 'TechFee',SUM(AmountOwing) AS 'AmountOwing' FROM 
	 (
		SELECT COALESCE(tblSaleCompletion.MerchantId,0) AS 'MerchantId'
		,COALESCE((tblSaleCompletion.fk_FleetId),'') AS FleetID
		,COALESCE(tblSaleCompletion.VehicleNo,'') AS 'VehicleNo'
		,COUNT(tblSaleCompletion.VehicleNo) AS TransCount
		,SUM(tblSaleCompletion.FareValue) AS 'FareValue'
		,SUM(tblSaleCompletion.Tip) AS 'Tip'
		,SUM(tblSaleCompletion.Surcharge) AS 'Surcharge'
		,SUM(tblSaleCompletion.Amount) AS 'Amount'
		,COALESCE(SUM(tblSaleCompletion.Fee),0)+COALESCE(SUM(tblSaleCompletion.FleetFee),0) AS 'Fee'
		,SUM(tblSaleCompletion.TechFee) AS 'TechFee'
		,COALESCE(SUM(tblSaleCompletion.Amount),0) - COALESCE(SUM(tblSaleCompletion.Fee),0)-COALESCE(SUM(tblSaleCompletion.FleetFee),0) AS 'AmountOwing'
		FROM 
		(
		SELECT 
		      MerchantId AS 'MerchantId'
			 ,fk_FleetId AS 'fk_FleetId'
			 ,VehicleNo AS 'VehicleNo'
			 ,FareValue AS 'FareValue'
			 ,Tip AS 'Tip'
			 ,Surcharge AS 'Surcharge'
			 ,Amount AS 'Amount'
			 ,Fee AS 'Fee'
			 ,TechFee AS 'TechFee'
			 ,FleetFee AS 'FleetFee'                                                                                                                                
	     FROM    dbo.MTDTransaction 								 													 								
		 WHERE   TxnType='Sale'
				 AND IsRefunded=0
			     AND IsVoided=0				    
		         AND ResponseCode IN ('000','002','00')
				 AND CAST(TxnDate AS DATE)>=@DateFrom 
		         AND CAST(TxnDate AS DATE)<@DateTo	
				 And fk_FleetId in (select fk_FleetId from UserFleet where fk_UserTypeId=5 and IsActive=1 and fk_UserID=@CorpUserId)   
		  UNION ALL
		  SELECT  tblId.MerchantId,
		          tblId.fk_FleetId,
			      tblSrc.VehicleNo,
				  tblId.FareValue,
			      tblId.Tip,
				  tblId.Surcharge,
			      tblSrc.Amount,
				  tblId.Fee,
				  tblId.TechFee,
				  tblId.FleetFee                                                              
	      FROM dbo.MTDTransaction tblSrc 
	      INNER JOIN dbo.MTDTransaction tblId
		  ON tblSrc.SourceId=tblId.Id						 								
		  WHERE  tblSrc.TxnType='Completion'
		         AND tblSrc.ResponseCode IN ('000','002','00')
				 AND CAST(tblSrc.TxnDate AS DATE)>=@DateFrom 
		         AND CAST(tblSrc.TxnDate AS DATE)<@DateTo	 
				 And tblSrc.fk_FleetId in (select fk_FleetId from UserFleet where fk_UserTypeId=5 and IsActive=1 and fk_UserID=@CorpUserId)
		) tblSaleCompletion	
		GROUP BY tblSaleCompletion.VehicleNo,tblSaleCompletion.MerchantId,tblSaleCompletion.fk_FleetId
	 ) V GROUP BY V.MerchantId,V.FleetID
    ) Trans ON (fl.FleetID = Trans.FleetID) 
   WHERE Fl.FleetTransaction > Trans.FTransCount OR Fl.CarTransaction > Trans.VTransCount OR Fl.TransactionValue > Trans.Amount 
  ) AT ON (T.FleetID = AT.FleetID) WHERE T.FleetID=@FleetId 
   INSERT INTO @TableData(FleetName,VehicleNo,FareValue,Tip,Surcharge,TechFee,Amount,Fee,AmountOwing)
   SELECT COALESCE(Fleet,'') 
          ,COALESCE(VehicleNo,'') 
          ,CONCAT('$',CAST(COALESCE(FareValue,0.00) AS DECIMAL(18,2)))
		  ,CONCAT('$',CAST(COALESCE(Tip,0.00) AS DECIMAL(18,2)))
		  ,CONCAT('$',CAST(COALESCE(Surcharge,0.00) AS DECIMAL(18,2))) 
		  ,CONCAT('$',CAST(COALESCE(TechFee,0.00) AS DECIMAL(18,2))) 
		  ,CONCAT('$',CAST(COALESCE(Amount,0.00) AS DECIMAL(18,2)))
		  ,CONCAT('$',CAST(COALESCE(Fee,0.00) AS DECIMAL(18,2))) 
		  ,CONCAT('$',CAST(COALESCE(AmountOwing,0.00) AS DECIMAL(18,2))) 
		  FROM #Temp 
    SELECT @ExpFleetTransaction=ExpFleetTransaction
	      ,@ExpCarTransaction=ExpCarTransaction
		  ,@ExpTransactionValue=ExpTransactionValue
		  ,@ActFleetTransaction=ActFleetTransaction
		  ,@ActCarTransaction=ActCarTransaction
		  ,@ActTransactionValue=ActTransactionValue 
    FROM #Temp
    IF(@ExpFleetTransaction>@ActFleetTransaction) 
	  BEGIN
        SET @Case='UnExp Trans'
      END
    IF(@ExpCarTransaction>@ActCarTransaction) 
	  BEGIN 
        IF(@Case<>'') 
		  BEGIN
            SET @Case=@Case+','+ 'UnExp Cars'
          END
        ELSE 
		 BEGIN
          SET @Case=@Case+ 'UnExp Cars'
         END
      END
    IF(@ExpTransactionValue>@ActTransactionValue) 
	 BEGIN
       IF(@Case<>'') 
	     BEGIN
          SET @Case=@Case+','+'UnExp Trans Value'
         END
       ELSE 
	     BEGIN
          SET @Case=@Case+'UnExp Trans Value'
         END
     END
    IF(@Case IS NOT NULL and @Case<>'') 
	  BEGIN
         SET @Case='Exception Case :'+@Case
      END
    --Set the footer for each fleet having summarized exception details
     INSERT INTO @TableData(FleetName,VehicleNo,FareValue,Tip,Surcharge,TechFee,Amount)
     SELECT DISTINCT COALESCE(@Case,'') 
                    ,CONCAT('Exp Trans:',ExpFleetTransaction) 
					,CONCAT('Act Trans:',ActFleetTransaction)
					,CONCAT('Exp Cars:',ExpCarTransaction) 
					,CONCAT('Act Cars:',ActCarTransaction) 
					,CONCAT('Exp Value:$',ExpTransactionValue) 
					,CONCAT('Act Value:$',ActTransactionValue)  
					FROM #Temp
  
     DROP TABLE #Temp
   FETCH NEXT FROM Exp_Cursor INTO @FleetId
   END   
  CLOSE Exp_Cursor   
  DEALLOCATE Exp_Cursor
  SELECT FleetName AS 'FleetName',
         VehicleNo AS 'VehicleNo',
		 FareValue AS 'FareValue',
		 Tip AS 'Tip',
		 Surcharge AS 'Surcharge',
		 TechFee AS 'TechFee',
		 Amount AS 'Amount',
		 Fee AS 'Fee',
		 AmountOwing AS 'AmountOwing'
   FROM @TableData
END TRY
--use catch to log error in the table  ProcErrorHandler
BEGIN CATCH
        INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
        SELECT ERROR_NUMBER()
		,ERROR_SEVERITY()
		,ERROR_STATE()
		,ERROR_PROCEDURE()
		,ERROR_LINE()
		,ERROR_MESSAGE()
		,SUSER_SNAME()
		,HOST_NAME()
		,GETDATE()
END CATCH

GO
