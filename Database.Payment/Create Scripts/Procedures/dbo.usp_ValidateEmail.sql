IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ValidateEmail]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_ValidateEmail]
GO

CREATE PROCEDURE dbo.usp_ValidateEmail
-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <17-09-2015>
-- Description:	<Validate mac address and token>
-- =============================================
(
    @MacAddress NVARCHAR(100),
	@DriverNo NVARCHAR(50),
	@Token NVARCHAR(1000),
	@FleetId INT
	)
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON
		DECLARE @Result BIT = 1

		IF NOT EXISTS (SELECT 1 FROM dbo.Terminal WHERE MacAdd = @MacAddress AND IsActive = 1)
		BEGIN
			SET @Result = 0
		END

		IF NOT EXISTS (SELECT 1 FROM dbo.DriverLogin WHERE DriverNo = @DriverNo AND Token = @Token AND FleetId = @FleetId)
		BEGIN
			SET @Result = 0
		END

		SELECT @Result
	END TRY
	BEGIN CATCH
		   INSERT INTO ProcErrorHandler (ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, UserName, HostName, [TimeStamp])
		   SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
	END CATCH
END
GO
