﻿using System;

namespace MTD.Data.Data
{
   public  class TxnTaxSurcharges
   {
       public Nullable<byte> SurchargeType { get; set; }
       public Nullable<decimal> SurchargeFixed { get; set; }
       public Nullable<decimal> SurchargePer { get; set; }
       public Nullable<decimal> SurMaxCap { get; set; }
       public Nullable<decimal> StateTaxRate { get; set; }
       public Nullable<decimal> FederalTaxRate { get; set; }
       public Nullable<decimal> TipPerLow { get; set; }
       public Nullable<decimal> TipPerMedium { get; set; }
       public Nullable<decimal> TipPerHigh { get; set; }
       public bool IsTaxInclusive { get; set; }
       public byte? FeeType { get; set; }
       public decimal? FeeFixed { get; set; }
       public decimal? FeePer { get; set; }
       public decimal? FeeMaxCap { get; set; }
    }
}
