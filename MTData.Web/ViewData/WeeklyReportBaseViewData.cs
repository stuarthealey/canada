﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.Web.ViewData
{
    public class WeeklyReportBaseViewData
    {
       public int fk_UserID { get; set; }
       public Nullable<bool> IsRecipient { get; set; }

    }
}