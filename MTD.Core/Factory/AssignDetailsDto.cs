﻿using MTD.Core.DataTransferObjects;
using MTD.Data.Data;

namespace MTD.Core.Factory
{
    public static class AssignDetailsDto
    {
        public static SessionDto AssignProperties(SessionMerchantUser sessionMerchantUser)
        {
            SessionDto sessionDto = new SessionDto();
            sessionDto.SessionUser = new UserDto();
            sessionDto.SessionMerchant = new MerchantDetailsDto();
            sessionDto.SessionMerchant.GatewayId = sessionMerchantUser.SessionMerchant.GatewayId;
            sessionDto.SessionMerchant.GatewayOffset = sessionMerchantUser.SessionMerchant.GatewayOffset;
            sessionDto.SessionMerchant.IsSaleSupported = sessionMerchantUser.SessionMerchant.IsSaleSupported;
            sessionDto.SessionMerchant.IsPreauthSupported = sessionMerchantUser.SessionMerchant.IsPreauthSupported;
            sessionDto.SessionMerchant.IsCaptureSupported = sessionMerchantUser.SessionMerchant.IsCaptureSupported;
            sessionDto.SessionMerchant.IsCancelSupported = sessionMerchantUser.SessionMerchant.IsCancelSupported;
            sessionDto.SessionMerchant.IsRefundSupported = sessionMerchantUser.SessionMerchant.IsRefundSupported;
            sessionDto.SessionMerchant.IsActive = sessionMerchantUser.SessionMerchant.IsActive;
            sessionDto.SessionMerchant.IsDefault = sessionMerchantUser.SessionMerchant.IsDefault;
            sessionDto.SessionMerchant.MerchantGatewayID = sessionMerchantUser.SessionMerchant.MerchantGatewayID;
            sessionDto.SessionMerchant.fk_MerchantID = sessionMerchantUser.SessionMerchant.fk_MerchantID;
            sessionDto.SessionMerchant.fk_GatewayID = sessionMerchantUser.SessionMerchant.fk_GatewayID;
            sessionDto.SessionMerchant.MerchantGatewayUserName = sessionMerchantUser.SessionMerchant.MerchantGatewayUserName;
            sessionDto.SessionMerchant.MerchantGatewayPassword = sessionMerchantUser.SessionMerchant.MerchantGatewayPCode;
            sessionDto.SessionMerchant.MerchantGatewayIsActive = sessionMerchantUser.SessionMerchant.IsActive;
            sessionDto.SessionMerchant.MerchantGatewayName = sessionMerchantUser.SessionMerchant.Name;
            sessionDto.SessionMerchant.MerchantGatewayCompany = sessionMerchantUser.SessionMerchant.Company;
            sessionDto.SessionMerchant.IsMtdataGateway = sessionMerchantUser.SessionMerchant.IsMtdataGateway;
            sessionDto.SessionMerchant.Type = sessionMerchantUser.SessionMerchant.Type;
            sessionDto.SessionMerchant.TerminalId = sessionMerchantUser.SessionMerchant.TerminalId;
            sessionDto.SessionMerchant.SerialNo = sessionMerchantUser.SessionMerchant.SerialNo;
            sessionDto.SessionMerchant.fk_MerchantId = sessionMerchantUser.SessionMerchant.fk_MerchantID;
            sessionDto.SessionMerchant.TerminalIsActive = sessionMerchantUser.SessionMerchant.IsActive;
            sessionDto.SessionMerchant.MerchantID = sessionMerchantUser.SessionMerchant.MerchantID;
            sessionDto.SessionMerchant.Company = sessionMerchantUser.SessionMerchant.Company;
            sessionDto.SessionMerchant.SetupFee = sessionMerchantUser.SessionMerchant.SetupFee;
            sessionDto.SessionMerchant.MonthlyFee = sessionMerchantUser.SessionMerchant.MonthlyFee;
            sessionDto.SessionMerchant.StateTaxRate = sessionMerchantUser.SessionMerchant.StateTaxRate;
            sessionDto.SessionMerchant.IsTaxInclusive = sessionMerchantUser.SessionMerchant.IsTaxInclusive;
            sessionDto.SessionMerchant.FederalTaxRate = sessionMerchantUser.SessionMerchant.FederalTaxRate;
            sessionDto.SessionMerchant.TipPerLow = sessionMerchantUser.SessionMerchant.TipPerLow;
            sessionDto.SessionMerchant.Surchagre = sessionMerchantUser.SessionMerchant.Surcharge;
            sessionDto.SessionMerchant.TipPerMedium = sessionMerchantUser.SessionMerchant.TipPerMedium;
            sessionDto.SessionMerchant.TipPerHigh = sessionMerchantUser.SessionMerchant.TipPerHigh;
            sessionDto.SessionMerchant.MerchantIsActive = sessionMerchantUser.SessionMerchant.IsActive;
            sessionDto.SessionMerchant.IsAllowSale = sessionMerchantUser.SessionMerchant.IsAllowSale;
            sessionDto.SessionMerchant.IsAllowPreauth = sessionMerchantUser.SessionMerchant.IsAllowPreauth;
            sessionDto.SessionMerchant.IsAllowCapture = sessionMerchantUser.SessionMerchant.IsAllowCapture;
            sessionDto.SessionMerchant.IsAllowRefund = sessionMerchantUser.SessionMerchant.IsAllowRefund;
            sessionDto.SessionMerchant.IsAllowVoid = sessionMerchantUser.SessionMerchant.IsAllowVoid;
            sessionDto.SessionUser.UserID = sessionMerchantUser.SessionUser.UserID;
            if (sessionMerchantUser.SessionUser.FName != null)
            {
                int length = sessionMerchantUser.SessionUser.FName.Length;
                if (length > 11)
                {

                    string nameNow = sessionMerchantUser.SessionUser.FName.Substring(0, 9) + "...";
                    sessionDto.SessionUser.FName = nameNow;
                }
                else
                {
                    sessionDto.SessionUser.FName = sessionMerchantUser.SessionUser.FName;
                }
            }
            sessionDto.SessionUser.LName = sessionMerchantUser.SessionUser.LName;
            sessionDto.SessionUser.Email = sessionMerchantUser.SessionUser.Email;
            sessionDto.SessionUser.Phone = sessionMerchantUser.SessionUser.Phone;
            sessionDto.SessionUser.fk_Country = sessionMerchantUser.SessionUser.fk_Country;
            sessionDto.SessionUser.fk_State = sessionMerchantUser.SessionUser.fk_State;
            sessionDto.SessionUser.fk_City = sessionMerchantUser.SessionUser.fk_City;
            sessionDto.SessionUser.fk_MerchantID = sessionMerchantUser.SessionUser.fk_MerchantID;
            sessionDto.SessionUser.fk_UserTypeID = sessionMerchantUser.SessionUser.fk_UserTypeID;
            sessionDto.SessionUser.IsLockedOut = sessionMerchantUser.SessionUser.IsLockedOut;
            sessionDto.SessionUser.IsActive = sessionMerchantUser.SessionUser.IsActive;
            sessionDto.SessionUser.RolesAssigned = sessionMerchantUser.SessionUser.RolesAssigned;
            sessionDto.SessionUser.fleetListId = sessionMerchantUser.SessionUser.fleetListId;
            sessionDto.SessionUser.Address = sessionMerchantUser.SessionUser.Address;
            sessionDto.SessionUser.IsFirstLogin = sessionMerchantUser.SessionUser.IsFirstLogin;
            return sessionDto;
        }
    }
}
