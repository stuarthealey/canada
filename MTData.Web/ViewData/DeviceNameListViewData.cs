﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MTData.Web.ViewData
{
    public class DeviceNameListViewData
    {
        public int PayTerminalId { get; set; }
        [Required(ErrorMessage="Please enter device name")]
        [RegularExpression(@"^[A-Za-z,0-9, - , : , .]+$", ErrorMessage = "Device name  is not valid")]
        public string DeviceName { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}