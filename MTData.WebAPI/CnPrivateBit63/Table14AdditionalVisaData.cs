﻿namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table14AdditionalVisaData
    {
        #region Private Constants and Variables
        #region Constants
        private const string tblId = "|31|34";
        #endregion

        #region Variables
        private string defaultACI = "|59"; // |59 represents hex of Y
        private string defaultTransID = "|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20";
        private string defaultValidationCode = "|20|20|20|20";
        private string defaultMSDI = "|20";
        private string defaultRPSI = "|20";
        private string defaultFirstAuthAmt = "|30|30|30|30|30|30|30|30|30|30|30|30";
        private string defaultTotalAuthAmt = "|30|30|30|30|30|30|30|30|30|30|30|30";

        #endregion
        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'31 34'
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        /// <summary>
        /// Authorization Characteristic Indicator
        /// default Y
        /// </summary>
        public string ACI
        {
            get { return defaultACI; }
            set { defaultACI = value; }
        }

        /// <summary>
        /// Transaction Id provided by Issuer
        /// default 15 Spaces(1 space= X'20')
        /// </summary>
        public string TransID
        {
            get { return defaultTransID; }
            set { defaultTransID = value; }
        }

        /// <summary>
        /// Validation Code provided by Issuer
        /// default 4 Spaces(1 space= X'20')
        /// </summary>
        public string ValidationCode
        {
            get { return defaultValidationCode; }
            set { defaultValidationCode = value; }
        }

        /// <summary>
        /// Market Specific Data Indicator
        /// default 1 Spaces(1 space= X'20')
        /// For Auto: A, Bill Payment: B, Hotel: H, Healthcare(Medical): M,
        /// Transit: T, B2B Straight Through Transactions: J
        /// </summary>`
        public string MSDI
        {
            get { return defaultMSDI; }
            set { defaultMSDI = value; }
        }

        /// <summary>
        /// Requested Payment Service Indicator
        /// default 1 Spaces(1 space= X'20')
        /// </summary>
        public string RPSI
        {
            get { return defaultRPSI; }
            set { defaultRPSI = value; }
        }

        /// <summary>
        /// Amount that was initially authorized
        /// default 12 Zero(1 Zero= X'30')
        /// </summary>
        public string FirstAuthorizedAmount
        {
            get { return defaultFirstAuthAmt; }
            set { defaultFirstAuthAmt = value; }
        }

        /// <summary>
        /// Total authorized amount after all incremental authorizations
        /// default 12 Zero(1 Zero= X'30')
        /// </summary>
        public string TotalAuthorizedAmount
        {
            get { return defaultTotalAuthAmt; }
            set { defaultTotalAuthAmt = value; }
        }

        #endregion
    }
}