﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using MTData.WebAPI.CanadaCommon;

namespace MTData.WebAPI.CanadaCert
{
    /// <summary>
    /// This class is used for traveling data between different layer for registering the merchant 
    /// </summary>
    [DataContract]
    public class MerchantRegistrationDetails
    {
        [DataMember]
        public string MID { get; set; }

        [DataMember]
        public string TID { get; set; }

        [DataMember]
        public string MerchantName { get; set; }

        [DataMember]
        public string ContactFirstName { get; set; }

        [DataMember]
        public string ContactLastName { get; set; }
        
        [DataMember]
        public bool ExitingDebtIndicator { get; set; }

        [DataMember]
        public string MccCode { get; set; }
        
        [DataMember]
        public IndustryType MerchantIndustry { get; set; }

        [DataMember]
        public string StreetLine1 { get; set; }

        [DataMember]
        public string StreetLine2 { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string ZipCode { get; set; }

        [DataMember]
        public string ContactNumber { get; set; }

        [DataMember]
        public string CustomerServiceNo { get; set; }

        [DataMember]
        public string MerchantUrl { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Website { get; set; }

        [DataMember]
        public string MvvCode { get; set; }

        [DataMember]//Added for amex charge descriptor
        public string ChargeDescriptor { get; set; } 

        #region Merchant Related public properties        

        public string DID { get; set; }

        public string ClientRef { get; set; }

        public string ResponseXml { get; set; }

        public string StatusCode { get; set; }

        public string StatusText { get; set; }

        public string Comment { get; set; }

        public List<string> RegUrls { get; set; }

        public bool ActivationStatus { get; set; }

        #endregion //end merchant log related
    }

    

}
