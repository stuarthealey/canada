﻿using MTD.Core.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class DownloadResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public ResponseHeader Header { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public IEnumerable<DownloadDriverDataDto> Result { get; set; }
    }
}