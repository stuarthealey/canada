﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using MTD.Core.Service;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository;
using MTD.Data.Repository.Interface;
using Ninject;
using Ninject.Web.Common;
using Ninject.Modules;
using MTD.Core.Factory.Interface;
using MTD.Core.Factory;

namespace MTDataDispatchSystemSync
{
    public class Global : NinjectHttpApplication
    {
        protected override IKernel CreateKernel()
        {
            var kernel = new StandardKernel();

            kernel.Bind<IMtDataDevlopmentsEntities>().To<MTDataDevlopmentsEntities>()
                 .InTransientScope().Named("MtDataDevlopments");

             kernel.Bind<IDriverRepository>().To<DriverRepository>()
            .InTransientScope().Named("DriverRepository");


            kernel.Bind<IDriverService>().To<DriverService>()
                .InTransientScope().Named("DriverService");
            kernel.Bind<ITerminalRepository>().To<TerminalRepository>();


                 kernel.Bind<IVehicleRepository>().To<VehicleRepository>()
           .InScope(ctx => HttpContext.Current).Named("VehicleRepository");


            kernel.Bind<IVehicleService>().To<VehicleService>()
                .InScope(ctx => HttpContext.Current).Named("VehicleService");
           

              kernel.Bind<IFleetRepository>().To<FleetRepository>() // Map IFleetRepository to FleetRepository  
           .InScope(ctx => HttpContext.Current).Named("FleetRepository");

            kernel.Bind<IFleetService>().To<FleetService>() // Map IFleetService to FleetService  
                .InScope(ctx => HttpContext.Current).Named("FleetService");

            kernel.Bind<IFleetFactory>().To<FleetFactory>()  // Map IFleetFactory to FleetFactory  
                  .InSingletonScope().Named("FleetFactory");

            return kernel;
        } 
    }
}