﻿using System;
using System.Text.RegularExpressions;
using System.Configuration;

using MTData.Utility;
using MTData.WebAPI.Models;
using MTData.WebAPI.CanadaCommon;

namespace MTData.WebAPI.CanadaCert
{
    public class CnPreparePostTransaction
    {
        /// <summary>
        /// Prepare payload request for ecommerce transactions
        /// </summary>
        /// <param name="transRequest"></param>
        /// <param name="clientRef"></param>
        /// <param name="cnTransRequest"></param>
        /// <returns></returns>
        public static string CheckAndPrepareEcommTrans(TransactionRequest transRequest, string clientRef, out CnTransactionEntities cnTransRequest)
        {
            ILogger logger = new Logger();

            // Debug
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
            {
                logger.LogInfoMessage(string.Format("CnPreparePostTransaction.CheckAndPrepareEcommTrans() clientRef:{0};PortalEmvRefundVoidReq:{1};IsTORTransaction:{2};", clientRef, transRequest.PortalEmvRefundVoidReq, transRequest.IsTORTransaction));
                logger.LogInfoMessage(string.Format("CnPreparePostTransaction.CheckAndPrepareEcommTrans() transRequest:{0};IsTA:{1};", transRequest.ToString(), transRequest.IsTransArmor));
            }

            cnTransRequest = CnTransactionMessage.MakeEcommTransRequest(transRequest);
            cnTransRequest.PortalEmvRefundVoidReq = transRequest.PortalEmvRefundVoidReq; //to identify EMV refund/void transaction request from portal

            // Set the IsTransArmor flag on the cnTransRequest, so it is passed down the line.
            cnTransRequest.IsTransArmor = transRequest.IsTransArmor;

            CnTransactionMessage transactionMessage = new CnTransactionMessage();

            string payload = transactionMessage.CreateMessage(cnTransRequest);

            // Debug
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                logger.LogInfoMessage(string.Format("CnPreparePostTransaction.CheckAndPrepareEcommTrans() clientRef:{0};Payload:{1};", clientRef, payload));

            // Create Complete transaction Request with Datwire format
            if (transRequest.IsTORTransaction)
                clientRef = CnTransactionMessage.GetCanadaClientRef();

            // Debug
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                logger.LogInfoMessage(string.Format("CnPreparePostTransaction.CheckAndPrepareEcommTrans() clientRef:{0};Payload:{1};", clientRef, payload));

            string transactionXml = CnTransactionMessage.SetTransactionXml(transRequest, payload, clientRef);

            // Debug
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                logger.LogInfoMessage(string.Format("CnPreparePostTransaction.CheckAndPrepareEcommTrans() clientRef:{0};Payload:{1};transactionXML:{2};", clientRef, payload, transactionXml));

            return transactionXml;
        }

        /// <summary>
        /// Prepare payload request for retail transactions
        /// </summary>
        /// <param name="transRequest"></param>
        /// <param name="clientRef"></param>
        /// <param name="cnTransRequest"></param>
        /// <returns></returns>
        public static string CheckAndPrepareRetailTrans(TransactionRequest transRequest, string clientRef, out CnTransactionEntities cnTransRequest)
        {
            ILogger logger = new Logger();

            // Debug
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                logger.LogInfoMessage(string.Format("CnPreparePostTransaction.CheckAndPrepareRetailTrans() clientRef:{0};TransType:{1};IsTOR:{2};Track2Data:{3};", clientRef, transRequest.TransType, transRequest.IsTORTransaction, transRequest.Track2Data));

            //SH 2018-07-25 - Removed as Chetu changed it back to a string.empty.
            if (transRequest.TransType.ToString() != TxnTypeType.Completion.ToString() && (!transRequest.IsTORTransaction))
            {
                //PAY-13 Check if the first character is non-numeric, and if so then trim it.
                if (!string.IsNullOrEmpty(transRequest.Track2Data))
                {
                    if (!Regex.IsMatch(transRequest.Track2Data, @"^\d+"))
                        transRequest.Track2Data = transRequest.Track2Data.Substring(1);
                }
            }
            else
                transRequest.Track2Data = string.Empty;

            cnTransRequest = CnTransactionMessage.MakeRetailTansRequest(transRequest);

            if (transRequest.TransType.ToString() == TxnTypeType.Completion.ToString())
                cnTransRequest.PrimaryAccountNumber = transRequest.CardNumber;

            CnTransactionMessage transactionMessage = new CnTransactionMessage();
            
            string payload = transactionMessage.CreateMessage(cnTransRequest);

            // Debug
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                logger.LogInfoMessage(string.Format("CnPreparePostTransaction.CheckAndPrepareRetailTrans() clientRef:{0};payload:{1};", clientRef, payload));

            // Create Complete transaction Request with Datawire format
            string transactionXml = CnTransactionMessage.SetTransactionXml(transRequest, payload, clientRef);

            // Debug
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                logger.LogInfoMessage(string.Format("CnPreparePostTransaction.CheckAndPrepareRetailTrans() clientRef:{0};payload:{1};Track2Data:{2};CardNumber:{3};transactionXml:{4};", clientRef, payload, transRequest.Track2Data, transRequest.CardNumber, transactionXml));

            return transactionXml;
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Prepare payload request for EMV transactions
        /// </summary>
        /// <param name="transRequest"></param>
        /// <param name="clientRef"></param>
        /// <param name="cnTransRequest"></param>
        /// <returns></returns>
        public static string PrepareEMVTransRequest(TransactionRequest transRequest, string clientRef, out CnTransactionEntities cnTransRequest)
        {
            cnTransRequest = CnTransactionMessage.MakeEMVTransRequest(transRequest);
            CnTransactionMessage transactionMessage = new CnTransactionMessage();

            string payload = transactionMessage.CreateIccMessage(cnTransRequest, transRequest);

            string transactionXml = CnTransactionMessage.SetTransactionXml(transRequest, payload, clientRef);

            return transactionXml;
        }

        /// <summary>
        /// Prepare payload request for EMV Contactless transactions 
        /// </summary>
        /// <param name="transRequest"></param>
        /// <param name="clientRef"></param>
        /// <param name="cnTransRequest"></param>
        /// <returns></returns>
        public static string PrepareEMVContactlessTrans(TransactionRequest transRequest, string clientRef, out CnTransactionEntities cnTransRequest)
        {
            cnTransRequest = CnTransactionMessage.MakeEMVContactlessTransRequest(transRequest);
            CnTransactionMessage transactionMessage = new CnTransactionMessage();

            string payload = transactionMessage.CreateIccMessage(cnTransRequest, transRequest);

            return CnTransactionMessage.SetTransactionXml(transRequest, payload, clientRef);
        }

        /// <summary>
        /// Purpose             :   Prepare payload request to be sent to first data
        /// Function Name       :   PrepareTokenOnlyPayloadRequest
        /// Created By          :   Naveen Kumar
        /// Created On          :   11/05/2016
        /// </summary>
        /// <returns>string</returns>
        public static string PrepareBookerAppPayloadRequest(TransactionRequest transRequest, string clientRef, out CnTransactionEntities cnTransRequest)
        {
            ILogger logger = new Logger();

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
            {
                logger.LogInfoMessage(string.Format("CnPreparePostTransaction.PrepareBookerAppPayloadRequest() IndustryType:{0};IsTA:{1};RCCI:{2};TokenType:{3};",
                                        transRequest.IndustryType, transRequest.IsTransArmor, (!string.IsNullOrEmpty(transRequest.RCCIToken) ? transRequest.RCCIToken : "<NULL>"), transRequest.TokenType));
            }

            string transactionXml = string.Empty;

            try
            {
                string messageType = string.Empty;
                if (transRequest.IndustryType == SigmaStaticValue.BookerAppToken)
                {
                    transRequest.CardType = CreditCardType.GetCardType(transRequest.CardNumber); //changes for card registration

                    cnTransRequest = CnTransactionMessage.PrepareFieldsForTokenOnlyRequest(transRequest);

                    cnTransRequest.MerchantZipCode = string.Empty;          // Clear the Zipcode value, to try and stop it appearing in the RCCI request payload.

                    messageType = SigmaStaticValue.MessageType_TokenOnly;
                }
                else
                {
                    if (transRequest.IndustryType == SigmaStaticValue.BookerAppPayment && !string.IsNullOrEmpty(transRequest.RCCIToken))
                        transRequest.CardNumber = string.Empty;

                    cnTransRequest = CnTransactionMessage.MakeEcommTransRequest(transRequest);
                    messageType = SigmaStaticValue.MessageType_ZeroOneZeroZero;
                }

                if (cnTransRequest.IsTORTransaction)
                    messageType = SigmaStaticValue.MessageType_ZeroFourZeroZero;

                // Set the IsTransArmor flag on the cnTransRequest, so it is passed down the line.
                cnTransRequest.IsTransArmor = transRequest.IsTransArmor;

                MessageType bookerAppType = (MessageType)Enum.Parse(typeof(MessageType), cnTransRequest.CardType.ToString() + cnTransRequest.TransType.ToString());
                CnTransactionMessage transactionMessage = new CnTransactionMessage();
                                
                string dataElements = transactionMessage.GetMessage(bookerAppType, cnTransRequest);
                string payload = string.Concat(messageType, dataElements);

                if (cnTransRequest.IsTORTransaction)
                    clientRef = CnTransactionMessage.GetCanadaClientRef();

                // Create Complete transaction Request with Datwire format
                transactionXml = CnTransactionMessage.SetTransactionXml(transRequest, payload, clientRef);

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    logger.LogInfoMessage(string.Format("CnPreparePostTransaction.PrepareBookerAppPayloadRequest() Payload:{0};", payload));
            }
            catch
            {
                throw;
            }

            return transactionXml;
        }

        /// <summary>
        /// Prepare KeyUpdate request
        /// </summary>
        /// <param name="transRequest"></param>
        /// <param name="clientRef"></param>
        /// <param name="cnTransRequest"></param>
        /// <returns></returns>
        public static string PrepareKeyUpdateRequest(TransactionRequest transRequest, string clientRef, out CnTransactionEntities cnTransRequest)
        {
            ILogger logger = new Logger();

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
            {
                logger.LogInfoMessage(string.Format("CnPreparePostTransaction.PrepareKeyUpdateRequest() IndustryType:{0};IsTA:{1};RCCI:{2};",
                                        transRequest.IndustryType, transRequest.IsTransArmor, (!string.IsNullOrEmpty(transRequest.RCCIToken) ? transRequest.RCCIToken : "<NULL>")));
            }

            cnTransRequest = CnTransactionMessage.MakeKeyUpdateRequest(transRequest);

            string messageId = "|08|00";
            MessageType type = MessageType.KeyUpdateRequest;

            CnTransactionMessage message1 = new CnTransactionMessage();

            // Get Message with Bitmap and Data Elements.
            string dataElements = message1.GetMessage(type, cnTransRequest);    // Pass payment type with transaction typr like CreditSale
            string finalMessage = string.Concat(messageId, dataElements);

            // Create Complete transaction Request with Datwire format
            if (transRequest.IsTORTransaction)
                clientRef = CnTransactionMessage.GetCanadaClientRef();

            transRequest.Did = "00016912627258666220";
            transRequest.FdMerchantId = "000082004330015";

            return CnTransactionMessage.SetTransactionXml(transRequest, finalMessage, clientRef);
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Prepare EMV fallback transaction request
        /// </summary>
        /// <param name="transRequest"></param>
        /// <param name="clientRef"></param>
        /// <param name="cnTransRequest"></param>
        /// <returns></returns>
        public static string PrepareFallbackCreditTrans(TransactionRequest transRequest, string clientRef, out CnTransactionEntities cnTransRequest)
        {
            cnTransRequest = CnTransactionMessage.MakeFallbackCreditTansRequest(transRequest);
            CnTransactionMessage transactionMessage = new CnTransactionMessage();
            string payload = transactionMessage.CreateIccMessage(cnTransRequest, transRequest);

            return CnTransactionMessage.SetTransactionXml(transRequest, payload, clientRef);
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Prepare payload request for Interac transactions
        /// </summary>
        /// <param name="transRequest"></param>
        /// <param name="clientRef"></param>
        /// <param name="cnTransRequest"></param>
        /// <returns></returns>
        public static string PrepareInteracTransRequest(TransactionRequest transRequest, string clientRef, out CnTransactionEntities cnTransRequest)
        {
            cnTransRequest = CnTransactionMessage.MakeInteracTransRequest(transRequest);
            cnTransRequest.MacData = transRequest.MacData;
            cnTransRequest.CheckDigit = transRequest.CheckDigit;
            cnTransRequest.IsMAcVerificationFailed = transRequest.IsMAcVerificationFailed;

            CnTransactionMessage transactionMessage = new CnTransactionMessage();

            string payload = transactionMessage.CreateIccMessage(cnTransRequest, transRequest);

            return CnTransactionMessage.SetTransactionXml(transRequest, payload, clientRef);
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Prepare payload request for Debit key update 
        /// </summary>
        /// <param name="transRequest"></param>
        /// <param name="clientRef"></param>
        /// <param name="cnTransRequest"></param>
        /// <returns></returns>
        public static string PrepareNewKeyRequest(TransactionRequest transRequest, string clientRef, out CnTransactionEntities cnTransRequest)
        {
            ILogger logger = new Logger();

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
            {
                logger.LogInfoMessage(string.Format("CnPreparePostTransaction.PrepareNewKeyRequest() IndustryType:{0};IsTA:{1};RCCI:{2};",
                                        transRequest.IndustryType, transRequest.IsTransArmor, (!string.IsNullOrEmpty(transRequest.RCCIToken) ? transRequest.RCCIToken : "<NULL>")));
            }

            cnTransRequest = CnTransactionMessage.MakeDebitKeyRequest(transRequest);

            string messageId = SigmaStaticValue.MessageType_TokenOnly;
            MessageType type = MessageType.NewKeyRequest;

            CnTransactionMessage message = new CnTransactionMessage();
            //Get Message with Bitmap and Data Elements.
            string dataElements = message.GetMessage(type, cnTransRequest);
            string finalMessage = string.Concat(messageId, dataElements);

            transRequest.Did = transRequest.Did;
            transRequest.FdMerchantId = transRequest.FdMerchantId;

            return CnTransactionMessage.SetTransactionXml(transRequest, finalMessage, clientRef);
        }

        //zzz Chetu 357
        /// <summary>
        /// Prepare payload request for MSR Contactless transactions
        /// </summary>
        /// <param name="transRequest"></param>
        /// <param name="clientRef"></param>
        /// <param name="cnTransRequest"></param>
        /// <returns></returns>
        public static string PrepareMSRContactlessTransRequest(TransactionRequest transRequest, string clientRef, out CnTransactionEntities cnTransRequest)
        {
            cnTransRequest = CnTransactionMessage.MakeMSRContactlessTransRequest(transRequest);

            CnTransactionMessage transactionMessage = new CnTransactionMessage();
            string payload = transactionMessage.CreateMessage(cnTransRequest);

            // Create Complete transaction Request with Datawire format
            return CnTransactionMessage.SetTransactionXml(transRequest, payload, clientRef);
        }

        /// <summary>
        /// Prepare payload request of Interac for EMV Contactless transaction
        /// </summary>
        /// <param name="transRequest"></param>
        /// <param name="clientRef"></param>
        /// <param name="cnTransRequest"></param>
        /// <returns></returns>
        public static string PrepareInteracContactlessTransRequest(TransactionRequest transRequest, string clientRef, out CnTransactionEntities cnTransRequest)
        {
            cnTransRequest = CnTransactionMessage.MakeInteracContactlessTransRequest(transRequest);
            
            CnTransactionMessage transactionMessage = new CnTransactionMessage();
            string payload = transactionMessage.CreateIccMessage(cnTransRequest, transRequest);

            return CnTransactionMessage.SetTransactionXml(transRequest, payload, clientRef);
        }

    }
}