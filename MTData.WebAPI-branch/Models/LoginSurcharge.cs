﻿using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class LoginSurcharge
    {
        private CcNoPrefix cardNumberPrefix;

        [DataMember]
        public CcNoPrefix CardNumberPrefix
        {
            get { return this.cardNumberPrefix; }
            set { this.cardNumberPrefix = value; }
        }
    }
}