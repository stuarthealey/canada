﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;


namespace MTData.Utility.Extension
{
    public static class GetCnDatawireResponse
    {
        public static CnDatawireResponse GetDatawireResponse(string respData)
        {
            //string data = File.ReadAllText(@"D:\dwireResp.txt");
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(respData);

            var innerXML = xmlDoc.FirstChild.InnerXml;
            innerXML = innerXML.Replace("&lt;", "<").Replace("&gt;", ">");
            xmlDoc.LoadXml(innerXML);

            var did = xmlDoc.SelectSingleNode("Response/RespClientID/DID").InnerText;
            var clientRef = xmlDoc.SelectSingleNode("Response/RespClientID/ClientRef").InnerText;

            var statusCodeNode = xmlDoc.SelectSingleNode("Response/Status");
            var statusCode = statusCodeNode.Attributes["StatusCode"].Value;
            return new CnDatawireResponse { DID = did, ClientRef = clientRef, StatusCode = statusCode, CompleteResponse = innerXML };
        }
    }
}
