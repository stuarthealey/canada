﻿using MTD.Core.DataTransferObjects;
using MTD.Data.Data;


namespace MTD.Core.Factory.Interface
{
   public interface ITransactionFactory
    {
        MTDTransaction CreateTransaction(TransactionDto transactionDto);
      
        
    }
}
