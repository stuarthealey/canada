﻿using System.Collections.Generic;
using MTD.Core.DataTransferObjects;
using MTD.Core.Extension;
using MTD.Core.Factory.Interface;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;
using Ninject;

namespace MTD.Core.Service
{
    public class ReceiptService : IReceiptService
    {
        private readonly IReceiptRepository _receiptRepository;
        private readonly IMerchantFactory _merchantFactory;


        public ReceiptService([Named("ReceiptRepository")] IReceiptRepository receiptRepository, [Named("MerchantFactory")] IMerchantFactory merchantFactory)
        {
            _receiptRepository = receiptRepository; //need to initilize object only once due to ninject
            _merchantFactory = merchantFactory;
        }

        /// <summary>
        /// Purpose             :   To get Receipt List
        /// Function Name       :   ReceiptList
        /// Created By          :   Salil Gupta
        /// Created On          :   02/19/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<ReceiptFormatDto> ReceiptList(int merchantId, string name, int logOnByUserId, int logOnByUserType)
        {
            try
            {
                IEnumerable<ReceiptFormatDto> receipt = _receiptRepository.ReceiptList(merchantId,name,logOnByUserId,logOnByUserType).ToDTOList();
                return receipt;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Receipt Fleet
        /// Function Name       :   GetReceiptFleet
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   02/19/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public IEnumerable<FleetReceiptDto> GetReceiptFleet(int fleetId)
        {
            try
            {
                IEnumerable<FleetReceiptDto> obj = _receiptRepository.GetReceiptFleet(fleetId).ToDTOList();
                //manageRecieptDto recieptDto = obj.ToManageRecDtoDto();
                return obj;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get User Fleet
        /// Function Name       :   GetUserFleet
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/17/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<FleetDto> GetUserFleet(int merchantId)
        {
            try
            {
                IEnumerable<Fleet> fleetlist = _receiptRepository.GetUserFleet(merchantId);
                var fleetDtoList = fleetlist.ToDTOList();
                return fleetDtoList;
            }
            catch
            {
                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To Get Receipt Fields
        /// Function Name       :   GetReceiptFields
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/16/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ReceiptMasterDto> GetReceiptFields()
        {
            try
            {
                IEnumerable<ReceiptMasterDto> master = _receiptRepository.GetReceiptFields().RecMasterToDTOList();
                return master;
            }
            catch
            {
                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To Update Receipt Format
        /// Function Name       :   Update
        /// Created By          :   Umesh kumar
        /// Created On          :   02/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="receptDto"></param>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public bool Update(ReceiptFormatDto receptDto, int fleetId)
        {
            try
            {
                var receiptFormat = receptDto.ToReceipt();
                return _receiptRepository.Update(receiptFormat, fleetId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Delete the Receipt
        /// Function Name       :   DeleteReceipt
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/19/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        public void DeleteReceipt(int fleetId)
        {
            try
            {
                _receiptRepository.DeleteReceipt(fleetId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Delete the Receipt
        /// Function Name       :   DeleteReceipt
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/19/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        public void DeleteReceipt(int fleetId, int merchantId)
        {
            try
            {
                _receiptRepository.DeleteReceipt(fleetId, merchantId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Delete Receipt Field
        /// Function Name       :   DeleteReceiptField
        /// Created By          :   Umesh kumar
        /// Created On          :   02/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fieldId"></param>
        public void DeleteReceiptField(int fieldId)
        {
            try
            {
                _receiptRepository.DeleteReceiptField(fieldId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Add a Reciept
        /// Function Name       :   AddReciept
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/15/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="recieptDto"></param>
        public void AddReciept(ReceiptFormatDto recieptDto)
        {
            try
            {
                ReceiptFormat recipt = recieptDto.ToReceipt();
                _receiptRepository.AddReciept(recipt);
            }
            catch
            {
                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To Add Reciept Field
        /// Function Name       :   AddRecieptField
        /// Created By          :   Naveen Kumar
        /// Created On          :   02/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="field"></param>
        /// <param name="receiptMasterDto"></param>
        public void AddRecieptField(string field, ReceiptMasterDto receiptMasterDto)
        {
            try
            {
                ReceiptMaster receiptMaster = _merchantFactory.ToReceiptMaster(receiptMasterDto);
                _receiptRepository.AddRecieptField(field, receiptMaster);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To check Field Exist
        /// Function Name       :   IsFieldExist
        /// Created By          :   Salil Gupta
        /// Created On          :   02/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public bool IsExistFleet(int fleetId)
        {
            try
            {
                var result = _receiptRepository.IsExistFleet(fleetId);
                return result;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To check Field Exist
        /// Function Name       :   IsFieldExist
        /// Created By          :   Salil Gupta
        /// Created On          :   02/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fieldId"></param>
        /// <returns></returns>
        public bool IsFieldExist(int fieldId)
        {
            try
            {
                var result = _receiptRepository.IsFieldExist(fieldId);
                return result;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To check Existing Fleet For Update
        /// Function Name       :   IsExistFleetForUpdate
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/15/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <param name="receiptId"></param>
        /// <returns></returns>
        public bool IsExistFleetForUpdate(int fleetId, int receiptId)
        {
            try
            {
                var result = _receiptRepository.IsExistFleetForUpdate(fleetId, receiptId);
                return result;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update Receipt Master
        /// Function Name       :   UpdateReceiptMaster
        /// Created By          :   Naveen Kumar
        /// Created On          :   02/13/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="receiptMasterDto"></param>
        /// <returns></returns>
        public bool UpdateReceiptMaster(ReceiptMasterDto receiptMasterDto)
        {
            try
            {
                ReceiptMaster receiptMaster = _merchantFactory.ToReceiptMaster(receiptMasterDto);
                return _receiptRepository.UpdateReceiptMaster(receiptMaster);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Receipts List
        /// Function Name       :   ReceiptListByFilter
        /// Created By          :   Salil Gupta
        /// Created On          :   02/13/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<ReceiptFormatDto> ReceiptListByFilter(FilterSearchParametersDto filterSearch, int logOnByUserId, int logOnByUserType)
        {
            try
            {
                var filterSearchClass = filterSearch.SearchParameters();
                IEnumerable<ReceiptFormatDto> receipt = _receiptRepository.ReceiptListByFilter(filterSearchClass,logOnByUserId,logOnByUserType).ToDTOList();
                return receipt;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Receipt To Show
        /// Function Name       :   ReceiptToShowByfleetId
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/10/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public IEnumerable<ReceiptListDto> ReceiptToShowByfleetId(int fleetId)
        {
            IEnumerable<ReceiptList> receipt = _receiptRepository.ReceiptToShowByfleetId(fleetId);
            IEnumerable<ReceiptListDto> rDto = receipt.ToDTODefaultList();
            return rDto;
        }

        /// <summary>
        /// Purpose             :   To get Receipt
        /// Function Name       :   ReceiptByfleetId
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/10/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public IEnumerable<ReceiptListDto> ReceiptByfleetId(int fleetId)
        {
            IEnumerable<ReceiptList> receipt = _receiptRepository.ReceiptByfleetId(fleetId);
            IEnumerable<ReceiptListDto> rDto = receipt.ToDTODefaultList();
            return rDto;
        }

        /// <summary>
        /// Purpose             :   To Get Fleet List
        /// Function Name       :   GetFleetList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/15/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<FleetDto> GetFleetList(int merchantId)
        {
            try
            {
                IEnumerable<Fleet> fleetlist = _receiptRepository.GetFleetList(merchantId);
                var fleetDtoList = fleetlist.ToDTOList();
                return fleetDtoList;
            }
            catch
            {
                throw;
            }
        }

    }
}
