--PAY-12 Adding Customer card AVS details
-----------------------------------------
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'CardToken' AND COLUMN_NAME = 'Unit')
BEGIN
	ALTER TABLE CardToken ADD 
		Unit NVARCHAR (150) NULL,
		StreetNumber NVARCHAR (150) NULL,
		Street NVARCHAR (150) NULL,
		AddressLine1 NVARCHAR (150) NULL,
		AddressLine2 NVARCHAR (150) NULL,
		Suburb NVARCHAR (150) NULL,
		State NVARCHAR (150) NULL,
		Postcode NVARCHAR (50) NULL

	PRINT 'CardToken table added AVS columns'
END
ELSE
BEGIN
	PRINT 'CardToken table AVS columns already exists'
END
