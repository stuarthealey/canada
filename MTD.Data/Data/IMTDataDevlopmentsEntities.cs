﻿using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;

namespace MTD.Data.Data
{
    public interface IMtDataDevlopmentsEntities
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        int CommitChanges();

        DbSet<Driver> Drivers { get; set; }
        DbSet<Fleet> Fleets { get; set; }
        DbSet<Gateway> Gateways { get; set; }
        DbSet<Gateway_Currency> Gateway_Currency { get; set; }
        DbSet<LogConfiguration> LogConfigurations { get; set; }
        DbSet<Merchant> Merchants { get; set; }
        DbSet<Merchant_Currency> Merchant_Currency { get; set; }
        DbSet<Merchant_Gateway> Merchant_Gateway { get; set; }
        DbSet<Receipt> Receipts { get; set; }
        DbSet<Role> Roles { get; set; }
        DbSet<Surcharge> Surcharges { get; set; }
        DbSet<Terminal> Terminals { get; set; }
        DbSet<UserRole> UserRoles { get; set; }
        DbSet<User> Users { get; set; }
        DbSet<UserType> UserTypes { get; set; }
        DbSet<Vehicle> Vehicles { get; set; }
        DbSet<ApplicationLog> ApplicationLogs { get; set; }
        DbSet<UserFleet> UserFleets { get; set; }
        DbSet<CountryCode> CountryCodes { get; set; }
        DbSet<CountryState> CountryStates { get; set; }
        DbSet<StateCity> StateCities { get; set; }
        DbSet<Currency> Currencies { get; set; }
        DbSet<DefaultSetting> DefaultSettings { get; set; }
        DbSet<CCPrefix> CCPrefixes { get; set; }
        DbSet<FleetReceipt> FleetReceipts { get; set; }
        DbSet<ReceiptMaster> ReceiptMasters { get; set; }
        DbSet<Recipient> Recipients { get; set; }
        DbSet<ScheduleReport> ScheduleReports { get; set; }
        DbSet<FDMerchant> FDMerchants { get; set; }
        DbSet<Datawire> Datawires { get; set; }
        DbSet<PaymentTerminal> PaymentTerminals { get; set; }
        DbSet<DriverLogin> DriverLogins { get; set; }
        DbSet<Report> Reports { get; set; }
        DbSet<MTDTransaction> MTDTransactions { get; set; }
        DbSet<CardToken> CardTokens { get; set; }
        DbSet<DriverReportRecipient> DriverReportRecipients { get; set; }
        DbSet<DriverTransactionHistory> DriverTransactionHistories { get; set; }
        DbSet<CarOwner> CarOwners { get; set; }
        DbSet<UserMerchant> UserMerchants { get; set; }
        DbSet<ExternalUser> ExternalUsers { get; set; }
        DbSet<CapKey> CapKeys { get; set; }
        DbSet<UKMtdtxnSattlement> UKMtdtxnSattlements { get; set; }
        DbSet<UKMTDTransaction> UKMTDTransactions { get; set; }
        DbSet<TransactionCountSlab> TransactionCountSlabs { get; set; }
        DbSet<ServiceDiscoveryProvider> ServiceDiscoveryProviders { get; set; }

        int usp_DeviceLogin(string serialNumber);
        int usp_GetUsersCredentials(Nullable<int> userId);
        ObjectResult<Nullable<int>> usp_DriverShiftReport(Nullable<int> driverId, Nullable<bool> isDispatchRequest);
        ObjectResult<usp_PreAuthInsertResponse_Result> usp_PreAuthInsertResponse(string xMLdata);
        ObjectResult<usp_GetDriversTransaction_Result> usp_GetDriversTransaction(Nullable<int> merchantId, Nullable<int> fleetId, string vehicleNumber, string driverNumber, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo);
        int usp_Schedule(Nullable<int> schedule_id, string name, string new_name, Nullable<byte> enabled, Nullable<int> freq_type, Nullable<int> freq_interval, Nullable<int> freq_subday_type, Nullable<int> freq_subday_interval, Nullable<int> freq_relative_interval, Nullable<int> freq_recurrence_factor, Nullable<int> active_start_date, Nullable<int> active_end_date, Nullable<int> active_start_time, Nullable<int> active_end_time, string owner_login_name, Nullable<bool> automatic_post);
        ObjectResult<usp_GetDrivers_Result> usp_GetDrivers(int? merchantId);
        ObjectResult<usp_GetVehicles_Result> usp_GetVehicles(int? merchantId);
        ObjectResult<usp_SearchTransactionReport_Result> usp_SearchTransactionReport(Nullable<int> transId, string vehicleId, string driverId, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, string cardType, string entryMode, string authorizeCode, string paymentType, string transType, Nullable<int> fk_MerchantID, string expDate, Nullable<decimal> minAmount, Nullable<decimal> maxAmount, string firstFour, string lastFour, string approval, string JobNumber, string fk_fleetId, Nullable<bool> isVarified, string rCTerminalId, string serialNo);
        ObjectResult<Nullable<int>> usp_Validation(string deviceId, string driverNo, string token);
        ObjectResult<usp_FirstDataDetails_Result> usp_FirstDataDetails(string deviceId, string driverNo, string token, int? fleetId, bool? requestType, bool? includeDatawire);     //T#6488
        ObjectResult<Nullable<int>> usp_OnDemandDriverReport(Nullable<int> driverId, Nullable<int> reportType, Nullable<System.DateTime> reportDate);
        ObjectResult<usp_GetLoginData_Result> usp_GetLoginData(string deviceId, string driverId, Nullable<int> userType);
        ObjectResult<Nullable<bool>> usp_ValidateEmail(string macAddress, string driverNo, string token, Nullable<int> fleetId);
        ObjectResult<usp_TechnologyDetailReport_Result> usp_TechnologyDetailReport(Nullable<int> merchantId, Nullable<int> fleetId, string vehicleNo, string driverNo, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, string cardType);
        ObjectResult<usp_TechnologySummaryReport_Result> usp_TechnologySummaryReport(Nullable<int> merchantId, Nullable<int> fleetId, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo);
        ObjectResult<usp_GetPayrollPaymentTransaction_Result> usp_GetPayrollPaymentTransaction(Nullable<int> merchantId, Nullable<int> fleetId, string payerType, string driverNumber, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> networkType);
        ObjectResult<usp_GetPayrollSAGETransaction_Result> usp_GetPayrollSAGETransaction(Nullable<int> merchantId, Nullable<int> fleetId, string payerType, string driverNumber, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> networkType);
        ObjectResult<usp_GetTransactionData_Result> usp_GetTransactionData(Nullable<int> fk_MerchantID, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, string transType, string downloadStatus, string vehicleId, string driverId, Nullable<int> fk_fleetId, string entryMode, int? batchNumber, bool? retrieveSinceBatch);

        //On Demand Report Section
        ObjectResult<usp_GetAdminSummaryReport_Result> usp_GetAdminSummaryReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo);
        ObjectResult<usp_GetCardTypeReport_Result> usp_GetCardTypeReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo);
        ObjectResult<usp_GetAdminExceptionReport_Result> usp_GetAdminExceptionReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo);
        ObjectResult<usp_GetTotalTransByVehicleReport_Result> usp_GetTotalTransByVehicleReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo);

        ObjectResult<usp_GetDetailReport_Result> usp_GetDetailReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantId);
        ObjectResult<usp_GetMerchantSummaryReport_Result> usp_GetMerchantSummaryReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantId);
        ObjectResult<usp_GetMerchantCardTypeReport_Result> usp_GetMerchantCardTypeReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantId);

        ObjectResult<usp_GetMerchantExceptionReport_Result> usp_GetMerchantExceptionReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantId);
        ObjectResult<usp_GetMerchantTotalTransByVehicleReport_Result> usp_GetMerchantTotalTransByVehicleReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantId);

        //On Demand Run Section admin
        int usp_AdminExceptionReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate);
        int usp_TotalTransByVehicleScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate);
        ObjectResult<int?> usp_CardTypeReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate);
        ObjectResult<int?> usp_AdminSummaryReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate);

        //On Demand Run Section Merchant
        int usp_MerchantSummaryReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, int? merchantId);
        int usp_MerchantTotalTransByVehicleScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, int? merchantId);
        int usp_MerchantExceptionReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate);
        int usp_DetailReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, int? merchantId);
        int usp_MerchantCardTypeReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, int? merchantId);

        //On Demand Merchnat Users section
        ObjectResult<usp_GetMerchantUserCardTypeReport_Result> usp_GetMerchantUserCardTypeReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantId, Nullable<int> merchantUserId);
        ObjectResult<usp_GetMerchantUserDetailReport_Result> usp_GetMerchantUserDetailReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantId, Nullable<int> merchantUserId);
        ObjectResult<usp_GetMerchantUserSummaryReport_Result> usp_GetMerchantUserSummaryReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantId, Nullable<int> merchantUserId);
        ObjectResult<usp_GetMerchantUserTotalTransByVehicleReport_Result> usp_GetMerchantUserTotalTransByVehicleReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantId, Nullable<int> merchantUserId);
        ObjectResult<usp_GetMerchantUserExceptionReport_Result> usp_GetMerchantUserExceptionReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantUserId);

        //On Demand Run Section Merchant User
        int usp_MerchantUserCardTypeReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> parentMerchantId, Nullable<int> merchantUserId);
        int usp_MerchantUserDetailReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> parentMerchantId, Nullable<int> merchantUserId);
        int usp_MerchantUserTotalTransByVehicleScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> parentMerchantId, Nullable<int> merchantUserId);
        int usp_MerchantUserSummaryReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> parentMerchantId, Nullable<int> merchantUserId);
        int usp_MerchantUserExceptionReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> merchantUserId);

        //On Demand Corporate Users section
        ObjectResult<usp_CorpUserGetTotalTransByVehicleReport_Result> usp_CorpUserGetTotalTransByVehicleReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> corpUserUserId);
        ObjectResult<usp_GetCorpUserCardTypeReport_Result> usp_GetCorpUserCardTypeReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> corpUserId);
        ObjectResult<usp_GetCorpUserExceptionReport_Result> usp_GetCorpUserExceptionReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> corpUserId);

        //On Demand Corporate Users RUN section
        int usp_CorpUserTotalTransByVehicleScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> corpUserId);
        ObjectResult<Nullable<int>> usp_CorpUserCardTypeReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> corpUserId);
        int usp_CorpUserExceptionReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> corpUserId);
        ObjectResult<usp_GetAdminWeeklySummaryCount_Result> usp_GetAdminWeeklySummaryCount();
        ObjectResult<usp_GetAdminWeeklyDollarVolume_Result> usp_GetAdminWeeklyDollarVolume();
    }
}
