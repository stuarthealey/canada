﻿using System;

namespace MTD.Core.DataTransferObjects
{
    public class TestTransaction
    {
        public int TransId { get; set; }
        public int? FkVehicleId { get; set; }
        public int? FkDriverId { get; set; }
        public string GatewayReturnCode { get; set; }
        public string GatewayReturnMsg { get; set; }
        public string GatewayResult { get; set; }
        public DateTime? TransDate { get; set; }
        public int? TransReturnCode { get; set; }
        public int? TransComId { get; set; }
        public decimal? TransAmount { get; set; }
        public string TransType { get; set; }
        public int? TransRefId { get; set; }
        public string TransRequestId { get; set; }
        public string TransFName { get; set; }
        public string TransLName { get; set; }
        public string TransEmail { get; set; }
        public string TransPhone { get; set; }
        public string TransAddress { get; set; }
        public string TransCity { get; set; }
        public string TransState { get; set; }
        public string TransZip { get; set; }
        public string TransCountry { get; set; }
        public string TransCcNo { get; set; }
        public string TransCcType { get; set; }
        public int? TransExpiryMonth { get; set; }
        public int? TransExpiryYear { get; set; }
        public string TransGatewayTnxId { get; set; }
        public string TransAuthCode { get; set; }
        public string TransReturnMsg { get; set; }
        public string TransOrderId { get; set; }
        public string Currency { get; set; }
        public decimal? SurAmt { get; set; }
        public decimal? TaxAmt { get; set; }
    }
}