﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace MTData.WebAPI.Common
{
    /// <summary>
    /// 
    /// </summary>
    public class AESCipher
    {
        #region Definitions

        public enum AESKeySize
        {
            AES128 = 128,
            AES192 = 192,
            AES256 = 256
        }

        public struct AESResult
        {
            public string Base64CipherText { get; set; }
            public string Base64IV { get; set; }
        }

        #endregion


        #region Attributes

        private RijndaelManaged _AesProvider;

        private byte[] _Key;

        #endregion


        #region Accessors

        public int KeySize { get; private set; }

        public const int BlockSize = 128;

        #endregion


        #region Constructors

        public AESCipher(AESKeySize keySize, byte[] key)
        {
            KeySize = (int)keySize;

            int keySizeBytes = KeySize / 8;
            _Key = new byte[keySizeBytes];
            Array.Copy(key, _Key, Math.Min(key.Length, _Key.Length));

            _AesProvider = new RijndaelManaged
            {
                KeySize = KeySize,
                BlockSize = 128,
                FeedbackSize = 128,
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
            };
        }

        #endregion


        #region Public Methods

        public AESResult Encrypt(string plainText, byte[] iv = null)
        {
            AESResult aesResult = new AESResult();

            if (iv == null)
            {
                iv = new byte[16];

                Random random = new Random();
                random.NextBytes(iv);
            }

            aesResult.Base64IV = Convert.ToBase64String(iv);

            if (_AesProvider != null && plainText != null)
            {
                // Create an encryptor to perform the stream transform
                using (ICryptoTransform encryptor = _AesProvider.CreateEncryptor(_Key, iv))
                {
                    byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
                    byte[] cipherTextBytes = Encrypt(plainTextBytes, encryptor);

                    aesResult.Base64CipherText = Convert.ToBase64String(cipherTextBytes);
                }
            }

            return aesResult;
        }

        public string Decrypt(byte[] ctpherText, byte[] iv)
        {
            string plaintext = null;

            if (_AesProvider != null)
            {
                // Create a decrytor to perform the stream transform
                using (ICryptoTransform decryptor = _AesProvider.CreateDecryptor(_Key, iv))
                {
                    byte[] plainTextBytes = Decrypt(ctpherText, decryptor);
                    plaintext = Encoding.UTF8.GetString(plainTextBytes);
                }
            }

            return plaintext;
        }

        public string Decrypt(AESResult aesResult)
        {
            byte[] cipherText = Convert.FromBase64String(aesResult.Base64CipherText);
            byte[] iv = Convert.FromBase64String(aesResult.Base64IV);

            return Decrypt(cipherText, iv);
        }

        #endregion


        #region Private Methods

        private byte[] Encrypt(byte[] plainText, ICryptoTransform encryptor)
        {
            byte[] cipherText;

            // Create streams used for encryption
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    // Encrypt plaintext
                    cryptoStream.Write(plainText, 0, plainText.Length);

                    // Finish encrypting
                    cryptoStream.FlushFinalBlock();

                    // Convert encrypted data from a memory stream into a byte array
                    cipherText = memoryStream.ToArray();
                }
            }

            return cipherText;
        }

        private byte[] Decrypt(byte[] cipherText, ICryptoTransform decryptor)
        {
            byte[] plainText;

            // Create streams used for decryption
            using (MemoryStream memoryStream = new MemoryStream(cipherText))
            {
                using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                {
                    // plainTextBuffer includes the plainText plus any padding
                    byte[] plainTextBuffer = new byte[cipherText.Length];

                    int plainTextLength = cryptoStream.Read(plainTextBuffer, 0, cipherText.Length);
                    plainText = new byte[plainTextLength];
                    Array.Copy(plainTextBuffer, 0, plainText, 0, plainTextLength);
                }
            }

            return plainText;
        }

        #endregion
    }
}

