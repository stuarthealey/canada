﻿using System;
using System.Text;
using System.Web.Http;
using Ninject;

using MTD.Core.Service.Interface;
using MTData.Utility;

namespace MTData.WebAPI.Controllers
{
    public class BaseController : ApiController
    {
        private static ITerminalService _terminalService;
        private StringBuilder _logMessage;
        static ILogger _logger = new Logger();

        public BaseController([Named("TerminalService")] ITerminalService terminalService, StringBuilder logMessage)
        {
            _terminalService = terminalService;
            _logMessage = logMessage;
        }

        /// <summary>
        /// Purpose             :   Get System trace number
        /// Function Name       :   GetSystemTraceNumber
        /// Created By          :   Umesh
        /// Created On          :   03/27/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="mId"></param>
        /// <param name="tId"></param>
        /// <returns></returns>
        public static string GetSystemTraceNumber(int mId, string tId)
        {
            string systemTrace;
            try
            {
                var terminalStan = _terminalService.GetTerminalStanRef(mId, tId);
                int straceNumber = 0;
                straceNumber = Convert.ToInt32(terminalStan.Stan) + 1;

                if (Convert.ToInt32(terminalStan.Stan) > 999999)
                    straceNumber = 1;

                systemTrace = straceNumber.ToString().PadLeft(6, '0');

                return systemTrace;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   Get transaction ReferenceNumber
        /// Function Name       :   GetReferenceNumber
        /// Created By          :   Umesh
        /// Created On          :   03/27/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY"  
        /// </summary>
        /// <param name="mId"></param>
        /// <param name="tId"></param>
        /// <returns></returns>
        public string GetReferenceNumber(int mId, string tId)
        {
            string referenceNumber;
            try
            {
                var terminalRef = _terminalService.GetTerminalStanRef(mId, tId);
                int timeDiff = Convert.ToInt32(DateTime.Now.Subtract(Convert.ToDateTime(terminalRef.TransactionDate)).ToString("hh"));
                referenceNumber = timeDiff <= 24 ? Convert.ToString(Convert.ToInt32(terminalRef.RefNumber) + 1) : terminalRef.RefNumber;

                return referenceNumber.PadLeft(12, '0');
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                throw;
            }
        }

    }
}
