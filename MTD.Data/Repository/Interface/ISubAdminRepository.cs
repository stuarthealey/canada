﻿using System.Collections.Generic;

using MTD.Data.Data;

namespace MTD.Data.Repository.Interface
{
    public interface ISubAdminRepository
    {
        IEnumerable<User> GetMerchantByFilter(PageSizeSearch pageSizeSearch);

        bool Remove(int subAdminId);

        IEnumerable<User> SubAdminList(PageSizeSearch pageSizeSearch);

        IEnumerable<MerchantUser> MyMerchantList(string name, int userId);

        IEnumerable<MerchantUser> GetMyMerchantByFilter(PageSizeSearch pageSizeSearch);

        IEnumerable<AllMerchants> MerchantList();

        IEnumerable<Fleet> GetFleet(string merchantListId, int userType, int userId);
        
        bool Add(User user);
        
        IEnumerable<AllMerchants> GetUserMerchant(int userId);
        
        string Update(User user, int userId);
        
        User GetUser(int userId);

        IEnumerable<UserFleet> GetUsersFleet(int userId);

        IEnumerable<usp_SearchTransactionReport_Result> GetTransaction(MTDTransaction transaction);

        int GetTransactionCount(MTDTransaction transaction);

        IEnumerable<usp_GetDriversTransaction_Result> GetDriver(MTDTransaction transaction);

        int GetDriverCount(MTDTransaction transaction);

        IEnumerable<usp_TechnologySummaryReport_Result> GetTechnologySummary(MTDTransaction tDto);

        int GetTechFeeCount(MTDTransaction tvd);

        int GetTechFeeDetailCount(MTDTransaction tvd);

        IEnumerable<usp_TechnologyDetailReport_Result> GetTechnologyDetail(MTDTransaction tvd);

        IEnumerable<DriverReportRecipient> GetUserDriverReceipent(string fleelistId);

        IEnumerable<AllMerchants> GetCorpUserMerchant(int userId);

    }
}
