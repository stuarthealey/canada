
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FirstDataDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FirstDataDetails]
GO

/****** Object:  StoredProcedure [dbo].[usp_FirstDataDetails]    Script Date: 11/2/2015 10:59:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[usp_FirstDataDetails]
(
	@deviceId varchar(100)=null,
	@driverNumber varchar(100)=null,
	@token varchar(max)=null,
	@fleetId INT,
	@isDispatchRequest BIT
)
as
begin
if exists (select 1 from DriverLogin where DriverNo = @driverNumber and FleetId=@fleetId and token=@token and IsDispatchRequest = @isDispatchRequest)
	begin
		select tmnl.TerminalID, tmnl.SerialNo, tmnl.DeviceName, tmnl.MacAdd, tmnl.fk_MerchantID, tmnl.IsTerminalAssigned, tmnl.DatawireId, 
		fdMer.FDId,fdMer.FDMerchantID, fdMer.GroupId, fdMer.serviceId, fdMer.ProjectId, fdMer.App, fdMer.IsAssigned,fdMer.TokenType,fdMer.EncryptionKey,fdMer.MCCode,
		dwr.ID, dwr.DID, dwr.DatewireXml, dwr.MerchantId, dwr.RCTerminalId, dwr.Stan, dwr.RefNumber 
		from Terminal tmnl
		inner join Merchant mer on mer.MerchantID=tmnl.fk_MerchantID
		inner join FDMerchant fdMer on fdMer.FDId=mer.Fk_FDId
		inner join Datawire dwr on dwr.ID= tmnl.DatawireId
		WHERE		
		tmnl.MacAdd=@deviceId and 
		tmnl.IsActive=1 
		and mer.IsActive=1
	end
else if (@token IS NULL)
	begin
	select tmnl.TerminalID, tmnl.SerialNo, tmnl.DeviceName, tmnl.MacAdd, tmnl.fk_MerchantID, tmnl.IsTerminalAssigned, tmnl.DatawireId, 
		fdMer.FDId,fdMer.FDMerchantID, fdMer.GroupId, fdMer.serviceId, fdMer.ProjectId, fdMer.App, fdMer.IsAssigned,fdMer.TokenType,fdMer.EncryptionKey,fdMer.MCCode,
		dwr.ID, dwr.DID, dwr.DatewireXml, dwr.MerchantId, dwr.RCTerminalId, dwr.Stan, dwr.RefNumber 
		from Terminal tmnl
		inner join Merchant mer on mer.MerchantID=tmnl.fk_MerchantID
		inner join FDMerchant fdMer on fdMer.FDId=mer.Fk_FDId
		inner join Datawire dwr on dwr.ID= tmnl.DatawireId
		WHERE		
		tmnl.MacAdd=@deviceId and 
		tmnl.IsActive=1 
		and mer.IsActive=1
	end
else
	begin
	-- To select a blank record if no criteria matches
	select tmnl.TerminalID, tmnl.SerialNo, tmnl.DeviceName, tmnl.MacAdd, tmnl.fk_MerchantID, tmnl.IsTerminalAssigned, tmnl.DatawireId, 
		fdMer.FDId,fdMer.FDMerchantID, fdMer.GroupId, fdMer.serviceId, fdMer.ProjectId, fdMer.App, fdMer.IsAssigned,fdMer.TokenType,fdMer.EncryptionKey,fdMer.MCCode,
		dwr.ID, dwr.DID, dwr.DatewireXml, dwr.MerchantId, dwr.RCTerminalId, dwr.Stan, dwr.RefNumber 
		from Terminal tmnl
		inner join Merchant mer on mer.MerchantID=tmnl.fk_MerchantID
		inner join FDMerchant fdMer on fdMer.FDId=mer.Fk_FDId
		inner join Datawire dwr on dwr.ID= tmnl.DatawireId
		WHERE		
		tmnl.MacAdd='-420' and 
		tmnl.IsActive=1 
		and mer.IsActive=1
	end
end

--select TerminalID, SerialNo, DeviceName, MacAdd, fk_MerchantID, IsTerminalAssigned, DatawireId from Terminal where MacAdd=@deviceId and IsActive=1
--select FDId, FDMerchantID, GroupId, serviceId, ProjectId, App, IsAssigned from FDMerchant as fd join Merchant as m on fd.FDId=m.Fk_FDId where MerchantID=@merchantId
--select ID, DID, DatewireXml, MerchantId, RCTerminalId, Stan, RefNumber from  Datawire where MerchantId = @merchantId and ID = @id and IsActive=1 and IsAssigned=1
--exec usp_FirstDataDetails '7c:F9:0E:1F:89:AD', '501','miTOGEoyWCSr5VewggGGb6Pa5AAmSQbW2IVJ98tS7RM=^NTAx:MTIzNDU2'

--exec usp_FirstDataDetails '08:08:C2:11:5E:D03', '123','wzxnlOCFzjQmD+oXHNFAxv0IdqzG5Y6roIvcJmuQIsY=^MTIz:MTIzNDU2'

--exec usp_FirstDataDetails '08:08:C2:11:5E:D03', '123',null




GO


