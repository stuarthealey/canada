﻿using System;
using System.Collections.Generic;

using MTD.Core.DataTransferObjects;

namespace MTD.Core.Service.Interface
{
    public interface ITransactionService
    {
        void Update(MTDTransactionDto transaction, int transId, string rapidConnectAuthId);
        MTDTransactionDto GetTransaction(int transId);
        MTDTransactionDto GetTransaction(int userType, int uId, int mId, int transId, string project);
        MTDTransactionDto GetTransaction(int transId, int? mid);
        IEnumerable<MTDTransactionDto> GetTransaction(MTDTransactionDto tDto);
        int GetTransactionCount(MTDTransactionDto tDto);
        int Create(MTDTransactionDto transactionDto);
        IEnumerable<DriverTransactionResult> GetDriver(MTDTransactionDto tDto);
        IEnumerable<TechnologySummaryReportDto> GetTechnologySummary(MTDTransactionDto tDto);
        IEnumerable<TechnologyDetailReportDto> GetTechnologyDetail(MTDTransactionDto tDto);
        int GetDriverCount(MTDTransactionDto tDto);
        int GetTechFeeCount(MTDTransactionDto tDto);
        int GetTechFeeDetailCount(MTDTransactionDto tDto);
        bool DriverVhecileExist(string driverNo, string vehicleNo, int merchantId);
        int GetMerchantId(string driverNo, string vehicleNo);
        int Add(MTDTransactionDto transactionDto);
        void Update(MTDTransactionDto transaction);
        TxnTaxSurchargesDto GetTaxSurchages(int merchantId);
        string GetDriverDevice(string vehicleNumber, int fleetId);
        int ManageCardToken(CardTokenDto cardTokenDto);
        TxnTaxSurchargesDto GetTaxSurchages(int merchantId, int vehicleNumber);
        TxnTaxSurchargesDto GetTaxSurchages();
        IEnumerable<DriverTxnListDto> GetDrivers(int merchantId);
        IEnumerable<VehicleTxnListDto> GetVehicle(int merchantId);
		//POd - CardToken is not unique.. needs customerid
        //string GetCardTypeByToken(string cardToken);
        IEnumerable<PayrollRportSageResult> GetpayrollSage(MTDTransactionDto tDto);
        int GetPayrollSageCount(MTDTransactionDto tDto);
        IEnumerable<PayrollRportPaymentResult> GetpayrollPayment(MTDTransactionDto tDto);
        int GetPayrollPaymentCount(MTDTransactionDto tDto);
        bool Varified(int txnId, bool isVerified);
        bool VarifiedMany(ManyTxnClassDto[] ManyTxnVerified);
        FleetDto GetFleetTechFee(int fleetId);
        IEnumerable<DownloadDriverDataDto> DownloadTransactionData(DownloadParametersDto dParam);
        int ValidateDownloadRequest(string uId, string pCode, int mCode);
        bool JobNumberRequired(int merchantId);
        CardTokenDto GetCardToken(string customerId, string cardToken);
        //CardTokenDto GetCardToken(string cardToken);
        int SaveCapKey(CapKeyDto capK);
        CapKeyDto GetCapKey();
        void CnUpdate(MTDTransactionDto mtdTransDto, int transId);

        string GetTransactionUrl();

        int GetMerchantId(int fleetId);

        //On Demand Report
        IEnumerable<GetAdminSummaryReportDto> GetadminSummary(DateTime? dateFrom, DateTime? dateTo);
        IEnumerable<GetCardTypeReportDto> AdminCardypeReport(DateTime? dateFrom, DateTime? dateTo);
        IEnumerable<GetAdminExceptionReportDto> AdminExceptionReport(DateTime? dateFrom, DateTime? dateTo);
        IEnumerable<GetTotalTransByVehicleReportDto> TransByVehicleReport(DateTime? dateFrom, DateTime? dateTo);
        IEnumerable<GetMerchantSummaryReportDto> MerchantSummaryReport(DateTime? dateFrom, DateTime? dateTo, int mId);
        IEnumerable<GetDetailReportDto> GetDetailsReport(DateTime? dateFrom, DateTime? dateTo, int mId);
        IEnumerable<GetMerchantExceptionReportDto> GetMerchantExceptionReport(DateTime? dateFrom, DateTime? dateTo, int mId);
        IEnumerable<GetMerchantTotalTransByVehicleReportDto> GetMerchantTotalTransByVehicle(DateTime? dateFrom, DateTime? dateTo, int mId);
        IEnumerable<GetMerchantCardTypeReportDto> GetMerchantCardTypeReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int mId);
        IEnumerable<GetMerchantExceptionReportDto> GetMerchantUserExceptionReport(DateTime? dateFrom, DateTime? dateTo, int? mId);

        //On Demand Report Merchant User
        IEnumerable<GetCardTypeReportDto> GetMerchantUserCardTypeReport(DateTime? dateFrom, DateTime? dateTo, Nullable<int> merchantId, Nullable<int> merchantUserId);
        IEnumerable<GetDetailReportDto> GetMerchantUserDetailReport(DateTime? dateFrom, DateTime? dateTo, Nullable<int> merchantId, Nullable<int> merchantUserId);//
        IEnumerable<GetMerchantSummaryReportDto> GetMerchantUserSummaryReport(DateTime? dateFrom, DateTime? dateTo, Nullable<int> merchantId, Nullable<int> merchantUserId);
        IEnumerable<GetMerchantTotalTransByVehicleReportDto> GetMerchantUserTotalTransByVehicleReport(DateTime? dateFrom, DateTime? dateTo, Nullable<int> merchantId, Nullable<int> merchantUserId);

        //On Demand Report Corporate User
        IEnumerable<GetCardTypeReportDto> GetCorpUserCardTypeReport(DateTime? dateFrom, DateTime? dateTo, Nullable<int> corpUserId);
        IEnumerable<GetTotalTransByVehicleReportDto> CorpUserGetTotalTransByVehicleReport(DateTime? dateFrom, DateTime? dateTo, Nullable<int> corpUserId);
        IEnumerable<GetMerchantExceptionReportDto> GetCorporateExceptionReport(DateTime? dateFrom, DateTime? dateTo, int cUserId);

        //
        int AdminExceptionReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate);
        int TotalTransByVehicleScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate);
        int CardTypeReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate);
        int AdminSummaryReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate);

        //Run Report Merchant
        int MerchantSummaryReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, int? merchantId);
        int MerchantTotalTransByVehicleScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, int? merchantId);
        int MerchantExceptionReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate);
        int DetailReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, int? merchantId);
        int MerchantCardTypeReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, int? merchantId);

        //Run Report Merchant User
        int MerchantUserCardTypeReportScheduler(bool? isOnDemandReport, DateTime? fromDate, DateTime? toDate, int? parentMerchantId, int? merchantUserId);
        int MerchantUserDetailReportScheduler(bool? isOnDemandReport, DateTime? fromDate, DateTime? toDate, int? parentMerchantId, int? merchantUserId);
        int MerchantUserTotalTransByVehicleScheduler(bool? isOnDemandReport, DateTime? fromDate, DateTime? toDate, int? parentMerchantId, int? merchantUserId);
        int MerchantUserSummaryReportScheduler(bool? isOnDemandReport, DateTime? fromDate, DateTime? toDate, int? parentMerchantId, int? merchantUserId);//////
        int MerchantUserExceptionReportScheduler(bool? isOnDemandReport, DateTime? fromDate, DateTime? toDate, int? merchantUserId);

        int CorpUserTotalTransByVehicleScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> corpUserId);
        int CorpUserCardTypeReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> corpUserId);
        int CorpUserExceptionReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> corpUserId);
        string GetRefNumber();
        string GetFastestUrl(string failOverUrls, List<string> urlList);
        MTDTransactionDto GetTransaction(int? mid, string tid, string stan);

        string GetTransactionId(string requestId);
        bool IsMacVerificationFailed(string transId);
    }
}
