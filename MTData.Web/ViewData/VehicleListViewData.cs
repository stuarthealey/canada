﻿namespace MTData.Web.ViewData
{
    public class VehicleListViewData
    {
        public int VehicleID { get; set; }
        public string VehicleNumber { get; set; }
    }
}