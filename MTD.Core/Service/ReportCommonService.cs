﻿using System.Collections.Generic;
using System.Linq;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Core.Service
{
    public class ReportCommonService : IReportCommonService
    {
        private readonly IReportCommonRepository _reportcommonRepository;

        public ReportCommonService([Named("ReportCommonRepository")] IReportCommonRepository reportcommonRepository)
        {
            _reportcommonRepository = reportcommonRepository;
        }

        /// <summary>
        /// Purpose             :   To Get Merchants List
        /// Function Name       :   GetMerchantList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/22/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MerchantListDto> GetMerchantList()
        {
            try
            {
                IEnumerable<Merchant> merchantList = _reportcommonRepository.MerchantList();
                return merchantList.Select(merchant => new MerchantListDto
                {
                    Company = merchant.Company,
                    fk_MerchantID = merchant.MerchantID
                }).ToList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Fleet List
        /// Function Name       :   GetFleetList
        /// Created By          :   Salil Gupta Shafeeque
        /// Created On          :   02/22/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<FleetListDto> GetFleetList(int id)
        {
            try
            {
                IEnumerable<Fleet> fleetList = _reportcommonRepository.FleetList(id);
                var fleetListDto = new List<FleetListDto>();

                foreach (var fleet in fleetList)
                {
                    FleetListDto fleetDto = new FleetListDto();
                    fleetDto.FleetName = fleet.FleetName;
                    fleetDto.FleetID = fleet.FleetID;
                    fleetListDto.Add(fleetDto);
                }
                return fleetListDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Fleet List
        /// Function Name       :   GetFleetList
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   02/22/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<FleetListDto> GetFleetList()
        {
            try
            {
                IEnumerable<Fleet> fleetList = _reportcommonRepository.FleetList();
                List<FleetListDto> fleetListDto = new List<FleetListDto>();

                foreach (Fleet fleet in fleetList)
                {
                    FleetListDto fleetDto = new FleetListDto();
                    fleetDto.FleetName = fleet.FleetName;
                    fleetDto.FleetID = fleet.FleetID;
                    fleetListDto.Add(fleetDto);
                }
                return fleetListDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Vehicles List
        /// Function Name       :   GetVehicleList
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   02/22/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<VehicleListDto> GetVehicleList()
        {
            try
            {
                IEnumerable<Vehicle> vehicleList = _reportcommonRepository.VehicleList();
                List<VehicleListDto> vehiclelistDto = new List<VehicleListDto>();

                foreach (Vehicle vehicle in vehicleList)
                {
                    VehicleListDto vehicleDto = new VehicleListDto();
                    vehicleDto.VehicleNumber = vehicle.VehicleNumber;
                    vehicleDto.VehicleID = vehicle.VehicleID;
                    vehiclelistDto.Add(vehicleDto);
                }
                return vehiclelistDto;
            }

            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Vehicle List
        /// Function Name       :   GetVehicleList
        /// Created By          :   Naveen Kumar
        /// Created On          :   02/22/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<VehicleListDto> GetVehicleList(int id)
        {
            try
            {
                IEnumerable<Vehicle> vehicleList = _reportcommonRepository.VehicleList(id);
                List<VehicleListDto> vehiclelistDto = new List<VehicleListDto>();

                foreach (Vehicle vehicle in vehicleList)
                {
                    VehicleListDto vehicleDto = new VehicleListDto();
                    vehicleDto.VehicleNumber = vehicle.VehicleNumber;
                    vehicleDto.VehicleID = vehicle.VehicleID;
                    vehiclelistDto.Add(vehicleDto);
                }
                return vehiclelistDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Vehicles List
        /// Function Name       :   GetDriverList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/22/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="vehicleNumber"></param>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public IEnumerable<VehicleListDto> GetVehicleList(string vehicleNumber, int fleetId)
        {
            try
            {
                IEnumerable<Vehicle> vehicleList = _reportcommonRepository.VehicleList(vehicleNumber, fleetId);
                List<VehicleListDto> vehiclelistDto = new List<VehicleListDto>();

                foreach (Vehicle vehicle in vehicleList)
                {
                    VehicleListDto vehicleDto = new VehicleListDto();
                    vehicleDto.VehicleNumber = vehicle.VehicleNumber;
                    vehicleDto.VehicleID = vehicle.VehicleID;
                    vehiclelistDto.Add(vehicleDto);
                }
                return vehiclelistDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Drivers List
        /// Function Name       :   GetDriverList
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/24/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverNumber"></param>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public IEnumerable<DriverListDto> GetDriverList(string driverNumber, int fleetId)
        {
            try
            {
                IEnumerable<Driver> driverList = _reportcommonRepository.DriverList(driverNumber, fleetId);

                return driverList.Select(driver => new DriverListDto
                {
                    DriverNumber = driver.DriverNo,
                    DriverID = driver.DriverID
                }).ToList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Driversname  List
        /// Function Name       :   GetDriverNameList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   04/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public IEnumerable<DrivernamelistDto> GetDriverNameList(int fleetId)
        {
            try
            {
                IEnumerable<Driver> driverList = _reportcommonRepository.DriverNameList(fleetId);

                return driverList.Select(driver => new DrivernamelistDto
                {
                    FName = driver.FName,
                    DriverNo = driver.DriverNo
                }).ToList();
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<UserDto> MerchantUserList(int mId)
        {
            try
            {
                IEnumerable<User> merchantList = _reportcommonRepository.MerchantUserList(mId);
                return merchantList.Select(merchant => new UserDto
                {
                    Email = merchant.Email,
                    UserID = merchant.UserID
                }).ToList();
            }
            catch
            {
                throw;
            }
        }

    }
}
