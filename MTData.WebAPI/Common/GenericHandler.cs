﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MTData.WebAPI.ISO8583;
 

namespace MTData.WebAPI.Common
{
    public static class GenericHandler
    {
        #region Get and Set Card Issuer on the basis of Card number
        /// <summary>
        /// Get the Issuer of Card on the basis of Card Number
        /// </summary>
        /// <param name="objTransactionEntities"> Object of Transaction entities</param>
        /// <returns>CardIssuer</returns>
        public static CardIssuer GetCardIssuer(Iso8583TransactionEntities objTransactionEntities)
        {
            CardIssuer result = CardIssuer.None;
            var cardNumber = string.Empty;

            //To get Account number if exist in Primary account number
            if (!String.IsNullOrEmpty(objTransactionEntities.PrimaryAccountNumber))
                cardNumber = objTransactionEntities.PrimaryAccountNumber.Trim();
            //To get Account number if exist in Track 2 Data
            else if (!String.IsNullOrEmpty(objTransactionEntities.Track2Data))
                cardNumber = objTransactionEntities.Track2Data.Trim();
            //To get Account number if exist in Track 1 Data
            else if (!String.IsNullOrEmpty(objTransactionEntities.Track1Data))
                cardNumber = objTransactionEntities.Track1Data.Trim().Substring(1);

            //Set card issuer on the basis of Initial Digits of Card Number(also Known as Account Number)
            if (cardNumber.StartsWith("4"))
                result = CardIssuer.Visa;
            else if (cardNumber.StartsWith("5"))
                result = CardIssuer.MC;
            else if (cardNumber.StartsWith("37") || cardNumber.StartsWith("34"))
                result = CardIssuer.Amex;
            else
            {
                int initDigit = Convert.ToInt32(cardNumber.Substring(0, 8));
                if ((initDigit >= 30000000 && initDigit <= 30599999) ||
                    (initDigit >= 30950000 && initDigit <= 30959999) ||
                    (initDigit >= 36000000 && initDigit <= 36999999) ||
                    (initDigit >= 38000000 && initDigit <= 39999999) ||
                    (initDigit >= 35280000 && initDigit <= 35899999) ||
                    (initDigit >= 60110000 && initDigit <= 60110399) ||
                    (initDigit >= 60110500 && initDigit <= 60110999) ||
                    (initDigit >= 60112000 && initDigit <= 60114999) ||
                    (initDigit >= 60117400 && initDigit <= 60117499) ||
                    (initDigit >= 60117700 && initDigit <= 60117999) ||
                    (initDigit >= 60118600 && initDigit <= 60119999) ||
                    (initDigit >= 62212600 && initDigit <= 62292599) ||
                    (initDigit >= 62400000 && initDigit <= 62699999) ||
                    (initDigit >= 62820000 && initDigit <= 62889999) ||
                    (initDigit >= 64400000 && initDigit <= 65059999) ||
                    (initDigit >= 65061100 && initDigit <= 65999999))
                    result = CardIssuer.DCI;

                //AMS -Changes for ATH type Date - Jan 14, 2014
                if (initDigit.Equals(02150201))
                {
                    result = CardIssuer.ATH;
                }
            }

            return result;
        }

        /// <summary>
        ///  Get the Issuer of Card on the basis of Card Number
        /// </summary>
        /// <param name="strCardData">Includes card number or track2</param>
        /// <returns></returns>
        public static string GetCardIssuer(string strCardData)
        {
            var result = string.Empty;
            try
            {
                #region Try Block

                #region Check for Empty card Data and return empty string.
                if (string.IsNullOrEmpty(strCardData))
                    return result;
                #endregion

                #region Set Card Number
                var cardNumber = string.Empty;
                if (strCardData.Contains("="))
                    //To get Account number if exist in Track 2 Data
                    cardNumber = strCardData.Split('=')[0];
                else
                    //To get Account number if exist in Primary account number
                    cardNumber = strCardData;
                #endregion

                #region Check the Length of Card Number
                if (cardNumber.Length < 8)
                    return result;
                #endregion

                #region Get the Initial Digit of Card Number to indentify Card Issuer
                int initDigit = Convert.ToInt32(cardNumber.Substring(0, 8));
                #endregion

                #region Set Card issuer on the basis of Card initials
                //Set card issuer on the basis of Initial Digits of Card Number(also Known as Account Number)
                if (cardNumber.StartsWith("4"))
                    result = AllCardIssuers.VI.ToString();
                else if (cardNumber.StartsWith("5"))
                    result = AllCardIssuers.MC.ToString();
                else if (cardNumber.StartsWith("37") || cardNumber.StartsWith("34"))
                    result = AllCardIssuers.AX.ToString();
                else if (initDigit >= 35280000 && initDigit <= 35899999)
                    result = AllCardIssuers.JCB.ToString();
                else if ((initDigit >= 30000000 && initDigit <= 30599999) ||
                        (initDigit >= 30950000 && initDigit <= 30959999) ||
                        (initDigit >= 36000000 && initDigit <= 36999999) ||
                        (initDigit >= 38000000 && initDigit <= 39999999))
                    result = AllCardIssuers.DN.ToString();
                else if ((initDigit >= 60110000 && initDigit <= 60110399) ||
                        (initDigit >= 60110500 && initDigit <= 60110999) ||
                        (initDigit >= 60112000 && initDigit <= 60114999) ||
                        (initDigit >= 60117400 && initDigit <= 60117499) ||
                        (initDigit >= 60117700 && initDigit <= 60117999) ||
                        (initDigit >= 60118600 && initDigit <= 60119999) ||
                        (initDigit >= 64400000 && initDigit <= 65059999) ||
                        (initDigit >= 65061100 && initDigit <= 65999999))
                    result = AllCardIssuers.DCI.ToString();
                else if ((initDigit >= 62212600 && initDigit <= 62292599) ||
                        (initDigit >= 62400000 && initDigit <= 62699999) ||
                        (initDigit >= 62820000 && initDigit <= 62889999))
                    result = AllCardIssuers.UP.ToString();

                    //AMS -Changes for ATH type Date - Jan 14, 2014
                else if (initDigit.Equals(02150201))
                    result = AllCardIssuers.ATH.ToString();
                else
                    result = AllCardIssuers.None.ToString();
                #endregion

                #endregion
            }
            catch
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Get the Issuer of Card on the basis of Card Number
        /// </summary>
        /// <param name="objTransactionEntities"> Object of Transaction entities</param>
        /// <returns>CardIssuer</returns>
        public static CardIssuer GetCardIssuer(TransactionRequestDetails objTransactionRequestDetails)
        {
            CardIssuer result = CardIssuer.None;
            var cardNumber = string.Empty;

            //To get Account number if exist in Card Number
            if (!String.IsNullOrEmpty(objTransactionRequestDetails.CardNumber))
                cardNumber = objTransactionRequestDetails.CardNumber.Trim();
            //To get Account number if exist in Track 2 Data
            else if (!String.IsNullOrEmpty(objTransactionRequestDetails.Track2Data))
                cardNumber = objTransactionRequestDetails.Track2Data.Trim();
            //To get Account number if exist in Track 1 Data
            else if (!String.IsNullOrEmpty(objTransactionRequestDetails.Track1Data))
                cardNumber = objTransactionRequestDetails.Track1Data.Trim().Substring(1);

            //Set card issuer on the basis of Initial Digits of Card Number(also Known as Account Number)
            if (cardNumber.StartsWith("4"))
                result = CardIssuer.Visa;
            else if (cardNumber.StartsWith("5"))
                result = CardIssuer.MC;
            else if (cardNumber.StartsWith("37") || cardNumber.StartsWith("34"))
                result = CardIssuer.Amex;
            else
            {
                int initDigit = Convert.ToInt32(cardNumber.Substring(0, 8));
                if ((initDigit >= 30000000 && initDigit <= 30599999) ||
                    (initDigit >= 30950000 && initDigit <= 30959999) ||
                    (initDigit >= 36000000 && initDigit <= 36999999) ||
                    (initDigit >= 38000000 && initDigit <= 39999999) ||
                    (initDigit >= 35280000 && initDigit <= 35899999) ||
                    (initDigit >= 60110000 && initDigit <= 60110399) ||
                    (initDigit >= 60110500 && initDigit <= 60110999) ||
                    (initDigit >= 60112000 && initDigit <= 60114999) ||
                    (initDigit >= 60117400 && initDigit <= 60117499) ||
                    (initDigit >= 60117700 && initDigit <= 60117999) ||
                    (initDigit >= 60118600 && initDigit <= 60119999) ||
                    (initDigit >= 62212600 && initDigit <= 62292599) ||
                    (initDigit >= 62400000 && initDigit <= 62699999) ||
                    (initDigit >= 62820000 && initDigit <= 62889999) ||
                    (initDigit >= 64400000 && initDigit <= 65059999) ||
                    (initDigit >= 65061100 && initDigit <= 65999999))
                    result = CardIssuer.DCI;

                //AMS -Changes for ATH type Date - Jan 14, 2014
                if (initDigit.Equals(02150201))
                {
                    result = CardIssuer.ATH;
                }
            }

            return result;
        }

        /// <summary>
        /// Get the merchant Auth with combination of Padded MID and TID
        /// </summary>
        /// <returns>Return The Padded Auth key for Merchant</returns>
        /// <CreatedBy>Mahatma Nishad</CreatedBy>
        public static string GetMerchantAuth(string merchantId,string terminalID)
        {
            string auth = string.Empty;
            string paddedTerminalID = terminalID.PadLeft(8, '0');
            string paddedMerchantID = merchantId.PadLeft(15, '0');
            auth = string.Format("{0}|{1}", paddedMerchantID, paddedTerminalID);
            return auth;
        }

        /// <summary>
        /// Get the Unique Client ref on with composite of 
        /// {7 digit Transaction no}V{3 digit major version with pad left}{3 digit revision no with pad right}
        /// it will return the 14 digit transaction unique string for each call
        /// </summary>
        /// <param name="appVersion">the app version need to parse with the required specification</param>
        /// <returns>Return the 14 digit transaction unique string for each call</returns>
        /// <CreatedBy>Mahatma Nishad</CreatedBy>
        public static string GetClientRef()
        {
            //var transactionNo = TransactionDataContext.GetReferenceNumber();
            var transactionNo = 00;
            var version=Global.Version.Split('.');
            var major=version[0].PadLeft(3,'0');
            var revision=version[1].PadRight(3,'0');
            return string.Format("{0}V{1}{2}", transactionNo, major, revision);
        }

        /// <summary>
        /// Generate the client reference number from reference number returned from database.
        /// </summary>
        /// <param name="systemTrace"></param>
        /// <returns>string</returns>
        /// <CreatedBy>Richi Bhanj</CreatedBy>
        public static string CreateClientRef(string referenceNumber)
        {
            var version = Global.Version.Split('.');
            var versionLeft = version[0].PadLeft(3, SigmaStaticValue.Zero);
            var versionRight = version[1].PadRight(3, SigmaStaticValue.Zero);
            return string.Format("{0}V{1}{2}", referenceNumber, versionLeft, versionRight);
        }

        #endregion

        #region Convert ASCII to Hex and Hex To ASCII
        public static string ConvertToHex(string data)
        {
            /* Convert text into an array of characters */
            StringBuilder outputstring = new StringBuilder();

            char[] char_array = data.ToCharArray();
            try
            {
                foreach (char letter in char_array)
                {
                    /* Get the integral value of the character */
                    var value = Convert.ToInt32(letter);

                    /* Convert the decimal value to a hexadecimal value in string form */
                    string hex = String.Format("{0:X}", value);

                    /* Append hexadecimal version of the char to the string outputstring*/
                    outputstring.Append(Convert.ToString(hex));
                }
                return outputstring.ToString();

            }
            catch (Exception)
            {
                throw;
            }

        }

        public static string ConvertHexToAscii(String hexString)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                for (int iCount = 0; iCount < hexString.Length; iCount += 2)
                {
                    string hs = hexString.Substring(iCount, 2);
                    sb.Append(Convert.ToChar(Convert.ToUInt32(hs, 16)));
                }
                return sb.ToString();
            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion
        /// <summary>
        /// pass the merchant industry
        /// </summary>
        /// <param name="merchantIndustry">source merchant industry string</param>
        public static IndustryType SetIndustryType(int? merchantIndustry)
        {
            IndustryType industryType = IndustryType.None;
            switch (merchantIndustry)
            {
                case 0:
                    industryType = IndustryType.None;
                    break;
                case 1:
                    industryType = IndustryType.EComm;
                    break;
                case 2:
                    industryType = IndustryType.Retail;
                    break;
            }
            return industryType;
        }
    }
}
