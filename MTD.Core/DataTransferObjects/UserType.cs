﻿using System.Collections.Generic;
using MTD.Data.Data;

namespace MTD.Core.DataTransferObjects
{
    public sealed class UserType
    {
        public UserType()
        {
            this.Users = new HashSet<User>();
        }

        public int UserTypeID { get; set; }
        public string UseType { get; set; }
        public string Description { get; set; }

        public ICollection<User> Users { get; set; }
    }
    
}