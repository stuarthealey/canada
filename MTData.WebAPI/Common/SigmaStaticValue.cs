﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Common
{
    public static class SigmaStaticValue
    {
        #region Declare constant for transaction message type
        /// <summary>
        /// Message Type for Credit Sale Request
        /// Value = "|01|00"
        /// </summary>
        public const string MessageType_ZeroOneZeroZero = "|01|00";
        /// <summary>
        ///Message Type for Credit Sale Response
        /// Value = "|01|10"
        /// </summary>
        public const string MessageType_ZeroOneOneZero = "|01|10";
        /// <summary>
        ///Message Type for Reversal Request
        /// Value = "|04|00"
        /// </summary>
        public const string MessageType_ZeroFourZeroZero = "|04|00";
        /// <summary>
        ///Message Type for Reversal Response
        /// Value = "|04|10"
        /// </summary>
        public const string MessageType_ZeroFourOneZero = "|04|10";
        /// <summary>
        ///Message Type for Debit Sale Request
        /// Value = "|02|00"
        /// </summary>
        public const string MessageType_ZeroTwoZeroZero = "|02|00";
        /// <summary>
        ///Message Type for Debit Sale Response
        /// Value = "|02|10"
        /// </summary>
        public const string MessageType_ZeroTwoOneZero = "|02|10";
        #endregion

        #region Merchant category code(MCC)
        /// <summary>
        /// Merchant Category Code for Retail. 
        /// Value="5999" 
        /// </summary>
        public const string MCCForRetail = "5999";

        /// <summary>
        /// Merchant Category Code for E-Com. 
        /// Value="5969" 
        /// </summary>
        public const string MCCForECom = "5969";

        /// <summary>
        /// Merchant Category Code for Direct Marketing. 
        /// Value="5968" 
        /// </summary>
        public const string MCCForDirectMarketing = "5968";

        /// <summary>
        /// Merchant Category Code for Restaurant. 
        /// Value="5812" 
        /// </summary>
        public const string MCCForRestaurant = "5812";
        #endregion

        #region Constant value for pos entry mode pin capability
        /// <summary>
        /// POS Entry Mode+ Pin Capability/Bitmap 22 for manual transaction.
        /// Value = "012" 
        /// "01":Manual/Key Entry and "2":Cannot accept PIN
        /// </summary>
        public const string POSEntryModePinCapabilityForManual = "012";

        /// <summary>
        /// POS Entry Mode+ Pin Capability/Bitmap 22 for manual transaction.
        /// Value = "012" 
        /// "01":Manual/Key Entry and "1":pin accepted.
        /// </summary>
        public const string POSEntryModePinCapabilityForManualRetail = "011";

        /// <summary>
        /// POS Entry Mode+ Pin Capability/Bitmap 22 for swiped transaction.
        /// Value = "902" 
        /// "90":Magnetic Stripe–CVV/CVC certified and "2":Cannot accept PIN
        /// </summary>
        public const string POSEntryModePinCapabilityForSwiped = "902";

        /// <summary>
        /// POS Entry Mode+ Pin Capability/Bitmap 22 for swiped transaction.
        /// Value = "901" 
        /// "90":Magnetic Stripe–CVV/CVC certified and "1":Can accept PIN
        /// </summary>
        public const string POSEntryModePinCapabilityForSwipedWithPin = "901";
        #endregion

        #region Declare constant for processing code
        /// <summary>
        /// Set the const value of processing code for Credit Sale, Credit Reversal.
        /// Value = "000000"
        /// </summary>
        public const string ProcessingCodeForCreditSale = "000000";

        /// <summary>
        /// Set the const value of processing code for credit Refund, Referral and Partial Reversal.
        /// Value = "200000"
        /// </summary>
        public const string ProcessingCodeForCreditRefund = "200000";

        /// <summary>
        /// Set the const value of processing code for debit reversal.
        /// Value = "009000"
        /// </summary>
        public const string ProcessingCodeForDebitReversal = "009000";

        /// <summary>
        /// Set the const value of processing code for debit sale pin-less
        /// Value="509000"
        /// </summary>
        public const string ProcessingCodeForPinlessDebitSale = "509000";

        /// <summary>
        /// Set the const value of processing code for debit cashBack.
        /// Value = "099000"
        /// </summary>
        public const string ProcessingCodeForDebitCashBack = "099000";

        /// <summary>
        /// Set the const value of processing code for debit refund.
        /// Value = "209000"
        /// </summary>
        public const string ProcessingCodeForDebitRefund = "209000";

        /// <summary>
        /// Set the const value of processing code for recurring bill payment.
        /// Value = "500000"
        /// </summary>
        public const string ProcessingCodeBillPaymentNRecurring = "500000";

        /// <summary>
        /// Set the const value of processing code for pin debit sale.
        /// Value = "009000"
        /// </summary>
        public const string ProcessingCodeForPinDebitSale = "009000";

        /// <summary>
        /// Set the const value of processing code for credit cash back
        /// Value = "009000"
        /// </summary>
        public const string ProcessingCodeForCreditCahBack = "090000";
        #endregion

        #region Constant value for Pos condition code
        /// <summary>
        /// Pos Condition Code/Bit 25 for Normal Presentment (i.e., customer is present with card).
        /// Value = "00" 
        /// </summary>
        public const string PosConditionCodeZeroZero = "00";

        /// <summary>
        /// Pos Condition Code/Bit 25 In case of Visa card AVS transaction.
        /// Value = "71"        
        /// </summary>
        public const string PosConditionCodeVisaAvs = "71";

        /// <summary>
        /// Pos Condition Code/Bit 25 For AVS check transaction 
        /// Value = "52"
        /// </summary>
        public const string PosConditionCodeAVSCheck = "52";

        /// <summary>
        /// Pos Condition Code/Bit 25 For CVV check transaction. 
        /// Value = "51"
        /// </summary>
        public const string PosConditionCodeCVV = "51";

        /// <summary>
        /// Pos Condition Code/Bit 25 For E-Com  transaction.
        /// Value = "59"
        /// </summary>
        public const string PosConditionCodeECom = "59";

        /// <summary>
        /// Pos Condition Code/Bit 25 For Recurring  transaction. 
        /// Value = "04"
        /// </summary>
        public const string PosConditionCodeRecurring = "04";

        /// <summary>
        /// Pos Condition Code/Bit 25 for Voice response Unit (VRU)/Telephone.
        /// Valid value for Debit Card PIN-Less Voice Response Unit or Telephone Transactions
        /// Value = "00" 
        /// </summary>
        public const string PosConditionCodeZeroSeven = "07";

        /// <summary>
        /// Pos Condition Code/Bit 25 For MailPhone order transaction. 
        /// Value = "08"
        /// </summary>
        public const string PosConditionCodeMailPhoneOrder = "08";
        #endregion

        #region Constant value for Table14 related data
        /// <summary>
        /// Get table 14 Id in Hex.
        /// </summary>
        public const string Table14IdInHex = "3134";

        /// <summary>
        /// Get table number 14.
        /// </summary>
        public const string TableNo14 = "14";
        #endregion

        /// <summary>
        /// Get the Value "00" for response code 00.
        /// </summary>
        public const string ResponseCodeZeroZero = "00";

        /// <summary>
        /// Get the Value "0" from Zero
        /// </summary>
        public const char Zero = '0';

        /// <summary>
        /// Get the Value "|" from pipe
        /// </summary>
        public const char Pipe = '|';

        /// <summary>
        /// Get the value "1C" from OneC.
        /// </summary>
        public const string OneC = "1C";

        /// <summary>
        /// Get the Value "=" from split.
        /// </summary>
        public const char SplitEquals = '=';

        /// <summary>
        /// Get the Value "|20" from PI20
        /// </summary>
        public const string PI20 = "|20";

        /// <summary>
        /// Get the Value "#" from Hash
        /// </summary>
        public const string Hash = "#";

        /// <summary>
        /// Get the Value "MMddHHmmss" from DateTimeFormatMMddHHmmss
        /// </summary>
        public const string DateTimeFormatMMddHHmmss = "MMddHHmmss";

        /// <summary>
        /// Get the Value "MMddHHmmss" from DateTimeFormatMMddHHmmss
        /// </summary>
        public const string DateTimeFormatMMdd = "MMdd";

        /// <summary>
        ///Get the Value "hhmmss" from TimeFormathhmmss.
        /// </summary>
        public const string TimeFormathhmmss = "hhmmss";

        /// <summary>
        /// Get the Value "01" for Aditional pos data.
        /// This is suggested by FirstData for Credit
        /// Sale/Full reversal and Pinless debit transactions.
        /// </summary>
        public const string AdditionalPossData = "01";

        /// <summary>
        /// Get the Value "42" for Aditional pos data.
        /// This is suggested by FirstData for Credit for Retail industry.
        /// </summary>
        public const string AdditionalPossDataForRetail = "42";

        /// <summary>
        /// Get the Value "001" for First Data NII excluding Canadian Debit and Credit.
        /// Get the Value "047" for Canadian processing Debit and Credit.
        /// </summary>
        public const string NetworkInternationId = "001";

        /// <summary>
        /// Get the value Dot(.).
        /// </summary>
        public const string Dot = ".";

        /// <summary>
        /// Get Single Space(" ").
        /// </summary>
        public const string SingleSpace = " ";

        /// <summary>
        /// key to get text for expiry date
        /// </summary>
        public const string CardExpiryDateForReversal = "0000";

        /// <summary>
        /// Debit reversal default pin.
        /// </summary>
        public const string DebitReversalDefaultPin = "0000000000000000";

        /// <summary>
        /// Get ClientReference.
        /// </summary>
        public const string ClientReference = "ClientReference";
        /// <summary>
        /// Get SystemTrace.
        /// </summary>
        public const string SystemTrace = "SystemTrace";
        /// <summary>
        /// Get TransactionCounter.
        /// </summary>
        public const string TransactionCounter = "TransactionCounter";
        /// <summary>
        /// Get response code text to create response code dynamically.
        /// </summary>
        public const string ResponseCode = "ResponseCode";
        /// <summary>
        /// Key to get unknown response code text.
        /// </summary>
        public const string UnknownResponseCode = "ResponseCodeUnknown";
        /// <summary>
        /// Key to get text for Merchant not registered.
        /// </summary>
        public const string MerchantNotRegistered = "MerchantNotRegistered";
        /// <summary>
        /// Ket to get test for invalid transaction Id.
        /// </summary>
        public const string InvalidTransactionId = "InvalidTransactionId";
        /// <summary>
        /// Key to get text for unable to process transaction.
        /// </summary>
        public const string TransactionProcessUnable = "TransactionProcessUnable";
        /// <summary>
        /// Key to get text for transaction voided.
        /// </summary>
        public const string TransactionVoided = "TransactionVoided";
        /// <summary>
        /// Key to get text for already voided transaction.
        /// </summary>
        public const string TransactionAlreadyVoided = "TransactionAlreadyVoided";
        /// <summary>
        /// Key to get text for refunded transaction.
        /// </summary>
        public const string TransactionRefunded = "TransactionRefunded";
        /// <summary>
        /// Key to get text for already refunded transaction.
        /// </summary>
        public const string TransactionAlreadyRefunded = "TransactionAlreadyRefunded";
        /// <summary>
        /// Key to get text for subsequent voided transaction.
        /// </summary>
        public const string SubsequentVoidedTransaction = "SubsequentVoidedTransaction";
        /// <summary>
        /// Key to get text for subsequent failed voided transaction.
        /// </summary>
        public const string SubsequentFailedVoidedTransaction = "SubsequentFailedVoidedTransaction";
        /// <summary>
        /// Key to get the message for client when any exception occurs in the application.
        /// </summary>
        public const string DatabaseConnectivityException = "DatabaseConnectivityException";
        /// <summary>
        /// Key to get the message for successfull referral transaction processed.
        /// </summary>
        public const string ReferralProcessed = "ReferralProcessed";
        /// <summary>
        /// Key to get the message for successfull referral transaction process updated.
        /// </summary>
        public const string ReferralAlreadyProcessed = "ReferralAlreadyProcessed";
        /// <summary>
        /// Key to get the message for successfully subsequent refunded transaction.
        /// </summary>
        public const string SubsequentRefundedTransaction = "SubsequentRefundedTransaction";

        public const string TransactionTimeout = "TransactionTimeout";

        public const string UnableToConnectNetwork = "UnableToConnectNetwork";

        public const string UnableToGenerateSettlementFile1 = "UnableToGenerateSettlementFile1";

        public const string UnableToGenerateSettlementFile2 = "UnableToGenerateSettlementFile2";

        public const string AmountPreviouslyVoidedOrRefunded = "AmountPreviouslyVoidedOrRefunded";

        public const string PartialVoidSuccessful = "PartialVoidSuccessful";

        public const string PartialRefundSuccessful = "PartialRefundSuccessful";

        public const string AmexNotSupportPartialVoid = "AmexNotSupportPartialVoid";

        public const string AmountMoreThanBalance = "AmountMoreThanBalance";

        public const string ReferralSettlementBatchClosed = "ReferralSettlementBatchClosed";

        public const string ReferralUnableToVoid = "ReferralUnableToVoid";

        public const string ReferralUnableToPartialVoid = "ReferralUnableToPartialVoid";

        public const string InvTransTypeAsConsideredInPTS = "InvTransTypeAsConsideredInPTS";

        public const string InvTransTypeAsNotConsideredInPTS = "InvTransTypeAsNotConsideredInPTS";

        public const string PartialReversalWithFullAmount = "PartialReversalWithFullAmount";

        public const string PartalRefundAmtMoreThanBalance = "PartalRefundAmtMoreThanBalance";

        #region Resource Key
        public const string INV_City = "INV_City";
        public const string INV_ContactNo = "INV_ContactNo";
        public const string INV_Country = "INV_Country";
        public const string INV_CustomerServiceNo = "INV_CustomerServiceNo";
        public const string INV_Email = "INV_Email";
        public const string INV_MCC = "INV_MCC";
        public const string INV_MerchantFN = "INV_MerchantFN";
        public const string INV_MerchantLN = "INV_MerchantLN";
        public const string INV_MID = "INV_MID";
        public const string INV_State = "INV_State";
        public const string INV_StreetLine1 = "INV_StreetLine1";
        public const string INV_StreetLine2 = "INV_StreetLine2";
        public const string INV_TID = "INV_TID";
        public const string INV_Website = "INV_Website";
        public const string INV_ZIP = "INV_ZIP";
        public const string MID_AL_Exits_Msg = "MID_AL_Exits_Msg";
        public const string MID_AL_Exits_ST = "MID_AL_Exits_ST";
        public const string MID_Sucess = "MID_Sucess";
        public const string NetworkError = "NetworkError";
        public const string OK = "OK";
        public const string OtherError = "OtherError";
        public const string RE_MID_MSG = "RE_MID_MSG";
        public const string RE_MID_ST = "RE_MID_ST";
        public const string FourZeroFiveDesc = "FourZeroFiveDesc";
        public const string FoundButDeactivated = "FoundButDeactivated";
        public const string FiveZeroFiveDesc = "FiveZeroFiveDesc";
        public const string Failed = "Failed";
        public const string Errors = "Errors";
        public const string De_Sucess = "De_Sucess";
        public const string De_Fail = "De_Fail";
        public const string AuthenticationError = "AuthenticationError";
        public const string Al_Deactivated = "Al_Deactivated";
        public const string AccessDenied = "AccessDenied";
        public const string INV_Token = "INV_Token";
        public const string INV_Expiry = "INV_Expiry";
        public const string INV_CardNumber = "INV_CardNumber";
        public const string INV_Avs = "INV_Avs";
        public const string INV_Amt = "INV_Amt";
        public const string INV_CardType = "INV_CardType";
        public const string INV_TranType = "INV_TranType";
        public const string INV_AuthCode = "INV_AuthCode";
        public const string INV_BlPayMode = "INV_BlPayMode";
        public const string INV_BlPayType = "INV_BlPayType";
        public const string INV_AvsOrCVV = "INV_AvsOrCVV";
        public const string INV_ChargeDescriptor = "INV_ChargeDescriptor";
        public const string PtsGenSuccess = "PtsGenSuccess";
        public const string PtsGenFail = "PtsGenFail";
        public const string IsAckRecieved = "IsAckRecieved";
        public const string FtpError = "FtpError";
        public const string InvalidACK = "InvalidACK";
        public const string AckSuccess = "AckSuccess";
        public const string PtsNotGenerated = "PtsNotGenerated";
        public const string NoParameterSupplied = "NoParameterSupplied";
        public const string INV_MvvCode = "INV_MvvCode";
        public const string INV_TranSource = "INV_TranSource";
        public const string INV_ExistDebt = "INV_ExistDebt";
        public const string TimeSlotSetting_Success = "TimeSlotSetting_Success";
        public const string TimeSlotSetting_Fail = "TimeSlotSetting_Fail";
        public const string Inv_OwnID = "Inv_OwnID";
        public const string Inv_TimeSlot = "Inv_TimeSlot";
        public const string Inv_TimeFormat = "Inv_TimeFormat";
        public const string Inv_TimeInterval = "Inv_TimeInterval";
        public const string Prm_Required = "Prm_Required";
        public const string RequestSucess = "RequestSucess";
        public const string RequestFail = "RequestFail";
        public const string RequestInputError = "RequestInputError";
        public const string PTSSuccessStatus = "PTSSuccessStatus";
        public const string PTSStatusIncorrectBatch = "PTSStatusIncorrectBatch";
        public const string AckReceived = "AckReceived";
        public const string AckNotReceived = "AckNotReceived";
        public const string InvalidPTSBatch = "InvalidPTSBatch";
        public const string BlackListing_Fail = "BlackListing_Fail";
        public const string BlackListing_Success = "BlackListing_Success";
        public const string BlackListing_Already = "BlackListing_Already";
        public const string CardNotExists = "CardNotExists";
        public const string BlackListing_Reason = "BlackListing_Reason";
        public const string BlackListing_Info = "BlackListing_Info";
        public const string DuplicateSetting_Success = "DuplicateSetting_Success";
        public const string DuplicateSetting_Fail = "DuplicateSetting_Fail";
        public const string CardNumberBlocked = "CardNumberBlocked";
        public const string DuplicateTranFound = "DuplicateTranFound";
        public const string DuplicateTransactionInterval = "DuplicateTransactionInterval";

        public const string InvalidTrack2Data = "INV_TRACK2DATA";
        public const string InvalidTrack1Data = "INV_TRACK1DATA";
        public const string AVS_Required = "Avs_Required";
        public const string INV_CashBackAmount = "INV_CashBackAmount";
        public const string INV_CardToSupportCashback = "INV_CardToSupportCashback";
        public const string INV_MerchantIndustry = "INV_MerchantIndustry";
        #endregion //End ResourceKey

        #region MCC code
        public const string Mcc6051 = "6051";
        public const string Mcc6012 = "6012";
        public const string PosConditionCodeOtherCards = "01";

        #endregion //end mcc code

    }
}
