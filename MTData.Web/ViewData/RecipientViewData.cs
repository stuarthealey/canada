﻿using System;
using System.Collections.Generic;

namespace MTData.Web.ViewData
{
    public class RecipientViewData
    {
        public int RecipientID { get; set; }
        public int? fk_MerchantID { get; set; }
        public Nullable<int> fk_UserID { get; set; }
        public bool IsRecipient { get; set; }
        public int ReportId { get; set; }
        public string Company { get; set; }
        public string ModifiedBy { get; set; }
        public bool chkAll { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<MerchantListViewData> MerchantList { get; set; }
        public IEnumerable<ReportListViewData> ReportList { get; set; }
        
    }
}