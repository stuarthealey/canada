﻿namespace MTD.Core.DataTransferObjects
{
    public class CountryStateDto
    {
        public string StatesID { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public string fk_CountryCodesID { get; set; }
    }
}
