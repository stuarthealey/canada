﻿using System.Text;
using System;
using System.Threading;
using System.Net.Mail;
using System.Collections.Generic;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Extension;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;
using MTData.Utility;

namespace MTD.Core.Service
{
    public class DriverService : IDriverService
    {
        private readonly IDriverRepository _driverRepository;
        private StringBuilder _logMessage;

        readonly ILogger _logger = new Logger();
        protected string company = string.Empty;

        public DriverService([Named("DriverRepository")] IDriverRepository driverRepository, StringBuilder logMessage)
        {
            _driverRepository = driverRepository;
            _logMessage = logMessage;
        }

        /// <summary>
        /// Purpose             :   To Add fleet
        /// Function Name       :   Add
        /// Created By          :   Umesh Kumar
        /// Created On          :   03/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverDto"></param>
        /// <returns></returns>
        public bool Add(DriverDto driverDto)
        {
            Driver driver = driverDto.ToDriver();
            return _driverRepository.Add(driver);
        }

        /// <summary>
        /// Purpose             :   To check Is Email Exist
        /// Function Name       :   IsEmailExist
        /// Created By          :   Asif Shafeeque
        /// Created On          :   03/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="emailId"></param>
        /// <param name="fleetId"></param>
        /// <param name="driverId"></param>
        /// <returns></returns>
        public bool IsEmailExist(string emailId, int fleetId, int driverId)
        {
            return _driverRepository.IsEmailExist(emailId, fleetId, driverId);
        }

        /// <summary>
        /// Purpose             :   To Get Drivers List
        /// Function Name       :   GetDriverDtoList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   03/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public IEnumerable<DriverDto> GetDriverDtoList(FilterSearchParametersDto filterSearch)
        {
            var driverList = _driverRepository.DriverList(filterSearch.SearchParameters());
            var driverDtoList = driverList.ToDTODriverList();
            return driverDtoList;
        }

        /// <summary>
        /// /
        /// </summary>
        /// <param name="driverId"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public DriverDto GetDriverDto(int driverId, int merchantId)
        {
            var driver = _driverRepository.GetDriver(driverId, merchantId);
            var driverDto = driver.ToDriverDto();
            return driverDto;
        }

        /// <summary>
        /// Purpose             :   To Update the existing driver
        /// Function Name       :   Update
        /// Created By          :   Umesh Kumar
        /// Created On          :   03/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverDto"></param>
        /// <param name="driverId"></param>
        /// <returns></returns>
        public bool Update(DriverDto driverDto, int driverId)
        {
            Driver driver = driverDto.ToDriver();
            return _driverRepository.Update(driver, driverId);
        }

        /// <summary>
        /// Purpose             :   To get the Admin User List
        /// Function Name       :   GetAdminUserListByFilter
        /// Created By          :   Asif Shafeeque
        /// Created On          :   03/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverId"></param>
        public void DeleteDriver(int driverId)
        {
            _driverRepository.DeleteDriver(driverId);
        }

        /// <summary>
        /// Purpose             :   To Get Derivers List
        /// Function Name       :   GetDeriverListByFilter
        /// Created By          :   Naveen Kumar
        /// Created On          :   03/15/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="filterSearch"></param>
        /// <returns></returns>
        public IEnumerable<DriverDto> GetDeriverListByFilter(FilterSearchParametersDto filterSearch)
        {
            var filterSearchClass = filterSearch.SearchParameters();
            var driverList = _driverRepository.DrivGetDeriverListByFiltererList(filterSearchClass);
            var driverDtoList = driverList.ToDTODriverList();
            return driverDtoList;
        }

        /// <summary>
        /// Purpose             :   To Get Driver
        /// Function Name       :   GetDriverDto
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   03/15/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverNo"></param>
        /// <param name="driverPin"></param>
        /// <returns></returns>
        public DriverDto GetDriverDto(string driverNo, string driverPin)
        {
            var driver = _driverRepository.GetDriverDto(driverNo, driverPin);
            var driverDto = driver.ToDriverDto();
            return driverDto;
        }

        /// <summary>
        /// Purpose             :   To check Driver No Valid
        /// Function Name       :   IsDriverNoValid
        /// Created By          :   Asif Shafeeque
        /// Created On          :   03/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverNo"></param>
        /// <param name="driverPin"></param>
        /// <returns></returns>
        public bool IsDriverNoValid(string driverNo, string driverPin)
        {
            bool isValid = _driverRepository.IsDriverNoValid(driverNo, driverPin);
            return isValid;
        }

        /// <summary>
        /// Purpose             :   To Get Drivers
        /// Function Name       :   GetDriverDto
        /// Created By          :   salil gupta
        /// Created On          :   03/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverNo"></param>
        /// <returns></returns>
        public DriverDto GetDriverDto(string driverNo)
        {
            var driver = _driverRepository.GetDriverDto(driverNo);
            var driverDto = driver.ToDriverDto();
            return driverDto;
        }

        /// <summary>
        /// Purpose             :   To upload data from excel
        /// Function Name       :   AddDriverList
        /// Created By          :   Ravit chaudhary
        /// Created On          :   03/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="objDriverDto"></param>
        /// <param name="merchantId"></param>
        /// <param name="userId"></param>
        /// <param name="userType"></param>
        /// <returns></returns>
        public List<DriverDto> AddDriverList(List<DriverDto> objDriverDto, int merchantId, int userId, int userType)
        {
            try
            {
                string _Error = string.Empty;
                List<DriverDto> driverExcelUpload = new List<DriverDto>();
                List<Driver> uploadedDriver = new List<Driver>();
                int response;

                foreach (DriverDto item in objDriverDto)
                {
                    Driver driver = item.ToDriver();
                    response = _driverRepository.IsDriverNoExist(driver, merchantId, userId, userType);
                    switch (response)
                    {
                        case 2:
                            _Error = "Fleet is not assigned to the merchant.";
                            break;
                        case 3:
                            _Error = " Email Id  already exists in same fleet.";
                            break;
                        case 4:
                            _Error = " Driver Number  already exists.";
                            break;
                        case 5:
                            _Error = "Fleet is not assigned to the user.";
                            break;
                        case 6:
                            _Error = "Invalid Country.";
                            break;
                        case 8:
                            _Error = "First name is required.";
                            break;
                        case 9:
                            _Error = "Last name is required.";
                            break;
                        case 10:
                            _Error = "Phone is required.";
                            break;
                        case 11:
                            _Error = "Driver number is required.";
                            break;
                        case 12:
                            _Error = "PIN is required.";
                            break;
                        case 13:
                            _Error = "Driver tax number is required.";
                            break;
                        case 14:
                            _Error = "Fleet is required.";
                            break;
                        case 15:
                            _Error = "Country is required.";
                            break;
                        case 16:
                            _Error = "Address is required.";
                            break;
                        case 17:
                            _Error = "Invalid Email Id.";
                            break;
                        case 18:
                            _Error = "Phone must be minimum length of 6.";
                            break;
                        case 19:
                            _Error = "PIN must be maximum length of 10.";
                            break;
                        case 20:
                            _Error = "PIN must be minimum length of 4.";
                            break;
                        case 21:
                            _Error = " PIN must be numeric.";
                            break;
                        case 22:
                            _Error = "First Name must not exceed the length of 50.";
                            break;
                        case 23:
                            _Error = "Last Name must not exceed the length of 50.";
                            break;
                        case 24:
                            _Error = "Email must not exceed the length of 50.";
                            break;
                        case 25:
                            _Error = "Country must not exceed the length of 50.";
                            break;
                        case 26:
                            _Error = "State must not exceed the length of 50.";
                            break;
                        case 27:
                            _Error = "City must not exceed the length of 50.";
                            break;
                        case 28:
                            _Error = "Address must not exceed the length of 100.";
                            break;
                        case 29:
                            _Error = "Sage Account must not exceed the length of 50.";
                            break;
                        case 30:
                            _Error = "Bank Name must not exceed the length of 50.";
                            break;
                        case 31:
                            _Error = "Account Name must not exceed the length of 50.";
                            break;
                        case 32:
                            _Error = "BSB must not exceed the length of 50.";
                            break;
                        case 33:
                            _Error = "Account must not exceed the length of 50.";
                            break;
                        case 34:
                            _Error = "Invalid First Name";
                            break;
                        case 35:
                            _Error = "Invalid Last Name";
                            break;
                        case 36:
                            _Error = "Invalid Driver Number";
                            break;
                        case 37:
                            _Error = "Invalid Driver Tax Number";
                            break;
                        case 38:
                            _Error = "Invalid State";
                            break;
                        case 39:
                            _Error = "Invalid City";
                            break;
                        case 40:
                            _Error = "Invalid Sage Account";
                            break;
                        case 41:
                            _Error = "Invalid Bank Name";
                            break;
                        case 42:
                            _Error = "Invalid Account Name";
                            break;
                        case 43:
                            _Error = "Invalid BSB";
                            break;
                        case 44:
                            _Error = "Invalid  Account";
                            break;
                        case 45:
                            _Error = "Phone number must be numeric";
                            break;
                    }

                    if (response == 0)
                    {
                        bool isUpload = _driverRepository.AddDriverList(driver, merchantId);
                        if (isUpload)
                        {
                            if (!string.IsNullOrEmpty(driver.Account))
                                driver.Account = StringExtension.EncryptDriver(driver.Account);

                            if (!string.IsNullOrEmpty(driver.SageAccount))
                                driver.SageAccount = StringExtension.EncryptDriver(driver.SageAccount);

                            uploadedDriver.Add(driver);
                            company = _driverRepository.GetDriverCompany(merchantId);
                        }
                        else
                        {
                            driverExcelUpload.Add(new DriverDto { FName = item.FName, LName = item.LName, Phone = item.Phone, Email = item.Email, DriverNo = item.DriverNo, PIN = item.PIN, DriverTaxNo = item.DriverTaxNo, FleetName = item.FleetName, fk_Country = item.fk_Country, Fk_State = item.Fk_State, Fk_City = item.Fk_City, Address = item.Address, SageAccount = item.SageAccount, BankName = item.BankName, AccountName = item.AccountName, BSB = item.BSB, Account = item.Account, FailedError = _Error });
                        }
                    }
                    else
                    {
                        driverExcelUpload.Add(new DriverDto { FName = item.FName, LName = item.LName, Phone = item.Phone, Email = item.Email, DriverNo = item.DriverNo, PIN = item.PIN, DriverTaxNo = item.DriverTaxNo, FleetName = item.FleetName, fk_Country = item.fk_Country, Fk_State = item.Fk_State, Fk_City = item.Fk_City, Address = item.Address,SageAccount = item.SageAccount,BankName = item.BankName,AccountName = item.AccountName,BSB =item.BSB,Account = item.Account, FailedError = _Error });
                    }
                }

                MailSender(uploadedDriver);
                return driverExcelUpload;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            : to reset pin of driver 
        /// Function Name       :   ResetPin
        /// Created By          :  Asf Shafeeque
        /// Created On          :  7/27/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverId"></param>
        /// <param name="pin"></param>
        /// <returns></returns>
        public bool ResetPin(int driverId, string pin)
        {
            return _driverRepository.ResetPin(driverId, pin);
        }

        /// <summary>
        ///  Purpose            : to get schedule driver report
        /// Function Name       :   GetDriverReceipent
        /// Created By          :  Asf Shafeeque
        /// Created On          :  7/27/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public IEnumerable<DriverRecipientDto> GetDriverReceipent(int merchantId, int fleetId)
        {
            IEnumerable<DriverReportRecipient> driverRec = _driverRepository.GetDriverReceipent(merchantId, fleetId);
            return driverRec.ToDtoList();
        }

        /// <summary>
        ///  Purpose            : to update schedule driver report
        /// Function Name       :   UpdateDriverRecipient
        /// Created By          :  Asf Shafeeque
        /// Created On          :  7/27/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverRecipient"></param>
        /// <param name="fleetId"></param>
        /// <param name="userType"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public bool UpdateDriverRecipient(DriverRecListDto driverRecipient, int fleetId, int userType, int merchantId)
        {
            DriverRecList driverReport = driverRecipient.toDriverReportRec();
            return _driverRepository.UpdateDriverRecipient(driverReport, fleetId, userType, merchantId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driverRecipient"></param>
        /// <param name="fleetId"></param>
        /// <param name="userType"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public bool MerchantDriverRecipient(DriverRecListDto driverRecipient, int fleetId, int userType, int merchantId)
        {
            DriverRecList driverReport = driverRecipient.toDriverReportRec();
            return _driverRepository.MerchantDriverRecipient(driverReport, fleetId, userType, merchantId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <summary>
        ///  Purpose            : to schedule driver report
        /// Function Name       :   ScheduleDriverReport
        /// Created By          :  Asf Shafeeque
        /// Created On          :  7/27/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="scheduleDto"></param>
        /// <returns></returns>
        public bool ScheduleDriverReport(ScheduleReportDto scheduleDto)
        {
            try
            {
                var scheduleReport = scheduleDto.ToScheduleReport();
                return _driverRepository.ScheduleDriverReport(scheduleReport);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            : to  driver report list
        /// Function Name       :   DriverReportList
        /// Created By          :  Asf Shafeeque
        /// Created On          :  7/27/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ReportListDto> DriverReportList()
        {
            try
            {
                var reportList = _driverRepository.DriverReportList();
                var reportDtoList = reportList.ToReportList();
                return reportDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            : to  driver report list
        /// Function Name       :   ScheduleReportListByFilter
        /// Created By          :  Asf Shafeeque
        /// Created On          :  7/27/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public IEnumerable<ScheduleReportDto> ScheduleReportListByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting)
        {
            try
            {
                var merchantList = _driverRepository.ScheduleReportListByFilter(name, jtStartIndex, jtPageSize, jtSorting);
                var merchantDtoList = merchantList.ToScheduleList();
                return merchantDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            : to  get  driver schedule report 
        /// Function Name       :   GetScheduleDto
        /// Created By          :  Asf Shafeeque
        /// Created On          :  7/27/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="scheduleId"></param>
        /// <returns></returns>
        public ScheduleReportDto GetScheduleDto(int scheduleId)
        {
            try
            {
                var scheduleReport = _driverRepository.GetScheduleReport(scheduleId);
                var scheduleReportDto = scheduleReport.ToScheduleReportDto();
                return scheduleReportDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            : to  save  driver schedule report 
        /// Function Name       :   ScheduleDriverReportUpdate
        /// Created By          :  Asf Shafeeque
        /// Created On          :  7/27/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="scheduleDto"></param>
        /// <returns></returns>
        public bool ScheduleDriverReportUpdate(ScheduleReportDto scheduleDto)
        {
            try
            {
                var scheduleReport = scheduleDto.ToScheduleReport();
                return _driverRepository.ScheduleDriverReportUpdate(scheduleReport);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Fleet List of all fleets
        /// Function Name       :   AllFleets
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/30/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<FleetDto> AllFleets()// get list of fleet
        {
            try
            {
                IEnumerable<Fleet> fleetlist = _driverRepository.AllFleets();
                var fleetDtoList = fleetlist.ToDTOList();
                return fleetDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   to get the list driver report list
        /// Function Name       :   ScheduleDriverReportList
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   7/28/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ScheduleReportDto> ScheduleDriverReportList()
        {
            try
            {
                IEnumerable<ScheduleReportDto> query = _driverRepository.ScheduleDriverReportList().ToScheduleList();
                return query;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DriverRecipientDto> GetDriverReceipent()
        {
            IEnumerable<DriverReportRecipient> driverRec = _driverRepository.GetDriverReceipent();
            return driverRec.ToDtoList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<DriverRecipientDto> GetMerchantDriverRecipient(int merchantId)
        {
            IEnumerable<DriverReportRecipient> driverRec = _driverRepository.GetMerchantDriverReceipent(merchantId);
            return driverRec.ToDtoList();
        }

        /// <summary>
        /// Purpose             :   To Get Drivers
        /// Function Name       :   GetDriverDto
        /// Created By          :   salil gupta
        /// Created On          :   03/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverNo"></param>
        /// <returns></returns>
        public DriverDto GetDriverDto(string driverNo, int fleetId)
        {
            var driver = _driverRepository.GetDriverDto(driverNo, fleetId);
            var driverDto = driver.ToDriverDto();
            return driverDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="userId"></param>
        /// <param name="userFleet"></param>
        /// <returns></returns>
        public IEnumerable<DriverDto> UserDriverList(FilterSearchParametersDto filterSearch)
        {
            var driverList = _driverRepository.UserDriverList(filterSearch.SearchParameters());
            var driverDtoList = driverList.ToDTODriverList();
            return driverDtoList;
        }

        /// <summary>
        /// Purpose             :   To get driver login data
        /// Function Name       :   GetDriverLoginDto
        /// Created By          :   Sunil Singh 
        /// Created On          :   26-Aug-2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DriverLoginDto> GetDriverLoginDto(string DeviceId, string DriverId, int UserType)
        {
            try
            {
                var driverLoginData = _driverRepository.GetDriverLoginDto(DeviceId, DriverId, UserType);
                var scheduleReportDto = driverLoginData.ToDTOList();
                return scheduleReportDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listToEmail"></param>
        public void MailSender(List<Driver> listToEmail)
        {
            Thread bgThread = new Thread(new ParameterizedThreadStart(SendEmail));
            bgThread.IsBackground = true;
            bgThread.Start(listToEmail);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driverDto"></param>
        void SendEmail(Object driverDto)
        {
            List<Driver> driver = (List<Driver>)driverDto;
            int i = driver.Count;
            int counter = 0;

            while (counter < i)
            {
                try
                {
                    string pin = driver[counter].PIN;
                    string hashedPin = driver[counter].PIN.Hash();
                    int driverId = driver[counter].DriverID;
                    bool result = _driverRepository.UpdatePin(driverId, hashedPin);
                    if (true)
                    {
                        if (!string.IsNullOrEmpty(driver[counter].Email))
                        {
                            _logMessage.Append("Mail Sending begin and reading file ~/HtmlTemplate/DriverMail.html");

                            string cMail = driver[counter].Email;
                            string driverNo = driver[counter].DriverNo;
                            string cuserName = driver[counter].FName;
                            StringBuilder sb = new StringBuilder();

                            sb.Append("<html><body><p> Hi " + cuserName + ",</p><p>Your account has been successfully created by MTData admin.You can login to MTData system using below credential.<br /></p><br />");
                            sb.Append("<table style='font-size: 14px; font-family:verdana;border:1px black;height:500px;width:800px;'>");
                            sb.Append(" <tr><td style='width: 140px; height: 24px;'>Driver No.: </td> <td>" + driverNo + "</td></tr>");
                            sb.Append("<tr><td style='width: 140px; height: 24px;'>PIN: </td> <td>" + pin + "</td></tr>");
                            sb.Append("<tr><td style='width: 140px; height: 20px;'>Company Name: </td> <td>" + company + " </td></tr>");
                            sb.Append(" <tr><td colspan='2'>&nbsp;</td></tr>");
                            sb.Append(" <tr><td colspan='2'>Please reset your password after login to system. </td></tr>");
                            sb.Append(" <tr><td colspan='2'>&nbsp;</td></tr>");
                            sb.Append(" <tr><td colspan='2'>Note: This is an auto generated mail. Please do not reply.</td></tr></table>");
                            sb.Append("<h4>Thanks and Regards<br />MTData Support team<br /></h4></body></html>");

                            var sendEmail = new EmailUtil();
                            _logMessage.Append("calling SendMail method of utility class");
                            sendEmail.SendMail(cMail, sb.ToString(), company, "Registered User");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), ex);
                    throw;
                }

                counter++;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driverID"></param>
        /// <returns></returns>
        public bool IsExist(int driverID)
        {
            return _driverRepository.IsExist(driverID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleetId"></param>
        /// <param name="driverId"></param>
        /// <returns></returns>
        public bool IsDupligate(int fleetId, string driverNo)
        {
            return _driverRepository.IsDupligate(fleetId, driverNo);
        }

        public DriverDto AddDispatchDriver(DriverDto driverDto)
        {
            Driver drDispatch = driverDto.ToDriver();
            var driver = _driverRepository.AddDispatchDriver(drDispatch);
            var driverDispatchDto = driver.ToDriverDto();
            return driverDispatchDto;
        }
        
    }
}
