﻿using System;

namespace MTData.Web.ViewData
{
    public class MerchantCurrencyViewData
    {
        public int GatewayCurrencyID { get; set; }
        public Nullable<int> fk_GatewayID { get; set; }
        public string CurrencyCode { get; set; }
    }
}