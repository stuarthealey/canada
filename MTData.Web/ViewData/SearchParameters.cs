﻿using System;

namespace MTData.Web.ViewData
{
    public class SearchParameters
    {
        public string VNumber { get; set; }
        public string DNumber { get; set; }
        public string CrdType { get; set; }
        public Nullable<DateTime> startDate { get; set; }
        public Nullable<DateTime> endDate { get; set; }
        public int? Id { get; set; }
        public string AddRespData { get; set; }
        public string TType { get; set; }
        public Nullable<int> MerchantId { get; set; }
        public bool? rejected { get;set; }
        public bool? approved { get; set; }
        public string AId { get; set; }
        public string Industry { get; set; }
        public  int? fleetId { get; set; }
        public string Sorting { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string ANumber { get; set; }
        public string ExpiryDate { get; set; }
        public decimal? MinAmt { get; set; }
        public decimal? MaxAmt { get; set; }
        public string FFD { get; set; }
        public string LFD { get; set; }
        public string JobNumber { get; set; }
        public string PayerType { get; set; }
        public bool? IsVarified { get; set; }
        public bool? IsVarifiedYes { get; set; }
        public bool? IsVarifiedNo { get; set; }
        public bool? IsVarifiedBoth { get; set; }
        public string startDateString { get; set; }
        public string endDateString { get; set; }
        public string SerialNo { get; set; }
        public string TerminalId { get; set; }
        public Nullable<int> NetworkType { get; set; }
    }
}