//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MTD.Data.Data
{
    using System;
    
    public partial class usp_GetDriversTransaction_Result
    {
        public string DriverNo { get; set; }
        public string Fleet { get; set; }
        public Nullable<int> NumberOfKeyed { get; set; }
        public Nullable<int> NumberOfSwiped { get; set; }
        public Nullable<decimal> KeyedSaleAmount { get; set; }
        public Nullable<decimal> SwipedSaleAmount { get; set; }
        public Nullable<int> TotalNumberOfSale { get; set; }
        public Nullable<decimal> TotalSaleAmount { get; set; }
        public Nullable<decimal> TechFee { get; set; }
        public Nullable<decimal> Fee { get; set; }
        public Nullable<decimal> SumSurcharge { get; set; }
        public Nullable<decimal> SumFleetFee { get; set; }
        public string MerchantName { get; set; }
        public string DriverName { get; set; }
        public int FleetID { get; set; }
        public string VehicleNo { get; set; }
    }
}
