﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MTData.Web.Resource;

namespace MTData.Web.ViewData
{
    public class DefaultSettingViewData
    {
        public int DefaultID { get; set; }
        public string DefaultName { get; set; }
        [Range(typeof(decimal), "0", "100", ErrorMessage = "Invalid state tax rate")]
        public Nullable<decimal> StateTaxRate { get; set; }
        [Range(typeof(decimal), "0", "100", ErrorMessage = "Invalid Federal tax rate")]
        public Nullable<decimal> FederalStateRate { get; set; }

        public Nullable<byte> SurchargeType { get; set; }
        public Nullable<decimal> SurchargeFixed { get; set; }
        public Nullable<decimal> SurchargePer { get; set; }
        public Nullable<decimal> SurchargeMaxCap { get; set; }
        public Nullable<byte> BookingFeeType { get; set; }
        public Nullable<decimal> BookingFeeFixed { get; set; }
        public Nullable<decimal> BookingFeePer { get; set; }
        public Nullable<decimal> BookingMaxCap { get; set; }
        [UIHint("MultilineText")]
        [AllowHtml]
        public string TermCondition { get; set; }
        [AllowHtml]
        [UIHint("MultilineText")]
        public string Disclaimer { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Nullable<decimal> Surcharge { get; set; }
        public Nullable<decimal> BookingFee { get; set; }
        public Nullable<decimal> FeePercentage { get; set; }
        public Nullable<decimal> SurPercentage { get; set; }
        public string FeeType { get; set;}
        public string SurType { get; set; }
        public int IsNew { get; set; }
        public Nullable<decimal> TipLow { get; set; }
        public Nullable<decimal> TipMedium { get; set; }
        public Nullable<decimal> TipHigh { get; set; }
        public IEnumerable<GatewayListViewData> GatewayList { get; set; }
        public IEnumerable<SurchargeTypeViewData> TypeList { get; set; }
        public IEnumerable<TechnologyFeeTypeViewData> TechnologyFeeList { get; set; }
        public string TechFee { get; set; }
        public Nullable<byte> TechFeeType { get; set; }
        public Nullable<decimal> TechFeeFixed { get; set; }
        public Nullable<decimal> TechFeePer { get; set; }
        public Nullable<decimal> TechFeeMaxCap { get; set; }
        public Nullable<int> TransCountSlablt1 { get; set; }
        public Nullable<int> TransCountSlabGt2 { get; set; }
        public Nullable<int> TransCountSlablt2 { get; set; }
        public Nullable<int> TransCountSlabGt3 { get; set; }
        public Nullable<int> TransCountSlablt3 { get; set; }
        public Nullable<int> TransCountSlabGt4 { get; set; }
        
      
    }
}