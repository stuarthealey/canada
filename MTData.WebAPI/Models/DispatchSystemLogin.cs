﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Models
{
    public class DispatchSystemLogin
    {
        public string DriverNumber { get; set; }
        public string Pin { get; set; }
        public string CarNumber { get; set; }
        public string DeviceId { get; set; }
        public string Stan { get; set; }
    }
}