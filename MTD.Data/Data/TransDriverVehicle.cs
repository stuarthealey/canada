﻿namespace MTD.Data.Data
{
    public class TransDriverVehicle
    {
        public string VehicleNumber { get; set; }
        public string DriverNumber { get; set; }
    }
}
