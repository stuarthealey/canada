﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Web.Http;
using System.Xml;
using Antlr.Runtime.Tree;
using Ninject;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Web;
using System.Web.Configuration;
using System.Net;
using System.Security;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Diagnostics;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.WebAPI.Models;
using MTData.WebAPI.Resources;
using MTData.WebAPI.Common;
using MTData.WebAPI.CanadaCert;
using MTData.WebAPI.CanadaCommon;

namespace MTData.WebAPI.Controllers
{
    public class PaymentController : TransactionRequestController
    {
        #region (Constructor Loading)

        private readonly ITransactionService _transactionService;
        private readonly ITerminalService _terminalService;
        private readonly IMerchantService _merchantService;
        private readonly IDriverService _driverService;
        private readonly ICommonService _commonService;
        private LocalVariables objLocalVars = new LocalVariables();
        private readonly StringBuilder _logMessage;
        private StringBuilder _logTracking;
        readonly ILogger _logger = new Logger();
        
        public PaymentController(
            [Named("TransactionService")] ITransactionService transactionService,
            [Named("MerchantService")] IMerchantService merchantService,
            [Named("DriverService")] IDriverService driverService,
            [Named("TerminalService")] ITerminalService terminalService,
            [Named("CommonService")] ICommonService commonService,
            StringBuilder logMessage, StringBuilder logTracking)
            : base(terminalService, logMessage)
        {
            _merchantService = merchantService;
            _transactionService = transactionService;
            _terminalService = terminalService;
            _driverService = driverService;
            _logMessage = logMessage;
            _commonService = commonService;
            _logTracking = logTracking;
        }

        fDMerchantDto _fdMerchantobj = new fDMerchantDto();
        DatawireDto _datawireObj;
        PaymentResponse response = new PaymentResponse();
        EmvCapkResponse capkresponse = new EmvCapkResponse();
        decimal fleetFee = 0;
        decimal techFee = 0;
        decimal txnFee = 0;
        bool _isRetryServiceDiscovery = false;
        readonly Stopwatch objStopWatch = new Stopwatch();
        private List<ServiceProvider> _serviceProvider = new List<ServiceProvider>();
        private static bool isFirstDiscovery = true;
        private static DateTime lstDiscvryDone = DateTime.Now;

        #endregion

        #region (Payment Process for US)

        /// <summary>
        /// Purpose             :   Sending Credit and debit card transaction to rapid connect
        /// Function Name       :   SendPayment
        /// Created By          :   Salil Gupta
        /// Created On          :   02/27/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY" 
        /// </summary>Sending data...
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult SendPayment(HttpRequestMessage request)
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            _logMessage.AppendLine("creating an object of XmlDocument Class");
            _logMessage.AppendLine("object created successfully of LoginResponse Class as response");
            LoginHeader mHeader = new LoginHeader();
            _logMessage.AppendLine("object created successfully of LoginHeader Class as mHeader");

            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                _logMessage.AppendLine("Loading request XML data.");
                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);
                _logTracking.AppendLine("Payment process start. DriverNo: " + objLocalVars._driverNo + " and  DeviceId " + objLocalVars._deviceId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());
                _logMessage.AppendLine("Initializing local variables from XML request data");

                #region Initialize local variable from XML Request.

                if (!SetLocalVarFromXMLRequest(xmlDoc))
                {
                    _logMessage.AppendLine("Failed to initializ local variables from request XML data.");
                    //_logger.LogInfoMessage(_logMessage.ToString());

                    //zzz
                    // Need to create an Exception to add the XML data to, to get in the ApplicationLog table.
                    StringWriter sw = new StringWriter();
                    System.Xml.XmlTextWriter tw = new XmlTextWriter(sw);
                    xmlDoc.WriteTo(tw);

                    Exception ex = new Exception(string.Format("XML_Data:{0}", sw.ToString()));

                    _logger.LogInfoFatel(_logMessage.ToString(), ex);

                    tw.Close();
                    sw.Close();

                    return Ok(response);
                }

                #endregion

                #region Validate Request

                dynamic firstDataDetails = string.Empty;
                _logMessage.AppendLine("Validating request xml data");
                if (!ValidateRequest(response, mHeader, out firstDataDetails))
                {
                    _logMessage.AppendLine("Failed to validate request xml data.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    return Ok(response);
                }

                #endregion

                // Broken v1.0.0.4
                //#region check merchant is configured for this industry type or not
                //if (objLocalVars._entryMode == PaymentEntryMode.Keyed.ToString() &&
                //    !string.Equals(objLocalVars._txnType, "Refund", StringComparison.OrdinalIgnoreCase) &&
                //    !string.Equals(objLocalVars._txnType, "Void", StringComparison.OrdinalIgnoreCase))
                //{
                //    firstDataDetails.ProjectId = firstDataDetails.EcommProjectId;
                //}
                //if (String.IsNullOrEmpty(firstDataDetails.ProjectId))
                //{
                //    _logMessage.AppendLine("Merchant is not configured for this industry type");
                //    _logger.LogInfoMessage(_logMessage.ToString());

                //    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.MerchantNotConfiguredForThisIndustry, objLocalVars._deviceId, objLocalVars._requestId);
                //    return Ok(response);
                //}
                //#endregion

                // Fixed 1.0.0.4    2016-04-13
                #region check merchant is configured for this industry type or not
                
                if (objLocalVars._entryMode == PaymentEntryMode.Keyed.ToString())
                {
                    firstDataDetails.ProjectId = firstDataDetails.EcommProjectId;
                }

                if (String.IsNullOrEmpty(firstDataDetails.ProjectId))
                {
                    _logMessage.AppendLine("Merchant is not configured for this industry type");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.MerchantNotConfiguredForThisIndustry, objLocalVars._deviceId, objLocalVars._requestId);
                    return Ok(response);
                }
                
                #endregion

                #region Preparing transaction
                
                _logMessage.AppendLine("Preparing transactions");
                var cardType = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/CardType");

                CardInfo cardInfo;
                if (!objLocalVars._entryMode.Equals(PaymentEntryMode.EmvContact.ToString()) &&
                    !objLocalVars._entryMode.Equals(PaymentEntryMode.EmvContactless.ToString()))
                {
                    _logMessage.AppendLine("Preparing NON-EMV transactions");
                    if (!NonEMVTransactions(out cardInfo, cardType))
                    {
                        _logMessage.AppendLine("Failed to prepare NON-EMV transactions.");
                        _logger.LogInfoMessage(_logMessage.ToString());

                        return Ok(response);
                    }
                }
                else
                {
                    _logMessage.AppendLine("Preparing EMV transactions");
                    if (!EMVTransactions(out cardInfo, cardType))
                    {
                        _logMessage.AppendLine("Failed to prepare NON-EMV transactions.");
                        _logger.LogInfoMessage(_logMessage.ToString());

                        return Ok(response);
                    }
                }
                
                #endregion

                #region Calculating Tech Fee
                
                _logMessage.AppendLine("Preparing Fleet and Tech fee.");

                decimal tip = Convert.ToDecimal(string.IsNullOrEmpty(objLocalVars.Tip) ? "0" : objLocalVars.Tip);
                decimal surcharge = Convert.ToDecimal(string.IsNullOrEmpty(objLocalVars.Surcharge) ? "0" : objLocalVars.Surcharge);
                decimal fare = Convert.ToDecimal(string.IsNullOrEmpty(objLocalVars.Fare) ? "0" : objLocalVars.Fare);
                decimal fareTipSurchare = tip + surcharge + fare;

                _logMessage.AppendLine("Calculating Tech Fee.");
                GetFleetTechFee(_transactionService, Convert.ToInt32(objLocalVars.FleetId), fareTipSurchare, out fleetFee, out techFee, out txnFee);
                
                #endregion

                #region Creating Transaction Request as per UMF specification.

                _logMessage.AppendLine("Loading XML request data into TransactionRequest");

                bool? isDispatchRequest = null;
                if (objLocalVars._requestType == "Android" || objLocalVars._requestType == null)
                    isDispatchRequest = false;
                if (objLocalVars._requestType == "Dispatch")
                    isDispatchRequest = true;

                TransactionRequest tr;
                try
                {
                    tr = InitiateTransaction(cardInfo, firstDataDetails);
                    tr.IsDispatchRequest = isDispatchRequest;
                    tr.FleetFee = Convert.ToDecimal(fleetFee);
                    tr.TechFee = Convert.ToDecimal(techFee);
                    tr.Fee = txnFee.ToString();
                }
                catch (Exception ex)
                {
                    _logMessage.AppendLine("Failed to load XML request data into TransactionRequest");
                    _logger.LogInfoFatel(_logMessage.ToString(), ex);

                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidTransactionRequest, objLocalVars._deviceId, objLocalVars._requestId);
                    return Ok(response);
                }
                
                _logMessage.AppendLine("Creating transaction request.");
                CreateTransactionRequest(tr);

                string xmlSerializedTransReq = GetXmlData();
                _logMessage.AppendLine("GetXMLData exceuted successfully");

                #endregion

                #region Validating XML request

                _logMessage.AppendLine("Validating XML request");
                int transId;
                string ccnum = tr.CardNumber;
                string expiry = tr.ExpiryDate;
                XmlDocument ValidateXml = new XmlDocument();
                ValidateXml.LoadXml(xmlSerializedTransReq);
                XmlElement elem = (XmlElement)ValidateXml.DocumentElement.FirstChild;
                if (elem.IsNull())
                {
                    _logMessage.AppendLine("Failed to validationg XML request.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidTransactionRequest, objLocalVars._deviceId, objLocalVars._requestId);
                    return Ok(response);
                }

                #endregion

                #region Save transaction into MTDTransaction Table

                _logMessage.AppendLine("calling XmlToTransactionDto takes xmlSerializedTransReq, tr");
                tr.TerminalId = tr.TerminalId.FormatTerminalId();
                MTDTransactionDto transactionDto = CommonFunctions.XmlToTransactionDto(tr);
                _logMessage.AppendLine("Fare,Tip,SurCH,Fee " + transactionDto.FareValue + " , " + transactionDto.Tip + " , " + transactionDto.Surcharge + " , " + transactionDto.Fee);

                _logMessage.AppendLine("Inserting Transaction.");
                transId = _transactionService.Add(transactionDto);
                _logMessage.AppendLine("Transaction inserted, transId-" + transId);

                _logMessage.AppendLine("Updating stan parameters" + transactionDto.Stan + "--" + transactionDto.TransRefNo + "--" + tr.TerminalId + "--" + tr.MtdMerchantId);
                _terminalService.UpdateStan(transactionDto.Stan, transactionDto.TransRefNo, tr.TerminalId, tr.MtdMerchantId);

                string clientRef = GetClientRef(firstDataDetails.ProjectId);
                _logMessage.AppendLine("GetClientRef executed successfully- " + clientRef);

                #endregion

                string isLog = string.Empty;
                isLog = WebConfigurationManager.AppSettings["LogPaymentResponse"];

                //Send data using SOAP protocol to Datawire
                _logMessage.AppendLine("Sending transaction request to rapid conect");
                string xmlSerializedTransResp = xmlSerializedTransResp = new SoapHandler().SendMessage(xmlSerializedTransReq, clientRef, tr);
                if (string.IsNullOrEmpty(xmlSerializedTransResp))
                {
                    //resp is null,  initializing Timeout
                    _logMessage.AppendLine("xmlSerializedTransResp is null,initializing timeout.");
                    TimeoutTransaction objTimeout = new TimeoutTransaction(_transactionService, _terminalService, _merchantService, _logMessage);
                    tr.CardNumber = ccnum;
                    tr.ExpiryDate = expiry;
                    xmlSerializedTransResp = objTimeout.TimeOut(tr, transId, clientRef, xmlSerializedTransResp);

                    //Updating DB if null response found after all timeouts.
                    if (string.IsNullOrEmpty(xmlSerializedTransResp))
                    {
                        _logMessage.AppendLine("xmlSerializedTransResp is null after all timeout requests, updating database");
                        _transactionService.Update(new MTDTransactionDto() { AddRespData = PaymentAPIResources.FDServerDown }, transId, tr.RapidConnectAuthId);

                        _logMessage.AppendLine("Database updated successfully, process completed, returning error response.");
                        _logger.LogInfoMessage(_logMessage.ToString());

                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.FDServerDown, objLocalVars._deviceId, objLocalVars._requestId);
                        return Ok(response);
                    }

                    if (isLog == "1")
                        _logMessage.AppendLine("TOR ResponseXML " + xmlSerializedTransResp);
                }
                else
                {
                    if (isLog == "1")
                        _logMessage.AppendLine("ResponseXML " + xmlSerializedTransResp);

                    //replacing invalid xml chars
                    var xmlResp = xmlSerializedTransResp.Replace("&", "&amp;");

                    //Updating transaction response into database.
                    _logMessage.AppendLine("xmlSerializedTransResp is not null");
                    MTDTransactionDto responsetransactionDto = CommonFunctions.XmlResponseToTransactionDto(
                        xmlResp, false, tr.PaymentType.ToString(), tr.IndustryType);

                    //Making TerminalId of lenght 8 by appending prefixing zeros.
                    tr.TerminalId = tr.TerminalId.FormatTerminalId();

                    _logMessage.AppendLine("Updating xmlSerializedTransResp into database.");
                    _transactionService.Update(responsetransactionDto, transId, tr.RapidConnectAuthId);
                }

                PaymentResponse payResponse = new PaymentResponse();
                if (!string.IsNullOrEmpty(xmlSerializedTransResp))
                    payResponse = CommonFunctions.TransactionResponse(tr, xmlSerializedTransResp, tr.PaymentType.ToString(), tr.IndustryType, transId);

                _logTracking.AppendLine("Payment process completed. DriverNo: " + objLocalVars._driverNo + " and  DeviceId " + objLocalVars._deviceId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());
                _logMessage.AppendLine("SendPayment process completed successfully.");
                _logger.LogInfoMessage(_logTracking.ToString());
                _logger.LogInfoMessage(_logMessage.ToString());

                return Ok(payResponse);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
                response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.Exception, objLocalVars._deviceId, objLocalVars._requestId);
                return Ok(response);
            }
        }

        #region Payment process for canada

        /// <summary>
        /// Purpose             :   Process canada transactions with first data 
        /// Function Name       :   CnSendPayment
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/25/2016
        /// Modification Made   :   ****************************
        /// Modified On       :    "MM/DD/YYYY" 
        /// </summary>
        /// <param name="cnRequest"></param>
        /// <returns>"transResp"</returns>   
        public IHttpActionResult CnSendPayment(HttpRequestMessage cnRequest)
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            _logMessage.AppendLine("creating an object of XmlDocument Class");
            _logMessage.AppendLine("object created successfully of LoginResponse Class as response");
        
            LoginHeader mHeader = new LoginHeader();
            
            _logMessage.AppendLine("object created successfully of LoginHeader Class as mHeader");
            
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                _logMessage.AppendLine("Loading request XML data.");

                xmlDoc.Load(cnRequest.Content.ReadAsStreamAsync().Result);
                
                _logTracking.AppendLine("Payment process start. DriverNo: " + objLocalVars._driverNo + " and  DeviceId " + objLocalVars._deviceId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());
                _logMessage.AppendLine("Initializing local variables from XML request data");

                #region Initialize local variable from XML Request.

                if (!SetLocalVarFromXMLRequest(xmlDoc))
                {
                    _logMessage.AppendLine("Failed to initializ local variables from request XML data.");
                    _logger.LogInfoMessage(_logMessage.ToString());
                    return Ok(response);
                }

                #endregion

                #region Validate Request

                dynamic firstDataDetails = string.Empty;
                _logMessage.AppendLine("Validating request xml data");
                if (!ValidateRequest(response, mHeader, out firstDataDetails))
                {
                    _logMessage.AppendLine("Failed to validate request xml data.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    return Ok(response);
                }

                #endregion

                #region Preparing transaction

                _logMessage.AppendLine("Preparing transactions");
                var cardType = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/CardType");

                //string[] cardInfo = { string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty };
                CardInfo cardInfo;
                if (!objLocalVars._entryMode.Equals(PaymentEntryMode.EmvContact.ToString()) && !objLocalVars._entryMode.Equals(PaymentEntryMode.EmvContactless.ToString()))
                {
                    _logMessage.AppendLine("Preparing NON-EMV transactions");
                    if (!NonEMVTransactions(out cardInfo, cardType))
                    {
                        _logMessage.AppendLine("Failed to prepare NON-EMV transactions.");
                        _logger.LogInfoMessage(_logMessage.ToString());

                        return Ok(response);
                    }
                }
                else
                {
                    _logMessage.AppendLine("Preparing EMV transactions");
                    if (!EMVTransactions(out cardInfo, cardType))
                    {
                        _logMessage.AppendLine("Failed to prepare NON-EMV transactions.");
                        _logger.LogInfoMessage(_logMessage.ToString());

                        return Ok(response);
                    }
                }

                #endregion

                #region Calculating Tech Fee
                
                _logMessage.AppendLine("Preparing Fleet and Tech fee.");
                decimal tip = Convert.ToDecimal(string.IsNullOrEmpty(objLocalVars.Tip) ? "0" : objLocalVars.Tip);
                decimal surcharge = Convert.ToDecimal(string.IsNullOrEmpty(objLocalVars.Surcharge) ? "0" : objLocalVars.Surcharge);
                decimal fare = Convert.ToDecimal(string.IsNullOrEmpty(objLocalVars.Fare) ? "0" : objLocalVars.Fare);
                decimal fareTipSurchare = tip + surcharge + fare;

                _logMessage.AppendLine("Calculating Tech Fee.");
                GetFleetTechFee(_transactionService, Convert.ToInt32(objLocalVars.FleetId), fareTipSurchare, out fleetFee, out techFee, out txnFee);
                
                #endregion

                #region Creating Transaction Request as per UMF specification.
                
                _logMessage.AppendLine("Loading XML request data into TransactionRequest");

                bool? isDispatchRequest = null;
                if (objLocalVars._requestType == RequestType.Android.ToString() || objLocalVars._requestType == null)
                    isDispatchRequest = false;
                if (objLocalVars._requestType == RequestType.Dispatch.ToString())
                    isDispatchRequest = true;

                TransactionRequest tr;
                try
                {
                    tr = InitiateTransaction(cardInfo, firstDataDetails);
                    tr.IsDispatchRequest = isDispatchRequest;
                    tr.FleetFee = Convert.ToDecimal(fleetFee);
                    tr.TechFee = Convert.ToDecimal(techFee);
                    tr.Fee = txnFee.ToString();
                }
                catch (Exception ex)
                {
                    _logMessage.AppendLine("Failed to load XML request data into TransactionRequest");
                    _logger.LogInfoFatel(_logMessage.ToString(), ex);

                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidTransactionRequest, objLocalVars._deviceId, objLocalVars._requestId);
                    return Ok(response);
                }

                #endregion

                #region Prepare payload in iso8583 message format

                string clientRef = CnTransactionMessage.GetCanadaClientRef();
                tr.Stan = GetSystemTraceNumber(firstDataDetails.fk_MerchantID, firstDataDetails.RCTerminalId);
                _logMessage.AppendLine("GetClientRef executed successfully" + clientRef);
                TransactionProcessed transType = (TransactionProcessed)Enum.Parse(typeof(TransactionProcessed), tr.TransactionType);
                
                string referenceNumber = _transactionService.GetRefNumber();
                
                switch (transType)
                {
                    case TransactionProcessed.Completion:
                        var originalTransaction = _transactionService.GetTransaction(Convert.ToInt32(tr.RapidConnectAuthId));
                        tr.TransRefNo = originalTransaction.TransRefNo;
                        tr.AuthIdentResponse = originalTransaction.AuthId;
                        tr.ResponseCode = originalTransaction.ResponseCode;
                        tr.AddRespdata = FormatFields.PrepareAddRespData(originalTransaction.AddRespData);
                        tr.CardNumber = string.Empty;
                        var bit63Response = originalTransaction.ResponseXml.ParseBit63Resoponse(tr.CardType.ToString());
                        tr.RespTable14 = bit63Response.Table14Response;
                        tr.RespTable49 = bit63Response.Table49Response;
                        tr.RespTableVI = bit63Response.TableVIResponse;
                        tr.RespTableSP = bit63Response.TableSPResponse;
                        tr.RespTableMC = bit63Response.TableMCResponse;
                        tr.RespTableDS = bit63Response.TableDSResponse;
                        break;

                    case TransactionProcessed.Refund:
                        var originalTransData1 = _transactionService.GetTransaction(Convert.ToInt32(tr.RapidConnectAuthId));
                        tr.OriginalTransType = originalTransData1.TxnType;
                        tr.CardNumber = string.Empty;
                        tr.ResponseCode = originalTransData1.ResponseCode;
                        tr.TransNote = originalTransData1.TransNote;
                        tr.MAXAmount = originalTransData1.Amount.ToString();
                        tr.TransRefNo = FormatFields.FormatReferenceNo(tr.TerminalId, tr.Stan, tr.PaymentType.ToString(), referenceNumber);
                        break;

                    case TransactionProcessed.Void:
                        var originalTransData = _transactionService.GetTransaction(Convert.ToInt32(tr.RapidConnectAuthId));
                        tr.TrnmsnDateTime = originalTransData.TransmissionDateTime;
                        tr.Stan = originalTransData.Stan;
                        tr.MAXAmount = originalTransData.Amount.ToString();
                        tr.OriginalTransType = originalTransData.TxnType;
                        tr.TransRefNo = originalTransData.TransRefNo;
                        tr.AuthIdentResponse = originalTransData.AuthId;
                        tr.CardNumber = string.Empty;
                        tr.IsReversal = true;
                        tr.ResponseCode = originalTransData.ResponseCode;
                        tr.TransNote = originalTransData.TransNote;
                        break;

                    default:
                        tr.TransRefNo = FormatFields.FormatReferenceNo(tr.TerminalId, tr.Stan, tr.PaymentType.ToString(), referenceNumber);
                        break;
                }

                CnTransactionEntities cnTrans = new CnTransactionEntities();
                PaymentIndustryType industryType = (PaymentIndustryType)Enum.Parse(typeof(PaymentIndustryType), tr.IndustryType);
                switch (industryType)
                {
                    case PaymentIndustryType.Ecommerce:
                        tr.CnPayload = CnPreparePostTransaction.CheckAndPrepareEcommTrans(tr, clientRef, out cnTrans);
                        break;
                    case PaymentIndustryType.Retail:
                        tr.CnPayload = CnPreparePostTransaction.CheckAndPrepareRetailTrans(tr, clientRef, out cnTrans);
                        break;
                }

                #endregion

                #region Save transaction into MTDTransaction Table and updating stan

                _logMessage.AppendLine("calling XmlToTransactionDto takes xmlSerializedTransReq, tr");
                tr.TerminalId = tr.TerminalId.FormatTerminalId();
                MTDTransactionDto transactionDto = CommonFunctions.XmlToTransactionDto(tr);
                
                if (tr.TransactionType == TxnTypeType.Completion.ToString())
                    transactionDto.SourceId = tr.RapidConnectAuthId;
                
                _logMessage.AppendLine("Fare,Tip,Surcharge,Fee" + transactionDto.FareValue + " , " + transactionDto.Tip + " , " + transactionDto.Surcharge + " , " + transactionDto.Fee);
                _logMessage.AppendLine("Inserting Transaction.");
                
                int transId = _transactionService.Add(transactionDto);

                _logMessage.AppendLine("Update stan parameters" + transactionDto.Stan + "--" + transactionDto.TransRefNo + "--" + tr.TerminalId + "--" + tr.MtdMerchantId);
                _terminalService.UpdateStan(transactionDto.Stan, transactionDto.TransRefNo, tr.TerminalId, tr.MtdMerchantId);
                
                #endregion

                #region  Sending transaction request and parsing the response then save in the database
                
                Global.ActiveUrl = _transactionService.GetTransactionUrl();
                string transResponse = String.Empty;
                transResponse = CnSendTransaction.SendMessageToDatawire(Global.ActiveUrl, tr.CnPayload);
                cnTrans.CompleteResponse = transResponse;
                Iso8583Response isoResponse = HandleResponse(cnTrans, transId);
                while (String.IsNullOrEmpty(transResponse))
                {
                    if (Global.UrlList != null)
                    {
                        if (Global.UrlList.Contains(Global.ActiveUrl))
                            Global.UrlList.Remove(Global.ActiveUrl);
                    }
                    
                    string fastestUrl = _transactionService.GetFastestUrl(Global.ActiveUrl, Global.UrlList);                    
                    if (String.IsNullOrEmpty(fastestUrl))
                    {
                        ServiceDiscovery();
                        Global.ActiveUrl = _transactionService.GetTransactionUrl();
                        transResponse = CnSendTransaction.SendMessageToDatawire(Global.ActiveUrl, tr.CnPayload);
                        cnTrans.CompleteResponse = transResponse;
                        isoResponse = HandleResponse(cnTrans, transId);
                    }
                    else
                    {
                        Global.ActiveUrl = fastestUrl;
                        transResponse = CnSendTransaction.SendMessageToDatawire(Global.ActiveUrl, tr.CnPayload);
                        cnTrans.CompleteResponse = transResponse;
                        isoResponse = HandleResponse(cnTrans, transId);
                    }

                    if (String.IsNullOrEmpty(transResponse))
                        continue;
                }

                if (CommonFunctions.IsFailed(isoResponse.ReturnCode))
                {
                    CnTimeOutTransaction timeOutTransaction = new CnTimeOutTransaction(_transactionService, _terminalService, _merchantService, _logMessage);
                    isoResponse = timeOutTransaction.TimeOut(tr, transId, clientRef);
                    if (Object.Equals(isoResponse, null))
                    {
                        isoResponse.Message = PaymentAPIResources.CnTimeOutError;
                        isoResponse.AdditionalResponseData = PaymentAPIResources.CnTimeOutError;
                    }
                }

                string objResponse = Serialization.Serialize<Iso8583Response>(isoResponse);
                isoResponse.CompleteResponse = objResponse;
                if (!tr.IsTORTransaction)
                {
                    MTDTransactionDto transDto = CnTransactionMessage.FillMerchantDto(isoResponse);
                    if (tr.TransactionType == TxnTypeType.Completion.ToString())
                        transDto.SourceId = tr.RapidConnectAuthId;
                    _transactionService.CnUpdate(transDto, transId);
                }

                return Ok(isoResponse);

                #endregion
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
                response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.Exception, objLocalVars._deviceId, objLocalVars._requestId);
                return Ok(response);
            }
        }

        #endregion

        /// <summary>
        /// Processing Non-EMV transactions
        /// </summary>
        /// <param name="cardInfo"></param>
        /// <param name="cardType"></param>
        private bool NonEMVTransactions(out CardInfo cardInfo, XmlNode cardType)
        {
            try
            {
                if (objLocalVars._industryType.Equals(PaymentIndustryType.Ecommerce.ToString()) && (objLocalVars._tranType.Equals(TxnTypeType.Sale) || objLocalVars._tranType.Equals(TxnTypeType.Authorization)))
                {
                    cardInfo = StringExtension.GetCardNumberData(objLocalVars._ccno);
                }
                else if (objLocalVars._industryType.Equals(PaymentIndustryType.Retail.ToString()) && (objLocalVars._tranType.Equals(TxnTypeType.Sale) || objLocalVars._tranType.Equals(TxnTypeType.Authorization)))
                {
                    var encryprtBlock = StringExtension.GetTrackThreeData(objLocalVars._track3Data);
                    if (!encryprtBlock.IsNull() && encryprtBlock.Length >= 3)
                    {
                        objLocalVars._track3Data = encryprtBlock[0];
                        string key = encryprtBlock[2];
                        objLocalVars._keyId = key.ToString().TrimEnd('\r', '\n');
                    }
                    else
                    {
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidCardData, objLocalVars._deviceId, objLocalVars._requestId);
                        return false;
                    }

                    cardInfo = StringExtension.GetCardNumberData(objLocalVars._track2data);
                    if (string.IsNullOrEmpty(cardInfo.ErrorMessage))
                    {
                        objLocalVars._expirydate = cardInfo.YYYYMM;
                        objLocalVars._serviceCode = cardInfo.ServiceCode;
                        if (!string.IsNullOrEmpty(objLocalVars._serviceCode) && (objLocalVars._serviceCode.Trim().StartsWith("2") || objLocalVars._serviceCode.Trim().StartsWith("6")))
                        {
                            objLocalVars._entryMode = "FSwiped";
                        }
                        objLocalVars._ccno = cardInfo.CardNumber;
                    }
                    else
                    {
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidCardData, objLocalVars._deviceId, objLocalVars._requestId);
                        return false;
                    }
                }

                if (!string.IsNullOrEmpty(cardType.InnerText))
                {
                    objLocalVars._cardType = (CardTypeType)Enum.Parse(typeof(CardTypeType), cardType.InnerText);
                }
                else if (string.IsNullOrEmpty(cardType.InnerText) && !string.IsNullOrEmpty(objLocalVars._ccno) && objLocalVars._paytype.ToString() != PaymentCardType.Debit.ToString())
                {
                    if (string.IsNullOrEmpty(cardInfo.ErrorMessage))
                    {
                        objLocalVars._cardType = CreditCardType.GetCardType(cardInfo.CardNumber);
                    }
                    else
                    {
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidCardData, objLocalVars._deviceId, objLocalVars._requestId);
                        return false;
                    }
                }
                else if (objLocalVars._paytype.ToString() == PaymentCardType.Debit.ToString() && !objLocalVars._industryType.Equals(PaymentIndustryType.Ecommerce.ToString()))
                {
                    string[] pinSerial = StringExtension.GetPinSerial(objLocalVars._pinData);
                    if (!pinSerial.IsNull() && pinSerial.Length >= 2)
                    {
                        objLocalVars._debitPin = pinSerial[0];
                        objLocalVars._KCN = pinSerial[1];
                    }
                    else
                    {
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidCardData, objLocalVars._deviceId, objLocalVars._requestId);
                        return false;
                    }
                }

                return true;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Processing EMV Transactions
        /// </summary>
        /// <param name="cardInfo"></param>
        /// <param name="cardType"></param>
        /// <param name="isFailed"></param>
        private bool EMVTransactions(out CardInfo cardInfo, XmlNode cardType)
        {
            try
            {
                //Extracting EMV data from XML request
                EmvDataValues emvDataRequest = objLocalVars._emvData.GetEMVdata(EmvTagType.AuthorizationRequestEMVTags);

                if ((!string.IsNullOrEmpty(emvDataRequest.PinData) && objLocalVars._paytype.ToString() == PaymentCardType.Credit.ToString()) && !objLocalVars._industryType.Equals(PaymentIndustryType.Ecommerce.ToString()))
                {
                    objLocalVars._pinData = emvDataRequest.PinData;
                    string[] pinSerial = StringExtension.GetPinSerial(objLocalVars._pinData);
                    if (!pinSerial.IsNull() && pinSerial.Length >= 2)
                    {
                        objLocalVars._debitPin = pinSerial[0];
                        objLocalVars._KCN = pinSerial[1];
                    }
                    else
                    {
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidCardData, objLocalVars._deviceId, objLocalVars._requestId);
                        return false;
                    }
                }

                //Track2 data
                objLocalVars._cSequence = emvDataRequest.CSequence;
                objLocalVars._track2data = emvDataRequest.Track2;

                //Preparing Expiry date
                cardInfo = StringExtension.GetCardNumberData(objLocalVars._track2data);

                if (string.IsNullOrEmpty(cardInfo.ErrorMessage))
                {
                    objLocalVars._expirydate = cardInfo.YYYYMM;
                    objLocalVars._ccno = cardInfo.CardNumber;
                }
                else
                {
                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidCardData, objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }
                if (!string.IsNullOrEmpty(cardType.InnerText) && objLocalVars._paytype.ToString() != PaymentCardType.Debit.ToString())
                {
                    objLocalVars._cardType = (CardTypeType)Enum.Parse(typeof(CardTypeType), cardType.InnerText);
                }
                else if (string.IsNullOrEmpty(cardType.InnerText) && !string.IsNullOrEmpty(objLocalVars._ccno) && objLocalVars._paytype.ToString() != PaymentCardType.Debit.ToString())
                {
                    if (string.IsNullOrEmpty(cardInfo.ErrorMessage))
                    {
                        objLocalVars._cardType = CreditCardType.GetCardType(cardInfo.CardNumber);
                    }
                    else
                    {
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidCardData, objLocalVars._deviceId, objLocalVars._requestId);
                        return false;
                    }
                }
                else if (objLocalVars._paytype.ToString() == PaymentCardType.Debit.ToString() && !objLocalVars._industryType.Equals(PaymentIndustryType.Ecommerce.ToString()))
                {
                    objLocalVars._pinData = emvDataRequest.PinData;
                    string[] pinSerial = StringExtension.GetPinSerial(objLocalVars._pinData);
                    if (!pinSerial.IsNull() && pinSerial.Length >= 2)
                    {
                        objLocalVars._debitPin = pinSerial[0];
                        objLocalVars._KCN = pinSerial[1];
                    }
                    else
                    {
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidCardData, objLocalVars._deviceId, objLocalVars._requestId);
                        return false;
                    }
                }

                //Track3 data
                objLocalVars._track3Data = emvDataRequest.Track3;

                //EMV data
                objLocalVars._emvData = emvDataRequest.EmvData;

                //Preparing Encryt Block
                if (!string.IsNullOrEmpty(objLocalVars._track3Data))
                {
                    string[] encryprtBlock = StringExtension.GetTrackThreeData(objLocalVars._track3Data);
                    if (!encryprtBlock.IsNull() && encryprtBlock.Length >= 3)
                    {
                        objLocalVars._track3Data = encryprtBlock[0];
                        objLocalVars._keyId = encryprtBlock[2];
                    }
                    else
                    {
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidCardData, objLocalVars._deviceId, objLocalVars._requestId);
                        return false;
                    }
                }
                objLocalVars._txnAmount = emvDataRequest.Amount;

                return true;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   Extract the value from XMLrequest and set to Local variables
        /// Function Name       :   SetLocalVarFromXMLRequest
        /// Created By          :   Sunil Singh
        /// Created On          :   14-Aug-2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private bool SetLocalVarFromXMLRequest(XmlDocument xmlDoc)
        {
            try
            {
                var dvcID = xmlDoc.SelectSingleNode("PaymentRequest/Header/DeviceId");
                if (!dvcID.IsNull())
                    objLocalVars._deviceId = dvcID.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/DeviceId".MissingElementMessage());
                    return false;
                }

                var reqID = xmlDoc.SelectSingleNode("PaymentRequest/Header/RequestId");
                if (!reqID.IsNull())
                    objLocalVars._requestId = reqID.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/RequestId".MissingElementMessage());
                    return false;
                }

                var drvrID = xmlDoc.SelectSingleNode("PaymentRequest/Header/DriverId");
                if (!drvrID.IsNull())
                    objLocalVars._driverNo = drvrID.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/DriverId".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var token = xmlDoc.SelectSingleNode("PaymentRequest/Header/Token");
                if (!token.IsNull())
                    objLocalVars._token = token.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/Token".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var requestType = xmlDoc.SelectSingleNode("PaymentRequest/Header/RequestType");
                if (!requestType.IsNull())
                    objLocalVars._requestType = requestType.InnerText;
                else
                {
                    objLocalVars._requestType = null;
                }

                var entryMode = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/EntryMode");
                if (!entryMode.IsNull())
                {
                    objLocalVars._entryMode = entryMode.InnerText;
                    if (string.IsNullOrEmpty(objLocalVars._entryMode))
                    {
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidEntryMode, objLocalVars._deviceId, objLocalVars._requestId);
                        return false;
                    }
                }
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/EntryMode".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var curCode = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/CurrencyCode");
                if (!curCode.IsNull())
                    objLocalVars._currency = curCode.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/CurrencyCode".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                //----------
                var emvData = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/EmvData");
                if (!emvData.IsNull())
                    objLocalVars._emvData = emvData.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/EmvData".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var ccno = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/CardNumber");
                if (!ccno.IsNull())
                    objLocalVars._ccno = ccno.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/CardNumber".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var expDate = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/ExpiryDate");
                if (!expDate.IsNull())
                    objLocalVars._expirydate = expDate.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/ExpiryDate".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var track2Data = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Track2Data");
                if (!track2Data.IsNull())
                    objLocalVars._track2data = track2Data.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Track2Data".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var track3Data = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Track3Data");
                if (!track3Data.IsNull())
                    objLocalVars._track3Data = track3Data.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Track3Data".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var PinData = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/PinData");
                if (!PinData.IsNull())
                    objLocalVars._pinData = PinData.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/PinData".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var transType = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/TransactionType");
                if (!transType.IsNull())
                    objLocalVars._txnType = transType.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/TransactionType".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                if (objLocalVars._txnType != PaymentTransactionType.Void.ToString())
                {
                    if (!transType.IsNull())
                    {
                        objLocalVars._tranType = (TxnTypeType)Enum.Parse(typeof(TxnTypeType), transType.InnerText);
                    }
                }

                var paymentType = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/PaymentType");
                if (!paymentType.IsNull())
                {
                    objLocalVars._paytype = (PymtTypeType)Enum.Parse(typeof(PymtTypeType), paymentType.InnerText);
                }
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/PaymentType".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var txnAmountTag = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/TransactionAmount");
                if (!txnAmountTag.IsNull())
                    objLocalVars._txnAmount = txnAmountTag.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/TransactionAmount".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var indType = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/IndustryType");
                if (!indType.IsNull())
                {
                    objLocalVars._industryType = indType.InnerText;
                    if (string.IsNullOrEmpty(objLocalVars._industryType))
                    {
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidIndustryType, objLocalVars._deviceId, objLocalVars._requestId);
                        return false;
                    }
                }
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/IndustryType".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                if (objLocalVars._industryType.Equals(PaymentIndustryType.Ecommerce.ToString()))
                {
                    var createdBy = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/CreatedBy");
                    if (!createdBy.IsNull())
                    {
                        objLocalVars._createdBy = createdBy.InnerText;
                    }
                    else
                    {
                        response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/CreatedBy".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                        return false;
                    }
                }
                else
                {
                    objLocalVars._createdBy = objLocalVars._driverNo;
                }

                //Transaction request variable initialization
                var cvv = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/CcvData");
                if (!txnAmountTag.IsNull())
                    objLocalVars.cardCvv = cvv.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/CcvData".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var vehicleNum = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/VehicleNumber");
                if (!vehicleNum.IsNull())
                    objLocalVars.VehicleNumber = vehicleNum.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/VehicleNumber".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var rapidConnectAuthId = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/RapidConnectAuthId");
                if (!rapidConnectAuthId.IsNull())
                    objLocalVars.RapidConnectAuthId = rapidConnectAuthId.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/RapidConnectAuthId".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var transNote = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/TransNote");
                if (!transNote.IsNull())
                    objLocalVars.TransNote = transNote.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/TransNote".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var streetAddress = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/StreetAddress");
                if (!streetAddress.IsNull())
                    objLocalVars.StreetAddress = streetAddress.InnerText;
                else
                {
                    //T#6162 Commented out until proven needed again.
                    //if (objLocalVars._txnType.ToUpper() != "REFUND")    //D#5517 No StreetAddress present for Refund transaction type, so ignore.
                    //{
                    //    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/StreetAddress".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    //    return false;
                    //}
                }

                var zipCode = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/ZipCode");
                if (!zipCode.IsNull())
                    objLocalVars.ZipCode = zipCode.InnerText;
                else
                {
                    if (objLocalVars._txnType.ToUpper() != "REFUND")    //D#5517 No ZipCode present for Refund transaction type, so ignore.
                    {
                        response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/ZipCode".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                        return false;
                    }
                }

                var pickAddress = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/PickUpAdd");
                if (!pickAddress.IsNull())
                    objLocalVars.PickAddress = pickAddress.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/PickUpAdd".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var destinationAddress = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/DropOffAdd");
                if (!destinationAddress.IsNull())
                    objLocalVars.DestinationAddress = destinationAddress.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/DropOffAdd".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var startLatitude = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/PickUpLat");
                if (!startLatitude.IsNull())
                    objLocalVars.StartLatitude = startLatitude.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/PickUpLat".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var endLatitude = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/DropOffLat");
                if (!endLatitude.IsNull())
                    objLocalVars.EndLatitude = endLatitude.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/DropOffLat".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var startLongitude = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/PickUpLng");
                if (!startLongitude.IsNull())
                    objLocalVars.StartLongitude = startLongitude.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/PickUpLng".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var endLongitude = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/DropOffLng");
                if (!endLongitude.IsNull())
                    objLocalVars.EndLongitude = endLongitude.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/DropOffLng".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var fare = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Fare");
                if (!fare.IsNull())
                    objLocalVars.Fare = fare.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Fare".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var taxes = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Taxes");
                if (!taxes.IsNull())
                    objLocalVars.Taxes = taxes.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Taxes".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var tip = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Tip");
                if (!tip.IsNull())
                    objLocalVars.Tip = tip.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Tip".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var tolls = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Tolls");
                if (!tolls.IsNull())
                    objLocalVars.Tolls = tolls.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Tolls".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var surcharge = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Surcharge");
                if (!surcharge.IsNull())
                    objLocalVars.Surcharge = surcharge.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Surcharge".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var fee = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Fee");
                if (!fee.IsNull())
                    objLocalVars.Fee = fee.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Fee".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var jobNumber = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/JobNumber");
                if (!jobNumber.IsNull())
                    objLocalVars.JobNumber = jobNumber.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/JobNumber".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var track1Data = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Track1Data");
                if (!track1Data.IsNull())
                    objLocalVars.Track1Data = track1Data.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Track1Data".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var flagFall = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/FlagFall");
                if (!flagFall.IsNull())
                    objLocalVars.FlagFall = flagFall.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/FlagFall".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var extras = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Extras");
                if (!extras.IsNull())
                    objLocalVars.Extras = extras.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Extras".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var gateFee = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/GateFee");
                if (!gateFee.IsNull())
                    objLocalVars.GateFee = gateFee.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/GateFee".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var others = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Others");
                if (!others.IsNull())
                    objLocalVars.Others = others.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Others".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var currencyCode = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/CurrencyCode");
                if (!currencyCode.IsNull())
                    objLocalVars.CurrencyCode = currencyCode.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/CurrencyCode".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var fleetId = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/FleetId");
                if (!fleetId.IsNull())
                    objLocalVars.FleetId = fleetId.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/FleetId".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                return true;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To initialize the TransactionRequest.
        /// Function Name       :   InitiateTransaction
        /// Created By          :   Sunil Singh
        /// Created On          :   14-Aug-2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private TransactionRequest InitiateTransaction(CardInfo cardInfo, dynamic firstDataDetails)
        {
            TransactionRequest tr = new TransactionRequest
                {
                    SerialNumber = objLocalVars._deviceId,
                    RequestedId = objLocalVars._requestId,
                    TransactionType = objLocalVars._txnType,
                    TransType = objLocalVars._tranType,
                    PaymentType = objLocalVars._paytype,
                    CardType = objLocalVars._cardType,
                    IndustryType = objLocalVars._industryType,
                    TransactionAmount = objLocalVars._txnAmount,
                    CardNumber = objLocalVars._ccno,
                    ExpiryDate = objLocalVars._expirydate,
                    CardCvv = objLocalVars.cardCvv,
                    DriverNumber = objLocalVars._driverNo,
                    VehicleNumber = objLocalVars.VehicleNumber,
                    RapidConnectAuthId = objLocalVars.RapidConnectAuthId,
                    TransNote = objLocalVars.TransNote,
                    CreatedBy = objLocalVars._createdBy,
                    StreetAddress = objLocalVars.StreetAddress,
                    ZipCode = objLocalVars.ZipCode,
                    PickAddress = objLocalVars.PickAddress,
                    DestinationAddress = objLocalVars.DestinationAddress,
                    StartLatitude = objLocalVars.StartLatitude,
                    EndLatitude = objLocalVars.EndLatitude,
                    StartLongitude = objLocalVars.StartLongitude,
                    EndLongitude = objLocalVars.EndLongitude,
                    Fare = objLocalVars.Fare,
                    Taxes = objLocalVars.Taxes,
                    Tip = objLocalVars.Tip,
                    Tolls = objLocalVars.Tolls,
                    Surcharge = objLocalVars.Surcharge,
                    Fee = objLocalVars.Fee,
                    JobNumber = objLocalVars.JobNumber,
                    Track1Data = objLocalVars.Track1Data,
                    Track2Data = objLocalVars._track2data,
                    Track3Data = objLocalVars._track3Data,
                    KeyId = objLocalVars._keyId,
                    FlagFall = objLocalVars.FlagFall,
                    Extras = objLocalVars.Extras,
                    GateFee = objLocalVars.GateFee,
                    Others = objLocalVars.Others,
                    EntryMode = objLocalVars._entryMode,
                    CurrencyCode = objLocalVars.CurrencyCode,
                    FleetId = objLocalVars.FleetId,
                    EmvData = objLocalVars._emvData,
                    CardSeq = objLocalVars._cSequence,
                    DebitPin = objLocalVars._debitPin,
                    KeySerialNumber = objLocalVars._KCN,
                    FirstFourCard = cardInfo.First4,
                    LastFourcard = cardInfo.Last4,
                    LocalDateTime = DateTime.Now.ToString("yyyyMMddHHmmss"),
                    TrnmsnDateTime = DateTime.UtcNow.ToString("yyyyMMddHHmmss"),
                    FdMerchantId = firstDataDetails.FDMerchantID,
                    MtdMerchantId = firstDataDetails.fk_MerchantID,
                    TerminalId = firstDataDetails.RCTerminalId,
                    TppId = firstDataDetails.ProjectId,
                    GroupId = firstDataDetails.GroupId,
                    TokenType = firstDataDetails.TokenType,
                    MCCode = firstDataDetails.MCCode,
                    EncryptionKey = firstDataDetails.EncryptionKey,
                    Did = firstDataDetails.DID,
                    App = firstDataDetails.App,
                    ServiceId = firstDataDetails.serviceId,
                    ServiceUrl = string.Empty,
                };

            return tr;
        }

        /// <summary>
        /// Validating payment request.
        /// Purpose             :   Validate reqest
        /// Function Name       :   ValidateRequest
        /// Created By          :   Sunil Singh
        /// Created On          :   08/19/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :    "MM/DD/YYYY" 
        /// <param name="response"></param>
        /// <param name="mHeader"></param>
        /// <param name="firstDataDetails"></param>
        /// <param name="isFailed"></param>
        /// <returns>bool</returns>
        private bool ValidateRequest(PaymentResponse response, LoginHeader mHeader, out dynamic firstDataDetails)
        {
            try
            {
                bool isPass = true;
                if (objLocalVars._currency != PaymentAPIResources.Cur_USA && objLocalVars._currency != PaymentAPIResources.Cur_Cn)
                {
                    isPass = false;
                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.SupportedCur, objLocalVars._deviceId, objLocalVars._requestId);
                }

                if (string.IsNullOrEmpty(objLocalVars._deviceId))
                {
                    isPass = false;
                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.DeviceIdRequired, objLocalVars._deviceId, objLocalVars._requestId);
                }

                bool isToken = string.IsNullOrEmpty(objLocalVars._token);
                if (isToken)
                {
                    firstDataDetails = _commonService.GetFirstDataDetails(objLocalVars._deviceId, objLocalVars._driverNo, (string)null, Convert.ToInt32(objLocalVars.FleetId), objLocalVars._requestType);
                }
                else
                {
                    firstDataDetails = _commonService.GetFirstDataDetails(objLocalVars._deviceId, objLocalVars._driverNo, objLocalVars._token, Convert.ToInt32(objLocalVars.FleetId), objLocalVars._requestType);
                }

                if (!objLocalVars._industryType.Equals(PaymentIndustryType.Ecommerce.ToString()))
                {
                    if (isToken || firstDataDetails == null)
                    {
                        isPass = false;
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.UnauthorizedAccess, objLocalVars._deviceId, objLocalVars._requestId);
                    }
                }

                if (firstDataDetails == null)
                {
                    isPass = false;
                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.UnauthorizedAccess, objLocalVars._deviceId, objLocalVars._requestId);
                }

                return isPass;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Calculating Tech Fee
        /// </summary>
        /// <param name="_transactionService"></param>
        /// <param name="fleetId"></param>
        /// <param name="fareTipSurchare"></param>
        /// <param name="fleetFee"></param>
        /// <param name="techFee"></param>
        private void GetFleetTechFee(ITransactionService _transactionService, int fleetId, decimal fareTipSurchare, out decimal fleetFee, out decimal techFee, out decimal txnFee)
        {
            try
            {
                var surchFee = _commonService.SurchargeFees(fleetId);

                fleetFee = 0;
                techFee = 0;
                txnFee = 0;

                if (!objLocalVars._tranType.Equals(PaymentTransactionType.Void) && !objLocalVars._tranType.Equals(PaymentTransactionType.Refund))
                {
                    if (surchFee.PayType == (int)PayType.PayNetwork)
                    {
                        //Applying FleetFee if PayNetwork Selected
                        decimal percentageFleet = 0, bothFleet = 0;
                        decimal maxCapFleet = Convert.ToDecimal(surchFee.FleetFeeMaxCap);
                        FeeType fFeeType = (FeeType)Convert.ToInt32(surchFee.FleetFeeType);

                        switch (fFeeType)
                        {
                            case FeeType.Fixed:
                                fleetFee = Convert.ToDecimal(surchFee.FleetFeeFixed);
                                break;

                            case FeeType.Percentage:
                                percentageFleet = fareTipSurchare * Convert.ToDecimal(surchFee.FleetFeePer) / 100;
                                fleetFee = (percentageFleet < maxCapFleet) ? percentageFleet : maxCapFleet;
                                break;

                            case FeeType.Both:
                                percentageFleet = fareTipSurchare * Convert.ToDecimal(surchFee.FleetFeePer) / 100;
                                bothFleet = percentageFleet + Convert.ToDecimal(surchFee.FleetFeeFixed);
                                fleetFee = (bothFleet < maxCapFleet) ? bothFleet : maxCapFleet;
                                break;
                        }
                    }
                    // Applying transaction fee when PayDriver or PayOwner selected
                    else if (surchFee.PayType == (int)PayType.PayDriver || surchFee.PayType == (int)PayType.PayOwner)
                    {
                        decimal maxCapTran = Convert.ToDecimal(surchFee.TechFeeMaxCap);
                        decimal percentageTran = 0, bothT = 0;
                        FeeType txnFeeType = (FeeType)Convert.ToInt32(surchFee.TechFeeType);

                        switch (txnFeeType)
                        {
                            case FeeType.Fixed:
                                txnFee = Convert.ToDecimal(surchFee.TechFeeFixed);
                                break;

                            case FeeType.Percentage:
                                percentageTran = fareTipSurchare * Convert.ToDecimal(surchFee.TechFeePer) / 100;
                                txnFee = (percentageTran < maxCapTran) ? percentageTran : maxCapTran;
                                break;

                            case FeeType.Both:
                                percentageTran = fareTipSurchare * Convert.ToDecimal(surchFee.TechFeePer) / 100;
                                bothT = percentageTran + Convert.ToDecimal(surchFee.TechFeeFixed);
                                txnFee = (bothT < maxCapTran) ? bothT : maxCapTran;
                                break;
                        }
                    }
                }

                //Applying TechFee in all cases
                decimal maxCap = Convert.ToDecimal(surchFee.TechFeeMaxCap);
                decimal percentage = 0, both = 0;
                FeeType tFeeType = (FeeType)Convert.ToInt32(surchFee.TechFeeType);
                switch (tFeeType)
                {
                    case FeeType.Fixed:
                        techFee = Convert.ToDecimal(surchFee.TechFeeFixed);
                        break;

                    case FeeType.Percentage:
                        percentage = fareTipSurchare * Convert.ToDecimal(surchFee.TechFeePer) / 100;
                        techFee = (percentage < maxCap) ? percentage : maxCap;
                        break;

                    case FeeType.Both:
                        percentage = fareTipSurchare * Convert.ToDecimal(surchFee.TechFeePer) / 100;
                        both = percentage + Convert.ToDecimal(surchFee.TechFeeFixed);
                        techFee = (both < maxCap) ? both : maxCap;
                        break;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        /// <summary>
        /// Purpose             :   Datwire registration method for terminal registration from web portal
        /// Function Name       :   DatawireRegistration
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   04/14/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult DatawireRegistration(HttpRequestMessage request)
        {
            _logMessage.AppendLine("Datawire registration process start.");
            DatawireResponse res = new DatawireResponse();
            try
            {
                _logMessage.AppendLine("Calling datawire registration class.");
                DatawireRegistration dwire = new DatawireRegistration(_terminalService, _merchantService, _logMessage);

                _logMessage.AppendLine("Datawire registration process complete.");
                _logger.LogInfoMessage(_logMessage.ToString());

                res = (DatawireResponse)dwire.Register(request, res, _fdMerchantobj, _logger);

                return Ok(res);
            }
            catch (Exception ex)
            {
                res.Status = PaymentAPIResources.Failed;
                res.Value = ex.Message;
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
                return Ok(res);
            }
        }

        /// <summary>
        /// Purpose             :   Datwire registration method for terminal registration from web portal
        /// Function Name       :   DatawireRegistrationCN
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   03/28/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult DatawireRegistrationCN(HttpRequestMessage request)
        {
            string resgist = string.Empty;
            _logMessage.AppendLine("Datawire registration process start.");
            DatawireResponse res = new DatawireResponse();
            try
            {
                _logMessage.AppendLine("Calling datawire registration class.");
                DatawireRegistration dwire = new DatawireRegistration(_terminalService, _merchantService, _logMessage);

                _logMessage.AppendLine("Datawire registration process complete.");
                _logger.LogInfoMessage(_logMessage.ToString());

                resgist = dwire.RegisterCN(request, res, _fdMerchantobj, _logger);

                return Ok(resgist);
            }
            catch (Exception ex)
            {
                res.Status = PaymentAPIResources.Failed;
                res.Value = ex.Message;
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
                return Ok(resgist);
            }
        }

        /// <summary>
        /// Purpose             :   Datwire registration method for terminal registration from web portal
        /// Function Name       :   DatawireService
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   03/29/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult DatawireService(HttpRequestMessage request)
        {
            string service = string.Empty;
            _logMessage.AppendLine("Datawire registration process start.");
            DatawireResponse res = new DatawireResponse();
            try
            {
                _logMessage.AppendLine("Calling datawire registration class.");
                DatawireRegistration dwire = new DatawireRegistration(_terminalService, _merchantService, _logMessage);

                _logMessage.AppendLine("Datawire registration process complete.");
                _logger.LogInfoMessage(_logMessage.ToString());

                service = dwire.ServiceRegister(request, res, _fdMerchantobj, _logger);

                return Ok(service);
            }
            catch (Exception ex)
            {
                res.Status = PaymentAPIResources.Failed;
                res.Value = ex.Message;
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
                return Ok(service);
            }
        }

        /// <summary>
        /// Purpose             :   Ping request
        /// Function Name       :   DatawirePing
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   03/30/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult DatawirePing(HttpRequestMessage request)
        {
            string ping = string.Empty;
            _logMessage.AppendLine("Datawire registration process start.");
            DatawireResponse res = new DatawireResponse();
            try
            {
                _logMessage.AppendLine("Calling datawire registration class.");
                DatawireRegistration dwire = new DatawireRegistration(_terminalService, _merchantService, _logMessage);

                _logMessage.AppendLine("Datawire registration process complete.");
                _logger.LogInfoMessage(_logMessage.ToString());

                ping = dwire.Pingregister(request, res, _fdMerchantobj, _logger);

                return Ok(ping);
            }
            catch (Exception ex)
            {
                res.Status = PaymentAPIResources.Failed;
                res.Value = ex.Message;
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
                return Ok(ping);
            }
        }

        /// <summary>
        /// Create the request message format for merchant provisioning
        /// </summary>
        /// <returns>Return the merchant provision request xml need to send on datawire</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string SendMessageToDatawire(string url, string xml, int timeOut = 100000, string requestType = "POST")
        {
            ILogger logger = new Logger();
            StringBuilder logMessage = new StringBuilder();

            logMessage.AppendLine("Controller Name:-WebAPI.Controllers.PaymentController; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                string xmlResponse = string.Empty;
                HttpWebRequest objWebrequest = (HttpWebRequest)WebRequest.Create(url);
                byte[] buf = Encoding.UTF8.GetBytes(xml);

                if (objWebrequest != null)
                {
                    #region Header setting

                    //Header setting for the request start 
                    objWebrequest.UserAgent = Global.UserAgent;
                    objWebrequest.Method = requestType;
                    objWebrequest.ContentLength = buf.Length;
                    objWebrequest.KeepAlive = true;
                    objWebrequest.Headers.Add(HttpRequestHeader.CacheControl, "no-cache");
                    objWebrequest.ContentType = "text/xml";
                    objWebrequest.Timeout = timeOut;

                    //Header setting for the request ends 
                    //Allowing the service to authenticate on any of following SSL certificate 
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                         | SecurityProtocolType.Tls11
                                                         | SecurityProtocolType.Tls12
                                                         | SecurityProtocolType.Ssl3;

                    // allows for validation of SSL conversations
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    
                    #endregion

                    #region request stream

                    //only when Request Type is post
                    if (requestType.Equals("POST"))
                    {
                        using (var stream = objWebrequest.GetRequestStream()) //get the request stream from request object created 
                        {
                            stream.Write(buf, 0, buf.Length);// write the content in the request stream                        
                        }
                    }

                    #endregion

                    #region Response stream

                    // get the response for the request                    
                    var response = (HttpWebResponse)objWebrequest.GetResponse();

                    // Get the response in a stream.
                    var receiveStream = response.GetResponseStream();

                    // Pipes the stream to a higher level stream reader with the required encoding format. 
                    if (receiveStream != null)
                    {
                        var readStream = new StreamReader(receiveStream, Encoding.UTF8);
                        //get the registration response
                        xmlResponse = readStream.ReadToEnd();
                        response.Close();
                        readStream.Close();
                    }

                    //set the system last used time                 
                    #endregion
                }

                return xmlResponse;
            }
            catch (WebException ex)
            {
                logger.LogInfoFatel(logMessage.ToString(), ex);
                return string.Empty;
            }
            catch (Exception ex)
            {
                logger.LogInfoFatel(logMessage.ToString(), ex);
                return string.Empty;
            }
        }

        /// <summary>
        /// Process the Service discovery with the xml supplied as the parameter 
        /// </summary>
        /// <param name="xml">Source URL to for discovery </param>
        /// <returns>Return the list of url</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        private static string ProcessServiceDiscoveryToDatawire(string url)
        {
            try
            {
                string response = CnSendTransaction.SendMessageToDatawire(url, "", 20000, "GET"); //No xml required for service discovery
                return response;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Process the ping request on list of service providers which pass as parameter 
        /// if ping request fail due to AuthenticateError, Timeout, UnknownServiceID
        /// it will retry on same url two time if response yet contain the above status 
        /// then it will delete the url from the list
        /// </summary>
        /// <param name="objProviders">Source list of service providers</param>
        private void ProcessPingToDatawire(List<ServiceProvider> objProviders, string datawireId)
        {
            try
            {
                string xmlResponse = string.Empty;
                int countRetry = 0;
                for (var index = 0; index < objProviders.Count; index++)
                {
                    var objService = objProviders[index];
                    objStopWatch.Start();
                    xmlResponse = this.SendPingRequest(objService.URL, datawireId);
                    if (string.IsNullOrEmpty(xmlResponse)) { continue; }
                    if (countRetry >= 2)
                    {
                        //Remove current url from list
                        var objItem = objProviders.Where(x => x.URL.Equals(objService.URL)).FirstOrDefault();
                        objProviders.Remove(objItem);
                    }

                    objStopWatch.Stop();
                    long timeTaken = objStopWatch.ElapsedMilliseconds;

                    //Parse the response and update the list provider with TransactionTime
                    this.GetServiceUrlData(xmlResponse, objService.URL, Convert.ToInt64(objService.MaxTransactionsInPackage), timeTaken);
                    countRetry = 0;

                    objStopWatch.Stop();
                }
            }
            catch
            {
                throw;
            }
        }

        private string SendPingRequest(string pingUrl, string datawireId)
        {
            try
            {
                string requestXml = this.CreatePingRequest(datawireId, Global.ServiceID);
                string responseXml = CnSendTransaction.SendMessageToDatawire(pingUrl, requestXml, 20000);
                return responseXml;
            }
            catch
            {
                throw;
            }
        }

        private string CreatePingRequest(string datawireID, string serviceID)
        {
            try
            {
                ReqClientIDType objRequestClientID = new ReqClientIDType();
                objRequestClientID.DID = datawireID;
                objRequestClientID.App = Global.AppNameNVersion;
                objRequestClientID.Auth = "000082004330015" + "|" + "01351646";
                objRequestClientID.ClientRef = "0059858V003000";

                PingRequest objPing = new PingRequest();
                objPing.ServiceID = serviceID;

                Request objRequest = new Request();
                objRequest.ReqClientID = objRequestClientID;
                objRequest.Ping = objPing;
                string requestXml = Serialization.Serialize<Request>(objRequest);
                return requestXml;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Set the service discovery response to the custom type List of ServiceProvider
        /// based on the parameter supplied as xmlResponse requestXml ,maxTran
        /// and merged all data into one and set it to class level variable 
        /// </summary>
        /// <param name="xmlResponse">the xml response which return from service discovery response</param>
        /// <param name="requestUrl">the url on which you have request for service discovery</param>
        /// <param name="maxTran">Its a number of transaction allowed on requested url</param>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        private void GetServiceUrlData(string xmlResponse, string requestUrl, long maxTran, long timeTakenInResponse)
        {
            try
            {
                Response response = Serialization.Deserialize<Response>(xmlResponse);
                PingResponse pingResponse = response.PingResponse;
                if (response.StatusTypeField.StatusCode == "OK")
                {
                    foreach (var serviceCost in pingResponse.ServiceCost)
                    {
                        ServiceProvider newSp = new ServiceProvider();
                        newSp.MaxTransactionsInPackage = maxTran.ToString();
                        newSp.URL = requestUrl;
                        string time = pingResponse.ServiceCost.Where(x => x.ServiceID == serviceCost.ServiceID).First().TransactionTimeMs;
                        newSp.TransactionTime = Convert.ToInt64(time) + timeTakenInResponse;
                        _serviceProvider.Add(newSp);
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Handle iso8583 response codes
        /// </summary>
        /// <returns>Iso8583Response object</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        private Iso8583Response HandleResponseCode(Iso8583Response iso8583Response)
        {
            if (iso8583Response.StatusCode == PaymentAPIResources.AuthenticationError)
            {
                iso8583Response.Message = PaymentAPIResources.AuthenticationMessage;
                iso8583Response.AdditionalResponseData = PaymentAPIResources.AuthenticationError;
            }
            else if (iso8583Response.ResponseCode == PaymentAPIResources.ResponseCode91 || iso8583Response.ResponseCode == PaymentAPIResources.ResponseCode14)
                iso8583Response.Message = PaymentAPIResources.FormatError;
            else if (iso8583Response.ResponseCode == PaymentAPIResources.ResponseCode51)
                iso8583Response.Message = PaymentAPIResources.ResponseCode51Message;
            return iso8583Response;
        }

        private Iso8583Response HandleResponse(CnTransactionEntities cnTrans, int transId)
        {
            Iso8583Response isoResponse = ResponseHandler.ParseAndSaveResponse(cnTrans, transId);
            isoResponse = HandleResponseCode(isoResponse);
            return isoResponse;
        }

        //
        //T#6162 Removed all methods as part of Canadian code release changes.
        //
        ///// <summary>
        ///// Purpose             :   Get the transaction details from Android for pulled completion
        ///// Function Name       :   GetTransactionDetails
        ///// Created By          :   Madhuri Tanwar
        ///// Created On          :   09/22/2015
        ///// Modificatios Made   :   ****************************
        ///// Modidified On       :   **** "MM/DD/YYYY"
        ///// </summary>
        ///// <param name="request"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public IHttpActionResult GetTransactionDetails(HttpRequestMessage request)
        //{
        //    _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
        //    _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

        //    _logMessage.AppendLine("Initializing Auth famility object");
        //    AuthRequest authRequest = new AuthRequest();
        //    AuthResponse authresponse = new AuthResponse();
        //    AuthHeader authHeader = new AuthHeader();
        //    AuthTransactionDetails transDetails = new AuthTransactionDetails();
        //    _logMessage.AppendLine("Initialization done Auth famility object");

        //    try
        //    {
        //        XmlDocument xmlDoc = new XmlDocument();

        //        _logMessage.AppendLine("Loading xml request.");
        //        xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

        //        _logMessage.AppendLine("Initialization variables from xml request.");
        //        authRequest.DriverNumber = xmlDoc.SelectSingleNode("AuthorizationDetails/DriverNumber").InnerText;
        //        authRequest.DeviceId = xmlDoc.SelectSingleNode("AuthorizationDetails/DeviceId").InnerText;
        //        authRequest.Token = xmlDoc.SelectSingleNode("AuthorizationDetails/Token").InnerText;
        //        authRequest.FleetId = xmlDoc.SelectSingleNode("AuthorizationDetails/FleetId").InnerText;
        //        authRequest.TransId = Convert.ToInt32(xmlDoc.SelectSingleNode("AuthorizationDetails/TransID").InnerText);

        //        _logTracking.AppendLine("GetTransactionDetails start. DriverNo: " + authRequest.DriverNumber + " and  DeviceId " + authRequest.DeviceId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());

        //        _logMessage.AppendLine("Authenticating Token");

        //        if (!string.IsNullOrEmpty(authRequest.Token))
        //        {
        //            bool isTokenValid = _commonService.IsTokenValid(authRequest.DriverNumber, authRequest.Token);
        //            if (!isTokenValid)
        //            {
        //                _logMessage.AppendLine("Authentication failed.");
        //                _logger.LogInfoMessage(_logMessage.ToString());

        //                authresponse = CommonFunctions.FailedResponse(authRequest, authresponse, authHeader, PaymentAPIResources.UnauthorizedAccess);
        //                return Ok(authresponse);
        //            }
        //        }
        //        else
        //        {
        //            _logMessage.AppendLine("Token was null/empty.");
        //            _logger.LogInfoMessage(_logMessage.ToString());

        //            authresponse = CommonFunctions.FailedResponse(authRequest, authresponse, authHeader, PaymentAPIResources.UnauthorizedAccess);
        //            return Ok(authresponse);
        //        }

        //        _logMessage.AppendLine("Decrypting DriverNo and DriverPin.");

        //        char[] deliem = { '^' };
        //        char[] deliemDriver = { ':' };
        //        string[] encArray = authRequest.Token.Split(deliem);
        //        string encDriverAndPin = encArray[1];
        //        string[] encArrayDriver = encDriverAndPin.Split(deliemDriver);
        //        string encDriverNo = encArrayDriver[0];
        //        string encDriverPin = encArrayDriver[1];
        //        string driverNo = StringExtension.DecryptDriver(encDriverNo);
        //        string driverPin = StringExtension.DecryptDriver(encDriverPin);

        //        _logMessage.AppendLine("Getting DriverDto.");
        //        var driverDto = _driverService.GetDriverDto(authRequest.DriverNumber, Convert.ToInt32(authRequest.FleetId));
        //        if (!driverDto.IsNull())
        //        {
        //            bool isValid = StringExtension.ValidatePassCode(driverPin, driverDto.PIN);
        //            if (isValid)
        //            {
        //                _logMessage.AppendLine("Fetching transaction detail by TransId.");
        //                MTDTransactionDto trxresponse = _transactionService.GetTransaction(authRequest.TransId);

        //                if (trxresponse.IsNull() || (trxresponse.fk_FleetId != Convert.ToInt32(authRequest.FleetId) && trxresponse.DriverNo != authRequest.DriverNumber))
        //                {
        //                    _logMessage.AppendLine("MTDTransactionDto trxresponse is null.");
        //                    _logger.LogInfoMessage(_logMessage.ToString());

        //                    authresponse = CommonFunctions.FailedResponse(authRequest, authresponse, authHeader, PaymentAPIResources.Trx_details + " " + authRequest.TransId);
        //                    return Ok(authresponse);
        //                }

        //                if ((bool)trxresponse.IsCompleted || (bool)trxresponse.IsVoided)
        //                {
        //                    _logMessage.AppendLine("MTDTransactionDto trxresponse type is invalid.");
        //                    _logger.LogInfoMessage(_logMessage.ToString());

        //                    authresponse = CommonFunctions.FailedResponse(authRequest, authresponse, authHeader, PaymentAPIResources.Transaction_already_completed + " " + authRequest.TransId);
        //                    return Ok(authresponse);
        //                }
        //                else
        //                {
        //                    _logMessage.AppendLine("initializing transaction variables.");
        //                    decimal amount = Convert.ToDecimal(trxresponse.Amount);
        //                    transDetails.TransactionID = Convert.ToString(trxresponse.Id);
        //                    transDetails.DriverNumber = trxresponse.DriverNo;
        //                    transDetails.VehicleNo = trxresponse.VehicleNo;
        //                    transDetails.Amount = Convert.ToString(decimal.Round(amount, 2, MidpointRounding.AwayFromZero));
        //                    transDetails.CardFirstFourDigits = trxresponse.FirstFourDigits;
        //                    transDetails.CardLastFourDigits = trxresponse.LastFourDigits;
        //                    transDetails.TransactionResponse = trxresponse.AddRespData;
        //                    transDetails.TransactionDate = Convert.ToString(trxresponse.TxnDate);
        //                    transDetails.EntryMode = trxresponse.EntryMode;
        //                    transDetails.PaymentType = trxresponse.PaymentType;
        //                    transDetails.AuthID = trxresponse.AuthId;
        //                    transDetails.CardType = trxresponse.CardType;
        //                    transDetails.TransactionType = trxresponse.TxnType;
        //                    authHeader.DeviceId = authRequest.DeviceId;
        //                    authHeader.TransID = authRequest.TransId;
        //                    authHeader.Message = PaymentAPIResources.Authorization_Transaction_Details + " " + authRequest.TransId;
        //                    authHeader.Status = PaymentAPIResources.Success;

        //                    authresponse.Header = authHeader;
        //                    authresponse.TransactionDetails = transDetails;

        //                    _logTracking.AppendLine("GetTransactionDetails complete. DriverNo: " + authRequest.DriverNumber + " and  DeviceId " + authRequest.DeviceId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());
        //                    _logger.LogInfoMessage(_logTracking.ToString());
        //                    _logger.LogInfoMessage(_logMessage.ToString());
        //                    return Ok(authresponse);
        //                }
        //            }
        //            else
        //            {
        //                _logMessage.AppendLine("Authentication failed for driver.");
        //                _logger.LogInfoMessage(_logMessage.ToString());

        //                authresponse = CommonFunctions.FailedResponse(authRequest, authresponse, authHeader, PaymentAPIResources.UnauthorizedAccess);
        //                return Ok(authresponse);
        //            }
        //        }
        //        else
        //        {
        //            _logMessage.AppendLine("driverDto was null.");
        //            _logger.LogInfoMessage(_logMessage.ToString());

        //            authresponse = CommonFunctions.FailedResponse(authRequest, authresponse, authHeader, PaymentAPIResources.InvalidDriver);
        //            return Ok(authresponse);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogInfoFatel(_logMessage.ToString(), ex);
        //        authresponse = CommonFunctions.FailedResponse(authRequest, authresponse, authHeader, PaymentAPIResources.Exception);
        //        return Ok(authresponse);
        //    }
        //}

        ///// <summary>
        ///// Purpose             :   Extract the value from XMLrequest and set to Local variables
        ///// Function Name       :   SetLocalVarFromCapkXMLRequest
        ///// Created By          :   Madhuri Tanwar
        ///// Created On          :   10/26/2015
        ///// Modificatios Made   :   ****************************
        ///// Modidified On       :   "MM/DD/YYYY" 
        ///// </summary>
        ///// <param name="request"></param>
        ///// <returns></returns>
        //private bool SetLocalVarFromCapkXMLRequest(XmlDocument xmlDoc)
        //{
        //    try
        //    {
        //        var dvcID = xmlDoc.SelectSingleNode("PaymentRequest/Header/DeviceId");
        //        if (!dvcID.IsNull())
        //            objLocalVars._deviceId = dvcID.InnerText;
        //        else
        //        {
        //            capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/DeviceId".MissingElementMessage());
        //            return false;
        //        }

        //        var reqID = xmlDoc.SelectSingleNode("PaymentRequest/Header/RequestId");
        //        if (!reqID.IsNull())
        //            objLocalVars._requestId = reqID.InnerText;
        //        else
        //        {
        //            capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/RequestId".MissingElementMessage());
        //            return false;
        //        }

        //        var drvrID = xmlDoc.SelectSingleNode("PaymentRequest/Header/DriverId");
        //        if (!drvrID.IsNull())
        //            objLocalVars._driverNo = drvrID.InnerText;
        //        else
        //        {
        //            capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/DriverId".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
        //            return false;
        //        }

        //        var token = xmlDoc.SelectSingleNode("PaymentRequest/Header/Token");
        //        if (!token.IsNull())
        //            objLocalVars._token = token.InnerText;
        //        else
        //        {
        //            capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/Token".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
        //            return false;
        //        }

        //        var requestType = xmlDoc.SelectSingleNode("PaymentRequest/Header/RequestType");
        //        if (!requestType.IsNull())
        //            objLocalVars._requestType = requestType.InnerText;
        //        else
        //        {
        //            objLocalVars._requestType = null;
        //        }

        //        var rapidConnectAuthId = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/RapidConnectAuthId");
        //        if (!rapidConnectAuthId.IsNull())
        //            objLocalVars.RapidConnectAuthId = rapidConnectAuthId.InnerText;
        //        else
        //        {
        //            objLocalVars.RapidConnectAuthId = null;
        //        }

        //        var transType = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/TransactionType");
        //        if (!transType.IsNull())
        //            objLocalVars._txnType = transType.InnerText;
        //        else
        //        {
        //            capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/TransactionType".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
        //            return false;
        //        }

        //        if (objLocalVars._txnType != PaymentTransactionType.Void.ToString())
        //        {
        //            if (!transType.IsNull())
        //            {
        //                objLocalVars._tranType = (TxnTypeType)Enum.Parse(typeof(TxnTypeType), transType.InnerText);
        //            }
        //        }

        //        var indType = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/IndustryType");
        //        if (!indType.IsNull())
        //        {
        //            objLocalVars._industryType = indType.InnerText;
        //            if (string.IsNullOrEmpty(objLocalVars._industryType))
        //            {
        //                capkresponse.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidIndustryType, objLocalVars._deviceId, objLocalVars._requestId);
        //                return false;
        //            }
        //        }
        //        else
        //        {
        //            capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/IndustryType".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
        //            return false;
        //        }

        //        var fleetId = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/FleetId");
        //        if (!fleetId.IsNull())
        //            objLocalVars.FleetId = fleetId.InnerText;
        //        else
        //        {
        //            capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/FleetId".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
        //            return false;
        //        }

        //        return true;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        ///// <summary>
        ///// Purpose             :   To initialize capk TransactionRequest.
        ///// Function Name       :   InitiateCapkTransaction
        ///// Created By          :   Madhuri Tanwar
        ///// Created On          :   10/26/2015
        ///// Modificatios Made   :   ****************************
        ///// Modidified On       :   "MM/DD/YYYY" 
        ///// </summary>
        ///// <param name="request"></param>
        ///// <returns></returns>
        //private TransactionRequest InitiateCapkTransaction(dynamic firstDataDetails)
        //{
        //    TransactionRequest CApktr = new TransactionRequest
        //    {
        //        SerialNumber = objLocalVars._deviceId,
        //        RequestedId = objLocalVars._requestId,
        //        TransactionType = objLocalVars._txnType,
        //        TransType = objLocalVars._tranType,
        //        IndustryType = objLocalVars._industryType,
        //        DriverNumber = objLocalVars._driverNo,
        //        RapidConnectAuthId = objLocalVars.RapidConnectAuthId,
        //        FleetId = objLocalVars.FleetId,
        //        LocalDateTime = DateTime.Now.ToString("yyyyMMddHHmmss"),
        //        TrnmsnDateTime = DateTime.UtcNow.ToString("yyyyMMddHHmmss"),
        //        FdMerchantId = firstDataDetails.FDMerchantID,
        //        MtdMerchantId = firstDataDetails.fk_MerchantID,
        //        TerminalId = firstDataDetails.RCTerminalId,
        //        TppId = firstDataDetails.ProjectId,
        //        GroupId = firstDataDetails.GroupId,
        //        TokenType = firstDataDetails.TokenType,
        //        MCCode = firstDataDetails.MCCode,
        //        EncryptionKey = firstDataDetails.EncryptionKey,
        //        Did = firstDataDetails.DID,
        //        App = firstDataDetails.App,
        //        FbSeq = objLocalVars._fbseq,
        //        Capkdata = objLocalVars._capdata,
        //        ReqFileOff = objLocalVars._reqFileOff,
        //        ServiceId = firstDataDetails.serviceId,
        //        ServiceUrl = string.Empty
        //    };

        //    return CApktr;
        //}

        ///// <summary>
        ///// Validating payment request.
        ///// Purpose             :   Validate capk reqest
        ///// Function Name       :   ValidateCapkRequest
        ///// Created By          :   Madhuri Tanwar
        ///// Created On          :   10/26/2015
        ///// Modificatios Made   :   ****************************
        ///// Modidified On       :    "MM/DD/YYYY" 
        ///// <param name="response"></param>
        ///// <param name="mHeader"></param>
        ///// <param name="firstDataDetails"></param>
        ///// <param name="isFailed"></param>
        ///// <returns>bool</returns>
        //private bool ValidateCapkRequest(EmvCapkResponse capkresponse, LoginHeader mHeader, out dynamic firstDataDetails)
        //{
        //    bool isPass = true;
        //    try
        //    {
        //        if (string.IsNullOrEmpty(objLocalVars._deviceId))
        //        {
        //            isPass = false;
        //            capkresponse.Header = CommonFunctions.FailedResponse(PaymentAPIResources.DeviceIdRequired, objLocalVars._deviceId, objLocalVars._requestId);
        //        }

        //        bool isToken = string.IsNullOrEmpty(objLocalVars._token);
        //        if (isToken)
        //        {
        //            firstDataDetails = _commonService.GetFirstDataDetails(objLocalVars._deviceId, objLocalVars._driverNo, (string)null, Convert.ToInt32(objLocalVars.FleetId), objLocalVars._requestType);
        //        }
        //        else
        //        {
        //            firstDataDetails = _commonService.GetFirstDataDetails(objLocalVars._deviceId, objLocalVars._driverNo, objLocalVars._token, Convert.ToInt32(objLocalVars.FleetId), objLocalVars._requestType);
        //        }

        //        if (firstDataDetails == null)
        //        {
        //            isPass = false;
        //            capkresponse.Header = CommonFunctions.FailedResponse(PaymentAPIResources.UnauthorizedAccess, objLocalVars._deviceId, objLocalVars._requestId);
        //        }

        //        return isPass;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        ///// <summary>
        ///// Purpose             :   Sending Credit and debit card transaction to rapid connect
        ///// Function Name       :   SendPayment
        ///// Created By          :   Salil Gupta
        ///// Created On          :   02/27/2015
        ///// Modificatios Made   :   ****************************
        ///// Modidified On       :   04/02/2015 "MM/DD/YYYY" 
        ///// </summary>Sending data...
        ///// <param name="request"></param>
        ///// <returns></returns>
        //public IHttpActionResult ISO8583SendPayment(HttpRequestMessage request)
        //{
        //    _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
        //    _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
        //    _logMessage.AppendLine("creating an object of XmlDocument Class");
        //    _logMessage.AppendLine("object created successfully of LoginResponse Class as response");
        //    LoginHeader mHeader = new LoginHeader();
        //    _logMessage.AppendLine("object created successfully of LoginHeader Class as mHeader");

        //    try
        //    {
        //        XmlDocument xmlDoc = new XmlDocument();
        //        _logMessage.AppendLine("Loading request XML data.");
        //        xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);
        //        _logTracking.AppendLine("Payment process start. DriverNo: " + objLocalVars._driverNo + " and  DeviceId " + objLocalVars._deviceId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());
        //        _logMessage.AppendLine("Initializing local variables from XML request data");
                
        //        #region Initialize local variable from XML Request.
                
        //        if (!SetLocalVarFromXMLRequest(xmlDoc))
        //        {
        //            _logMessage.AppendLine("Failed to initializ local variables from request XML data.");
        //            _logger.LogInfoMessage(_logMessage.ToString());
        //            return Ok(response);
        //        }

        //        #endregion

        //        #region Validate Request

        //        dynamic firstDataDetails = string.Empty;
        //        _logMessage.AppendLine("Validating request xml data");
        //        if (!ValidateRequest(response, mHeader, out firstDataDetails))
        //        {
        //            _logMessage.AppendLine("Failed to validate request xml data.");
        //            _logger.LogInfoMessage(_logMessage.ToString());
        //            return Ok(response);
        //        }
                
        //        #endregion

        //        #region Preparing transaction

        //        _logMessage.AppendLine("Preparing transactions");
        //        var cardType = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/CardType");
        //        CardInfo cardInfo;
        //        if (!objLocalVars._entryMode.Equals(PaymentEntryMode.EmvContact.ToString()) &&
        //            !objLocalVars._entryMode.Equals(PaymentEntryMode.EmvContactless.ToString()))
        //        {
        //            _logMessage.AppendLine("Preparing NON-EMV transactions");
        //            if (!NonEMVTransactions(out cardInfo, cardType))
        //            {
        //                _logMessage.AppendLine("Failed to prepare NON-EMV transactions.");
        //                _logger.LogInfoMessage(_logMessage.ToString());
        //                return Ok(response);
        //            }
        //        }
        //        else
        //        {
        //            _logMessage.AppendLine("Preparing EMV transactions");
        //            if (!EMVTransactions(out cardInfo, cardType))
        //            {
        //                _logMessage.AppendLine("Failed to prepare NON-EMV transactions.");
        //                _logger.LogInfoMessage(_logMessage.ToString());
        //                return Ok(response);
        //            }
        //        }

        //        #endregion

        //        #region Calculating Tech Fee

        //        _logMessage.AppendLine("Preparing Fleet and Tech fee.");
        //        decimal tip = Convert.ToDecimal(string.IsNullOrEmpty(objLocalVars.Tip) ? "0" : objLocalVars.Tip);
        //        decimal surcharge = Convert.ToDecimal(string.IsNullOrEmpty(objLocalVars.Surcharge) ? "0" : objLocalVars.Surcharge);
        //        decimal fare = Convert.ToDecimal(string.IsNullOrEmpty(objLocalVars.Fare) ? "0" : objLocalVars.Fare);
        //        decimal fareTipSurchare = tip + surcharge + fare;
        //        _logMessage.AppendLine("Calculating Tech Fee.");
        //        GetFleetTechFee(_transactionService, Convert.ToInt32(objLocalVars.FleetId), fareTipSurchare, out fleetFee, out techFee, out txnFee);
                
        //        #endregion

        //        #region Creating Transaction Request as per UMF specification.

        //        _logMessage.AppendLine("Loading XML request data into TransactionRequest");
        //        bool? isDispatchRequest = null;
        //        if (objLocalVars._requestType == "Android" || objLocalVars._requestType == null)
        //            isDispatchRequest = false;
        //        if (objLocalVars._requestType == "Dispatch")
        //            isDispatchRequest = true;
        //        TransactionRequest tr;
        //        try
        //        {
        //            tr = InitiateTransaction(cardInfo, firstDataDetails);
        //            tr.IsDispatchRequest = isDispatchRequest;
        //            tr.FleetFee = Convert.ToDecimal(fleetFee);
        //            tr.TechFee = Convert.ToDecimal(techFee);
        //            tr.Fee = txnFee.ToString();
        //        }
        //        catch (Exception ex)
        //        {
        //            _logMessage.AppendLine("Failed to load XML request data into TransactionRequest");
        //            _logger.LogInfoFatel(_logMessage.ToString(), ex);

        //            response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidTransactionRequest, objLocalVars._deviceId, objLocalVars._requestId);
        //            return Ok(response);
        //        }

        //        _logMessage.AppendLine("Creating transaction request.");
        //        CreateTransactionRequest(tr);
        //        string xmlSerializedTransReq = GetXmlData();
        //        _logMessage.AppendLine("GetXMLData exceuted successfully");
                
        //        #endregion
                
        //        #region Validating XML request
                
        //        _logMessage.AppendLine("Validationg XML request");
        //        int transId;
        //        string ccnum = tr.CardNumber;
        //        string expiry = tr.ExpiryDate;
        //        XmlDocument ValidateXml = new XmlDocument();
        //        ValidateXml.LoadXml(xmlSerializedTransReq);
        //        XmlElement elem = (XmlElement)ValidateXml.DocumentElement.FirstChild;
        //        if (elem.IsNull())
        //        {
        //            _logMessage.AppendLine("Failed to validationg XML request.");
        //            _logger.LogInfoMessage(_logMessage.ToString());

        //            response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidTransactionRequest, objLocalVars._deviceId, objLocalVars._requestId);
        //            return Ok(response);
        //        }
                
        //        #endregion
                
        //        #region Save transaction into MTDTransaction Table
                
        //        _logMessage.AppendLine("calling XmlToTransactionDto takes xmlSerializedTransReq, tr");
        //        tr.TerminalId = tr.TerminalId.FormatTerminalId();
        //        MTDTransactionDto transactionDto = CommonFunctions.XmlToTransactionDto(tr);
        //        _logMessage.AppendLine("Fare,Tip,SurCH,Fee" + transactionDto.FareValue + " , " + transactionDto.Tip + " , " + transactionDto.Surcharge + " , " + transactionDto.Fee);
        //        _logMessage.AppendLine("Inserting Transaction.");
        //        transId = _transactionService.Add(transactionDto);
        //        _logMessage.AppendLine("Update stan parameters" + transactionDto.Stan + "--" + transactionDto.TransRefNo + "--" + tr.TerminalId + "--" + tr.MtdMerchantId);
        //        _terminalService.UpdateStan(transactionDto.Stan, transactionDto.TransRefNo, tr.TerminalId, tr.MtdMerchantId);
        //        string clientRef = GetClientRef(firstDataDetails.ProjectId);
        //        _logMessage.AppendLine("GetClientRef executed successfully" + clientRef);
        //        //end Save transaction into MTDTransaction Table 
                
        //        #endregion
                
        //        //Send data using SOAP protocol to Datawire
        //        return Ok("SUCCESS");
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogInfoFatel(_logMessage.ToString(), ex);
        //        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.Exception, objLocalVars._deviceId, objLocalVars._requestId);
        //        return Ok(response);
        //    }
        //}

        //private Dictionary<Element, string> FillData(Iso8583TransactionEntities objTransactionEntities)
        //{
        //    Dictionary<Element, string> objDataDictionary = new Dictionary<Element, string>();
        //    try
        //    {
        //        var objTransactionProperties = objTransactionEntities.GetType().GetProperties();
        //        foreach (var item in objTransactionProperties)
        //        {
        //            var bitmapProperty = typeof(Iso8583BitmapDefinition).GetProperty(item.Name);
        //            if (!object.Equals(bitmapProperty, null))
        //            {
        //                var bitmapDefination = (Element)bitmapProperty.GetValue(null);
        //                var transactionEntitiesValues = item.GetValue(objTransactionEntities);
        //                if (!string.IsNullOrEmpty(Convert.ToString(transactionEntitiesValues)))
        //                {
        //                    //apply ISO 8583 message format
        //                    //apply case with element id and iterate through each item to create format(like account no format, amount format)
        //                    //if length indicator is there then apply | before length data ex ample “|07” then 7 digit data “1234567” last data |071234567
        //                    transactionEntitiesValues = ApplyISOFormatting(bitmapDefination, transactionEntitiesValues);

        //                    //apply datawire format for BCD format type elements
        //                    if (bitmapDefination.Format == FormatType.BCD || bitmapDefination.Format == FormatType.HD)
        //                    {
        //                        //Apply | on the data example: if data is as “2345667829”, then |23|45|66|78|29
        //                        transactionEntitiesValues = ApplyDatawireFormat(Convert.ToString(transactionEntitiesValues));
        //                    }

        //                    //add the formatted elements to the dictionary
        //                    objDataDictionary.Add(bitmapDefination, Convert.ToString(transactionEntitiesValues));
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }

        //    return objDataDictionary;
        //}

        //private object ApplyISOFormatting(Element entityProperty, object entityValue)
        //{
        //    var result = entityValue;
        //    switch (entityProperty.ElementID)
        //    {
        //        case 3:     //format the transaction amount as per the format specified in ISO8583 specs
        //        case 4:                    
        //            result = FormatAmount(Convert.ToDouble(entityValue));
        //            break;

        //        case 35:    //format the track2 data as per the format specified in ISO8583 specs
        //            result = FormatTrackData(entityProperty, Convert.ToString(entityValue));
        //            break;

        //        case 2:     //format the PAN data as per the format specified in ISO8583 specs
        //            result = FormatPAN(entityProperty, Convert.ToString(entityValue));
        //            break;
                
        //        case 41:    //format the Terminal ID as per the format specified in ISO8583 specs
        //            result = Convert.ToString(entityValue).PadLeft(8, '0');
        //            break;
                
        //        case 42:    //format the Merchant ID as per the format specified in ISO8583 specs
        //            result = Convert.ToString(entityValue).PadLeft(15, '0');
        //            break;
                
        //        case 44:    //format the Additional Response Data as per the format specified in ISO8583 specs
        //            result = (SetAdditionalTableLength(Convert.ToString(entityValue)));
        //            break;

        //        case 54:    //format the Additional amount as per the format specified in ISO8583 specs                    
        //            result = FormatAmountWithLengthIndicator(Convert.ToString(entityValue));
        //            break;
                
        //        default:    //format the numeric fields  as per the format specified in ISO8583 specs
        //            if (entityProperty.Type == DataType.Numeric)
        //                result = FormatNumeric(entityProperty, Convert.ToString(entityValue));

        //            //format the alphanumeric fields  as per the format specified in ISO8583 specs
        //            if (entityProperty.Type == DataType.Alphanumeric)
        //                result = FormatAlphaNumeric(entityProperty, Convert.ToString(entityValue));
        //            break;
        //    }

        //    return result;
        //}

        //private string FormatNumeric(Element element, string entityValue)
        //{
        //    if (entityValue.Length < element.LengthMax)
        //    {
        //        entityValue.PadLeft(element.LengthMax, '0');
        //    }
        //    return entityValue;
        //}

        //private string FormatAlphaNumeric(Element element, string entityValue)
        //{
        //    if (entityValue.Length < element.LengthMax)
        //    {
        //        entityValue.PadRight(element.LengthMax, ' ');
        //    }
        //    return entityValue;
        //}

        //private string FormatAmount(double transAmount)
        //{
        //    string amount = Math.Round(transAmount, 2).ToString();
        //    string formatttedAmount = amount.Replace(".", String.Empty);
        //    if (formatttedAmount.Length < 12)
        //    {
        //        formatttedAmount.PadLeft(12, '0');
        //    }
        //    return formatttedAmount;
        //}

        //private string FormatTrack2Data(Element element, string trackData)
        //{
        //    if (trackData.Length % 2 != 0)
        //    {
        //        trackData = trackData.PadRight(trackData.Length + 1, '0');
        //    }

        //    trackData = String.Concat("|", trackData.Length, trackData);

        //    return trackData.Replace("=", "D");
        //}

        //private string FormatPAN(Element element, string pan)
        //{
        //    if (pan.Length % 2 != 0)
        //    {
        //        pan = pan.PadRight(pan.Length + 1, '0');
        //    }

        //    return String.Concat("|", pan.Length, pan);
        //}

        //private string SetAdditionalTableLength(string additionalResponse)
        //{
        //    return String.Concat("|", additionalResponse.Length, additionalResponse);
        //}

        ///// <summary>
        ///// Format the data amount on the basis of currency code
        ///// </summary>
        ///// <param name="data"></param>
        ///// <returns></returns>
        //private string FormatAmountWithLengthIndicator(string data)
        //{
        //    var amount = FormatAmount(Convert.ToDouble(data));

        //    return SetAdditionalTableLength(amount);
        //}

        ////Apply data wire format is putting | in data like |01|23|45|57|23 .
        ////Get chanks means retrieve a string array which contains data in 1 bytes. Like in 0th position the value is 56, in 1st the value is 34, in 2nd the value is 45 and so on.
        ////Example string[] objarray = new string[]{“45”,”56”,”45”,”45”,”45”};
        //private string ApplyDatawireFormat(string dataToFormat)
        //{
        //    string result = String.Empty;
        //    if (dataToFormat.Length > 2)
        //    {
        //        //Get chunks means retrieve a string array which contains data in 1 bytes. Like in 0th position the value is 56, in 1st the value is 34, in 2nd the value is 45 and so on.
        //        var dataArray = GetChunks(dataToFormat, 2).ToArray();
        //        result = Convert.ToString("|") + String.Join(Convert.ToString("|"), dataArray);//pad with the '|' as per datawire
        //    }
        //    else
        //        result = Convert.ToString("|") + dataToFormat;

        //    return result;
        //}

        //private IEnumerable<String> GetChunks(string dataToFormat, int partlength)
        //{
        //    for (var i = 0; i < dataToFormat.Length; i += partlength)
        //    {
        //        yield return dataToFormat.Substring(i, Math.Min(partlength, dataToFormat.Length - i));
        //    }
        //}

        //private string PrepareBitmap(Dictionary<Element, string> formattedDataFields)
        //{
        //    string bitMap = "0";
        //    List<Element> elementList = formattedDataFields.Keys.ToList();
        //    List<int> elementIdList = new List<int>();
        //    foreach (var item in elementList)
        //    {
        //        elementIdList.Add(item.ElementID);
        //    }

        //    for (int index = 2; index <= 64; index++)
        //    {
        //        if (elementIdList.Contains(index))
        //        {
        //            bitMap = bitMap + "1";
        //        }
        //        else
        //        {
        //            bitMap = bitMap + "0";
        //        }
        //    }

        //    return Convert.ToInt32(bitMap).ToString("X");
        //}

        //private string FormatMessageType(string tranType, string paymentType)
        //{
        //    string messageType;

        //    //if tranaction types are auth only,auth capture and capture onlyn and payment type is credit
        //    if (((tranType == "Authorization") && (paymentType == "Credit")) || ((tranType == "Sale") && (paymentType == "Credit")) || ((tranType == "Completion") && (paymentType == "Credit")))
        //    {
        //        messageType = "0100";
        //    }
        //    else if (((tranType == "Authorization") && (paymentType == "Debit")) || ((tranType == "Sale") && (paymentType == "Debit")) || ((tranType == "Completion") && (paymentType == "Debit ")))
        //    {
        //        messageType = "0200";
        //    }
        //    else if (tranType == "Void")
        //    {
        //        messageType = "0400";
        //    }
        //    //else if(tranType=="Completion")
        //    //{
        //    //    messageType = "0120";//0220
        //    //}
        //    else
        //    {
        //        messageType = String.Empty;
        //    }

        //    return messageType;
        //}

        //private string FormatProcessingCode()
        //{
        //    return "000000";
        //}

        //private string FormatTransDateTime(string transDateTime)
        //{
        //    return transDateTime.Substring(4, 10);
        //}

        //private string FormatLocalTime(string transDateTime)
        //{
        //    return transDateTime.Substring(8, 6);
        //}

        //private string FormatLocalDate(string transDateTime)
        //{
        //    return transDateTime.Substring(4, 4);
        //}

        //private string FormatReferenceNo(string terminalId, string stan)
        //{
        //    return String.Concat("0000", terminalId.Substring(6, 2), stan);
        //}

        //private string FormatExpDate(string transDate)
        //{
        //    return transDate.Substring(2, 4);
        //}

        //private string FormatPosEntryMode()
        //{
        //    return "901";
        //}

        //private string FormatNii()
        //{
        //    return "0047";
        //}

        //private string FormatPosConditionCode()
        //{
        //    return "00";
        //}

        //private string FormatTrack1Data(string track1Data)
        //{
        //    return track1Data = String.Concat("|", track1Data.Length, track1Data);
        //}

        //private string FormatCurrency(string currency)
        //{
        //    return "0124";
        //}

        //private string FormatAddPosInfo()
        //{
        //    return "02";
        //}

        //private string FormatZipCode()
        //{
        //    return " ";//L4W 5A4.
        //}

        //private object FormatTrackData(Element entityProperty, string data)
        //{
        //    _logMessage.AppendLine("Executing the method FormatTrackData.");
        //    string strResult = data;
        //    var length = data.Length;
        //    if ((length % 2) > 0)
        //    {
        //        strResult = strResult.PadRight(length + 1, (char)SigmaStaticValue.Zero);
        //    }

        //    strResult = FormatLengthIndicator(entityProperty, length.ToString()) + strResult; //adding length to the PAN string          
        //    strResult = strResult.Replace((char)SigmaStaticValue.SplitEquals, 'D');
        //    _logMessage.AppendLine("The method FormatTrackData executted successfully.");

        //    return strResult;
        //}

        //private string FormatLengthIndicator(Element entityProperty, string data)
        //{
        //    var length = data.Length;
        //    if ((length % 2) > 0)
        //        data = data.PadLeft(length + 1, (char)SigmaStaticValue.Zero);

        //    //if ascii format element has lenght indicator then apply datawire format explicitly
        //    if (entityProperty.Format == FormatType.ASCII)
        //        data = ApplyDatawireFormat(data);

        //    return data;
        //}

    }
}
