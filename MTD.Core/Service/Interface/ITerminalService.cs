﻿using System.Collections.Generic;

using MTD.Core.DataTransferObjects;

namespace MTD.Core.Service.Interface
{
    public interface ITerminalService
    {
        bool Add(TerminalDto terminalDto, string termId);
        void Update(TerminalDto terminalDto, int terminalId, string termId);
        IEnumerable<TerminalDto> GetTerminalDtoList(int merchantId);
        TerminalDto GetTerminalDto(int terminalId);
        TerminalDto GetTerminalDto(string deviceId);
        string DeleteTerminal(int terminalId, string modifiedBy);
        IEnumerable<TerminalDto> TerminalListByFiter(FilterSearchParametersDto filterSearch);
        DatawireDto GetTerminalStanRef(int merchantId, string serialNumber);
        bool IsRegistered(int merchantId, string serialNumber);
        MTDTransactionDto Completion(string transId, string serialNo, int merchantId);
        void UpdateStan(string stan, string transRefNo, string serialNo, int merchantId);
        bool IsValid(string serialNumber);
        bool IsDatawireRegistered(string serialNumber, string terminalId);
        bool IsExist(string rcTerminalId);
        void AddRcTerminal(RapidConnectTerminalDto rcTerminalDto);
        string GetRcTerminalId(int terminalId);
        DeviceNameListDto GetDevice(int deviceId);
        bool AddDevicename(DeviceNameListDto deviceNameDto);
        void UpdateDevicename(DeviceNameListDto deviceNameDto,int deviceId);
        bool DeleteDevicename(int deviceId);
        IEnumerable<DeviceNameListDto> GetDeviceList();
        IEnumerable<DeviceNameListDto> DeviceListByFiter(string name, int index, int pageSize, string sorting);
        List<TerminalExcelUploadDto> AddDeviceList(List<TerminalExcelUploadDto> objTerminalDto);
        IEnumerable<TerminalDto> GetUserTerminal(FilterSearchParametersDto filterSearch);
        int GetUserTerminalCount(FilterSearchParametersDto filterSearch);
        int GetTerminalCount(FilterSearchParametersDto fsParametes);
        int GetDeviceListCount(string name);
        bool IsMacAddReg(int terminalId, string macAdd);
        bool UpdateCapVersion(string macAdd);
        void SaveServiceDiscoveryUrls(ServiceDiscoveryParametersDto serviceDiscoveryDto);
        List<string> GetServiceDiscoveryUrls();
        DatawireDto GetRandomMerchantDetail();
        fDMerchantDto GetFDMerchant(int merchantId);
        List<string> SaveServiceProviderUrls(ServiceProviderUrlDto serviceProviderUrls);
        bool IsClsCapKey(int terminalId);
    }
}
