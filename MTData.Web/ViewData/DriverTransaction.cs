﻿using System.Collections.Generic;

namespace MTData.Web.ViewData
{
    public class DriverTransaction
    {
        public MtdTransactionsViewData transaction { get; set; }
        public IEnumerable<DriverTransactionViewData> searchResult { get; set; }
        public IEnumerable<MerchantListViewData> MerchantList { get; set; }
        public IEnumerable<FleetListViewData> FleetList { get; set; }
        public IEnumerable<VehicleListViewData> VehicleList { get; set; }
        public IEnumerable<CarListViewData> CarList { get; set; }
        public string MerchantName { get; set; }
        public string FtName { get; set; }
        public string VehicleName { get; set; }
        public int? MerchantID { get; set; }
        public int? FleetID { get; set; }
        public int VehicleID { get; set; }
        public string startDateString { get; set; }
        public string endDateString { get; set; }
    }
}