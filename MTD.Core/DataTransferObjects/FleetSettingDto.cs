﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
   public class FleetSettingDto
    {
        public int fk_FleetID { get; set; }
        public int FleetID { get; set; }
        public int fk_MerchantID { get; set; }
        public string FleetName { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> FleetTransaction { get; set; }
        public Nullable<decimal> CarTransaction { get; set; }
        public Nullable<decimal> TransactionValue { get; set; }
        public Nullable<bool> IsCardSwiped { get; set; }
        public Nullable<bool> IsContactless { get; set; }
        public Nullable<bool> IsChipAndPin { get; set; }
        public Nullable<bool> IsShowTip { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public IEnumerable<FleetDto> fleetlist { get; set; }
        public Nullable<bool> IsAssigned { get; set; }
        public Nullable<int> fK_CurrencyCodeID { get; set; }
        public string CurrencyCode { get; set; }
        public Nullable<byte> PayType { get; set; }
        public Nullable<decimal> FleetFee { get; set; }
        public Nullable<byte> TechFeeType { get; set; }
        public Nullable<decimal> TechFeeFixed { get; set; }
        public Nullable<decimal> TechFeePer { get; set; }
        public Nullable<decimal> TechFeeMaxCap { get; set; }

        
    }
}
