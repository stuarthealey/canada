﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Data.Data
{
   public class DriverRecList
    {
        public Nullable<int> fk_FleetId { get; set; }
        public Nullable<int> UserType { get; set; }
        public string ModifiedBy { get; set; }
        public IEnumerable<DriverRecipient> UserList { get; set; }
        //public IEnumerable<Fleet> fleetlist { get; set; }
    }
}

