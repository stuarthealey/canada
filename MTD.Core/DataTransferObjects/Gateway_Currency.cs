﻿namespace MTD.Core.DataTransferObjects
{
    public class GatewayCurrency
    {
        public int GatewayCurrencyId { get; set; }
        public int? FkGatewayId { get; set; }
        public string CurrencyCode { get; set; }
    }
}