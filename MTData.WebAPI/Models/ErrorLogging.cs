﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Globalization;

using MTData.WebAPI.Common;

namespace MTData.WebAPI.Models
{
    public static class ErrorLogging
    {
        public static void LogDatawireExceptions(string returnOrStatusCode)
        {
            string exceptionMessage = string.Empty;
            try
            {
                switch (returnOrStatusCode.ToLower())
                {
                    case "authenticationerror":
                        exceptionMessage = MessageTypeDW.StatusCodeAuthenticationError.ToString();
                        break;
                    case "unknownserviceid":
                        exceptionMessage = MessageTypeDW.StatusCodeUnknownServiceid.ToString();
                        break;
                    case "wrongsessioncontext":
                        exceptionMessage = MessageTypeDW.StatusCodeWrongSessionContext.ToString();
                        break;
                    case "accessdenied":
                        exceptionMessage = MessageTypeDW.StatusCodeAccessDenied.ToString();
                        break;
                    case "failed":
                        exceptionMessage = MessageTypeDW.StatusCodeFailed.ToString();
                        break;
                    case "retry":
                        exceptionMessage = MessageTypeDW.StatusCodeRetry.ToString();
                        break;
                    case "timemout":
                        exceptionMessage = MessageTypeDW.StatusCodeTimeOut.ToString();
                        break;
                    case "xmlerror":
                        exceptionMessage = MessageTypeDW.StatusCodeXmlError.ToString();
                        break;
                    case "othererror":
                        exceptionMessage = MessageTypeDW.StatusCodeOtherError.ToString();
                        break;
                    case "008":
                        exceptionMessage = MessageTypeDW.StatusCode008.ToString();
                        break;
                    case "200":
                        exceptionMessage = MessageTypeDW.StatusCode200.ToString();
                        break;
                    case "201":
                        exceptionMessage = MessageTypeDW.StatusCode201.ToString();
                        break;
                    case "202":
                        exceptionMessage = MessageTypeDW.StatusCode202.ToString();
                        break;
                    case "203":
                        exceptionMessage = MessageTypeDW.StatusCode203.ToString();
                        break;
                    case "204":
                        exceptionMessage = MessageTypeDW.StatusCode204.ToString();
                        break;
                    case "205":
                        exceptionMessage = MessageTypeDW.StatusCode205.ToString();
                        break;
                    case "206":
                        exceptionMessage = MessageTypeDW.StatusCode206.ToString();
                        break;
                    case "405":
                        exceptionMessage = MessageTypeDW.StatusCode405.ToString();
                        break;
                    case "505":
                        exceptionMessage = MessageTypeDW.StatusCode505.ToString();
                        break;
                }
            }
            catch
            {
                throw;
            }
        }

    }

}


