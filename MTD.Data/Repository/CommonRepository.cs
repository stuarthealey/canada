﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.Core.Objects;

using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Data.Repository
{
    public class CommonRepository : BaseRepository, ICommonRepository
    {
        public CommonRepository(IMtDataDevlopmentsEntities dbMtData) : base(dbMtData)
        {
        }

        /// <summary>
        /// Purpose             :   To get list of countries
        /// Function Name       :   CountryList
        /// Created By          :   Umesh Kumar
        /// Created On          :   12/30/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CountryCode> CountryList()
        {
            try
            {
                return DbMtData.CountryCodes.OrderBy(x => x.Title);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get list of states
        /// Function Name       :   GetStateList
        /// Created By          :   Umesh Kumar
        /// Created On          :   12/30/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="mtch"></param>
        /// <returns></returns>
        public IEnumerable<CountryState> GetStateList(string countryId, string mtch)
        {
            try
            {
                var stateList = from m in DbMtData.CountryStates
                                where m.fk_CountryCodesID == countryId && m.Name.StartsWith(mtch)
                                select m;
                return stateList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mtch"></param>
        /// <returns></returns>
        public IEnumerable<StateCity> GetCityList(string mtch)
        {
            try
            {
                var stateList = from m in DbMtData.StateCities where m.CityName.StartsWith(mtch) select m;
                return stateList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get list of fleet
        /// Function Name       :   FleetList
        /// Created By          :   Umesh Kumar
        /// Created On          :   12/30/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<Fleet> FleetList(int merchantId)
        {
            try
            {
                var fleetList = from m in DbMtData.Fleets
                                where m.IsActive && m.fk_MerchantID == merchantId
                                orderby m.FleetName ascending
                                select m;
                return fleetList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get list of terminals
        /// Function Name       :   TerminalList
        /// Created By          :   Umesh Kumar
        /// Created On          :   12/30/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Terminal> TerminalList(int merchantId)
        {
            try
            {
                var terminalList = from m in DbMtData.Terminals
                                   where m.IsTerminalAssigned == false && m.fk_MerchantID == merchantId && m.IsActive
                                   select m;
                return terminalList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get merchant list
        /// Function Name       :   MerchantList
        /// Created By          :   Umesh Kumar
        /// Created On          :   12/30/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Merchant> MerchantList()
        {
            try
            {
                var merchantList = from m in DbMtData.Merchants where m.IsActive select m;
                return merchantList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get list of gateway
        /// Function Name       :   GatewayList
        /// Created By          :   Umesh Kumar
        /// Created On          :   12/30/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Gateway> GatewayList()
        {
            try
            {
                var gatewayList = from m in DbMtData.Gateways where m.IsActive select m;
                return gatewayList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To check for email exit
        /// Function Name       :   IsExist
        /// Created By          :   Umesh Kumar
        /// Created On          :   12/30/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="emailId"></param>
        /// <returns></returns>
        public bool IsExist(string emailId)
        {
            bool isExist = DbMtData.Users.Any(m => m.Email == emailId && m.IsActive);
            return isExist;
        }

        /// <summary>
        /// Purpose             :   To get tax of merchants
        /// Function Name       :   TaxMerchantList
        /// Created By          :   Umesh Kumar
        /// Created On          :   12/30/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Merchant> TaxMerchantList()
        {
            try
            {
                var merchantList = from m in DbMtData.Merchants
                                   where m.IsActive && m.StateTaxRate == null && m.FederalTaxRate == null
                                   select m;
                return merchantList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get terms of merchant
        /// Function Name       :   GetTerm
        /// Created By          :   Umesh Kumar
        /// Created On          :   12/30/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="termId"></param>
        /// <returns></returns>
        public DefaultSetting GetTerm(int termId)
        {
            try
            {
                DefaultSetting termDb = DbMtData.DefaultSettings.Find(termId);
                return termDb;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To check the member locked or not
        /// Function Name       :   IsLockedMember
        /// Created By          :   Umesh Kumar
        /// Created On          :   12/30/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool IsLockedMember(int id, bool status)
        {
            try
            {
                User userDb = DbMtData.Users.Single(x => x.UserID == id);
                userDb.IsLockedOut = status;
                bool res = Convert.ToBoolean(DbMtData.CommitChanges());
                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get the list of receipents
        /// Function Name       :   RecipientList
        /// Created By          :   Umesh Kumar
        /// Created On          :   12/30/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Recipient> RecipientList()
        {
            try
            {
                var recipientList = (from m in DbMtData.Recipients
                                     join n in DbMtData.Merchants on m.fk_MerchantID equals n.MerchantID
                                     join j in DbMtData.Users on m.fk_MerchantID equals j.fk_MerchantID
                                     where j.fk_UserTypeID == 2 && n.IsActive
                                     select new { n.Company, m.fk_MerchantID, m.IsRecipient, j.Email }).AsEnumerable()
                    .Select(
                        objNew =>
                            new Recipient
                            {
                                fk_MerchantID = objNew.fk_MerchantID,
                                Company = objNew.Company,
                                IsRecipient = objNew.IsRecipient,
                                ModifiedBy = objNew.Email
                            })
                    .ToList();

                return recipientList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get the datawire unregistered terminal list
        /// Function Name       :   UnRegisteredTerminalList
        /// Created By          :   Umesh Kumar
        /// Created On          :   04/15/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<Datawire> RegisteredTerminalList(int merchantId)
        {
            try
            {
                var terminalList = from m in DbMtData.Datawires
                                   where !(bool)m.IsAssigned && m.MerchantId == merchantId && m.IsActive
                                   select m;
                return terminalList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reportId"></param>
        /// <param name="userType"></param>
        /// <returns></returns>
        public IEnumerable<Recipient> RecipientList(int reportId, int userType)
        {
            try
            {
                if (userType == 1)
                {
                    var recipientListAdmin = (from r in DbMtData.Recipients
                                              join u in DbMtData.Users on r.fk_UserID equals u.UserID
                                              where u.IsActive && r.fk_ReportID == reportId && r.fk_UserTypeID == userType
                                              select
                                                  new { r.fk_MerchantID, r.IsRecipient, r.fk_UserID, r.fk_ReportID, u.Email, u.FName, u.UserID })
                        .AsEnumerable()
                        .Select(
                            objNew =>
                                new Recipient
                                {
                                    fk_MerchantID = objNew.fk_MerchantID,
                                    Company = objNew.FName,
                                    IsRecipient = objNew.IsRecipient,
                                    ModifiedBy = objNew.Email,
                                    fk_UserID = objNew.fk_UserID,
                                    fk_ReportID = objNew.fk_ReportID,
                                    UserID = objNew.UserID
                                })
                        .ToList();

                    return recipientListAdmin;
                }

                var recipientList = (from m in DbMtData.Recipients
                                     join n in DbMtData.Merchants on m.fk_MerchantID equals n.MerchantID
                                     join j in DbMtData.Users on m.fk_MerchantID equals j.fk_MerchantID
                                     where
                                         j.fk_UserTypeID == userType && n.IsActive && m.fk_ReportID == reportId &&
                                         m.fk_UserTypeID == userType
                                     select new { n.Company, m.fk_MerchantID, m.IsRecipient, m.fk_UserID, m.fk_ReportID, j.Email })
                    .AsEnumerable()
                    .Select(
                        objNew =>
                            new Recipient
                            {
                                fk_MerchantID = objNew.fk_MerchantID,
                                Company = objNew.Company,
                                IsRecipient = objNew.IsRecipient,
                                ModifiedBy = objNew.Email,
                                fk_UserID = objNew.fk_UserID,
                                fk_ReportID = objNew.fk_ReportID
                            })
                    .ToList();

                return recipientList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Recipient> RecipientListDistinct()
        {
            try
            {
                var recipientList = (from m in DbMtData.Recipients
                                     join n in DbMtData.Merchants on m.fk_MerchantID equals n.MerchantID
                                     join j in DbMtData.Users on m.fk_MerchantID equals j.fk_MerchantID
                                     where j.fk_UserTypeID == 2 && n.IsActive && m.fk_UserTypeID == 2
                                     select new { n.Company, m.fk_MerchantID, m.IsRecipient, j.Email }).AsEnumerable()
                    .Select(
                        objNew =>
                            new Recipient
                            {
                                fk_MerchantID = objNew.fk_MerchantID,
                                Company = objNew.Company,
                                IsRecipient = objNew.IsRecipient,
                                ModifiedBy = objNew.Email
                            })
                    .ToList();

                return recipientList.GroupBy(x => x.fk_MerchantID).Select(grp => grp.First()).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Recipient> AdminRecipientListDistinct()
        {
            try
            {
                var recipientList = (from r in DbMtData.Recipients
                                     join u in DbMtData.Users on r.fk_UserID equals u.UserID
                                     where u.fk_UserTypeID == 1 && u.IsActive && r.fk_UserTypeID == 1
                                     select new { r.fk_MerchantID, r.IsRecipient, r.fk_UserID, r.fk_ReportID, u.Email, u.FName, u.UserID })
                    .AsEnumerable()
                    .Select(
                        objNew =>
                            new Recipient
                            {
                                fk_MerchantID = objNew.fk_MerchantID,
                                Company = objNew.FName,
                                IsRecipient = objNew.IsRecipient,
                                ModifiedBy = objNew.Email,
                                fk_ReportID = objNew.fk_ReportID,
                                UserID = objNew.UserID
                            })
                    .ToList();

                var res = (from m in recipientList where m.fk_ReportID == 1 select m).ToList();
                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driverNo"></param>
        /// <param name="vehicleNo"></param>
        /// <param name="token"></param>
        /// <param name="isDispatchRequest"></param>
        /// <param name="fleetId"></param>
        public void SaveLogInDetails(string driverNo, string vehicleNo, string token, bool isDispatchRequest, int fleetId)
        {
            try
            {
                bool isDriverIdExist = DbMtData.DriverLogins.Any(m => m.DriverNo == driverNo && m.FleetId == fleetId && m.IsDispatchRequest == isDispatchRequest);

                if (isDriverIdExist)
                {
                    var driverLog = (from m in DbMtData.DriverLogins
                                     where m.DriverNo == driverNo && m.FleetId == fleetId && m.IsDispatchRequest == isDispatchRequest
                                     select m).FirstOrDefault();

                    driverLog.VehicleNo = vehicleNo;
                    driverLog.LastLogInTime = driverLog.LogInDateTime;
                    driverLog.LastLogOutTime = driverLog.LogOutDateTime;
                    driverLog.LogInDateTime = DateTime.Now;
                    driverLog.LogOutDateTime = null;
                    driverLog.Token = token;
                    DbMtData.CommitChanges();
                }
                else
                {
                    var driverNewLog = new DriverLogin
                    {
                        LogInDateTime = DateTime.Now,
                        DriverNo = driverNo,
                        VehicleNo = vehicleNo,
                        Token = token,
                        IsDispatchRequest = isDispatchRequest,
                        FleetId = fleetId
                    };

                    DbMtData.DriverLogins.Add(driverNewLog);
                    DbMtData.CommitChanges();
                }

                int? driverTransId = (from m in DbMtData.DriverTransactionHistories
                                      where m.DriverNo == driverNo && m.FleetId == fleetId && m.IsDispatchRequest == isDispatchRequest
                                      select m).Max(m => (int?)m.TarnsHistoryId);

                if (driverTransId != null)
                {
                    var driverTransDetail = DbMtData.DriverTransactionHistories.Find(driverTransId);
                    if (driverTransDetail != null)
                    {
                        if (driverTransDetail.LogOffDateTime == null)
                        {
                            driverTransDetail.LogOffDateTime = DateTime.Now;
                        }
                    }
                }

                var driverTransactionHistory = new DriverTransactionHistory
                {
                    DriverNo = driverNo,
                    VehicleNo = vehicleNo,
                    FleetId = fleetId,
                    LogOnDateTime = DateTime.Now,
                    IsDispatchRequest = isDispatchRequest
                };

                DbMtData.DriverTransactionHistories.Add(driverTransactionHistory);
                DbMtData.CommitChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driverNo"></param>
        public int SaveLogOutDetails(string driverNo, bool isDispatchRequest, int fleetId)
        {
            try
            {
                int driverId =
                    (from m in DbMtData.Drivers
                     where m.DriverNo == driverNo && m.fk_FleetID == fleetId && m.IsActive
                     select m.DriverID).FirstOrDefault();

                var driverLogOut = (from m in DbMtData.DriverLogins
                                    where m.DriverNo == driverNo && m.FleetId == fleetId && m.IsDispatchRequest == isDispatchRequest
                                    select m).FirstOrDefault();
                
                if (driverLogOut != null)
                {
                    driverLogOut.LogOutDateTime = DateTime.Now;
                    driverLogOut.Token = null;
                }
                
                int driverTransHistoryId = (from m in DbMtData.DriverTransactionHistories
                                            where m.DriverNo == driverNo && m.FleetId == fleetId && m.IsDispatchRequest == isDispatchRequest
                                            select m).Max(m => m.TarnsHistoryId);

                var driverHistory = DbMtData.DriverTransactionHistories.Find(driverTransHistoryId);
                driverHistory.LogOffDateTime = DateTime.Now;
                DbMtData.CommitChanges();

                int res = Convert.ToInt32(DbMtData.usp_DriverShiftReport(driverId, isDispatchRequest).FirstOrDefault());
                
                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driverNo"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public bool IsTokenValid(string driverNo, string token)
        {
            try
            {
                return DbMtData.DriverLogins.Any(m => m.DriverNo == driverNo && m.Token == token);
            }
            catch (Exception)
            {
                throw;
            }
        } 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="driverNo"></param>
        /// <param name="token"></param>
        /// <param name="fleetId"></param>
        /// <param name="requestType"></param>
        /// <param name="includeDatawire">T#6488 Add includeDatawire parameter</param>
        /// <returns></returns>
        public usp_FirstDataDetails_Result GetFirstDataDetails(string deviceId, string driverNo, string token, int fleetId, string requestType, bool includeDatawire)
        {
            bool? isDispatchRequest = false;

            if (requestType == "Android")
                isDispatchRequest = false;

            if (requestType == "Dispatch")
                isDispatchRequest = true;
            
            usp_FirstDataDetails_Result fdr = DbMtData.usp_FirstDataDetails(deviceId, driverNo, token, fleetId, isDispatchRequest, includeDatawire).FirstOrDefault();

            return fdr;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driverId"></param>
        /// <param name="reportType"></param>
        /// <param name="reportDate"></param>
        /// <returns></returns>
        public int OnDemandReport(int driverId, int reportType, DateTime reportDate)
        {
            try
            {
                return Convert.ToInt32(DbMtData.usp_OnDemandDriverReport(driverId, reportType, reportDate).FirstOrDefault());
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public IEnumerable<DriverList> GetDrivers(int fleetId)
        {
            try
            {
                IEnumerable<DriverList> driversList = (from driver in DbMtData.Drivers
                                                       where driver.IsActive && driver.fk_FleetID == fleetId
                                                       select new
                                                       {
                                                           driver.DriverNo,
                                                           driver.DriverID
                                                       }).AsEnumerable().Select(objNew =>
                                                    new DriverList
                                                    {
                                                        DriverID = objNew.DriverID,
                                                        DriverNumber = objNew.DriverNo
                                                    });

                return driversList.OrderBy(x => x.DriverNumber);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public IEnumerable<CarList> GetCars(int fleetId)
        {
            try
            {
                IEnumerable<CarList> carList = (from vehicle in DbMtData.Vehicles
                                                where vehicle.IsActive && vehicle.fk_FleetID == fleetId
                                                select new
                                                {
                                                    vehicle.fk_FleetID,
                                                    vehicle.VehicleNumber
                                                }).AsEnumerable().Select(objNew =>
                                                    new CarList
                                                    {
                                                        FleetId = objNew.fk_FleetID,
                                                        VehicleNumber = objNew.VehicleNumber
                                                    });

                return carList.OrderBy(x => x.VehicleNumber);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="macAdd"></param>
        /// <param name="driverNo"></param>
        /// <param name="token"></param>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public bool IsValidateTokenMacAdd(string macAdd, string driverNo, string token, int fleetId)
        {
            return Convert.ToBoolean(DbMtData.usp_ValidateEmail(macAdd, driverNo, token, fleetId).FirstOrDefault());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public Tuple<string, string> GetCardAndPlate(int requestId)
        {
            string plateNumber = string.Empty;
            string cardNumber = string.Empty;

            var result = (from m in DbMtData.MTDTransactions
                          join n in DbMtData.Vehicles on m.fk_FleetId equals n.fk_FleetID
                          where m.Id == requestId
                          select new { m.FirstFourDigits, m.LastFourDigits, n.PlateNumber }).FirstOrDefault();

            if (result != null)
            {
                plateNumber = result.PlateNumber;
                cardNumber = result.FirstFourDigits + new string('0', 8) + result.LastFourDigits;
            }

            return Tuple.Create(plateNumber, cardNumber);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public SurchargeFleetFee SurchargeFees(int fleetId)
        {
            try
            {
                bool isExists = DbMtData.Surcharges.Any(s => s.FleetID == fleetId && s.IsActive);

                if (isExists)
                {
                    var surchargeList = (from s in DbMtData.Surcharges
                                         join f in DbMtData.Fleets on s.FleetID equals f.FleetID
                                         where s.IsActive && s.FleetID == fleetId && f.IsActive      //D#6111 Fixed by linking to only Active surcharge record.
                                         select
                                              new SurchargeFleetFee
                                              {
                                                  FleetFeeType = f.FleetFeeType,
                                                  FleetFeeFixed = f.FleetFeeFixed,
                                                  FleetFeePer = f.FleetFeePer,
                                                  FleetFeeMaxCap = f.FleetFeeMaxCap,
                                                  TechFeeType = f.TechFeeType,
                                                  TechFeeFixed = f.TechFeeFixed,
                                                  TechFeePer = f.TechFeePer,
                                                  TechFeeMaxCap = f.TechFeeMaxCap,
                                                  PayType = f.PayType,

                                                  BookingFeeType = s.BookingFeeType,
                                                  BookingFeeFixed = s.BookingFeeFixed,
                                                  BookingFeePer = s.BookingFeePer,
                                                  BookingFeeMaxCap = s.BookingFeeMaxCap

                                              }).FirstOrDefault();

                    return surchargeList;
                }
                else
                {
                    var defSurcharges = DbMtData.DefaultSettings.Where(x => x.DefaultID == 2).FirstOrDefault();
                    var defTechFee = DbMtData.DefaultSettings.Where(x => x.DefaultID == 6).FirstOrDefault();
                    var fleet = DbMtData.Fleets.Where(x => x.FleetID == fleetId && x.IsActive).FirstOrDefault();
                    var surchargeList = new SurchargeFleetFee()
                                               {
                                                   FleetFeeType = fleet.FleetFeeType,
                                                   FleetFeeFixed = fleet.FleetFeeFixed,
                                                   FleetFeePer = fleet.FleetFeePer,
                                                   FleetFeeMaxCap = fleet.FleetFeeMaxCap,
                                                   TechFeeType = defTechFee.TechFeeType,
                                                   TechFeeFixed = defTechFee.TechFeeFixed,
                                                   TechFeePer = defTechFee.TechFeePer,
                                                   TechFeeMaxCap = defTechFee.TechFeeMaxCap,
                                                   PayType = fleet.PayType,

                                                   BookingFeeType = defSurcharges.BookingFeeType,
                                                   BookingFeeFixed = defSurcharges.BookingFeeFixed,
                                                   BookingFeePer = defSurcharges.BookingFeePer,
                                                   BookingFeeMaxCap = defSurcharges.BookingMaxCap
                                               };

                    return surchargeList;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Recipient> GetCorporateUserList(int corporateUserId)
        {
            try
            {          
               var coporateUserList = (from m in DbMtData.Users 
                                       where m.fk_UserTypeID == 5 && m.IsActive && m.ParentUserID == corporateUserId 
                                       orderby m.UserID
                                        select new{m.FName, m.UserID, m.Email}).AsEnumerable().Select(
                        objNew =>
                            new Recipient
                            {
                                Company = objNew.FName,
                                UserID = objNew.UserID,                       
                                IsRecipient = true,
                                ModifiedBy = objNew.Email
                            })
                    .ToList();

                return coporateUserList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Recipient> GetUpdatedCorporateUserList(int reportId,int corporateUserId)
        {
            try
            {          
               var coporateUserList=(from m in DbMtData.Users 
                                 join k in DbMtData.Recipients on m.UserID equals k.fk_UserID
                                 into t 
                                 from rt in t.DefaultIfEmpty() where m.fk_UserTypeID == 5 && rt.fk_UserTypeID == 5 &&
                                 m.IsActive && rt.fk_ReportID == reportId && m.ParentUserID == corporateUserId orderby m.UserID
                                 select new{m.FName,m.UserID,rt.IsRecipient,m.Email}).AsEnumerable().Select(
                        objNew =>
                            new Recipient
                            {
                                FName = objNew.FName,
                                UserID = objNew.UserID,                       
                                IsRecipient = objNew.IsRecipient,
                                Email = objNew.Email
                            })
                    .ToList();            
                return coporateUserList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string MerchantTimeZone(int merchantId)
        {
            bool isMerchantExist = DbMtData.Merchants.Any(x => x.MerchantID == merchantId);
            if (isMerchantExist)
            {
                string timeZone = DbMtData.Merchants.Where(x => x.MerchantID == merchantId).FirstOrDefault().TimeZone;
                return timeZone;
            }
            return string.Empty;
        }

        public IEnumerable<Recipient> WeeklyReportRecipients(int reportId)
        {
            try
            {
                var recipientList = (from u in DbMtData.Users 
                                     join r in  DbMtData.Recipients on u.UserID equals r.fk_UserID into recipientData
                                     from rec in recipientData.DefaultIfEmpty()
                                     where u.fk_UserTypeID == 1 && u.IsActive && rec.fk_ReportID == reportId && rec.fk_UserTypeID==1
                                     select new {u.UserID,rec.IsRecipient,rec.fk_ReportID, u.Email, u.FName})
                    .AsEnumerable()
                    .Select(
                        objNew =>
                            new Recipient
                            {
                                Company = objNew.FName,
                                IsRecipient = objNew.IsRecipient,
                                ModifiedBy = objNew.Email,
                                fk_ReportID = objNew.fk_ReportID,
                                UserID = objNew.UserID
                            })
                    .ToList();
                return recipientList;

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
