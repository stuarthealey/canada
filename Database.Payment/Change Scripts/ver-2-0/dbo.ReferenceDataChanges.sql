-- Update version number
------------------------
UPDATE dbo.SystemConfig
SET Version = '2.0.0.0'
WHERE SystemConfigID = (SELECT MAX(SystemConfigID) FROM dbo.SystemConfig)