﻿using System;
using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    /// <summary>
    ///TransactionRequest class contains the required information for the transaction.
    /// </summary>
    [DataContract]
    public class TransactionRequest
    {
        #region "Public Properties"

        /// <summary>
        /// gets or sets function detect code 
        /// </summary>
        public PymtTypeType PaymentType { get; set; }

        /// <summary>
        /// set and get transaction Amount
        /// </summary>
        public string TransactionAmount { get; set; }

        /// <summary>
        /// set and get Card Number 
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// set and get  CardType
        /// </summary>
        public CardTypeType CardType { get; set; }

        /// <summary>
        /// set and get ExpiryDate
        /// </summary>
        public string ExpiryDate { get; set; }

        /// <summary>
        /// set and get Card Verification Value
        /// </summary>
        public string CardCVV { get; set; }

        /// <summary>
        /// get or sets the Zip Code for the request
        /// </summary>
        public string ZipCode { get; set; }
        /// <summary>
        /// get or sets the Street address type for the request
        /// </summary>
        public string StreetAddress { get; set; }

        /// <summary>
        /// First Name of the customer 
        /// provided by user at the time of submitting Transaction
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Last Name of the Customer
        /// provided by user at the time of submitting Transaction
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// E-mail id of the customer
        /// provided by user at the time of submitting Transaction
        /// </summary>
        public string EMailId { get; set; }

        /// <summary>
        /// Description about the submited transaction
        /// provided by user at the time of submitting Transaction
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Company Name of the Customer
        /// provided by user at the time of submitting Transaction
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// gets or sets the Merchant category code.
        /// </summary>
        public string MerchantCategoryCode { get; set; }

        /// <summary>
        /// gets or sets the transaction type
        /// </summary>
        public string TransactionType { get; set; }

        public TxnTypeType TransType { get; set; }
        /// <summary>
        /// gets or sets the Merchant Terminal ID
        /// </summary>
        public string TerminalID { get; set; }

        /// <summary>
        /// gets or sets the MerchantID
        /// </summary>
        public string FdMerchantId { get; set; }

        public int MtdMerchantId { get; set; }
        /// <summary>
        /// gets or sets the UserID
        /// </summary>
        public int UserID { get; set; }

        /// <summary>
        /// gets or sets the Track2Data
        /// </summary>
        public string Track2Data { get; set; }

        /// <summary>
        /// gets or sets the Merchant category code.
        /// </summary>
        public string IndustryType { get; set; }

        public string SerialNumber { get; set; }

        public string TppId { get; set; }

        public string GroupId { get; set; }

        public string App { get; set; }

        public string Did { get; set; }

        public string ServiceUrl { get; set; }

        public string ServiceId { get; set; }

        public CCVIndType CCVIndType { get; set; }

        public string DriverNumber { get; set; }

        public string VehicleNumber { get; set; }

        public string SerialNo { get; set; }

        public string RapidConnectAuthId { get; set; }

        public string PinData { get; set; }

        public string KeySerNumData { get; set; }

        public string AdditionalAmout { get; set; }

        
        #endregion


    }
}
