﻿using System;
using System.Collections.Generic;

namespace MTData.Web.ViewData
{
    public class AdminRecipientViewData
    {
        public int fk_UserId { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public bool IsRecipient { get; set; }
        public IEnumerable<AdminRecipientMasterViewData> AdminList { get; set; }
    }

}