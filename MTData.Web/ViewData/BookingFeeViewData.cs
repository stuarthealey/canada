﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace MTData.Web.ViewData
{
    public class BookingFeeViewData
    {
        public int BookingFeeID { get; set; }
       [Required(ErrorMessage = @"Please select the booking fee type")]
        public Nullable<byte> BookingFeeType { get; set; }
        public Nullable<decimal> BookingFeeFixed { get; set; }
        public Nullable<decimal> BookingFeePer { get; set; }
        [Required(ErrorMessage = @"Please enter booking fee, max cap")]
        public Nullable<decimal> BookingFeeMaxCap { get; set; }
        [Required(ErrorMessage = @"Please select a gateway")]
        public Nullable<int> fk_GatewayID { get; set; }
        public Nullable<int> fk_MerchantID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Nullable<decimal> FeePercentage { get; set; }
        [Required(ErrorMessage = @"Please enter the booking fee")]
        public Nullable<decimal> BookingFee { get; set; }
        public string Type { get; set; }
        public string Company { get; set; }
        public string MerchantName { get; set; }
        public string Name { get; set; }
        public IEnumerable<MerchantListViewData> MerchantList { get; set; }
        public IEnumerable<GatewayListViewData> GatewayList { get; set; }
        public IEnumerable<SurchargeTypeViewData> TypeList { get; set; }
    }
}