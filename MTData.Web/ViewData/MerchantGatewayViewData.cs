﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MTData.Web.ViewData
{
    public class MerchantGatewayViewData
    {
        public int MerchantGatewayID { get; set; }
       
        public Nullable<int> fk_MerchantID { get; set; }
        [Required(ErrorMessage = "Please select the gateway")]
        public int fk_GatewayID { get; set; }
        [Required(ErrorMessage = "Please enter the user name")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Please enter a password")]
        public string PCode { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public bool IsDefault { get; set; }
        public Nullable<bool> IsMtdataGateway { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public IEnumerable<GatewayListViewData> GatewayList { get; set; }
        public IEnumerable<SurchargeTypeViewData> TypeList { get; set; }
        public int GatewayType { get; set; }
     
     
    }
}