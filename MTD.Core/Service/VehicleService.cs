﻿using System.Collections.Generic;
using MTD.Core.DataTransferObjects;
using MTD.Core.Extension;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;
using Ninject;
using System.Text;
using System;

namespace MTD.Core.Service
{
    public class VehicleService : IVehicleService
    {
        private readonly IVehicleRepository _vehicleRepository;
        public VehicleService([Named("VehicleRepository")] IVehicleRepository vehicleRepository)
        {
            _vehicleRepository = vehicleRepository;
        }

        /// <summary>
        /// Purpose             :   To Add the Vehicle
        /// Function Name       :   Add
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="vehicleDto"></param>
        public int Add(VehicleDto vehicleDto)
        {
            try
            {
                Vehicle vehicle = vehicleDto.ToVehicle();
                return _vehicleRepository.Add(vehicle);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Vehicle List
        /// Function Name       :   VehicleDtoList
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<VehicleDto> GetVehicleDtoList(int merchantId)
        {
            try
            {
                var vehicleList = _vehicleRepository.VehicleList(merchantId);
                var vehicleDtoList = vehicleList.ToDTOVehicleList();
                return vehicleDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Vehicle
        /// Function Name       :   GetVehicleDto
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        public VehicleDto GetVehicleDto(int vehicleId)
        {
            try
            {
                var vehicle = _vehicleRepository.GetVehicle(vehicleId);
                var vehicleDto = vehicle.ToVehicleDto();
                return vehicleDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update the vehicle
        /// Function Name       :   Update
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="vehicleDto"></param>
        /// <param name="vehicleId"></param>
        /// <param name="terminalId"></param>
        public int Update(VehicleDto vehicleDto, int vehicleId, int terminalId)
        {
            try
            {
                Vehicle vehicle = vehicleDto.ToVehicle();
                return _vehicleRepository.Update(vehicle, vehicleId, terminalId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Delete the Vehicle
        /// Function Name       :   DeleteVehicle
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="vehicleId"></param>
        public void DeleteVehicle(int vehicleId)
        {
            try
            {
                _vehicleRepository.DeleteVehicle(vehicleId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Vehile List
        /// Function Name       :   GetVehileListByFilter
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<VehicleDto> GetVehileListByFilter(FilterSearchParametersDto fsParametes)
        {
            try
            {
                var vehicleList = _vehicleRepository.GetVehileListByFilter(fsParametes.SearchParameters());
                var vehicleDtoList = vehicleList.ToDTOVehicleList();
                return vehicleDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Vehicle By Terminal
        /// Function Name       :   GetVehicleByTerminal
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        public VehicleDto GetVehicleByTerminal(int terminalId)
        {
            var vehicle = _vehicleRepository.GetVehicleByTerminal(terminalId);
            var vehicleDto = vehicle.ToVehicleDto();
            return vehicleDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<VehicleDto> GetVehileCount(FilterSearchParametersDto fsParametes)
        {
            try
            {
                var vehicleList = _vehicleRepository.GetVehileList(fsParametes.SearchParameters());
                var vehicleDtoList = vehicleList.ToDTOVehicleList();
                return vehicleDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDriverDto"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public List<VehicleDto> AddVehicleList(List<VehicleDto> objVehicleDto, int merchantId, int userId, int userType)
        {
            try
            {
                string _Error = string.Empty;
                List<VehicleDto> vehicleExcelUpload = new List<VehicleDto>();
                int response = -1;

                foreach (VehicleDto item in objVehicleDto)
                {
                    Vehicle vehicle = item.ToVehicle();

                    response = _vehicleRepository.IsValidVehicle(vehicle, merchantId, userId, userType);
                    switch (response)
                    {
                        case 5:
                            _Error = "Fleet is not assigned to the merchant.";
                            break;
                        case 6:
                           _Error = "Terminal is not free or not assigned to the merchant.";
                            break;
                        case 8:
                          _Error ="Fleet is not assigned to the user.";
                            break;
                        case 9:
                            _Error = "Car Number is required.";
                            break;
                        case 10:
                            _Error = "Car registration number is required.";
                            break;
                        case 11:
                            _Error = "Plate number is required.";
                            break;
                        case 12:
                            _Error = "Fleet is required.";
                            break;
                        case 13:
                            _Error = "Invalid EmailId of car owner";
                            break;
                        case 14:
                            _Error = "Description must not exceed the length of 200 characters";
                            break;
                        case 15:
                            _Error = "Car No  must not exceed the length 200 characters";
                            break;
                        case 16:
                            _Error = "Car Reg No  must not exceed the length 200 characters";
                            break;
                        case 17:
                            _Error = "Plate  must not exceed the length 200 characters";
                            break;
                        case 18:
                            _Error = " Invalid Car No ";
                            break;
                        case 19:
                            _Error = " Invalid Car Reg No";
                            break;
                        case 20:
                            _Error = "Invalid Plate ";
                            break;
                    }

                    if (response == 7)
                    {
                        Vehicle vehicleDb = _vehicleRepository.VehicelExcel(vehicle, merchantId);
                        if (vehicleDb.fk_FleetID != null)
                        {
                        response = _vehicleRepository.Add(vehicleDb);
                        switch (response)
                        {
                            case 1:
                                _Error="Car Number is already exists.";
                                break;
                            case 2:
                                _Error="Car Registration Number is already exists.";
                                break;
                            case 3:
                                _Error = "Plate Number is already exists.";
                                break;
                        }
                    }
                    }
                    if (response != 0)
                    {
                        vehicleExcelUpload.Add(new VehicleDto() { VehicleNumber = item.VehicleNumber, VehicleRegNo = item.VehicleRegNo, PlateNumber = item.PlateNumber, FleetName = item.FleetName, DeviceName = item.DeviceName, Description = item.Description, FailedError = _Error });
                    }
                }

                return vehicleExcelUpload;
            }
            catch
            {
                throw;
            }
        }

         public int UserVehicleCount(FilterSearchParametersDto fsParameters)
        {
            try
            {
                return _vehicleRepository.UserVehicleCount(fsParameters.SearchParameters());
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fsParametes"></param>
        /// <returns></returns>
         public IEnumerable<VehicleDto> UserVehicleList(FilterSearchParametersDto fsParametes)
         {
             try
             {
                 var vehicleList = _vehicleRepository.UserVehicleList(fsParametes.SearchParameters());
                 var vehicleDtoList = vehicleList.ToDTOVehicleList();
                 return vehicleDtoList;
             }
             catch
             {
                 throw;
             }
         }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
         public IEnumerable<CarOwnersDto> CarOwnerList(int merchantId)
         {
             try
             {
                 return _vehicleRepository.CarOwnerList(merchantId).ToCarOwnerList();
             }
             catch
             {
                 throw;
             }
         }
         /// <summary>
         /// 
         /// </summary>
         /// <returns></returns>
         public IEnumerable<CarOwnersDto> PayCarOwnerList(int FleetId)
         {
             try
             {
                 return _vehicleRepository.PayCarOwnerList(FleetId).ToPayCarOwnerList();
             }
             catch
             {
                 throw;
             }
         }

         public VehicleDto VehicleListInFleet(string token, string carNumber, int fleetID)
         {
             try
             {
                 var vehicleList = _vehicleRepository.VehicleListInFleet(token, carNumber, fleetID);
                 var vehicleDtoList = vehicleList.ToVehicleDto();
                 return vehicleDtoList;
             }
             catch
             {
                 throw;
             }
         }

         public bool IsDuplicateCarNumber(int fleetId, string CarNumber)
         {
             return _vehicleRepository.IsDuplicateCarNumber(fleetId, CarNumber);
         }

         public bool FindFleet(int fleetId)
         {
             return _vehicleRepository.FindFleet(fleetId);
         }

         public VehicleDto AddDespetchVehicle(VehicleDto vehicleDto, string token)
         {
             try
             {
                 Vehicle vehicle = vehicleDto.ToVehicle();
                 var vehicleDtoList = _vehicleRepository.AddDespetchVehicle(vehicle, token);
                 var vehicleDtos = vehicleDtoList.ToVehicleDto();
                 return vehicleDtos;
             }
             catch
             {
                 throw;
             }
         }
    }
}
