﻿using System;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class TableRT_SP
    {
        #region Private Constants and Variables
        #region Constants
        private const string tblId = "|53|50";

        #endregion

        #region Variables
        private const string securitylevelTag01 = "|30|31";
        private string securitylevelTagLength = "|30|30|31";
        private const string securitylevelTag01Vlaue = "|33";

        private const string securitylevelTag02 = "|30|32";
        private string securitylevelTag02Length = "|30|30|31";
        private const string securitylevelTag02Vlaue = "|31";

        private const string securitylevelTag03 = "|30|33";
        private string securitylevelTag03Length = "|30|30|31";
        private string securitylevelTag03Vlaue = "|32";

        private const string securitylevelTag04 = "|30|34";//id
        private const string securitylevelTag04Length = "|30|31|31";
        private string securitylevelTag04Vlaue = "|32";

        private const string securitylevelTag05 = "|30|35";//block
        private string securitylevelTag05Length = "|39|39|39";//999
        private string securitylevelTag05Vlaue = "|32";
        
        private const string tag06 = "|30|36";
        private const string tag06Length = "|30|30|34";
        private string tag06Value = "|53|36|37|35";//"|S6|75";
        #endregion

        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'36 30' Table 60
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        /// <summary>
        /// Gets or Sets the value for DCC Conversion Indicator
        /// </summary>
        public string SecurityLevelTag01
        {
            get { return securitylevelTag01; }

        }
        public string SecuritylevelTagLength
        {
            get { return securitylevelTagLength; }

        }
        
        /// <summary>
        /// Gets or Sets the value for DCC Transaction Time Zone
        /// </summary>
        public string SecuritylevelTag01Vlaue
        {
            get { return securitylevelTag01Vlaue; }

        }
                
        public string SecurityLevelTag02
        {
            get { return securitylevelTag02; }

        }
        public string SecuritylevelTag02Length
        {
            get { return securitylevelTag02Length; }

        }
        
        /// <summary>
        /// Gets or Sets the value for DCC Transaction Time Zone
        /// </summary>
        public string SecuritylevelTag02Vlaue
        {
            get { return securitylevelTag02Vlaue; }

        }
        
        public string SecurityLevelTag03
        {
            get { return securitylevelTag03; }

        }
        public string SecuritylevelTag03Length
        {
            get { return securitylevelTag03Length; }

        }
        
        /// <summary>
        /// Gets or Sets the value for DCC Transaction Time Zone
        /// </summary>
        public string SecuritylevelTag03Vlaue
        {
            get { return securitylevelTag03Vlaue; }

        }
        
        public string SecurityLevelTag04
        {
            get { return securitylevelTag04; }

        }
        public string SecuritylevelTag04Length
        {
            get { return securitylevelTag04Length; }

        }
        
        /// <summary>
        /// Gets or Sets the value for DCC Transaction Time Zone
        /// </summary>
        public string SecuritylevelTag04Vlaue
        {
            get { return securitylevelTag04Vlaue; }
            set { securitylevelTag04Vlaue = value; }

        }

        public string SecurityLevelTag05
        {
            get { return securitylevelTag05; }



        }
        public string SecuritylevelTag05Length
        {
            get { return securitylevelTag05Length; }
            set { securitylevelTag05Vlaue = value; }

        }
        
        /// <summary>
        /// Gets or Sets the value for DCC Transaction Time Zone
        /// </summary>
        public string SecuritylevelTag05Vlaue
        {
            get { return securitylevelTag05Vlaue; }
            set { securitylevelTag05Vlaue = value; }

        }
        
        /// <summary>
        /// Gets or Sets the value for Foreign Conversion Rate
        /// </summary>
        public string Tag06
        {
            get { return tag06; }

        }
        
        public string Tag06Length
        {
            get { return tag06Length; }

        }
        
        public string Tag06Value
        {
            get { return tag06Value; }
            set { tag06Value = value; }
        }
        
        #endregion
    }
}