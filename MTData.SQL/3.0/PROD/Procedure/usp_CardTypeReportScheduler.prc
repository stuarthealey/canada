IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CardTypeReportScheduler]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_CardTypeReportScheduler]
GO

CREATE PROCEDURE [dbo].[usp_CardTypeReportScheduler]
(
  @IsOnDemandReport BIT,
  @FromDate DATETIME=NULL,
  @ToDate DATETIME=NULL
)
AS
BEGIN TRY
 SET NOCOUNT ON;
 SET ANSI_WARNINGS OFF
 DECLARE @Res INT,
         @UserId INT,
         @To NVARCHAR(50),
         @Body NVARCHAR(MAX),
         @Error NVARCHAR(200),
         @MailId INT,
         @Valid INT,
         @MsgXml NVARCHAR(40),
         @XmlData NVARCHAR(MAX),
         @UserName NVARCHAR(45),
         @ReportType INT,
         @RepType NVARCHAR(50),
         @RepHeading NVARCHAR(200),
         @FDate DATETIME,
         @TDate DATETIME,        
		 @TotalNoVisa INT,
		 @TotalNoMasterCard INT,
		 @TotalNoAmex INT,
		 @TotalNoDiscover INT,
	     @TotalNoJcb INT,
	     @TotalNoDiner INT,
		 @TotalVisa NVARCHAR(30),
		 @TotalMasterCard NVARCHAR(30),
		 @TotalAmex NVARCHAR(30),
		 @TotalDiscover NVARCHAR(30),
	     @TotalJcb NVARCHAR(30),
	     @TotalDiner NVARCHAR(30),
		 @NextScheduleDateTime DATETIME,
		 @Result INT
    --Declaring cursor for fetching admin recipients one by one from database
    DECLARE Queue_Cursor CURSOR FOR 
    SELECT u.UserID,u.Email FROM Users u INNER JOIN Recipient r ON u.UserID=r.fk_UserId 
	                        WHERE r.IsRecipient=1 AND r.fk_UserTypeId=1 
                            AND   r.fk_ReportId=8 AND u.IsActive=1 AND u.fk_UserTypeID=1 
    OPEN Queue_Cursor
    FETCH NEXT FROM Queue_Cursor INTO @UserId,@To
    WHILE @@FETCH_STATUS = 0   
    BEGIN   
       SET @XmlData=''
       SELECT @UserName=FName FROM Users WHERE UserID=@UserId AND IsActive=1
       --Calling store procedure AdminSummaryReport to get the data in xml format
       EXEC @Result=usp_CardTypeReport @IsOnDemandReport,@FromDate,@ToDate,@XmlData OUTPUT,
                               @ReportType OUTPUT,@FDate OUTPUT,@TDate OUTPUT,
                               @TotalNoVisa OUTPUT, @TotalNoMasterCard OUTPUT,
                               @TotalNoAmex OUTPUT,@TotalNoDiscover OUTPUT,
                               @TotalNoJcb OUTPUT,@TotalNoDiner  OUTPUT,
                               @TotalVisa OUTPUT,@TotalMasterCard OUTPUT,
                               @TotalAmex OUTPUT,@TotalDiscover OUTPUT,
							   @TotalJcb OUTPUT,@TotalDiner OUTPUT,@NextScheduleDateTime OUTPUT	
	  IF(@Result<>0)
	  BEGIN
	    SELECT 1;
		RETURN;
	  END	
	   IF(@IsOnDemandReport=1)
	   BEGIN
	     SET @RepType='On Demand Card Type Report'
	   END	
	   ELSE
	   BEGIN				   			
       SET  @RepType=CASE @ReportType WHEN 1 THEN 'Daily Card Type Report'
                                      WHEN 2 THEN 'Weekly Card Type Report' 
									  ELSE 'Monthly Card Type Report' END
	   END
       SET @RepHeading=@RepType+' '+'(Dated from'+' '+ CONVERT(VARCHAR(11),@FDate,106)+' '+'to'+' '+CONVERT(VARCHAR(11),@TDate,106)+')'
       IF(@XmlData IS NULL OR @XmlData='') 
	     BEGIN
           SET @Valid=0
           INSERT INTO MailMessage(RepUserId,[Status],SendTo,Body,RepUserTypeId,[Date],ReportType) VALUES(@UserId,-1,@To,'No records in database',1,GETDATE(),'CardTypeReport')
         END
       ELSE 
	     BEGIN
           SET @Valid=1
         END
       IF(@Valid=1) 
	     BEGIN
       --Set the xml data in the html format 
       SET @Body ='<html>
		             <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		             <style>td{text-align:left;font-family:verdana;font-size:12px}
                     p{font-family:verdana;font-size:13px}span{font-family:verdana;font-size:13px}</style>
                     <body><table style="background-color:#e0e0e0; padding:20px; height:100%;"><tr><td><p style="font-size:11px;font-weight:bold">'+CONCAT('Dear',' ',@UserName,',')+'</p>
                     <p style="font-size:11px;font-weight:bold">Listed below is the report containing transaction details card type wise for all merchants.</p>
                     <h5 style="color:#003366;text-align:center;font-size:11px;font-family:verdana; padding:10px;">'+@RepHeading+'</h5>
                    <table border = 1 cellpadding="0" cellspacing="0" style="background-color:#aed6eb; border:2px solid #003366; border-collapse:collapse; padding:2px;">
                    <thead style="background-color:#2b79c7; padding:10px; "><tr>
	               <th style="color:grey;font-size:10px;width:90px;text-align:left;font-family:verdana; color:#fff; padding:5px;">Sr.No</th>
                   <th style="color:grey;font-size:10px;width:600px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Merchant</th>
                   <th style="color:grey;font-size:10px;width:600px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Fleet</th>
                   <th style="color:grey;font-size:10px;width:140px;text-align:left;font-family:verdana; color:#fff; padding:2px;">No of Trans</th>
                   <th style="color:grey;font-size:10px;width:150px;text-align:left;font-family:verdana; color:#fff; padding:2px;">VISA</th>
                   <th style="color:grey;font-size:10px;width:140px;text-align:left;font-family:verdana; color:#fff; padding:2px;">No of Trans</th>
				   <th style="color:grey;font-size:10px;width:150px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Master Card</th>
                   <th style="color:grey;font-size:10px;width:140px;text-align:left;font-family:verdana; color:#fff; padding:2px;">No of Trans</th>
				   <th style="color:grey;font-size:10px;width:150px;text-align:left;font-family:verdana; color:#fff; padding:2px;">American Express</th>
                   <th style="color:grey;font-size:10px;width:140px;text-align:left;font-family:verdana; color:#fff; padding:2px;">No of Trans</th>
				   <th style="color:grey;font-size:10px;width:150px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Discover</th>
                   <th style="color:grey;font-size:10px;width:140px;text-align:left;font-family:verdana; color:#fff; padding:2px;">No of Trans</th>
				   <th style="color:grey;font-size:10px;width:150px;text-align:left;font-family:verdana; color:#fff; padding:2px;">JCB</th>
                   <th style="color:grey;font-size:10px;width:140px;text-align:left;font-family:verdana; color:#fff; padding:2px;">No of Trans</th>
				   <th style="color:grey;font-size:10px;width:150px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Diners Club</th></tr></thead>' 
      SET @Body = @Body + @XmlData +'<tfoot><tr style="height:25px;font-weight:bold">
                                             <td colspan=3 style="font-size:11px;text-align:center">Total</td>
                                             <td style="font-size:11px">' +COALESCE(CAST(@TotalNoVisa AS NVARCHAR(50)),'0')+'</td>
                                             <td style="font-size:11px">'+CONCAT('$',COALESCE(@TotalVisa,'0.00'))+'</td>
                                             <td style="font-size:11px">'+COALESCE(CAST(@TotalNoMasterCard AS NVARCHAR(50)),'0')+'</td>
                                             <td style="font-size:11px">'+CONCAT('$',COALESCE(@TotalMasterCard,'0.00'))+'</td>
											 <td style="font-size:11px">'+COALESCE(CAST(@TotalNoAmex AS NVARCHAR(50)),'0')+'</td>
                                             <td style="font-size:11px">'+CONCAT('$',COALESCE(@TotalAmex,'0.00'))+'</td>
											 <td style="font-size:11px">'+COALESCE(CAST(@TotalNoDiscover AS NVARCHAR(50)),'0')+'</td>
                                             <td style="font-size:11px">'+CONCAT('$',COALESCE(@TotalDiscover,'0.00'))+'</td>
											 <td style="font-size:11px">'+COALESCE(CAST(@TotalNoJcb AS NVARCHAR(50)),'0')+'</td>
                                             <td style="font-size:11px">'+CONCAT('$',COALESCE(@TotalJcb,'0.00'))+'</td>
											 <td style="font-size:11px">'+COALESCE(CAST(@TotalNoDiner AS NVARCHAR(50)),'0')+'</td>
                                             <td style="font-size:11px">'+CONCAT('$',COALESCE(@TotalDiner,'0.00'))+'</td></tr>
											 </tfoot></table><br/><br/>
											 <span>Thanks & Regards,<br/><br/>MTData Developments Pty Ltd.<br/>www.mtdata.us<span>
                                             <h4 style="color:grey;font-size:12px;font-family:verdana">MTData-All rights reserved</h4>
											  <img src="D:\logo-1.png" width="138" height="44" alt="logo">
                                             <p style="font-size:11px;color:black">Note:This is an automated message. Do not Reply or Reply All to this message
                                             as replies are not being accepted.</p></body></html>'
       INSERT INTO MailMessage(RepUserId,[Status],SendTo,Body,RepUserTypeId,[Date],ReportType) VALUES(@UserId,0,@To,@Body,1,GETDATE(),'CardTypeReport')
       SET @MailId=SCOPE_IDENTITY()
       --Execute the sp_send_dbmail to send the summary report in html format 
       EXEC @Res=msdb.dbo.sp_send_dbmail
       @profile_name = 'MTData',
       @recipients=@To,
       @body =@Body,
       @subject = 'Admin Card Type Report',
       @body_format ='HTML'
       IF (@Res != 0)
        BEGIN
            SET @Error =@@ERROR
            UPDATE MailMessage SET Error=@Error,[Date]=GETDATE() WHERE MailId=@MailId
       END;
       ELSE
         BEGIN
         UPDATE MailMessage SET [Status]=1,[Date]=GETDATE() WHERE MailId=@MailId
       END
     END
  FETCH NEXT FROM Queue_Cursor INTO @UserId,@To
 END   
 CLOSE Queue_Cursor   
 DEALLOCATE Queue_Cursor
  IF(@IsOnDemandReport=0)
  BEGIN
    UPDATE ScheduleReport SET NextScheduleDateTime=@NextScheduleDateTime WHERE fk_MerchantId IS NULL 
                                                                      AND CorporateUserId IS NULL 
																	  AND fk_ReportID=8	AND IsCreated=1	
  END
 
 SELECT 0									  	
END TRY
--use catch to log error with details in ProcErrorHandler table
BEGIN CATCH
     INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
     SELECT ERROR_NUMBER()
	       ,ERROR_SEVERITY()
		   ,ERROR_STATE()
		   ,ERROR_PROCEDURE()
		   ,ERROR_LINE()
		   ,ERROR_MESSAGE()
		   ,SUSER_SNAME()
		   ,HOST_NAME()
		   ,GETDATE()
     SELECT 1
END CATCH
GO