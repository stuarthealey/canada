IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_OnDemandDriverReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_OnDemandDriverReport]
GO

CREATE PROCEDURE dbo.usp_OnDemandDriverReport
-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <17-07-2015>
-- Description:	<Prepare and send daily,weekly and monthly summary reports to drivers as per the driver request from android device>
-- 1 for daily report, 2 for weekly report, 3 for monthly report   
-- ==============================================
--	S.Healey	2016-06-06		- Changed company name reference to MTData, LLC
--
(
	@DriverId INT,
	@ReportType INT,
	@ReportDate DATETIME
)
AS
BEGIN
	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SET NOCOUNT ON
		SET ANSI_WARNINGS OFF

		DECLARE	@DriverNo NVARCHAR(60),
				@DriverName NVARCHAR(60),
				@DriverEmail NVARCHAR(60),
				@DriverFirstName NVARCHAR(60),
				@FleetId INT,
				@DateFrom DATETIME,
				@DateTo DATETIME,
				@ReportName NVARCHAR(80),
				@XmlData NVARCHAR(MAX),
				@MsgXml NVARCHAR(50),
				@Valid INT=1,
				@DateCommenced NVARCHAR(80),
				@Body NVARCHAR(MAX),
				@Res INT,
				@MailId INT,
				@NoFare INT,
				@Error NVARCHAR(200),
				@IsDriverRec BIT,
				@TotalFareValue NVARCHAR(30),
				@TotalTip NVARCHAR(30),
				@TotalTax NVARCHAR(30),
				@TotalAmt NVARCHAR(30),
				@TotalFee NVARCHAR(30),
				@NetAmt NVARCHAR(30),
				@BaseLogInTime DATETIME,
				@BaseLogOutTime DATETIME,
				@IsDaily BIT,
				@IsWeekly BIT,
				@IsMonthly BIT,
				@PayType TINYINT,
				@TotalFleetFee NVARCHAR(30),
				@TotalAmtForTechFee NVARCHAR(30)

		DECLARE @TotalData TABLE (
				Id INT,
				JobNumber NVARCHAR(50),
				PickAddress NVARCHAR(100),
				DestinationAddress NVARCHAR(100),
				FareValue MONEY,
				Taxes MONEY,
				Fee MONEY,
				Tip MONEY,
				Amount MONEY,
				TechFee MONEY,	
				FleetFee MONEY,  
				TxnType NVARCHAR(30),
				VehicleNo NVARCHAR(60),
				TxnDate DATETIME,
				EntryMode NVARCHAR(30)
			)

		SET @ReportDate = DATEADD(dd, DATEDIFF(dd, 0, @ReportDate), 0)

		SELECT	@DriverNo = DriverNo,
				@DriverName = CONCAT(FName, ' ', LNAME),
				@DriverEmail = Email,
				@DriverFirstName = FName,
				@FleetId = fk_FleetID 
		FROM dbo.Driver 
		WHERE DriverID = @DriverId 
		  AND IsActive = 1

		SELECT	@IsDaily = IsDaily,
				@IsWeekly = IsWeekly,
				@IsMonthly = IsMonthly
		FROM dbo.DriverReportRecipient
		WHERE fk_DriverId = @DriverId 
		  AND IsRecipient = 1 
		  AND fk_FleetId = @FleetId 
		  AND @DriverEmail IS NOT NULL 

		IF (@ReportType = 1)
		BEGIN
			SET @DateFrom = @ReportDate
			SET @DateTo = @ReportDate + 1
			SET @ReportName = 'Driver Daily Report' 
			SET @IsDriverRec = @IsDaily
			SET @DateCommenced = 'Date Commenced  : ' + CONVERT(VARCHAR(11), @DateFrom, 106) + ' To ' + CONVERT(VARCHAR(11), @DateTo - 1, 106) + '' 
		END
		ELSE IF (@ReportType = 2)
		BEGIN  
			SET @DateFrom = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK, 0, @ReportDate)), 0) - 1
			SET @DateTo = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK, 1, @DateFrom)), 0) - 1
			SET @ReportName = 'Driver Weekly Report'
			SET @IsDriverRec = @IsWeekly
			SET @DateCommenced = 'Date Range : ' + CONVERT(VARCHAR(11), @DateFrom, 106) + ' To ' + CONVERT(VARCHAR(11), @DateTo -1, 106) + '' 
		END
		ELSE IF (@ReportType = 3)
		BEGIN
			SET @DateFrom = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH, 0, @ReportDate)), 0)
			SET @DateTo = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH, 1 , @DateFrom)), 0)
			SET @ReportName = 'Driver Monthly Report'
			SET @IsDriverRec = @IsMonthly
			SET @DateCommenced = 'Month : ' + DATENAME(mm, @ReportDate) + ' ' + DATENAME(yy, @ReportDate) + '' 
		END   
		ELSE
		BEGIN
			SELECT 4
		END

		IF (@IsDriverRec = 1)
		BEGIN
			SELECT @BaseLogInTime = MIN(LogOnDateTime),
				   @BaseLogOutTime = MAX(LogOffDateTime) 
			FROM dbo.DriverTransactionHistory
			WHERE DriverNo = @DriverNo
			  AND FleetId = @FleetId
			  AND LogOnDateTime > @DateFrom - 5
			  AND LogOnDateTime < @DateTo
			  AND LogOffDateTime > @DateFrom
			  AND LogOffDateTime < @DateTo

			EXEC dbo.usp_PayDriverOwnerNetwork @DriverNo, @FleetId, @DateFrom, @DateTo, @ReportType, @XmlData OUT  	  

			IF (@XmlData = '' OR @XmlData IS NULL)
			BEGIN
				SET @Valid = 0
			
				INSERT INTO MailMessage(RepUserId, [Status], SendTo, Body, RepUserTypeId, [Date], ReportType) 
				VALUES (@DriverId, -1, @DriverEmail, 'No records in database', 4, GETDATE(), 'OnDemandDriverReport')

				SELECT 2
			END

			IF (@Valid = 1)
			BEGIN
				SELECT	@NoFare = COALESCE(SUM(Id), 0),
						@TotalFareValue = CONCAT('$', CAST(SUM(FareValue) AS DECIMAL(18, 2))),
						@TotalTip = CONCAT('$',CAST(SUM(Tip) AS DECIMAL(18, 2))),
						@TotalFee = CONCAT('$',CAST(COALESCE(SUM(Fee), 0.00) + COALESCE(SUM(FleetFee), 0.00) AS DECIMAL(18, 2))),
						@TotalTax = CONCAT('$',CAST(SUM(Taxes) AS DECIMAL(18, 2))),
						@TotalAmt = CONCAT('$',CAST(COALESCE(SUM(Amount), 0.00) - COALESCE(SUM(Surcharge), 0.00) AS DECIMAL(18, 2))),
						@NetAmt = CONCAT('$',CAST(COALESCE(SUM(Amount), 0.00) - COALESCE(SUM(FleetFee), 0.00) - COALESCE(SUM(Fee), 0.00) - COALESCE(SUM(Surcharge), 0.00) AS DECIMAL(18, 2)))  
				FROM (	SELECT	COALESCE(Count(Id), 0) AS 'Id',
								COALESCE(SUM(FareValue), 0.00) AS 'FareValue',
								COALESCE(SUM(Tip), 0.00) AS 'Tip',
								COALESCE(SUM(Fee), 0.00) AS 'Fee',
								COALESCE(SUM(Taxes), 0.00) AS 'Taxes',
								COALESCE(SUM(Amount), 0.00) AS 'Amount',
								COALESCE(SUM(FleetFee), 0.00) AS 'FleetFee',
								COALESCE(SUM(Surcharge), 0.00) AS 'Surcharge'
						FROM dbo.MTDTransaction 
						WHERE TxnType = 'Sale'
						  AND IsRefunded = 0
						  AND IsVoided = 0
						  AND DriverNo = @DriverNo
						  AND fk_FleetId = @FleetId
						  AND ResponseCode IN ('000', '00', '002')
						  AND ((TxnDate >= @BaseLogInTime AND TxnDate <= @BaseLogOutTime AND EntryMode NOT IN ('Keyed')) 
								OR (TxnDate > @DateFrom AND TxnDate < @DateTo AND EntryMode = 'Keyed'))

				UNION ALL

				SELECT  COALESCE(COUNT(tblId.Id), 0),
						COALESCE(SUM(tblId.FareValue), 0.00),
						COALESCE(SUM(tblId.Tip), 0.00),
						COALESCE(SUM(tblId.Fee), 0.00),
						COALESCE(SUM(tblId.Taxes), 0.00),
						COALESCE(SUM(tblSrc.Amount), 0.00),
						COALESCE(SUM(tblId.FleetFee), 0.00),
						COALESCE(SUM(tblId.Surcharge), 0.00) 
				FROM dbo.MTDTransaction tblSrc
					INNER JOIN dbo.MTDTransaction tblId ON tblSrc.SourceId=tblId.Id
				WHERE  tblSrc.TxnType = 'Completion'
				  AND tblSrc.DriverNo = @DriverNo
				  AND tblSrc.fk_FleetId = @FleetId
				  AND tblSrc.ResponseCode IN ('000', '00', '002')
				  AND ((tblSrc.TxnDate >= @BaseLogInTime AND tblSrc.TxnDate <= @BaseLogOutTime AND tblId.EntryMode NOT IN ('Keyed')) 
						OR  (tblSrc.TxnDate > @DateFrom AND tblSrc.TxnDate < @DateTo AND tblId.EntryMode = 'Keyed'))
				) tblSaleCompletion

				IF (@ReportType = 1)
				BEGIN
					SET @MsgXml = CAST((SELECT CONCAT('Dear', ' ', @DriverFirstName, ',') AS 'p' FOR XML PATH('p'), ELEMENTS ) AS NVARCHAR(MAX))

					SET @Body = '<html><style>td{text-align:left;font-family:verdana;font-size:12px}
						p{font-family:verdana;font-size:13px}span{font-family:verdana;font-size:13px}
						div{font-size:13px;text-align:center;horizontal-align: middle;background-color:lightgrey}</style>
						<body><p>Listed below is the daily transaction report containing details of transactions done in the below mentioned shifts.</p>
						<H5 style="color:black;text-align:center;font-size:11px;font-family:verdana"> ' + @ReportName + '</H5>
						<H5 style="color:grey;text-align:center;font-size:11px;font-family:verdana">Driver No : ' + @DriverNo + '&nbsp;&nbsp;&nbsp;&nbsp;Driver Name : ' + @DriverName + '&nbsp;&nbsp;&nbsp;&nbsp;' + @DateCommenced + '</H5>
						<table border = 1 cellpadding="0" cellspacing="0" style="background-color:white;border-collapse:collapse"><tr>		  
						<th style="color:grey;font-size:13px;width:300px;text-align:left">MTData Job No</th>
						<th style="color:grey;font-size:13px;width:600px;text-align:left">Pickup Address</th>
						<th style="color:grey;font-size:13px;width:600px;text-align:left">Destination Address</th>
						<th style="color:grey;font-size:13px;width:600px;text-align:left">Trans Type</th>
						<th style="color:grey;font-size:13px;width:140px;text-align:left">Fare Amt</th>
						<th style="color:grey;font-size:13px;width:150px;text-align:left">Taxes</th>
						<th style="color:grey;font-size:13px;width:150px;text-align:left">Tips</th>
						<th style="color:grey;font-size:13px;width:150px;text-align:left">Total Amt</th>
						<th style="color:grey;font-size:13px;width:150px;text-align:left">Fees</th>
						<th style="color:grey;font-size:13px;width:150px;text-align:left">Amount Payable</th></tr>'

					SET @Body = @MsgXml + @Body + @XmlData + '<tfoot>
						<tr style="height:16px;font-weight:bold">
						<td colspan=4 style="font-size:11px;text-align:center">Total </td>
						<td style="font-size:11px;text-align:left">' + @TotalFareValue + '</td>
						<td style="font-size:11px;text-align:left">' + @TotalTax + '</td>
						<td style="font-size:11px;text-align:left">' + @TotalTip + '</td>
						<td style="font-size:11px;text-align:left">' + @TotalAmt + '</td>
						<td style="font-size:11px;text-align:left">' + @TotalFee + '</td>
						<td style="font-size:11px;text-align:left">' + @NetAmt + '</td>'

					SET @Body = @Body + '</tr></tfoot></table><br/><br/>
										<span>Thanks & Regards,<br/><br/>MTData, LLC.<br/>www.mtdata.com<span>
										<h4 style="color:grey;font-size:12px;font-family:verdana">MTData-All rights reserved</h4>
										<p style="font-size:11px;color:black">Note:This is an automated message. Do not Reply or Reply All
										to this message as replies are not being accepted.</p> </body></html>'
				END
				ELSE
				BEGIN
					SET @MsgXml = CAST((SELECT CONCAT('Dear', ' ', @DriverFirstName, ',') AS 'p' FOR XML PATH('p'), ELEMENTS ) AS NVARCHAR(MAX))
					
					SET @Body = '<html><style>td{text-align:left;font-family:verdana;font-size:12px}
						p{font-family:verdana;font-size:13px}span{font-family:verdana;font-size:13px}</style>
						<body><p>Listed below is the driver transaction report containing summarized details of transactions done in below mentioned shifts.</p>
						<H5 style="color:black;text-align:center;font-size:11px;font-family:verdana"> ' + @ReportName + '</H5>
						<H5 style="color:grey;text-align:center;font-size:11px;font-family:verdana">Driver No : ' + @DriverNo + '&nbsp;&nbsp;&nbsp;&nbsp;Driver Name : ' + @DriverName + '&nbsp;&nbsp;&nbsp;&nbsp;' + @DateCommenced + '</H5>
						<table border = 1 cellpadding="0" cellspacing="0" style="background-color:white;border-collapse:collapse"> <tr>
						<th style="color:grey;font-size:13px;width:320px;text-align:left">Seq No</th>
						<th style="color:grey;font-size:13px;width:1200;text-align:left">Logon Date/Time</th>
						<th style="color:grey;font-size:13px;width:1200px;text-align:left">Logoff Date/Time</th>
						<th style="color:grey;font-size:13px;width:500px;text-align:left">Car Number</th>
						<th style="color:grey;font-size:13px;width:300px;text-align:left">No Fares</th>
						<th style="color:grey;font-size:13px;width:340px;text-align:left">Fare Amt</th>
						<th style="color:grey;font-size:13px;width:350px;text-align:left">Total Taxes</th>
						<th style="color:grey;font-size:13px;width:350px;text-align:left">Total Tips</th>
						<th style="color:grey;font-size:13px;width:350px;text-align:left">Total Amt</th>
						<th style="color:grey;font-size:13px;width:350px;text-align:left">Fees</th>
						<th style="color:grey;font-size:13px;width:350px;text-align:left">Amount Payable</th></tr>'

					SET @Body = @MsgXml + @Body + @XmlData + '<tfoot>
						<tr style="height:16px;font-weight:bold">
						<td colspan=4 style="font-size:11px;text-align:center">Total </td>
						<td style="font-size:11px;text-align:left">' + CAST(@NoFare AS VARCHAR) + '</td>
						<td style="font-size:11px;text-align:left">' + @TotalFareValue + '</td>
						<td style="font-size:11px;text-align:left">' + @TotalTax + '</td>
						<td style="font-size:11px;text-align:left">' + @TotalTip + '</td>
						<td style="font-size:11px;text-align:left">' + @TotalAmt + '</td>
						<td style="font-size:11px;text-align:left">' + @TotalFee + '</td>
						<td style="font-size:11px;text-align:left">' + @NetAmt + '</td>'

					SET @Body = @Body + '</tr></tfoot></table><br/><br/>
									<span>Thanks & Regards,<br/><br/>MTData, LLC.<br/>www.mtdata.com<span>
									<h4 style="color:grey;font-size:12px;font-family:verdana">MTData-All rights reserved</h4>
									<p style="font-size:11px;color:black">Note:This is an automated message. Do not Reply or Reply All
									to this message as replies are not being accepted.</p> </body></html>'
				END

				INSERT INTO MailMessage (RepUserId, [Status], SendTo, Body, [RepUserTypeId], [Date], [ReportType]) 
				VALUES (@DriverId, 0, @DriverEmail, @Body, 4, GETDATE(), 'OnDemandDriverReport')

				SET @MailId = SCOPE_IDENTITY()

				EXEC @Res = msdb.dbo.sp_send_dbmail @profile_name = 'MTData', @recipients = @DriverEmail, @body = @Body, @subject = 'On Demand Driver Report', @body_format ='HTML'

				IF (@Res != 0)
				BEGIN
					SET @Error = @@ERROR
					UPDATE dbo.MailMessage SET Error = @Error, [Date] = GETDATE() WHERE MailId = @MailId
					SELECT 4
				END
				ELSE
				BEGIN
					UPDATE dbo.MailMessage SET [Status] = 1, [Date] = GETDATE() WHERE MailId = @MailId
					SELECT 0
				END
			END
		END
		ELSE
		BEGIN
			SELECT 3
		END
	END TRY
	BEGIN CATCH    		
		   INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
		   SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
	END CATCH
END
GO
