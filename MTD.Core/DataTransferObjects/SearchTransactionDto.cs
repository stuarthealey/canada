﻿using System;

namespace MTD.Core.DataTransferObjects
{
   public class SearchTransactionDto
    {
        public int Id { get; set; }
        public Nullable<int> MerchantId { get; set; }
        public Nullable<int> TerminalId { get; set; }
        public string VehicleNo { get; set; }
        public string DriverNo { get; set; }
        public string CrdHldrName { get; set; }
        public string CrdHldrPhone { get; set; }
        public string CrdHldrCity { get; set; }
        public string CrdHldrState { get; set; }
        public string CrdHldrZip { get; set; }
        public string CrdHldrAddress { get; set; }
        public string Stan { get; set; }
        public string TransRefNo { get; set; }
        public string TransOrderNo { get; set; }
        public string PaymentType { get; set; }
        public string TxnType { get; set; }
        public string LocalDateTime { get; set; }
        public string TransmissionDateTime { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string Currency { get; set; }
        public string LastFourDigits { get; set; }
        public string ExpiryDate { get; set; }
        public string CardType { get; set; }
        public string Industry { get; set; }
        public string EntryMode { get; set; }
        public string ResponseCode { get; set; }
        public string AddRespData { get; set; }
        public string CardLvlResult { get; set; }
        public string SrcReasonCode { get; set; }
        public string GatewayTxnId { get; set; }
        public string AuthId { get; set; }
        public string AthNtwId { get; set; }
        public string RequestXml { get; set; }
        public string ResponseXml { get; set; }
        public string SerialNo { get; set; }
        public string SourceId { get; set; }
        public Nullable<System.DateTime> TxnDate { get; set; }
        public string MerchantName { get; set; }
        public string DriverName { get; set; }
        public Nullable<decimal> FleetFee { get; set; }
        public Nullable<decimal> Fee { get; set; }
    }
}
