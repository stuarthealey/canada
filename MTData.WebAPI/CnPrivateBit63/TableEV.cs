﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class TableEV
    {
        #region Private Constants and Variables

        #region Constants
        private const string tblID = "|45|56";
        private  string serviceCode = string.Empty;
        private  string posEntryMode = string.Empty;
        #endregion

        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'45 56' in Request
        /// </summary>
        public string TableID
        {
            get { return tblID; }
        }

        public string ServiceCode 
        {
            get { return serviceCode; }
            set { serviceCode = value; }
        }

        public string POSEntryMode
        {
            get { return posEntryMode; }
            set { posEntryMode = value; }
        }
        #endregion
    }
}