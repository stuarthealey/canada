﻿using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using MTData.Web.Models;
using System;

namespace MTData.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ModelBinders.Binders.DefaultBinder = new TrimModelBinder();  
        }
       
        protected void Application_BeginRequest()
        {
            bool isSecure = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsSecure"]);
            if (isSecure)
                if (!Context.Request.IsSecureConnection)
                {
                    HttpContext context = HttpContext.Current;
                    string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                    Response.Redirect(redirectUrl, false);
                    context.ApplicationInstance.CompleteRequest();
                }
        }
      
    }
}
