﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Data.Data
{
    public class AdminRecipientMaster
    {
        public int fk_UserId { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public Nullable<bool> IsRecipient { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int fk_ReportID { get; set; }
        public int fk_MerchantID { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Company { get; set; }
        public int UserID { get; set; }
    }
}
