IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_TechnologySummaryReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_TechnologySummaryReport]
GO

/****** Object:  StoredProcedure [dbo].[usp_TechnologySummaryReport]    Script Date: 11/2/2015 11:07:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <05-10-2015>
-- Description:	<Prepare technology report fleet wise>
-- =============================================
CREATE PROCEDURE [dbo].[usp_TechnologySummaryReport]
	-- Add the parameters for the stored procedure here
	@MerchantId INT=NULL,
    @FleetId INT=NULL,
	@DateFrom DATETIME=NULL,
	@DateTo DATETIME=NULL
AS
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	SELECT (SELECT COALESCE(Company,'') FROM dbo.Merchant 
	        WHERE MerchantID=(
			                  SELECT fk_MerchantID  
							  FROM dbo.Fleet 
							  WHERE FleetID=T.fk_FleetId)
		    ) AS 'Merchant'
	      ,(SELECT COALESCE(FleetName,'') 
		    FROM dbo.Fleet 
			WHERE FleetID=T.fk_FleetId
			) AS 'FleetName'
			,  T.fk_FleetId
		  ,(SELECT COALESCE(PayType,'') 
		    FROM dbo.Fleet 
			WHERE FleetID=T.fk_FleetId
			) AS 'PayType'
	      ,(SELECT COALESCE(COUNT(DISTINCT VehicleNo),0) 
		    FROM dbo.MTDTransaction 
			WHERE fk_FleetId=T.fk_FleetId 
				  AND TxnDate >= @DateFrom
				  AND TxnDate <= @DateTo
			) AS 'CountVehicle'
		  ,(SELECT COALESCE(COUNT(Id),0) 
		    FROM dbo.MTDTransaction 
			WHERE fk_FleetId=T.fk_FleetId 
			      AND EntryMode NOT IN ('Keyed') 
				  AND TxnType IN ('Sale','Completion','Refund','Void') 
				  AND TxnDate >= @DateFrom
				  AND TxnDate <= @DateTo
			) AS 'AutomaticTrans'
	      ,(SELECT COALESCE(COUNT(Id),0) 
		    FROM dbo.MTDTransaction 
			WHERE fk_FleetId=T.fk_FleetId 
			      AND EntryMode='Keyed' 
				  AND TxnType IN ('Sale','Completion','Refund','Void') 
				  AND TxnDate >= @DateFrom
				  AND TxnDate <= @DateTo
			) AS 'ManualTrans'
		  ,(SELECT COALESCE(COUNT(Id),0) 
		    FROM dbo.MTDTransaction 
			WHERE fk_FleetId=T.fk_FleetId 
			      AND TxnType='Refund' 
				  AND TxnDate >= @DateFrom
				  AND TxnDate <= @DateTo
		    ) AS 'CountRefund'
		  ,(SELECT COALESCE(COUNT(Id),0) 
		    FROM dbo.MTDTransaction 
			WHERE fk_FleetId=T.fk_FleetId 
			      AND TxnType='Void' 
				  AND TxnDate >= @DateFrom
				  AND TxnDate <= @DateTo
			)  AS 'CountVoid'
		  ,(SELECT COALESCE(COUNT(Id),0) 
		    FROM dbo.MTDTransaction 
			WHERE fk_FleetId=T.fk_FleetId 
			      AND TxnType IN ('Sale','Completion','Refund','Void') 
				  AND TxnDate >= @DateFrom
				  AND TxnDate <= @DateTo
			) AS 'TotalTrans'
		  ,(SELECT CONCAT('$',CAST(COALESCE(SUM(TechFee),0.00) AS DECIMAL(18,2))) 
		    FROM dbo.MTDTransaction 
			WHERE fk_FleetId=T.fk_FleetId 
				  AND ((TxnType IN ('Sale','Refund','Void')) 
				  OR (TxnType='Authorization' AND IsCompleted=1))
				  AND TxnDate >= @DateFrom
				  AND TxnDate <= @DateTo
			) AS 'TechFee'
		  ,(SELECT CONCAT('$',CAST(COALESCE(SUM(FleetFee),0.00) AS DECIMAL(18,2))) 
		    FROM dbo.MTDTransaction 
			WHERE fk_FleetId=T.fk_FleetId 
				  AND ((TxnType IN ('Sale','Refund','Void')) 
				  OR (TxnType='Authorization' AND IsCompleted=1))
				  AND TxnDate >= @DateFrom
				  AND TxnDate <= @DateTo
			) AS 'FleetFee'
		  ,(SELECT CONCAT('$',CAST(COALESCE(SUM(Surcharge),0.00) AS DECIMAL(18,2))) 
		    FROM dbo.MTDTransaction 
			WHERE fk_FleetId=T.fk_FleetId 
			      AND ResponseCode IN ('000','002') 
				  AND ((TxnType ='Sale') 
				  OR (TxnType='Authorization' AND IsCompleted=1))
				  AND TxnDate >= @DateFrom
				  AND TxnDate <= @DateTo
			) AS 'Surcharge'
		   ,(SELECT CONCAT('$',CAST(COALESCE(SUM(Fee),0.00) AS DECIMAL(18,2))) 
		    FROM dbo.MTDTransaction 
			WHERE fk_FleetId=T.fk_FleetId 
			      AND ResponseCode IN ('000','002') 
				  AND ((TxnType ='Sale') 
				  OR (TxnType='Authorization' AND IsCompleted=1))
				  AND TxnDate >= @DateFrom
				  AND TxnDate <= @DateTo
			) AS 'Booking Fee'
		   ,(SELECT CONCAT('$',CAST(COALESCE(SUM(Amount),0.00)-(SELECT COALESCE(SUM(Amount),0.00)
		                                                        FROM MTDTransaction 
																WHERE fk_FleetId=T.fk_FleetId
																AND TxnType='Refund' 
																AND ResponseCode IN ('000','002')
																AND TxnDate >= @DateFrom
				                                                AND TxnDate <= @DateTo
																) AS DECIMAL(18,2))) 
		    FROM dbo.MTDTransaction 
			WHERE fk_FleetId=T.fk_FleetId 
			      AND TxnType IN ('Sale','Completion') 
				  AND IsVoided=0 
				  AND ResponseCode IN ('000','002')
				  AND TxnDate >= @DateFrom
				  AND TxnDate <= @DateTo
			) AS 'TotalAmt'
	FROM dbo.MTDTransaction T
	WHERE(T.MerchantId = @MerchantId OR COALESCE(@MerchantId,0) = 0)
	AND (T.fk_FleetId = @FleetId OR COALESCE(@FleetId,'') = '')
	AND (TxnDate >= @DateFrom OR @DateFrom IS NULL)
	AND (TxnDate <= @DateTo OR @DateTo IS NULL)
	AND T.fk_FleetId IS NOT NULL
	AND T.Id NOT IN (SELECT Id FROM dbo.MTDTransaction WHERE TxnType='Authorization' AND IsCompleted=0)
	GROUP BY T.fk_FleetId
	ORDER BY FleetName
	OPTION (RECOMPILE)
END TRY
--use catch to log error with details in ProcErrorHandler table
BEGIN CATCH
     INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
     SELECT ERROR_NUMBER()
	       ,ERROR_SEVERITY()
		   ,ERROR_STATE()
		   ,ERROR_PROCEDURE()
		   ,ERROR_LINE()
		   ,ERROR_MESSAGE()
		   ,SUSER_SNAME()
		   ,HOST_NAME()
		   ,GETDATE()
END CATCH
GO