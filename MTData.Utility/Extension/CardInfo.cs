﻿using System;
using System.Globalization;

namespace MTData.Utility
{
    public struct CardInfo
    {
        public string ErrorMessage { get; set; }
        public string First4 { get; set; }
        public string Last4 { get; set; }
        public string CardNumber { get; set; }
        public string YY { get; set; }
        public string MM { get; set; }
        public string ServiceCode { get; set; }

        public string YYYYMM
        {
            get
            {
                DateTime expiryDateTime;

                if (DateTime.TryParseExact(YY + MM, "yyMM", CultureInfo.InvariantCulture, DateTimeStyles.None, out expiryDateTime))
                    return expiryDateTime.ToString("yyyyMM");
                else
                    return string.Empty;
            }
        }

        public override string ToString()
        {
            return string.Format("CardInfo. First4:{0};Last4:{1};CardNo:{2};YY:{3};MM:{4};Error:{5};", First4, Last4, CardNumber, YY, MM, ErrorMessage);
        }
    }
}
