IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MTDTransaction' AND COLUMN_NAME = 'ResponseBit63')
BEGIN
	ALTER TABLE MTDTransaction ADD ResponseBit63 NVARCHAR (4000) NULL

	PRINT 'MTDTransaction table added ResponseBit63'
END
ELSE
BEGIN
	PRINT 'MTDTransaction table ResponseBit63 already exists'
END

