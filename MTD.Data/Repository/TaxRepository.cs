﻿using System;
using System.Collections.Generic;
using System.Linq;

using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Data.Repository
{
    public class TaxRepository : BaseRepository, ITaxRepository
    {
        public TaxRepository(IMtDataDevlopmentsEntities dbMtData) : base(dbMtData)
        {
        }

        /// <summary>
        /// Purpose             :   To add tax of merchant
        /// Function Name       :   Add
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/17/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="taxMerchant"></param>
        /// <returns></returns>
        public bool Add(Merchant taxMerchant)
        {
            try
            {
                if (taxMerchant.MerchantID != 0)
                {
                    int taxId = taxMerchant.MerchantID;
                    Merchant merchantDb = DbMtData.Merchants.Find(taxId);
                    merchantDb.StateTaxRate = taxMerchant.StateTaxRate;
                    merchantDb.FederalTaxRate = taxMerchant.FederalTaxRate;
                    merchantDb.IsTaxInclusive = taxMerchant.IsTaxInclusive;
                    merchantDb.ModifiedBy = taxMerchant.ModifiedBy;
                    merchantDb.ModifiedDate = DateTime.Now;

                    DbMtData.CommitChanges();

                    return true;
                }

                var taxDb = DbMtData.DefaultSettings.Find(1);
                if (taxDb.StateTaxRate == 0 && taxDb.FederalStateRate == 0)
                {
                    taxDb.StateTaxRate = taxMerchant.StateTaxRate;
                    taxDb.FederalStateRate = taxMerchant.FederalTaxRate;

                    DbMtData.CommitChanges();

                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Tax of merchant
        /// Function Name       :   GetTax
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/17/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public Merchant GetTax(int merchantId)
        {
            try
            {
                Merchant taxDb = DbMtData.Merchants.Find(merchantId);
                return taxDb;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update Tax of merchant
        /// Function Name       :   Update
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/17/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchant"></param>
        /// <param name="merchantId"></param>
        public void Update(Merchant merchant, int merchantId)
        {
            try
            {
                if (merchantId != 0)
                {
                    Merchant merchantDb = DbMtData.Merchants.Find(merchantId);
                    merchantDb.StateTaxRate = merchant.StateTaxRate;
                    merchantDb.FederalTaxRate = merchant.FederalTaxRate;
                    merchantDb.IsTaxInclusive = merchant.IsTaxInclusive;
                    merchantDb.ModifiedBy = merchant.ModifiedBy;
                    merchantDb.ModifiedDate = DateTime.Now;
                }
                else
                {
                    var taxDb = DbMtData.DefaultSettings.Find(1);
                    taxDb.StateTaxRate = merchant.StateTaxRate;
                    taxDb.FederalStateRate = merchant.FederalTaxRate;
                    DbMtData.CommitChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }

            DbMtData.CommitChanges();
        }

        /// <summary>
        /// Purpose             :   To delete Tax of merchant
        /// Function Name       :   DeleteTax
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/17/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="merchantId"></param>
        public void DeleteTax(int merchantId)
        {
            try
            {
                if (merchantId != 0)
                {
                    Merchant taxDb = DbMtData.Merchants.Find(merchantId);
                    taxDb.StateTaxRate = null;
                    taxDb.FederalTaxRate = null;
                    taxDb.IsTaxInclusive = null;
                    DbMtData.CommitChanges();
                }
                else
                {
                    var taxDb = DbMtData.DefaultSettings.Find(1);
                    taxDb.StateTaxRate = 0;
                    taxDb.FederalStateRate = 0;
                    DbMtData.CommitChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To list Tax of merchant
        /// Function Name       :   TaxList
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/17/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"  
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Merchant> TaxList()
        {
            try
            {
                List<Merchant> merchant = (from m in DbMtData.Merchants
                                            where (m.IsActive) && m.StateTaxRate != null && m.FederalTaxRate != null
                                            select m).ToList();

                return merchant;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get default Tax of merchant
        /// Function Name       :   GetDefaultTax
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/17/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"   
        /// </summary>
        /// <returns></returns>
        public DefaultSetting GetDefaultTax()
        {
            try
            {
                var taxDb = (from m in DbMtData.DefaultSettings select m).FirstOrDefault();
                return taxDb;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}



