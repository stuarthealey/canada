﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Common
{
    public static class FailoverUrls
    {
        /// <summary>
        /// Check the response code for failed response codes
        /// </summary>
        /// <param name="respCode">Response code</param>
        /// <returns>true: if exists, false: if not exists</returns>
        public static bool IsFailed(string respCode)
        {
            List<string> failedRespCode = new List<string> { "200", "201", "203", "204", "205", "206", "405", "505" };
            if (!string.IsNullOrEmpty(respCode))
            {
                if (failedRespCode.Contains(respCode))
                    return true;
            }
            return false;
        }
    }
}