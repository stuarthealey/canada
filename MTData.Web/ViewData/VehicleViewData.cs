﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace MTData.Web.ViewData
{
    public class VehicleViewData
    {
        public int VehicleID { get; set; }
        [StringLength(100)]
        [Required(ErrorMessage = "Please enter the Car number")]
        [Display(Name = "Car number")]
        [RegularExpression(@"^[A-Za-z,0-9, - , : , .]+$", ErrorMessage = " Car number is not valid")]
        public string VehicleNumber { get; set; }
        [StringLength(100)]
        [Required(ErrorMessage = "Please enter car registration number")]
        [Display(Name = "Car Registration No")]
        [RegularExpression(@"^[A-Za-z,0-9, - , : , .]+$", ErrorMessage = "Car registration No is not valid")]
        public string VehicleRegNo { get; set; }
        [StringLength(100)]
        [Required(ErrorMessage = "Please enter the plate number")]
        [Display(Name = "Plate number")]
        [RegularExpression(@"^[A-Za-z,0-9, - , : , .]+$", ErrorMessage = "Plate number is not valid")]
        public string PlateNumber { get; set; }
        [Required(ErrorMessage = "Please select a fleet")]
        public int fk_FleetID { get; set; }
        public int? fk_TerminalID { get; set; }
        public string Description { get; set; } 
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        [Display(Name = "Fleet name")]
        public string FleetName { get; set; }
        [Display(Name = "Terminal")]
        public string DeviceName { get; set; }
        public Nullable<int> fk_CarOwenerId { get; set; }
        public HttpPostedFileBase uploadFile { get; set; }
        public IEnumerable<FleetListViewData> FleetList { get; set; }
        public IEnumerable<TerminalListViewData> TerminalList { get; set; }
        public List<VehicleViewData> VehicleListViewData { get; set; }
        public IEnumerable<CarOwnerViewData> CarOwnerList { get; set; }
        public bool IsVehicleError { get; set; }
        public string FailedError { get; set; }
        public string CarOwnerEmail { get; set; }
        public string CarOwnerName { get; set; }
    }
}