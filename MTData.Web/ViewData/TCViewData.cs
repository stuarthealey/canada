﻿namespace MTData.Web.ViewData
{
    public class TCViewData
    {
        public int DefaultID { get; set; }
        public string TermCondition { get; set; }
        public string Disclaimer { get; set; }
    }
}