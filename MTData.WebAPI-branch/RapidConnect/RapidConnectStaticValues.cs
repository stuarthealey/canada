﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTData.Utility.RapidConnect
{
    public class RapidConnectStaticValues
    {
        /// <summary>
        /// gets or sets the SplitAND
        /// </summary>
        public static char SplitAnd = '&';

        /// <summary>
        /// Merchant Category Code for Retail. 
        /// Value="1212" 
        /// </summary>
        public static string MccForRetail = "RETAIL"; //need to update

        /// <summary>
        /// gets or sets the Rapid Connect ID
        /// </summary>
        public static string RapidConnectId = "RMT001";

        /// <summary>
        /// gets or sets the MerchantID
        /// </summary>
        public static string MerchantId = "9058";

        /// <summary>
        /// gets or sets the TerminalID
        /// </summary>
        public static string TerminalId = "00000001";

        /// <summary>
        /// gets or sets the GroupID
        /// </summary>
        public static string GroupId = "20001";

        /// <summary>
        /// gets or sets the POSEntryMode
        /// </summary>
        public static string PosEntryMode = "011";

        /// <summary>
        /// gets or sets the POSCondCode
        /// </summary>
        public static string PosCondCode = "00";

        /// <summary>
        /// gets or sets the TermCatCode
        /// </summary>
        public static string TermCatCode = "01";

        /// <summary>
        /// gets or sets the TermEntryCapablt
        /// </summary>
        public static string TermEntryCapablt = "01";

        /// <summary>
        /// gets or sets the CurrencyCode
        /// </summary>
        public static string CurrencyCode = "840";

        /// <summary>
        /// gets or sets the Zero.
        /// </summary>
        public static char Zero = '0';

        /// <summary>
        ///  Datawire AuthKey for terminal ID 00000001
        /// </summary>
        public static string AuthKeyTidOne = "10001RCTST0000008087|00000001";

        /// <summary>
        ///  Datawire AuthKey for terminal ID 00000002
        /// </summary>
        public static string AuthKeyTidTwo = "10001RCTST0000008087|00000002";

        /// <summary>
        ///  Datawire AuthKey for terminal ID 00000003
        /// </summary>
        public static string AuthKeyTidThree = "10001RCTST0000008087|00000003";

        /// <summary>
        /// Datawire Application ID
        /// </summary>
        public static string App = "RAPIDCONNECTSRS";

        /// <summary>
        /// Service ID
        /// </summary>
        public static string ServiceId = "160";

        public static string Version = "3";


        #region "Card Details"

        /// <summary>
        /// CardHolder's card Number
        /// </summary>
        public static string CardNumber = "5424180273333333";

        /// <summary>
        /// Card expiry date
        /// </summary>
        public static string ExpiryDate = "20160430";

        /// <summary>
        /// status is Ok
        /// </summary>
        public static string OK = "OK";


        /// <summary>
        /// status is retry
        /// </summary>
        public static string Retry = "Retry";


        public static string DatawireId = "00011233527303762182";


        #endregion


        #region "Customer information"

        /// <summary>
        /// Customer Billing Address
        /// </summary>
        public static string BillingAddress = "AVSBillingAddr";

        /// <summary>
        /// Customer Billing postal code
        /// </summary>
        public static string BillingPostalCode = "11747";

        #endregion
    }
}
