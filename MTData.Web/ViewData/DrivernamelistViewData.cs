﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.Web.ViewData
{
    public class DrivernamelistViewData
    {
        public string DriverNo { get; set; }
        public string FName { get; set; }
    }
}