﻿namespace MTData.Web.ViewData
{
    public class TerminalListViewData
    {
        public int TerminalID { get; set; }
        public string MacAdd { get; set; }
    }
}