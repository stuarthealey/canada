﻿using System.Collections.Generic;
using MTD.Core.DataTransferObjects;

namespace MTD.Core.Service.Interface
{
    public interface IDriverService
    {
        bool Add(DriverDto driverDto);

        bool IsEmailExist(string emailId,int fleetId,int driverId);

        bool Update(DriverDto driverDto, int driverId);

        IEnumerable<DriverDto> GetDriverDtoList(FilterSearchParametersDto filterSearch);

        DriverDto GetDriverDto(int driverId, int merchantId);

        DriverDto GetDriverDto(string driverNo, string driverPin);

        DriverDto GetDriverDto(string driverNo, int fleetId);

        void DeleteDriver(int driverId);

        IEnumerable<DriverDto> GetDeriverListByFilter(FilterSearchParametersDto filterSearch);

        bool IsDriverNoValid(string driverNo, string driverPin);

        DriverDto GetDriverDto(string driverNo);

        List<DriverDto> AddDriverList(List<DriverDto> objDriverDto, int merchantId,int userId,int userType);
        
        bool ResetPin(int driverId,string pin);
        
        IEnumerable<DriverRecipientDto> GetDriverReceipent(int merchantId, int fleetId);
        
        bool UpdateDriverRecipient(DriverRecListDto driverRecipient, int fleetId, int userType, int merchantId);
        
        bool ScheduleDriverReport(ScheduleReportDto scheduleDto);
        
        IEnumerable<ReportListDto> DriverReportList();
        
        IEnumerable<ScheduleReportDto> ScheduleReportListByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting);
        
        ScheduleReportDto GetScheduleDto(int scheduleId);
        
        bool ScheduleDriverReportUpdate(ScheduleReportDto scheduleDto);

        IEnumerable<FleetDto> AllFleets();
        
        IEnumerable<ScheduleReportDto> ScheduleDriverReportList();

        bool MerchantDriverRecipient(DriverRecListDto driverRecipient, int fleetId, int userType, int merchantId);
        
        IEnumerable<DriverRecipientDto> GetDriverReceipent();

        IEnumerable<DriverRecipientDto> GetMerchantDriverRecipient(int merchantId);
        
        IEnumerable<DriverLoginDto> GetDriverLoginDto(string DeviceId, string DriverId, int UserType);
       
        IEnumerable<DriverDto> UserDriverList(FilterSearchParametersDto filterSearch);

        bool IsExist(int driverID);

        bool IsDupligate(int fleetId, string driverId);

        DriverDto AddDispatchDriver(DriverDto driverDto);
    }
}
