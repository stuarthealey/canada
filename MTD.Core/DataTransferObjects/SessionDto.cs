﻿namespace MTD.Core.DataTransferObjects
{
    public class SessionDto
    {
        public UserDto SessionUser { get; set; }
        public MerchantDetailsDto SessionMerchant { get; set; }

    }
}
