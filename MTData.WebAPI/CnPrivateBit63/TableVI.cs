﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class TableVI
    {
        #region Private Constants and Variables
        #region Constants
        private const string tblId = "|56|49";
        #endregion
        #endregion

        #region Public Properties
        public string ID
        { get { return tblId; } }
        #endregion
    }
}