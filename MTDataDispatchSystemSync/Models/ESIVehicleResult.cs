﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace MTDataDispatchSystemSync.Models
{
    [XmlRoot]
    public class ESIVehicleResult
    {
        public string Error { get; set; }
        public string ErrorMessage { get; set; }
        public List<Vehicle> Vehicles { get; set; }
        public bool Succeeded { get; set; }
        public Vehicle Vehicle { get; set; }
    }
}