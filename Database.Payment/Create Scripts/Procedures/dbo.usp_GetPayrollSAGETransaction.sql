IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetPayrollSAGETransaction]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_GetPayrollSAGETransaction]
GO

CREATE PROCEDURE dbo.usp_GetPayrollSAGETransaction
-- =============================================
-- Author:		
-- Create date: 
-- Description:	Get Payroll SAGE transaction report
-- =============================================
--	S.Healey	2016-12-08		- Deduct Fleet Fee and Tech Fee as well as Fee for proper payment amount.
--
( 
	@MerchantId INT = NULL,
	@FleetId INT = NULL,
	@PayerType NVARCHAR(50) = NULL,
	@DriverNumber NVARCHAR(50) = NULL,
	@DateFrom DATETIME = NULL,
	@DateTo DATETIME = NULL,
	@NetworkType INT = NULL
)
AS
BEGIN
	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SET NOCOUNT ON

		DECLARE @PayType INT

		IF (@PayerType = 'Driver')
		BEGIN
			SET @PayType = 1
		END
		ELSE IF (@PayerType = 'Car Owner')
		BEGIN
			SET @PayType = 2
		END
		ELSE IF (@PayerType = 'Network')
		BEGIN
		   SET @PayType = 3
		END

		IF (@PayType = 1 OR @PayType = 3 OR @PayType IS NULL)
		BEGIN
			SELECT 'PA' AS [PaymentType],
					D.SageAccount AS [SageAccount],
					'1200' AS [BankAccount],
					'1' AS [Default1],
					COALESCE(CONVERT(DATE, T.TxnDate), '') AS [DefaultDate],
					'BACS' AS [DefaultBACS],
					'Payment' AS [DefaultPayment],
					'T9' AS [DefaultTaxCode],
					'0' AS [VAT],
					CASE 
						WHEN SUM([TransactionAmount]) < 0 THEN 0
						ELSE SUM([TransactionAmount])
					END AS [TransactionAmount],
					f.PayType AS 'FleetPayType' 
			INTO #TempDriver
			FROM (SELECT DriverNo, TxnDate, FareValue, tip, TechFee, fk_FleetId, MerchantID, ResponseCode, TxnType, 
						CASE 
							WHEN TxnType IN ('Sale', 'Completion') THEN CASE 
								WHEN (ISNULL(FareValue, 0) + ISNULL(Tip, 0) + ISNULL(Toll, 0) + ISNULL(Taxes, 0) + ISNULL(Extras, 0) - (ISNULL(Fee, 0) + ISNULL(FleetFee, 0) + ISNULL(TechFee, 0)) < 0) THEN 0
								ELSE (ISNULL(FareValue, 0) + ISNULL(Tip, 0) + ISNULL(Toll, 0) + ISNULL(Taxes, 0) + ISNULL(Extras, 0) - (ISNULL(Fee, 0) + ISNULL(FleetFee, 0) + ISNULL(TechFee, 0)))
							END
							ELSE - Amount
						END AS TransactionAmount
					FROM dbo.MTDTransaction) T
				INNER JOIN dbo.Driver d ON T.DriverNo = d.DriverNo
				INNER JOIN dbo.Fleet f ON d.fk_FleetID = f.FleetID
			WHERE T.fk_FleetId = d.fk_FleetID
			  AND T.TxnDate BETWEEN @DateFrom AND @DateTo
			  AND T.ResponseCode IN ('000', '00', '002')
			  AND T.TxnType IN ('Sale', 'Completion', 'Refund')
			  AND (COALESCE(@MerchantId, 0) = 0 OR T.MerchantID = @MerchantId)
			  AND (COALESCE(@FleetId, 0) = 0 OR T.fk_FleetId = @FleetId)
			  AND (COALESCE(@DriverNumber, '') = '' OR T.DriverNo = @DriverNumber)
			  AND (COALESCE(@PayType,0) = 0 OR f.PayType = @PayType)
			GROUP BY CONVERT(DATE, T.TxnDate), D.SageAccount, f.PayType
		END

		IF (@PayType = 2 OR @PayType = 3 OR @PayType IS NULL)
		BEGIN
			SELECT 'PA' AS PaymentType,
					cw.SageAccount AS SageAccount,
					'1200' AS BankAccount,
					'1' AS Default1,
					COALESCE(CONVERT(DATE, T.TxnDate), '') AS DefaultDate,
					'BACS' AS DefaultBACS,
					'Payment' AS DefaultPayment,
					'T9' AS DefaultTaxCode,
					'0' AS VAT,
					CASE 
						WHEN SUM(TransactionAmount) < 0 THEN 0
						ELSE SUM(TransactionAmount)
					END AS TransactionAmount,
					f.PayType AS 'FleetPayType' 
			INTO #TempOwner
			FROM (SELECT VehicleNo, TxnDate, FareValue, tip, TechFee, fk_FleetId, MerchantID, ResponseCode, TxnType,
						CASE 
							WHEN TxnType IN ('Sale', 'Completion') THEN CASE 
										WHEN (COALESCE(FareValue, 0) + COALESCE(tip, 0) + COALESCE(Taxes, 0) - (COALESCE(fee, 0) + COALESCE(FleetFee, 0)) < 0) THEN 0
										ELSE (COALESCE(FareValue, 0) + COALESCE(tip, 0) + COALESCE(Taxes, 0) - (COALESCE(fee, 0) + COALESCE(FleetFee, 0)))
									END
							ELSE - Amount
						END AS TransactionAmount
					FROM dbo.MTDTransaction) T
				INNER JOIN dbo.Vehicle v ON T.VehicleNo = v.VehicleNumber
				INNER JOIN dbo.Fleet f ON v.fk_FleetID = f.FleetID
				INNER JOIN dbo.CarOwners cw ON v.fk_CarOwenerId = cw.Id
			WHERE T.fk_FleetId = v.fk_FleetID
			  AND T.TxnDate BETWEEN @DateFrom AND @DateTo
			  AND T.ResponseCode IN ('000', '00', '002')
			  AND T.TxnType IN ('Sale', 'Completion', 'Refund')
			  AND (COALESCE(@MerchantId, 0) = 0 OR T.MerchantID = @MerchantId)
			  AND (COALESCE(@FleetId, 0) = 0 OR T.fk_FleetId = @FleetId)
			  AND (COALESCE(@DriverNumber, '') = '' OR v.fk_CarOwenerId = CAST(@DriverNumber AS INT))
			  AND (COALESCE(@PayType, 0) = 0 OR F.PayType = @PayType)
			GROUP BY CONVERT(DATE, T.TxnDate), cw.SageAccount, f.PayType
		END

		IF (@PayType = 1 OR (@PayType = 3 AND @NetworkType = 1))
		BEGIN
			SELECT PaymentType, SageAccount, BankAccount, Default1, DefaultDate, DefaultBACS, DefaultPayment, DefaultTaxCode, VAT, TransactionAmount
			FROM #TempDriver
		END
		ELSE IF (@PayType = 2 OR (@PayType = 3 AND @NetworkType = 2))
		BEGIN
			SELECT PaymentType, SageAccount, BankAccount, Default1, DefaultDate, DefaultBACS, DefaultPayment, DefaultTaxCode, VAT, TransactionAmount
			FROM #TempOwner
		END
		ELSE IF (@PayType = 3 AND @NetworkType IS NULL)
		BEGIN
			SELECT PaymentType, SageAccount, BankAccount, Default1, DefaultDate, DefaultBACS, DefaultPayment, DefaultTaxCode, VAT, TransactionAmount
			FROM #TempDriver

			UNION ALL

			SELECT PaymentType, SageAccount, BankAccount, Default1, DefaultDate, DefaultBACS, DefaultPayment, DefaultTaxCode, VAT, TransactionAmount
			FROM #TempOwner
		END
		ELSE IF (@PayType IS NULL)
		BEGIN
			SELECT PaymentType, SageAccount, BankAccount, Default1, DefaultDate, DefaultBACS, DefaultPayment, DefaultTaxCode, VAT, TransactionAmount
			FROM #TempDriver 
			WHERE FleetPayType IN (1, 3)

			UNION ALL

			SELECT PaymentType, SageAccount, BankAccount, Default1, DefaultDate, DefaultBACS, DefaultPayment, DefaultTaxCode, VAT, TransactionAmount
			FROM #TempOwner 
			WHERE FleetPayType IN (2, 3)
		END

		IF OBJECT_ID('tempdb..#TempDriver') IS NOT NULL
		BEGIN
			DROP TABLE #TempDriver
		END

		IF OBJECT_ID('tempdb..#TempOwner') IS NOT NULL
		BEGIN
			DROP TABLE #TempOwner
		END	 
	END TRY
	BEGIN CATCH
		INSERT INTO dbo.ProcErrorHandler (ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, UserName, HostName, [TimeStamp])
		SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
	END CATCH
END
GO
