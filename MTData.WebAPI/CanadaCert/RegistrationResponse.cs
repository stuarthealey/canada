﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CanadaCert
{
    /// <summary>
    /// Class for handling merchant registration response
    /// </summary>
    public class RegistrationResponse
    {
        public string DID { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("URL")]
        public List<string> URL { get; set; }
    }
}