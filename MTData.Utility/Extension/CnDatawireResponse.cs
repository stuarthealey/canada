﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTData.Utility.Extension
{
   public class CnDatawireResponse
    {
        public string DID { get; set; }
        public string ClientRef { get; set; }
        public string StatusCode { get; set; }
        public string CompleteResponse { get; set; }
    }
}
