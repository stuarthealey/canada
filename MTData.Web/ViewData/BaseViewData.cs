﻿namespace MTData.Web.ViewData
{
    public abstract class BaseViewData
    {
        [AutoMapper.IgnoreMap]
        public string SiteTitle { get; set; }

        [AutoMapper.IgnoreMap]
        public string RootUrl { get; set; }

        [AutoMapper.IgnoreMap]
        public UserViewData CurrentUser { get; set; }

        [AutoMapper.IgnoreMap]
        public bool IsCurrentUserAuthenticated { get; set; }

        [AutoMapper.IgnoreMap]
        public string ReturnUrl { get; set; }
    }
}