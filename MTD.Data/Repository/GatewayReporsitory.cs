﻿using System;
using System.Collections.Generic;
using System.Linq;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Data.Repository
{
    public class GatewayReporsitory : BaseRepository, IGatewayReporsitory
    {
        public GatewayReporsitory(IMtDataDevlopmentsEntities dbMtData)
            : base(dbMtData)
        {

        }

        /// <summary>
        /// Purpose             :   To create gateway
        /// Function Name       :   Create
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   01/07/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gateway"></param>
        /// <returns></returns>
        public int Create(Gateway gateway)
        {
            bool isGatewayExist = DbMtData.Gateways.Any(m => m.Name == gateway.Name && m.IsActive);
            if (isGatewayExist)
                return 0;
            if (gateway.IsDefault)
            {
                Gateway gatewayFound = DbMtData.Gateways.FirstOrDefault(a => a.IsDefault);
                if (gatewayFound != null)
                {
                    gatewayFound.IsDefault = false;
                    DbMtData.CommitChanges();
                }
            }
            gateway.IsActive = true;
            DbMtData.Gateways.Add(gateway);
            return DbMtData.CommitChanges();
        }

        /// <summary>
        /// Purpose             :   To get list of gateway
        /// Function Name       :   GatewaysList
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   01/07/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Gateway> GatewaysList()
        {
            try
            {
                return DbMtData.Gateways.Where(m => m.IsActive);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To delete gateway
        /// Function Name       :   DeleteGateway
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   01/07/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gatewayId"></param>
        public void DeleteGateway(int gatewayId)
        {
            try
            {
            var gateway = DbMtData.Gateways.Find(gatewayId);
            gateway.IsActive = false;         
            DbMtData.CommitChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get gatway
        /// Function Name       :   GetGateway
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   01/07/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gatewayId"></param>
        /// <returns></returns>
        public Gateway GetGateway(int gatewayId)
        {
            try
            {
            return DbMtData.Gateways.Find(gatewayId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To update gateway
        /// Function Name       :   Update
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   01/07/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gateway"></param>
        /// <returns></returns>
        public bool Update(Gateway gateway)
        {
            try
            {
             bool isGatewayExist = DbMtData.Gateways.Any(m => m.Name == gateway.Name && m.IsActive && m.GatewayID!=gateway.GatewayID);
             if (isGatewayExist)
                  return false;
            var gatewaydb = DbMtData.Gateways.Find(gateway.GatewayID);
            if (gateway.IsDefault)
            {
                Gateway gatewayFound = DbMtData.Gateways.FirstOrDefault(a => a.IsDefault);
                if (gatewayFound != null)
                {
                    gatewayFound.IsDefault = false;
                    DbMtData.CommitChanges();
                }
            }
            if (gatewaydb != null)
            {
                gatewaydb.Name = gateway.Name;
                gatewaydb.IsDefault = gateway.IsDefault;
                gatewaydb.UserName = gateway.UserName;
                gatewaydb.PCode = gateway.PCode;
                gatewaydb.GatewayCurrency = gateway.GatewayCurrency;
                gatewaydb.GatewayUrl = gateway.GatewayUrl;
                gatewaydb.GatewayOffset = gateway.GatewayOffset;
                gatewaydb.IsSaleSupported = gateway.IsSaleSupported;
                gatewaydb.IsPreauthSupported = gateway.IsPreauthSupported;
                gatewaydb.IsCaptureSupported = gateway.IsCaptureSupported;
                gatewaydb.IsCancelSupported = gateway.IsCancelSupported;
                gatewaydb.IsRefundSupported = gateway.IsRefundSupported;
            }
            DbMtData.CommitChanges();
            return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get list of currency gateway
        /// Function Name       :   GetGatewayCurrencyList
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   01/07/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Currency> GetGatewayCurrencyList()
        {
            try
            {
            return DbMtData.Currencies;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get list of gateway
        /// Function Name       :   GetGatewayByFilter
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   01/07/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <param name="sorting"></param>
        /// <returns></returns>
        public IEnumerable<Gateway> GetGatewayByFilter(string name, int startIndex, int count, string sorting)
        {
            try
            {
                IEnumerable<Gateway> query = from m in DbMtData.Gateways where m.IsActive ==true select m;

                //Filters
                if (!string.IsNullOrEmpty(name))
                {
                    query = query.Where(p => p.Name.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sorting))
                {
                    query = SortedGateway(sorting, query);
                }
                return count > 0
                    ? query.Skip(startIndex).Take(count).ToList() //Paging
                    : query.ToList(); //No paging             
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<Gateway> SortedGateway(string sorting, IEnumerable<Gateway> query)
        {
            switch (sorting)
            {
                case "Name ASC":
                    query = query.OrderBy(p => p.Name);
                    break;
                case "Name DESC":
                    query = query.OrderByDescending(p => p.Name);
                    break;
                case "IsSaleSupported ASC":
                    query = query.OrderBy(p => p.IsSaleSupported);
                    break;
                case "IsSaleSupported DESC":
                    query = query.OrderByDescending(p => p.IsSaleSupported);
                    break;
                case "IsPreauthSupported ASC":
                    query = query.OrderBy(p => p.IsPreauthSupported);
                    break;
                case "IsPreauthSupported DESC":
                    query = query.OrderByDescending(p => p.IsPreauthSupported);
                    break;
                case "IsCaptureSupported ASC":
                    query = query.OrderBy(p => p.IsCaptureSupported);
                    break;
                case "IsCaptureSupported DESC":
                    query = query.OrderByDescending(p => p.IsCaptureSupported);
                    break;
                case "IsCancelSupported ASC":
                    query = query.OrderBy(p => p.IsCancelSupported);
                    break;
                case "IsCancelSupported DESC":
                    query = query.OrderByDescending(p => p.IsCancelSupported);
                    break;
                case "IsRefundSupported ASC":
                    query = query.OrderBy(p => p.IsRefundSupported);
                    break;
                case "IsRefundSupported DESC":
                    query = query.OrderByDescending(p => p.IsRefundSupported);
                    break;
                case "IsActive ASC":
                    query = query.OrderBy(p => p.IsActive);
                    break;
                case "IsActive DESC":
                    query = query.OrderByDescending(p => p.IsActive);
                    break;
            }
            return query;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int GatewaysListCount(string name)
        {
            try
            {
                var gatewayList =  DbMtData.Gateways.Where(m => m.IsActive).ToList();
                if (!string.IsNullOrEmpty(name))
                {
                    gatewayList = gatewayList.Where(p => p.Name.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                return gatewayList.Count();
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}