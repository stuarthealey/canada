﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Data.Data
{
    public class AllMerchants
    {
        public string Company { get; set; }
        public int MerchantID { get; set; }
    }

    public class AllMerchantsComparer : IEqualityComparer<AllMerchants>
    {
        public bool Equals(AllMerchants allMerchants1, AllMerchants allMerchants2)
        {
            if (object.ReferenceEquals(allMerchants1, allMerchants2))
                return true;
            if (allMerchants1 == null || allMerchants2 == null)
                return false;

            return allMerchants1.MerchantID.Equals(allMerchants2.MerchantID);
        }

        public int GetHashCode(AllMerchants allMerchants)
        {
            return allMerchants.MerchantID.GetHashCode();
        }
    }
}
