﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MTData.Web.Controllers
{
    public class MtdataController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult Disclaimer()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            return View();
        }

        public ActionResult terms_of_use()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            return View();
        }
	}
}