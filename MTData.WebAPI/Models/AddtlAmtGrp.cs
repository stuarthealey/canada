﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Models
{
    public class AddtlAmtGrp
    {
        public string AddAmt { get; set; }
        public string AddAmtCrncy { get; set; }
        public string AddAmtType { get; set; }
        public string AddAmtAcctType { get; set; }
    }
}