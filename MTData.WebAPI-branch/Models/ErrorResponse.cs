﻿using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class ErrorResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string status { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string errormessage { get; set; }
    }
}