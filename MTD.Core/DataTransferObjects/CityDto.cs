﻿namespace MTD.Core.DataTransferObjects
{
  public  class CityDto
    {
        public string CityID { get; set; }
        public string CityName { get; set; }
        public string fk_StateID { get; set; }
    }
}
