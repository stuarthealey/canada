﻿namespace MTData.Web.ViewData
{
    public class WeekDayViewData
    {
        public int DayID { get; set; }
        public string DayName { get; set; }
    }
}