-- SystemConfig table for storing information about the system.
---------------------------------------------------------------
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SystemConfig]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
	CREATE TABLE dbo.SystemConfig (
		[SystemConfigID] [int] IDENTITY(1,1) NOT NULL,
		[SystemID] [nvarchar](10) NULL,
		[CompanyName] [nvarchar](50) NULL,
		[ProductKey] [nvarchar](20) NULL,
		[Version] [nvarchar](10) NULL,
		[_Timestamp] [datetime] NULL,
		CONSTRAINT [PK_SystemConfig] PRIMARY KEY CLUSTERED 
		(
			[SystemConfigID] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	PRINT 'Table dbo.SystemConfig created'
END
ELSE
BEGIN
	PRINT 'Table dbo.SystemConfig already exists'
END

