IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ScheduleUnsent]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_ScheduleUnsent]
GO

CREATE PROCEDURE dbo.usp_ScheduleUnsent
-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <17-06-2015>
-- Description:	<Send unsent reports to merchants>
-- =============================================
AS
BEGIN
	DECLARE @SendTo NVARCHAR(200),
			@Body NVARCHAR(MAX),
			@Res INT,
			@Error NVARCHAR(500),
			@MailId INT

	DECLARE Unsent_Cursor CURSOR FOR SELECT MailId, Body, SendTo FROM dbo.MailMessage WHERE [Status] = 0
	OPEN Unsent_Cursor

	FETCH NEXT FROM Unsent_Cursor INTO @MailId, @Body, @SendTo
	WHILE @@FETCH_STATUS = 0   
	BEGIN
		EXEC @Res = msdb.dbo.sp_send_dbmail @profile_name = 'MTData', @recipients = @SendTo, @body = @Body, @subject = 'Report', @body_format = 'HTML'

		IF (@Res != 0)
		BEGIN
			SET @Error = 'Please check profile name or account name or verify parameters of sp_send_dbmail procedure'
			UPDATE dbo.MailMessage SET Error = @Error, [Date] = GETDATE() WHERE MailId = @MailId
		END
		ELSE
		BEGIN
			UPDATE dbo.MailMessage SET [Status] = 1, [Date] = GETDATE() WHERE MailId = @MailId
		END

		FETCH NEXT FROM Unsent_Cursor INTO @MailId, @Body, @SendTo
	END
	CLOSE Unsent_Cursor   
	DEALLOCATE Unsent_Cursor
END
GO
