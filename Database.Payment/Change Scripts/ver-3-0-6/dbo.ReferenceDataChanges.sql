-- Update version number
------------------------
UPDATE dbo.SystemConfig
SET Version = '3.0.6.0'
WHERE SystemConfigID = (SELECT MAX(SystemConfigID) FROM dbo.SystemConfig)