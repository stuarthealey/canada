﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Data.Data
{
    public class ServiceDiscoveryParametersData
    {
        public List<string> DiscoveryUrls { get; set; }
        public string DID { get; set; }
        public string XmlResponse { get; set; }
        public int MerchantId { get; set; }
        public string TID { get; set; }
    }
}
