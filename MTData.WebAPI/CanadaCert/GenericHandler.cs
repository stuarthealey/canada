﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Text;
namespace MTData.WebAPI.CanadaCert
{
    public static class GenericHandler
    {
        #region Convert ASCII to Hex and Hex To ASCII
        public static string ConvertToHex(string data)
        {
            /* Convert text into an array of characters */
            StringBuilder outputstring = new StringBuilder();

            char[] char_array = data.ToCharArray();
            try
            {
                foreach (char letter in char_array)
                {
                    /* Get the integral value of the character */
                    var value = Convert.ToInt32(letter);

                    /* Convert the decimal value to a hexadecimal value in string form */
                    string hex = String.Format("{0:X}", value);

                    /* Append hexadecimal version of the char to the string outputstring*/
                    outputstring.Append(Convert.ToString(hex));
                }
                return outputstring.ToString();

            }
            catch (Exception)
            {
                throw;
            }

        }

        public static string ConvertHexToAscii(String hexString)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                for (int iCount = 0; iCount < hexString.Length; iCount += 2)
                {
                    string hs = hexString.Substring(iCount, 2);
                    sb.Append(Convert.ToChar(Convert.ToUInt32(hs, 16)));
                }
                return sb.ToString();
            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion
    }
}