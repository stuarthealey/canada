﻿using MTData.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using MTData.WebAPI.Common;


namespace MTData.WebAPI
{
    public class BookerAppPaymentResp
    {
        public XmlDocument reponseDoc { get; set; }
        public XmlElement responseElement { get; set; }
        public BookerAppPaymentResp()
        {
            reponseDoc = new XmlDocument();
            reponseDoc.LoadXml("<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\"><soap12:Body></soap12:Body> </soap12:Envelope>");
            XmlElement rootElement = reponseDoc.DocumentElement;

            XmlNode Root = reponseDoc.GetElementsByTagName("soap12:Body").Item(0);

            responseElement = reponseDoc.CreateElement("BasicAuthoriseResponse");
            XmlAttribute requestTypeNameSpaceAttibute = reponseDoc.CreateAttribute("xmlns");
            requestTypeNameSpaceAttibute.Value = "http://services.mtdata.com/payment";
            responseElement.Attributes.Append(requestTypeNameSpaceAttibute);
            Root.AppendChild(responseElement);
        }

        public XmlDocument ConstructDoc(BasicAuthoriseResult resp)
        {
            XmlDocumentFragment requestID = reponseDoc.CreateDocumentFragment();
            requestID.InnerXml = CommonFunctions.Serialize(resp);
            responseElement.AppendChild(requestID.FirstChild as XmlElement);

            return reponseDoc;
        }

    }
}