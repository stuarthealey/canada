﻿namespace MTD.Core.DataTransferObjects
{
    public class MerchantCurrency
    {
        public int MerCurrencyId { get; set; }
        public int? FkMerchantId { get; set; }
        public string CurrencyCode { get; set; }
    }
}