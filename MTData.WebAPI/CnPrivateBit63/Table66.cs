﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table66
    {
        #region Private Constants and Variables
        #region Constants
        private const string tblId = "|36|36";
        #endregion

        #region Variables
        private string debitNetworkID = "|30|30|30|30|30|30";//Default value.
        #endregion
        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table.
        /// Default X ‘36 36’ Table 66.
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }
        /// <summary>
        /// Debit Network ID.
        /// </summary>
        public string DebitNetworkID
        {
            get { return debitNetworkID; }
            set { debitNetworkID = value; }
        }
        #endregion
    }
}