﻿namespace MTData.WebAPI.Models
{
    public class CommonGrp
    {
        public string PymtType { get; set; }
        public string TxnType { get; set; }
        public string LocalDateTime { get; set; }
        public string TrnmsnDateTime { get; set; }
        public string TermID { get; set; }
        public string MerchID { get; set; }
        public string TxnAmt { get; set; }
        public string TxnCrncy { get; set; }
    }
}