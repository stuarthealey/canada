--
-- _MonthlyReportFilter population.
--

-- Type values. 
-- FV = FleetID Value report
-- FT = FleetID Transaction count
-- V = VehicleNo
-- AV = AddRespData Value report
-- AT = AddRespData Transaction count
--

SET NOCOUNT ON

-- Canada
---------

-- Fleets
DELETE MTDataPaymentsCA.dbo._MonthlyReportFilter WHERE Type = 'FV'
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 2)
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 3)
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 4)
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 5)
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 6)
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 7)
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 8)
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 9)
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 10)

DELETE MTDataPaymentsCA.dbo._MonthlyReportFilter WHERE Type = 'FT'
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 1)
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 2)
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 3)
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 4)
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 5)
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 6)
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 7)
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 8)
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 9)
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 10)

DELETE MTDataPaymentsCA.dbo._MonthlyReportFilter WHERE Type = 'V'
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', '10034')
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', '1956')
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', 'mikes car')

DELETE MTDataPaymentsCA.dbo._MonthlyReportFilter WHERE Type = 'AV'
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('AV', 'APPROVAL')

DELETE MTDataPaymentsCA.dbo._MonthlyReportFilter WHERE Type = 'AT'
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'DECLINED')
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'DECLINED DO NOT HONOUR')
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'FORMATERROR')
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'FORMAT ERROR')
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'INVALID ACCOUNT')
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'SERV NOT ALLOWED')
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'SERV_NOT_ALLOWED')
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'TRAN NOT ALLOWED')
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'SYSTEM PROBLEM')
INSERT INTO MTDataPaymentsCA.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'TOKEN FAILURE')


-- US
-----
DELETE MTDataDevlopments.dbo._MonthlyReportFilter WHERE Type = 'FV'
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 292)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 287)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 288)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 289)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 279)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 297)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 301)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 302)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 303)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 304)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 300)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 305)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 306)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 307)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 308)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 309)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 310)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 311)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 312)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 313)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 314)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 315)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 316)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 317)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 318)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 319)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 320)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 321)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 322)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 323)
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FV', 324)

DELETE MTDataDevlopments.dbo._MonthlyReportFilter WHERE Type = 'FT'
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 251) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 252) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 253) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 254) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 255) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 257) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 258) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 259) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 260) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 261) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 262) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 263) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 264) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 265) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 266) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 267) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 268) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 269) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 270) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 271) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 272) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 273) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 274) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 275) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 276) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 277) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 278) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 279) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 280) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 281) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 282) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 283) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 284) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 285) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 286) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 287) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 288) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 289) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 290) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 291) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 292) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 293) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 294) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 295) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 296) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 297) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 298) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 300) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 301) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 302) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 303) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 304) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 305) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 306) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 307) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 308) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 309) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 310) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 311) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 312) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 313) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 314) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 315) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 316) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 317) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 318) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 319) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 320) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 321) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 322) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 323) 
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('FT', 324)


DELETE MTDataDevlopments.dbo._MonthlyReportFilter WHERE Type = 'V'
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', '1956')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', '195611')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', 'B4000')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', '1832')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', 'voyager')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', 'astro')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', '10101')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', 'mikes car')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', 'pbt')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', 'Mtdjrs')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', 'Win8pbt')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', 'Transdev1')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', 'Transdev9')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', 'Transdev01')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', 'Transdev04')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', '888888')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('V', '10034')

DELETE MTDataDevlopments.dbo._MonthlyReportFilter WHERE Type = 'AV'
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AV', 'APPROVAL')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AV', 'APPRV LESSER AMT')

DELETE MTDataDevlopments.dbo._MonthlyReportFilter WHERE Type = 'AT'
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'SYSTEM PROBLEM')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'INV TRAN')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'SERV NOT ALLOWED')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'TOKEN FAILURE')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'TRAN NOT ALLOWED')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'FORMAT ERROR')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'DECLINED-005')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'DECLINED-051')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'DECLINED-058')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'DECLINED-139')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'DECLINED-152')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'DECLINED-161')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'DECLINED-165')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'DECLINED-176')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'DECLINED-182')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'DECLINED-206')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'DECLINED-XXX')
INSERT INTO MTDataDevlopments.dbo._MonthlyReportFilter (Type, Value) VALUES ('AT', 'CVV2 DECLINED')


SET NOCOUNT OFF
