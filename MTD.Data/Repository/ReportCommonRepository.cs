﻿using System.Collections.Generic;
using System.Linq;

using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Data.Repository
{
    public class ReportCommonRepository : BaseRepository, IReportCommonRepository
    {
        public ReportCommonRepository(IMtDataDevlopmentsEntities dbMtData) : base(dbMtData)
        {
        }

        /// <summary>
        /// Purpose             :   To get list of merchant
        /// Function Name       :   MerchantList
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/11/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Merchant> MerchantList()
        {
            var merchantList = from m in DbMtData.Merchants where m.IsActive select m;
            var newList = merchantList.OrderBy(x => x.Company).ToList();
            return newList;
        }

        /// <summary>
        /// Purpose             :   To get list of fleet
        /// Function Name       :   FleetList
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/11/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<Fleet> FleetList(int id)
        {
            var fleetList = from m in DbMtData.Fleets where (m.fk_MerchantID == id) && m.IsActive select m;
            var newList = fleetList.OrderBy(x => x.FleetName).ToList();
            return newList;
        }

        /// <summary>
        /// Purpose             :   To get list of fleet
        /// Function Name       :   FleetList
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/11/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Fleet> FleetList()
        {
            var fleetList = from m in DbMtData.Fleets where m.IsActive select m;
            var newList = fleetList.OrderBy(x => x.FleetName).ToList();// new list was not in use method returning fleetList ETC issue changed by uk3
            return newList;
        }

        /// <summary>
        /// Purpose             :   To get list of Vehicle
        /// Function Name       :   VehicleList
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/11/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Vehicle> VehicleList()
        {
            var vehicleList = from m in DbMtData.Vehicles where m.IsActive select m;
            return vehicleList;
        }

        /// <summary>
        /// Purpose             :   To get list of Vehicle based on id
        /// Function Name       :   VehicleList
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/11/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<Vehicle> VehicleList(int id)
        {
            var vehicleList = from m in DbMtData.Vehicles where (m.fk_FleetID == id) && m.IsActive select m;
            return vehicleList;
        }

        /// <summary>
        /// Purpose             :   To get list of Vehicle based on fleet Id and Vehicle number
        /// Function Name       :   VehicleList
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/11/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"  
        /// </summary>
        /// <param name="vehicleNumber"></param>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public IEnumerable<Vehicle> VehicleList(string vehicleNumber, int fleetId)
        {
            var vehicleList = from m in DbMtData.Vehicles where (m.VehicleNumber.StartsWith(vehicleNumber)) && m.fk_FleetID == fleetId && m.IsActive select m;
            return vehicleList;
        }

        /// <summary>
        /// Purpose             :   To get list of driver
        /// Function Name       :   DriverList
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/11/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"   
        /// </summary>
        /// <param name="driverNumber"></param>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public IEnumerable<Driver> DriverList(string driverNumber, int fleetId)
        {
            var driverList = from m in DbMtData.Drivers where (m.DriverNo.StartsWith(driverNumber)) && m.fk_FleetID == fleetId && m.IsActive select m;
            return driverList;
        }

        /// <summary>
        /// Purpose             :   To get list of driver name
        /// Function Name       :   DriverNameList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   04/14/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"   
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public IEnumerable<Driver> DriverNameList(int fleetId)
        {
            var driverList = from m in DbMtData.Drivers where (m.fk_FleetID == fleetId && m.IsActive) select m;
            var newList = driverList.OrderBy(x => x.FName).ToList();
            return newList;
        }

        public IEnumerable<User> MerchantUserList(int mId)
        {
            var merchantList = DbMtData.Users.Where(x => x.IsActive && x.fk_MerchantID == mId && x.fk_UserTypeID == 4);
            var newList = merchantList.OrderBy(x => x.Email).ToList();
            return newList;
        }

    }
}
