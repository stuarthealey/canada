﻿using System;

namespace MTD.Core.DataTransferObjects
{
  public   class BookigFeeDto
    {
        public int BookingFeeID { get; set; }
        public Nullable<byte> BookingFeeType { get; set; }
        public Nullable<decimal> BookingFeeFixed { get; set; }
        public Nullable<decimal> BookingFeePer { get; set; }
        public Nullable<decimal> BookingFeeMaxCap { get; set; }
        public Nullable<int> fk_GatewayID { get; set; }
        public Nullable<int> fk_MerchantID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
  }
    }

