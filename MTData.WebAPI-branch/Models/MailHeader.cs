﻿using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class MailHeader
    {
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string DeviceID { get; set; }
        [DataMember]
        public string RequestId { get; set; }
        [DataMember]
        public string Message { get; set; }
    }
}