﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.Web.ViewData
{
    public class TxnTaxSurchargesViewData
    {
        public Nullable<byte> SurchargeType { get; set; }
        
        public Nullable<decimal> SurchargeFixed { get; set; }
        public Nullable<decimal> SurchargePer { get; set; }
        public Nullable<decimal> SurMaxCap { get; set; }
        
        public Nullable<decimal> StateTaxRate { get; set; }
        public Nullable<decimal> FederalTaxRate { get; set; }
        
        public Nullable<decimal> TipPerLow { get; set; }
        public Nullable<decimal> TipPerMedium { get; set; }
        public Nullable<decimal> TipPerHigh { get; set; }
        
        public bool IsTaxInclusive { get; set; }
    }
}