﻿using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class LoginResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public LoginHeader Header { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public LoginReceipt ReceiptFormat { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public LoginCompany CompanyDetails { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public LoginTips Tips { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public LoginSurcharge Surcharge { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public LoginTaxes Taxes { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public string Disclaimer { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public string TermsConditions { get; set; }
        
        [DataMember(EmitDefaultValue = false)]
        public string IsTaxIncluded { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string IsShowTip { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Token { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CurrencyCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FleetId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CardSwiped { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Contactless { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ChipAndPin { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public MACVerification MACVerification { get; set; }
    }
}