﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MTData.Web.ViewData
{
    public class AddressDetails
    {
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Country")]
        [StringLength(40, ErrorMessage = "Country cannot be longer than 40 characters")]
        public string Country { get; set; }
        [DataType(DataType.Text)]
        [Display(Name = "State")]
        [StringLength(40, ErrorMessage = "State name cannot be longer than 40 characters")]
        public string State { get; set; }
        //[Required]
        [StringLength(40, ErrorMessage = "City name cannot be longer than 40 characters")]
        [Display(Name = "City")]
        [DataType(DataType.Text)]
        public string City { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Address")]
        [StringLength(100, ErrorMessage = "Address cannot be longer than 100 characters")]
        public string Address { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Zip Code")]
        public string ZipCode { get; set; }

        public IEnumerable<CountryViewData> CountryList { get; set; }
    
        public string TimeZone { get; set; }
        public IEnumerable<TimeZoneViewData> TimeZones { get; set; }
    }
}