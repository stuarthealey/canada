﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MTD.Data.Data;

namespace MTD.Data.Repository.Interface
{
    public interface ITransactionRepositoryUK
    {
        int AddSemiTxn(UKMTDTransaction ukTxn, MTDTransaction UsmtdDto);
        int AddSemiSattlementTxn(UKMtdtxnSattlement ukTxn);
    }
}
