﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    [XmlSerializerFormat]
    public class TransactionDetails
    {
        private string _type;

        [DataMember]
        public string PaymentType
        {
            get { return string.IsNullOrEmpty(this._type) ? string.Empty : this._type; }
            set { this._type = value; }
        }

        private string _note;

        [DataMember]
        public string TransNote
        {
            get { return string.IsNullOrEmpty(this._note) ? string.Empty : this._note; }
            set { this._note = value; }
        }

        private string _cType;

        [DataMember]
        public string CardType
        {
            get { return string.IsNullOrEmpty(this._cType) ? string.Empty : this._cType; }
            set { this._cType = value; }
        }

        private string _mode;

        [DataMember]
        public string EntryMode
        {
            get { return string.IsNullOrEmpty(this._mode) ? string.Empty : this._mode; }
            set { this._mode = value; }
        }

        private string _transType;

        [DataMember]
        public string TransactionType
        {
            get { return string.IsNullOrEmpty(this._transType) ? string.Empty : this._transType; }
            set { this._transType = value; }
        }

        private string _transAmount;

        [DataMember]
        public string TransactionAmount
        {
            get { return string.IsNullOrEmpty(this._transAmount) ? string.Empty : this._transAmount; }
            set { this._transAmount = value; }
        }

        private string _industry;

        [DataMember]
        public string IndustryType
        {
            get { return string.IsNullOrEmpty(this._industry) ? string.Empty : this._industry; }
            set { this._industry = value; }
        }

        private string _driver;

        [DataMember]
        public string DriverNumber
        {
            get { return string.IsNullOrEmpty(this._driver) ? string.Empty : this._driver; }
            set { this._driver = value; }
        }

        private string _vehicle;

        [DataMember]
        public string VehicleNumber
        {
            get { return string.IsNullOrEmpty(this._vehicle) ? string.Empty : this._vehicle; }
            set { this._vehicle = value; }
        }

        private string _created;

        [DataMember]
        public string CreatedBy
        {
            get { return string.IsNullOrEmpty(this._created) ? string.Empty : this._created; }
            set { this._created = value; }
        }

        private string _address;

        [DataMember]
        public string StreetAddress
        {
            get { return string.IsNullOrEmpty(this._address) ? string.Empty : this._address; }
            set { this._address = value; }
        }

        private string _pickLat;

        [DataMember]
        public string PickUpLat
        {
            get { return string.IsNullOrEmpty(this._pickLat) ? string.Empty : this._pickLat; }
            set { this._pickLat = value; }
        }

        private string _pickLng;

        [DataMember]
        public string PickUpLng
        {
            get { return string.IsNullOrEmpty(this._pickLng) ? string.Empty : this._pickLng; }
            set { this._pickLng = value; }
        }

        private string _pickAdd;

        [DataMember]
        public string PickUpAdd
        {
            get { return string.IsNullOrEmpty(this._pickAdd) ? string.Empty: this._pickAdd; }
            set { this._pickAdd = value; }
        }

        private string _dropLat;

        [DataMember]
        public string DropOffLat
        {
            get { return string.IsNullOrEmpty(this._dropLat) ? string.Empty : this._dropLat; }
            set { this._dropLat = value; }
        }

        private string _dropLng;

        [DataMember]
        public string DropOffLng
        {
            get { return string.IsNullOrEmpty(this._dropLng) ? string.Empty : this._dropLng; }
            set { this._dropLng = value; }
        }

        private string _dropAdd;

        [DataMember]
        public string DropOffAdd
        {
            get { return string.IsNullOrEmpty(this._dropAdd) ? string.Empty : this._dropAdd; }
            set { this._dropAdd = value; }
        }

        private string _zip;

        [DataMember]
        public string ZipCode
        {
            get { return string.IsNullOrEmpty(this._zip) ? string.Empty : this._zip; }
            set { this._zip = value; }
        }

        private string _authcode;

        [DataMember]
        public string AuthId
        {
            get { return string.IsNullOrEmpty(this._authcode) ? string.Empty : this._authcode; }
            set { this._authcode = value; }
        }

        private string _transId;

        [DataMember]
        public string TransactionId
        {
            get { return string.IsNullOrEmpty(this._transId) ? string.Empty : this._transId; }
            set { this._transId = value; }
        }

        private string _rescode;

        [DataMember]
        public string RespCode
        {
            get { return string.IsNullOrEmpty(this._rescode) ? string.Empty : this._rescode; }
            set { this._rescode = value; }
        }

        private string _emvResponse;

        [DataMember]
        public string EmvResponse
        {
            get { return string.IsNullOrEmpty(this._emvResponse) ? string.Empty : this._emvResponse; }
            set { this._emvResponse = value; }
        }

        private string _ccnumber;

        [DataMember]
        public string CardNumber
        {
            get { return string.IsNullOrEmpty(this._ccnumber) ? string.Empty : this._ccnumber; }
            set { this._ccnumber = value; }
        }

        private string _fdmerchantid;

        [DataMember]
        public string MID
        {
            get { return string.IsNullOrEmpty(this._fdmerchantid) ? string.Empty : this._fdmerchantid; }
            set { this._fdmerchantid = value; }
        }

        private string _fdterminalID;

        [DataMember]
        public string TID
        {
            get { return string.IsNullOrEmpty(this._fdterminalID) ? string.Empty : this._fdterminalID; }
            set { this._fdterminalID = value; }
        }

        #region Interac

        private string _stan;

        [DataMember]
        public string StanNo
        {
            get { return string.IsNullOrEmpty(this._stan) ? string.Empty : this._stan; }
            set { this._stan = value; }
        }

        private string _transRefNo;

        [DataMember]
        public string TransRefNo
        {
            get { return string.IsNullOrEmpty(this._transRefNo) ? string.Empty : this._transRefNo; }
            set { this._transRefNo = value; }
        }

        private string _macData;
        [DataMember]
        public string MacData
        {
            get { return string.IsNullOrEmpty(this._macData) ? string.Empty : this._macData; }
            set { this._macData = value; }
        }

        private string _checkDigit;

        [DataMember]
        public string CheckDigit
        {
            get { return string.IsNullOrEmpty(this._checkDigit) ? string.Empty : this._checkDigit; }
            set { this._checkDigit = value; }
        }

        private string _tMac;

        [DataMember]
        public string TMAC
        {
            get { return string.IsNullOrEmpty(this._tMac) ? string.Empty : this._tMac; }
            set { this._tMac = value; }
        }

        private string _tKpe;

        [DataMember]
        public string TKPE
        {
            get { return string.IsNullOrEmpty(this._tKpe) ? string.Empty : this._tKpe; }
            set { this._tKpe = value; }
        }

        private string _tKme;

        [DataMember]
        public string TKME
        {
            get { return string.IsNullOrEmpty(this._tKme) ? string.Empty : this._tKme; }
            set { this._tKme = value; }
        }

        private string _processingCode;

        [DataMember]
        public string ProcessingCode
        {
            get { return string.IsNullOrEmpty(this._processingCode) ? string.Empty : this._processingCode; }
            set { this._processingCode = value; }
        }

        #endregion
    }
}