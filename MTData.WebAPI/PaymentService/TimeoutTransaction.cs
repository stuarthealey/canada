﻿using System;
using System.Text;
using System.Threading;

using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.WebAPI.Resources;
using MTData.WebAPI.Common;
using MTData.WebAPI.Controllers;
using MTData.WebAPI.Models;

namespace MTData.WebAPI
{
    public class TimeoutTransaction : TransactionRequestController
    {
        private readonly ITransactionService _transactionService;
        private static ITerminalService _terminalService;
        private readonly IMerchantService _merchantService;
        private StringBuilder _logMessage;

        public TimeoutTransaction(
            [Named("TransactionService")] ITransactionService transactionService,
            [Named("TerminalService")] ITerminalService terminalService,
            [Named("MerchantService")] IMerchantService merchantService,
            StringBuilder logMessage)
            : base(terminalService, logMessage)
        {
            _transactionService = transactionService;
            _terminalService = terminalService;
            _merchantService = merchantService;
            _logMessage = logMessage;
        }

        /*  RISC Holds the line.
            Client performs a test transaction while FD Holds the Line. Client gets a Fail Response.
            FD Keeps holding the line and Client initiates a TOR after a minimum gap of 35 Seconds. Client gets no response.
            FD Keeps holding the line and Client initiates one more TOR after a minimum gap of 35 Seconds. Client gets no response.
            FD releases the hold on the line and Client initiates the third TOR after a minimum gap of 35 Seconds and up to a maximum 25 minutes and gets a response back this time.
            - A maximum of 3 TOR attempt is allowed for the Client within which they need to get a response back. Client should not submit a 4th TOR if they fail to get a response back even in the 3rd TOR attempt.
            Cert Analyst verifies the TOR test result
            During the TOR testing, check for the time interval between two consecutive transactions in the sandbox. The minimum time difference should be 35secs.
            Check if all the other required parameter values has been passed correctly in the XML request.
            We run the test 3 times with 3 different card types with RISC bringing the line up and taking it down.
        */
        public string TimeOut(TransactionRequest tr, int transId, string clientRef, string xmlSerializedTransResp)
        {
            if (string.IsNullOrEmpty(xmlSerializedTransResp))
            {
                Thread.Sleep(35000);//FD Keeps holding the line and Client initiates a TOR after a minimum gap of 35 Seconds
                xmlSerializedTransResp = SendTimeoutTransaction(tr, transId, clientRef);

                _logMessage.Append("SendTimeoutTransaction executed successfully " + transId);

                if (string.IsNullOrEmpty(xmlSerializedTransResp))
                {
                    _logMessage.Append("checking for timeout");
                    for (int i = 0; i <= 1; i++)
                    {
                        Thread.Sleep(35000);
                        xmlSerializedTransResp = SendTimeoutTransaction(tr, transId, clientRef);

                        _logMessage.Append("timeout sent again");
                        if (!string.IsNullOrEmpty(xmlSerializedTransResp))
                            break;
                    }
                }
            }

            if (string.IsNullOrEmpty(xmlSerializedTransResp))
                xmlSerializedTransResp = PaymentAPIResources.TimeoutError;

            return xmlSerializedTransResp;
        }

        //Uncomment below line to test timeout
        //static int tCounter = 1;
        public string SendTimeoutTransaction(TransactionRequest tr, int prevtransId, string clientRef)
        {
            tr.TransactionType = "Timeout";
            tr.RapidConnectAuthId = Convert.ToString(prevtransId);
            tr.LocalDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
            tr.TrnmsnDateTime = DateTime.UtcNow.ToString("yyyyMMddHHmmss");

            CreateTransactionRequest(tr);
            string xmlSerializedTimeoutReq = GetXmlData();
            //Save transaction into MTDTransaction Table

            MTDTransactionDto transactiontimeoutDto = CommonFunctions.XmlToTransactionDto(tr);
            int transId = _transactionService.Add(transactiontimeoutDto);
            _terminalService.UpdateStan(transactiontimeoutDto.Stan, transactiontimeoutDto.TransRefNo, tr.TerminalId, tr.MtdMerchantId);
            //end Save transaction into MTDTransaction Table

            #region Test Timeout
            //string xmlSerializedTransResp = string.Empty;
            //if (tCounter > 2)
            //    xmlSerializedTransResp = new SoapHandler().SendMessage(xmlSerializedTimeoutReq, clientRef, tr);
            //tCounter++; 
            #endregion

            /* Send data using SOAP protocol to Datawire*/
            string xmlSerializedTransResp = new SoapHandler().SendMessage(xmlSerializedTimeoutReq, clientRef, tr);

            //  update response of transaction
            if (string.IsNullOrEmpty(xmlSerializedTransResp))
            {
                return null;
            }

            MTDTransactionDto responsetimeoutDto = CommonFunctions.XmlResponseToTransactionDto(xmlSerializedTransResp, false, tr.PaymentType.ToString(), tr.IndustryType);
            _transactionService.Update(responsetimeoutDto, transId, tr.RapidConnectAuthId);

            //Uncomment below line to test timeout
            //tCounter = 0;

            return xmlSerializedTransResp;
        }
    }
}
