﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CanadaCert
{
    [Serializable()]
    public class ServiceDiscoveryResponse
    {
        [System.Xml.Serialization.XmlElementAttribute("ServiceProvider", typeof(ServiceProvider))]
        public List<ServiceProvider> ServiceProvider { get; set; }
    }

    [Serializable()]
    public class ServiceProvider
    {
        /// <summary>
        /// Gets or sets the transaction time 
        /// of the service url 
        /// </summary>
        public long TransactionTime
        {
            get;
            set;
        }

        public string URL { get; set; }
        /// <summary>
        /// Gets or sets the maximum transactions
        /// that can be carried out using the service url
        /// </summary>
        public string MaxTransactionsInPackage { get; set; }

        public bool IsActiveUrl { get; set; }
    }
}