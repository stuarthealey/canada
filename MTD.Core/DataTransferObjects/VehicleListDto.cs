﻿namespace MTD.Core.DataTransferObjects
{
    public class VehicleListDto
    {
        public int VehicleID { get; set; }
        public string VehicleNumber { get; set; }
    }
}
