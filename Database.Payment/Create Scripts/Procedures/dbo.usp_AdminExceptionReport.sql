IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_AdminExceptionReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_AdminExceptionReport]
GO

CREATE PROCEDURE dbo.usp_AdminExceptionReport
-- =============================================
-- Author:	<Naveen Kumar>
-- Create date: <10-03-2016>
-- Description:	<Prepare and Send exception reports to admin users>
-- =============================================
-- S.Healey		2016-11-08		- Performance optimisation with use of indexes and improved queries.
--
(
	@IsOnDemandReport BIT,
	@FromDate DATETIME = NULL,
	@ToDate DATETIME = NULL,
	@XmlData NVARCHAR(MAX) OUT,
	@ReportType INT OUT,
	@FDate DATE OUT,
	@TDate DATE OUT,
	@NextScheduleDateTime DATETIME OUT
)
AS
BEGIN
	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SET NOCOUNT ON
		SET ANSI_WARNINGS OFF

		DECLARE @Frequency INT,
				@FrequencyType INT,
				@DateFrom DATE,
				@DateTo DATE,
				@Merchant NVARCHAR(100),
				@Fleet NVARCHAR(100),
				@ExpFleetTrans INT,
				@ActFleetTrans INT,
				@ExpCar INT,
				@ActCar INT,
				@ExpValue DECIMAL(18, 2),
				@ActValue DECIMAL(18, 2),
				@Case VARCHAR(100),
				@InnerXml NVARCHAR(MAX)='',
				@ScheduleId INT

		SELECT	@ScheduleId = ScheduleID,
				@Frequency = Frequency,
				@FrequencyType = FrequencyType,
				@NextScheduleDateTime = NextScheduleDateTime
		FROM dbo.ScheduleReport 
		WHERE fk_ReportId = 3 
		  AND IsCreated = 1 
		  AND fk_MerchantId IS NULL 
		  AND CorporateUserId IS NULL

		IF (@IsOnDemandReport = 1)
		BEGIN
			SET @DateFrom = @FromDate
			SET @DateTo = @ToDate
		END
		ELSE
		BEGIN
			IF (@Frequency = 1)
			BEGIN
				SET @DateTo = GETDATE()
				SET @DateFrom = GETDATE() - @FrequencyType
				SET @NextScheduleDateTime = @NextScheduleDateTime + @FrequencyType
			END
			ELSE IF (@Frequency = 2)
			BEGIN
				SET @DateFrom = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK, -@FrequencyType, GETDATE( ))), 0) - 1
				SET @DateTo = DATEADD(WEEK, DATEDIFF(WEEK, 0, GETDATE()), 0) - 1
				SET @NextScheduleDateTime = DATEADD(WK, @FrequencyType, @NextScheduleDateTime)
			END
			ELSE IF (@Frequency = 3)
			BEGIN
				SET @DateFrom = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH, -@FrequencyType, GETDATE())), 0)
				SET @DateTo = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) 
				SET @NextScheduleDateTime = DATEADD(MONTH, @FrequencyType, @NextScheduleDateTime)
			END  
			ELSE
			BEGIN
				RETURN 1
			END 
		END

		SELECT	COALESCE((SELECT Company FROM dbo.Merchant M WHERE M.MerchantID = Trans.MerchantId), '') AS 'Merchant',
				COALESCE((SELECT FleetName FROM dbo.Fleet F WHERE F.FleetID = Fl.FleetID), '') AS 'Fleet',
				COALESCE(Fl.FleetTransaction, 0) AS 'ExpFleetTrans',
				COALESCE(Trans.FTransCount, 0) AS 'ActFleetTrans',
				COALESCE(Fl.CarTransaction, 0) AS 'ExpCar',
				COALESCE(Trans.VTransCount, 0) AS 'ActCar',
				COALESCE(Fl.TransactionValue, 0.00) AS 'ExpValue',
				COALESCE(Trans.Amount, 0.00) AS 'ActValue' 
		INTO #Temp
		FROM ( SELECT fk_MerchantID AS MerchantID, FleetID, FleetName, FleetTransaction, CarTransaction, CAST(TransactionValue AS DECIMAL(18, 2)) AS TransactionValue
				FROM dbo.Fleet F 
				WHERE F.IsActive = 1 
				  AND (F.FleetTransaction IS NOT NULL OR F.CarTransaction IS NOT NULL OR F.TransactionValue IS NOT NULL) ) Fl
			INNER JOIN ( SELECT V.MerchantId, V.FleetID, SUM(V.TransCount) AS FTransCount, COUNT(V.TransCount) AS VTransCount, SUM(V.FareValue) AS FareValue, SUM(V.Tip) AS 'Tip',
								SUM(V.Surcharge) AS 'Surcharge', SUM(V.Amount) AS 'Amount', SUM(V.Fee) AS 'Fee', SUM(V.TechFee) AS 'TechFee', SUM(V.AmountOwing) AS 'AmountOwing' 
						FROM (SELECT COALESCE(T.MerchantId, 0) AS 'MerchantId',
									COALESCE((SELECT TOP 1 fk_FleetID FROM dbo.Vehicle V WHERE V.fk_FleetId = T.fk_FleetId), '') AS FleetID,
									COALESCE(T.VehicleNo, '') AS 'VehicleNo',
									COUNT(T.VehicleNo) AS TransCount,
									SUM(T.FareValue) AS 'FareValue',
									SUM(T.Tip) AS 'Tip',
									SUM(T.Surcharge) AS 'Surcharge',
									cast(SUM(T.Amount) AS DECIMAL(18, 2)) AS 'Amount',
									COALESCE(SUM(T.Fee), 0) + COALESCE(SUM(T.FleetFee), 0) AS 'Fee',
									SUM(T.TechFee) AS 'TechFee',
									COALESCE(SUM(T.Amount), 0) - COALESCE(SUM(T.FleetFee), 0) - COALESCE(SUM(T.Fee), 0) AS 'AmountOwing'
								FROM dbo.MTDTransaction T WITH (INDEX(IX_MTDTransactionTxnDate))
								WHERE T.TxnDate >= @DateFrom
								  AND T.TxnDate < @DateTo
								  AND T.ResponseCode IN ('000', '00', '002')
								  AND T.TxnType IN ('Sale', 'Completion')
								  AND IsRefunded = 0
								  AND IsVoided = 0
								GROUP BY T.VehicleNo, T.MerchantId, T.fk_FleetId
								) V GROUP BY V.MerchantId, V.FleetID
						  ) Trans ON (fl.FleetID = Trans.FleetID)

		WHERE Fl.FleetTransaction > Trans.FTransCount 
		   OR Fl.CarTransaction > Trans.VTransCount 
		   OR Fl.TransactionValue > Trans.Amount

		--Declaring cursor for fetching exception data row one by one from temp table
		DECLARE MerExp_Cursor CURSOR FOR SELECT Merchant, Fleet, ExpFleetTrans, ActFleetTrans, ExpCar, ActCar, ExpValue, ActValue FROM #Temp
		OPEN MerExp_Cursor

		FETCH NEXT FROM MerExp_Cursor INTO @Merchant, @Fleet, @ExpFleetTrans, @ActFleetTrans, @ExpCar, @ActCar, @ExpValue, @ActValue
		WHILE @@FETCH_STATUS = 0 
		BEGIN  
			SET @Case = ''

			IF (@ExpFleetTrans > @ActFleetTrans)
			BEGIN
				SET @Case = 'UnExp Trans'
			END

			IF (@ExpCar > @ActCar) 
			BEGIN
				IF (@Case <> '') 
				BEGIN
					SET @Case = @Case + ',' + 'UnExp Cars'
				END
				ELSE 
				BEGIN
					SET @Case = @Case + 'UnExp Cars'
				END
			END
			IF (@ExpValue > @ActValue)
			BEGIN
				IF (@Case <> '') 
				BEGIN
					SET @Case = @Case + ',' + 'UnExp Trans Value'
				END
				ELSE 
				BEGIN
					SET @Case = @Case + 'UnExp Trans Value'
				END
			END

			--Set data in xml format
			SET @InnerXml = @InnerXml + COALESCE(CAST((SELECT COALESCE(@Merchant, '') AS 'td', '', COALESCE(@Fleet, '') AS 'td', '', COALESCE(@ExpFleetTrans, 0) AS 'td', '',
									COALESCE(@ActFleetTrans, 0) AS 'td', '', COALESCE(@ExpCar, 0) AS 'td', '', COALESCE(@ActCar, 0) AS 'td','', CONCAT('$', COALESCE(@ExpValue, 0.00)) AS 'td', '',
									CONCAT('$', COALESCE(@ActValue, 0.00)) AS 'td', '', COALESCE(@Case, '') AS 'td' FOR XML PATH('tr'), ELEMENTS) AS NVARCHAR(MAX)), '')

			FETCH NEXT FROM MerExp_Cursor INTO @Merchant, @Fleet, @ExpFleetTrans, @ActFleetTrans, @ExpCar, @ActCar, @ExpValue, @ActValue
		END

		CLOSE MerExp_Cursor    
		DEALLOCATE MerExp_Cursor 

		DROP TABLE #Temp

		SET @XmlData = @InnerXml
		SET @ReportType = @Frequency
		SET @FDate = @DateFrom
		SET @TDate = DATEADD(DAY, -1, @DateTo)
	END TRY
	BEGIN CATCH
		INSERT INTO ProcErrorHandler(ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, UserName, HostName, [TimeStamp])
		SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
	END CATCH
END
GO
