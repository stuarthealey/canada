﻿using System.Collections.Generic;

using MTD.Data.Data;

namespace MTD.Data.Repository.Interface
{
    public interface IFleetRepository
    {
        bool Add(Fleet fleet, int userType, int userId, int merchantId);

        IEnumerable<Fleet> FleetList(int merchantId, int UserType, int UserId);

        Fleet GetFleet(int fleetId);

        Fleet GetFleet(int fleetId, int merchantId);

        bool Update(Fleet fleet, int fleetId, int merchantId);

        bool DeleteFleet(int fleetId);

        Fleet GetCurrency(int CurrencyCodeID);

        IEnumerable<Fleet> GetUserFleetListByFilter(FilterSearchParameters fsParametes, int logOnByUserId,int logOnByUserType);

        bool Add(Fleet fleet);

        IEnumerable<Fleet> FleetSettingListByFiter(FilterSearchParameters fsParametes);

        Fleet GetFleetSetting(int fleetId);

        bool DeleteFleetSetting(int fleetId);

        IEnumerable<Fleet> GetSettingFleetList(int merchantId);

        IEnumerable<Currency> CurrencyList();

        int FleetListCount(FilterSearchParameters fsParametes,int logOnByUserId,int logOnByUserType);

        int FleetSettingListCount(FilterSearchParameters fsParametes);

        IEnumerable<Fleet> FeeFleetList(int merchantId, int userType, int userId);

        bool AddFleetFee(Fleet fleet);

        IEnumerable<Surcharge> FleetFeetListByFilter(FilterSearchParameters fsParametes);

        Surcharge GetFleetFee(int surchargeId);

        int FleetFeeListCount(FilterSearchParameters fsParametes);

        bool UpdateFleetFee(Surcharge surcharge);

        void DeleteFleetFee(int surchargeId);

        List<int> GetAccesibleFleet(int logOnByUserId, int merchantId);

        string Authenticate(string SystemID, string userName, string pCode);
        
        bool tokenValidation(string token, int fid);

        bool tokenReset(string token);

        int GetCurrencyByFleet(int fleetId);
    }
}
