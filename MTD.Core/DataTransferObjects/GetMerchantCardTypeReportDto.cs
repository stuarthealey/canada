﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
    public class GetMerchantCardTypeReportDto
    {
        public Nullable<long> SrNo { get; set; }
        public string FleetName { get; set; }
        public string VISA { get; set; }
        public string MasterCard { get; set; }
        public string AmericanExpress { get; set; }
        public string Discover { get; set; }
        public string JCB { get; set; }
        public string DinersClub { get; set; }
        public Nullable<int> VISATxn { get; set; }
        public Nullable<int> MasterTxn { get; set; }
        public Nullable<int> AmexTxn { get; set; }
        public Nullable<int> DisTxn { get; set; }
        public Nullable<int> JCBTxn { get; set; }
        public Nullable<int> DinerTxn { get; set; }
    }
}
