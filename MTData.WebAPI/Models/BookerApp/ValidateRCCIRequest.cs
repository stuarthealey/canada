﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Models
{
    public class ValidateRCCIReq
    {
        public GPSLocation GPSLocation { get; set; }

        public ValueNode CustomerId { get; set; }

        public ValueNode RCCI { get; set; }
    }
}