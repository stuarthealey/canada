﻿using MTData.WebAPI.CanadaCert;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class TableSP
    {
        #region Private Constants and Variables
        
        #region Constants

        private const string tblId = "|53|50";

        #endregion

        #region Variables

        private const string securitylevelTag01 = "|30|31";
        private string securitylevelTagLength = "|30|30|31";
        private const string securitylevelTag01Vlaue = "|33";

        private const string tag06 = "|30|36";
        private const string tag06Length = "|30|30|34";
        private string tag06Value = "|53|36|37|35";//"|S6|75";

        #endregion

        /// <summary>
        /// Class constructor, to check the Region and set the tag06Value accordingly.
        /// </summary>
        /// 
        //zzz
        //PAY-13 Remove the generic constructor, as we must always pass the Token Type.
        //public TableSP()
        //{
        //    //if (ConfigurationManager.AppSettings["Project"] == PaymentAPIResources.CanadaProject)
        //    //    tag06Value = "|53|41|42|4d";
        //}

        /// <summary>
        /// PAY-13 Overload to allow passing of the actual Merchant Token Type value, to fix RCCI problem of hard coded value from above!!!
        /// </summary>
        /// <param name="tokenType"></param>
        public TableSP(string tokenType)
        {
            //if (ConfigurationManager.AppSettings["Project"] == PaymentAPIResources.CanadaProject)
            //    tag06Value = "|53|41|42|4d";
            Tag06Value = CnTransactionMessage.ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tokenType));
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// ID of Table
        /// default X'36 30' Table 60
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        /// <summary>
        /// Gets or Sets the value for DCC Conversion Indicator
        /// </summary>
        public string SecurityLevelTag01
        {
            get { return securitylevelTag01; }

        }
        public string SecuritylevelTagLength
        {
            get { return securitylevelTagLength; }
        }

        /// <summary>
        /// Gets or Sets the value for DCC Transaction Time Zone
        /// </summary>
        public string SecuritylevelTag01Vlaue
        {
            get { return securitylevelTag01Vlaue; }
        }

        /// <summary>
        /// Gets or Sets the value for Foreign Conversion Rate
        /// </summary>
        public string Tag06
        {
            get { return tag06; }
        }

        public string Tag06Length
        {
            get { return tag06Length; }
        }

        /// <summary>
        /// Gets or Sets the value for Amount in Merchant Base Currency
        /// </summary>
        public string Tag06Value
        {
            get { return tag06Value; }
            set { tag06Value = value; }
        }

        #endregion
    }
}