﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
   public class TechnologySummaryReportDto
    {
        public string Merchant { get; set; }
        public string FleetName { get; set; }
        public Nullable<byte> PayType { get; set; }
        public Nullable<int> CountVehicle { get; set; }
        public Nullable<int> AutomaticTrans { get; set; }
        public Nullable<int> ManualTrans { get; set; }
        public Nullable<int> CountRefund { get; set; }
        public Nullable<int> CountVoid { get; set; }
        public Nullable<int> TotalTrans { get; set; }
        public string TechFee { get; set; }
        public string FleetFee { get; set; }
        public string Surcharge { get; set; }
        public string Booking_Fee { get; set; }
        public string TotalAmt { get; set; }
        public Nullable<int> fk_FleetId { get; set; }
    }
}
