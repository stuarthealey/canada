﻿namespace MTData.WebAPI.Models
{
    public class CardGrp
    {
        public string AcctNum { get; set; }
        public string AVSResultCode { get; set; }
        public string CCVResultCode { get; set; }        
    }
}