﻿using System;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Xml;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Configuration;
using System.Web.Configuration;
using System.Net;
using System.Collections.Generic;
using System.Diagnostics;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.WebAPI.Models;
using MTData.WebAPI.Common;
using MTData.WebAPI.Resources;
using MTData.WebAPI.CanadaCert;
using MTData.WebAPI.CanadaCommon;

namespace MTData.WebAPI.Controllers
{
    public class PaymentController : TransactionRequestController
    {
        private readonly ITransactionService _transactionService;
        private readonly ITerminalService _terminalService;
        private readonly IMerchantService _merchantService;
        private readonly IDriverService _driverService;
        private readonly ICommonService _commonService;
        private LocalVariables _localVars = new LocalVariables();
        private readonly StringBuilder _logMessage;
        private StringBuilder _logTracking;
        readonly ILogger _logger = new Logger();

        public PaymentController(
            [Named("TransactionService")] ITransactionService transactionService,
            [Named("MerchantService")] IMerchantService merchantService,
            [Named("DriverService")] IDriverService driverService,
            [Named("TerminalService")] ITerminalService terminalService,
            [Named("CommonService")] ICommonService commonService,
            StringBuilder logMessage, StringBuilder logTracking) : base(terminalService, logMessage)
        {
            _merchantService = merchantService;
            _transactionService = transactionService;
            _terminalService = terminalService;
            _driverService = driverService;
            _logMessage = logMessage;
            _commonService = commonService;
            _logTracking = logTracking;
        }

        fDMerchantDto _fdMerchantobj = new fDMerchantDto();
        PaymentResponse response = new PaymentResponse();
        EmvCapkResponse capkresponse = new EmvCapkResponse();

        decimal fleetFee = 0;
        decimal techFee = 0;
        decimal txnFee = 0;

        readonly Stopwatch objStopWatch = new Stopwatch();
        private List<ServiceProvider> _serviceProvider = new List<ServiceProvider>();
        int transId = 0;

        #region (Payment Process for US)

        /// <summary>
        /// Purpose             :   Sending Credit and debit card transaction to rapid connect
        /// Function Name       :   SendPayment
        /// Created By          :   Salil Gupta
        /// Created On          :   02/27/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY" 
        /// </summary>Sending data...
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult SendPayment(HttpRequestMessage request)
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            
            // First lets check that we are not running in Canadian mode. If we are, then call the CnSendPayment() method.
            if (WebConfigurationManager.AppSettings["Project"] == PaymentAPIResources.CanadaProject)
            {
                _logMessage.AppendLine("Running as Canada project.  Calling CnSendPayment() method.");
                _logger.LogInfoMessage(_logMessage.ToString());

                try
                {
                    return CnSendPayment(request);
                }
                catch (Exception ex)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), ex);
                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.Exception, _localVars.DeviceId, _localVars.RequestId);
                    return Ok(response);
                }
            }
            else
            {
                try
                {
                    XmlDocument xmlDoc = new XmlDocument();

                    _logMessage.AppendLine("Loading request XML data.");

                    xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                    // Used to log request and response as per requirement
                    int logReqResp = Convert.ToInt32(WebConfigurationManager.AppSettings["LogFirstDataRequestResponse"]);

                    if (logReqResp == 1)
                        _logMessage.AppendLine("Android Request : \n" + xmlDoc.InnerXml);

                    _logTracking.AppendLine("Payment process start. DriverNo: " + _localVars.DriverNo + " and  DeviceId " + _localVars.DeviceId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());
                    _logMessage.AppendLine("Initializing local variables from XML request data");

                    // Initialize local variable from XML Request.
                    if (!SetLocalVarFromXMLRequest(xmlDoc))
                    {
                        _logMessage.AppendLine("Failed to initialize local variables from request XML data.");

                        // Need to create an Exception to add the XML data to, to get in the ApplicationLog table.
                        StringWriter sw = new StringWriter();
                        System.Xml.XmlTextWriter tw = new XmlTextWriter(sw);
                        xmlDoc.WriteTo(tw);

                        Exception ex = new Exception(string.Format("XML_Data:{0}", sw.ToString()));

                        _logger.LogInfoFatal(_logMessage.ToString(), ex);

                        tw.Close();
                        sw.Close();

                        return Ok(response);
                    }

                    // Validate Request
                    _logMessage.AppendLine("Validating request xml data");

                    dynamic firstDataDetails = string.Empty;
                    //if (!ValidateRequest(response, mHeader, out firstDataDetails))
                    if (!ValidateRequest(response, out firstDataDetails))
                    {
                        _logMessage.AppendLine("Failed to validate request xml data.");
                        _logger.LogInfoMessage(_logMessage.ToString());

                        return Ok(response);
                    }

                    _logMessage.AppendLine(string.Format("firstDataDetails.ProjectId:{0};firstDataDetails.EcommProjectId:{1};",
                                                                (firstDataDetails.ProjectId != null ? firstDataDetails.ProjectId : "<NULL>"),
                                                                (firstDataDetails.EcommProjectId != null ? firstDataDetails.EcommProjectId : "<NULL>")));

                    // Check merchant is configured for this industry type or not
                    if (_localVars.EntryMode == PaymentEntryMode.Keyed.ToString())
                        firstDataDetails.ProjectId = firstDataDetails.EcommProjectId;

                    if (String.IsNullOrEmpty(firstDataDetails.ProjectId))
                    {
                        _logMessage.AppendLine("Merchant is not configured for this industry type");
                        _logger.LogInfoMessage(_logMessage.ToString());

                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.MerchantNotConfiguredForThisIndustry, _localVars.DeviceId, _localVars.RequestId);
                        return Ok(response);
                    }

                    // Preparing transaction
                    _logMessage.AppendLine("Preparing transactions");

                    var cardType = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/CardType");

                    CardInfo cardInfo;
                    if (!_localVars.EntryMode.Equals(PaymentEntryMode.EmvContact.ToString()) &&
                        !_localVars.EntryMode.Equals(PaymentEntryMode.EmvContactless.ToString()))
                    {
                        _logMessage.AppendLine("Preparing NON-EMV transactions");

                        if (!NonEMVTransactions(out cardInfo, cardType))
                        {
                            _logMessage.AppendLine("Failed to prepare NON-EMV transactions.");
                            _logger.LogInfoMessage(_logMessage.ToString());

                            return Ok(response);
                        }
                    }
                    else
                    {
                        _logMessage.AppendLine("Preparing EMV transactions");

                        if (!EMVTransactions(out cardInfo, cardType))
                        {
                            _logMessage.AppendLine("Failed to prepare NON-EMV transactions.");
                            _logger.LogInfoMessage(_logMessage.ToString());

                            return Ok(response);
                        }
                    }

                    // Calculating Tech Fee
                    _logMessage.AppendLine("Preparing Fleet and Tech fee.");

                    decimal tip = Convert.ToDecimal(string.IsNullOrEmpty(_localVars.Tip) ? "0" : _localVars.Tip);
                    decimal surcharge = Convert.ToDecimal(string.IsNullOrEmpty(_localVars.Surcharge) ? "0" : _localVars.Surcharge);
                    decimal fare = Convert.ToDecimal(string.IsNullOrEmpty(_localVars.Fare) ? "0" : _localVars.Fare);
                    decimal fareTipSurcharge = tip + surcharge + fare;

                    _logMessage.AppendLine("Calculating Tech Fee.");

                    GetFleetTechFee(_transactionService, _commonService, _localVars.TxnType, Convert.ToInt32(_localVars.FleetId), _logger, fareTipSurcharge, out fleetFee, out techFee, out txnFee);
                    //zzz Chetu 357 : GetFleetTechFeeMTI(_transactionService, _commonService, _localVars.TxnType, Convert.ToInt32(_localVars.FleetId), _logger, fareTipSurcharge, out fleetFee, out techFee, out txnFee);

                    // Creating Transaction Request as per UMF specification.
                    _logMessage.AppendLine("Loading XML request data into TransactionRequest");

                    bool? isDispatchRequest = null;
                    if (_localVars.RequestType == "Android" || _localVars.RequestType == null)
                        isDispatchRequest = false;

                    if (_localVars.RequestType == "Dispatch")
                        isDispatchRequest = true;

                    TransactionRequest tr;
                    try
                    {
                        tr = InitiateTransaction(cardInfo, firstDataDetails);
                        tr.IsDispatchRequest = isDispatchRequest;
                        tr.FleetFee = Convert.ToDecimal(fleetFee);
                        tr.TechFee = Convert.ToDecimal(techFee);
                        tr.Fee = txnFee.ToString();     //zzz Why is the Fee set as a string????    //20161212 SH
                    }
                    catch (Exception ex)
                    {
                        _logMessage.AppendLine("Failed to load XML request data into TransactionRequest");
                        _logger.LogInfoFatal(_logMessage.ToString(), ex);

                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidTransactionRequest, _localVars.DeviceId, _localVars.RequestId);
                        return Ok(response);
                    }

                    _logMessage.AppendLine("Creating transaction request.");

                    CreateTransactionRequest(tr);

                    string xmlSerializedTransReq = GetXmlData();

                    _logMessage.AppendLine("GetXMLData exceuted successfully");

                    // Validating XML request
                    _logMessage.AppendLine("Validating XML request");

                    int transId;
                    string ccnum = tr.CardNumber;
                    string expiry = tr.ExpiryDate;
                    XmlDocument ValidateXml = new XmlDocument();
                    ValidateXml.LoadXml(xmlSerializedTransReq);
                    XmlElement elem = (XmlElement)ValidateXml.DocumentElement.FirstChild;

                    if (elem.IsNull())
                    {
                        _logMessage.AppendLine("Failed to validate XML request.");
                        _logger.LogInfoMessage(_logMessage.ToString());

                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidTransactionRequest, _localVars.DeviceId, _localVars.RequestId);
                        return Ok(response);
                    }

                    // Save transaction into MTDTransaction Table
                    _logMessage.AppendLine("calling XmlToTransactionDto takes xmlSerializedTransReq, tr");

                    tr.TerminalId = tr.TerminalId.FormatTerminalId();
                    MTDTransactionDto transactionDto = CommonFunctions.XmlToTransactionDto(tr);

                    _logMessage.AppendLine(string.Format("Fare:{0};Tip:{1};SurCH:{2};Fee:{3};TxnAmt:{4};", transactionDto.FareValue, transactionDto.Tip, transactionDto.Surcharge, transactionDto.Fee, transactionDto.Amount));
                    _logMessage.AppendLine("Inserting Transaction.");

                    transId = _transactionService.Add(transactionDto);

                    _logMessage.AppendLine(string.Format("Transaction inserted, transId-{0}", transId));
                    _logMessage.AppendLine("Updating stan parameters" + transactionDto.Stan + "--" + transactionDto.TransRefNo + "--" + tr.TerminalId + "--" + tr.MtdMerchantId);

                    _terminalService.UpdateStan(transactionDto.Stan, transactionDto.TransRefNo, tr.TerminalId, tr.MtdMerchantId);

                    string clientRef = GetClientRef(firstDataDetails.ProjectId);

                    _logMessage.AppendLine("GetClientRef executed successfully- " + clientRef);

                    string isLog = string.Empty;
                    isLog = WebConfigurationManager.AppSettings["LogPaymentResponse"];

                    //Send data using SOAP protocol to Datawire
                    _logMessage.AppendLine("Sending transaction request to rapid conect");

                    string xmlSerializedTransResp = xmlSerializedTransResp = new SoapHandler().SendMessage(xmlSerializedTransReq, clientRef, tr);

                    if (logReqResp == 2)
                    {
                        _logMessage.AppendLine("FD Request : \n" + xmlSerializedTransReq);
                        _logMessage.AppendLine("FD Response : \n" + xmlSerializedTransResp);
                    }

                    if (string.IsNullOrEmpty(xmlSerializedTransResp))
                    {
                        //resp is null,  initializing Timeout
                        _logMessage.AppendLine("xmlSerializedTransResp is null, initializing timeout.");

                        TimeoutTransaction objTimeout = new TimeoutTransaction(_transactionService, _terminalService, _merchantService, _logMessage);
                        tr.CardNumber = ccnum;
                        tr.ExpiryDate = expiry;
                        xmlSerializedTransResp = objTimeout.TimeOut(tr, transId, clientRef, xmlSerializedTransResp);

                        //Updating DB if null response found after all timeouts.
                        if (string.IsNullOrEmpty(xmlSerializedTransResp))
                        {
                            xmlSerializedTransResp = PaymentAPIResources.TimeoutError;

                            _logMessage.AppendLine("xmlSerializedTransResp is null after all timeout requests, updating database");

                            _transactionService.Update(new MTDTransactionDto() { AddRespData = PaymentAPIResources.FDServerDown, ResponseCode = PaymentAPIResources.ResponseCode200 }, transId, tr.RapidConnectAuthId);

                            _logMessage.AppendLine("Database updated successfully, process completed, returning error response.");
                            _logger.LogInfoMessage(_logMessage.ToString());

                            response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.FDServerDown, _localVars.DeviceId, _localVars.RequestId);
                            return Ok(response);
                        }

                        if (isLog == "1")
                            _logMessage.AppendLine("TOR ResponseXML " + xmlSerializedTransResp);
                    }
                    else
                    {
                        if (isLog == "1")
                            _logMessage.AppendLine("ResponseXML " + xmlSerializedTransResp);

                        //replacing invalid xml chars
                        var xmlResp = xmlSerializedTransResp.Replace("&", "&amp;");

                        //Updating transaction response into database.
                        _logMessage.AppendLine("xmlSerializedTransResp is not null");

                        MTDTransactionDto responsetransactionDto = CommonFunctions.XmlResponseToTransactionDto(xmlResp, false, tr.PaymentType.ToString(), tr.IndustryType);

                        //Making TerminalId of lenght 8 by appending prefixing zeros.
                        tr.TerminalId = tr.TerminalId.FormatTerminalId();

                        _logMessage.AppendLine("Updating xmlSerializedTransResp into database.");

                        _transactionService.Update(responsetransactionDto, transId, tr.RapidConnectAuthId);
                    }

                    PaymentResponse payResponse = new PaymentResponse();
                    if (!string.IsNullOrEmpty(xmlSerializedTransResp))
                        payResponse = CommonFunctions.TransactionResponse(tr, xmlSerializedTransResp, tr.PaymentType.ToString(), tr.IndustryType, transId);

                    _logTracking.AppendLine("Payment process completed. DriverNo: " + _localVars.DriverNo + " and  DeviceId " + _localVars.DeviceId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());
                    _logMessage.AppendLine("SendPayment process completed successfully.");
                    _logger.LogInfoMessage(_logTracking.ToString());
                    _logger.LogInfoMessage(_logMessage.ToString());

                    return Ok(payResponse);
                }
                catch (Exception ex)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), ex);
                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.Exception, _localVars.DeviceId, _localVars.RequestId);
                    return Ok(response);
                }
            }
        }

        #region Payment process for canada

        /// <summary>
        /// Purpose             :   Process canada transactions with first data 
        /// Function Name       :   CnSendPayment
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/25/2016
        /// Modification Made   :   ****************************
        /// Modified On       :    "MM/DD/YYYY" 
        /// </summary>
        /// <param name="cnRequest"></param>
        /// <returns>"transResp"</returns>   
        public IHttpActionResult CnSendPayment(HttpRequestMessage cnRequest)
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                _logMessage.AppendLine("Loading request XML data.");

                xmlDoc.Load(cnRequest.Content.ReadAsStreamAsync().Result);

                // Used to log request and response as per requirement
                int logReqResp = Convert.ToInt32(WebConfigurationManager.AppSettings["LogFirstDataRequestResponse"]);

                if (logReqResp == 1)
                    _logMessage.AppendLine("Android Request : \n" + xmlDoc.InnerXml);

                _logTracking.AppendLine("CnSendPayment process start. DriverNo: " + _localVars.DriverNo + " and DeviceId " + _localVars.DeviceId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());
                _logMessage.AppendLine("Initializing local variables from XML request data");

                // Initialize local variable from XML Request.

                if (!SetLocalVarFromXMLRequest(xmlDoc))
                {
                    _logMessage.AppendLine("Failed to initialize local variables from request XML data.");
                    _logger.LogInfoMessage(_logMessage.ToString());
                    return Ok(response);
                }

                // Validate Request
                _logMessage.AppendLine("Validating request xml data");

                dynamic firstDataDetails = string.Empty;
                //if (!ValidateRequest(response, mHeader, out firstDataDetails))
                if (!ValidateRequest(response, out firstDataDetails))
                {
                    _logMessage.AppendLine("Failed to validate request xml data.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    return Ok(response);
                }

                // Validate Refund/Void Request
                if (string.IsNullOrEmpty(_localVars.TransactionId) && _localVars.TxnType != TxnTypeType.Sale.ToString() && !string.IsNullOrEmpty(_localVars.RapidConnectAuthId))
                {
                    if (!ValidateRefundVoidRequest(Convert.ToInt32(_localVars.RapidConnectAuthId), firstDataDetails.fk_MerchantID))
                    {
                        _logMessage.AppendLine("This transaction is already refunded/voided : " + _localVars.RapidConnectAuthId);
                        _logger.LogInfoMessage(_logMessage.ToString());

                        return Ok(response);
                    }
                }
                else if (_localVars.TxnType != TxnTypeType.Sale.ToString())
                {
                    _localVars.TransactionId = string.Empty;
                }

                // Preparing transaction
                _logMessage.AppendLine("Preparing transactions");

                var cardType = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/CardType");

                CardInfo cardInfo;
                if (!_localVars.EntryMode.Equals(PaymentEntryMode.EmvContact.ToString()) && !_localVars.EntryMode.Equals(PaymentEntryMode.EmvContactless.ToString()))
                {
                    _logMessage.AppendLine("Preparing NON-EMV transactions");

                    if (!NonEMVTransactions(out cardInfo, cardType))
                    {
                        _logMessage.AppendLine("Failed to prepare NON-EMV transactions.");
                        _logger.LogInfoMessage(_logMessage.ToString());

                        return Ok(response);
                    }
                }
                else
                {
                    _logMessage.AppendLine("Preparing EMV transactions");

                    if (!EMVTransactions(out cardInfo, cardType))
                    {
                        _logMessage.AppendLine("Failed to prepare EMV transactions.");
                        _logger.LogInfoMessage(_logMessage.ToString());

                        return Ok(response);
                    }
                }

                _logMessage.AppendLine(string.Format("Track2Data:{0}", _localVars.Track2Data));

                // Calculating Tech Fee
                _logMessage.AppendLine("Preparing Fleet and Tech fee.");

                decimal tip = Convert.ToDecimal(string.IsNullOrEmpty(_localVars.Tip) ? "0" : _localVars.Tip);
                decimal surcharge = Convert.ToDecimal(string.IsNullOrEmpty(_localVars.Surcharge) ? "0" : _localVars.Surcharge);
                decimal fare = Convert.ToDecimal(string.IsNullOrEmpty(_localVars.Fare) ? "0" : _localVars.Fare);
                decimal fareTipSurcharge = tip + surcharge + fare;

                _logMessage.AppendLine("Calculating Tech Fee.");

                GetFleetTechFee(_transactionService, _commonService, _localVars.TxnType, Convert.ToInt32(_localVars.FleetId), _logger, fareTipSurcharge, out fleetFee, out techFee, out txnFee);
                //zzz Chetu 357 : GetFleetTechFeeMTI(_transactionService, _commonService, _localVars.TxnType, Convert.ToInt32(_localVars.FleetId), _logger, fareTipSurcharge, out fleetFee, out techFee, out txnFee);

                // Creating Transaction Request as per UMF specification.
                _logMessage.AppendLine("Loading XML request data into TransactionRequest");

                bool? isDispatchRequest = null;
                
                if (_localVars.RequestType == RequestType.Android.ToString() || _localVars.RequestType == null)
                    isDispatchRequest = false;
                
                if (_localVars.RequestType == RequestType.Dispatch.ToString())
                    isDispatchRequest = true;

                TransactionRequest tr;
                try
                {
                    tr = InitiateTransaction(cardInfo, firstDataDetails);
                    tr.IsDispatchRequest = isDispatchRequest;
                    tr.FleetFee = Convert.ToDecimal(fleetFee);
                    tr.TechFee = Convert.ToDecimal(techFee);
                    tr.Fee = txnFee.ToString();
                }
                catch (Exception ex)
                {
                    _logMessage.AppendLine("Failed to load XML request data into TransactionRequest");
                    _logger.LogInfoFatal(_logMessage.ToString(), ex);

                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidTransactionRequest, _localVars.DeviceId, _localVars.RequestId);
                    return Ok(response);
                }

                tr.MerchantZipCode = _merchantService.GetMerchantZipCode(tr.MtdMerchantId);

                tr.IsTransArmor = _merchantService.IsTransArmor(tr.MtdMerchantId);      //PAY-13 Get the Merchant UseTransArmor flag as well.

                // Prepare payload in iso8583 message format
                string clientRef = CnTransactionMessage.GetCanadaClientRef();
                tr.Stan = GetSystemTraceNumber(firstDataDetails.fk_MerchantID, firstDataDetails.RCTerminalId);

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    _logMessage.AppendLine("GetClientRef executed successfully" + clientRef);
                
                TransactionProcessed transType = (TransactionProcessed)Enum.Parse(typeof(TransactionProcessed), tr.TransactionType);

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    _logMessage.AppendLine(string.Format("CnSendPayment() transType:{0};PortalEmvRefundVoidReq:{1};IsTOR:{2};IsTA:{3};TerminalID:{4};Lane:{5};", transType, _localVars.PortalEmvRefundVoidReq, tr.IsTORTransaction, tr.IsTransArmor, tr.TerminalId, tr.TerminalLaneNo));

                string referenceNumber = _transactionService.GetRefNumber();

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    _logMessage.AppendLine(string.Format("CnSendPayment() referenceNumber:{0};transType:{1};", referenceNumber, transType));

                switch (transType)
                {
                    case TransactionProcessed.Completion:
                        var originalTransaction = _transactionService.GetTransaction(Convert.ToInt32(tr.RapidConnectAuthId));
                        tr.TransRefNo = originalTransaction.TransRefNo;
                        tr.AuthIdentResponse = originalTransaction.AuthId;
                        tr.ResponseCode = originalTransaction.ResponseCode;
                        
                        //tr.AddRespdata = FormatFields.PrepareAddRespData(originalTransaction.AddRespData);
                        //tr.CardNumber = string.Empty;
                        if (tr.IndustryType == PaymentIndustryType.Retail.ToString())
                        {
                            tr.Track2Data = string.Empty;
                            tr.Track3Data = string.Empty;
                        }

                        if (tr.IndustryType == PaymentIndustryType.Retail.ToString() && tr.TransactionType == TxnTypeType.Completion.ToString())
                            tr.CardType = (CardTypeType)Enum.Parse(typeof(CardTypeType), originalTransaction.CardType);

                        var bit63Response = originalTransaction.ResponseXml.ParseBit63Resoponse(tr.CardType.ToString(), tr.IndustryType);
                        tr.RespTable14 = bit63Response.Table14Response;
                        tr.RespTable49 = bit63Response.Table49Response;
                        tr.RespTableVI = bit63Response.TableVIResponse;
                        tr.RespTableSP = bit63Response.TableSPResponse;
                        tr.RespTableMC = bit63Response.TableMCResponse;
                        tr.RespTableDS = bit63Response.TableDSResponse;
                        break;

                    case TransactionProcessed.Refund:
                        var originalTransData1 = _transactionService.GetTransaction(Convert.ToInt32(tr.RapidConnectAuthId));
                        tr.OriginalTransType = originalTransData1.TxnType;
                        
                        tr.FirstFourCard = originalTransData1.FirstFourDigits;
                        tr.LastFourcard = originalTransData1.LastFourDigits;

                        tr.ResponseCode = originalTransData1.ResponseCode;
                        tr.ResponseBit63 = originalTransData1.ResponseBit63;
                        tr.MAXAmount = originalTransData1.Amount.ToString();
                        tr.TransRefNo = FormatFields.FormatReferenceNo(tr.TerminalId, tr.Stan, tr.PaymentType.ToString(), referenceNumber);

                        if (originalTransData1.TxnType == TxnTypeType.Completion.ToString())
                            tr.SourceTransaction = TxnTypeType.Completion.ToString();

                        tr.CardNumber = string.Empty;

                        if (_localVars.PortalEmvRefundVoidReq)
                        {
                            XmlDocument transData = new XmlDocument();
                            transData.LoadXml(originalTransData1.RequestXml);
                            tr.CardNumber = transData.SelectSingleNode("TransactionRequest/CardNumber").InnerText;
                        }

                        break;

                    case TransactionProcessed.Void:
                        if (string.IsNullOrEmpty(tr.RapidConnectAuthId) && !string.IsNullOrEmpty(_localVars.VoidRequestId))
                        {
                            tr.RapidConnectAuthId = _transactionService.GetTransactionId(_localVars.VoidRequestId);
                            if (string.IsNullOrEmpty(tr.RapidConnectAuthId))
                            {
                                response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.RequestIdError, _localVars.DeviceId, _localVars.RequestId);
                                return Ok(response);
                            }

                            if (_transactionService.IsMacVerificationFailed(tr.RapidConnectAuthId))
                                tr.IsMAcVerificationFailed = true;
                            else
                                tr.IsMAcVerificationFailed = false;
                        }

                        var originalTransData = _transactionService.GetTransaction(Convert.ToInt32(tr.RapidConnectAuthId));
                        tr.TrnmsnDateTime = originalTransData.TransmissionDateTime;
                        tr.Stan = originalTransData.Stan;
                        tr.MAXAmount = originalTransData.Amount.ToString();
                        tr.OriginalTransType = originalTransData.TxnType;
                        tr.TransRefNo = originalTransData.TransRefNo;
                        tr.AuthIdentResponse = originalTransData.AuthId;
                        tr.CardNumber = string.Empty;
                        tr.IsReversal = true;
                        tr.ResponseCode = originalTransData.ResponseCode;
                        tr.ResponseBit63 = originalTransData.ResponseBit63;

                        if (originalTransData.TxnType == TxnTypeType.Completion.ToString())
                            tr.SourceTransaction = TxnTypeType.Completion.ToString();

                        if (originalTransData.TxnType == TxnTypeType.Authorization.ToString())
                            tr.SourceTransaction = TxnTypeType.Authorization.ToString();

                        //zzz Chetu 357
                        if (string.IsNullOrEmpty(tr.ResponseBit63))
                        {
                            response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.No_Driver_Transaction, _localVars.DeviceId, _localVars.RequestId);
                            return Ok(response);
                        }

                        break;

                    default:
                        tr.TransRefNo = FormatFields.FormatReferenceNo(tr.TerminalId, tr.Stan, tr.PaymentType.ToString(), referenceNumber);
                        break;
                }

                //Changes done for EMV Contact certification of Canada
                CnTransactionEntities cnTrans = new CnTransactionEntities();
                PaymentIndustryType industryType = (PaymentIndustryType)Enum.Parse(typeof(PaymentIndustryType), tr.IndustryType);
                var entryMode = (PaymentEntryMode)Enum.Parse(typeof(PaymentEntryMode), tr.EntryMode, true);

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                {
                    _logMessage.AppendLine(string.Format("CnSendPayment() IndustryType:{0};clientRef:{1};entryMode:{2};CardType:{3};IsTOR:{4};IsTA:{5};", industryType, clientRef, entryMode, tr.CardType.ToString(), tr.IsTORTransaction, tr.IsTransArmor));
                    _logMessage.AppendLine(string.Format("CnSendPayment() ResponseBit63:{0};", tr.ResponseBit63));
                }

                switch (industryType)
                {
                    case PaymentIndustryType.Ecommerce:
                        tr.CnPayload = CnPreparePostTransaction.CheckAndPrepareEcommTrans(tr, clientRef, out cnTrans);
                        break;
                    case PaymentIndustryType.Retail:
                        switch (entryMode)
                        {
                            // MSRContact
                            case PaymentEntryMode.Swiped:
                                tr.CnPayload = CnPreparePostTransaction.CheckAndPrepareRetailTrans(tr, clientRef, out cnTrans);
                                break;

                            // MSRContactless
                            case PaymentEntryMode.MSRContactless:
                                //for MSR Contactless transaction of Canada
                                //PAY-13 
                                tr.CnPayload = CnPreparePostTransaction.PrepareMSRContactlessTransRequest(tr, clientRef, out cnTrans);
                                break;

                            // EMVContact Canada
                            case PaymentEntryMode.EmvContact:
                                if (tr.CardType.ToString() == CardTypeType.Interac.ToString())
                                {
                                    // Set stan & rrn no from request for Interac
                                    tr.Stan = _localVars.Stan;
                                    tr.TransRefNo = _localVars.RRN;
                                    tr.CnPayload = CnPreparePostTransaction.PrepareInteracTransRequest(tr, clientRef, out cnTrans); //for Interac
                                }
                                else
                                {
                                    tr.CnPayload = CnPreparePostTransaction.PrepareEMVTransRequest(tr, clientRef, out cnTrans);
                                }
                                break;

                            // EMVContactless Canada
                            case PaymentEntryMode.EmvContactless:
                                //for Credit & Interac Contactless transaction
                                if (tr.CardType.ToString() == CardTypeType.Interac.ToString())
                                {
                                    //set stan & rrn no from request for Interac
                                    tr.Stan = _localVars.Stan;
                                    tr.TransRefNo = _localVars.RRN;
                                    tr.CnPayload = CnPreparePostTransaction.PrepareInteracContactlessTransRequest(tr, clientRef, out cnTrans); //for Interac
                                }
                                else
                                {
                                    tr.CnPayload = CnPreparePostTransaction.PrepareEMVContactlessTrans(tr, clientRef, out cnTrans);
                                }
                                break;

                            // FSwiped
                            case PaymentEntryMode.FSwiped:
                                tr.CnPayload = CnPreparePostTransaction.PrepareFallbackCreditTrans(tr, clientRef, out cnTrans);
                                break;
                        }

                        break;
                }

                // Save transaction into MTDTransaction Table and updating stan
                tr.TerminalId = tr.TerminalId.FormatTerminalId();
                MTDTransactionDto transactionDto = CommonFunctions.XmlToTransactionDto(tr);

                if (tr.TransactionType == TxnTypeType.Completion.ToString())
                    transactionDto.SourceId = tr.RapidConnectAuthId;

                _logMessage.AppendLine(string.Format("Fare:{0};Tip:{1};Surcharge:{2};Fee:{3};", transactionDto.FareValue, transactionDto.Tip, transactionDto.Surcharge, transactionDto.Fee, transactionDto.Amount));
                _logMessage.AppendLine("Inserting Transaction.");

                transId = _transactionService.Add(transactionDto);

                _logMessage.AppendLine(string.Format("Update stan parameters Stan:{0};TransRefNo:{1};TerminalId:{2};MerchantId:{3};TransId:{4};", transactionDto.Stan, transactionDto.TransRefNo, tr.TerminalId, tr.MtdMerchantId, transId));

                _terminalService.UpdateStan(transactionDto.Stan, transactionDto.TransRefNo, tr.TerminalId, tr.MtdMerchantId);

                /// MORE LOGGING REQUIRED
                // Sending transaction request and parsing the response then save in the database

                Global.ActiveUrl = _transactionService.GetTransactionUrl();

                _logMessage.AppendLine(string.Format("ActiveUrl:{0};", Global.ActiveUrl));

                string transResponse = CnSendTransaction.SendMessageToDatawire(Global.ActiveUrl, tr.CnPayload);

                _logMessage.AppendLine(string.Format("TransResponse:{0};CnPayload:{1};", transResponse, tr.CnPayload));

                cnTrans.CompleteResponse = transResponse;

                Iso8583Response isoResponse = HandleResponse(cnTrans, transId);

                _logMessage.AppendLine(string.Format("isoResponse.Message:{0};cnTrans.ResponseCode:{1};cnTrans.CompleteRequest:{2};cnTrans.CompleteResponse:{3};TagAID:{4};FDPrivData63:{5};",
                    isoResponse.Message, cnTrans.ResponseCode, cnTrans.CompleteRequest, cnTrans.CompleteResponse,
                    _localVars.TagAID,
                    (string.IsNullOrEmpty(isoResponse.FDPrivateUsageData63) ? "<<NULL>>" : isoResponse.FDPrivateUsageData63)));


                //Changes done for EMV Contact certification of Canada for Interac transaction response               
                //if (_localVars.TagAID == InteracAID)
                if (_localVars.TagAID == SigmaStaticValue.InteracAID)
                {
                    //var table63Data = CommonFunctions.GetTable63Data(isoResponse.FDPrivateUsageData63);
                    //isoResponse.Bit32 = CnGenericHandler.ConvertHexToAscii(table63Data["3332"]).PadRight(20, ' ');
                    //isoResponse.Bit34 = CnGenericHandler.ConvertHexToAscii(table63Data["3334"]).PadRight(48, ' ');
                    //isoResponse.Message = CnGenericHandler.ConvertHexToAscii(table63Data["3232"]).Trim();

                    //zzz Chetu 357
                    try
                    {
                        var table63Data = CommonFunctions.GetTable63Data(isoResponse.FDPrivateUsageData63);
                        isoResponse.Bit32 = CnGenericHandler.ConvertHexToAscii(table63Data["3332"]).PadRight(20, ' ');
                        isoResponse.Bit34 = CnGenericHandler.ConvertHexToAscii(table63Data["3334"]).PadRight(48, ' ');
                        isoResponse.Message = CnGenericHandler.ConvertHexToAscii(table63Data["3232"]).Trim();
                    }
                    catch (Exception ex)
                    {
                        _logMessage.AppendLine(string.Format("isoResponse.FDPrivateUsageData63 cannot be processed. Ex:{0}", ex.ToString()));

                        isoResponse.Bit32 = CnGenericHandler.ConvertHexToAscii("").PadRight(20, ' ');
                        isoResponse.Bit34 = CnGenericHandler.ConvertHexToAscii("").PadRight(48, ' ');
                        isoResponse.Message = CnGenericHandler.ConvertHexToAscii("").Trim();
                    }
                }

                //zzz WHILE loop
                int iteration = 1;    // For debug purposes.
                while (String.IsNullOrEmpty(transResponse))
                {
                    _logMessage.AppendLine(string.Format("TransResponse handling:{0};Iteration:{1};Global.ActiveUrl:{2};", transResponse, iteration, Global.ActiveUrl));

                    if (Global.UrlList != null)
                    {
                        _logMessage.AppendLine(string.Format("TransResponse UrlListCount:{0};", Global.UrlList.Count));

                        if (Global.UrlList.Contains(Global.ActiveUrl))
                            Global.UrlList.Remove(Global.ActiveUrl);
                    }

                    string fastestUrl = _transactionService.GetFastestUrl(Global.ActiveUrl, Global.UrlList);

                    if (String.IsNullOrEmpty(fastestUrl))
                    {
                        _logMessage.AppendLine(string.Format("TransResponse fastestUrl:<EMPTY>;"));

                        ServiceDiscovery();

                        Global.ActiveUrl = _transactionService.GetTransactionUrl();

                        _logMessage.AppendLine(string.Format("TransResponse ActiveUrl:{0};CnPayload:{1};", Global.ActiveUrl, tr.CnPayload));

                        transResponse = CnSendTransaction.SendMessageToDatawire(Global.ActiveUrl, tr.CnPayload);

                        _logMessage.AppendLine(string.Format("TransResponse transResponse:{0};", string.IsNullOrEmpty(transResponse) ? "<EMPTY>" : transResponse));

                        cnTrans.CompleteResponse = transResponse;
                        isoResponse = HandleResponse(cnTrans, transId);
                    }
                    else
                    {
                        _logMessage.AppendLine(string.Format("TransResponse fastestUrl:{0};", fastestUrl));

                        Global.ActiveUrl = fastestUrl;

                        _logMessage.AppendLine(string.Format("TransResponse ActiveUrl:{0};CnPayload:{1};", Global.ActiveUrl, tr.CnPayload));

                        transResponse = CnSendTransaction.SendMessageToDatawire(Global.ActiveUrl, tr.CnPayload);

                        _logMessage.AppendLine(string.Format("TransResponse transResponse:{0};", string.IsNullOrEmpty(transResponse) ? "<EMPTY>" : transResponse));
                        
                        cnTrans.CompleteResponse = transResponse;
                        isoResponse = HandleResponse(cnTrans, transId);
                    }

                    iteration++;    // For debug purposes.

                    if (String.IsNullOrEmpty(transResponse))
                        continue;
                }

                // Add config flag to always force a timeout.
                bool debugTimeout = (ConfigurationManager.AppSettings["DebugTimeout"] == "Y");

                if (CommonFunctions.IsFailed(isoResponse.ReturnCode) || debugTimeout)
                {
                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    {
                        if (debugTimeout)
                            _logger.LogInfoMessage("PaymentController.CnSendPayment() Manually forced timeout for Debug.");
                        else
                            _logger.LogInfoMessage("PaymentController.CnSendPayment() isoResponse triggered Timeout.");

                        _logger.LogInfoMessage(string.Format("PaymentController.CnSendPayment() TransId:{0};ResponseBit63:{1};CnPayload:{2};", transId, tr.ResponseBit63, tr.CnPayload));
                    }

                    CnTimeOutTransaction timeOutTransaction = new CnTimeOutTransaction(_transactionService, _terminalService, _logMessage);
                    isoResponse = timeOutTransaction.TimeOut(tr, transId, clientRef);
                    if (Object.Equals(isoResponse, null))
                    {
                        isoResponse = new Iso8583Response();
                        isoResponse.Message = PaymentAPIResources.CnTimeOutError;
                        isoResponse.AdditionalResponseData = PaymentAPIResources.CnTimeOutError;
                    }
                }

                string objResponse = Serialization.Serialize<Iso8583Response>(isoResponse);
                isoResponse.CompleteResponse = objResponse;

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                {
                    _logger.LogInfoMessage(string.Format("PaymentController.CnSendPayment() CnPayload:{0};IsTOR:{1};IsTA:{2};ISO=CompleteResp:{3};AddRespData:{4};Msg:{5};RespCode:{6};RespMsgID:{7};RetCode:{8};",
                                    tr.CnPayload, tr.IsTORTransaction, tr.IsTransArmor, isoResponse.CompleteResponse, isoResponse.AdditionalResponseData, isoResponse.Message, isoResponse.ResponseCode, isoResponse.ResponseMessageID, isoResponse.ReturnCode));
                }

                if (!tr.IsTORTransaction)
                {
                    MTDTransactionDto transDto = CnTransactionMessage.FillMerchantDto(isoResponse);
                    if (tr.TransactionType == TxnTypeType.Completion.ToString())
                        transDto.SourceId = tr.RapidConnectAuthId;

                    _transactionService.CnUpdate(transDto, transId);
                }

                // Changes done for EMV Contact certification of Canada
                // Ensure we have all the required items and no null\obj ref as these cause exceptions.
                if (isoResponse == null)
                    _logMessage.AppendLine("CnSendPayment isoResponse is NULL.");

                if (_localVars == null)
                    _logMessage.AppendLine("CnSendPayment _localVars is NULL.");

                if (firstDataDetails.FDMerchantID == null)
                    _logMessage.AppendLine("CnSendPayment firstDataDetails.FDMerchantID is NULL.");

                var finalResponse = CommonFunctions.IsoToPaymentResponse(isoResponse, _localVars, firstDataDetails.FDMerchantID);

                _logMessage.AppendLine("CnSendPayment process completed successfully.");
                _logTracking.AppendLine("CnSendPayment process completed. DriverNo: " + _localVars.DriverNo + " and  DeviceId " + _localVars.DeviceId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());

                return Ok(finalResponse);
            }
            catch (Exception ex)
            {
                _logger.LogInfoMessage(_logTracking.ToString());
                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.Exception, _localVars.DeviceId, _localVars.RequestId);

                return Ok(response);
            }
            finally
            {
                _logger.LogInfoMessage(_logTracking.ToString());
                _logger.LogInfoMessage(_logMessage.ToString());
            }
        }

        #endregion

        /// <summary>
        /// Processing Non-EMV transactions
        /// </summary>
        /// <param name="cardInfo"></param>
        /// <param name="cardType"></param>
        private bool NonEMVTransactions(out CardInfo cardInfo, XmlNode cardType)
        {
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
            {
                _logger.LogInfoMessage(string.Format("PaymentController.NonEMVTransactions() cardType:{0};TransType:{1};IndustryType:{2};PayType:{3};", cardType.OuterXml, _localVars.TransType, _localVars.IndustryType, _localVars.PayType));
                _logger.LogInfoMessage(string.Format("PaymentController.NonEMVTransactions() Ccno:{0};Track2Data:{1};", _localVars.Ccno, _localVars.Track2Data));
            }

            try
            {
                if (_localVars.IndustryType.Equals(PaymentIndustryType.Ecommerce.ToString()) && (_localVars.TransType.Equals(TxnTypeType.Sale) || _localVars.TransType.Equals(TxnTypeType.Authorization)))
                {
                    cardInfo = StringExtension.GetCardNumberData(_localVars.Ccno);

                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("PaymentController.NonEMVTransactions()-1 cardInfo:{0};", cardInfo.ToString()));
                }
                else if (_localVars.IndustryType.Equals(PaymentIndustryType.Retail.ToString()) && (_localVars.TransType.Equals(TxnTypeType.Sale) || _localVars.TransType.Equals(TxnTypeType.Authorization)))
                {
                    var encryprtBlock = StringExtension.GetTrackThreeData(_localVars.Track3Data);
                    if (!encryprtBlock.IsNull() && encryprtBlock.Length >= 3)
                    {
                        _localVars.Track3Data = encryprtBlock[0];
                        string key = encryprtBlock[2];
                        _localVars.KeyId = key.ToString().TrimEnd('\r', '\n');
                    }

                    cardInfo = StringExtension.GetCardNumberData(_localVars.Track2Data);

                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("PaymentController.NonEMVTransactions()-2 cardInfo:{0};", cardInfo.ToString()));

                    if (string.IsNullOrEmpty(cardInfo.ErrorMessage))
                    {
                        _localVars.ExpiryDate = cardInfo.YYYYMM;
                        _localVars.ServiceCode = cardInfo.ServiceCode;
                        if (!_localVars.EntryMode.Equals(PaymentEntryMode.MSRContactless.ToString()))
                        {
                            if (!string.IsNullOrEmpty(_localVars.ServiceCode) && (_localVars.ServiceCode.Trim().StartsWith("2") || _localVars.ServiceCode.Trim().StartsWith("6")))
                            {
                                _localVars.EntryMode = "FSwiped";
                            }
                        }

                        _localVars.Ccno = cardInfo.CardNumber;
                    }
                }

                if (!string.IsNullOrEmpty(cardType.InnerText) && _localVars.PayType.ToString() != PaymentCardType.Debit.ToString())
                {
                    _localVars.CardType = (CardTypeType)Enum.Parse(typeof(CardTypeType), cardType.InnerText);
                }
                else if (string.IsNullOrEmpty(cardType.InnerText) && !string.IsNullOrEmpty(_localVars.Ccno) && _localVars.PayType.ToString() != PaymentCardType.Debit.ToString())
                {
                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("PaymentController.NonEMVTransactions() cardInfo:{0};Ccno:{1};", cardInfo.ToString(), _localVars.Ccno));

                    if (string.IsNullOrEmpty(cardInfo.ErrorMessage))
                    {
                        cardInfo.CardNumber = _localVars.Ccno;
                        _localVars.CardType = CreditCardType.GetCardType(cardInfo.CardNumber);
                    }
                    else
                    {
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidCardData, _localVars.DeviceId, _localVars.RequestId);
                        return false;
                    }
                }
                else if (_localVars.PayType.ToString() == PaymentCardType.Debit.ToString() && !_localVars.IndustryType.Equals(PaymentIndustryType.Ecommerce.ToString()))
                {
                    _localVars.CardType = CreditCardType.GetCardType(cardInfo.CardNumber);
                    string[] pinSerial = StringExtension.GetPinSerial(_localVars.PinData);
                    if (!pinSerial.IsNull() && pinSerial.Length >= 2)
                    {
                        _localVars.DebitPin = pinSerial[0];
                        _localVars.KCN = pinSerial[1];
                    }
                    else
                    {
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidCardData, _localVars.DeviceId, _localVars.RequestId);
                        return false;
                    }
                }

                return true;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Processing EMV Transactions
        /// </summary>
        /// <param name="cardInfo"></param>
        /// <param name="cardType"></param>
        /// <param name="isFailed"></param>
        private bool EMVTransactions(out CardInfo cardInfo, XmlNode cardType)
        {
            try
            {
                //Extracting EMV data from XML request
                EmvDataValues emvDataRequest = _localVars.EmvData.GetEMVdata(EmvTagType.AuthorizationRequestEMVTags);

                if (!string.IsNullOrEmpty(emvDataRequest.PaymentType.ToString()))
                    _localVars.PayType = emvDataRequest.PaymentType;

                if ((!string.IsNullOrEmpty(emvDataRequest.PinData) && _localVars.PayType.ToString() == PaymentCardType.Credit.ToString()) && !_localVars.IndustryType.Equals(PaymentIndustryType.Ecommerce.ToString()))
                {
                    _localVars.PinData = emvDataRequest.PinData;
                    string[] pinSerial = StringExtension.GetPinSerial(_localVars.PinData);
                    if (!pinSerial.IsNull() && pinSerial.Length >= 2)
                    {
                        _localVars.DebitPin = pinSerial[0];
                        _localVars.KCN = pinSerial[1];
                    }
                    else
                    {
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidCardData, _localVars.DeviceId, _localVars.RequestId);
                        return false;
                    }
                }

                //Track2 data
                _localVars.CSequence = emvDataRequest.CSequence;
                _localVars.Track2Data = emvDataRequest.Track2;

                //Preparing Expiry date
                cardInfo = StringExtension.GetCardNumberData(_localVars.Track2Data);

                if (string.IsNullOrEmpty(cardInfo.ErrorMessage))
                {
                    _localVars.ExpiryDate = cardInfo.YYYYMM;
                    _localVars.Ccno = cardInfo.CardNumber;
                }
                else
                {
                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidCardData, _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }
                if (!string.IsNullOrEmpty(cardType.InnerText) && _localVars.PayType.ToString() != PaymentCardType.Debit.ToString())
                {
                    _localVars.CardType = (CardTypeType)Enum.Parse(typeof(CardTypeType), cardType.InnerText);
                }
                else if (string.IsNullOrEmpty(cardType.InnerText) && !string.IsNullOrEmpty(_localVars.Ccno) && _localVars.PayType.ToString() != PaymentCardType.Debit.ToString())
                {
                    if (string.IsNullOrEmpty(cardInfo.ErrorMessage))
                    {
                        _localVars.CardType = CreditCardType.GetCardType(cardInfo.CardNumber);
                    }
                    else
                    {
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidCardData, _localVars.DeviceId, _localVars.RequestId);
                        return false;
                    }
                }
                else if (_localVars.PayType.ToString() == PaymentCardType.Debit.ToString() && !_localVars.IndustryType.Equals(PaymentIndustryType.Ecommerce.ToString()))
                {
                    // Changes done for EMV Contact certification of Canada
                    if (ConfigurationManager.AppSettings["Project"] == PaymentAPIResources.CanadaProject && emvDataRequest.TagAID == SigmaStaticValue.InteracAID)
                        _localVars.CardType = CardTypeType.Interac;
                    else
                        _localVars.CardType = CreditCardType.GetCardType(cardInfo.CardNumber);

                    _localVars.PinData = emvDataRequest.PinData;
                    string[] pinSerial = StringExtension.GetPinSerial(_localVars.PinData);
                    if (!pinSerial.IsNull() && pinSerial.Length >= 2)
                    {
                        _localVars.DebitPin = pinSerial[0];
                        _localVars.KCN = pinSerial[1];
                    }
                    else
                    {
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidCardData, _localVars.DeviceId, _localVars.RequestId);
                        return false;
                    }
                }

                //Track3 data
                _localVars.Track3Data = emvDataRequest.Track3;

                //EMV data
                _localVars.EmvData = emvDataRequest.EmvData;

                //Preparing Encryt Block
                if (!string.IsNullOrEmpty(_localVars.Track3Data))
                {
                    string[] encryprtBlock = StringExtension.GetTrackThreeData(_localVars.Track3Data);
                    if (!encryprtBlock.IsNull() && encryprtBlock.Length >= 3)
                    {
                        _localVars.Track3Data = encryprtBlock[0];
                        _localVars.KeyId = encryprtBlock[2];
                    }
                    else
                    {
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidCardData, _localVars.DeviceId, _localVars.RequestId);
                        return false;
                    }
                }

                // Changes done for EMV Contact certification of Canada
                _localVars.TxnAmount = emvDataRequest.Amount;
                _localVars.AccountType = emvDataRequest.AccountType;
                _localVars.TagAID = emvDataRequest.TagAID;
                _localVars.TerminalLaneNo = emvDataRequest.DeviceSerialNo;

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                {
                    _logger.LogInfoMessage(string.Format("PaymentController.EMVTransactions() EMV:{0};", _localVars.EmvData));
                    _logger.LogInfoMessage(string.Format("PaymentController.EMVTransactions() Track2:{0};", _localVars.Track2Data));
                    _logger.LogInfoMessage(string.Format("PaymentController.EMVTransactions() Track3:{0};", _localVars.Track3Data));
                    _logger.LogInfoMessage(string.Format("PaymentController.EMVTransactions() CardInfo:{0};", cardInfo.ToString()));
                    _logger.LogInfoMessage(string.Format("PaymentController.EMVTransactions() TxnAmt:{0};AccType:{1};TagAID:{2};TerminalLaneNo:{3};KeyID:{4};", _localVars.TxnAmount, _localVars.AccountType, _localVars.TagAID, _localVars.TerminalLaneNo, _localVars.KeyId));
                }

                return true;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   Extract the value from XMLrequest and set to Local variables
        /// Function Name       :   SetLocalVarFromXMLRequest
        /// Created By          :   Sunil Singh
        /// Created On          :   14-Aug-2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private bool SetLocalVarFromXMLRequest(XmlDocument xmlDoc)
        {
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                _logger.LogInfoMessage(string.Format("PaymentController.SetLocalVarFromXMLRequest() xmlDoc:{0};", xmlDoc.OuterXml));

            try
            {
                var dvcID = xmlDoc.SelectSingleNode("PaymentRequest/Header/DeviceId");
                if (!dvcID.IsNull())
                    _localVars.DeviceId = dvcID.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/DeviceId".MissingElementMessage());
                    return false;
                }

                var reqID = xmlDoc.SelectSingleNode("PaymentRequest/Header/RequestId");
                if (!reqID.IsNull())
                    _localVars.RequestId = reqID.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/RequestId".MissingElementMessage());
                    return false;
                }

                var drvrID = xmlDoc.SelectSingleNode("PaymentRequest/Header/DriverId");
                if (!drvrID.IsNull())
                    _localVars.DriverNo = drvrID.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/DriverId".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var token = xmlDoc.SelectSingleNode("PaymentRequest/Header/Token");
                if (!token.IsNull())
                    _localVars.Token = token.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/Token".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var requestType = xmlDoc.SelectSingleNode("PaymentRequest/Header/RequestType");
                if (!requestType.IsNull())
                    _localVars.RequestType = requestType.InnerText;
                else
                {
                    _localVars.RequestType = null;
                }

                var entryMode = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/EntryMode");
                if (!entryMode.IsNull())
                {
                    _localVars.EntryMode = entryMode.InnerText;
                    if (string.IsNullOrEmpty(_localVars.EntryMode))
                    {
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidEntryMode, _localVars.DeviceId, _localVars.RequestId);
                        return false;
                    }
                }
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/EntryMode".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var curCode = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/CurrencyCode");
                if (!curCode.IsNull())
                    _localVars.Currency = curCode.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/CurrencyCode".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                //----------
                var emvData = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/EmvData");
                if (!emvData.IsNull())
                    _localVars.EmvData = emvData.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/EmvData".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var ccno = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/CardNumber");
                if (!ccno.IsNull())
                    _localVars.Ccno = ccno.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/CardNumber".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var expDate = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/ExpiryDate");
                if (!expDate.IsNull())
                    _localVars.ExpiryDate = expDate.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/ExpiryDate".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var track2Data = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Track2Data");
                if (!track2Data.IsNull())
                {
                    _localVars.Track2Data = track2Data.InnerText;
                    //zzz Chetu 357 : if (ConfigurationManager.AppSettings["Project"] == PaymentAPIResources.CanadaProject && !_localVars.Track2Data.StartsWith(SigmaStaticValue.SemiColon.ToString())) //added to prepare track2 data compatible for canada
                    if (ConfigurationManager.AppSettings["Project"] == PaymentAPIResources.CanadaProject) //added to prepare track2 data compatible for canada
                        _localVars.Track2Data = SigmaStaticValue.SemiColon + _localVars.Track2Data;
                }
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Track2Data".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var track3Data = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Track3Data");
                if (!track3Data.IsNull())
                    _localVars.Track3Data = track3Data.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Track3Data".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var PinData = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/PinData");
                if (!PinData.IsNull())
                    _localVars.PinData = PinData.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/PinData".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var transType = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/TransactionType");
                if (!transType.IsNull())
                    _localVars.TxnType = transType.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/TransactionType".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                if (_localVars.TxnType != PaymentTransactionType.Void.ToString())
                {
                    if (!transType.IsNull())
                        _localVars.TransType = (TxnTypeType)Enum.Parse(typeof(TxnTypeType), transType.InnerText);
                }

                var paymentType = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/PaymentType");
                if (_localVars.EntryMode != PaymentEntryMode.EmvContact.ToString() && _localVars.EntryMode != PaymentEntryMode.EmvContactless.ToString())
                {
                    if (!paymentType.IsNull())
                    {
                        _localVars.PayType = (PymtTypeType)Enum.Parse(typeof(PymtTypeType), paymentType.InnerText);
                    }
                    else
                    {
                        response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/PaymentType".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                        return false;
                    }
                }

                var txnAmountTag = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/TransactionAmount");
                if (!txnAmountTag.IsNull())
                    _localVars.TxnAmount = txnAmountTag.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/TransactionAmount".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var indType = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/IndustryType");
                if (!indType.IsNull())
                {
                    _localVars.IndustryType = indType.InnerText;
                    if (string.IsNullOrEmpty(_localVars.IndustryType))
                    {
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidIndustryType, _localVars.DeviceId, _localVars.RequestId);
                        return false;
                    }
                }
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/IndustryType".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                if (_localVars.IndustryType.Equals(PaymentIndustryType.Ecommerce.ToString()))
                {
                    var createdBy = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/CreatedBy");
                    if (!createdBy.IsNull())
                    {
                        _localVars.CreatedBy = createdBy.InnerText;
                    }
                    else
                    {
                        response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/CreatedBy".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                        return false;
                    }
                }
                else
                {
                    _localVars.CreatedBy = _localVars.DriverNo;
                }

                //Transaction request variable initialization
                var cvv = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/CcvData");
                if (!txnAmountTag.IsNull())
                    _localVars.CardCvv = cvv.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/CcvData".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var vehicleNum = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/VehicleNumber");
                if (!vehicleNum.IsNull())
                    _localVars.VehicleNumber = vehicleNum.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/VehicleNumber".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var rapidConnectAuthId = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/RapidConnectAuthId");
                if (!rapidConnectAuthId.IsNull())
                    _localVars.RapidConnectAuthId = rapidConnectAuthId.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/RapidConnectAuthId".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var transNote = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/TransNote");
                if (!transNote.IsNull())
                    _localVars.TransNote = transNote.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/TransNote".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var streetAddress = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/StreetAddress");
                if (!streetAddress.IsNull())
                    _localVars.StreetAddress = streetAddress.InnerText;
                else
                {
                    //T#6162 Commented out until proven needed again.
                    //if (_localVars.TxnType.ToUpper() != "REFUND")    //D#5517 No StreetAddress present for Refund transaction type, so ignore.
                    //{
                    //    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/StreetAddress".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    //    return false;
                    //}
                }

                var zipCode = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/ZipCode");
                if (!zipCode.IsNull())
                    _localVars.ZipCode = zipCode.InnerText;
                else
                {
                    if (_localVars.TxnType.ToUpper() != "REFUND")    //D#5517 No ZipCode present for Refund transaction type, so ignore.
                    {
                        response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/ZipCode".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                        return false;
                    }
                }

                var pickAddress = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/PickUpAdd");
                if (!pickAddress.IsNull())
                    _localVars.PickAddress = pickAddress.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/PickUpAdd".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var destinationAddress = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/DropOffAdd");
                if (!destinationAddress.IsNull())
                    _localVars.DestinationAddress = destinationAddress.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/DropOffAdd".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var startLatitude = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/PickUpLat");
                if (!startLatitude.IsNull())
                    _localVars.StartLatitude = startLatitude.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/PickUpLat".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var endLatitude = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/DropOffLat");
                if (!endLatitude.IsNull())
                    _localVars.EndLatitude = endLatitude.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/DropOffLat".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var startLongitude = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/PickUpLng");
                if (!startLongitude.IsNull())
                    _localVars.StartLongitude = startLongitude.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/PickUpLng".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var endLongitude = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/DropOffLng");
                if (!endLongitude.IsNull())
                    _localVars.EndLongitude = endLongitude.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/DropOffLng".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var fare = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Fare");
                if (!fare.IsNull())
                    _localVars.Fare = fare.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Fare".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var taxes = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Taxes");
                if (!taxes.IsNull())
                    _localVars.Taxes = taxes.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Taxes".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var tip = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Tip");
                if (!tip.IsNull())
                    _localVars.Tip = tip.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Tip".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var tolls = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Tolls");
                if (!tolls.IsNull())
                    _localVars.Tolls = tolls.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Tolls".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var surcharge = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Surcharge");
                if (!surcharge.IsNull())
                    _localVars.Surcharge = surcharge.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Surcharge".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var fee = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Fee");
                if (!fee.IsNull())
                    _localVars.Fee = fee.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Fee".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var jobNumber = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/JobNumber");
                if (!jobNumber.IsNull())
                    _localVars.JobNumber = jobNumber.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/JobNumber".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var track1Data = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Track1Data");
                if (!track1Data.IsNull())
                    _localVars.Track1Data = track1Data.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Track1Data".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var flagFall = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/FlagFall");
                if (!flagFall.IsNull())
                    _localVars.FlagFall = flagFall.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/FlagFall".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var extras = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Extras");
                if (!extras.IsNull())
                    _localVars.Extras = extras.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Extras".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var gateFee = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/GateFee");
                if (!gateFee.IsNull())
                    _localVars.GateFee = gateFee.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/GateFee".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var others = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Others");
                if (!others.IsNull())
                    _localVars.Others = others.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/Others".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var currencyCode = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/CurrencyCode");
                if (!currencyCode.IsNull())
                    _localVars.CurrencyCode = currencyCode.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/CurrencyCode".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                var fleetId = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/FleetId");
                if (!fleetId.IsNull())
                    _localVars.FleetId = fleetId.InnerText;
                else
                {
                    response.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/FleetId".MissingElementMessage(), _localVars.DeviceId, _localVars.RequestId);
                    return false;
                }

                //For adding RFUDEV
                var devTypeId = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/RFUDEV");
                if (!devTypeId.IsNull() && !string.IsNullOrWhiteSpace(devTypeId.InnerText))
                    _localVars.RFUDEV = devTypeId.InnerText;
                else
                    _localVars.RFUDEV = PaymentAPIResources.Zero;

                //Changes done for EMV Contact certification of Canada
                var macData = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/CalculatedMACValue");
                if (!macData.IsNull() && !string.IsNullOrWhiteSpace(macData.InnerText))
                    _localVars.MacData = macData.InnerText;

                var checkDigit = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/CheckDigit");
                if (!checkDigit.IsNull() && !string.IsNullOrWhiteSpace(checkDigit.InnerText))
                    _localVars.CheckDigit = checkDigit.InnerText;

                var transactionId = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/TransactionId");
                if (!transactionId.IsNull() && !string.IsNullOrWhiteSpace(transactionId.InnerText))
                    _localVars.TransactionId = transactionId.InnerText;

                var stan = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/StanNo");
                if (!stan.IsNull() && !string.IsNullOrWhiteSpace(stan.InnerText))
                    _localVars.Stan = stan.InnerText;

                var rrn = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/RRN");
                if (!rrn.IsNull() && !string.IsNullOrWhiteSpace(rrn.InnerText))
                    _localVars.RRN = rrn.InnerText;

                var transCount = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/TransCount");
                if (!transCount.IsNull() && !string.IsNullOrWhiteSpace(transCount.InnerText))
                    _localVars.TransCount = transCount.InnerText;

                var deviceType = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/DeviceType");
                if (!deviceType.IsNull() && !string.IsNullOrWhiteSpace(deviceType.InnerText))
                    _localVars.DeviceType = deviceType.InnerText;

                var emvRefundVoid = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/EmvRefundVoid");
                if (!emvRefundVoid.IsNull() && !string.IsNullOrWhiteSpace(emvRefundVoid.InnerText))
                    _localVars.PortalEmvRefundVoidReq = Convert.ToBoolean(emvRefundVoid.InnerText);
                else
                    _localVars.PortalEmvRefundVoidReq = false;

                var voidRequestId = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/VoidRequestId");
                if (!voidRequestId.IsNull() && !string.IsNullOrWhiteSpace(voidRequestId.InnerText))
                    _localVars.VoidRequestId = voidRequestId.InnerText;

                return true;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Purpose             :   To initialize the TransactionRequest.
        /// Function Name       :   InitiateTransaction
        /// Created By          :   Sunil Singh
        /// Created On          :   14-Aug-2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private TransactionRequest InitiateTransaction(CardInfo cardInfo, dynamic firstDataDetails)
        {
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
            {
                _logger.LogInfoMessage(string.Format("PaymentController.InitiateTransaction() ccNo:{0};cardInfo:{1};", _localVars.Ccno, cardInfo.ToString()));
            }

            //TransactionRequest tr = new TransactionRequest
            //{
            //    SerialNumber = _localVars.DeviceId,
            //    RequestedId = _localVars.RequestId,
            //    TransactionType = _localVars.TxnType,
            //    TransType = _localVars.TransType,
            //    PaymentType = _localVars.PayType,
            //    CardType = _localVars.CardType,
            //    IndustryType = _localVars.IndustryType,
            //    TransactionAmount = _localVars.TxnAmount,
            //    CardNumber = _localVars.Ccno,
            //    ExpiryDate = _localVars.ExpiryDate,
            //    CardCvv = _localVars.CardCvv,
            //    DriverNumber = _localVars.DriverNo,
            //    VehicleNumber = _localVars.VehicleNumber,
            //    RapidConnectAuthId = _localVars.RapidConnectAuthId,
            //    TransNote = _localVars.TransNote,
            //    CreatedBy = _localVars.CreatedBy,
            //    StreetAddress = _localVars.StreetAddress,
            //    ZipCode = _localVars.ZipCode,
            //    PickAddress = _localVars.PickAddress,
            //    DestinationAddress = _localVars.DestinationAddress,
            //    StartLatitude = _localVars.StartLatitude,
            //    EndLatitude = _localVars.EndLatitude,
            //    StartLongitude = _localVars.StartLongitude,
            //    EndLongitude = _localVars.EndLongitude,
            //    Fare = _localVars.Fare,
            //    Taxes = _localVars.Taxes,
            //    Tip = _localVars.Tip,
            //    Tolls = _localVars.Tolls,
            //    Surcharge = _localVars.Surcharge,
            //    Fee = _localVars.Fee,
            //    JobNumber = _localVars.JobNumber,
            //    Track1Data = _localVars.Track1Data,
            //    Track2Data = _localVars.Track2Data,
            //    Track3Data = _localVars.Track3Data,
            //    KeyId = _localVars.KeyId,
            //    FlagFall = _localVars.FlagFall,
            //    Extras = _localVars.Extras,
            //    GateFee = _localVars.GateFee,
            //    Others = _localVars.Others,
            //    EntryMode = _localVars.EntryMode,
            //    CurrencyCode = _localVars.CurrencyCode,
            //    FleetId = _localVars.FleetId,
            //    EmvData = _localVars.EmvData,
            //    CardSeq = _localVars.CSequence,
            //    DebitPin = _localVars.DebitPin,
            //    KeySerialNumber = _localVars.KCN,
            //    FirstFourCard = cardInfo.First4,
            //    LastFourcard = cardInfo.Last4,
            //    LocalDateTime = DateTime.Now.ToString("yyyyMMddHHmmss"),
            //    TrnmsnDateTime = DateTime.UtcNow.ToString("yyyyMMddHHmmss"),
            //    FdMerchantId = firstDataDetails.FDMerchantID,
            //    MtdMerchantId = firstDataDetails.fk_MerchantID,
            //    TerminalId = firstDataDetails.RCTerminalId,
            //    TppId = firstDataDetails.ProjectId,
            //    GroupId = firstDataDetails.GroupId,
            //    TokenType = firstDataDetails.TokenType,
            //    MCCode = firstDataDetails.MCCode,
            //    EncryptionKey = firstDataDetails.EncryptionKey,
            //    Did = firstDataDetails.DID,
            //    App = firstDataDetails.App,
            //    ServiceId = firstDataDetails.serviceId,
            //    ServiceUrl = string.Empty,
            //    RFUDEV = _localVars.RFUDEV,
            //    ServiceCode = _localVars.ServiceCode,
            //    MacData = _localVars.MacData,
            //    CheckDigit = _localVars.CheckDigit,
            //    AccountType = _localVars.AccountType,
            //    PortalEmvRefundVoidReq = _localVars.PortalEmvRefundVoidReq
            //};

            //return tr;

            //zzz Chetu 357
            TransactionRequest tr = new TransactionRequest
            {
                SerialNumber = _localVars.DeviceId,
                RequestedId = _localVars.RequestId,
                TransactionType = _localVars.TxnType,
                TransType = _localVars.TransType,
                PaymentType = _localVars.PayType,
                CardType = _localVars.CardType,
                IndustryType = _localVars.IndustryType,
                TransactionAmount = _localVars.TxnAmount,
                CardNumber = _localVars.Ccno,
                ExpiryDate = _localVars.ExpiryDate,
                CardCvv = _localVars.CardCvv,
                DriverNumber = _localVars.DriverNo,
                VehicleNumber = _localVars.VehicleNumber,
                RapidConnectAuthId = _localVars.RapidConnectAuthId,
                TransNote = _localVars.TransNote,
                CreatedBy = _localVars.CreatedBy,
                StreetAddress = _localVars.StreetAddress,
                ZipCode = _localVars.ZipCode,
                PickAddress = _localVars.PickAddress,
                DestinationAddress = _localVars.DestinationAddress,
                StartLatitude = _localVars.StartLatitude,
                EndLatitude = _localVars.EndLatitude,
                StartLongitude = _localVars.StartLongitude,
                EndLongitude = _localVars.EndLongitude,
                Fare = _localVars.Fare,
                Taxes = _localVars.Taxes,
                Tip = _localVars.Tip,
                Tolls = _localVars.Tolls,
                Surcharge = _localVars.Surcharge,
                Fee = _localVars.Fee,
                JobNumber = _localVars.JobNumber,
                Track1Data = _localVars.Track1Data,
                Track2Data = _localVars.Track2Data,
                Track3Data = _localVars.Track3Data,
                KeyId = _localVars.KeyId,
                FlagFall = _localVars.FlagFall,
                Extras = _localVars.Extras,
                GateFee = _localVars.GateFee,
                Others = _localVars.Others,
                EntryMode = _localVars.EntryMode,
                CurrencyCode = _localVars.CurrencyCode,
                FleetId = _localVars.FleetId,
                EmvData = _localVars.EmvData,
                CardSeq = _localVars.CSequence,
                DebitPin = _localVars.DebitPin,
                KeySerialNumber = _localVars.KCN,
                FirstFourCard = cardInfo.First4,
                LastFourcard = cardInfo.Last4,
                LocalDateTime = DateTime.Now.ToString("yyyyMMddHHmmss"),
                TrnmsnDateTime = DateTime.UtcNow.ToString("yyyyMMddHHmmss"),
                FdMerchantId = firstDataDetails.FDMerchantID,
                MtdMerchantId = firstDataDetails.fk_MerchantID,
                TerminalId = firstDataDetails.RCTerminalId,
                TppId = firstDataDetails.ProjectId,
                GroupId = firstDataDetails.GroupId,
                TokenType = firstDataDetails.TokenType,
                MCCode = firstDataDetails.MCCode,
                EncryptionKey = firstDataDetails.EncryptionKey,
                Did = firstDataDetails.DID,
                App = firstDataDetails.App,
                ServiceId = firstDataDetails.serviceId,
                ServiceUrl = string.Empty,
                RFUDEV = _localVars.RFUDEV,
                ServiceCode = _localVars.ServiceCode,
                MacData = _localVars.MacData,
                CheckDigit = _localVars.CheckDigit,
                AccountType = _localVars.AccountType,
                PortalEmvRefundVoidReq = _localVars.PortalEmvRefundVoidReq,
                TerminalLaneNo = _localVars.TerminalLaneNo,
                DeviceType = _localVars.DeviceType,
                VoidRequestId = _localVars.VoidRequestId
            };

            return tr;
        }

        /// <summary>
        /// Validating payment request.
        /// Purpose             :   Validate reqest
        /// Function Name       :   ValidateRequest
        /// Created By          :   Sunil Singh
        /// Created On          :   08/19/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :    "MM/DD/YYYY" 
        /// <param name="response"></param>
        /// <param name="mHeader"></param>
        /// <param name="firstDataDetails"></param>
        /// <param name="isFailed"></param>
        /// <returns>bool</returns>
        //private bool ValidateRequest(PaymentResponse response, LoginHeader mHeader, out dynamic firstDataDetails)
        private bool ValidateRequest(PaymentResponse response, out dynamic firstDataDetails)
        {
            try
            {
                bool isPass = true;
                bool missingFleetID = false;

                if (_localVars.Currency != PaymentAPIResources.Cur_USA && _localVars.Currency != PaymentAPIResources.Cur_Cn)
                {
                    isPass = false;
                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.SupportedCur, _localVars.DeviceId, _localVars.RequestId);
                }

                if (string.IsNullOrEmpty(_localVars.DeviceId))
                {
                    isPass = false;
                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.DeviceIdRequired, _localVars.DeviceId, _localVars.RequestId);
                }

                bool isToken = string.IsNullOrEmpty(_localVars.Token);

                // Check if we have a FleetId specified, before converting to an Int.
                int testFleetID = -1;
                if (Int32.TryParse(_localVars.FleetId, out testFleetID))
                {
                    if (isToken)
                        firstDataDetails = _commonService.GetFirstDataDetails(_localVars.DeviceId, _localVars.DriverNo, (string)null, Convert.ToInt32(_localVars.FleetId), _localVars.RequestType);
                    else
                        firstDataDetails = _commonService.GetFirstDataDetails(_localVars.DeviceId, _localVars.DriverNo, _localVars.Token, Convert.ToInt32(_localVars.FleetId), _localVars.RequestType);
                }
                else
                {
                    firstDataDetails = null;

                    isPass = false;
                    missingFleetID = true;
                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.FleetIdRequired, _localVars.DeviceId, _localVars.RequestId);

                    _logger.LogWarningMessage(string.Format("PaymentController.ValidateRequest() FleetID is null. DeviceId:{0};DriverId:{1};RequestId:{2};", _localVars.DeviceId, _localVars.DriverNo, _localVars.RequestType));
                }

                if (!_localVars.IndustryType.Equals(PaymentIndustryType.Ecommerce.ToString()))
                {
                    if (isToken || firstDataDetails == null)
                    {
                        isPass = false;
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.UnauthorizedAccess, _localVars.DeviceId, _localVars.RequestId);
                    }
                }

                if (firstDataDetails == null)
                {
                    isPass = false;

                    if (!missingFleetID)    // If missing FleetID, we have already set the response header.
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.UnauthorizedAccess, _localVars.DeviceId, _localVars.RequestId);
                }

                return isPass;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(string.Format("PaymentController.ValidateRequest() Exception:{0};", ex.ToString()), ex);

                throw;
            }
        }

        /// <summary>
        /// Calculating Tech Fee
        /// </summary>
        /// <param name="_transactionService"></param>
        /// <param name="fleetId"></param>
        /// <param name="fareTipSurcharge"></param>
        /// <param name="fleetFee"></param>
        /// <param name="techFee"></param>
        public static void GetFleetTechFee(ITransactionService _transactionService, ICommonService commonService, string transactionType, 
            int fleetId, ILogger _logger, decimal fareTipSurcharge, out decimal fleetFee, out decimal techFee, out decimal txnFee)
        {
            try
            {
                var surchFee = commonService.SurchargeFees(fleetId);

                fleetFee = 0;
                techFee = 0;
                txnFee = 0;

                if (!transactionType.Equals(PaymentTransactionType.Void) && !transactionType.Equals(PaymentTransactionType.Refund))
                {
                    if (surchFee.PayType == (int)PayType.PayNetwork)
                    {
                        // Applying FleetFee if PayNetwork Selected
                        decimal percentageFleet = 0, bothFleet = 0;
                        decimal maxCapFleet = Convert.ToDecimal(surchFee.FleetFeeMaxCap);
                        FeeType fFeeType = (FeeType)Convert.ToInt32(surchFee.FleetFeeType);

                        switch (fFeeType)
                        {
                            case FeeType.Fixed:
                                fleetFee = Convert.ToDecimal(surchFee.FleetFeeFixed);
                                break;

                            case FeeType.Percentage:
                                percentageFleet = fareTipSurcharge * Convert.ToDecimal(surchFee.FleetFeePer) / 100;
                                fleetFee = (percentageFleet < maxCapFleet) ? percentageFleet : maxCapFleet;
                                break;

                            case FeeType.Both:
                                percentageFleet = fareTipSurcharge * Convert.ToDecimal(surchFee.FleetFeePer) / 100;
                                bothFleet = percentageFleet + Convert.ToDecimal(surchFee.FleetFeeFixed);
                                fleetFee = (bothFleet < maxCapFleet) ? bothFleet : maxCapFleet;
                                break;
                        }
                    }
                    // Applying transaction fee when PayDriver or PayOwner selected
                    //D#6076 Correctly calculate transaction (booking) fee, as Chetu cannot tell the difference between transacion and technology!!!
                    else if (surchFee.PayType == (int)PayType.PayDriver || surchFee.PayType == (int)PayType.PayOwner)
                    {
                        decimal maxCapTran = Convert.ToDecimal(surchFee.BookingFeeMaxCap);
                        decimal percentageTran = 0, bothT = 0;
                        FeeType txnFeeType = (FeeType)Convert.ToInt32(surchFee.BookingFeeType);

                        switch (txnFeeType)
                        {
                            case FeeType.Fixed:
                                txnFee = Convert.ToDecimal(surchFee.BookingFeeFixed);
                                break;

                            case FeeType.Percentage:
                                percentageTran = fareTipSurcharge * Convert.ToDecimal(surchFee.BookingFeePer) / 100;
                                txnFee = (percentageTran < maxCapTran) ? percentageTran : maxCapTran;
                                break;

                            case FeeType.Both:
                                percentageTran = fareTipSurcharge * Convert.ToDecimal(surchFee.BookingFeePer) / 100;
                                bothT = percentageTran + Convert.ToDecimal(surchFee.BookingFeeFixed);
                                txnFee = (bothT < maxCapTran) ? bothT : maxCapTran;
                                break;
                        }
                    }
                }

                // Applying TechFee in all cases
                decimal maxCap = Convert.ToDecimal(surchFee.TechFeeMaxCap);
                decimal percentage = 0, both = 0;
                FeeType tFeeType = (FeeType)Convert.ToInt32(surchFee.TechFeeType);
                switch (tFeeType)
                {
                    case FeeType.Fixed:
                        techFee = Convert.ToDecimal(surchFee.TechFeeFixed);
                        break;

                    case FeeType.Percentage:
                        percentage = fareTipSurcharge * Convert.ToDecimal(surchFee.TechFeePer) / 100;
                        techFee = (percentage < maxCap) ? percentage : maxCap;
                        break;

                    case FeeType.Both:
                        percentage = fareTipSurcharge * Convert.ToDecimal(surchFee.TechFeePer) / 100;
                        both = percentage + Convert.ToDecimal(surchFee.TechFeeFixed);
                        techFee = (both < maxCap) ? both : maxCap;
                        break;
                }

                // Ensure all value amounts are rounded to 2 decimal places.
                fleetFee = Math.Round(fleetFee, 2, MidpointRounding.AwayFromZero);
                techFee = Math.Round(techFee, 2, MidpointRounding.AwayFromZero);
                txnFee = Math.Round(txnFee, 2, MidpointRounding.AwayFromZero);
            }
            catch
            {
                throw;
            }
        }

        //zzz Chetu 357
        ///// <summary>
        ///// Calculating Tech Fee
        ///// </summary>
        ///// <param name="_transactionService"></param>
        ///// <param name="fleetId"></param>
        ///// <param name="fareTipSurcharge"></param>
        ///// <param name="fleetFee"></param>
        ///// <param name="techFee"></param>
        //public static void GetFleetTechFeeMTI(ITransactionService _transactionService, ICommonService commonService, string transactionType,
        //    int fleetId, ILogger _logger, decimal fareTipSurcharge, out decimal fleetFee, out decimal techFee, out decimal txnFee)
        //{
        //    try
        //    {
        //        var surchFee = commonService.SurchargeFees(fleetId);
        //
        //        fleetFee = 0;
        //        techFee = 0;
        //        txnFee = 0;
        //
        //        if (!transactionType.Equals(PaymentTransactionType.Void) && !transactionType.Equals(PaymentTransactionType.Refund))
        //        {
        //            if (surchFee.PayType == (int)PayType.PayNetwork)
        //            {
        //                // Applying FleetFee if PayNetwork Selected
        //                decimal percentageFleet = 0, bothFleet = 0;
        //                decimal maxCapFleet = Convert.ToDecimal(surchFee.FleetFeeMaxCap);
        //                FeeType fFeeType = (FeeType)Convert.ToInt32(surchFee.FleetFeeType);
        //
        //                switch (fFeeType)
        //                {
        //                    case FeeType.Fixed:
        //                        fleetFee = Convert.ToDecimal(surchFee.FleetFeeFixed);
        //                        break;
        //
        //                    case FeeType.Percentage:
        //                        percentageFleet = fareTipSurcharge * Convert.ToDecimal(surchFee.FleetFeePer) / 100;
        //                        fleetFee = (percentageFleet < maxCapFleet) ? percentageFleet : maxCapFleet;
        //                        break;
        //
        //                    case FeeType.Both:
        //                        percentageFleet = fareTipSurcharge * Convert.ToDecimal(surchFee.FleetFeePer) / 100;
        //                        bothFleet = percentageFleet + Convert.ToDecimal(surchFee.FleetFeeFixed);
        //                        fleetFee = (bothFleet < maxCapFleet) ? bothFleet : maxCapFleet;
        //                        break;
        //                }
        //            }
        //            // Applying transaction fee when PayDriver or PayOwner selected
        //            //D#6076 Correctly calculate transaction (booking) fee, as Chetu cannot tell the difference between transacion and technology!!!
        //            else if (surchFee.PayType == (int)PayType.PayDriver || surchFee.PayType == (int)PayType.PayOwner)
        //            {
        //                decimal maxCapTran = Convert.ToDecimal(surchFee.BookingFeeMaxCap);
        //                decimal percentageTran = 0, bothT = 0;
        //                FeeType txnFeeType = (FeeType)Convert.ToInt32(surchFee.BookingFeeType);
        //
        //                switch (txnFeeType)
        //                {
        //                    case FeeType.Fixed:
        //                        txnFee = Convert.ToDecimal(surchFee.BookingFeeFixed);
        //                        break;
        //
        //                    case FeeType.Percentage:
        //                        percentageTran = fareTipSurcharge * Convert.ToDecimal(surchFee.BookingFeePer) / 100;
        //                        txnFee = (percentageTran < maxCapTran) ? percentageTran : maxCapTran;
        //                        break;
        //
        //                    case FeeType.Both:
        //                        percentageTran = fareTipSurcharge * Convert.ToDecimal(surchFee.BookingFeePer) / 100;
        //                        bothT = percentageTran + Convert.ToDecimal(surchFee.BookingFeeFixed);
        //                        txnFee = (bothT < maxCapTran) ? bothT : maxCapTran;
        //                        break;
        //                }
        //            }
        //        }
        //
        //        // Applying TechFee in all cases
        //        decimal maxCap = Convert.ToDecimal(surchFee.TechFeeMaxCap);
        //        decimal percentage = 0, both = 0;
        //        FeeType tFeeType = (FeeType)Convert.ToInt32(surchFee.TechFeeType);
        //        switch (tFeeType)
        //        {
        //            case FeeType.Fixed:
        //                techFee = Convert.ToDecimal(surchFee.TechFeeFixed);
        //                break;
        //
        //            case FeeType.Percentage:
        //                percentage = fareTipSurcharge * Convert.ToDecimal(surchFee.TechFeePer) / 100;
        //                techFee = (percentage < maxCap) ? percentage : maxCap;
        //                break;
        //
        //            case FeeType.Both:
        //                percentage = fareTipSurcharge * Convert.ToDecimal(surchFee.TechFeePer) / 100;
        //                both = percentage + Convert.ToDecimal(surchFee.TechFeeFixed);
        //                techFee = (both < maxCap) ? both : maxCap;
        //                break;
        //        }
        //
        //        // Ensure all value amounts are rounded to 2 decimal places.
        //        fleetFee = Math.Round(fleetFee, 2, MidpointRounding.AwayFromZero);
        //        techFee = Math.Round(techFee, 2, MidpointRounding.AwayFromZero);
        //        txnFee = Math.Round(txnFee, 2, MidpointRounding.AwayFromZero);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        #endregion

        /// <summary>
        /// Purpose             :   Datwire registration method for terminal registration from web portal
        /// Function Name       :   DatawireRegistration
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   04/14/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult DatawireRegistration(HttpRequestMessage request)
        {
            _logMessage.AppendLine("Datawire registration process start.");

            DatawireResponse res = new DatawireResponse();
            try
            {
                _logMessage.AppendLine("Calling datawire registration class.");

                DatawireRegistration dwire = new DatawireRegistration(_terminalService, _merchantService, _logMessage);

                _logMessage.AppendLine("Datawire registration process complete.");
                _logger.LogInfoMessage(_logMessage.ToString());

                res = (DatawireResponse)dwire.Register(request, res, _fdMerchantobj, _logger);

                return Ok(res);
            }
            catch (Exception ex)
            {
                res.Status = PaymentAPIResources.Failed;
                res.Value = ex.Message;
                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                return Ok(res);
            }
        }

        /// <summary>
        /// Purpose             :   Datwire registration method for terminal registration from web portal
        /// Function Name       :   DatawireRegistrationCN
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   03/28/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult DatawireRegistrationCN(HttpRequestMessage request)
        {
            string resgist = string.Empty;
            _logMessage.AppendLine("Datawire registration process start.");

            DatawireResponse res = new DatawireResponse();
            try
            {
                _logMessage.AppendLine("Calling datawire registration class.");
                DatawireRegistration dwire = new DatawireRegistration(_terminalService, _merchantService, _logMessage);

                resgist = dwire.RegisterCN(request, res, _fdMerchantobj, _logger);

                _logMessage.AppendLine(string.Format("Datawire registration result:{0};", resgist));
                _logger.LogInfoMessage(_logMessage.ToString());

                return Ok(resgist);
            }
            catch (Exception ex)
            {
                res.Status = PaymentAPIResources.Failed;
                res.Value = ex.Message;

                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Ok(resgist);
            }
        }

        /// <summary>
        /// Purpose             :   Datwire registration method for terminal registration from web portal
        /// Function Name       :   DatawireService
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   03/29/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult DatawireService(HttpRequestMessage request)
        {
            string service = string.Empty;
            _logMessage.AppendLine("Datawire registration process start.");

            DatawireResponse res = new DatawireResponse();
            try
            {
                _logMessage.AppendLine("Calling datawire registration class.");
                DatawireRegistration dwire = new DatawireRegistration(_terminalService, _merchantService, _logMessage);

                _logMessage.AppendLine("Datawire registration process complete.");
                _logger.LogInfoMessage(_logMessage.ToString());

                //zzz Chetu 357 : service = dwire.ServiceRegister(request, res, _logger);
                service = dwire.ServiceRegister(request, res, _fdMerchantobj, _logger);

                return Ok(service);
            }
            catch (Exception ex)
            {
                res.Status = PaymentAPIResources.Failed;
                res.Value = ex.Message;
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Ok(service);
            }
        }

        /// <summary>
        /// Purpose             :   Ping request
        /// Function Name       :   DatawirePing
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   03/30/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult DatawirePing(HttpRequestMessage request)
        {
            string ping = string.Empty;
            _logMessage.AppendLine("Datawire registration process start.");
            DatawireResponse res = new DatawireResponse();
            try
            {
                _logMessage.AppendLine("Calling datawire registration class.");
                DatawireRegistration dwire = new DatawireRegistration(_terminalService, _merchantService, _logMessage);

                _logMessage.AppendLine("Datawire registration process complete.");
                _logger.LogInfoMessage(_logMessage.ToString());

                ping = dwire.Pingregister(request, res, _fdMerchantobj, _logger);

                return Ok(ping);
            }
            catch (Exception ex)
            {
                res.Status = PaymentAPIResources.Failed;
                res.Value = ex.Message;

                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                return Ok(ping);
            }
        }

        /// <summary>
        /// Create the request message format for merchant provisioning
        /// </summary>
        /// <returns>Return the merchant provision request xml need to send on datawire</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string SendMessageToDatawire(string url, string xml, int timeOut = 100000, string requestType = "POST")
        {
            ILogger logger = new Logger();
            StringBuilder logMessage = new StringBuilder();

            logMessage.AppendLine("Controller Name:-WebAPI.Controllers.PaymentController; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                string xmlResponse = string.Empty;
                HttpWebRequest objWebrequest = (HttpWebRequest)WebRequest.Create(url);
                byte[] buf = Encoding.UTF8.GetBytes(xml);

                if (objWebrequest != null)
                {
                    // Header setting

                    //Header setting for the request start 
                    objWebrequest.UserAgent = Global.UserAgent;
                    objWebrequest.Method = requestType;
                    objWebrequest.ContentLength = buf.Length;
                    objWebrequest.KeepAlive = true;
                    objWebrequest.Headers.Add(HttpRequestHeader.CacheControl, "no-cache");
                    objWebrequest.ContentType = "text/xml";
                    objWebrequest.Timeout = timeOut;

                    //Header setting for the request ends 
                    //Allowing the service to authenticate on any of following SSL certificate 
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                         | SecurityProtocolType.Tls11
                                                         | SecurityProtocolType.Tls12
                                                         | SecurityProtocolType.Ssl3;

                    // allows for validation of SSL conversations
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    // Request stream

                    //only when Request Type is post
                    if (requestType.Equals("POST"))
                    {
                        using (var stream = objWebrequest.GetRequestStream()) //get the request stream from request object created 
                        {
                            stream.Write(buf, 0, buf.Length);// write the content in the request stream                        
                        }
                    }

                    // Response stream

                    // get the response for the request                    
                    var response = (HttpWebResponse)objWebrequest.GetResponse();

                    // Get the response in a stream.
                    var receiveStream = response.GetResponseStream();

                    // Pipes the stream to a higher level stream reader with the required encoding format. 
                    if (receiveStream != null)
                    {
                        var readStream = new StreamReader(receiveStream, Encoding.UTF8);
                        //get the registration response
                        xmlResponse = readStream.ReadToEnd();
                        response.Close();
                        readStream.Close();
                    }
                }

                return xmlResponse;
            }
            catch (WebException ex)
            {
                logger.LogInfoFatal(logMessage.ToString(), ex);
                return string.Empty;
            }
            catch (Exception ex)
            {
                logger.LogInfoFatal(logMessage.ToString(), ex);
                return string.Empty;
            }
        }

        /// <summary>
        /// Process the Service discovery with the xml supplied as the parameter 
        /// </summary>
        /// <param name="xml">Source URL to for discovery </param>
        /// <returns>Return the list of url</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        private static string ProcessServiceDiscoveryToDatawire(string url)
        {
            try
            {
                string response = CnSendTransaction.SendMessageToDatawire(url, "", 20000, "GET"); //No xml required for service discovery
                return response;
            }
            catch
            {
                throw;
            }
        }

        //zzz Delete once system running.
        ///// <summary>
        ///// Process the ping request on list of service providers which pass as parameter 
        ///// if ping request fail due to AuthenticateError, Timeout, UnknownServiceID
        ///// it will retry on same url two time if response yet contain the above status 
        ///// then it will delete the url from the list
        ///// </summary>
        ///// <param name="objProviders">Source list of service providers</param>
        //private void ProcessPingToDatawire(List<ServiceProvider> objProviders, string datawireId)
        //{
        //    try
        //    {
        //        string xmlResponse = string.Empty;
        //        int countRetry = 0;
        //        for (var index = 0; index < objProviders.Count; index++)
        //        {
        //            var objService = objProviders[index];
        //            objStopWatch.Start();
        //            xmlResponse = this.SendPingRequest(objService.URL, datawireId);
        //            if (string.IsNullOrEmpty(xmlResponse)) { continue; }
        //            if (countRetry >= 2)
        //            {
        //                //Remove current url from list
        //                var objItem = objProviders.Where(x => x.URL.Equals(objService.URL)).FirstOrDefault();
        //                objProviders.Remove(objItem);
        //            }

        //            objStopWatch.Stop();
        //            long timeTaken = objStopWatch.ElapsedMilliseconds;

        //            //Parse the response and update the list provider with TransactionTime
        //            this.GetServiceUrlData(xmlResponse, objService.URL, Convert.ToInt64(objService.MaxTransactionsInPackage), timeTaken);
        //            countRetry = 0;

        //            objStopWatch.Stop();
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        private string SendPingRequest(string pingUrl, string datawireId)
        {
            try
            {
                string requestXml = this.CreatePingRequest(datawireId, Global.ServiceID);
                string responseXml = CnSendTransaction.SendMessageToDatawire(pingUrl, requestXml, 20000);
                return responseXml;
            }
            catch
            {
                throw;
            }
        }

        private string CreatePingRequest(string datawireID, string serviceID)
        {
            try
            {
                ReqClientIDType objRequestClientID = new ReqClientIDType();
                objRequestClientID.DID = datawireID;
                objRequestClientID.App = Global.AppNameNVersion;
                objRequestClientID.Auth = "000082004330015" + "|" + "01351646";
                objRequestClientID.ClientRef = "0059858V003000";

                PingRequest objPing = new PingRequest();
                objPing.ServiceID = serviceID;

                Request objRequest = new Request();
                objRequest.ReqClientID = objRequestClientID;
                objRequest.Ping = objPing;
                string requestXml = Serialization.Serialize<Request>(objRequest);
                return requestXml;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Set the service discovery response to the custom type List of ServiceProvider
        /// based on the parameter supplied as xmlResponse requestXml ,maxTran
        /// and merged all data into one and set it to class level variable 
        /// </summary>
        /// <param name="xmlResponse">the xml response which return from service discovery response</param>
        /// <param name="requestUrl">the url on which you have request for service discovery</param>
        /// <param name="maxTran">Its a number of transaction allowed on requested url</param>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        private void GetServiceUrlData(string xmlResponse, string requestUrl, long maxTran, long timeTakenInResponse)
        {
            try
            {
                Response response = Serialization.Deserialize<Response>(xmlResponse);
                PingResponse pingResponse = response.PingResponse;
                if (response.StatusTypeField.StatusCode == "OK")
                {
                    foreach (var serviceCost in pingResponse.ServiceCost)
                    {
                        ServiceProvider newSp = new ServiceProvider();
                        newSp.MaxTransactionsInPackage = maxTran.ToString();
                        newSp.URL = requestUrl;
                        string time = pingResponse.ServiceCost.Where(x => x.ServiceID == serviceCost.ServiceID).First().TransactionTimeMs;
                        newSp.TransactionTime = Convert.ToInt64(time) + timeTakenInResponse;
                        _serviceProvider.Add(newSp);
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Handle iso8583 response codes
        /// </summary>
        /// <returns>Iso8583Response object</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        private static Iso8583Response HandleResponseCode(Iso8583Response iso8583Response)
        {
            if (iso8583Response.StatusCode == PaymentAPIResources.AuthenticationError)
            {
                iso8583Response.Message = PaymentAPIResources.AuthenticationMessage;
                iso8583Response.AdditionalResponseData = PaymentAPIResources.AuthenticationError;
            }
            else if (iso8583Response.ResponseCode == PaymentAPIResources.ResponseCode00)
                iso8583Response.Message = PaymentAPIResources.Approval;
            else if (iso8583Response.ResponseCode == PaymentAPIResources.ResponseCode91)    // || iso8583Response.ResponseCode == PaymentAPIResources.ResponseCode14)
                iso8583Response.Message = PaymentAPIResources.FormatError;
            else if (iso8583Response.ResponseCode == PaymentAPIResources.ResponseCode14)
                iso8583Response.Message = PaymentAPIResources.InvalidAccount;               // PAY-13 Added Invalid Account response message.
            else if (iso8583Response.ResponseCode == PaymentAPIResources.ResponseCode51)    // Declined Insufficient funds
                iso8583Response.Message = PaymentAPIResources.ResponseCode51Message;
            else if (iso8583Response.ResponseCode == PaymentAPIResources.ResponseCode63)    // Service not allowed
                iso8583Response.Message = PaymentAPIResources.ResponseCode63Message;
            else if (iso8583Response.ResponseCode == PaymentAPIResources.ResponseCode05)    // Declined Do not honour   //PAY-13
                iso8583Response.Message = PaymentAPIResources.ResponseCode05Message;
            else if (iso8583Response.ResponseCode == PaymentAPIResources.ResponseCode54)    // Expired Card
                iso8583Response.Message = PaymentAPIResources.ResponseCode54Message;

            return iso8583Response;
        }

        public static Iso8583Response HandleResponse(CnTransactionEntities cnTrans, int transId)
        {
            Iso8583Response isoResponse = ResponseHandler.ParseAndSaveResponse(cnTrans, transId);
            isoResponse = HandleResponseCode(isoResponse);
            return isoResponse;
        }

        ///// <summary>
        ///// Purpose             :   Get the transaction details from Android for pulled completion
        ///// Function Name       :   GetTransactionDetails
        ///// Created By          :   Madhuri Tanwar
        ///// Created On          :   09/22/2015
        ///// Modificatios Made   :   ****************************
        ///// Modidified On       :   **** "MM/DD/YYYY"
        ///// </summary>
        ///// <param name="request"></param>
        ///// <returns></returns>
        [HttpPost]
        public IHttpActionResult GetTransactionDetails(HttpRequestMessage request)
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            _logMessage.AppendLine("Initializing Auth famility object");

            AuthRequest authRequest = new AuthRequest();
            AuthResponse authresponse = new AuthResponse();
            AuthHeader authHeader = new AuthHeader();
            AuthTransactionDetails transDetails = new AuthTransactionDetails();

            _logMessage.AppendLine("Initialization done Auth famility object");

            try
            {
                XmlDocument xmlDoc = new XmlDocument();

                _logMessage.AppendLine("Loading xml request.");

                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                _logMessage.AppendLine("Initialization variables from xml request.");

                authRequest.DriverNumber = xmlDoc.SelectSingleNode("AuthorizationDetails/DriverNumber").InnerText;
                authRequest.DeviceId = xmlDoc.SelectSingleNode("AuthorizationDetails/DeviceId").InnerText;
                authRequest.Token = xmlDoc.SelectSingleNode("AuthorizationDetails/Token").InnerText;
                authRequest.FleetId = xmlDoc.SelectSingleNode("AuthorizationDetails/FleetId").InnerText;
                authRequest.TransId = Convert.ToInt32(xmlDoc.SelectSingleNode("AuthorizationDetails/TransID").InnerText);

                _logTracking.AppendLine("GetTransactionDetails start. DriverNo: " + authRequest.DriverNumber + " and  DeviceId " + authRequest.DeviceId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());

                _logMessage.AppendLine("Authenticating Token");
                if (!string.IsNullOrEmpty(authRequest.Token))
                {
                    bool isTokenValid = _commonService.IsTokenValid(authRequest.DriverNumber, authRequest.Token);
                    if (!isTokenValid)
                    {
                        _logMessage.AppendLine("Authentication failed.");
                        _logger.LogInfoMessage(_logMessage.ToString());

                        authresponse = CommonFunctions.FailedResponse(authRequest, authresponse, authHeader, PaymentAPIResources.UnauthorizedAccess);
                        return Ok(authresponse);
                    }
                }
                else
                {
                    _logMessage.AppendLine("Token was null/empty.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    authresponse = CommonFunctions.FailedResponse(authRequest, authresponse, authHeader, PaymentAPIResources.UnauthorizedAccess);
                    return Ok(authresponse);
                }

                _logMessage.AppendLine("Decrypting DriverNo and DriverPin.");

                char[] deliem = { '^' };
                char[] deliemDriver = { ':' };
                string[] encArray = authRequest.Token.Split(deliem);
                string encDriverAndPin = encArray[1];
                string[] encArrayDriver = encDriverAndPin.Split(deliemDriver);
                string encDriverNo = encArrayDriver[0];
                string encDriverPin = encArrayDriver[1];
                string driverNo = StringExtension.DecryptDriver(encDriverNo);
                string driverPin = StringExtension.DecryptDriver(encDriverPin);

                _logMessage.AppendLine("Getting DriverDto.");
                var driverDto = _driverService.GetDriverDto(authRequest.DriverNumber, Convert.ToInt32(authRequest.FleetId));
                if (!driverDto.IsNull())
                {
                    bool isValid = StringExtension.ValidatePassCode(driverPin, driverDto.PIN);
                    if (isValid)
                    {
                        _logMessage.AppendLine("Fetching transaction detail by TransId.");
                        MTDTransactionDto trxresponse = _transactionService.GetTransaction(authRequest.TransId);
                        if (trxresponse.IsNull() || (trxresponse.fk_FleetId != Convert.ToInt32(authRequest.FleetId) && trxresponse.DriverNo != authRequest.DriverNumber))
                        {
                            _logMessage.AppendLine("MTDTransactionDto trxresponse is null.");
                            _logger.LogInfoMessage(_logMessage.ToString());

                            authresponse = CommonFunctions.FailedResponse(authRequest, authresponse, authHeader, PaymentAPIResources.Trx_details + " " + authRequest.TransId);
                            return Ok(authresponse);
                        }
                        if ((bool)trxresponse.IsCompleted || (bool)trxresponse.IsVoided)
                        {
                            _logMessage.AppendLine("MTDTransactionDto trxresponse type is invalid.");
                            _logger.LogInfoMessage(_logMessage.ToString());

                            authresponse = CommonFunctions.FailedResponse(authRequest, authresponse, authHeader, PaymentAPIResources.Transaction_already_completed + " " + authRequest.TransId);
                            return Ok(authresponse);
                        }
                        else
                        {
                            _logMessage.AppendLine("initializing transaction variables.");

                            decimal amount = Convert.ToDecimal(trxresponse.Amount);
                            transDetails.TransactionID = Convert.ToString(trxresponse.Id);
                            transDetails.DriverNumber = trxresponse.DriverNo;
                            transDetails.VehicleNo = trxresponse.VehicleNo;
                            transDetails.Amount = Convert.ToString(decimal.Round(amount, 2, MidpointRounding.AwayFromZero));
                            transDetails.CardFirstFourDigits = trxresponse.FirstFourDigits;
                            transDetails.CardLastFourDigits = trxresponse.LastFourDigits;
                            transDetails.TransactionResponse = trxresponse.AddRespData;
                            transDetails.TransactionDate = Convert.ToString(trxresponse.TxnDate);
                            transDetails.EntryMode = trxresponse.EntryMode;
                            transDetails.PaymentType = trxresponse.PaymentType;
                            transDetails.AuthID = trxresponse.AuthId;
                            transDetails.CardType = trxresponse.CardType;
                            transDetails.TransactionType = trxresponse.TxnType;

                            authHeader.DeviceId = authRequest.DeviceId;
                            authHeader.TransID = authRequest.TransId;
                            authHeader.Message = PaymentAPIResources.Authorization_Transaction_Details + " " + authRequest.TransId;
                            authHeader.Status = PaymentAPIResources.Success;

                            authresponse.Header = authHeader;
                            authresponse.TransactionDetails = transDetails;

                            _logTracking.AppendLine("GetTransactionDetails complete. DriverNo: " + authRequest.DriverNumber + " and  DeviceId " + authRequest.DeviceId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());
                            _logger.LogInfoMessage(_logTracking.ToString());
                            _logger.LogInfoMessage(_logMessage.ToString());
                            return Ok(authresponse);
                        }
                    }
                    else
                    {
                        _logMessage.AppendLine("Authentication failed for driver.");
                        _logger.LogInfoMessage(_logMessage.ToString());

                        authresponse = CommonFunctions.FailedResponse(authRequest, authresponse, authHeader, PaymentAPIResources.UnauthorizedAccess);
                        return Ok(authresponse);
                    }
                }
                else
                {
                    _logMessage.AppendLine("driverDto was null.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    authresponse = CommonFunctions.FailedResponse(authRequest, authresponse, authHeader, PaymentAPIResources.InvalidDriver);
                    return Ok(authresponse);
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                authresponse = CommonFunctions.FailedResponse(authRequest, authresponse, authHeader, PaymentAPIResources.Exception);
                return Ok(authresponse);
            }
        }

        public IHttpActionResult CnKeyUpdateRequest()
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            
            try
            {
                // Prepare payload in iso8583 message format
                TransactionRequest tr = new TransactionRequest();
                tr.TerminalId = "01351646";     //zzz What should this value actually be?  Really? What is it????

                string clientRef = CnTransactionMessage.GetCanadaClientRef();
                tr.Stan = GetSystemTraceNumber(240, tr.TerminalId);

                string referenceNumber = _transactionService.GetRefNumber();
                tr.TransRefNo = FormatFields.FormatReferenceNo(tr.TerminalId, tr.Stan, "Credit", referenceNumber);
                tr.IndustryType = "KeyUpdateRequest";

                CnTransactionEntities cnTrans = new CnTransactionEntities();
                tr.CnPayload = CnPreparePostTransaction.PrepareKeyUpdateRequest(tr, clientRef, out cnTrans);

                // Sending transaction request and parsing the response then save in the database
                Global.ActiveUrl = _transactionService.GetTransactionUrl();
                string transResponse = String.Empty;
                transResponse = CnSendTransaction.SendMessageToDatawire(Global.ActiveUrl, tr.CnPayload);

                //zzz Chetu 357 : return Ok("sucess");
                return Ok("success");
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.Exception, _localVars.DeviceId, _localVars.RequestId);

                return Ok(response);
            }
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Used to get mac details from first data
        /// </summary>
        /// <param name="firstDataDetails"></param>
        /// <returns>MACVerification</returns>
        public MACVerification GetMacDetails(FirstDataDetailsResultDto firstDataDetails, string stan = null)
        {
            MACVerification macDetail = null;

            _logMessage.AppendLine("Controller Name :-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                macDetail = new MACVerification();

                // Prepare payload in iso8583 message format
                TransactionRequest tr = new TransactionRequest();
                tr.Did = firstDataDetails.DID;
                tr.FdMerchantId = firstDataDetails.FDMerchantID;
                tr.TerminalId = firstDataDetails.RCTerminalId;

                string clientRef = CnTransactionMessage.GetCanadaClientRef();
                tr.Stan = stan;

                string referenceNumber = _transactionService.GetRefNumber();
                tr.TransRefNo = FormatFields.FormatReferenceNo(tr.TerminalId, tr.Stan, PymtTypeType.Debit.ToString(), referenceNumber);

                CnTransactionEntities cnTrans = new CnTransactionEntities();

                tr.IndustryType = SigmaStaticValue.NewKeyRequest.ToString();
                tr.CnPayload = CnPreparePostTransaction.PrepareNewKeyRequest(tr, clientRef, out cnTrans);

                _logMessage.AppendLine(string.Format("IndustryType:{0};clientRef:{1};TransRefNo:{2};", tr.IndustryType, clientRef, tr.TransRefNo));

                // Sending transaction request and parsing the response then save in the database
                Global.ActiveUrl = _transactionService.GetTransactionUrl();
                string transResponse = CnSendTransaction.SendMessageToDatawire(Global.ActiveUrl, tr.CnPayload);

                _logMessage.AppendLine(string.Format("GlobalActiveURL:{0};", Global.ActiveUrl));
                _logMessage.AppendLine(string.Format("CnPayload:{0};", tr.CnPayload));

                if (transResponse != null)
                {
                    _logMessage.AppendLine(string.Format("TransResponse:{0};", transResponse));

                    cnTrans.IsCapKey = true;
                    int transId = 0;
                    cnTrans.CompleteResponse = transResponse;
                    Iso8583Response isoResponse = HandleResponse(cnTrans, transId);

                    //zzz
                    // We are obviously not getting a ResponseCode = Code00!!!
                    if (isoResponse.ResponseCode == PaymentAPIResources.ResponseCode00)
                    {
                        string bit63 = CnGenericHandler.ConvertHexToAscii(isoResponse.FDPrivateUsageData63).ToString();
                        string s32 = bit63.Substring(32, 20).ToString();
                        string s34 = bit63.Substring(56, 48).ToString();

                        _logMessage.AppendLine(string.Format("PrivateData63:{0};Bit63:{1};s32:{2};s34:{3};", isoResponse.FDPrivateUsageData63, bit63, s32, s34));

                        macDetail.MacData = s32.Substring(0, 16).ToString();
                        macDetail.CheckDigit = s32.Substring(16, 4).ToString();
                        macDetail.TMAC = s34.Substring(0, 16).ToString();
                        macDetail.TKPE = s34.Substring(16, 16).ToString();
                        macDetail.TKME = s34.Substring(32, 16).ToString();

                        _logMessage.AppendLine(string.Format("Bit32:{0};Bit34:{1};Stan:{2};MacData:{3};CheckDigit:{4};", s32, s34, macDetail.STAN, macDetail.MacData, macDetail.CheckDigit));
                    }
                    else
                    {
                        _logMessage.Append(string.Format("ResponseCode:{0};Bit63:{1};AddData:{2};Message:{3};RetCode:{4};", isoResponse.ResponseCode, isoResponse.FDPrivateUsageData63, isoResponse.AdditionalResponseData, isoResponse.Message, isoResponse.ReturnCode));
                    }

                    _logger.LogInfoMessage(_logMessage.ToString());
                }

                return macDetail;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                return macDetail;
            }
        }

        /// <summary>
        /// Validate request for refund and void transaction 
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="merchantId"></param>
        /// <returns>Boolean</returns>
        private bool ValidateRefundVoidRequest(int transactionId, int merchantId)
        {
            try
            {
                bool isValid = true;

                MTDTransactionDto transDto = _transactionService.GetTransaction(transactionId, merchantId);

                if ((bool)transDto.IsRefunded)
                {
                    isValid = false;
                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.Transaction_Already_Refunded_Voided, _localVars.DeviceId, _localVars.RequestId);
                }

                if ((bool)transDto.IsVoided)
                {
                    isValid = false;
                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.Transaction_Already_Refunded_Voided, _localVars.DeviceId, _localVars.RequestId);
                }

                return isValid;
            }
            catch
            {
                throw;
            }
        }
    }
}
