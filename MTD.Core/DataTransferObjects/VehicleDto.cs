﻿using System;

namespace MTD.Core.DataTransferObjects
{
    public class VehicleDto
    {
        public int VehicleID { get; set; }
        public string VehicleNumber { get; set; }
        public string VehicleRegNo { get; set; }

        public string PlateNumber { get; set; }
        public int fk_FleetID { get; set; }
        public int? fk_TerminalID { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string FleetName { get; set; }
        public string DeviceName { get; set; }
        public Nullable<int> fk_CarOwenerId { get; set; }
        public string FailedError { get; set; }
        public string CarOwnerEmail { get; set; }
        public string CarOwnerName { get; set; }
    }
}