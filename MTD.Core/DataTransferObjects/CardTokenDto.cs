﻿using System;

namespace MTD.Core.DataTransferObjects
{
    public class CardTokenDto
    {
        public int? Id { get; set; }
        public string CardNumber { get; set; }
        public string Token { get; set; }
        public string CardType { get; set; }
        public string CustomerId { get; set; }
        public string ExpiryDate { get; set; }
        public Nullable<int> EncryptMethod { get; set; }
        public Nullable<int> KeyVersion { get; set; }

        //PAY-12 Customer AVS details
        public string Unit { get; set; }
        public string StreetNumber { get; set; }
        public string Street { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }

        public override string ToString()
        {
            return string.Format("CardNumber:{0};Token:{1};CardType:{2};CustomerId:{3};ExpiryDate:{4};EncryptMeghod:{5};KeyVersion:{6};Unit:{7};StreetNumber:{8};Street:{9};Line1:{10};Line2:{11};Suburb:{12};State:{13};Postcode:{14};",
                CardNumber, Token, CardType, CustomerId, ExpiryDate, EncryptMethod, KeyVersion, Unit, StreetNumber, Street, AddressLine1, AddressLine2, Suburb, State, Postcode);
        }
    }
}
