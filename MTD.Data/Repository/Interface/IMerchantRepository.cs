﻿using System.Collections.Generic;

using MTD.Data.Data;

namespace MTD.Data.Repository.Interface
{
    public interface IMerchantRepository
    {

        bool Add(MerchantUser merchant);
        int Update(MerchantUser merchant, int merchantId);
        MerchantUser GetMerchant(int merchantId, int userId);
        MerchantUser GetMerchantViewAs(int merchantId, int userId);
        void DeleteMerchant(int merchantId);
        IEnumerable<Merchant> GetMerchants();
        IEnumerable<MerchantUser> MerchantList(string name);
        IEnumerable<Role> GetUsersRole();
        void AddReciept(ManageReciept reciept);
        Merchant GetMerchant(int merchantId);
        IEnumerable<MerchantUser> GetMerchantByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting);
        FDMerchant GetFDMerchant(int merchantId);
        FDMerchant FdMerchantExist(int fkDId);
        Datawire GetDataWireDetails(int merchantId, int id);
        Terminal TerminalDetails(string serialNo);
        bool DeleteFdMerchant(int fdMerchantId);
        FDMerchant GetFdMerchantUpdate(int fdMerchantId);
        void UpdateFdMerchant(FDMerchant fdMerchant, int fdMerchantId);
        FDMerchant GetFdMerchantForUpdate(int fdId);
        IEnumerable<FDMerchant> GetFDMerchantList();
        IEnumerable<FDMerchant> GetFdMerchantByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting);
        bool FdMerchantAdd(FDMerchant merchant);
        int GetMerchantId(string email);
        int AddFdMerchant(FDMerchant merchant);
        bool CompanyExsit(string company);
        bool CompanyExsit(string company, int merchantId);
        bool EmailExist(string email);

        bool UpdatePassword(int userId, string newPassword);
        bool UpdateTCPassword(int merchantId, string userName, string newPassword);
        string IsAllReadySet(int merchantId);
        string GetEmail(int userId);
        IEnumerable<Recipient> GetUserReceipent(int merchantId);
        bool UpdateUserRecipient(RecipientMaster recipient);
        IEnumerable<Report> GetReportList();
        IEnumerable<Recipient> GetUserReceipent(int merchantId, int reportId);
        bool IsUserAvailable(string userName);
        string GetMerchantZipCode(int merchantId);

        //PAY-13 Trans Armor flag set from Merchant UseTransArmor setting.
        bool IsTransArmor(int merchantId);
    }
}
