﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;

namespace MTData.WebAPI.CanadaCommon
{
    public static class Global
    {
        /// <summary>
        /// The ServiceID which used for processing the request on datawire
        /// </summary>
        internal const string ServiceID = "115";

        /// <summary>
        /// User Agent is used for adding header
        /// </summary>
        internal const string UserAgent = "vxnapi_xml_3_2_0_19";

        /// <summary>
        /// internal constant string ApplicationID = "";  //Not in use because its using the AppNameNVersion as App element
        /// </summary>
        internal const string AppNameNVersion = "MTDATATAXIGWYXML";

        internal static string Version { get { return "3.0"; } }

        internal static string RegistrationServerUrl
        {
            get { return ConfigurationManager.AppSettings["RegistrationServerUrl"]; }
        }

        internal static string ActiveUrl { get; set; }

        internal static List<string> UrlList { get; set; }

        /// <summary>
        /// Get the transaction timeout time in miliseconds.
        /// </summary>
        internal static int TransactionTimeout
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["TransactionTimeout"]);  }
        }

        internal static string BaseDerivationKeyID
        {
            get
            {
                //BaseDerivationKeyID should be pad on LEFT with ‘F’s if its length is less then 9.
                //string baseDerivationKeyID = ConfigurationManager.AppSettings["BaseDerivationKeyID"];
                string baseDerivationKeyID = "testId";//own case
                return baseDerivationKeyID.Length >= 9 ? baseDerivationKeyID.Substring(0, 8) : baseDerivationKeyID.PadLeft(9, 'F');
            }
        }

        internal static int DuplicateTransactionInterval
        {
            get { return 1234; }
        }

        public static bool IsNewGlobalBinAvailable { get; set; }

        public static void UpdateIsNewGlobalBinAvailable()
        {
            var result = false;
            //reset to unavailable
            IsNewGlobalBinAvailable = result;
        }

        /// <summary>
        /// Get waiting time, before sending a TOR transaction.
        /// </summary>
        internal static long TORWaitingTime
        {
            get {  return 2; }
        }
    }

}