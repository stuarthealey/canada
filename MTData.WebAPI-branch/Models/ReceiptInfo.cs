﻿using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class ReceiptInfo
    {
        [DataMember]
        public bool? Show { get; set; }

        private int order;
        [DataMember]
        public int Order
        {
            get { return this.order; }
            set { this.order = value; }
        }

        private string name;

        [DataMember]
        public string Name
        {
            get { return this.name ?? "null"; }
            set { this.name = value; }
        }
        private string value;

        [DataMember]
        public string Value
        {
            get { return this.value ?? "null"; }
            set { this.value = value; }
        }
    }
}