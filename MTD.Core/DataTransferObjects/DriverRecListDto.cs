﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
    public class DriverRecListDto
    {
        public Nullable<int> fk_FleetId { get; set; }
        public Nullable<int> UserType { get; set; }
        public string ModifiedBy { get; set; }
        public IEnumerable<DriverRecipientDto> UserList { get; set; }
        public IEnumerable<FleetDto> fleetlist { get; set; }
    }
    
}
