﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Globalization;
using System.Reflection;
using System.Text;
using Ninject;

using MTD.Core.Service.Interface;
using MTData.Web.Resource;
using MTData.Web.ViewData;
using MTData.Utility;

namespace MTData.Web.Controllers
{
    public class DriverRecordController : Controller
    {
        private readonly IReportCommonService _reportcommonService;
        private StringBuilder _logMessage;
        readonly ILogger _logger = new Logger();

        public DriverRecordController([Named("ReportCommonService")] IReportCommonService reportcommonService, StringBuilder logMessage)
        {
            _reportcommonService = reportcommonService;
            _logMessage = logMessage;
        }

        /// <summary>
        /// Purpose             :   To get the fleet
        /// Function Name       :   GetFleet
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   19/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>   
        /// <param name="id"></param>      
        /// <returns></returns>
        public JsonResult GetFleet(int id)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                _logMessage.Append("calling GetFleetList method of _reportcommonService takes id= "+id);
                var fleetDtoList = _reportcommonService.GetFleetList(id);
                _logMessage.Append("GetFleetList method of _reportcommonService class executed successfully");
                _logMessage.Append("taking records from  fleetDtoList and adding to list<FleetListViewData>");
                List<FleetListViewData> objFleetlist = fleetDtoList.Select(fvd => new FleetListViewData {FleetName = fvd.FleetName, FleetID = fvd.FleetID}).ToList();
                _logMessage.Append("get records from  fleetDtoList successfully");
                var fleet = new List<SelectListItem> { new SelectListItem { Text = MtdataResource.Select_Fleet, Value = "0" } };
                _logMessage.Append("taking records from  objFleetlist and adding to list<SelectListItem>");
                fleet.AddRange(from fleetView in objFleetlist where true select new SelectListItem {Text = fleetView.FleetName, Value = Convert.ToString(fleetView.FleetID)});
                _logMessage.Append("get records from  objFleetlist successfully");
                return Json(new SelectList(fleet, "Value", "Text"));
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

        /// <summary>
        /// Purpose             :   To get the vehicle
        /// Function Name       :   GetVehicle
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   20/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>   
        /// <param name="id"></param>      
        /// <returns></returns>
        public JsonResult GetVehicle(int id)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                _logMessage.Append("calling GetVehicleList method of _reportcommonService takes id= " + id);
                var vehicleDtoList = _reportcommonService.GetVehicleList(id);
                _logMessage.Append("GetVehicleList method of _reportcommonService class executed successfully");
                _logMessage.Append("taking records from  vehicleDtoList and adding to list<VehicleListViewData>");
                var lvd = vehicleDtoList.Select(fvd => new VehicleListViewData {VehicleID = fvd.VehicleID, VehicleNumber = fvd.VehicleNumber}).ToList();
              
                _logMessage.Append("get records from  lvd successfully");
                return Json(new SelectList(lvd, "Value", "Text"));
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

        /// <summary>
        /// Purpose             :   To get the vehicle data
        /// Function Name       :   GetVehicleData
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   21/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>   
        /// <param name="vehicleNumber"></param>    
        /// <param name="fleetId"></param>    
        ///   
        /// <returns></returns>
        public JsonResult GetVehicleData(string vehicleNumber, int fleetId)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                _logMessage.Append("calling GetVehicleList method of _reportcommonService takes vehicleNumber and fleetId = " + vehicleNumber + ", " + fleetId);
                var vehicleDtoList = _reportcommonService.GetVehicleList(vehicleNumber, fleetId);
                _logMessage.Append("GetVehicleList method of _reportcommonService class executed successfully");
                _logMessage.Append("taking records from  vehicleDtoList and adding to list<VehicleListViewData>");
                List<VehicleListViewData> lvd = vehicleDtoList.Select(fvd => new VehicleListViewData {VehicleID = fvd.VehicleID, VehicleNumber = fvd.VehicleNumber}).ToList();
                _logMessage.Append("get records from  objFleetlist successfully");
                _logMessage.Append("taking records from  lvd and adding to list<VehicleListViewData>");
                List<string> vehicle = (from vehicleView in lvd where true select vehicleView.VehicleNumber).ToList();
                _logMessage.Append("get records from  lvd successfully");
                return Json(vehicle, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

        /// <summary>
        /// Purpose             :   To get the driver data
        /// Function Name       :   GetDriverData
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   22/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>   
        /// <param name="driverNumber"></param>    
        /// <param name="fleetId"></param>    
        ///   
        /// <returns></returns>
        public JsonResult GetDriverData(string driverNumber, int fleetId)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                var vehicleDtoList = _reportcommonService.GetDriverList(driverNumber, fleetId);

                _logMessage.Append("taking records from  vehicleDtoList and adding to list<VehicleListViewData>");
                List<DriverListViewData> lvd = vehicleDtoList.Select(fvd => new DriverListViewData {DriverID = fvd.DriverID, DriverNumber = fvd.DriverNumber}).ToList();
                _logMessage.Append("get records from  objFleetlist successfully");
                _logMessage.Append("taking records from  lvd and adding to list<VehicleListViewData>");
                List<string> driver = (from driverView in lvd where true select driverView.DriverNumber).ToList();
                _logMessage.Append("get records from  lvd successfully");
                return Json(driver, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }
    }
}