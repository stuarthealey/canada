﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace MTData.WebAPI.Common
{
    /// <summary>
    /// Purpose             :   AES256 Encryption Logic
    /// Class Name          :   AES256EncryptionLogic
    /// Created By          :   Sunil Singh
    /// Created On          :   23-Sep-2015
    /// Modification Made   :   *************
    /// Modified On         :   "MM/DD/YYYY"  
    /// </summary>
    /// <param name="cipherText"></param>
    /// <returns></returns>
    public static class AES256EncryptionLogic
    {
        /// <summary>
        /// Shake salt byte
        /// </summary>
        private static byte[] _saltShaker = new byte[]
        {
            0x47,0xCF,0x6D,0xFF,0x3A,0xAE,0x29,0x78,0xBA,0x25,0xD0,0xD7,0x04,0x46,0x75,0xC1,0x8B,0x35,0x0A,0xA2,0xC2,0x1F,0x63,0xE9,0x4C,0x0D,0x13,0x7D,0x43,0x86,0xE4,0x76,
            0x42,0xDC,0x68,0xC9,0x79,0xAB,0xFB,0x12,0x4F,0xB4,0x80,0xC7,0x89,0xF5,0x36,0x9D,0xEC,0x9E,0x4B,0xCA,0xA7,0x59,0x34,0x24,0x6B,0x85,0xB6,0x2C,0x9B,0xB1,0x32,0x33,
            0x45,0x5D,0x96,0x26,0xE2,0xAD,0x50,0xE1,0x3B,0xF8,0x58,0x0B,0xA4,0x9C,0x64,0x56,0xD8,0x74,0x91,0xA6,0x31,0x16,0xD1,0xEB,0xA5,0x3E,0x5F,0xF2,0xF7,0xCD,0x73,0x99,
            0x7F,0x44,0x83,0x22,0x61,0x09,0xC0,0x3D,0x53,0x6E,0x92,0xB7,0x02,0x23,0x8A,0xD6,0x1C,0x62,0x5B,0x20,0x1A,0x98,0xFD,0x28,0x95,0xB5,0x8D,0xAA,0x51,0x5E,0xED,0x71,
            0x88,0xC4,0x94,0x5A,0x14,0x30,0xF4,0xCE,0xBE,0x4D,0x52,0xD5,0x8F,0x15,0xE7,0x7C,0xC3,0x90,0xD9,0xA0,0x70,0x48,0x7E,0xF0,0x3F,0x55,0xA9,0xBD,0x8C,0x11,0xF3,0xF9,
            0x82,0x18,0x21,0x40,0xAC,0x1D,0xC5,0xEA,0x6A,0x9A,0x6C,0xDB,0x37,0x2A,0xB0,0x72,0xD3,0x06,0xE6,0xCB,0x8E,0x4A,0xFC,0x2B,0x2F,0xAF,0x08,0xC8,0xA3,0x38,0x39,0xA8,
            0x60,0x5C,0x41,0x6F,0xEF,0xB2,0xE0,0x97,0xDE,0xE5,0x10,0xE3,0xDF,0xB3,0xB8,0x07,0xBC,0x17,0xF1,0x69,0x1E,0x1B,0x66,0x19,0x0F,0xFA,0x01,0xFE,0x0C,0xEE,0x54,0x2D,
            0x65,0xD2,0x7B,0xDD,0xDA,0x2E,0x84,0x27,0x3C,0x67,0xE8,0xA1,0x0E,0x03,0x7A,0x9F,0xF6,0xD4,0x00,0x81,0x57,0xBF,0x77,0x49,0xCC,0x05,0x87,0xC6,0xB9,0xBB,0x4E,0x93
        };

        /// <summary>
        /// Preparing salt
        /// </summary>
        /// <param name="pwd"></param>
        /// <returns></returns>
        private static byte[] MakeSalt(string pwd)
        {
            byte[] salt = new byte[16];
            for (int i = 0; i < salt.Length; i++)
            {
                salt[i] = _saltShaker[(pwd[i % pwd.Length] + i) & 0xFF];
            }

            return salt;
        }

        /// <summary>
        /// Encrypt using AES256 algorithm
        /// </summary>
        /// <param name="plainText">Plain text to encrypt</param>
        /// <param name="symmetricKey">Symmetric key to be use for encryption</param>
        /// <returns>Encrypted text</returns>
        public static string EncryptAES256(this string plainText, string symmetricKey)
        {
            string encryptedText = string.Empty;
            byte[] clearBytes = Encoding.Unicode.GetBytes(plainText);
            
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes rfcDeriveBytes = new Rfc2898DeriveBytes(symmetricKey, MakeSalt(symmetricKey));
                encryptor.KeySize = 256;
                encryptor.Key = rfcDeriveBytes.GetBytes(32);
                encryptor.IV = rfcDeriveBytes.GetBytes(16);
                rfcDeriveBytes.Dispose();

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(clearBytes, 0, clearBytes.Length);
                        cryptoStream.Close();
                    }

                    encryptedText = Convert.ToBase64String(memoryStream.ToArray());
                }
            }

            return encryptedText;
        }

        /// <summary>
        /// Decrypt using AES256 algorithm
        /// </summary>
        /// <param name="encryptedText">Encrypted text using AES256 algorithm</param>
        /// <param name="symmetricKey">Symmetric key to be use for encryption</param>
        /// <returns>Decrypted Text</returns>
        public static string DecryptAES256(this string encryptedText, string symmetricKey)
        {
            string plainText = string.Empty;
            byte[] cipherBytes = Convert.FromBase64String(encryptedText);

            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes rfc2898DeriveBytes = new Rfc2898DeriveBytes(symmetricKey, MakeSalt(symmetricKey));
                encryptor.KeySize = 256;
                encryptor.Key = rfc2898DeriveBytes.GetBytes(32);
                encryptor.IV = rfc2898DeriveBytes.GetBytes(16);
                rfc2898DeriveBytes.Dispose();
                
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(cipherBytes, 0, cipherBytes.Length);
                        cryptoStream.Close();
                    }

                    plainText = Encoding.Unicode.GetString(memoryStream.ToArray());
                }
            }

            return plainText;
        }
    }
}