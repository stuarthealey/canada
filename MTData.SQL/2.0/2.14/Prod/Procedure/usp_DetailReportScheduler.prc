
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DetailReportScheduler]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_DetailReportScheduler]
GO

/****** Object:  StoredProcedure [dbo].[usp_DetailReportScheduler]    Script Date: 06/01/2016 10:59:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <14-06-2015>
-- Description:	<Prepare and Send detail reports to merchants>
-- =============================================
CREATE PROCEDURE [dbo].[usp_DetailReportScheduler]
AS
BEGIN TRY
 SET NOCOUNT ON;
 SET ANSI_WARNINGS OFF
 DECLARE @Res INT,
  @MerchantId INT,
  @To VARCHAR(50),
  @Body NVARCHAR(MAX),
  @Error VARCHAR(200),
  @MailId INT,
  @Valid INT,
  @MsgXml NVARCHAR(40),
  @XmlData NVARCHAR(MAX),
  @MerchantName VARCHAR(50),    
  @Company VARCHAR(100),
  @ReportType INT,
  @RepType VARCHAR(50),
  @RepHeading VARCHAR(200),
  @FDate DATETIME,
  @TDate DATETIME,
  @ReportId INT,
  @NextScheduleDateTime DATETIME,
  @NextAdminScheduleDateTime DATETIME,
  @Frequency INT,
  @FrequencyType INT,
  @ScheduleId INT
  DECLARE @CursorData TABLE
    (
      MerchantId INT,
	  Email NVARCHAR(80),
	  Company NVARCHAR(80),
	  ReportId  INT NULL
    )
  SELECT @NextScheduleDateTime=NextScheduleDateTime FROM ScheduleReport WHERE fk_MerchantId IS NULL AND fk_ReportID=2 AND IsCreated=1
  INSERT INTO @CursorData(MerchantId,Email,Company,ReportId)
  SELECT distinct m.MerchantID,u.Email,m.Company,NULLIF(2,sc.fk_ReportID)
  FROM Merchant  m INNER JOIN recipient r ON m.MerchantID=r.fk_MerchantID 
  INNER JOIN  Users u ON m.MerchantID=u.fk_MerchantID 
  LEFT JOIN ScheduleReport sc on sc.fk_MerchantId=m.MerchantID
  WHERE r.IsRecipient=1 AND r.fk_UserTypeId=2 AND r.fk_ReportId=2
  AND m.IsActive=1 AND u.fk_UserTypeID=2 
  AND ((sc.fk_ReportID=2 AND sc.IsCreated=1 AND sc.NextScheduleDateTime>=DATEADD(SECOND,-900,GETDATE()) AND sc.NextScheduleDateTime<GETDATE())
  OR (sc.fk_ReportID IS NULL AND @NextScheduleDateTime>=DATEADD(SECOND,-900,GETDATE()) AND @NextScheduleDateTime<GETDATE())
  OR (sc.fk_ReportID IN (1,3) AND 1<>(SELECT COUNT(*) FROM ScheduleReport WHERE fk_MerchantId=sc.fk_MerchantId AND IsCreated=1 AND fk_ReportID=2) 
  AND @NextScheduleDateTime>=DATEADD(SECOND,-900,GETDATE()) AND @NextScheduleDateTime<GETDATE()))
 --Declaring cursor for fetching all the merchant recipients for detail report
  DECLARE queue_cursor CURSOR FOR 
  SELECT MerchantId,Email,Company,ReportId FROM @CursorData
  OPEN queue_cursor
  FETCH NEXT FROM queue_cursor INTO @MerchantId,@To,@Company,@ReportId
  WHILE @@FETCH_STATUS = 0   
  BEGIN   
  SELECT @MerchantName=FName FROM Users  WHERE fk_MerchantID=@MerchantId AND fk_UserTypeID=2
  --Exceuting store procedure DetailReport to get data in xml format
  EXEC usp_DetailReport @MerchantId,@ReportId,@XmlData OUTPUT,@ReportType OUTPUT,@FDate OUTPUT,@TDate OUTPUT
  SET  @RepType=CASE @ReportType WHEN 1 THEN 'Daily Detailed Transaction Report'
  WHEN 2 THEN 'Weekly Detailed Transaction Report' ELSE 'Monthly Detailed Transaction Report' END
  SET @RepHeading=@RepType+' '+'(Dated from'+' '+ CONVERT(VARCHAR(11),@FDate,106)+' '+'to'+' '+CONVERT(VARCHAR(11),@TDate,106)+')'
  IF(@XmlData IS NULL or @XmlData='') BEGIN
  SET @Valid=0
  INSERT INTO MailMessage(RepUserId,[Status],SendTo,Body,RepUserTypeId,[Date],ReportType) VALUES(@MerchantId,-1,@To,'No records in database',2,GETDATE(),'DetailReport')
  END
  ELSE BEGIN
  SET @Valid=1
  END
  IF(@Valid=1) BEGIN
  SET @MsgXml=CAST((SELECT CONCAT('Dear',' ',@MerchantName,',') FOR XML PATH('p'), ELEMENTS ) AS NVARCHAR(MAX))
  --Set the xml data into html fromat
  SET @Body ='<html><style>div{font-size:13px;text-align:center;horizontal-align: middle;background-color:lightgrey}
  td{text-align:left;font-family:verdana;font-size:12px;}p{font-family:verdana;font-size:13px}span{font-family:verdana;font-size:13px}
  </style><body><p>Listed below is the transaction report containing details of all transactions.</p>
  <H5 style="color:black;text-align:center;font-size:11px;font-family:verdana"> Merchant  :  '+@Company+'</H5>
  <H5 style="color:grey;text-align:center;font-size:11px;font-family:verdana">'+@RepHeading+'</H5>
  <table border = 1 cellpadding="0" cellspacing="0" style="background-color:white;border-collapse:collapse"> <tr>
  <th style="color:grey;font-size:13px;width:900px;text-align:left">Merchant</th>
  <th style="color:grey;font-size:13px;width:700px;text-align:left">Report Date</th>
  <th style="color:grey;font-size:13px;width:120px;text-align:left">Trans Id</th>
  <th style="color:grey;font-size:13px;width:180px;text-align:left">Transaction Date</th>
  <th style="color:grey;font-size:13px;width:130px;text-align:left">Trans Type</th>
  <th style="color:grey;font-size:13px;width:140px;text-align:left">Card Type</th>
  <th style="color:grey;font-size:13px;width:180px;text-align:left">Last Four</th>
  <th style="color:grey;font-size:13px;width:150px;text-align:left">Auth Code</th>
  <th style="color:grey;font-size:13px;width:200px;text-align:left">Trip</th>
  <th style="color:grey;font-size:13px;width:500px;text-align:left">Driver</th>
  <th style="color:grey;font-size:13px;width:500px;text-align:left">Car Number</th>
   <th style="color:grey;font-size:13px;width:500px;text-align:left">Tech Fee</th>
  <th style="color:grey;font-size:13px;width:500px;text-align:left">Surcharge</th>
  <th style="color:grey;font-size:13px;width:150px;text-align:left">Gross Amt</th>
  <th style="color:grey;font-size:13px;width:150px;text-align:left">Fees</th>
  <th style="color:grey;font-size:13px;width:150px;text-align:left">Net Amt</th></tr>'    
  SET @Body = @MsgXml+@Body + @XmlData +'</table><br/><br/><span>Thanks & Regards,<br/><br/>MTData Developments Pty Ltd.<br/>www.mtdata.com<span>
  <h4 style="color:grey;font-size:12px;font-family:verdana">MTData-All rights reserved</h4>
  <p style="font-size:11px;color:black">Note:This is an automated message. Do not Reply or Reply All
  to this message as replies are not being accepted.</p></body></html>'
  INSERT INTO MailMessage(RepUserId,[Status],SendTo,Body,RepUserTypeId,[Date],ReportType) VALUES(@MerchantId,0,@To,@Body,2,GETDATE(),'DetailReport')
  SET @MailId=SCOPE_IDENTITY()
  --Exceute the store procedure sp_send_dbmail to send the detail report to corresponding merchant
  EXEC @Res=msdb.dbo.sp_send_dbmail
  @profile_name = 'MTData',
  @recipients =@To,
  @body =@Body,
  @subject = 'Merchant Detailed Transaction Report',
  @body_format ='HTML'
  IF (@Res != 0)
  BEGIN
    SET @Error =@@ERROR
    UPDATE MailMessage SET Error=@Error,[Date]=GETDATE() WHERE MailId=@MailId
  END;
  ELSE
  BEGIN
     UPDATE MailMessage SET [Status]=1,[Date]=GETDATE() WHERE MailId=@MailId
  END
  END
  FETCH NEXT FROM queue_cursor INTO @MerchantId,@To,@Company,@ReportId
  END   
  CLOSE queue_cursor   
  DEALLOCATE queue_cursor
  SELECT  @ScheduleId=ScheduleID,
	      @Frequency=Frequency,
          @FrequencyType=FrequencyType,
	      @NextAdminScheduleDateTime=NextScheduleDateTime		 
   FROM ScheduleReport 
   WHERE fk_ReportId=2 AND IsCreated=1 AND fk_MerchantId IS NULL
   IF(@NextAdminScheduleDateTime>=DATEADD(SECOND,-900,GETDATE()) AND @NextAdminScheduleDateTime<GETDATE())
   BEGIN
   IF(@Frequency=1)
   BEGIN
	  SET @NextAdminScheduleDateTime=@NextAdminScheduleDateTime+@FrequencyType
   END
   ELSE IF(@Frequency=2)
   BEGIN
	  SET @NextAdminScheduleDateTime=DATEADD(WK,@FrequencyType,@NextAdminScheduleDateTime)
   END
   ELSE IF(@Frequency=3)
   BEGIN
	  SET @NextAdminScheduleDateTime=DATEADD(MONTH,@FrequencyType,@NextAdminScheduleDateTime)
   END  
	UPDATE ScheduleReport SET NextScheduleDateTime=@NextAdminScheduleDateTime WHERE ScheduleID=@ScheduleId
	END
END TRY
--use catch to log error in table ProcErrorHandler
BEGIN CATCH
       INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
       SELECT ERROR_NUMBER()
	         ,ERROR_SEVERITY()
			 ,ERROR_STATE()
			 ,ERROR_PROCEDURE()
			 ,ERROR_LINE()
			 ,ERROR_MESSAGE()
			 ,SUSER_SNAME()
			 ,HOST_NAME()
			 ,GETDATE()
END CATCH
GO



