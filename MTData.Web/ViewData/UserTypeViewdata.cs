﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.Web.ViewData
{
    public class UserTypeViewData
    {
        public int UsetTypeId { get; set; }
        public string UserType { get; set; }
    }
}