﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.Web.ViewData
{
    public class GetAdminExceptionReportViewData
    {
        public string MerchantName { get; set; }
        public string FleetName { get; set; }
        public int ExpFleetTrans { get; set; }
        public int ActFleetTrans { get; set; }
        public int ExpCar { get; set; }
        public int ActCar { get; set; }
        public string ExpValue { get; set; }
        public string ActValue { get; set; }
        public string ExpCase { get; set; }
    }
}