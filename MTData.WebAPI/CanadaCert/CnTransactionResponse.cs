﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using MTData.WebAPI.CanadaCommon;

namespace MTData.WebAPI.CanadaCert
{
    [Serializable, DataContract]
    public class CnTransactionResponse
    {
        #region Public properties
        /// <summary>
        /// Get and set the transaction message in details.
        /// </summary>
        [DataMember]
        public string TransactionMessage { get; set; }

        /// <summary>
        /// Get and set the transaction Response Code.
        /// </summary>
        [DataMember]
        public string ResponseCode { get; set; }

        /// <summary>
        /// Get and set the responded transaction amount.
        /// </summary>
        [DataMember]
        public decimal TransactionAmount { get; set; }

        /// <summary>
        /// Get and set the Transaction Id.
        /// Transaction Id can be obtained from database after saving the request.
        /// </summary>
        [DataMember]
        public long TransactionId { get; set; }

        /// <summary>
        /// Unique ID from end user 
        /// </summary>
        [DataMember]
        public string InvoiceNo { get; set; }

        /// <summary>
        /// Get and set the AVS response.
        /// </summary>
        [DataMember]
        public string AVSResponse { get; set; }

        /// <summary>
        /// Get and set the Card Code Value.
        /// </summary>
        [DataMember]
        public string CCVResponse { get; set; }

        /// <summary>
        /// Used to hold the list of error return by service
        /// </summary>
        [DataMember]
        public List<string> ValidationErrors { get; set; }

        /// <summary>
        /// Get and set the processed transaction type.
        /// </summary>
        [DataMember]
        public TransactionProcessed TransactionProcessed { get; set; }

        /// <summary>
        /// Get and set the responded balance amount.
        /// </summary>
        [DataMember]
        public decimal? BalanceAmount { get; set; }

        #endregion

        #region Other Properties
        /// <summary>
        /// Get and set the response Transaction Id.
        /// This Transaction Id can be obtained after saving the response.
        /// </summary>
        public long ResponseTransactionId { get; set; }

        /// <summary>
        /// Gets or sets the AVS response match status with merchant specified AVS response.
        /// </summary>
        public bool? IsAVSMatch { get; set; }

        /// <summary>
        /// gets or sets the CVV response match status
        /// </summary>
        public bool? IsCVVMatch { get; set; }

        /// <summary>
        /// gets or sets whether TOR required or not.
        /// </summary>
        public bool IsTimeoutReversalRequired { get; set; }

        /// <summary>
        /// gets or sets whether TOR should be repeated or not.
        /// </summary>
        public bool IsTORRepeatRequired { get; set; }

        /// <summary>
        /// get or set the credit cash back amount
        /// </summary>
        public decimal CashBackAmount { get; set; }

        /// <summary>
        /// get or set the Elapsed Time of request and response.
        /// </summary>
        public long ResponseElapsedTime { get; set; }
        #endregion
    }
}