ECHO ** Push release to Canada Web Apps
PAUSE

XCOPY "C:\developmentSV\Payment\canada\build\MTDataPayments" "C:\inetpub\wwwroot\MTDataPaymentsCAN" /S /Y
XCOPY "C:\developmentSV\Payment\canada\build\MTDataPaymentsApp" "C:\inetpub\wwwroot\MTDataPaymentsAppCAN" /S /Y
XCOPY "C:\developmentSV\Payment\canada\build\MTDataPaymentsTerminal" "C:\inetpub\wwwroot\MTDataPaymentsTerminalCAN" /S /Y

ECHO ** Push to Canada Web APPS completed
PAUSE

IISRESET
PAUSE
