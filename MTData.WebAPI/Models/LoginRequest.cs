﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Models
{
    public class LoginRequest
    {
        public string RequestId { get; set; }
        public string DeviceId { get; set; }
        public string DriverId { get; set; }
        public string DriverPin { get; set; }
    }
}