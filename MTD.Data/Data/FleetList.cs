﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Data.Data
{
    public class FleetList
    {
        public int FleetID { get; set; }
        public string FleetName { get; set; }
    }
}
