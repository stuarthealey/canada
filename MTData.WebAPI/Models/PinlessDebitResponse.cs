﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Models
{
    public class PinlessDebitResponse
    {
        public CommonGrp CommonGrp { get; set; }
        public CardGrp CardGrp { get; set; }
        public TAGrp TAGrp { get; set; }
        public RespGrp RespGrp { get; set; }
        public VisaGrp VisaGrp { get; set; }
        public MCGrp MCGrp { get; set; }
        public AmexGrp AmexGrp { get; set; }
        public DSGrp DSGrp { get; set; }
        public EMVGrp EMVGrp { get; set; }
    }
}