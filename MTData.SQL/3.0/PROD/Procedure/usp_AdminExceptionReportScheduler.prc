IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_AdminExceptionReportScheduler]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_AdminExceptionReportScheduler]
GO

CREATE PROCEDURE [dbo].[usp_AdminExceptionReportScheduler]
(
  @IsOnDemandReport BIT,
  @FromDate DATETIME=NULL,
  @ToDate DATETIME=NULL
)
AS
BEGIN TRY
  SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF
  DECLARE @Res INT,
       @UserId INT,
       @To VARCHAR(50),
       @Body NVARCHAR(MAX),
       @Error VARCHAR(200),
       @MailId INT,
       @Valid INT,
       @MsgXml NVARCHAR(40),
       @XmlData NVARCHAR(MAX),
       @UserName VARCHAR(50),
       @ReportType INT,
       @RepType VARCHAR(50),
       @RepHeading VARCHAR(200),
       @FDate DATETIME,
       @TDate DATETIME,
	   @NextScheduleDateTime DATETIME,
	   @Result INT
       --Declaring cursor for fetching admin recipients for exception report
   DECLARE Queue_Cursor CURSOR FOR 
   SELECT u.UserID,u.Email FROM Users u 
                           INNER JOIN Recipient r 
						   ON u.UserID=r.fk_UserId 
						   WHERE r.IsRecipient=1 AND r.fk_UserTypeId=1 AND r.fk_ReportId=3  
						   AND u.IsActive=1 AND u.fk_UserTypeID=1 
   OPEN Queue_Cursor
   FETCH NEXT FROM Queue_Cursor INTO @UserId,@To
   WHILE @@FETCH_STATUS = 0   
    BEGIN   
     SELECT @UserName=FName FROM Users WHERE UserID=@UserId AND IsActive=1
     --Exceute store procedure AdminExceptionReport to get exception data in xml format
     EXEC @Result=usp_AdminExceptionReport @IsOnDemandReport,@FromDate,@ToDate,
	                                   @XmlData OUTPUT,@ReportType OUTPUT,
									   @FDate OUTPUT,@TDate OUTPUT,@NextScheduleDateTime OUTPUT
	 IF(@Result<>0)
	  BEGIN
	    SELECT 1;
		RETURN;
	  END
	 IF(@IsOnDemandReport=1)
	  BEGIN
	    SET @RepType='On Demand Exception Report' 
	  END
	 ELSE
	  BEGIN
        SET  @RepType=CASE @ReportType WHEN 1 THEN 'Daily Exception Report' WHEN 2 THEN 'Weekly Exception Report' ELSE 'Monthly Exception Report' END
	  END
     SET @RepHeading=@RepType+' '+'(Dated from'+' '+ CONVERT(VARCHAR(11),@FDate,106)+' '+'to'+' '+CONVERT(VARCHAR(11),@TDate,106)+')'
      IF(@XmlData IS NULL or  @XmlData='') 
	   BEGIN
         SET @Valid=0
         INSERT INTO MailMessage(RepUserId,[Status],SendTo,Body,RepUserTypeId,[Date],ReportType) 
		 VALUES(@UserId,-1,@To,'No records in database',1,GETDATE(),'AdminExceptionReport')
       END
      ELSE 
	    BEGIN
          SET @Valid=1
         END
        IF(@Valid=1) 
		  BEGIN
         --Set data in html format
       SET @Body ='<html>
		             <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		             <style>td{text-align:left;font-family:verdana;font-size:12px}
                     p{font-family:verdana;font-size:13px}span{font-family:verdana;font-size:13px}</style>
                     <body><table style="background-color:#e0e0e0; padding:20px; height:100%;"><tr><td><p style="font-size:11px;font-weight:bold">'+CONCAT('Dear',' ',@UserName,',')+'</p>
                     <p style="font-size:11px;font-weight:bold">Listed below is the exception report containing fleet exception details corresponding to merchants.</p>
                     <h5 style="color:#003366;text-align:center;font-size:11px;font-family:verdana; padding:10px;">'+@RepHeading+'</h5>
                    <table border = 1 cellpadding="0" cellspacing="0" style="background-color:#aed6eb; border:2px solid #003366; border-collapse:collapse; padding:2px;">
                    <thead style="background-color:#2b79c7; padding:10px; "><tr>
                    <th style="color:grey;font-size:10px;width:600px;text-align:left;font-family:verdana;color:#fff;padding:5px;">Merchant</th>
                    <th style="color:grey;font-size:10px;width:600px;text-align:left;font-family:verdana;color:#fff;padding:2px;">Fleet</th>
                    <th style="color:grey;font-size:10px;width:600px;text-align:left;font-family:verdana;color:#fff;padding:2px;">Exp Fleet Trans</th>
                    <th style="color:grey;font-size:10px;width:500px;text-align:left;font-family:verdana;color:#fff;padding:2px;">Act Fleet Trans</th>
                    <th style="color:grey;font-size:10px;width:200px;text-align:left;font-family:verdana;color:#fff;padding:2px;">Exp Car</th>
                    <th style="color:grey;font-size:10px;width:200px;left-align:left;font-family:verdana;color:#fff;padding:2px;">Act Car</th>
                    <th style="color:grey;font-size:10px;width:300px;text-align:left;font-family:verdana;color:#fff;padding:2px;">Exp  Value</th>
                    <th style="color:grey;font-size:10px;width:300px;text-align:left;font-family:verdana;color:#fff;padding:2px;">Act Trans Value</th>
                    <th style="color:grey;font-size:10px;width:800px;text-align:left;font-family:verdana;color:#fff;padding:2px;">Exp Case</th></tr></thead>'    
        SET @Body = @Body + @XmlData +'</table><br/><br/><span>Thanks & Regards,<br/><br/>MTData Developments Pty Ltd.
                                               <br/>www.mtdata.us<span><h4 style="color:grey;font-size:12px;font-family:verdana">MTData-All rights reserved</h4>
											     <img src="D:\logo-1.png" width="138" height="44" alt="logo">
                                               <p style="font-size:11px;color:black">Note:This is an automated message. Do not Reply or Reply All to this message as 
                                               replies are not being accepted.</p></body></html>'
        INSERT INTO MailMessage(RepUserId,[Status],SendTo,Body,RepUserTypeId,[Date],ReportType) 
		VALUES(@UserId,0,@To,@Body,1,GETDATE(),'AdminExceptionReport')
        SET @MailId=SCOPE_IDENTITY()
        EXEC @Res=msdb.dbo.sp_send_dbmail
             @profile_name = 'MTData',
             @recipients =@To,
             @body =@Body,
             @subject = 'Admin Exception Report',
             @body_format ='HTML'
       IF (@Res != 0)
         BEGIN
           SET @Error =@@ERROR
           UPDATE MailMessage set Error=@Error,[Date]=GETDATE() WHERE MailId=@MailId
         END;
       ELSE
         BEGIN
           UPDATE MailMessage set [Status]=1,[Date]=GETDATE() where MailId=@MailId
         END
     END
   FETCH NEXT FROM Queue_Cursor INTO @UserId,@To
 END   
 CLOSE Queue_Cursor   
 DEALLOCATE Queue_Cursor
 IF(@IsOnDemandReport=0)
 BEGIN
   UPDATE ScheduleReport SET NextScheduleDateTime=@NextScheduleDateTime WHERE fk_MerchantId IS NULL 
                                                                      AND CorporateUserId IS NULL 
																	  AND fk_ReportID=3	AND IsCreated=1	 
 END
 SELECT 0	
END TRY
--use catch to log error in the table  ProcErrorHandler
BEGIN CATCH
       INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
       SELECT ERROR_NUMBER()
	         ,ERROR_SEVERITY()
			 ,ERROR_STATE()
			 ,ERROR_PROCEDURE()
			 ,ERROR_LINE()
			 ,ERROR_MESSAGE()
			 ,SUSER_SNAME()
			 ,HOST_NAME()
			 ,GETDATE()
       SELECT 1
END CATCH
GO