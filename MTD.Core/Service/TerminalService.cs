﻿using System;
using System.Collections.Generic;

using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Extension;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;
using ResourceData = MTD.Core.Resource.Resource;

namespace MTD.Core.Service
{
    public class TerminalService : ITerminalService
    {
        private readonly ITerminalRepository _terminalRepository;

        public TerminalService([Named("TerminalRepository")] ITerminalRepository terminalRepository)
        {
            _terminalRepository = terminalRepository;
        }

        /// <summary>
        /// Purpose             :   To Add the new ternimal
        /// Function Name       :   Add
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/02/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="terminalDto"></param>
        /// <param name="termId"></param>
        public bool Add(TerminalDto terminalDto, string termId)
        {
            try
            {
                if (_terminalRepository.PaymentDeviceIsExist(terminalDto.Device) && (_terminalRepository.RcTerminalIdIsExist(termId, terminalDto.fk_MerchantID)) && !_terminalRepository.IsMacAddressRegistered(terminalDto.MacAdd))
                {
                    Terminal terminal = terminalDto.ToTerminal();
                    return _terminalRepository.Add(terminal, termId);
                }

                return false;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Terminals ist
        /// Function Name       :   GetTerminalDtoList
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/04/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<TerminalDto> GetTerminalDtoList(int merchantId)
        {
            try
            {
                var terminalList = _terminalRepository.TerminalList(merchantId);
                var driverDtoList = terminalList.ToDTOTerminalList();
                return driverDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get the Terminal
        /// Function Name       :   GetTerminalDto
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/04/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        public TerminalDto GetTerminalDto(int terminalId)
        {
            try
            {
                var terminal = _terminalRepository.GetTerminal(terminalId);
                var terminalDto = terminal.ToTerminalDto();

                return terminalDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update the terminal
        /// Function Name       :   Update
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/05/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="terminalDto"></param>
        /// <param name="terminalId"></param>
        /// <param name="termId"></param>
        public void Update(TerminalDto terminalDto, int terminalId, string termId)
        {
            try
            {
                Terminal terminal = terminalDto.ToTerminal();
                _terminalRepository.Update(terminal, terminalId, termId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Delete the Terminal
        /// Function Name       :   DeleteTerminal
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/05/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        public string DeleteTerminal(int terminalId, string modifiedBy)
        {
            try
            {
                return _terminalRepository.DeleteTerminal(terminalId, modifiedBy);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Terminals List
        /// Function Name       :   TerminalListByFiter
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/06/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="filterSearch"></param>
        /// <returns></returns>
        public IEnumerable<TerminalDto> TerminalListByFiter(FilterSearchParametersDto filterSearch)
        {
            try
            {
                var filterSearchClass = filterSearch.SearchParameters();
                var terminalList = _terminalRepository.TerminalListByFiter(filterSearchClass);
                var driverDtoList = terminalList.ToDTOTerminalList();
                return driverDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Terminal
        /// Function Name       :   GetTerminalDto
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/07/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public TerminalDto GetTerminalDto(string deviceId)
        {
            var terminal = _terminalRepository.GetTerminal(deviceId);
            var terminalDto = terminal.ToTerminalDto();

            return terminalDto;
        }

        /// <summary>
        /// Purpose             :   To Get Terminal,  Stan and Ref
        /// Function Name       :   GetTerminalStanRef
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/07/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="serialNumber"></param>
        /// <returns></returns>
        public DatawireDto GetTerminalStanRef(int merchantId, string serialNumber)
        {
            try
            {
                var terminal = _terminalRepository.GetTerminalStanRef(merchantId, serialNumber);
                var terminalDto = terminal.TodatawireDto();

                return terminalDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To check terminal Registered
        /// Function Name       :   IsRegistered
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/08/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="serialNumber"></param>
        /// <returns></returns>
        public bool IsRegistered(int merchantId, string serialNumber)
        {
            return _terminalRepository.IsRegistered(merchantId, serialNumber);
        }

        /// <summary>
        /// Purpose             :   To Completion the transaction
        /// Function Name       :   Completion
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/11/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="transId"></param>
        /// <param name="serialNo"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public MTDTransactionDto Completion(string transId, string serialNo, int merchantId)
        {
            var compTrans = _terminalRepository.Completion(transId, serialNo, merchantId);
            var transDto = compTrans.ToMtdTransactionDto();

            return transDto;
        }

        /// <summary>
        /// Purpose             :   To Update transaction Stan
        /// Function Name       :   UpdateStan
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/11/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="stan"></param>
        /// <param name="transRefNo"></param>
        /// <param name="serialNo"></param>
        /// <param name="merchantId"></param>
        public void UpdateStan(string stan, string transRefNo, string serialNo, int merchantId)
        {
            _terminalRepository.UpdateStan(stan, transRefNo, serialNo, merchantId);
        }

        /// <summary>
        /// Purpose             :   To check Valid serial number
        /// Function Name       :   IsValid
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/12/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <returns></returns>
        public bool IsValid(string serialNumber)
        {
            bool isValid = _terminalRepository.IsValid(serialNumber);

            return isValid;
        }

        /// <summary>
        /// Purpose             :   To check Valid serial number
        /// Function Name       :   IsValid
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/12/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        public bool IsDatawireRegistered(string serialNumber, string terminalId)
        {
            bool isValid = _terminalRepository.IsDatawireRegistered(serialNumber, terminalId);

            return isValid;
        }

        /// <summary>
        /// Purpose             :   To check rapid connect already exists or not
        /// Function Name       :   IsExist
        /// Created By          :   Naveen Kumar
        /// Created On          :   04/13/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="rcTerminalId"></param>
        /// <returns></returns>
        public bool IsExist(string rcTerminalId)
        {
            return _terminalRepository.IsRcExist(rcTerminalId);
        }

        /// <summary>
        /// Purpose             :   To add rapid connect terminal
        /// Function Name       :   AddRcTerminal
        /// Created By          :   Naveen Kumar
        /// Created On          :   04/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="rapidConnectTerminalDto"></param>
        /// <returns></returns>
        public void AddRcTerminal(RapidConnectTerminalDto rapidConnectTerminalDto)
        {
            Datawire datawire = rapidConnectTerminalDto.ToRcDatawire();
            _terminalRepository.AddRcTerminalId(datawire);
        }

        /// <summary>
        /// Purpose             :   To Get Rc Terminal Id
        /// Function Name       :   GetRcTerminalId
        /// Created By          :   Umesh Kumar
        /// Created On          :   04/14/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        public string GetRcTerminalId(int terminalId)
        {
            return _terminalRepository.GetRcTerminalId(terminalId);
        }

        /// <summary>
        /// Purpose             :   To Get Device Name
        /// Function Name       :   GetRcTerminalId
        /// Created By          :   Umesh Kumar
        /// Created On          :   05/01/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public DeviceNameListDto GetDevice(int deviceId)
        {
            var device = _terminalRepository.GetDevice(deviceId);
            var deviceDto = device.ToPaymentDeviceDto();

            return deviceDto;
        }

        /// <summary>
        /// Purpose             :   To Get Device List
        /// Function Name       :   GetRcTerminalId
        /// Created By          :   Umesh Kumar
        /// Created On          :   05/08/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DeviceNameListDto> GetDeviceList()
        {
            var deviceList = _terminalRepository.GetDeviceList();
            var deviceListDto = deviceList.ToDtoDeviceDto();

            return deviceListDto;
        }

        /// <summary>
        /// Purpose             :   To Add Device name
        /// Function Name       :   AddDevicename
        /// Created By          :   Salil Gupta
        /// Created On          :   05/08/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="deviceNameDto"></param>
        /// <returns></returns>
        public bool AddDevicename(DeviceNameListDto deviceNameDto)
        {
            var paymentDevice = deviceNameDto.ToPaymentDevice();

            return _terminalRepository.AddDevicename(paymentDevice);
        }

        /// <summary>
        /// Purpose             :   To Update Device name
        /// Function Name       :   AddDevicename
        /// Created By          :   Salil Gupta
        /// Created On          :   05/08/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="deviceNameDto"></param>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public void UpdateDevicename(DeviceNameListDto deviceNameDto, int deviceId)
        {
            var paymentDevice = deviceNameDto.ToPaymentDevice();
            _terminalRepository.UpdateDevicename(paymentDevice, deviceId);
        }

        /// <summary>
        /// Purpose             :   To Update Device name
        /// Function Name       :   AddDevicename
        /// Created By          :   Salil Gupta
        /// Created On          :   05/08/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public bool DeleteDevicename(int deviceId)
        {
            return _terminalRepository.DeleteDevicename(deviceId);
        }

        /// <summary>
        /// Purpose             :   To Device List
        /// Function Name       :   DeviceListByFiter
        /// Created By          :   Umesh Kumar
        /// Created On          :   05/22/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="index"></param>
        /// <param name="pageSize"></param>
        /// <param name="sorting"></param>
        /// <returns></returns>
        public IEnumerable<DeviceNameListDto> DeviceListByFiter(string name, int index, int pageSize, string sorting)
        {
            var deviceList = _terminalRepository.DeviceListByFiter(name, index, pageSize, sorting);
            var deviceListDto = deviceList.ToDtoDeviceDto();

            return deviceListDto;
        }

        /// <summary>
        /// Purpose             :   To Add Device List
        /// Function Name       :   AddDeviceList
        /// Created By          :   Ravit
        /// Created On          :   06/22/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="index"></param>
        /// <param name="pageSize"></param>
        /// <param name="sorting"></param>
        /// <returns></returns>
        public List<TerminalExcelUploadDto> AddDeviceList(List<TerminalExcelUploadDto> objTerminalDto)
        {
            try
            {
                string _Error = string.Empty;
                List<TerminalExcelUploadDto> terminalExcelUpload = new List<TerminalExcelUploadDto>();

                int response = -1;
                foreach (var item in objTerminalDto)
                {
                    var terminalEX = item.toTerminalExcel();
                    response = _terminalRepository.IsValidTerminalExcel(terminalEX);

                    switch (response)
                    {
                        case 2:
                            _Error = ResourceData.Device_not_exist; // "Device does not exists.";
                            break;
                        case 3:
                            _Error = ResourceData.RCTerminal_already_assigned; // "Rapid Connect terminal is already assigned.";
                            break;
                        case 4:
                            _Error = ResourceData.DatawireId_already_exist; // "DatawireId is already exists.";
                            break;
                        case 5:
                            _Error = ResourceData.Device_name_required; // "Device name is required.";
                            break;
                        case 6:
                            _Error = ResourceData.Serial_number_required; // "Serial number is required.";
                            break;
                        case 7:
                            _Error = ResourceData.Terminal_required; // "Terminal is required.";
                            break;
                        case 8:
                            _Error = ResourceData.Mac_address_required; // "Mac Address is required.";
                            break;
                        case 9:
                            _Error = ResourceData.Mac_alreay_exist; // "Mac No. is alreay exist.";
                            break;
                        case 11:
                            _Error = ResourceData.Datawire_not_registered; // "Datawire ID is not registered with this terminal.";
                            break;
                        case 12:
                            _Error = ResourceData.Description_max_length; // "Description No must not exceed the length of 200 characters";
                            break;
                        case 13:
                            _Error = ResourceData.Device_name_max_length; // "Device Name must not exceed the length of 100 characters";
                            break;
                        case 14:
                            _Error = ResourceData.Invalid_Serial_No; // "Invalid Serial No";
                            break;
                        case 15:
                            _Error = ResourceData.Invalid_RCTerminal; // "Invalid Rapid Terminal Connect Id";
                            break;
                        case 16:
                            _Error = ResourceData.Invalid_Mac_Address; // "Invalid Mac Address";
                            break;
                        case 17:
                            _Error = ResourceData.Serial_no_max_length; // "Serial No must not exceed the length of 50 characters";
                            break;
                        case 18:
                            _Error = ResourceData.Mac_address_max_length; // "Mac Address must not exceed the length of 100 characters";
                            break;
                    }

                    if (response == 0)
                    {
                        TerminalExcelUpload terminal = item.ToTerminalDevice();
                        _terminalRepository.AddTermialDevice(terminal);
                    }
                    else
                    {
                        terminalExcelUpload.Add(new TerminalExcelUploadDto() { DeviceName = item.DeviceName, SerialNo = item.SerialNo, TermId = item.TermId, MacAdd = item.MacAdd, Description = item.Description, FailedError = _Error.ToString() });
                    }
                }

                return terminalExcelUpload;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterSearch"></param>
        /// <returns></returns>
        public IEnumerable<TerminalDto> GetUserTerminal(FilterSearchParametersDto filterSearch)
        {
            try
            {
                var userterminalList = _terminalRepository.GetUserTerminal(filterSearch.SearchParameters());
                var terminalList = userterminalList.ToDTOTerminalList();

                return terminalList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterSearch"></param>
        /// <returns></returns>
        public int GetUserTerminalCount(FilterSearchParametersDto filterSearch)
        {
            try
            {
                return _terminalRepository.GetUserTerminalCount(filterSearch.SearchParameters());
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterSearch"></param>
        /// <returns></returns>
        public int GetTerminalCount(FilterSearchParametersDto filterSearch)
        {
            try
            {
                return _terminalRepository.GetTerminalCount(filterSearch.SearchParameters());
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public int GetDeviceListCount(string name)
        {
            try
            {
                return _terminalRepository.GetDeviceListCount(name);
            }
            catch
            {
                throw;
            }
        }

        public bool IsMacAddReg(int terminalId, string macAdd)
        {
            try
            {
                return _terminalRepository.IsMacAddReg(terminalId, macAdd);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To update Cap Key version in terminal for device.
        /// Function Name       :   UpdateCapVersion
        /// Created By          :   Salil Gupta
        /// Created On          :   02-Feb-2016
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="macAdd"></param>
        /// <returns></returns>
        public bool UpdateCapVersion(string macAdd)
        {
            try
            {
                return _terminalRepository.UpdateCapVersion(macAdd);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void SaveServiceDiscoveryUrls(ServiceDiscoveryParametersDto serviceDiscoveryDto)
        {
            var serviceData = serviceDiscoveryDto.ToServiceDiscovery();
            _terminalRepository.SaveServiceDiscoveryUrls(serviceData);
        }

        public List<string> GetServiceDiscoveryUrls()
        {
            return _terminalRepository.GetServiceDiscoveryUrls();
        }

        public DatawireDto GetRandomMerchantDetail()
        {
            var datawire = _terminalRepository.GetRandomMerchantDetail();
            return datawire.TodatawireDto();
        }

        public fDMerchantDto GetFDMerchant(int merchantId)
        {
            var fdMerchant = _terminalRepository.GetFDMerchant(merchantId);
            return fdMerchant.TofdMerchantDto();
        }

        public List<string> SaveServiceProviderUrls(ServiceProviderUrlDto serviceProviderUrls)
        {
            var serviceProviderData = serviceProviderUrls.ToServiceProvider();

            return _terminalRepository.SaveServiceProviderUrls(serviceProviderData);
        }

        public bool IsClsCapKey(int terminalId)
        {
            return _terminalRepository.IsClsCapKey(terminalId);
        }
    }
}
