﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class TableSP_Reversal
    {
        #region Private Constants and Variables
        #region Constants
        private const string tblId = "|53|50";

        #endregion

        #region Variables

        private const string securitylevelTag01 = "|30|31";
        private string securitylevelTagLength = "|30|30|31";
        private const string securitylevelTag01Vlaue = "|33";

        private const string tag06 = "|30|36";
        private const string tag06Length = "|30|30|34";
        private string tag06Value = "|53|36|37|35";//"|S6|75";

        private const string tag07 = "|30|37";
        private string tag07Length = "|30|31|36";
        private string tag07Value = "|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20";

        #endregion
        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'36 30' Table 60
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        /// <summary>
        /// Gets or Sets the value for DCC Conversion Indicator
        /// </summary>
        public string SecurityLevelTag01
        {
            get { return securitylevelTag01; }

        }
        public string SecuritylevelTagLength
        {
            get { return securitylevelTagLength; }

        }


        /// <summary>
        /// Gets or Sets the value for DCC Transaction Time Zone
        /// </summary>
        public string SecuritylevelTag01Vlaue
        {
            get { return securitylevelTag01Vlaue; }

        }

        /// <summary>
        /// Gets or Sets the value for Foreign Conversion Rate
        /// </summary>
        public string Tag06
        {
            get { return tag06; }

        }


        public string Tag06Length
        {
            get { return tag06Length; }

        }


        public string Tag06Value
        {
            get { return tag06Value; }
            set { tag06Value = value; }
        }



        public string Tag07
        {
            get { return tag07; }

        }



        public string Tag07Length
        {
            get { return tag07Length; }
            set { tag07Length = value; }
        }
        /// <summary>
        /// Gets or Sets the value for Amount in Merchant Base Currency
        /// </summary>
        public string Tag07Value
        {
            get { return tag07Value; }
            set { tag07Value = value; }
        }



        #endregion
    }
}