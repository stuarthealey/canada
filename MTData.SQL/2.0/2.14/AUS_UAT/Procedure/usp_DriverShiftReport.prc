IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DriverShiftReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_DriverShiftReport]
GO

/****** Object:  StoredProcedure [dbo].[usp_DriverShiftReport]    Script Date: 11/2/2015 10:59:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <07-07-2015>
-- Description:	<Send shift report to driver at logout from dispatch system and android device>
-- =============================================
CREATE PROCEDURE [dbo].[usp_DriverShiftReport]
(
  -- Add the parameters for the stored procedure here
  @DriverId INT,
  @IsDispatchRequest BIT
)
AS
BEGIN TRY
  -- SET NOCOUNT ON added to prevent extra result sets from
  SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;
  DECLARE 
  @Valid INT=1,
  @FleetId INT,
  @LogInDateTime DATETIME,
  @LogOutDateTime DATETIME,
  @XmlData NVARCHAR(MAX),
  @DriverNo NVARCHAR(80),
  @DriverName NVARCHAR(50),
  @CarNumber NVARCHAR(50),
  @DriverEmail NVARCHAR(50),
  @CarOwnerUserId INT,
  @CarOwnerEmail NVARCHAR(50),
  @CarOwnerName NVARCHAR(60),
  @Body NVARCHAR(MAX),
  @DriverBody NVARCHAR(MAX),
  @OwnerBody NVARCHAR(MAX),
  @MsgXml NVARCHAR(60),
  @IsOwnerRecipient BIT,
  @PayType TINYINT,
  @Res INT,
  @DriverFirstName NVARCHAR(30),
  @MailId INT,
  @Error NVARCHAR(200),
  @TotalFareValue VARCHAR(30),
  @TotalTip NVARCHAR(30),
  @TotalTax NVARCHAR(30),
  @TotalAmt NVARCHAR(30),
  @TotalAmtForTechFee NVARCHAR(30),
  @TotalFee NVARCHAR(30),
  @TotalFleetFee NVARCHAR(30),
  @NetAmt NVARCHAR(30),
  @BodyPart NVARCHAR(MAX),
  @AddHtml NVARCHAR(500)
  DECLARE @TableData TABLE
    (
	   Id INT,
	   SourceId NVARCHAR(50),
	   JobNumber  NVARCHAR(30),
	   PickAddress NVARCHAR(100),
	   DestinationAddress NVARCHAR(100),
	   FareValue MONEY,
	   Taxes MONEY,
	   Fee MONEY,
	   Tip MONEY,
	   Amount MONEY,
	   TechFee MONEY,
	   FleetFee MONEY,		 
	   TxnType NVARCHAR(30),
	   VehicleNo NVARCHAR(60),
	   IsRefunded BIT,
	   IsVoided BIT,
	   Surcharge DECIMAL(18,2)
    )
  SELECT @DriverNo=DriverNo,@DriverName=CONCAT(FName,' ',LNAME),@DriverEmail=Email,@DriverFirstName=FName,@FleetId=fk_FleetID FROM dbo.Driver WHERE DriverID=@DriverId
  IF EXISTS(SELECT 1 FROM dbo.DriverReportRecipient WHERE fk_DriverId=@DriverId AND IsRecipient=1 AND fk_FleetId=@FleetId AND IsDaily=1)
  BEGIN
    SELECT @LogInDateTime=LogInDateTime,
	       @LogOutDateTime=LogOutDateTime 
	FROM dbo.DriverLogin 
	WHERE DriverNo=@DriverNo 
	      AND FleetId=@FleetId 
		  AND IsDispatchRequest=@IsDispatchRequest
	INSERT INTO @TableData
    SELECT  Id
	       ,SourceId
	       ,JobNumber
           ,PickAddress
           ,DestinationAddress
		   ,FareValue
		   ,Taxes
		   ,Fee
		   ,Tip
		   ,Amount
		   ,TechFee
		   ,FleetFee		
		   ,TxnType
		   ,VehicleNo
		   ,IsRefunded
		   ,IsVoided
		   ,Surcharge
   FROM dbo.MTDTransaction 
   WHERE TxnDate>=@LogInDateTime 
         AND TxnDate<=@LogOutDateTime 
		 AND DriverNo=@DriverNo
		 AND fk_FleetId=@FleetId
		 AND ResponseCode IN ('000','002')
    SET @CarNumber=(SELECT TOP 1 VehicleNo FROM @TableData)	              
	SET @XmlData =  COALESCE(CAST( (
	                SELECT   ROW_NUMBER() OVER (ORDER BY tblSaleCompletion.JobNumber) AS 'td','',
					COALESCE(tblSaleCompletion.JobNumber,'') AS 'td','',
				    COALESCE(tblSaleCompletion.PickAddress,'') AS 'td','',
					COALESCE(tblSaleCompletion.DestinationAddress,'') AS 'td','',
					COALESCE(tblSaleCompletion.TxnType,'') AS 'td','',
					CONCAT('$',CAST(COALESCE(tblSaleCompletion.FareValue,0.00) AS DECIMAL(18,2))) AS 'td','',
					CONCAT('$',CAST(COALESCE(tblSaleCompletion.Taxes,0.00) AS DECIMAL(18,2))) AS 'td','',
					CONCAT('$',CAST(COALESCE(tblSaleCompletion.Tip,0.00) AS DECIMAL(18,2))) AS 'td','',
					CONCAT('$',CAST(COALESCE(tblSaleCompletion.Amount,0.00)-COALESCE(tblSaleCompletion.Surcharge,0.00)  AS DECIMAL(18,2))) AS 'td','',
					CONCAT('$',CAST(COALESCE(tblSaleCompletion.FleetFee,0.00)+COALESCE(tblSaleCompletion.Fee,0.00) AS DECIMAL(18,2))) AS 'td','',
					CONCAT('$',CAST(COALESCE(tblSaleCompletion.Amount,0.00)-COALESCE(tblSaleCompletion.FleetFee,0.00)-COALESCE(tblSaleCompletion.Fee,0.00)-COALESCE(tblSaleCompletion.Surcharge,0.00) AS DECIMAL(18,2))) AS 'td'										
		    FROM
              (
	         SELECT    COALESCE(JobNumber,'') AS 'JobNumber' ,
	                   COALESCE(PickAddress,'') AS 'PickAddress', 
				       COALESCE(DestinationAddress,'') AS 'DestinationAddress',	
					   COALESCE(TxnType,'') AS 'TxnType',																	 
	                   COALESCE(FareValue,0.00) AS 'FareValue',
					   COALESCE(Taxes,0.00) AS 'Taxes',
					   COALESCE(Fee,0.00) AS 'Fee',
	                   COALESCE(Tip,0.00) AS 'Tip',   
					   COALESCE(Amount,0.00) AS 'Amount',
					   COALESCE(FleetFee,0.00) AS 'FleetFee',
					   COALESCE(Surcharge,0.00) AS 'Surcharge'                                                                  
	            FROM @TableData 								 													 								
			    WHERE  TxnType='Sale'	
				       AND IsRefunded=0
					   AND IsVoided=0
				UNION ALL
				SELECT  COALESCE(tblId.JobNumber,''),
	                    COALESCE(tblId.PickAddress,''),
				        COALESCE(tblId.DestinationAddress,''),	
						COALESCE(tblSrc.TxnType,''),																	 
	                    COALESCE(tblId.FareValue,0.00),
						COALESCE(tblId.Taxes,0.00),
						COALESCE(tblId.Fee,0.00),
	                    COALESCE(tblId.Tip,0.00), 
					    COALESCE(tblSrc.Amount,0.00),
						COALESCE(tblId.FleetFee,0.00),
						COALESCE(tblId.Surcharge,0.00)                                                             
	             FROM @TableData tblSrc 
				 INNER JOIN @TableData tblId
				 ON tblSrc.SourceId=tblId.Id						 								
				 WHERE  tblSrc.TxnType='Completion'
			  ) tblSaleCompletion	
			  FOR XML PATH('tr'), ELEMENTS) AS NVARCHAR(MAX)),'')	
     IF(@XmlData IS NULL OR @XmlData='') 
     BEGIN 
         SET @Valid=0
         INSERT INTO dbo.MailMessage(RepUserId,[Status],SendTo,Body,[Date],ReportType,RepUserTypeId) 
		 VALUES(@DriverId,-1,@DriverEmail,'No records in database',GETDATE(),'DriverShiftReport',4)
         SELECT 2
     END
    IF(@Valid=1) 
    BEGIN
       SELECT @TotalFareValue=CONCAT('$',CAST(COALESCE(SUM(TblTotal.FareValue),0.00) AS DECIMAL(18,2))),
              @TotalTip=CONCAT('$',CAST(COALESCE(SUM(TblTotal.Tip),0.00) AS DECIMAL(18,2))),
			  @TotalTax=CONCAT('$',CAST(COALESCE(SUM(TblTotal.Taxes),0.00) AS DECIMAL(18,2))),		
			  @TotalFee=CONCAT('$',CAST(COALESCE(SUM(TblTotal.Fee),0.00)+COALESCE(SUM(TblTotal.FleetFee),0.00) AS DECIMAL(18,2))),
			  @TotalAmt=CONCAT('$',CAST(COALESCE(SUM(TblTotal.Amount),0.00)-COALESCE(SUM(TblTotal.Surcharge),0.00) AS DECIMAL(18,2))),
			  @NetAmt=CONCAT('$',CAST((COALESCE(SUM(TblTotal.Amount),0.00)-COALESCE(SUM(TblTotal.Fee),0.00)-COALESCE(SUM(TblTotal.FleetFee),0.00)-COALESCE(SUM(TblTotal.Surcharge),0.00)) AS DECIMAL(18,2)))
		 FROM
		 (
		 SELECT COALESCE(SUM(FareValue),0.00) 'FareValue'
		       ,COALESCE(SUM(Tip),0.00) 'Tip'
			   ,COALESCE(SUM(Taxes),0.00) AS 'Taxes'
			   ,COALESCE(SUM(Fee),0.00) 'Fee'
			   ,COALESCE(SUM(Amount),0.00) AS 'Amount'			   
			   ,COALESCE(SUM(FleetFee),0.00) AS 'FleetFee'
			   ,COALESCE(SUM(Surcharge),0.00) AS 'Surcharge'
		  FROM @TableData 								 													 								
		  WHERE  TxnType='Sale'
		         AND IsRefunded=0
				 AND IsVoided=0
		  UNION ALL 
		  SELECT COALESCE(SUM(tblId.FareValue),0.00)
		  ,COALESCE(SUM(tblId.Tip),0.00)
		  ,COALESCE(SUM(tblId.Taxes),0.00)
		  ,COALESCE(SUM(tblId.Fee),0.00)
		  ,COALESCE(SUM(tblSrc.Amount),0.00)
		  ,COALESCE(SUM(tblId.FleetFee),0.00) 
		  ,COALESCE(SUM(tblId.Surcharge),0.00) AS 'Surcharge'
		  FROM @TableData tblSrc 
		  INNER JOIN @TableData tblId
		  ON tblSrc.SourceId=tblId.Id						 								
		  WHERE  tblSrc.TxnType='Completion'	
		 )TblTotal
		SELECT @CarOwnerUserId=co.Id,
		       @CarOwnerEmail=co.EmailAddress,
			   @CarOwnerName=co.OwnerName 
		FROM dbo.CarOwners co 
		WHERE co.Id=(SELECT TOP 1 v.fk_CarOwenerId 
		             FROM dbo.Vehicle v 
					 WHERE v.VehicleNumber=@CarNumber 
					       AND v.fk_FleetID=@FleetId 
						   AND v.IsActive=1)
		      AND  co.IsActive=1
		SET @IsOwnerRecipient=(SELECT TOP 1 IsOwnerRecipient 
		                       FROM dbo.DriverReportRecipient 
							   WHERE fk_DriverId=@DriverId 
							         AND fk_FleetId=@FleetId)
        SET @Body ='<html><style>td{text-align:left;font-family:verdana;font-size:12px}
        p{font-family:verdana;font-size:13px}span{font-family:verdana;font-size:13px}</style>
        <body><p>Listed below is the driver transaction report containing the transaction details done in the below mentioned shift.</p>
        <table><tr><td style="color:black;text-align:left;font-size:11px;font-weight:bold;font-family:verdana
		;background-color:lightgrey">Driver Name :'+@DriverName+'<td>&nbsp;
        <td style="color:black;text-align:left;font-size:11px;font-weight:bold;font-family:verdana
		;background-color:lightgrey">Car Number :'+@CarNumber+'<td>&nbsp;
        <td style="color:black;text-align:left;font-size:11px;font-weight:bold;font-family:verdana
		;background-color:lightgrey">Shift from '+CONVERT(VARCHAR(17),@LogInDateTime, 113)+' To '
		+CONVERT(VARCHAR(17),@LogOutDateTime, 113)+'<td>&nbsp;</tr></table><br/>
        <table border = 1 cellpadding="0" cellspacing="0" style="background-color:white;border-collapse:collapse"> <tr>
        <th style="color:grey;font-size:13px;width:220px;text-align:left">Seq No</th>
        <th style="color:grey;font-size:13px;width:400px;text-align:left">MTData Job No</th>
        <th style="color:grey;font-size:13px;width:500px;text-align:left">Pickup Address</th>
        <th style="color:grey;font-size:13px;width:500px;text-align:left">Destination Address</th>
		<th style="color:grey;font-size:13px;width:140px;text-align:left">Trans Type</th>
        <th style="color:grey;font-size:13px;width:140px;text-align:left">Fare Amt</th>
		<th style="color:grey;font-size:13px;width:150px;text-align:left">Tax</th>
        <th style="color:grey;font-size:13px;width:150px;text-align:left">Tip</th>      
        <th style="color:grey;font-size:13px;width:150px;text-align:left">Total Amt</th>
		<th style="color:grey;font-size:13px;width:150px;text-align:left">Fees</th>
		<th style="color:grey;font-size:13px;width:150px;text-align:left">Amount Payable</th></tr>' 
        SET @Body =@Body + @XmlData +'<tfoot>
		<tr style="height:20px;font-weight:bold">
		<td colspan=5 style="font-size:11px;text-align:center">Total </td>
		<td style="font-size:11px;text-align:left">'+@TotalFareValue+'</td>
		<td style="font-size:11px;text-align:left">'+@TotalTax+'</td>
		<td style="font-size:11px;text-align:left">'+@TotalTip+'</td>
		<td style="font-size:11px;text-align:left">'+@TotalAmt+'</td>
		<td style="font-size:11px;text-align:left">'+@TotalFee+'</td>
		<td style="font-size:11px;text-align:left">'+@NetAmt+'</td>'			
	    SET @Body=@Body+'</tr></tfoot></table><br/><br/>
        <span>Thanks & Regards,<br/><br/>MTData Developments Pty Ltd.<br/>www.mtdata.com<span>
        <h4 style="color:grey;font-size:12px;font-family:verdana">MTData-All rights reserved</h4>
        <p style="font-size:11px;color:black">Note:This is an automated message. Do not Reply or Reply All
        to this message as replies are not being accepted.</p> </body></html>'     
        INSERT INTO dbo.MailMessage(RepUserId,[Status],SendTo,Body,[RepUserTypeId],[Date],[ReportType]) 
		VALUES(@DriverId,0,@DriverEmail,@Body,4,GETDATE(),'DriverShiftReport')
		IF(@@SERVERNAME='DATA-PROD\SQL2014')
		BEGIN
		SELECT 0
		END
        SET @MailId=SCOPE_IDENTITY()
		IF(@DriverEmail IS NOT NULL)
		BEGIN
		   SET @MsgXml=CAST((SELECT CONCAT('Dear',' ',@DriverFirstName,',') AS 'p' FOR XML PATH('p'), ELEMENTS ) AS NVARCHAR(MAX))
		   SET @DriverBody=@MsgXml+@Body
		   EXEC @Res=msdb.dbo.sp_send_dbmail
           @profile_name = 'MTData',
           @recipients =@DriverEmail,
           @body =@DriverBody,
           @subject = 'Driver Shift Report',
           @body_format ='HTML'
           IF (@Res != 0)
           BEGIN
              SET @Error =@@ERROR
              UPDATE dbo.MailMessage set Error=@Error,[Date]=GETDATE() where MailId=@MailId
		      SELECT 4
           END;
           ELSE
           BEGIN
              UPDATE dbo.MailMessage set [Status]=1,[Date]=GETDATE() where MailId=@MailId
			  SELECT 0
           END
		END
		IF(@CarOwnerEmail IS NOT NULL AND @IsOwnerRecipient=1)
		BEGIN
		   SET @MsgXml=CAST((SELECT CONCAT('Dear',' ',@CarOwnerName,',') AS 'p' FOR XML PATH('p'), ELEMENTS ) AS NVARCHAR(MAX))
		   SET @OwnerBody=@MsgXml+@Body
		   EXEC @Res=msdb.dbo.sp_send_dbmail
           @profile_name = 'MTData',
           @recipients =@CarOwnerEmail,
           @body =@OwnerBody,
           @subject = 'Driver Shift Report',
           @body_format ='HTML'
           IF (@Res != 0)
           BEGIN
              SET @Error =@@ERROR
              INSERT INTO dbo.MailMessage(RepUserId,[Status],SendTo,Body,[RepUserTypeId],[Date],[ReportType],Error) 
			  VALUES(@CarOwnerUserId,0,@CarOwnerEmail,@Body,5,GETDATE(),'DriverShiftReport',@Error)
		      SELECT 4
           END;
           ELSE
           BEGIN
              INSERT INTO dbo.MailMessage(RepUserId,[Status],SendTo,Body,[RepUserTypeId],[Date],[ReportType]) 
			  VALUES(@CarOwnerUserId,1,@CarOwnerEmail,@Body,5,GETDATE(),'DriverShiftReport')
			  SELECT 0
           END
		END
		
      END
   END
   ELSE
   BEGIN
       SELECT 3
   END
END TRY
--use catch to log error in the table  ProcErrorHandler
BEGIN CATCH
      INSERT INTO dbo.ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
      SELECT ERROR_NUMBER()
	        ,ERROR_SEVERITY()
			,ERROR_STATE()
			,ERROR_PROCEDURE()
			,ERROR_LINE()
			,ERROR_MESSAGE()
			,SUSER_SNAME()
			,HOST_NAME()
			,GETDATE()
END CATCH







