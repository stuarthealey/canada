﻿using System;

namespace MTD.Data.Data
{
  public  class MerchantRecipient
    {

        public int fk_MerchantID { get; set; }
        public Nullable<int> fk_UserID { get; set; }
        public string Company { get; set; }
        public bool IsRecipient { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int fk_ReportID { get; set; }
    }
}
