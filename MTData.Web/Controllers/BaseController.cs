﻿using System.Web.Mvc;
using MTD.Core.Factory.Interface;
using MTData.Web.ViewData;

namespace MTData.Web.Controllers
{
    public class BaseController : Controller
    {
        /// <summary>
        /// Purpose             :   To authenticate the user
        /// Function Name       :   CreateViewData
        /// Created By          :   Umesh Kumar
        /// Created On          :   26/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public T CreateViewData<T>() where T : BaseViewData, new()
        {
            T viewData = new T
            {
                SiteTitle =Setting.SiteTitle,
                RootUrl = Setting.RootUrl,
                IsCurrentUserAuthenticated=this.IsCurrentUserAuthenticated,
                ReturnUrl=this.ReturnUrl,
            };
            return viewData;
        }
        /// <summary>
        /// Purpose             :   To authenticate the current user
        /// Function Name       :   IsCurrentUserAuthenticated
        /// Created By          :   Umesh Kumar
        /// Created On          :   26/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public bool IsCurrentUserAuthenticated
        {
            get
            {
                return (HttpContext.User.Identity.IsAuthenticated && (CurrentUser != null));
            }
        }
        public ISystemSetting Setting { get; set; }
        public string ReturnUrl { get; set; }
        public UserViewData CurrentUser { get; set; }
    }
}