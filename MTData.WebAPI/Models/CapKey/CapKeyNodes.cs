﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Models
{
    public class CapKeyNodes
    {
        public string x9F06 { get; set; }
        public string x9F22 { get; set; }
        public string x9F8123 { get; set; }
        public string x9F8122 { get; set; }
        public string x9F8121 { get; set; }

        //cless
        public string x9F918704 { get; set; }
        public string x9F918703 { get; set; }
        public string x9F918702 { get; set; }

        /// <summary>
        /// Concatenated x9F06 and x9F22 to create a search key
        /// </summary>
        public string SearchKey { get; set; }

        public int Counter { get; set; }
    }
}