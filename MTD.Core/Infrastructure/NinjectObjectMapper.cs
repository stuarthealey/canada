﻿using System.Web;
using Ninject.Modules;

using MTD.Core.Factory;
using MTD.Core.Service;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository;
using MTD.Data.Repository.Interface;
using MTD.Core.Factory.Interface;

namespace MTD.Core.Infrastructure
{
    public class NinjectObjectMapper : NinjectModule
    {
        public override void Load()
        {
            Bind<IMtDataDevlopmentsEntities>().To<MTDataDevlopmentsEntities>()
                .InTransientScope().Named("MtDataDevlopments");

            Bind<IMerchantRepository>().To<MerchantRepository>() // Map IMerchantRepository to MerchantRepository  
                .InScope(ctx => HttpContext.Current).Named("MerchantRepository");

            Bind<IMerchantFactory>().To<MerchantFactory>()  // Map IMerchantFactory to MerchantFactory  
                .InScope(ctx => HttpContext.Current).Named("MerchantFactory");

            Bind<IMerchantService>().To<MerchantService>() // Map IMerchantService to MerchantService  
                .InScope(ctx => HttpContext.Current).Named("MerchantService");

            Bind<IMemberRepository>().To<MemberRepository>() // Map IMerchantRepository to MerchantRepository  
                .InScope(ctx => HttpContext.Current).Named("LoginRepository");

            Bind<IMemberFactory>().To<MemberFactory>()  // Map IMerchantFactory to MerchantFactory  
                .InScope(ctx => HttpContext.Current).Named("MemberFactory");

            Bind<IMemberService>().To<MemberService>() // Map IMerchantService to MerchantService  
                .InScope(ctx => HttpContext.Current).Named("LoginService");

            Bind<IAdminUserRepository>().To<AdminUserRepository>() // Map IAdminUserRepository to AdminUserRepository  
                .InScope(ctx => HttpContext.Current).Named("AdminUserRepository");

            Bind<IAdminUserFactory>().To<AdminUserFactory>()  // Map IAdminUserFactory to AdminUserFactory  
                .InScope(ctx => HttpContext.Current).Named("AdminUserFactory");

            Bind<IAdminUserService>().To<AdminUserService>() // Map IAdminUserService to AdminUserService  
                .InScope(ctx => HttpContext.Current).Named("AdminUserService");

            Bind<IGatewayReporsitory>().To<GatewayReporsitory>() // Map IAdminUserRepository to AdminUserRepository  
                .InScope(ctx => HttpContext.Current).Named("GatewayRepository");

            Bind<IGatewayService>().To<GatewayService>() // Map IAdminUserService to AdminUserService  
                .InScope(ctx => HttpContext.Current).Named("GatewayService");

            Bind<IUserRepository>().To<UserRepository>() // Map IUserRepository to UserRepository  
                .InScope(ctx => HttpContext.Current).Named("UserRepository");

            Bind<IUserFactory>().To<UserFactory>()  // Map IUserFactory to UserFactory  
                .InSingletonScope().Named("UserFactory");

            Bind<IUserService>().To<UserService>() // Map IUserService to UserService  
                .InScope(ctx => HttpContext.Current).Named("UserService");

            Bind<IFleetRepository>().To<FleetRepository>() // Map IFleetRepository to FleetRepository  
               .InScope(ctx => HttpContext.Current).Named("FleetRepository");

            Bind<IFleetFactory>().To<FleetFactory>()  // Map IFleetFactory to FleetFactory  
                .InSingletonScope().Named("FleetFactory");

            Bind<IFleetService>().To<FleetService>() // Map IFleetService to FleetService  
                .InScope(ctx => HttpContext.Current).Named("FleetService");

            Bind<IDriverRepository>().To<DriverRepository>()
                .InTransientScope().Named("DriverRepository");

            Bind<IDriverService>().To<DriverService>()
                .InTransientScope().Named("DriverService");

            Bind<ITerminalRepository>().To<TerminalRepository>()
                .InScope(ctx => HttpContext.Current).Named("TerminalRepository");

            Bind<ITerminalService>().To<TerminalService>()
                .InScope(ctx => HttpContext.Current).Named("TerminalService");

            Bind<ICommonRepository>().To<CommonRepository>()
                .InScope(ctx => HttpContext.Current).Named("CommonRepository");

            Bind<ICommonService>().To<CommonService>()
                .InScope(ctx => HttpContext.Current).Named("CommonService");

            Bind<IVehicleRepository>().To<VehicleRepository>()
                .InScope(ctx => HttpContext.Current).Named("VehicleRepository");

            Bind<IVehicleService>().To<VehicleService>()
                .InScope(ctx => HttpContext.Current).Named("VehicleService");
           
            Bind<ITaxRepository>().To<TaxRepository>()
               .InScope(ctx => HttpContext.Current).Named("TaxRepository");

            Bind<ITaxService>().To<TaxService>()
                .InScope(ctx => HttpContext.Current).Named("TaxService");

            Bind<ISurchargeRepository>().To<SurchargeRepository>()
                .InScope(ctx => HttpContext.Current).Named("SurchargeRepository");

            Bind<ISurchargeService>().To<SurchargeService>()
                .InScope(ctx => HttpContext.Current).Named("SurchargeService");

            Bind<ITransactionRepository>().To<TransactionRepository>() // Map ITransactionRepository to TransactionRepository  
                .InScope(ctx => HttpContext.Current).Named("TransactionRepository");

            Bind<ITransactionFactory>().To<TransactionFactory>()  // Map ITransactionFactory to TransactionFactory  
                .InSingletonScope().Named("TransactionFactory");

            Bind<ITransactionService>().To<TransactionService>() // Map ITransactionService to TransactionService  
                .InScope(ctx => HttpContext.Current).Named("TransactionService");

            Bind<IMerchantGatewayRepository>().To<MerchantGatewayRepository>()
                .InScope(ctx => HttpContext.Current).Named("MerchantGatewayRepository");

            Bind<IMerchantGatewayService>().To<MerchantGatewayService>()
               .InScope(ctx => HttpContext.Current).Named("MerchantGatewayService");

            Bind<IDefaultRepository>().To<DefaultRepository>() //
                .InScope(ctx => HttpContext.Current).Named("DefaultRepository");

            Bind<IDefaultService>().To<DefaultService>() // 
                .InScope(ctx => HttpContext.Current).Named("DefaultService");

            Bind<IPrefixRepository>().To<PrefixRepository>() //
                .InScope(ctx => HttpContext.Current).Named("PrefixRepository");

            Bind<IPrefixService>().To<PrefixService>() // 
                .InScope(ctx => HttpContext.Current).Named("PrefixService");

            Bind<IReportCommonRepository>().To<ReportCommonRepository>()
                .InScope(ctx => HttpContext.Current).Named("ReportCommonRepository");

            Bind<IReportCommonService>().To<ReportCommonService>()
                .InScope(ctx => HttpContext.Current).Named("ReportCommonService");

            Bind<IReceiptRepository>().To<ReceiptRepository>() //
                .InScope(ctx => HttpContext.Current).Named("ReceiptRepository");

            Bind<IReceiptService>().To<ReceiptService>() // 
                .InScope(ctx => HttpContext.Current).Named("ReceiptService");

            Bind<IReceiptFactory>().To<ReceiptFactory>()  // Map IUserFactory to UserFactory  
                .InScope(ctx => HttpContext.Current).Named("ReceiptFactory");

            Bind<IScheduleRepository>().To<ScheduleRepository>() //
                .InScope(ctx => HttpContext.Current).Named("ScheduleRepository");

            Bind<IScheduleService>().To<ScheduleService>() // 
                .InScope(ctx => HttpContext.Current).Named("ScheduleService");

            Bind<ICarOwnersRepository>().To<CarOwnerRepository>()
                .InScope(ctx => HttpContext.Current).Named("CarOwnersRepository");

            Bind<ICarOwnersService>().To<CarOwnersService>()
                .InScope(ctx => HttpContext.Current).Named("CarOwnersService");

            Bind<ISubAdminRepository>().To<SubAdminRepository>()
                .InScope(ctx => HttpContext.Current).Named("SubAdminRepository");

            Bind<ISubAdminService>().To<SubAdminService>()
                .InScope(ctx => HttpContext.Current).Named("SubAdminService");

            Bind<ITransactionRepositoryUK>().To<TransactionRepositoryUK>()
                .InScope(ctx => HttpContext.Current).Named("UkTxnRepository");

            Bind<ITransactionServiceUK>().To<TransactionServiceUK>()
                .InScope(ctx => HttpContext.Current).Named("UkTxnService");
        }
    }
}
