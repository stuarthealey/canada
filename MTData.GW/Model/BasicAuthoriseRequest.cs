﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace MTData.WCFGW.Model
{
    public class BasicAuthoriseRequest
    {
        public AuthenticationHeader AuthenticationHeader { get; set; }
        public BasicAuthorise BasicAuthorise { get; set; }

    }

    public class AuthenticationHeader
    {
        public string Password { get; set; }
        public string Token { get; set; }
        public string Username { get; set; }
    }

    public class BasicAuthorise
    {
        public string accountNumber { get; set; }
        public string cvv2 { get; set; }
        public string deviceId { get; set; }

        string _discountAmt = "0";
        public string discountAmt { get { return _discountAmt; } set { _discountAmt = (value == null) ? "0" : value; } }

        string _drpoffDt = DateTime.Now.ToString();
        public string dropoffDate { get { return _drpoffDt; } set { _drpoffDt = (value == null) ? DateTime.Now.ToString() : value; } }

        string _dropoffLatitude = "0";
        public string dropoffLatitude { get { return _dropoffLatitude; } set { _dropoffLatitude = (value == null) ? "0" : value; } }

        string _dropoffLongitude = "0";
        public string dropoffLongitude { get { return _dropoffLongitude; } set { _dropoffLongitude = (value == null) ? "0" : value; } }

        public string encryptedToken { get; set; }
        public byte encryptionAlgorithm { get; set; }
        public byte encryptionKeyVersion { get; set; }
        public string expiryDate { get; set; }

        string _extrasAmt = "0";
        public string extrasAmt { get { return _extrasAmt; } set { _extrasAmt = (value == null) ? "0" : value; } }

        string _fareAmt = "0";
        public string fareAmt { get { return _fareAmt; } set { _fareAmt = (value == null) ? "0" : value; } }

        string _flgflamt = "0";
        public string flagfallAmt { get { return _flgflamt; } set { _flgflamt = (value == null) ? "0" : value; } }

        string _jobDistance = "0";
        public string jobDistance { get { return _jobDistance; } set { _jobDistance = (value == null) ? "0" : value; } }

        string _jobDuration = "0";
        public string jobDuration { get { return _jobDuration; } set { _jobDuration = (value == null) ? "0" : value; } }
        public long jobId { get; set; }

        string _passengerCount = "0";//byte
        public string passengerCount { get { return _passengerCount; } set { _passengerCount = (value == null) ? "0" : value; } }
        public decimal paymentAmt { get; set; }

        string _pkpDt = DateTime.Now.ToString();
        public string pickupDate { get { return _pkpDt; } set { _pkpDt = (value == null) ? DateTime.Now.ToString() : value; } }

        string _pickupLatitude = "0";
        public string pickupLatitude { get { return _pickupLatitude; } set { _pickupLatitude = (value == null) ? "0" : value; } }

        string _pickupLongitude = "0";
        public string pickupLongitude { get { return _pickupLongitude; } set { _pickupLongitude = (value == null) ? "0" : value; } }
        public string postCode { get; set; }
        private string _RCCI = String.Empty;
        public string RCCI { get { return _RCCI; } set { _RCCI = (value == null) ? String.Empty : value; } }
        public string requestId { get; set; }
        public string swipeData { get; set; }
        public byte swipeMethod { get; set; }

        string _tipAmt = "0";
        public string tipAmt { get { return _tipAmt; } set { _tipAmt = (value == null) ? "0" : value; } }

        string _tollAmt = "0";
        public string tollAmt { get { return _tollAmt; } set { _tollAmt = (value == null) ? "0" : value; } }
        public string transactionId { get; set; }
        public string transactionType { get; set; }
        public int userId { get; set; }

    }

}