﻿namespace MTD.Core.DataTransferObjects
{
    public class FeeDeduction
    {
        public int ChargeTypeId { get; set; }
        public decimal? Charge { get; set; }
        public string ChargeName { get; set; }
    }
}