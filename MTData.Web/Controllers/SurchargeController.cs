﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Reflection;
using System.Globalization;
using System.Web.Security;
using System.Text;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Web.ViewData;
using MTData.Web.Resource;
using MTData.Utility;

namespace MTData.Web.Controllers
{
    public class SurchargeController : Controller
    {
        readonly ILogger _logger = new Logger();
        private readonly ISurchargeService _surchargeService;
        private readonly ICommonService _commonService;
        private StringBuilder _logMessage;
        private StringBuilder _logTracking;

        public SurchargeController([Named("SurchargeService")] ISurchargeService surchargeService, [Named("CommonService")] ICommonService commomService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _surchargeService = surchargeService;
            _commonService = commomService;
            _logMessage = logMessage;
            _logTracking = logTracking;
        }

        /// <summary>
        ///  Purpose             : to display the surcharge
        /// Function Name       :   Index
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/19/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("calling GetSurchargeDtoList");

                var surchargeDtoList = _surchargeService.GetSurchargeDtoList();
                
                _logMessage.Append("GetSurchargeDtoList executed successfully");
                _logMessage.Append("calling ToSurchargeViewDataList");
                
                var viewDataList = surchargeDtoList.ToSurchargeViewDataList();
                
                _logMessage.Append("ToSurchargeViewDataList executed successfully");
                
                return View(viewDataList);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            : to create the surcharge
        /// Function Name       :   Create
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/19/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var surchargeView = new SurchargeViewData();

                _logMessage.Append("calling MerchantList, GatewayList, TypeList method of same class");

                surchargeView.MerchantList = MerchantList();
                surchargeView.GatewayList = GatewayList();
                surchargeView.TypeList = TypeList();

                _logMessage.Append("MerchantList, GatewayList, TypeList methods executed successfully");

                return View(surchargeView);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            : to create the surcharge 
        /// Function Name       :   Create
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/20/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="surchargeViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(SurchargeViewData surchargeViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 2)
                return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (ModelState.IsValid)
                {
                    _logMessage.Append("calling ToSurchareDto method to map SurchargeViewData to SurchargeDto and retrun SurchargeDto");

                    var surchargeDto = surchargeViewData.ToSurchareDto();

                    _logMessage.Append("ToSurchareDto method executed successfully");

                    surchargeDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                    _logMessage.Append("calling Add method of _surchargeService parameters are " + surchargeDto.BookingFeeFixed + ", " + surchargeDto.BookingFeeMaxCap + ", " + surchargeDto.BookingFeePer + ", " + surchargeDto.BookingFeeType + ", " + surchargeDto.CCType + ", " + surchargeDto.Company + ", " + surchargeDto.CreatedBy + ", " + surchargeDto.CreatedDate + ", " + surchargeDto.FeeType + ", " + surchargeDto.IsActive + ", " + surchargeDto.ModifiedBy + ", " + surchargeDto.Name + ", " + surchargeDto.SurchargeFixed + ", " + surchargeDto.SurchargePer + ", " + surchargeDto.SurchargeType + ", " + surchargeDto.SurID + ", " + surchargeDto.SurMaxCap + ", " + surchargeDto.SurType + ", " + surchargeDto.Type);

                    _surchargeService.Add(surchargeDto);

                    _logMessage.Append("Add method of _surchargeService executed successfully");

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + " create surcharge for merchant ID " + surchargeDto.MerchantId+ "at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return RedirectToAction("Index");
                }

                _logMessage.Append("calling MerchantList,  GatewayList, TypeList method of same class");

                surchargeViewData.MerchantList = MerchantList();
                surchargeViewData.GatewayList = GatewayList();
                surchargeViewData.TypeList = TypeList();

                _logMessage.Append("MerchantList,  GatewayList, TypeList methods executed successfully");

                return View(surchargeViewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            : to update the surcharge
        /// Function Name       :  Update
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/21/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="surchargeId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Update(int surchargeId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var surchargeDto = _surchargeService.GetSurchargeDto(surchargeId);
                var surchargeView = surchargeDto.ToSurchargeViewData();

                _logMessage.Append("calling MerchantList,  GatewayList, TypeList method of same class");

                surchargeView.MerchantList = MerchantList();
                surchargeView.GatewayList = GatewayList();
                surchargeView.TypeList = TypeList();

                _logMessage.Append("MerchantList,  GatewayList, TypeList methods executed successfully");

                return View(surchargeView);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            : to  update the surcharge 
        /// Function Name       :  Update
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/21/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="surchargeId"></param>
        /// <param name="surchargeViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(int surchargeId, SurchargeViewData surchargeViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 2)
                return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (ModelState.IsValid)
                {
                    _logMessage.Append("calling ToSurchareDto method to map SurchargeViewData to SurchargeDto and retrun SurchargeDto");

                    SurchargeDto surchargeDto = surchargeViewData.ToSurchareDto();

                    _logMessage.Append("ToSurchareDto method executed successfully");

                    surchargeDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                    _logMessage.Append("calling Update method of  _surchargeService takes surchargeId and scheduleDto parameters are " + surchargeDto.BookingFeeFixed + ", " + surchargeDto.BookingFeeMaxCap + ", " + surchargeDto.BookingFeePer + ", " + surchargeDto.BookingFeeType + ", " + surchargeDto.CCType + ", " + surchargeDto.Company + ", " + surchargeDto.CreatedBy + ", " + surchargeDto.CreatedDate + ", " + surchargeDto.FeeType + ", " + surchargeDto.IsActive + ", " + surchargeDto.ModifiedBy + ", " + surchargeDto.Name + ", " + surchargeDto.SurchargeFixed + ", " + surchargeDto.SurchargePer + ", " + surchargeDto.SurchargeType + ", " + surchargeDto.SurID + ", " + surchargeDto.SurMaxCap + ", " + surchargeDto.SurType + ", " + surchargeDto.Type + ", surchargeId=" + surchargeId);

                    _surchargeService.Update(surchargeDto, surchargeId);

                    _logMessage.Append("Add method of  _surchargeService executed successfully");

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;

                    _logTracking.Append("User " + userName + " update surcharge for surchargeId " + surchargeId + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return RedirectToAction("Index");
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            _logMessage.Append("calling MerchantList,  GatewayList, TypeList method of same class");

            surchargeViewData.MerchantList = MerchantList();
            surchargeViewData.GatewayList = GatewayList();
            surchargeViewData.TypeList = TypeList();

            _logMessage.Append("MerchantList,  GatewayList, TypeList methods executed successfully");

            return View(surchargeViewData);
        }

        /// <summary>
        ///  Purpose            : To delete surcharge 
        /// Function Name       :  Delete
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/22/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="surchargeId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Delete(int surchargeId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("calling DeleteSurcharge method of  _surchargeService takes surchargeId= " + surchargeId);

                _surchargeService.DeleteSurcharge(surchargeId);

                _logMessage.Append(" DeleteSurcharge method of  _surchargeService executed successfully");

                string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                _logTracking.Append("User " + userName + " delete surcharge Id " + surchargeId + "at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        ///  Purpose            : To get the list of merchants
        /// Function Name       :  MerchantList
        /// Created By          :  Naveen Kumar
        /// Created On          :   1/23/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<MerchantListViewData> MerchantList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                _logMessage.Append("calling GetMerchantList of  _commonService to get Merchant List");

                var merchantDtoList = _commonService.GetMerchantList();

                _logMessage.Append("GetMerchantList of  _commonService executed successfully");
                _logMessage.Append("calling ToMerchantViewDataSelectList method to map SurchargeViewData to SurchargeDto and return list of MerchantListViewData");

                List<MerchantListViewData> listMerchant = merchantDtoList.ToMerchantViewDataSelectList().ToList();

                _logMessage.Append("ToMerchantViewDataSelectList method executed successfully");

                return listMerchant;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

        /// <summary>
        ///  Purpose            : to get the list of gateways
        /// Function Name       : GatewayList
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/23/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<GatewayListViewData> GatewayList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("calling GetGatewayList of  _commonService to get Gateway List");

                var gatewayDtoList = _commonService.GetGatewayList();

                _logMessage.Append("GetGatewayList of  _commonService executed successfully");
                _logMessage.Append("calling ToGatewayViewDataList method to map GatewayListViewData to GatewayListDto and return list of GatewayListViewData");

                List<GatewayListViewData> lst = gatewayDtoList.ToGatewayViewDataList().ToList();

                _logMessage.Append("ToGatewayViewDataList method executed successfully");

                return lst;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

        /// <summary>
        ///  Purpose            : to get the type of surcharge or booking fee
        /// Function Name       :  TypeList
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/24/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<SurchargeTypeViewData> TypeList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +                                   MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var listSurchargeType = new List<SurchargeTypeViewData>
                {
                    new SurchargeTypeViewData { TypeID = 0, TypeName = MtdataResource.Fixed }, new SurchargeTypeViewData { TypeID = 1, TypeName =MtdataResource.Percentage }
                };

                return listSurchargeType;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

    }
}