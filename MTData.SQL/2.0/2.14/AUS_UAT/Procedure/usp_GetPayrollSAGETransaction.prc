IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetPayrollSAGETransaction]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetPayrollSAGETransaction]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetPayrollSAGETransaction]    Script Date: 11/2/2015 11:07:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--[usp_GetPayrollSAGETransaction] 107,107,'Driver',123,'10-20-2015','11-21-2015'
--[usp_GetPayrollSAGETransaction] 352,253,'CarOwner',1102,'10-20-2015','11-21-2015'
CREATE PROCEDURE [dbo].[usp_GetPayrollSAGETransaction] @MerchantId INT = NULL
	,@FleetId INT = NULL
	,@PayerType NVARCHAR(50) = NULL
	,@DriverNumber NVARCHAR(50) = NULL
	,@DateFrom DATETIME = NULL
	,@DateTo DATETIME = NULL
	,@NetworkType INT=NULL
AS
BEGIN TRY
    DECLARE @PayType INT;
	SET NOCOUNT ON;
	IF(@PayerType='Driver')
	BEGIN
	   SET @PayType=1
	END
	ELSE IF(@PayerType='Car Owner')
	BEGIN
	    SET @PayType=2
	END
	ELSE IF(@PayerType='Network')
	BEGIN
	   SET @PayType=3
	END
	IF (@PayType =1 OR  @PayType = 3 OR @PayType IS NULL)
	BEGIN
		SELECT 'PA' AS [PaymentType]
			,D.SageAccount AS [SageAccount]
			,'1200' AS [BankAccount]
			,'1' AS [Default1]
			,COALESCE(CONVERT(DATE, T.TxnDate), '') AS [DefaultDate]
			,'BACS' AS [DefaultBACS]
			,'Payment' AS [DefaultPayment]
			,'T9' AS [DefaultTaxCode]
			,'0' AS [VAT]
			,CASE 
				WHEN SUM([TransactionAmount]) < 0
					THEN 0
				ELSE SUM([TransactionAmount])
				END AS [TransactionAmount]
			,f.PayType AS 'FleetPayType' into #TempDriver
		FROM (
			SELECT DriverNo
				,TxnDate
				,FareValue
				,tip
				,TechFee
				,fk_FleetId
				,MerchantID
				,ResponseCode
				,TxnType
				,CASE 
					WHEN TxnType IN (
							'Sale'
							,'Completion'
							)
						THEN CASE 
								WHEN (COALESCE(FareValue, 0) + COALESCE(tip, 0) + COALESCE(Taxes, 0) - (COALESCE(fee, 0) + COALESCE(FleetFee, 0)) < 0)
									THEN 0
								ELSE (COALESCE(FareValue, 0) + COALESCE(tip, 0) + COALESCE(Taxes, 0) - (COALESCE(fee, 0) + COALESCE(FleetFee, 0)))
								END
					ELSE - Amount
					END AS [TransactionAmount]
			FROM [MTDTransaction]
			) T
		INNER JOIN Driver d ON T.DriverNo = d.DriverNo
		INNER JOIN Fleet f ON d.fk_FleetID = f.FleetID
		WHERE T.fk_FleetId = d.fk_FleetID
			AND (
				COALESCE(@MerchantId, 0) = 0
				OR T.MerchantID = @MerchantId
				)
			AND (
				COALESCE(@FleetId, 0) = 0
				OR T.fk_FleetId = @FleetId
				)
			AND (
				COALESCE(@DriverNumber, '') = ''
				OR T.DriverNo = @DriverNumber
				)
			AND T.TxnDate BETWEEN @DateFrom
				AND @DateTo
			AND (
				COALESCE(@PayType,0) = 0
				OR f.PayType = @PayType
				)
			AND T.ResponseCode IN (
				'000'
				,'002'
				)
			AND T.TxnType IN (
				'Sale'
				,'Completion'
				,'Refund'
				)
		GROUP BY CONVERT(DATE, T.TxnDate)
			,D.SageAccount,f.PayType
	END
	IF(@PayType=2 OR @PayType=3 OR @PayType IS NULL)
	BEGIN
		SELECT 'PA' AS [PaymentType]
			,cw.SageAccount AS [SageAccount]
			,'1200' AS [BankAccount]
			,'1' AS [Default1]
			,COALESCE(CONVERT(DATE, T.TxnDate), '') AS [DefaultDate]
			,'BACS' AS [DefaultBACS]
			,'Payment' AS [DefaultPayment]
			,'T9' AS [DefaultTaxCode]
			,'0' AS [VAT]
			,CASE 
				WHEN SUM([TransactionAmount]) < 0
					THEN 0
				ELSE SUM([TransactionAmount])
				END AS [TransactionAmount]
			,f.PayType AS 'FleetPayType' INTO #TempOwner
		FROM (
			SELECT VehicleNo
				,TxnDate
				,FareValue
				,tip
				,TechFee
				,fk_FleetId
				,MerchantID
				,ResponseCode
				,TxnType
				,CASE 
					WHEN TxnType IN (
							'Sale'
							,'Completion'
							)
						THEN CASE 
								WHEN (COALESCE(FareValue, 0) + COALESCE(tip, 0) + COALESCE(Taxes, 0) - (COALESCE(fee, 0) + COALESCE(FleetFee, 0)) < 0)
									THEN 0
								ELSE (COALESCE(FareValue, 0) + COALESCE(tip, 0) + COALESCE(Taxes, 0) - (COALESCE(fee, 0) + COALESCE(FleetFee, 0)))
								END
					ELSE - Amount
					END AS [TransactionAmount]
			FROM [MTDTransaction]
			) T
		INNER JOIN Vehicle v ON T.VehicleNo = v.VehicleNumber
		INNER JOIN Fleet f ON v.fk_FleetID = f.FleetID
		INNER JOIN CarOwners cw ON v.fk_CarOwenerId = cw.Id
		WHERE T.fk_FleetId = v.fk_FleetID
			AND (
				COALESCE(@MerchantId, 0) = 0
				OR T.MerchantID = @MerchantId
				)
			AND (
				COALESCE(@FleetId, 0) = 0
				OR T.fk_FleetId = @FleetId
				)
			AND T.TxnDate BETWEEN @DateFrom
				AND @DateTo
			AND (
				COALESCE(@DriverNumber, '') = ''
				OR v.fk_CarOwenerId = CAST(@DriverNumber AS INT)
				)
			AND (
				COALESCE(@PayType,0)=0
				OR F.PayType = @PayType
				)
			AND T.ResponseCode IN (
				'000'
				,'002'
				)
			AND T.TxnType IN (
				'Sale'
				,'Completion'
				,'Refund'
				)
		GROUP BY CONVERT(DATE, T.TxnDate)
			,cw.SageAccount,f.PayType
	  END
	    IF(@PayType=1 OR (@PayType=3 AND @NetworkType=1))
		BEGIN
		    SELECT PaymentType,SageAccount,BankAccount,Default1,DefaultDate,DefaultBACS,DefaultPayment,DefaultTaxCode,VAT,TransactionAmount
			FROM #TempDriver
	    END
		ELSE IF(@PayType=2 OR (@PayType=3 AND @NetworkType=2))
		BEGIN
		   SELECT PaymentType,SageAccount,BankAccount,Default1,DefaultDate,DefaultBACS,DefaultPayment,DefaultTaxCode,VAT,TransactionAmount
			FROM #TempOwner
	    END
		ELSE IF(@PayType=3 AND @NetworkType  IS NULL)
		BEGIN
		   SELECT PaymentType,SageAccount,BankAccount,Default1,DefaultDate,DefaultBACS,DefaultPayment,DefaultTaxCode,VAT,TransactionAmount
			FROM #TempDriver
		UNION ALL
		   SELECT PaymentType,SageAccount,BankAccount,Default1,DefaultDate,DefaultBACS,DefaultPayment,DefaultTaxCode,VAT,TransactionAmount
			FROM #TempOwner
	    END
		ELSE IF (@PayType IS NULL)
		BEGIN
		    SELECT PaymentType,SageAccount,BankAccount,Default1,DefaultDate,DefaultBACS,DefaultPayment,DefaultTaxCode,VAT,TransactionAmount
			FROM #TempDriver WHERE FleetPayType IN (1,3)
		UNION ALL
		    SELECT PaymentType,SageAccount,BankAccount,Default1,DefaultDate,DefaultBACS,DefaultPayment,DefaultTaxCode,VAT,TransactionAmount
			FROM #TempOwner WHERE FleetPayType IN (2,3)
		END
	 IF OBJECT_ID('tempdb..#TempDriver') IS NOT NULL
		BEGIN
		  DROP TABLE #TempDriver
		END
		IF OBJECT_ID('tempdb..#TempOwner') IS NOT NULL
		BEGIN
		  DROP TABLE #TempOwner
		END	  
END TRY

--use catch to log error with details in ProcErrorHandler table
BEGIN CATCH
	INSERT INTO ProcErrorHandler (
		ErrorNumber
		,ErrorSeverity
		,ErrorState
		,ErrorProcedure
		,ErrorLine
		,ErrorMessage
		,UserName
		,HostName
		,[TimeStamp]
		)
	SELECT ERROR_NUMBER()
		,ERROR_SEVERITY()
		,ERROR_STATE()
		,ERROR_PROCEDURE()
		,ERROR_LINE()
		,ERROR_MESSAGE()
		,SUSER_SNAME()
		,HOST_NAME()
		,GETDATE()
END CATCH

GO
