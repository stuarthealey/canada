IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_TotalTransByVehicleScheduler]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_TotalTransByVehicleScheduler]
GO

CREATE PROCEDURE [dbo].[usp_TotalTransByVehicleScheduler]
(
  @IsOnDemandReport BIT,
  @FromDate DATETIME=NULL,
  @ToDate DATETIME=NULL
)
AS
BEGIN TRY
  SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF
  DECLARE @Res INT,
     @UserId INT,
     @To NVARCHAR(50),
     @Body NVARCHAR(MAX),
     @Error NVARCHAR(200),
     @MailId INT,
     @Valid INT,
     @MsgXml NVARCHAR(40),
     @XmlData NVARCHAR(MAX),
     @UserName NVARCHAR(45),
     @ReportType INT,
     @RepType NVARCHAR(50),
     @RepHeading NVARCHAR(200),
     @FDate DATETIME,
     @TDate DATETIME,
     @TotalTransaction INT,
     @TotalActualFare NVARCHAR(30),
     @TotalTips NVARCHAR(30),
     @TotalFareAmount NVARCHAR(30),
     @TotalSurcharge NVARCHAR(30),
     @TotalFee  NVARCHAR(30),
     @NoChargeBack INT,
     @ChargeBackValue NVARCHAR(25),
     @NoVoid INT,
     @VoidValue NVARCHAR(25),
     @AmountOwing  NVARCHAR(30),
     @TotalTechFee NVARCHAR(30),
     @Slab1Message NVARCHAR(40),
     @Slab2Message NVARCHAR(40),
     @Slab3Message NVARCHAR(40),
     @Slab4Message NVARCHAR(40),
     @TotalSlab1 INT,
     @TotalSlab2 INT,
     @TotalSlab3 INT,
     @TotalSlab4 INT,
	 @NextScheduleDateTime DATETIME,
	 @Result INT
     --Declaring cursor for fetching admin recipients one by one from database
    DECLARE Queue_Cursor CURSOR FOR 
    SELECT u.UserID,u.Email FROM Users u 
	                        INNER JOIN Recipient r ON u.UserID=r.fk_UserId 
							WHERE r.IsRecipient=1 AND r.fk_UserTypeId=1 
                            AND r.fk_ReportId=7 AND u.IsActive=1 AND u.fk_UserTypeID=1 
    OPEN Queue_Cursor
    FETCH NEXT FROM Queue_Cursor INTO @UserId,@To
    WHILE @@FETCH_STATUS = 0   
     BEGIN   
       SET @XmlData=''
       SELECT @UserName=FName FROM Users WHERE UserID=@UserId AND IsActive=1
       --Calling store procedure AdminSummaryReport to get the data in xml format
       EXEC @Result=usp_TotalTransByVehicle @IsOnDemandReport,@FromDate,@ToDate,@XmlData OUTPUT,
                                        @ReportType OUTPUT,@FDate OUTPUT,@TDate OUTPUT,
                                        @TotalTransaction OUTPUT, @TotalActualFare OUTPUT,
                                        @TotalTips OUTPUT,@TotalFareAmount OUTPUT,
                                        @TotalSurcharge OUTPUT,@TotalFee  OUTPUT,
                                        @NoChargeBack OUTPUT,@ChargeBackValue OUTPUT,
                                        @NoVoid OUTPUT,@VoidValue OUTPUT,@TotalTechFee OUTPUT,@AmountOwing OUTPUT,
                                        @Slab1Message OUTPUT,@Slab2Message OUTPUT,@Slab3Message OUTPUT,@Slab4Message OUTPUT,
                                        @TotalSlab1 OUTPUT,@TotalSlab2 OUTPUT,@TotalSlab3 OUTPUT,@TotalSlab4 OUTPUT,@NextScheduleDateTime OUTPUT
        IF(@Result<>0)
	     BEGIN
	       SELECT 1;
		   RETURN;
	     END
	   IF(@IsOnDemandReport=1)
	    BEGIN
		  SET @RepType='On Demand Total Trans By Vehicle Report'
		END
		ELSE
		 BEGIN
		   SET  @RepType=CASE @ReportType WHEN 1 THEN 'Daily Total Trans By Vehicle Report'
                                         WHEN 2 THEN 'Weekly Total Trans By Vehicle Report' 
										 ELSE 'Monthly Total Trans By Vehicle Report' END
		 END   
         SET @RepHeading=@RepType+' '+'(Dated from'+' '+ CONVERT(VARCHAR(11),@FDate,106)+' '+'to'+' '+CONVERT(VARCHAR(11),@TDate,106)+')'
         IF(@XmlData IS NULL OR @XmlData='') 
		  BEGIN
            SET @Valid=0
            INSERT INTO MailMessage(RepUserId,[Status],SendTo,Body,RepUserTypeId,[Date],ReportType) 
			VALUES(@UserId,-1,@To,'No records in database',1,GETDATE(),'AdminTotalTransByVehicleReport')
          END
    ELSE 
		   BEGIN
            SET @Valid=1
           END
           IF(@Valid=1) 
		    BEGIN
              --Set the xml data in the html format 
              SET @Body ='<html>
		             <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		             <style>td{text-align:left;font-family:verdana;font-size:12px}
                     p{font-family:verdana;font-size:13px}span{font-family:verdana;font-size:13px}</style>
                     <body><table style="background-color:#e0e0e0; padding:20px; height:100%;"><tr><td><p style="font-size:11px;font-weight:bold">'+CONCAT('Dear',' ',@UserName,',')+'</p>
                     <p style="font-size:11px;font-weight:bold">Listed below is the report containing details of number of vehicles based on the number of transactions for all merchants.</p>
                     <h5 style="color:#003366;text-align:center;font-size:11px;font-family:verdana; padding:10px;">'+@RepHeading+'</h5>
                    <table border = 1 cellpadding="0" cellspacing="0" style="background-color:#aed6eb; border:2px solid #003366; border-collapse:collapse; padding:2px;">
                    <thead style="background-color:#2b79c7; padding:10px;"><tr>
                    <th style="color:grey;font-size:10px;width:90px;text-align:left;font-family:verdana; color:#fff; padding:5px;">Sr.No</th>
                    <th style="color:grey;font-size:10px;width:600px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Merchant</th>
                    <th style="color:grey;font-size:10px;width:600px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Fleet</th>
                    <th style="color:grey;font-size:10px;width:140px;text-align:left;font-family:verdana; color:#fff; padding:2px;">No of Trans</th>
                    <th style="color:grey;font-size:10px;width:150px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Fare Amt</th>
                    <th style="color:grey;font-size:10px;width:100px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Tips</th>
                    <th style="color:grey;font-size:10px;width:120px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Surcharges</th>
                    <th style="color:grey;font-size:10px;width:100px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Tech Fee</th>
                    <th style="color:grey;font-size:10px;width:100px;text-align:left;font-family:verdana; color:#fff; padding:2px;">No Chargebacks</th>
                    <th style="color:grey;font-size:10px;width:100px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Chargeback Value</th>
                    <th style="color:grey;font-size:10px;width:130px;text-align:left;font-family:verdana; color:#fff; padding:2px;">No of void</th>
                    <th style="color:grey;font-size:10px;width:100px;text-align:left;font-family:verdana; color:#fff; padding:2px;">void Value</th>
                    <th style="color:grey;font-size:10px;width:150px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Total Amount</th>
                    <th style="color:grey;font-size:10px;width:150px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Fees</th>
                    <th style="color:grey;font-size:10px;width:100px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Amount Owing</th>
                    <th style="color:grey;font-size:10px;width:50px;text-align:left;font-family:verdana; color:#fff; padding:2px;">NoOfVehicles('+@Slab1Message+')</th>
                    <th style="color:grey;font-size:10px;width:50px;text-align:left;font-family:verdana; color:#fff; padding:2px;">NoOfVehicles('+@Slab2Message+')</th>
                    <th style="color:grey;font-size:10px;width:50px;text-align:left;font-family:verdana; color:#fff; padding:2px;">NoOfVehicles('+@Slab3Message+')</th>
                    <th style="color:grey;font-size:10px;width:50px;text-align:left;font-family:verdana; color:#fff; padding:2px;">NoOfVehicles(Trans greater than 14)</th></tr></thead>'    
              SET @Body = @Body + @XmlData +'<tfoot><tr style="height:25px;font-weight:bold">
                                                     <td colspan=3 style="font-size:11px;text-align:center">Total</td>
                                                     <td style="font-size:11px">' +CAST(@TotalTransaction AS NVARCHAR(50))+'</td>
                                                     <td style="font-size:11px">'+@TotalActualFare+'</td>
                                                     <td style="font-size:11px">'+@TotalTips+'</td>
                                                     <td style="font-size:11px">'+@TotalSurcharge+'</td>
                                                     <td style="font-size:11px">'+@TotalTechFee+'</td>
                                                     <td style="font-size:11px">'+CAST(@NoChargeBack AS NVARCHAR(50))+'</td>
                                                     <td style="font-size:11px">'+@ChargeBackValue+'</td>
                                                     <td style="font-size:11px">'+CAST(@NoVoid AS NVARCHAR(50))+'</td>
                                                     <td style="font-size:11px">'+@VoidValue+'</td>
                                                     <td style="font-size:11px">'+@TotalFareAmount+'</td>
                                                     <td style="font-size:11px">'+@TotalFee+'</td>
                                                     <td style="font-size:11px">'+@AmountOwing+'</td>
                                                     <td style="font-size:11px">'+CAST(@TotalSlab1 AS NVARCHAR(50))+'</td>
                                                     <td style="font-size:11px">'+CAST(@TotalSlab2 AS NVARCHAR(50))+'</td>
                                                     <td style="font-size:11px">'+CAST(@TotalSlab3 AS NVARCHAR(50))+'</td>
                                                     <td style="font-size:11px">'+CAST(@TotalSlab4 AS NVARCHAR(50))+'</td>
                                                     </tr></tfoot></table><br/><br/><span>Thanks & Regards,<br/><br/>MTData Developments Pty Ltd.<br/>www.mtdata.us</span>
                                                     <h4 style="color:grey;font-size:12px;font-family:verdana">MTData-All rights reserved</h4>
													 <img src="D:\logo-1.png" width="138" height="44" alt="logo">
                                                     <p style="font-size:11px;color:black">Note:This is an automated message. Do not Reply or Reply All to this message
                                                      as replies are not being accepted.</p></body></html>'
         INSERT INTO MailMessage(RepUserId,[Status],SendTo,Body,RepUserTypeId,[Date],ReportType) 
		 VALUES(@UserId,0,@To,@Body,1,GETDATE(),'AdminTotalTransByVehicleReport')
         SET @MailId=SCOPE_IDENTITY()
         --Execute the sp_send_dbmail to send the summary report in html format 
         EXEC @Res=msdb.dbo.sp_send_dbmail
              @profile_name = 'MTData',
              @recipients=@To,
              @body =@Body,
              @subject = 'Admin Total Trans By Vehicle Report',
              @body_format ='HTML'
         IF (@Res != 0)
          BEGIN
           SET @Error =@@ERROR
           UPDATE MailMessage SET Error=@Error,[Date]=GETDATE() WHERE MailId=@MailId
          END;
         ELSE
          BEGIN
           UPDATE MailMessage SET [Status]=1,[Date]=GETDATE() WHERE MailId=@MailId
          END
       END
    FETCH NEXT FROM Queue_Cursor INTO @UserId,@To
   END   
  CLOSE Queue_Cursor   
  DEALLOCATE Queue_Cursor
   IF(@IsOnDemandReport=0)
   BEGIN
      UPDATE ScheduleReport SET NextScheduleDateTime=@NextScheduleDateTime WHERE fk_MerchantId IS NULL 
      AND CorporateUserId IS NULL  AND fk_ReportID=7	AND IsCreated=1	
   END
  
  SELECT 0
END TRY
--use catch to log error with details in ProcErrorHandler table
BEGIN CATCH
     INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
     SELECT ERROR_NUMBER()
	       ,ERROR_SEVERITY()
		   ,ERROR_STATE()
		   ,ERROR_PROCEDURE()
		   ,ERROR_LINE()
		   ,ERROR_MESSAGE()
		   ,SUSER_SNAME()
		   ,HOST_NAME()
		   ,GETDATE()
	 SELECT 1
END CATCH
GO