﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using MTD.Core.DataTransferObjects;
using MTData.Utility.Extension;
namespace MTData.WebAPI.Models
{
    public sealed class CommonFunctions
    {
        public MTDTransactionDto XmlToTransactionDto(string xmlSerializedTransReq, TransactionRequest transactionRequest)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlSerializedTransReq);
            MTDTransactionDto tDto = new MTDTransactionDto();
            string res =
               xmlSerializedTransReq.Replace(
                   "<?xml version=\"1.0\" encoding=\"utf-8\"?><GMF xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"com/firstdata/Merchant/gmfV3.10\"><Credit",
                   "<GMF><");
            res = res.Replace(
                   "<?xml version=\"1.0\" encoding=\"utf-8\"?><GMF xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"com/firstdata/Merchant/gmfV3.10\"><Debit",
                   "<GMF><");
            res = res.Replace(
                  "<?xml version=\"1.0\" encoding=\"utf-8\"?><GMF xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"com/firstdata/Merchant/gmfV3.10\"><PinlessDebit",
                  "<GMF><");

            res = res.Replace(
                  "<?xml version=\"1.0\" encoding=\"utf-8\"?><GMF xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"com/firstdata/Merchant/gmfV3.10\"><Reversal",
                  "<GMF><");
            res = res.Replace("CreditRequest", "Request");
            res = res.Replace("DebitRequest", "Request");
            res = res.Replace("PinlessDebitRequest", "Request");
            res = res.Replace("PinlessRequest", "Request");
            res = res.Replace("ReversalRequest", "Request");
            
            xmlDoc.LoadXml(res);

            tDto.MerchantId = Convert.ToInt32(transactionRequest.MtdMerchantId);
            tDto.TerminalId = Convert.ToInt32(transactionRequest.TerminalID);
            tDto.VehicleNo = transactionRequest.VehicleNumber;
            tDto.DriverNo = transactionRequest.DriverNumber;

            //CrdHldrName = xmlDoc.SelectSingleNode("TransactionRequest/TransactionAmount").InnerText;
            //CrdHldrPhone = xmlDoc.SelectSingleNode("TransactionRequest/CardNumber").InnerText;
            //CrdHldrCity= xmlDoc.SelectSingleNode("TransactionRequest/CardCVV").InnerText;
            //CrdHldrState = xmlDoc.SelectSingleNode("TransactionRequest/ZipCode").InnerText;
            //CrdHldrZip = Convert.ToInt32(xmlDoc.SelectSingleNode("TransactionRequest/StreetAddress").InnerText);
            //CrdHldrAddress = xmlDoc.SelectSingleNode("TransactionRequest/FirstName").InnerText;

            XmlNode selectSingleStan = xmlDoc.SelectSingleNode("GMF/Request/CommonGrp/STAN");
            if (selectSingleStan != null)
                tDto.Stan = selectSingleStan.InnerText;

            var selectSingleRefNum = xmlDoc.SelectSingleNode("GMF/Request/CommonGrp/RefNum");
            if (selectSingleRefNum != null)
                tDto.TransRefNo = selectSingleRefNum.InnerText;

            var selectSingleOrderNum = xmlDoc.SelectSingleNode("GMF/Request/CommonGrp/OrderNum");
            if (selectSingleOrderNum != null)
                tDto.TransOrderNo = selectSingleOrderNum.InnerText;

            var selectSinglePymtType = xmlDoc.SelectSingleNode("GMF/Request/CommonGrp/PymtType");
            if (selectSinglePymtType != null)
                tDto.PaymentType = selectSinglePymtType.InnerText;

            var selectSingleCrdType = xmlDoc.SelectSingleNode("GMF/Request/CardGrp/CardType");
            if (selectSingleCrdType != null)
                tDto.CardType = selectSingleCrdType.InnerText;

            tDto.TxnType = transactionRequest.TransactionType == "Void" ? transactionRequest.TransactionType : transactionRequest.TransType.ToString();
            XmlNode selectSingleIndRev = xmlDoc.SelectSingleNode("GMF/Request/CommonGrp/ReversalInd");
            if (selectSingleIndRev != null)
                tDto.TxnType = selectSingleIndRev.InnerText;

            tDto.LocalDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
            tDto.TransmissionDateTime = DateTime.UtcNow.ToString("yyyyMMddHHmmss");
            tDto.Amount = Convert.ToDecimal(transactionRequest.TransactionAmount);
            tDto.Currency = "840";
            tDto.AccountNumber =StringExtension.Encrypt(transactionRequest.CardNumber);
            tDto.ExpiryDate = StringExtension.Encrypt(transactionRequest.ExpiryDate);
            //tDto.CardType = transactionRequest.CardType.ToString();
            tDto.Industry = transactionRequest.IndustryType;
            tDto.ResponseCode = "";
            tDto.AddRespData = "";
            tDto.CardLvlResult = "";
            tDto.SrcReasonCode = "";
            tDto.GatewayTxnId = "";
            tDto.AuthId = "";
            tDto.AthNtwId = "";
            tDto.SerialNo = transactionRequest.SerialNumber;
            tDto.SourceId = transactionRequest.RapidConnectAuthId;
            XmlSerializer xsSubmit = new XmlSerializer(typeof(XmlDocument));
            var subReq = xmlDoc;//new GlobalTransport.Response();
            StringWriter sww = new StringWriter();
            XmlWriter writer = XmlWriter.Create(sww);
            xsSubmit.Serialize(writer, subReq);
            var xml = sww.ToString();
           // xml =  StringExtension.Encrypt(xml);
            tDto.RequestXml = xml;
            tDto.ResponseXml = null;
            return tDto;
        }

        public MTDTransactionDto XmlResponseToTransactionDto(string xmlSerializedTransResp)
        {
            MTDTransactionDto tDto = new MTDTransactionDto();
            XmlDocument xmlDoc = new XmlDocument();
            string res =
                xmlSerializedTransResp.Replace(
                    "<?xml version=\"1.0\" encoding=\"utf-8\"?><GMF xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"com/firstdata/Merchant/gmfV3.10\">",
                    "<GMF>");
            res = res.Replace("ReversalResponse", "CreditResponse");
            res = res.Replace("PinlessDebitResponse", "CreditResponse");
            xmlDoc.LoadXml(res);

            var selectSingleNode = xmlDoc.SelectSingleNode("GMF/CreditResponse/CommonGrp/RefNum");
            if (selectSingleNode != null)
                tDto.TransRefNo = selectSingleNode.InnerText;
            var singleNode = xmlDoc.SelectSingleNode("GMF/CreditResponse/RespGrp/RespCode");
            if (singleNode != null)
                tDto.ResponseCode = singleNode.InnerText;
            var xmlNode = xmlDoc.SelectSingleNode("GMF/CreditResponse/RespGrp/AddtlRespData");
            if (xmlNode != null)
                tDto.AddRespData = xmlNode.InnerText;
            if (tDto.AddRespData == null)
            {
                var xmlNodeError = xmlDoc.SelectSingleNode("GMF/CreditResponse/RespGrp/ErrorData");
                if (xmlNodeError != null)
                    tDto.AddRespData = xmlNodeError.InnerText;
            }
            var selectSingleCardLevelResult = xmlDoc.SelectSingleNode("GMF/VisaGrp/CardLevelResult");
            if (selectSingleCardLevelResult != null)
                tDto.CardLvlResult = selectSingleCardLevelResult.InnerText;


            var selectSingleAmExTranId = xmlDoc.SelectSingleNode("GMF/CreditResponse/AmexGrp/AmExTranID");
            if (selectSingleAmExTranId != null)
                tDto.GatewayTxnId = selectSingleAmExTranId.InnerText;

            var selectSingleDiscNrid = xmlDoc.SelectSingleNode("GMF/CreditResponse/DSGrp/DiscNRID");
            if (selectSingleDiscNrid != null)
                tDto.GatewayTxnId = selectSingleDiscNrid.InnerText;

            var selectSingleSrcReasonCode = xmlDoc.SelectSingleNode("GMF/VisaGrp/SrcReasonCode");
            if (selectSingleSrcReasonCode != null)
                tDto.SrcReasonCode = selectSingleSrcReasonCode.InnerText;
            var selectSingleTrack2Data = xmlDoc.SelectSingleNode("TransactionRequest/Track2Data");
            if (selectSingleTrack2Data != null)
                tDto.GatewayTxnId = selectSingleTrack2Data.InnerText;

            var node = xmlDoc.SelectSingleNode("GMF/CreditResponse/RespGrp/AuthID");
            if (node != null)
                tDto.AuthId = node.InnerText;
            var xmlNodeAthNtwkId = xmlDoc.SelectSingleNode("GMF/CreditResponse/RespGrp/AthNtwkID");
            if (xmlNodeAthNtwkId != null)
                tDto.AthNtwId = xmlNodeAthNtwkId.InnerText;

            XmlSerializer xsSubmit = new XmlSerializer(typeof(XmlDocument));
            var subReq = xmlDoc;//new GlobalTransport.Response();
            StringWriter sww = new StringWriter();
            XmlWriter writer = XmlWriter.Create(sww);
            xsSubmit.Serialize(writer, subReq);
            var xml = sww.ToString();
            sww.Dispose();
            //xml = StringExtension.Encrypt(xml);
            tDto.ResponseXml = xml;
            return tDto;

        }
    }
}