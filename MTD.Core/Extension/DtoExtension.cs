﻿using AutoMapper;
using System.Collections.Generic;

using MTD.Core.DataTransferObjects;
using MTD.Data.Data;

namespace MTD.Core.Extension
{
    /// <summary>
    /// Purpose             :   To Map ViewData to Dto and visa versa
    /// Class Name          :   DtoExtension
    /// Created By          :   Umesh Kumar
    /// Created On          :   12/17/2014
    /// Modifications Made   :   ****************************
    /// Modified On       :   ##/##/#### "MM/DD/YYYY"
    /// </summary>
    public static class DtoExtension
    {
        static DtoExtension()
        {
            Mapper.CreateMap<Merchant, MerchantDto>();
            Mapper.CreateMap<MerchantUser, MerchantUserDto>();
            Mapper.CreateMap<User, UserDto>().ForMember(source => source.ErrorMessage, dest => dest.Ignore());
            Mapper.CreateMap<Datawire, DatawireDto>();
            Mapper.CreateMap<FDMerchant, fDMerchantDto>();
            Mapper.CreateMap<MTDTransaction, MTDTransactionDto>().ForMember(source => source.IsChargeBackAllow, dest => dest.Ignore());
            Mapper.CreateMap<MTDTransactionDto, MTDTransaction>().ForMember(source => source.Fleet, dest => dest.Ignore()).ForMember(source => source.UKMTDTransactions, dest => dest.Ignore()).ForMember(source => source.UKMTDTransactions1, dest => dest.Ignore());
            Mapper.CreateMap<usp_SearchTransactionReport_Result, MTDTransactionDto>().ForMember(source => source.IsDispatchRequest, dest => dest.Ignore()).ForMember(source => source.IsChargeBackAllow, dest => dest.Ignore()).ForMember(source => source.PayerType, dest => dest.Ignore()).ForMember(source => source.NetworkType, dest => dest.Ignore()).ForMember(source => source.ResponseBit63, dest => dest.Ignore());
            Mapper.CreateMap<CardToken, CardTokenDto>();
            Mapper.CreateMap<CardTokenDto, CardToken>();
            Mapper.CreateMap<CardToken, CardTokenDto>();
            Mapper.CreateMap<CardTokenDto, CardToken>();
            Mapper.CreateMap<TransDriverVehicle, TransDriverVehicleDto>();
            Mapper.CreateMap<GatewayDto, Gateway>().ForMember(source => source.Surcharges, dest => dest.Ignore()).ForMember(source => source.CCPrefixes, dest => dest.Ignore()).ForMember(source => source.Surcharges1, dest => dest.Ignore())
                .ForMember(source => source.Gateway_Currency, dest => dest.Ignore()).ForMember(source => source.Merchant_Gateway, dest => dest.Ignore());
            Mapper.CreateMap<User, UserDto>().ForMember(result => result.ConfirmPCode, dest => dest.Ignore());
            Mapper.AssertConfigurationIsValid();
            Mapper.CreateMap<Role, RoleDto>();
            Mapper.CreateMap<Fleet, FleetDto>();
            Mapper.CreateMap<Gateway, GatewayDto>();
            Mapper.CreateMap<CountryState, CountryStateDto>();
            Mapper.CreateMap<StateCity, CityDto>();
            Mapper.CreateMap<GatewayDto, Gateway>();

            Mapper.CreateMap<DriverDto, Driver>().ForMember(x => x.CountryCode, d => d.Ignore()).ForMember(x => x.Fleet, d => d.Ignore()).ForMember(result => result.DriverLogins, dest => dest.Ignore()).ForMember(result => result.DriverTransactionHistories, dest => dest.Ignore()).ForMember(result => result.DriverReportRecipients, dest => dest.Ignore());
            Mapper.CreateMap<Driver, DriverDto>().ForMember(result => result.ConfirmPIN, dest => dest.Ignore()).ForMember(result => result.FailedError, dest => dest.Ignore());
            Mapper.CreateMap<Terminal, TerminalDto>().ForMember(result => result.Device, dest => dest.Ignore());
            Mapper.CreateMap<TerminalDto, Terminal>().ForMember(result => result.Merchant, dest => dest.Ignore()).ForMember(x => x.Datawire, d => d.Ignore()).ForMember(result => result.Merchant1, dest => dest.Ignore())
                .ForMember(result => result.Vehicles, dest => dest.Ignore());
            Mapper.CreateMap<CountryCode, CountryCodeDto>();
            Mapper.CreateMap<TxnTaxSurcharges, TxnTaxSurchargesDto>();
            Mapper.CreateMap<ScheduleReport, ScheduleReportDto>().ForMember(result => result.TimeInString, dest => dest.Ignore()).ForMember(result => result.FrequencyName, dest => dest.Ignore()).ForMember(result => result.WeekName, dest => dest.Ignore());
            Mapper.CreateMap<Vehicle, VehicleDto>().ForMember(result => result.FailedError, dest => dest.Ignore());
            Mapper.CreateMap<VehicleDto, Vehicle>().ForMember(result => result.Fleet, dest => dest.Ignore()).ForMember(result => result.Terminal, dest => dest.Ignore()).ForMember(result => result.DriverLogins, dest => dest.Ignore()).ForMember(result => result.DriverTransactionHistories, dest => dest.Ignore()).ForMember(result => result.CarOwner, dest => dest.Ignore());
            Mapper.CreateMap<TaxDto, Merchant>().ForMember(result => result.ScheduleReports, dest => dest.Ignore()).ForMember(result => result.SetupFee, dest => dest.Ignore()).ForMember(result => result.UserFleets, dest => dest.Ignore()).ForMember(result => result.Fleets2, dest => dest.Ignore()).ForMember(result => result.Fleets1, dest => dest.Ignore()).ForMember(result => result.LogConfigurations1, dest => dest.Ignore()).ForMember(result => result.LogConfigurations2, dest => dest.Ignore()).ForMember(result => result.Merchant_Currency1, dest => dest.Ignore()).ForMember(result => result.Merchant_Currency2, dest => dest.Ignore())
                .ForMember(result => result.MonthlyFee, dest => dest.Ignore()).ForMember(result => result.Disclaimer, dest => dest.Ignore()).ForMember(result => result.TermCondition, dest => dest.Ignore()).ForMember(result => result.TipPerHigh, dest => dest.Ignore())
                .ForMember(result => result.TipPerMedium, dest => dest.Ignore()).ForMember(result => result.TipPerHigh, dest => dest.Ignore()).ForMember(result => result.IsActive, dest => dest.Ignore()).ForMember(result => result.CreatedBy, dest => dest.Ignore()).ForMember(result => result.CreatedDate, dest => dest.Ignore()).ForMember(result => result.ModifiedBy, dest => dest.Ignore()).ForMember(result => result.ModifiedDate, dest => dest.Ignore()).ForMember(result => result.IsAllowCapture, dest => dest.Ignore()).ForMember(result => result.IsAllowPreauth, dest => dest.Ignore()).ForMember(result => result.IsAllowRefund, dest => dest.Ignore())
                .ForMember(result => result.IsAllowSale, dest => dest.Ignore()).ForMember(result => result.IsAllowVoid, dest => dest.Ignore()).ForMember(result => result.Fleets, dest => dest.Ignore()).ForMember(result => result.LogConfigurations, dest => dest.Ignore()).ForMember(result => result.Merchant_Currency, dest => dest.Ignore()).ForMember(result => result.Merchant_Gateway, dest => dest.Ignore()).ForMember(result => result.Terminals, dest => dest.Ignore()).ForMember(result => result.Terminals1, dest => dest.Ignore()).ForMember(result => result.Users, dest => dest.Ignore()).ForMember(result => result.TipPerLow, dest => dest.Ignore())
                .ForMember(result => result.CompanyTaxNumber, dest => dest.Ignore()).ForMember(result => result.Users1, dest => dest.Ignore()).ForMember(result => result.Datawires, dest => dest.Ignore()).ForMember(result => result.Fk_FDId, dest => dest.Ignore()).ForMember(result => result.FDMerchant, dest => dest.Ignore())
                .ForMember(result => result.Fleets1, dest => dest.Ignore()).ForMember(result => result.Fleets2, dest => dest.Ignore()).ForMember(result => result.LogConfigurations1, dest => dest.Ignore()).ForMember(result => result.LogConfigurations2, dest => dest.Ignore()).ForMember(result => result.Merchant_Currency1, dest => dest.Ignore()).ForMember(result => result.Merchant_Currency2, dest => dest.Ignore()).ForMember(result => result.Recipients, dest => dest.Ignore()).ForMember(result => result.CCPrefixes, dest => dest.Ignore()).ForMember(result => result.CarOwners, dest => dest.Ignore()).ForMember(result => result.UserMerchants, dest => dest.Ignore())
                .ForMember(result => result.UserFleets, dest => dest.Ignore()).ForMember(result => result.UserFleets2, dest => dest.Ignore()).ForMember(result => result.UserFleets1, dest => dest.Ignore()).ForMember(result => result.IsJobNoRequired, dest => dest.Ignore())
                .ForMember(result => result.TransactionCountSlabs, dest => dest.Ignore()).ForMember(result => result.TimeZone, dest => dest.Ignore()).ForMember(result => result.ZipCode, dest => dest.Ignore())
                .ForMember(result => result.UseTransArmor, dest => dest.Ignore()    //PAY-13
                );
            Mapper.CreateMap<Merchant, TaxDto>().ForMember(result => result.fk_UserID, dest => dest.Ignore());
            Mapper.CreateMap<SurchargeDto, Surcharge>()
                .ForMember(result => result.Gateway, dest => dest.Ignore()).ForMember(result => result.Fleet, dest => dest.Ignore()).ForMember(result => result.Gateway1, dest => dest.Ignore());
            Mapper.CreateMap<Surcharge, SurchargeDto>();
            Mapper.CreateMap<Gateway, GatewayListDto>();
            Mapper.CreateMap<Fleet, FleetListDto>();
            Mapper.CreateMap<UserFleet, UserFleetDto>();
            Mapper.CreateMap<Terminal, TerminalListDto>();
            Mapper.CreateMap<Merchant, MerchantListDto>().ForMember(result => result.IsRecipient, dest => dest.Ignore()).ForMember(result => result.fk_MerchantID, dest => dest.Ignore()).ForMember(result => result.FName, dest => dest.Ignore()).ForMember(result => result.LName, dest => dest.Ignore()).ForMember(result => result.Email, dest => dest.Ignore())
                .ForMember(result => result.UserID, dest => dest.Ignore()).ForMember(result => result.fk_ReportID, dest => dest.Ignore()).ForMember(result => result.FirstName, dest => dest.Ignore()).ForMember(x => x.fk_UserID, d => d.Ignore());
            Mapper.CreateMap<SurchargeList, SurchargeDto>().ForMember(x => x.MerchantId, d => d.Ignore());
            Mapper.CreateMap<Currency, CurrencyDto>();
            Mapper.CreateMap<MerchantGatewayDto, Merchant_Gateway>().ForMember(result => result.Gateway, dest => dest.Ignore()).ForMember(result => result.Merchant, dest => dest.Ignore());
            Mapper.CreateMap<Merchant_Gateway, MerchantGatewayDto>();
            // Mapper.CreateMap<BookigFeeDto, BookingFee>();
            //Mapper.CreateMap<BookingFee, BookigFeeDto>();
            Mapper.CreateMap<DefaultSetting, DefaultTaxDto>();
            Mapper.CreateMap<ManageReciept, ManageRecieptDto>();
            Mapper.CreateMap<DefaultSetting, DefaultSettingDto>();
            Mapper.CreateMap<DefaultSettingDto, DefaultSetting>();
            Mapper.CreateMap<usp_SearchTransactionReport_Result, SearchTransactionDto>().ForMember(result => result.MerchantName, dest => dest.Ignore()).ForMember(result => result.DriverName, dest => dest.Ignore());
            Mapper.CreateMap<DefaultSetting, TCDto>();
            Mapper.CreateMap<CCPrefix, PrefixDto>();
            Mapper.CreateMap<PrefixDto, CCPrefix>().ForMember(result => result.Gateway, dest => dest.Ignore()).ForMember(result => result.Merchant, dest => dest.Ignore());
            Mapper.CreateMap<usp_GetDriversTransaction_Result, DriverTransactionResult>();
            Mapper.CreateMap<ReceiptMaster, ReceiptMasterDto>().ForMember(result => result.FleetReceiptID, dest => dest.Ignore()).ForMember(result => result.fk_FleetID, dest => dest.Ignore());//modified by asif on 7/8/2015
            Mapper.CreateMap<ReceiptFormatDto, ReceiptFormat>().ForMember(result => result.fleetlist, dest => dest.Ignore());
            Mapper.CreateMap<ReceiptFormat, ReceiptFormatDto>();
            Mapper.CreateMap<ReceiptMasterDto, ReceiptMaster>().ForMember(result => result.FleetReceipts, dest => dest.Ignore());//modified by asif on 7/8/2015
            Mapper.CreateMap<FleetReceiptDto, FleetReceipt>();
            Mapper.CreateMap<FleetReceiptDto, FleetReceipt>()
                .ForMember(result => result.IsActive, dest => dest.Ignore())
                .ForMember(result => result.ReceiptMaster, dest => dest.Ignore())
                .ForMember(result => result.Fleet, dest => dest.Ignore());
            Mapper.CreateMap<ReceiptFormat, ReceiptFormatDto>().ForMember(result => result.fleetReceipt, dest => dest.Ignore());
            Mapper.CreateMap<fleetReceiptTemp, FleetReceiptDto>();
            //Mapper.CreateMap<ScheduleReport, ScheduleReportDto>();
            Mapper.CreateMap<ScheduleReportDto, ScheduleReport>().ForMember(result => result.Report, dest => dest.Ignore()).ForMember(result => result.Merchant, dest => dest.Ignore());
            Mapper.CreateMap<Recipient, MerchantListDto>().ForMember(result => result.FirstName, dest => dest.Ignore()); ;
            Mapper.CreateMap<RecipientDto, RecipientMaster>();
            Mapper.CreateMap<MerchantListDto, MerchantRecipient>();
            Mapper.CreateMap<ReceiptList, ReceiptListDto>();
            Mapper.CreateMap<FDMerchant, FDMerchantListDto>();
            Mapper.CreateMap<FDMerchant, fDMerchantDto>();
            Mapper.CreateMap<fDMerchantDto, FDMerchant>().ForMember(dest => dest.Merchants, dest => dest.Ignore());
            Mapper.CreateMap<Datawire, RapidConnectTerminalDto>();
            Mapper.CreateMap<RapidConnectTerminalDto, Datawire>()
                .ForMember(result => result.CreatedDate, dest => dest.Ignore())
                .ForMember(result => result.IsActive, dest => dest.Ignore())
                .ForMember(result => result.IsAssigned, dest => dest.Ignore())
                .ForMember(result => result.ModifiedBy, dest => dest.Ignore())
                .ForMember(result => result.ModifiedDate, dest => dest.Ignore())
                .ForMember(result => result.Merchant, dest => dest.Ignore())
                .ForMember(result => result.Terminals, dest => dest.Ignore())
                .ForMember(result => result.Stan, dest => dest.Ignore())
                .ForMember(result => result.RefNumber, dest => dest.Ignore())
                .ForMember(result => result.TransactionDate, dest => dest.Ignore()
                );
            Mapper.CreateMap<FilterSearchParametersDto, FilterSearchParameters>();
            Mapper.CreateMap<PaymentTerminal, DeviceNameListDto>();
            Mapper.CreateMap<DeviceNameListDto, PaymentTerminal>();
            Mapper.CreateMap<FleetSettingDto, Fleet>().ForMember(dest => dest.Merchant, dest => dest.Ignore()).ForMember(dest => dest.Receipts, dest => dest.Ignore()).ForMember(dest => dest.FleetReceipts, dest => dest.Ignore()).ForMember(result => result.DriverReportRecipients, dest => dest.Ignore())
                .ForMember(dest => dest.DispatchFleetID, dest => dest.Ignore())
                .ForMember(dest => dest.BookingChannel, dest => dest.Ignore()).ForMember(dest => dest.UserName, dest => dest.Ignore()).ForMember(dest => dest.PCode, dest => dest.Ignore())
                .ForMember(dest => dest.URL, dest => dest.Ignore()).ForMember(dest => dest.Token, dest => dest.Ignore())
                .ForMember(dest => dest.UserFleets, dest => dest.Ignore()).ForMember(dest => dest.Merchant1, dest => dest.Ignore()).ForMember(dest => dest.Merchant2, dest => dest.Ignore()).ForMember(s => s.IsReceipt, d => d.Ignore()).ForMember(dest => dest.Fleet1, dest => dest.Ignore()).ForMember(dest => dest.Fleet2, dest => dest.Ignore()).ForMember(dest => dest.Currency, dest => dest.Ignore())
                .ForMember(dest => dest.CurrencyCode, dest => dest.Ignore()).ForMember(dest => dest.currencyWithSymbol, dest => dest.Ignore()); ;
            Mapper.CreateMap<Fleet, FleetSettingDto>().ForMember(dest => dest.fleetlist, dest => dest.Ignore());
            Mapper.CreateMap<FleetSettingDto, Fleet>().ForMember(dest => dest.Fleet1, dest => dest.Ignore()).ForMember(dest => dest.Fleet2, dest => dest.Ignore()).ForMember(dest => dest.Currency, dest => dest.Ignore()).ForMember(dest => dest.currencyWithSymbol, dest => dest.Ignore())
                .ForMember(dest => dest.Vehicles, dest => dest.Ignore()).ForMember(dest => dest.Surcharges, dest => dest.Ignore()).ForMember(dest => dest.Drivers, dest => dest.Ignore()).ForMember(dest => dest.MTDTransactions, dest => dest.Ignore()).ForMember(dest => dest.FleetFeeType, dest => dest.Ignore())
                .ForMember(dest => dest.FleetFeeFixed, dest => dest.Ignore())
                    .ForMember(dest => dest.FleetFeePer, dest => dest.Ignore()).ForMember(dest => dest.FleetFeeMaxCap, dest => dest.Ignore());
            Mapper.CreateMap<AdminRecipientList, AdminRecipientListDto>();
            Mapper.CreateMap<AdminRecipientListDto, AdminRecipientList>();
            Mapper.CreateMap<AdminRecipientMaster, AdminRecipienMastertListDto>();
            Mapper.CreateMap<AdminRecipienMastertListDto, AdminRecipientMaster>();
            Mapper.CreateMap<Report, ReportListDto>();
            Mapper.CreateMap<SurchargeList, SurchargeDto>();
            Mapper.CreateMap<TerminalExcelUploadDto, TerminalExcelUpload>();
            Mapper.CreateMap<DriverRecipientDto, DriverReportRecipient>().ForMember(dest => dest.Fleet, dest => dest.Ignore()).ForMember(dest => dest.Driver, dest => dest.Ignore());
            Mapper.CreateMap<DriverReportRecipient, DriverRecipientDto>();
            //Mapper.CreateMap<DriverRecListDto, DriverReportRecipient>().ForMember(dest => dest.Fleet, dest => dest.Ignore());
            Mapper.CreateMap<DriverRecipientDto, DriverRecipient>();
            Mapper.CreateMap<DriverRecListDto, DriverRecList>();
            Mapper.CreateMap<DriverRecList, DriverRecListDto>().ForMember(dest => dest.fleetlist, dest => dest.Ignore()).ForMember(dest => dest.UserList, dest => dest.Ignore());

            Mapper.CreateMap<usp_FirstDataDetails_Result, FirstDataDetailsResultDto>();
            Mapper.CreateMap<DriverList, DriverListsDto>();
            Mapper.CreateMap<CarList, CarListDto>();
            Mapper.CreateMap<FleetList, FleetListDto>();
            Mapper.CreateMap<usp_GetLoginData_Result, DriverLoginDto>();
            Mapper.CreateMap<DriverLoginDto, usp_GetLoginData_Result>().ForMember(d => d.TAKeyUpdateRequired, d => d.Ignore());
            Mapper.CreateMap<UserFleetDto, UserFleet>().ForMember(dest => dest.User, dest => dest.Ignore()).ForMember(dest => dest.Fleet, dest => dest.Ignore()).ForMember(dest => dest.Merchant, dest => dest.Ignore()).ForMember(dest => dest.UserType, dest => dest.Ignore())
                .ForMember(dest => dest.CorporateMerchantID, dest => dest.Ignore())
                .ForMember(dest => dest.Merchant1, dest => dest.Ignore())
                .ForMember(dest => dest.Merchant2, dest => dest.Ignore())
                .ForMember(dest => dest.UserType1, dest => dest.Ignore());

            Mapper.CreateMap<usp_GetLoginData_Result, DriverLoginDto>();
            Mapper.CreateMap<DriverLoginDto, usp_GetLoginData_Result>();
            Mapper.CreateMap<CarOwnersDto, CarOwner>().ForMember(s => s.Vehicles, d => d.Ignore()).ForMember(s => s.Merchant, d => d.Ignore());
            Mapper.CreateMap<CarOwner, CarOwnersDto>().ForMember(s => s.FailedError, d => d.Ignore());
            Mapper.CreateMap<usp_TechnologySummaryReport_Result, TechnologySummaryReportDto>();
            Mapper.CreateMap<usp_TechnologyDetailReport_Result, TechnologyDetailReportDto>();
            Mapper.CreateMap<ManyTxnClassDto, ManyTxnClass>();
            Mapper.CreateMap<usp_GetPayrollSAGETransaction_Result, PayrollRportSageResult>();
            Mapper.CreateMap<usp_GetPayrollPaymentTransaction_Result, PayrollRportPaymentResult>();
            Mapper.CreateMap<PageSizeSearchDto, PageSizeSearch>();
            Mapper.CreateMap<usp_GetTransactionData_Result, DownloadDriverDataDto>();
            Mapper.CreateMap<DownloadParametersDto, DownloadParameters>();
            Mapper.CreateMap<AllMerchants, AllMerchantsDto>();
            Mapper.CreateMap<SurchargeFleetFee, SurchargeFleetFeeDto>();
            Mapper.CreateMap<MTDSemiIntigratedTxnDto, MTDSemiIntigratedTxn>();
            Mapper.CreateMap<CardTokenDto, CardToken>();
            Mapper.CreateMap<CapKeyDto, CapKey>();
            Mapper.CreateMap<CapKey, CapKeyDto>();
            Mapper.CreateMap<TAKeyDto, TAKey>();
            Mapper.CreateMap<TAKey, TAKeyDto>();
            Mapper.CreateMap<MtdtxnSemiUKDto, UKMTDTransaction>().ForMember(s => s.MTDTransaction, d => d.Ignore()).ForMember(s => s.Fk_MtdTxnId, d => d.Ignore()).ForMember(s => s.MTDTransaction1, d => d.Ignore()).ForMember(s => s.Fk_TxnId, d => d.Ignore());
            Mapper.CreateMap<MtdtxnSattlementUKDto, UKMtdtxnSattlement>();
            Mapper.CreateMap<TransactionCountSlab, TransactionCountSlabDto>();
            Mapper.CreateMap<TransactionCountSlabDto, TransactionCountSlab>().ForMember(s => s.Merchant, d => d.Ignore());
            Mapper.CreateMap<usp_GetAdminSummaryReport_Result, GetAdminSummaryReportDto>();
            Mapper.CreateMap<usp_GetCardTypeReport_Result, GetCardTypeReportDto>();
            Mapper.CreateMap<usp_GetAdminExceptionReport_Result, GetAdminExceptionReportDto>();
            Mapper.CreateMap<usp_GetTotalTransByVehicleReport_Result, GetTotalTransByVehicleReportDto>();
            Mapper.CreateMap<usp_GetMerchantSummaryReport_Result, GetMerchantSummaryReportDto>();
            Mapper.CreateMap<usp_GetDetailReport_Result, GetDetailReportDto>();
            Mapper.CreateMap<usp_GetCorpUserExceptionReport_Result, GetMerchantExceptionReportDto>();

            Mapper.CreateMap<usp_GetMerchantExceptionReport_Result, GetMerchantExceptionReportDto>();
            Mapper.CreateMap<usp_GetMerchantUserExceptionReport_Result, GetMerchantExceptionReportDto>();
            Mapper.CreateMap<usp_GetMerchantTotalTransByVehicleReport_Result, GetMerchantTotalTransByVehicleReportDto>();
            Mapper.CreateMap<usp_GetMerchantUserTotalTransByVehicleReport_Result, GetMerchantTotalTransByVehicleReportDto>();
            Mapper.CreateMap<usp_GetMerchantSummaryReport_Result, GetMerchantSummaryReportDto>();
            Mapper.CreateMap<usp_GetMerchantUserSummaryReport_Result, GetMerchantSummaryReportDto>();
            Mapper.CreateMap<usp_GetMerchantUserDetailReport_Result, GetDetailReportDto>();
            Mapper.CreateMap<usp_GetMerchantUserCardTypeReport_Result, GetCardTypeReportDto>();
            Mapper.CreateMap<usp_GetCorpUserCardTypeReport_Result, GetCardTypeReportDto>();
            Mapper.CreateMap<usp_GetMerchantCardTypeReport_Result, GetMerchantCardTypeReportDto>();
            Mapper.CreateMap<usp_CorpUserGetTotalTransByVehicleReport_Result, GetTotalTransByVehicleReportDto>();
            Mapper.CreateMap<ServiceDiscoveryParametersDto, ServiceDiscoveryParametersData>();
            Mapper.CreateMap<ServiceProviderUrlDto, ServiceProviderUrlData>();
            Mapper.CreateMap<DatawireServiceUrlDto, DatawireServiceUrlData>();
            Mapper.CreateMap<usp_GetMerchantUserTotalTransByVehicleReport_Result, GetMerchantTotalTransByVehicleReportDto>();
            Mapper.CreateMap<usp_GetMerchantSummaryReport_Result, GetMerchantSummaryReportDto>();
            Mapper.CreateMap<usp_GetMerchantUserSummaryReport_Result, GetMerchantSummaryReportDto>();
            Mapper.CreateMap<usp_GetMerchantUserDetailReport_Result, GetDetailReportDto>();
            Mapper.CreateMap<usp_GetMerchantUserCardTypeReport_Result, GetCardTypeReportDto>();
            Mapper.CreateMap<usp_GetCorpUserCardTypeReport_Result, GetCardTypeReportDto>();
            Mapper.CreateMap<usp_GetMerchantCardTypeReport_Result, GetMerchantCardTypeReportDto>();
            Mapper.CreateMap<usp_CorpUserGetTotalTransByVehicleReport_Result, GetTotalTransByVehicleReportDto>();
            Mapper.CreateMap<usp_GetAdminWeeklySummaryCount_Result, GetAdminWeeklySummaryCountDto>();
            Mapper.CreateMap<usp_GetAdminWeeklyDollarVolume_Result, WeeklyDollarVolumeDto>();

            Mapper.AssertConfigurationIsValid();
        }

        public static IEnumerable<fDMerchantDto> ToFdDTOList(this IEnumerable<FDMerchant> fdMerchants) // converting MerchantUser to MerchantUserDtoList
        {
            return Mapper.Map<IEnumerable<FDMerchant>, IEnumerable<fDMerchantDto>>(fdMerchants);
        }

        public static IEnumerable<FleetReceiptDto> ToDTOList(this IEnumerable<fleetReceiptTemp> receipt)
        {
            return Mapper.Map<IEnumerable<fleetReceiptTemp>, IEnumerable<FleetReceiptDto>>(receipt);
        }

        public static IEnumerable<FleetReceiptDto> ToDTOList(this IEnumerable<FleetReceipt> receipt)
        {
            return Mapper.Map<IEnumerable<FleetReceipt>, IEnumerable<FleetReceiptDto>>(receipt);
        }

        public static ReceiptFormat ToReceipt(this ReceiptFormatDto receipt)
        {
            return Mapper.Map<ReceiptFormatDto, ReceiptFormat>(receipt);
        }

        public static ReceiptMaster ToReceiptMaster(this ReceiptFormatDto receiptMaster)
        {
            return Mapper.Map<ReceiptFormatDto, ReceiptMaster>(receiptMaster);
        }

        public static IEnumerable<ReceiptFormatDto> ToDTOList(this IEnumerable<ReceiptFormat> receipt)
        {
            return Mapper.Map<IEnumerable<ReceiptFormat>, IEnumerable<ReceiptFormatDto>>(receipt);
        }

        public static IEnumerable<ReceiptMasterDto> RecMasterToDTOList(this IEnumerable<ReceiptMaster> receiptMaster) // converting MerchantUser to MerchantUserDtoList
        {
            return Mapper.Map<IEnumerable<ReceiptMaster>, IEnumerable<ReceiptMasterDto>>(receiptMaster);
        }

        public static IEnumerable<ManageRecieptDto> ToDTOList(this IEnumerable<ManageReciept> receipt) // converting MerchantUser to MerchantUserDtoList
        {
            return Mapper.Map<IEnumerable<ManageReciept>, IEnumerable<ManageRecieptDto>>(receipt);
        }

        public static Gateway ToGateway(this GatewayDto gateway)
        {
            return Mapper.Map<GatewayDto, Gateway>(gateway);
        }

        public static GatewayDto ToGatewayDto(this Gateway gateway)
        {
            return Mapper.Map<Gateway, GatewayDto>(gateway);
        }

        public static IEnumerable<MerchantUserDto> ToDTOList(this IEnumerable<MerchantUser> merchant) // converting MerchantUser to MerchantUserDtoList
        {
            return Mapper.Map<IEnumerable<MerchantUser>, IEnumerable<MerchantUserDto>>(merchant);
        }

        public static ManageRecieptDto ToManageRecDtoDto(this ManageReciept receipt)
        {
            return Mapper.Map<ManageReciept, ManageRecieptDto>(receipt);
        }

        public static UserDto ToDtoUser(this User usr)
        {
            return Mapper.Map<User, UserDto>(usr);
        }

        public static MerchantUserDto ToDtoMerchantUser(this MerchantUser usr)  // converting MerchantUser to MerchantUser
        {
            return Mapper.Map<MerchantUser, MerchantUserDto>(usr);
        }

        public static IEnumerable<UserDto> ToDTOUserList(this IEnumerable<User> user)
        {
            return Mapper.Map<IEnumerable<User>, IEnumerable<UserDto>>(user);
        }

        public static MerchantDto ToDtoMerchant(this Merchant user)
        {
            return Mapper.Map<Merchant, MerchantDto>(user);
        }

        public static fDMerchantDto TofdMerchantDto(this FDMerchant fdMerchant)
        {
            return Mapper.Map<FDMerchant, fDMerchantDto>(fdMerchant);
        }

        public static FDMerchant ToFdMerchant(this fDMerchantDto fDMerchantDto)
        {
            return Mapper.Map<fDMerchantDto, FDMerchant>(fDMerchantDto);
        }

        public static IEnumerable<FDMerchantListDto> ToFdMerchantListDTO(this IEnumerable<FDMerchant> fdMerchants) // converting MerchantUser to MerchantUserDtoList
        {
            return Mapper.Map<IEnumerable<FDMerchant>, IEnumerable<FDMerchantListDto>>(fdMerchants);
        }

        public static IEnumerable<GatewayDto> ToDtoGatewaysList(this IEnumerable<Gateway> gateways)
        {
            return Mapper.Map<IEnumerable<Gateway>, IEnumerable<GatewayDto>>(gateways);
        }

        public static IEnumerable<RoleDto> ToDTOList(this IEnumerable<Role> role) // converting MerchantUser to MerchantUserDtoList
        {
            return Mapper.Map<IEnumerable<Role>, IEnumerable<RoleDto>>(role);
        }

        public static IEnumerable<UserDto> ToDTOList(this IEnumerable<User> role) // converting MerchantUser to MerchantUserDtoList
        {
            return Mapper.Map<IEnumerable<User>, IEnumerable<UserDto>>(role);
        }

        public static IEnumerable<FleetDto> ToDTOList(this IEnumerable<Fleet> merchant) // converting fleet to fleetDtoList
        {
            return Mapper.Map<IEnumerable<Fleet>, IEnumerable<FleetDto>>(merchant);
        }

        public static FleetDto ToDtoFleet(this Fleet fleet)
        {
            return Mapper.Map<Fleet, FleetDto>(fleet);
        }

        public static IEnumerable<DriverDto> ToDTODriverList(this IEnumerable<Driver> driver)
        {
            return Mapper.Map<IEnumerable<Driver>, IEnumerable<DriverDto>>(driver);
        }

        public static Driver ToDriver(this DriverDto driverDto)
        {
            return Mapper.Map<DriverDto, Driver>(driverDto);
        }

        public static DriverDto ToDriverDto(this Driver driver)
        {
            return Mapper.Map<Driver, DriverDto>(driver);
        }

        public static IEnumerable<TerminalDto> ToDTOTerminalList(this IEnumerable<Terminal> terminal)
        {
            return Mapper.Map<IEnumerable<Terminal>, IEnumerable<TerminalDto>>(terminal);
        }

        public static Terminal ToTerminal(this TerminalDto terminalDto)
        {
            return Mapper.Map<TerminalDto, Terminal>(terminalDto);
        }

        public static TerminalDto ToTerminalDto(this Terminal terminal)
        {
            return Mapper.Map<Terminal, TerminalDto>(terminal);
        }

        public static IEnumerable<CountryCodeDto> ToDTOCountryList(this IEnumerable<CountryCode> country)
        {
            return Mapper.Map<IEnumerable<CountryCode>, IEnumerable<CountryCodeDto>>(country);
        }

        public static IEnumerable<CountryStateDto> ToDtoStateList(this IEnumerable<CountryState> state)
        {
            return Mapper.Map<IEnumerable<CountryState>, IEnumerable<CountryStateDto>>(state);
        }

        public static IEnumerable<CityDto> ToDtoCityList(this IEnumerable<StateCity> cities)
        {
            return Mapper.Map<IEnumerable<StateCity>, IEnumerable<CityDto>>(cities);
        }

        public static IEnumerable<UserFleetDto> ToDtoUserFleetList(this IEnumerable<UserFleet> userFleet)
        {
            return Mapper.Map<IEnumerable<UserFleet>, IEnumerable<UserFleetDto>>(userFleet);
        }

        public static Merchant ToTax(this TaxDto taxRateDto)
        {

            return Mapper.Map<TaxDto, Merchant>(taxRateDto);
        }

        public static IEnumerable<TaxDto> ToDTOTaxList(this IEnumerable<Merchant> taxList)
        {
            return Mapper.Map<IEnumerable<Merchant>, IEnumerable<TaxDto>>(taxList);
        }

        public static IEnumerable<MerchantListDto> ToDTOMerchantSelectList(this IEnumerable<Merchant> merchant)
        {
            return Mapper.Map<IEnumerable<Merchant>, IEnumerable<MerchantListDto>>(merchant);
        }

        public static IEnumerable<SurchargeDto> ToDTOSurchargeList(this IEnumerable<Surcharge> surcharge)
        {
            return Mapper.Map<IEnumerable<Surcharge>, IEnumerable<SurchargeDto>>(surcharge);
        }

        public static Surcharge ToSurcharge(this SurchargeDto surchargeDto)
        {
            return Mapper.Map<SurchargeDto, Surcharge>(surchargeDto);
        }

        public static SurchargeDto ToSurchargeDto(this Surcharge surcharge)
        {
            return Mapper.Map<Surcharge, SurchargeDto>(surcharge);
        }

        public static IEnumerable<GatewayListDto> ToDTOGatewayList(this IEnumerable<Gateway> gateway)
        {
            return Mapper.Map<IEnumerable<Gateway>, IEnumerable<GatewayListDto>>(gateway);
        }

        public static IEnumerable<FleetListDto> ToDTOFleetSelectList(this IEnumerable<Fleet> fleet)
        {
            return Mapper.Map<IEnumerable<Fleet>, IEnumerable<FleetListDto>>(fleet);
        }

        public static IEnumerable<TerminalListDto> ToDTOTerminalSelectList(this IEnumerable<Terminal> terminal)
        {
            return Mapper.Map<IEnumerable<Terminal>, IEnumerable<TerminalListDto>>(terminal);
        }

        public static TaxDto ToTaxDto(this Merchant taxRate)
        {
            return Mapper.Map<Merchant, TaxDto>(taxRate);
        }

        public static Vehicle ToVehicle(this VehicleDto vehicleDto)
        {
            return Mapper.Map<VehicleDto, Vehicle>(vehicleDto);
        }

        public static IEnumerable<VehicleDto> ToDTOVehicleList(this IEnumerable<Vehicle> vehicle)
        {
            return Mapper.Map<IEnumerable<Vehicle>, IEnumerable<VehicleDto>>(vehicle);
        }

        public static VehicleDto ToVehicleDto(this Vehicle vehicle)
        {
            return Mapper.Map<Vehicle, VehicleDto>(vehicle);
        }

        public static MTDTransaction ToTransaction(this MTDTransaction gateway)
        {
            return Mapper.Map<MTDTransaction, MTDTransaction>(gateway);
        }

        public static MTDTransaction ToMtdTransaction(this MTDTransactionDto gateway)
        {
            return Mapper.Map<MTDTransactionDto, MTDTransaction>(gateway);
        }

        public static CardToken ToCardToken(this CardTokenDto cardTokenDto)
        {
            return Mapper.Map<CardTokenDto, CardToken>(cardTokenDto);
        }

        public static MTDTransactionDto ToDtoTransaction(this MTDTransaction trx)  // converting transaction
        {
            return Mapper.Map<MTDTransaction, MTDTransactionDto>(trx);
        }

        public static IEnumerable<CurrencyDto> ToDtoCurrencyList(this IEnumerable<Currency> currencies)
        {
            return Mapper.Map<IEnumerable<Currency>, IEnumerable<CurrencyDto>>(currencies);
        }

        public static Merchant_Gateway ToMerchantGateway(this MerchantGatewayDto merchantGateway)
        {
            return Mapper.Map<MerchantGatewayDto, Merchant_Gateway>(merchantGateway);
        }

        public static IEnumerable<MerchantGatewayDto> ToDTOGatewayList(this IEnumerable<Merchant_Gateway> merchantGateway)
        {
            return Mapper.Map<IEnumerable<Merchant_Gateway>, IEnumerable<MerchantGatewayDto>>(merchantGateway);
        }

        public static MerchantGatewayDto ToMerchantGatewayDto(this Merchant_Gateway merchantGateway)
        {
            return Mapper.Map<Merchant_Gateway, MerchantGatewayDto>(merchantGateway);
        }

        public static DefaultTaxDto ToDefaultTaxDto(this DefaultSetting defaultTax)
        {
            return Mapper.Map<DefaultSetting, DefaultTaxDto>(defaultTax);
        }

        public static DefaultSetting ToDefaultSetting(this DefaultSettingDto defaultDto)
        {
            return Mapper.Map<DefaultSettingDto, DefaultSetting>(defaultDto);
        }

        public static IEnumerable<DefaultSettingDto> ToDTODefaultList(this IEnumerable<DefaultSetting> defaultSetting)
        {
            return Mapper.Map<IEnumerable<DefaultSetting>, IEnumerable<DefaultSettingDto>>(defaultSetting);
        }

        public static IEnumerable<ReceiptListDto> ToDTODefaultList(this IEnumerable<ReceiptList> defaultSetting)
        {
            return Mapper.Map<IEnumerable<ReceiptList>, IEnumerable<ReceiptListDto>>(defaultSetting);
        }

        public static DefaultSettingDto ToDefaultDto(this DefaultSetting defaultSetting)
        {
            return Mapper.Map<DefaultSetting, DefaultSettingDto>(defaultSetting);
        }

        public static IEnumerable<MTDTransactionDto> ToTransaction(this IEnumerable<usp_SearchTransactionReport_Result> trans)
        {
            return Mapper.Map<IEnumerable<usp_SearchTransactionReport_Result>, IEnumerable<MTDTransactionDto>>(trans);
        }

        public static TCDto ToTCDto(this DefaultSetting termCondition)
        {
            return Mapper.Map<DefaultSetting, TCDto>(termCondition);
        }

        public static CCPrefix ToPrefix(this PrefixDto prefixDto)
        {
            return Mapper.Map<PrefixDto, CCPrefix>(prefixDto);
        }

        public static IEnumerable<PrefixDto> ToDTOPrefixList(this IEnumerable<CCPrefix> prefix)
        {
            return Mapper.Map<IEnumerable<CCPrefix>, IEnumerable<PrefixDto>>(prefix);
        }

        public static PrefixDto ToPrefixDto(this CCPrefix prefix)
        {
            return Mapper.Map<CCPrefix, PrefixDto>(prefix);
        }

        public static IEnumerable<DriverTransactionResult> ToDriverReport(this IEnumerable<usp_GetDriversTransaction_Result> trx)
        {
            return Mapper.Map<IEnumerable<usp_GetDriversTransaction_Result>, IEnumerable<DriverTransactionResult>>(trx);
        }

        public static IEnumerable<TechnologySummaryReportDto> ToTechSummaryReport(this IEnumerable<usp_TechnologySummaryReport_Result> trx)
        {
            return Mapper.Map<IEnumerable<usp_TechnologySummaryReport_Result>, IEnumerable<TechnologySummaryReportDto>>(trx);
        }

        public static IEnumerable<TechnologyDetailReportDto> ToTechDetailReport(this IEnumerable<usp_TechnologyDetailReport_Result> trx)
        {
            return Mapper.Map<IEnumerable<usp_TechnologyDetailReport_Result>, IEnumerable<TechnologyDetailReportDto>>(trx);
        }

        public static ScheduleReportDto ToScheduleReportDto(this ScheduleReport scheduleReport)
        {
            return Mapper.Map<ScheduleReport, ScheduleReportDto>(scheduleReport);
        }

        public static ScheduleReport ToScheduleReport(this ScheduleReportDto scheduleReportDto)
        {
            return Mapper.Map<ScheduleReportDto, ScheduleReport>(scheduleReportDto);
        }

        public static IEnumerable<MerchantListDto> ToDtoRecipientList(this IEnumerable<Recipient> recipient)
        {
            return Mapper.Map<IEnumerable<Recipient>, IEnumerable<MerchantListDto>>(recipient);
        }

        public static RecipientMaster ToRecipientMaster(this RecipientDto recipientDto)
        {
            return Mapper.Map<RecipientDto, RecipientMaster>(recipientDto);
        }

        public static MerchantRecipient ToRecipient(this MerchantListDto recipientDto)
        {
            return Mapper.Map<MerchantListDto, MerchantRecipient>(recipientDto);
        }

        public static DatawireDto TodatawireDto(this Datawire datawire)
        {
            return Mapper.Map<Datawire, DatawireDto>(datawire);
        }

        public static MTDTransaction ToTransaction(this MTDTransactionDto gateway)
        {
            return Mapper.Map<MTDTransactionDto, MTDTransaction>(gateway);
        }

        public static TxnTaxSurchargesDto ToTxnSurchages(this TxnTaxSurcharges gateway)
        {
            return Mapper.Map<TxnTaxSurcharges, TxnTaxSurchargesDto>(gateway);
        }

        public static MTDTransactionDto ToMtdTransactionDto(this MTDTransaction mtdTrans)
        {
            return Mapper.Map<MTDTransaction, MTDTransactionDto>(mtdTrans);
        }

        public static TransDriverVehicleDto ToDVehicle(this TransDriverVehicle gateway)
        {
            return Mapper.Map<TransDriverVehicle, TransDriverVehicleDto>(gateway);
        }

        public static IEnumerable<RapidConnectTerminalDto> ToDtoRegisteredTerminal(this IEnumerable<Datawire> datawire)
        {
            return Mapper.Map<IEnumerable<Datawire>, IEnumerable<RapidConnectTerminalDto>>(datawire);
        }

        public static Datawire ToRcDatawire(this RapidConnectTerminalDto rcDto)
        {
            return Mapper.Map<RapidConnectTerminalDto, Datawire>(rcDto);
        }

        public static FilterSearchParameters SearchParameters(this FilterSearchParametersDto fsparameters)
        {
            return Mapper.Map<FilterSearchParametersDto, FilterSearchParameters>(fsparameters);
        }

        public static IEnumerable<DeviceNameListDto> ToDtoDeviceDto(this IEnumerable<PaymentTerminal> payment)   //created by naveen
        {
            return Mapper.Map<IEnumerable<PaymentTerminal>, IEnumerable<DeviceNameListDto>>(payment);
        }

        public static PaymentTerminal ToPaymentDevice(this DeviceNameListDto payment)   //created by naveen
        {
            return Mapper.Map<DeviceNameListDto, PaymentTerminal>(payment);
        }

        public static DeviceNameListDto ToPaymentDeviceDto(this PaymentTerminal payment)   //created by naveen
        {
            return Mapper.Map<PaymentTerminal, DeviceNameListDto>(payment);
        }

        public static IEnumerable<SurchargeDto> ToDTODefaultList(this IEnumerable<SurchargeList> defaultSetting)
        {
            return Mapper.Map<IEnumerable<SurchargeList>, IEnumerable<SurchargeDto>>(defaultSetting);
        }

        public static Fleet ToFleet(this FleetSettingDto fleetSetting)   //created by asif
        {
            return Mapper.Map<FleetSettingDto, Fleet>(fleetSetting);
        }

        public static IEnumerable<FleetSettingDto> ToFleetSettingDtoList(this IEnumerable<Fleet> fleetlist) //created by asif
        {
            return Mapper.Map<IEnumerable<Fleet>, IEnumerable<FleetSettingDto>>(fleetlist);
        }

        public static FleetSettingDto ToFleetSettingDto(this Fleet fleetSetting)   //created by asif
        {
            return Mapper.Map<Fleet, FleetSettingDto>(fleetSetting);
        }

        public static IEnumerable<AdminRecipienMastertListDto> ToDtoList(this IEnumerable<AdminRecipientMaster> adminRecipientUser)
        {
            return Mapper.Map<IEnumerable<AdminRecipientMaster>, IEnumerable<AdminRecipienMastertListDto>>(adminRecipientUser);
        }

        public static IEnumerable<AdminRecipienMastertListDto> ToAdminRecipietDtoList(this IEnumerable<AdminRecipientMaster> adminRecipientUser)
        {
            return Mapper.Map<IEnumerable<AdminRecipientMaster>, IEnumerable<AdminRecipienMastertListDto>>(adminRecipientUser);
        }

        public static AdminRecipientList ToAdminRecipientMaster(this AdminRecipientListDto recipientDto)
        {
            return Mapper.Map<AdminRecipientListDto, AdminRecipientList>(recipientDto);
        }

        public static IEnumerable<ScheduleReportDto> ToScheduleList(this IEnumerable<ScheduleReport> user)
        {
            return Mapper.Map<IEnumerable<ScheduleReport>, IEnumerable<ScheduleReportDto>>(user);
        }

        public static IEnumerable<ReportListDto> ToReportList(this IEnumerable<Report> user)
        {
            return Mapper.Map<IEnumerable<Report>, IEnumerable<ReportListDto>>(user);
        }

        public static List<TerminalExcelUpload> ToTerminalList(this List<TerminalExcelUploadDto> terminal)
        {
            return Mapper.Map<List<TerminalExcelUploadDto>, List<TerminalExcelUpload>>(terminal);
        }

        public static TerminalExcelUpload ToTerminalDevice(this TerminalExcelUploadDto terminal)
        {
            return Mapper.Map<TerminalExcelUploadDto, TerminalExcelUpload>(terminal);
        }

        public static List<TerminalExcelUploadDto> ToTerminalDtoList(this List<TerminalExcelUpload> terminal)
        {
            return Mapper.Map<List<TerminalExcelUpload>, List<TerminalExcelUploadDto>>(terminal);
        }

        public static List<Driver> ToDriverList(this List<DriverDto> driverDto)
        {
            return Mapper.Map<List<DriverDto>, List<Driver>>(driverDto);
        }

        public static List<DriverDto> ToDriverDtoList(this List<Driver> driver)
        {
            return Mapper.Map<List<Driver>, List<DriverDto>>(driver);
        }

        public static IEnumerable<DriverRecipientDto> ToDtoList(this IEnumerable<DriverReportRecipient> driverRecipient)
        {
            return Mapper.Map<IEnumerable<DriverReportRecipient>, IEnumerable<DriverRecipientDto>>(driverRecipient);
        }

        public static IEnumerable<DriverReportRecipient> toReportRecipient(this IEnumerable<DriverRecipientDto> reportRecipient)
        {
            return Mapper.Map<IEnumerable<DriverRecipientDto>, IEnumerable<DriverReportRecipient>>(reportRecipient);
        }

        public static IEnumerable<DriverRecipientDto> ToDTOList(this IEnumerable<DriverRecipient> driverRecipient)
        {
            return Mapper.Map<IEnumerable<DriverRecipient>, IEnumerable<DriverRecipientDto>>(driverRecipient);
        }

        public static DriverRecList toDriverReportRec(this DriverRecListDto driverReportRecipient)
        {
            return Mapper.Map<DriverRecListDto, DriverRecList>(driverReportRecipient);
        }

        public static IEnumerable<DriverRecipient> toDriverRecList(this IEnumerable<DriverRecipientDto> driverRecipient)
        {
            return Mapper.Map<IEnumerable<DriverRecipientDto>, IEnumerable<DriverRecipient>>(driverRecipient);
        }

        public static FirstDataDetailsResultDto ToFirstDataDto(this usp_FirstDataDetails_Result fd)
        {
            return Mapper.Map<usp_FirstDataDetails_Result, FirstDataDetailsResultDto>(fd);
        }

        public static IEnumerable<DriverListsDto> ToDtoList(this IEnumerable<DriverList> driverlist)
        {
            return Mapper.Map<IEnumerable<DriverList>, IEnumerable<DriverListsDto>>(driverlist);
        }

        public static IEnumerable<CarListDto> ToDtoList(this IEnumerable<CarList> carList)
        {
            return Mapper.Map<IEnumerable<CarList>, IEnumerable<CarListDto>>(carList);
        }

        public static IEnumerable<FleetListDto> ToDTOList(this IEnumerable<FleetList> fleetList)
        {
            return Mapper.Map<IEnumerable<FleetList>, IEnumerable<FleetListDto>>(fleetList);
        }

        public static DriverLoginDto ToDTOList(this usp_GetLoginData_Result driverLoginData)
        {
            return Mapper.Map<usp_GetLoginData_Result, DriverLoginDto>(driverLoginData);
        }

        public static IEnumerable<DriverLoginDto> ToDTOList(this IEnumerable<usp_GetLoginData_Result> fleetList)
        {
            return Mapper.Map<IEnumerable<usp_GetLoginData_Result>, IEnumerable<DriverLoginDto>>(fleetList);
        }

        public static IEnumerable<UserFleet> toUserFleetList(this IEnumerable<UserFleetDto> UserFleet)
        {
            return Mapper.Map<IEnumerable<UserFleetDto>, IEnumerable<UserFleet>>(UserFleet);
        }

        public static TerminalExcelUpload toTerminalExcel(this TerminalExcelUploadDto terminal)
        {
            return Mapper.Map<TerminalExcelUploadDto, TerminalExcelUpload>(terminal);
        }

        public static CarOwnersDto ToCarOwner(this CarOwner carOwners)
        {
            return Mapper.Map<CarOwner, CarOwnersDto>(carOwners);
        }

        public static CarOwner ToCarOwner(this CarOwnersDto carOwners)
        {
            return Mapper.Map<CarOwnersDto, CarOwner>(carOwners);
        }

        public static IEnumerable<CarOwnersDto> ToCarOwnerList(this IEnumerable<CarOwner> carOwners)
        {
            return Mapper.Map<IEnumerable<CarOwner>, IEnumerable<CarOwnersDto>>(carOwners);
        }

        public static IEnumerable<PayrollRportSageResult> ToPayrollSageReport(this IEnumerable<usp_GetPayrollSAGETransaction_Result> trx)
        {
            return Mapper.Map<IEnumerable<usp_GetPayrollSAGETransaction_Result>, IEnumerable<PayrollRportSageResult>>(trx);
        }

        public static IEnumerable<CarOwnersDto> ToPayCarOwnerList(this IEnumerable<CarOwner> carOwners)
        {
            return Mapper.Map<IEnumerable<CarOwner>, IEnumerable<CarOwnersDto>>(carOwners);
        }

        public static IEnumerable<PayrollRportPaymentResult> ToPayrollPaymentReport(this IEnumerable<usp_GetPayrollPaymentTransaction_Result> trx)
        {
            return Mapper.Map<IEnumerable<usp_GetPayrollPaymentTransaction_Result>, IEnumerable<PayrollRportPaymentResult>>(trx);
        }

        public static ManyTxnClass[] ToVarifiedMany(this ManyTxnClassDto[] transactionView)
        {
            return Mapper.Map<ManyTxnClassDto[], ManyTxnClass[]>(transactionView);
        }

        public static PageSizeSearch ToPageSizeSearch(this PageSizeSearchDto pageSize)
        {
            return Mapper.Map<PageSizeSearchDto, PageSizeSearch>(pageSize);
        }

        public static IEnumerable<AllMerchantsDto> ToDTOList(this IEnumerable<AllMerchants> merchant) // converting MerchantUser to MerchantUserDtoList
        {
            return Mapper.Map<IEnumerable<AllMerchants>, IEnumerable<AllMerchantsDto>>(merchant);
        }

        public static DownloadParameters ToDownloadDriver(this DownloadParametersDto pageSize)
        {
            return Mapper.Map<DownloadParametersDto, DownloadParameters>(pageSize);
        }

        public static IEnumerable<DownloadDriverDataDto> ToDTOList(this IEnumerable<usp_GetTransactionData_Result> downloadData) // converting MerchantUser to MerchantUserDtoList
        {
            return Mapper.Map<IEnumerable<usp_GetTransactionData_Result>, IEnumerable<DownloadDriverDataDto>>(downloadData);
        }

        public static SurchargeFleetFeeDto ToSurchargeFleetFee(this SurchargeFleetFee fleetSetting)   //created by asif
        {
            return Mapper.Map<SurchargeFleetFee, SurchargeFleetFeeDto>(fleetSetting);
        }

        public static UKMTDTransaction ToSemiIntigratedTxn(this MtdtxnSemiUKDto semiTxn)
        {
            return Mapper.Map<MtdtxnSemiUKDto, UKMTDTransaction>(semiTxn);
        }

        public static UKMtdtxnSattlement ToSemiSattelementTxn(this MtdtxnSattlementUKDto semiTxn)
        {
            return Mapper.Map<MtdtxnSattlementUKDto, UKMtdtxnSattlement>(semiTxn);
        }

        public static CardTokenDto ToCardToken(this CardToken crdTkn)
        {
            return Mapper.Map<CardToken, CardTokenDto>(crdTkn);
        }

        public static CapKey ToCapKey(this CapKeyDto cpK)
        {
            return Mapper.Map<CapKeyDto, CapKey>(cpK);
        }

        public static CapKeyDto ToCapKeyDto(this CapKey cpK)
        {
            return Mapper.Map<CapKey, CapKeyDto>(cpK);
        }

        public static TAKey ToTAKey(this TAKeyDto cpK)
        {
            return Mapper.Map<TAKeyDto, TAKey>(cpK);
        }

        public static TAKeyDto ToCapKeyDto(this TAKey cpK)
        {
            return Mapper.Map<TAKey, TAKeyDto>(cpK);
        }  

        public static TransactionCountSlab ToDTO(this TransactionCountSlabDto txnSlab)
        {
            return Mapper.Map<TransactionCountSlabDto, TransactionCountSlab>(txnSlab);
        }

        public static TransactionCountSlabDto ToDefaultDto(this TransactionCountSlab defaultSetting)
        {
            return Mapper.Map<TransactionCountSlab, TransactionCountSlabDto>(defaultSetting);
        }

        //Report on demand
        public static IEnumerable<GetAdminSummaryReportDto> ToAdminSummaryList(this IEnumerable<usp_GetAdminSummaryReport_Result> summaryData)
        {
            return Mapper.Map<IEnumerable<usp_GetAdminSummaryReport_Result>, IEnumerable<GetAdminSummaryReportDto>>(summaryData);
        }

        public static IEnumerable<GetCardTypeReportDto> ToCardTypeList(this IEnumerable<usp_GetCardTypeReport_Result> cardData)
        {
            return Mapper.Map<IEnumerable<usp_GetCardTypeReport_Result>, IEnumerable<GetCardTypeReportDto>>(cardData);
        }

        public static IEnumerable<GetMerchantCardTypeReportDto> ToCardTypeListMerchant(this IEnumerable<usp_GetMerchantCardTypeReport_Result> cardData)
        {
            return Mapper.Map<IEnumerable<usp_GetMerchantCardTypeReport_Result>, IEnumerable<GetMerchantCardTypeReportDto>>(cardData);
        }

        public static IEnumerable<GetAdminExceptionReportDto> ToAdminExceptionList(this IEnumerable<usp_GetAdminExceptionReport_Result> summaryData)
        {
            return Mapper.Map<IEnumerable<usp_GetAdminExceptionReport_Result>, IEnumerable<GetAdminExceptionReportDto>>(summaryData);
        }

        public static IEnumerable<GetTotalTransByVehicleReportDto> ToTransByVehicleList(this IEnumerable<usp_GetTotalTransByVehicleReport_Result> cardData)
        {
            return Mapper.Map<IEnumerable<usp_GetTotalTransByVehicleReport_Result>, IEnumerable<GetTotalTransByVehicleReportDto>>(cardData);
        }

        public static IEnumerable<GetMerchantSummaryReportDto> ToMerchantSummaryList(this IEnumerable<usp_GetMerchantSummaryReport_Result> summaryData)
        {
            return Mapper.Map<IEnumerable<usp_GetMerchantSummaryReport_Result>, IEnumerable<GetMerchantSummaryReportDto>>(summaryData);
        }

        public static IEnumerable<GetDetailReportDto> ToDetailsList(this IEnumerable<usp_GetDetailReport_Result> detailsData)
        {
            return Mapper.Map<IEnumerable<usp_GetDetailReport_Result>, IEnumerable<GetDetailReportDto>>(detailsData);
        }
        //
        public static IEnumerable<GetMerchantExceptionReportDto> ToMerchantExecptionReportList(this IEnumerable<usp_GetMerchantExceptionReport_Result> summaryData)
        {
            return Mapper.Map<IEnumerable<usp_GetMerchantExceptionReport_Result>, IEnumerable<GetMerchantExceptionReportDto>>(summaryData);
        }

        public static IEnumerable<GetMerchantExceptionReportDto> ToMerchantExecptionReportList(this IEnumerable<usp_GetMerchantUserExceptionReport_Result> summaryData)
        {
            return Mapper.Map<IEnumerable<usp_GetMerchantUserExceptionReport_Result>, IEnumerable<GetMerchantExceptionReportDto>>(summaryData);
        }

        public static IEnumerable<GetMerchantExceptionReportDto> ToCorpExecptionReportList(this IEnumerable<usp_GetCorpUserExceptionReport_Result> summaryData)
        {
            return Mapper.Map<IEnumerable<usp_GetCorpUserExceptionReport_Result>, IEnumerable<GetMerchantExceptionReportDto>>(summaryData);
        }

        public static IEnumerable<GetMerchantTotalTransByVehicleReportDto> ToMerchantVehicleList(this IEnumerable<usp_GetMerchantTotalTransByVehicleReport_Result> detailsData)
        {
            return Mapper.Map<IEnumerable<usp_GetMerchantTotalTransByVehicleReport_Result>, IEnumerable<GetMerchantTotalTransByVehicleReportDto>>(detailsData);
        }

        //On Demand Merchant users
        public static IEnumerable<GetCardTypeReportDto> ToMuCardTypeList(this IEnumerable<usp_GetMerchantUserCardTypeReport_Result> cardData)
        {
            return Mapper.Map<IEnumerable<usp_GetMerchantUserCardTypeReport_Result>, IEnumerable<GetCardTypeReportDto>>(cardData);
        }

        public static IEnumerable<GetDetailReportDto> TomuDetailsList(this IEnumerable<usp_GetMerchantUserDetailReport_Result> detailsData)
        {
            return Mapper.Map<IEnumerable<usp_GetMerchantUserDetailReport_Result>, IEnumerable<GetDetailReportDto>>(detailsData);
        }

        public static IEnumerable<GetMerchantSummaryReportDto> ToMerchantUserSummaryList(this IEnumerable<usp_GetMerchantUserSummaryReport_Result> summaryData)
        {
            return Mapper.Map<IEnumerable<usp_GetMerchantUserSummaryReport_Result>, IEnumerable<GetMerchantSummaryReportDto>>(summaryData);
        }

        public static IEnumerable<GetMerchantTotalTransByVehicleReportDto> ToMerchantUserVehicleList(this IEnumerable<usp_GetMerchantUserTotalTransByVehicleReport_Result> detailsData)
        {
            return Mapper.Map<IEnumerable<usp_GetMerchantUserTotalTransByVehicleReport_Result>, IEnumerable<GetMerchantTotalTransByVehicleReportDto>>(detailsData);
        }

        //Corp Users
        public static IEnumerable<GetTotalTransByVehicleReportDto> ToCorpUserVehicleList(this IEnumerable<usp_CorpUserGetTotalTransByVehicleReport_Result> detailsData)
        {
            return Mapper.Map<IEnumerable<usp_CorpUserGetTotalTransByVehicleReport_Result>, IEnumerable<GetTotalTransByVehicleReportDto>>(detailsData);
        }

        public static IEnumerable<GetCardTypeReportDto> ToCorpCardTypeList(this IEnumerable<usp_GetCorpUserCardTypeReport_Result> cardData)
        {
            return Mapper.Map<IEnumerable<usp_GetCorpUserCardTypeReport_Result>, IEnumerable<GetCardTypeReportDto>>(cardData);
        }

        public static IEnumerable<GetAdminWeeklySummaryCountDto> ToWeeklyList(this IEnumerable<usp_GetAdminWeeklySummaryCount_Result> receipt)
        {
            return Mapper.Map<IEnumerable<usp_GetAdminWeeklySummaryCount_Result>, IEnumerable<GetAdminWeeklySummaryCountDto>>(receipt);
        }

        public static ServiceDiscoveryParametersData ToServiceDiscovery(this ServiceDiscoveryParametersDto serviceProvider)
        {
            return Mapper.Map<ServiceDiscoveryParametersDto, ServiceDiscoveryParametersData>(serviceProvider);
        }

        public static ServiceProviderUrlData ToServiceProvider(this ServiceProviderUrlDto serviceProviderDto)
        {
            return Mapper.Map<ServiceProviderUrlDto, ServiceProviderUrlData>(serviceProviderDto);
        }

        public static DatawireServiceUrlData ToServiceProvider(this DatawireServiceUrlDto serviceProviderDto)
        {
            return Mapper.Map<DatawireServiceUrlDto, DatawireServiceUrlData>(serviceProviderDto);
        }
        public static IEnumerable<WeeklyDollarVolumeDto> ToWeeklyDollarVolumeList(this IEnumerable<usp_GetAdminWeeklyDollarVolume_Result> weeklyReport)
        {
            return Mapper.Map<IEnumerable<usp_GetAdminWeeklyDollarVolume_Result>, IEnumerable<WeeklyDollarVolumeDto>>(weeklyReport);
        }
    


    }
}
