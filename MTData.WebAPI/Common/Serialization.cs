﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace MTData.WebAPI.Common
{
    public static class Serialization
    {
        //Default value of document declaration at the time of serialization
        private static string _documentTypeName = "Request";
        private static string _publisherID = "-//Datawire Communication Networks INC//DTD VXN XML Version 3.0//EN";
        private static string _systemID = "http://www.datawire.net/xmldtd/dwxmlapi3.dtd";
        private static string _subsetID = null;

        //static Serialization()
        //{
        //Value can be overridden in constructor of the document declaration 
        //}
        /// <summary>
        /// Serialize youType to string which create the string from UTF-8
        /// and return its types string XML to the caller.
        /// If we did not pass the includeNamespace it will include the namespace in the returned XML string
        /// </summary>
        /// <param name="yourType">Parameter need to pass to serialize</param>
        /// <param name="includeNameSpace">its a default valued parameter for including namespace in it  </param>
        /// /// <param name="isUtf8">specifies the parameter if you want any other encoding from utf-8</param>
        /// <returns>Return the serialized XML string of you object</returns>    
        public static string Serialize<T>(T yourType, bool includeNameSpace = false, bool includeDeclaration = true, bool isUtf8 = true)
        {
            
            try
            {
                var xmlSerializer = new XmlSerializer(yourType.GetType());
                using (var ms = new MemoryStream())
                {
                    
                    //settings.Encoding = new UnicodeEncoding(false, false);
                    if (isUtf8) //Required for Datawire communication
                    {
                        XmlWriterSettings settings = new XmlWriterSettings();
                        settings.Encoding = new UTF8Encoding(false);
                        settings.Indent = true;
                        settings.NewLineOnAttributes = true;
                        var xw = XmlWriter.Create(ms, settings);
                        if (includeDeclaration)
                        {
                            xw.WriteDocType(_documentTypeName, _publisherID, _systemID, _subsetID);
                        }

                        if (includeNameSpace)
                        {
                            xmlSerializer.Serialize(xw, yourType); //Default namespace will be included at the root element of the returned xml string.
                        }
                        else
                        {
                            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                            ns.Add("", "");
                            xmlSerializer.Serialize(xw, yourType, ns);
                        }
                        return Encoding.UTF8.GetString(ms.ToArray());
                    }
                    else //serialize the type in other encoding utf-16
                    {                        
                        StringWriter sw = new StringWriter();
                        XmlTextWriter tw = new XmlTextWriter(sw);
                        xmlSerializer.Serialize(sw, yourType);
                        return sw.ToString();
                    }    
                }
            }
            catch (SerializationException ex)
            {
                
                throw;
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }
        /// <summary>
        /// Deserialized your XML to the type which was used for serialization
        /// and return the type which you want to convert by passing as generic type
        /// </summary>
        /// <typeparam name="T">Name of type on which you want your type to deserialized</typeparam>
        /// <param name="xmlString">Source serialized xml string of your type</param>        
        public static T Deserialize<T>(string xmlString)
        {
            
            try
            {
                var xmlSerializer = new XmlSerializer(typeof(T));
                using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(xmlString)))
                {
                    var xr = XmlReader.Create(ms, new XmlReaderSettings { DtdProcessing = DtdProcessing.Ignore });
                    return (T)xmlSerializer.Deserialize(xr);
                }
            }
            catch (SerializationException ex)
            { 
                throw;
            }
            catch (Exception ex)
            {
              
                throw;
            }
        }
    }
}
