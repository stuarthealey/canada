﻿using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    
    [DataContract(Namespace = "")]
    public class MailResponse
    {
        [DataMember]
        public LoginHeader Header { get; set; }
       
    }
}