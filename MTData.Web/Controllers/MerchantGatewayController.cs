﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Web.ViewData;
using MTData.Utility;
using MTData.Web.Resource;

namespace MTData.Web.Controllers
{
    public class MerchantGatewayController : Controller
    {
        readonly ILogger _logger = new Logger();
        private readonly IMerchantGatewayService _merchantGatewayService;
        private readonly ICommonService _commonService;
        private StringBuilder _logMessage;
        private StringBuilder _logTracking;

        public MerchantGatewayController([Named("MerchantGatewayService")] IMerchantGatewayService gatewayService, [Named("CommonService")] ICommonService commomService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _merchantGatewayService = gatewayService;
            _commonService = commomService;
            _logMessage = logMessage;
            _logTracking = logTracking;
        }

        /// <summary>
        /// Purpose             :   to display Merchant gateway
        /// Function Name       :   Index
        /// Created By          :   Naveen Kumar 
        /// Created On          :   1/19/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("index", "Member");

            return View();
        }

        /// <summary>
        /// Purpose             :   Creating Merchant gateway
        /// Function Name       :   Create
        /// Created By          :    Naveen Kumar 
        /// Created On          :   1/19/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("using object initilizar for MerchantGatewayViewData and getting list of Gateway and TypeList");

                var gatewayView = new MerchantGatewayViewData
                {
                    GatewayList = GatewayList(),
                    TypeList = TypeList(),
                    GatewayType = 2
                };

                _logMessage.Append("list of Gateway and TypeList successfully bind");

                return View(gatewayView);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   Creating Merchant gateway
        /// Function Name       :   Create
        /// Created By          :     Naveen Kumar  
        /// Created On          :   1/20/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gatewayViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(MerchantGatewayViewData gatewayViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

            try
            {
                var httpCookie = Request.Cookies["CookieViewAs"];
                if (httpCookie != null)
                    gatewayViewData.fk_MerchantID = Convert.ToInt32(httpCookie["merchantId"]);

                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("getting list of Gateway and TypeList");

                gatewayViewData.GatewayList = GatewayList();
                gatewayViewData.TypeList = TypeList();

                _logMessage.Append("list of Gateway and TypeList successfully bind");

                if (ModelState.IsValid)
                {
                    gatewayViewData.IsMtdataGateway = FindGatewayType(gatewayViewData);

                    _logMessage.Append("calling ToMerchantGatewayDto method to map MerchantGatewayDto to MerchantGatewayViewData and retrun MerchantGatewayDto");

                    var gatewayDto = gatewayViewData.ToMerchantGatewayDto();
                    _logMessage.Append("ToMerchantGatewayDto method executed successfully");

                    gatewayDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                    _logMessage.Append("Calling Add method of _merchantGatewayService class takes gatewayDto, gatewayDto properties Company = " + gatewayViewData.Company + ", Created By = " + gatewayViewData.CreatedBy + ", Gateway Type = " + gatewayViewData.GatewayType + ", IsActive= " + gatewayViewData.IsActive + ", Is Mtdata Gateway= " + gatewayViewData.IsMtdataGateway + ", Name= " + gatewayViewData.Name + ", UserName = " + gatewayViewData.UserName + ", Password= " + gatewayViewData.PCode);

                    bool isExist = _merchantGatewayService.Add(gatewayDto);
                    if (isExist)
                    {
                        _logMessage.Append("Default gateway has been already set returning to view");
                        ViewBag.Message = MtdataResource.default_gateway;
                        return View(gatewayViewData);
                    }

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + " add  merchant gateway" + gatewayDto.Name+ " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                    _logMessage.Append("Add method of _merchantGatewayService class executed successfully");

                    return RedirectToAction("Index");
                }

                return View(gatewayViewData);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   updating Merchant gateway
        /// Function Name       :  update
        /// Created By          :   Naveen Kumar 
        /// Created On          :   1/20/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantGatewayId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Update(int merchantGatewayId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetMerchantGatewayDto method of _merchantGatewayService class takes merchantGatewayId = " + merchantGatewayId);

                var gatewayDto = _merchantGatewayService.GetMerchantGatewayDto(merchantGatewayId);

                _logMessage.Append("GetMerchantGatewayDto method of _merchantGatewayService executed successfully");
                _logMessage.Append("calling ToMerchantGatewayViewData method to map MerchantGatewayViewData to MerchantGatewayDto and retrun MerchantGatewayViewData");

                MerchantGatewayViewData gatewayViewData = gatewayDto.ToMerchantGatewayViewData();

                _logMessage.Append("ToMerchantGatewayViewData method executed successfully");
                _logMessage.Append("getting list of Gateway and TypeList");

                gatewayViewData.GatewayList = GatewayList();
                gatewayViewData.TypeList = TypeList();

                _logMessage.Append("list of Gateway and TypeList successfully bind returing to view");

                return View(gatewayViewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   updating Merchant gateway
        /// Function Name       :   update
        /// Created By          :   Asif Shafeeque 
        /// Created On          :  1/20/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantGatewayId"></param>
        /// <param name="gatewayViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(int merchantGatewayId, MerchantGatewayViewData gatewayViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("index", "Member");

            try
            {
                var httpCookie = Request.Cookies["CookieViewAs"];
                if (httpCookie != null)
                    gatewayViewData.fk_MerchantID = Convert.ToInt32(httpCookie["merchantId"]);

                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("getting list of Gateway and TypeList");

                gatewayViewData.GatewayList = GatewayList();
                gatewayViewData.TypeList = TypeList();

                _logMessage.Append("list of Gateway and TypeList successfully bind");

                if (ModelState.IsValid)
                {
                    gatewayViewData.IsMtdataGateway = FindGatewayType(gatewayViewData);
                    MerchantGatewayDto gatewayDto = gatewayViewData.ToMerchantGatewayDto();
                    gatewayDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                    _logMessage.Append("Calling Update method of _merchantGatewayService class takes gatewayDto, gatewayDto properties Company = " + gatewayViewData.Company + ", Created By = " + gatewayViewData.CreatedBy + ", Gateway Type = " + gatewayViewData.GatewayType + ", IsActive= " + gatewayViewData.IsActive + ", Is Mtdata Gateway= " + gatewayViewData.IsMtdataGateway + ", Name= " + gatewayViewData.Name + ", UserName = " + gatewayViewData.UserName + ", Password= " + gatewayViewData.PCode + " and merchantGatewayId= " + merchantGatewayId);

                    bool isExist = _merchantGatewayService.Update(gatewayDto, merchantGatewayId);
                    if (isExist)
                    {
                        ViewBag.Message = MtdataResource.default_gateway;
                        _logMessage.Append("Default gateway has been already set returning to view");
                        return View(gatewayViewData);
                    }

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + "update merchant gateway" + gatewayDto .Name+ " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                    _logMessage.Append("Update method of _merchantGatewayService class executed successfully");

                    return RedirectToAction("Index");
                }

                return View(gatewayViewData);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :  to get the list of gateway
        /// Function Name       :   GatewayList
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   1/22/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<GatewayListViewData> GatewayList()
        {
            var gatewayDtoList = _commonService.GetGatewayList();
            var gatewayList = gatewayDtoList.ToGatewayViewDataList();

            return gatewayList;
        }

        /// <summary>
        /// Purpose             :   to get the types of merchant gateway list
        /// Function Name       :   TypeList
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   1/22/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public static IEnumerable<SurchargeTypeViewData> TypeList()
        {
            var listSurchargeType = new List<SurchargeTypeViewData>
            {
                new SurchargeTypeViewData { TypeID = 0, TypeName = "Personal" },new SurchargeTypeViewData { TypeID = 1, TypeName = "MTData" }
            };

            return listSurchargeType;
        }

        /// <summary>
        /// Purpose             :  to find merchant gateway type
        /// Function Name       :   FindGatewayType
        /// Created By          :   Asif Shafeeque 
        /// Created On          :  1/23/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gateway"></param>
        /// <returns></returns>
        [NonAction]
        public static bool FindGatewayType(MerchantGatewayViewData gateway)
        {
            return gateway.GatewayType != 0;
        }

        /// <summary>
        /// Purpose             :   to find merchant gateway id
        /// Function Name       :   FindGatewayTypeId
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   1/24/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gateway"></param>
        /// <returns></returns>
        [NonAction]
        public static int FindGatewayTypeId(MerchantGatewayViewData gateway)
        {
            return gateway.IsMtdataGateway == false ? 0 : 1;
        }

        /// <summary>
        /// Purpose             :   Apply page indexing
        /// Function Name       :   MerchantGateListByFilter
        /// Created By          :   Asif Shafeeque 
        /// Created On          :    2/5/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult MerchantGateListByFilter(string name = null, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                int merchantId = 0;

                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var httpCookie = Request.Cookies["CookieViewAs"];
                if (httpCookie != null)
                    merchantId = Convert.ToInt32(httpCookie["merchantId"]);

                int gatewayDtoListCount;

                _logMessage.Append("Calling MerchantGateListByFilter method of _merchantGatewayService class to get gateway List");

                FilterSearchParametersDto filterSearch = new FilterSearchParametersDto()
                {
                    Name = name,
                    StartIndex = jtStartIndex,
                    PageSize = jtPageSize,
                    Sorting = jtSorting,
                    MerchantId = merchantId
                };

                var gateways = _merchantGatewayService.MerchantGateListByFilter(filterSearch);
                _logMessage.Append("MerchantGateListByFilter of _merchantGatewayService executed successfully");

                if (string.IsNullOrEmpty(name))
                {
                    _logMessage.Append("Calling GetMerchantGatewayDtoList method of _merchantGatewayService class to count gateway");

                    gatewayDtoListCount = _merchantGatewayService.GetMerchantGatewayDtoList(merchantId).Count();

                    _logMessage.Append("GetMerchantGatewayDtoList method of _merchantGatewayService executed successfully result = " + gatewayDtoListCount);

                    return Json(new { Result = "OK", Records = gateways, TotalRecordCount = gatewayDtoListCount });
                }

                _logMessage.Append("converting merchantGatewayDtos to toarray");
                var merchantGatewayDtos = gateways as MerchantGatewayDto[] ?? gateways.ToArray();
                _logMessage.Append("conversion merchantGatewayDtos to toarray successful");

                gatewayDtoListCount = merchantGatewayDtos.ToList().Count();

                return Json(new { Result = "OK", Records = merchantGatewayDtos, TotalRecordCount = gatewayDtoListCount });
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);

                return Json(new { Result = "ERROR", exception.Message });
            }
        }

        /// <summary>
        /// Purpose             :   Apply page indexing
        /// Function Name       :   Delete
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   2/5/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantGatewayId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(int merchantGatewayId)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return Json(new { Result = "No" });
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling DeleteMerchantGateway method of _merchantGatewayService class takes merchantGatewayId= " + merchantGatewayId);

                _merchantGatewayService.DeleteMerchantGateway(merchantGatewayId);

                _logMessage.Append("DeleteMerchantGateway method of _merchantGatewayService class executed successfully");

                Thread.Sleep(50);

                string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                _logTracking.Append("User " + userName + " delete merchant gateway ID" + merchantGatewayId + " at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());

                return Json(new { Result = "OK" });
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return Json(new { Result = "No" });
        }

    }
}