﻿using System.Collections.Generic;
using MTD.Core.DataTransferObjects;
using MTD.Core.Extension;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;
using Ninject;

namespace MTD.Core.Service
{
    class SurchargeService : ISurchargeService
    {
        private readonly ISurchargeRepository _surchargeRepository;

        public SurchargeService([Named("SurchargeRepository")] ISurchargeRepository surchargeRepository)
        {
            _surchargeRepository = surchargeRepository;
        }

        /// <summary>
        /// Purpose             :   To Add Surcharge
        /// Function Name       :   Add
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/25/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="surchargeDto"></param>
        /// <returns></returns>
        public bool Add(SurchargeDto surchargeDto)
        {
            try
            {
                Surcharge surcharge = surchargeDto.ToSurcharge();
                return _surchargeRepository.Add(surcharge);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Surcharge List
        /// Function Name       :   GetSurchargeDtoList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/25/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SurchargeDto> GetSurchargeDtoList()
        {
            try
            {
                var surchargeList = _surchargeRepository.SurchargeList();
                var surchargeDtoList = surchargeList.ToDTOSurchargeList();
                return surchargeDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Surcharge
        /// Function Name       :   GetSurchargeDto
        /// Created By          :   Naveen Kumar
        /// Created On          :   02/25/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="surchargeId"></param>
        /// <returns></returns>
        public SurchargeDto GetSurchargeDto(int surchargeId)
        {
            try
            {
                var surcharge = _surchargeRepository.GetSurcharge(surchargeId);
                var surchargeDto = surcharge.ToSurchargeDto();
                return surchargeDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update Surcharge
        /// Function Name       :   Update
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/26/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="surchargeDto"></param>
        /// <param name="surchargeId"></param>
        /// <returns></returns>
        public bool Update(SurchargeDto surchargeDto, int surchargeId)
        {
            try
            {
                Surcharge surcharge = surchargeDto.ToSurcharge();
                return _surchargeRepository.Update(surcharge, surchargeId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Delete Surcharge
        /// Function Name       :   DeleteSurcharge
        /// Created By          :   Salil Gupta
        /// Created On          :   02/25/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="surchargeId"></param>
        public void DeleteSurcharge(int surchargeId)
        {
            try
            {
                _surchargeRepository.DeleteSurcharge(surchargeId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Find Surcharge
        /// Function Name       :   FindSurcharge
        /// Created By          :   Salil Gupta
        /// Created On          :   02/27/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gatewayId"></param>
        /// <returns></returns>
        public bool FindSurcharge(int gatewayId)
        {
            try
            {
                return _surchargeRepository.FindSurcharge(gatewayId);
            }
            catch
            {
                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To get Surcharges List
        /// Function Name       :   SurchargeListByFiter
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/28/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="filterSearch"></param>
        /// <returns></returns>
        public IEnumerable<SurchargeDto> SurchargeListByFiter(FilterSearchParametersDto filterSearch)
        {
            try
            {
                var filterSearchClass = filterSearch.SearchParameters();
                var surchargeList = _surchargeRepository.SurchargeListByFiter(filterSearchClass);
                var surchargeDtoList = surchargeList.ToDTOSurchargeList();
                return surchargeDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Surcharge
        /// Function Name       :   Surcharge By MerchantID
        /// Created By          :   Sunil Singh
        /// Created On          :   06/04/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<SurchargeDto> SurchargeByMerchantId(int merchantId)
        {
            IEnumerable<SurchargeList> surcharge = _surchargeRepository.SurchargeListByMerchantId(merchantId);
            IEnumerable<SurchargeDto> rDto = surcharge.ToDTODefaultList();
            return rDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterSearch"></param>
        /// <returns></returns>
        public int SurchargeListCount(FilterSearchParametersDto filterSearch)
        {
            try
            {
                return _surchargeRepository.SurchargeListCount(filterSearch.SearchParameters());
            }
            catch
            {
                throw;
            }
        }


    }
}