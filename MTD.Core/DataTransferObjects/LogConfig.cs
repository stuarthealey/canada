﻿namespace MTD.Core.DataTransferObjects
{
    public class LogConfig
    {
        public int LogConfigId { get; set; }
        public int? FkMerchantId { get; set; }
        public bool? IsCcSurcharge { get; set; }
        public bool? IsTipPerLow { get; set; }
        public bool? IsTipPerMedium { get; set; }
        public bool? IsTipPerHigh { get; set; }
    }
}