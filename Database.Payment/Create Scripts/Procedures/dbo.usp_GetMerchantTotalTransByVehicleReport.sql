IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetMerchantTotalTransByVehicleReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_GetMerchantTotalTransByVehicleReport]
GO

CREATE PROCEDURE dbo.usp_GetMerchantTotalTransByVehicleReport
-- =============================================
-- Author:	<Naveen Kumar>
-- Create date: <10-03-2016>
-- Description:	<Get merchant total trans by vehicle report>
-- =============================================
( 
	@DateFrom DATETIME,
	@DateTo DATETIME,
	@MerchantId INT
)
AS
BEGIN
	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SET ANSI_WARNINGS OFF
		SET NOCOUNT ON

		DECLARE  @TransCountSlablt1 INT,
				 @TransCountSlabGt2 INT,
				 @TransCountSlablt2 INT,
				 @TransCountSlabGt3 INT,
				 @TransCountSlablt3 INT,
				 @TransCountSlabGt4 INT

		DECLARE @InnerTable TABLE (
			Id INT, 
			FareValue MONEY,
			Tip MONEY, 
			Amount MONEY, 
			Surcharge MONEY, 
			Fee MONEY,
			VehicleNo NVARCHAR(70), 
			TxnType NVARCHAR(25),
			ResponseCode NVARCHAR(30),
			fk_FleetId INT,
			IsRefunded BIT,
			IsVoided BIT,
			IsCompleted BIT,
			TechFee MONEY,
			FleetFee MONEY,
			MerchantId INT
			)

		--Insert data into table variable InnnerTable for reuse                      
		 INSERT INTO @InnerTable (Id, FareValue, Tip, Amount, Surcharge, Fee, VehicleNo, TxnType, ResponseCode, fk_FleetId, IsRefunded, IsVoided, IsCompleted, TechFee, FleetFee, MerchantId)                   
		 SELECT MT.Id, MT.FareValue, MT.Tip, MT.Amount, MT.Surcharge, MT.Fee, MT.VehicleNo, MT.TxnType, MT.ResponseCode, MT.fk_FleetId, MT.IsRefunded, MT.IsVoided, MT.IsCompleted, MT.TechFee, MT.FleetFee, MT.MerchantId
		 FROM dbo.MTDTransaction MT WITH (INDEX(IX_MTDTransactionTxnDate))
			INNER JOIN dbo.Vehicle VH ON VH.VehicleNumber = MT.VehicleNo AND VH.IsActive = 1
			INNER JOIN dbo.Fleet FL ON FL.FleetID = VH.fk_FleetID AND FL.IsActive = 1
		WHERE CAST(MT.TxnDate AS DATE) >= @DateFrom
		  AND CAST(MT.TxnDate AS DATE) < @DateTo
		  AND MT.fk_FleetId = VH.fk_FleetID

		SELECT  @TransCountSlablt1 = TransCountSlablt1,
				@TransCountSlabGt2 = TransCountSlabGt2,
				@TransCountSlablt2 = TransCountSlablt2,
				@TransCountSlabGt3 = TransCountSlabGt3,
				@TransCountSlablt3 = TransCountSlablt3,
				@TransCountSlabGt4 = TransCountSlabGt4
		FROM dbo.TransactionCountSlab
		WHERE fk_MerchantId = @MerchantId

		SELECT	COUNT(Id) AS 'Id', 
				VehicleNo AS 'VehicleNo',
				fk_FleetId AS 'fk_FleetId' 
		INTO #temp 
		FROM @InnerTable it 
		WHERE it.ResponseCode IN ('000', '00', '002') 
		  AND it.MerchantId = @MerchantId
		GROUP BY it.VehicleNo, it.fk_FleetId	

		--Prepare data for summary report in xml format                            
		SELECT	ROW_NUMBER() OVER (ORDER BY FleetName) AS 'SrNo',
				ISNULL(vh.FleetName,'')  AS 'FleetName',
				(SELECT ISNULL(COUNT(Id),0) FROM @InnerTable it WHERE it.fk_FleetId=vh.FleetID AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND it.IsVoided=0 AND it.ResponseCode IN ('000', '00', '002')) AS 'NoOfTrans',
				CONCAT('$',(SELECT ISNULL(SUM(FareValue),'0.00') FROM @InnerTable it WHERE it.fk_FleetId=vh.FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'FareAmt',
				CONCAT('$',(SELECT ISNULL(SUM(Tip),'0.00') FROM @InnerTable it WHERE it.fk_FleetId=vh.FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND  it.ResponseCode IN ('000', '00', '002'))) AS 'Tips',
				CONCAT('$',(SELECT ISNULL(SUM(surcharge),'0.00')FROM @InnerTable it WHERE it.fk_FleetId=vh.FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'Surcharges',
				CONCAT('$',(SELECT ISNULL(SUM(TechFee),'0.00')FROM @InnerTable it WHERE it.fk_FleetId=vh.FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'TechFee',
				(SELECT ISNULL(COUNT(id),0) FROM @InnerTable it WHERE it.fk_FleetId=vh.FleetID AND it.TxnType='Refund' AND it.ResponseCode IN ('000', '00')) AS 'NoOfChargebacks',
				CONCAT('$',(SELECT ISNULL(SUM(Amount),'0.00') FROM @InnerTable it WHERE it.fk_FleetId=vh.FleetID AND it.TxnType='Refund' AND it.ResponseCode IN ('000', '00'))) AS 'ChargebackValue',
				(SELECT ISNULL(COUNT(id),0) FROM @InnerTable it WHERE it.fk_FleetId=vh.FleetID AND it.TxnType='Void' AND it.ResponseCode IN ('000', '00')) AS 'NoOfVoid',
				CONCAT('$',(SELECT ISNULL(SUM(Amount),'0.00') FROM @InnerTable it WHERE it.fk_FleetId=vh.FleetID AND it.TxnType='Void' AND it.ResponseCode IN ('000', '00'))) AS 'VoidValue',
				CONCAT('$',(SELECT ISNULL(SUM(Amount),'0.00') FROM  @InnerTable it WHERE it.fk_FleetId=vh.FleetID AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND IT.IsVoided=0 AND it.ResponseCode IN ('000', '00', '002'))) AS 'TotalAmount',
				CONCAT('$',(SELECT ISNULL(SUM(Fee),'0.00')+ISNULL(SUM(FleetFee),'0.00') FROM @InnerTable it WHERE it.fk_FleetId=vh.FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'Fees',
				
				CONCAT('$',(SELECT ISNULL(SUM(Amount),'0.00') FROM  @InnerTable it WHERE it.fk_FleetId=vh.FleetID AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND IT.IsVoided=0 AND it.ResponseCode IN ('000', '00', '002'))-
					(SELECT ISNULL(SUM(Fee),'0.00')+ISNULL(SUM(FleetFee),'0.00') FROM @InnerTable it WHERE it.fk_FleetId=vh.FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'AmountOwing',
				
				(SELECT COALESCE(COUNT(t.VehicleNo),0) FROM #temp t WHERE t.fk_FleetId = vh.FleetID AND t.Id < @TransCountSlablt1) AS 'TransCountForSlab1',
				(SELECT COALESCE(COUNT(t.VehicleNo),0)  FROM #temp t WHERE t.fk_FleetId = vh.FleetID AND t.Id >= @TransCountSlabGt2 AND t.Id < @TransCountSlablt2) AS 'TransCountForSlab2',
				(SELECT COALESCE(COUNT(t.VehicleNo),0)  FROM #temp t WHERE t.fk_FleetId = vh.FleetID AND t.Id >= @TransCountSlabGt3 AND t.Id < @TransCountSlablt3) AS 'TransCountForSlab3',
				(SELECT COALESCE(COUNT(t.VehicleNo),0)  FROM #temp t WHERE t.fk_FleetId = vh.FleetID AND t.Id >= @TransCountSlabGt4) AS 'TransCountForSlab4'
		FROM dbo.MTDTransaction m 
			INNER JOIN (SELECT v.VehicleNumber AS 'VehicleNumber', v.fk_FleetID AS 'fk_FleetID', f.FleetID AS 'FleetID', f.FleetName AS 'FleetName', mer.Company AS 'Company'
						FROM (SELECT VehicleNumber, fk_FleetID FROM dbo.Vehicle WHERE IsActive = 1) v  
							INNER JOIN (SELECT FleetID, FleetName, fk_MerchantID FROM dbo.Fleet WHERE IsActive = 1) f ON v.fk_FleetID = f.FleetID
							INNER JOIN (SELECT MerchantID, Company FROM dbo.Merchant WHERE IsActive = 1) mer ON f.fk_MerchantID = mer.MerchantID 
						WHERE mer.MerchantID = @MerchantId) vh ON m.VehicleNo = vh.VehicleNumber 
			WHERE CAST(m.TxnDate AS DATE) >= @DateFrom 
			  AND CAST(m.TxnDate AS DATE) < @DateTo 
 			  AND m.fk_FleetId = vh.fk_FleetID
			  AND m.ResponseCode IN ('000', '00', '002') 
			  AND m.Id NOT IN (SELECT Id FROM dbo.MTDTransaction WHERE m.TxnType = 'Authorization' AND m.IsCompleted = 0)
			GROUP BY vh.FleetName, vh.FleetID, vh.Company	                    					   					        											    
			ORDER BY FleetName
	END TRY
	--use catch to log error with details in ProcErrorHandler table
	BEGIN CATCH
		 INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
		 SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
	END CATCH
END
GO
