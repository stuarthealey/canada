﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Data.Data
{
    public class PageSizeSearch
    {
        public string Name { get; set; }
        public int JtStartIndex { get; set; }
        public int JtPageSize { get; set; }
        public string JtSorting { get; set; }
        public int UserId { get; set; }
        public int UserType { get; set; }
        
    }
}
