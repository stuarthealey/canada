-- Update version number
------------------------
UPDATE dbo.SystemConfig
SET Version = '3.0.5.9'
WHERE SystemConfigID = (SELECT MAX(SystemConfigID) FROM dbo.SystemConfig)