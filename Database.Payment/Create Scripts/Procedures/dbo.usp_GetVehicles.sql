IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetVehicles]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_GetVehicles]
GO

CREATE PROCEDURE dbo.usp_GetVehicles
(
	@MerchantId INT
)
AS
BEGIN
	SELECT * 
	FROM dbo.Vehicle
	WHERE fk_FleetID IN (SELECT f.FleetID FROM dbo.Fleet f WHERE f.fk_MerchantID = @MerchantId AND IsActive = 1)
	  AND IsActive = 1
END
GO
