﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MTData.WCFGW.Model
{
	[DataContract(Namespace = "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService")]
	public class CreateRCCIResp
	{
		[DataMember]
		public ResultStatus Result { get; set; }

		[DataMember]
		public ValueNode RCCI { get; set; }

		[DataMember]
		public CreditCardStatus CreditCardStatus { get; set; }

		[DataMember]
		public TruncatedCreditCardDetails TruncatedCreditCardDetails { get; set; }
	}

	[DataContract(Namespace = "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService")]
	public class CreditCardStatus
	{
		[DataMember]
		public bool IsValid { get; set; }

		[DataMember]
		public string StatusDescription { get; set; }
	}

	[DataContract(Namespace = "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService")]
	public class TruncatedCreditCardDetails
	{
		[DataMember]
		public string TruncatedPAN { get; set; }

		[DataMember]
		public string ExpiryMonth { get; set; }

		[DataMember]
		public string ExpiryYear { get; set; }

		[DataMember]
		public string CardType { get; set; }
	}

	public enum ResultCode
	{
		Success,
		InvalidRCCI,
		InvalidCreditCard,
		Error,
	}

	[DataContract(Namespace = "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService")]
	public class ResultStatus
	{
		[DataMember]
		public ResultCode Code { get; set; }

		[DataMember]
		public string ErrorDescription { get; set; }
	}
}
