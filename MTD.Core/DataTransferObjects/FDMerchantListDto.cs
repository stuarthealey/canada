﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
    public class FDMerchantListDto
    {
        public int FDId { get; set; }
        public string FdMerchantId { get; set; }
    }
}
