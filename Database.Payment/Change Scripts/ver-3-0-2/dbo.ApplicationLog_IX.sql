-- Create indexes on ApplicationLog for performance.
--==================================================

IF NOT EXISTS (SELECT * FROM sysindexes si INNER JOIN sysobjects so ON so.id = si.id WHERE si.name = 'IX_ApplicationLogDate' AND OBJECTPROPERTY(so.id, N'IsUserTable') = 1 AND so.name = 'ApplicationLog')
BEGIN
	PRINT 'Creating Index IX_ApplicationLogDate on ApplicationLog'

	CREATE NONCLUSTERED INDEX IX_ApplicationLogDate ON dbo.ApplicationLog (Date, Id)
		WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
ELSE
BEGIN
	PRINT 'Index IX_ApplicationLogDate on ApplicationLog already exists'
END
