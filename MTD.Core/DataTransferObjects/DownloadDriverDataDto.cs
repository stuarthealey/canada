﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
    [DataContract(Namespace = "", Name = "TransactionData")]
    [XmlSerializerFormat]
    public class DownloadDriverDataDto
    {
        [DataMember]
        public int ID { get; set; }

        string _termId = string.Empty;
        [DataMember]
        public string TerminalID { get { return string.IsNullOrEmpty(_termId) ? string.Empty : _termId; } set { _termId = value; } }

        string _driverNo = string.Empty;
        [DataMember]
        public string DriverNo { get { return string.IsNullOrEmpty(_driverNo) ? string.Empty : _driverNo; } set { _driverNo = value; } }

        string _vehicleNo = string.Empty;
        [DataMember]
        public string VehicleNo { get { return string.IsNullOrEmpty(_vehicleNo) ? string.Empty : _vehicleNo; } set { _vehicleNo = value; } }

        string _jobNumbr = string.Empty;
        [DataMember]
        public string JobNumber { get { return string.IsNullOrEmpty(_jobNumbr) ? string.Empty : _jobNumbr; } set { _jobNumbr = value; } }

        string _crdHdrName = string.Empty;
        [DataMember]
        public string CrdHldrName { get { return string.IsNullOrEmpty(_crdHdrName) ? string.Empty : _crdHdrName; } set { _crdHdrName = value; } }

        string _crdPhone = string.Empty;
        [DataMember]
        public string CrdHldrPhone { get { return string.IsNullOrEmpty(_crdPhone) ? string.Empty : _crdPhone; } set { _crdPhone = value; } }

        string _crdCity = string.Empty;
        [DataMember]
        public string CrdHldrCity { get { return string.IsNullOrEmpty(_crdCity) ? string.Empty : _crdCity; } set { _crdCity = value; } }

        string _crdState = string.Empty;
        [DataMember]
        public string CrdHldrState { get { return string.IsNullOrEmpty(_crdState) ? string.Empty : _crdState; } set { _crdState = value; } }

        string _crdHdrZip = string.Empty;
        [DataMember]
        public string CrdHldrZip { get { return string.IsNullOrEmpty(_crdHdrZip) ? string.Empty : _crdHdrZip; } set { _crdHdrZip = value; } }

        string _crdAdrs = string.Empty;
        [DataMember]
        public string CrdHldrAddress { get { return string.IsNullOrEmpty(_crdAdrs) ? string.Empty : _crdAdrs; } set { _crdAdrs = value; } }

        string _txnRefNo = string.Empty;
        [DataMember]
        public string TransRefNo { get { return string.IsNullOrEmpty(_txnRefNo) ? string.Empty : _txnRefNo; } set { _txnRefNo = value; } }

        string _pType = string.Empty;
        [DataMember]
        public string PaymentType { get { return string.IsNullOrEmpty(_pType) ? string.Empty : _pType; } set { _pType = value; } }

        string _txnType = string.Empty;
        [DataMember]
        public string TxnType { get { return string.IsNullOrEmpty(_txnType) ? string.Empty : _txnType; } set { _txnType = value; } }

        decimal _fareVal = 0;
        [DataMember]
        public Nullable<decimal> FareValue { get { return _fareVal; } set { _fareVal = Math.Round(Convert.ToDecimal(value), 2, MidpointRounding.AwayFromZero); } }

        decimal _tip = 0;
        [DataMember]
        public Nullable<decimal> Tip { get { return _tip; } set { _tip = Math.Round(Convert.ToDecimal(value), 2, MidpointRounding.AwayFromZero); } }

        decimal _surCharg = 0;
        [DataMember]
        public Nullable<decimal> Surcharge { get { return _surCharg; } set { _surCharg = Math.Round(Convert.ToDecimal(value), 2, MidpointRounding.AwayFromZero); } }

        decimal _fee = 0;
        [DataMember]
        public Nullable<decimal> Fee { get { return _fee; } set { _fee = Math.Round(Convert.ToDecimal(value), 2, MidpointRounding.AwayFromZero); } }

        decimal _taxes = 0;
        [DataMember]
        public Nullable<decimal> Taxes { get { return _taxes; } set { _taxes = Math.Round(Convert.ToDecimal(value), 2, MidpointRounding.AwayFromZero); } }

        decimal _toll = 0;
        [DataMember]
        public Nullable<decimal> Toll { get { return _toll; } set { _toll = Math.Round(Convert.ToDecimal(value), 2, MidpointRounding.AwayFromZero); } }

        decimal _amount = 0;
        [DataMember]
        public Nullable<decimal> Amount { get { return _amount; } set { _amount = Math.Round(Convert.ToDecimal(value), 2, MidpointRounding.AwayFromZero); } }

        string _expDate = string.Empty;
        [DataMember(EmitDefaultValue = false)]
        public string ExpiryDate { get { return null; } set { _expDate = value; } }

        string _crdType = string.Empty;
        [DataMember]
        public string CardType { get { return string.IsNullOrEmpty(_crdType) ? string.Empty : _crdType; } set { _crdType = value; } }

        string _industry = string.Empty;
        [DataMember]
        public string Industry { get { return string.IsNullOrEmpty(_industry) ? string.Empty : _industry; } set { _industry = value; } }

        [DataMember]
        public string EntryMode { get; set; }

        [DataMember]
        public string ResponseCode { get; set; }

        [DataMember]
        public string AddRespData { get; set; }

        string _authId = string.Empty;
        [DataMember]
        public string AuthId { get { return string.IsNullOrEmpty(_authId) ? string.Empty : _authId; } set { _authId = value; } }

        string _srcid = string.Empty;
        [DataMember]
        public string SourceId { get { return string.IsNullOrEmpty(_srcid) ? string.Empty : _srcid; } set { _srcid = value; } }

        string _txnNote = string.Empty;
        [DataMember]
        public string TransNote { get { return string.IsNullOrEmpty(_txnNote) ? string.Empty : _txnNote; } set { _txnNote = value; } }

        [DataMember]
        public Nullable<bool> IsCompleted { get; set; }

        [DataMember]
        public Nullable<bool> IsRefunded { get; set; }

        string _pkAdrs = string.Empty;
        [DataMember]
        public string PickAddress { get { return string.IsNullOrEmpty(_pkAdrs) ? string.Empty : _pkAdrs; } set { _pkAdrs = value; } }

        string _dstAdrs = string.Empty;
        [DataMember]
        public string DestinationAddress { get { return string.IsNullOrEmpty(_dstAdrs) ? string.Empty : _dstAdrs; } set { _dstAdrs = value; } }

        [DataMember]
        public Nullable<System.DateTime> TxnDate { get; set; }

        string _firstFour = string.Empty;
        [DataMember]
        public string FirstFourDigits { get { return string.IsNullOrEmpty(_firstFour) ? string.Empty : _firstFour; } set { _firstFour = value; } }

        string _lastFour = string.Empty;
        [DataMember]
        public string LastFourDigits { get { return string.IsNullOrEmpty(_lastFour) ? string.Empty : _lastFour; } set { _lastFour = value; } }

        decimal _flgFall = 0;
        [DataMember]
        public Nullable<decimal> FlagFall { get { return _flgFall; } set { _flgFall = Math.Round(Convert.ToDecimal(value), 2, MidpointRounding.AwayFromZero); } }

        decimal _extras = 0;
        [DataMember]
        public Nullable<decimal> Extras { get { return _extras; } set { _extras = Math.Round(Convert.ToDecimal(value), 2, MidpointRounding.AwayFromZero); } }

        string _txnId = string.Empty;
        [DataMember]
        public string GatewayTxnId { get { return string.IsNullOrEmpty(_txnId) ? string.Empty : _txnId; } set { _txnId = value; } }

        [DataMember]
        public Nullable<int> fk_FleetId { get; set; }

        string _reqId = string.Empty;
        [DataMember]
        public string RequestId { get { return string.IsNullOrEmpty(_reqId) ? string.Empty : _reqId; } set { _reqId = value; } }

        [DataMember]
        public Nullable<bool> IsVoided { get; set; }

        [DataMember]
        public Nullable<bool> IsDownloaded { get; set; }

        string _txnSeqNo = string.Empty;
        [DataMember]
        public string TransSeqNo { get { return string.IsNullOrEmpty(_txnSeqNo) ? string.Empty : _txnSeqNo; } set { _txnSeqNo = value; } }

        [DataMember]
        public Nullable<int> BatchNumber { get; set; }

        public Nullable<bool> RetrieveSinceBatch { get; set; }
    }
}
