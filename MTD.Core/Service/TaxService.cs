﻿using System.Collections.Generic;
using MTD.Core.DataTransferObjects;
using MTD.Core.Extension;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;
using Ninject;

namespace MTD.Core.Service
{
    public class TaxService : ITaxService
    {
        private readonly ITaxRepository _taxRepository;

        public TaxService([Named("TaxRepository")] ITaxRepository taxRepository)
        {
            _taxRepository = taxRepository;

        }

        /// <summary>
        /// Purpose             :   To Add the tax
        /// Function Name       :   Add
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/07/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="taxDto"></param>
        /// <returns></returns>
        public bool Add(TaxDto taxDto)
        {
            try
            {
                Merchant tax = taxDto.ToTax();
                bool result = _taxRepository.Add(tax);
                return result;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get the Tax  List
        /// Function Name       :   GetTaxDtoList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/08/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TaxDto> GetTaxDtoList()
        {
            try
            {
                var taxList = _taxRepository.TaxList();
                var taxDtoList = taxList.ToDTOTaxList();
                return taxDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Tax
        /// Function Name       :   GetTaxDto
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/11/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public TaxDto GetTaxDto(int merchantId)
        {
            try
            {
                var tax = _taxRepository.GetTax(merchantId);
                var taxDto = tax.ToTaxDto();
                return taxDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update the existing tax
        /// Function Name       :   Update
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/11/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="taxDto"></param>
        /// <param name="merchantId"></param>
        public void Update(TaxDto taxDto, int merchantId)
        {
            try
            {
                Merchant tax = taxDto.ToTax();
                _taxRepository.Update(tax, merchantId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Delete the Tax
        /// Function Name       :   DeleteTax
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/12/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        public void DeleteTax(int merchantId)
        {
            try
            {
                _taxRepository.DeleteTax(merchantId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Default Tax
        /// Function Name       :   GetDefaultTaxDto
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/12/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public DefaultTaxDto GetDefaultTaxDto()
        {
            try
            {
                var tax = _taxRepository.GetDefaultTax();
                var taxDto = tax.ToDefaultTaxDto();
                return taxDto;
            }
            catch
            {
                throw;
            }
        }

    }
}
