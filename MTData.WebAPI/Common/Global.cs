﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTData.WebAPI.Common
{
    public static class Global
    {

        private static StringBuilder _debugMessage = new StringBuilder();
        
        /// <summary>
        /// The ServiceID which used for processing the request on datawire
        /// </summary>
        internal const string ServiceID = "119";

        /// <summary>
        /// User Agent is used for adding header
        /// </summary>
        internal const string UserAgent = "vxnapi_xml_3_2_0_19";

        /// <summary>
        /// internal constant string ApplicationID = "";  //Not in use because its using the AppNameNVersion as App element
        /// </summary>
        internal const string AppNameNVersion = "SIGMAPAYGATEYXML";

        internal static string Version { get { return "1.0"; } }

        internal static string RegistrationServerUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["RegistrationServerUrl"];
            }
        }

        internal static string ActiveUrl { get; set; }

        //public static Logger Log = new Logger();


        #region Functions to create trace log

        /// <summary>
        /// Adds the start of function log trace in trace log
        /// </summary>
        /// <param name="functionName">The Calling function</param>
        /// <param name="functionParams">all parameter in function</param>
        public static void CreateFunctionStartTrace(string functionName, string functionParams)
        {
            var functionInfo = "=> Start of function : " + functionName + Environment.NewLine;
            var strngSymbol = new string('-', functionInfo.Length) + Environment.NewLine;
            _debugMessage.Append(functionInfo);
            _debugMessage.Append(strngSymbol);
            _debugMessage.Append(functionParams);
            _debugMessage.Append(strngSymbol);

        }

        /// <summary>
        /// Adds the End of function trace to the tracelog
        /// </summary>
        /// <param name="functionName"></param>
        public static void CreateFunctionEndTrace(string functionName)
        {
            var function = "=> End of function : " + functionName + Environment.NewLine;
            _debugMessage.Append(function);
        }
        #endregion

        /// <summary>
        /// Get the transaction timeout time in miliseconds.
        /// </summary>
        internal static int TransactionTimeout
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["TransactionTimeout"]);
            }
        }

        internal static string BaseDerivationKeyID
        {
            get
            {
                //BaseDerivationKeyID should be pad on LEFT with ‘F’s if its length is less then 9.
                string baseDerivationKeyID = ConfigurationManager.AppSettings["BaseDerivationKeyID"];
                return baseDerivationKeyID.Length >= 9 ? baseDerivationKeyID.Substring(0, 8) : baseDerivationKeyID.PadLeft(9, 'F');
            }
        }
        internal static int DuplicateTransactionInterval
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["DuplicateTransactionInterval"]);
            }
        }

        public static bool IsNewGlobalBinAvailable { get; set; }

        public static void UpdateIsNewGlobalBinAvailable()
        {
            //var result=TransactionDataContext.UpdateIsNewGlobalBinAvailable();//umesh
            var result = false;
            //reset to unavailable
            IsNewGlobalBinAvailable = result;
        }

        /// <summary>
        /// Get waiting time, before sending a TOR transaction.
        /// </summary>
        internal static long TORWaitingTime
        {
            get
            {
                return Convert.ToInt64(ConfigurationManager.AppSettings["TORWaitingTime"]);
            }
        }
    }


}
