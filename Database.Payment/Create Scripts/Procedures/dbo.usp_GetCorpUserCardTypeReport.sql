IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetCorpUserCardTypeReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetCorpUserCardTypeReport]
GO

CREATE  PROCEDURE [dbo].[usp_GetCorpUserCardTypeReport]
(
  @DateFrom DATETIME,
  @DateTo DATETIME,
  @CorpUserUserId int 
)
AS
BEGIN TRY
  SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF
  DECLARE @InnerTable TABLE
   (
     Id INT, 
     Amount MONEY, 
     fk_FleetId INT, 
     ResponseCode VARCHAR(25),
	 IsRefunded BIT,
	 IsVoided BIT,
	 CardType NVARCHAR(25),
	 TxnType NVARCHAR(20)
   )    
    --insert data into table variable InnerTable for reuse*/                         
    INSERT INTO @InnerTable(Id,CardType,Amount,fk_FleetId,TxnType,IsRefunded,IsVoided)                   
    SELECT Id,CardType,Amount,fk_FleetId,TxnType,IsRefunded,IsVoided
                              FROM MTDTransaction 
						      WHERE CAST(TxnDate AS DATE)>=@DateFrom 
							  AND CAST(TxnDate AS DATE)<@DateTo
							  AND ResponseCode IN ('000', '00', '002')                            
							  AND fk_FleetId in  (select fk_FleetID from UserFleet where fk_UserTypeId=5 and fk_UserID=@CorpUserUserId and IsActive=1)							
    SELECT ROW_NUMBER() OVER (ORDER BY trans.Company) AS 'SrNo'
                               ,ISNULL(trans.Company,'') AS 'MerchantName' 
							   ,ISNULL(trans.FleetName,'')  AS 'FleetName'
							   ,(SELECT ISNULL(COUNT(it.Id),0) FROM @InnerTable it WHERE it.fk_FleetId=trans.FleetID AND it.CardType='Visa' AND it.TxnType IN ('Sale','Completion')) AS 'VISATxn'
							   ,(SELECT CONCAT('$',ISNULL(SUM(it.Amount),0.00)) FROM @InnerTable it WHERE it.fk_FleetId=trans.FleetID AND it.CardType='Visa' AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND IsVoided=0) AS 'VISA'
							   ,(SELECT ISNULL(COUNT(it.Id),0) FROM @InnerTable it WHERE it.fk_FleetId=trans.FleetID AND it.CardType='MasterCard' AND it.TxnType IN ('Sale','Completion')) AS 'MasterTxn'
							   ,(SELECT CONCAT('$',ISNULL(SUM(it.Amount),0.00)) FROM @InnerTable it WHERE it.fk_FleetId=trans.FleetID AND it.CardType='MasterCard' AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND IsVoided=0) AS 'MasterCard'
							   ,(SELECT ISNULL(COUNT(it.Id),0) FROM @InnerTable it WHERE  it.fk_FleetId=trans.FleetID AND it.CardType='Amex' AND it.TxnType IN ('Sale','Completion')) AS 'AmexTxn'
							   ,(SELECT CONCAT('$',ISNULL(SUM(it.Amount),0.00)) FROM @InnerTable it WHERE it.fk_FleetId=trans.FleetID AND it.CardType='Amex' AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND IsVoided=0) AS 'AmericanExpress'
							   ,(SELECT ISNULL(COUNT(it.Id),0) FROM @InnerTable it WHERE it.fk_FleetId=trans.FleetID AND it.CardType='Discover' AND it.TxnType IN ('Sale','Completion')) AS 'DisTxn'
							   ,(SELECT CONCAT('$',ISNULL(SUM(it.Amount),0.00)) FROM @InnerTable it WHERE it.fk_FleetId=trans.FleetID AND it.CardType='Discover' AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND IsVoided=0) AS 'Discover'
							   ,(SELECT ISNULL(COUNT(it.Id),0) FROM @InnerTable it WHERE it.fk_FleetId=trans.FleetID AND it.CardType='JCB' AND it.TxnType IN ('Sale','Completion')) AS 'JCBTxn'
							   ,(SELECT CONCAT('$',ISNULL(SUM(it.Amount),0.00)) FROM @InnerTable it WHERE it.fk_FleetId=trans.FleetID AND it.CardType='JCB' AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND IsVoided=0) AS 'JCB'
							   ,(SELECT ISNULL(COUNT(it.Id),0) FROM @InnerTable it WHERE it.fk_FleetId=trans.FleetID AND it.CardType='DinersClub' AND it.TxnType IN ('Sale','Completion')) AS 'DinerTxn'
							   ,(SELECT CONCAT('$',ISNULL(SUM(it.Amount),0.00)) FROM @InnerTable it WHERE it.fk_FleetId=trans.FleetID AND it.CardType='DinersClub' AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND IsVoided=0)	AS 'DinersClub'					  							  
                          FROM 
						     (
							    SELECT mtd.FleetID,mtd.FleetName,mtd.Company,m.ResponseCode,m.TxnDate,m.CardType,m.TxnType,m.Id,m.Amount 
								FROM MTDTransaction m
								INNER JOIN 
								(
								  SELECT fl.FleetID,fl.FleetName,mer.Company FROM Fleet fl
								  INNER JOIN 
								  (
								    SELECT MerchantID,Company FROM Merchant
								  ) mer
								  ON fl.fk_MerchantID= mer.MerchantID 
								) mtd
								ON m.fk_FleetId=mtd.FleetID 
						     )trans
					    WHERE trans.ResponseCode IN ('000', '00', '002')   
						AND CAST(trans.TxnDate AS DATE)>=@DateFrom
						AND CAST(trans.TxnDate AS DATE)<@DateTo  
						 AND trans.FleetID in (select fk_FleetID from UserFleet where fk_UserTypeId=5 and fk_UserID=@CorpUserUserId and IsActive=1)							  
                        GROUP BY trans.FleetID,trans.FleetName,trans.Company 
					    ORDER BY trans.Company 	
END TRY 
--use catch to log error with details in ProcErrorHandler table
BEGIN CATCH
      INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
      SELECT ERROR_NUMBER()
	     ,ERROR_SEVERITY()
			,ERROR_STATE()
			,ERROR_PROCEDURE()
			,ERROR_LINE()
			,ERROR_MESSAGE()
			,SUSER_SNAME()
			,HOST_NAME()
			,GETDATE()
END CATCH

return

GO