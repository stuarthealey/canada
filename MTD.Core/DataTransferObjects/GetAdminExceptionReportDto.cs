﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
    public class GetAdminExceptionReportDto
    {
        public string MerchantName { get; set; }
        public string FleetName { get; set; }
        public int ExpFleetTrans { get; set; }
        public int ActFleetTrans { get; set; }
        public int ExpCar { get; set; }
        public int ActCar { get; set; }
        public string ExpValue { get; set; }
        public string ActValue { get; set; }
        public string ExpCase { get; set; }
    }
}
