﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Data.Repository
{
    public class AdminUserRepository : BaseRepository, IAdminUserRepository
    {
        public AdminUserRepository(IMtDataDevlopmentsEntities dbMtData) : base(dbMtData)
        {
        }

        /// <summary>
        /// Purpose             :   To add user
        /// Function Name       :   Add
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/16/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool Add(User user)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    bool isExist = DbMtData.Users.Any(m => m.Email == user.Email && m.IsActive);
                    if (isExist)
                    {
                        return true;
                    }
                    
                    user.fk_UserTypeID = 1;
                    user.IsLockedOut = false;
                    user.IsActive = true;
                    user.IsFirstLogin = true;
                    user.CreatedBy = user.CreatedBy;
                    user.CreatedDate = DateTime.Now;
                    user.ModifiedDate = DateTime.Now;
                    user.ModifiedBy = user.ModifiedBy;
                    user.PCodeExpDate = DateTime.Now.AddDays(90);
                    DbMtData.Users.Add(user);
                    DbMtData.CommitChanges();

                    var report = DbMtData.Reports.Where(x => x.ReportId < 4).ToList();
                    report = report.Concat(DbMtData.Reports.Where(x => x.ReportId > 6 && x.ReportId < 9)).ToList();
                    report = report.Concat(DbMtData.Reports.Where(x => x.ReportId > 26 && x.ReportId < 31)).ToList();
                    Recipient[] recObj = new Recipient[report.Count()];
                    
                    int count = 0;                    
                    foreach (var item in report)
                    {
                        recObj[count] = new Recipient();
                        recObj[count].fk_MerchantID = null;
                        recObj[count].IsRecipient = true;
                        recObj[count].fk_UserID = user.UserID;
                        recObj[count].fk_UserTypeID = 1;
                        recObj[count].fk_ReportID = item.ReportId;
                        recObj[count].ModifiedBy = user.ModifiedBy;
                        recObj[count].ModifiedDate = (DateTime)user.ModifiedDate;
                        DbMtData.Recipients.Add(recObj[count]);
                        count++;
                    }
                    
                    DbMtData.CommitChanges();
                    transactionScope.Complete();

                    return false;
                }
                catch
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Purpose             :   To get admin list
        /// Function Name       :   AdminList
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/16/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<User> AdminList(string name)
        {
            try
            {
                IEnumerable<User> usersList = (from users in DbMtData.Users
                                               join countries in DbMtData.CountryCodes on users.fk_Country equals countries.CountryCodesID
                                               select new { Users = users, CountryCodes = countries }).AsEnumerable().Select(objNew =>
                   new User
                   {
                       FName = objNew.Users.FName,
                       LName = objNew.Users.LName,
                       Email = objNew.Users.Email,
                       Address = objNew.Users.Address,
                       GridCity = objNew.Users.fk_City,
                       GridState = objNew.Users.fk_State,
                       fk_Country = objNew.CountryCodes.Title,
                       Phone = objNew.Users.Phone,
                       IsActive = objNew.Users.IsActive,
                       IsLockedOut = objNew.Users.IsLockedOut,
                       UserID = objNew.Users.UserID,
                       fk_UserTypeID = objNew.Users.fk_UserTypeID
                   }).ToList();
                
                var adminList = from m in usersList where (m.fk_UserTypeID == 1 && m.IsActive) select m;

                if (!string.IsNullOrEmpty(name))
                {
                    adminList = adminList.Where(p => p.FName.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }

                return adminList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get user profile list
        /// Function Name       :   UserProfiles
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/16/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<User> UserProfiles()
        {
            try
            {
                var adminUserList = from user in DbMtData.Users where (user.fk_UserTypeID == 1) select user;
                return adminUserList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To update admin user
        /// Function Name       :   Update
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/16/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="user"></param>
        /// <param name="adminUserId"></param>
        /// <returns></returns>
        public bool Update(User user, int adminUserId)
        {
            if (adminUserId != user.UserID)
            {
                try
                {
                    var userDb = DbMtData.Users.Find(adminUserId);
                    userDb.FName = user.FName;
                    userDb.LName = user.LName;
                    userDb.Phone = user.Phone;
                    userDb.Email = user.Email;
                    userDb.fk_Country = user.fk_Country;
                    userDb.fk_State = user.fk_State;
                    userDb.fk_City = user.fk_City;
                    userDb.Address = user.Address;
                    userDb.ModifiedBy = user.CreatedBy;
                    userDb.ModifiedDate = DateTime.Now;

                    return Convert.ToBoolean(DbMtData.CommitChanges());
                }
                catch
                {
                    throw;
                }
            }

            return false;
        }

        /// <summary>
        /// Purpose             :   To update profile
        /// Function Name       :   UpdateProfile
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/16/2011
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="user"></param>
        /// <param name="adminUserId"></param>
        /// <returns></returns>
        public bool UpdateProfile(User user, int adminUserId)
        {
            if (adminUserId == user.UserID) return Convert.ToBoolean(DbMtData.CommitChanges());
            try
            {
                var userDb = DbMtData.Users.Find(adminUserId);
                userDb.FName = user.FName;
                userDb.LName = user.LName;
                userDb.Phone = user.Phone;
                userDb.Email = user.Email;
                userDb.fk_Country = user.fk_Country;
                userDb.fk_State = user.fk_State;
                userDb.fk_City = user.fk_City;
                userDb.Address = user.Address;
                userDb.IsActive = user.IsActive;
                userDb.IsLockedOut = user.IsLockedOut;
                userDb.ModifiedBy = user.ModifiedBy;
                userDb.ModifiedDate = DateTime.Now;
            }
            catch
            {
                throw;
            }

            return Convert.ToBoolean(DbMtData.CommitChanges());
        }

        /// <summary>
        /// Purpose             :   To get admin user
        /// Function Name       :   GetAdminUser
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/16/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="adminUserId"></param>
        /// <returns></returns>
        public User GetAdminUser(int adminUserId)
        {
            User userDb = DbMtData.Users.Find(adminUserId);
            var role = (from roles in DbMtData.UserRoles where roles.fk_UserID == adminUserId select new { roles.fk_RoleID }).ToList();
            string temp = "";

            for (int i = 0; i < role.Count; i++)
            {
                if (i == role.Count - 1)
                    temp = temp + role[i].fk_RoleID;
                else
                    temp = temp + role[i].fk_RoleID + ",";
            }

            userDb.RolesAssigned = temp;

            return userDb;
        }

        /// <summary>
        /// Purpose             :   To delete admin user
        /// Function Name       :   DeleteAdminUser
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/16/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="adminUserId"></param>
        public void DeleteAdminUser(int adminUserId)
        {
            try
            {
                var userDb = DbMtData.Users.Find(adminUserId);
                userDb.IsActive = false;
                DbMtData.CommitChanges();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get admin roles
        /// Function Name       :   GetAdminRole
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/16/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Role> GetAdminRole()
        {
            try
            {
                IEnumerable<Role> userRoles = (from role in DbMtData.Roles where role.fk_UserTypeID == 0 select new { role.RoleName, role.RoleID }).AsEnumerable().Select(objNew =>
                                                      new Role
                                                      {
                                                          RoleID = objNew.RoleID,
                                                          RoleName = objNew.RoleName
                                                      }).ToList();

                return userRoles;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get list of all admin user
        /// Function Name       :   GetAdminUserListByFilter
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/17/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <param name="sorting"></param>
        /// <returns></returns>
        public IEnumerable<User> GetAdminUserListByFilter(string name, int startIndex, int count, string sorting)
        {
            try
            {
                IEnumerable<User> usersList = (from users in DbMtData.Users
                                               join countries in DbMtData.CountryCodes on users.fk_Country equals countries.CountryCodesID
                                               select new { Users = users, CountryCodes = countries }).AsEnumerable().Select(objNew =>
                   new User
                   {

                       FName = objNew.Users.FName + " " + objNew.Users.LName,
                       Email = objNew.Users.Email,
                       Address = objNew.Users.Address + ", " + objNew.Users.fk_City + ", " + objNew.Users.fk_State + ", " + objNew.CountryCodes.Title,
                       Phone = objNew.Users.Phone,
                       IsActive = objNew.Users.IsActive,
                       IsLockedOut = objNew.Users.IsLockedOut,
                       UserID = objNew.Users.UserID,
                       fk_UserTypeID = objNew.Users.fk_UserTypeID,
                       CreatedDate = objNew.Users.CreatedDate
                   }).ToList();
                
                var adminList = from m in usersList where (m.fk_UserTypeID == 1 && m.IsActive) select m;
                
                if (!string.IsNullOrEmpty(name))
                {
                    adminList = adminList.Where(p => p.FName.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0);

                }

                if (!string.IsNullOrEmpty(sorting))
                    adminList = SortedUsers(sorting, adminList);

                List<User> mUsers = new List<User>();
                foreach (var mu in adminList)
                {
                    mu.Address = mu.Address.Replace(", , ,", ", ");
                    mu.Address = mu.Address.Replace(", ,", ", ");
                    mUsers.Add(mu);
                }

                return count > 0
                            ? mUsers.Skip(startIndex).Take(count).ToList() //Paging
                            : mUsers.ToList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="adminList"></param>
        /// <returns></returns>
        private IEnumerable<User> SortedUsers(string sorting, IEnumerable<User> adminList)
        {
            switch (sorting)
            {
                case "FName ASC":
                    adminList = adminList.OrderBy(p => p.FName);
                    break;
                case "FName DESC":
                    adminList = adminList.OrderByDescending(p => p.FName);
                    break;
                case "LName ASC":
                    adminList = adminList.OrderBy(p => p.LName);
                    break;
                case "LName DESC":
                    adminList = adminList.OrderByDescending(p => p.LName);
                    break;
                case "Phone ASC":
                    adminList = adminList.OrderBy(p => p.Phone);
                    break;
                case "Phone DESC":
                    adminList = adminList.OrderByDescending(p => p.Phone);
                    break;
                case "Email ASC":
                    adminList = adminList.OrderBy(p => p.Email);
                    break;
                case "Email DESC":
                    adminList = adminList.OrderByDescending(p => p.Email);
                    break;
                case "CreatedDate ASC":
                    adminList = adminList.OrderBy(p => p.CreatedDate);
                    break;
                case "CreatedDate DESC":
                    adminList = adminList.OrderByDescending(p => p.CreatedDate);
                    break;
            }

            return adminList;
        }

    }
}
