﻿using System;

namespace MTData.WebAPI.Models
{
    public class LocalVariables
    {
        //Common variables
        public TxnTypeType TransType { get; set; }
        public CardTypeType CardType { get; set; }
        public PymtTypeType PayType { get; set; }
        public string ExpiryDate { get; set; }
        public string DeviceId { get; set; }
        public string DriverNo { get; set; }
        public string Token { get; set; }
        public string TxnType { get; set; }
        public string TxnAmount { get; set; }
        public string IndustryType { get; set; }
        public string RequestId { get; set; }
        public string RequestType { get; set; }
        public string Ccno { get; set; }
        public string CardCvv { get; set; }
        public string Rcci { get; set; }
        public string EntryMode { get; set; }
        public string StartLatitude { get; set; }
        public string EndLatitude { get; set; }
        public string StartLongitude { get; set; }
        public string EndLongitude { get; set; }
        public string Fare { get; set; }
        public string Taxes { get; set; }
        public string Tip { get; set; }
        public string Tolls { get; set; }
        public string Surcharge { get; set; }
        public string Fee { get; set; }
        public string FlagFall { get; set; }
        public string Extras { get; set; }
        public string CurrencyCode { get; set; }
        public string FleetId { get; set; }

        //Payment controller variables
        public string Track1Data { get; set; }
        public string Track2Data { get; set; }
        public string Track3Data { get; set; }
        public string ServiceCode { get; set; }
        public string DebitPin { get; set; }
        public string KCN { get; set; }
        public string Currency { get; set; }
        public string KeyId { get; set; }
        public string CreatedBy { get; set; }
        public string EmvData { get; set; }
        public string PinData { get; set; }
        public string CSequence { get; set; }
        public string DriverPin { get; set; }
        public string VehicleNumber { get; set; }
        public string RapidConnectAuthId { get; set; }
        public string TransNote { get; set; }
        public string StreetAddress { get; set; }
        public string ZipCode { get; set; }
        public string PickAddress { get; set; }
        public string DestinationAddress { get; set; }
        public string JobNumber { get; set; }
        public string GateFee { get; set; }
        public string Others { get; set; }
        public string TknType { get; set; }
        public string FbSeq { get; set; }
        public string ReqFileOff { get; set; }
        public string CapData { get; set; }
        
        //BookerApp variables
        public string EncryptedToken { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string CustomerId { get; set; }
        public long JobId { get; set; }
        public Nullable<int> EncryptMethod { get; set; }
        public Nullable<int> KeyVersion { get; set; }

        //CapKey variables
        public string CapKStatus { get; set; }
        public string CapkFileCreationDt { get; set; }
        public string FileSize { get; set; }
        public string FileCRC16 { get; set; }

        //Added for DevTypeId
        public string RFUDEV { get; set; }

        //Changes done for EMV Contact certification of Canada
        public string MacData { get; set; }
        public string CheckDigit { get; set; }
        public string AccountType { get; set; }
        public string TagAID { get; set; }
        public string TransactionId { get; set; }
        public string Stan { get; set; } //System Trace No
        public string RRN { get; set; } //Retrieval Ref No
        public string TransCount { get; set; } //Request counter for Interac
        public string TerminalLaneNo { get; set; }
        public string DeviceType { get; set; } //Device type indicator for Canada
        public bool PortalEmvRefundVoidReq { get; set; } //emv refund/void request of Canada
        //zzz Chetu 357
        public string VoidRequestId { get; set; } //Request for Interac void 

        //PAY-12 AVS address details for customer for RCCI
        public string AVSUnit { get; set; }
        public string AVSStreetNumber { get; set; }
        public string AVSStreet { get; set; }
        public string AVSAddressLine1 { get; set; }
        public string AVSAddressLine2 { get; set; }
        public string AVSSuburb { get; set; }
        public string AVSState { get; set; }
        public string AVSPostcode { get; set; }
    }
}