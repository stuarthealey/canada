﻿using System.Collections.Generic;
using System.Web.Http;

namespace MTData.WebAPI.Controllers
{
    public class DefaultController : ApiController
    {
        //private readonly IMerchantService _merchantService;
        //private readonly IMemberService _loginService;

        // GET api/default
        public IEnumerable<string> Get()
        {
            return new[] { "value1", "value2" };
        }

        // GET api/default/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/default
        public void Post([FromBody]string value)
        {
        }

        // PUT api/default/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/default/5
        public void Delete(int id)
        {
        }
    }
}
