﻿

function AllowCharsNumber(obj, tLength) {
    var key = window.event ? event.keyCode : obj.keyCode;

    if (key != 37 && key != 8) {
        if (!$(obj).val().match(/^[A-Za-z]+$/) && $.trim($(obj).val()) != "") {
            $(obj).val($(obj).val().replace(/[^A-Za-z,0-9]/g, ''));
        }
        $(obj).val($(obj).val().slice(0, tLength));
    }

}


function AllowCharsNumberSpace(obj, tLength) {
    var key = window.event ? event.keyCode : obj.keyCode;
    
    if (key != 37 && key!=8)
    {
            if (!$(obj).val().match(/^[A-Za-z]+$/) && $.trim($(obj).val()) != "") {
                $(obj).val($(obj).val().replace(/[^A-Za-z,0-9,' ']/g, ''));
            }
            $(obj).val($(obj).val().slice(0, tLength));
        }

    }
function AllowCharsNumber(obj, tLength) {
    var key = window.event ? event.keyCode : obj.keyCode;

    if (key != 37 && key != 8) {
        if (!$(obj).val().match(/^[A-Za-z]+$/) && $.trim($(obj).val()) != "") {
            $(obj).val($(obj).val().replace(/[^A-Za-z,0-9]/g, ''));
        }
        $(obj).val($(obj).val().slice(0, tLength));
    }

}



    function AllowEverything(obj, tLength) {
    var key = window.event ? event.keyCode : obj.keyCode;

    if (key != 37 && key != 8) {
        if (obj.val().trim() > 40) {
            $(obj).val($(obj).val().slice(0, tLength));
        }
        
    }
}

    function AllowNumbers(obj, tLength) {
 
    var key = window.event ? event.keyCode : obj.keyCode;
    if (key != 37 && key != 8) {
            if (!$(obj).val().match(/^[0-9]+$/) && $.trim($(obj).val()) != "") {
                $(obj).val($(obj).val().replace(/\D/g, ''));
            }
            $(obj).val($(obj).val().slice(0, tLength));
        }
}
    function AllowDecimalNumbers(obj, tLength) {
    var key = window.event ? event.keyCode : obj.keyCode;

    if (key != 37 && key != 8) {
            if (!$(obj).val().match(/^[0-9.]+$/) && $.trim($(obj).val()) != "") {
                $(obj).val($(obj).val().replace(/[^0-9.]/g, ""));
            }
            $(obj).val($(obj).val().slice(0, tLength));
        }

}
    function AllowCharsSpace(obj, tLength) {
    var key = window.event ? event.keyCode : obj.keyCode;

    if (key != 37 && key != 8) {
            if (!$(obj).val().match(/^[A-Za-z]+$/) && $.trim($(obj).val()) != "") {
                $(obj).val($(obj).val().replace(/[^A-Za-z,' ']/g, ''));
            }
            $(obj).val($(obj).val().slice(0, tLength));
        }
}

    function LengthOnly(obj, tLength) {
       
        $(obj).val($(obj).val().substr(0, tLength));
    }

    function AllowCharsNumberSymbol(obj, tLength) {
        var key = window.event ? event.keyCode : obj.keyCode;

        if (key != 37 && key != 8) {
            if (!$(obj).val().match(/^[A-Za-z]+$/) && $.trim($(obj).val()) != "") {
                $(obj).val($(obj).val().replace(/[^A-Za-z,0-9, - , : , .]/g, ''));
            }
            $(obj).val($(obj).val().slice(0, tLength));
        }
    }

    function AllowCharWithSpecChar(obj, tLength) {
        var key = window.event ? event.keyCode : obj.keyCode;

        if (key != 37 && key != 8) {
            if (!$(obj).val().match(/^[A-Za-z,@.#%*!,0-9]+$/) && $.trim($(obj).val()) != "") {
                $(obj).val($(obj).val().replace(/[^A-Za-z,' ']/g, ''));
            }
            $(obj).val($(obj).val().slice(0, tLength));
        }
    }
    function AllowNumbersOnly(obj, tLength) {

        var key = window.event ? event.keyCode : obj.keyCode;
        if (key != 37 && key != 8) {
            if (!$(obj).val().match(/^[0-9]+$/) && $(obj).val() != "") {
                $(obj).val($(obj).val().replace(/\D/g, ''));
            }
            $(obj).val($(obj).val().slice(0, tLength));
        }
    }