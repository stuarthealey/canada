﻿using System.Configuration;
using System.Linq;
using System.Transactions;
using System.Data.Entity.Validation;

using MTData.Utility;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Data.Repository
{
    public class TransactionRepositoryUK : BaseRepository, ITransactionRepositoryUK
    {
        readonly ILogger _logger = new Logger();

        public TransactionRepositoryUK(IMtDataDevlopmentsEntities dbMtData) : base(dbMtData)
        {
        }

        /// <summary>
        ///  Purpose            :   To add transaction into database
        /// Function Name       :   Add
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/12/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public int AddSemiTxn(UKMTDTransaction uktransaction, MTDTransaction UsmtdTxn)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    if (uktransaction.TransactionID != string.Empty)
                    {
                        if (DbMtData.UKMTDTransactions.Any(x => x.TransactionID == uktransaction.TransactionID))
                        {
                            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                _logger.LogInfoMessage(string.Format("TransactionRepositoryUK.AddSemiTxn(). TransactionID already exists. TransactionID:{0};UKTrans:{1};UsMTDTxn:{2};", uktransaction.TransactionID, uktransaction.ToString(), UsmtdTxn.ToString()));

                            return 0;
                        }
                    }

                    try
                    {
                        DbMtData.MTDTransactions.Add(UsmtdTxn);
                        DbMtData.CommitChanges();
                    }
                    catch (DbEntityValidationException ex)
                    {
                        if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        {
                            foreach (var eve in ex.EntityValidationErrors)
                            {
                                _logger.LogInfoMessage(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State));

                                foreach (var ve in eve.ValidationErrors)
                                {
                                    _logger.LogInfoMessage(string.Format("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"", ve.PropertyName, eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName), ve.ErrorMessage));
                                }
                            }
                        }
                    }

                    uktransaction.Fk_MtdTxnId = UsmtdTxn.Id;

                    try
                    {
                        DbMtData.UKMTDTransactions.Add(uktransaction);
                        DbMtData.CommitChanges();
                    }
                    catch (DbEntityValidationException ex)
                    {
                        if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        {
                            foreach (var eve in ex.EntityValidationErrors)
                            {
                                _logger.LogInfoMessage(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State));

                                foreach (var ve in eve.ValidationErrors)
                                {
                                    _logger.LogInfoMessage(string.Format("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"", ve.PropertyName, eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName), ve.ErrorMessage));
                                }
                            }
                        }
                    }

                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("TransactionRepositoryUK.AddSemiTxn(). UKTransaction added. UKTransactionID:{0};UKTrans:{1};UsMTDTxn:{2};", uktransaction.Id, uktransaction.ToString(), UsmtdTxn.ToString()));

                    transactionScope.Complete();

                    return UsmtdTxn.Id;
                }
                catch
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public int AddSemiSattlementTxn(UKMtdtxnSattlement transaction)
        {
            try
            {
                DbMtData.UKMtdtxnSattlements.Add(transaction);
                DbMtData.CommitChanges();
                return transaction.Id;
            }
            catch
            {
                throw;
            }
        }
    }
}
