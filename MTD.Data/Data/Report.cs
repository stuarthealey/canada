//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MTD.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Report
    {
        public Report()
        {
            this.Recipients = new HashSet<Recipient>();
            this.ScheduleReports = new HashSet<ScheduleReport>();
        }
    
        public int ReportId { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public Nullable<bool> IsCreated { get; set; }
    
        public virtual ICollection<Recipient> Recipients { get; set; }
        public virtual ICollection<ScheduleReport> ScheduleReports { get; set; }
    }
}
