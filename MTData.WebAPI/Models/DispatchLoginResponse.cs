﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class DispatchLoginResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string DriverNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CarNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string DeviceID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FleetId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FleetName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Message { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Token { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CardSwiped { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Contactless { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ChipAndPin { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CurrencyCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string RCTerminal { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public MACVerification MACVerification { get; set; }
    }
}