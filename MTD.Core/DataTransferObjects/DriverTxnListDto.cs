﻿namespace MTD.Core.DataTransferObjects
{
  public  class DriverTxnListDto
    {
        public string DriverText { get; set; }
        public string DriverValue { get; set; }
    }
}
