﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace MTData.Web.ViewData
{
    public class PayrollReportPayment
    {
        public MtdTransactionsViewData transaction { get; set; }
        public IEnumerable<DriverTransactionViewData> searchResult { get; set; }
        public IEnumerable<MerchantListViewData> MerchantList { get; set; }
        public IEnumerable<FleetListViewData> FleetList { get; set; }
        public IEnumerable<VehicleListViewData> VehicleList { get; set; }
        public string MerchantName { get; set; }
        public string FtName { get; set; }
        public int? MerchantID { get; set; }
        public int? FleetID { get; set; }
        public int VehicleID { get; set; }
        public string startDateString { get; set; }
        public string endDateString { get; set; }
        public string TType { get; set; }
    }
}