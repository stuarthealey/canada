-- Create indexes on MTDTransaction for performance.
-- Drop and re-create the MTDTransaction index on Merchant and 
-- Response, to include some additional columns for performance.
--==============================================================

IF EXISTS (SELECT * FROM sysindexes si INNER JOIN sysobjects so ON so.id = si.id WHERE si.name = 'IX_MTDTransactionMerchantResponse' AND OBJECTPROPERTY(so.id, N'IsUserTable') = 1 AND so.name = 'MTDTransaction')
BEGIN
	-- Check if there is the TxnDate INCLUDE column. If not drop existing and create new one.
	IF NOT EXISTS (SELECT K.*, C.*
			FROM dbo.sysindexes I
				INNER JOIN dbo.sysindexkeys K ON K.id = I.id AND K.indid = I.indid
				INNER JOIN dbo.syscolumns C ON C.id = I.id AND C.colid = K.colid
			WHERE I.name = 'IX_MTDTransactionMerchantResponse'
			  AND K.keyno = 0
			  AND C.name = 'TxnDate')
	BEGIN
		PRINT 'Drop existing IX_MTDTransactionMerchantResponse'
		DROP INDEX IX_MTDTransactionMerchantResponse ON dbo.MTDTransaction
	END
	ELSE
	BEGIN
		PRINT 'IX_MTDTransactionMerchantResponse already has INCLUDE columns.'
	END
END
GO

-- Create the IX_MTDTransactionMerchantResponse with the additional INCLUDE columns.
------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT * FROM sysindexes si INNER JOIN sysobjects so ON so.id = si.id WHERE si.name = 'IX_MTDTransactionMerchantResponse' AND OBJECTPROPERTY(so.id, N'IsUserTable') = 1 AND so.name = 'MTDTransaction')
BEGIN
	PRINT 'Creating Index IX_MTDTransactionMerchantResponse on MTDTransaction'

	CREATE NONCLUSTERED INDEX IX_MTDTransactionMerchantResponse ON dbo.MTDTransaction (MerchantId ASC, ResponseCode ASC, Id ASC)
		INCLUDE (TxnDate, BatchNumber)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
END
ELSE
BEGIN
	PRINT 'Index IX_MTDTransactionMerchantResponse on MTDTransaction already exists'
END
GO