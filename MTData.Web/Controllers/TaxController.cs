﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Text;
using System.Globalization;
using System.Reflection;
using System.Web.Security;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Web.ViewData;
using MTData.Web.Resource;
using MTData.Utility;

namespace MTData.Web.Controllers
{
    public class TaxController : Controller
    {
        private const string Default_Tax = "Default Tax";
        readonly ILogger _logger = new Logger();
        private readonly ITaxService _taxService;
        private readonly ICommonService _commonService;
        private StringBuilder _logMessage;
        private StringBuilder _logTracking;

        public TaxController([Named("TaxService")] ITaxService taxService, [Named("CommonService")] ICommonService commonService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _taxService = taxService;
            _commonService = commonService;
            _logMessage = logMessage;
            _logTracking = logTracking;
        }

        /// <summary>
        /// Purpose              :   To Display the tax
        /// Function Name       :   Index
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/26/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var taxDtoList = _taxService.GetTaxDtoList();

                _logMessage.Append("calling ToTaxViewDataList to convert TaxDto into TaxViewData and create list");

                List<TaxViewData> taxView = taxDtoList.ToTaxViewDataList().ToList();
                _logMessage.Append("ToTaxViewDataList executed successfully");
                _logMessage.Append("calling GetDefaultTaxDto method of _taxService");

                var defaultTaxDto = _taxService.GetDefaultTaxDto();

                _logMessage.Append("GetDefaultTaxDto method of _taxService executed successfully");
                _logMessage.Append("calling ToDefaultTaxViewData to convert DefaultTaxDto into DefaultTaxViewData");

                var defaultTax = defaultTaxDto.ToDefaultTaxViewData();

                _logMessage.Append("ToDefaultTaxViewData executed successfully");

                var taxViewData = new TaxViewData();
                taxViewData.Company = Default_Tax;  // "Default Tax";
                taxViewData.MerchantID = 0;
                taxViewData.StateTaxRate = defaultTax.StateTaxRate;
                taxViewData.FederalTaxRate = defaultTax.FederalStateRate;
                taxView.Insert(0, taxViewData);

                return View(taxView);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   creating tax
        /// Function Name       :   Create
        /// Created By          :   Naveen Kumar 
        /// Created On          :   12/29/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var taxView = new TaxViewData();
                taxView.MerchantList = TaxMerchantList();

                _logMessage.Append("calling GetDefaultTaxDto method of _taxService");

                var defaultTaxDto = _taxService.GetDefaultTaxDto();

                _logMessage.Append("GetDefaultTaxDto method of _taxService executed successfully");
                _logMessage.Append("calling ToDefaultTaxViewData to convert DefaultTaxDto into DefaultTaxViewData");

                var defaultTax = defaultTaxDto.ToDefaultTaxViewData();

                _logMessage.Append("ToDefaultTaxViewData executed successfully");

                taxView.StateTaxRate = defaultTax.StateTaxRate;
                taxView.FederalTaxRate = defaultTax.FederalStateRate;

                return View(taxView);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   creating tax
        /// Function Name       :   Create
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/30/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="taxViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(TaxViewData taxViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (ModelState.IsValid)
                {
                    var taxDto = taxViewData.ToTaxDto();
                    bool result = _taxService.Add(taxDto);
                    if (!result)
                    {
                        _logMessage.Append("Calling MerchantList() method of same class");

                        taxViewData.MerchantList = MerchantList();
                        ViewBag.Message = MtdataResource.Default_tax_exist;   //"Default Tax has been already set";

                        return View(taxViewData);
                    }

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + " create tax for merchant Id" + taxDto.MerchantID+ "at" + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return RedirectToAction("Index");
                }

                taxViewData.MerchantList = TaxMerchantList();

                return View(taxViewData);
            }
            catch (Exception exception)
            {
                _logger.LogException(exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   Updating the tax amount
        /// Function Name       :   Update
        /// Created By          :   Naveen Kumar 
        /// Created On          :   1/2/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Update(int merchantId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (merchantId != 0)
                {
                    _logMessage.Append("Calling GetTaxDto method of _taxService class takes merchantId= " + merchantId);

                    var taxDto = _taxService.GetTaxDto(merchantId);

                    _logMessage.Append("GetTaxDto method of _taxService executed successfully");
                    _logMessage.Append("calling ToTaxViewData to convert TaxDto into TaxViewData");

                    var tax = taxDto.ToTaxViewData();

                    _logMessage.Append("ToTaxViewData  executed successfully");
                    _logMessage.Append("Calling MerchantList() method of same class");

                    tax.MerchantList = MerchantList();

                    return View(tax);
                }

                var defaultTaxDto = _taxService.GetDefaultTaxDto();
                var defaultTax = defaultTaxDto.ToDefaultTaxViewData();
                var taxViewData = new TaxViewData();

                taxViewData.MerchantID = 0;
                taxViewData.StateTaxRate = defaultTax.StateTaxRate;
                taxViewData.FederalTaxRate = defaultTax.FederalStateRate;

                _logMessage.Append("Calling MerchantList() method of same class");

                taxViewData.MerchantList = MerchantList();

                return View(taxViewData);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   Update the tax amount
        /// Function Name       :   Update
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/5/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="taxViewData"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(TaxViewData taxViewData, int merchantId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (ModelState.IsValid)
                {
                    _logMessage.Append("Calling MerchantList() method of same class");

                    taxViewData.MerchantList = MerchantList();

                    _logMessage.Append("calling ToTaxDto to convert TaxViewData into TaxDto");

                    TaxDto taxDto = taxViewData.ToTaxDto();

                    _logMessage.Append("ToTaxDto executed successfully");
                    _logMessage.Append("calling Update method of _taxService takes taxDto and  merchantId tax dto properties are =" + taxViewData.Company + ", " + taxViewData.FederalTaxRate + ", " + taxViewData.MerchantID + ", " + taxViewData.IsTaxInclusive + ", " + taxViewData.MerchantName + ", " + taxViewData.StateTaxRate + ", and merchant id= " + merchantId);

                    _taxService.Update(taxDto, merchantId);

                    _logMessage.Append("Update method of _taxService executed successfully");

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + " update tax for merchant Id " + merchantId + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return RedirectToAction("Index");
                }

                _logMessage.Append("Calling MerchantList() method of same class");

                taxViewData.MerchantList = MerchantList();

                return View(taxViewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   Deleting tax
        /// Function Name       :   Delete
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/9/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Delete(int merchantId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("DeleteTax method of _taxService takes merchantId= " + merchantId);

                _taxService.DeleteTax(merchantId);

                _logMessage.Append("DeleteTax method of _taxService executed successfully returing to view");

                string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                _logTracking.Append("User " + userName + " delete tax at for merchant Id " + merchantId+"at" + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Purpose              :   to get merchant list from database
        /// Function Name       :   MerchantList
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   1/19/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<MerchantListViewData> MerchantList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("calling GetMerchantList method of _commonService to get Tax List of Merchant");

                var merchantDtoList = _commonService.GetMerchantList();

                _logMessage.Append("GetMerchantList method of _commonService executed successfully");
                _logMessage.Append("calling ToMerchantViewDataSelectList to convert MerchantListDto into MerchantListViewData and create list");

                List<MerchantListViewData> listMerchant = merchantDtoList.ToMerchantViewDataSelectList().ToList();

                _logMessage.Append("ToMerchantViewDataSelectList executed successfully");

                var merchant = new MerchantListViewData();
                merchant.fk_MerchantID = 0;
                merchant.Company = Default_Tax;//"Default Tax";

                _logMessage.Append("adding merchant to List<MerchantListViewData>");

                listMerchant.Add(merchant);

                _logMessage.Append("returing lst");

                return listMerchant;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return null;
        }

        /// <summary>
        /// Purpose             :   to get merchant tax list
        /// Function Name       :   TaxMerchantList
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   1/20/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<MerchantListViewData> TaxMerchantList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("calling GetTaxMerchantList method of _commonService to get Tax List of Merchant");

                var merchantDtoList = _commonService.GetTaxMerchantList();

                _logMessage.Append("GetTaxMerchantList method of _commonService executed successfully");
                _logMessage.Append("calling ToMerchantViewDataSelectList to convert MerchantListDto into MerchantListViewData and create list");

                List<MerchantListViewData> lst = merchantDtoList.ToMerchantViewDataSelectList().ToList();

                _logMessage.Append("ToMerchantViewDataSelectList executed successfully");

                var merchant = new MerchantListViewData();
                merchant.fk_MerchantID = 0;
                merchant.Company = Default_Tax; // "Default Tax";

                _logMessage.Append("adding merchant to List<MerchantListViewData>");

                lst.Add(merchant);

                _logMessage.Append("returing lst");

                return lst;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return null;
        }

    }

}
