﻿using System.Collections.Generic;

namespace MTData.Web.ViewData
{
    public class ReceiptFormatViewData
    {
        public int FleetID { get; set; }
        public int fk_MerchantID { get; set; }
        public string FleetName { get; set; }
        public string Description { get; set; }
        public IEnumerable<FleetReceiptViewData> fleetReceipt { get; set; }
        public IEnumerable<ReceiptMasterViewData> recceiptMaster { get; set; }
        public int FleetReceiptID { get; set; }
        public int? fk_FleetID { get; set; }
        public int fk_FieldID { get; set; }
        public string fk_FieldText { get; set; }
        public int Postion { get; set; }
        public bool? IsShow { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public IEnumerable<FleetViewData> fleetlist { get; set; }
    }
}