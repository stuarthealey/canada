﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.Web.Models
{
    public class ManyTxnClass
    {
        public int Id { get; set; }
        public float Amt { get; set; }
        public bool Status { get; set; }
        public string TxnType {get; set; }
    }
}