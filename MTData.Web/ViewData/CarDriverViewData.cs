﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.Web.ViewData
{
    public class CarDriverViewData
    {
        public IEnumerable<DriverListViewData> DriverList { get; set; }
        public IEnumerable<CarListViewData> CarList { get; set; }
    }
}