﻿using System;
using System.Net.Http;
using System.Text;
using System.Xml;
using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.WebAPI.Models;
using Ninject;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using MTData.WebAPI.Resources;
using MTData.WebAPI.Common;
using System.Web.Configuration;
using MTData.WebAPI.Controllers;

namespace MTData.WebAPI
{
    public class BookerAppPayment : TransactionRequestController
    {
        #region (Load Constructor)
        private LocalVariables objLocalVars = new LocalVariables();
        private readonly ITransactionService _transactionService;
        private static ITerminalService _terminalService;
        private readonly IMerchantService _merchantService;
        private readonly ICommonService _commonService;
        private StringBuilder _logMessage;
        private StringBuilder _logTracking;


        public BookerAppPayment(
            [Named("TransactionService")] ITransactionService transactionService,
            [Named("TerminalService")] ITerminalService terminalService,
            [Named("MerchantService")] IMerchantService merchantService,
            [Named("CommonService")] ICommonService commonService,
            StringBuilder logMessage, StringBuilder logTracking)
            : base(terminalService, logMessage)
        {
            _transactionService = transactionService;
            _terminalService = terminalService;
            _merchantService = merchantService;
            _commonService = commonService;
            _logMessage = logMessage;
            _logTracking = logTracking;
        }
        #endregion

        /// <summary>
        /// Purpose             :   Creating booker app token 
        /// Function Name       :   BookerAppPayment
        /// Created By          :   Salil Gupta
        /// Created On          :   07/04/2015
        /// Modification Made   :   Refactored and added Logging, tracing, error handling
        /// Modified By         :   Sunil Singh
        /// Modified At         :   03-Sep-2015
        /// </summary>
        /// <param name="request"></param>
        /// <param name="_fdMerchantobj"></param>
        /// <param name="_datawireObj"></param>
        /// <param name="_logger"></param>
        /// <returns></returns>
        public XmlDocument MakePayment(HttpRequestMessage request, fDMerchantDto _fdMerchantobj, DatawireDto _datawireObj, ILogger _logger)
        {
            _logMessage.Append("BookerApp MakePayment process start:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append("Creating instance of BookerAppPaymentResp as soapResp");
            BookerAppPaymentResp soapGenarator = new BookerAppPaymentResp();
            BasicAuthoriseResult basicAuthResult = new BasicAuthoriseResult();
            /*Booker app payment using RCCI */
            try
            {
                string response = string.Empty;
                XmlDocument soapResponse = new XmlDocument();
                XmlDocument xmlDoc = new XmlDocument();
                XNamespace ns2 = "http://services.mtdata.com/payment";
                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);
                var loginresp = XDocument.Parse(xmlDoc.InnerXml);

                //Loading element [arg0] from XML request.
                var argElements = loginresp.Descendants(ns2 + "arg0").FirstOrDefault();
                objLocalVars.EncryptedToken = (string)argElements.Element(ns2 + "encryptedToken");

                #region AuthenticateRequest
                var authHedr = loginresp.Descendants(ns2 + "AuthenticationHeader").FirstOrDefault();
                if (!AuthenticateRequest(authHedr, ns2))
                {
                    _logMessage.Append("BookerApp AuthenticateRequest process failed.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    basicAuthResult.ErrorMessage = PaymentAPIResources.UnauthorizedAccess;
                    soapResponse = soapGenarator.ConstructDoc(basicAuthResult);
                    return soapResponse;
                }
                #endregion

                //initialize local variables
                SetLocalVarFromXMLRequest(argElements, ns2);

                _logTracking.Append("BookerApp MakePayment process start. DeviceID: " + objLocalVars._deviceId + " at " + System.DateTime.Now.ToUniversalTime());


                #region firstDataDetails
                _logMessage.AppendLine("[BookerApp > MakePayment] _commonService.GetFirstDataDetails(objLocalVars._deviceId, (string)null, (string)null)");
                var firstDataDetails = _commonService.GetFirstDataDetails(objLocalVars._deviceId, (string)null, (string)null, Convert.ToInt32(objLocalVars.FleetId), objLocalVars._requestType);
                if (firstDataDetails.IsNull())
                {
                    _logMessage.AppendLine("[BookerApp > MakePayment] Failed executing _commonService.GetFirstDataDetails(objLocalVars._deviceId, (string)null, (string)null)");
                    _logger.LogInfoMessage(_logTracking.ToString());

                    basicAuthResult.ErrorMessage = PaymentAPIResources.InvalidTerminal;
                    soapResponse = soapGenarator.ConstructDoc(basicAuthResult);
                    return soapResponse;
                }
                objLocalVars.TknType = firstDataDetails.TokenType;
                #endregion

                #region terminal
                //Retreiving terminal detail
                _logMessage.Append("BookerApp MakePayment process. Retreiving terminal detail");
                var terminal = _terminalService.GetTerminalDto(objLocalVars._deviceId);
                if (terminal.IsNull())
                {
                    _logMessage.Append("BookerApp MakePayment process failed, terminal detail is null");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    basicAuthResult.ErrorMessage = PaymentAPIResources.InvalidTerminal;
                    soapResponse = soapGenarator.ConstructDoc(basicAuthResult);
                    return soapResponse;
                }
                #endregion

                #region _fdMerchantobj
                //Retreiving merchant detail
                _logMessage.Append("BookerApp MakePayment process. Retreiving merchant detail");
                _fdMerchantobj = _merchantService.GetFDMerchant(terminal.fk_MerchantID);
                if (_fdMerchantobj.IsNull())
                {
                    _logMessage.Append("BookerApp MakePayment process failed, merchant detail is null");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    basicAuthResult.ErrorMessage = PaymentAPIResources.FdMerchantNotExist;
                    soapResponse = soapGenarator.ConstructDoc(basicAuthResult);
                    return soapResponse;
                }
                #endregion

                #region _datawireObj
                /* Generate Client Ref Number in the format <STAN>|<TPPID>, right justified and left padded with "0" */
                _logMessage.Append("BookerApp MakePayment process. Retreiving datawire detail");
                _datawireObj = _merchantService.GetDataWireDetails(terminal.fk_MerchantID, Convert.ToInt32(terminal.DatawireId));
                if (_datawireObj.IsNull())
                {
                    _logMessage.Append("BookerApp MakePayment process failed, datawire detail is null");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    basicAuthResult.ErrorMessage = PaymentAPIResources.DatawireDetailsNull;
                    soapResponse = soapGenarator.ConstructDoc(basicAuthResult);
                    return soapResponse;
                }
                #endregion

                #region Client Reference
                _logMessage.Append("BookerApp MakePayment process. Retreiving client reference");
                string clientRef = GetClientRef(_fdMerchantobj.ProjectId);
                if (clientRef.IsNull())
                {
                    _logMessage.Append("BookerApp MakePayment process failed, clientRef is null");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    basicAuthResult.ErrorMessage = PaymentAPIResources.FdMerchantNotExist;
                    soapResponse = soapGenarator.ConstructDoc(basicAuthResult);
                    return soapResponse;
                }
                #endregion

                #region Initializing transaction request from XML data
                TransactionRequest tr = null;
                try
                {
                    tr = InitiateTransaction(_fdMerchantobj, _datawireObj, terminal);
                }
                catch (Exception ex)
                {

                    _logMessage.Append("BookerApp MakePayment process failed on Initializing transaction request from XML data");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    _logger.LogInfoFatel(_logMessage.ToString(), ex);

                    basicAuthResult.ErrorMessage = PaymentAPIResources.Exception;
                    soapResponse = soapGenarator.ConstructDoc(basicAuthResult);
                    return soapResponse;
                }
                //Craete transaction request as per UMF specification
                CreateTransactionRequest(tr);

                //Generate formatted XML data from above auth transaction request.
                string xmlSerializedTransReq = GetXmlData();
                #endregion

                //Save transaction into MTDTransaction Table
                MTDTransactionDto transactionDto = CommonFunctions.XmlToTransactionDto(tr);
                int transId = _transactionService.Add(transactionDto);


                /* Send data using SOAP protocol to Datawire*/
                _logMessage.AppendLine("Sending transaction request to rapid conect");
                string xmlSerializedTransResp = new SoapHandler().SendMessage(xmlSerializedTransReq, clientRef, tr);


                #region Update transaction response into database
                if (string.IsNullOrEmpty(xmlSerializedTransResp))
                {
                    _logMessage.AppendLine("xmlSerializedTransResp is null");
                    MTDTransactionDto responsetransactionDto = CommonFunctions.XmlResponseToTransactionDto(
                        xmlSerializedTransResp, true, tr.PaymentType.ToString(), tr.IndustryType);

                    _transactionService.Update(responsetransactionDto, transId, tr.RapidConnectAuthId);
                    _logMessage.AppendLine("xmlSerializedTransResp is null updated into database");
                }
                else
                {
                    MTDTransactionDto responsetransactionDto = CommonFunctions.XmlResponseToTransactionDto(
                        xmlSerializedTransResp, false, tr.PaymentType.ToString(), tr.IndustryType);

                    _transactionService.Update(responsetransactionDto, transId, tr.RapidConnectAuthId);
                }
                #endregion

                //send timeout
                if (string.IsNullOrEmpty(xmlSerializedTransResp))
                {
                    TimeoutTransaction objTimeout = new TimeoutTransaction(_transactionService, _terminalService, _merchantService, _logMessage);
                    xmlSerializedTransResp = objTimeout.TimeOut(tr, transId, clientRef, xmlSerializedTransResp);
                }

                
                if (!string.IsNullOrEmpty(xmlSerializedTransResp))
                    basicAuthResult = CommonFunctions.AppPayment(tr, xmlSerializedTransResp, transId);

                //Generating SOAP response.
                soapResponse = soapGenarator.ConstructDoc(basicAuthResult);

                _logTracking.Append("BookerApp MakePayment process completed. DeviceID: " + objLocalVars._deviceId + " at " + System.DateTime.Now.ToUniversalTime());
                _logMessage.AppendLine("BookerApp MakePayment process completed");
                _logger.LogInfoMessage(_logTracking.ToString());
                _logger.LogInfoMessage(_logMessage.ToString());

                return soapResponse;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);

                basicAuthResult.ErrorMessage = PaymentAPIResources.Exception;
                var soapResponse = soapGenarator.ConstructDoc(basicAuthResult);
                return soapResponse;
            }
        }

        /// <summary>       
        /// Purpose             :   Initializign local variables.
        /// Function Name       :   SetLocalVarFromXMLRequest
        /// Created By          :   Sunil Singh
        /// Created On          :   03-Sep-2015
        /// Modification Made   :   
        /// Modified By         :   
        /// Modified At         :  
        /// </summary>
        /// <param name="argElements"></param>
        /// <param name="ns2"></param>
        private void SetLocalVarFromXMLRequest(XElement argElements, XNamespace ns2)
        {
            try
            {
                //objLocalVars._deviceId = (string)argElements.Element(ns2 + "deviceId");
                objLocalVars._deviceId = WebConfigurationManager.AppSettings["BookerAppDeviceId"];
                objLocalVars._txnAmount = (string)argElements.Element(ns2 + "paymentAmt");
                objLocalVars._driverNo = (string)argElements.Element(ns2 + "userId");
                objLocalVars.StartLatitude = (string)argElements.Element(ns2 + "pickupLatitude");
                objLocalVars.EndLatitude = (string)argElements.Element(ns2 + "dropoffLatitude");
                objLocalVars.StartLongitude = (string)argElements.Element(ns2 + "pickupLongitude");
                objLocalVars.EndLongitude = (string)argElements.Element(ns2 + "dropoffLongitude");
                objLocalVars.Fare = (string)argElements.Element(ns2 + "fareAmt");
                objLocalVars.Tip = (string)argElements.Element(ns2 + "tipAmt");
                objLocalVars.Tolls = (string)argElements.Element(ns2 + "tollAmt");
                objLocalVars.FlagFall = (string)argElements.Element(ns2 + "flagfallAmt");
                objLocalVars.Extras = (string)argElements.Element(ns2 + "extrasAmt");
                objLocalVars._entryMode = (string)argElements.Element(ns2 + "swipeMethod");
                objLocalVars.rcci = (string)argElements.Element(ns2 + "RCCI");

                var transType = (string)argElements.Element(ns2 + "transactionType");
                if (!transType.IsNull())
                    if (transType != "Void")
                    {
                        if (!transType.IsNull())
                            objLocalVars._tranType = (TxnTypeType)Enum.Parse(typeof(TxnTypeType), transType);
                    }

                //var encryptedToken = (string)argElements.Element(ns2 + "encryptedToken");
                var crdNo = (string)argElements.Element(ns2 + "accountNumber");
                var expDt = (string)argElements.Element(ns2 + "expiryDate");
                var cvv = (string)argElements.Element(ns2 + "cvv2");

                if (!string.IsNullOrEmpty(crdNo) && string.IsNullOrEmpty(objLocalVars.rcci))
                {
                    objLocalVars._ccno = crdNo.DecryptRijndael(objLocalVars.EncryptedToken);
                    objLocalVars._expirydate = expDt.DecryptRijndael(objLocalVars.EncryptedToken);
                    objLocalVars.cardCvv = cvv.DecryptRijndael(objLocalVars.EncryptedToken);
                    objLocalVars._cardType = CreditCardType.GetCardType(crdNo);
                }


                if (string.IsNullOrEmpty(objLocalVars.rcci))
                {
                    objLocalVars._industryType = PaymentAPIResources.BookerAppPayment;
                }
                else
                {
                    objLocalVars._industryType = PaymentAPIResources.BookerAppPayment;
                    var ctype = _transactionService.GetCardTypeByToken(objLocalVars.rcci.Encrypt());
                    objLocalVars._cardType = (CardTypeType)Enum.Parse(typeof(CardTypeType), ctype);
                    objLocalVars._expirydate = expDt.DecryptRijndael(objLocalVars.EncryptedToken);
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>       
        /// Purpose             :   Initializing transaction request
        /// Function Name       :   InitiateTransaction
        /// Created By          :   Sunil Singh
        /// Created On          :   03-Sep-2015
        /// Modification Made   :   
        /// Modified By         :   
        /// Modified At         :  
        /// </summary>
        /// <param name="argElements"></param>
        /// <param name="ns2"></param>
        /// <param name="_fdMerchantobj"></param>
        /// <param name="_datawireObj"></param>
        /// <param name="terminal"></param>
        /// <returns></returns>
        private TransactionRequest InitiateTransaction(fDMerchantDto _fdMerchantobj, DatawireDto _datawireObj, TerminalDto terminal)
        {
            TransactionRequest tr = new TransactionRequest
            {
                //SerialNumber = _deviceId,
                //RequestedId = _requestId,
                TransactionType = objLocalVars._tranType.ToString(),// objLocalVars._tType,
                TransType = objLocalVars._tranType,
                PaymentType = PymtTypeType.Credit,
                CardType = objLocalVars._cardType,
                IndustryType = objLocalVars._industryType,
                TransactionAmount = objLocalVars._txnAmount,
                CardNumber = objLocalVars._ccno,
                ExpiryDate = objLocalVars._expirydate,
                CardCvv = objLocalVars.cardCvv,
                DriverNumber = objLocalVars._driverNo,
                StartLatitude = objLocalVars.StartLatitude,
                EndLatitude = objLocalVars.EndLatitude,
                StartLongitude = objLocalVars.StartLongitude,
                EndLongitude = objLocalVars.EndLongitude,
                Fare = objLocalVars.Fare,
                //Taxes = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/Taxes").InnerText,
                Tip = objLocalVars.Tip,
                Tolls = objLocalVars.Tolls,
                FlagFall = objLocalVars.FlagFall,
                Extras = objLocalVars.Extras,
                EntryMode = objLocalVars._entryMode,
                LocalDateTime = DateTime.Now.ToString("yyyyMMddHHmmss"),
                TrnmsnDateTime = DateTime.UtcNow.ToString("yyyyMMddHHmmss"),
                FdMerchantId = Convert.ToString(_fdMerchantobj.FDMerchantID),
                MtdMerchantId = terminal.fk_MerchantID,
                TerminalId = _datawireObj.RCTerminalId,
                TppId = _fdMerchantobj.ProjectId,
                GroupId = Convert.ToString(_fdMerchantobj.GroupId),
                Did = _datawireObj.DID,
                App = _fdMerchantobj.App,
                ServiceId = Convert.ToString(_fdMerchantobj.ServiceId),
                ServiceUrl = _fdMerchantobj.ServiceUrl,
                CreatedBy = "BookerAppPayment",
                RCCIToken = objLocalVars.rcci,
                TokenType = objLocalVars.TknType
            };

            return tr;
        }

        /// <summary>       
        /// Purpose             :   To authenticate the requset data.
        /// Function Name       :   AuthenticateRequest
        /// Created By          :   Sunil Singh
        /// Created On          :   17-Sep-2015
        /// Modification Made   :   
        /// Modified By         :   
        /// Modified At         :  
        /// </summary>
        /// <param name="argElements"></param>
        /// <param name="ns2"></param>
        /// <returns></returns>
        private bool AuthenticateRequest(XElement argElements, XNamespace ns2)
        {
            try
            {
                bool isValid = false;
                string uName = (string)argElements.Element(ns2 + "Username");
                string pwd = (string)argElements.Element(ns2 + "Password");

                if (!string.IsNullOrEmpty(uName) && !string.IsNullOrEmpty(pwd))
                {

                    string confgUname = WebConfigurationManager.AppSettings["BappUnm"].DecryptRijndael(objLocalVars.EncryptedToken);
                    string confgPwd = WebConfigurationManager.AppSettings["BappPwd"].DecryptRijndael(objLocalVars.EncryptedToken);

                    uName = uName.DecryptRijndael(objLocalVars.EncryptedToken);
                    pwd = pwd.DecryptRijndael(objLocalVars.EncryptedToken);

                    if (confgUname.Equals(uName) && confgPwd.Equals(pwd))
                        isValid = true;
                    else
                        isValid = false;
                }
                return isValid;
            }
            catch
            {
                throw;
            }
        }

    }
}