﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
   public  class DatawireServiceUrlDto
    {
        public string Url
        {
            get;
            set;
        }

        public long TransactionTime
        {
            get;
            set;
        }

        public int MaximumTransactionInPackage
        {
            get;
            set;
        }
    }
}
