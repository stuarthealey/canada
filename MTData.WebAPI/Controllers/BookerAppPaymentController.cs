﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Configuration;
using System.Web.Configuration;
using System.Web.Http;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.WebAPI.Common;
using MTData.WebAPI.Models;
using MTData.WebAPI.Resources;
using MTData.WebAPI.CanadaCert;

namespace MTData.WebAPI.Controllers
{
    public class BookerAppPaymentController : TransactionRequestController
    {
        #region Definitions

        protected class ParseException : Exception
        {
            public ParseException(string message, Exception innerException) : base(message, innerException)
            {
            }
        }

        #endregion

        #region (Constructor Loading)

        private readonly ITransactionService _transactionService;
        private readonly ITerminalService _terminalService;
        private readonly IMerchantService _merchantService;
        private readonly ICommonService _commonService;
        private readonly IVehicleService _vehicleService;

        private readonly StringBuilder _logMessage;
        readonly ILogger _logger = new Logger();

        public BookerAppPaymentController(
            [Named("TransactionService")] ITransactionService transactionService,
            [Named("MerchantService")] IMerchantService merchantService,
            [Named("TerminalService")] ITerminalService terminalService,
            [Named("CommonService")] ICommonService commonService,
            [Named("VehicleService")] IVehicleService vehicleService,
            StringBuilder logMessage) : base(terminalService, logMessage)
        {
            _merchantService = merchantService;
            _transactionService = transactionService;
            _terminalService = terminalService;
            _logMessage = logMessage;
            _commonService = commonService;
            _vehicleService = vehicleService;
        }

        private fDMerchantDto _fdMerchantobj;
        private DatawireDto _datawireObj;
        
        #endregion
        
        #region HttpResponseMethods

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpGet]
        public HttpResponseMessage GenerateRSAKey()
        {
            return new HttpResponseMessage
            {
                Content = new StringContent(RSAEncryptionLogic.Instance.GenerateNewKey(RSAEncryptionLogic.RSAKeySize.RSA4096))
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> BasicAuthorisation(HttpRequestMessage request)
        {
            _logMessage.AppendLine("BookerApp MakePayment process start:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");

            BasicAuthoriseResponse basicAuthoriseResponse;

            try
            {
                _logMessage.AppendLine("Reading request content");
                string requestContent = await request.Content.ReadAsStringAsync();

                _logMessage.AppendLine("Parsing request as XDocument");
                
                BasicAuthoriseRequest basicAuthoriseRequest;

				XDocument requestEnvelope = XDocument.Parse(requestContent);

				string xmlRequest = requestEnvelope.Root.Value;
				try
                {
                    //_logMessage.AppendLine(string.Format("XML to BasicAuthoriseRequest. XML:{0};", xmlRequest));
                    _logger.LogInfoMessage(string.Format("BookerApp MakePayment XML to BasicAuthoriseRequest. XML:{0};", xmlRequest));

                    basicAuthoriseRequest = BasicAuthoriseRequestFromXmlString(xmlRequest.Trim());
                }
                catch (Exception ex)
                {
                    _logMessage.AppendLine("Casting request XML as BasicAuthoriseRequest type failed");
                    _logger.LogInfoFatal(_logMessage.ToString(), ex);

                    // XmlSerializer.Deserialize throws a rather generic System.InvalidOperationException. Wrap this in a ParseException so that it is handled verbosely
                    throw new ParseException("Error parsing SOAP payload (inner XML): " + ex.Message, ex);
                }

                BasicAuthorise basicAuthorise = basicAuthoriseRequest.BasicAuthorise;
                AuthenticationHeader authenticationHeader = basicAuthoriseRequest.AuthenticationHeader;

                _logMessage.AppendLine("Creating encryption logic");

                // Initialize approprate EncryptionLogic
                IEncryptionLogic encryptionLogic = null;
                switch (basicAuthorise.encryptionAlgorithm)
                {
                    case 0:
                        encryptionLogic = new PassthroughEncryptionLogic();
                        break;

                    case 1:
                        encryptionLogic = RSAEncryptionLogic.Instance;
                        break;

                    case 5:
                        if (basicAuthorise.encryptionKeyVersion > 0 && basicAuthorise.encryptionKeyVersion < 13)
                        {
                            string RSAPassword = CultureInfo.InvariantCulture.DateTimeFormat.GetMonthName(basicAuthorise.encryptionKeyVersion).ToLower();
                            encryptionLogic = new AESEncryptionLogic(RSAPassword, true);

                            _logMessage.AppendLine("Encryption logic created");
                        }
                        else
                        {
                            _logMessage.AppendLine("Invalid encryptionKeyVersion supplied: " + basicAuthorise.encryptionKeyVersion);
                        }
                        break;

                    default:
                        _logMessage.AppendLine("Invalid encryptionAlgorithm supplied: " + basicAuthorise.encryptionAlgorithm);
                        break;
                }

                string password;
                string unEncryptedToken;

                // Authenticate User
                _logMessage.AppendLine("Authenticating request");

                if (encryptionLogic == null)
                {
                    _logMessage.AppendLine("Request Authentication process failed with invalid encryptionAlgorithm/encryptionKeyVersion");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    basicAuthoriseResponse = CommonFunctions.BasicAuthFailedResp("Invalid encryptionAlgorithm or encryptionKeyVersion");
                }
                else if (authenticationHeader.Username != WebConfigurationManager.AppSettings["BookerAppUsername"])
                {
                    _logMessage.AppendLine("Request Authentication process failed: invalid Username: " + authenticationHeader.Username);
                    _logger.LogInfoMessage(_logMessage.ToString());

                    basicAuthoriseResponse = CommonFunctions.BasicAuthFailedResp("Invalid Credentials");
                }
                else if (!encryptionLogic.TryDecrypt(authenticationHeader.Password, out password))
                {
                    _logMessage.AppendLine("Request Authentication process failed: could not decrypt Password");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    basicAuthoriseResponse = CommonFunctions.BasicAuthFailedResp("Invalid Credentials");
                }
                else if (password != WebConfigurationManager.AppSettings["BookerAppPassword"])
                {
                    _logMessage.AppendLine("Request Authentication process failed: invalid Password: " + password);
                    _logger.LogInfoMessage(_logMessage.ToString());

                    basicAuthoriseResponse = CommonFunctions.BasicAuthFailedResp("Invalid Credentials");
                }
                else if (!encryptionLogic.TryDecrypt(basicAuthorise.encryptedToken, out unEncryptedToken))
                {
                    _logMessage.AppendLine("Request Authentication process failed: could not decrypt encryptedToken");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    basicAuthoriseResponse = CommonFunctions.BasicAuthFailedResp("Invalid encryptedToken");
                }
                else if (unEncryptedToken != "Mobile Tracking and Data")
                {
                    _logMessage.AppendLine("Request Authentication process failed: Invalid encryptedToken value: " + unEncryptedToken);
                    _logger.LogInfoMessage(_logMessage.ToString());

                    basicAuthoriseResponse = CommonFunctions.BasicAuthFailedResp("Invalid encryptedToken");
                }
                else // User is authenticated, return verbose errors from this point onward 
                {
                    _logMessage.AppendLine("Request credentials authenticated");

                    List<string> errorList = new List<string>();

                    // Check required fields
                    if (authenticationHeader.Token != WebConfigurationManager.AppSettings["BookerAppToken"])
                        errorList.Add("Invalid Token");

                    if (string.IsNullOrWhiteSpace(basicAuthorise.requestId))
                        errorList.Add("requestId is required");

                    if (string.IsNullOrWhiteSpace(basicAuthorise.deviceId))
                        errorList.Add("deviceId is required");

                    if (!basicAuthorise.userId.HasValue)
                        errorList.Add("userId is required");

                    if (!basicAuthorise.jobId.HasValue)
                        errorList.Add("jobId is required");

                    if (string.IsNullOrWhiteSpace(basicAuthorise.transactionType))
                        errorList.Add("transactionType is required");

                    _logMessage.AppendLine("handling transaction type");
                    TxnTypeType txnTypeType;
                    switch (basicAuthorise.transactionType.ToUpper())
                    {
                        case "PUR":
                            txnTypeType = TxnTypeType.Sale;
                            break;
                        case "INQ":
                            txnTypeType = TxnTypeType.BalanceInquiry;
                            break;
                        case "PREAUTH":
                            txnTypeType = TxnTypeType.Verification; //TODO: verify this is correct
                            break;
                        case "COMPLETION":
                            txnTypeType = TxnTypeType.Completion;
                            break;
                        default:
                            txnTypeType = TxnTypeType.Sale;
                            errorList.Add("Invalid transactionType");
                            break;
                    }

                    _logMessage.AppendLine("handling entry mode");
                    string entryMode;
                    switch (basicAuthorise.swipeMethod)
                    {
                        case 0:
                            entryMode = "Swiped";
                            break;
                        case 1:
                            entryMode = "EmvContactless";
                            break;
                        case 2:
                            entryMode = "Keyed";
                            break;
                        default:
                            entryMode = "None";
                            break;
                    }

                    // Retrieve payment details
                    string rcci;
                    encryptionLogic.TryDecrypt(basicAuthorise.RCCI, out rcci);

                    _logMessage.AppendLine("decrypting swipe data");

                    string swipeData;
                    encryptionLogic.TryDecrypt(basicAuthorise.swipeData, out swipeData);

                    _logMessage.AppendLine(string.Format("rcci:{0};swipeData:{1};", rcci, swipeData));

                    string cardNumber = null;
                    string cvv = null;
                    string expiryDate = null;
                    CardTypeType cardType = new CardTypeType(); //This implicately sets the default cardType to Amex (the first type specified in CardTypeType)

					CardTokenDto cardToken = null;

                    if (!string.IsNullOrWhiteSpace(rcci))
                    {
                        _logMessage.AppendLine("Making payment via RCCI token");

						//	POD : rcci is a combination of CH:<customerID>:<cardToken.Token>
						if (!rcci.StartsWith("CH:"))
						{
							errorList.Add("Invalid RCCI - Not generated by this system");
						}
						else
						{
							string[] parts = rcci.Split(':');
							if (parts.Length < 3)
							{
								errorList.Add("Invalid RCCI - Incorrect Format");
							}
							else
							{
								string customerID = parts[1];
								rcci = parts[2];

								cardToken = _transactionService.GetCardToken(customerID, rcci);
								if (cardToken == null)
								{
									errorList.Add(string.Format("Invalid RCCI - token Not Found. CustomerID:{0};RCCI:{1};", customerID, rcci));
								}
								else
								{
									Enum.TryParse(cardToken.CardType, out cardType);
									expiryDate = cardToken.ExpiryDate.Decrypt();

									_logMessage.AppendLine("handling expiry date");

									DateTime expiryDateTime;
									if (DateTime.TryParseExact(expiryDate, (expiryDate != null && expiryDate.Length == 6) ? "yyyyMM" : "yyMM", CultureInfo.InvariantCulture, DateTimeStyles.None, out expiryDateTime))
										expiryDate = expiryDateTime.ToString("yyyyMM");
									else
										errorList.Add("expiryDate must be of the format yyMM");

                                    _logMessage.AppendLine(string.Format("expiry date:{0};", expiryDate));
                                }
                            }
						}
                    }
                    //TODO: need to implement payment via swiped data (MSR Payment)
                    //else if (!string.IsNullOrEmpty(swipeData))
                    //{
                    //  CardInfo cardInfo = StringExtension.GetCardNumberData(swipeData);

                    //  cardNumber = cardInfo.CardNumber;
                    //  cvv = cardInfo.ServiceCode;
                    //  expiryDate = cardInfo.YYYYMM;

                    //  cardType = CreditCardType.GetCardType(cardInfo.CardNumber);

                    //  cardType = new CardTypeType();
                    //}
                    else
                    {
                        _logMessage.AppendLine("Making payment via card detail");

                        encryptionLogic.TryDecrypt(basicAuthorise.accountNumber, out cardNumber);

                        if (string.IsNullOrWhiteSpace(cardNumber))
                            errorList.Add("accountNumber is required");
                        else
                            cardType = CreditCardType.GetCardType(cardNumber);

                        encryptionLogic.TryDecrypt(basicAuthorise.cvv2, out cvv);
                        if (string.IsNullOrWhiteSpace(cvv))
                            errorList.Add("cvv2 is required");

                        _logMessage.AppendLine("handling expiry date");
                        if (encryptionLogic.TryDecrypt(basicAuthorise.expiryDate, out expiryDate))
                        {
                            DateTime expiryDateTime;

                            if (DateTime.TryParseExact(expiryDate, "yyMM", CultureInfo.InvariantCulture, DateTimeStyles.None, out expiryDateTime))
                                expiryDate = expiryDateTime.ToString("yyyyMM");
                            else
                                errorList.Add("expiryDate must be of the format yyMM");

                            _logMessage.AppendLine(string.Format("expiry date:{0};", expiryDate));
                        }
                        else
                        {
                            errorList.Add("expiryDate is required");
                        }
                    }

                    // If data is complete, attempt to Authorize
                    _logMessage.AppendLine("starting payment process");
                    if (errorList.Count == 0)
                    {
                        TerminalDto terminal;
                        VehicleDto vehicle;
                        FirstDataDetailsResultDto firstDataDetails;
                        string clientRef;

                        _logMessage.AppendLine("fetching txn data from database");
                        if ((firstDataDetails = _commonService.GetFirstDataDetails(basicAuthorise.deviceId, null, null, 0, null)).IsNull())
                        {
                            string errorMessage = string.Concat("Failed to retreive FirstData details for Device ID: ", basicAuthorise.deviceId);
                            _logMessage.AppendLine(errorMessage);
                            _logger.LogInfoMessage(_logMessage.ToString());

                            basicAuthoriseResponse = CommonFunctions.BasicAuthFailedResp(errorMessage);
                        }
                        else if ((terminal = _terminalService.GetTerminalDto(basicAuthorise.deviceId)).IsNull())
                        {
                            string errorMessage = string.Concat("Failed to retreive terminal details for Device ID: ", basicAuthorise.deviceId);
                            _logMessage.AppendLine(errorMessage);
                            _logger.LogInfoMessage(_logMessage.ToString());

                            basicAuthoriseResponse = CommonFunctions.BasicAuthFailedResp(errorMessage);
                        }
                        else if ((vehicle = _vehicleService.GetVehicleByTerminal(terminal.TerminalID)).IsNull())
                        {
                            string errorMessage = string.Concat("Failed to retreive vehicle details for Terminal ID: ", terminal.TerminalID);
                            _logMessage.AppendLine(errorMessage);
                            _logger.LogInfoMessage(_logMessage.ToString());

                            basicAuthoriseResponse = CommonFunctions.BasicAuthFailedResp(errorMessage);
                        }
                        else if ((_fdMerchantobj = _merchantService.GetFDMerchant(terminal.fk_MerchantID)).IsNull())
                        {
                            string errorMessage = string.Concat("Failed to retreive merchant details for Merchant ID: ", terminal.fk_MerchantID);
                            _logMessage.AppendLine(errorMessage);
                            _logger.LogInfoMessage(_logMessage.ToString());

                            basicAuthoriseResponse = CommonFunctions.BasicAuthFailedResp(errorMessage);
                        }
                        else if ((_datawireObj = _merchantService.GetDataWireDetails(terminal.fk_MerchantID, terminal.DatawireId.GetValueOrDefault())).IsNull())
                        {
                            string errorMessage = string.Concat("Failed to retreive Datawire details for Merchant ID: ", terminal.fk_MerchantID, ", Datawire ID: ", terminal.DatawireId);
                            _logMessage.AppendLine(errorMessage);
                            _logger.LogInfoMessage(_logMessage.ToString());

                            basicAuthoriseResponse = CommonFunctions.BasicAuthFailedResp(errorMessage);
                        }
                        else if ((clientRef = GetClientRef(firstDataDetails.EcommProjectId)).IsNull() && WebConfigurationManager.AppSettings["Project"] == PaymentAPIResources.USProject)
                        {
                            string errorMessage = string.Concat("Failed to retreive ClientRef for Project ID: ", firstDataDetails.EcommProjectId);

                            _logMessage.AppendLine(errorMessage);
                            _logger.LogInfoMessage(_logMessage.ToString());

                            basicAuthoriseResponse = CommonFunctions.BasicAuthFailedResp(errorMessage);
                        }
                        else
                        {
                            _fdMerchantobj.ProjectId = firstDataDetails.EcommProjectId;

                            var vehicleData = _vehicleService.GetVehicleByTerminal(terminal.TerminalID);

                            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                            {
                                if (cardToken != null)
                                    _logMessage.AppendLine(string.Format("CardToken. Token:{0};ExpDate:{1};Postcode:{2};Num:{3};PassedRCCI:{4};ProjID:{5};TokenType:{6};FD_ID:{7};", cardToken.Token, expiryDate, cardToken.Postcode, cardToken.CardNumber, rcci, firstDataDetails.EcommProjectId, firstDataDetails.TokenType, firstDataDetails.FDId));
                                else
                                    _logMessage.AppendLine("CardToken is <null>");
                            }

                            _logMessage.AppendLine("Creating transaction request");
                            TransactionRequest tr = new TransactionRequest
                            {
                                SerialNumber = basicAuthorise.deviceId,
                                VehicleNumber = vehicle.VehicleNumber,
                                FleetId = vehicle.fk_FleetID.ToString(),
                                JobNumber = basicAuthorise.jobId.ToString(),
                                RequestedId = basicAuthorise.requestId,
                                TransactionType = txnTypeType.ToString(),
                                TransType = txnTypeType,
                                PaymentType = PymtTypeType.Credit,
                                CardType = cardType,
                                IndustryType = PaymentAPIResources.BookerAppPayment,
                                TransactionAmount = basicAuthorise.paymentAmt.ToString(),
                                CardNumber = cardNumber,
                                ExpiryDate = expiryDate,
                                CardCvv = cvv,
                                FirstFourCard = cardNumber.First(4),
                                LastFourcard = cardNumber.Last(4),
                                Track2Data = swipeData,
                                DriverNumber = basicAuthorise.userId.ToString(),
                                StartLatitude = basicAuthorise.pickupLatitude.ToString(),
                                StartLongitude = basicAuthorise.pickupLongitude.ToString(),
                                EndLatitude = basicAuthorise.dropoffLatitude.ToString(),
                                EndLongitude = basicAuthorise.dropoffLongitude.ToString(),
                                Fare = basicAuthorise.fareAmt.ToString(),
                                Tip = basicAuthorise.tipAmt.ToString(),
                                Tolls = basicAuthorise.tollAmt.ToString(),
                                FlagFall = basicAuthorise.flagfallAmt.ToString(),
                                Extras = basicAuthorise.extrasAmt.ToString(),
								
                                //-- Set default value for fields to prevent NULL in the MTDTransaction table.
                                //TODO: Database default for null values should be set downstream rather than populating them in the FirstData authorization request
								Taxes = "0.00",
                                Surcharge = "0.00",
                                Fee = "0.00",
                                TechFee = 0,
                                FleetFee = 0,
                                TransNote = string.Empty,
                                PickAddress = string.Empty,
                                DestinationAddress = string.Empty,
								//----------------------------------------------

                                EntryMode = entryMode,
                                LocalDateTime = DateTime.Now.ToString("yyyyMMddHHmmss"),
                                TrnmsnDateTime = DateTime.UtcNow.ToString("yyyyMMddHHmmss"),
                                FdMerchantId = Convert.ToString(_fdMerchantobj.FDMerchantID),
                                MtdMerchantId = terminal.fk_MerchantID,
                                TerminalId = _datawireObj.RCTerminalId,
                                TppId = _fdMerchantobj.ProjectId,
                                GroupId = Convert.ToString(_fdMerchantobj.GroupId),
                                Did = _datawireObj.DID,
                                App = _fdMerchantobj.App,
                                ServiceId = Convert.ToString(_fdMerchantobj.ServiceId),
                                ServiceUrl = _fdMerchantobj.ServiceUrl,
                                CreatedBy = "BookerAppPayment",
                                RCCIToken = (cardToken != null) ? cardToken.Token : null, //PODbasicAuthorise.RCCI,
                                TokenType = firstDataDetails.TokenType,
                                CurrencyCode = PaymentAPIResources.Cur_USA,
                                ZipCode = PaymentAPIResources.NoZipCode
                            };

                            // Prepare and send booker app payment request with token and card number for canada
                            if (WebConfigurationManager.AppSettings["Project"] == PaymentAPIResources.CanadaProject)
                            {
                                _logMessage.AppendLine("Canada project processing.");

                                int transactionId;
                                tr.RCCIToken = rcci;

                                if (!string.IsNullOrEmpty(rcci))
                                    tr.CardCvv = basicAuthorise.cvv2;

                                tr.MerchantZipCode = _merchantService.GetMerchantZipCode(tr.MtdMerchantId);

                                tr.IsTransArmor = _merchantService.IsTransArmor(tr.MtdMerchantId);      //PAY-13 Get the Merchant UseTransArmor flag as well.

                                // Added for AVS
                                //tr.ZipCode = basicAuthorise.zipCode;      // Zip being taken from the RCCI token details.
                                tr.StreetAddress = basicAuthorise.streetAddress;

                                tr.CurrencyCode = PaymentAPIResources.Cur_Cn;

                                //PAY-12 We need to populate this for Canada, I believe.
                                tr.ZipCode = (cardToken != null) ? cardToken.Postcode : basicAuthorise.zipCode;

                                CnBookerAppTokenPayment cnBookerAppTokenPayment = new CnBookerAppTokenPayment(_transactionService, _merchantService, _terminalService, _logMessage);

                                CardTokenDto storedCardToken = null;
                                Iso8583Response iso8583Response = cnBookerAppTokenPayment.CnBookerAppTransaction(tr, null, out transactionId, out storedCardToken);

                                if (object.Equals(iso8583Response, null))
                                {
                                    _logMessage.AppendLine("ISO8583 response is <null>");

                                    basicAuthoriseResponse = CommonFunctions.BasicAuthFailedResp(PaymentAPIResources.CnBookerAppFailedError, transactionId);
                                }
                                else
                                {
                                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                    {
                                        _logMessage.AppendLine(string.Format("ISO8583 RespCode:{0};RespMsgID:{1};RetCode:{2};MerchID:{3};iso.Token:{4};rcci:{5};", iso8583Response.ResponseCode, iso8583Response.ResponseMessageID, iso8583Response.ReturnCode, iso8583Response.MerchantId, iso8583Response.Token, rcci));
                                        _logMessage.AppendLine(string.Format("StoredToken:{0};Postcode:{1};TxnID:{2};", storedCardToken.Token, storedCardToken.Postcode, transactionId));
                                    }

                                    basicAuthoriseResponse = CommonFunctions.CanadaBasicAuthResp(tr,
                                                                                                iso8583Response,
                                                                                                transactionId,
                                                                                                basicAuthorise.jobId.GetValueOrDefault(),
                                                                                                (cardToken != null) ? (cardToken.CardNumber != null ? cardToken.CardNumber.Last(4) : null) : null);
                                }

                                // Output the logging.
                                _logger.LogInfoMessage(_logMessage.ToString());
                            }
                            else
                            {
                                #region US

                                //US\other regions app payment processing.
                                _logMessage.AppendLine("Creating transaction request as per UMF specification");
                                CreateTransactionRequest(tr);

                                string xmlSerializedTransReq = GetXmlData();

                                _logMessage.AppendLine("Fetching XmlToTransactionDto");
                                MTDTransactionDto transactionDto = CommonFunctions.XmlToTransactionDto(tr);

                                _logMessage.AppendLine("Adding transaction requiest into database");
                                int transId = _transactionService.Add(transactionDto);

                                _logMessage.AppendLine("Sending transaction to payment processor");
                                string xmlSerializedTransResp = new SoapHandler().SendMessage(xmlSerializedTransReq, clientRef, tr);

                                if (!string.IsNullOrEmpty(xmlSerializedTransResp))
                                {
                                    _logMessage.AppendLine("Fetching response into XmlResponseToTransactionDto");
                                    MTDTransactionDto responsetransactionDto = CommonFunctions.XmlResponseToTransactionDto(xmlSerializedTransResp, false, tr.PaymentType.ToString(), tr.IndustryType);

                                    _logMessage.AppendLine("Updating transaction response into database");
                                    _transactionService.Update(responsetransactionDto, transId, tr.RapidConnectAuthId);

                                    basicAuthoriseResponse = CommonFunctions.BasicAuthResp(tr, 
                                                                                            xmlSerializedTransResp, 
                                                                                            transId, 
                                                                                            basicAuthorise.jobId.GetValueOrDefault(),
                                                                                            (cardToken != null) ? (cardToken.CardNumber != null ? cardToken.CardNumber.Last(4) : null) : null);

                                    _logMessage.AppendLine(string.Concat("BasicAuthorisation received First Data response: ", xmlSerializedTransResp));
                                    _logMessage.AppendLine("BookerApp payment process completed");
                                    _logger.LogInfoMessage(_logMessage.ToString());
                                }
                                else
                                {
                                    _logMessage.AppendLine("Processing timeout transaction");
                                    MTDTransactionDto responsetransactionDto = CommonFunctions.XmlResponseToTransactionDto(xmlSerializedTransResp, true, tr.PaymentType.ToString(), tr.IndustryType);
                                    _transactionService.Update(responsetransactionDto, transId, tr.RapidConnectAuthId);

                                    TimeoutTransaction objTimeout = new TimeoutTransaction(_transactionService, _terminalService, _merchantService, _logMessage);
                                    xmlSerializedTransResp = objTimeout.TimeOut(tr, transId, clientRef, xmlSerializedTransResp);

                                    if (!string.IsNullOrEmpty(xmlSerializedTransResp))
                                    {
                                        basicAuthoriseResponse = CommonFunctions.BasicAuthResp(tr, 
                                                                                                xmlSerializedTransResp, 
                                                                                                transId, 
                                                                                                basicAuthorise.jobId.GetValueOrDefault(),
                                                                                                (cardToken != null) ? (cardToken.CardNumber != null ? cardToken.CardNumber.Last(4) : null) : null);

                                        _logMessage.AppendLine(string.Concat("BasicAuthorisation received First Data response: ", xmlSerializedTransResp));
                                    }
                                    else
                                    {
                                        basicAuthoriseResponse = CommonFunctions.BasicAuthFailedResp("Error communicating with First Data services.", transId);

                                        _logMessage.AppendLine(string.Concat("No data returned from Fist Data Services.  TransID: ", transId));
                                    }

                                    _logMessage.AppendLine("BookerApp payment process completed with timeout transaction.");

                                    _logger.LogInfoMessage(_logMessage.ToString());
                                }
                                #endregion US
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = string.Join(", ", errorList);
                        basicAuthoriseResponse = CommonFunctions.BasicAuthFailedResp(errorMessage);

                        _logMessage.AppendLine(string.Concat("BasicAuthorisation failed with following errors: ", errorMessage));
                        _logger.LogInfoMessage(_logMessage.ToString());
                    }
                }
            }
            catch (XmlException xmlEx)
            {
                basicAuthoriseResponse = CommonFunctions.BasicAuthFailedResp("XML Error:" + xmlEx.Message);

                _logMessage.AppendLine("BookerApp payment process completed with exception.");
                _logger.LogInfoFatal(_logMessage.ToString(), xmlEx);
            }
            catch (ParseException parseEx)
            {
                basicAuthoriseResponse = CommonFunctions.BasicAuthFailedResp(parseEx.Message);

                _logMessage.AppendLine("BookerApp payment process completed with exception.");
                _logger.LogInfoFatal(_logMessage.ToString(), parseEx);
            }
            catch (Exception ex)
            {
                basicAuthoriseResponse = CommonFunctions.BasicAuthFailedResp(PaymentAPIResources.Exception);

                _logMessage.AppendLine("BookerApp payment process completed with exception.");
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            string responseInnerXML = BasicAuthoriseResponseToXmlString(basicAuthoriseResponse);

            string soapXmlnsPrefix = "soap";
            XNamespace soapXmlns = XNamespace.Get("http://schemas.xmlsoap.org/soap/envelope/");
            XNamespace tempuriXmlns = XNamespace.Get("http://tempuri.org/");
            XNamespace xsi = XNamespace.Get("http://www.w3.org/2001/XMLSchema-instance");
            XNamespace xsd = XNamespace.Get("http://www.w3.org/2001/XMLSchema");

            XDocument responseEnvelope = new XDocument(new XDeclaration("1.0", "utf-8", null),
            new XElement(soapXmlns + "Envelope",
            new XAttribute(XNamespace.Xmlns + soapXmlnsPrefix, soapXmlns),
            new XAttribute(XNamespace.Xmlns + "xsi", xsi),
            new XAttribute(XNamespace.Xmlns + "xsd", xsd)));

            responseEnvelope.Root.Add(
                new XElement(soapXmlns + "Body",
                    new XElement(tempuriXmlns + "BasicAuthorisationResponse",
                        new XElement(tempuriXmlns + "BasicAuthorisationResult", responseInnerXML))));

            string soapResponse = responseEnvelope.ToString(SaveOptions.DisableFormatting);

            return new HttpResponseMessage() { Content = new StringContent(soapResponse, Encoding.UTF8, "application/soap+xml") };
        }

        #endregion

        #region Private Methods

        private BasicAuthoriseRequest BasicAuthoriseRequestFromXmlString(string xmlRequest)
        {
            using (MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlRequest)))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(BasicAuthoriseRequest), "http://services.mtdata.com/payment");
                return (BasicAuthoriseRequest)xmlSerializer.Deserialize(memoryStream);
            }
        }

		/// <summary>
		/// convert BasicAuthoriseResponse to result xml
		/// </summary>
		/// <remarks>
		/// Namespace removed from output as MDC code does not expect it and will fail if it is present.
		/// </remarks>
		/// <param name="response"></param>
		/// <returns></returns>
        private string BasicAuthoriseResponseToXmlString(BasicAuthoriseResponse response)
        {
            var xmlSerializer = new XmlSerializer(typeof(BasicAuthoriseResponse), "");

            XmlWriterSettings settings = new XmlWriterSettings
            {
                Encoding = new UTF8Encoding(false), // surpress BOM that will cause havoc when included inside a SOAP evelope
                Indent = false,
                NewLineHandling = NewLineHandling.None,
                OmitXmlDeclaration = true
            };

            using (var memoryStream = new MemoryStream())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(memoryStream, settings))
                {
                    xmlSerializer.Serialize(xmlWriter, response);
                    return Encoding.UTF8.GetString(memoryStream.ToArray());
                }
            }
        }

        #endregion
    }
}
