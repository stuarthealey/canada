﻿using MTD.Data.Data;
namespace MTD.Data.Repository.Interface
{
    public  interface IDefaultRepository
    {
        void Update(DefaultSetting defaultSetting, int defaultId);
        DefaultSetting GetDefault(int defaultId);
        TransactionCountSlab GetTransactionCountSlab(int merchantId);
        TransactionCountSlab GetTransactionCountSlabCorp(int userId);
        void Update(TransactionCountSlab defaultSetting, int merchantId);
        void UpdateCorp(TransactionCountSlab defaultSetting, int userId);
    }
}
