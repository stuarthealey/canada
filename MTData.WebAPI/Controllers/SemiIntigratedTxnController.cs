﻿using System.Globalization;
using System.Reflection;
using System.Text;
using System.Web.Http;
using System.Net.Http;
using System;
using System.Xml;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.WebAPI.Models;
using MTData.WebAPI.Resources;
using MTData.WebAPI.Common;

namespace MTData.WebAPI.Controllers
{
    public class SemiIntigratedTxnController : TransactionRequestController
    {
        private readonly ITransactionService _transactionService;
        private readonly ITransactionServiceUK _transactionServiceUk;
        private LocalVariables objLocalVars = new LocalVariables();
        private readonly StringBuilder _logMessage;
        private readonly ICommonService _commonService;
        private StringBuilder _logTracking;
        readonly ILogger _logger = new Logger();

        LoginHeader mHeader = new LoginHeader();
        PaymentResponse response = new PaymentResponse();

        public SemiIntigratedTxnController(
            [Named("TransactionService")] ITransactionService transactionService, [Named("TerminalService")] ITerminalService terminalService, StringBuilder logMessage, StringBuilder logTracking,
            [Named("UkTxnService")] ITransactionServiceUK transactionServiceUK,
            [Named("CommonService")] ICommonService commonService) : base(terminalService, logMessage)
        {
            _transactionService = transactionService;
            _logMessage = logMessage;
            _logTracking = logTracking;
            _transactionServiceUk = transactionServiceUK;
            _commonService = commonService;
        }

        /// <summary>
        /// Purpose             :   To save UK transaction data into database after for a valid request
        /// Function Name       :   SaveTxnRequestResponse
        /// Created By          :   
        /// Created On          :   
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   
        /// </summary>Sending data...
        /// <param name="request"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        public IHttpActionResult SaveTxnRequestResponse(HttpRequestMessage request)
        {
            MtdtxnSemiUKDto semiIntigratedTxn = new MtdtxnSemiUKDto();
            try
            {
                _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                XmlDocument xmlDocumentRequest = new XmlDocument();
                xmlDocumentRequest.Load(request.Content.ReadAsStreamAsync().Result);

                // Log the received XML, in case we lose everything after this because of an exception.
                _logger.LogInfoMessage(string.Format("SemiIntigratedTxnController.SaveTxnRequestResponse() Received new request. XML:{0};", xmlDocumentRequest.OuterXml));

                // Validate Request
                _logMessage.AppendLine("Validating request xml data");

                if (!ValidateRequest(response, mHeader, xmlDocumentRequest))
                //zzz Chetu 357if (!ValidateRequest(response, mHeader, xmlDocumentRequest))
                {
                    _logMessage.AppendLine("Failed to validate request xml data.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    return Ok(response);
                }

                int merchantId = 0;
                int fleetId = 0;
                string driverNo = string.Empty;
                string vehicleNo = string.Empty;

                //Loading request XML into object
                _logMessage.AppendLine("calling SemiIntigratedTxn()");

                semiIntigratedTxn = SemiIntigratedTxn(xmlDocumentRequest, out merchantId, out fleetId, out driverNo, out vehicleNo);

                _logMessage.AppendLine("calling CreateTxnData()");

                MTDTransactionDto mtdData = CreateTxnData(semiIntigratedTxn);

                mtdData.MerchantId = merchantId;
                mtdData.fk_FleetId = fleetId;
                mtdData.DriverNo = driverNo;
                mtdData.VehicleNo = vehicleNo;

                // Calculate the tech\booking\fee charges for the transaction.
                _logMessage.AppendLine("Calculating Tech Fee.");

                decimal fleetFee = 0;
                decimal techFee = 0;
                decimal txnFee = 0;

                decimal tip = Convert.ToDecimal(mtdData.Tip);
                decimal surcharge = Convert.ToDecimal(mtdData.Surcharge);
                decimal fareValue = Convert.ToDecimal(mtdData.FareValue);

                decimal fareTipSurcharge = fareValue + surcharge + tip;

                PaymentController.GetFleetTechFee(_transactionService, _commonService, mtdData.TxnType, fleetId, _logger, fareTipSurcharge, out fleetFee, out techFee, out txnFee);
                //zzz Chetu 357 : PaymentController.GetFleetTechFeeMTI(_transactionService, _commonService, mtdData.TxnType, fleetId, _logger, fareTipSurcharge, out fleetFee, out techFee, out txnFee);

                mtdData.FleetFee = fleetFee;
                mtdData.TechFee = techFee;
                mtdData.Fee = txnFee;

                _logMessage.AppendLine(string.Format("Calculated Tech Fee. FleetID:{0};TxnType:{1};FareTipSurcharge:{2};FleetFee:{3};TechFee:{4};Fee:{5};", fleetId, mtdData.TxnType, fareTipSurcharge, fleetFee, techFee, txnFee));

                _logMessage.AppendLine("calling CreateXml() of CommonFunctions");

                semiIntigratedTxn.RequestXml = CommonFunctions.CreateXml(semiIntigratedTxn);

                _logMessage.AppendLine("calling AddSemiTxn() of _transactionServiceUk");

                int txnID = _transactionServiceUk.AddSemiTxn(semiIntigratedTxn, mtdData);

                response.Header = CommonFunctions.FailedResponse("Transaction Id: " + txnID, objLocalVars.DeviceId, objLocalVars.RequestId, status: "Success");

                _logMessage.AppendLine("SaveTxnRequestResponse (UK) process completed successfully.");

                _logger.LogInfoMessage(_logMessage.ToString());

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.Failed, objLocalVars.DeviceId, objLocalVars.RequestId, status: "Failed");
                return Ok(response);
            }
        }

        /// <summary>
        /// Purpose: To load request xml into MtdtxnSemiUKDto object
        /// </summary>
        /// <param name="xmlDocumentRequest"></param>
        /// <param name="xmlDocumentResponse"></param>
        /// <returns></returns>
        private MtdtxnSemiUKDto SemiIntigratedTxn(XmlDocument xmlDocumentRequest, out int merchantId, out int fleetId, out string driverNo, out string vehicleNo)
        {
            try
            {
                _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                _logMessage.AppendLine("creating  MtdtxnSemiUKDto from xmlDocumentRequest method name SemiIntigratedTxn ");

                MtdtxnSemiUKDto semiDto = new MtdtxnSemiUKDto();
                semiDto.Surcharge = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/Surcharge").InnerText);
                semiDto.Revision = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/Revision").InnerText;
                semiDto.SessionId = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/SessionId").InnerText;
                semiDto.TransactionStatus = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/TransactionStatus").InnerText;
                semiDto.EntryMethod = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/EntryMethod").InnerText;
                semiDto.ReceiptNumber = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/ReceiptNumber").InnerText;
                semiDto.AcquirerMerchantID = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/AcquirerMerchantID").InnerText;
                semiDto.DateTime = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/DateTime").InnerText;
                semiDto.GemsReceiptID = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/GemsReceiptID").InnerText;
                semiDto.AuthorizationCode = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/AuthorizationCode").InnerText;
                semiDto.AcquirerResponseCode = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/AcquirerResponseCode").InnerText;
                semiDto.Reference = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/Reference").InnerText;
                semiDto.MerchantName = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/MerchantName").InnerText;
                semiDto.MerchantAddress1 = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/MerchantAddress1").InnerText;
                semiDto.MerchantAddress2 = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/MerchantAddress2").InnerText;
                semiDto.AID = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/AID").InnerText;
                semiDto.PANSequenceNum = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/PANSequenceNum").InnerText;
                semiDto.StartDate = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/StartDate").InnerText;
                semiDto.TerminalIdentity = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/TerminalIdentity").InnerText;
                semiDto.TransactionAmount = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/TransactionAmount").InnerText);

                string IsDCCTxn = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/IsDCCTxn").InnerText;

                _logMessage.AppendLine("IsDCCTxn finished");
                if (!string.IsNullOrEmpty(IsDCCTxn))
                    semiDto.IsDCCTxn = StringToBool(IsDCCTxn);
                else
                    semiDto.IsDCCTxn = null;

                string IsLoyaltyTxn = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/IsLoyaltyTxn").InnerText;
                if (!string.IsNullOrEmpty(IsLoyaltyTxn))
                    semiDto.IsLoyaltyTxn = StringToBool(IsLoyaltyTxn);
                else
                    semiDto.IsLoyaltyTxn = null;

                semiDto.DCCAmount = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/DCCAmount").InnerText);
                semiDto.DonationAmount = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/DonationAmount").InnerText);
                semiDto.RedeemedAmount = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/RedeemedAmount").InnerText);
                semiDto.DCCCurrency = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/DCCCurrency").InnerText;
                semiDto.FXRateApplied = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/FXRateApplied").InnerText;
                
                string FXExponentApplied = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/FXExponentApplied").InnerText;
                if (!string.IsNullOrEmpty(FXExponentApplied))
                    semiDto.FXExponentApplied = StringToBool(FXExponentApplied);
                else
                    semiDto.FXExponentApplied = null;
                
                _logMessage.AppendLine("IsDCCTxn FXExponentApplied finished ");
                semiDto.DCCCurrencyExponent = Convert.ToInt32(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/DCCCurrencyExponent").InnerText);
                semiDto.MessageHost = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/MessageHost").InnerText;
                semiDto.TransactionType = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/TransactionType").InnerText;
                semiDto.GratuityAmount = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/GratuityAmount").InnerText);
                semiDto.CashAmount = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/CashAmount").InnerText);
                semiDto.TotalTransactionAmount = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/TotalTransactionAmount").InnerText);
                semiDto.ICCApplicationFileName = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/ICCApplicationFileName").InnerText;
                semiDto.ICCApplicationPreferredName = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/ICCApplicationPreferredName").InnerText;
                semiDto.eCardVerificationMethod = Convert.ToInt32(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/eCardVerificationMethod").InnerText);
                semiDto.TransactionID = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/TransactionID").InnerText;
                
                _logMessage.AppendLine("IsDCCTxn TransactionID finished");
                
                int fleetID = Convert.ToInt32(xmlDocumentRequest.SelectSingleNode("PaymentRequest/Header/FleetId").InnerText);
                merchantId = _transactionService.GetMerchantId(fleetID);
                fleetId = fleetID;
                driverNo = xmlDocumentRequest.SelectSingleNode("PaymentRequest/Header/DriverId").InnerText;
                vehicleNo = xmlDocumentRequest.SelectSingleNode("PaymentRequest/Header/VehicleNo").InnerText;
                
                // More tags
                semiDto.TransNote = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/TransNote").InnerText;
                semiDto.CardType = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/CardType").InnerText;
                semiDto.ZipCode = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/ZipCode").InnerText;
                semiDto.StreetAddress = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/StreetAddress").InnerText;
                semiDto.AuthorizationCode = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/RapidConnectAuthId").InnerText;
                semiDto.PickUpAdd = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/PickUpAdd").InnerText;
                semiDto.JobNumber = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/JobNumber").InnerText;
                semiDto.PaymentType = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/PaymentType").InnerText;
                
                //semiDto.PickUpLat = Convert.ToDouble(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/PickUpLat").InnerText);
                //semiDto.PickUpLng = Convert.ToDouble(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/PickUpLng").InnerText);
                semiDto.Others = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/Others").InnerText);
                semiDto.FlagFall = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/FlagFall").InnerText);
                semiDto.Fee = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/Fee").InnerText);
                semiDto.Fare = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/Fare").InnerText);
                semiDto.Extras = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/Extras").InnerText);
                semiDto.Tolls = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/Tolls").InnerText);
                semiDto.Taxes = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/Taxes").InnerText);

                _logMessage.AppendLine("semiDto returned successfully");

                return semiDto;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            driverNo = string.Empty;
            vehicleNo = string.Empty;
            fleetId = 0;
            merchantId = 0;

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult SaveSattlementTxn(HttpRequestMessage request)
        {
            MtdtxnSattlementUKDto semiIntigratedTxn = new MtdtxnSattlementUKDto();
            PaymentResponse response = new PaymentResponse();
            try
            {
                XmlDocument xmlDocumentRequest = new XmlDocument();
                xmlDocumentRequest.Load(request.Content.ReadAsStreamAsync().Result);

                // Validate Request
                _logMessage.AppendLine("Validating request xml data");
                //zzz Chetu 357 : if (!ValidateRequest(response, xmlDocumentRequest))
                if (!ValidateRequest(response, mHeader, xmlDocumentRequest))
                {
                    _logMessage.AppendLine("Failed to validate request xml data.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    return Ok(response);
                }

                semiIntigratedTxn = SemiIntigratedSattlementTxn(xmlDocumentRequest);
                semiIntigratedTxn.RequestXml = CommonFunctions.CreateXml(semiIntigratedTxn);
                int id = _transactionServiceUk.AddSemiSattlementTxn(semiIntigratedTxn);

                response.Header = CommonFunctions.FailedResponse("Transaction Id: " + id, objLocalVars.DeviceId, objLocalVars.RequestId, status: "Success");

                return Ok(response);
            }
            catch (Exception ex)
            {
                response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.Failed, objLocalVars.DeviceId, objLocalVars.RequestId, status: "Failed");
                return Ok(response);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDocumentRequest"></param>
        /// <param name="xmlDocumentResponse"></param>
        /// <returns></returns>
        private MtdtxnSattlementUKDto SemiIntigratedSattlementTxn(XmlDocument xmlDocumentRequest)
        {
            try
            {
                _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                _logMessage.AppendLine("creating  MtdtxnSattlementUKDto from xmlDocumentRequest method name SemiIntigratedSattlementTxn ");
                
                var selectSingleNode = xmlDocumentRequest.SelectSingleNode("PaymentRequest");
                MtdtxnSattlementUKDto semiDto = new MtdtxnSattlementUKDto();
                
                semiDto.Revision = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/Revision").InnerText;
                semiDto.MerchantIndex = Convert.ToInt32(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/MerchantIndex").InnerText);
                semiDto.settlementResult = Convert.ToInt32(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/settlementResult").InnerText);
                semiDto.AcquirerName = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/AcquirerName").InnerText;
                semiDto.MerchantAddress1 = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/MerchantAddress1").InnerText;
                semiDto.MerchantAddress2 = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/MerchantAddress2").InnerText;
                semiDto.AcquirerMerchantID = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/AcquirerMerchantID").InnerText;
                semiDto.TerminalIdentity = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/TerminalIdentity").InnerText;
                semiDto.HostMessage = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/HostMessage").InnerText;
                semiDto.FirstMessageNumber = Convert.ToInt16(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/FirstMessageNumber").InnerText);
                semiDto.LastMessageNumber = Convert.ToInt16(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/LastMessageNumber").InnerText);
                semiDto.DateTime = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/DateTime").InnerText;
                semiDto.NumScheme = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/NumScheme").InnerText;
                semiDto.QuantityDebits = Convert.ToInt32(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/QuantityDebits").InnerText);
                semiDto.ValueDebits = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/ValueDebits").InnerText);
                semiDto.QuantityCredits = Convert.ToInt32(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/QuantityCredits").InnerText);
                semiDto.ValueCredits = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/ValueCredits").InnerText);
                semiDto.QuantityCash = Convert.ToInt32(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/QuantityCash").InnerText);
                semiDto.ValueCash = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/ValueCash").InnerText);
                semiDto.QuantityClessCredits = Convert.ToInt32(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/QuantityClessCredits").InnerText);
                semiDto.ValueClessCredits = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/ValueClessCredits").InnerText);
                semiDto.QuantityClessDebits = Convert.ToInt32(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/QuantityClessDebits").InnerText);
                semiDto.ValueClessDebits = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/ValueClessDebits").InnerText);
                semiDto.CardSchemeName = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/CardSchemeName").InnerText;

                _logMessage.AppendLine("Finished");
                
                return semiDto;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stringValue"></param>
        /// <returns></returns>
        protected bool StringToBool(string stringValue)
        {
            int val = Convert.ToInt32(stringValue);
            if (val == 1)
                return true;
            else
                return false;
        }

        protected MTDTransactionDto CreateTxnData(MtdtxnSemiUKDto semiIntigratedTxn)
        {
            try
            {
                _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                _logMessage.AppendLine("creating  MTDTransactionDto from semiIntigratedTxn method name CreateTxnData ");

                MTDTransactionDto mtdDto = new MTDTransactionDto();
                string responseCode;
                mtdDto.AddRespData = MakeAdditionalResponseData(Convert.ToInt32(semiIntigratedTxn.TransactionStatus), out responseCode);
                mtdDto.EntryMode = MakeEntryMethod(Convert.ToInt32(semiIntigratedTxn.EntryMethod));
                mtdDto.ExpiryDate = StringExtension.DateFormat(semiIntigratedTxn.DateTime);

                if (!string.IsNullOrEmpty(semiIntigratedTxn.TransactionType))
                    mtdDto.TxnType = MakeTransactionType(Convert.ToInt32(semiIntigratedTxn.TransactionType));
                
                mtdDto.AuthId = semiIntigratedTxn.AuthorizationCode;
                mtdDto.IsVarified = false;
                mtdDto.IsDownloaded = false;
                mtdDto.Tip = semiIntigratedTxn.GratuityAmount;
                mtdDto.Amount = Convert.ToDecimal(semiIntigratedTxn.TotalTransactionAmount);
                mtdDto.CountryCodes = "GB";// == United Kingdom
                mtdDto.TerminalId = semiIntigratedTxn.TerminalIdentity;
                mtdDto.CreatedBy = "admin@mtdata.com";
                mtdDto.ResponseCode = responseCode;
                mtdDto.TxnDate = DateTime.Now;
                mtdDto.Industry = "Retail";
                mtdDto.Surcharge = semiIntigratedTxn.Surcharge;

                mtdDto.TransNote = semiIntigratedTxn.TransNote;
                mtdDto.CardType = semiIntigratedTxn.CardType;
                mtdDto.CrdHldrZip = semiIntigratedTxn.ZipCode;
                mtdDto.CrdHldrAddress = semiIntigratedTxn.StreetAddress;
                mtdDto.AuthId = semiIntigratedTxn.AuthorizationCode;
                mtdDto.PickAddress = semiIntigratedTxn.PickUpAdd;
                mtdDto.JobNumber = semiIntigratedTxn.JobNumber;
                mtdDto.StartLatitude = semiIntigratedTxn.PickUpLat;
                mtdDto.StartLongitude = semiIntigratedTxn.PickUpLng;
                mtdDto.OthersFee = semiIntigratedTxn.Others;
                mtdDto.FlagFall = semiIntigratedTxn.FlagFall;
                mtdDto.Fee = semiIntigratedTxn.Fee;
                mtdDto.FareValue = semiIntigratedTxn.Fare;
                mtdDto.Extras = semiIntigratedTxn.Extras;
                mtdDto.Toll = semiIntigratedTxn.Tolls;
                mtdDto.Taxes = semiIntigratedTxn.Taxes;

                _logMessage.AppendLine("created and returned mtdDto successfully");

                return mtdDto;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return null;
        }

        private string MakeAdditionalResponseData(int status, out string responseCode)
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                switch (status)
                {
                    case 0:
                        responseCode = "000";
                        _logMessage.AppendLine("responseCode = 000");
                        return "APPROVAL";
                    case 6:
                        responseCode = "000";
                        _logMessage.AppendLine("responseCode = 000");
                        return "Void";
                    default:
                        responseCode = "500";
                        _logMessage.AppendLine("responseCode = 500");
                        return "DECLINED-005";
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            responseCode = "500";

            return "DECLINED-005";
        }

        private string MakeEntryMethod(int mode)
        {
            switch (mode)
            {
                case 0:
                    return "None";
                case 1:
                    return "Keyed";
                case 2:
                    return "Swiped";
                case 3:
                    return "EmvContact";
                case 4:
                    return "EmvContactless";
                case 5:
                    return "Keyed";
                default:
                    return "None";
            }
        }

        private string MakeTransactionType(int txnType)
        {
            switch (txnType)
            {
                case 0:
                    return "Sale";
                case 1:
                    return "Refund";
                case 6:
                    return "Authorization";
                case 7:
                    return "Completion";
                case 10:
                    return "Void";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// Validating payment request.
        /// Purpose             :   Validate reqest
        /// Function Name       :   ValidateRequest
        /// Created By          :   Sunil Singh
        /// Created On          :   08/19/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :    "MM/DD/YYYY" 
        /// <param name="response"></param>
        /// <param name="mHeader"></param>
        /// <param name="firstDataDetails"></param>
        /// <param name="isFailed"></param>
        /// <returns>bool</returns>
        //zzz Chetu 357 : private bool ValidateRequest(PaymentResponse response, XmlDocument xmlDoc)
        private bool ValidateRequest(PaymentResponse response, LoginHeader mHeader, XmlDocument xmlDoc)
        {
            try
            {
                bool isPass = true;

                var dvcID = xmlDoc.SelectSingleNode("PaymentRequest/Header/DeviceId");

                if (string.IsNullOrEmpty(dvcID.InnerText))
                {
                    isPass = false;
                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.DeviceIdRequired, objLocalVars.DeviceId, objLocalVars.RequestId);
                }

                var fleetId = xmlDoc.SelectSingleNode("PaymentRequest/Header/FleetId");

                if (string.IsNullOrEmpty(fleetId.InnerText))
                {
                    isPass = false;
                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.FleetIdRequired, objLocalVars.DeviceId, objLocalVars.RequestId);
                }

                var token = xmlDoc.SelectSingleNode("PaymentRequest/Header/Token");
                dynamic firstDataDetails = string.Empty;
                bool isToken = string.IsNullOrEmpty(token.InnerText);
                if (!isToken)
                {
                    var driverNo = xmlDoc.SelectSingleNode("PaymentRequest/Header/DriverId");
                    if (string.IsNullOrEmpty(driverNo.InnerText))
                    {
                        isPass = false;
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.DeviceIdRequired, dvcID.InnerText, objLocalVars.RequestId);
                    }

                    var requestVal = xmlDoc.SelectSingleNode("PaymentRequest/Header/RequestType");
                    if (string.IsNullOrEmpty(requestVal.InnerText))
                    {
                        isPass = false;
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidRequest, dvcID.InnerText, objLocalVars.RequestId);
                    }

                    firstDataDetails = _commonService.GetFirstDataDetails(dvcID.InnerText, driverNo.InnerText, token.InnerText, Convert.ToInt32(fleetId.InnerText), requestVal.InnerText);//fleetId -requestVal.InnerText
                    if (firstDataDetails == null)
                    {
                        isPass = false;
                        response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.UnauthorizedAccess, dvcID.InnerText, objLocalVars.RequestId);
                    }
                }
                else
                {
                    isPass = false;
                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.Invalid_token, dvcID.InnerText, objLocalVars.RequestId);
                }

                return isPass;
            }
            catch
            {
                throw;
            }
        }

    }
}
