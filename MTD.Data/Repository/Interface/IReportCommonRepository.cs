﻿using System.Collections.Generic;

using MTD.Data.Data;

namespace MTD.Data.Repository.Interface
{
    public interface IReportCommonRepository
    {
        IEnumerable<Fleet> FleetList();
        IEnumerable<Fleet> FleetList(int id);
        IEnumerable<Vehicle> VehicleList();
        IEnumerable<Vehicle> VehicleList(int id);
        IEnumerable<Vehicle> VehicleList(string vehicleNumber,int fleetId);
        IEnumerable<Driver> DriverList(string driverNumber, int fleetId);
        IEnumerable<Merchant> MerchantList();
        IEnumerable<Driver> DriverNameList(int fleetId);
        IEnumerable<User> MerchantUserList(int mId);
    }
}
