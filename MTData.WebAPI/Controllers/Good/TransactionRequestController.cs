﻿using System;
using System.Configuration;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Linq;
using System.Threading;
using System.Web.Http;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.Web.Configuration;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.WebAPI.Models;
using MTData.WebAPI.Resources;
using MTData.Utility;
using MTData.WebAPI.schema;
using MTData.WebAPI.Common;
using MTData.WebAPI.CanadaCert;
using MTData.WebAPI.CanadaCommon;

namespace MTData.WebAPI.Controllers
{
    public class TransactionRequestController : BaseController
    {
        #region Load Constructor
        private static int _count = 0;
        private const string Ecommerce = "Ecommerce";
        private const string Retail = "Retail";
        private const string Credit = "Credit";
        private const string Debit = "Debit";
        private const string TokenOnly = "TokenOnly";
        private const string TAKey = "TAKey";
        private const string Void = "Void";
        private const string Timeout = "Timeout";
        private const string Completion = "Completion";
        private const string Refund = "Refund";
        private const string Authorization = "Authorization";
        private const string Visa = "Visa";
        private const string Amex = "Amex";
        private const string Diners = "Diners";
        private const string MasterCard = "MasterCard";
        private const string Discover = "Discover";
        private const string Sale = "Sale";
        private const string EMV2KEY = "EMV2KEY";
        private const string FileCRCValue = "3E30";
        private const string FileSizeValue = "15593";
        private const string ReqFBMaxSizeValue = "999";
        private const string CAPKUpdate = "CAPKUpdate";
        private const string CAPKDownload = "CAPKDownload";
        private const string BookerAppPayment = "BookerAppPayment";
        protected string IndustryType;
        protected string PaymentType;
        private int retryCount;
        protected string TransactionType;
        private static ITerminalService _terminalService;
        private StringBuilder _logMessage;
        readonly ILogger _logger = new Logger();

        public TransactionRequestController([Named("TerminalService")] ITerminalService terminalService, StringBuilder logMessage) : base(terminalService, logMessage)
        {
            _terminalService = terminalService;
            _logMessage = logMessage;
        }

        private readonly GMFMessageVariants _gmfMsgVar = new GMFMessageVariants();
        private readonly CreditRequestDetails _creditReq = new CreditRequestDetails();                      // Credit request for Ecomm/retail transaction
        private readonly PinlessDebitRequestDetails _pinlessDebit = new PinlessDebitRequestDetails();       // Debit request for Ecomm transaction
        private readonly DebitRequestDetails _debitReq = new DebitRequestDetails();                         // Debit request for Retail transaction
        private readonly AdminRequestDetails _adminReq = new AdminRequestDetails();
        private readonly VoidTOReversalRequestDetails _revDetails = new VoidTOReversalRequestDetails();     // Void TO Reversal Request
        private readonly TARequestDetails _taRequest = new TARequestDetails();
        private readonly MessageProcessor _objMessageProcessor = new MessageProcessor();
        public static readonly string StrAppPath = Assembly.GetCallingAssembly().Location;

        #endregion

        /// <summary>
        /// Purpose             :   Finding credit and debit card request of Retail and ecommerce domain for First Data. Transaction Implemented Authorization, Completion, Sale, Refund, Void.
        /// Function Name       :   CreateTransactionRequest
        /// Created By          :   Salil Gupta
        /// Created On          :   03/10/2015
        /// Modificatios Made   :   Madhuri Tanwar
        /// Modidified On       :   07/27/2015
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        public IHttpActionResult CreateTransactionRequest(TransactionRequest transactionRequest)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                string industryType = transactionRequest.IndustryType;
                string paymentType = transactionRequest.PaymentType.ToString();
                string transactionType = (transactionRequest.TransactionType == Void || transactionRequest.TransactionType == Timeout) ? Void : transactionRequest.TransType.ToString();

                switch (industryType)
                {
                    #region Ecommerce
                    case Ecommerce:
                        if (paymentType == Credit)
                        {
                            switch (transactionType)
                            {
                                case Void:
                                    VoidTOReversalRequestDetails creditRequestVoid = ReversalRequest(transactionRequest);
                                    return Ok(creditRequestVoid);
                                default:
                                    CreditRequestDetails creditRequest = CreateEcommerceRequest(transactionRequest);
                                    return Ok(creditRequest);
                            }
                        }

                        if (paymentType == Debit)
                        {
                            PinlessDebitRequestDetails debitSale = CreatePinlessDebitRequest(transactionRequest);
                            return Ok(debitSale);
                        }

                        break;
                    #endregion

                    #region Retail
                    case Retail:
                        var entryMode = (PaymentEntryMode)Enum.Parse(typeof(PaymentEntryMode), transactionRequest.EntryMode, true);
                        var txnType = (PaymentTransactionType)Enum.Parse(typeof(PaymentTransactionType), transactionType, true);
                        switch (entryMode)
                        {
                            #region Swiped
                            case PaymentEntryMode.Swiped:
                                if (paymentType == PaymentCardType.Credit.ToString())
                                {
                                    switch (txnType)
                                    {
                                        case PaymentTransactionType.Void:
                                            VoidTOReversalRequestDetails creditRequestVerification = ReversalRequestMsr(transactionRequest);
                                            return Ok(creditRequestVerification);
                                        default:
                                            CreditRequestDetails creditRequest = CreateRetailRequest(transactionRequest);
                                            return Ok(creditRequest);
                                    }
                                }
                                else if (paymentType == PaymentCardType.Debit.ToString())
                                {
                                    DebitRequestDetails debitCard = CreatePinDebitRequest(transactionRequest);
                                    return Ok(debitCard);
                                }
                                break;
                            #endregion

                            #region FSwiped
                            case PaymentEntryMode.FSwiped:
                                if (paymentType == PaymentCardType.Credit.ToString())
                                {
                                    switch (txnType)
                                    {
                                        case PaymentTransactionType.Void:
                                            VoidTOReversalRequestDetails creditRequestVerification = ReversalRequestEmv(transactionRequest);
                                            return Ok(creditRequestVerification);
                                        default: //Full EMV Transaction
                                            CreditRequestDetails creditRequest = CreateFallbackMsrRequest(transactionRequest);
                                            return Ok(creditRequest);
                                    }
                                }
                                else if (paymentType == PaymentCardType.Debit.ToString())
                                {
                                    DebitRequestDetails debitCard = CreateFallbackMsrDebitRequest(transactionRequest);
                                    return Ok(debitCard);
                                }

                                break;
                            #endregion

                            #region EmvContact
                            case PaymentEntryMode.EmvContact:

                                if (paymentType == PaymentCardType.Credit.ToString())
                                {
                                    switch (txnType)
                                    {
                                        case PaymentTransactionType.Void:
                                            VoidTOReversalRequestDetails creditRequestVerification = ReversalRequestEmv(transactionRequest);
                                            return Ok(creditRequestVerification);
                                        default: //Full EMV Transaction
                                            CreditRequestDetails creditRequest = CreateEmvContactRequest(transactionRequest);
                                            return Ok(creditRequest);
                                    }
                                }
                                else if (paymentType == PaymentCardType.Debit.ToString())
                                {
                                    DebitRequestDetails debitCard = CreateEmvContactPinDebitRequest(transactionRequest);
                                    return Ok(debitCard);
                                }

                                break;
                            #endregion

                            #region EmvContactlsess
                            case PaymentEntryMode.EmvContactless:

                                if (paymentType == PaymentCardType.Credit.ToString())
                                {
                                    switch (txnType)
                                    {
                                        case PaymentTransactionType.Void:
                                            VoidTOReversalRequestDetails creditRequestVerification = ReversalRequestEmv(transactionRequest);
                                            return Ok(creditRequestVerification);
                                        default: //Full EMV Transaction
                                            CreditRequestDetails creditRequest = CreateEmvContactlessRequest(transactionRequest);
                                            return Ok(creditRequest);
                                    }
                                }
                                else if (paymentType == PaymentCardType.Debit.ToString())
                                {
                                    DebitRequestDetails debitCard = CreateEmvContactlessPinDebitRequest(transactionRequest);
                                    return Ok(debitCard);
                                }

                                break;
                            #endregion
                        }
                        break;
                    #endregion

                    #region Token
                    case TokenOnly:
                        TARequestDetails creditTaRequest = TaRequest(transactionRequest);
                        return Ok(creditTaRequest);

                    case TAKey:
                        TARequestDetails creditTaKeyRequest = TaKeyRequest(transactionRequest);
                        return Ok(creditTaKeyRequest);
                    #endregion

                    #region CAPK
                    case CAPKUpdate:
                        AdminRequestDetails updateAdminRequestDetails = CreateEmvContactUpdateCapkRequest(transactionRequest);
                        return Ok(updateAdminRequestDetails);

                    case CAPKDownload:
                        AdminRequestDetails downloadAdminRequestDetails = CreateEmvContactDownloadCapkRequest(transactionRequest);
                        return Ok(downloadAdminRequestDetails);
                    #endregion

                    #region bookerApp
                    case BookerAppPayment:
                        CreditRequestDetails bookerAppCreditRequest = BookerAppRequest(transactionRequest);
                        return Ok(bookerAppCreditRequest);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return Ok();
        }

        /// <summary>
        /// Purpose             :   Creating credit card request
        /// Function Name       :   CreateRequest
        /// Created By          :   Salil Gupta
        /// Created On          :   03/16/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/017/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        private CreditRequestDetails CreateEcommerceRequest(TransactionRequest transactionRequest)
        {

            try
            {
                if (!transactionRequest.IsNull())
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                    MTDTransactionDto mtdTransaction = new MTDTransactionDto();
                    if (!string.IsNullOrEmpty(transactionRequest.RapidConnectAuthId))
                    {
                        mtdTransaction = _terminalService.Completion(transactionRequest.RapidConnectAuthId, transactionRequest.SerialNumber, transactionRequest.MtdMerchantId);
                        if (mtdTransaction.IsNull())
                            return null;
                    }

                    #region Common Group

                    CommonGrp cmnGrp = new CommonGrp();

                    /* The payment type of the transaction. */
                    PymtTypeType typePayment = transactionRequest.PaymentType;
                    cmnGrp.PymtType = typePayment;
                    cmnGrp.PymtTypeSpecified = true;
                    /* merchant category code. */
                    cmnGrp.MerchCatCode = transactionRequest.MCCode;
                    /* The type of transaction being performed. */
                    cmnGrp.TxnType = transactionRequest.TransType;
                    cmnGrp.TxnTypeSpecified = true;
                    /* The local date and time in which the transaction was performed. */
                    cmnGrp.LocalDateTime = transactionRequest.LocalDateTime;//DateTime.Now.ToString("yyyyMMddHHmmss");
                    /* The transmission date and time of the transaction (in GMT/UCT). */
                    cmnGrp.TrnmsnDateTime = transactionRequest.TrnmsnDateTime;//DateTime.UtcNow.ToString("yyyyMMddHHmmss");
                    /* A number assigned by the merchant to uniquely reference the transaction. 
                     * This number must be unique within a day per Merchant ID per Terminal ID. */
                    cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    if (string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                    {
                        cmnGrp.RefNum = mtdTransaction.TransRefNo;
                        cmnGrp.OrderNum = mtdTransaction.TransOrderNo;
                    }
                    else
                    {
                        cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);
                        cmnGrp.OrderNum = cmnGrp.RefNum;
                    }
                    /* An ID assigned by First Data, for the Third Party Processor or 
                     * Software Vendor that generated the transaction. */
                    cmnGrp.TPPID = transactionRequest.TppId;

                    /* A unique ID assigned to a terminal. */
                    cmnGrp.TermID = transactionRequest.TerminalId;
                    /* A unique ID assigned by First Data, to identify the Merchant. */
                    cmnGrp.MerchID = transactionRequest.FdMerchantId;

                    cmnGrp.GroupID = transactionRequest.GroupId;
                    //Group ID value will be assigned by First Data. 
                    //get terminal id from transaction request
                    cmnGrp.POSEntryMode = PaymentAPIResources.EcomPosEntryMode;
                    cmnGrp.POSCondCode = POSCondCodeType.Item59;
                    cmnGrp.POSCondCodeSpecified = true;
                    cmnGrp.TermCatCode = TermCatCodeType.Item00;
                    cmnGrp.TermCatCodeSpecified = true;
                    cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item10;
                    cmnGrp.TermEntryCapabltSpecified = true;
                    cmnGrp.TermLocInd = TermLocIndType.Item1;
                    cmnGrp.TermLocIndSpecified = true;
                    cmnGrp.CardCaptCap = CardCaptCapType.Item0;
                    cmnGrp.CardCaptCapSpecified = true;

                    EcommGrp ecomgrp = new EcommGrp();
                    ecomgrp.EcommTxnInd = EcommTxnIndType.Item03;
                    ecomgrp.EcommTxnIndSpecified = true;
                    if (!string.Equals(cmnGrp.TxnType.ToString(), Authorization, StringComparison.CurrentCultureIgnoreCase))
                        ecomgrp.EcommURL = PaymentAPIResources.EcomUrl;
                    _creditReq.EcommGrp = ecomgrp;

                    /* The amount of the transaction. This may be an authorization amount, 
                     * adjustment amount or a reversal    based on the type of transaction. 
                     * It is inclusive of all additional amounts. 
                     * It is submitted in the currency represented by the Transaction Currency field.  
                     * The field is overwritten in the response for a partial authorization. */
                    double num = Convert.ToDouble(transactionRequest.TransactionAmount);
                    string amt = num.ToString("0.00").Replace(".", string.Empty);
                    cmnGrp.TxnAmt = Convert.ToString(amt);
                    cmnGrp.TxnAmt = cmnGrp.TxnAmt.PadLeft(12, '0');

                    cmnGrp.TxnCrncy = PaymentAPIResources.Cur_USA;
                    /* An indicator that describes the location of the terminal. */
                    /* Indicates Group ID. */
                    _creditReq.CommonGrp = cmnGrp;
                    #endregion

                    CardGrp cardGrp = new CardGrp();


                    #region Transarmor group
                    if (string.Equals(cmnGrp.TxnType.ToString(), Authorization, StringComparison.CurrentCultureIgnoreCase) || string.Equals(cmnGrp.TxnType.ToString(), Sale, StringComparison.CurrentCultureIgnoreCase))
                    {

                        cardGrp.AcctNum = transactionRequest.CardNumber;
                        cardGrp.CardExpiryDate = transactionRequest.ExpiryDate;
                        cardGrp.CardType = transactionRequest.CardType;
                        cardGrp.CardTypeSpecified = true;
                        cardGrp.CCVInd = CCVIndType.Prvded;
                        cardGrp.CCVIndSpecified = true;
                        cardGrp.CCVData = transactionRequest.CardCvv;
                        _creditReq.CardGrp = cardGrp;

                        TAGrp taGrp = new TAGrp();
                        taGrp.SctyLvl = SctyLvlType.Tknizatn;
                        taGrp.SctyLvlSpecified = true;
                        taGrp.TknType = transactionRequest.TokenType;
                        _creditReq.TAGrp = taGrp;
                    }

                    if (string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (mtdTransaction != null)
                        {
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(mtdTransaction.ResponseXml);
                            string xmlString = CommonFunctions.GetXmlAsString(xmlDoc);
                            var response = CommonFunctions.GetMCResponse(xmlString);
                            xmlDoc.LoadXml(response);
                            var aVSResultCode = xmlDoc.SelectSingleNode("CreditResponse/CardGrp/AVSResultCode");
                            var cCVResultCode = xmlDoc.SelectSingleNode("CreditResponse/CardGrp/CCVResultCode");
                            cardGrp.CardType = transactionRequest.CardType;
                            cardGrp.CardTypeSpecified = true;
                            if (aVSResultCode != null)
                            {
                                cardGrp.AVSResultCode = (AVSResultCodeType)Enum.Parse(typeof(AVSResultCodeType), aVSResultCode.InnerText);
                                cardGrp.AVSResultCodeSpecified = true;
                            }
                            if (cCVResultCode != null)
                            {
                                cardGrp.CCVResultCode = (CCVResultCodeType)Enum.Parse(typeof(CCVResultCodeType), cCVResultCode.InnerText);
                                cardGrp.CCVResultCodeSpecified = true;
                            }
                        }

                        _creditReq.CardGrp = cardGrp;

                        TAGrp taGrp = new TAGrp();
                        taGrp.SctyLvl = SctyLvlType.Tknizatn;
                        taGrp.SctyLvlSpecified = true;
                        taGrp.Tkn = mtdTransaction.CardToken.Decrypt();
                        taGrp.TknType = transactionRequest.TokenType;
                        _creditReq.TAGrp = taGrp;
                    }
                    if (string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.CurrentCultureIgnoreCase))
                    {
                        cardGrp.CardType = transactionRequest.CardType;
                        cardGrp.CardTypeSpecified = true;
                        _creditReq.CardGrp = cardGrp;

                        TAGrp taGrp = new TAGrp();
                        taGrp.SctyLvl = SctyLvlType.Tknizatn;
                        taGrp.SctyLvlSpecified = true;
                        taGrp.Tkn = mtdTransaction.CardToken.Decrypt();
                        taGrp.TknType = transactionRequest.TokenType;
                        _creditReq.TAGrp = taGrp;
                    }
                    #endregion

                    if (!string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase) && !string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.OrdinalIgnoreCase))
                    {
                        #region Additional Amount Group
                        AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();
                        addAmtGrp.PartAuthrztnApprvlCapablt = PartAuthrztnApprvlCapabltType.Item1;
                        addAmtGrp.PartAuthrztnApprvlCapabltSpecified = true;
                        AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[1];
                        addAmtGrpArr[0] = addAmtGrp;
                        _creditReq.AddtlAmtGrp = addAmtGrpArr;
                        #endregion
                    }

                    #region Visa Group
                    if (string.Equals(cardGrp.CardType.ToString(), Visa, StringComparison.OrdinalIgnoreCase) && !string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.OrdinalIgnoreCase))
                    {
                        VisaGrp visaGrp = new VisaGrp();
                        visaGrp.VisaBID = PaymentAPIResources.VisaBID;
                        visaGrp.VisaAUAR = PaymentAPIResources.VisaAUAR;
                        if (!string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                        {
                            visaGrp.ACI = ACIType.Y;
                            visaGrp.ACISpecified = true;
                            visaGrp.TaxAmtCapablt = TaxAmtCapabltType.Item1;
                            visaGrp.TaxAmtCapabltSpecified = true;
                        }
                        else
                        {
                            visaGrp.ACI = ACIType.T;
                            visaGrp.ACISpecified = true;
                        }
                        _creditReq.Item = visaGrp;
                    }
                    if (((string.Equals(cardGrp.CardType.ToString(), MasterCard, StringComparison.OrdinalIgnoreCase)) && (string.Equals(cmnGrp.TxnType.ToString(), "completion", StringComparison.OrdinalIgnoreCase))))
                    {
                        MCGrp mcGrp = new MCGrp();
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(mtdTransaction.ResponseXml);
                        string xmlString = CommonFunctions.GetXmlAsString(xmlDoc);
                        var response = CommonFunctions.GetMCResponse(xmlString);
                        xmlDoc.LoadXml(response);
                        var banknetData = xmlDoc.SelectSingleNode("CreditResponse/MCGrp/BanknetData");
                        if (banknetData != null) { mcGrp.BanknetData = banknetData.InnerText; }
                        _creditReq.Item = mcGrp;
                    }
                    #endregion

                    #region AmexGrp Group
                    if (string.Equals(cardGrp.CardType.ToString(), Amex, StringComparison.OrdinalIgnoreCase) && string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                    {
                        AmexGrp amexgrp = new AmexGrp();
                        if (mtdTransaction != null)
                        {
                            amexgrp.AmExTranID = mtdTransaction.GatewayTxnId;
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(mtdTransaction.ResponseXml);
                            string xmlString = CommonFunctions.GetXmlAsString(xmlDoc);
                            var response = CommonFunctions.GetMCResponse(xmlString);
                            xmlDoc.LoadXml(response);
                            var amExPOSData = xmlDoc.SelectSingleNode("CreditResponse/AmexGrp/AmExPOSData");
                            if (amExPOSData != null)
                            { amexgrp.AmExPOSData = amExPOSData.InnerText; }
                        }
                        _creditReq.Item = amexgrp;

                    }
                    #endregion

                    #region Discover Group
                    if ((string.Equals(cardGrp.CardType.ToString(), Diners, StringComparison.OrdinalIgnoreCase) || string.Equals(cardGrp.CardType.ToString(), Discover, StringComparison.OrdinalIgnoreCase) || string.Equals(cardGrp.CardType.ToString(), "JCB", StringComparison.OrdinalIgnoreCase)) && string.Equals(cmnGrp.TxnType.ToString(), "completion", StringComparison.OrdinalIgnoreCase))
                    {
                        DSGrp dsGrp = new DSGrp();
                        if (mtdTransaction != null)
                        {
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(mtdTransaction.ResponseXml);
                            string xmlString = CommonFunctions.GetXmlAsString(xmlDoc);
                            var response = CommonFunctions.GetMCResponse(xmlString);
                            xmlDoc.LoadXml(response);
                            var discProcCode = xmlDoc.SelectSingleNode("CreditResponse/DSGrp/DiscProcCode");
                            if (discProcCode != null)
                            {
                                dsGrp.DiscProcCode = discProcCode.InnerText;
                            }

                            var discPOSEntry = xmlDoc.SelectSingleNode("CreditResponse/DSGrp/DiscPOSEntry");
                            if (discPOSEntry != null)
                            {
                                dsGrp.DiscPOSEntry = discPOSEntry.InnerText;
                            }

                            var discRespCode = xmlDoc.SelectSingleNode("CreditResponse/DSGrp/DiscRespCode");
                            if (discRespCode != null)
                            {
                                dsGrp.DiscRespCode = discRespCode.InnerText;
                            }

                            var discPOSData = xmlDoc.SelectSingleNode("CreditResponse/DSGrp/DiscPOSData");
                            if (discPOSData != null)
                            {
                                dsGrp.DiscPOSData = discPOSData.InnerText;
                            }

                            var discTransQualifier = xmlDoc.SelectSingleNode("CreditResponse/DSGrp/DiscTransQualifier");
                            if (discTransQualifier != null)
                            {
                                dsGrp.DiscTransQualifier = discTransQualifier.InnerText;
                            }

                            var discNRID = xmlDoc.SelectSingleNode("CreditResponse/DSGrp/DiscNRID");
                            if (discNRID != null)
                            {
                                dsGrp.DiscNRID = discNRID.InnerText;
                            }

                            dsGrp.DiscNRID = mtdTransaction.GatewayTxnId;
                        }

                        _creditReq.Item = dsGrp;
                    }
                    #endregion

                    #region Customer info Group
                    if (!string.IsNullOrEmpty(transactionRequest.StreetAddress) || !string.IsNullOrEmpty(transactionRequest.ZipCode))
                    {
                        if (!string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.OrdinalIgnoreCase) && !string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                        {
                            CustInfoGrp custinfo = new CustInfoGrp();
                            if (!string.IsNullOrEmpty(transactionRequest.StreetAddress))
                            {
                                custinfo.AVSBillingAddr = transactionRequest.StreetAddress;
                            }
                            if (!string.IsNullOrEmpty(transactionRequest.ZipCode))
                            {
                                custinfo.AVSBillingPostalCode = transactionRequest.ZipCode;
                            }
                            _creditReq.CustInfoGrp = custinfo;
                        }
                    }
                    #endregion

                    #region addAm Group

                    if (string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                    {
                        AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();

                        if (mtdTransaction != null)
                        {
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(mtdTransaction.ResponseXml);
                            string xmlString = CommonFunctions.GetXmlAsString(xmlDoc);
                            var response = CommonFunctions.GetMCResponse(xmlString);
                            xmlDoc.LoadXml(response);
                            var originalamt = xmlDoc.SelectSingleNode("CreditResponse/CommonGrp/TxnAmt");
                            if (originalamt != null)
                            {
                                var amountP = originalamt.InnerText;
                                addAmtGrp.AddAmt = amountP.PadLeft(12, '0');
                            }

                            addAmtGrp.AddAmtCrncy = PaymentAPIResources.Cur_USA;
                            addAmtGrp.AddAmtType = AddAmtTypeType.FirstAuthAmt;
                            addAmtGrp.AddAmtTypeSpecified = true;

                            AddtlAmtGrp addAmtGrp1 = new AddtlAmtGrp();

                            var originalamt1 = xmlDoc.SelectSingleNode("CreditResponse/CommonGrp/TxnAmt");
                            if (originalamt1 != null)
                            {
                                var amtP = originalamt1.InnerText;
                                addAmtGrp1.AddAmt = amtP.PadLeft(12, '0');
                            }

                            addAmtGrp1.AddAmtCrncy = PaymentAPIResources.Cur_USA;
                            addAmtGrp1.AddAmtType = AddAmtTypeType.TotalAuthAmt;
                            addAmtGrp1.AddAmtTypeSpecified = true;

                            AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[2];
                            addAmtGrpArr[0] = addAmtGrp;
                            addAmtGrpArr[1] = addAmtGrp1;
                            _creditReq.AddtlAmtGrp = addAmtGrpArr;
                        }

                        OrigAuthGrp orgAuthgrp = new OrigAuthGrp();
                        if (mtdTransaction != null)
                        {
                            orgAuthgrp.OrigAuthID = mtdTransaction.AuthId;

                            orgAuthgrp.OrigLocalDateTime = mtdTransaction.LocalDateTime;
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(mtdTransaction.RequestXml);
                            var trnmsnDateTime = xmlDoc.SelectSingleNode("TransactionRequest/TrnmsnDateTime");
                            if (trnmsnDateTime != null)
                            {
                                orgAuthgrp.OrigTranDateTime = trnmsnDateTime.InnerText;
                            }

                            orgAuthgrp.OrigSTAN = Convert.ToString(mtdTransaction.Stan);
                            orgAuthgrp.OrigRespCode = Convert.ToString(mtdTransaction.ResponseCode);
                        }
                        _creditReq.OrigAuthGrp = orgAuthgrp;

                    }
                    #endregion

                    _gmfMsgVar.Item = _creditReq;
                    transactionRequest.Stan = cmnGrp.STAN;
                    transactionRequest.TransRefNo = cmnGrp.RefNum;
                    transactionRequest.TransOrderNo = cmnGrp.OrderNum;
                    return _creditReq;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :   Creating credit card request
        /// Function Name       :   CreateRequest
        /// Created By          :   Salil Gupta
        /// Created On          :   03/16/2015
        /// Modificatios Made   :   Madhuri Tanwar
        /// Modidified On       :   06/012/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        private CreditRequestDetails CreateRetailRequest(TransactionRequest transactionRequest)
        {

            try
            {
                if (!transactionRequest.IsNull())
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                    MTDTransactionDto mtdTransaction = new MTDTransactionDto();
                    if (!string.IsNullOrEmpty(transactionRequest.RapidConnectAuthId))
                    {
                        mtdTransaction = _terminalService.Completion(
                           transactionRequest.RapidConnectAuthId,
                      transactionRequest.SerialNumber, transactionRequest.MtdMerchantId);
                        if (mtdTransaction.IsNull())
                            return null;
                    }

                    #region Common Group

                    CommonGrp cmnGrp = new CommonGrp();

                    /* The payment type of the transaction. */
                    PymtTypeType typePayment = transactionRequest.PaymentType;
                    cmnGrp.PymtType = typePayment;
                    cmnGrp.PymtTypeSpecified = true;
                    /* merchant category code. */
                    cmnGrp.MerchCatCode = transactionRequest.MCCode;
                    /* The type of transaction being performed. */
                    cmnGrp.TxnType = transactionRequest.TransType;
                    cmnGrp.TxnTypeSpecified = true;
                    /* The local date and time in which the transaction was performed. */
                    cmnGrp.LocalDateTime = transactionRequest.LocalDateTime;//DateTime.Now.ToString("yyyyMMddHHmmss");
                    /* The transmission date and time of the transaction (in GMT/UCT). */
                    cmnGrp.TrnmsnDateTime = transactionRequest.TrnmsnDateTime;//DateTime.UtcNow.ToString("yyyyMMddHHmmss");
                    /* A number assigned by the merchant to uniquely reference the transaction. 
                     * This number must be unique within a day per Merchant ID per Terminal ID. */
                    cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);
                    /* A number assigned by the merchant to uniquely reference a set of transactions. */
                    //cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MerchantID, transactionRequest.TerminalID);//"480061115979";
                    if (string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                    {
                        cmnGrp.RefNum = mtdTransaction.TransRefNo;
                        cmnGrp.OrderNum = mtdTransaction.TransOrderNo;
                    }
                    else
                    {
                        cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);
                        cmnGrp.OrderNum = cmnGrp.RefNum;
                    }
                    /* An ID assigned by First Data, for the Third Party Processor or 
                     * Software Vendor that generated the transaction. */
                    cmnGrp.TPPID = transactionRequest.TppId;

                    /* A unique ID assigned to a terminal. */
                    cmnGrp.TermID = transactionRequest.TerminalId;

                    //get terminal id from transaction request

                    /* A unique ID assigned by First Data, to identify the Merchant. */
                    cmnGrp.MerchID = transactionRequest.FdMerchantId;

                    cmnGrp.GroupID = transactionRequest.GroupId;
                    //Group ID value will be assigned by First Data. 
                    //get terminal id from transaction request
                    cmnGrp.POSEntryMode = PaymentAPIResources.RetailPosEntryMode;
                    cmnGrp.POSCondCode = POSCondCodeType.Item00;
                    cmnGrp.POSCondCodeSpecified = true;
                    cmnGrp.TermCatCode = TermCatCodeType.Item01;
                    cmnGrp.TermCatCodeSpecified = true;
                    cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item04;
                    cmnGrp.TermEntryCapabltSpecified = true;
                    cmnGrp.TermLocInd = TermLocIndType.Item0;
                    cmnGrp.TermLocIndSpecified = true;
                    cmnGrp.CardCaptCap = CardCaptCapType.Item1;
                    cmnGrp.CardCaptCapSpecified = true;

                    /* The amount of the transaction. This may be an authorization amount, 
                     * adjustment amount or a reversal amount based on the type of transaction. 
                     * It is inclusive of all additional amounts. 
                     * It is submitted in the currency represented by the Transaction Currency field.  
                     * The field is overwritten in the response for a partial authorization. */

                    double num = Convert.ToDouble(transactionRequest.TransactionAmount);
                    string amt = num.ToString("0.00").Replace(".", string.Empty);
                    cmnGrp.TxnAmt = Convert.ToString(amt);
                    cmnGrp.TxnAmt = cmnGrp.TxnAmt.PadLeft(12, '0');

                    /* The numeric currency of the Transaction Amount. */
                    cmnGrp.TxnCrncy = PaymentAPIResources.Cur_USA;

                    _creditReq.CommonGrp = cmnGrp;
                    #endregion

                    #region Transarmor group
                    CardGrp cardGrp = new CardGrp();
                    if (string.Equals(cmnGrp.TxnType.ToString(), Authorization, StringComparison.CurrentCultureIgnoreCase) || string.Equals(cmnGrp.TxnType.ToString(), Sale, StringComparison.CurrentCultureIgnoreCase))
                    {

                        cardGrp.CardType = transactionRequest.CardType;
                        cardGrp.CardTypeSpecified = true;
                        _creditReq.CardGrp = cardGrp;

                        TAGrp taGrp = new TAGrp();
                        taGrp.SctyLvl = SctyLvlType.EncrptTknizatn;
                        taGrp.SctyLvlSpecified = true;
                        taGrp.EncrptType = EncrptTypeType.RSA;
                        taGrp.EncrptTypeSpecified = true;
                        taGrp.EncrptTrgt = EncrptTrgtType.Track2;
                        taGrp.EncrptTrgtSpecified = true;
                        taGrp.KeyID = transactionRequest.KeyId;
                        taGrp.EncrptBlock = transactionRequest.Track3Data;
                        taGrp.TknType = transactionRequest.TokenType;
                        _creditReq.TAGrp = taGrp;

                    }
                    if (string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.CurrentCultureIgnoreCase) || string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (mtdTransaction != null) cardGrp.CardType = (CardTypeType)Enum.Parse(typeof(CardTypeType), mtdTransaction.CardType);
                        transactionRequest.CardType = cardGrp.CardType;
                        cardGrp.CardTypeSpecified = true;
                        _creditReq.CardGrp = cardGrp;

                        TAGrp taGrp = new TAGrp();
                        taGrp.SctyLvl = SctyLvlType.EncrptTknizatn;
                        taGrp.SctyLvlSpecified = true;
                        taGrp.Tkn = mtdTransaction.CardToken.Decrypt();
                        taGrp.TknType = transactionRequest.TokenType;
                        _creditReq.TAGrp = taGrp;
                    }
                    #endregion
                    if (!string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase) && !string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.OrdinalIgnoreCase))
                    {
                        #region Additional Amount Group
                        AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();
                        addAmtGrp.PartAuthrztnApprvlCapablt = PartAuthrztnApprvlCapabltType.Item1;
                        addAmtGrp.PartAuthrztnApprvlCapabltSpecified = true;
                        AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[1];
                        addAmtGrpArr[0] = addAmtGrp;
                        _creditReq.AddtlAmtGrp = addAmtGrpArr;
                        #endregion
                    }

                    #region Visa Group
                    if (string.Equals(cardGrp.CardType.ToString(), Visa, StringComparison.OrdinalIgnoreCase) && !string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.OrdinalIgnoreCase))
                    {
                        VisaGrp visaGrp = new VisaGrp();
                        visaGrp.VisaBID = PaymentAPIResources.VisaBID;
                        visaGrp.VisaAUAR = PaymentAPIResources.VisaAUAR;
                        if (!string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                        {
                            visaGrp.ACI = ACIType.Y;
                            visaGrp.ACISpecified = true;
                            visaGrp.TaxAmtCapablt = TaxAmtCapabltType.Item1;
                            visaGrp.TaxAmtCapabltSpecified = true;
                        }
                        else

                            if (mtdTransaction != null)
                            {
                                XmlDocument xmlDoc = new XmlDocument();
                                xmlDoc.LoadXml(mtdTransaction.ResponseXml);
                                string xmlString = CommonFunctions.GetXmlAsString(xmlDoc);
                                var response = CommonFunctions.GetMCResponse(xmlString);
                                xmlDoc.LoadXml(response);
                                var aCI = xmlDoc.SelectSingleNode("CreditResponse/VisaGrp/ACI");
                                if (aCI != null)
                                {
                                    visaGrp.ACI = (ACIType)Enum.Parse(typeof(ACIType), aCI.InnerText);
                                    visaGrp.ACISpecified = true;

                                }

                                var cardLevelResult = xmlDoc.SelectSingleNode("CreditResponse/VisaGrp/CardLevelResult");
                                if (cardLevelResult != null)
                                {
                                    visaGrp.CardLevelResult = cardLevelResult.InnerText;

                                }


                                var transID = xmlDoc.SelectSingleNode("CreditResponse/VisaGrp/TransID");
                                if (transID != null)
                                {
                                    visaGrp.TransID = transID.InnerText;

                                }

                            }
                        _creditReq.Item = visaGrp;
                    }


                    if (((string.Equals(cardGrp.CardType.ToString(), MasterCard, StringComparison.OrdinalIgnoreCase)) && (string.Equals(cmnGrp.TxnType.ToString(), "completion", StringComparison.OrdinalIgnoreCase))))
                    {
                        MCGrp mcGrp = new MCGrp();
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(mtdTransaction.ResponseXml);
                        string xmlString = CommonFunctions.GetXmlAsString(xmlDoc);
                        var response = CommonFunctions.GetMCResponse(xmlString);
                        xmlDoc.LoadXml(response);
                        var banknetData = xmlDoc.SelectSingleNode("CreditResponse/MCGrp/BanknetData");

                        if (banknetData != null)
                        {
                            mcGrp.BanknetData = banknetData.InnerText;

                        }

                        _creditReq.Item = mcGrp;
                    }
                    #endregion

                    #region AmexGrp Group
                    if (string.Equals(cardGrp.CardType.ToString(), Amex, StringComparison.OrdinalIgnoreCase) && string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                    {
                        AmexGrp amexgrp = new AmexGrp();
                        if (mtdTransaction != null)
                        {
                            amexgrp.AmExTranID = mtdTransaction.GatewayTxnId;
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(mtdTransaction.ResponseXml);
                            string xmlString = CommonFunctions.GetXmlAsString(xmlDoc);
                            var response = CommonFunctions.GetMCResponse(xmlString);
                            xmlDoc.LoadXml(response);
                            var amExPOSData = xmlDoc.SelectSingleNode("CreditResponse/AmexGrp/AmExPOSData");

                            if (amExPOSData != null)
                            {
                                amexgrp.AmExPOSData = amExPOSData.InnerText;

                            }
                        }
                        _creditReq.Item = amexgrp;
                    }
                    #endregion

                    #region Discover Group
                    if ((string.Equals(cardGrp.CardType.ToString(), Diners, StringComparison.OrdinalIgnoreCase) || string.Equals(cardGrp.CardType.ToString(), Discover, StringComparison.OrdinalIgnoreCase) || string.Equals(cardGrp.CardType.ToString(), "JCB", StringComparison.OrdinalIgnoreCase)) && string.Equals(cmnGrp.TxnType.ToString(), "completion", StringComparison.OrdinalIgnoreCase))
                    {
                        DSGrp dsGrp = new DSGrp();
                        if (mtdTransaction != null)
                        {
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(mtdTransaction.ResponseXml);
                            string xmlString = CommonFunctions.GetXmlAsString(xmlDoc);
                            var response = CommonFunctions.GetMCResponse(xmlString);
                            xmlDoc.LoadXml(response);
                            var discProcCode = xmlDoc.SelectSingleNode("CreditResponse/DSGrp/DiscProcCode");
                            if (discProcCode != null)
                            {
                                dsGrp.DiscProcCode = discProcCode.InnerText;

                            }

                            var discPOSEntry = xmlDoc.SelectSingleNode("CreditResponse/DSGrp/DiscPOSEntry");
                            if (discPOSEntry != null)
                            {
                                dsGrp.DiscPOSEntry = discPOSEntry.InnerText;

                            }

                            var discRespCode = xmlDoc.SelectSingleNode("CreditResponse/DSGrp/DiscRespCode");
                            if (discRespCode != null)
                            {
                                dsGrp.DiscRespCode = discRespCode.InnerText;

                            }

                            var discPOSData = xmlDoc.SelectSingleNode("CreditResponse/DSGrp/DiscPOSData");
                            if (discPOSData != null)
                            {
                                dsGrp.DiscPOSData = discPOSData.InnerText;

                            }

                            var discTransQualifier = xmlDoc.SelectSingleNode("CreditResponse/DSGrp/DiscTransQualifier");
                            if (discTransQualifier != null)
                            {
                                dsGrp.DiscTransQualifier = discTransQualifier.InnerText;

                            }

                            var discNRID = xmlDoc.SelectSingleNode("CreditResponse/DSGrp/DiscNRID");
                            if (discNRID != null)
                            {
                                dsGrp.DiscNRID = discNRID.InnerText;

                            }

                            dsGrp.DiscNRID = mtdTransaction.GatewayTxnId;
                        }


                        _creditReq.Item = dsGrp;
                    }
                    #endregion

                    #region Customer info Group
                    if (!string.IsNullOrEmpty(transactionRequest.StreetAddress) || !string.IsNullOrEmpty(transactionRequest.ZipCode))
                    {
                        if (!string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.OrdinalIgnoreCase) && !string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                        {
                            CustInfoGrp custinfo = new CustInfoGrp();
                            if (!string.IsNullOrEmpty(transactionRequest.StreetAddress))
                            {
                                custinfo.AVSBillingAddr = transactionRequest.StreetAddress;
                            }
                            if (!string.IsNullOrEmpty(transactionRequest.ZipCode))
                            {
                                custinfo.AVSBillingPostalCode = transactionRequest.ZipCode;
                            }
                            _creditReq.CustInfoGrp = custinfo;
                        }
                    }
                    #endregion



                    #region addAm Group

                    if (string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                    {
                        AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();

                        if (mtdTransaction != null)
                        {
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(mtdTransaction.ResponseXml);
                            string xmlString = CommonFunctions.GetXmlAsString(xmlDoc);
                            var response = CommonFunctions.GetMCResponse(xmlString);
                            xmlDoc.LoadXml(response);
                            var originalamt = xmlDoc.SelectSingleNode("CreditResponse/CommonGrp/TxnAmt");
                            if (originalamt != null)
                            {
                                var amntP = originalamt.InnerText;
                                addAmtGrp.AddAmt = amntP.PadLeft(12, '0');


                            }

                            addAmtGrp.AddAmtCrncy = PaymentAPIResources.Cur_USA;
                            addAmtGrp.AddAmtType = AddAmtTypeType.FirstAuthAmt;
                            addAmtGrp.AddAmtTypeSpecified = true;

                            AddtlAmtGrp addAmtGrp1 = new AddtlAmtGrp();

                            var originalamt1 = xmlDoc.SelectSingleNode("CreditResponse/CommonGrp/TxnAmt");
                            if (originalamt1 != null)
                            {
                                var amnt = originalamt1.InnerText;
                                addAmtGrp1.AddAmt = amnt.PadLeft(12, '0');


                            }

                            addAmtGrp1.AddAmtCrncy = PaymentAPIResources.Cur_USA;
                            addAmtGrp1.AddAmtType = AddAmtTypeType.TotalAuthAmt;
                            addAmtGrp1.AddAmtTypeSpecified = true;

                            AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[2];
                            addAmtGrpArr[0] = addAmtGrp;
                            addAmtGrpArr[1] = addAmtGrp1;
                            _creditReq.AddtlAmtGrp = addAmtGrpArr;
                        }

                        OrigAuthGrp orgAuthgrp = new OrigAuthGrp();
                        if (mtdTransaction != null)
                        {
                            orgAuthgrp.OrigAuthID = mtdTransaction.AuthId;

                            orgAuthgrp.OrigLocalDateTime = mtdTransaction.LocalDateTime;

                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(mtdTransaction.RequestXml);
                            var trnmsnDateTime = xmlDoc.SelectSingleNode("TransactionRequest/TrnmsnDateTime");
                            if (trnmsnDateTime != null)
                            {
                                orgAuthgrp.OrigTranDateTime = trnmsnDateTime.InnerText;

                            }

                            orgAuthgrp.OrigSTAN = Convert.ToString(mtdTransaction.Stan);
                            orgAuthgrp.OrigRespCode = Convert.ToString(mtdTransaction.ResponseCode);
                        }
                        _creditReq.OrigAuthGrp = orgAuthgrp;

                    }

                    #endregion

                    /* Add the data populated object to GMF message variant object */
                    _gmfMsgVar.Item = _creditReq;
                    transactionRequest.Stan = cmnGrp.STAN;
                    transactionRequest.TransRefNo = cmnGrp.RefNum;
                    transactionRequest.TransOrderNo = cmnGrp.OrderNum;
                    return _creditReq;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;

        }

        /// <summary>
        /// Purpose             :   Creating credit card request
        /// Function Name       :   CreateRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   06/5/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :    06/5/2015 "MM/DD/YYYY"  
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        private CreditRequestDetails CreateEmvContactRequest(TransactionRequest transactionRequest)
        {
            try
            {
                if (!transactionRequest.IsNull())
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                                          MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                        " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    MTDTransactionDto mtdTransaction = new MTDTransactionDto();
                    if (!string.IsNullOrEmpty(transactionRequest.RapidConnectAuthId))
                    {
                        mtdTransaction = _terminalService.Completion(
                           transactionRequest.RapidConnectAuthId,
                      transactionRequest.SerialNumber, transactionRequest.MtdMerchantId);
                        if (mtdTransaction.IsNull())
                            return null;
                    }

                    #region Common Group

                    CommonGrp cmnGrp = new CommonGrp();

                    /* The payment type of the transaction. */
                    PymtTypeType typePayment = transactionRequest.PaymentType;
                    cmnGrp.PymtType = typePayment;
                    cmnGrp.PymtTypeSpecified = true;
                    /* merchant category code. */
                    cmnGrp.MerchCatCode = transactionRequest.MCCode;
                    /* The type of transaction being performed. */
                    cmnGrp.TxnType = transactionRequest.TransType;
                    cmnGrp.TxnTypeSpecified = true;
                    /* The local date and time in which the transaction was performed. */
                    cmnGrp.LocalDateTime = transactionRequest.LocalDateTime;//DateTime.Now.ToString("yyyyMMddHHmmss");
                    /* The transmission date and time of the transaction (in GMT/UCT). */
                    cmnGrp.TrnmsnDateTime = transactionRequest.TrnmsnDateTime;//DateTime.UtcNow.ToString("yyyyMMddHHmmss");
                    /* A number assigned by the merchant to uniquely reference the transaction. 
                     * This number must be unique within a day per Merchant ID per Terminal ID. */
                    cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* A number assigned by the merchant to uniquely reference a set of transactions. */
                    cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* A number assigned by the merchant to uniquely reference a transaction order sequence. */
                    cmnGrp.OrderNum = cmnGrp.RefNum;

                    /* An ID assigned by First Data, for the Third Party Processor or 
                     * Software Vendor that generated the transaction. */
                    cmnGrp.TPPID = transactionRequest.TppId;

                    /* A unique ID assigned to a terminal. */
                    cmnGrp.TermID = transactionRequest.TerminalId;

                    //get terminal id from transaction request

                    /* A unique ID assigned by First Data, to identify the Merchant. */
                    cmnGrp.MerchID = transactionRequest.FdMerchantId;

                    cmnGrp.GroupID = transactionRequest.GroupId;
                    //Group ID value will be assigned by First Data. 
                    //get terminal id from transaction request
                    cmnGrp.POSEntryMode = PaymentAPIResources.RetailContactEMVPosEntryMode;
                    cmnGrp.POSCondCode = POSCondCodeType.Item00;
                    cmnGrp.POSCondCodeSpecified = true;
                    // cmnGrp.TermCatCode = TermCatCodeType.Item01;
                    cmnGrp.TermCatCode = TermCatCodeType.Item01;
                    cmnGrp.TermCatCodeSpecified = true;
                    cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item04;
                    cmnGrp.TermEntryCapabltSpecified = true;
                    cmnGrp.TermLocInd = TermLocIndType.Item0;
                    cmnGrp.TermLocIndSpecified = true;
                    cmnGrp.CardCaptCap = CardCaptCapType.Item1;
                    cmnGrp.CardCaptCapSpecified = true;

                    /* The amount of the transaction. This may be an authorization amount, 
                      * adjustment amount or a reversal amount based on the type of transaction. 
                      * It is inclusive of all additional amounts. 
                      * It is submitted in the currency represented by the Transaction Currency field.  
                      * The field is overwritten in the response for a partial authorization. */

                    double num = Convert.ToDouble(transactionRequest.TransactionAmount);
                    string amt = num.ToString("0.00").Replace(".", string.Empty);
                    cmnGrp.TxnAmt = Convert.ToString(amt);
                    cmnGrp.TxnAmt = cmnGrp.TxnAmt.PadLeft(12, '0');

                    /* The numeric currency of the Transaction Amount. */
                    cmnGrp.TxnCrncy = transactionRequest.CurrencyCode;

                    /* An indicator that describes the location of the terminal. */
                    /* Indicates Group ID. */
                    _creditReq.CommonGrp = cmnGrp;
                    #endregion


                    CardGrp cardGrp = new CardGrp();
                    #region Transarmor group
                    if (string.Equals(cmnGrp.TxnType.ToString(), Authorization, StringComparison.CurrentCultureIgnoreCase) || string.Equals(cmnGrp.TxnType.ToString(), Sale, StringComparison.CurrentCultureIgnoreCase))
                    {

                        cardGrp.CardType = transactionRequest.CardType;
                        cardGrp.CardTypeSpecified = true;
                        _creditReq.CardGrp = cardGrp;
                        //For RSA
                        TAGrp taGrp = new TAGrp();
                        taGrp.SctyLvl = SctyLvlType.EncrptTknizatn;
                        taGrp.SctyLvlSpecified = true;
                        taGrp.EncrptType = EncrptTypeType.RSA;
                        taGrp.EncrptTypeSpecified = true;
                        taGrp.EncrptTrgt = EncrptTrgtType.Track2;
                        taGrp.EncrptTrgtSpecified = true;
                        taGrp.KeyID = transactionRequest.KeyId;
                        taGrp.EncrptBlock = transactionRequest.Track3Data;
                        taGrp.TknType = transactionRequest.TokenType;

                        _creditReq.TAGrp = taGrp;


                        if (string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.CurrentCultureIgnoreCase) || string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.CurrentCultureIgnoreCase))
                        {

                            //cardGrp.CardExpiryDate = transactionRequest.ExpiryDate;
                            cardGrp.CardType = transactionRequest.CardType;
                            cardGrp.CardTypeSpecified = true;
                            _creditReq.CardGrp = cardGrp;

                            TAGrp taGrp1 = new TAGrp();
                            taGrp1.SctyLvl = SctyLvlType.EncrptTknizatn;
                            taGrp1.SctyLvlSpecified = true;
                            taGrp1.Tkn = mtdTransaction.CardToken.Decrypt();
                            taGrp1.TknType = transactionRequest.TokenType;
                            _creditReq.TAGrp = taGrp1;
                        }
                    #endregion
                        if (!string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase) && !string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.OrdinalIgnoreCase))
                        {
                            #region Additional Amount Group
                            AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();
                            addAmtGrp.PartAuthrztnApprvlCapablt = PartAuthrztnApprvlCapabltType.Item1;
                            addAmtGrp.PartAuthrztnApprvlCapabltSpecified = true;
                            AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[1];
                            addAmtGrpArr[0] = addAmtGrp;
                            _creditReq.AddtlAmtGrp = addAmtGrpArr;
                            #endregion
                        }

                        #region emv group
                        EMVGrp emvGrp = new EMVGrp();
                        emvGrp.EMVData = transactionRequest.EmvData;
                        if (transactionRequest.CardSeq != null && transactionRequest.CardSeq != "000")
                        {
                            emvGrp.CardSeqNum = transactionRequest.CardSeq;
                        }
                        _creditReq.EMVGrp = emvGrp;
                        #endregion

                        #region PIN Group
                        if (!string.IsNullOrEmpty(transactionRequest.DebitPin) && !string.IsNullOrEmpty(transactionRequest.KeySerialNumber))
                        {
                            PINGrp pinGroup = new PINGrp();
                            pinGroup.PINData = transactionRequest.DebitPin;
                            pinGroup.KeySerialNumData = transactionRequest.KeySerialNumber;
                            _creditReq.PINGrp = pinGroup;
                        }
                        #endregion

                        #region Visa Group
                        if (string.Equals(cardGrp.CardType.ToString(), Visa, StringComparison.OrdinalIgnoreCase) && !string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.OrdinalIgnoreCase))
                        {
                            VisaGrp visaGrp = new VisaGrp();
                            visaGrp.ACI = ACIType.Y;
                            visaGrp.ACISpecified = true;

                            _creditReq.Item = visaGrp;
                        }
                        #endregion

                        #region AmexGrp Group
                        if (string.Equals(cardGrp.CardType.ToString(), Amex, StringComparison.OrdinalIgnoreCase) && string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                        {
                            AmexGrp amexgrp = new AmexGrp();
                            if (mtdTransaction != null) amexgrp.AmExTranID = mtdTransaction.GatewayTxnId;
                            _creditReq.Item = amexgrp;
                        }
                        #endregion

                        #region Discover Group
                        if ((string.Equals(cardGrp.CardType.ToString(), Diners, StringComparison.OrdinalIgnoreCase) || string.Equals(cardGrp.CardType.ToString(), Discover, StringComparison.OrdinalIgnoreCase) || string.Equals(cardGrp.CardType.ToString(), "JCB", StringComparison.OrdinalIgnoreCase)) && string.Equals(cmnGrp.TxnType.ToString(), "completion", StringComparison.OrdinalIgnoreCase))
                        {
                            DSGrp dsGrp = new DSGrp();
                            if (mtdTransaction != null) dsGrp.DiscNRID = mtdTransaction.GatewayTxnId;
                            _creditReq.Item = dsGrp;
                        }
                        #endregion

                        #region addAm Group

                        if (string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                        {
                            AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();
                            double amt1 = Convert.ToDouble(mtdTransaction.Amount);
                            string addamt = amt1.ToString("0.00").Replace(".", string.Empty);
                            addAmtGrp.AddAmt = addamt;
                            addAmtGrp.AddAmt = addAmtGrp.AddAmt.PadLeft(12, '0');
                            addAmtGrp.AddAmtCrncy = PaymentAPIResources.Cur_USA;
                            addAmtGrp.AddAmtType = AddAmtTypeType.FirstAuthAmt;
                            addAmtGrp.AddAmtTypeSpecified = true;

                            AddtlAmtGrp addAmtGrp1 = new AddtlAmtGrp();
                            addAmtGrp1.AddAmt = addamt;
                            addAmtGrp1.AddAmt = addAmtGrp1.AddAmt.PadLeft(12, '0');
                            addAmtGrp1.AddAmtCrncy = PaymentAPIResources.Cur_USA;
                            addAmtGrp1.AddAmtType = AddAmtTypeType.TotalAuthAmt;
                            addAmtGrp1.AddAmtTypeSpecified = true;

                            AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[2];
                            addAmtGrpArr[0] = addAmtGrp;
                            addAmtGrpArr[1] = addAmtGrp1;
                            _creditReq.AddtlAmtGrp = addAmtGrpArr;

                            OrigAuthGrp orgAuthgrp = new OrigAuthGrp();
                            if (mtdTransaction != null)
                            {
                                orgAuthgrp.OrigAuthID = mtdTransaction.AuthId; // "OK3542";

                                orgAuthgrp.OrigLocalDateTime = mtdTransaction.LocalDateTime;
                                orgAuthgrp.OrigTranDateTime = mtdTransaction.TransmissionDateTime;

                                orgAuthgrp.OrigSTAN = Convert.ToString(mtdTransaction.Stan); //"000001";
                                orgAuthgrp.OrigRespCode = Convert.ToString(mtdTransaction.ResponseCode); //"002";
                            }
                            _creditReq.OrigAuthGrp = orgAuthgrp;

                        }
                        #endregion
                        /* Add the data populated object to GMF message variant object */
                        _gmfMsgVar.Item = _creditReq;
                        transactionRequest.Stan = cmnGrp.STAN;
                        transactionRequest.TransRefNo = cmnGrp.RefNum;
                        transactionRequest.TransOrderNo = cmnGrp.OrderNum;
                        return _creditReq;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;

        }

        /// <summary>
        /// Purpose             :   Creating pinless debit request
        /// Function Name       :   CreateEmvContactlessRequest
        /// Created By          :   
        /// Created On          :   03/20/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/10/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns> 
        private CreditRequestDetails CreateEmvContactlessRequest(TransactionRequest transactionRequest)
        {
            try
            {
                if (!transactionRequest.IsNull())
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                    MTDTransactionDto mtdTransaction = new MTDTransactionDto();
                    if (!string.IsNullOrEmpty(transactionRequest.RapidConnectAuthId))
                    {
                        mtdTransaction = _terminalService.Completion(
                            transactionRequest.RapidConnectAuthId,
                            transactionRequest.SerialNumber,
                            transactionRequest.MtdMerchantId);
                        if (mtdTransaction.IsNull())
                            return null;
                    }

                    #region Common Group

                    CommonGrp cmnGrp = new CommonGrp();

                    /* The payment type of the transaction. */
                    PymtTypeType typePayment = transactionRequest.PaymentType;
                    cmnGrp.PymtType = typePayment;
                    cmnGrp.PymtTypeSpecified = true;
                    /* merchant category code. */
                    cmnGrp.MerchCatCode = transactionRequest.MCCode;
                    /* The type of transaction being performed. */
                    cmnGrp.TxnType = transactionRequest.TransType;
                    cmnGrp.TxnTypeSpecified = true;
                    /* The local date and time in which the transaction was performed. */
                    cmnGrp.LocalDateTime = transactionRequest.LocalDateTime;//DateTime.Now.ToString("yyyyMMddHHmmss");
                    /* The transmission date and time of the transaction (in GMT/UCT). */
                    cmnGrp.TrnmsnDateTime = transactionRequest.TrnmsnDateTime;//DateTime.UtcNow.ToString("yyyyMMddHHmmss");
                    /* A number assigned by the merchant to uniquely reference the transaction. 
                     * This number must be unique within a day per Merchant ID per Terminal ID. */
                    cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* A number assigned by the merchant to uniquely reference a set of transactions. */
                    cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* A number assigned by the merchant to uniquely reference a transaction order sequence. */
                    cmnGrp.OrderNum = cmnGrp.RefNum;

                    /* An ID assigned by First Data, for the Third Party Processor or 
                     * Software Vendor that generated the transaction. */
                    cmnGrp.TPPID = transactionRequest.TppId;

                    /* A unique ID assigned to a terminal. */
                    cmnGrp.TermID = transactionRequest.TerminalId;

                    //get terminal id from transaction request

                    /* A unique ID assigned by First Data, to identify the Merchant. */
                    cmnGrp.MerchID = transactionRequest.FdMerchantId;

                    cmnGrp.GroupID = transactionRequest.GroupId;
                    //Group ID value will be assigned by First Data. 
                    //get terminal id from transaction request
                    cmnGrp.POSEntryMode = PaymentAPIResources.RetailContactlessEMVPosEntryMode;
                    cmnGrp.POSCondCode = POSCondCodeType.Item00;
                    cmnGrp.POSCondCodeSpecified = true;
                    // cmnGrp.TermCatCode = TermCatCodeType.Item01;
                    cmnGrp.TermCatCode = TermCatCodeType.Item01;
                    cmnGrp.TermCatCodeSpecified = true;
                    cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item04;
                    cmnGrp.TermEntryCapabltSpecified = true;
                    cmnGrp.TermLocInd = TermLocIndType.Item0;
                    cmnGrp.TermLocIndSpecified = true;
                    cmnGrp.CardCaptCap = CardCaptCapType.Item1;
                    cmnGrp.CardCaptCapSpecified = true;

                    /* The amount of the transaction. This may be an authorization amount, 
                      * adjustment amount or a reversal amount based on the type of transaction. 
                      * It is inclusive of all additional amounts. 
                      * It is submitted in the currency represented by the Transaction Currency field.  
                      * The field is overwritten in the response for a partial authorization. */

                    double num = Convert.ToDouble(transactionRequest.TransactionAmount);
                    string amt = num.ToString("0.00").Replace(".", string.Empty);
                    cmnGrp.TxnAmt = Convert.ToString(amt);
                    cmnGrp.TxnAmt = cmnGrp.TxnAmt.PadLeft(12, '0');

                    /* The numeric currency of the Transaction Amount. */
                    cmnGrp.TxnCrncy = transactionRequest.CurrencyCode;

                    /* An indicator that describes the location of the terminal. */
                    /* Indicates Group ID. */
                    _creditReq.CommonGrp = cmnGrp;
                    #endregion


                    CardGrp cardGrp = new CardGrp();
                    #region Transarmor group
                    if (string.Equals(cmnGrp.TxnType.ToString(), Authorization, StringComparison.CurrentCultureIgnoreCase) || string.Equals(cmnGrp.TxnType.ToString(), Sale, StringComparison.CurrentCultureIgnoreCase))
                    {

                        cardGrp.CardType = transactionRequest.CardType;
                        cardGrp.CardTypeSpecified = true;
                        _creditReq.CardGrp = cardGrp;
                        //For RSA
                        TAGrp taGrp = new TAGrp();
                        taGrp.SctyLvl = SctyLvlType.EncrptTknizatn;
                        taGrp.SctyLvlSpecified = true;
                        taGrp.EncrptType = EncrptTypeType.RSA;
                        taGrp.EncrptTypeSpecified = true;
                        taGrp.EncrptTrgt = EncrptTrgtType.Track2;
                        taGrp.EncrptTrgtSpecified = true;
                        taGrp.KeyID = transactionRequest.KeyId;
                        taGrp.EncrptBlock = transactionRequest.Track3Data;
                        taGrp.TknType = transactionRequest.TokenType;

                        _creditReq.TAGrp = taGrp;

                    }
                    if (string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.CurrentCultureIgnoreCase) || string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.CurrentCultureIgnoreCase))
                    {

                        //cardGrp.CardExpiryDate = transactionRequest.ExpiryDate;
                        cardGrp.CardType = transactionRequest.CardType;
                        cardGrp.CardTypeSpecified = true;
                        _creditReq.CardGrp = cardGrp;

                        TAGrp taGrp = new TAGrp();
                        taGrp.SctyLvl = SctyLvlType.EncrptTknizatn;
                        taGrp.SctyLvlSpecified = true;
                        taGrp.Tkn = mtdTransaction.CardToken.Decrypt();
                        taGrp.TknType = transactionRequest.TokenType;
                        _creditReq.TAGrp = taGrp;
                    }
                    #endregion
                    if (!string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase) && !string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.OrdinalIgnoreCase))
                    {
                        #region Additional Amount Group
                        AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();
                        addAmtGrp.PartAuthrztnApprvlCapablt = PartAuthrztnApprvlCapabltType.Item1;
                        addAmtGrp.PartAuthrztnApprvlCapabltSpecified = true;
                        AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[1];
                        addAmtGrpArr[0] = addAmtGrp;
                        _creditReq.AddtlAmtGrp = addAmtGrpArr;
                        #endregion
                    }

                    #region emv group
                    EMVGrp emvGrp = new EMVGrp();
                    emvGrp.EMVData = transactionRequest.EmvData;
                    _creditReq.EMVGrp = emvGrp;
                    #endregion

                    #region Visa Group
                    if (string.Equals(cardGrp.CardType.ToString(), Visa, StringComparison.OrdinalIgnoreCase) && !string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.OrdinalIgnoreCase))
                    {
                        VisaGrp visaGrp = new VisaGrp();
                        visaGrp.VisaBID = PaymentAPIResources.VisaBID;
                        visaGrp.VisaAUAR = PaymentAPIResources.VisaAUAR;
                        if (!string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                        {
                            visaGrp.ACI = ACIType.Y;
                            visaGrp.ACISpecified = true;
                            visaGrp.TaxAmtCapablt = TaxAmtCapabltType.Item1;
                            visaGrp.TaxAmtCapabltSpecified = true;
                        }
                        else
                        {
                            visaGrp.ACI = ACIType.T;
                            visaGrp.ACISpecified = true;
                        }
                        _creditReq.Item = visaGrp;
                    }
                    #endregion

                    #region AmexGrp Group
                    if (string.Equals(cardGrp.CardType.ToString(), Amex, StringComparison.OrdinalIgnoreCase) && string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                    {
                        AmexGrp amexgrp = new AmexGrp();
                        if (mtdTransaction != null) amexgrp.AmExTranID = mtdTransaction.GatewayTxnId;
                        _creditReq.Item = amexgrp;
                    }
                    #endregion

                    #region Discover Group
                    if ((string.Equals(cardGrp.CardType.ToString(), Diners, StringComparison.OrdinalIgnoreCase) || string.Equals(cardGrp.CardType.ToString(), Discover, StringComparison.OrdinalIgnoreCase) || string.Equals(cardGrp.CardType.ToString(), "JCB", StringComparison.OrdinalIgnoreCase)) && string.Equals(cmnGrp.TxnType.ToString(), "completion", StringComparison.OrdinalIgnoreCase))
                    {
                        DSGrp dsGrp = new DSGrp();
                        if (mtdTransaction != null) dsGrp.DiscNRID = mtdTransaction.GatewayTxnId;
                        _creditReq.Item = dsGrp;
                    }
                    #endregion


                    #region addAm Group

                    if (string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                    {
                        AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();
                        double amt1 = Convert.ToDouble(mtdTransaction.Amount);
                        string addamt = amt1.ToString("0.00").Replace(".", string.Empty);
                        addAmtGrp.AddAmt = addamt;
                        addAmtGrp.AddAmt = addAmtGrp.AddAmt.PadLeft(12, '0');
                        addAmtGrp.AddAmtCrncy = PaymentAPIResources.Cur_USA;
                        addAmtGrp.AddAmtType = AddAmtTypeType.FirstAuthAmt;
                        addAmtGrp.AddAmtTypeSpecified = true;

                        AddtlAmtGrp addAmtGrp1 = new AddtlAmtGrp();
                        addAmtGrp1.AddAmt = addamt;
                        addAmtGrp1.AddAmt = addAmtGrp1.AddAmt.PadLeft(12, '0');
                        addAmtGrp1.AddAmtCrncy = PaymentAPIResources.Cur_USA;
                        addAmtGrp1.AddAmtType = AddAmtTypeType.TotalAuthAmt;
                        addAmtGrp1.AddAmtTypeSpecified = true;

                        AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[2];
                        addAmtGrpArr[0] = addAmtGrp;
                        addAmtGrpArr[1] = addAmtGrp1;
                        _creditReq.AddtlAmtGrp = addAmtGrpArr;

                        OrigAuthGrp orgAuthgrp = new OrigAuthGrp();
                        if (mtdTransaction != null)
                        {
                            orgAuthgrp.OrigAuthID = mtdTransaction.AuthId; // "OK3542";

                            orgAuthgrp.OrigLocalDateTime = mtdTransaction.LocalDateTime;
                            orgAuthgrp.OrigTranDateTime = mtdTransaction.TransmissionDateTime;

                            orgAuthgrp.OrigSTAN = Convert.ToString(mtdTransaction.Stan); //"000001";
                            orgAuthgrp.OrigRespCode = Convert.ToString(mtdTransaction.ResponseCode); //"002";
                        }
                        _creditReq.OrigAuthGrp = orgAuthgrp;

                    }
                    #endregion
                    /* Add the data populated object to GMF message variant object */
                    _gmfMsgVar.Item = _creditReq;
                    transactionRequest.Stan = cmnGrp.STAN;
                    transactionRequest.TransRefNo = cmnGrp.RefNum;
                    transactionRequest.TransOrderNo = cmnGrp.OrderNum;
                    return _creditReq;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;

        }

        /// <summary>
        /// Purpose             :   Creating pinless debit request
        /// Function Name       :   CreatePinlessDebitRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   03/20/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/10/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        private PinlessDebitRequestDetails CreatePinlessDebitRequest(TransactionRequest transactionRequest)
        {
            /* Based on the GMF Specification UMF_Schema_V1.1.14.xsd, fields that are mandatory or related to 
               this transaction should be populated.*/
            try
            {
                if (!transactionRequest.IsNull())
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                                               MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                        " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    MTDTransactionDto mtdTransaction = new MTDTransactionDto();
                    if (!string.IsNullOrEmpty(transactionRequest.RapidConnectAuthId))
                    {
                        mtdTransaction = _terminalService.Completion(
                           transactionRequest.RapidConnectAuthId,
                      transactionRequest.SerialNumber, transactionRequest.MtdMerchantId);
                        if (mtdTransaction.IsNull())
                            return null;
                    }
                    #region Common Group

                    /* Populate values for Common Group */
                    CommonGrp cmnGrp = new CommonGrp();

                    /* The payment type of the transaction. */
                    cmnGrp.PymtType = PymtTypeType.PLDebit;
                    cmnGrp.PymtTypeSpecified = true;

                    /* The type of transaction being performed. */
                    cmnGrp.TxnType = transactionRequest.TransactionType == Void
                        ? TxnTypeType.Sale
                        : transactionRequest.TransType;
                    cmnGrp.TxnTypeSpecified = true;
                    cmnGrp.MerchCatCode = transactionRequest.MCCode;
                    /* The local date and time in which the transaction was performed. */
                    cmnGrp.LocalDateTime = transactionRequest.LocalDateTime;//DateTime.Now.ToString("yyyyMMddHHmmss");
                    /* The transmission date and time of the transaction (in GMT/UCT). */
                    cmnGrp.TrnmsnDateTime = transactionRequest.TrnmsnDateTime;//DateTime.UtcNow.ToString("yyyyMMddHHmmss");

                    /* A number assigned by the merchant to uniquely reference the transaction. 
                     * This number must be unique within a day per Merchant ID per Terminal ID. */
                    cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* A number assigned by the merchant to uniquely reference a set of transactions. 
                     * sThis number must be unique within a day for a given Merchant ID/ Terminal ID. */
                    cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* Order number of the transaction */
                    cmnGrp.OrderNum = cmnGrp.RefNum;

                    /* An ID assigned by First Data, for the Third Party Processor or 
                     * Software Vendor that generated the transaction. */
                    cmnGrp.TPPID = transactionRequest.TppId;

                    /* A unique ID assigned to a terminal. */
                    cmnGrp.TermID = transactionRequest.TerminalId;

                    /* A unique ID assigned by First Data, to identify the Merchant. */
                    cmnGrp.MerchID = transactionRequest.FdMerchantId;

                    /* An identifier used to indicate the terminal’s account number entry mode 
                     * and authentication capability via the Point-of-Service. */
                    cmnGrp.POSEntryMode = PaymentAPIResources.EcomPosEntryMode;

                    /* An identifier used to indicate the authorization conditions at the Point-of-Service (POS). */
                    cmnGrp.POSCondCode = POSCondCodeType.Item59;
                    cmnGrp.POSCondCodeSpecified = true;

                    /* An identifier used to describe the type of terminal being used for the transaction. */
                    cmnGrp.TermCatCode = TermCatCodeType.Item00;
                    cmnGrp.TermCatCodeSpecified = true;

                    /* An identifier used to indicate the entry mode capability of the terminal. */
                    cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item00;
                    cmnGrp.TermEntryCapabltSpecified = true;

                    /* The amount of the transaction. This may be an authorization amount, 
                     * adjustment amount or a reversal amount based on the type of transaction. 
                     * It is inclusive of all additional amounts. 
                     * It is submitted in the currency represented by the Transaction Currency field.  
                     * The field is overwritten in the response for a partial authorization. */
                    //decimal amt = Convert.ToDecimal(transactionRequest.TransactionAmount);
                    //amt = amt*100;
                    //cmnGrp.TxnAmt = Convert.ToString(amt);
                    //cmnGrp.TxnAmt = cmnGrp.TxnAmt.Replace(".00", string.Empty).PadLeft(12, '0');
                    double num = Convert.ToDouble(transactionRequest.TransactionAmount);
                    string amt = num.ToString("0.00").Replace(".", string.Empty);
                    cmnGrp.TxnAmt = Convert.ToString(amt);
                    cmnGrp.TxnAmt = cmnGrp.TxnAmt.PadLeft(12, '0');

                    /* The numeric currency of the Transaction Amount. */
                    cmnGrp.TxnCrncy = PaymentAPIResources.Cur_USA;

                    /* An indicator that describes the location of the terminal. */
                    cmnGrp.TermLocInd = TermLocIndType.Item1;
                    cmnGrp.TermLocIndSpecified = true;

                    /* Indicates whether or not the terminal has the capability to capture the card data. */
                    cmnGrp.CardCaptCap = CardCaptCapType.Item0;
                    cmnGrp.CardCaptCapSpecified = true;

                    /* Indicates Group ID. */
                    cmnGrp.GroupID = transactionRequest.GroupId;
                    //Group ID value will be assigned by First Data.  //This is dummy value. Please use the actual value

                    _pinlessDebit.CommonGrp = cmnGrp;

                    #endregion

                    #region Card Group
                    /* Populate values for Card Group */
                    CardGrp crdGrp = new CardGrp();
                    TAGrp taGrp = new TAGrp();
                    if (string.Equals(cmnGrp.TxnType.ToString(), Sale, StringComparison.CurrentCultureIgnoreCase) ||
                        string.Equals(cmnGrp.TxnType.ToString(), Authorization, StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (transactionRequest.TransactionType == Void)
                        {
                            taGrp.Tkn = mtdTransaction.CardToken.Decrypt();
                        }
                        else
                            crdGrp.AcctNum = transactionRequest.CardNumber;
                    }
                    else
                    {
                        taGrp.Tkn = mtdTransaction.CardToken.Decrypt();
                    }
                    crdGrp.CardExpiryDate = transactionRequest.ExpiryDate;
                    _pinlessDebit.CardGrp = crdGrp;

                    #endregion

                    #region TAgrp

                    taGrp.SctyLvl = SctyLvlType.Tknizatn;
                    taGrp.SctyLvlSpecified = true;
                    taGrp.TknType = transactionRequest.TokenType;
                    _pinlessDebit.TAGrp = taGrp;
                    #endregion

                    #region Ecommerce Group

                    EcommGrp ecomgrp = new EcommGrp();
                    ecomgrp.EcommTxnInd = EcommTxnIndType.Item03;
                    ecomgrp.EcommTxnIndSpecified = true;

                    ecomgrp.EcommURL = PaymentAPIResources.EcomUrl;
                    _pinlessDebit.EcommGrp = ecomgrp;

                    #endregion

                    #region addAm Group

                    if (string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                    {
                        AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();
                        double amt1 = Convert.ToDouble(mtdTransaction.Amount);
                        string addamt = amt1.ToString("0.00").Replace(".", string.Empty);
                        addAmtGrp.AddAmt = addamt;
                        addAmtGrp.AddAmt = addAmtGrp.AddAmt.PadLeft(12, '0');
                        addAmtGrp.AddAmtCrncy = PaymentAPIResources.Cur_USA;
                        addAmtGrp.AddAmtType = AddAmtTypeType.FirstAuthAmt;
                        addAmtGrp.AddAmtTypeSpecified = true;

                        AddtlAmtGrp addAmtGrp1 = new AddtlAmtGrp();
                        addAmtGrp1.AddAmt = addamt;
                        addAmtGrp1.AddAmt = addAmtGrp1.AddAmt.PadLeft(12, '0');
                        addAmtGrp1.AddAmtCrncy = PaymentAPIResources.Cur_USA;
                        addAmtGrp1.AddAmtType = AddAmtTypeType.TotalAuthAmt;
                        addAmtGrp1.AddAmtTypeSpecified = true;

                        AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[2];
                        addAmtGrpArr[0] = addAmtGrp;
                        addAmtGrpArr[1] = addAmtGrp1;
                        _pinlessDebit.AddtlAmtGrp = addAmtGrpArr;

                        OrigAuthGrp orgAuthgrp = new OrigAuthGrp();
                        if (mtdTransaction != null)
                        {
                            orgAuthgrp.OrigAuthID = mtdTransaction.AuthId; // "OK3542";

                            orgAuthgrp.OrigLocalDateTime = mtdTransaction.LocalDateTime;
                            orgAuthgrp.OrigTranDateTime = mtdTransaction.TransmissionDateTime;

                            orgAuthgrp.OrigSTAN = Convert.ToString(mtdTransaction.Stan); //"000001";
                            orgAuthgrp.OrigRespCode = Convert.ToString(mtdTransaction.ResponseCode); //"002";
                        }
                        _pinlessDebit.OrigAuthGrp = orgAuthgrp;

                    }
                    #endregion

                    /* Add the data populated object to GMF message variant object */
                    _gmfMsgVar.Item = _pinlessDebit;
                    transactionRequest.Stan = cmnGrp.STAN;
                    transactionRequest.TransRefNo = cmnGrp.RefNum;
                    transactionRequest.TransOrderNo = cmnGrp.OrderNum;
                    return _pinlessDebit;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :   Creating pin debit request
        /// Function Name       :   CreatePinDebitRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   03/20/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/20/2015 "MM/DD/YYYY"  
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        private DebitRequestDetails CreatePinDebitRequest(TransactionRequest transactionRequest)
        {
            /* Based on the GMF Specification UMF_Schema_V1.1.14.xsd, fields that are mandatory or related to 
               this transaction should be populated.*/
            try
            {
                if (!transactionRequest.IsNull())
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                                          MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                        " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    MTDTransactionDto mtdTransaction = new MTDTransactionDto();
                    if (!string.IsNullOrEmpty(transactionRequest.RapidConnectAuthId))
                    {
                        mtdTransaction = _terminalService.Completion(
                           transactionRequest.RapidConnectAuthId,
                      transactionRequest.SerialNumber, transactionRequest.MtdMerchantId);
                        if (mtdTransaction.IsNull())
                            return null;
                    }

                    #region Common Group
                    /* Populate values for Common Group */
                    CommonGrp cmnGrp = new CommonGrp();

                    /* The payment type of the transaction. */
                    cmnGrp.PymtType = PymtTypeType.Debit;
                    cmnGrp.PymtTypeSpecified = true;
                    cmnGrp.MerchCatCode = transactionRequest.MCCode;
                    /* The type of transaction being performed. */
                    cmnGrp.TxnType = transactionRequest.TransactionType == Void ? TxnTypeType.Sale : transactionRequest.TransType;
                    cmnGrp.TxnTypeSpecified = true;

                    /* The local date and time in which the transaction was performed. */
                    cmnGrp.LocalDateTime = transactionRequest.LocalDateTime;//DateTime.Now.ToString("yyyyMMddHHmmss");
                    /* The transmission date and time of the transaction (in GMT/UCT). */
                    cmnGrp.TrnmsnDateTime = transactionRequest.TrnmsnDateTime;//DateTime.UtcNow.ToString("yyyyMMddHHmmss");

                    /* A number assigned by the merchant to uniquely reference the transaction. 
                     * This number must be unique within a day per Merchant ID per Terminal ID. */
                    cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* A number assigned by the merchant to uniquely reference a set of transactions. 
                     * sThis number must be unique within a day for a given Merchant ID/ Terminal ID. */
                    cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* Order number of the transaction*/
                    cmnGrp.OrderNum = cmnGrp.RefNum;

                    /* An ID assigned by First Data, for the Third Party Processor or 
                     * Software Vendor that generated the transaction. */
                    cmnGrp.TPPID = transactionRequest.TppId;

                    /* A unique ID assigned to a terminal. */
                    cmnGrp.TermID = transactionRequest.TerminalId;

                    /* A unique ID assigned by First Data, to identify the Merchant. */
                    cmnGrp.MerchID = transactionRequest.FdMerchantId;


                    /* An identifier used to indicate the terminal’s account number entry mode 
                     * and authentication capability via the Point-of-Service. */
                    cmnGrp.POSEntryMode = PaymentAPIResources.RetailPosEntryMode;

                    /* An identifier used to indicate the authorization conditions at the Point-of-Service (POS). */
                    cmnGrp.POSCondCode = POSCondCodeType.Item00;
                    cmnGrp.POSCondCodeSpecified = true;

                    /* An identifier used to describe the type of terminal being used for the transaction. */
                    cmnGrp.TermCatCode = TermCatCodeType.Item00;
                    cmnGrp.TermCatCodeSpecified = true;

                    /* An identifier used to indicate the entry mode capability of the terminal. */
                    cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item01;
                    cmnGrp.TermEntryCapabltSpecified = true;

                    /* The amount of the transaction. This may be an authorization amount, 
                     * adjustment amount or a reversal amount based on the type of transaction. 
                     * It is inclusive of all additional amounts. 
                     * It is submitted in the currency represented by the Transaction Currency field.  
                     * The field is overwritten in the response for a partial authorization. */
                    //cmnGrp.TxnAmt = transactionRequest.TransactionAmount;
                    //decimal amt = Convert.ToDecimal(transactionRequest.TransactionAmount);
                    //amt = amt * 100;
                    //cmnGrp.TxnAmt = Convert.ToString(amt);
                    //cmnGrp.TxnAmt = cmnGrp.TxnAmt.Replace(".00", string.Empty).PadLeft(12, '0');
                    double num = Convert.ToDouble(transactionRequest.TransactionAmount);
                    string amt = num.ToString("0.00").Replace(".", string.Empty);
                    cmnGrp.TxnAmt = Convert.ToString(amt);
                    cmnGrp.TxnAmt = cmnGrp.TxnAmt.PadLeft(12, '0');

                    /* The numeric currency of the Transaction Amount. */
                    cmnGrp.TxnCrncy = PaymentAPIResources.Cur_USA;

                    /* An indicator that describes the location of the terminal. */
                    cmnGrp.TermLocInd = TermLocIndType.Item0;
                    cmnGrp.TermLocIndSpecified = true;

                    /* Indicates whether or not the terminal has the capability to capture the card data. */
                    cmnGrp.CardCaptCap = CardCaptCapType.Item1;
                    cmnGrp.CardCaptCapSpecified = true;

                    /* Indicates Group ID. */
                    cmnGrp.GroupID = transactionRequest.GroupId; //Group ID value will be assigned by First Data.  //This is dummy value. Please use the actual value

                    _debitReq.CommonGrp = cmnGrp;
                    #endregion

                    #region TAgrp
                    if (string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                    {
                        TAGrp taGrp = new TAGrp();
                        taGrp.SctyLvl = SctyLvlType.EncrptTknizatn;
                        taGrp.SctyLvlSpecified = true;
                        taGrp.Tkn = StringExtension.Decrypt(mtdTransaction.CardToken);
                        taGrp.TknType = transactionRequest.TokenType;
                        _debitReq.TAGrp = taGrp;
                    }
                    else
                    {
                        TAGrp taGrp = new TAGrp();
                        taGrp.SctyLvl = SctyLvlType.EncrptTknizatn;
                        taGrp.SctyLvlSpecified = true;
                        taGrp.EncrptType = EncrptTypeType.RSA;
                        taGrp.EncrptTypeSpecified = true;
                        taGrp.EncrptTrgt = EncrptTrgtType.Track2;
                        taGrp.EncrptTrgtSpecified = true;
                        taGrp.KeyID = transactionRequest.KeyId; //"VAR11;TEST1";
                        taGrp.EncrptBlock = transactionRequest.Track3Data;
                        taGrp.TknType = transactionRequest.TokenType;
                        _debitReq.TAGrp = taGrp;

                        #region PIN Group
                        /* Populate values for PIN Group */
                        PINGrp pinGroup = new PINGrp();

                        /* The PIN Data for the Debit or EBT transaction being submitted.
                         * HEXADecimal value need to be entered. */
                        pinGroup.PINData = transactionRequest.DebitPin;
                        //pinGroup.PINData = transactionRequest.PinData; // remove hard code
                        /* Provides the initialization vector for DUKPT PIN Debit and EBT transactions. */
                        pinGroup.KeySerialNumData = transactionRequest.KeySerialNumber; // remove hard code
                        //pinGroup.KeySerialNumData = transactionRequest.KeySerNumData;

                        _debitReq.PINGrp = pinGroup;
                        #endregion
                    }
                    #endregion


                    #region Additional Amount Group

                    if (string.Equals(cmnGrp.TxnType.ToString(), Sale, StringComparison.OrdinalIgnoreCase) || string.Equals(cmnGrp.TxnType.ToString(), Authorization, StringComparison.OrdinalIgnoreCase))
                    {
                        /*  Populate values for Additional Amount Group */
                        AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();

                        /* An identifier used to indicate whether or not the 
                         * terminal/software can support partial authorization approvals.  */
                        addAmtGrp.PartAuthrztnApprvlCapablt = PartAuthrztnApprvlCapabltType.Item1;
                        addAmtGrp.PartAuthrztnApprvlCapabltSpecified = true;

                        /* Creating a generic array of Additional Amount 
                         * Group type to sent the data to as an array */
                        AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[1];
                        addAmtGrpArr[0] = addAmtGrp;

                        _debitReq.AddtlAmtGrp = addAmtGrpArr;
                    }

                    #endregion
                    #region addAm Group

                    if (string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                    {
                        AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();
                        double amt1 = Convert.ToDouble(mtdTransaction.Amount);
                        string addamt = amt1.ToString("0.00").Replace(".", string.Empty);
                        addAmtGrp.AddAmt = addamt;
                        addAmtGrp.AddAmt = addAmtGrp.AddAmt.PadLeft(12, '0');
                        addAmtGrp.AddAmtCrncy = PaymentAPIResources.Cur_USA;
                        addAmtGrp.AddAmtType = AddAmtTypeType.FirstAuthAmt;
                        addAmtGrp.AddAmtTypeSpecified = true;

                        AddtlAmtGrp addAmtGrp1 = new AddtlAmtGrp();
                        addAmtGrp1.AddAmt = addamt;
                        addAmtGrp1.AddAmt = addAmtGrp1.AddAmt.PadLeft(12, '0');
                        addAmtGrp1.AddAmtCrncy = PaymentAPIResources.Cur_USA;
                        addAmtGrp1.AddAmtType = AddAmtTypeType.TotalAuthAmt;
                        addAmtGrp1.AddAmtTypeSpecified = true;

                        AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[2];
                        addAmtGrpArr[0] = addAmtGrp;
                        addAmtGrpArr[1] = addAmtGrp1;
                        _debitReq.AddtlAmtGrp = addAmtGrpArr;

                        OrigAuthGrp orgAuthgrp = new OrigAuthGrp();
                        if (mtdTransaction != null)
                        {
                            orgAuthgrp.OrigAuthID = mtdTransaction.AuthId; // "OK3542";

                            orgAuthgrp.OrigLocalDateTime = mtdTransaction.LocalDateTime;
                            orgAuthgrp.OrigTranDateTime = mtdTransaction.TransmissionDateTime;

                            orgAuthgrp.OrigSTAN = Convert.ToString(mtdTransaction.Stan); //"000001";
                            orgAuthgrp.OrigRespCode = Convert.ToString(mtdTransaction.ResponseCode); //"002";
                        }
                        _debitReq.OrigAuthGrp = orgAuthgrp;

                    }
                    #endregion
                    /* Add the data populated object to GMF message variant object */
                    _gmfMsgVar.Item = _debitReq;
                    transactionRequest.Stan = cmnGrp.STAN;
                    transactionRequest.TransRefNo = cmnGrp.RefNum;
                    transactionRequest.TransOrderNo = cmnGrp.OrderNum;
                    return _debitReq;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :   Creating pin debit request for contact EMV
        /// Function Name       :   CreateEmvContactPinDebitRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   07/27/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/20/2015 "MM/DD/YYYY"  
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        private DebitRequestDetails CreateEmvContactPinDebitRequest(TransactionRequest transactionRequest)
        {
            /* Based on the GMF Specification UMF_Schema_V1.1.14.xsd, fields that are mandatory or related to 
               this transaction should be populated.*/
            try
            {
                if (!transactionRequest.IsNull())
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                                          MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                        " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    #region Common Group

                    CommonGrp cmnGrp = new CommonGrp();

                    /* The payment type of the transaction. */
                    cmnGrp.PymtType = PymtTypeType.Debit;
                    cmnGrp.PymtTypeSpecified = true;
                    cmnGrp.MerchCatCode = transactionRequest.MCCode;
                    /* The type of transaction being performed. */
                    cmnGrp.TxnType = transactionRequest.TransactionType == Void ? TxnTypeType.Sale : transactionRequest.TransType;
                    cmnGrp.TxnTypeSpecified = true;

                    /* The local date and time in which the transaction was performed. */
                    cmnGrp.LocalDateTime = transactionRequest.LocalDateTime;//DateTime.Now.ToString("yyyyMMddHHmmss");
                    /* The transmission date and time of the transaction (in GMT/UCT). */
                    cmnGrp.TrnmsnDateTime = transactionRequest.TrnmsnDateTime;//DateTime.UtcNow.ToString("yyyyMMddHHmmss");

                    /* A number assigned by the merchant to uniquely reference the transaction. 
                     * This number must be unique within a day per Merchant ID per Terminal ID. */
                    cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* A number assigned by the merchant to uniquely reference a set of transactions. 
                     * sThis number must be unique within a day for a given Merchant ID/ Terminal ID. */
                    cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* Order number of the transaction*/
                    cmnGrp.OrderNum = cmnGrp.RefNum;

                    /* An ID assigned by First Data, for the Third Party Processor or 
                     * Software Vendor that generated the transaction. */
                    cmnGrp.TPPID = transactionRequest.TppId;

                    /* A unique ID assigned to a terminal. */
                    cmnGrp.TermID = transactionRequest.TerminalId;

                    /* A unique ID assigned by First Data, to identify the Merchant. */
                    cmnGrp.MerchID = transactionRequest.FdMerchantId;


                    /* An identifier used to indicate the terminal’s account number entry mode 
                     * and authentication capability via the Point-of-Service. */
                    cmnGrp.POSEntryMode = PaymentAPIResources.RetailContactEMVPosEntryMode;

                    /* An identifier used to indicate the authorization conditions at the Point-of-Service (POS). */
                    cmnGrp.POSCondCode = POSCondCodeType.Item00;
                    cmnGrp.POSCondCodeSpecified = true;

                    /* An identifier used to describe the type of terminal being used for the transaction. */
                    cmnGrp.TermCatCode = TermCatCodeType.Item01;
                    cmnGrp.TermCatCodeSpecified = true;

                    /* An identifier used to indicate the entry mode capability of the terminal. */
                    cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item04;
                    cmnGrp.TermEntryCapabltSpecified = true;

                    /* The amount of the transaction. This may be an authorization amount, 
                     * adjustment amount or a reversal amount based on the type of transaction. 
                     * It is inclusive of all additional amounts. 
                     * It is submitted in the currency represented by the Transaction Currency field.  
                     * The field is overwritten in the response for a partial authorization. */

                    double num = Convert.ToDouble(transactionRequest.TransactionAmount);
                    string amt = num.ToString("0.00").Replace(".", string.Empty);
                    cmnGrp.TxnAmt = Convert.ToString(amt);
                    cmnGrp.TxnAmt = cmnGrp.TxnAmt.PadLeft(12, '0');

                    /* The numeric currency of the Transaction Amount. */
                    cmnGrp.TxnCrncy = PaymentAPIResources.Cur_USA;

                    /* An indicator that describes the location of the terminal. */
                    cmnGrp.TermLocInd = TermLocIndType.Item0;
                    cmnGrp.TermLocIndSpecified = true;

                    /* Indicates whether or not the terminal has the capability to capture the card data. */
                    cmnGrp.CardCaptCap = CardCaptCapType.Item1;
                    cmnGrp.CardCaptCapSpecified = true;

                    /* Indicates Group ID. */
                    cmnGrp.GroupID = transactionRequest.GroupId; //Group ID value will be assigned by First Data.  //This is dummy value. Please use the actual value

                    _debitReq.CommonGrp = cmnGrp;
                    #endregion

                    #region TAgrp
                    TAGrp taGrp = new TAGrp();
                    taGrp.SctyLvl = SctyLvlType.EncrptTknizatn;
                    taGrp.SctyLvlSpecified = true;
                    taGrp.EncrptType = EncrptTypeType.RSA;
                    taGrp.EncrptTypeSpecified = true;
                    taGrp.EncrptTrgt = EncrptTrgtType.Track2;
                    taGrp.EncrptTrgtSpecified = true;
                    taGrp.KeyID = transactionRequest.KeyId; //"VAR11;TEST1";
                    taGrp.EncrptBlock = transactionRequest.Track3Data;
                    taGrp.TknType = transactionRequest.TokenType;
                    _debitReq.TAGrp = taGrp;
                    #endregion

                    #region PIN Group

                    PINGrp pinGroup = new PINGrp();
                    pinGroup.PINData = transactionRequest.DebitPin;// "FFFFFFFFFFFFFFFF";//
                    pinGroup.KeySerialNumData = transactionRequest.KeySerialNumber;   // "00000000000000000000";
                    _debitReq.PINGrp = pinGroup;
                    #endregion

                    #region emv group
                    EMVGrp emvGrp = new EMVGrp();
                    emvGrp.EMVData = transactionRequest.EmvData;
                    emvGrp.CardSeqNum = transactionRequest.CardSeq;
                    _debitReq.EMVGrp = emvGrp;
                    #endregion

                    #region Additional Amount Group

                    if (string.Equals(cmnGrp.TxnType.ToString(), Sale, StringComparison.OrdinalIgnoreCase))
                    {
                        /*  Populate values for Additional Amount Group */
                        AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();

                        /* An identifier used to indicate whether or not the 
                         * terminal/software can support partial authorization approvals.  */
                        addAmtGrp.PartAuthrztnApprvlCapablt = PartAuthrztnApprvlCapabltType.Item1;
                        addAmtGrp.PartAuthrztnApprvlCapabltSpecified = true;

                        /* Creating a generic array of Additional Amount 
                         * Group type to sent the data to as an array */
                        AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[1];
                        addAmtGrpArr[0] = addAmtGrp;

                        _debitReq.AddtlAmtGrp = addAmtGrpArr;
                    }

                    #endregion

                    /* Add the data populated object to GMF message variant object */
                    _gmfMsgVar.Item = _debitReq;
                    transactionRequest.Stan = cmnGrp.STAN;
                    transactionRequest.TransRefNo = cmnGrp.RefNum;
                    transactionRequest.TransOrderNo = cmnGrp.OrderNum;
                    return _debitReq;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        private DebitRequestDetails CreateEmvContactlessPinDebitRequest(TransactionRequest transactionRequest)
        {
            /* Based on the GMF Specification UMF_Schema_V1.1.14.xsd, fields that are mandatory or related to 
               this transaction should be populated.*/
            try
            {
                if (!transactionRequest.IsNull())
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                                          MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                        " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    #region Common Group

                    CommonGrp cmnGrp = new CommonGrp();

                    /* The payment type of the transaction. */
                    cmnGrp.PymtType = PymtTypeType.Debit;
                    cmnGrp.PymtTypeSpecified = true;
                    cmnGrp.MerchCatCode = transactionRequest.MCCode;
                    /* The type of transaction being performed. */
                    cmnGrp.TxnType = transactionRequest.TransactionType == Void ? TxnTypeType.Sale : transactionRequest.TransType;
                    cmnGrp.TxnTypeSpecified = true;

                    /* The local date and time in which the transaction was performed. */
                    cmnGrp.LocalDateTime = transactionRequest.LocalDateTime;//DateTime.Now.ToString("yyyyMMddHHmmss");
                    /* The transmission date and time of the transaction (in GMT/UCT). */
                    cmnGrp.TrnmsnDateTime = transactionRequest.TrnmsnDateTime;//DateTime.UtcNow.ToString("yyyyMMddHHmmss");

                    /* A number assigned by the merchant to uniquely reference the transaction. 
                     * This number must be unique within a day per Merchant ID per Terminal ID. */
                    cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* A number assigned by the merchant to uniquely reference a set of transactions. 
                     * sThis number must be unique within a day for a given Merchant ID/ Terminal ID. */
                    cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* Order number of the transaction*/
                    cmnGrp.OrderNum = cmnGrp.RefNum;

                    /* An ID assigned by First Data, for the Third Party Processor or 
                     * Software Vendor that generated the transaction. */
                    cmnGrp.TPPID = transactionRequest.TppId;

                    /* A unique ID assigned to a terminal. */
                    cmnGrp.TermID = transactionRequest.TerminalId;

                    /* A unique ID assigned by First Data, to identify the Merchant. */
                    cmnGrp.MerchID = transactionRequest.FdMerchantId;


                    /* An identifier used to indicate the terminal’s account number entry mode 
                     * and authentication capability via the Point-of-Service. */
                    cmnGrp.POSEntryMode = PaymentAPIResources.RetailContactEMVPosEntryMode;

                    /* An identifier used to indicate the authorization conditions at the Point-of-Service (POS). */
                    cmnGrp.POSCondCode = POSCondCodeType.Item00;
                    cmnGrp.POSCondCodeSpecified = true;

                    /* An identifier used to describe the type of terminal being used for the transaction. */
                    cmnGrp.TermCatCode = TermCatCodeType.Item00;
                    cmnGrp.TermCatCodeSpecified = true;

                    /* An identifier used to indicate the entry mode capability of the terminal. */
                    cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item04;
                    cmnGrp.TermEntryCapabltSpecified = true;

                    /* The amount of the transaction. This may be an authorization amount, 
                     * adjustment amount or a reversal amount based on the type of transaction. 
                     * It is inclusive of all additional amounts. 
                     * It is submitted in the currency represented by the Transaction Currency field.  
                     * The field is overwritten in the response for a partial authorization. */

                    double num = Convert.ToDouble(transactionRequest.TransactionAmount);
                    string amt = num.ToString("0.00").Replace(".", string.Empty);
                    cmnGrp.TxnAmt = Convert.ToString(amt);
                    cmnGrp.TxnAmt = cmnGrp.TxnAmt.PadLeft(12, '0');

                    /* The numeric currency of the Transaction Amount. */
                    cmnGrp.TxnCrncy = PaymentAPIResources.Cur_USA;

                    /* An indicator that describes the location of the terminal. */
                    cmnGrp.TermLocInd = TermLocIndType.Item0;
                    cmnGrp.TermLocIndSpecified = true;

                    /* Indicates whether or not the terminal has the capability to capture the card data. */
                    cmnGrp.CardCaptCap = CardCaptCapType.Item1;
                    cmnGrp.CardCaptCapSpecified = true;

                    /* Indicates Group ID. */
                    cmnGrp.GroupID = transactionRequest.GroupId; //Group ID value will be assigned by First Data.  //This is dummy value. Please use the actual value

                    _debitReq.CommonGrp = cmnGrp;
                    #endregion

                    #region TAgrp
                    TAGrp taGrp = new TAGrp();
                    taGrp.SctyLvl = SctyLvlType.EncrptTknizatn;
                    taGrp.SctyLvlSpecified = true;
                    taGrp.EncrptType = EncrptTypeType.RSA;
                    taGrp.EncrptTypeSpecified = true;
                    taGrp.EncrptTrgt = EncrptTrgtType.Track2;
                    taGrp.EncrptTrgtSpecified = true;
                    taGrp.KeyID = transactionRequest.KeyId; //"VAR11;TEST1";
                    taGrp.EncrptBlock = transactionRequest.Track3Data;
                    taGrp.TknType = transactionRequest.TokenType;
                    _debitReq.TAGrp = taGrp;
                    #endregion

                    #region PIN Group

                    PINGrp pinGroup = new PINGrp();
                    pinGroup.PINData = transactionRequest.DebitPin;
                    pinGroup.KeySerialNumData = transactionRequest.KeySerialNumber;
                    _debitReq.PINGrp = pinGroup;
                    #endregion

                    #region emv group
                    EMVGrp emvGrp = new EMVGrp();
                    emvGrp.EMVData = transactionRequest.EmvData;
                    emvGrp.CardSeqNum = transactionRequest.CardSeq;
                    _creditReq.EMVGrp = emvGrp;
                    #endregion

                    #region Additional Amount Group

                    if (string.Equals(cmnGrp.TxnType.ToString(), Sale, StringComparison.OrdinalIgnoreCase))
                    {
                        /*  Populate values for Additional Amount Group */
                        AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();

                        /* An identifier used to indicate whether or not the 
                         * terminal/software can support partial authorization approvals.  */
                        addAmtGrp.PartAuthrztnApprvlCapablt = PartAuthrztnApprvlCapabltType.Item1;
                        addAmtGrp.PartAuthrztnApprvlCapabltSpecified = true;

                        /* Creating a generic array of Additional Amount 
                         * Group type to sent the data to as an array */
                        AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[1];
                        addAmtGrpArr[0] = addAmtGrp;

                        _debitReq.AddtlAmtGrp = addAmtGrpArr;
                    }

                    #endregion

                    /* Add the data populated object to GMF message variant object */
                    _gmfMsgVar.Item = _debitReq;
                    transactionRequest.Stan = cmnGrp.STAN;
                    transactionRequest.TransRefNo = cmnGrp.RefNum;
                    transactionRequest.TransOrderNo = cmnGrp.OrderNum;
                    return _debitReq;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :   Creating fallback MSR for credit
        /// Function Name       :   CreateFallbackMsrRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   08/4/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :    "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        private CreditRequestDetails CreateFallbackMsrRequest(TransactionRequest transactionRequest)
        {
            /* Based on the GMF Specification UMF_Schema_V1.1.14.xsd, fields that are mandatory or related to 
                   this transaction should be populated.*/
            try
            {
                if (!transactionRequest.IsNull())
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                                          MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                        " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    MTDTransactionDto mtdTransaction = new MTDTransactionDto();
                    if (!string.IsNullOrEmpty(transactionRequest.RapidConnectAuthId))
                    {
                        mtdTransaction = _terminalService.Completion(
                           transactionRequest.RapidConnectAuthId,
                      transactionRequest.SerialNumber, transactionRequest.MtdMerchantId);
                        if (mtdTransaction.IsNull())
                            return null;
                    }

                    #region Common Group

                    CommonGrp cmnGrp = new CommonGrp();

                    /* The payment type of the transaction. */
                    PymtTypeType typePayment = transactionRequest.PaymentType;
                    cmnGrp.PymtType = typePayment;
                    cmnGrp.PymtTypeSpecified = true;
                    /* merchant category code. */
                    cmnGrp.MerchCatCode = transactionRequest.MCCode;
                    /* The type of transaction being performed. */
                    cmnGrp.TxnType = transactionRequest.TransType;
                    cmnGrp.TxnTypeSpecified = true;
                    /* The local date and time in which the transaction was performed. */
                    cmnGrp.LocalDateTime = transactionRequest.LocalDateTime;//DateTime.Now.ToString("yyyyMMddHHmmss");
                    /* The transmission date and time of the transaction (in GMT/UCT). */
                    cmnGrp.TrnmsnDateTime = transactionRequest.TrnmsnDateTime;//DateTime.UtcNow.ToString("yyyyMMddHHmmss");
                    /* A number assigned by the merchant to uniquely reference the transaction. 
                     * This number must be unique within a day per Merchant ID per Terminal ID. */
                    cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* A number assigned by the merchant to uniquely reference a set of transactions. */
                    //cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MerchantID, transactionRequest.TerminalID);//"480061115979";
                    cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);
                    //"480061115979";


                    /* A number assigned by the merchant to uniquely reference a transaction order sequence. */
                    cmnGrp.OrderNum = cmnGrp.RefNum; //"162091864975";


                    /* An ID assigned by First Data, for the Third Party Processor or 
                     * Software Vendor that generated the transaction. */
                    cmnGrp.TPPID = transactionRequest.TppId;

                    /* A unique ID assigned to a terminal. */
                    cmnGrp.TermID = transactionRequest.TerminalId;

                    //get terminal id from transaction request

                    /* A unique ID assigned by First Data, to identify the Merchant. */
                    cmnGrp.MerchID = transactionRequest.FdMerchantId;

                    cmnGrp.GroupID = transactionRequest.GroupId;
                    //Group ID value will be assigned by First Data. 
                    //get terminal id from transaction request
                    cmnGrp.POSEntryMode = PaymentAPIResources.FSwipePosEntryMode;
                    cmnGrp.POSCondCode = POSCondCodeType.Item00;
                    cmnGrp.POSCondCodeSpecified = true;
                    cmnGrp.TermCatCode = TermCatCodeType.Item12;
                    cmnGrp.TermCatCodeSpecified = true;
                    cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item04;
                    cmnGrp.TermEntryCapabltSpecified = true;
                    cmnGrp.TermLocInd = TermLocIndType.Item0;
                    cmnGrp.TermLocIndSpecified = true;
                    cmnGrp.CardCaptCap = CardCaptCapType.Item1;
                    cmnGrp.CardCaptCapSpecified = true;

                    /* The amount of the transaction. This may be an authorization amount, 
                     * adjustment amount or a reversal amount based on the type of transaction. 
                     * It is inclusive of all additional amounts. 
                     * It is submitted in the currency represented by the Transaction Currency field.  
                     * The field is overwritten in the response for a partial authorization. */

                    double num = Convert.ToDouble(transactionRequest.TransactionAmount);
                    string amt = num.ToString("0.00").Replace(".", string.Empty);
                    cmnGrp.TxnAmt = Convert.ToString(amt);
                    cmnGrp.TxnAmt = cmnGrp.TxnAmt.PadLeft(12, '0');

                    /* The numeric currency of the Transaction Amount. */
                    cmnGrp.TxnCrncy = PaymentAPIResources.Cur_USA;

                    /* An indicator that describes the location of the terminal. */
                    /* Indicates Group ID. */
                    _creditReq.CommonGrp = cmnGrp;
                    #endregion

                    CardGrp cardGrp = new CardGrp();
                    #region Transarmor group
                    if (string.Equals(cmnGrp.TxnType.ToString(), Authorization, StringComparison.CurrentCultureIgnoreCase) || string.Equals(cmnGrp.TxnType.ToString(), Sale, StringComparison.CurrentCultureIgnoreCase))
                    {

                        cardGrp.CardType = transactionRequest.CardType;
                        cardGrp.CardTypeSpecified = true;
                        _creditReq.CardGrp = cardGrp;
                        //For RSA
                        TAGrp taGrp = new TAGrp();
                        taGrp.SctyLvl = SctyLvlType.EncrptTknizatn;
                        taGrp.SctyLvlSpecified = true;
                        taGrp.EncrptType = EncrptTypeType.RSA;
                        taGrp.EncrptTypeSpecified = true;
                        taGrp.EncrptTrgt = EncrptTrgtType.Track2;
                        taGrp.EncrptTrgtSpecified = true;
                        taGrp.KeyID = transactionRequest.KeyId;
                        taGrp.EncrptBlock = transactionRequest.Track3Data;
                        taGrp.TknType = transactionRequest.TokenType;
                        _creditReq.TAGrp = taGrp;

                    }
                    if (string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.CurrentCultureIgnoreCase) || string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (mtdTransaction != null) cardGrp.CardType = (CardTypeType)Enum.Parse(typeof(CardTypeType), mtdTransaction.CardType);//mtdTransaction.CardType;
                        transactionRequest.CardType = cardGrp.CardType;
                        cardGrp.CardTypeSpecified = true;
                        _creditReq.CardGrp = cardGrp;

                        TAGrp taGrp = new TAGrp();
                        taGrp.SctyLvl = SctyLvlType.EncrptTknizatn;
                        taGrp.SctyLvlSpecified = true;
                        taGrp.Tkn = mtdTransaction.CardToken.Decrypt();
                        taGrp.TknType = transactionRequest.TokenType;
                        _creditReq.TAGrp = taGrp;
                    }
                    #endregion
                    if (!string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase) && !string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.OrdinalIgnoreCase))
                    {
                        #region Additional Amount Group
                        AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();
                        addAmtGrp.PartAuthrztnApprvlCapablt = PartAuthrztnApprvlCapabltType.Item1;
                        addAmtGrp.PartAuthrztnApprvlCapabltSpecified = true;
                        AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[1];
                        addAmtGrpArr[0] = addAmtGrp;
                        _creditReq.AddtlAmtGrp = addAmtGrpArr;
                        #endregion
                    }

                    #region Visa Group
                    if (string.Equals(cardGrp.CardType.ToString(), Visa, StringComparison.OrdinalIgnoreCase) && !string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.OrdinalIgnoreCase))
                    {
                        VisaGrp visaGrp = new VisaGrp();
                        visaGrp.VisaBID = PaymentAPIResources.VisaBID;
                        visaGrp.VisaAUAR = PaymentAPIResources.VisaAUAR;
                        if (!string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                        {
                            visaGrp.ACI = ACIType.Y;
                            visaGrp.ACISpecified = true;
                            visaGrp.TaxAmtCapablt = TaxAmtCapabltType.Item1;
                            visaGrp.TaxAmtCapabltSpecified = true;
                        }
                        else
                        {
                            visaGrp.ACI = ACIType.T;
                            visaGrp.ACISpecified = true;
                        }
                        _creditReq.Item = visaGrp;
                    }
                    #endregion

                    #region AmexGrp Group
                    if (string.Equals(cardGrp.CardType.ToString(), Amex, StringComparison.OrdinalIgnoreCase) && string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                    {
                        AmexGrp amexgrp = new AmexGrp();
                        if (mtdTransaction != null) amexgrp.AmExTranID = mtdTransaction.GatewayTxnId;
                        _creditReq.Item = amexgrp;
                    }
                    #endregion

                    #region Discover Group
                    if ((string.Equals(cardGrp.CardType.ToString(), Diners, StringComparison.OrdinalIgnoreCase) || string.Equals(cardGrp.CardType.ToString(), Discover, StringComparison.OrdinalIgnoreCase) || string.Equals(cardGrp.CardType.ToString(), "JCB", StringComparison.OrdinalIgnoreCase)) && string.Equals(cmnGrp.TxnType.ToString(), "completion", StringComparison.OrdinalIgnoreCase))
                    {
                        DSGrp dsGrp = new DSGrp();
                        if (mtdTransaction != null) dsGrp.DiscNRID = mtdTransaction.GatewayTxnId;
                        _creditReq.Item = dsGrp;
                    }
                    #endregion

                    #region Customer info Group
                    if (!string.IsNullOrEmpty(transactionRequest.StreetAddress) || !string.IsNullOrEmpty(transactionRequest.ZipCode))
                    {
                        if (!string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.OrdinalIgnoreCase) && !string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                        {
                            CustInfoGrp custinfo = new CustInfoGrp();
                            if (!string.IsNullOrEmpty(transactionRequest.StreetAddress))
                            {
                                custinfo.AVSBillingAddr = transactionRequest.StreetAddress;
                            }
                            if (!string.IsNullOrEmpty(transactionRequest.ZipCode))
                            {
                                custinfo.AVSBillingPostalCode = transactionRequest.ZipCode;
                            }
                            _creditReq.CustInfoGrp = custinfo;
                        }
                    }
                    #endregion

                    #region addAm Group

                    if (string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                    {
                        AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();
                        double amt1 = Convert.ToDouble(mtdTransaction.Amount);
                        string addamt = amt1.ToString("0.00").Replace(".", string.Empty);
                        addAmtGrp.AddAmt = addamt;
                        addAmtGrp.AddAmt = addAmtGrp.AddAmt.PadLeft(12, '0');
                        addAmtGrp.AddAmtCrncy = PaymentAPIResources.Cur_USA;
                        addAmtGrp.AddAmtType = AddAmtTypeType.FirstAuthAmt;
                        addAmtGrp.AddAmtTypeSpecified = true;

                        AddtlAmtGrp addAmtGrp1 = new AddtlAmtGrp();
                        addAmtGrp1.AddAmt = addamt;
                        addAmtGrp1.AddAmt = addAmtGrp1.AddAmt.PadLeft(12, '0');
                        addAmtGrp1.AddAmtCrncy = PaymentAPIResources.Cur_USA;
                        addAmtGrp1.AddAmtType = AddAmtTypeType.TotalAuthAmt;
                        addAmtGrp1.AddAmtTypeSpecified = true;

                        AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[2];
                        addAmtGrpArr[0] = addAmtGrp;
                        addAmtGrpArr[1] = addAmtGrp1;
                        _creditReq.AddtlAmtGrp = addAmtGrpArr;

                        OrigAuthGrp orgAuthgrp = new OrigAuthGrp();
                        if (mtdTransaction != null)
                        {
                            orgAuthgrp.OrigAuthID = mtdTransaction.AuthId;

                            orgAuthgrp.OrigLocalDateTime = mtdTransaction.LocalDateTime;
                            orgAuthgrp.OrigTranDateTime = mtdTransaction.TransmissionDateTime;

                            orgAuthgrp.OrigSTAN = Convert.ToString(mtdTransaction.Stan);
                            orgAuthgrp.OrigRespCode = Convert.ToString(mtdTransaction.ResponseCode);
                        }
                        _creditReq.OrigAuthGrp = orgAuthgrp;

                    }
                    #endregion
                    /* Add the data populated object to GMF message variant object */
                    _gmfMsgVar.Item = _creditReq;
                    transactionRequest.Stan = cmnGrp.STAN;
                    transactionRequest.TransRefNo = cmnGrp.RefNum;
                    transactionRequest.TransOrderNo = cmnGrp.OrderNum;
                    return _creditReq;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;

        }

        /// <summary>
        /// Purpose             :   Creating fallback MSR for debit
        /// Function Name       :   CreateFallbackMsrDebitRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   08/18/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :    "MM/DD/YYYY"  
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        private DebitRequestDetails CreateFallbackMsrDebitRequest(TransactionRequest transactionRequest)
        {
            /* Based on the GMF Specification UMF_Schema_V1.1.14.xsd, fields that are mandatory or related to 
               this transaction should be populated.*/
            try
            {
                if (!transactionRequest.IsNull())
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                                          MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                        " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    MTDTransactionDto mtdTransaction = new MTDTransactionDto();
                    if (!string.IsNullOrEmpty(transactionRequest.RapidConnectAuthId))
                    {
                        mtdTransaction = _terminalService.Completion(
                           transactionRequest.RapidConnectAuthId,
                      transactionRequest.SerialNumber, transactionRequest.MtdMerchantId);
                        if (mtdTransaction.IsNull())
                            return null;
                    }

                    #region Common Group
                    /* Populate values for Common Group */
                    CommonGrp cmnGrp = new CommonGrp();

                    /* The payment type of the transaction. */
                    cmnGrp.PymtType = PymtTypeType.Debit;
                    cmnGrp.PymtTypeSpecified = true;
                    cmnGrp.MerchCatCode = transactionRequest.MCCode;
                    /* The type of transaction being performed. */
                    cmnGrp.TxnType = transactionRequest.TransactionType == Void ? TxnTypeType.Sale : transactionRequest.TransType;
                    cmnGrp.TxnTypeSpecified = true;

                    /* The local date and time in which the transaction was performed. */
                    cmnGrp.LocalDateTime = transactionRequest.LocalDateTime;//DateTime.Now.ToString("yyyyMMddHHmmss");
                    /* The transmission date and time of the transaction (in GMT/UCT). */
                    cmnGrp.TrnmsnDateTime = transactionRequest.TrnmsnDateTime;//DateTime.UtcNow.ToString("yyyyMMddHHmmss");

                    /* A number assigned by the merchant to uniquely reference the transaction. 
                     * This number must be unique within a day per Merchant ID per Terminal ID. */
                    cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* A number assigned by the merchant to uniquely reference a set of transactions. 
                     * sThis number must be unique within a day for a given Merchant ID/ Terminal ID. */
                    cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* Order number of the transaction*/
                    cmnGrp.OrderNum = cmnGrp.RefNum;

                    /* An ID assigned by First Data, for the Third Party Processor or 
                     * Software Vendor that generated the transaction. */
                    cmnGrp.TPPID = transactionRequest.TppId;

                    /* A unique ID assigned to a terminal. */
                    cmnGrp.TermID = transactionRequest.TerminalId;

                    /* A unique ID assigned by First Data, to identify the Merchant. */
                    cmnGrp.MerchID = transactionRequest.FdMerchantId;


                    /* An identifier used to indicate the terminal’s account number entry mode 
                     * and authentication capability via the Point-of-Service. */
                    cmnGrp.POSEntryMode = PaymentAPIResources.FSwipePosEntryMode;

                    /* An identifier used to indicate the authorization conditions at the Point-of-Service (POS). */
                    cmnGrp.POSCondCode = POSCondCodeType.Item00;
                    cmnGrp.POSCondCodeSpecified = true;

                    /* An identifier used to describe the type of terminal being used for the transaction. */
                    cmnGrp.TermCatCode = TermCatCodeType.Item12;
                    cmnGrp.TermCatCodeSpecified = true;

                    /* An identifier used to indicate the entry mode capability of the terminal. */
                    cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item04;
                    cmnGrp.TermEntryCapabltSpecified = true;

                    /* The amount of the transaction. This may be an authorization amount, 
                     * adjustment amount or a reversal amount based on the type of transaction. 
                     * It is inclusive of all additional amounts. 
                     * It is submitted in the currency represented by the Transaction Currency field.  
                     * The field is overwritten in the response for a partial authorization. */

                    double num = Convert.ToDouble(transactionRequest.TransactionAmount);
                    string amt = num.ToString("0.00").Replace(".", string.Empty);
                    cmnGrp.TxnAmt = Convert.ToString(amt);
                    cmnGrp.TxnAmt = cmnGrp.TxnAmt.PadLeft(12, '0');

                    /* The numeric currency of the Transaction Amount. */
                    cmnGrp.TxnCrncy = PaymentAPIResources.Cur_USA;

                    /* An indicator that describes the location of the terminal. */
                    cmnGrp.TermLocInd = TermLocIndType.Item0;
                    cmnGrp.TermLocIndSpecified = true;

                    /* Indicates whether or not the terminal has the capability to capture the card data. */
                    cmnGrp.CardCaptCap = CardCaptCapType.Item1;
                    cmnGrp.CardCaptCapSpecified = true;

                    /* Indicates Group ID. */
                    cmnGrp.GroupID = transactionRequest.GroupId; //Group ID value will be assigned by First Data.

                    _debitReq.CommonGrp = cmnGrp;
                    #endregion

                    #region TAgrp
                    if (string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                    {
                        TAGrp taGrp = new TAGrp();
                        taGrp.SctyLvl = SctyLvlType.EncrptTknizatn;
                        taGrp.SctyLvlSpecified = true;
                        taGrp.Tkn = StringExtension.Decrypt(mtdTransaction.CardToken);
                        taGrp.TknType = transactionRequest.TokenType;
                        _debitReq.TAGrp = taGrp;
                    }
                    else
                    {
                        TAGrp taGrp = new TAGrp();
                        taGrp.SctyLvl = SctyLvlType.EncrptTknizatn;
                        taGrp.SctyLvlSpecified = true;
                        taGrp.EncrptType = EncrptTypeType.RSA;
                        taGrp.EncrptTypeSpecified = true;
                        taGrp.EncrptTrgt = EncrptTrgtType.Track2;
                        taGrp.EncrptTrgtSpecified = true;
                        taGrp.KeyID = transactionRequest.KeyId;
                        taGrp.EncrptBlock = transactionRequest.Track3Data;
                        taGrp.TknType = transactionRequest.TokenType;//PaymentAPIResources.TknType;//"1173";
                        _debitReq.TAGrp = taGrp;

                        #region PIN Group
                        /* Populate values for PIN Group */
                        PINGrp pinGroup = new PINGrp();

                        /* The PIN Data for the Debit or EBT transaction being submitted.
                         * HEXADecimal value need to be entered. */
                        pinGroup.PINData = transactionRequest.DebitPin;
                        //pinGroup.PINData = transactionRequest.PinData; // remove hard code
                        /* Provides the initialization vector for DUKPT PIN Debit and EBT transactions. */
                        pinGroup.KeySerialNumData = transactionRequest.KeySerialNumber; // remove hard code
                        //pinGroup.KeySerialNumData = transactionRequest.KeySerNumData;

                        _debitReq.PINGrp = pinGroup;
                        #endregion
                    }
                    #endregion


                    #region Additional Amount Group

                    if (string.Equals(cmnGrp.TxnType.ToString(), Sale, StringComparison.OrdinalIgnoreCase) || string.Equals(cmnGrp.TxnType.ToString(), Authorization, StringComparison.OrdinalIgnoreCase))
                    {
                        /*  Populate values for Additional Amount Group */
                        AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();

                        /* An identifier used to indicate whether or not the 
                         * terminal/software can support partial authorization approvals.  */
                        addAmtGrp.PartAuthrztnApprvlCapablt = PartAuthrztnApprvlCapabltType.Item1;
                        addAmtGrp.PartAuthrztnApprvlCapabltSpecified = true;

                        /* Creating a generic array of Additional Amount 
                         * Group type to sent the data to as an array */
                        AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[1];
                        addAmtGrpArr[0] = addAmtGrp;

                        _debitReq.AddtlAmtGrp = addAmtGrpArr;
                    }

                    #endregion

                    #region addAm Group

                    if (string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                    {
                        AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();
                        double amt1 = Convert.ToDouble(mtdTransaction.Amount);
                        string addamt = amt1.ToString("0.00").Replace(".", string.Empty);
                        addAmtGrp.AddAmt = addamt;
                        addAmtGrp.AddAmt = addAmtGrp.AddAmt.PadLeft(12, '0');
                        addAmtGrp.AddAmtCrncy = PaymentAPIResources.Cur_USA;
                        addAmtGrp.AddAmtType = AddAmtTypeType.FirstAuthAmt;
                        addAmtGrp.AddAmtTypeSpecified = true;

                        AddtlAmtGrp addAmtGrp1 = new AddtlAmtGrp();
                        addAmtGrp1.AddAmt = addamt;
                        addAmtGrp1.AddAmt = addAmtGrp1.AddAmt.PadLeft(12, '0');
                        addAmtGrp1.AddAmtCrncy = PaymentAPIResources.Cur_USA;
                        addAmtGrp1.AddAmtType = AddAmtTypeType.TotalAuthAmt;
                        addAmtGrp1.AddAmtTypeSpecified = true;

                        AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[2];
                        addAmtGrpArr[0] = addAmtGrp;
                        addAmtGrpArr[1] = addAmtGrp1;
                        _debitReq.AddtlAmtGrp = addAmtGrpArr;

                        OrigAuthGrp orgAuthgrp = new OrigAuthGrp();
                        if (mtdTransaction != null)
                        {
                            orgAuthgrp.OrigAuthID = mtdTransaction.AuthId; // "OK3542";

                            orgAuthgrp.OrigLocalDateTime = mtdTransaction.LocalDateTime;
                            orgAuthgrp.OrigTranDateTime = mtdTransaction.TransmissionDateTime;

                            orgAuthgrp.OrigSTAN = Convert.ToString(mtdTransaction.Stan); //"000001";
                            orgAuthgrp.OrigRespCode = Convert.ToString(mtdTransaction.ResponseCode); //"002";
                        }
                        _debitReq.OrigAuthGrp = orgAuthgrp;

                    }
                    #endregion
                    /* Add the data populated object to GMF message variant object */
                    _gmfMsgVar.Item = _debitReq;
                    transactionRequest.Stan = cmnGrp.STAN;
                    transactionRequest.TransRefNo = cmnGrp.RefNum;
                    transactionRequest.TransOrderNo = cmnGrp.OrderNum;
                    return _debitReq;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :   Creating Reversal Void transaction request
        /// Function Name       :   ReversalRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   03/20/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        public VoidTOReversalRequestDetails ReversalRequest(TransactionRequest transactionRequest)
        {
            try
            {
                if (!transactionRequest.IsNull())
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                                          MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                        " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    #region Common Group
                    MTDTransactionDto mtdTransaction = new MTDTransactionDto();
                    if (!string.IsNullOrEmpty(transactionRequest.RapidConnectAuthId))
                    {
                        mtdTransaction = _terminalService.Completion(
                           transactionRequest.RapidConnectAuthId,
                       transactionRequest.SerialNumber, transactionRequest.MtdMerchantId);
                        if (mtdTransaction.IsNull())
                            return null;
                    }
                    /* Populate values for Common Group */
                    CommonGrp cmnGrp = new CommonGrp();

                    /* The payment type of the transaction. */
                    PymtTypeType typePayment = transactionRequest.PaymentType;
                    cmnGrp.PymtType = typePayment;
                    cmnGrp.PymtTypeSpecified = true;
                    cmnGrp.ReversalInd = transactionRequest.TransactionType == Void ? ReversalIndType.Void : ReversalIndType.Timeout;
                    cmnGrp.ReversalIndSpecified = true;
                    cmnGrp.MerchCatCode = transactionRequest.MCCode;
                    MTDTransactionDto mtdVoidTransaction = new MTDTransactionDto();

                    if (mtdTransaction != null && mtdTransaction.TxnType == Sale)
                        cmnGrp.TxnType = TxnTypeType.Sale;
                    if (mtdTransaction != null && mtdTransaction.TxnType == Refund)
                        cmnGrp.TxnType = TxnTypeType.Refund;
                    if (mtdTransaction != null && mtdTransaction.TxnType == Authorization)
                        cmnGrp.TxnType = TxnTypeType.Authorization;
                    if (mtdTransaction != null && mtdTransaction.TxnType == Completion)
                        cmnGrp.TxnType = TxnTypeType.Completion;
                    if (mtdTransaction != null && mtdTransaction.TxnType == Void)
                    {
                        mtdVoidTransaction = _terminalService.Completion(Convert.ToString(mtdTransaction.Id), transactionRequest.SerialNumber, transactionRequest.MtdMerchantId);
                        cmnGrp.TxnType = (TxnTypeType)Enum.Parse(typeof(TxnTypeType), mtdVoidTransaction.TxnType);
                    }
                    cmnGrp.TxnTypeSpecified = true;

                    /* The local date and time in which the transaction was performed. */
                    cmnGrp.LocalDateTime = transactionRequest.LocalDateTime;//DateTime.Now.ToString("yyyyMMddHHmmss");

                    /* The transmission date and time of the transaction (in GMT/UCT). */
                    cmnGrp.TrnmsnDateTime = transactionRequest.TrnmsnDateTime;//DateTime.UtcNow.ToString("yyyyMMddHHmmss");

                    /* A number assigned by the merchant to uniquely reference the transaction. 
                     * This number must be unique within a day per Merchant ID per Terminal ID. */
                    cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* A number assigned by the merchant to uniquely reference a set of transactions. */
                    cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);


                    /* A number assigned by the merchant to uniquely reference a transaction order sequence. */
                    cmnGrp.OrderNum = cmnGrp.RefNum;

                    /* An ID assigned by First Data, for the Third Party Processor or 
                     * Software Vendor that generated the transaction. */
                    cmnGrp.TPPID = transactionRequest.TppId;

                    /* A unique ID assigned to a terminal. */
                    cmnGrp.TermID = transactionRequest.TerminalId;

                    /* A unique ID assigned by First Data, to identify the Merchant. */
                    cmnGrp.MerchID = transactionRequest.FdMerchantId;

                    cmnGrp.POSEntryMode = PaymentAPIResources.EcomPosEntryMode;
                    cmnGrp.POSCondCode = POSCondCodeType.Item59;
                    cmnGrp.POSCondCodeSpecified = true;
                    cmnGrp.TermCatCode = TermCatCodeType.Item00;
                    cmnGrp.TermCatCodeSpecified = true;
                    cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item10;
                    cmnGrp.TermEntryCapabltSpecified = true;
                    cmnGrp.TermLocInd = TermLocIndType.Item1;
                    cmnGrp.TermLocIndSpecified = true;
                    cmnGrp.CardCaptCap = CardCaptCapType.Item0;
                    cmnGrp.CardCaptCapSpecified = true;

                    EcommGrp ecomgrp = new EcommGrp();
                    ecomgrp.EcommTxnInd = EcommTxnIndType.Item03;
                    ecomgrp.EcommTxnIndSpecified = true;
                    if (!string.Equals(transactionRequest.TransactionType, Void, StringComparison.CurrentCultureIgnoreCase) && !string.Equals(mtdTransaction.TxnType, Authorization, StringComparison.CurrentCultureIgnoreCase))
                    {
                        ecomgrp.EcommURL = PaymentAPIResources.EcomUrl;
                    }
                    _revDetails.EcommGrp = ecomgrp;

                    /* The amount of the transaction. This may be an authorization amount, 
                     * adjustment amount or a reversal amount based on the type of transaction. 
                     * It is inclusive of all additional amounts. 
                     * It is submitted in the currency represented by the Transaction Currency field.  
                     * The field is overwritten in the response for a partial authorization. */
                    double num = Convert.ToDouble(transactionRequest.TransactionAmount);
                    string amt = num.ToString("0.00").Replace(".", string.Empty);
                    cmnGrp.TxnAmt = Convert.ToString(amt);
                    cmnGrp.TxnAmt = cmnGrp.TxnAmt.PadLeft(12, '0');

                    /* The numeric currency of the Transaction Amount. */
                    cmnGrp.TxnCrncy = PaymentAPIResources.Cur_USA;

                    /* An indicator that describes the location of the terminal. */


                    /* Indicates Group ID. */
                    cmnGrp.GroupID = transactionRequest.GroupId;
                    _revDetails.CommonGrp = cmnGrp;
                    #endregion
                    /* Populate values for Card Group */
                    #region Card Group
                    CardGrp cardGrp = new CardGrp();
                    TAGrp taGrp = new TAGrp();
                    if (transactionRequest.TransactionType != Void && (mtdTransaction.TxnType == Sale || mtdTransaction.TxnType == Authorization))
                    {

                        cardGrp.AcctNum = transactionRequest.CardNumber;
                    }
                    else
                    {
                        taGrp.Tkn = mtdTransaction.CardToken.Decrypt();
                    }

                    cardGrp.CardExpiryDate = transactionRequest.ExpiryDate;
                    cardGrp.CardType = transactionRequest.CardType;
                    cardGrp.CardTypeSpecified = true;


                    taGrp.SctyLvl = SctyLvlType.Tknizatn;
                    taGrp.SctyLvlSpecified = true;
                    taGrp.TknType = transactionRequest.TokenType;
                    _revDetails.TAGrp = taGrp;

                    _revDetails.CardGrp = cardGrp;
                    #endregion

                    #region Additional Amount Group
                    AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();
                    addAmtGrp.AddAmt = Convert.ToString(amt);
                    addAmtGrp.AddAmt = addAmtGrp.AddAmt.PadLeft(12, '0');
                    addAmtGrp.AddAmtCrncy = PaymentAPIResources.Cur_USA;
                    addAmtGrp.AddAmtType = AddAmtTypeType.TotalAuthAmt;
                    addAmtGrp.AddAmtTypeSpecified = true;
                    if (transactionRequest.TransactionType != Void)
                    {
                        addAmtGrp.PartAuthrztnApprvlCapablt = PartAuthrztnApprvlCapabltType.Item1;
                        addAmtGrp.PartAuthrztnApprvlCapabltSpecified = true;
                    }
                    AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[1];
                    addAmtGrpArr[0] = addAmtGrp;
                    _revDetails.AddtlAmtGrp = addAmtGrpArr;
                    #endregion

                    #region Discover Group
                    if (cardGrp.CardType.ToString().Equals(Discover))
                    {
                        DSGrp dsGrp = new DSGrp();
                        if (transactionRequest.TransactionType == Void)
                        {
                            if (mtdTransaction != null) dsGrp.DiscNRID = mtdTransaction.GatewayTxnId;
                            _revDetails.Item = dsGrp;
                        }
                    }
                    #endregion

                    #region Visa Group
                    if (cardGrp.CardType.ToString().Equals(Visa))
                    {
                        VisaGrp visaGrp = new VisaGrp();
                        visaGrp.ACI = !string.Equals(transactionRequest.TransactionType, Void,
                            StringComparison.CurrentCultureIgnoreCase) ? ACIType.Y : ACIType.T;
                        visaGrp.ACISpecified = true;
                        _revDetails.Item = visaGrp;
                    }
                    #endregion

                    //  this group required for visa completion trax
                    #region originalAuth Group

                    OrigAuthGrp orgAuthgrp = new OrigAuthGrp();
                    if (mtdTransaction != null)
                    {
                        switch (transactionRequest.TransactionType)
                        {
                            case Void:
                                orgAuthgrp.OrigAuthID = mtdTransaction.AuthId;
                                orgAuthgrp.OrigRespCode = mtdTransaction.ResponseCode;
                                break;
                        }
                        switch (mtdTransaction.TxnType)
                        {
                            case Void:
                                orgAuthgrp.OrigAuthID = mtdVoidTransaction.AuthId;
                                orgAuthgrp.OrigRespCode = mtdVoidTransaction.ResponseCode;
                                break;
                        }

                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(mtdTransaction.RequestXml);
                        var localDateTime = xmlDoc.SelectSingleNode("TransactionRequest/LocalDateTime");
                        if (localDateTime != null)
                        {
                            orgAuthgrp.OrigLocalDateTime = localDateTime.InnerText;

                        }
                        var trnmsnDateTime = xmlDoc.SelectSingleNode("TransactionRequest/TrnmsnDateTime");
                        if (trnmsnDateTime != null)
                        {
                            orgAuthgrp.OrigTranDateTime = trnmsnDateTime.InnerText;

                        }

                        orgAuthgrp.OrigSTAN = mtdTransaction.Stan;
                    }
                    _revDetails.OrigAuthGrp = orgAuthgrp;


                    #endregion

                    /* Add the data populated object to GMF message variant object */
                    _gmfMsgVar.Item = _revDetails;
                    transactionRequest.Stan = cmnGrp.STAN;
                    transactionRequest.TransRefNo = cmnGrp.RefNum;
                    transactionRequest.TransOrderNo = cmnGrp.OrderNum;
                    return _revDetails;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :   Creating Reversal Void transaction request
        /// Function Name       :   ReversalRequestMsr
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   03/20/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        public VoidTOReversalRequestDetails ReversalRequestMsr(TransactionRequest transactionRequest)
        {
            try
            {
                if (!transactionRequest.IsNull())
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                                          MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                        " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    #region Common Group
                    MTDTransactionDto mtdTransaction = new MTDTransactionDto();
                    if (!string.IsNullOrEmpty(transactionRequest.RapidConnectAuthId))
                    {
                        mtdTransaction = _terminalService.Completion(
                           transactionRequest.RapidConnectAuthId,
                       transactionRequest.SerialNumber, transactionRequest.MtdMerchantId);
                        if (mtdTransaction.IsNull())
                            return null;
                    }
                    /* Populate values for Common Group */
                    CommonGrp cmnGrp = new CommonGrp();

                    /* The payment type of the transaction. */
                    PymtTypeType typePayment = transactionRequest.PaymentType;
                    cmnGrp.PymtType = typePayment;
                    cmnGrp.PymtTypeSpecified = true;
                    cmnGrp.ReversalInd = transactionRequest.TransactionType == Void ? ReversalIndType.Void : ReversalIndType.Timeout;
                    cmnGrp.ReversalIndSpecified = true;
                    cmnGrp.MerchCatCode = transactionRequest.MCCode;
                    MTDTransactionDto mtdVoidTransaction = new MTDTransactionDto();
                    //cmnGrp.MerchCatCode = "5965";
                    /* The type of transaction being performed. */
                    if (mtdTransaction != null && mtdTransaction.TxnType == Sale)
                        cmnGrp.TxnType = TxnTypeType.Sale;
                    if (mtdTransaction != null && mtdTransaction.TxnType == Refund)
                        cmnGrp.TxnType = TxnTypeType.Refund;
                    if (mtdTransaction != null && mtdTransaction.TxnType == Authorization)
                        cmnGrp.TxnType = TxnTypeType.Authorization;
                    if (mtdTransaction != null && mtdTransaction.TxnType == Completion)
                        cmnGrp.TxnType = TxnTypeType.Completion;
                    if (mtdTransaction != null && mtdTransaction.TxnType == Void)
                    {
                        mtdVoidTransaction = _terminalService.Completion(Convert.ToString(mtdTransaction.Id), transactionRequest.SerialNumber, transactionRequest.MtdMerchantId);
                        cmnGrp.TxnType = (TxnTypeType)Enum.Parse(typeof(TxnTypeType), mtdVoidTransaction.TxnType);
                    }
                    cmnGrp.TxnTypeSpecified = true;

                    /* The local date and time in which the transaction was performed. */
                    cmnGrp.LocalDateTime = transactionRequest.LocalDateTime;//DateTime.Now.ToString("yyyyMMddHHmmss");

                    /* The transmission date and time of the transaction (in GMT/UCT). */
                    cmnGrp.TrnmsnDateTime = transactionRequest.TrnmsnDateTime;//DateTime.UtcNow.ToString("yyyyMMddHHmmss");

                    /* A number assigned by the merchant to uniquely reference the transaction. 
                     * This number must be unique within a day per Merchant ID per Terminal ID. */
                    cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* A number assigned by the merchant to uniquely reference a set of transactions. */
                    cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);
                    //"480061115979";

                    /* A number assigned by the merchant to uniquely reference a transaction order sequence. */
                    cmnGrp.OrderNum = cmnGrp.RefNum; //"162091864975";

                    /* An ID assigned by First Data, for the Third Party Processor or 
                     * Software Vendor that generated the transaction. */
                    cmnGrp.TPPID = transactionRequest.TppId;

                    /* A unique ID assigned to a terminal. */
                    cmnGrp.TermID = transactionRequest.TerminalId;

                    /* A unique ID assigned by First Data, to identify the Merchant. */
                    cmnGrp.MerchID = transactionRequest.FdMerchantId;

                    cmnGrp.POSEntryMode = PaymentAPIResources.RetailPosEntryMode;
                    cmnGrp.POSCondCode = POSCondCodeType.Item00;
                    cmnGrp.POSCondCodeSpecified = true;
                    cmnGrp.TermCatCode = TermCatCodeType.Item00;
                    cmnGrp.TermCatCodeSpecified = true;
                    cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item10;
                    cmnGrp.TermEntryCapabltSpecified = true;
                    cmnGrp.TermLocInd = TermLocIndType.Item1;
                    cmnGrp.TermLocIndSpecified = true;
                    cmnGrp.CardCaptCap = CardCaptCapType.Item0;
                    cmnGrp.CardCaptCapSpecified = true;

                    /* The amount of the transaction. This may be an authorization amount, 
                     * adjustment amount or a reversal amount based on the type of transaction. 
                     * It is inclusive of all additional amounts. 
                     * It is submitted in the currency represented by the Transaction Currency field.  
                     * The field is overwritten in the response for a partial authorization. */
                    double num = Convert.ToDouble(transactionRequest.TransactionAmount);
                    string amt = num.ToString("0.00").Replace(".", string.Empty);
                    cmnGrp.TxnAmt = Convert.ToString(amt);
                    cmnGrp.TxnAmt = cmnGrp.TxnAmt.PadLeft(12, '0');

                    /* The numeric currency of the Transaction Amount. */
                    cmnGrp.TxnCrncy = PaymentAPIResources.Cur_USA;

                    /* An indicator that describes the location of the terminal. */


                    /* Indicates Group ID. */
                    cmnGrp.GroupID = transactionRequest.GroupId;
                    _revDetails.CommonGrp = cmnGrp;
                    #endregion
                    /* Populate values for Card Group */
                    #region Card Group
                    CardGrp cardGrp = new CardGrp();
                    TAGrp taGrp = new TAGrp();
                    if (transactionRequest.TransactionType != Void && (mtdTransaction.TxnType == Sale || mtdTransaction.TxnType == Authorization))
                    {
                        taGrp.EncrptType = EncrptTypeType.RSA;
                        taGrp.EncrptTypeSpecified = true;
                        taGrp.EncrptTrgt = EncrptTrgtType.Track2;
                        taGrp.EncrptTrgtSpecified = true;
                        taGrp.KeyID = transactionRequest.KeyId;
                        taGrp.EncrptBlock = transactionRequest.Track3Data;


                    }
                    else
                    {
                        taGrp.Tkn = mtdTransaction.CardToken.Decrypt();
                    }

                    taGrp.SctyLvl = SctyLvlType.EncrptTknizatn;
                    taGrp.SctyLvlSpecified = true;
                    taGrp.TknType = transactionRequest.TokenType;
                    cardGrp.CardType = transactionRequest.CardType;
                    cardGrp.CardTypeSpecified = true;
                    _revDetails.TAGrp = taGrp;

                    _revDetails.CardGrp = cardGrp;
                    #endregion

                    #region Additional Amount Group
                    AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();
                    addAmtGrp.AddAmt = Convert.ToString(amt);
                    addAmtGrp.AddAmt = addAmtGrp.AddAmt.PadLeft(12, '0');
                    addAmtGrp.AddAmtCrncy = PaymentAPIResources.Cur_USA;
                    addAmtGrp.AddAmtType = AddAmtTypeType.TotalAuthAmt;
                    addAmtGrp.AddAmtTypeSpecified = true;
                    if (transactionRequest.TransactionType != Void)
                    {
                        addAmtGrp.PartAuthrztnApprvlCapablt = PartAuthrztnApprvlCapabltType.Item1;
                        addAmtGrp.PartAuthrztnApprvlCapabltSpecified = true;
                    }
                    AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[1];
                    addAmtGrpArr[0] = addAmtGrp;
                    _revDetails.AddtlAmtGrp = addAmtGrpArr;
                    #endregion

                    #region Discover Group
                    if (cardGrp.CardType.ToString().Equals(Discover))
                    {
                        DSGrp dsGrp = new DSGrp();
                        if (transactionRequest.TransactionType == Void)
                            if (mtdTransaction != null) dsGrp.DiscNRID = mtdTransaction.GatewayTxnId;
                        _revDetails.Item = dsGrp;
                    }
                    #endregion

                    #region Visa Group
                    if (cardGrp.CardType.ToString().Equals(Visa))
                    {
                        VisaGrp visaGrp = new VisaGrp();
                        visaGrp.ACI = !string.Equals(transactionRequest.TransactionType, Void,
                            StringComparison.CurrentCultureIgnoreCase) ? ACIType.Y : ACIType.T;
                        visaGrp.ACISpecified = true;
                        _revDetails.Item = visaGrp;
                    }
                    #endregion

                    //  this group required for visa completion trax
                    #region originalAuth Group

                    OrigAuthGrp orgAuthgrp = new OrigAuthGrp();
                    if (mtdTransaction != null)
                    {
                        switch (transactionRequest.TransactionType)
                        {
                            case Void:
                                orgAuthgrp.OrigAuthID = mtdTransaction.AuthId;
                                orgAuthgrp.OrigRespCode = mtdTransaction.ResponseCode;
                                break;
                        }
                        switch (mtdTransaction.TxnType)
                        {
                            case Void:
                                orgAuthgrp.OrigAuthID = mtdVoidTransaction.AuthId;
                                orgAuthgrp.OrigRespCode = mtdVoidTransaction.ResponseCode;
                                break;
                        }

                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(mtdTransaction.RequestXml);
                        var localDateTime = xmlDoc.SelectSingleNode("TransactionRequest/LocalDateTime");
                        if (localDateTime != null)
                        {
                            orgAuthgrp.OrigLocalDateTime = localDateTime.InnerText;

                        }
                        var trnmsnDateTime = xmlDoc.SelectSingleNode("TransactionRequest/TrnmsnDateTime");
                        if (trnmsnDateTime != null)
                        {
                            orgAuthgrp.OrigTranDateTime = trnmsnDateTime.InnerText;

                        }

                        orgAuthgrp.OrigSTAN = mtdTransaction.Stan;
                    }
                    _revDetails.OrigAuthGrp = orgAuthgrp;


                    #endregion

                    /* Add the data populated object to GMF message variant object */
                    _gmfMsgVar.Item = _revDetails;
                    transactionRequest.Stan = cmnGrp.STAN;
                    transactionRequest.TransRefNo = cmnGrp.RefNum;
                    transactionRequest.TransOrderNo = cmnGrp.OrderNum;
                    return _revDetails;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :   Creating Reversal Void and TOR transaction request
        /// Function Name       :   ReversalRequestEmv
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   11/24/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        public VoidTOReversalRequestDetails ReversalRequestEmv(TransactionRequest transactionRequest)
        {
            try
            {
                if (!transactionRequest.IsNull())
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                                          MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                        " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    #region Common Group
                    MTDTransactionDto mtdTransaction = new MTDTransactionDto();
                    if (!string.IsNullOrEmpty(transactionRequest.RapidConnectAuthId))
                    {
                        mtdTransaction = _terminalService.Completion(
                           transactionRequest.RapidConnectAuthId,
                       transactionRequest.SerialNumber, transactionRequest.MtdMerchantId);
                        if (mtdTransaction.IsNull())
                            return null;
                    }
                    /* Populate values for Common Group */
                    CommonGrp cmnGrp = new CommonGrp();

                    /* The payment type of the transaction. */
                    PymtTypeType typePayment = transactionRequest.PaymentType;
                    cmnGrp.PymtType = typePayment;
                    cmnGrp.PymtTypeSpecified = true;
                    cmnGrp.ReversalInd = transactionRequest.TransactionType == Void ? ReversalIndType.Void : ReversalIndType.Timeout;
                    cmnGrp.ReversalIndSpecified = true;
                    cmnGrp.MerchCatCode = transactionRequest.MCCode;
                    MTDTransactionDto mtdVoidTransaction = new MTDTransactionDto();
                    //cmnGrp.MerchCatCode = "5965";
                    /* The type of transaction being performed. */
                    if (mtdTransaction != null && mtdTransaction.TxnType == Sale)
                        cmnGrp.TxnType = TxnTypeType.Sale;
                    if (mtdTransaction != null && mtdTransaction.TxnType == Refund)
                        cmnGrp.TxnType = TxnTypeType.Refund;
                    if (mtdTransaction != null && mtdTransaction.TxnType == Authorization)
                        cmnGrp.TxnType = TxnTypeType.Authorization;
                    if (mtdTransaction != null && mtdTransaction.TxnType == Completion)
                        cmnGrp.TxnType = TxnTypeType.Completion;
                    if (mtdTransaction != null && mtdTransaction.TxnType == Void)
                    {
                        mtdVoidTransaction = _terminalService.Completion(Convert.ToString(mtdTransaction.Id), transactionRequest.SerialNumber, transactionRequest.MtdMerchantId);
                        cmnGrp.TxnType = (TxnTypeType)Enum.Parse(typeof(TxnTypeType), mtdVoidTransaction.TxnType);
                    }
                    cmnGrp.TxnTypeSpecified = true;

                    /* The local date and time in which the transaction was performed. */
                    cmnGrp.LocalDateTime = transactionRequest.LocalDateTime;//DateTime.Now.ToString("yyyyMMddHHmmss");

                    /* The transmission date and time of the transaction (in GMT/UCT). */
                    cmnGrp.TrnmsnDateTime = transactionRequest.TrnmsnDateTime;//DateTime.UtcNow.ToString("yyyyMMddHHmmss");

                    /* A number assigned by the merchant to uniquely reference the transaction. 
                     * This number must be unique within a day per Merchant ID per Terminal ID. */
                    cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* A number assigned by the merchant to uniquely reference a set of transactions. */
                    cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);
                    //"480061115979";

                    /* A number assigned by the merchant to uniquely reference a transaction order sequence. */
                    cmnGrp.OrderNum = cmnGrp.RefNum; //"162091864975";

                    /* An ID assigned by First Data, for the Third Party Processor or 
                     * Software Vendor that generated the transaction. */
                    cmnGrp.TPPID = transactionRequest.TppId;

                    /* A unique ID assigned to a terminal. */
                    cmnGrp.TermID = transactionRequest.TerminalId;

                    /* A unique ID assigned by First Data, to identify the Merchant. */
                    cmnGrp.MerchID = transactionRequest.FdMerchantId;

                    cmnGrp.GroupID = transactionRequest.GroupId;
                    //Group ID value will be assigned by First Data. 
                    //get terminal id from transaction request
                    cmnGrp.POSEntryMode = PaymentAPIResources.RetailContactEMVPosEntryMode;
                    cmnGrp.POSCondCode = POSCondCodeType.Item00;
                    cmnGrp.POSCondCodeSpecified = true;
                    // cmnGrp.TermCatCode = TermCatCodeType.Item01;
                    cmnGrp.TermCatCode = TermCatCodeType.Item12;
                    cmnGrp.TermCatCodeSpecified = true;
                    cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item08;
                    cmnGrp.TermEntryCapabltSpecified = true;
                    cmnGrp.TermLocInd = TermLocIndType.Item0;
                    cmnGrp.TermLocIndSpecified = true;
                    cmnGrp.CardCaptCap = CardCaptCapType.Item1;
                    cmnGrp.CardCaptCapSpecified = true;


                    /* The amount of the transaction. This may be an authorization amount, 
                     * adjustment amount or a reversal amount based on the type of transaction. 
                     * It is inclusive of all additional amounts. 
                     * It is submitted in the currency represented by the Transaction Currency field.  
                     * The field is overwritten in the response for a partial authorization. */
                    double num = Convert.ToDouble(transactionRequest.TransactionAmount);
                    string amt = num.ToString("0.00").Replace(".", string.Empty);
                    cmnGrp.TxnAmt = Convert.ToString(amt);
                    cmnGrp.TxnAmt = cmnGrp.TxnAmt.PadLeft(12, '0');

                    /* The numeric currency of the Transaction Amount. */
                    cmnGrp.TxnCrncy = PaymentAPIResources.Cur_USA;

                    /* An indicator that describes the location of the terminal. */


                    /* Indicates Group ID. */
                    cmnGrp.GroupID = transactionRequest.GroupId;
                    _revDetails.CommonGrp = cmnGrp;
                    #endregion
                    /* Populate values for Card Group */
                    #region Card Group
                    CardGrp cardGrp = new CardGrp();
                    TAGrp taGrp = new TAGrp();
                    if (transactionRequest.TransactionType != Void && (mtdTransaction.TxnType == Sale || mtdTransaction.TxnType == Authorization))
                    {
                        taGrp.EncrptType = EncrptTypeType.RSA;
                        taGrp.EncrptTypeSpecified = true;
                        taGrp.EncrptTrgt = EncrptTrgtType.Track2;
                        taGrp.EncrptTrgtSpecified = true;
                        taGrp.KeyID = transactionRequest.KeyId;
                        taGrp.EncrptBlock = transactionRequest.Track3Data;

                    }
                    else
                    {
                        taGrp.Tkn = mtdTransaction.CardToken.Decrypt();
                    }


                    cardGrp.CardType = transactionRequest.CardType;
                    cardGrp.CardTypeSpecified = true;


                    taGrp.SctyLvl = SctyLvlType.EncrptTknizatn;
                    taGrp.SctyLvlSpecified = true;
                    taGrp.TknType = transactionRequest.TokenType;
                    _revDetails.TAGrp = taGrp;

                    _revDetails.CardGrp = cardGrp;
                    #endregion

                    #region Visa Group
                    if (cardGrp.CardType.ToString().Equals(Visa))
                    {
                        VisaGrp visaGrp = new VisaGrp();
                        if (transactionRequest.TransactionType != Void)
                        {
                            visaGrp.ACI = ACIType.Y;
                            visaGrp.ACISpecified = true;

                        }
                        else

                            if (mtdTransaction != null)
                            {
                                XmlDocument xmlDoc = new XmlDocument();
                                xmlDoc.LoadXml(mtdTransaction.ResponseXml);
                                string xmlString = CommonFunctions.GetXmlAsString(xmlDoc);
                                var response = CommonFunctions.GetMCResponse(xmlString);
                                xmlDoc.LoadXml(response);
                                var aCI = xmlDoc.SelectSingleNode("CreditResponse/VisaGrp/ACI");
                                if (aCI != null)
                                {
                                    visaGrp.ACI = (ACIType)Enum.Parse(typeof(ACIType), aCI.InnerText);
                                    visaGrp.ACISpecified = true;

                                }

                                var transID = xmlDoc.SelectSingleNode("CreditResponse/VisaGrp/TransID");
                                if (transID != null)
                                {
                                    visaGrp.TransID = transID.InnerText;

                                }

                            }
                        _revDetails.Item = visaGrp;
                    }

                    #endregion

                    #region MC Group
                    if (cardGrp.CardType.ToString().Equals(MasterCard))
                    {
                        MCGrp mcGrp = new MCGrp();
                        if (transactionRequest.TransactionType == Void)
                        {
                            if (mtdTransaction != null)
                            {
                                XmlDocument xmlDoc = new XmlDocument();

                                xmlDoc.LoadXml(mtdTransaction.ResponseXml);
                                string xmlString = CommonFunctions.GetXmlAsString(xmlDoc);
                                var response = CommonFunctions.GetMCResponse(xmlString);
                                xmlDoc.LoadXml(response);
                                var banknetData = xmlDoc.SelectSingleNode("CreditResponse/MCGrp/BanknetData");

                                if (banknetData != null)
                                {
                                    mcGrp.BanknetData = banknetData.InnerText;

                                }
                            }
                            _revDetails.Item = mcGrp;
                        }

                    }
                    #endregion

                    #region Amex Group
                    if (cardGrp.CardType.ToString().Equals(Amex))
                    {
                        AmexGrp amexgrp = new AmexGrp();
                        if (transactionRequest.TransactionType == Void)
                        {
                            if (mtdTransaction != null)
                            {
                                amexgrp.AmExTranID = mtdTransaction.GatewayTxnId;
                                XmlDocument xmlDoc = new XmlDocument();
                                xmlDoc.LoadXml(mtdTransaction.ResponseXml);
                                string xmlString = CommonFunctions.GetXmlAsString(xmlDoc);
                                var response = CommonFunctions.GetMCResponse(xmlString);
                                xmlDoc.LoadXml(response);
                                var amExPOSData = xmlDoc.SelectSingleNode("CreditResponse/AmexGrp/AmExPOSData");

                                if (amExPOSData != null)
                                {
                                    amexgrp.AmExPOSData = amExPOSData.InnerText;

                                }

                            }
                            _revDetails.Item = amexgrp;
                        }

                    }
                    #endregion

                    #region Discover Group
                    if (cardGrp.CardType.ToString().Equals(Discover))
                    {
                        DSGrp dsGrp = new DSGrp();
                        if (transactionRequest.TransactionType == Void)
                        {
                            if (mtdTransaction != null)
                            {
                                XmlDocument xmlDoc = new XmlDocument();
                                xmlDoc.LoadXml(mtdTransaction.ResponseXml);
                                string xmlString = CommonFunctions.GetXmlAsString(xmlDoc);
                                var response = CommonFunctions.GetMCResponse(xmlString);
                                xmlDoc.LoadXml(response);
                                var discProcCode = xmlDoc.SelectSingleNode("CreditResponse/DSGrp/DiscProcCode");
                                if (discProcCode != null)
                                {
                                    dsGrp.DiscProcCode = discProcCode.InnerText;

                                }

                                var discPOSEntry = xmlDoc.SelectSingleNode("CreditResponse/DSGrp/DiscPOSEntry");
                                if (discPOSEntry != null)
                                {
                                    dsGrp.DiscPOSEntry = discPOSEntry.InnerText;

                                }

                                var discRespCode = xmlDoc.SelectSingleNode("CreditResponse/DSGrp/DiscRespCode");
                                if (discRespCode != null)
                                {
                                    dsGrp.DiscRespCode = discRespCode.InnerText;

                                }

                                var discPOSData = xmlDoc.SelectSingleNode("CreditResponse/DSGrp/DiscPOSData");
                                if (discPOSData != null)
                                {
                                    dsGrp.DiscPOSData = discPOSData.InnerText;

                                }

                                var discTransQualifier = xmlDoc.SelectSingleNode("CreditResponse/DSGrp/DiscTransQualifier");
                                if (discTransQualifier != null)
                                {
                                    dsGrp.DiscTransQualifier = discTransQualifier.InnerText;

                                }

                                var discNRID = xmlDoc.SelectSingleNode("CreditResponse/DSGrp/DiscNRID");
                                if (discNRID != null)
                                {
                                    dsGrp.DiscNRID = discNRID.InnerText;

                                }

                            }
                            _revDetails.Item = dsGrp;
                        }

                    }
                    #endregion

                    #region emv group
                    EMVGrp emvGrp = new EMVGrp();
                    emvGrp.EMVData = transactionRequest.EmvData;
                    if (transactionRequest.CardSeq != null && transactionRequest.TransactionType != Void)
                    {
                        emvGrp.CardSeqNum = transactionRequest.CardSeq;
                    }

                    _revDetails.EMVGrp = emvGrp;

                    #endregion

                    #region addAm Group

                    AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();

                    if (mtdTransaction != null)
                    {
                        if (transactionRequest.TransactionType == Void)
                        {
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(mtdTransaction.ResponseXml);
                            string xmlString = CommonFunctions.GetXmlAsString(xmlDoc);
                            var response = CommonFunctions.GetMCResponse(xmlString);
                            xmlDoc.LoadXml(response);
                            var originalamt = xmlDoc.SelectSingleNode("CreditResponse/CommonGrp/TxnAmt");
                            if (originalamt != null)
                            {
                                var amountP = originalamt.InnerText;
                                addAmtGrp.AddAmt = amountP.PadLeft(12, '0');


                            }
                        }
                        if (transactionRequest.TransactionType != Void)
                        {
                            addAmtGrp.AddAmt = Convert.ToString(amt);
                            addAmtGrp.AddAmt = addAmtGrp.AddAmt.PadLeft(12, '0');
                            addAmtGrp.PartAuthrztnApprvlCapablt = PartAuthrztnApprvlCapabltType.Item1;
                            addAmtGrp.PartAuthrztnApprvlCapabltSpecified = true;
                        }
                        addAmtGrp.AddAmtCrncy = PaymentAPIResources.Cur_USA;
                        addAmtGrp.AddAmtType = AddAmtTypeType.TotalAuthAmt;
                        addAmtGrp.AddAmtTypeSpecified = true;


                        AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[1];
                        addAmtGrpArr[0] = addAmtGrp;
                        //addAmtGrpArr[1] = addAmtGrp1;
                        _revDetails.AddtlAmtGrp = addAmtGrpArr;
                    }

                    OrigAuthGrp orgAuthgrp = new OrigAuthGrp();


                    if (mtdTransaction != null)
                    {
                        switch (transactionRequest.TransactionType)
                        {
                            case Void:
                                orgAuthgrp.OrigAuthID = mtdTransaction.AuthId;
                                orgAuthgrp.OrigRespCode = mtdTransaction.ResponseCode;
                                break;
                        }
                        switch (mtdTransaction.TxnType)
                        {
                            case Void:
                                orgAuthgrp.OrigAuthID = mtdVoidTransaction.AuthId;
                                orgAuthgrp.OrigRespCode = mtdVoidTransaction.ResponseCode;
                                break;
                        }
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(mtdTransaction.RequestXml);
                        var localDateTime = xmlDoc.SelectSingleNode("TransactionRequest/LocalDateTime");
                        if (localDateTime != null)
                        {
                            orgAuthgrp.OrigLocalDateTime = localDateTime.InnerText;

                        }
                        var trnmsnDateTime = xmlDoc.SelectSingleNode("TransactionRequest/TrnmsnDateTime");
                        if (trnmsnDateTime != null)
                        {
                            orgAuthgrp.OrigTranDateTime = trnmsnDateTime.InnerText;

                        }

                        orgAuthgrp.OrigSTAN = mtdTransaction.Stan;
                    }
                    _revDetails.OrigAuthGrp = orgAuthgrp;

                    #endregion

                    _gmfMsgVar.Item = _revDetails;
                    transactionRequest.Stan = cmnGrp.STAN;
                    transactionRequest.TransRefNo = cmnGrp.RefNum;
                    transactionRequest.TransOrderNo = cmnGrp.OrderNum;
                    return _revDetails;
                }
            }

            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;
        }

        ///// <summary>
        ///// Purpose             :   Creating Transarmor request
        ///// Function Name       :   TaRequest
        ///// Created By          :   Madhuri Tanwar
        ///// Created On          :   04/20/2015
        ///// Modificatios Made   :   ****************************
        ///// Modidified On       :   "MM/DD/YYYY"  
        ///// </summary>
        ///// <param name="transactionRequest"></param>
        ///// <returns></returns>
        public TARequestDetails TaRequest(TransactionRequest transactionRequest)
        {
            try
            {
                if (!transactionRequest.IsNull())
                {
                    #region Common Group

                    /* Populate values for Common Group */
                    CommonGrp cmnGrp = new CommonGrp();

                    /* The payment type of the transaction. */
                    PymtTypeType typePayment = transactionRequest.PaymentType;
                    cmnGrp.PymtType = typePayment;
                    cmnGrp.PymtTypeSpecified = true;

                    /* The type of transaction being performed. */
                    cmnGrp.TxnType = TxnTypeType.TATokenRequest;
                    cmnGrp.TxnTypeSpecified = true;

                    /* The local date and time in which the transaction was performed. */
                    cmnGrp.LocalDateTime = transactionRequest.LocalDateTime;//DateTime.Now.ToString("yyyyMMddHHmmss");
                    /* The transmission date and time of the transaction (in GMT/UCT). */
                    cmnGrp.TrnmsnDateTime = transactionRequest.TrnmsnDateTime;//DateTime.UtcNow.ToString("yyyyMMddHHmmss");

                    /* A number assigned by the merchant to uniquely reference the transaction. 
                     * This number must be unique within a day per Merchant ID per Terminal ID. */
                    cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* A number assigned by the merchant to uniquely reference a set of transactions. */
                    cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);
                    //"480061115979";

                    /* A number assigned by the merchant to uniquely reference a transaction order sequence. */
                    //cmnGrp.OrderNum = cmnGrp.RefNum; //"162091864975";

                    /* An ID assigned by First Data, for the Third Party Processor or 
                     * Software Vendor that generated the transaction. */
                    cmnGrp.TPPID = transactionRequest.TppId;

                    /* A unique ID assigned to a terminal. */
                    cmnGrp.TermID = transactionRequest.TerminalId;

                    /* A unique ID assigned by First Data, to identify the Merchant. */
                    cmnGrp.MerchID = transactionRequest.FdMerchantId;

                    /* Indicates Group ID. */
                    cmnGrp.GroupID = transactionRequest.GroupId;
                    _taRequest.CommonGrp = cmnGrp;

                    #endregion

                    #region Card Group

                    /* Populate values for Card Group */
                    CardGrp cardGrp = new CardGrp();

                    cardGrp.AcctNum = transactionRequest.CardNumber;

                    _taRequest.CardGrp = cardGrp;

                    #endregion

                    #region TransArmor group

                    /* Populate values for Card Group */
                    TAGrp taGrp = new TAGrp();

                    taGrp.SctyLvl = SctyLvlType.Tknizatn;
                    taGrp.SctyLvlSpecified = true;

                    taGrp.TknType = transactionRequest.TokenType;

                    _taRequest.TAGrp = taGrp;

                    #endregion


                    /* Add the data populated object to GMF message variant object */
                    _gmfMsgVar.Item = _taRequest;
                    transactionRequest.Stan = cmnGrp.STAN;
                    transactionRequest.TransRefNo = cmnGrp.RefNum;
                    transactionRequest.TransOrderNo = cmnGrp.OrderNum;
                    return _taRequest;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;

        }

        /// <summary>
        /// Purpose             :   Creating Transarmor request
        /// Function Name       :   TaRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   09/20/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :    "MM/DD/YYYY"  
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        public TARequestDetails TaKeyRequest(TransactionRequest transactionRequest)
        {
            try
            {
                if (!transactionRequest.IsNull())
                {
                    #region Common Group

                    /* Populate values for Common Group */
                    CommonGrp cmnGrp = new CommonGrp();

                    /* The payment type of the transaction. */
                    //PymtTypeType typePayment = transactionRequest.PaymentType;
                    //cmnGrp.PymtType = typePayment;
                    //cmnGrp.PymtTypeSpecified = true;

                    /* The type of transaction being performed. */
                    cmnGrp.TxnType = TxnTypeType.TAKeyRequest;
                    cmnGrp.TxnTypeSpecified = true;

                    /* The local date and time in which the transaction was performed. */
                    cmnGrp.LocalDateTime = transactionRequest.LocalDateTime;//DateTime.Now.ToString("yyyyMMddHHmmss");
                    /* The transmission date and time of the transaction (in GMT/UCT). */
                    cmnGrp.TrnmsnDateTime = transactionRequest.TrnmsnDateTime;//DateTime.UtcNow.ToString("yyyyMMddHHmmss");

                    /* A number assigned by the merchant to uniquely reference the transaction. 
                     * This number must be unique within a day per Merchant ID per Terminal ID. */
                    cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* A number assigned by the merchant to uniquely reference a set of transactions. */
                    cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);
                    //"480061115979";

                    /* A number assigned by the merchant to uniquely reference a transaction order sequence. */
                    //cmnGrp.OrderNum = cmnGrp.RefNum; //"162091864975";

                    /* An ID assigned by First Data, for the Third Party Processor or 
                     * Software Vendor that generated the transaction. */
                    cmnGrp.TPPID = transactionRequest.TppId;

                    /* A unique ID assigned to a terminal. */
                    cmnGrp.TermID = transactionRequest.TerminalId;

                    /* A unique ID assigned by First Data, to identify the Merchant. */
                    cmnGrp.MerchID = transactionRequest.FdMerchantId;

                    /* Indicates Group ID. */
                    cmnGrp.GroupID = transactionRequest.GroupId;
                    _taRequest.CommonGrp = cmnGrp;

                    #endregion


                    #region TransArmor group

                    /* Populate values for Card Group */
                    TAGrp taGrp = new TAGrp();

                    taGrp.SctyLvl = SctyLvlType.EncrptTknizatn;
                    taGrp.SctyLvlSpecified = true;
                    taGrp.EncrptType = EncrptTypeType.RSA;
                    taGrp.EncrptTypeSpecified = true;
                    _taRequest.TAGrp = taGrp;

                    #endregion


                    /* Add the data populated object to GMF message variant object */
                    _gmfMsgVar.Item = _taRequest;
                    transactionRequest.Stan = cmnGrp.STAN;
                    transactionRequest.TransRefNo = cmnGrp.RefNum;
                    transactionRequest.TransOrderNo = cmnGrp.OrderNum;
                    return _taRequest;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;

        }

        /// <summary>
        /// Purpose             :   Creating credit card request
        /// Function Name       :   CreateRequest
        /// Created By          :   Salil Gupta
        /// Created On          :   03/16/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/017/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        private CreditRequestDetails BookerAppRequest(TransactionRequest transactionRequest)
        {
            /* Based on the GMF Specification UMF_Schema_V1.1.14.xsd, fields that are mandatory or related to 
                   this transaction should be populated.*/
            try
            {
                if (!transactionRequest.IsNull())
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                                          MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                        " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    MTDTransactionDto mtdTransaction = new MTDTransactionDto();
                    if (!string.IsNullOrEmpty(transactionRequest.RapidConnectAuthId))
                    {
                        mtdTransaction = _terminalService.Completion(
                           transactionRequest.RapidConnectAuthId,
                      transactionRequest.SerialNumber, transactionRequest.MtdMerchantId);
                        if (mtdTransaction.IsNull())
                            return null;
                    }

                    #region Common Group

                    CommonGrp cmnGrp = new CommonGrp();

                    /* The payment type of the transaction. */
                    PymtTypeType typePayment = transactionRequest.PaymentType;
                    cmnGrp.PymtType = typePayment;
                    cmnGrp.PymtTypeSpecified = true;
                    /* merchant category code. */
                    cmnGrp.MerchCatCode = transactionRequest.MCCode;
                    /* The type of transaction being performed. */
                    cmnGrp.TxnType = transactionRequest.TransType;
                    cmnGrp.TxnTypeSpecified = true;
                    /* The local date and time in which the transaction was performed. */
                    cmnGrp.LocalDateTime = transactionRequest.LocalDateTime;//DateTime.Now.ToString("yyyyMMddHHmmss");
                    /* The transmission date and time of the transaction (in GMT/UCT). */
                    cmnGrp.TrnmsnDateTime = transactionRequest.TrnmsnDateTime;//DateTime.UtcNow.ToString("yyyyMMddHHmmss");
                    /* A number assigned by the merchant to uniquely reference the transaction. 
                     * This number must be unique within a day per Merchant ID per Terminal ID. */
                    cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    /* A number assigned by the merchant to uniquely reference a set of transactions. */
                    //cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MerchantID, transactionRequest.TerminalID);//"480061115979";
                    cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);
                    //"480061115979";


                    /* A number assigned by the merchant to uniquely reference a transaction order sequence. */
                    cmnGrp.OrderNum = cmnGrp.RefNum; //"162091864975";


                    /* An ID assigned by First Data, for the Third Party Processor or 
                     * Software Vendor that generated the transaction. */
                    cmnGrp.TPPID = transactionRequest.TppId;

                    /* A unique ID assigned to a terminal. */
                    cmnGrp.TermID = transactionRequest.TerminalId;

                    //get terminal id from transaction request

                    /* A unique ID assigned by First Data, to identify the Merchant. */
                    cmnGrp.MerchID = transactionRequest.FdMerchantId;

                    cmnGrp.GroupID = transactionRequest.GroupId;
                    //Group ID value will be assigned by First Data. 
                    //get terminal id from transaction request
                    cmnGrp.POSEntryMode = PaymentAPIResources.EcomPosEntryMode;
                    cmnGrp.POSCondCode = POSCondCodeType.Item59;
                    cmnGrp.POSCondCodeSpecified = true;
                    cmnGrp.TermCatCode = TermCatCodeType.Item00;
                    cmnGrp.TermCatCodeSpecified = true;
                    cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item10;
                    cmnGrp.TermEntryCapabltSpecified = true;
                    cmnGrp.TermLocInd = TermLocIndType.Item1;
                    cmnGrp.TermLocIndSpecified = true;
                    cmnGrp.CardCaptCap = CardCaptCapType.Item0;
                    cmnGrp.CardCaptCapSpecified = true;

                    EcommGrp ecomgrp = new EcommGrp();
                    ecomgrp.EcommTxnInd = EcommTxnIndType.Item03;
                    ecomgrp.EcommTxnIndSpecified = true;
                    if (!string.Equals(cmnGrp.TxnType.ToString(), Authorization, StringComparison.CurrentCultureIgnoreCase))
                        ecomgrp.EcommURL = PaymentAPIResources.EcomUrl;
                    _creditReq.EcommGrp = ecomgrp;

                    /* The amount of the transaction. This may be an authorization amount, 
                     * adjustment amount or a reversal    based on the type of transaction. 
                     * It is inclusive of all additional amounts. 
                     * It is submitted in the currency represented by the Transaction Currency field.  
                     * The field is overwritten in the response for a partial authorization. */
                    double num = Convert.ToDouble(transactionRequest.TransactionAmount);
                    string amt = num.ToString("0.00").Replace(".", string.Empty);
                    cmnGrp.TxnAmt = Convert.ToString(amt);
                    cmnGrp.TxnAmt = cmnGrp.TxnAmt.PadLeft(12, '0');

                    cmnGrp.TxnCrncy = PaymentAPIResources.Cur_USA;
                    /* An indicator that describes the location of the terminal. */
                    /* Indicates Group ID. */
                    _creditReq.CommonGrp = cmnGrp;
                    #endregion

                    CardGrp cardGrp = new CardGrp();
                    cardGrp.CardType = transactionRequest.CardType;
                    cardGrp.CardTypeSpecified = true;
                    cardGrp.CardExpiryDate = transactionRequest.ExpiryDate;

                    TAGrp taGrp = new TAGrp();
                    taGrp.SctyLvl = SctyLvlType.Tknizatn;
                    taGrp.SctyLvlSpecified = true;
                    if (!string.IsNullOrEmpty(transactionRequest.RCCIToken))
                        taGrp.Tkn = transactionRequest.RCCIToken.Decrypt();
                    else
                    {
                        cardGrp.AcctNum = transactionRequest.CardNumber;
                        cardGrp.CCVInd = CCVIndType.Prvded;
                        cardGrp.CCVIndSpecified = true;
                        cardGrp.CCVData = transactionRequest.CardCvv;
                    }
                    taGrp.TknType = transactionRequest.TokenType;
                    _creditReq.TAGrp = taGrp;
                    _creditReq.CardGrp = cardGrp;


                    #region Additional Amount Group
                    AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();
                    addAmtGrp.PartAuthrztnApprvlCapablt = PartAuthrztnApprvlCapabltType.Item1;
                    addAmtGrp.PartAuthrztnApprvlCapabltSpecified = true;
                    AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[1];
                    addAmtGrpArr[0] = addAmtGrp;
                    _creditReq.AddtlAmtGrp = addAmtGrpArr;
                    #endregion

                    // Only for Visa Card
                    #region Visa Group
                    if (string.Equals(cardGrp.CardType.ToString(), Visa, StringComparison.OrdinalIgnoreCase) && !string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.OrdinalIgnoreCase))
                    {
                        VisaGrp visaGrp = new VisaGrp();
                        visaGrp.VisaBID = PaymentAPIResources.VisaBID;
                        visaGrp.VisaAUAR = PaymentAPIResources.VisaAUAR;
                        if (!string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                        {
                            visaGrp.ACI = ACIType.Y;
                            visaGrp.ACISpecified = true;
                            visaGrp.TaxAmtCapablt = TaxAmtCapabltType.Item1;
                            visaGrp.TaxAmtCapabltSpecified = true;
                        }
                        else
                        {
                            visaGrp.ACI = ACIType.T;
                            visaGrp.ACISpecified = true;
                        }
                        _creditReq.Item = visaGrp;
                    }
                    #endregion

                    #region AmexGrp Group
                    if (string.Equals(cardGrp.CardType.ToString(), Amex, StringComparison.OrdinalIgnoreCase) && string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                    {
                        AmexGrp amexgrp = new AmexGrp();
                        // amexgrp.AmExPOSData ="";
                        if (mtdTransaction != null) amexgrp.AmExTranID = mtdTransaction.GatewayTxnId;
                        _creditReq.Item = amexgrp;
                    }
                    #endregion

                    #region Discover Group
                    if ((string.Equals(cardGrp.CardType.ToString(), Diners, StringComparison.OrdinalIgnoreCase) || string.Equals(cardGrp.CardType.ToString(), Discover, StringComparison.OrdinalIgnoreCase) || string.Equals(cardGrp.CardType.ToString(), "JCB", StringComparison.OrdinalIgnoreCase)) && string.Equals(cmnGrp.TxnType.ToString(), "completion", StringComparison.OrdinalIgnoreCase))
                    {
                        DSGrp dsGrp = new DSGrp();
                        if (mtdTransaction != null) dsGrp.DiscNRID = mtdTransaction.GatewayTxnId;
                        _creditReq.Item = dsGrp;
                    }
                    #endregion

                    #region Customer info Group
                    if (!string.IsNullOrEmpty(transactionRequest.StreetAddress) || !string.IsNullOrEmpty(transactionRequest.ZipCode))
                    {
                        if (!string.Equals(cmnGrp.TxnType.ToString(), Refund, StringComparison.OrdinalIgnoreCase) && !string.Equals(cmnGrp.TxnType.ToString(), Completion, StringComparison.OrdinalIgnoreCase))
                        {
                            CustInfoGrp custinfo = new CustInfoGrp();
                            if (!string.IsNullOrEmpty(transactionRequest.StreetAddress))
                            {
                                custinfo.AVSBillingAddr = transactionRequest.StreetAddress;
                            }
                            if (!string.IsNullOrEmpty(transactionRequest.ZipCode))
                            {
                                custinfo.AVSBillingPostalCode = transactionRequest.ZipCode;
                            }
                            _creditReq.CustInfoGrp = custinfo;
                        }
                    }
                    #endregion

                    /* Add the data populated object to GMF message variant object */
                    _gmfMsgVar.Item = _creditReq;
                    transactionRequest.Stan = cmnGrp.STAN;
                    transactionRequest.TransRefNo = cmnGrp.RefNum;
                    transactionRequest.TransOrderNo = cmnGrp.OrderNum;
                    return _creditReq;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;

        }

        /// <summary>
        /// Purpose             :   Generate Client Ref Number in the format STAN|TPPID, right justified and left padded with "0"
        /// Function Name       :   GetClientRef
        /// Created By          :   Umesh
        /// Created On          :   03/23/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY"  
        /// </summary>
        /// <returns></returns>
        public static string GetClientRef(string projectId)
        {

            int transactionId = new Random(Guid.NewGuid().GetHashCode()).Next(10000, 99999);
            string clientRef = transactionId + "V" + "003000";
            clientRef = "00" + clientRef;
            return clientRef;
        }

        /// <summary>
        /// Purpose             :   The method will convert the GMF transaction object into an XML string
        /// Function Name       :   GetXmlData
        /// Created By          :   Umesh
        /// Created On          :   03/24/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY"  
        /// </summary>
        /// <returns></returns>
        public String GetXmlData()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                string xmlString;
                MemoryStream memoryStream;
                using (memoryStream = new MemoryStream())
                {
                    XmlSerializer xs = new XmlSerializer(_gmfMsgVar.GetType());
                    XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);

                    XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                    ns.Add("", "com/firstdata/Merchant/gmfV4.02");

                    xs.Serialize(xmlTextWriter, _gmfMsgVar, ns);
                    memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                    var encoding = new UTF8Encoding();
                    xmlString = encoding.GetString(memoryStream.ToArray());
                    memoryStream.Dispose();
                    xmlString = xmlString.Substring(1, xmlString.Length - 1);
                }

                return xmlString;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :   datawire entities 
        /// Function Name       :   DatawireEntities
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   29/March/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :    "MM/DD/YYYY"  
        private DatawireEntities DataWireEntities
        {
            get { return _objMessageProcessor.ObjDatawireEntities; }
            set
            {
                _objMessageProcessor.ObjDatawireEntities = value;

            }
        }

        /// <summary>
        /// Purpose             :   Sending request to register the terminal with datawire
        /// Function Name       :   SendRegistrationRequest
        /// Created By          :   Salil Gupta
        /// Created On          :   03/25/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY"
        /// </summary>
        /// <param name="clientRef"></param>
        /// <param name="fdMerchantobj"></param>
        /// <param name="tId"></param>
        /// <returns></returns>
        public DatawireResponse SendRegistrationRequest(fDMerchantDto fdMerchantobj, string tId)
        {
            DatawireResponse datawireResponse = new DatawireResponse();
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                              MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                ResponseType res;
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                ServiceURL.SrsServiceUrl = WebConfigurationManager.AppSettings["SrsURL"];
                using (srsService client = new srsService())
                {
                    MTData.WebAPI.schema.ReqClientIDType reqClientIdType = new MTData.WebAPI.schema.ReqClientIDType();
                    reqClientIdType.App = fdMerchantobj.App;
                    reqClientIdType.DID = "";
                    reqClientIdType.Auth = fdMerchantobj.GroupId + "" + fdMerchantobj.FDMerchantID + "|" + tId;
                    reqClientIdType.ClientRef = GetClientRef(fdMerchantobj.ProjectId);
                    RegistrationType registrationType = new RegistrationType();
                    registrationType.ServiceID = Convert.ToString(fdMerchantobj.ServiceId);
                    MTData.WebAPI.schema.RequestType requestType = new MTData.WebAPI.schema.RequestType();
                    requestType.ReqClientID = reqClientIdType;
                    requestType.Item = registrationType;
                    client.UserAgent = WebConfigurationManager.AppSettings["UserAgent"];// "Android Taxi v1.0";

                    res = client.SrsOperation(requestType);

                    if (res.Status.StatusCode.ToString() != "OK")
                    {
                        datawireResponse.Status = res.Status.StatusCode.ToString();
                        datawireResponse.Value = res.Status.Value;
                        datawireResponse.ClientRef = res.RespClientID.ClientRef;
                        datawireResponse.Did = res.RespClientID.DID;
                        datawireResponse.Version = res.Version;
                    }
                }

                if (res.Status.StatusCode.ToString() == "OK")
                {
                    ResponseType activeResponse = SendActivationRequest(res.RespClientID.DID, fdMerchantobj, tId);
                    if (activeResponse.Status.StatusCode.ToString() == "OK")
                    {
                        try
                        {
                            datawireResponse.Status = res.Status.StatusCode.ToString();
                            datawireResponse.Value = res.Status.Value;
                            datawireResponse.ClientRef = res.RespClientID.ClientRef;
                            datawireResponse.Did = res.RespClientID.DID;
                            datawireResponse.Version = res.Version;
                            return datawireResponse;
                        }
                        catch (Exception ex)
                        {
                            _logger.LogInfoFatel(_logMessage.ToString(), ex);
                        }
                    }
                }
                else if (res.Status.StatusCode == StatusCodeType.Retry ||
                         res.Status.StatusCode == StatusCodeType.InternalError ||
                         res.Status.StatusCode == StatusCodeType.OtherError)
                {
                    while (_count < 3)
                    {
                        Thread.Sleep(3000);
                        _count++;
                        SendRegistrationRequest(fdMerchantobj, tId);

                    }
                }

                return datawireResponse;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :   Sending request to register the terminal with datawire
        /// Function Name       :   SendActivationRequest
        /// Created By          :   Salil Gupta
        /// Created On          :   03/25/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="clientRef"></param>
        /// <param name="dId"></param>
        /// <param name="fdMerchantobj"></param>
        /// <param name="tId"></param>
        /// <returns></returns>
        private ResponseType SendActivationRequest(string dId, fDMerchantDto fdMerchantobj, string tId)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                              MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                ResponseType response;
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                ServiceURL.SrsServiceUrl = WebConfigurationManager.AppSettings["SrsURL"];
                using (srsService client = new srsService())
                {
                    MTData.WebAPI.schema.ReqClientIDType reqClientIdType = new MTData.WebAPI.schema.ReqClientIDType();
                    reqClientIdType.App = fdMerchantobj.App; //"RAPIDCONNECTSRS";
                    reqClientIdType.DID = dId;
                    reqClientIdType.Auth = fdMerchantobj.GroupId + "" + fdMerchantobj.FDMerchantID + "|" + tId;
                    reqClientIdType.ClientRef = GetClientRef(fdMerchantobj.ProjectId);
                    ActivationType activationType = new ActivationType();
                    activationType.ServiceID = Convert.ToString(fdMerchantobj.ServiceId);
                    MTData.WebAPI.schema.RequestType requestType = new MTData.WebAPI.schema.RequestType();
                    requestType.ReqClientID = reqClientIdType;
                    requestType.Item = activationType;
                    client.UserAgent = WebConfigurationManager.AppSettings["UserAgent"];
                    response = client.SrsOperation(requestType);
                    if (response.Status.StatusCode.ToString() == "OK")
                    {
                        response.RespClientID.DID = reqClientIdType.DID;
                        response.RespClientID.ClientRef = reqClientIdType.ClientRef;
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }

            return null;
        }

        /// <summary>
        /// Purpose             :   registration for canada
        /// Function Name       :   CanadaSendRegistrationRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   29/March/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :    "MM/DD/YYYY"  
        public string SendRegistrationRequestCN(fDMerchantDto fdMerchantobj, string tId, int merchantId)
        {
            string xmlresponseAct = string.Empty;
            string registerResponse = string.Empty;
            string xmlResponse = string.Empty;
            
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                var mid = fdMerchantobj.FDMerchantID;
                var tid = tId;
                string clientReferId = CnTransactionMessage.GetCanadaClientRef();

                DataWireEntities = new DatawireEntities
                {
                    AppID = fdMerchantobj.App,
                    MID = mid,
                    TID = tid,
                    ServiceId = Convert.ToString(fdMerchantobj.ServiceId),
                    ClientRefId = clientReferId,
                    AuthID = fdMerchantobj.FDMerchantID + "|" + tId,
                    TransactionTime = DateTime.Now
                };

                double transTime;
                xmlResponse = SendHttpRequest(DataWireRequestType.Registration, out transTime);
                #region reg response
                string returnCode = CommonFunctions.GetReturnCode(xmlResponse);
                if (!string.IsNullOrEmpty(returnCode) && !returnCode.Equals(MessageTypeDW.StatusSuccess))
                {
                    ErrorLogging.LogDatawireExceptions(returnCode);
                }

                string status = CommonFunctions.GetStatus(xmlResponse);
                if (!string.IsNullOrEmpty(status))
                {
                    ErrorLogging.LogDatawireExceptions(status);

                    if (status.ToLower().Equals(MessageTypeDW.accessdenied.ToString()))
                    {
                        xmlResponse = MessageTypeDW.AccessDenied.ToString();
                        return xmlResponse;
                    }
                    
                    if (status.ToLower().Equals(MessageTypeDW.authenticationerror.ToString()))
                    {
                        xmlResponse = MessageTypeDW.AuthenticationError.ToString();
                        return xmlResponse;
                    }

                    if (status.ToLower().Equals(MessageTypeDW.retry))
                    {
                        retryCount += 1;
                        if (retryCount <= 3)
                            SendRegistrationRequestCN(fdMerchantobj, tId, merchantId);
                        else
                        {
                            xmlResponse = MessageTypeDW.TransactionCancel.ToString();
                            return xmlResponse;
                        }
                    }

                    IEnumerable<string> serviceDiscoveryUrls = null;
                    string did1 = String.Empty;
                    if (status.ToLower().Equals(MessageTypeDW.ok.ToString()))
                    {
                        did1 = CommonFunctions.GetDid(xmlResponse);
                        serviceDiscoveryUrls = CommonFunctions.GetUrls(xmlResponse);
                        DataWireEntities.DiscoveryUrls = new Collection<string>();
                        DataWireEntities.DID = did1;
                        foreach (string urls in serviceDiscoveryUrls)
                        {
                            DataWireEntities.DiscoveryUrls.Add(urls);
                        }

                        xmlresponseAct = SendActivationRequest(xmlResponse);
                    }
                    #endregion
                    string statusAct = CommonFunctions.GetStatus(xmlresponseAct);

                    if (!string.IsNullOrEmpty(statusAct))
                    {
                        ErrorLogging.LogDatawireExceptions(statusAct);

                        if (statusAct.ToLower().Equals(MessageTypeDW.accessdenied.ToString()))
                        {
                            xmlresponseAct = MessageTypeDW.AccessDenied.ToString();

                            return xmlresponseAct;
                        }
                        
                        if (statusAct.ToLower().Equals(MessageTypeDW.authenticationerror.ToString()))
                        {
                            xmlresponseAct = MessageTypeDW.AuthenticationError.ToString();
                            return xmlresponseAct;
                        }

                        if (statusAct.ToLower().Equals(MessageTypeDW.notfound.ToString()))
                        {
                            xmlresponseAct = MessageTypeDW.NoTicketProvisioned.ToString();
                            return xmlresponseAct;
                        }

                        if (statusAct.ToLower().Equals(MessageTypeDW.ok.ToString()))
                        {
                            ServiceDiscoveryParametersDto discoveryParemeters = new ServiceDiscoveryParametersDto { DiscoveryUrls = serviceDiscoveryUrls.ToList(), DID = did1, XmlResponse = xmlResponse, MerchantId = merchantId, TID = tid };
                            _terminalService.SaveServiceDiscoveryUrls(discoveryParemeters);
                            string DiscovercyResponse = ServiceDiscovery();
                        }

                        return xmlResponse;
                    }
                }

                return xmlResponse;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   Activation for canada
        /// Function Name       :   SendActivationRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   29/March/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :    "MM/DD/YYYY"  
        public string SendActivationRequest(string xmlRegResponse)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            
            string xmlResponse = null;
            try
            {
                double transTime = 0;
                _logMessage.AppendLine("send datwire activation request.");
                xmlResponse = SendHttpRequest(DataWireRequestType.Activation, out  transTime);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
                throw;
            }

            return xmlResponse;
        }

        /// <summary>
        /// Purpose             :    service discovery
        /// Function Name       :   ServiceDiscovery
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   01/april/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :    "MM/DD/YYYY"  
        public string ServiceDiscovery()
        {
            string xmlResponse = string.Empty;
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                var datawireDetails = _terminalService.GetRandomMerchantDetail();
                var fDmerchantDetail = _terminalService.GetFDMerchant(Convert.ToInt32(datawireDetails.MerchantId));
                List<string> discoveryUrls = _terminalService.GetServiceDiscoveryUrls();
                if (discoveryUrls != null && discoveryUrls.Count > 0)
                {
                    for (int retry = 0; retry <= 2; retry++)
                    {
                        foreach (string url in discoveryUrls)
                        {
                            xmlResponse = SendServiceDiscoveryRequest(url + "/" + Global.ServiceID, fDmerchantDetail);
                            if (!string.IsNullOrEmpty(xmlResponse))
                            {
                                #region Response handling

                                GetServiceUrlData(xmlResponse, fDmerchantDetail, datawireDetails);
                                
                                var sortedServiceProviderUrls = DataWireEntities.ServiceProviderUrls.OrderBy(o => o.TransactionTime).ToList();
                                var sortedDatawireServiceUrls = new Collection<DatawireServiceUrls>(sortedServiceProviderUrls);

                                if (sortedDatawireServiceUrls.Count > 0)
                                {
                                    DataWireEntities.AssignServiceUrl(sortedDatawireServiceUrls);
                                    DataWireEntities.ActiveUrl = DataWireEntities.ServiceProviderUrls[0].Url;
                                    DataWireEntities.MaxNumberOfTransaction = DataWireEntities.ServiceProviderUrls[0].MaximumTransactionInPackage;
                                    DataWireEntities.TransactionCount = 0;
                                    DataWireEntities.TransactionTime = DateTime.Now;
                                    var serviceProviderUrlDto = AssignDatawireEntitiesToServiceProvider(DataWireEntities.ServiceProviderUrls, DataWireEntities.ActiveUrl);
                                    Global.UrlList = _terminalService.SaveServiceProviderUrls(serviceProviderUrlDto);
                                    break;
                                }
                                #endregion

                            }
                        }

                        if (!string.IsNullOrEmpty(xmlResponse))
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
                throw;
            }

            return xmlResponse;
        }

        /// <summary>
        /// Purpose             :   sending ping request
        /// Function Name       :   SendPingRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   01/april/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :    "MM/DD/YYYY"  
        public string SendPingRequest(string pingUrl, fDMerchantDto fdmerchnt, string tId, string did)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            string url = fdmerchnt.ServiceUrl;
            string xmlResponse = string.Empty;

            try
            {
                var mid = fdmerchnt.FDMerchantID;
                var tid = tId;
                string clientReferId = GetClientRef(fdmerchnt.ProjectId);
                _logMessage.AppendLine("set entities paramerters .");
                DataWireEntities = new DatawireEntities
                {
                    AppID = fdmerchnt.App,
                    MID = mid,
                    TID = tid,
                    ServiceId = Convert.ToString(fdmerchnt.ServiceId),
                    ClientRefId = clientReferId,
                    AuthID = fdmerchnt.FDMerchantID + "|" + tId,
                    DID = did,
                    TransactionTime = DateTime.Now
                };

                double transTime = 0;
                _logMessage.AppendLine("send ping request.");
                xmlResponse = SendHttpRequest(DataWireRequestType.Ping, out transTime, pingUrl: url);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
                throw;
            }

            return xmlResponse;
        }

        /// <summary>
        /// Purpose             :   sending service discovery request
        /// Function Name       :   SendServiceDiscoveryRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   01/april/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :    "MM/DD/YYYY"  
        private string SendServiceDiscoveryRequest(string requestUrl, fDMerchantDto fdmerchnt)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            string xmlResponse = string.Empty;

            try
            {
                _logMessage.AppendLine("service registration request process start.");
                var objWebrequest = (HttpWebRequest)WebRequest.Create(requestUrl);
                objWebrequest.UserAgent = Global.UserAgent;
                objWebrequest.Method = "GET";
                objWebrequest.Headers.Add(HttpRequestHeader.CacheControl, "no-cache");
                objWebrequest.KeepAlive = true;
                objWebrequest.Timeout = 10000;
                _logMessage.AppendLine("service registration response process start.");
                var response = (HttpWebResponse)objWebrequest.GetResponse();
                var receiveStream = response.GetResponseStream();
                if (receiveStream != null)
                {
                    var readStream = new StreamReader(receiveStream, Encoding.UTF8);
                    xmlResponse = readStream.ReadToEnd();
                    response.Close();
                    readStream.Close();
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
                return null;
            }

            return xmlResponse;
        }

        /// <summary>
        /// Purpose             :   Send request
        /// Function Name       :   SendHttpRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   29/March/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :    "MM/DD/YYYY"  
        private string SendHttpRequest(DataWireRequestType requestType, out double transTime, int timeOut = 10000, string sequenceNumber = "", string pingUrl = "", string payloadData = "")
        {
            transTime = 0;
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                
                HttpWebRequest objWebrequest = null;
                byte[] buf = null;

                string xmlResponse = string.Empty;
                string xmlRequest = string.Empty;

                switch (requestType)
                {
                    case DataWireRequestType.Registration:
                        _logMessage.AppendLine("merchant registration request process start.");
                        xmlRequest = CommonFunctions.GenerateDataWireXml((int)DataWireRequestType.Registration, DataWireEntities);
                        buf = Encoding.UTF8.GetBytes(xmlRequest);
                        objWebrequest = (HttpWebRequest)WebRequest.Create(WebConfigurationManager.AppSettings["RegistrationServerUrl"]);

                        break;

                    case DataWireRequestType.Activation:
                        _logMessage.AppendLine("merchant Activation request process start.");
                        xmlRequest = CommonFunctions.GenerateDataWireXml((int)DataWireRequestType.Activation, DataWireEntities);
                        buf = Encoding.UTF8.GetBytes(xmlRequest);
                        objWebrequest = (HttpWebRequest)WebRequest.Create(WebConfigurationManager.AppSettings["RegistrationServerUrl"]);

                        break;

                    case DataWireRequestType.Ping:
                        _logMessage.AppendLine("merchant ping request process start.");
                        xmlRequest = CommonFunctions.GenerateDataWireXml((int)DataWireRequestType.Ping, DataWireEntities);
                        buf = Encoding.UTF8.GetBytes(xmlRequest);
                        objWebrequest = (HttpWebRequest)WebRequest.Create(pingUrl);

                        break;
                }

                _logMessage.AppendLine(" request process start.");
                if (objWebrequest != null)
                {
                    objWebrequest.UserAgent = "vxnapi_xml_3_2_0_19";
                    objWebrequest.Method = "POST";
                    objWebrequest.ContentLength = buf.Length;
                    objWebrequest.KeepAlive = true;
                    objWebrequest.Headers.Add(HttpRequestHeader.CacheControl, "no-cache");
                    objWebrequest.ContentType = "text/xml";
                    objWebrequest.Timeout = timeOut;

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                         | SecurityProtocolType.Tls11
                                                         | SecurityProtocolType.Tls12
                                                         | SecurityProtocolType.Ssl3;

                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    using (var stream = objWebrequest.GetRequestStream())
                    {
                        stream.Write(buf, 0, buf.Length);
                    }

                    _logMessage.AppendLine("response process start.");
                    DateTime beforeRequest = DateTime.Now;
                    var response = (HttpWebResponse)objWebrequest.GetResponse();
                    DateTime afterRequest = DateTime.Now;
                    TimeSpan timeSpan = afterRequest - beforeRequest;
                    transTime = (double)timeSpan.TotalMilliseconds;
                    
                    var receiveStream = response.GetResponseStream();
                    if (receiveStream != null)
                    {
                        var readStream = new StreamReader(receiveStream, Encoding.UTF8);
                        xmlResponse = readStream.ReadToEnd();
                        response.Close();
                        readStream.Close();
                    }
                }

                return xmlResponse;
            }
            catch 
            { 
                return null; 
            }
        }

        /// <summary>
        /// Purpose             :   Creating credit card request
        /// Function Name       :   CreateEmvContactCapkRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   10/20/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :    "MM/DD/YYYY"  
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        private AdminRequestDetails CreateEmvContactUpdateCapkRequest(TransactionRequest transactionRequest)
        {
            try
            {
                if (!transactionRequest.IsNull())
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                    MTDTransactionDto mtdTransaction = new MTDTransactionDto();
                    if (!string.IsNullOrEmpty(transactionRequest.RapidConnectAuthId))
                    {
                        mtdTransaction = _terminalService.Completion(transactionRequest.RapidConnectAuthId, transactionRequest.SerialNumber, transactionRequest.MtdMerchantId);
                        if (mtdTransaction.IsNull())
                            return null;
                    }

                    #region Common Group

                    CommonGrp cmnGrp = new CommonGrp();

                    //cmnGrp.TxnType = TxnTypeType.FileDownload;
                    cmnGrp.TxnType = transactionRequest.TransType;
                    cmnGrp.TxnTypeSpecified = true;

                    /* The local date and time in which the transaction was performed. */
                    cmnGrp.LocalDateTime = transactionRequest.LocalDateTime;//DateTime.Now.ToString("yyyyMMddHHmmss");
                    /* The transmission date and time of the transaction (in GMT/UCT). */
                    cmnGrp.TrnmsnDateTime = transactionRequest.TrnmsnDateTime;//DateTime.UtcNow.ToString("yyyyMMddHHmmss");
                    /* A number assigned by the merchant to uniquely reference the transaction. 
                     * This number must be unique within a day per Merchant ID per Terminal ID. */
                    cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);


                    cmnGrp.TPPID = transactionRequest.TppId;

                    /* A unique ID assigned to a terminal. */
                    cmnGrp.TermID = transactionRequest.TerminalId;

                    //get terminal id from transaction request

                    /* A unique ID assigned by First Data, to identify the Merchant. */
                    cmnGrp.MerchID = transactionRequest.FdMerchantId;

                    cmnGrp.GroupID = transactionRequest.GroupId;

                    _adminReq.CommonGrp = cmnGrp;
                    #endregion

                    #region FileDLGrp Group
                    FileDLGrp fileDlgrp = new FileDLGrp();
                    fileDlgrp.FileType = EMV2KEY;
                    fileDlgrp.FunCode = "R";

                    if (!string.IsNullOrEmpty(transactionRequest.RapidConnectAuthId))
                    {
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(mtdTransaction.ResponseXml);
                        string xmlString = CommonFunctions.GetXmlAsString(xmlDoc);
                        var response = CommonFunctions.GetMCResponse(xmlString);
                        xmlDoc.LoadXml(response);
                        var currFileCreatDt = xmlDoc.SelectSingleNode("AdminResponse/FileDLGrp/CurrFileCreationDt");
                        if (currFileCreatDt != null)
                        {
                            fileDlgrp.CurrFileCreationDt = currFileCreatDt.InnerText; 
                        }
                        
                        var fileSize = xmlDoc.SelectSingleNode("AdminResponse/FileDLGrp/FileSize");
                        if (fileSize != null)
                        {
                            fileDlgrp.FileSize = fileSize.InnerText; 
                        }
                        
                        var fileCRC16 = xmlDoc.SelectSingleNode("AdminResponse/FileDLGrp/FileCRC16");
                        if (fileCRC16 != null)
                        {
                            fileDlgrp.FileCRC16 = fileCRC16.InnerText; 
                        }
                    }
                    else
                    {
                        fileDlgrp.CurrFileCreationDt = PaymentAPIResources.CurrFileCreationDt;
                        fileDlgrp.FileSize = FileSizeValue;
                        fileDlgrp.FileCRC16 = FileCRCValue;
                    }

                    _adminReq.FileDLGrp = fileDlgrp;
                    #endregion
                    _gmfMsgVar.Item = _adminReq;
                    transactionRequest.Stan = cmnGrp.STAN;
                    transactionRequest.TransRefNo = cmnGrp.RefNum;
                    transactionRequest.TransOrderNo = cmnGrp.OrderNum;
                    return _adminReq;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }

            return null;
        }

        /// <summary>
        /// Purpose             :   Creating credit card request
        /// Function Name       :   CreateEmvContactCapkRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   10/20/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :    "MM/DD/YYYY"  
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        private AdminRequestDetails CreateEmvContactDownloadCapkRequest(TransactionRequest transactionRequest)
        {
            try
            {
                if (!transactionRequest.IsNull())
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    
                    MTDTransactionDto mtdTransaction = new MTDTransactionDto();

                    if (!string.IsNullOrEmpty(transactionRequest.RapidConnectAuthId))
                    {
                        mtdTransaction = _terminalService.Completion(transactionRequest.RapidConnectAuthId,transactionRequest.SerialNumber, transactionRequest.MtdMerchantId);
                        if (mtdTransaction.IsNull())
                            return null;
                    }
                    #region Common Group

                    CommonGrp cmnGrp = new CommonGrp();

                    cmnGrp.TxnType = transactionRequest.TransType;
                    cmnGrp.TxnTypeSpecified = true;
                    /* The local date and time in which the transaction was performed. */
                    cmnGrp.LocalDateTime = transactionRequest.LocalDateTime;//DateTime.Now.ToString("yyyyMMddHHmmss");
                    /* The transmission date and time of the transaction (in GMT/UCT). */
                    cmnGrp.TrnmsnDateTime = transactionRequest.TrnmsnDateTime;//DateTime.UtcNow.ToString("yyyyMMddHHmmss");
                    cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);
                    cmnGrp.TPPID = transactionRequest.TppId;
                    cmnGrp.TermID = transactionRequest.TerminalId;
                    /* A unique ID assigned by First Data, to identify the Merchant. */
                    cmnGrp.MerchID = transactionRequest.FdMerchantId;
                    cmnGrp.GroupID = transactionRequest.GroupId;
                    _adminReq.CommonGrp = cmnGrp;
                    #endregion

                    #region FileDLGrp Group
                    FileDLGrp fileDlgrp = new FileDLGrp();
                    fileDlgrp.FileType = EMV2KEY;
                    fileDlgrp.FunCode = "D";
                    
                    if (!string.IsNullOrEmpty(transactionRequest.FbSeq))
                    { 
                        fileDlgrp.FBSeq = transactionRequest.FbSeq; 
                    }
                    fileDlgrp.ReqFBMaxSize = ReqFBMaxSizeValue;
                    
                    if (!string.IsNullOrEmpty(transactionRequest.ReqFileOff))
                    { 
                        fileDlgrp.ReqFileOffset = transactionRequest.ReqFileOff; 
                    }
                    else
                    {
                        fileDlgrp.FBSeq = PaymentAPIResources.FBSeq;
                        fileDlgrp.ReqFBMaxSize = ReqFBMaxSizeValue;
                        fileDlgrp.ReqFileOffset = PaymentAPIResources.ReqFileOffset;
                    }

                    _adminReq.FileDLGrp = fileDlgrp;
                    #endregion

                    _gmfMsgVar.Item = _adminReq;
                    transactionRequest.Stan = cmnGrp.STAN;
                    transactionRequest.TransRefNo = cmnGrp.RefNum;
                    transactionRequest.TransOrderNo = cmnGrp.OrderNum;
                    return _adminReq;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }

            return null;
        }

        private void GetServiceUrlData(string xmlResponse, fDMerchantDto fdMerchantDto, DatawireDto datawireDto)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                var xEle = XElement.Parse(xmlResponse);
                var xElementServiceProviders = xEle.Descendants(MessageTypeDW.ServiceProvider.ToString());
                var elements = xElementServiceProviders as IList<XElement> ?? xElementServiceProviders.ToList();
                List<DatawireServiceUrls> serviceUrls = new List<DatawireServiceUrls>();
                for (int retry = 0; retry <= 2; retry++)
                {
                    for (var i = 0; i < elements.Count(); i++)
                    {
                        var element = elements.ElementAt(i).Element(MessageTypeDW.URL.ToString());
                        var objServiceUrls = new DatawireServiceUrls();
                        if (element != null)
                        {
                            objServiceUrls.Url = element.Value;
                        }

                        var urlTransactionTime = SendPingUrlRequest(objServiceUrls.Url, fdMerchantDto, datawireDto);
                        objServiceUrls.TransactionTime = urlTransactionTime;
                        
                        var xElement = elements.ElementAt(i).Element(MessageTypeDW.MaxTransactionsInPackage.ToString());                        
                        if (xElement != null)
                            objServiceUrls.MaximumTransactionInPackage = Convert.ToInt32(xElement.Value, CultureInfo.InvariantCulture);
                        
                        if (urlTransactionTime != 0)
                            serviceUrls.Add(objServiceUrls);
                    }

                    if (serviceUrls != null && serviceUrls.Count > 0)
                        break;
                }

                foreach (DatawireServiceUrls url in serviceUrls)
                {
                    DataWireEntities.ServiceProviderUrls.Add(url);
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
                throw;
            }
        }

        public static String GetMessage(MessageTypeDW search)
        {
            try
            {
                var doc = new XmlDocument();
                doc.Load(ResourceFilePath);
                var xdoc = XDocument.Parse(doc.InnerXml);
                var nodes = doc.SelectNodes("Response/ServiceDiscoveryResponse/ServiceProvider/URL");
                var result = (from c in xdoc.Descendants("item")
                              where ((String)c.Attribute("key")).Equals(search.ToString())
                              select c.Attribute("value").Value).FirstOrDefault();

                return result;
            }
            catch
            {
                throw;
            }
        }

        private static string ResourceFilePath
        {
            get
            {
                string strResorcePath = StrAppPath.Remove(StrAppPath.LastIndexOf('\\'));
                strResorcePath += @"\MessagesResource.xml";
                return strResorcePath;
            }
        }

        private long SendPingUrlRequest(string pingUrl, fDMerchantDto fdMerchantDto, DatawireDto datawireDto)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                double transTime;
                DataWireEntities = new DatawireEntities
                {
                    AppID = fdMerchantDto.App,
                    MID = fdMerchantDto.FDMerchantID,
                    TID = datawireDto.RCTerminalId,
                    ServiceId = Global.ServiceID,
                    ClientRefId = CnTransactionMessage.GetCanadaClientRef(),
                    AuthID = fdMerchantDto.FDMerchantID + "|" + datawireDto.RCTerminalId,
                    DID = datawireDto.DID,
                    TransactionTime = DateTime.Now
                };

                var xmlResponse = SendHttpRequest(DataWireRequestType.Ping, out transTime, pingUrl: pingUrl);

                #region Response hanldling

                if (string.IsNullOrEmpty(xmlResponse))
                {
                    return 0;
                }

                var xEle = XElement.Parse(xmlResponse);

                //extract the time of transaction from xml response
                var time = (from p in xEle.Descendants(MessageTypeDW.ServiceCost.ToString())
                            let element = p.Element(MessageTypeDW.TransactionTimeMs.ToString())
                            where element != null
                            select new
                            {
                                TransactionTimeMs = Convert.ToString(element.Value, CultureInfo.InvariantCulture)
                            }).ToList();

                long transactionTime;
                var isLongTransTime = long.TryParse(time[0].TransactionTimeMs, out transactionTime);
                transactionTime = transactionTime + (long)transTime;
                if (isLongTransTime)//for CA1806
                {
                    return transactionTime;
                }

                #endregion
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
                return 0;
            }

            return 0;
        }

        public ServiceProviderUrlDto AssignDatawireEntitiesToServiceProvider(Collection<DatawireServiceUrls> datawireService, string activeUrl)
        {
            ServiceProviderUrlDto serviceProviderUrlDto = new ServiceProviderUrlDto();
            List<DatawireServiceUrlDto> datawireUrlDto = new List<DatawireServiceUrlDto>();
            foreach (DatawireServiceUrls datawireUrl in datawireService)
            {
                datawireUrlDto.Add(AssignData(datawireUrl));
            }

            serviceProviderUrlDto.ServiceProviderUrls = datawireUrlDto;
            serviceProviderUrlDto.ActiveUrl = activeUrl;
            return serviceProviderUrlDto;
        }

        public DatawireServiceUrlDto AssignData(DatawireServiceUrls serviceUrls)
        {
            return new DatawireServiceUrlDto { Url = serviceUrls.Url, TransactionTime = serviceUrls.TransactionTime, MaximumTransactionInPackage = serviceUrls.MaximumTransactionInPackage };
        }

    }
}
