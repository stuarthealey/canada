﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
    public class GetMerchantSummaryReportDto
    {
        public Nullable<long> SrNo { get; set; }
        public string FleetName { get; set; }
        public string VehicleNumber { get; set; }
        public Nullable<int> NoOfTrans { get; set; }
        public string FareAmt { get; set; }
        public string Tips { get; set; }
        public string Surcharges { get; set; }
        public string TechFee { get; set; }
        public Nullable<int> NoOfChargebacks { get; set; }
        public string ChargebackValue { get; set; }
        public Nullable<int> NoOfVoid { get; set; }
        public string VoidValue { get; set; }
        public string TotalAmount { get; set; }
        public string Fees { get; set; }
        public string AmountOwing { get; set; }
    }
}
