﻿using MTD.Core.DataTransferObjects;
using User = MTD.Data.Data.User;

namespace MTD.Core.Factory.Interface
{
    public interface IAdminUserFactory
    {
        User CreateAdminUser(UserDto adminUserDto);
    }
}
