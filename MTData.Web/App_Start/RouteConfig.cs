﻿using System.Web.Mvc;
using System.Web.Routing;

namespace MTData.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "FailWhale",
               url: "FailWhale/{action}/{id}",
               defaults: new
               {
                   controller = "Error",
                   action = "FailWhale",
                   id = UrlParameter.Optional
               }
           );

            routes.MapRoute("Default", "{controller}/{action}/{id}", new { controller = "Member", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
