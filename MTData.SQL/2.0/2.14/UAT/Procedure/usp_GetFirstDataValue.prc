
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetFirstDataValue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetFirstDataValue]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetFirstDataValue]    Script Date: 11/2/2015 10:59:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <15-06-2015>
-- Description:	<Get first data details>
-- =============================================
CREATE proc [dbo].[usp_GetFirstDataValue]
@deviceId varchar(100)=null, 
@ResultType int
AS 
BEGIN 
	IF(@ResultType=1) 
	BEGIN 
		select tmnl.TerminalID, tmnl.SerialNo, tmnl.DeviceName, tmnl.MacAdd, tmnl.fk_MerchantID, tmnl.IsTerminalAssigned, tmnl.DatawireId, 
		fdMer.FDId,fdMer.FDMerchantID, fdMer.GroupId, fdMer.serviceId, fdMer.ProjectId,fdMer.EcommProjectId,fdmer.ProjectType,fdMer.App, fdMer.IsAssigned,fdMer.TokenType,fdMer.EncryptionKey,fdMer.MCCode, 
		dwr.ID, dwr.DID, dwr.DatewireXml, dwr.MerchantId, dwr.RCTerminalId, dwr.Stan, dwr.RefNumber
		from Terminal tmnl 
		inner join Merchant mer on mer.MerchantID=tmnl.fk_MerchantID 
		inner join FDMerchant fdMer on fdMer.FDId=mer.Fk_FDId 
		inner join Datawire dwr on dwr.ID= tmnl.DatawireId 
		WHERE 
		tmnl.MacAdd=@deviceId and 
		tmnl.IsActive=1 
		and mer.IsActive=1
	END
	ELSE IF(@ResultType=0) 
	BEGIN 
		select tmnl.TerminalID, tmnl.SerialNo, tmnl.DeviceName, tmnl.MacAdd, tmnl.fk_MerchantID, tmnl.IsTerminalAssigned, tmnl.DatawireId, 
		fdMer.FDId,fdMer.FDMerchantID, fdMer.GroupId, fdMer.serviceId, fdMer.ProjectId,fdMer.EcommProjectId,fdmer.ProjectType,fdMer.App, fdMer.IsAssigned,fdMer.TokenType,fdMer.EncryptionKey,fdMer.MCCode, 
		dwr.ID, dwr.DID, dwr.DatewireXml, dwr.MerchantId, dwr.RCTerminalId, dwr.Stan, dwr.RefNumber
		from Terminal tmnl 
		inner join Merchant mer on mer.MerchantID=tmnl.fk_MerchantID 
		inner join FDMerchant fdMer on fdMer.FDId=mer.Fk_FDId 
		inner join Datawire dwr on dwr.ID= tmnl.DatawireId 
		WHERE 
		tmnl.MacAdd='-420' and 
		tmnl.IsActive=1 
		and mer.IsActive=1
	END 
END

GO