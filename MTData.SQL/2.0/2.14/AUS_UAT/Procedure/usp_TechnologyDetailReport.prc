
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_TechnologyDetailReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_TechnologyDetailReport]
GO

/****** Object:  StoredProcedure [dbo].[usp_TechnologyDetailReport]    Script Date: 11/2/2015 11:06:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <05-10-2015>
-- Description:	<Prepare technology fee transactions report>
-- =============================================
CREATE PROCEDURE [dbo].[usp_TechnologyDetailReport] 
	-- Add the parameters for the stored procedure here
	@MerchantId INT=NULL,
    @FleetId INT=NULL,
	@VehicleNo NVARCHAR(80)=NULL,
	@DriverNo NVARCHAR(80)=NULL,
	@DateFrom DATETIME=NULL,
	@DateTo DATETIME=NULL,	
	@CardType NVARCHAR(20) = NULL
AS
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	SELECT (SELECT COALESCE(Company,'') 
	        FROM dbo.Merchant 
			WHERE MerchantID=T.MerchantId
		    ) AS 'Merchant'
			,  T.fk_FleetId
	      ,(SELECT COALESCE(FleetName,'') 
		    FROM dbo.Fleet 
			WHERE FleetID=T.fk_FleetId
		   ) AS 'FleetName'
		  ,(SELECT COALESCE(PayType,'') 
		    FROM dbo.Fleet 
			WHERE FleetID=T.fk_FleetId
		    ) AS 'PayType'
		  ,T.Id AS 'TransId'
		  ,COALESCE(CAST(T.TxnDate AS DATE),'') AS 'TxnDate'
		  ,COALESCE(T.VehicleNo,'') AS 'VehicleNo'
		  ,COALESCE(T.DriverNo,'') AS 'DriverNo'
		  ,COALESCE(T.TxnType,'') AS 'TxnType'
		  ,CONCAT('$',CAST(COALESCE(T.Amount,0.00) AS DECIMAL(18,2))) AS 'Amount'
		  ,COALESCE(T.CardType,'') AS 'CardType'
		  ,CONCAT('$',CAST(COALESCE(T.TechFee,0.00) AS DECIMAL(18,2))) AS 'TechFee'
		  ,CONCAT('$',CAST(COALESCE(T.FleetFee,0.00) AS DECIMAL(18,2))) AS 'FleetFee'
		  ,CONCAT('$',CAST(COALESCE(T.Surcharge,0.00) AS DECIMAL(18,2))) AS 'Surcharge'
		  ,COALESCE(T.AuthId,'') AS 'AuthId'
	FROM [MTDTransaction] T
	WHERE (T.MerchantId = @MerchantId OR COALESCE(@MerchantId,'') = '')
	AND (T.fk_FleetId = @FleetId OR COALESCE(@FleetId,'') = '')
	AND (T.VehicleNo = @VehicleNo OR COALESCE(@VehicleNo,'') = '')
	AND (T.DriverNo = @DriverNo OR COALESCE(@DriverNo,'') = '')
	AND (T.TxnDate >= @DateFrom OR @DateFrom IS NULL)
	AND (T.TxnDate <= @DateTo OR @DateTo IS NULL)
	AND (T.CardType = @CardType OR COALESCE(@CardType,'') = '')
	AND T.fk_FleetId IS NOT NULL
	AND T.MerchantId IS NOT NULL
	AND T.ResponseCode IN ('000','002')
	OPTION (RECOMPILE)
END TRY
BEGIN CATCH
     INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
     SELECT ERROR_NUMBER()
	       ,ERROR_SEVERITY()
		   ,ERROR_STATE()
		   ,ERROR_PROCEDURE()
		   ,ERROR_LINE()
		   ,ERROR_MESSAGE()
		   ,SUSER_SNAME()
		   ,HOST_NAME()
		   ,GETDATE()
END CATCH


GO


