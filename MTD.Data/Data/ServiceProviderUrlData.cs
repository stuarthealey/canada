﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Data.Data
{
    public class ServiceProviderUrlData
    {
        public List<DatawireServiceUrlData> ServiceProviderUrls { get; set; }
        public string ActiveUrl { get; set; }
       
    }
}
