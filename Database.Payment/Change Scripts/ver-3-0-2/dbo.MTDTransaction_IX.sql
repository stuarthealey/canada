-- Create indexes on MTDTransaction for performance.
--==================================================

-- Redundent if we are using TxnDate, fk_FleetId index instead.
--IF NOT EXISTS (SELECT * FROM sysindexes si INNER JOIN sysobjects so ON so.id = si.id WHERE si.name = 'IX_MTDTransactionFleetDate' AND OBJECTPROPERTY(so.id, N'IsUserTable') = 1 AND so.name = 'MTDTransaction')
--BEGIN
--	PRINT 'Creating Index IX_MTDTransactionFleetDate on MTDTransaction'

--	CREATE NONCLUSTERED INDEX IX_MTDTransactionFleetDate ON dbo.MTDTransaction (fk_FleetId, TxnDate)
--		WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--END
--ELSE
--BEGIN
--	PRINT 'Index IX_MTDTransactionFleetDate on MTDTransaction already exists'
--END

IF NOT EXISTS (SELECT * FROM sysindexes si INNER JOIN sysobjects so ON so.id = si.id WHERE si.name = 'IX_MTDTransactionTxnType' AND OBJECTPROPERTY(so.id, N'IsUserTable') = 1 AND so.name = 'MTDTransaction')
BEGIN
	PRINT 'Creating Index IX_MTDTransactionTxnType on MTDTransaction'

	CREATE NONCLUSTERED INDEX IX_MTDTransactionTxnType ON dbo.MTDTransaction (TxnType, IsCompleted)
		WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
ELSE
BEGIN
	PRINT 'Index IX_MTDTransactionTxnType on MTDTransaction already exists'
END

--SH 2016-11-08
-- Massive performance issues identified with reports on MTDTransaction selecting on the TxnDate column as the primary selection criteria.
IF NOT EXISTS (SELECT * FROM sysindexes si INNER JOIN sysobjects so ON so.id = si.id WHERE si.name = 'IX_MTDTransactionTxnDate' AND OBJECTPROPERTY(so.id, N'IsUserTable') = 1 AND so.name = 'MTDTransaction')
BEGIN
	PRINT 'Creating Index IX_MTDTransactionTxnDate on MTDTransaction'

	CREATE NONCLUSTERED INDEX IX_MTDTransactionTxnDate ON dbo.MTDTransaction (TxnDate, fk_FleetId)
		WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
ELSE
BEGIN
	PRINT 'Index IX_MTDTransactionTxnDate on MTDTransaction already exists'
END
