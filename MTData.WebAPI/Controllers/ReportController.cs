﻿using System;
using System.Xml;
using System.Text;
using System.Net.Http;
using System.Web.Http;

using Ninject;

using MTD.Core.Service.Interface;
using MTD.Core.DataTransferObjects;
using MTData.Utility;
using MTData.WebAPI.Models;
using MTData.WebAPI.Resources;
using MTData.WebAPI.Common;

namespace MTData.WebAPI.Controllers
{
    public class ReportController : ApiController
    {
        #region Constructor Loading

        readonly ILogger _logger = new Logger();
        private readonly IDriverService _driverService;

        private readonly ITerminalService _terminalService;
        private readonly IMerchantService _merchantService;
        private readonly IReceiptService _receiptService;
        private readonly IVehicleService _vehicleService;

        private readonly ITransactionService _transactionService;
        private StringBuilder _logMessage;
        private readonly ICommonService _commonService;

        private DownloadParametersDto objLocalVars = new DownloadParametersDto();
        DownloadResponse response = new DownloadResponse();

        //zzz Chetu 357
        //public ReportController([Named("DriverService")] IDriverService driverService,
        //    [Named("CommonService")] ICommonService commonService,
        //    [Named("TransactionService")] ITransactionService transactionService,
        //    StringBuilder logMessage, StringBuilder logTracking)
        public ReportController([Named("DriverService")] IDriverService driverService, [Named("TerminalService")] ITerminalService terminalService,
            [Named("MerchantService")] IMerchantService merchantService,
            [Named("ReceiptService")] IReceiptService receiptService, [Named("VehicleService")] IVehicleService vehicleService, [Named("CommonService")] ICommonService commonService,
            [Named("TransactionService")] ITransactionService transactionService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _driverService = driverService;
            _terminalService = terminalService;
            _merchantService = merchantService;
            _receiptService = receiptService;
            _vehicleService = vehicleService;

            _transactionService = transactionService;
            _commonService = commonService;
            _logMessage = logMessage;
        }

        #endregion

        #region (OnDemandReport)

        /// <summary>
        /// Purpose             :   Android login and sending receipt information
        /// Function Name       :   SignIn
        /// Created By          :   Salil Gupta
        /// Created On          :   12/15/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/01/2015 "MM/DD/YYYY"
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult OnDemandReport(HttpRequestMessage request)
        {
            OnDemadReportResponse onDemandResponse = new OnDemadReportResponse();
            OnDemadReportRequest onDemand = new OnDemadReportRequest();
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                onDemand.DriverNumber = xmlDoc.SelectSingleNode("OnDemandReport/DriverNumber").InnerText;
                onDemand.ReportType = Convert.ToInt32(xmlDoc.SelectSingleNode("OnDemandReport/ReportType").InnerText);
                onDemand.ReportDate = Convert.ToDateTime(xmlDoc.SelectSingleNode("OnDemandReport/ReportDate").InnerText);
                onDemand.Token = xmlDoc.SelectSingleNode("OnDemandReport/Token").InnerText;
                onDemand.FleetId = xmlDoc.SelectSingleNode("OnDemandReport/FleetId").InnerText;

                bool isToken = string.IsNullOrEmpty(onDemand.Token);
                if (isToken)
                {
                    onDemandResponse.DriverNumber = onDemand.DriverNumber;
                    onDemandResponse.Message = PaymentAPIResources.UnauthorizedAccess;
                    onDemandResponse.Status = PaymentAPIResources.Failed;
                    return Ok(onDemandResponse);
                }

                bool isTokenValid = _commonService.IsTokenValid(onDemand.DriverNumber, onDemand.Token);
                if (!isTokenValid)
                {
                    onDemandResponse.DriverNumber = onDemand.DriverNumber;
                    onDemandResponse.Message = PaymentAPIResources.UnauthorizedAccess;
                    onDemandResponse.Status = PaymentAPIResources.Failed;
                    return Ok(onDemandResponse);
                }

                char[] deliem = { '^' };
                char[] deliemDriver = { ':' };
                string[] encArray = onDemand.Token.Split(deliem);
                string encDriverAndPin = encArray[1];
                string[] encArrayDriver = encDriverAndPin.Split(deliemDriver);
                string encDriverPin = encArrayDriver[1];
                string driverPin = StringExtension.DecryptDriver(encDriverPin);

                var driverDto = _driverService.GetDriverDto(onDemand.DriverNumber, Convert.ToInt32(onDemand.FleetId));
                if (driverDto != null)
                {
                    bool isValid = StringExtension.ValidatePassCode(driverPin, driverDto.PIN);
                    if (isValid)
                    {
                        int driverId = driverDto.DriverID;
                        int response = _commonService.OnDemandReport(driverId, onDemand.ReportType, onDemand.ReportDate);
                        if (response == 2)
                        {
                            onDemandResponse.DriverNumber = onDemand.DriverNumber;
                            onDemandResponse.Message = PaymentAPIResources.No_Driver_Transaction;
                            onDemandResponse.Status = PaymentAPIResources.Failed;
                        }
                        else if (response == 0)
                        {
                            onDemandResponse.DriverNumber = onDemand.DriverNumber;
                            onDemandResponse.Message = PaymentAPIResources.Driver_Report_will_be_sent;
                            onDemandResponse.Status = PaymentAPIResources.Success;
                        }
                        else if (response == 3)
                        {
                            onDemandResponse.DriverNumber = onDemand.DriverNumber;
                            onDemandResponse.Message = PaymentAPIResources.NotRecipient;
                            onDemandResponse.Status = PaymentAPIResources.Failed;
                        }
                        else
                        {
                            onDemandResponse.DriverNumber = onDemand.DriverNumber;
                            onDemandResponse.Message = PaymentAPIResources.Driver_Invalid_data;
                            onDemandResponse.Status = PaymentAPIResources.Failed;
                        }

                        return Ok(onDemandResponse);
                    }
                    else
                    {
                        onDemandResponse.DriverNumber = onDemand.DriverNumber;
                        onDemandResponse.Message = PaymentAPIResources.Invalid_token;
                        onDemandResponse.Status = PaymentAPIResources.Failed;
                        
                        return Ok(onDemandResponse);
                    }
                }
                else
                {
                    onDemandResponse.DriverNumber = onDemand.DriverNumber;
                    onDemandResponse.Message = PaymentAPIResources.Invalid_driver_No;
                    onDemandResponse.Status = PaymentAPIResources.Failed;
                    
                    return Ok(onDemandResponse);
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                onDemandResponse.DriverNumber = onDemand.DriverNumber;
                onDemandResponse.Message = ex.ToString();
                onDemandResponse.Status = PaymentAPIResources.Failed;

                return Ok(onDemandResponse);
            }
        }

        #endregion

        #region (GetTransactionData)

        [HttpPost]
        public IHttpActionResult GetTransactionData(HttpRequestMessage request)
        {
            try
            {
                _logMessage.AppendLine("GetTransactionData process start.");

                // Initialize local variable from XML Request.
                XmlDocument xmlDoc = new XmlDocument();
                _logMessage.AppendLine("Loading request XML data.");

                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                _logMessage.AppendLine("Initializing local variables from XML request data");
                if (!SetLocalVarFromXMLRequest(xmlDoc))
                {
                    _logMessage.AppendLine("Failed to initializ local variables from request XML data.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    return Ok(response);
                }

                _logMessage.AppendLine("Data download process start. DriverNo: " + objLocalVars.DriverId + " and  VehicleId " + objLocalVars.VehicleId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());
                
                // AuthenticateRequest
                _logMessage.AppendLine("Authenticating request.");

                switch (AuthenticateRequest())
                {
                    case 0:
                        _logMessage.AppendLine("Request authentication failed." + PaymentAPIResources.AuthenticationFailed);
                        _logger.LogInfoMessage(_logMessage.ToString());
                        
                        response = CommonFunctions.DownloadDataResponse(message: PaymentAPIResources.AuthenticationFailed);
                        return Ok(response);

                    case 2:
                        _logMessage.AppendLine("Request authentication failed." + PaymentAPIResources.InvalidMerchant);
                        _logger.LogInfoMessage(_logMessage.ToString());

                        response = CommonFunctions.DownloadDataResponse(message: PaymentAPIResources.InvalidMerchant);
                        return Ok(response);
                }

                // ValidateFleetId
                _logMessage.AppendLine("Validating FleetId.");

                if (!ValidateFleet())
                {
                    _logMessage.AppendLine("FleetId validation failed.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    response = CommonFunctions.DownloadDataResponse(message: PaymentAPIResources.FleetIdRequired);
                    return Ok(response);
                }

                _logMessage.AppendLine("Fetching record from database.");
                
                var dData = _transactionService.DownloadTransactionData(objLocalVars);

                _logMessage.AppendLine("Preparing reponse.");
                
                var result = CommonFunctions.DownloadDataResponse(result: dData, status: PaymentAPIResources.Success);

                _logMessage.AppendLine("GetTransactionData process complete.");
                
                _logMessage.AppendLine("Data download process complete. DriverNo: " + objLocalVars.DriverId + " and  VehicleId " + objLocalVars.VehicleId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());
                
                _logger.LogInfoMessage(_logMessage.ToString());

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                response = CommonFunctions.DownloadDataResponse(message: PaymentAPIResources.Exception);
                return Ok(response);
            }
        }

        /// <summary>
        /// Purpose             :   Extract the value from XMLrequest and set to Local variables
        /// Function Name       :   SetLocalVarFromXMLRequest
        /// Created By          :   Sunil Singh
        /// Created On          :   11/23/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private bool SetLocalVarFromXMLRequest(XmlDocument xmlDoc)
        {
            try
            {
                // UserId
                var dvcID = xmlDoc.SelectSingleNode("GetTransactionData/UserId");
                if (!dvcID.IsNull())
                    objLocalVars.UserId = dvcID.InnerText;
                else
                {
                    response = CommonFunctions.DownloadDataResponse(message: "GetTransactionData/UserId".MissingElementMessage());
                    return false;
                }

                // Password
                var pwd = xmlDoc.SelectSingleNode("GetTransactionData/Password");
                if (!pwd.IsNull())
                    objLocalVars.PCode = pwd.InnerText;
                else
                {
                    response = CommonFunctions.DownloadDataResponse(message: "GetTransactionData/Password".MissingElementMessage());
                    return false;
                }

                // MerchantID
                var mid = xmlDoc.SelectSingleNode("GetTransactionData/MerchantID");
                if (!mid.IsNull())
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(mid.InnerText))
                            objLocalVars.Fk_MerchantID = Convert.ToInt32(mid.InnerText);
                        else
                        {
                            response = CommonFunctions.DownloadDataResponse(message: PaymentAPIResources.MerchantIdRequired);
                            return false;
                        }
                    }
                    catch (FormatException)
                    {
                        response = CommonFunctions.DownloadDataResponse(message: "GetTransactionData/MerchantID".InvalidFormat());
                        return false;
                    }
                }
                else
                {
                    response = CommonFunctions.DownloadDataResponse(message: "GetTransactionData/MerchantID".MissingElementMessage());
                    return false;
                }

                // StartDate
                var dfFrom = xmlDoc.SelectSingleNode("GetTransactionData/StartDate");
                if (!dfFrom.IsNull())
                {
                    if (!string.IsNullOrEmpty(dfFrom.InnerText))
                    {
                        try
                        {
                            var dtStrt = DateTime.ParseExact(dfFrom.InnerText, "yyyy-MM-dd HH:mm:ss", null);
                            objLocalVars.DateFrom = Convert.ToDateTime(dtStrt);
                        }
                        catch (FormatException)
                        {
                            response = CommonFunctions.DownloadDataResponse(message: PaymentAPIResources.InvalidDateFormat);
                            return false;
                        }
                    }
                }
                else
                {
                    response = CommonFunctions.DownloadDataResponse(message: "GetTransactionData/StartDate".MissingElementMessage());
                    return false;
                }

                // EndDate
                var dtEnd = xmlDoc.SelectSingleNode("GetTransactionData/EndDate");
                if (!dtEnd.IsNull())
                {
                    if (!string.IsNullOrEmpty(dtEnd.InnerText))
                    {
                        try
                        {
                            var dtEndNew = DateTime.ParseExact(dtEnd.InnerText, "yyyy-MM-dd HH:mm:ss", null);
                            objLocalVars.DateTo = Convert.ToDateTime(dtEndNew);
                        }
                        catch (FormatException)
                        {
                            response = CommonFunctions.DownloadDataResponse(message: PaymentAPIResources.InvalidDateFormat);
                            return false;
                        }
                    }
                    else
                        objLocalVars.DateTo = null;
                }
                else
                {
                    response = CommonFunctions.DownloadDataResponse(message: "GetTransactionData/EndDate".MissingElementMessage());
                    return false;
                }

                // IndustryType
                var entryMode = xmlDoc.SelectSingleNode("GetTransactionData/EntryMode");
                if (!entryMode.IsNull())
                    objLocalVars.EntryMode = entryMode.InnerText;
                else
                {
                    response = CommonFunctions.DownloadDataResponse(message: "GetTransactionData/EntryMode".MissingElementMessage());
                    return false;
                }

                // DriverNumber
                var dvrNo = xmlDoc.SelectSingleNode("GetTransactionData/DriverNumber");
                if (!dvrNo.IsNull())
                    objLocalVars.DriverId = dvrNo.InnerText;
                else
                {
                    response = CommonFunctions.DownloadDataResponse(message: "GetTransactionData/DriverNumber".MissingElementMessage());
                    return false;
                }

                // VehicleNumber
                var vhclNo = xmlDoc.SelectSingleNode("GetTransactionData/VehicleNumber");
                if (!vhclNo.IsNull())
                    objLocalVars.VehicleId = vhclNo.InnerText;
                else
                {
                    response = CommonFunctions.DownloadDataResponse(message: "GetTransactionData/VehicleNumber".MissingElementMessage());
                    return false;
                }

                // FleetID
                var fleet = xmlDoc.SelectSingleNode("GetTransactionData/FleetID");
                if (!fleet.IsNull())
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(fleet.InnerText))
                            objLocalVars.Fk_FleetId = Convert.ToInt32(fleet.InnerText);
                        else
                            objLocalVars.Fk_FleetId = null;
                    }
                    catch (FormatException)
                    {
                        response = CommonFunctions.DownloadDataResponse(message: "GetTransactionData/FleetID".InvalidFormat());
                        return false;
                    }
                }
                else
                {
                    response = CommonFunctions.DownloadDataResponse(message: "GetTransactionData/FleetID".MissingElementMessage());
                    return false;
                }

                // TransactionType
                var txnType = xmlDoc.SelectSingleNode("GetTransactionData/TransactionType");
                if (!txnType.IsNull())
                    objLocalVars.TransType = txnType.InnerText;
                else
                {
                    response = CommonFunctions.DownloadDataResponse(message: "GetTransactionData/TransactionType".MissingElementMessage());
                    return false;
                }

                // TransactionStatus                
                var txnStatus = xmlDoc.SelectSingleNode("GetTransactionData/DownloadStatus");
                if (!txnStatus.IsNull())
                    objLocalVars.DownloadStatus = txnStatus.InnerText;
                else
                {
                    response = CommonFunctions.DownloadDataResponse(message: "GetTransactionData/DownloadStatus".MissingElementMessage());
                    return false;
                }

                // BatchNumber
                var batchNumber = xmlDoc.SelectSingleNode("GetTransactionData/BatchNumber");
                if (!batchNumber.IsNull())
                {
                    if (!string.IsNullOrEmpty(batchNumber.InnerText))
                        objLocalVars.BatchNumber = Convert.ToInt32(batchNumber.InnerText);
                    else
                        objLocalVars.BatchNumber = null;
                }
                else
                {
                    response = CommonFunctions.DownloadDataResponse(message: "GetTransactionData/BatchNumber".MissingElementMessage());
                    return false;
                }
                
                if (objLocalVars.BatchNumber.IsNull() && objLocalVars.DateFrom.IsNull())
                {
                    response = CommonFunctions.DownloadDataResponse(message: PaymentAPIResources.BtchOrDateRqrd);
                    return false;
                }

                // RetrieveSinceBatch
                var retrieveSinceBatch = xmlDoc.SelectSingleNode("GetTransactionData/RetrieveSinceBatch");
                if (!retrieveSinceBatch.IsNull())
                {
                    if (!string.IsNullOrEmpty(retrieveSinceBatch.InnerText))
                        objLocalVars.RetrieveSinceBatch = Convert.ToBoolean(retrieveSinceBatch.InnerText);
                    else
                        objLocalVars.RetrieveSinceBatch = false;
                }
                else
                    objLocalVars.RetrieveSinceBatch = false;

                return true;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>       
        /// Purpose             :   To authenticate the requset data.
        /// Function Name       :   AuthenticateRequest
        /// Created By          :   Sunil Singh
        /// Created On          :   11/23/2015
        /// Modification Made   :   
        /// Modified By         :   
        /// Modified At         :  
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="argElements"></param>
        /// <param name="ns2"></param>
        /// <returns></returns>
        private int AuthenticateRequest()
        {
            try
            {
                if (!string.IsNullOrEmpty(objLocalVars.UserId) && !string.IsNullOrEmpty(objLocalVars.PCode) && !objLocalVars.Fk_MerchantID.IsNull())
                    return _transactionService.ValidateDownloadRequest(objLocalVars.UserId, objLocalVars.PCode, Convert.ToInt32(objLocalVars.Fk_MerchantID));
                else
                    return 0;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// If VehicleId or DriverId is supplied then FleetId must be there.
        /// </summary>
        /// <returns></returns>
        private bool ValidateFleet()
        {
            try
            {
                bool isValid = false;
                if (!string.IsNullOrEmpty(objLocalVars.VehicleId) || !string.IsNullOrEmpty(objLocalVars.DriverId))
                {
                    if (!objLocalVars.Fk_FleetId.IsNull())
                        isValid = true;
                    else
                        isValid = false;
                }
                else
                    isValid = true;

                return isValid;
            }
            catch
            {
                throw;
            }
        }

        #endregion
    }
}
