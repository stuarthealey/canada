IF NOT EXISTS (SELECT 1 FROM dbo.Currency WHERE CurrencyCode IN (44))
	INSERT INTO dbo.Currency VALUES ('44','United Kingdom Pound','GBP','�')
GO
 
-- Admin table.
---------------
begin tran abc
declare @tempMer table(id int identity(1,1),  UserId int )

insert @tempMer select distinct fk_UserID from Recipient  where fk_UserTypeID in(1) and fk_MerchantID is null 

--select * from @tempMer
declare @maxCount int,@min int=1,@merId int, @UserType int, @userID int
select @maxCount = count(*) from @tempMer
while (@min <= @maxCount)
BEGIN
	select  @userID=UserId
	from @tempMer where id=@min

	if not exists(select 1 from Recipient where fk_UserID=@userID and fk_UserTypeID=1 and fk_ReportID in(7))
		insert into Recipient values(null,@userID,1,'admin@mtdata.com',GETDATE(),1,7)

	if not exists(select 1 from Recipient where fk_UserID=@userID and fk_UserTypeID=1 and fk_ReportID in(8))
		insert into Recipient values(null,@userID,1,'admin@mtdata.com',GETDATE(),1,8)

	set @min=@min+1
END
commit transaction abc

GO

-- CorporateUser table.
-----------------------
begin tran abc

declare @tempMer table(id int identity(1,1),  UserId int )

insert @tempMer
select distinct fk_UserID from Recipient  where fk_UserTypeID in(5) and fk_MerchantID is null and fk_UserID is not null

select * from @tempMer

declare @maxCount int,@min int=0,@merId int, @UserType int, @userID int
select @maxCount=count(*) from @tempMer
while (@min <= @maxCount)
BEGIN
	select  @userID=UserId
	from @tempMer where id=@min

	if not exists(select 1 from Recipient where fk_UserID=@userID and fk_UserTypeID=5 and fk_ReportID in(7))
	insert into Recipient values(null,@userID,1,'admin@mtdata.com',GETDATE(),5,7)

	if not exists(select 1 from Recipient where fk_UserID=@userID and fk_UserTypeID=5 and fk_ReportID in(8))
	insert into Recipient values(null,@userID,1,'admin@mtdata.com',GETDATE(),5,8)

	set @min=@min+1
END
commit transaction abc
GO

-- Merchant table.
------------------
begin tran abc

declare @tempMer table(id int identity(1,1),  UserId int )

insert @tempMer
select distinct fk_MerchantID from Recipient where fk_UserTypeID  in(2) and fk_UserID is null --second

select * from @tempMer

declare @maxCount int,@min int=1,@merId int, @UserType int, @userID int
select @maxCount=count(*) from @tempMer
while (@min <= @maxCount)
BEGIN
	select  @userID=UserId  from @tempMer where id=@min

	if not exists(select 1 from Recipient where fk_MerchantID=@userID and fk_UserTypeID=2 and fk_ReportID in(7))
	insert into Recipient values(@userID,null,1,'admin@mtdata.com',GETDATE(),2,7)

	if not exists(select 1 from Recipient where fk_MerchantID=@userID and fk_UserTypeID=2 and fk_ReportID in(8))
	insert into Recipient values(@userID,null,1,'admin@mtdata.com',GETDATE(),2,8)

	set @min=@min+1
end
commit transaction abc
GO

-- MerchantAdmin table.
-----------------------
begin tran abc

declare @tempMer table(id int identity(1,1), MId int,  UserId int )

insert @tempMer
select distinct fk_MerchantID, fk_UserID from Recipient where fk_UserTypeID in(3)-- first

select * from @tempMer

declare @maxCount int,@min int=0,@merId int, @UserType int, @userID int
select @maxCount=count(*) from @tempMer
while (@min <= @maxCount)
BEGIN
	select  @userID=UserId,@merId=MId
	from @tempMer where id=@min

	if not exists(select 1 from Recipient where fk_UserID=@userID and fk_MerchantID=@merId and fk_UserTypeID=3 and fk_ReportID in(7))
	insert into Recipient values(@merId,@userID,1,'admin@mtdata.com',GETDATE(),3,7)

	if not exists(select 1 from Recipient where fk_UserID=@userID and fk_MerchantID=@merId and fk_UserTypeID=3 and fk_ReportID in(8))
	insert into Recipient values(@merId,@userID,1,'admin@mtdata.com',GETDATE(),3,8)

	set @min=@min+1
END
commit transaction abc
GO

-- MerchantUser table.
----------------------

begin tran abc

declare @tempMer table(id int identity(1,1), MId int,  UserId int )

insert @tempMer select distinct fk_MerchantID, fk_UserID from Recipient where fk_UserTypeID in (4)-- first

--select * from @tempMer

declare @maxCount int,@min int=0,@merId int, @UserType int, @userID int

select @maxCount=count(*) from @tempMer

while (@min <= @maxCount)
BEGIN
	select  @userID=UserId,@merId=MId from @tempMer where id=@min

	if not exists(select 1 from Recipient where fk_UserID=@userID and fk_MerchantID=@merId and fk_UserTypeID=4 and fk_ReportID in(7))
	insert into Recipient values(@merId,@userID,1,'admin@mtdata.com',GETDATE(),4,7)

	if not exists(select 1 from Recipient where fk_UserID=@userID and fk_MerchantID=@merId and fk_UserTypeID=4 and fk_ReportID in(8))
	insert into Recipient values(@merId,@userID,1,'admin@mtdata.com',GETDATE(),4,8)

	set @min=@min+1
END
commit transaction abc
GO