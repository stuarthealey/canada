﻿using MTData.WebAPI.CanadaCommon;

namespace MTData.WebAPI.CanadaCert
{
    public static class CnBitmapDefinition
    {
        #region Public Properties

        /// <summary>
        /// Purpose             :   To get all the details related to bitmap 2
        /// Property Name       :   PrimaryAccountNumber
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/27/2016
        /// Modification  Made   :   ****************************
        /// Modified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <returns></returns> 
        public static Element PrimaryAccountNumber
        {
            get
            {
                return new Element
                {
                    ElementID = 2,
                    Format = FormatType.BCD,
                    IsRequired = false,
                    LengthMax = 19,
                    LengthMin = 0,
                    LengthType = LengthType.Variable,
                    LentghIndicator = 1,
                    Type = DataType.Numeric
                };
            }
        }

        /// <summary>
        /// Purpose             :   To get all the details related to bitmap 3
        /// Property Name       :   ProcessingCode
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/27/2016
        /// Modification  Made   :   ****************************
        /// Modified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <returns></returns>        
        public static Element ProcessingCode
        {
            get
            {
                return new Element
                {
                    ElementID = 3,
                    Format = FormatType.BCD,
                    IsRequired = true,
                    LengthMax = 6,
                    LengthMin = 6,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Numeric
                };
            }
        }

        /// <summary>
        /// Purpose             :   To get all the details related to bitmap 4
        /// Property Name       :   ProcessingCode
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/27/2016
        /// Modification  Made   :   ****************************
        /// Modified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <returns></returns>        
        public static Element AmountOfTransaction
        {
            get
            {
                return new Element
                {
                    ElementID = 4,
                    Format = FormatType.BCD,
                    IsRequired = true,
                    LengthMax = 12,
                    LengthMin = 12,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Numeric
                };
            }
        }

        /// <summary>
        /// Bitmap 7 format(MMDDhhmmss)
        /// </summary>
        public static Element TransmissionDateTime
        {
            get
            {
                return new Element
                {
                    ElementID = 7,
                    Format = FormatType.BCD,
                    IsRequired = false,
                    LengthMax = 10,
                    LengthMin = 10,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Numeric
                };
            }
        }

        /// <summary>
        /// Bitmap 11
        /// </summary>
        public static Element SystemTrace
        {
            get
            {
                return new Element
                {
                    ElementID = 11,
                    Format = FormatType.BCD,
                    IsRequired = true,
                    LengthMax = 6,
                    LengthMin = 6,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Numeric
                };
            }
        }

        /// <summary>
        /// Bitmap 12 format(hhmmss)
        /// </summary>
        public static Element TimeLocalTransmission
        {
            get
            {
                return new Element
                {
                    ElementID = 12,
                    Format = FormatType.BCD,
                    IsRequired = true,
                    LengthMax = 6,
                    LengthMin = 6,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Numeric
                };
            }
        }

        /// <summary>
        /// Bitmap 13 format(MMDD)
        /// </summary>
        public static Element DateLocalTrans
        {
            get
            {
                return new Element
                {
                    ElementID = 13,
                    Format = FormatType.BCD,
                    IsRequired = true,
                    LengthMax = 4,
                    LengthMin = 4,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Numeric
                };
            }
        }

        /// <summary>
        /// Bitmap 14 format(YYMM) default 0000
        /// </summary>
        public static Element CardExpirationDate
        {
            get
            {
                return new Element
                {
                    ElementID = 14,
                    Format = FormatType.BCD,
                    IsRequired = false,
                    LengthMax = 4,
                    LengthMin = 4,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Numeric
                };
            }
        }

        /// <summary>
        /// Bitmap 18
        /// </summary>
        public static Element MerchantCategoryCode
        {
            get
            {
                return new Element
                {
                    ElementID = 18,
                    Format = FormatType.BCD,
                    IsRequired = true,
                    LengthMax = 4,
                    LengthMin = 4,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Numeric
                };
            }
        }

        /// <summary>
        /// Bitmap 22
        /// </summary>
        public static Element PosEntryModePinCapability
        {
            get
            {
                return new Element
                {
                    ElementID = 22,
                    Format = FormatType.BCD,
                    IsRequired = true,
                    LengthMax = 3,
                    LengthMin = 3,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Numeric
                };
            }
        }

        /// <summary>
        /// Bitmap 23 range 000 -099
        /// </summary>
        public static Element CardSequenceNumber
        {
            get
            {
                return new Element
                {
                    ElementID = 23,
                    Format = FormatType.BCD,
                    IsRequired = false,
                    LengthMax = 4,
                    LengthMin = 4,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Numeric
                };
            }
        }

        /// <summary>
        /// Bitmap 24
        /// </summary>
        public static Element NetworkInternationId
        {
            get
            {
                return new Element
                {
                    ElementID = 24,
                    Format = FormatType.BCD,
                    IsRequired = true,
                    LengthMax = 3,
                    LengthMin = 3,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Numeric
                };
            }
        }

        /// <summary>
        /// Bitmap 25
        /// </summary>
        public static Element PosConditionCode
        {
            get
            {
                return new Element
                {
                    ElementID = 25,
                    Format = FormatType.BCD,
                    IsRequired = true,
                    LengthMax = 2,
                    LengthMin = 2,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Numeric
                };
            }
        }

        /// <summary>
        /// Bitmap 31
        /// </summary>
        public static Element AcquirerReference
        {
            get
            {
                return new Element
                {
                    ElementID = 31,
                    Format = FormatType.ASCII,
                    IsRequired = false,
                    LengthMax = 1,
                    LengthMin = 0,
                    LengthType = LengthType.Variable,
                    LentghIndicator = 1,
                    Type = DataType.Alphanumeric
                };
            }
        }

        /// <summary>
        /// Bitmap 32 depricated 
        /// Default value is 000000000001
        /// </summary>
        public static Element AcquiringId
        {
            get
            {
                return new Element
                {
                    ElementID = 32,
                    Format = FormatType.BCD,
                    IsRequired = false,
                    LengthMax = 12,
                    LengthMin = 0,
                    LengthType = LengthType.Variable,
                    LentghIndicator = 1,
                    Type = DataType.Numeric
                };
            }
        }

        /// <summary>
        /// Bitmap 35
        /// </summary>
        public static Element Track2Data
        {
            get
            {
                return new Element
                {
                    ElementID = 35,
                    Format = FormatType.BCD,
                    IsRequired = false,
                    LengthMax = 74,
                    LengthMin = 0,
                    LengthType = LengthType.Variable,
                    LentghIndicator = 1,
                    Type = DataType.None
                };
            }
        }

        /// <summary>
        /// Bitmap 37
        /// </summary>
        public static Element RetrievalRefNumber
        {
            get
            {
                return new Element
                {
                    ElementID = 37,
                    Format = FormatType.ASCII,
                    IsRequired = true,
                    LengthMax = 12,
                    LengthMin = 12,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Alphanumeric
                };
            }
        }

        /// <summary>
        /// Bitmap 38
        /// </summary>
        public static Element AuthIdentificationResponse
        {
            get
            {
                return new Element
                {
                    ElementID = 38,
                    Format = FormatType.ASCII,
                    IsRequired = false,
                    LengthMax = 6,
                    LengthMin = 6,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Alphanumeric
                };
            }
        }

        /// <summary>
        /// Bitmap 39
        /// </summary>
        public static Element ResponseCode
        {
            get
            {
                return new Element
                {
                    ElementID = 39,
                    Format = FormatType.ASCII,
                    IsRequired = false,
                    LengthMax = 2,
                    LengthMin = 2,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Alphanumeric
                };
            }
        }

        /// <summary>
        /// Bitmap 41
        /// </summary>
        public static Element TerminalId
        {
            get
            {
                return new Element
                {
                    ElementID = 41,
                    Format = FormatType.ASCII,
                    IsRequired = true,
                    LengthMax = 8,
                    LengthMin = 8,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Alphanumeric
                };
            }
        }

        /// <summary>
        /// Bitmap 42
        /// </summary>
        public static Element MerchantId
        {
            get
            {
                return new Element
                {
                    ElementID = 42,
                    Format = FormatType.ASCII,
                    IsRequired = true,
                    LengthMax = 15,
                    LengthMin = 15,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Alphanumeric
                };
            }
        }

        /// <summary>
        /// Bitmap 43
        /// </summary>
        public static Element AltMerchantNameOrLocation
        {
            get
            {
                return new Element
                {
                    ElementID = 43,
                    Format = FormatType.ASCII,
                    IsRequired = false,
                    LengthMax = 107,
                    LengthMin = 107,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Alphanumeric
                };
            }
        }

        /// <summary>
        /// Bitmap 44
        /// </summary>
        public static Element AdditionalResponseData
        {
            get
            {
                return new Element
                {
                    ElementID = 44,
                    Format = FormatType.ASCII,
                    IsRequired = false,
                    LengthMax = 1,
                    LengthMin = 1,
                    LengthType = LengthType.Variable,
                    LentghIndicator = 2,
                    Type = DataType.Alphanumeric
                };
            }
        }

        /// <summary>
        /// Bitmap 45
        /// </summary>
        public static Element Track1Data
        {
            get
            {
                return new Element
                {
                    ElementID = 45,
                    Format = FormatType.ASCII,
                    IsRequired = false,
                    LengthMax = 76,
                    LengthMin = 0,
                    LengthType = LengthType.Variable,
                    LentghIndicator = 1,
                    Type = DataType.Alphanumeric
                };
            }
        }

        /// <summary>
        /// Bitmap 48
        /// </summary>
        public static Element FDPrivateUsageData48
        {
            get
            {
                return new Element
                {
                    ElementID = 48,
                    LengthType = LengthType.Variable
                };
            }
        }

        /// <summary>
        /// Bitmap 49
        /// </summary>
        public static Element TransactionCurrencyCode
        {
            get
            {
                return new Element
                {
                    ElementID = 49,
                    Format = FormatType.BCD,
                    IsRequired = false,
                    LengthMax = 4,
                    LengthMin = 4,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Numeric
                };
            }
        }

        /// <summary>
        /// Bitmap 52
        /// </summary>
        public static Element EncryptedPinData
        {
            get
            {
                return new Element
                {
                    ElementID = 52,
                    Format = FormatType.HD,
                    IsRequired = false,
                    LengthMax = 64,
                    LengthMin = 64,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Binary
                };
            }
        }

        /// <summary>
        /// Bitmap 54
        /// </summary>
        public static Element AdditionalAmount
        {
            get
            {
                return new Element
                {
                    ElementID = 54,
                    Format = FormatType.ASCII,
                    IsRequired = false,
                    LengthMax = 12,
                    LengthMin = 0,
                    LengthType = LengthType.Variable,
                    LentghIndicator = 2,
                    Type = DataType.Numeric
                };
            }
        }

        /// <summary>
        /// Bitmap 55
        /// </summary>
        public static Element ICCData
        {
            get
            {
                return new Element
                {
                    ElementID = 55,
                    Format = FormatType.BCD,
                    IsRequired = false,
                    LengthMax = 999,
                    LengthMin = 0,
                    LengthType = LengthType.Variable,
                    LentghIndicator = 2,
                    Type = DataType.Numeric
                };
            }
        }

        /// <summary>
        /// Bitmap 59
        /// </summary>
        public static Element MerchantZipCode
        {
            get
            {
                return new Element
                {
                    ElementID = 59,
                    Format = FormatType.ASCII,
                    IsRequired = true,
                    LengthMax = 9,
                    LengthMin = 9,
                    LengthType = LengthType.Variable,
                    LentghIndicator = 1,
                    Type = DataType.Alphanumeric
                };
            }
        }

        /// <summary>
        /// Bitmap 60
        /// </summary>
        public static Element AdditionalPosData
        {
            get
            {
                return new Element
                {
                    ElementID = 60,
                    Format = FormatType.BCD,
                    IsRequired = false,
                    LengthMax = 2,
                    LengthMin = 2,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Numeric
                };
            }
        }

        /// <summary>
        /// Bitmap 62
        /// </summary>
        public static Element FDPrivateUsageData62
        {
            get
            {
                return new Element
                {
                    ElementID = 62,
                    LengthType = LengthType.Variable,
                    LentghIndicator = 2,
                    Type = DataType.Alphanumeric
                };
            }
        }

        /// <summary>
        /// Bitmap 63
        /// </summary>
        public static Element FDPrivateUsageData63
        {
            get
            {
                return new Element
                {
                    ElementID = 63,
                    LengthType = LengthType.Variable,
                    LentghIndicator = 2,
                    Type = DataType.Alphanumeric
                };
            }
        }

        /// <summary>
        /// Bitmap 70 (Mandatory for 0800/08100)
        /// </summary>
        public static Element NetworkInfoCode
        {
            get
            {
                return new Element
                {
                    ElementID = 70,
                    Format = FormatType.BCD,
                    IsRequired = false,
                    LengthMax = 3,
                    LengthMin = 3,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Numeric
                };
            }
        }

        /// <summary>
        /// Bitmap 93 (Mandatory for 0810)
        /// </summary>
        public static Element ResponseIndicator
        {
            get
            {
                return new Element
                {
                    ElementID = 93,
                    Format = FormatType.ASCII,
                    IsRequired = false,
                    LengthMax = 10,
                    LengthMin = 10,
                    LengthType = LengthType.Fixed,
                    LentghIndicator = 0,
                    Type = DataType.Alphanumeric
                };
            }
        }

        /// <summary>
        /// Bitmap 96
        /// </summary>
        public static Element KeyManagementData
        {
            get
            {
                return new Element
                {
                    ElementID = 96,
                    Format = FormatType.HB,
                    IsRequired = false,
                    LengthMax = 18,
                    LengthMin = 0,
                    LengthType = LengthType.Variable,
                    LentghIndicator = 1,
                    Type = DataType.Binary
                };
            }
        }

        /// <summary>
        /// Bitmap 100
        /// </summary>
        public static Element ReceivingInstitutionId
        {
            get
            {
                return new Element
                {
                    ElementID = 100,
                    Format = FormatType.BCD,
                    IsRequired = false,
                    LengthMax = 22,
                    LengthMin = 0,
                    LengthType = LengthType.Variable,
                    LentghIndicator = 1,
                    Type = DataType.Numeric
                };
            }
        }

        #endregion
    }
}