--PAY-13 Add UseTransArmor flag to Merchant table.
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Merchant' AND COLUMN_NAME = 'UseTransArmor')
BEGIN
	ALTER TABLE Merchant ADD UseTransArmor BIT NULL

	PRINT 'Merchant table added UseTransArmor'
END
ELSE
BEGIN
	PRINT 'Merchant table already has UseTransArmor'
END
