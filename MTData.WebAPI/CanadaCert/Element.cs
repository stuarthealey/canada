﻿using MTData.WebAPI.CanadaCommon;

namespace MTData.WebAPI.CanadaCert
{
    public class Element
    {
        #region Public Properties

        /// <summary>
        /// get or sets the ElementID of the ISO 8583 message format
        /// </summary>
        public int ElementID { get; set; }
        
        /// <summary>
        /// gets or sets the data type for the ISO 8583 message elements
        /// </summary>
        public DataType Type { get; set; }
        
        /// <summary>
        /// gets or sets the minimum length ISO 8583 message elements
        /// </summary>
        public int LengthMin { get; set; }
        
        /// <summary>
        /// gets or sets the max length for ISO 8583 message elements
        /// </summary>
        public int LengthMax { get; set; }
        
        /// <summary>
        /// gets or sets the format type for ISO 8583 message elements
        /// </summary>
        public FormatType Format { get; set; }
        
        /// <summary>
        /// gets or sets the length type for ISO 8583 message elements
        /// </summary>
        public LengthType LengthType { get; set; }
        
        /// <summary>
        /// gets or sets the Isrequired for ISO 8583 message elements
        /// </summary>
        public bool IsRequired { get; set; }
        
        /// <summary>
        /// gets or sets the Length Indicator for ISO 8583 message elements
        /// </summary>
        public int LentghIndicator { get; set; }

        #endregion
    }
}