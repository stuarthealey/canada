﻿using MTD.Core.DataTransferObjects;
namespace MTD.Core.Service.Interface
{
    public interface IMemberService
    {
        SessionDto Login(LoginDto login);

        SessionDto LoginAsAdmin(LoginDto login);

        UserDto GetPassword(string emailId);

        int UpdatePassword(LoginDto login);

        bool Reset(string email, string newPassword);

        bool PassCodeExpiration(string email);

        int ResetExpiredPassword(LoginDto login);

        bool IsLocked(string email);

        bool CountFailedAttempt(string email);

    }
}
