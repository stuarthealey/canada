﻿
namespace MTData.WebAPI.CnPrivateBit63
{
    public class TableSP_Retail
    {
        #region Constants

        private const string tableId = "|53|50";

        #endregion

        #region Variables

        private const string securityLevelTag = "|30|31";
        private const string securityLevelTagDataLength = "|30|30|31";
        private const string securityLevelTagValue = "|31";

        private const string encryptionTypeTag = "|30|32";
        private const string encryptionTypeTagDataLength = "|30|30|31";
        private const string encryptionTypeTagValue = "|31";

        private const string encryptionTargetTag = "|30|33";
        private const string encryptionTargetTagDataLength = "|30|30|31";
        private const string encryptionTargetTagValue = "|32";

        private const string keyIdTag = "|30|34";
        private const string keyIdTagDataLength = "|30|31|31";
        private string keyIdTagValue = "|30|30|30|30|30|30|30|30|30|30|30";//should be of 11 bytes

        private const string encryptionBlockTag = "|30|35";
        private string encryptionBlockTagDataLength = "|30|31|31";//Calculated length of Track 3 Data 
        private string encryptionBlockTagValue = "|30|30|30|30|30|30|30|30|30|30|30";//Track 3 Data

        private const string tokenTypeTag = "|30|36";
        private const string tokenTypeTagLength = "|30|30|34";
        private string tokenTypeValue = "|53|36|37|35";//"|S6|75";

        #endregion

        #region Public Properties

        /// <summary>
        /// ID of Table
        /// default X'36 30' Table 60
        /// </summary>
        public string TableID
        {
            get { return tableId; }
        }

        /// <summary>
        /// Gets or Sets the value for DCC Conversion Indicator
        /// </summary>
        public string SecurityLevelTag
        {
            get { return securityLevelTag; }
        }

        public string SecurityLevelTagDataLength
        {
            get { return securityLevelTagDataLength; }
        }

        /// <summary>
        /// Gets or Sets the value for DCC Transaction Time Zone
        /// </summary>
        public string SecurityLevelTagValue
        {
            get { return securityLevelTagValue; }
        }

        /// <summary>
        /// Gets or Sets the value for Foreign Conversion Rate
        /// </summary>
        public string EncryptionTypeTag
        {
            get { return encryptionTypeTag; }
        }

        public string EncryptionTypeTagDataLength
        {
            get { return encryptionTypeTagDataLength; }
        }

        /// <summary>
        /// Gets or Sets the value for Amount in Merchant Base Currency
        /// </summary>
        public string EncryptionTypeTagValue
        {
            get { return encryptionTypeTagValue; }
        }

        /// <summary>
        /// Gets or Sets the value for Foreign Conversion Rate
        /// </summary>
        public string EncryptionTargetTag
        {
            get { return encryptionTargetTag; }
        }

        public string EncryptionTargetTagDataLength
        {
            get { return encryptionTargetTagDataLength; }
        }

        /// <summary>
        /// Gets or Sets the value for Amount in Merchant Base Currency
        /// </summary>
        public string EncryptionTargetTagValue
        {
            get { return encryptionTargetTagValue; }
        }

        /// <summary>
        /// Gets or Sets the value for Foreign Conversion Rate
        /// </summary>
        public string KeyIdTag
        {
            get { return keyIdTag; }
        }

        public string KeyIdTagDataLength
        {
            get { return keyIdTagDataLength; }
        }

        /// <summary>
        /// Gets or Sets the value for Amount in Merchant Base Currency
        /// </summary>
        public string KeyIdTagValue
        {
            get { return keyIdTagValue; }
            set { keyIdTagValue = value; }
        }

        /// <summary>
        /// Gets or Sets the value for Foreign Conversion Rate
        /// </summary>
        public string EncryptionBlockTag
        {
            get { return encryptionBlockTag; }
        }

        public string EncryptionBlockTagDataLength
        {
            get { return encryptionBlockTagDataLength; }
            set { encryptionBlockTagDataLength = value; }
        }

        /// <summary>
        /// Gets or Sets the value for Amount in Merchant Base Currency
        /// </summary>
        public string EncryptionBlockTagValue
        {
            get { return encryptionBlockTagValue; }
            set { encryptionBlockTagValue = value; }
        }

        /// <summary>
        /// Gets or Sets the value for Foreign Conversion Rate
        /// </summary>
        public string TokenTypeTag
        {
            get { return tokenTypeTag; }
        }

        public string TokenTypeTagLength
        {
            get { return tokenTypeTagLength; }
        }

        /// <summary>
        /// Gets or Sets the value for Amount in Merchant Base Currency
        /// </summary>
        public string TokenTypeValue
        {
            get { return tokenTypeValue; }
            set { tokenTypeValue = value; }
        }

        #endregion
    }
}