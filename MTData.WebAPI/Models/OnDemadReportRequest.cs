﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Models
{
    public class OnDemadReportRequest
    {
        public string DriverNumber { get; set; }
        public string FleetId { get; set; }
        public int ReportType { get; set; } //1 for Daily, 2 for Weekly, 3 for Monthly
        public DateTime ReportDate { get; set; }
        public string Token { get; set; } 
    }
}