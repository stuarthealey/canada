﻿using System;
using System.Globalization;
using System.Text;
using System.Web.Http;
using MTD.Core.Service.Interface;
using MTData.Utility.Logging;
using MTData.Utility.Logging.InLogger;
using Ninject;

namespace MTData.WebAPI.Controllers
{
    public class BaseController : ApiController
    {
        private static ITerminalService _terminalService;
        private static StringBuilder _logMessage;
        static ILogger _logger = new Logger();

        public BaseController([Named("TerminalService")] ITerminalService terminalService, StringBuilder logMessage)
        {
            _terminalService = terminalService;
            _logMessage = logMessage;
        }
       
        /// <summary>
        /// Purpose             :   Get System trace number
        /// Function Name       :   GetSystemTraceNumber
        /// Created By          :   Umesh
        /// Created On          :   03/27/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="mId"></param>
        /// <param name="tId"></param>
        /// <returns></returns>
        public static string GetSystemTraceNumber(int mId, string tId)
        {
            try
            {
                var terminalStan = _terminalService.GetTerminalStanRef(mId, tId);
                int straceNumber = Convert.ToInt32(terminalStan.Stan) + 1;
                if (Convert.ToInt32(terminalStan.Stan) > 999999)
                {
                    straceNumber = 1;
                }
                string systemTrace = straceNumber.ToString(CultureInfo.InvariantCulture).PadLeft(6, '0');
                return systemTrace;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :   Get transaction ReferenceNumber
        /// Function Name       :   GetReferenceNumber
        /// Created By          :   Umesh
        /// Created On          :   03/27/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY"  
        /// </summary>
        /// <param name="mId"></param>
        /// <param name="tId"></param>
        /// <returns></returns>
        public static string GetReferenceNumber(int mId, string tId)
        {
            try
            {
                var terminalRef = _terminalService.GetTerminalStanRef(mId, tId);
                int timeDiff = Convert.ToInt32(DateTime.Now.Subtract(Convert.ToDateTime(terminalRef.TransactionDate)).ToString("hh"));
                string referenceNumber = timeDiff <= 24 ? Convert.ToString(Convert.ToInt32(terminalRef.RefNumber) + 1) : terminalRef.RefNumber;
                return referenceNumber.PadLeft(12, '0');
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;
        }
    }
}
