﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTDataDispatchSystemSync.Models
{
    public class ESIAuthenticateResult
    {
        public string Error { get; set; }
        public string ErrorMessage { get; set; }
        public bool Succeeded { get; set; }
        public string Token { get; set; }
    }
}