IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetDriversTransaction]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetDriversTransaction]
GO
--[GetDriversTransaction] null,null,null,null,'01/01/2015','02/26/2015'
CREATE  PROCEDURE [dbo].[usp_GetDriversTransaction]
	@MerchantId INT = NULL,
	@FleetId INT = NULL,
	@VehicleNumber NVARCHAR(50) = NULL,
	@DriverNumber NVARCHAR(50) = NULL,
	@DateFrom DATETime = NULL,
	@DateTo DATETime = NULL
AS
BEGIN
SELECT COALESCE(DriverNo,'') DriverNo, COALESCE(Fleet,'') Fleet
,COALESCE(SUM(NumberOfKeyed),0) NumberOfKeyed, COALESCE(SUM(NumberOfSwiped),0) NumberOfSwiped, COALESCE(SUM(TolalSumKeyed),0) AS KeyedSaleAmount,COALESCE(SUM(ToalSumSwiped),0) AS SwipedSaleAmount 
,COALESCE(SUM(NumberOfKeyed),0) + COALESCE(SUM(NumberOfSwiped),0) AS TotalNumberOfSale, COALESCE(SUM(TolalSumKeyed),0) + COALESCE(SUM(ToalSumSwiped),0) AS TotalSaleAmount,  COALESCE(SUM(TotalSumTechFee),0) TechFee
, COALESCE(SUM(TotalSumSurcharge),0) SumSurcharge,  COALESCE(SUM(TotalSumFleetFee),0)  SumFleetFee 
,MerchantName,DriverName,FleetID
FROM
(
	SELECT * FROM
	(
		SELECT DriverNo,Industry
		,COALESCE(F.FleetName,'') Fleet
		,CASE WHEN T.Industry='ecommerce' THEN COUNT(T.Industry) ELSE 0 END NumberOfKeyed
		,CASE WHEN T.Industry='retail' THEN COUNT(T.Industry) ELSE 0 END NumberOfSwiped
		,CASE WHEN T.Industry='ecommerce' THEN COALESCE(SUM(T.Amount),0) ELSE 0 END TolalSumKeyed
		,CASE WHEN T.Industry='retail' THEN COALESCE(SUM(T.Amount),0) ELSE 0 END ToalSumSwiped
		,CASE WHEN T.Industry in('retail','ecommerce')  THEN COALESCE(SUM(T.TechFee),0) ELSE 0 END TotalSumTechFee
		,CASE WHEN T.Industry in('retail','ecommerce')  THEN COALESCE(SUM(T.Surcharge),0) ELSE 0 END TotalSumSurcharge
		,CASE WHEN T.Industry in('retail','ecommerce')  THEN COALESCE(SUM(T.FleetFee),0) ELSE 0 END TotalSumFleetFee
		,COALESCE(SUM(0),0) AS TotalSale
		,M.Company AS MerchantName
		,(SELECT top 1 D.Fname + ' ' + D.LName AS DriverName FROM Driver D WHERE D.DriverNo = T.DriverNo AND D.fk_FleetID=T.fk_FleetId) AS DriverName
		,COALESCE(F.FleetID,0) FleetID
		FROM 
		(
			SELECT DriverNo,VehicleNo,Industry,CASE WHEN TxnType IN ('Sale','Completion') THEN Amount ELSE -Amount END AS Amount
			,TechFee,Surcharge,FleetFee,MerchantID,fk_FleetId,ResponseCode, TxnDate FROM [MTDTransaction] WHERE TxnType IN ('Sale','Completion','Refund')
		) T
		INNER JOIN Merchant M ON (T.MerchantID = M.MerchantID)
		INNER JOIN Fleet F ON (T.fk_FleetId = F.FleetID)
		WHERE (COALESCE(@MerchantId,0) = 0 OR @MerchantId = T.MerchantID)
		AND (COALESCE(@FleetId,0) = 0 OR @FleetId = T.fk_FleetId)
		AND (COALESCE(@FleetId,0) = 0 OR @FleetId = T.fk_FleetId)
		AND (COALESCE(@VehicleNumber,'') = '' OR @VehicleNumber = T.VehicleNo)
		AND (COALESCE(@DriverNumber,'') = '' OR @DriverNumber = T.DriverNo)
		AND T.TxnDate BETWEEN @DateFrom AND @DateTo
		AND T.ResponseCode in ('000','002')
		GROUP BY DriverNo,T.Industry,F.FleetName,M.Company,F.FleetID,T.fk_FleetId
	) M
	PIVOT
	(
		SUM(TotalSale)
		FOR Industry IN (['ecommerce'],['retail'])
	) AS P
) S
GROUP BY DriverNo,Fleet,MerchantName,DriverName,FleetID

END 
GO