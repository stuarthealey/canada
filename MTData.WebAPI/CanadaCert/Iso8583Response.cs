﻿
namespace MTData.WebAPI.CanadaCert
{
    public class Iso8583Response
    {
        #region Transaction Status

        public string Message { get; set; }

        #endregion

        #region Public ISO Properties

        /// <summary>
        /// gets or sets the type of message being sent
        /// </summary>
        public string ResponseMessageID { get; set; }

        /// <summary>
        /// gets or sets the details of bit set in the response
        /// </summary>
        public string Bitmap { get; set; }

        /// <summary>
        /// gets or sets the Processing Code
        /// </summary>
        public string ProcessingCode { get; set; }

        /// <summary>
        /// gets or sets the amount of transaction
        /// </summary>
        public string AmountOfTransaction { get; set; }

        /// <summary>
        /// gets or sets the transmission date/time
        /// </summary>
        public string TransmissionDateTime { get; set; }

        /// <summary>
        /// gets or sets the system trace for the transaction 
        /// which is A system-generated number provided by the merchant,
        /// the System Trace Number uniquely identifies a transaction
        /// </summary>
        public string SystemTrace { get; set; }

        /// <summary>
        /// get or sets the local transmission time From First Data
        /// </summary>
        public string TimeLocalTransmission { get; set; }

        /// <summary>
        /// get or sets the local transmission date From First Data
        /// </summary>
        public string DateLocalTrans { get; set; }

        /// <summary>
        /// gets or sets Identifies the acquiring host 
        /// </summary>
        public string NetworkInternationId { get; set; }

        /// <summary>
        /// gets or sets the conditions at the POS location
        /// for a particular transaction
        /// </summary>
        public string PosConditionCode { get; set; }

        /// <summary>
        /// gets or sets the acquiring Id for the transaction 
        /// </summary>
        public string AcquiringId { get; set; }

        /// <summary>
        /// gets or sets the system generated retrieval ref#
        /// </summary>
        public string RetrievalRefNumber { get; set; }

        /// <summary>
        /// gets or sets the auth code
        /// This field is provided only 
        /// if the authorization was approved by the issuer.
        /// </summary>
        public string AuthIdentificationResponse { get; set; }

        /// <summary>
        /// gets or sets the status of the transaction(Approved, Decliened,  Referral etc.)
        /// </summary>
        public string ResponseCode { get; set; }

        /// <summary>
        /// gets or sets the First Data assigned code 
        /// that identifies the merchant’s terminal
        /// </summary>
        public string TerminalId { get; set; }

        public string MerchantId { get; set; } //Added for capkey

        /// <summary>
        /// gets or sets the merchant location and name
        /// The purpose of this field is to override information 
        /// stored for the merchant on First Data Merchant master.
        ///For American Express and Debit, it is recommended that 
        ///the Street Address also be provided.
        /// </summary>
        public string AltMerchantNameOrLocation { get; set; }

        /// <summary>
        /// gets or sets the Address Verification Response 
        /// This is Bitmap 44 — Additional Response Data
        /// </summary>
        public string AdditionalResponseData { get; set; }

        /// <summary>
        /// gets or sets the Bitmap 55
        /// </summary>
        public string ICCData { get; set; }

        /// <summary>
        /// gets or sets the Bitmap 63
        /// The response returned is based on the tables 
        /// that are submitted in the request messages
        /// </summary>
        public string FDPrivateUsageData63 { get; set; }

        #endregion

        #region Other Properties
 
        public string PaymentType { get; set; }

        public string CardType { get; set; }

        public string TransactionType { get; set; }

        /// <summary>
        /// get and set the transaction Id created for every transaction.
        /// </summary>
        public long TransactionId { get; set; }

        public int SourceId { get; set; }

        /// <summary>
        /// get and set the StatusCode returned from Datawire.
        /// </summary>
        public string StatusCode { get; set; }

        /// <summary>
        /// get and set the ReturnCode returned from Datawire.
        /// </summary>
        public string ReturnCode { get; set; }

        /// <summary>
        /// get and set the complete response from the Datawire.
        /// </summary>
        public string CompleteResponse { get; set; }  

        /// <summary>
        /// get and set whether TOR transaction or not.
        /// Required to set when processing TOR.
        /// </summary>
        public bool IsTORTransaction { get; set; }

        public string Token { get; set; }

        public string BookerAppCardExpiryDate { get; set; }

        public string Bit32 { get; set; }

        public string Bit34 { get; set; }

        #endregion
    }
}