var pages = {};  // All page-specific functions stored in this object.
// Don't actually modify the above line to include the functions, just insert
// the functions where you define them.
//
// i.e.:  pages.campaign.view = function() { ....
//
// Remember, I want to keep everything organized, so that above code snippet
// might be located in `expeditor/static/javascript/campaigns.js`.

$(function() {
	// Call the function in the `js-function` body data attribute.
	var func = $("body").data("js-function")
	if(func) {
		var parts = func.split(".");
		var curObj = pages;
		for(var i=0; i<parts.length; i++) {
			curObj = pages[parts[0]];
			if(typeof curObj == "undefined") {
				console.error("No such function " + func);
				return;
			}
		}
		if(typeof curObj == "function") {
			curObj();
		} else {
			console.error(func + " is not a function.");
		}
	}
});
