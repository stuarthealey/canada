﻿namespace MTData.Web.ViewData
{
    public class CityViewData
    {
        public int CityID { get; set; }
        public string CityName { get; set; }
        public int fk_StateID { get; set; }
    }
}