﻿using System.Collections.Generic;
using MTD.Data.Data;

namespace MTD.Data.Repository.Interface
{
    public interface IBookingFeeRepository
    { 
        IEnumerable<BookingFee> FeeList();
        void Update(BookingFee fee, int feeId);
        BookingFee GetFee(int feeId);
        void DeleteFee(int feeId);
        IEnumerable<BookingFee> FeeListByFiter(FilterSearchParameters searchParameters);
        
    }
}
