﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Text;

using MTD.Core.Service.Interface;
using MTData.WebAPI.Controllers;

namespace MTData.WebAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //GlobalConfiguration.Configuration.MessageHandlers.Add(new
            //ApplicationAuthenticationHandler());
        }

        protected void Application_BeginRequest()
        {
            bool isSecure = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsSecure"]);
            if (isSecure)
                if (!Context.Request.IsSecureConnection)
                {
                    HttpContext context = HttpContext.Current;
                    string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                    Response.Redirect(redirectUrl, false);
                    context.ApplicationInstance.CompleteRequest();
                }
        }

    }
}
