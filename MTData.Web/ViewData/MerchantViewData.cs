﻿using System;

namespace MTData.Web.ViewData
{
    public class MerchantViewData : BaseViewData
    {
        public MerchantViewData()
        {
        }

        public int MerchantID { get; set; }
        public string Company { get; set; }
        public Nullable<decimal> SetupFee { get; set; }
        public Nullable<decimal> MonthlyFee { get; set; }
        public string Disclaimer { get; set; }
        public string TermCondition { get; set; }
        public Nullable<decimal> StateTaxRate { get; set; }
        public Nullable<decimal> FederalTaxRate { get; set; }
        public Nullable<decimal> TipPerLow { get; set; }
        public Nullable<decimal> TipPerMedium { get; set; }
        public Nullable<decimal> TipPerHigh { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Nullable<bool> IsAllowSale { get; set; }
        public Nullable<bool> IsAllowPreauth { get; set; }
        public Nullable<bool> IsAllowCapture { get; set; }
        public Nullable<bool> IsAllowRefund { get; set; }
        public Nullable<bool> IsAllowVoid { get; set; }
        public string CompanyTaxNumber { get; set; }
        public Nullable<bool> IsTaxInclusive { get; set; }
        public bool IsJobNoRequired { get; set; }
        public string TimeZone { get; set; }

        public Nullable<bool> UseTransArmor { get; set; }       //PAY-13
    }
}
