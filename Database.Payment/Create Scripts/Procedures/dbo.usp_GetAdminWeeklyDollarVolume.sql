IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAdminWeeklyDollarVolume]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_GetAdminWeeklyDollarVolume]
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <27-06-2016>
-- Description:	<Get weekly summary dollar volume report>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetAdminWeeklyDollarVolume]
AS
BEGIN
	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SET NOCOUNT ON
		SET ANSI_WARNINGS OFF  

		DECLARE @DateFromThisWeek DATETIME,
				@DateToThisWeek DATETIME,
				@DateFromWeek1 DATETIME,
				@DateToWeek1 DATETIME,
				@DateFromWeek2 DATETIME,
				@DateToWeek2 DATETIME,
				@DateFromWeek3 DATETIME,
				@DateToWeek3 DATETIME,
				@DateFromWeek4 DATETIME,
				@DateToWeek4 DATETIME,
				@DateFromWeek5 DATETIME,
				@DateToWeek5 DATETIME,
				@DateFromWeek6 DATETIME,
				@DateToWeek6 DATETIME

		SELECT  @DateFromThisWeek = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,0,GETDATE())), 0) - 1,
				@DateToThisWeek = GETDATE(),
				@DateFromWeek1 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-1,GETDATE())), 0) - 1,
				@DateToWeek1 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-1,GETDATE())), 7) - 1,
				@DateFromWeek2 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-2,GETDATE())), 0) - 1,
				@DateToWeek2 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-2,GETDATE())), 7) - 1,
				@DateFromWeek3 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-3,GETDATE())), 0) - 1,
				@DateToWeek3 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-3,GETDATE())), 7) - 1,
				@DateFromWeek4 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-4,GETDATE())), 0) - 1,
				@DateToWeek4 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-4,GETDATE())), 7) - 1,
				@DateFromWeek5 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-5,GETDATE())), 0) - 1,
				@DateToWeek5 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-5,GETDATE())), 7) - 1,
				@DateFromWeek6 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-6,GETDATE())), 0) - 1,
				@DateToWeek6 = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-6,GETDATE())), 7) - 1

		DECLARE @DataTable TABLE (
			Id INT,
			FleetId INT,
			Txndate DATETIME,
			Amount DECIMAL(18,2)
			)

		INSERT INTO @DataTable
		SELECT Id, fk_FleetId, TxnDate, Amount FROM dbo.MTDTransaction WHERE ResponseCode IN ('00', '000', '002') AND TxnType IN ('Sale', 'Completion') AND  IsRefunded = 0 AND IsVoided = 0
	   
		SELECT  ROW_NUMBER() OVER (ORDER BY f.FleetName) AS 'Sr.No',
						COALESCE(f.FleetName, '')  AS  'FleetName',
						(SELECT CONCAT('$', CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2))) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromThisWeek AND Txndate < @DateToThisWeek) AS 'TransAmtThisWeek',
						(SELECT CONCAT('$', CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2))) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromWeek1 AND Txndate < @DateToWeek1) AS 'TransAmtWeekOne',
						(SELECT CONCAT('$', CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2))) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromWeek2 AND Txndate < @DateToWeek2) AS 'TransAmtWeekTwo',
						(SELECT CONCAT('$', CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2))) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromWeek3 AND Txndate < @DateToWeek3) AS 'TransAmtWeekThree',
						(SELECT CONCAT('$', CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2))) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromWeek4 AND Txndate < @DateToWeek4) AS 'TransAmtWeekFour',
						(SELECT CONCAT('$', CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2))) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromWeek5 AND Txndate < @DateToWeek5) AS 'TransAmtWeekFive',
						(SELECT CONCAT('$', CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2))) FROM @DataTable WHERE FleetId = f.FleetID AND Txndate >= @DateFromWeek6 AND Txndate < @DateToWeek6) AS 'TransAmtWeekSix'
		FROM dbo.MTDTransaction m 
			INNER JOIN dbo.Fleet f ON m.fk_FleetId = f.FleetID 
		WHERE m.ResponseCode IN ('00', '000', '002') 
		  AND m.TxnType IN ('Sale', 'Completion') 
		  AND m.IsRefunded = 0 
		  AND m.IsVoided = 0
		  AND m.txndate >= @DateFromWeek6 
		  AND m.txndate < @DateToThisWeek 
		GROUP BY f.FleetName, f.FleetID
		ORDER BY f.FleetName
	END TRY
	BEGIN CATCH
		INSERT INTO dbo.ProcErrorHandler(ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, UserName, HostName, [TimeStamp])
		SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
	END CATCH
END
GO
