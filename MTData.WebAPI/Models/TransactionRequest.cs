﻿using System;
using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    /// <summary>
    ///TransactionRequest class contains the required information for the transaction.
    /// </summary>
    [DataContract]
    public class TransactionRequest
    {
        #region "Public Properties"

        public bool? IsDispatchRequest { get; set; }
        public PymtTypeType PaymentType { get; set; }
        public string TransactionAmount { get; set; }
        public string CardNumber { get; set; }
        public CardTypeType CardType { get; set; }
        public string ExpiryDate { get; set; }
        public string CardCvv { get; set; }
        public string ZipCode { get; set; }
        public string StreetAddress { get; set; }
        public string MerchantCategoryCode { get; set; }
        public string TransactionType { get; set; }
        public TxnTypeType TransType { get; set; }
        public string TerminalId { get; set; }
        public string FdMerchantId { get; set; }
        public int MtdMerchantId { get; set; }
        public string Track2Data { get; set; }
        public string IndustryType { get; set; }
        public string SerialNumber { get; set; }
        public string RequestedId { get; set; }
        public string TppId { get; set; }
        public string GroupId { get; set; }
        public string App { get; set; }
        public string Did { get; set; }
        public string ServiceUrl { get; set; }
        public string ServiceId { get; set; }
        public string DriverNumber { get; set; }
        public string VehicleNumber { get; set; }
        public string RapidConnectAuthId { get; set; }
        public string TransNote { get; set; }
        public string CreatedBy { get; set; }
        public string LocalDateTime { get; set; }
        public string TrnmsnDateTime { get; set; }
        public string EntryMode { get; set; }
        public string PickAddress { get; set; }
        public string DestinationAddress { get; set; }
        public string StartLatitude { get; set; }
        public string EndLatitude { get; set; }
        public string StartLongitude { get; set; }
        public string EndLongitude { get; set; }
        public string Stan { get; set; }
        public string TransRefNo { get; set; }
        public string TransOrderNo { get; set; }
        public string CrdHldrName { get; set; }
        public string CrdHldrPhone { get; set; }
        public string Fare { get; set; }
        public string Taxes { get; set; }
        public string Tip { get; set; }
        public string Tolls { get; set; }
        public string Surcharge { get; set; }
        public string Fee { get; set; }
        public string JobNumber { get; set; }
        public string Track1Data { get; set; }
        public string Track3Data { get; set; }
        public string KeyId { get; set; }
        public string FlagFall { get; set; }
        public string Extras { get; set; }
        public string GateFee { get; set; }
        public string Others { get; set; }
        public string MCCode { get; set; }
        public string EncryptionKey { get; set; }
        public string TokenType { get; set; }
        public string FirstFourCard { get; set; }
        public string LastFourcard { get; set; }
        public string RCCIToken { get; set; }
        public string CurrencyCode { get; set; }
        public string PinData { get; set; }
        public string DebitPin { get; set; }
        public string KeySerialNumber { get; set; }
        public string FleetId { get; set; }
        public string EmvData { get; set; }
        public string CardSeq { get; set; }
        public string ReqFileOff { get; set; }
        public string Capkdata { get; set; }
        public string FbSeq { get; set; }
        public Nullable<decimal> TechFee { get; set; }
        public Nullable<decimal> FleetFee { get; set; }
        public string CardSeqNum { get; set; }
        public string CustomerId { get; set; }
        public string CapkFileCreationDt { get; set; }
        public string fileSize { get; set; }
        public string fileCRC16 { get; set; }
        public string CnPayload { get; set; }
        public bool IsReversal { get; set; }
        public string OriginalTransType { get; set; }
        public string AuthIdentResponse { get; set; }
        public Nullable<int> EncryptMethod { get; set; }
        public Nullable<int> KeyVersion { get; set; }

        public string RestrandRef { get; set; }
        public string MAXAmount { get; set; }
        public string RespTable14 { get; set; }
        public string RespTable49 { get; set; }
        public string RespTableVI { get; set; }
        public string RespTableMC { get; set; }
        public string RespTableDS { get; set; }
        public string RespTableSP { get; set; }
        public string ResponseCode { get; set; }
        public string AuthID { get; set; }
        public string AddRespdata { get; set; }
        public bool IsTORTransaction { get; set; }
        public string MerchantZipCode { get; set; }
        public string SourceTransaction { get; set; }
        public string ResponseBit63 { get; set; }
        public string RFUDEV { get; set; }//added for deviceTypeId

        public string ServiceCode { get; set; }

        //Changes done for EMV Contact certification of Canada
        public string MacData { get; set; }
        public string CheckDigit { get; set; }
        public string AccountType { get; set; }

        public string TerminalLaneNo { get; set; }
        public string DeviceType { get; set; }
        public bool PortalEmvRefundVoidReq { get; set; }

        //zzz Chetu 357
        public string VoidRequestId { get; set; }
        public bool IsMAcVerificationFailed { get; set; }

        //PAY-13 Flag to indicate transaction is to be treated as a Trans Armor one, and to use TA code paths.
        public bool IsTransArmor { get; set; }

        #endregion
    }
}
