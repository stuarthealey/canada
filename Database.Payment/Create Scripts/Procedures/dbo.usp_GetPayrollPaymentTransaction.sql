IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetPayrollPaymentTransaction]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_GetPayrollPaymentTransaction]
GO

CREATE PROCEDURE dbo.usp_GetPayrollPaymentTransaction
-- =============================================
-- Author:		
-- Create date: 
-- Description:	Get Payroll Payment transaction report
-- =============================================
--	S.Healey	2016-12-08		- Deduct Fleet Fee and Tech Fee as well as Fee for proper payment amount.
--
(
	@MerchantId INT = NULL,
	@FleetId INT = NULL,
	@PayerType NVARCHAR(50) = NULL,
	@DriverNumber NVARCHAR(50) = NULL,
	@DateFrom DATETIME = NULL,
	@DateTo DATETIME = NULL,
	@NetworkType INT = NULL
)
AS
BEGIN
	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SET NOCOUNT ON
		DECLARE @PayType INT

		IF (@PayerType = 'Driver')
		BEGIN
		   SET @PayType = 1
		END
		ELSE IF (@PayerType  = 'Car Owner')
		BEGIN
			SET @PayType = 2
		END
		ELSE IF (@PayerType = 'Network')
		BEGIN
		   SET @PayType = 3
		END

		IF (@PayType = 1 OR @PayType = 3 OR @PayType IS NULL)
		BEGIN
			SELECT	CONVERT(DATE, T.TxnDate) AS [Date],
					ISNULL(D.Fname, '') + ' ' + ISNULL(D.LName, '') AS [Name],
					ISNULL(D.BankName, '') AS BankName,
					ISNULL(D.BSB, '') AS BSB,
					ISNULL(D.Account, '') AS AccountNumber,
					CASE 
						WHEN SUM(AmountPayable) < 0 THEN 0
						ELSE SUM(AmountPayable)
					END AS AmountPayable,
					f.PayType AS 'FleetPayType' into #TempDriver
			FROM (SELECT	DriverNo,
							TxnDate,
							FareValue,
							tip,
							TechFee,
							fk_FleetId,
							MerchantID,
							ResponseCode,
							TxnType,
							Taxes,
							CASE 
								WHEN TxnType IN ('Sale', 'Completion')
									THEN CASE 
											WHEN (ISNULL(FareValue, 0) + ISNULL(Tip, 0) + ISNULL(Toll, 0) + ISNULL(Taxes, 0) + ISNULL(Extras, 0) - (ISNULL(Fee, 0) + ISNULL(FleetFee, 0) + ISNULL(TechFee, 0)) < 0) THEN 0
											ELSE (ISNULL(FareValue, 0) + ISNULL(Tip, 0) + ISNULL(Toll, 0) + ISNULL(Taxes, 0) + ISNULL(Extras, 0) - (ISNULL(Fee, 0) + ISNULL(FleetFee, 0) + ISNULL(TechFee, 0)))
										END
								ELSE - Amount
							END AS AmountPayable

				FROM dbo.MTDTransaction) T
					INNER JOIN dbo.Driver d ON T.DriverNo = d.DriverNo
					INNER JOIN dbo.Fleet f ON d.fk_FleetID = f.FleetID

			WHERE T.fk_FleetId = d.fk_FleetID
				AND T.TxnDate BETWEEN @DateFrom AND @DateTo
				AND T.ResponseCode IN ('000', '00', '002')
				AND T.TxnType IN ('Sale', 'Completion', 'refund')
				AND (ISNULL(@MerchantId, 0) = 0 OR T.MerchantID = @MerchantId)
				AND (ISNULL(@FleetId, 0) = 0 OR T.fk_FleetId = @FleetId)
				AND (ISNULL(@DriverNumber, '') = '' OR T.DriverNo = @DriverNumber)
				AND (ISNULL(@PayType, 0) = 0 OR f.PayType  = @PayType)

			GROUP BY CONVERT(DATE, T.TxnDate), D.Fname, D.LName, D.BankName, D.BSB, D.Account, f.PayType
		END

		IF (@PayType = 2 OR @PayType = 3 OR @PayType IS NULL)
		BEGIN
			SELECT	CONVERT(DATE, T.TxnDate) AS [Date],
					ISNULL(cw.OwnerName, '') AS [Name],
					ISNULL(cw.BankName, '') AS BankName,
					ISNULL(cw.BSB, '') AS BSB,
					ISNULL(cw.Account, '') AS AccountNumber,
					CASE 
						WHEN SUM(AmountPayable) < 0 THEN 0
						ELSE SUM(AmountPayable)
					END AS AmountPayable,
					f.PayType AS 'FleetPayType' INTO #TempOwner
			FROM (SELECT VehicleNo,
						TxnDate,
						FareValue,
						tip,
						TechFee,
						fk_FleetId,
						MerchantID,
						ResponseCode,
						TxnType,
						Taxes,
						CASE 
							WHEN TxnType IN ('Sale', 'Completion')
								THEN CASE 
										WHEN (ISNULL(FareValue, 0) + ISNULL(Tip, 0) + ISNULL(Toll, 0) + ISNULL(Taxes, 0) + ISNULL(Extras, 0) - (ISNULL(Fee, 0) + ISNULL(FleetFee, 0) + ISNULL(TechFee, 0)) < 0) THEN 0
										ELSE (ISNULL(FareValue, 0) + ISNULL(Tip, 0) + ISNULL(Toll, 0) + ISNULL(Taxes, 0) + ISNULL(Extras, 0) - (ISNULL(Fee, 0) + ISNULL(FleetFee, 0) + ISNULL(TechFee, 0)))
									END
							ELSE - Amount
						END AS AmountPayable
				FROM dbo.MTDTransaction) T
					INNER JOIN dbo.Vehicle v ON T.VehicleNo = v.VehicleNumber
					INNER JOIN dbo.Fleet f ON v.fk_FleetID = f.FleetID
					INNER JOIN dbo.CarOwners cw ON v.fk_CarOwenerId = cw.Id

			WHERE T.fk_FleetId = v.fk_FleetID
				AND T.TxnDate BETWEEN @DateFrom AND @DateTo
				AND T.ResponseCode IN ('000', '00', '002')
				AND T.TxnType IN ('Sale', 'Completion', 'refund')
				AND (ISNULL(@MerchantId, 0) = 0 OR T.MerchantID = @MerchantId)
				AND (ISNULL(@FleetId, 0) = 0 OR T.fk_FleetId = @FleetId)
				AND (ISNULL(@DriverNumber, '') = '' OR v.fk_CarOwenerId = CAST(@DriverNumber AS INT))
				AND (ISNULL(@PayType,0) = 0 OR F.PayType = @PayType)

			GROUP BY CONVERT(DATE, T.TxnDate), cw.OwnerName, cw.BankName, cw.BSB, cw.Account, f.PayType
		END

		IF (@PayType = 1 OR (@PayType = 3 AND @NetworkType = 1))
		BEGIN
			SELECT [Date], [Name], BankName, BSB, AccountNumber, AmountPayable
			FROM #TempDriver
		END
		ELSE IF (@PayType = 2 OR (@PayType = 3 AND @NetworkType = 2))
		BEGIN
			SELECT [Date], [Name], BankName, BSB, AccountNumber, AmountPayable
			FROM #TempOwner
		END
		ELSE IF (@PayType = 3 AND @NetworkType IS NULL)
		BEGIN
			SELECT [Date], [Name], BankName, BSB, AccountNumber, AmountPayable
			FROM #TempDriver

			UNION ALL

			SELECT [Date], [Name], BankName, BSB, AccountNumber, AmountPayable
			FROM #TempOwner
		END
		ELSE IF (@PayType IS NULL)
		BEGIN
			SELECT [Date], [Name], BankName, BSB, AccountNumber, AmountPayable
			FROM #TempDriver 
			WHERE FleetPayType IN (1, 3)

			UNION ALL

			SELECT [Date], [Name], BankName, BSB, AccountNumber, AmountPayable
			FROM #TempOwner 
			WHERE FleetPayType IN (2, 3)
		END

		IF OBJECT_ID('tempdb..#TempDriver') IS NOT NULL
		BEGIN
			DROP TABLE #TempDriver
		END

		IF OBJECT_ID('tempdb..#TempOwner') IS NOT NULL
		BEGIN
			DROP TABLE #TempOwner
		END	 
	END TRY
	BEGIN CATCH
		INSERT INTO ProcErrorHandler (ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, UserName, HostName, [TimeStamp])
		SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
	END CATCH
END
GO
