IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CorpUserExceptionReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_CorpUserExceptionReport]
GO

CREATE PROCEDURE dbo.usp_CorpUserExceptionReport
(
	@IsOnDemandReport BIT,
	@FromDate DATETIME = NULL,
	@ToDate DATETIME = NULL,
	@CorpUserId INT,
	@ReportId INT,
	@XmlData NVARCHAR(MAX) OUT,
	@ReportType INT OUT,
	@FDate DATE OUT,
	@TDate DATE OUT
)
AS
BEGIN
	BEGIN TRY  
		 SET NOCOUNT ON;
		 SET ANSI_WARNINGS OFF
		 DECLARE @NextScheduleDateTime DATETIME,
				 @ScheduleId INT
		 DECLARE  @FleetId INT,
				  @InnerXml NVARCHAR(MAX)='',
				  @XmlFooter NVARCHAR(500),
				  @Frequency INT,
				  @FrequencyType INT,
				  @DateFrom DATE,
				  @DateTo DATE,
				  @ExpFleetTransaction INT,
				  @ExpCarTransaction INT,
				  @ExpTransactionValue DECIMAL(18,2),
				  @ActFleetTransaction  INT,
				  @ActCarTransaction INT,
				  @ActTransactionValue DECIMAL(18,2),
				  @Case VARCHAR(100),
				  @MerchantId int=null

		set @MerchantId= (select top 1 fk_MerchantId from UserFleet where fk_UserID=@CorpUserId)
		 IF(@ReportId=3)
		 BEGIN
		 SELECT  @ScheduleId=ScheduleID,
				 @Frequency=Frequency,
				 @FrequencyType=FrequencyType,
				 @NextScheduleDateTime=NextScheduleDateTime		 
		 FROM ScheduleReport 
		 WHERE fk_ReportId=3 AND IsCreated=1 AND fk_MerchantId IS NULL AND CorporateUserId IS NULL
		 END
		 IF(@ReportId IS NULL)
		 BEGIN
		 SELECT  @ScheduleId=ScheduleID,
				 @Frequency=Frequency,
				 @FrequencyType=FrequencyType,
				 @NextScheduleDateTime=NextScheduleDateTime		 
		 FROM ScheduleReport 
		 WHERE fk_ReportId=3 AND IsCreated=1 AND fk_MerchantId= @MerchantId
		 END
		 --Set dates in between exception data to be prepared 
		 IF(@IsOnDemandReport=1)
		  BEGIN
		   SET @DateFrom=@FromDate
		   SET @DateTo=@ToDate
		  END
		  ELSE
		   BEGIN
			 IF(@Frequency=1) 
			  BEGIN
			   SET @DateFrom=GETDATE()-@FrequencyType
			   SET @DateTo=GETDATE()  
			   SET @NextScheduleDateTime=@NextScheduleDateTime+@FrequencyType   
			  END
			  ELSE IF(@Frequency=2) 
			  BEGIN
			   SET @DateFrom = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-@FrequencyType,GETDATE())), 0)-1
			   SET @DateTo= DATEADD(WEEK, DATEDIFF(WEEK, 0, GETDATE()), 0)-1
			   SET @NextScheduleDateTime=DATEADD(WK,@FrequencyType,@NextScheduleDateTime)
			  END
			  ELSE IF(@Frequency=3) 
			  BEGIN
				SET @DateFrom =DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH,-@FrequencyType,GETDATE())), 0)
				SET @DateTo=DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) 
				SET @NextScheduleDateTime=DATEADD(MONTH,@FrequencyType,@NextScheduleDateTime)
			  END    
			 ELSE
			  BEGIN
			   RETURN 1
			  END 
			END
			IF(@ReportId IS NULL AND @IsOnDemandReport=0)
			 BEGIN
			  UPDATE ScheduleReport SET NextScheduleDateTime=@NextScheduleDateTime WHERE ScheduleID=@ScheduleId
			 END
		 --Declaring cursor for fetching fleets one by one corresponding to a particular merchant
		 DECLARE Exp_Cursor CURSOR FOR 
		 SELECT FleetID FROM Fleet flt WHERE flt.fk_MerchantID=@MerchantId AND flt.IsActive=1
		 OPEN Exp_Cursor
		 FETCH NEXT FROM Exp_Cursor INTO @FleetId
		 WHILE @@FETCH_STATUS = 0   
		 BEGIN   
		 SET @Case=''
		 SELECT COALESCE((SELECT Company FROM Merchant M WHERE M.MerchantID = T.MerchantId),'') AS Merchant
			   ,COALESCE((SELECT [FleetName] FROM Fleet F WHERE F.FleetID = T.FleetID),'') AS Fleet
			   ,T.MerchantId
			   ,T.FleetID
			   ,T.VehicleNo
			   ,T.FareValue
			   ,T.Tip 
			   ,T.Surcharge 
			   ,T.Amount
			   ,T.AmountOwing
			   ,T.TechFee
			   ,At.ExpFleetTransaction
			   ,At.ExpCarTransaction
			   ,At.ExpTransactionValue
			   ,AT.ActFleetTransaction
			   ,AT.ActCarTransaction
			   ,At.ActTransactionValue
			   ,T.Fee
				INTO #Temp 
		 FROM
		 (
		  SELECT COALESCE(tblSaleCompletion.MerchantId,0) AS 'MerchantId'
			   ,COALESCE((tblSaleCompletion.fk_FleetId),'') AS 'FleetID'
			   ,COALESCE(tblSaleCompletion.[VehicleNo],'') AS 'VehicleNo'
			   ,COALESCE(tblSaleCompletion.[FareValue],0) AS 'FareValue'
			   ,COALESCE(tblSaleCompletion.Tip,0) AS 'Tip'
			   ,COALESCE(tblSaleCompletion.Surcharge,0) AS 'Surcharge'
			   ,COALESCE(tblSaleCompletion.[Amount],0) AS 'Amount'
			   ,COALESCE(tblSaleCompletion.Fee,0)+COALESCE(tblSaleCompletion.FleetFee,0) AS 'Fee'
			   ,COALESCE(tblSaleCompletion.TechFee,0) AS 'TechFee'
			   ,COALESCE(tblSaleCompletion.Amount,0) - COALESCE(tblSaleCompletion.FleetFee,0)-COALESCE(tblSaleCompletion.Fee,0) AS 'AmountOwing'
			   ,COALESCE(tblSaleCompletion.TxnDate,'') AS 'TransDate'
		 FROM
		 (
		 SELECT 
				  MerchantId AS 'MerchantId'
				 ,fk_FleetId AS 'fk_FleetId'
				 ,VehicleNo AS 'VehicleNo'
				 ,FareValue AS 'FareValue'
				 ,Tip AS 'Tip'
				 ,Surcharge AS 'Surcharge'
				 ,Amount AS 'Amount'
				 ,Fee AS 'Fee'
				 ,TechFee AS 'TechFee'
				 ,TxnDate AS 'TxnDate'
				 ,FleetFee AS 'FleetFee'		                                                                                                                                  
			 FROM    dbo.MTDTransaction 								 													 								
			 WHERE   TxnType='Sale'
					 AND IsRefunded=0
					 AND IsVoided=0				    
					 AND ResponseCode IN ('000', '00', '002')
					 AND CAST(TxnDate AS DATE)>=@DateFrom 
					 AND CAST(TxnDate AS DATE)<@DateTo	   
			  UNION ALL
			  SELECT  tblId.MerchantId,
					  tblId.fk_FleetId,
					  tblSrc.VehicleNo,
					  tblId.FareValue,
					  tblId.Tip,
					  tblId.Surcharge,
					  tblSrc.Amount,
					  tblId.Fee,
					  tblId.TechFee,
					  tblSrc.TxnDate,
					  tblId.FleetFee	                                                                
			  FROM dbo.MTDTransaction tblSrc 
			  INNER JOIN dbo.MTDTransaction tblId
			  ON tblSrc.SourceId=tblId.Id						 								
			  WHERE  tblSrc.TxnType='Completion'
					 AND tblSrc.ResponseCode IN ('000', '00', '002')
					 AND CAST(tblSrc.TxnDate AS DATE)>=@DateFrom 
					 AND CAST(tblSrc.TxnDate AS DATE)<@DateTo	 
			) tblSaleCompletion	
		 ) T
		 INNER JOIN
		 (
		  SELECT Trans.MerchantId
		  ,Trans.FleetID
		  ,Fl.FleetTransaction ExpFleetTransaction,Fl.CarTransaction ExpCarTransaction,Fl.TransactionValue ExpTransactionValue
		  ,Trans.FTransCount ActFleetTransaction,Trans.VTransCount ActCarTransaction,Trans.Amount ActTransactionValue FROM
		 (
		  SELECT fk_MerchantID MerchantID, FleetID, FleetName, FleetTransaction, CarTransaction, TransactionValue  
		  FROM Fleet F WHERE  F.IsActive = 1 
		  AND ( F.FleetTransaction IS NOT NULL OR  F.CarTransaction IS NOT NULL OR  F.TransactionValue IS NOT NULL) 
		  ) Fl
		 INNER JOIN
		 (
		 SELECT V.MerchantId,V.FleetID,SUM(V.TransCount) FTransCount,Count(V.TransCount) VTransCount,SUM(V.FareValue) FareValue,SUM(V.Tip) AS 'Tip'
		 ,SUM(V.Surcharge) AS 'Surcharge',SUM(V.Amount) AS 'Amount',SUM(V.Fee) AS 'Fee',SUM(V.TechFee) AS 'TechFee',SUM(AmountOwing) AS 'AmountOwing' FROM 
		 (
			SELECT COALESCE(tblSaleCompletion.MerchantId,0) AS 'MerchantId'
			,COALESCE((tblSaleCompletion.fk_FleetId),'') AS FleetID
			,COALESCE(tblSaleCompletion.VehicleNo,'') AS 'VehicleNo'
			,COUNT(tblSaleCompletion.VehicleNo) AS TransCount
			,SUM(tblSaleCompletion.FareValue) AS 'FareValue'
			,SUM(tblSaleCompletion.Tip) AS 'Tip'
			,SUM(tblSaleCompletion.Surcharge) AS 'Surcharge'
			,SUM(tblSaleCompletion.Amount) AS 'Amount'
			,COALESCE(SUM(tblSaleCompletion.Fee),0)+COALESCE(SUM(tblSaleCompletion.FleetFee),0) AS 'Fee'
			,SUM(tblSaleCompletion.TechFee) AS 'TechFee'
			,COALESCE(SUM(tblSaleCompletion.Amount),0) - COALESCE(SUM(tblSaleCompletion.Fee),0)-COALESCE(SUM(tblSaleCompletion.FleetFee),0) AS 'AmountOwing'
			FROM 
			(
			SELECT 
				  MerchantId AS 'MerchantId'
				 ,fk_FleetId AS 'fk_FleetId'
				 ,VehicleNo AS 'VehicleNo'
				 ,FareValue AS 'FareValue'
				 ,Tip AS 'Tip'
				 ,Surcharge AS 'Surcharge'
				 ,Amount AS 'Amount'
				 ,Fee AS 'Fee'
				 ,TechFee AS 'TechFee'
				 ,FleetFee AS 'FleetFee'                                                                                                                                
			 FROM    dbo.MTDTransaction 								 													 								
			 WHERE   TxnType='Sale'
					 AND IsRefunded=0
					 AND IsVoided=0				    
					 AND ResponseCode IN ('000', '00', '002')
					 AND CAST(TxnDate AS DATE)>=@DateFrom 
					 AND CAST(TxnDate AS DATE)<@DateTo
					 AND fk_FleetId IN (SELECT DISTINCT fk_FleetId FROM UserFleet WHERE fk_UserID=@CorpUserId AND fk_MerchantId=@MerchantId AND IsActive=1 AND fk_UserTypeId=5)	   
			  UNION ALL
			  SELECT  tblId.MerchantId,
					  tblId.fk_FleetId,
					  tblSrc.VehicleNo,
					  tblId.FareValue,
					  tblId.Tip,
					  tblId.Surcharge,
					  tblSrc.Amount,
					  tblId.Fee,
					  tblId.TechFee,
					  tblId.FleetFee                                                              
			  FROM dbo.MTDTransaction tblSrc 
			  INNER JOIN dbo.MTDTransaction tblId
			  ON tblSrc.SourceId=tblId.Id						 								
			  WHERE  tblSrc.TxnType='Completion'
					 AND tblSrc.ResponseCode IN ('000', '00', '002')
					 AND CAST(tblSrc.TxnDate AS DATE)>=@DateFrom 
					 AND CAST(tblSrc.TxnDate AS DATE)<@DateTo	 
					 AND tblSrc.fk_FleetId IN (SELECT DISTINCT fk_FleetId FROM UserFleet WHERE fk_UserID=@CorpUserId AND fk_MerchantId=@MerchantId AND IsActive=1 AND fk_UserTypeId=5)	 
			) tblSaleCompletion	
			GROUP BY tblSaleCompletion.VehicleNo,tblSaleCompletion.MerchantId,tblSaleCompletion.fk_FleetId
		 ) V GROUP BY V.MerchantId,V.FleetID
		) Trans ON (fl.FleetID = Trans.FleetID) 
	   WHERE Fl.FleetTransaction > Trans.FTransCount OR Fl.CarTransaction > Trans.VTransCount OR Fl.TransactionValue > Trans.Amount 
	  ) AT ON (T.FleetID = AT.FleetID) WHERE T.FleetID=@FleetId 
	   --Set data in xml format
	   SET @InnerXml=@InnerXml+COALESCE(CAST((SELECT COALESCE(Fleet,'') AS 'td',''
													,COALESCE(VehicleNo,'') AS 'td','',
													 CONCAT('$',CAST(COALESCE(FareValue,0.00) AS DECIMAL(18,2))) AS 'td',''
													,CONCAT('$',CAST(COALESCE(Tip,0.00) AS DECIMAL(18,2))) AS 'td',''
													,CONCAT('$',CAST(COALESCE(Surcharge,0.00) AS DECIMAL(18,2))) AS 'td',''
													,CONCAT('$',CAST(COALESCE(TechFee,0.00) AS DECIMAL(18,2))) AS 'td',''
													,CONCAT('$',CAST(COALESCE(Amount,0.00) AS DECIMAL(18,2))) AS 'td',''
													,CONCAT('$',CAST(COALESCE(Fee,0.00) AS DECIMAL(18,2))) AS 'td',''
													,CONCAT('$',CAST(COALESCE(AmountOwing,0.00) AS DECIMAL(18,2))) AS 'td' 
											   FROM #Temp 
											   FOR XML PATH('tr'), ELEMENTS ) AS NVARCHAR(MAX)),'')
		SELECT @ExpFleetTransaction=ExpFleetTransaction
			  ,@ExpCarTransaction=ExpCarTransaction
			  ,@ExpTransactionValue=ExpTransactionValue
			  ,@ActFleetTransaction=ActFleetTransaction
			  ,@ActCarTransaction=ActCarTransaction
			  ,@ActTransactionValue=ActTransactionValue 
		FROM #Temp
		IF(@ExpFleetTransaction>@ActFleetTransaction) 
		  BEGIN
			SET @Case='UnExp Trans'
		  END
		IF(@ExpCarTransaction>@ActCarTransaction) 
		  BEGIN 
			IF(@Case<>'') 
			  BEGIN
				SET @Case=@Case+','+ 'UnExp Cars'
			  END
			ELSE 
			 BEGIN
			  SET @Case=@Case+ 'UnExp Cars'
			 END
		  END
		IF(@ExpTransactionValue>@ActTransactionValue) 
		 BEGIN
		   IF(@Case<>'') 
			 BEGIN
			  SET @Case=@Case+','+'UnExp Trans Value'
			 END
		   ELSE 
			 BEGIN
			  SET @Case=@Case+'UnExp Trans Value'
			 END
		 END
		IF(@Case IS NOT NULL and @Case<>'') 
		  BEGIN
			 SET @Case='Exception Case :'+@Case
		  END
		--Set the footer for each fleet having summarized exception details
		SET @XmlFooter='<div style="font-size:10px;background-color:blue;">'+COALESCE(CAST((SELECT DISTINCT COALESCE(@Case,'') AS 'td',''
																					,CONCAT('Exp Trans:',ExpFleetTransaction) AS 'td',''
																					,CONCAT('Act Trans:',ActFleetTransaction) AS 'td',''
																					,CONCAT('Exp Cars:',ExpCarTransaction) AS 'td',''
																					,CONCAT('Act Cars:',ActCarTransaction) AS 'td',''
																					,CONCAT('Exp Value:$',ExpTransactionValue) AS 'td',''
																					,CONCAT('Act Value:$',ActTransactionValue)  AS 'td' 
																	  FROM #Temp
		FOR XML PATH('tr'), ELEMENTS ) AS NVARCHAR(MAX)),'')+'</div>'
		IF(@XmlFooter='<div style="font-size:10px"></div>') 
		 BEGIN
			  SET @XmlFooter='';
		 END
		 SET @InnerXml=@InnerXml+@XmlFooter
		 DROP TABLE #Temp
	   FETCH NEXT FROM Exp_Cursor INTO @FleetId
	   END   
	  CLOSE Exp_Cursor   
	  DEALLOCATE Exp_Cursor
	  SET @XmlData=@InnerXml
	  SET @ReportType=@Frequency
	  SET @FDate=@DateFrom
	  SET @TDate= DATEADD(DAY,-1,@DateTo)
	END TRY
	BEGIN CATCH
			INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
			SELECT ERROR_NUMBER()
			,ERROR_SEVERITY()
			,ERROR_STATE()
			,ERROR_PROCEDURE()
			,ERROR_LINE()
			,ERROR_MESSAGE()
			,SUSER_SNAME()
			,HOST_NAME()
			,GETDATE()
	END CATCH
END
GO