IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ValidateEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ValidateEmail]
GO

/****** Object:  StoredProcedure [dbo].[usp_ValidateEmail]    Script Date: 11/2/2015 10:46:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <17-09-2015>
-- Description:	<Validate mac address and token>
-- =============================================
CREATE PROCEDURE [dbo].[usp_ValidateEmail]
	-- Add the parameters for the stored procedure here
    @MacAddress NVARCHAR(100),
	@DriverNo NVARCHAR(50),
	@Token NVARCHAR(1000),
	@FleetId INT
AS
BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON
	DECLARE @Result BIT=1
	IF NOT EXISTS(SELECT 1 FROM Terminal WHERE MacAdd=@MacAddress AND IsActive=1)
	BEGIN
	    SET @Result=0
	END
	IF NOT EXISTS(SELECT 1 FROM DriverLogin WHERE DriverNo=@DriverNo AND Token=@Token AND FleetId=@FleetId)
	BEGIN
	    SET @Result=0
	END
	SELECT @Result
END TRY
--use catch to log error in the table  ProcErrorHandler
BEGIN CATCH
       INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
       SELECT ERROR_NUMBER()
	         ,ERROR_SEVERITY()
			 ,ERROR_STATE()
			 ,ERROR_PROCEDURE()
			 ,ERROR_LINE()
			 ,ERROR_MESSAGE()
			 ,SUSER_SNAME()
			 ,HOST_NAME()
			 ,GETDATE()
END CATCH

GO


