IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'DefaultSetting' AND COLUMN_NAME = 'TechFeeType')
	ALTER TABLE DefaultSetting add [TechFeeType] [tinyint] NULL
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'DefaultSetting' AND COLUMN_NAME = 'TechFeeFixed')
	ALTER TABLE DefaultSetting add [TechFeeFixed] [decimal](18, 2) NULL
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'DefaultSetting' AND COLUMN_NAME = 'TechFeePer')
	ALTER TABLE DefaultSetting add [TechFeePer] [decimal](6, 2) NULL
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'DefaultSetting' AND COLUMN_NAME = 'TechFeeMaxCap')
	ALTER TABLE DefaultSetting add [TechFeeMaxCap] [decimal](18, 2) NULL
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'DefaultSetting' AND COLUMN_NAME = 'TransCountSlablt1')
	ALTER TABLE defaultsetting add TransCountSlablt1 int null
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'DefaultSetting' AND COLUMN_NAME = 'TransCountSlabGt2')
	ALTER TABLE defaultsetting add TransCountSlabGt2 int null
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'DefaultSetting' AND COLUMN_NAME = 'TransCountSlablt2')
	ALTER TABLE defaultsetting add TransCountSlablt2 int null
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'DefaultSetting' AND COLUMN_NAME = 'TransCountSlabGt3')
	ALTER TABLE defaultsetting add TransCountSlabGt3 int null
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'DefaultSetting' AND COLUMN_NAME = 'TransCountSlablt3')
	ALTER TABLE defaultsetting add TransCountSlablt3 int null
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'DefaultSetting' AND COLUMN_NAME = 'TransCountSlabGt4')
	ALTER TABLE defaultsetting add TransCountSlabGt4 int null
GO

IF NOT EXISTS (SELECT 1 FROM DefaultSetting WHERE DefaultID = 6)
	INSERT INTO DefaultSetting VALUES(6, 'Technology Fee', null, null, null, null, null, null, null, null, null, null, null, null, null, 3, 5.00, 6.00, 7.00, null, null, 1, 'admin@mtdata.com', '2015-01-17', null, null, null, null, null, null, null, null)
GO

IF NOT EXISTS (SELECT 1 FROM DefaultSetting WHERE DefaultID = 7)
	INSERT INTO DefaultSetting(DefaultID, DefaultName, IsActive, CreatedBy, CreatedDate) VALUES(7, 'ConfigAdminReport', 1, 'admin@mtdata.com', GETDATE())
GO
