﻿using System;

namespace MTData.Web.ViewData
{
    public class DriverTransactionViewData
    {

        public string DriverNo { get; set; }
        public string VehicleNo { get; set; }
        public string Fleet { get; set; }
        public Nullable<int> NumberOfKeyed { get; set; }
        public Nullable<int> NumberOfSwiped { get; set; }
        public Nullable<decimal> KeyedSaleAmount { get; set; }
        public Nullable<decimal> SwipedSaleAmount { get; set; }
        public Nullable<int> TotalNumberOfSale { get; set; }
        public Nullable<decimal> TotalSaleAmount { get; set; }
        public Nullable<decimal> SumSurcharge { get; set; }
    }
}