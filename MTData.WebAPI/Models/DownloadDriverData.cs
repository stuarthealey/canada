﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Models
{
    public class DownloadDriverData
    {
        public int Id { get; set; }
        public Nullable<int> MerchantId { get; set; }
        public string VehicleNo { get; set; }
        public string DriverNo { get; set; }
        public string JobNumber { get; set; }
        public string Stan { get; set; }
        public string TransRefNo { get; set; }
        public string TransOrderNo { get; set; }
        public string PaymentType { get; set; }
        public string TxnType { get; set; }
        public string LocalDateTime { get; set; }
        public string TransmissionDateTime { get; set; }
        public Nullable<decimal> FareValue { get; set; }
        public Nullable<decimal> Tip { get; set; }
        public Nullable<decimal> Surcharge { get; set; }
        public Nullable<decimal> Fee { get; set; }
        public Nullable<decimal> Taxes { get; set; }
        public Nullable<decimal> Toll { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string Currency { get; set; }
        public string ExpiryDate { get; set; }
        public string CardType { get; set; }
        public string Industry { get; set; }
        public string EntryMode { get; set; }
        public string ResponseCode { get; set; }
        public string AddRespData { get; set; }
        public string AuthId { get; set; }
        public string AthNtwId { get; set; }
        public string RequestXml { get; set; }
        public string ResponseXml { get; set; }
        public string SourceId { get; set; }
        public string TransNote { get; set; }
        public string CardToken { get; set; }
        public Nullable<bool> IsCompleted { get; set; }
        public Nullable<bool> IsRefunded { get; set; }
        public Nullable<bool> IsVoided { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> TxnDate { get; set; }
        public Nullable<int> fk_FleetId { get; set; }
        public Nullable<decimal> TechFee { get; set; }
        public Nullable<decimal> FleetFee { get; set; }
        public Nullable<bool> IsVarified { get; set; }
        public string TerminalId { get; set; }
        public string SerialNo { get; set; }
    }
}