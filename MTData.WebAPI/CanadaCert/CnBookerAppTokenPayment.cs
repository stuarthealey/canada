﻿using System;
using System.Text;
using System.Configuration;
using Ninject;

using MTD.Core.Service.Interface;
using MTD.Core.DataTransferObjects;
using MTData.Utility;
using MTData.WebAPI.Models;
using MTData.WebAPI.Controllers;
using MTData.WebAPI.Common;
using MTData.WebAPI.CanadaCommon;
using MTData.WebAPI.Resources;

namespace MTData.WebAPI.CanadaCert
{
    public class CnBookerAppTokenPayment
    {
        private readonly ITransactionService _transactionService;
        private readonly ITerminalService _terminalService;
        private readonly IMerchantService _merchantService;
        private readonly StringBuilder _logMessage;
        readonly ILogger _logger = new Logger();
        CnTransactionEntities cnTransactionEntities;
        private string transResponse;
        Iso8583Response isoResponse;
        
        public CnBookerAppTokenPayment([Named("TransactionService")] ITransactionService transactionService, [Named("MerchantService")] IMerchantService merchantService, [Named("TerminalService")] ITerminalService terminalService, StringBuilder logMessage)
        {
            _transactionService = transactionService;
            _terminalService = terminalService;
            _logMessage = logMessage;
            _merchantService = merchantService;
        }

        public Iso8583Response CnBookerAppTransaction(TransactionRequest transactionRequest, CreateRCCIReq createRCCIreq, out int transactionId, out CardTokenDto storedCardToken)
        {
            StringBuilder logMessage = new StringBuilder();

            logMessage.AppendLine("CnBookerAppTransaction() process start:");

            logMessage.AppendLine(string.Format("[CnBApp.AppTrans] IndustryType:{0};TerminalId:{1};Stan:{2};PaymentType:{3};", transactionRequest.IndustryType, transactionRequest.TerminalId, transactionRequest.Stan, transactionRequest.PaymentType));

            try
            {
                string cnClientRef = CnTransactionMessage.GetCanadaClientRef();
                string referenceNumber = _transactionService.GetRefNumber();

                transactionRequest.TransRefNo = FormatFields.FormatReferenceNo(transactionRequest.TerminalId, transactionRequest.Stan, Convert.ToString(transactionRequest.PaymentType), referenceNumber);
                transactionRequest.Stan = BaseController.GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                cnTransactionEntities = new CnTransactionEntities();

                transactionRequest.CnPayload = CnTransactionMessage.CnPrepareBookerAppRequest(transactionRequest, cnClientRef, out cnTransactionEntities);

                //zzzzzz
                // Do we try and clear the DID and GroupID here, before transaction, but after authentication that happened earlier?
                // The CnPayload looks like it HAS to have the DID, otherwise we get an Authentication Error response.
                transactionRequest.Did = string.Empty;
                transactionRequest.GroupId = string.Empty;

                //zzz transactionRequest.TokenType = 

                logMessage.AppendLine(string.Format("[CnBApp.AppTrans] cnClientRef:{0};CnPayload:{1};", cnClientRef, transactionRequest.CnPayload));

                CardTokenDto cardTknDto = new CardTokenDto();
                if (transactionRequest.IndustryType == SigmaStaticValue.BookerAppToken)
                {
                    cardTknDto = CommonFunctions.XmlToCardTokenDto(transactionRequest);
                    cardTknDto.ExpiryDate = transactionRequest.ExpiryDate.Length >= 6 ? transactionRequest.ExpiryDate.Substring(2, 4).Encrypt() : string.Empty;
                    cardTknDto.Id = _transactionService.ManageCardToken(cardTknDto);

                    transactionRequest.CardType = (CardTypeType)Enum.Parse(typeof(CardTypeType), cardTknDto.CardType);
                    transactionRequest.ZipCode = cardTknDto.Postcode;       //PAY-12 get the Postcode\Zipcode from the Customer Token details.

                    logMessage.AppendLine(string.Format("[CnBApp.AppTrans] TknExpDate:{0};TknId:{1};TknCardType:{2};Zip:{3};TokenType:{4};", cardTknDto.ExpiryDate, cardTknDto.Id, transactionRequest.CardType, transactionRequest.ZipCode, transactionRequest.TokenType));
                }

                transactionId = SaveTransRequestInDatabase(transactionRequest);

                logMessage.AppendLine(string.Format("[CnBApp.AppTrans] TransactionID:{0};", transactionId));

                Global.ActiveUrl = _transactionService.GetTransactionUrl();
                transResponse = CnSendTransaction.SendMessageToDatawire(Global.ActiveUrl, transactionRequest.CnPayload);    //zzzzzz
                cnTransactionEntities.CompleteResponse = transResponse;

                logMessage.AppendLine(string.Format("[CnBApp.AppTrans] ActiveUrl:{0};transResponse:{1};", Global.ActiveUrl, transResponse));

                isoResponse = PaymentController.HandleResponse(cnTransactionEntities, transactionId);

                logMessage.AppendLine(string.Format("[CnBApp.AppTrans] isoResponse ReturnCode:{0};ResponseCode:{1};StatusCode:{2};Message:{3};", 
                    isoResponse.ReturnCode, isoResponse.ResponseCode, isoResponse.StatusCode, isoResponse.Message));

                // Perform service discovery if required
                SrsDiscovery(transResponse, transactionRequest.CnPayload, transactionId);

                // Send time out transaction if required
                if (CommonFunctions.IsFailed(isoResponse.ReturnCode))
                {
                    logMessage.AppendLine(string.Format("[CnBApp.AppTrans] Failed isoResponse.ReturnCode:{0};", isoResponse.ReturnCode));

                    CnTimeOutTransaction timeOutTransaction = new CnTimeOutTransaction(_transactionService, _terminalService, _logMessage);

                    isoResponse = timeOutTransaction.TimeOut(transactionRequest, transactionId, cnClientRef);
                    if (Object.Equals(isoResponse, null))
                    {
                        logMessage.AppendLine(string.Format("[CnBApp.AppTrans] Failed Timeout Trans NULL;"));

                        storedCardToken = null;
                        return null;
                    }
                    else
                    {
                        logMessage.AppendLine(string.Format("[CnBApp.AppTrans] Failed Timeout Trans isoResponse.ReturnCode:{0};", isoResponse.ReturnCode));
                    }
                }
                else
                {
                    logMessage.AppendLine(string.Format("[CnBApp.AppTrans] Passed isoResponse.ReturnCode:{0};", isoResponse.ReturnCode));
                }

                // Serailize and update the transaction response in the database
                string objResponse = Serialization.Serialize<Iso8583Response>(isoResponse);
                isoResponse.CompleteResponse = objResponse;

                if (transactionRequest.IndustryType == SigmaStaticValue.BookerAppToken && !object.Equals(isoResponse, null))
                {
                    if (isoResponse.CompleteResponse.IndexOf(PaymentAPIResources.TkApproval) != -1)
                        isoResponse.ResponseCode = PaymentAPIResources.ResponseCode00;
                }

                logMessage.AppendLine(string.Format("[CnBApp.AppTrans] isoResponse.ResponseCode:{0};IsTORTran:{1};", isoResponse.ResponseCode, transactionRequest.IsTORTransaction));

                if (!transactionRequest.IsTORTransaction)
                {
                    MTDTransactionDto cnTransactionDto = CnTransactionMessage.FillMerchantDto(isoResponse);
                    _transactionService.CnUpdate(cnTransactionDto, transactionId);
                }

                if (transactionRequest.IndustryType == SigmaStaticValue.BookerAppToken && !string.IsNullOrEmpty(transResponse) && !object.Equals(cardTknDto, null))
                {
                    if (!object.Equals(isoResponse, null))
                    {
                        logMessage.AppendLine(string.Format("[CnBApp.AppTrans] ResponseCode:{0};FDPrivateUsageData63:{1};", isoResponse.ResponseCode, isoResponse.FDPrivateUsageData63));

                        if (isoResponse.ResponseCode == PaymentAPIResources.ResponseCode00 &&  !string.IsNullOrEmpty(isoResponse.FDPrivateUsageData63))
                        {
                            int startTokenIndex;
                            if (transactionRequest.CardType.ToString() == CardTypeType.Diners.ToString())
                            {
                                startTokenIndex = isoResponse.FDPrivateUsageData63.IndexOf(PaymentAPIResources.Diners_TokenOnly_Prefix);
                                cardTknDto.Token = CnGenericHandler.ConvertHexToAscii(isoResponse.FDPrivateUsageData63.Substring(startTokenIndex + 10, 28));

                                logMessage.AppendLine(string.Format("[CnBApp.AppTrans] Diners: StartIndex:{0};Token:{1};", startTokenIndex, cardTknDto.Token));
                            }
                            else if (transactionRequest.CardType.ToString() == CardTypeType.Amex.ToString())
                            {
                                startTokenIndex = isoResponse.FDPrivateUsageData63.IndexOf(PaymentAPIResources.Amex_TokenOnly_Prefix);
                                cardTknDto.Token = CnGenericHandler.ConvertHexToAscii(isoResponse.FDPrivateUsageData63.Substring(startTokenIndex + 10, 30));

                                logMessage.AppendLine(string.Format("[CnBApp.AppTrans] Amex: StartIndex:{0};Token:{1};", startTokenIndex, cardTknDto.Token));
                            }
                            else
                            {
                                startTokenIndex = isoResponse.FDPrivateUsageData63.IndexOf(PaymentAPIResources.Mc_Visa_DS_TokenOnly_Prefix);
                                cardTknDto.Token = CnGenericHandler.ConvertHexToAscii(isoResponse.FDPrivateUsageData63.Substring(startTokenIndex + 10, 32));

                                logMessage.AppendLine(string.Format("[CnBApp.AppTrans] Mc_Visa_DS: StartIndex:{0};Token:{1};", startTokenIndex, cardTknDto.Token));
                            }

                            //zzzzzz
                            //PAY-12 We need to extract the AVS details from the transaction request to populate the token object.
                            if (createRCCIreq != null)
                            {
                                logMessage.AppendLine(string.Format("[CnBApp.AppTrans] Populate AVS address details against token object."));

                                cardTknDto.Unit = createRCCIreq.AVSDetails.Unit;
                                cardTknDto.StreetNumber = createRCCIreq.AVSDetails.StreetNumber;
                                cardTknDto.Street = createRCCIreq.AVSDetails.Street;
                                cardTknDto.AddressLine1 = createRCCIreq.AVSDetails.AddressLine1;
                                cardTknDto.AddressLine2 = createRCCIreq.AVSDetails.AddressLine2;
                                cardTknDto.Suburb = createRCCIreq.AVSDetails.Suburb;
                                cardTknDto.State = createRCCIreq.AVSDetails.State;
                                cardTknDto.Postcode = createRCCIreq.AVSDetails.Postcode;
                            }

                            _transactionService.ManageCardToken(cardTknDto);

                            isoResponse.Token = cardTknDto.Token;
                        }
                        else
                        {
                            logMessage.AppendLine(string.Format("[CnBApp.AppTrans] ResponseCode-FDPrivateUsageData63: failed.;"));
                        }
                    }
                    else
                    {
                        logMessage.AppendLine(string.Format("[CnBApp.AppTrans] isoResponse: failed.;"));
                    }
                }
                else
                {
                    logMessage.AppendLine(string.Format("[CnBApp.AppTrans] Industry-TransResponse-CardTknDto: failed.;"));
                }

                isoResponse.BookerAppCardExpiryDate = cnTransactionEntities.BookerAppCardExpiry;

                storedCardToken = cardTknDto;

                logMessage.AppendLine("[CnBApp.AppTrans] Process complete.");

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    _logger.LogInfoMessage(logMessage.ToString());

                return isoResponse;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(logMessage.ToString(), ex);

                transactionId = 0;
                storedCardToken = null;

                return null;
            }
        }

        public int SaveTransRequestInDatabase(TransactionRequest cnTransactionRequest)
        {
            int transactionId;
            try
            {
                cnTransactionRequest.TerminalId = cnTransactionRequest.TerminalId.FormatTerminalId();
                MTDTransactionDto transDto = CommonFunctions.XmlToTransactionDto(cnTransactionRequest);
                transDto.Currency = PaymentAPIResources.Cur_Cn;
                transDto.EntryMode = Convert.ToString(EntryMode.Keyed);
                transactionId = _transactionService.Add(transDto);

                _terminalService.UpdateStan(transDto.Stan, transDto.TransRefNo, cnTransactionRequest.TerminalId, cnTransactionRequest.MtdMerchantId);
            }
            catch
            {
                throw;
            }

            return transactionId;            
        }

        public void SrsDiscovery(string transResponse,string payload,int transactionId)
        {
            try
            {
                while (String.IsNullOrEmpty(transResponse))
                {
                    if (Global.UrlList != null)
                    {
                        if (Global.UrlList.Contains(Global.ActiveUrl))
                            Global.UrlList.Remove(Global.ActiveUrl);
                    }

                    string fastestUrl = _transactionService.GetFastestUrl(Global.ActiveUrl, Global.UrlList);
                    if (String.IsNullOrEmpty(fastestUrl))
                    {
                        TransactionRequestController transactionRequestController = new TransactionRequestController(_terminalService, _logMessage);
                        transactionRequestController.ServiceDiscovery();
                        Global.ActiveUrl = _transactionService.GetTransactionUrl();
                        transResponse = CnSendTransaction.SendMessageToDatawire(Global.ActiveUrl, payload);
                        cnTransactionEntities.CompleteResponse = transResponse;
                        isoResponse = PaymentController.HandleResponse(cnTransactionEntities, transactionId);
                    }
                    else
                    {
                        Global.ActiveUrl = fastestUrl;
                        transResponse = CnSendTransaction.SendMessageToDatawire(Global.ActiveUrl, payload);
                        cnTransactionEntities.CompleteResponse = transResponse;
                        isoResponse = PaymentController.HandleResponse(cnTransactionEntities, transactionId);
                    }

                    if (String.IsNullOrEmpty(transResponse))
                        continue;
                }
            }
            catch
            {
                throw;
            }
        }

    }
}