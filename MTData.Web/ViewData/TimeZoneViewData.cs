﻿namespace MTData.Web.ViewData
{
    public class TimeZoneViewData
    {
        public string TimeZoneName { get; set; }
        public string TimeZoneTime { get; set; }
    }
}