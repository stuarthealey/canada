﻿using System.Collections.Generic;

namespace MTData.Web.ViewData
{
    public class SearchTransaction
    {
        public TransactionViewData transaction { get; set; }
        public IEnumerable<SearchTransactionViewData> searchResult { get; set; }
    }
}