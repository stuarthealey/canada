﻿using System;
using System.IO;
using System.Xml;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Globalization;
using System.Reflection;

using MTD.Core.DataTransferObjects;
using MTData.Utility;
using MTData.WebAPI.Resources;
using MTData.WebAPI.Models;
using MTData.WebAPI.CanadaCert;

namespace MTData.WebAPI.Common
{
	public sealed class CommonFunctions
	{
		private const string Ecommerce = "Ecommerce";
		private const string Credit = "Credit";
		private const string Debit = "Debit";
		private const string Retail = "Retail";
		private const string TokenOnly = "TokenOnly";
		private const string CAPKUpdate = "CAPKUpdate";
		private const string CAPKDownload = "CAPKDownload";
		private const string BookerAppPayment = "BookerAppPayment";
		private const string FileDownload = "FileDownload";
		private const string POS = "POS";
		private const string Timeout = "Timeout";
		private const string AdminResponse = "AdminResponse";
		private const string RejectResponse = "RejectResponse";

        /// <summary>
        /// Purpose             :   to save FD request into database
        /// Function Name       :   XmlToTransactionDto
        /// Created By          :   Madhuri tanwar
        /// Created On          :   7/6/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   11/6/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns> 
        public static MTDTransactionDto XmlToTransactionDto(TransactionRequest transactionRequest)
		{
			MTDTransactionDto tDto = new MTDTransactionDto();
			tDto.MerchantId = Convert.ToInt32(transactionRequest.MtdMerchantId);
			tDto.TerminalId = transactionRequest.TerminalId;
			tDto.IsDispatchRequest = transactionRequest.IsDispatchRequest;
			tDto.VehicleNo = transactionRequest.VehicleNumber;
			tDto.DriverNo = transactionRequest.DriverNumber;
			tDto.TransNote = transactionRequest.TransNote;
			tDto.CreatedBy = transactionRequest.CreatedBy;
			tDto.CrdHldrAddress = transactionRequest.StreetAddress;
			tDto.CrdHldrZip = transactionRequest.ZipCode;
			tDto.DestinationAddress = transactionRequest.DestinationAddress;
			tDto.FleetFee = transactionRequest.FleetFee;
			tDto.TechFee = transactionRequest.TechFee;
			tDto.PickAddress = transactionRequest.PickAddress;

			if (!string.IsNullOrEmpty(transactionRequest.FleetId))
				tDto.fk_FleetId = Convert.ToInt32(transactionRequest.FleetId);

			if (!string.IsNullOrEmpty(transactionRequest.StartLatitude))
				tDto.StartLatitude = Convert.ToDouble(transactionRequest.StartLatitude);

			if (!string.IsNullOrEmpty(transactionRequest.EndLatitude))
				tDto.EndLatitude = Convert.ToDouble(transactionRequest.EndLatitude);

			if (!string.IsNullOrEmpty(transactionRequest.StartLongitude))
				tDto.StartLongitude = Convert.ToDouble(transactionRequest.StartLongitude);

			if (!string.IsNullOrEmpty(transactionRequest.EndLongitude))
				tDto.EndLongitude = Convert.ToDouble(transactionRequest.EndLongitude);

			tDto.Stan = transactionRequest.Stan;
			tDto.TransRefNo = transactionRequest.TransRefNo;
			tDto.TransOrderNo = transactionRequest.TransOrderNo;
			tDto.PaymentType = transactionRequest.PaymentType.ToString();

            // Commented due to card type not saved in db for debit transactions
            //if (transactionRequest.PaymentType.ToString() == Debit)
            //{
            //    tDto.CardType = string.Empty;
            //}
            //else
            //{
            //    tDto.CardType = transactionRequest.CardType.ToString();
            //}
            tDto.CardType = transactionRequest.CardType.ToString();

            tDto.TxnType = transactionRequest.TransactionType;
			tDto.LocalDateTime = transactionRequest.LocalDateTime;
			tDto.TransmissionDateTime = transactionRequest.TrnmsnDateTime;
			tDto.Amount = Convert.ToDecimal(transactionRequest.TransactionAmount);
            tDto.Currency = transactionRequest.CurrencyCode;    //added to set currency code passed in the request
            tDto.ExpiryDate = transactionRequest.ExpiryDate.Encrypt();

			tDto.Industry = transactionRequest.IndustryType;
			tDto.CrdHldrName = transactionRequest.CrdHldrName;
			tDto.CrdHldrPhone = transactionRequest.CrdHldrPhone;
			tDto.EntryMode = transactionRequest.EntryMode;
			tDto.ResponseCode = "";
			tDto.AddRespData = "";
			tDto.CardLvlResult = "";
			tDto.SrcReasonCode = "";
			tDto.GatewayTxnId = "";
			tDto.AuthId = "";
			tDto.AthNtwId = "";
			tDto.CardToken = "";
			tDto.RequestId = transactionRequest.RequestedId;

			if (!string.IsNullOrEmpty(transactionRequest.Fare))
				tDto.FareValue = Convert.ToDecimal(transactionRequest.Fare);
			
            if (!string.IsNullOrEmpty(transactionRequest.FlagFall))
				tDto.FlagFall = Convert.ToDecimal(transactionRequest.FlagFall);
			
            if (!string.IsNullOrEmpty(transactionRequest.Taxes))
				tDto.Taxes = Convert.ToDecimal(transactionRequest.Taxes);
			
            if (!string.IsNullOrEmpty(transactionRequest.Tip))
				tDto.Tip = Convert.ToDecimal(transactionRequest.Tip);
			
            if (!string.IsNullOrEmpty(transactionRequest.Fee))
				tDto.Fee = Convert.ToDecimal(transactionRequest.Fee);
			
            if (!string.IsNullOrEmpty(transactionRequest.GateFee))
				tDto.GateFee = Convert.ToDecimal(transactionRequest.GateFee);
			
            if (!string.IsNullOrEmpty(transactionRequest.Surcharge))
				tDto.Surcharge = Convert.ToDecimal(transactionRequest.Surcharge);
			
            if (!string.IsNullOrEmpty(transactionRequest.Tolls))
				tDto.Toll = Convert.ToDecimal(transactionRequest.Tolls);
			
            if (!string.IsNullOrEmpty(transactionRequest.Extras))
				tDto.Extras = Convert.ToDecimal(transactionRequest.Extras);
			
            if (!string.IsNullOrEmpty(transactionRequest.Others))
				tDto.OthersFee = Convert.ToDecimal(transactionRequest.Others);

			tDto.JobNumber = transactionRequest.JobNumber;

			tDto.FirstFourDigits = transactionRequest.FirstFourCard;
			tDto.LastFourDigits = transactionRequest.LastFourcard;
			tDto.SerialNo = transactionRequest.SerialNumber;
			tDto.SourceId = transactionRequest.RapidConnectAuthId;

			if (transactionRequest.IndustryType != BookerAppPayment && transactionRequest.IndustryType != PaymentAPIResources.BookerAppIndustryType)
			{
				if (transactionRequest.TransactionType == Timeout && transactionRequest.IndustryType == Ecommerce)
				{
					transactionRequest.CardNumber = transactionRequest.CardNumber;
				}
				else if (transactionRequest.TransactionType != Timeout)
				{
					if (transactionRequest.CurrencyCode != PaymentAPIResources.Cur_Cn)
					{
						transactionRequest.ExpiryDate = string.Empty;
						transactionRequest.CardNumber = transactionRequest.CardNumber.MaskCardNumber();
					}
				}
			}

			if (transactionRequest.CurrencyCode != PaymentAPIResources.Cur_Cn)
				transactionRequest.CardCvv = string.Empty;

			transactionRequest.Track1Data = string.Empty;
            
            if (transactionRequest.IndustryType != PaymentIndustryType.Retail.ToString())
                transactionRequest.Track2Data = string.Empty;
            
            transactionRequest.Track3Data = transactionRequest.Track3Data;
			transactionRequest.KeyId = transactionRequest.KeyId;
			tDto.RequestXml = CreateXml(transactionRequest);

			return tDto;
		}

		/// <summary>
		/// Purpose             :   Insert Card data into database
		/// Function Name       :   XmlToCardTokenDto
		/// Created By          :   Sunil Singh
		/// Created On          :   29/6/2015
		/// Modificatios Made   :   ****************************
		/// Modidified On       :    
		/// </summary>
		/// <param name="transactionRequest"></param>
		/// <returns></returns>
		public static CardTokenDto XmlToCardTokenDto(TransactionRequest transactionRequest)
		{
			try
			{
				CardTokenDto cardTokenDto = new CardTokenDto
				{
					CardType = CreditCardType.GetCardType(transactionRequest.CardNumber).ToString(),
					CardNumber = transactionRequest.CardNumber.MaskCardNumber(),
					Token = transactionRequest.RCCIToken,
					CustomerId = transactionRequest.CustomerId,
					ExpiryDate = transactionRequest.ExpiryDate.Encrypt()
				};

				return cardTokenDto;
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Purpose             :   Updating response of credit, debit, transarmor response
		/// Function Name       :   XmlResponseToTransactionDto
		/// Created By          :   Salil Gupta
		/// Created On          :   06/6/2015
		/// Modificatios Made   :   ****************************
		/// Modidified On       :   06/6/2015 "MM/DD/YYYY" 
		/// </summary>
		/// <param name="xmlSerializedTransResp"></param>
		/// <param name="isTimeout"></param>
		/// <param name="typePay"></param>
		/// <param name="industryType"></param>
		/// <returns></returns>
		public static MTDTransactionDto XmlResponseToTransactionDto(string xmlSerializedTransResp, bool isTimeout, string typePay, string industryType)
		{
			MTDTransactionDto tDto = new MTDTransactionDto();
			if (!isTimeout)
			{
				xmlSerializedTransResp = xmlSerializedTransResp.HandleCdata();

				XmlDocument xmlDoc = new XmlDocument();
				xmlSerializedTransResp = xmlSerializedTransResp.HandleCdata();//To handle invalid request error data
				xmlDoc.LoadXml(xmlSerializedTransResp);

				string typeResponse = GetTagName(xmlDoc);

				switch (industryType)
				{
					case Ecommerce:
						if (typePay == Credit)
						{
							switch (typeResponse)
							{
								case "CreditResponse":
									string xmlString = GetXmlAsString(xmlDoc);
									var response = GetResponse(xmlString);
									var resp = XmlToObject(response, typeof(CreditResponse));
									tDto = ResponseToDatabase(resp);
									break;

								case "ReversalResponse":
									string reversalxmlString = GetXmlAsString(xmlDoc);
									var reversalxmlResponse = GetResponse(reversalxmlString);
									var reversalResp = XmlToObject(reversalxmlResponse, typeof(ReversalResponse));
									tDto = ResponseToDatabase(reversalResp);
									break;

								case "RejectResponse":
									string rejectxmlString = GetXmlAsString(xmlDoc);
									var rejectxmlResponse = GetResponse(rejectxmlString);
									var rejectResp = XmlToObject(rejectxmlResponse, typeof(RejectResponse));
									tDto = ResponseToDatabase(rejectResp);
									break;
							}
						}

						if (typePay == Debit)
						{
							switch (typeResponse)
							{
								case "PinlessDebitResponse":
									string pinlessxmlString = GetXmlAsString(xmlDoc);
									var pinlessxmlresponse = GetResponse(pinlessxmlString);
									var pinlessxmlResp = XmlToObject(pinlessxmlresponse, typeof(PinlessDebitResponse));
									tDto = ResponseToDatabase(pinlessxmlResp);
									break;

								case "RejectResponse":
									string rejectxmlString = GetXmlAsString(xmlDoc);
									var rejectxmlResponse = GetResponse(rejectxmlString);
									var rejectResp = XmlToObject(rejectxmlResponse, typeof(RejectResponse));
									tDto = ResponseToDatabase(rejectResp);
									break;
							}
						}
						break;

					case Retail:
						if (typePay == Credit)
						{
							switch (typeResponse)
							{
								case "CreditResponse":
									string xmlString = GetXmlAsString(xmlDoc);
									var response = GetResponse(xmlString);
									var resp = XmlToObject(response, typeof(CreditResponse));
									tDto = ResponseToDatabase(resp);
									break;

								case "ReversalResponse":
									string reversalxmlString = GetXmlAsString(xmlDoc);
									var reversalxmlResponse = GetResponse(reversalxmlString);
									var reversalResp = XmlToObject(reversalxmlResponse, typeof(ReversalResponse));
									tDto = ResponseToDatabase(reversalResp);
									break;

								case "RejectResponse":
									string rejectxmlString = GetXmlAsString(xmlDoc);
									var rejectxmlResponse = GetResponse(rejectxmlString);
									var rejectResp = XmlToObject(rejectxmlResponse, typeof(RejectResponse));
									tDto = ResponseToDatabase(rejectResp);
									break;
							}
						}

						if (typePay == Debit)
						{
							switch (typeResponse)
							{
								case "DebitResponse":
									string pinlessxmlString = GetXmlAsString(xmlDoc);
									var pinlessxmlresponse = GetResponse(pinlessxmlString);
									var pinlessxmlResp = XmlToObject(pinlessxmlresponse, typeof(DebitResponse));
									tDto = ResponseToDatabase(pinlessxmlResp);
									break;

								case "ReversalResponse":
									string reversalxmlString = GetXmlAsString(xmlDoc);
									var reversalxmlResponse = GetResponse(reversalxmlString);
									var reversalResp = XmlToObject(reversalxmlResponse, typeof(ReversalResponse));
									tDto = ResponseToDatabase(reversalResp);
									break;

								case "RejectResponse":
									string rejectxmlString = GetXmlAsString(xmlDoc);
									var rejectxmlResponse = GetResponse(rejectxmlString);
									var rejectResp = XmlToObject(rejectxmlResponse, typeof(RejectResponse));
									tDto = ResponseToDatabase(rejectResp);
									break;
							}
						}
						break;

					case TokenOnly:
						switch (typeResponse)
						{
							case "TransArmorResponse":
								string transArmorxmlString = GetXmlAsString(xmlDoc);
								var transArmorresponse = GetResponse(transArmorxmlString);
								var transArmorResp = XmlToObject(transArmorresponse, typeof(TransArmorResponse));
								tDto = ResponseToDatabase(transArmorResp);
								break;

							case "RejectResponse":
								string rejectxmlString = GetXmlAsString(xmlDoc);
								var rejectxmlResponse = GetResponse(rejectxmlString);
								var rejectResp = XmlToObject(rejectxmlResponse, typeof(RejectResponse));
								tDto = ResponseToDatabase(rejectResp);
								break;

							case "CreditResponse":
								string creditRespxmlString = GetXmlAsString(xmlDoc);
								var creditxmlResponse = GetResponse(creditRespxmlString);
								var creditResp = XmlToObject(creditxmlResponse, typeof(CreditResponse));
								tDto = ResponseToDatabase(creditResp);
								break;
						}
						break;

					case BookerAppPayment:
						if (typePay == Credit)
						{
							switch (typeResponse)
							{
								case "CreditResponse":
									string xmlString = GetXmlAsString(xmlDoc);
									var response = GetResponse(xmlString);
									var resp = XmlToObject(response, typeof(CreditResponse));
									tDto = ResponseToDatabase(resp);
									break;

								case "ReversalResponse":
									string reversalxmlString = GetXmlAsString(xmlDoc);
									var reversalxmlResponse = GetResponse(reversalxmlString);
									var reversalResp = XmlToObject(reversalxmlResponse, typeof(ReversalResponse));
									tDto = ResponseToDatabase(reversalResp);
									break;

								case "RejectResponse":
									string rejectxmlString = GetXmlAsString(xmlDoc);
									var rejectxmlResponse = GetResponse(rejectxmlString);
									var rejectResp = XmlToObject(rejectxmlResponse, typeof(RejectResponse));
									tDto = ResponseToDatabase(rejectResp);
									break;
							}
						}
						break;
				}

				xmlDoc.LoadXml(xmlSerializedTransResp);
				XmlSerializer xsSubmit = new XmlSerializer(typeof(XmlDocument));

				var subReq = xmlDoc;
				using (StringWriter sww = new StringWriter())
				{
					XmlWriter writer = XmlWriter.Create(sww);
					xsSubmit.Serialize(writer, subReq);
					var xml = sww.ToString();
					tDto.ResponseXml = xml;
				}
			}
			else
			{
				tDto.AddRespData = Timeout;
				tDto.ResponseCode = "205";
			}

			return tDto;
		}

		/// <summary>
		/// Purpose             :   Create transaction xml
		/// Function Name       :   CreateXml
		/// Created By          :   Salil Gupta
		/// Created On          :   06/6/2015
		/// Modificatios Made   :   ****************************
		/// Modidified On       :   06/6/2015 "MM/DD/YYYY" 
		/// </summary>
		/// <param name="transactionRequest"></param>
		/// <returns></returns>
		public static string CreateXml(TransactionRequest transactionRequest)
		{
			XmlDocument xmlDoc = new XmlDocument();   //Represents an XML document, 
			XmlSerializer xmlSerializer = new XmlSerializer(transactionRequest.GetType());

			using (MemoryStream xmlStream = new MemoryStream())
			{
				xmlSerializer.Serialize(xmlStream, transactionRequest);
				xmlStream.Position = 0;

				xmlDoc.Load(xmlStream);
				return xmlDoc.InnerXml;
			}
		}

        /// <summary>
        /// Purpose             :   Converting xml to object
        /// Function Name       :   XmlToObject
        /// Created By          :   Madhuri tanwar
        /// Created On          :   06/6/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   06/6/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="xmlString"></param>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public static Object XmlToObject(string xmlString, Type objectType)
		{
			try
			{
				using (StringReader strReader = new StringReader(xmlString))
				{
					XmlTextReader xmlReader = new XmlTextReader(strReader);
					XmlSerializer serializer = new XmlSerializer(objectType);
					Object anObject = serializer.Deserialize(xmlReader);
					return anObject;
				}
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Purpose             :   Getting XML as string
		/// Function Name       :   GetXmlAsString
		/// Created By          :   Madhuri tanwar
		/// Created On          :   06/6/2015
		/// Modificatios Made   :   ****************************
		/// Modidified On       :   06/6/2015 "MM/DD/YYYY" 
		/// </summary>
		/// <param name="myxml"></param>
		/// <returns></returns>
		public static string GetXmlAsString(XmlDocument myxml)
		{
			return myxml.OuterXml;
		}

		/// <summary>
		/// Purpose             :   Getting XML response
		/// Function Name       :   GetResponse
		/// Created By          :   Madhuri tanwar
		/// Created On          :   06/6/2015
		/// Modificatios Made   :   ****************************
		/// Modidified On       :   06/6/2015 "MM/DD/YYYY" 
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static string GetResponse(string str)
		{
			string value1 = str;
			int i = GetNthIndex(value1, '<', 3);
			string val1 = value1.Substring(i, value1.Length - i - 6);
			return val1;
		}

		/// <summary>
		/// Purpose             :   Getting XML response
		/// Function Name       :   GetMCResponse
		/// Created By          :   Madhuri tanwar
		/// Created On          :   10/6/2015
		/// Modificatios Made   :   ****************************
		/// Modidified On       :   10/6/2015 "MM/DD/YYYY" 
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns> 
		public static string GetMCResponse(string str)
		{
			string value1 = str;
			int i = GetNthIndex(value1, '<', 2);
			string val1 = value1.Substring(i, value1.Length - i - 6);
			return val1;
		}

		public static string GetDWResponse(string str)
		{
			string value1 = str;
			int i = GetNthIndex(value1, '<', 1);
			string val1 = value1.Substring(i, value1.Length - i - 0);
			return val1;
		}

		public static string GetPIResponse(string str)
		{
			string value1 = str;
			int i = GetNthIndex(value1, '<', 3);
			string val1 = value1.Substring(i, value1.Length - i - 0);
			return val1;
		}

		/// <summary>
		/// Purpose             :   Getting XML response
		/// Function Name       :   GetCNResponse
		/// Created By          :   Madhuri tanwar
		/// Created On          :   04/01/2016
		/// Modificatios Made   :   ****************************
		/// Modidified On       :    "MM/DD/YYYY" 
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns> 
		public static string GetCNResponse(string str)
		{
			string value1 = str;
			int i = GetNthIndex(value1, '<', 3);
			string val1 = value1.Substring(i, value1.Length - i - 0);
			return val1;
		}

		/// <summary>
		/// Purpose             :   Getting XML response
		/// Function Name       :   GetCapkResponse
		/// Created By          :   Madhuri tanwar
		/// Created On          :   10/29/2016
		/// Modificatios Made   :   ****************************
		/// Modidified On       :   "MM/DD/YYYY" 
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns> 
		public static string GetCapkResponse(string str)
		{
			string value1 = str;
			int i = GetNthIndex(value1, '<', 3);
			string val1 = value1.Substring(i, value1.Length - i - 6);
			return val1;
		}

		/// <summary>
		/// Purpose             :   Getting Index
		/// Function Name       :   GetNthIndex
		/// Created By          :   Madhuri tanwar
		/// Created On          :   06/6/2015
		/// Modificatios Made   :   ****************************
		/// Modidified On       :   06/6/2015 "MM/DD/YYYY"  
		/// </summary>
		/// <param name="s"></param>
		/// <param name="t"></param>
		/// <param name="n"></param>
		/// <returns></returns>
		private static int GetNthIndex(string s, char t, int n)
		{
			int count = 0;
			for (int i = 0; i < s.Length; i++)
			{
				if (s[i] == t)
				{
					count++;
					if (count == n)
						return i;
				}
			}

			return -1;
		}

		/// <summary>
		/// Purpose             :   Saving response to database
		/// Function Name       :   ResponseToDatabase
		/// Created By          :   Madhuri tanwar
		/// Created On          :   06/6/2015
		/// Modificatios Made   :   ****************************
		/// Modidified On       :   06/6/2015 "MM/DD/YYYY"  
		/// </summary>
		/// <param name="objectType"></param>
		/// <returns></returns>
		public static MTDTransactionDto ResponseToDatabase(dynamic objectType)
		{
			try
			{
				MTDTransactionDto tDto = new MTDTransactionDto();

				if (objectType.CommonGrp != null)
				{
					tDto.LocalDateTime = objectType.CommonGrp.LocalDateTime;
					tDto.TransmissionDateTime = objectType.CommonGrp.TrnmsnDateTime;
					tDto.TxnType = objectType.CommonGrp.TxnType.ToString();
				}

				tDto.TxnDate = DateTime.Now;

				if (objectType.RespGrp != null)
				{
					tDto.ResponseCode = objectType.RespGrp.RespCode;
					tDto.AddRespData = objectType.RespGrp.AddtlRespData ?? objectType.RespGrp.ErrorData;
					tDto.AuthId = objectType.RespGrp.AuthID;
					tDto.AthNtwId = objectType.RespGrp.AthNtwkID;
				}

                //D#6108 Need to handle Approved for Lesser amount response (002) and update the transaction amount to reflect the response TxnAmt
                ILogger logger = new Logger();

                if ((objectType.CommonGrp != null) && (objectType.RespGrp != null))
                {
                    // Check the response is an APPRV LESSER AMT (RespCode=002)
                    if (objectType.RespGrp.RespCode != null)
                    {
                        if (Convert.ToString(objectType.RespGrp.RespCode) == "002")
                        {
                            if (objectType.CommonGrp.TxnAmt != null)
                            {
                                int responseTxnAmt = 0;

                                if (Int32.TryParse(Convert.ToString(objectType.CommonGrp.TxnAmt), out responseTxnAmt))
                                {
                                    tDto.Amount = Convert.ToDecimal(responseTxnAmt / 100.00);
                                }
                                else
                                {
                                    //logger.LogInfoMessage(string.Format("CommonFunctions.ResponseToDB. TxnAmt TryParse() failed.", objectType.CommonGrp.TxnAmt));
                                }
                            }
                        }
                    }
                }

                //TA group
				if (objectType.TAGrp != null)
					tDto.CardToken = StringExtension.Encrypt(objectType.TAGrp.Tkn.ToString());

				//VisaGrp group
				if (objectType.VisaGrp != null)
				{
					tDto.CardLvlResult = objectType.VisaGrp.CardLevelResult;
					tDto.SrcReasonCode = objectType.VisaGrp.SourceReasonCode;
				}

				//AmexGrp group
				if (objectType.AmexGrp != null)
				{
					tDto.GatewayTxnId = objectType.AmexGrp.AmExTranID;
					tDto.CardToken = StringExtension.Encrypt(objectType.TAGrp.Tkn.ToString());
				}

				//DSGrp
				if (objectType.DSGrp != null)
					tDto.GatewayTxnId = objectType.DSGrp.DiscNRID;

				return tDto;
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Purpose             :   Getting XML tag name from string
		/// Function Name       :   GetTagName
		/// Created By          :   Sunil Singh
		/// Created On          :   06/6/2015
		/// Modificatios Made   :   ****************************
		/// Modidified On       :   06/6/2015 "MM/DD/YYYY"  
		/// </summary>
		/// <param name="param"></param>
		/// <returns></returns>
		public static string GetTagName(string param)
		{
			string value = param.Replace("</GMF>", " ");
			int j = value.LastIndexOf('<');
			string tagName = value.Substring(j, value.Length - j);
			string s = tagName.Replace("</", " ").Replace('>', ' ').Trim();

			return s;
		}

		/// <summary>
		/// Purpose             :   Getting XML tag name from xml document
		/// Function Name       :   GetTagName
		/// Created By          :   Sunil Singh
		/// Created On          :   06/6/2015
		/// Modificatios Made   :   ****************************
		/// Modidified On       :   06/6/2015 "MM/DD/YYYY"  
		/// </summary>
		/// <param name="xmlDoc"></param>
		/// <returns></returns>
		public static string GetTagName(XmlDocument xmlDoc)
		{
			string tagName = string.Empty;
			if (xmlDoc.DocumentElement != null)
			{
				XmlElement elem = (XmlElement)xmlDoc.DocumentElement.FirstChild;
				tagName = elem.Name;
			}

			return tagName;
		}

		/// <summary>
		/// Purpose             :   Preparing transaction response for android device
		/// Function Name       :   TransactionResponse
		/// Created By          :   Salil Gupta
		/// Created On          :   06/6/2015
		/// Modificatios Made   :   ****************************
		/// Modidified On       :   06/6/2015 "MM/DD/YYYY"  
		/// </summary>
		/// <param name="transactionRequest"></param>
		/// <param name="xmlSerializedTransResp"></param>
		/// <param name="typePay"></param>
		/// <param name="industryType"></param>
		/// <param name="txnId"></param>
		/// <returns></returns>
		public static PaymentResponse TransactionResponse(TransactionRequest transactionRequest, string xmlSerializedTransResp, string typePay, string industryType, int txnId)
		{
			PaymentResponse paymentResponse = new PaymentResponse();
			XmlDocument xmlDoc = new XmlDocument();
			xmlSerializedTransResp = xmlSerializedTransResp.HandleCdata();//To handle invalid request error data
			xmlDoc.LoadXml(xmlSerializedTransResp);

			string typeResponse = GetTagName(xmlDoc);
			switch (industryType)
			{
				// Ecommerce
				case Ecommerce:
					if (typePay == Credit)
					{
						switch (typeResponse)
						{
							case "CreditResponse":
								string xmlString = GetXmlAsString(xmlDoc);
								var response = GetResponse(xmlString);
								var resp = XmlToObject(response, typeof(CreditResponse));
								paymentResponse = ResponsePayment(resp, transactionRequest, txnId);
								break;

							case "ReversalResponse":
								string reversalxmlString = GetXmlAsString(xmlDoc);
								var reversalxmlResponse = GetResponse(reversalxmlString);
								var reversalResp = XmlToObject(reversalxmlResponse, typeof(ReversalResponse));
								paymentResponse = ResponsePayment(reversalResp, transactionRequest, txnId);
								break;

							case "RejectResponse":
								string rejectxmlString = GetXmlAsString(xmlDoc);
								var rejectxmlResponse = GetResponse(rejectxmlString);
								var rejectResp = XmlToObject(rejectxmlResponse, typeof(RejectResponse));
								paymentResponse = ResponsePayment(rejectResp, transactionRequest, txnId);
								break;
						}
					}
					if (typePay == Debit)
					{
						switch (typeResponse)
						{
							case "PinlessDebitResponse":
								string pinlessxmlString = GetXmlAsString(xmlDoc);
								var pinlessxmlresponse = GetResponse(pinlessxmlString);
								var pinlessxmlResp = XmlToObject(pinlessxmlresponse, typeof(PinlessDebitResponse));
								paymentResponse = ResponsePayment(pinlessxmlResp, transactionRequest, txnId);
								break;

							case "RejectResponse":
								string rejectxmlString = GetXmlAsString(xmlDoc);
								var rejectxmlResponse = GetResponse(rejectxmlString);
								var rejectResp = XmlToObject(rejectxmlResponse, typeof(RejectResponse));
								paymentResponse = ResponsePayment(rejectResp, transactionRequest, txnId);
								break;
						}
					}
					break;

				// Retail
				case Retail:
					if (typePay == Credit)
					{
						switch (typeResponse)
						{
							case "CreditResponse":
								string xmlString = GetXmlAsString(xmlDoc);
								var response = GetResponse(xmlString);
								var resp = XmlToObject(response, typeof(CreditResponse));
								paymentResponse = ResponsePayment(resp, transactionRequest, txnId);
								break;

							case "ReversalResponse":
								string reversalxmlString = GetXmlAsString(xmlDoc);
								var reversalxmlResponse = GetResponse(reversalxmlString);
								var reversalResp = XmlToObject(reversalxmlResponse, typeof(ReversalResponse));
								paymentResponse = ResponsePayment(reversalResp, transactionRequest, txnId);
								break;

							case "RejectResponse":
								string rejectxmlString = GetXmlAsString(xmlDoc);
								var rejectxmlResponse = GetResponse(rejectxmlString);
								var rejectResp = XmlToObject(rejectxmlResponse, typeof(RejectResponse));
								paymentResponse = ResponsePayment(rejectResp, transactionRequest, txnId);
								break;
						}
					}

					if (typePay == Debit)
					{
						switch (typeResponse)
						{
							case "DebitResponse":
								string pinlessxmlString = GetXmlAsString(xmlDoc);
								var pinlessxmlresponse = GetResponse(pinlessxmlString);
								var pinlessxmlResp = XmlToObject(pinlessxmlresponse, typeof(DebitResponse));
								paymentResponse = ResponsePayment(pinlessxmlResp, transactionRequest, txnId);
								break;

							case "ReversalResponse":
								string reversalxmlString = GetXmlAsString(xmlDoc);
								var reversalxmlResponse = GetResponse(reversalxmlString);
								var reversalResp = XmlToObject(reversalxmlResponse, typeof(ReversalResponse));
								paymentResponse = ResponsePayment(reversalResp, transactionRequest, txnId);
								break;

							case "RejectResponse":
								string rejectxmlString = GetXmlAsString(xmlDoc);
								var rejectxmlResponse = GetResponse(rejectxmlString);
								var rejectResp = XmlToObject(rejectxmlResponse, typeof(RejectResponse));
								paymentResponse = ResponsePayment(rejectResp, transactionRequest, txnId);
								break;
						}
					}
					break;
			}

			return paymentResponse;
		}

		/// <summary>
		/// Purpose             :   Sending transaction response for android device
		/// Function Name       :   ResponsePayment
		/// Created By          :   Salil Gupta
		/// Created On          :   06/6/2015
		/// Modificatios Made   :   ****************************
		/// Modidified On       :   06/6/2015 "MM/DD/YYYY"  
		/// </summary>
		/// <param name="objectType"></param>
		/// <param name="transactionRequest"></param>
		/// <param name="tId"></param>
		/// <returns></returns>
		public static PaymentResponse ResponsePayment(dynamic objectType, TransactionRequest transactionRequest, int tId)
		{
			try
			{
				PaymentResponse paymentResponse = new PaymentResponse();
				paymentResponse.Header = new LoginHeader();
				paymentResponse.TransactionDetails = new TransactionDetails();
				paymentResponse.Header.DeviceId = transactionRequest.SerialNumber;
				paymentResponse.Header.RequestId = transactionRequest.RequestedId;

				string respMgs = string.Empty;
				string status = string.Empty;
				if (objectType.RespGrp.AddtlRespData == null)
					status = PaymentAPIResources.Failed;
				else
				{
					respMgs = objectType.RespGrp.RespCode;
					{
						if (respMgs.Equals("000") || respMgs.Equals("002"))
						    status = PaymentAPIResources.Success;
						else
							status = PaymentAPIResources.Failed;
					}
				}

				paymentResponse.Header.Status = status;
				paymentResponse.Header.Message = objectType.RespGrp.AddtlRespData ?? objectType.RespGrp.ErrorData;
				paymentResponse.TransactionDetails.PaymentType = transactionRequest.PaymentType.ToString();
				paymentResponse.TransactionDetails.TransNote = transactionRequest.TransNote;
				paymentResponse.TransactionDetails.CardType = (transactionRequest.PaymentType.ToString() == Credit) ? transactionRequest.CardType.ToString() : Debit;
				paymentResponse.TransactionDetails.EntryMode = transactionRequest.EntryMode;
				paymentResponse.TransactionDetails.TransactionType = transactionRequest.TransactionType;

				if (objectType.CommonGrp != null)
					paymentResponse.TransactionDetails.TransactionAmount = Convert.ToString(Convert.ToDecimal(string.IsNullOrEmpty(objectType.CommonGrp.TxnAmt) ? "0" : objectType.CommonGrp.TxnAmt) / 100);

				paymentResponse.TransactionDetails.IndustryType = transactionRequest.IndustryType;
				paymentResponse.TransactionDetails.DriverNumber = transactionRequest.DriverNumber;
				paymentResponse.TransactionDetails.VehicleNumber = transactionRequest.VehicleNumber;
				paymentResponse.TransactionDetails.CreatedBy = transactionRequest.CreatedBy;
				paymentResponse.TransactionDetails.StreetAddress = transactionRequest.StreetAddress;
				paymentResponse.TransactionDetails.PickUpLat = transactionRequest.StartLatitude;
				paymentResponse.TransactionDetails.PickUpLng = transactionRequest.StartLongitude;
				paymentResponse.TransactionDetails.PickUpAdd = transactionRequest.PickAddress;
				paymentResponse.TransactionDetails.DropOffLat = transactionRequest.EndLatitude;
				paymentResponse.TransactionDetails.DropOffLng = transactionRequest.StartLongitude;
				paymentResponse.TransactionDetails.DropOffAdd = transactionRequest.DestinationAddress;
				paymentResponse.TransactionDetails.ZipCode = transactionRequest.ZipCode;
				paymentResponse.TransactionDetails.AuthId = objectType.RespGrp.AuthID;
				paymentResponse.TransactionDetails.TransactionId = Convert.ToString(tId);
				paymentResponse.TransactionDetails.RespCode = objectType.RespGrp.RespCode;
				paymentResponse.TransactionDetails.CardNumber = transactionRequest.CardNumber;
				paymentResponse.TransactionDetails.MID = transactionRequest.FdMerchantId;
				paymentResponse.TransactionDetails.TID = transactionRequest.TerminalId;

				if (objectType.EMVGrp != null)
				{
					string emvResponse = objectType.EMVGrp.EMVData;
					paymentResponse.TransactionDetails.EmvResponse = string.IsNullOrEmpty(emvResponse) ? string.Empty : emvResponse.GetEMVResponse();
				}
				else
				{
					if (transactionRequest.EntryMode == PaymentEntryMode.EmvContact.ToString() || transactionRequest.EntryMode == PaymentEntryMode.EmvContactless.ToString())
					{
						respMgs = objectType.RespGrp.RespCode;
						{
							if (respMgs.Equals("000") || respMgs.Equals("002"))
								paymentResponse.TransactionDetails.EmvResponse = "T8A:02:3030";
							else
								paymentResponse.TransactionDetails.EmvResponse = "T8A:02:3035";
						}
					}
				}

				return paymentResponse;
			}
			catch
			{
				throw;
			}
		}

        /// <summary>
        /// Purpose             :   Updating response of capk
        /// Function Name       :   CapkXmlToTransactionDto
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   10/26/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        public static MTDTransactionDto CapkXmlToTransactionDto(TransactionRequest transactionRequest)
		{
			MTDTransactionDto tDto = new MTDTransactionDto();
			tDto.MerchantId = Convert.ToInt32(transactionRequest.MtdMerchantId);
			tDto.TerminalId = transactionRequest.TerminalId;
			tDto.DriverNo = transactionRequest.DriverNumber;

			if (!string.IsNullOrEmpty(transactionRequest.FleetId))
				tDto.fk_FleetId = Convert.ToInt32(transactionRequest.FleetId);

			tDto.SerialNo = transactionRequest.SerialNumber;
			tDto.Stan = transactionRequest.Stan;
			tDto.SourceId = transactionRequest.RapidConnectAuthId;
			tDto.TxnType = transactionRequest.TransactionType;
			tDto.LocalDateTime = transactionRequest.LocalDateTime;
			tDto.TransmissionDateTime = transactionRequest.TrnmsnDateTime;
			tDto.Industry = transactionRequest.IndustryType;
			tDto.CreatedBy = POS;
			tDto.RequestId = transactionRequest.RequestedId;
			tDto.RequestXml = CreateXml(transactionRequest);

			return tDto;
		}

		/// <summary>
		/// Purpose             :   Updating response of credit, debit, transarmor response
		/// Function Name       :   XmlEmvCapkResponseToTransactionDto
		/// Created By          :   Madhuri Tanwar
		/// Created On          :   10/29/2015
		/// Modificatios Made   :   ****************************
		/// Modidified On       :    "MM/DD/YYYY" 
		/// </summary>
		/// <param name="xmlSerializedTransResp"></param>
		/// <param name="isTimeout"></param>
		/// <param name="typePay"></param>
		/// <param name="industryType"></param>
		/// <returns></returns>
		public static MTDTransactionDto XmlEmvCapkResponseToTransactionDto(string xmlSerializedTransResp, bool isTimeout, string TransType)
		{
			MTDTransactionDto tDto = new MTDTransactionDto();
			if (!isTimeout)
			{
				xmlSerializedTransResp = xmlSerializedTransResp.HandleCdata();

				XmlDocument xmlDoc = new XmlDocument();
				xmlSerializedTransResp = xmlSerializedTransResp.HandleCdata();//To handle invalid request error data
				xmlDoc.LoadXml(xmlSerializedTransResp);

				string typeResponse = GetTagName(xmlDoc);
				switch (TransType)
				{
					case FileDownload:
						switch (typeResponse)
						{
							case AdminResponse:
								string adminResponsexmlString = GetXmlAsString(xmlDoc);
								var adminResponse = GetResponse(adminResponsexmlString);
								var adminResponseResp = XmlToObject(adminResponse, typeof(AdminResponse));
								tDto = ResponseToDatabase(adminResponseResp);
								break;

							case RejectResponse:
								string rejectxmlString = GetXmlAsString(xmlDoc);
								var rejectxmlResponse = GetResponse(rejectxmlString);
								var rejectResp = XmlToObject(rejectxmlResponse, typeof(RejectResponse));
								tDto = ResponseToDatabase(rejectResp);
								break;
						}
						break;
				}

				xmlDoc.LoadXml(xmlSerializedTransResp);
				XmlSerializer xsSubmit = new XmlSerializer(typeof(XmlDocument));
				var subReq = xmlDoc;
				using (StringWriter sww = new StringWriter())
				{
					XmlWriter writer = XmlWriter.Create(sww);
					xsSubmit.Serialize(writer, subReq);
					var xml = sww.ToString();
					tDto.ResponseXml = xml;
				}
			}
			else
			{
				tDto.AddRespData = Timeout;
				tDto.ResponseCode = "205";
			}

			return tDto;
		}

		/// <summary>
		/// Purpose             :   Preparing transaction response for android device
		/// Function Name       :   CapkTransactionResponse
		/// Created By          :   Madhuri Tanwar
		/// Created On          :   10/29/2015
		/// Modificatios Made   :   ****************************
		/// Modidified On       :   "MM/DD/YYYY"  
		/// </summary>
		/// <param name="transactionRequest"></param>
		/// <param name="xmlSerializedTransResp"></param>
		/// <param name="typePay"></param>
		/// <param name="industryType"></param>
		/// <param name="txnId"></param>
		/// <returns></returns>
		public static EmvCapkResponse CapkTransactionResponse(TransactionRequest transactionRequest, string xmlSerializedTransResp, string industryType, int txnId)
		{
			EmvCapkResponse CapkResponse = new EmvCapkResponse();
			XmlDocument xmlDoc = new XmlDocument();
			xmlSerializedTransResp = xmlSerializedTransResp.HandleCdata();//To handle invalid request error data
			xmlDoc.LoadXml(xmlSerializedTransResp);
			string typeResponse = GetTagName(xmlDoc);
			string trxType = transactionRequest.TransactionType.ToString();

			switch (trxType)
			{
				case FileDownload:
					switch (typeResponse)
					{
						case "AdminResponse":
							string adminResponsexmlString = GetXmlAsString(xmlDoc);
							var adminResponse = GetResponse(adminResponsexmlString);
							var adminResponseResp = XmlToObject(adminResponse, typeof(AdminResponse));
							CapkResponse = emvCapkResponse(adminResponseResp, transactionRequest, txnId);
							break;

						case "RejectResponse":
							string rejectxmlString = GetXmlAsString(xmlDoc);
							var rejectxmlResponse = GetResponse(rejectxmlString);
							var rejectResp = XmlToObject(rejectxmlResponse, typeof(RejectResponse));
							CapkResponse = emvCapkResponse(rejectResp, transactionRequest, txnId);
							break;
					}
					break;
			}

			return CapkResponse;
		}

		/// <summary>
		/// Purpose             :   Sending emv Capk response for android device
		/// Function Name       :   emvCapkResponse
		/// Created By          :   Madhuri Tanwar
		/// Created On          :   10/26/2015
		/// Modificatios Made   :   ****************************
		/// Modidified On       :    "MM/DD/YYYY"  
		/// </summary>
		/// <param name="objectType"></param>
		/// <param name="transactionRequest"></param>
		/// <param name="tId"></param>
		/// <returns></returns>
		public static EmvCapkResponse emvCapkResponse(dynamic objectType, TransactionRequest transactionRequest, int tId)
		{
			try
			{
				EmvCapkResponse CapkResponse = new EmvCapkResponse();
				CapkResponse.Header = new LoginHeader();
				CapkResponse.EmvCapkDetails = new EmvCapkDetails();
				CapkResponse.Header.DeviceId = transactionRequest.SerialNumber;
				CapkResponse.Header.RequestId = transactionRequest.RequestedId;

				string respMgs = string.Empty;
				string status = string.Empty;

				if (objectType.RespGrp.RespCode == null)
					status = PaymentAPIResources.Failed;
				else
				{
					respMgs = objectType.RespGrp.RespCode;

					if (respMgs.Equals("703") && (transactionRequest.IndustryType == CAPKUpdate))
					{
						status = PaymentAPIResources.CAPK_Update_Required;
					}
					else
					{
						if (respMgs.Equals("701") && (transactionRequest.IndustryType == CAPKUpdate))
							status = PaymentAPIResources.CAPK_update_not_required;
						else if (respMgs.Equals("703") && (transactionRequest.IndustryType == CAPKDownload))
							status = PaymentAPIResources.More_Capk_file_Available;
						else if (respMgs.Equals("701") && (transactionRequest.IndustryType == CAPKDownload))
							status = PaymentAPIResources.Capkfile_for_Device;
						else
							status = PaymentAPIResources.Failed;
					}
				}

				CapkResponse.Header.Status = status;
				CapkResponse.EmvCapkDetails.DriverNumber = transactionRequest.DriverNumber;
				CapkResponse.EmvCapkDetails.CapkData = transactionRequest.Capkdata;

				if (objectType.FileDLGrp != null)
				{
					if (objectType.FileDLGrp.FunCode == "L" && (transactionRequest.IndustryType == CAPKDownload))
					{
						string creatDate = objectType.FileDLGrp.CurrFileCreationDt;
						CapkResponse.EmvCapkDetails.CreationDate = creatDate;
					}
				}

				return CapkResponse;
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Purpose             :   Create TokenResponse by XML response
		/// Function Name       :   TokenResponse
		/// Created By          :   Sunil Singh
		/// Created On          :   06-Aug-15
		/// Modificatios Made   :   ****************************
		/// </summary>
		/// <param name="transactionRequest"></param>
		/// <param name="xmlSerializedTransResp"></param>
		/// <returns></returns>
		public static CreateRCCIResp CreateRCCIResp(TransactionRequest transactionRequest, string xmlSerializedTransResp)
		{
			CreateRCCIResp tokenResponse = new CreateRCCIResp();
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(xmlSerializedTransResp);
			string typeResponse = GetTagName(xmlDoc);
			switch (typeResponse)
			{
				case "TransArmorResponse":
					string transArmorxmlString = GetXmlAsString(xmlDoc);
					var transArmorresponse = GetResponse(transArmorxmlString);
					var transArmorResp = XmlToObject(transArmorresponse, typeof(TransArmorResponse));
					tokenResponse = ResponseToken(transArmorResp, transactionRequest);
					break;

				case "RejectResponse":
					string rejectxmlString = GetXmlAsString(xmlDoc);
					var rejectxmlResponse = GetResponse(rejectxmlString);
					var rejectResp = XmlToObject(rejectxmlResponse, typeof(RejectResponse));
					tokenResponse = ResponseToken(rejectResp, transactionRequest);
					break;

				case "CreditResponse":
					string creditRespxmlString = GetXmlAsString(xmlDoc);
					var creditxmlResponse = GetResponse(creditRespxmlString);
					var creditResp = XmlToObject(creditxmlResponse, typeof(CreditResponse));
					tokenResponse = ResponseToken(creditResp, transactionRequest);
					break;
			}

			return tokenResponse;
		}

		/// <summary>
		/// Purpose             :   Create token response for BookerAppToken.
		/// Function Name       :   ResponseToken
		/// Created By          :   Sunil Singh
		/// Created On          :   06-Aug-15
		/// Modificatios Made   :   ****************************
		/// Modidified On       :    
		/// </summary>
		/// <param name="objectType"></param>
		/// <param name="transactionRequest"></param>
		/// <returns></returns>
		public static CreateRCCIResp ResponseToken(dynamic objectType, TransactionRequest transactionRequest)
		{
			CreateRCCIResp resp = new CreateRCCIResp();
			ResultStatus result = new ResultStatus();
			CreditCardStatus cardStatus = new CreditCardStatus();
			TruncatedCreditCardDetails truncatedDetails = new TruncatedCreditCardDetails();
			ValueNode rcci = new ValueNode();

            if (objectType.RespGrp.RespCode != null)
			{
				var responseCode = objectType.RespGrp.RespCode;
				result.Code = (responseCode == "000") ? ResultCode.Success : ResultCode.InvalidCreditCard;
				cardStatus.IsValid = (responseCode == "000") ? true : false;
				cardStatus.StatusDescription = (responseCode == "000") ? string.Empty : objectType.RespGrp.AddtlRespData;
				result.ErrorDescription = (responseCode == "000") ? string.Empty : objectType.RespGrp.AddtlRespData;
			}

			rcci.Value = (objectType.TAGrp != null) ? StringExtension.Encrypt(objectType.TAGrp.Tkn) : string.Empty;
			truncatedDetails.TruncatedPAN = transactionRequest.CardNumber.Substring(transactionRequest.CardNumber.Length - 4, 4);
			truncatedDetails.ExpiryMonth = transactionRequest.ExpiryDate.Substring(4, 2);
			truncatedDetails.ExpiryYear = transactionRequest.ExpiryDate.Substring(2, 2);
			truncatedDetails.CardType = CreditCardType.GetCardType(transactionRequest.CardNumber).ToString();

			resp.Result = result;
			resp.CreditCardStatus = cardStatus;
			resp.RCCI = rcci;
			resp.TruncatedCreditCardDetails = truncatedDetails;
			return resp;
		}

		/// <summary>
		/// Purpose             :   Create a response for failed conditions.
		/// Function Name       :   BookerAppFailedResponse
		/// Created By          :   Sunil Singh
		/// Created On          :   06-Aug-15
		/// Modificatios Made   :   ****************************
		/// Modidified On       :    
		/// </summary>
		/// <param name="transactionRequest"></param>
		/// <param name="message"></param>
		/// <returns></returns>
		public static CreateRCCIResp CreateRCCIFailedResp(TransactionRequest transactionRequest, string message)
		{
			CreateRCCIResp resp = new CreateRCCIResp();
			ResultStatus result = new ResultStatus();
			CreditCardStatus cardStatus = new CreditCardStatus();
			ValueNode rcci = new ValueNode();

			result.Code = ResultCode.Error;
			result.ErrorDescription = message;
			cardStatus.IsValid = false;
			rcci.Value = string.Empty;

			TruncatedCreditCardDetails truncatedDetails = new TruncatedCreditCardDetails();
			if (!string.IsNullOrEmpty(transactionRequest.CardNumber))
			{
				truncatedDetails.TruncatedPAN = transactionRequest.CardNumber.Substring(transactionRequest.CardNumber.Length - 4, 4);
				truncatedDetails.CardType = CreditCardType.GetCardType(transactionRequest.CardNumber).ToString();
			}

			truncatedDetails.ExpiryMonth = transactionRequest.ExpiryDate;
			truncatedDetails.ExpiryYear = transactionRequest.ExpiryDate;

			resp.Result = result;
			resp.CreditCardStatus = cardStatus;
			resp.RCCI = rcci;
			resp.TruncatedCreditCardDetails = truncatedDetails;

			return resp;
		}

		/// <summary>
		/// BookerApp BasicAuthorization response
		/// </summary>
		/// <param name="transactionRequest"></param>
		/// <param name="xmlSerializedTransResp"></param>
		/// <param name="txnId"></param>
		/// <returns></returns>
        public static BasicAuthoriseResponse BasicAuthResp(TransactionRequest transactionRequest, string xmlSerializedTransResp, int txnId, long jobId, string rcciLast4CardNumber)
		{
			BasicAuthoriseResponse resp = new BasicAuthoriseResponse();
			BasicAuthoriseResult tokenResponse = new BasicAuthoriseResult();
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(xmlSerializedTransResp.HandleCdata());
			string typeResponse = GetTagName(xmlDoc);

            switch (typeResponse)
			{
				case "CreditResponse":
					string transArmorxmlString = GetXmlAsString(xmlDoc);
					var transArmorresponse = GetResponse(transArmorxmlString);
					var transArmorResp = XmlToObject(transArmorresponse, typeof(CreditResponse));
                    tokenResponse = AppPaymentResponse(transArmorResp, transactionRequest, txnId, jobId, rcciLast4CardNumber);
					break;

				case "RejectResponse":
					string rejectxmlString = GetXmlAsString(xmlDoc);
					var rejectxmlResponse = GetResponse(rejectxmlString);
					var rejectResp = XmlToObject(rejectxmlResponse, typeof(RejectResponse));
                    tokenResponse = AppPaymentResponse(rejectResp, transactionRequest, txnId, jobId, rcciLast4CardNumber);
					break;
			}

			resp.BasicAuthoriseResult = tokenResponse;
			return resp;
		}

		/// <summary>
		/// BookerApp BasicAuthorization failed response
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="message"></param>
		/// <param name="txnId"></param>
		/// <returns></returns>
		public static BasicAuthoriseResponse BasicAuthFailedResp(string message, int txnId = 0)
		{
			BasicAuthoriseResponse resp = new BasicAuthoriseResponse()
			{
				BasicAuthoriseResult = new BasicAuthoriseResult()
				{
					ErrorMessage = message,
					AuthorisationDate = DateTime.Now,
					TransactionId = txnId.ToString()
				}
			};
			return resp;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="objectType"></param>
		/// <param name="transactionRequest"></param>
		/// <param name="txnId"></param>
        /// <param name="jobId"></param>
        /// <param name="rcciLast4CardNumber"></param>
		/// <returns></returns>
        public static BasicAuthoriseResult AppPaymentResponse(dynamic objectType, TransactionRequest transactionRequest, int txnId, long jobId, string rcciLast4CardNumber)
		{
			BasicAuthoriseResult bookerAppResult = new BasicAuthoriseResult();
			if (objectType.RespGrp != null)
			{
				var responseCode = objectType.RespGrp.RespCode;
				bookerAppResult.AuthorisationCode = objectType.RespGrp.AuthID;
				bookerAppResult.ErrorMessage = (responseCode == "000") ? string.Empty : objectType.RespGrp.ErrorData;
				bookerAppResult.ResponseType = (responseCode == "000") ? 1 : (responseCode == "002") ? 2 : 0;

				if (bookerAppResult.ResponseType == 0 && string.IsNullOrEmpty(bookerAppResult.ErrorMessage))
					bookerAppResult.ErrorMessage = objectType.RespGrp.AddtlRespData;
			}
			else
			{
				bookerAppResult.ErrorMessage = PaymentAPIResources.NoResponse;
				bookerAppResult.ResponseType = 0;
			}

			if (objectType.CommonGrp != null)
			{
				bookerAppResult.AuthorisationDate = DateTime.ParseExact(objectType.CommonGrp.LocalDateTime.ToString(), "yyyyMMddHHmmss", null);
				bookerAppResult.BalanceAmt = (Convert.ToDecimal(transactionRequest.TransactionAmount)) - (Convert.ToDecimal(objectType.CommonGrp.TxnAmt.ToString()) / 100);
			}

			bookerAppResult.CardType = transactionRequest.CardType.ToString();

			bookerAppResult.ExpiryDate = transactionRequest.ExpiryDate;
			bookerAppResult.JobId = jobId;
			bookerAppResult.PaymentAmt = Convert.ToDecimal(transactionRequest.TransactionAmount);
			bookerAppResult.RequestID = transactionRequest.RequestedId;
			bookerAppResult.TransactionId = Convert.ToString(txnId);

			if (!string.IsNullOrEmpty(transactionRequest.CardNumber))
				bookerAppResult.TruncatedPAN = transactionRequest.CardNumber.Substring(transactionRequest.CardNumber.Length - 4, 4);
            else if (!string.IsNullOrEmpty(transactionRequest.RCCIToken))
                bookerAppResult.TruncatedPAN = (rcciLast4CardNumber != null ? rcciLast4CardNumber : null);      //D#6500 Lookup the card for the token, and then take the last 4 digits of the card recorded against the token.                

            return bookerAppResult;
		}

		/// <summary>
		/// Purpose             :   Creating xml for payment
		/// Function Name       :   CreateXml
		/// Created By          :   Madhuri Tanwar
		/// Created On          :   13/04/2015
		/// Modification Made   :   ***************************
		/// </summary>
		/// <param name="yourClassObject"></param>
		/// <returns></returns>
		public static string CreateXml(Object yourClassObject)
		{
			XmlDocument xmlDoc = new XmlDocument();   //Represents an XML document, 
			XmlSerializer xmlSerializer = new XmlSerializer(yourClassObject.GetType());

			using (MemoryStream xmlStream = new MemoryStream())
			{
				xmlSerializer.Serialize(xmlStream, yourClassObject);
				xmlStream.Position = 0;

				xmlDoc.Load(xmlStream);
				return xmlDoc.InnerXml;
			}
		}

		public enum SoapWrapType
		{
			None,
			Soap11,
			Soap12
		}

		public static SoapWrapType GetSoapType(XmlDocument xmlDoc)
		{
			SoapWrapType soapType = SoapWrapType.None;
			if (xmlDoc.DocumentElement.Name.EndsWith("Envelope"))
			{
				if (xmlDoc.DocumentElement.NamespaceURI == "http://schemas.xmlsoap.org/soap/envelope/")
					soapType = SoapWrapType.Soap11;

				if (xmlDoc.DocumentElement.NamespaceURI == "http://www.w3.org/2003/05/soap-envelope")
					soapType = SoapWrapType.Soap12;
			}

			return soapType;
		}

		public static string UnwrapSoap(string xml, WrapSoapArgs args)
		{
			return UnwrapSoap(xml, args.requestNodeName, args.documentElementName);
		}
        
		public static string UnwrapSoap(string xml, string requestNodeName, string documentElementName)
		{
			int startIndex = xml.IndexOf(string.Format("<{0}>", requestNodeName));
			int endIndex = xml.IndexOf(string.Format("</{0}>", requestNodeName));

			return  string.Format("<{0}>{1}</{0}>", documentElementName, xml.Substring(startIndex + requestNodeName.Length + 2, endIndex - startIndex - requestNodeName.Length - 2));
		}

		const string soap11prefix = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd = \"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body>";
		const string soap11postfix = "</soap:Body></soap:Envelope>";

		const string soap12prefix = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\"><soap12:Body>";
		const string soap12postfix = "</soap12:Body></soap12:Envelope>";

		public class WrapSoapArgs
		{
			public SoapWrapType soapType { get; set; }
			public string soapResponseNode { get; set; }
			public string responseNamespace { get; set; }
			public string soapResultNode { get; set; }
			public string serialisedNodeName { get; set; }

			public string requestNodeName {get;set;}
            public string documentElementName { get; set; }
		}

		public static string WrapSoap(XmlDocument document, WrapSoapArgs args)
		{
			return WrapSoap(document, args.soapType, args.soapResponseNode, args.responseNamespace, args.soapResultNode, args.serialisedNodeName);
		}

		public static string WrapSoap(XmlDocument document, SoapWrapType soapType, string soapResponseNode, string responseNamespace, string soapResultNode, string serialisedNodeName)
		{
			string result = document.InnerXml;
			if (document.DocumentElement.Name == serialisedNodeName)
			{
				switch (soapType)
				{
					case SoapWrapType.Soap11:
						result = string.Format("{0}<{1} xmlns=\"{5}\"><{2}>{3}</{2}></{1}>{4}", soap11prefix, soapResponseNode, soapResultNode,
							document.DocumentElement.InnerXml,
							soap11postfix, responseNamespace);
						break;
					case SoapWrapType.Soap12:
						result = string.Format("{0}<{1} xmlns=\"{5}\"><{2}>{3}</{2}></{1}>{4}", soap12prefix, soapResponseNode, soapResultNode,
							document.DocumentElement.InnerXml,
							soap12postfix, responseNamespace);
						break;
					default:
						break;
				}
			}

			return result;
		}

		/// <summary>
		/// Purpose             :   get xml header
		/// Function Name       :   GetDataWireXmlHeader
		/// Created By          :   Madhuri tanwar
		/// Created On          :   31-march-2016
		/// Modificatios Made   :   ****************************
		/// Modidified On       :   "MM/DD/YYYY"
		public static string GetDataWireXmlHeader
        {
            get
            {
                return "<?xml version='1.0' encoding='UTF-8'?>" +
                       "<!DOCTYPE Request PUBLIC '-//Datawire Communication Networks INC//DTD VXN XML Version 3.0//EN' 'http://www.datawire.net/xmldtd/srs.dtd'>";
            }
        }

        /// <summary>
        /// Purpose             :   Generate Client Ref Number in the format STAN|TPPID, right justified and left padded with "0"
        /// Function Name       :   GetClientRef
        /// Created By          :   Umesh
        /// Created On          :   03/23/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY"  
        /// </summary>
        /// <returns></returns>
        public static string GetClientRef()     //(string projectId)
        {
            int transactionId = new Random(Guid.NewGuid().GetHashCode()).Next(10000, 99999);
            string clientRef = transactionId + "V" + "003000";
            clientRef = "00" + clientRef;

            return clientRef;
        }

        /// <summary>
        /// Purpose             :   generate datawire xml
        /// Function Name       :   GenerateDataWireXml
        /// Created By          :   Madhuri tanwar
        /// Created On          :   31-march-2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY"
        public static string GenerateDataWireXml(int condition, DatawireEntities objDataWireEntities)
        {
            ILogger logger = new Logger();
            StringBuilder logMessage = new StringBuilder();

            logMessage.AppendLine("Controller Name:-WebAPI.Common.CommonFunctions; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                var xmlRequest = GetDataWireXmlHeader;
                var doc = new XmlDocument();
                var root = doc.CreateElement("Request");
                root.SetAttribute("Version", "3");
                doc.AppendChild(root);
                var reqClientid = doc.CreateElement("ReqClientID");
                var did = doc.CreateElement("DID");
                var app = doc.CreateElement("App");
                app.InnerText = objDataWireEntities.AppID;
                
                var auth = doc.CreateElement("Auth");
                auth.InnerText = objDataWireEntities.AuthID;
                
                var clientRef = doc.CreateElement("ClientRef");
                clientRef.InnerText = GetClientRef();   // ("VMT001");
                reqClientid.AppendChild(did);
                reqClientid.AppendChild(app);
                reqClientid.AppendChild(auth);
                reqClientid.AppendChild(clientRef);
                root.AppendChild(reqClientid);
                
                var serviceID = doc.CreateElement("ServiceID");
                serviceID.InnerText = objDataWireEntities.ServiceId;

                switch (condition)
                {
                    case 1:
                        var registration = doc.CreateElement("Registration");
                        registration.AppendChild(serviceID);
                        root.AppendChild(registration);
                        break;
                    case 2:
                        var activation = doc.CreateElement("Activation");
                        did.InnerText = objDataWireEntities.DID;
                        activation.AppendChild(serviceID);
                        root.AppendChild(activation);
                        break;
                    case 3:
                        var ping = doc.CreateElement("Ping");
                        did.InnerText = objDataWireEntities.DID;//"00012789581658621404"; //get dynamic  through portal
                        ping.AppendChild(serviceID);
                        root.AppendChild(ping);
                        break;
                }

                var xmlOutput = doc.OuterXml;
                xmlRequest = xmlRequest + xmlOutput;

                return xmlRequest;
            }
            catch (Exception ex)
            {
                logger.LogInfoFatal(logMessage.ToString(), ex);
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   get response code
        /// Function Name       :   GetReturnCode
        /// Created By          :   Madhuri tanwar
        /// Created On          :   31-march-2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY"
        public static string GetReturnCode(string xmlString)
        {
            ILogger logger = new Logger();
            StringBuilder logMessage = new StringBuilder();

            logMessage.AppendLine("Controller Name:-WebAPI.Common.CommonFunctions; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            string returnCode = string.Empty;

            try
            {
                XElement xEle = XElement.Parse(xmlString);
                var returnCodes = (from p in xEle.Descendants(MessageTypeDW.ReturnCode.ToString())
                                   select p.Value);

                returnCode = returnCodes.ElementAtOrDefault(0);
            }
            catch (Exception ex)
            {
                logger.LogInfoFatal(logMessage.ToString(), ex);
                throw;
            }

            return returnCode;
        }

        /// <summary>
        /// Purpose             :   get status code
        /// Function Name       :   GetStatus
        /// Created By          :   Madhuri tanwar
        /// Created On          :   31-march-2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY"
        public static string GetStatus(string xmlString)
        {
            ILogger logger = new Logger();
            StringBuilder logMessage = new StringBuilder();

            logMessage.AppendLine("Controller Name:-WebAPI.Common.CommonFunctions; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            var statusCode = string.Empty;

            try
            {
                var xEle = XElement.Parse(xmlString);

                var status = (from p in xEle.Descendants(MessageTypeDW.Status.ToString())
                              select new
                              {
                                  StatusCode = Convert.ToString(p.FirstAttribute.Value, CultureInfo.InvariantCulture),
                                  Statusdescription = Convert.ToString(p.Value, CultureInfo.InvariantCulture)
                              }).ToList();

                statusCode = status[0].StatusCode;
            }
            catch (Exception ex)
            {
                logger.LogInfoFatal(logMessage.ToString(), ex);
                throw;
            }

            return statusCode;
        }

        /// <summary>
        /// Purpose             :  Get Discovery Urls
        /// Function Name       :   GetUrls
        /// Created By          :   Madhuri tanwar
        /// Created On          :   31-march-2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY"
        public static IEnumerable<string> GetUrls(string xmlString)
        {
            ILogger logger = new Logger();
            StringBuilder logMessage = new StringBuilder();

            logMessage.AppendLine("Controller Name:-WebAPI.Common.CommonFunctions; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            string[] urLs = null;

            try
            {
                var xEle = XElement.Parse(xmlString);

                urLs = (from p in xEle.Descendants(MessageTypeDW.URL.ToString())
                        select p.Value).ToArray();
            }
            catch (Exception ex)
            {
                logger.LogInfoFatal(logMessage.ToString(), ex);
                throw;
            }

            return urLs;
        }

        /// <summary>
        /// Purpose             :  Get did
        /// Function Name       :   GetDid
        /// Created By          :   Madhuri tanwar
        /// Created On          :   31-march-2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY"
        public static string GetDid(string xmlString)
        {
            var datawireId = string.Empty;
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlString);
                string xmlStringCN = CommonFunctions.GetXmlAsString(xmlDoc);
                var response = CommonFunctions.GetCNResponse(xmlStringCN);
                xmlDoc.LoadXml(response);
                var banknetData = xmlDoc.SelectSingleNode("Response/RespClientID/DID");
                if (banknetData != null)
                { 
                    datawireId = banknetData.InnerText; 
                }
            }
            catch
            {
                throw;
            }

            return datawireId;
        }

        /// <summary>
        /// Purpose             :   Centralized response header for all type of requests or failuers.
        /// Function Name       :   FailedResponse
        /// Created By          :   Sunil Singh
        /// Created On          :   19-Aug-2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY"
        /// </summary>
        /// <param name="deviceID"></param>
        /// <param name="requestID"></param>
        /// <param name="responseType"></param>
        /// <returns></returns>
        public static LoginHeader FailedResponse(string responseMessage, string deviceID = null, string requestID = null, string status = null)
        {
            try
            {
                LoginHeader mHeader = new LoginHeader();
                if (!string.IsNullOrEmpty(responseMessage))
                {
                    mHeader.Status = status.IsNull() ? PaymentAPIResources.Failed : status;
                    mHeader.DeviceId = deviceID;
                    mHeader.RequestId = requestID;
                    mHeader.Message = responseMessage;
                }
                return mHeader;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   Centralized failed login for DispatchLogin.
        /// Function Name       :   FailedResponse
        /// Created By          :   Sunil Singh
        /// Created On          :   19-Aug-2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY"
        /// </summary>
        /// <param name="dispatchresponse"></param>
        /// <param name="dispatchLogin"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static DispatchLoginResponse FailedResponse(DispatchLoginResponse dispatchresponse, string message, string deviceId, string carNo = "0", string driverNo = "0")
        {
            try
            {
                dispatchresponse.CarNumber = carNo;
                dispatchresponse.DriverNumber = driverNo;
                dispatchresponse.FleetId = "0";
                dispatchresponse.FleetName = "0";
                dispatchresponse.DeviceID = deviceId;
                dispatchresponse.Message = message;

                return dispatchresponse;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   Centralized failed login for AuthResponse.
        /// Function Name       :   FailedResponse
        /// Created By          :   Sunil Singh
        /// Created On          :   01-Oct-2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY"
        /// </summary>
        /// <param name="authHeader"></param>
        /// <param name="authRequest"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static AuthResponse FailedResponse(AuthRequest authRequest, AuthResponse authresponse, AuthHeader authHeader, string errorMessage)
        {
            authHeader.DeviceId = authRequest.DeviceId;
            authHeader.TransID = authRequest.TransId;
            authHeader.Status = PaymentAPIResources.Failed;
            authHeader.Message = errorMessage;
            authresponse.Header = authHeader;
            return authresponse;
        }

        /// <summary>
        /// XML Serializer 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static String Serialize<T>(T value)
        {
            if (value == null)
            {
                return null;
            }

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            
            ns.Add("", "");
            
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = new UnicodeEncoding(false, false);
            settings.Indent = false;
            settings.OmitXmlDeclaration = true;

            using (StringWriter textWriter = new StringWriter())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    serializer.Serialize(xmlWriter, value, ns);
                }

                return textWriter.ToString();
            }
        }

        /// <summary>
        /// Purpose             :   Rresponse for GetTransactionData.
        /// Function Name       :   DownloadDataResponse
        /// Created By          :   Sunil Singh
        /// Created On          :   11/23/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY"
        /// </summary>
        /// <param name="deviceID"></param>
        /// <param name="requestID"></param>
        /// <param name="responseType"></param>
        /// <returns></returns>
        public static DownloadResponse DownloadDataResponse(IEnumerable<DownloadDriverDataDto> result = null, string message = null, string status = null)
        {
            try
            {
                DownloadResponse resp = new DownloadResponse();
                ResponseHeader lgnHdr = new ResponseHeader();
                lgnHdr.Status = status.IsNull() ? PaymentAPIResources.Failed : status;
                lgnHdr.Message = message;
                resp.Header = lgnHdr;
                resp.Result = result;
                return resp;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Check the response code for failed response codes
        /// </summary>
        /// <param name="respCode">Response code</param>
        /// <returns>true: if exists, false: if not exists</returns>
        public static bool IsFailed(string respCode)
        {
            List<string> failedRespCode = new List<string> { "200", "201", "202", "203", "205", "206", "405", "505" };

            if (failedRespCode.Contains(respCode))
                return true;

            return false;
        }

        /// <summary>
        /// Purpose             :   To check the response type and code of Capk request. int[0]=1 or 0 for success or failed, int[1]=response code
        /// Function Name       :   CapkResponse
        /// Created By          :   Sunil Singh
        /// Created On          :   25-Jan-2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY"
        /// </summary>
        /// <param name="xmlSerializedTransResp"></param>
        /// <returns>int[0]=1/0 for success/failed, int[1]=response code</returns>
        public static int[] CapkResponse(string xmlSerializedTransResp)
        {
            int[] resp = { 0, 0 };
            string respCode = "0";

            XmlDocument reqXML = new XmlDocument();
            XNamespace ns = "com/firstdata/Merchant/gmfV4.02";
            xmlSerializedTransResp = xmlSerializedTransResp.HandleCdata();//To handle invalid request error data
            reqXML.LoadXml(xmlSerializedTransResp);
            var requestXML = XDocument.Parse(reqXML.InnerXml);
            var respGrp = requestXML.Descendants(ns + "RespGrp").FirstOrDefault();
            respCode = (string)respGrp.Element(ns + "RespCode");
            
            string typeResponse = GetTagName(reqXML);
            switch (typeResponse)
            {
                case AdminResponse:
                    resp[0] = 1;
                    resp[1] = Convert.ToInt32(respCode);
                    break;
                case RejectResponse:
                    resp[0] = 0;
                    resp[1] = Convert.ToInt32(respCode);
                    break;
            }

            return resp;
        }

        public static BasicAuthoriseResponse CanadaBasicAuthResp(TransactionRequest transactionRequest, Iso8583Response iso8583Response, int transactionId, long jobId, string rcciLast4CardNumber)
        {
            BasicAuthoriseResponse response = new BasicAuthoriseResponse();
            BasicAuthoriseResult basicAuthorisation = new BasicAuthoriseResult();
            basicAuthorisation = CanadaAppPaymentResponse(iso8583Response, transactionRequest, transactionId, jobId, rcciLast4CardNumber);
            response.BasicAuthoriseResult = basicAuthorisation;

            return response;
        }

        public static BasicAuthoriseResult CanadaAppPaymentResponse(Iso8583Response iso8583Response, TransactionRequest transactionRequest, int transactionId, long jobId, string rcciLast4CardNumber)
        {
            BasicAuthoriseResult bookerAppResult = new BasicAuthoriseResult();
            if (!string.IsNullOrEmpty(iso8583Response.ResponseCode))
            {
                var responseCode = iso8583Response.ResponseCode;
                bookerAppResult.AuthorisationCode = iso8583Response.AuthIdentificationResponse;
                bookerAppResult.ErrorMessage = (responseCode == PaymentAPIResources.ResponseCode00) ? string.Empty : iso8583Response.Message;
                bookerAppResult.ResponseType = (responseCode == PaymentAPIResources.ResponseCode00) ? 1 : 0;

                if (bookerAppResult.ResponseType == 0 && string.IsNullOrEmpty(bookerAppResult.ErrorMessage))
                    bookerAppResult.ErrorMessage = iso8583Response.AdditionalResponseData;
            }
            else
            {
                bookerAppResult.ErrorMessage = PaymentAPIResources.NoResponse;
                bookerAppResult.ResponseType = 0;
            }

            bookerAppResult.AuthorisationDate = DateTime.ParseExact(transactionRequest.LocalDateTime.ToString(), "yyyyMMddHHmmss", null);
            bookerAppResult.BalanceAmt = (Convert.ToDecimal(transactionRequest.TransactionAmount)) - ((Convert.ToDecimal(transactionRequest.TransactionAmount)) / 100);
            bookerAppResult.CardType = transactionRequest.CardType.ToString();
            bookerAppResult.ExpiryDate = transactionRequest.ExpiryDate;
            bookerAppResult.JobId = jobId;
            bookerAppResult.PaymentAmt = Convert.ToDecimal(transactionRequest.TransactionAmount);
            bookerAppResult.RequestID = transactionRequest.RequestedId;
            bookerAppResult.TransactionId = Convert.ToString(transactionId);

            if (!string.IsNullOrEmpty(transactionRequest.CardNumber))
                bookerAppResult.TruncatedPAN = transactionRequest.CardNumber.Substring(transactionRequest.CardNumber.Length - 4, 4);
            else if (!string.IsNullOrEmpty(transactionRequest.RCCIToken))
                bookerAppResult.TruncatedPAN = (rcciLast4CardNumber != null ? rcciLast4CardNumber : null);      //D#6500 Lookup the card for the token, and then take the last 4 digits of the card recorded against the token.                

            return bookerAppResult;
        }

        /// <summary>
        /// Purpose             :   Create token response for canada
        /// Function Name       :   CanadaTokenResponse
        /// Created By          :   Naveen Kumar
        /// Created On          :   21-Nov-2016
        /// Modification Made   :   ****************************
        /// Modified On       :    
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        public static CreateRCCIResp CanadaTokenResponse(Iso8583Response iso8583Response, TransactionRequest transactionRequest)
        {
            CreateRCCIResp resp = new CreateRCCIResp();
            ResultStatus result = new ResultStatus();
            CreditCardStatus cardStatus = new CreditCardStatus();
            TruncatedCreditCardDetails truncatedDetails = new TruncatedCreditCardDetails();
            ValueNode rcci = new ValueNode();

            if (!string.IsNullOrEmpty(iso8583Response.ResponseCode))
            {
                var responseCode = iso8583Response.ResponseCode;
                result.Code = (responseCode == PaymentAPIResources.ResponseCode00) ? ResultCode.Success : ResultCode.InvalidCreditCard;
                cardStatus.IsValid = (responseCode == PaymentAPIResources.ResponseCode00) ? true : false;
                cardStatus.StatusDescription = (responseCode == PaymentAPIResources.ResponseCode00) ? string.Empty : iso8583Response.AdditionalResponseData;
                result.ErrorDescription = (responseCode == PaymentAPIResources.ResponseCode00) ? string.Empty : iso8583Response.AdditionalResponseData;
            }
            else
            {
                result.Code = ResultCode.Error;
                result.ErrorDescription = PaymentAPIResources.NoResponse;
                cardStatus.IsValid = false;
            }

            rcci.Value = !string.IsNullOrEmpty(iso8583Response.Token) ? iso8583Response.Token : string.Empty;
            truncatedDetails.TruncatedPAN = transactionRequest.CardNumber.Substring(transactionRequest.CardNumber.Length - 4, 4);
            truncatedDetails.ExpiryMonth = iso8583Response.BookerAppCardExpiryDate.Substring(4, 2);
            truncatedDetails.ExpiryYear = iso8583Response.BookerAppCardExpiryDate.Substring(2, 2);
            truncatedDetails.CardType = CreditCardType.GetCardType(transactionRequest.CardNumber).ToString();

            resp.Result = result;
            resp.CreditCardStatus = cardStatus;
            resp.RCCI = rcci;
            resp.TruncatedCreditCardDetails = truncatedDetails;

            return resp;
        }

        public static CreateRCCIResp CanadaFailedTokenResponse(TransactionRequest transactionRequest)
        {
            CreateRCCIResp resp = new CreateRCCIResp();
            ResultStatus result = new ResultStatus();
            CreditCardStatus cardStatus = new CreditCardStatus();
            TruncatedCreditCardDetails truncatedDetails = new TruncatedCreditCardDetails();
            ValueNode rcci = new ValueNode();

            result.Code = ResultCode.InvalidCreditCard;
            cardStatus.IsValid = false;
            cardStatus.StatusDescription = PaymentAPIResources.Failed;
            result.ErrorDescription = PaymentAPIResources.CnBookerAppFailedError;
            rcci.Value = string.Empty;
            truncatedDetails.TruncatedPAN = transactionRequest.CardNumber.Substring(transactionRequest.CardNumber.Length - 4, 4);
            truncatedDetails.ExpiryMonth = transactionRequest.ExpiryDate.Substring(4, 2);
            truncatedDetails.ExpiryYear = transactionRequest.ExpiryDate.Substring(2, 2);
            truncatedDetails.CardType = CreditCardType.GetCardType(transactionRequest.CardNumber).ToString();

            resp.Result = result;
            resp.CreditCardStatus = cardStatus;
            resp.RCCI = rcci;
            resp.TruncatedCreditCardDetails = truncatedDetails;

            return resp;
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// To convert ISO response to payment response
        /// </summary>
        /// <param name="isoResponse"></param>
        /// <param name="objLocalVars"></param>
        /// <param name="mid"></param>
        /// <returns>PaymentResponse</returns>
        public static PaymentResponse IsoToPaymentResponse(Iso8583Response isoResponse, LocalVariables objLocalVars, string mid)
        {
            PaymentResponse response = new PaymentResponse();

            LoginHeader loginHeader = new LoginHeader();
            loginHeader.DeviceId = objLocalVars.DeviceId;
            loginHeader.RequestId = objLocalVars.RequestId;
            loginHeader.Status = isoResponse.ResponseCode == PaymentAPIResources.ResponseCode00 ? PaymentAPIResources.Success : PaymentAPIResources.Failed;
            loginHeader.Message = isoResponse.Message;

            TransactionDetails tranDetails = new TransactionDetails();
            tranDetails.CreatedBy = objLocalVars.CreatedBy;
            tranDetails.CardNumber = objLocalVars.Ccno;
            tranDetails.CardType = isoResponse.CardType;
            tranDetails.PaymentType = isoResponse.PaymentType;
            tranDetails.RespCode = isoResponse.ResponseCode == PaymentAPIResources.ResponseCode00 ? PaymentAPIResources.ResponseCode000 : isoResponse.ResponseCode;
            tranDetails.DriverNumber = objLocalVars.DriverNo;
            tranDetails.EntryMode = objLocalVars.EntryMode;
            tranDetails.IndustryType = objLocalVars.IndustryType;
            tranDetails.MID = mid;
            tranDetails.TID = isoResponse.TerminalId;
            
            //20171109 BAD tranDetails.TransactionAmount = objLocalVars.Fare;
            tranDetails.TransactionAmount = objLocalVars.TxnAmount;
            
            tranDetails.TransactionId = isoResponse.TransactionId.ToString();
            tranDetails.TransactionType = isoResponse.TransactionType;
            tranDetails.VehicleNumber = objLocalVars.VehicleNumber;
            tranDetails.PickUpLat = objLocalVars.StartLatitude;
            tranDetails.PickUpLng = objLocalVars.EndLongitude;
            tranDetails.DropOffLng = objLocalVars.EndLongitude;
            tranDetails.DropOffLat = objLocalVars.EndLatitude;
            tranDetails.StreetAddress = objLocalVars.StreetAddress;
            tranDetails.DropOffAdd = objLocalVars.DestinationAddress;
            tranDetails.PickUpAdd = objLocalVars.PickAddress;
            tranDetails.ZipCode = objLocalVars.ZipCode;
            tranDetails.AuthId = isoResponse.AuthIdentificationResponse;
            tranDetails.StanNo = isoResponse.SystemTrace;
            tranDetails.TransRefNo = isoResponse.RetrievalRefNumber;
            tranDetails.ProcessingCode = isoResponse.ProcessingCode;

            //to handle emv data for response
            if (isoResponse.ICCData != null)
            {
                string emvResponse = isoResponse.ICCData;
                tranDetails.EmvResponse = string.IsNullOrEmpty(emvResponse) ? string.Empty : emvResponse.GetEMVResponse().Trim();
            }
            else if (tranDetails.EntryMode == PaymentEntryMode.EmvContact.ToString() || tranDetails.EntryMode == PaymentEntryMode.EmvContactless.ToString())
            {
                if (string.IsNullOrEmpty(isoResponse.ResponseCode)) //for timeout
                    tranDetails.EmvResponse = "T8A:02:3035";
                else if (isoResponse.ResponseCode.Equals(PaymentAPIResources.ResponseCode00))
                    tranDetails.EmvResponse = "T8A:02:3030";
                else
                    tranDetails.EmvResponse = "T8A:02:3035";
            }

            // Interac
            if (tranDetails.CardType == CardTypeType.Interac.ToString())
            {
                //2018-01-18 SH Check for NULL Bit32
                if (string.IsNullOrEmpty(isoResponse.Bit32))
                    isoResponse.Bit32 = string.Empty;

                if (isoResponse.Bit32.Length == 0)
                    isoResponse.Bit32 = isoResponse.Bit32.PadRight(32, ' ');

                tranDetails.MacData = isoResponse.Bit32.Substring(0, 16).Trim();
                tranDetails.CheckDigit = isoResponse.Bit32.Substring(16, 4).Trim();

                //2018-01-18 SH Check for NULL Bit32
                if (string.IsNullOrEmpty(isoResponse.Bit34))
                    isoResponse.Bit34 = string.Empty;

                if (isoResponse.Bit34.Length == 0)
                    isoResponse.Bit34 = isoResponse.Bit34.PadRight(48, ' ');

                tranDetails.TMAC = isoResponse.Bit34.Substring(0, 16).Trim();
                tranDetails.TKPE = isoResponse.Bit34.Substring(16, 16).Trim();
                tranDetails.TKME = isoResponse.Bit34.Substring(32, 16).Trim();
            }

            response.Header = loginHeader;
            response.TransactionDetails = tranDetails;

            return response;
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Get table 63 value from First Data response for Interac
        /// </summary>
        /// <param name="bit63Value"></param>
        /// <returns>string value</returns>
        public static Dictionary<string, string> GetTable63Data(string bit63Value)
        {
            Dictionary<string, string> table63Response = new Dictionary<string, string>();
            string value = bit63Value;
            string[] tags = "3332,3334,3232".Split(',');

            if (!string.IsNullOrEmpty(bit63Value))
            {
                bool isAlive = true;

                while (isAlive)
                {
                    value = value.Substring(4);
                    int lengthToDiscard = 0;
                    int valueLength = 0;

                    if (value.Length > 0)
                    {
                        string tagInResp = value.Substring(4, 4);
                        string tagLength = value.Substring(0, 4);

                        string tagValue = string.Empty;
                        lengthToDiscard = Convert.ToInt32(tagLength) * 2;

                        if (tags.Contains(tagInResp))
                        {
                            valueLength = lengthToDiscard;
                            tagValue = value.Substring(4, lengthToDiscard);

                            string tag = tagValue.Substring(4);

                            table63Response.Add(tagInResp, tag);
                        }
                        else
                        {
                            if (!table63Response.ContainsKey(tagInResp))
                                table63Response.Add(tagInResp, string.Empty);
                        }

                        value = value.Substring(lengthToDiscard, value.Length - lengthToDiscard);
                        if (string.IsNullOrEmpty(value))
                            isAlive = false;
                    }
                }
            }

            if (!table63Response.ContainsKey("3232"))
                table63Response.Add("3232", string.Empty);

            if (!table63Response.ContainsKey("3332"))
                table63Response.Add("3332", string.Empty);

            if (!table63Response.ContainsKey("3334"))
                table63Response.Add("3334", string.Empty);

            return table63Response;
        }

        //zzz Chetu 357
        /// <summary>
        /// Get table SP details for retail
        /// </summary>
        /// <param name="bit63Data"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetTableSPDetails(string bit63Data)
        {
            ILogger logger = new Logger();

            Dictionary<string, string> table63Response = new Dictionary<string, string>();
            string value = bit63Data;
            string[] tags = "ZX,03,07,08,10".Split(',');

            logger.LogInfoMessage(string.Format("CommonFunctions.GetTableSPDetails(); bit63Data:{0}", bit63Data));

            try
            {
                if (!string.IsNullOrEmpty(bit63Data))
                {
                    bool isAlive = true;

                    while (isAlive)
                    {
                        value = value.Substring(4);

                        logger.LogInfoMessage(string.Format("CommonFunctions.GetTableSPDetails(); value:{0}", value));

                        int lenghtToDiscard = 0;
                        int valueLength = 0;

                        if (value.Length > 0)
                        {
                            string tagInResp = value.Substring(4, 4);
                            string tagLength = value.Substring(0, 4);

                            string tagValue = string.Empty;
                            lenghtToDiscard = Convert.ToInt32(tagLength) * 2;

                            if (string.Equals(tagInResp, "5350"))
                            {
                                valueLength = lenghtToDiscard;
                                tagValue = value.Substring(4, lenghtToDiscard);

                                string tag = tagValue.Substring(4);

                                table63Response.Add(tagInResp, tag);

                                break;
                            }

                            value = value.Substring(lenghtToDiscard, value.Length - lenghtToDiscard);

                            if (string.IsNullOrEmpty(value))
                                isAlive = false;
                        }
                    }
                }

                string spData = string.Empty;
                
                // Ensure we actually have an entry for 5350 in the response list, populated from above.
                if ((table63Response.Count > 0) && (table63Response["5350"] != null))
                    spData = CnGenericHandler.ConvertHexToAscii(table63Response["5350"].ToString());

                if (!string.IsNullOrEmpty(spData))
                {
                    bool isAlive = true;

                    while (isAlive)
                    {
                        int lenghtToDiscard = 0;
                        int valueLength = 0;

                        if (spData.Length > 0)
                        {
                            string tagInResp = spData.Substring(0, 2);
                            string tagLength = spData.Substring(2, 3);

                            string tagValue = string.Empty;
                            lenghtToDiscard = Convert.ToInt32(tagLength);

                            if (tags.Contains(tagInResp))
                            {
                                valueLength = lenghtToDiscard;
                                tagValue = spData.Substring(5, lenghtToDiscard);

                                table63Response.Add(tagInResp, tagValue);
                            }
                            else
                            {
                                if (!table63Response.ContainsKey(tagInResp))
                                    table63Response.Add(tagInResp, string.Empty);
                            }

                            spData = spData.Substring(5 + lenghtToDiscard);
                            if (string.IsNullOrEmpty(spData))
                                isAlive = false;
                        }
                    }
                }

                return table63Response;
            }
            catch (Exception ex)
            {
                logger.LogInfoFatal("CommonFunctions.GetTableSPDetails();", ex);

                throw;
            }
        }
    }
}