﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace MTData.WebAPI.Common
{
    /// <summary>
    /// 
    /// </summary>
    public class PassthroughEncryptionLogic : IEncryptionLogic
    {
        #region Public Methods

        public string Encrypt(string plainText)
        {
            return plainText;
        }

        public bool TryDecrypt(string base64CipherText, out string plainText)
        {
            plainText = base64CipherText;
            return true;
        }

        #endregion
    }

}



