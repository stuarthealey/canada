﻿namespace MTD.Core.DataTransferObjects
{
    public class DriverListDto
    {
        public int DriverID { get; set; }
        public string DriverNumber { get; set; }
    }
}
