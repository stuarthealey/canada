﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Data.Data
{
    public class DriverList
    {
        public int DriverID { get; set; }
        public string DriverNumber { get; set; }
    }
}
