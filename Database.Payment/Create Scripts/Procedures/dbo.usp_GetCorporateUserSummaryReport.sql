IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetCorporateUserSummaryReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetCorporateUserSummaryReport]
GO

-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <08-06-2015>
-- Description:	<Prepare transaction summary reports for merchants>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCorporateUserSummaryReport]
( 
 @DateFrom DATETIME,
 @DateTo DATETIME,
 @CorporateUserId INT
)
AS
BEGIN TRY
    SET ANSI_WARNINGS OFF
    SET NOCOUNT ON;
     DECLARE @InnerTable TABLE
      (
        Id INT, 
	    FareValue MONEY,
	    Tip MONEY, 
	    Amount MONEY, 
	    Surcharge MONEY, 
	    Fee MONEY,
	    VehicleNo NVARCHAR(70), 
	    TxnType NVARCHAR(25),
	    ResponseCode NVARCHAR(30),
	    fk_FleetId INT,
		IsRefunded BIT,
		IsVoided BIT,
		IsCompleted BIT,
		TechFee MONEY,
		FleetFee MONEY
      )           
    --Insert data into table variable InnnerTable for reuse                      
     INSERT INTO @InnerTable(Id,FareValue,Tip,Amount,Surcharge,Fee,VehicleNo,TxnType,ResponseCode,fk_FleetId,IsRefunded,IsVoided,IsCompleted,TechFee,FleetFee)                   
     SELECT     mt.Id
               ,mt.FareValue
		       ,mt.Tip
		       ,mt.Amount
		       ,mt.Surcharge
		       ,mt.Fee
		       ,mt.VehicleNo
		       ,mt.TxnType
		       ,mt.ResponseCode
		       ,mt.fk_FleetId 
			   ,mt.IsRefunded
			   ,mt.IsVoided
			   ,mt.IsCompleted
			   ,mt.TechFee
			   ,mt.FleetFee
      FROM 
	    (
		 SELECT Id,FareValue,Tip,Amount,Surcharge,Fee,VehicleNo,TxnType,ResponseCode,fk_FleetId,TxnDate,IsRefunded,IsVoided,IsCompleted,TechFee,FleetFee
		 FROM MTDTransaction
	    ) mt
       JOIN 
	    (
		  SELECT VehicleNumber,fk_FleetID FROM Vehicle WHERE IsActive=1
		) vh 
     ON mt.VehicleNo=vh.VehicleNumber 
     WHERE CAST(mt.TxnDate AS DATE)>=@DateFrom 
	       AND CAST(mt.TxnDate AS DATE)<@DateTo    
           AND mt.fk_FleetId=vh.fk_FleetID     
     --Prepare data for summary report in xml format                            
      SELECT   ROW_NUMBER() OVER (ORDER BY FleetName) AS 'SrNo'
                                ,ISNULL(v.FleetName,'') AS 'FleetName'
							    ,ISNULL(v.VehicleNumber,'') AS 'VehicleNumber'
							    ,(SELECT ISNULL(COUNT(Id),0) FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID  AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND it.IsVoided=0 AND it.ResponseCode IN ('000','002','00')) AS 'NoOfTrans'
							    ,CONCAT('$',(SELECT ISNULL(SUM(FareValue),'0.00') FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002','00'))) AS 'FareAmt'
                                ,CONCAT('$',(SELECT ISNULL(SUM(Tip),'0.00') FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND  it.ResponseCode IN ('000','002','00'))) AS 'Tips'
								,CONCAT('$',(SELECT ISNULL(SUM(surcharge),'0.00')FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002','00'))) AS 'Surcharges'				
								,CONCAT('$',(SELECT ISNULL(SUM(TechFee),'0.00')FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002','00'))) AS 'TechFee'             
							    ,(SELECT ISNULL(COUNT(id),0) FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID   AND it.TxnType='Refund' AND it.ResponseCode IN ('000','00')) AS 'NoOfChargebacks'
							    ,CONCAT('$',(SELECT ISNULL(SUM(Amount),'0.00') FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID   AND it.TxnType='Refund' AND it.ResponseCode IN('000','00'))) AS 'ChargebackValue'
							    ,(SELECT ISNULL(COUNT(id),0) FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID  AND it.TxnType='Void' AND it.ResponseCode IN ('000','00')) AS 'NoOfVoid'
							    ,CONCAT('$',(SELECT ISNULL(SUM(Amount),'0.00') FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID   AND it.TxnType='Void' AND it.ResponseCode IN ('000','00'))) AS 'VoidValue'
								,CONCAT('$',(SELECT ISNULL(SUM(Amount),'0.00') FROM  @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND IT.IsVoided=0 AND it.ResponseCode IN ('000','002','00'))) AS 'TotalAmount'
								,CONCAT('$',(SELECT ISNULL(SUM(Fee),'0.00')+ISNULL(SUM(FleetFee),'0.00') FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002','00')))  AS 'Fees'           
							    ,CONCAT('$',(SELECT ISNULL(SUM(Amount),'0.00') FROM  @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND IT.IsVoided=0 AND it.ResponseCode IN ('000','002','00'))-
								 (SELECT ISNULL(SUM(Fee),'0.00')+ISNULL(SUM(FleetFee),'0.00') FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002','00'))) AS 'AmountOwing'
                        FROM MTDTransaction m 
					     INNER JOIN 
						    (
							 SELECT v.VehicleNumber AS 'VehicleNumber'
							         ,v.fk_FleetID AS 'fk_FleetID'
									 ,f.FleetID AS 'FleetID'
									 ,f.FleetName AS 'FleetName'
									 ,mer.Company AS 'Company' 									
							  FROM 
							    (
								 SELECT VehicleNumber,fk_FleetID FROM Vehicle WHERE IsActive=1
							    ) v  
							  INNER JOIN 
							    (
								 SELECT FleetID,FleetName,fk_MerchantID FROM Fleet WHERE IsActive=1
							    ) f
							    ON v.fk_FleetID=f.FleetID
							  INNER JOIN
							    (
								 SELECT MerchantID,Company FROM Merchant WHERE IsActive=1
							    ) mer
                                ON f.fk_MerchantID=mer.MerchantID
						   ) v
						   ON m.VehicleNo=v.VehicleNumber 
						   WHERE m.ResponseCode IN ('000','002','00') 
						   AND CAST(m.TxnDate AS DATE)>=@DateFrom 
						   AND CAST(m.TxnDate AS DATE)<@DateTo 
						   AND m.fk_FleetId=v.fk_FleetID
						   AND m.Id NOT IN (SELECT Id FROM dbo.MTDTransaction WHERE m.TxnType='Authorization' AND m.IsCompleted=0)
						   AND m.MerchantId  in (select distinct fk_MerchantId from UserFleet where fk_UserID=@CorporateUserId and fk_UserTypeId=5 and IsActive =1)
						   AND m.fk_FleetId in (select distinct fk_FleetID from UserFleet where fk_UserID=@CorporateUserId and fk_UserTypeId=5 and IsActive =1)                                   		 
			            GROUP BY v.FleetName,v.FleetID,v.VehicleNumber,v.Company,v.fk_FleetID                        					   					        											    
					    ORDER BY FleetName
    
END TRY
--use catch to log error with details in ProcErrorHandler table
BEGIN CATCH
     INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
     SELECT ERROR_NUMBER()
	       ,ERROR_SEVERITY()
		   ,ERROR_STATE()
		   ,ERROR_PROCEDURE()
		   ,ERROR_LINE()
		   ,ERROR_MESSAGE()
		   ,SUSER_SNAME()
		   ,HOST_NAME()
		   ,GETDATE()
END CATCH


GO