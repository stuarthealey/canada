﻿using System.Collections.Generic;
using MTD.Core.DataTransferObjects;

namespace MTD.Core.Service.Interface
{
    public interface IVehicleService
    {
        int Add(VehicleDto vehicleDto);

        int Update(VehicleDto vehicleDto, int vehicleId, int terminalId);

        IEnumerable<VehicleDto> GetVehicleDtoList(int merchantId);

        VehicleDto GetVehicleDto(int vehicleId);

        VehicleDto GetVehicleByTerminal(int terminalId);

        void DeleteVehicle(int vehicleId);

        IEnumerable<VehicleDto> GetVehileListByFilter(FilterSearchParametersDto fsParametes);

        IEnumerable<VehicleDto> GetVehileCount(FilterSearchParametersDto fsParametes);

        List<VehicleDto> AddVehicleList(List<VehicleDto> objVehicleDto, int merchantId, int userId, int userType);

        IEnumerable<VehicleDto> UserVehicleList(FilterSearchParametersDto fsParametes);

        int UserVehicleCount(FilterSearchParametersDto fsParametes);

        IEnumerable<CarOwnersDto> CarOwnerList(int merchantId);

        IEnumerable<CarOwnersDto> PayCarOwnerList(int FleetId);

        VehicleDto VehicleListInFleet(string token, string carNumber, int fleetID);

        bool IsDuplicateCarNumber(int fleetId, string CarNumber);

        bool FindFleet(int fleetId);

        VehicleDto AddDespetchVehicle(VehicleDto vehicleDto, string token);
    }
}
