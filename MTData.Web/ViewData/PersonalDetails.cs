﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MTData.Web.ViewData
{
    public class PersonalDetails
    {
        public int isPersonalUpdate { get; set; }
        public int UserID { get; set; }
        public int Mid { get; set; }

        [DataType(DataType.Text)]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "Please enter valid company name")]
        [RegularExpression(@"^[A-Za-z 0-9]+$", ErrorMessage = "Company name  is not valid")]
        [Display(Name = "Company")]
        public string Company { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Company Tax Number")]
        [StringLength(18, MinimumLength = 1, ErrorMessage = "Please enter valid company tax number")]
        [RegularExpression(@"^[A-Za-z 0-9]+$", ErrorMessage = "Company name  is not valid")]
        public string CompanyTaxNumber { get; set; }

        [DataType(DataType.Text)]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "Please enter valid first name")]
        [Display(Name = "First Name")]
        [RegularExpression(@"^[A-Za-z 0-9]+$", ErrorMessage = "First name  is not valid")]
        public string FName { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Last Name")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "Please enter valid last name")]
        [RegularExpression(@"^[A-Za-z 0-9]+$", ErrorMessage = "Last name  is not valid")]
        public string LName { get; set; }

        [Display(Name = "Email")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "Please enter valid email")]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Email is not valid")]
        public string Email { get; set; }

        [Display(Name = "Phone")]
        [DataType(DataType.PhoneNumber)]
        [MinLength(6, ErrorMessage = "Phone  must be minimum length of 6")]
        [StringLength(18, MinimumLength = 1, ErrorMessage = "Please enter valid phone number")]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Phone  is not valid")]
        public string Phone { get; set; }

        public IEnumerable<FDMerchantListViewData> FdMerchantList { get; set; }
        public string isExist { get; set; }
        public int Fk_FDId { get; set; }

        //PAY-13
        [Display(Name = "Use Trans Armor")]
        public bool UseTransArmor { get; set; }
    }
}