-- Create indexes on DriverTransactionHistory for performance.
--============================================================

IF NOT EXISTS (SELECT * FROM sysindexes si INNER JOIN sysobjects so ON so.id = si.id WHERE si.name = 'IX_DriverTransactionHistoryDriverFleetLogon' AND OBJECTPROPERTY(so.id, N'IsUserTable') = 1 AND so.name = 'DriverTransactionHistory')
BEGIN
	PRINT 'Creating Index IX_DriverTransactionHistoryDriverFleetLogon on DriverTransactionHistory'

	CREATE NONCLUSTERED INDEX IX_DriverTransactionHistoryDriverFleetLogon ON dbo.DriverTransactionHistory (DriverNo, FleetId, LogOnDateTime)
		WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
ELSE
BEGIN
	PRINT 'Index IX_DriverTransactionHistoryDriverFleetLogon on DriverTransactionHistory already exists'
END
