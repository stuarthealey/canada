﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MTData.WebAPI.CanadaCert
{
    /// <summary>
    /// Class for hadling merchant response when request for Registration, Deactivation, FraudSetting
    /// </summary>
    [DataContract]
    public class MerchantResponse
    {
        [DataMember]
        public string StatusCode { get; set; }

        [DataMember]
        public long Token { get; set; }

        [DataMember]
        public string TransactionMessage { get; set; }

        [DataMember]
        public List<string> ValidationErrors { get; set; }

        [DataMember]
        public bool SucessIndicator { get; set; }

    }
}