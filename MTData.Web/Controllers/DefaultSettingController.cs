﻿using System;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Web.Security;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Web.Resource;
using MTData.Web.ViewData;
using MTData.Utility;

namespace MTData.Web.Controllers
{
    public class DefaultSettingController : Controller
    {
        private static int _isNew;
        readonly ILogger _logger = new Logger();
        private readonly IDefaultService _defaultService;
        private StringBuilder _logMessage;
        private StringBuilder _logTracking;

        public DefaultSettingController([Named("DefaultService")] IDefaultService defaultService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _defaultService = defaultService;
            _logMessage = logMessage;
            _logTracking = logTracking;
        }

        /// <summary>
        /// Purpose             :   To manage the default tax
        /// Function Name       :   TaxManage
        /// Created By          :   Umesh Kumar
        /// Created On          :   20/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult TaxManage()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                _logMessage.Append("calling GetDefaultDto method of _defaultService");
                var defaultDto = _defaultService.GetDefaultDto(1);
                _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                var defaultTax = defaultDto.ToDefaultViewData();
                _logMessage.Append("ToDefaultViewData method executed successfully");
                if (defaultTax.StateTaxRate == null && defaultTax.FederalStateRate == null)
                {
                    _isNew = 1;
                    return RedirectToAction("TaxUpdate");
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return RedirectToAction("TaxIndex");
        }

        /// <summary>
        /// Purpose             :   To display the default tax
        /// Function Name       :   TaxIndex
        /// Created By          :   Umesh Kumar
        /// Created On          :   21/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult TaxIndex()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                _logMessage.Append("calling GetDefaultDto method of _defaultService");
                var defaultDto = _defaultService.GetDefaultDto(1);
                _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                var defaultTax = defaultDto.ToDefaultViewData();
                _logMessage.Append("ToDefaultViewData method executed successfully");
                _isNew = 0;
                return View(defaultTax);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// Purpose             :   To update the default tax
        /// Function Name       :   TaxUpdate
        /// Created By          :   Umesh Kumar
        /// Created On          :   22/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult TaxUpdate()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            try
            {
                if (_isNew == 1)
                {
                    _logMessage.Append("New");
                    _logMessage.Append("calling GetDefaultDto method of _defaultService");
                    var defaultDto = _defaultService.GetDefaultDto(1);
                    _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                    _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                    var defaultTax = defaultDto.ToDefaultViewData();
                    _logMessage.Append("ToDefaultViewData method executed successfully");
                    defaultTax.IsNew = 1;
                    return View(defaultTax);
                }
                else
                {
                    _logMessage.Append("Not New");
                    _logMessage.Append("calling GetDefaultDto method of _defaultService");
                    var defaultDto = _defaultService.GetDefaultDto(1);
                    _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                    _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                    var defaultTax = defaultDto.ToDefaultViewData();
                    _logMessage.Append("ToDefaultViewData method executed successfully");
                    defaultTax.IsNew = 0;
                    return View(defaultTax);
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// Purpose             :   To update the default tax
        /// Function Name       :   TaxUpdate
        /// Created By          :   Umesh Kumar
        /// Created On          :   22/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="defaultViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult TaxUpdate(DefaultSettingViewData defaultViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                if (defaultViewData.StateTaxRate == null)
                {
                    ModelState.AddModelError("StateTaxRate", MtdataResource.Please_Enter_State_Tax_Rate);
                }
                if (defaultViewData.FederalStateRate == null)
                {
                    ModelState.AddModelError("FederalStateRate", MtdataResource.Please_Enter_Federal_Tax_Rate);
                }
                if (!ModelState.IsValid)
                {
                    return View(defaultViewData);
                }
                _logMessage.Append("calling ToDefaultDto method to map DefaultSettingDto to DefaultSettingViewData and retrun DefaultSettingDto");
                var defaultDto = defaultViewData.ToDefaultDto();
                _logMessage.Append("ToDefaultDto method executed successfully");
                defaultDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                _logMessage.Append("Calling Update method of _defaultService class takes defaultDto, defaultDto properties Booking Fee = " + defaultViewData.BookingFee + ", Booking Fee Fixed= " + defaultViewData.BookingFeeFixed + ", Booking Fee Per = " + defaultViewData.BookingFeePer + ", Booking Fee Type = " + defaultViewData.BookingFeeType + ", Booking Max Cap = " + defaultViewData.BookingMaxCap + ", State= " + defaultViewData.CreatedBy + ", Default ID = " + defaultViewData.DefaultID + ", Default Name = " + defaultViewData.DefaultName + ", Disclaimer = " + defaultViewData.Disclaimer + "Federal State Rate= " + defaultViewData.FederalStateRate);
                _defaultService.Update(defaultDto, 1);
                _logMessage.Append("Update method of _defaultService executed successfully");
                _logTracking.Append("TAX " + defaultViewData.DefaultID + " was updated by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                return RedirectToAction("TaxIndex");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// Purpose             :   To manage default surcharge
        /// Function Name       :   SurchargeManage
        /// Created By          :   Umesh Kumar
        /// Created On          :   23/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SurchargeManage()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                _logMessage.Append("calling GetDefaultDto method of _defaultService");
                var defaultDto = _defaultService.GetDefaultDto(2);
                _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                var defaultTax = defaultDto.ToDefaultViewData();
                _logMessage.Append("ToDefaultViewData method executed successfully");
                if (defaultTax.IsActive != true)
                {
                    _logMessage.Append("New");
                    _isNew = 1;
                    _logMessage.Append("Redirecting to  action SurchargeUpdate");
                    return RedirectToAction("SurchargeUpdate");
                }
                _logMessage.Append("Redirecting to  action SurchargeIndex");
                return RedirectToAction("SurchargeIndex");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View("SurchargeIndex");
        }

        /// <summary>
        /// Purpose             :   To display default surcharge
        /// Function Name       :   SurchargeIndex
        /// Created By          :   Umesh Kumar
        /// Created On          :   24/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SurchargeIndex()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                _logMessage.Append("calling GetDefaultDto method of _defaultService");
                var defaultDto = _defaultService.GetDefaultDto(2);
                _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                var defaultSurcharge = defaultDto.ToDefaultViewData();
                _logMessage.Append("ToDefaultViewData method executed successfully");
                _logMessage.Append("apply fix percentage and both to surcharges");
                FindSurcharge(defaultSurcharge);
                FindBookingFee(defaultSurcharge);
                FindTechnologyType(defaultSurcharge);
                _isNew = 0;
                return View(defaultSurcharge);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// Purpose             :   To update default surcharge
        /// Function Name       :   SurchargeUpdate
        /// Created By          :   Umesh Kumar
        /// Created On          :   24/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SurchargeUpdate()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            if (_isNew == 1)
            {
                _logMessage.Append("New");
                _logMessage.Append("calling GetDefaultDto method of _defaultService");
                var defaultDto = _defaultService.GetDefaultDto(2);
                _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                var defaultSurcharge = defaultDto.ToDefaultViewData();
                _logMessage.Append("ToDefaultViewData method executed successfully");
                _logMessage.Append("getting TypeList from TypeList() method");
                defaultSurcharge.TypeList = TypeList();
                _logMessage.Append("TypeList method executed successfully");
                defaultSurcharge.IsNew = 1;
                return View(defaultSurcharge);
            }
            else
            {
                _logMessage.Append("calling GetDefaultDto method of _defaultService");
                var defaultDto = _defaultService.GetDefaultDto(2);
                _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                var defaultSurcharge = defaultDto.ToDefaultViewData();
                _logMessage.Append("ToDefaultViewData method executed successfully");
                _logMessage.Append("getting TypeList from TypeList() method");
                defaultSurcharge.TypeList = TypeList();
                _logMessage.Append("TypeList method executed successfully");
                FindSurchargeId(defaultSurcharge);
                FindFeeId(defaultSurcharge);
                defaultSurcharge.IsNew = 0;
                return View(defaultSurcharge);
            }
        }

        /// <summary>
        /// Purpose             :   To update default surcharge
        /// Function Name       :   SurchargeUpdate
        /// Created By          :   Umesh Kumar
        /// Created On          :   25/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="defaultViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SurchargeUpdate(DefaultSettingViewData defaultViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("getting TypeList from TypeList() method");

                defaultViewData.TypeList = TypeList();
                _logMessage.Append("TypeList method executed successfully");

                if (defaultViewData.SurchargeType == 2)
                {
                    if (defaultViewData.SurchargePer == null)
                    {
                        ModelState.AddModelError("SurchargePer", MtdataResource.Please_enter_surcharge_percentage);
                    }
                    if (defaultViewData.SurchargeMaxCap == null)
                    {
                        ModelState.AddModelError("SurchargeMaxCap", MtdataResource.SurchargeViewData_SurMaxCap_Please_enter_the_surcharge_s_max_cap);
                    }
                }
                if (defaultViewData.SurchargeType == 1)
                {
                    if (defaultViewData.Surcharge == null)
                    {
                        ModelState.AddModelError("Surcharge", MtdataResource.Please_enter_surcharge);
                    }
                }
                if (defaultViewData.SurchargeType == 3)
                {
                    if (defaultViewData.SurchargePer == null)
                    {
                        ModelState.AddModelError("SurchargePer", MtdataResource.Please_enter_surcharge_percentage);
                    }
                    if (defaultViewData.Surcharge == null)
                    {
                        ModelState.AddModelError("Surcharge", MtdataResource.Please_enter_surcharge);
                    }
                    if (defaultViewData.SurchargeMaxCap == null)
                    {
                        ModelState.AddModelError("SurchargeMaxCap", MtdataResource.SurchargeViewData_SurMaxCap_Please_enter_the_surcharge_s_max_cap);
                    }
                }
                if (defaultViewData.BookingFeeType == 1)
                {

                    if (defaultViewData.BookingFee == null)
                    {
                        ModelState.AddModelError("BookingFee", MtdataResource.Please_txnFee_surcharge);
                    }
                }
                if (defaultViewData.BookingFeeType == 2)
                {
                    if (defaultViewData.FeePercentage == null)
                    {
                        ModelState.AddModelError("FeePercentage", MtdataResource.Please_enter_txnFee_percentage);
                    }
                    if (defaultViewData.BookingMaxCap == null)
                    {
                        ModelState.AddModelError("BookingMaxCap", MtdataResource.SurchargeViewData_txnFee_Please_enter_the_surcharge_s_max_cap);
                    }
                }
                if (defaultViewData.BookingFeeType == 3)
                {
                    if (defaultViewData.FeePercentage == null)
                    {
                        ModelState.AddModelError("FeePercentage", MtdataResource.Please_enter_txnFee_percentage);
                    }
                    if (defaultViewData.BookingFee == null)
                    {
                        ModelState.AddModelError("BookingFee", MtdataResource.Please_txnFee_surcharge);
                    }
                    if (defaultViewData.BookingMaxCap == null)
                    {
                        ModelState.AddModelError("BookingMaxCap", MtdataResource.SurchargeViewData_txnFee_Please_enter_the_surcharge_s_max_cap);
                    }
                }

                if (defaultViewData.TechFeeType == 1)
                {
                    if (defaultViewData.TechFeeFixed == null)
                    {
                        ModelState.AddModelError("TechFeeFixed", MtdataResource.Please_enter_technology_fee);
                    }
                }

                if (defaultViewData.TechFeeType == 2)
                {
                    if (defaultViewData.TechFeePer == null)
                    {
                        ModelState.AddModelError("TechFeePer", MtdataResource.Please_enter_technology_Fee_Percentage);
                    }
                    if (defaultViewData.TechFeeMaxCap == null)
                    {
                        ModelState.AddModelError("TechFeeMaxCap", MtdataResource.Please_enter_technology_feemax_cap);
                    }
                }

                if (defaultViewData.TechFeeType == 3)
                {
                    if (defaultViewData.TechFeePer == null)
                    {
                        ModelState.AddModelError("TechFeePer", MtdataResource.Please_enter_technology_Fee_Percentage);
                    }
                    if (defaultViewData.TechFeeFixed == null)
                    {
                        ModelState.AddModelError("TechFeeFixed", MtdataResource.Please_enter_technology_fee);
                    }
                    if (defaultViewData.TechFeeMaxCap == null)
                    {
                        ModelState.AddModelError("TechFeeMaxCap", MtdataResource.Please_enter_technology_feemax_cap);
                    }
                }
                
                if (!ModelState.IsValid)
                {
                    return View(defaultViewData);
                }

                FindSurchargeType(defaultViewData);
                FindFeeType(defaultViewData);
                FindTechnologyType(defaultViewData);

                _logMessage.Append("calling ToDefaultDto method to map DefaultSettingDto to DefaultSettingViewData and retrun DefaultSettingDto");
                
                var defaultDto = defaultViewData.ToDefaultDto();
                
                _logMessage.Append("ToDefaultDto method executed successfully");
                
                defaultDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                _defaultService.Update(defaultDto, 2);
                _defaultService.Update(defaultDto, 6);
                
                _logTracking.Append("Surchagre " + defaultViewData.DefaultID + " was updated by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                
                return RedirectToAction("SurchargeIndex");
            }
            catch (Exception exception)
            {
                _logger.LogException(exception);
            }
            return View(defaultViewData);
        }

        /// <summary>
        /// Purpose             :   To manage default terms and conditions
        /// Function Name       :   TermConditionManage
        /// Created By          :   Umesh Kumar
        /// Created On          :   25/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult TermConditionManage()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("calling GetDefaultDto method of _defaultService");
                var defaultDto = _defaultService.GetDefaultDto(4);
                _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                var defaultTerm = defaultDto.ToDefaultViewData();
                _logMessage.Append("ToDefaultViewData method executed successfully");
                _logMessage.Append("checing  is Disclaimer is null and TermCondition too");
                if (defaultTerm.Disclaimer == null && defaultTerm.TermCondition == null)
                {
                    _logMessage.Append("Disclaimer is null and TermCondition too redirecting to TermConditionUpdate");
                    _isNew = 1;
                    return RedirectToAction("TermConditionUpdate");
                }
                _logMessage.Append("redirecting to TermConditionIndex");
                return RedirectToAction("TermConditionIndex");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View("TermConditionIndex");
        }

        /// <summary>
        /// Purpose             :   To display default terms and conditions
        /// Function Name       :   TermConditionIndex
        /// Created By          :   Umesh Kumar
        /// Created On          :   26/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult TermConditionIndex()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                _logMessage.Append("calling GetDefaultDto method of _defaultService");
                var defaultDto = _defaultService.GetDefaultDto(4);
                _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                var defaultTerm = defaultDto.ToDefaultViewData();
                _logMessage.Append("ToDefaultViewData method executed successfully");
                _isNew = 0;
                _logMessage.Append("IsNew= 0 and return to view");
                return View(defaultTerm);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// Purpose             :   To update default terms and conditions
        /// Function Name       :   TermConditionUpdate
        /// Created By          :   Umesh Kumar
        /// Created On          :   26/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult TermConditionUpdate()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                if (_isNew == 1)
                {
                    _logMessage.Append("calling GetDefaultDto method of _defaultService");
                    var defaultDto = _defaultService.GetDefaultDto(4);
                    _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                    _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                    var defaultTerm = defaultDto.ToDefaultViewData();
                    _logMessage.Append("ToDefaultViewData method executed successfully");
                    defaultTerm.IsNew = 1;
                    _logMessage.Append("IsNew= 1 and return to view");
                    return View(defaultTerm);
                }
                else
                {
                    _logMessage.Append("calling GetDefaultDto method of _defaultService");
                    var defaultDto = _defaultService.GetDefaultDto(4);
                    _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                    _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                    var defaultTerm = defaultDto.ToDefaultViewData();
                    _logMessage.Append("ToDefaultViewData method executed successfully");
                    defaultTerm.IsNew = 0;
                    _logMessage.Append("IsNew= 1 and return to view");
                    return View(defaultTerm);
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// Purpose             :   To update default terms and conditions
        /// Function Name       :   TermConditionUpdate
        /// Created By          :   Umesh Kumar
        /// Created On          :   26/01/2015
        /// Modification Made   :   ****************************/// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="defaultViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult TermConditionUpdate(DefaultSettingViewData defaultViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                if (String.IsNullOrEmpty(defaultViewData.Disclaimer))
                {
                    ModelState.AddModelError("Disclaimer", MtdataResource.Please_enter_disclaimer);
                    return View(defaultViewData);
                }
                if (String.IsNullOrEmpty(defaultViewData.TermCondition))
                {
                    ModelState.AddModelError("TermCondition", MtdataResource.Please_enter_terms_and_conditions);
                    return View(defaultViewData);
                }

                _logMessage.Append("calling ToDefaultDto method to map DefaultSettingDto to DefaultSettingViewData and retrun DefaultSettingDto");
                var defaultDto = defaultViewData.ToDefaultDto();
                _logMessage.Append("ToDefaultDto method executed successfully");
                defaultDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                _logMessage.Append("calling Update method of _defaultService takes parameter 4 and defaultDto parameters are Vehicle Number =" + defaultDto.TermCondition + ", Plate Number= " + defaultDto.Disclaimer);

                _defaultService.Update(defaultDto, 4);
                _logTracking.Append("Term Conditions " + defaultDto.DefaultID + " was updated by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                _logMessage.Append("Update method of _defaultService executed successfully");
                return RedirectToAction("TermConditionIndex");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View(defaultViewData);
        }

        /// <summary>
        /// Purpose             :   To get the type of surcharge or booking fee
        /// Function Name       :   TypeList
        /// Created By          :   Umesh Kumar
        /// Created On          :   27/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public static IEnumerable<SurchargeTypeViewData> TypeList()
        {
            var listSurchargeType = new List<SurchargeTypeViewData>
            {
               new SurchargeTypeViewData { TypeID = 1, TypeName = MtdataResource.Fixed}, new SurchargeTypeViewData { TypeID = 2, TypeName = MtdataResource.Percentage }, new SurchargeTypeViewData { TypeID = 3, TypeName = "Both" }
            };

            return listSurchargeType;
        }

        /// <summary>
        /// Purpose             :   To get the type of surcharge or booking fee
        /// Function Name       :   FindSurchargeType
        /// Created By          :   Umesh Kumar
        /// Created On          :   27/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="defaultViewData"></param>
        /// <returns></returns>
        [NonAction]
        public void FindSurchargeType(DefaultSettingViewData defaultViewData)
        {
            switch (defaultViewData.SurchargeType)
            {
                case 1:
                    defaultViewData.SurchargeFixed = defaultViewData.Surcharge;
                    break;
                case 2:
                    defaultViewData.SurchargePer = defaultViewData.SurchargePer;
                    break;
                case 3:
                    defaultViewData.SurchargeFixed = defaultViewData.Surcharge;
                    defaultViewData.SurchargePer = defaultViewData.SurchargePer;

                    if (defaultViewData.SurchargePer == null)
                    {
                        ModelState.AddModelError("SurchargePer", MtdataResource.Please_enter_surcharge_percentage);
                    }
                    break;
            }
        }

        /// <summary>
        /// Purpose             :   To get the surcharge Id
        /// Function Name       :   FindSurchargeId
        /// Created By          :   Umesh Kumar
        /// Created On          :   27/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="defaultViewData"></param>
        /// <returns></returns>
        [NonAction]
        public static void FindSurchargeId(DefaultSettingViewData defaultViewData)
        {
            switch (defaultViewData.SurchargeType)
            {
                case 1:
                    defaultViewData.Surcharge = defaultViewData.SurchargeFixed;
                    break;
                case 2:
                    defaultViewData.Surcharge = defaultViewData.SurchargePer;
                    break;
                case 3:
                    defaultViewData.Surcharge = defaultViewData.SurchargeFixed;
                    defaultViewData.SurPercentage = defaultViewData.SurchargePer;
                    break;
            }
        }

        /// <summary>
        /// Purpose             :   To find the surcharge
        /// Function Name       :   FindSurcharge
        /// Created By          :   Umesh Kumar
        /// Created On          :   28/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="defaultSetting"></param>
        /// <returns></returns>
        [NonAction]
        public static void FindSurcharge(DefaultSettingViewData defaultSetting)
        {
            switch (defaultSetting.SurchargeType)
            {
                case 1:
                    defaultSetting.SurType = MtdataResource.Fixed;
                    defaultSetting.Surcharge = defaultSetting.SurchargeFixed;
                    break;
                case 2:
                    defaultSetting.SurType = MtdataResource.Percentage;
                    defaultSetting.Surcharge = defaultSetting.SurchargePer;
                    break;
            }

            if (defaultSetting.SurchargeType != 3) return;

            defaultSetting.SurType = MtdataResource.both_percentage_fixed;
            defaultSetting.Surcharge = defaultSetting.SurchargeFixed;
            defaultSetting.SurPercentage = defaultSetting.SurchargePer;
        }

        /// <summary>
        /// Purpose             :   To find the booking fee
        /// Function Name       :   FindBookingFee
        /// Created By          :   Umesh Kumar
        /// Created On          :   29/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="defaultSetting"></param>
        /// <returns></returns>
        [NonAction]
        public static void FindBookingFee(DefaultSettingViewData defaultSetting)
        {
            if (defaultSetting.BookingFeeType == 1)
            {
                defaultSetting.FeeType = MtdataResource.Fixed;
                defaultSetting.BookingFee = defaultSetting.BookingFeeFixed;
            }

            if (defaultSetting.BookingFeeType == 2)
            {
                defaultSetting.FeeType = MtdataResource.Percentage;
                defaultSetting.BookingFee = defaultSetting.BookingFeePer;
            }

            if (defaultSetting.BookingFeeType != 3) return;

            defaultSetting.FeeType = MtdataResource.both_percentage_fixed;
            defaultSetting.BookingFee = defaultSetting.BookingFeeFixed;
            defaultSetting.FeePercentage = defaultSetting.BookingFeePer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="defaultSetting"></param>
        public static void FindTechnologyType(DefaultSettingViewData defaultSetting)
        {
            if (defaultSetting.TechFeeType == 1)
            {
                defaultSetting.TechFee = MtdataResource.Fixed;
                defaultSetting.TechFeeFixed = defaultSetting.TechFeeFixed;
            }

            if (defaultSetting.TechFeeType == 2)
            {
                defaultSetting.TechFee = MtdataResource.Percentage;
                defaultSetting.TechFeePer = defaultSetting.TechFeePer;
            }

            if (defaultSetting.TechFeeType != 3) return;

            defaultSetting.TechFee = MtdataResource.both_percentage_fixed;
            defaultSetting.TechFeeFixed = defaultSetting.TechFeeFixed;
            defaultSetting.TechFeePer = defaultSetting.TechFeePer;
        }

        /// <summary>
        /// Purpose             :   To find the the booking fee type
        /// Function Name       :   FindFeeType
        /// Created By          :   Umesh Kumar
        /// Created On          :   30/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="defaultViewData"></param>
        /// <returns></returns>
        [NonAction]
        public static void FindFeeType(DefaultSettingViewData defaultViewData)
        {
            if (defaultViewData.BookingFeeType == 1)
            {
                defaultViewData.BookingFeeFixed = defaultViewData.BookingFee;
            }
            if (defaultViewData.BookingFeeType == 2)
            {
                defaultViewData.BookingFeePer = defaultViewData.FeePercentage;
            }

            if (defaultViewData.BookingFeeType != 3) return;

            defaultViewData.BookingFeeFixed = defaultViewData.BookingFee;
            defaultViewData.BookingFeePer = defaultViewData.FeePercentage;
        }

        /// <summary>
        /// Purpose             :   To find the the booking fee Id
        /// Function Name       :   FindFeeId
        /// Created By          :   Umesh Kumar
        /// Created On          :   30/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="defaultViewData"></param>
        /// <returns></returns>
        [NonAction]
        public static void FindFeeId(DefaultSettingViewData defaultViewData)
        {
            if (defaultViewData.BookingFeeType == 1)
            {
                defaultViewData.BookingFee = defaultViewData.BookingFeeFixed;
            }

            if (defaultViewData.BookingFeeType == 2)
            {
                defaultViewData.BookingFee = defaultViewData.BookingFeePer;
            }

            if (defaultViewData.BookingFeeType == 3)
            {
                defaultViewData.BookingFee = defaultViewData.BookingFeeFixed;
                defaultViewData.FeePercentage = defaultViewData.BookingFeePer;
            }
        }

        /// <summary>
        /// Purpose             :   To display the default tip
        /// Function Name       :   TipIndex
        /// Created By          :   Asif Shafeeque
        /// Created On          :   05/06/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult TipIndex()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                _logMessage.Append("calling GetDefaultDto method of _defaultService");
                var defaultDto = _defaultService.GetDefaultDto(5);
                _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                var defaultTip = defaultDto.ToDefaultViewData();
                _logMessage.Append("ToDefaultViewData method executed successfully");
                _isNew = 0;
                return View(defaultTip);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// Purpose             :   To update the default tip
        /// Function Name       :   TipUpdate
        /// Created By          :   Asif Shafeeque
        /// Created On          :   06/05/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult TipUpdate()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            try
            {
                if (_isNew == 1)
                {
                    _logMessage.Append("New");
                    _logMessage.Append("calling GetDefaultDto method of _defaultService");
                    var defaultDto = _defaultService.GetDefaultDto(5);
                    _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                    _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                    var defaultTip = defaultDto.ToDefaultViewData();
                    _logMessage.Append("ToDefaultViewData method executed successfully");
                    defaultTip.IsNew = 1;
                    return View(defaultTip);
                }
                else
                {
                    _logMessage.Append("Not New");
                    _logMessage.Append("calling GetDefaultDto method of _defaultService");
                    var defaultDto = _defaultService.GetDefaultDto(5);
                    _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                    _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                    var defaultTip = defaultDto.ToDefaultViewData();
                    _logMessage.Append("ToDefaultViewData method executed successfully");
                    defaultTip.IsNew = 0;
                    return View(defaultTip);
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// Purpose             :   To update the default tip
        /// Function Name       :   TipUpdate
        /// Created By          :  Asif Shafeeque
        /// Created On          :   06/06/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="defaultViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult TipUpdate(DefaultSettingViewData defaultViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                bool isValid = true;
                if (defaultViewData.TipLow == null)
                {
                    ModelState.AddModelError("TipLow", MtdataResource.Please_enter_tip_low);
                    isValid = false;
                }
                if (defaultViewData.TipMedium == null)
                {
                    ModelState.AddModelError("TipMedium", MtdataResource.Please_enter_tip_medium);
                    isValid = false;
                }
                if (defaultViewData.TipHigh == null)
                {
                    ModelState.AddModelError("TipHigh", MtdataResource.Please_enter_tip_high);
                    isValid = false;
                }
                if (!isValid)
                {
                    return View(defaultViewData);
                }
                _logMessage.Append("calling ToDefaultDto method to map DefaultSettingDto to DefaultSettingViewData and retrun DefaultSettingDto");
                var defaultDto = defaultViewData.ToDefaultDto();
                _logMessage.Append("ToDefaultDto method executed successfully");
                defaultDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                _logMessage.Append("Calling Update method of _defaultService class takes defaultDto, defaultDto properties Booking Fee = " + defaultViewData.BookingFee + ", Booking Fee Fixed= " + defaultViewData.BookingFeeFixed + ", Booking Fee Per = " + defaultViewData.BookingFeePer + ", Booking Fee Type = " + defaultViewData.BookingFeeType + ", Booking Max Cap = " + defaultViewData.BookingMaxCap + ", State= " + defaultViewData.CreatedBy + ", Default ID = " + defaultViewData.DefaultID + ", Default Name = " + defaultViewData.DefaultName + ", Disclaimer = " + defaultViewData.Disclaimer + "Federal State Rate= " + defaultViewData.FederalStateRate);
                _defaultService.Update(defaultDto, 5);
                _logMessage.Append("Update method of _defaultService executed successfully");
                _logTracking.Append("Default Tip " + defaultDto.DefaultID + " was updated by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                return RedirectToAction("TipIndex");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ConfigAdminReportManage()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                _logMessage.Append("calling GetDefaultDto method of _defaultService");
                var defaultDto = _defaultService.GetDefaultDto(7);
                _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                var tranCount = defaultDto.ToDefaultViewData();
                _logMessage.Append("ToDefaultViewData method executed successfully");
                if (tranCount.TransCountSlablt1 == null && tranCount.TransCountSlabGt2 == null && tranCount.TransCountSlablt2 == null && tranCount.TransCountSlabGt3 == null && tranCount.TransCountSlabGt3 == null && tranCount.TransCountSlabGt4 == null)
                {
                    _isNew = 1;
                    return RedirectToAction("ConfigAdminReportUpdate");
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return RedirectToAction("ConfigAdminReportIndex");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ConfigAdminReportIndex()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                _logMessage.Append("calling GetDefaultDto method of _defaultService");
                var defaultDto = _defaultService.GetDefaultDto(7);
                _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                var defaultTranCount = defaultDto.ToDefaultViewData();
                _logMessage.Append("ToDefaultViewData method executed successfully");
                _isNew = 0;
                return View(defaultTranCount);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ConfigAdminReportUpdate()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            try
            {
                if (_isNew == 1)
                {
                    _logMessage.Append("New");
                    _logMessage.Append("calling GetDefaultDto method of _defaultService");
                    var defaultDto = _defaultService.GetDefaultDto(7);
                    _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                    _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                    var defaultTrans = defaultDto.ToDefaultViewData();
                    _logMessage.Append("ToDefaultViewData method executed successfully");
                    defaultTrans.IsNew = 1;
                    return View(defaultTrans);
                }
                else
                {
                    _logMessage.Append("Not New");
                    _logMessage.Append("calling GetDefaultDto method of _defaultService");
                    var defaultDto = _defaultService.GetDefaultDto(7);
                    _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                    _logMessage.Append(                        "calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                    var defaultTrans = defaultDto.ToDefaultViewData();
                    _logMessage.Append("ToDefaultViewData method executed successfully");
                    defaultTrans.IsNew = 0;
                    return View(defaultTrans);
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="defaultViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ConfigAdminReportUpdate(DefaultSettingViewData defaultViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                if (defaultViewData.TransCountSlablt1 == null)
                {
                    ModelState.AddModelError("TransCountSlablt1", MtdataResource.Please_Enter_Slab1);
                    return View(defaultViewData);
                }
                else if (defaultViewData.TransCountSlabGt2 == null)
                {
                    ModelState.AddModelError("TransCountSlabGt2", MtdataResource.Please_Enter_Slab2);
                    return View(defaultViewData);
                }
                else if (defaultViewData.TransCountSlablt2 == null)
                {
                    ModelState.AddModelError("TransCountSlablt2", MtdataResource.Please_Enter_Slab2);
                    return View(defaultViewData);
                }
                else if (defaultViewData.TransCountSlabGt3 == null)
                {
                    ModelState.AddModelError("TransCountSlabGt3", MtdataResource.Please_Enter_Slab3);
                    return View(defaultViewData);
                }
                else if (defaultViewData.TransCountSlablt3 == null)
                {
                    ModelState.AddModelError("TransCountSlablt3", MtdataResource.Please_Enter_Slab3);
                    return View(defaultViewData);
                }
                else if (defaultViewData.TransCountSlabGt4 == null)
                {
                    ModelState.AddModelError("TransCountSlabGt4", MtdataResource.Please_Enter_Slab4);
                    return View(defaultViewData);
                }
                if (!ModelState.IsValid)
                {
                    return View(defaultViewData);
                }

                _logMessage.Append("calling ToDefaultDto method to map DefaultSettingDto to DefaultSettingViewData and retrun DefaultSettingDto");
                var defaultDto = defaultViewData.ToDefaultDto();
                _logMessage.Append("ToDefaultDto method executed successfully");
                defaultDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                _logMessage.Append("Calling Update method of _defaultService class takes defaultDto, defaultDto properties Booking Fee = " + defaultViewData.BookingFee + ", Booking Fee Fixed= " + defaultViewData.BookingFeeFixed + ", Booking Fee Per = " + defaultViewData.BookingFeePer + ", Booking Fee Type = " + defaultViewData.BookingFeeType + ", Booking Max Cap = " + defaultViewData.BookingMaxCap + ", State= " + defaultViewData.CreatedBy + ", Default ID = " + defaultViewData.DefaultID + ", Default Name = " + defaultViewData.DefaultName + ", Disclaimer = " + defaultViewData.Disclaimer + "Federal State Rate= " + defaultViewData.FederalStateRate);
                _defaultService.Update(defaultDto, 7);
                _logMessage.Append("Update method of _defaultService executed successfully");
                _logTracking.Append("TAX " + defaultViewData.DefaultID + " was updated by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());

                return RedirectToAction("ConfigAdminReportIndex");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ConfigMerchantReportIndex()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 2) return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                int mID = (int)((SessionDto)Session["User"]).SessionUser.fk_MerchantID;

                _logMessage.Append("calling GetDefaultDto method of _defaultService");
                var defaultDto = _defaultService.GetTransactionCountSlab(mID);
                _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                var defaultTranCount = defaultDto.ToViewData();
                if (defaultDto == null)
                {
                    TransactionCountSlabViewData dSetViewData = new TransactionCountSlabViewData();
                    dSetViewData.TransCountSlablt3 = 0;
                    dSetViewData.TransCountSlablt2 = 0;
                    dSetViewData.TransCountSlablt1 = 0;
                    dSetViewData.TransCountSlabGt4 = 0;
                    dSetViewData.TransCountSlabGt3 = 0;
                    dSetViewData.TransCountSlabGt2 = 0;
                    return View(dSetViewData);
                }
                _logMessage.Append("ToDefaultViewData method executed successfully");
                _isNew = 0;
                return View(defaultTranCount);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ConfigMerchantReportUpdate()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 2) return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                int mID = (int)((SessionDto)Session["User"]).SessionUser.fk_MerchantID;
                _logMessage.Append("calling GetDefaultDto method of _defaultService");
                var defaultDto = _defaultService.GetTransactionCountSlab(mID);
                _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                var defaultTranCount = defaultDto.ToViewData();
                _logMessage.Append("ToDefaultViewData method executed successfully");
                _isNew = 0;
                return View(defaultTranCount);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="defaultViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ConfigMerchantReportUpdate(TransactionCountSlabViewData slabViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 2) return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                if (slabViewData.TransCountSlablt1 == null)
                {
                    ModelState.AddModelError("TransCountSlablt1", MtdataResource.Please_Enter_Slab1);
                    return View(slabViewData);
                }
                else if (slabViewData.TransCountSlabGt2 == null)
                {
                    ModelState.AddModelError("TransCountSlabGt2", MtdataResource.Please_Enter_Slab2);
                    return View(slabViewData);
                }
                else if (slabViewData.TransCountSlablt2 == null)
                {
                    ModelState.AddModelError("TransCountSlablt2", MtdataResource.Please_Enter_Slab2);
                    return View(slabViewData);
                }
                else if (slabViewData.TransCountSlabGt3 == null)
                {
                    ModelState.AddModelError("TransCountSlabGt3", MtdataResource.Please_Enter_Slab3);
                    return View(slabViewData);
                }
                else if (slabViewData.TransCountSlablt3 == null)
                {
                    ModelState.AddModelError("TransCountSlablt3", MtdataResource.Please_Enter_Slab3);
                    return View(slabViewData);
                }
                else if (slabViewData.TransCountSlabGt4 == null)
                {
                    ModelState.AddModelError("TransCountSlabGt4", MtdataResource.Please_Enter_Slab4);
                    return View(slabViewData);
                }
                if (!ModelState.IsValid)
                {
                    return View(slabViewData);
                }

                _logMessage.Append("calling ToDefaultDto method to map DefaultSettingDto to DefaultSettingViewData and retrun DefaultSettingDto");
                var slabDto = slabViewData.ToDto();
                _logMessage.Append("ToDefaultDto method executed successfully");
                slabDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                int mID = (int)((SessionDto)Session["User"]).SessionUser.fk_MerchantID;
                _defaultService.Update(slabDto, mID);
                _logMessage.Append("Update method of _defaultService executed successfully");
                _logger.LogInfoMessage(_logTracking.ToString());
                return RedirectToAction("ConfigMerchantReportIndex");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

    }
}
