﻿using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class LoginHeader
    {
        [DataMember]
        public string Status { get; set; }

        private string deviceID;

        [DataMember]
        public string DeviceId
        {
            get { return this.deviceID ?? "null"; }
            set { this.deviceID = value; }
        }

        private string requestId { get; set; }

        [DataMember]
        public string RequestId
        {
            get { return this.requestId ?? "null"; }
            set { this.requestId = value; }
        }


        private string message { get; set; }

        [DataMember]
        public string Message
        {
            get { return this.message ?? "null"; }
            set { this.message = value; }
        }

        [DataMember(EmitDefaultValue = false)]
        public string CapkUpdateRequired { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TAKeyUpdateRequired { get; set; }
    }
}