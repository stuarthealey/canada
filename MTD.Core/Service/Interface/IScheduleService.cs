﻿using System.Collections.Generic;
using MTD.Core.DataTransferObjects;

namespace MTD.Core.Service.Interface
{
    public interface IScheduleService
    {

        void Add(ScheduleReportDto scheduleDto);

        void Update(ScheduleReportDto scheduleDto);

        ScheduleReportDto GetScheduleDto(int scheduleId);

        bool UpdateRecipient(RecipientDto recipientDto);

        bool UpdateAdminRecipient(AdminRecipientListDto adminRecipientListDto);

        IEnumerable<ScheduleReportDto> ScheduleReportList();

        IEnumerable<ScheduleReportDto> ScheduleReportListByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting);

        IEnumerable<ScheduleReportDto> ScheduleReportListByFilter(FilterSearchParametersDto filterSearchParameterDto, string jtSorting);
        
        IEnumerable<ReportListDto> ReportList();
        
        IEnumerable<ReportListDto> ReportList(int merchantId);
        
        IEnumerable<ReportListDto> ReportListReadOnly();
        
        void Delete(int ScheduleID);
        
        void UpdateCorporateUserRecipient(RecipientDto recipientDto,int corporateUserId,string modifiedBy);
        
        void AddCorporateUser(ScheduleReportDto scheduleDto);
        
        void UpdateCorporate(ScheduleReportDto scheduleDto);
        
        IEnumerable<ReportListDto> CorporateReportList(int corporateId);
        
        void DeleteCorporate(int scheduleId);

        IEnumerable<ReportListDto> ReportList(int to,int from);

        IEnumerable<GetAdminWeeklySummaryCountDto> WeeklySummary(FilterSearchParametersDto filterSearch);

        int WeeklySummaryCount(FilterSearchParametersDto filterSearch);

        IEnumerable<WeeklyDollarVolumeDto> WeeklyDollarVolumeReport(FilterSearchParametersDto filterSearch);

        int WeeklyDollarVolumeReportCount(FilterSearchParametersDto filterSearch);

    }
}

