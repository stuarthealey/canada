
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_AdminExceptionReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_AdminExceptionReport]
GO

/****** Object:  StoredProcedure [dbo].[usp_AdminExceptionReport]    Script Date: 11/2/2015 10:59:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <15-06-2015>
-- Description:	<Prepare exception reports for admin users>
-- =============================================
USE [MTDataDevlopments]
GO
/****** Object:  StoredProcedure [dbo].[usp_AdminExceptionReport]    Script Date: 06/01/2016 4:43:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <15-06-2015>
-- Description:	<Prepare exception reports for admin users>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AdminExceptionReport]
(
  @XmlData NVARCHAR(MAX) OUT,
  @ReportType INT OUT,
  @FDate DATE OUT,
  @TDate DATE OUT
)
AS
BEGIN TRY
  -- SET NOCOUNT ON added to prevent extra result sets from
  SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF
  DECLARE @Frequency INT,
  @FrequencyType INT,
  @DateFrom DATE,
  @DateTo DATE,
  @Merchant NVARCHAR(100),
  @Fleet NVARCHAR(100),
  @ExpFleetTrans INT,
  @ActFleetTrans INT,
  @ExpCar INT,
  @ActCar INT,
  @ExpValue DECIMAL(18,2),
  @ActValue DECIMAL(18,2),
  @Case VARCHAR(100),
  @InnerXml NVARCHAR(MAX)='',
  @ScheduleId INT,
  @NextScheduleDateTime DATETIME
  SELECT @ScheduleId=ScheduleID
        ,@Frequency=Frequency
        ,@FrequencyType=FrequencyType
		,@NextScheduleDateTime=NextScheduleDateTime 
  FROM ScheduleReport 
  WHERE fk_ReportId=3 AND IsCreated=1 AND fk_MerchantId IS NULL
  IF(@Frequency=1)
  BEGIN
    SET @DateTo=GETDATE()
    SET @DateFrom=GETDATE()-@FrequencyType
	SET @NextScheduleDateTime=@NextScheduleDateTime+@FrequencyType
  END
  IF(@Frequency=2)
  BEGIN
   SET @DateFrom = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-@FrequencyType,GETDATE())), 0)-1
   SET @DateTo= DATEADD(WEEK, DATEDIFF(WEEK, 0, GETDATE()), 0)-1
   SET @NextScheduleDateTime=DATEADD(WK,@FrequencyType,@NextScheduleDateTime)
  END
  IF(@Frequency=3)
  BEGIN
   SET @DateFrom =DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH,-@FrequencyType,GETDATE())), 0)
   SET @DateTo=DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) 
   SET @NextScheduleDateTime=DATEADD(MONTH,@FrequencyType,@NextScheduleDateTime)
  END   
  SELECT COALESCE((SELECT Company FROM Merchant M WHERE M.MerchantID = Trans.MerchantId),'') 'Merchant',
  COALESCE((SELECT [FleetName] FROM Fleet F WHERE F.FleetID = Fl.FleetID),'')  'Fleet',
  COALESCE(Fl.FleetTransaction,0) 'ExpFleetTrans',COALESCE(Trans.FTransCount,0) 'ActFleetTrans',
  COALESCE(Fl.CarTransaction,0) 'ExpCar',COALESCE(Trans.VTransCount,0) 'ActCar',
  COALESCE(Fl.TransactionValue,0.00) 'ExpValue',COALESCE(Trans.Amount,0.00)  'ActValue' INTO #Temp
  FROM
  (
	SELECT fk_MerchantID MerchantID, FleetID, FleetName, FleetTransaction, CarTransaction, CAST(TransactionValue  AS DECIMAL(18,2)) TransactionValue
	FROM Fleet F WHERE  F.IsActive = 1 
	AND ( F.FleetTransaction IS NOT NULL OR  F.CarTransaction IS NOT NULL OR  F.TransactionValue IS NOT NULL) 
  ) Fl
  INNER JOIN
  (
    SELECT V.MerchantId,V.FleetID,SUM(V.TransCount) FTransCount,Count(V.TransCount) VTransCount,SUM(V.FareValue) FareValue,SUM(V.Tip) AS 'Tip'
	,SUM(V.Surcharge) AS 'Surcharge',SUM(V.Amount) AS 'Amount',SUM(V.Fee) AS 'Fee',SUM(V.TechFee) AS 'TechFee',SUM(V.AmountOwing) AS 'AmountOwing' FROM 
	 (
		SELECT COALESCE(T.MerchantId,0) AS 'MerchantId'
		,COALESCE((SELECT TOP 1 fk_FleetID FROM Vehicle V WHERE V.fk_FleetId=T.fk_FleetId),'') AS FleetID
		,COALESCE(T.VehicleNo,'') AS 'VehicleNo'
		,COUNT(T.VehicleNo) AS TransCount
		,SUM(T.FareValue) AS 'FareValue'
		,SUM(T.Tip) AS 'Tip'
		,SUM(T.Surcharge) AS 'Surcharge'
		,cast(SUM(T.Amount) as decimal(18,2)) AS 'Amount'
		,COALESCE(SUM(T.Fee),0)+COALESCE(SUM(T.FleetFee),0) AS 'Fee'
		,SUM(T.TechFee) AS 'TechFee'
		,COALESCE(SUM(T.Amount),0) - COALESCE(SUM(T.FleetFee),0)-COALESCE(SUM(T.Fee),0) AS 'AmountOwing'
		FROM MTDTransaction T
		WHERE T.ResponseCode IN ('000','002') 
		      AND T.TxnType IN ('Sale','Completion')
			  AND CAST(T.TxnDate AS DATE)>=@DateFrom 
			  AND CAST(T.TxnDate AS DATE)<@DateTo 
			  AND IsRefunded=0 AND IsVoided=0
		GROUP BY T.VehicleNo,T.MerchantId,T.fk_FleetId
	) V GROUP BY V.MerchantId,V.FleetID
 ) Trans ON (fl.FleetID = Trans.FleetID)
 WHERE Fl.FleetTransaction > Trans.FTransCount OR Fl.CarTransaction > Trans.VTransCount OR Fl.TransactionValue > Trans.Amount
  --Declaring cursor for fetching exception data row one by one from temp table
 DECLARE MerExp_Cursor CURSOR FOR 
 SELECT Merchant,Fleet,ExpFleetTrans,ActFleetTrans,ExpCar,ActCar,ExpValue,ActValue FROM #Temp
 OPEN MerExp_Cursor 
 FETCH NEXT FROM MerExp_Cursor  INTO @Merchant,@Fleet,@ExpFleetTrans,@ActFleetTrans,@ExpCar,@ActCar,@ExpValue,@ActValue
 WHILE @@FETCH_STATUS = 0 BEGIN  
     SET @Case=''
 IF(@ExpFleetTrans>@ActFleetTrans) BEGIN
     SET @Case='UnExp Trans'
 END
 IF(@ExpCar>@ActCar) BEGIN 
 IF(@Case<>'') BEGIN
     SET @Case=@Case+','+ 'UnExp Cars'
 END
 ELSE BEGIN
     SET @Case=@Case+ 'UnExp Cars'
 END
 END
 IF(@ExpValue>@ActValue) BEGIN
 IF(@Case<>'') BEGIN
     SET @Case=@Case+','+'UnExp Trans Value'
 END
 ELSE BEGIN
     SET @Case=@Case+'UnExp Trans Value'
 END
 END
 --Set data in xml format
 SET @InnerXml=@InnerXml+ COALESCE(CAST((SELECT COALESCE(@Merchant,'') AS 'td','',COALESCE(@Fleet,'') AS 'td','',COALESCE(@ExpFleetTrans,0) AS 'td','',
 COALESCE(@ActFleetTrans,0) AS 'td','',COALESCE(@ExpCar,0) AS 'td','',COALESCE(@ActCar,0) AS 'td','', CONCAT('$',COALESCE(@ExpValue,0.00)) AS 'td','',
 CONCAT('$',COALESCE(@ActValue,0.00)) AS 'td','',COALESCE(@Case,'') AS 'td' FOR XML PATH('tr'),ELEMENTS) AS NVARCHAR(MAX)),'')
 FETCH NEXT FROM MerExp_Cursor  INTO @Merchant,@Fleet,@ExpFleetTrans,@ActFleetTrans,@ExpCar,@ActCar,@ExpValue,@ActValue
 END   
 CLOSE MerExp_Cursor    
 DEALLOCATE MerExp_Cursor 
 DROP TABLE #Temp
 SET @XmlData=@InnerXml
 SET @ReportType=@Frequency
 SET @FDate=@DateFrom
 SET @TDate= DATEADD(DAY,-1,@DateTo)
 SELECT @XmlData,@ReportType,@FDate,@TDate
END TRY
--use catch to log error in the table  ProcErrorHandler
BEGIN CATCH
        INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
        SELECT ERROR_NUMBER()
		      ,ERROR_SEVERITY()
			  ,ERROR_STATE()
			  ,ERROR_PROCEDURE()
			  ,ERROR_LINE()
			  ,ERROR_MESSAGE()
			  ,SUSER_SNAME()
			  ,HOST_NAME()
			  ,GETDATE()
END CATCH
GO