﻿using System.ComponentModel.DataAnnotations;

namespace MTData.Web.Models
{
  

   

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string PCode { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

   
}
