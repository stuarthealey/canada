﻿using System.Collections.Generic;

using MTD.Core.DataTransferObjects;

namespace MTD.Core.Service.Interface
{
    public interface ISubAdminService
    {
        IEnumerable<AllMerchantsDto> MerchantList();

        IEnumerable<FleetDto> GetFleet(string merchantListId, int userType, int userId);

        bool Add(UserDto userDto);

        IEnumerable<AllMerchantsDto> GetUserMerchant(int userId);

        string Update(UserDto userDto, int userId);

        UserDto GetUser(int userId);

        IEnumerable<UserDto> GetSusbAdminByFilter(PageSizeSearchDto pageSizeSearch);

        bool Remove(int subAdminId);

        IEnumerable<UserDto> SubAdminList(PageSizeSearchDto pageSizeSearch);

        IEnumerable<MerchantUserDto> GetMyMerchantByFilter(PageSizeSearchDto pageSizeSearch);

        IEnumerable<MerchantUserDto> MyMerchantList(string name, int userId);

        IEnumerable<MerchantListDto> GetMerchantList(int userId);

        IEnumerable<UserFleetDto> GetUsersFleet(int userId);

        IEnumerable<MTDTransactionDto> GetTransaction(MTDTransactionDto tDto);

        int GetTransactionCount(MTDTransactionDto tDto);

        int GetDriverCount(MTDTransactionDto tDto);

        IEnumerable<DriverTransactionResult> GetDriver(MTDTransactionDto tDto);

        int GetTechFeeDetailCount(MTDTransactionDto tDto);

        IEnumerable<TechnologyDetailReportDto> GetTechnologyDetail(MTDTransactionDto tDto);

        int GetTechFeeCount(MTDTransactionDto tDto);

        IEnumerable<TechnologySummaryReportDto> GetTechnologySummary(MTDTransactionDto tDto);

        IEnumerable<DriverRecipientDto> GetUserDriverReceipent(string fleelistId);

        IEnumerable<AllMerchantsDto> GetCorpUserMerchant(int userId);

    }
}
