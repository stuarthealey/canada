﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.ObjectModel;

namespace MTD.Core.DataTransferObjects
{
    public class ServiceProviderUrlDto
    {
        public List<DatawireServiceUrlDto> ServiceProviderUrls { get; set; }
        public string ActiveUrl { get; set; }
       
        
    }
}
