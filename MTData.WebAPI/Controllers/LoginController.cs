﻿using System;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using System.Configuration;
using System.Web.Configuration;
using Ninject;

using MTD.Core.Service.Interface;
using MTD.Core.DataTransferObjects;
using MTData.WebAPI.Models;
using MTData.WebAPI.Resources;
using MTData.WebAPI.Common;
using MTData.WebAPI.CanadaCommon;
using MTData.Utility;

namespace MTData.WebAPI.Controllers
{
    public class LoginController : ApiController
    {
        #region (Constructor Loading)

        private LocalVariables objLocalVars = new LocalVariables();
        readonly ILogger _logger = new Logger();
        private readonly IDriverService _driverService;
        private readonly ITerminalService _terminalService;
        private readonly IFleetService _fleetService;
        private readonly IMerchantService _merchantService;
        private readonly IUserService _userService;
        private readonly IReceiptService _receiptService;
        private readonly IVehicleService _vehicleService;
        private readonly ITransactionService _transactionService;
        private StringBuilder _logMessage;
        private StringBuilder _logTracking;
        private readonly ICommonService _commonService;

        public LoginController([Named("DriverService")] IDriverService driverService, [Named("TerminalService")] ITerminalService terminalService,
            [Named("FleetService")] IFleetService fleetService, [Named("MerchantService")] IMerchantService merchantService, [Named("UserService")] IUserService userService,
            [Named("ReceiptService")] IReceiptService receiptService, [Named("VehicleService")] IVehicleService vehicleService, [Named("CommonService")] ICommonService commonService,
            [Named("TransactionService")] ITransactionService transactionService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _driverService = driverService;
            _terminalService = terminalService;
            _fleetService = fleetService;
            _merchantService = merchantService;
            _userService = userService;
            _receiptService = receiptService;
            _vehicleService = vehicleService;

            _transactionService = transactionService;
            _commonService = commonService;
            _logMessage = logMessage;
            _logTracking = logTracking;
        }
        #endregion

        #region (Sign in Action)
        /// <summary>
        /// Purpose             :   Android login and sending receipt information
        /// Function Name       :   SignIn
        /// Created By          :   Salil Gupta
        /// Created On          :   12/15/2014
        /// Modificatios Made   :   Optimize process and refatored
        /// Modified By         :   Sunil Singh
        /// Modidified On       :   19-Aug-2015
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult SignIn(HttpRequestMessage request)
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            _logMessage.AppendLine("[START] creating an instance of relevent classes for SingIn");

            LoginHeader mHeader = new LoginHeader();
            LoginReceipt mReceipt = new LoginReceipt();
            LoginCompany mCompany = new LoginCompany();
            LoginTips mTips = new LoginTips();
            LoginSurcharge mSurcharge = new LoginSurcharge();
            LoginTaxes mTaxes = new LoginTaxes();
            LoginResponse response = new LoginResponse();

            _logMessage.AppendLine("[END] creating an instance of relevent classes for SignIn");

            try
            {
                _logMessage.AppendLine("creating an object of XmlDocument Class");

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                _logMessage.AppendLine("load method of xmlDoc class executed successfully");

                // Used to log request and response as per requirement
                int logReqResp = Convert.ToInt32(WebConfigurationManager.AppSettings["LogFirstDataRequestResponse"]);

                if (logReqResp == 1)
                    _logTracking.AppendLine("Android Request : \n" + xmlDoc.InnerXml);

                #region Setting local variables
                _logMessage.AppendLine("SelectSingleNode method of xmlDoc class");
                var selectSingleNode = xmlDoc.SelectSingleNode("LoginRequest");
                if (selectSingleNode.IsNull())
                {
                    _logMessage.AppendLine("Failed to SelectSingleNode method of xmlDoc class");

                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidFormat);
                    return Ok(response);
                }

                _logMessage.AppendLine("[START] Initializing local variables from XML request data");
                
                SetLocalVarFromXMLRequest(xmlDoc);
                
                _logMessage.AppendLine("[END] Initializing local variables from XML request data");
                #endregion

                #region Fetching login data
                LoginRequest lr = new LoginRequest
                {
                    RequestId = objLocalVars.RequestId,
                    DeviceId = objLocalVars.DeviceId,
                    DriverId = objLocalVars.DriverNo,
                    DriverPin = objLocalVars.DriverPin
                };

                _logMessage.AppendLine("executing GetDriverLoginDto to get driver data");
                var driverLoginDto = _driverService.GetDriverLoginDto(lr.DeviceId, lr.DriverId, (int)UserTypes.Merchant).FirstOrDefault();
                if (!string.IsNullOrEmpty(driverLoginDto.Error))
                {
                    _logMessage.AppendLine("Failed to execute getDriverLoginDto to get driver data");

                    response.Header = CommonFunctions.FailedResponse(driverLoginDto.Error, lr.DeviceId, lr.RequestId);
                    return Ok(response);
                }

                _logMessage.AppendLine("validating driver PIN");
                bool IsValidPin = StringExtension.ValidatePassCode(lr.DriverPin, driverLoginDto.PIN);
                if (!IsValidPin)
                {
                    _logMessage.AppendLine("Failed to driver PIN validation.");

                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidLogin, lr.DeviceId, lr.RequestId);
                    return Ok(response);
                }
                #endregion

                _logMessage.AppendLine("Calling ReceiptByfleetId method of _receiptService class to get fleet with fleetId " + driverLoginDto.fleetId);
                
                List<ReceiptListDto> receipt = _receiptService.ReceiptByfleetId(Convert.ToInt32(driverLoginDto.fleetId)).ToList();
                
                _logMessage.AppendLine("ReceiptByfleetId method of _receiptService executed successfully");
                _logMessage.AppendLine("making list of receiptInformation and getting information from recipet");

                List<ReceiptInfo> receiptInformation = receipt.Select(t => new ReceiptInfo
                 {
                     Order = t.Postion,
                     Show = (t.IsShow == true) ? "1" : "0",
                     Name = t.FieldName.Trim(),
                     Value = t.fk_FieldText.Trim()
                 }).ToList();

                _logMessage.AppendLine("making list of receiptInformation and getting information from recipet completed");
                _logMessage.AppendLine("_transactionService.GetTaxSurchages executing");

                TxnTaxSurchargesDto surCharge = _transactionService.GetTaxSurchages(Convert.ToInt32(driverLoginDto.fk_MerchantID), Convert.ToInt32(driverLoginDto.fleetId));

                _logMessage.AppendLine("_transactionService.GetTaxSurchages executed");
                _logMessage.AppendLine("GenerateToken");

                string token = StringExtension.GenerateToken(lr.DriverId, lr.DriverPin);

                response.Token = token;
                mReceipt.ReceiptField = receiptInformation;

                //request Header information
                mHeader.DeviceId = lr.DeviceId;
                mHeader.RequestId = lr.RequestId;
                mHeader.Status = PaymentAPIResources.Success;
                mHeader.Message = PaymentAPIResources.SuccessLogIn;
                mHeader.CapkUpdateRequired = driverLoginDto.UpdateRequired;

                _logMessage.AppendLine("setting login company");
                
                mCompany.Fleet = driverLoginDto.FleetName;
                mCompany.Vehicle = driverLoginDto.VehicleNumber;
                mCompany.Driver = driverLoginDto.DriverNo;
                mCompany.DriversTaxNumber = driverLoginDto.DriverTaxNo;
                mCompany.CompanyTaxNumber = driverLoginDto.CompanyTaxNumber;
                mCompany.CompanyName = driverLoginDto.Company;
                driverLoginDto.Address = string.IsNullOrEmpty(driverLoginDto.Address) ? "" : driverLoginDto.Address + " , ";
                driverLoginDto.fk_City = string.IsNullOrEmpty(driverLoginDto.fk_City) ? "" : driverLoginDto.fk_City + " , ";
                driverLoginDto.fk_State = string.IsNullOrEmpty(driverLoginDto.fk_State) ? "" : driverLoginDto.fk_State + " , ";
                driverLoginDto.fk_Country = string.IsNullOrEmpty(driverLoginDto.fk_Country) ? "" : driverLoginDto.fk_Country;
                mCompany.CompanyAddress = driverLoginDto.Address + driverLoginDto.fk_City + driverLoginDto.fk_State + driverLoginDto.fk_Country;
                mCompany.CompanyPhoneNumber = driverLoginDto.Phone;

                _logMessage.AppendLine("setting tip");
                
                mTips.TipPercentageLow = driverLoginDto.TipPerLow;
                mTips.TipPercentageMedium = driverLoginDto.TipPerMedium;
                mTips.TipPercentageHigh = driverLoginDto.TipPerHigh;
                mTaxes.FederalTax = driverLoginDto.FederalTaxRate;
                mTaxes.StateTax = driverLoginDto.StateTaxRate;

                _logMessage.AppendLine("setting surcharge");
                
                mSurcharge.SurchargeType = surCharge.SurchargeType;
                mSurcharge.SurchargeFixed = surCharge.SurchargeFixed;
                mSurcharge.SurchargePercentage = surCharge.SurchargePer;
                mSurcharge.SurchargeMaxCap = surCharge.SurMaxCap;
                mSurcharge.BookingFeeType = surCharge.FeeType;
                mSurcharge.FeeFixed = surCharge.FeeFixed;
                mSurcharge.FeeMaxCap = surCharge.FeeMaxCap;
                mSurcharge.FeePercentage = surCharge.FeePer;

                _logMessage.AppendLine("Preparing login response");

                response.Surcharge = mSurcharge;
                response.Taxes = mTaxes;
                response.CardSwiped = (driverLoginDto.IsCardSwiped == true) ? "1" : "0";
                response.Contactless = (driverLoginDto.IsContactless == true) ? "1" : "0";
                response.ChipAndPin = (driverLoginDto.IsChipAndPin == true) ? "1" : "0";
                response.FleetId = Convert.ToString(driverLoginDto.fleetId);
                response.CurrencyCode = driverLoginDto.CurrencyCode;
                response.Disclaimer = driverLoginDto.DisclaimerPlainText;
                response.TermsConditions = driverLoginDto.TermConditionPlainText;
                response.IsTaxIncluded = (driverLoginDto.IsTaxInclusive == true) ? "1" : "0";
                response.Header = mHeader;
                response.ReceiptFormat = mReceipt;
                response.CompanyDetails = mCompany;
                response.Tips = mTips;
                response.IsShowTip = (driverLoginDto.IsShowTip == true) ? "1" : "0";

                _logMessage.AppendLine("Login response prepared");
                _logMessage.AppendLine("Saving login detail into DataBase");

                _commonService.SaveLogInDetails(driverLoginDto.DriverNo, driverLoginDto.VehicleNumber, token, false, Convert.ToInt32(driverLoginDto.fleetId));
                
                _logTracking.AppendLine("DriverNo " + objLocalVars.DriverNo + " and  DeviceId " + objLocalVars.DeviceId + " LoggedIn at " + System.DateTime.Now.ToUniversalTime());

                _logMessage.AppendLine("Returning response");
                _logger.LogInfoMessage(_logMessage.ToString());

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                response = new LoginResponse();
                response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.Exception, mHeader.DeviceId, mHeader.RequestId);
                return Ok(response);
            }
        }

        /// <summary>
        /// Initializing local variables.
        /// </summary>
        /// <param name="xmlDoc"></param>
        private void SetLocalVarFromXMLRequest(XmlDocument xmlDoc)
        {
            try
            {
                var reqID = xmlDoc.SelectSingleNode("LoginRequest/Header/RequestId");
                if (!reqID.IsNull())
                    objLocalVars.RequestId = xmlDoc.SelectSingleNode("LoginRequest/Header/RequestId").InnerText;

                var dvcID = xmlDoc.SelectSingleNode("LoginRequest/Header/DeviceId");
                if (!dvcID.IsNull())
                    objLocalVars.DeviceId = xmlDoc.SelectSingleNode("LoginRequest/Header/DeviceId").InnerText;

                var drvrNo = xmlDoc.SelectSingleNode("LoginRequest/Login/DriverNumber");
                if (!drvrNo.IsNull())
                    objLocalVars.DriverNo = xmlDoc.SelectSingleNode("LoginRequest/Login/DriverNumber").InnerText;

                var drvrPIN = xmlDoc.SelectSingleNode("LoginRequest/Login/Pin");
                if (!drvrPIN.IsNull())
                    objLocalVars.DriverPin = xmlDoc.SelectSingleNode("LoginRequest/Login/Pin").InnerText;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region (EMAIL Action)
        /// <summary>
        /// Purpose             :   Sending Email to merchant customer
        /// Function Name       :   Email
        /// Created By          :   Salil Gupta
        /// Created On          :   01/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/01/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult Email(HttpRequestMessage request)
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            var response = new MailResponse();

            string emailFormat = "{0}<tr><td>{1}</td><td>{2}</td><td>";

            try
            {
                var xmlDoc = new XmlDocument();

                _logMessage.AppendLine("calling load method of xmlDoc class to get xml request from android device and load");
                
                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                _logMessage.AppendLine("calling SelectSingleNode method of xmlDoc class");

                var selectSingleNode = xmlDoc.SelectSingleNode("MailRequest");

                _logMessage.AppendLine("checking if parent node of requet xml is not null");

                if (selectSingleNode.IsNull())
                {
                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidFormat);
                    return Ok(response);
                }

                _logMessage.AppendLine("receiving transaction information from android");

                //Initializing email request.
                EmailRequest er = InitializeEmailRequest(xmlDoc);

                _logMessage.AppendLine("transaction information from android recieved successfully");
                _logMessage.AppendLine("making html table for sending mail");

                var tablestring = string.Empty;
                tablestring = string.Format("{0}<html><body><table style='font-size:12px; font-family:verdana;border:1px black;height:500px;width:800px;'>", tablestring);

                _logMessage.AppendLine("Calling ReceiptToShowByfleetId method of _receiptService class to get fleet with fleetId " + er.FleetId);

                bool isValid = _commonService.IsValidateTokenMacAdd(er.DeviceId, er.DriverId, er.CToken, er.FleetId);
                bool isToken = string.IsNullOrEmpty(er.CToken);
                if (isToken || !isValid)
                {
                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.UnauthorizedAccess, er.DeviceId, er.RequestId);
                    return Ok(response);
                }

                List<ReceiptListDto> receipt = _receiptService.ReceiptToShowByfleetId(er.FleetId).ToList();

                //Fetching Card Number and Plate Number from database
                var cardPlate = _commonService.GetCardAndPlate(Convert.ToInt32(er.RequestId));
                er.PlateNumber = cardPlate.Item1;
                er.CreditCardNumber = cardPlate.Item2;

                _logMessage.AppendLine("ReceiptToShowByfleetId method of _receiptService executed successfully");

                if (receipt.Count.Equals(0))
                {
                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.FleetNotFound, er.DeviceId, er.RequestId);
                    return Ok(response);
                }

                _logMessage.AppendLine("receipt is not null");
                _logMessage.AppendLine("begin for loop to get reciept information and append data to mail table");

                //Fromating table string
                tablestring = FromatTableString(receipt, emailFormat, er, tablestring);
                tablestring = tablestring + "</table></body></html>";
                
                _logMessage.AppendLine("mail table created successfully that is " + tablestring);

                bool responseEmail = false;
                using (var em = new EmailUtil())
                {
                    _logMessage.AppendLine("Calling SendMail method of EmailUtil to send mail taking parameters mail, tablestring, cName, Invoice =" + er.CMail + ", " + tablestring + ", " + er.CName + ", Invoice");

                    responseEmail = em.SendMail(er.CMail, tablestring, er.CName, er.CName + " Invoice");
                }

                _logMessage.AppendLine("SendMail method of EmailUtil to send executed successfully response true");

                if (!responseEmail)
                {
                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.MailError, er.DeviceId, er.RequestId);
                    return Ok(response);
                }

                _logMessage.AppendLine("SendMail method of EmailUtil to send executed successfully response true");

                response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.MailSuccess, er.DeviceId, er.RequestId, PaymentAPIResources.Success);

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.Exception);
                return Ok(response);
            }
        }

        /// <summary>
        /// Initialize EmailRequest object
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        private EmailRequest InitializeEmailRequest(XmlDocument xmlDoc)
        {
            try
            {
                EmailRequest er = new EmailRequest
                {
                    RequestId = xmlDoc.SelectSingleNode("MailRequest/Header/RequestId").InnerText,
                    DeviceId = xmlDoc.SelectSingleNode("MailRequest/Header/DeviceId").InnerText,
                    DriverId = xmlDoc.SelectSingleNode("MailRequest/Header/DriverId").InnerText,
                    CMail = xmlDoc.SelectSingleNode("MailRequest/Header/CustomerMailId").InnerText,
                    CToken = xmlDoc.SelectSingleNode("MailRequest/Header/Token").InnerText,
                    CName = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/CompName").InnerText,
                    CPhone = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/CompPhone").InnerText,
                    CTaxNo = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/CompTaxNumber").InnerText,
                    CAddress = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/CompanyAddress").InnerText,
                    DestAdd = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/DestAdd").InnerText,
                    Disclaimer = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/Disclaimer").InnerText,
                    DriverTaxNo = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/DriverTaxNo").InnerText,
                    Extra = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/Extra").InnerText,
                    Fare = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/Fare").InnerText,
                    FederalTaxAmt = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/FederalTaxAmt").InnerText,
                    FederalTaxPer = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/FederalTaxPer").InnerText,
                    Flagfall = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/FlagFall").InnerText,
                    FleetId = Convert.ToInt32(xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/Fleet").InnerText),
                    PickAdd = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/PickAdd").InnerText,
                    StateMaxTax = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/StateMaxTax").InnerText,
                    StateTaxPer = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/StateTaxPer").InnerText,
                    SubTotal = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/SubTotal").InnerText,
                    SurAmt = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/SurAmt").InnerText,
                    SurPer = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/SurPer").InnerText,
                    TermsAndConditions = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/TermsAndConditions").InnerText,
                    Tip = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/Tip").InnerText,
                    Tolls = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/Tolls").InnerText,
                    Total = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/Total").InnerText,
                    Vehicle = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/Vehicle").InnerText
                };

                return er;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get formated HTML Table for email body
        /// </summary>
        /// <param name="receipt"></param>
        /// <param name="emailFormat"></param>
        /// <param name="er"></param>
        /// <param name="tablestring"></param>
        /// <returns></returns>
        private string FromatTableString(List<ReceiptListDto> receipt, string emailFormat, EmailRequest er, string tablestring)
        {
            try
            {
                tablestring = string.Empty;
                tablestring = "<html lang='en' xmlns='http://www.w3.org/1999/xhtml>'<head><style>.bod  { border-bottom: 1px solid black;width:300px;overflow: hidden; white-space: nowrap;word-wrap: break-word;}</style></head><body>";
                tablestring += "<table width='400' style='font-size:12px; font-family:verdana;border-top: 1px solid black;border-right: 1px solid black;border-left: 1px solid black;height:500px;overflow: hidden; white-space: nowrap;width:400px;table-layout:fixed;'>";
                
                foreach (ReceiptListDto t in receipt)
                {
                    var value = (EmailField)Enum.Parse(typeof(EmailField), t.FieldName.Replace(" ", String.Empty), true);

                    switch (value)
                    {
                        case EmailField.CompanyName: // "CompanyName":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.CName + "</td></tr>";
                            break;
                        case EmailField.CompanyPhone: //"CompanyPhone":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.CAddress + "</td></tr>";
                            break;
                        case EmailField.CompanyAddress: //"CompanyAddress":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.CAddress + "</td></tr>";
                            break;
                        case EmailField.CompanyTaxNumber: //"CompanyTaxNumber":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.CTaxNo + "</td></tr>";
                            break;
                        case EmailField.Driver: //"Driver":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.DriverId + "</td></tr>";
                            break;
                        case EmailField.DriverTaxNumber: //"DriverTaxNumber":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.DriverTaxNo + "</td></tr>";
                            break;
                        case EmailField.Extra: //"Extra":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.Extra + "</td></tr>";
                            break;
                        case EmailField.Fare: //"Fare":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.Fare + "</td></tr>";
                            break;
                        case EmailField.FederalTaxAmount: //"FederalTaxAmount":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.FederalTaxAmt + "</td></tr>";
                            break;
                        case EmailField.FederalTaxPercentage: //"FederalTaxPercentage":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.FederalTaxPer + "</td></tr>";
                            break;
                        case EmailField.Flagfall: //"Flagfall":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.Flagfall + "</td></tr>";
                            break;
                        case EmailField.Fleet: //"Fleet":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.FleetId + "</td></tr>";
                            break;
                        case EmailField.PickAddress: //"PickAddress":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.PickAdd + "</td></tr>";
                            break;
                        case EmailField.StateTaxAmount: //"StateTaxAmount":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.StateMaxTax + "</td></tr>";
                            break;
                        case EmailField.StateTaxPercentage: //"StateTaxPercentage":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.StateTaxPer + "</td></tr>";
                            break;
                        case EmailField.Subtotal: //"Subtotal":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.SubTotal + "</td></tr>";
                            break;
                        case EmailField.SurchargeAmount: //"SurchargeAmount":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.SurAmt + "</td></tr>";
                            break;
                        case EmailField.SurchargePercentage: //"SurchargePercentage":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.SurPer + "</td></tr>";
                            //tablestring = string.Format(emailFormat, tablestring, t.fk_FieldText, er.SurPer);
                            break;
                        case EmailField.TermsandConditions: //"TermsandConditions":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.TermsAndConditions + "</td></tr>";
                            break;
                        case EmailField.Tip: //"Tip":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.Tip + "</td></tr>";
                            break;
                        case EmailField.Tolls: //"Tolls":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.Tolls + "</td></tr>";
                            break;
                        case EmailField.Total: //"Total":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.Total + "</td></tr>";
                            break;
                        case EmailField.Vehicle: //"Vehicle":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.Vehicle + "</td></tr>";
                            break;
                        case EmailField.Disclaimer: //"Disclaimer":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.Disclaimer + "</td></tr>";
                            break;
                        case EmailField.DestinationAddress: //"DestinationAddress":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.DestAdd + "</td></tr>";
                            break;
                        case EmailField.CreditCardNumber://"CreditCardNumber":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.CreditCardNumber + "</td></tr>";
                            break;
                        case EmailField.PlateNumber://"PlateNumber":
                            tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + er.PlateNumber + "</td></tr>";
                            break;


                    }
                }
                tablestring = tablestring + "</table></br>";
                return tablestring;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult SignOut(HttpRequestMessage request)
        {
            LogoutResponse logOutResponse = new LogoutResponse();
            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(request.Content.ReadAsStreamAsync().Result);

                var selectSingleNode = xmlDocument.SelectSingleNode("LogoutRequest");
                if (selectSingleNode.IsNull())
                {
                    logOutResponse.Status = PaymentAPIResources.Failed;
                    logOutResponse.Message = PaymentAPIResources.InvalidFormat;
                    return Ok(logOutResponse);
                }

                var driverNumber = xmlDocument.SelectSingleNode("LogoutRequest/Logout/DriverNumber").InnerText;
                var token = xmlDocument.SelectSingleNode("LogoutRequest/Header/Token").InnerText;
                var fleetId = xmlDocument.SelectSingleNode("LogoutRequest/Header/FleetId").InnerText;
                
                _logTracking.AppendLine("SignOut process start for DriverNo: " + driverNumber + " and  FleetID " + fleetId + " at " + System.DateTime.Now.ToUniversalTime());
                
                bool isToken = string.IsNullOrEmpty(token);
                bool isTokenValid = _commonService.IsTokenValid(driverNumber, token);
                if (!isTokenValid || isToken)
                {
                    logOutResponse.Message = PaymentAPIResources.SuccessLogOut;
                    logOutResponse.Status = PaymentAPIResources.Success;
                    return Ok(logOutResponse);
                }

                char[] deliem = { '^' };
                char[] deliemDriver = { ':' };
                string[] encArray = token.Split(deliem);
                string encDriverAndPin = encArray[1];
                string[] encArrayDriver = encDriverAndPin.Split(deliemDriver);
                string encDriverNo = encArrayDriver[0];
                string encDriverPin = encArrayDriver[1];
                string driverNo = StringExtension.DecryptDriver(encDriverNo);
                string driverPin = StringExtension.DecryptDriver(encDriverPin);
                DriverDto driver = _driverService.GetDriverDto(driverNo, Convert.ToInt32(fleetId));
               
                bool isDriverValid = StringExtension.ValidatePassCode(driverPin, driver.PIN);
                if (!isDriverValid)
                {
                    logOutResponse.Message = PaymentAPIResources.UnauthorizedAccess;
                    logOutResponse.Status = PaymentAPIResources.Failed;
                    return Ok(logOutResponse);
                }

                int result = _commonService.SaveLogOutDetails(driverNo, false, Convert.ToInt32(fleetId));
                if (result == 2)
                {
                    logOutResponse.Status = PaymentAPIResources.Success;
                    logOutResponse.Message = PaymentAPIResources.NoRecordFound;
                    return Ok(logOutResponse);
                }
                else if (result == 0)
                {
                    logOutResponse.Status = PaymentAPIResources.Success;
                    logOutResponse.Message = PaymentAPIResources.SuccessLogOut;
                    _logTracking.AppendLine("SignOut process end for DriverNo: " + driverNumber + " and  FleetID " + fleetId + " at " + System.DateTime.Now.ToUniversalTime());
                    return Ok(logOutResponse);
                }
                else if (result == 3)
                {
                    logOutResponse.Status = PaymentAPIResources.Success;
                    logOutResponse.Message = PaymentAPIResources.NotRecipient;
                    return Ok(logOutResponse);
                }
                else
                {
                    logOutResponse.Status = PaymentAPIResources.Failed;
                    logOutResponse.Message = PaymentAPIResources.InvalidInput;
                    return Ok(logOutResponse);
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logTracking.ToString(), ex);
                logOutResponse.Message = PaymentAPIResources.Exception;
                logOutResponse.Status = PaymentAPIResources.Failed;
                return Ok(logOutResponse);
            }
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult DispatchSystemLogin(HttpRequestMessage request)
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            _logger.LogInfoMessage(_logMessage.ToString());

            DispatchLoginResponse dispatchresponse = new DispatchLoginResponse();
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                // Used to log request and response as per requirement
                int logReqResp = Convert.ToInt32(WebConfigurationManager.AppSettings["LogFirstDataRequestResponse"]);

                if (logReqResp == 1)
                    _logTracking.AppendLine("DispatchLoginRequest : \n" + xmlDoc.InnerXml);

                // Need to check if the Stan is included in the dispatch login request. If it is not, then ensure we can handle it.
                string stanInnerText = string.Empty;
                var stan = xmlDoc.SelectSingleNode("DispatchLoginRequest/StanNo");
                if (!stan.IsNull() && !string.IsNullOrWhiteSpace(stan.InnerText))
                    stanInnerText = stan.InnerText;

                DispatchSystemLogin dispatchLogin = new DispatchSystemLogin
                {
                    DriverNumber = xmlDoc.SelectSingleNode("DispatchLoginRequest/DriverNumber").InnerText,
                    Pin = xmlDoc.SelectSingleNode("DispatchLoginRequest/Pin").InnerText,
                    CarNumber = xmlDoc.SelectSingleNode("DispatchLoginRequest/CarNumber").InnerText,
                    DeviceId = xmlDoc.SelectSingleNode("DispatchLoginRequest/DeviceId").InnerText,

                    //Stan = xmlDoc.SelectSingleNode("DispatchLoginRequest/StanNo").InnerText
                    Stan = stanInnerText
                };

                //Authenticating driver PIN
                _logMessage.AppendLine(string.Format("Authenticating driver. DriverNo:{0};DeviceID:{1};", dispatchLogin.DriverNumber, dispatchLogin.DeviceId));

                string configPin = WebConfigurationManager.AppSettings["DispatchPIN"];
                if (!dispatchLogin.Pin.Equals(configPin))
                {
                    dispatchresponse = CommonFunctions.FailedResponse(dispatchresponse, PaymentAPIResources.InvalidDriver, dispatchLogin.DeviceId, dispatchLogin.CarNumber, dispatchLogin.DriverNumber);

                    _logMessage.AppendLine(string.Format("Failed Authentication. DriverNo:{0};DeviceID:{1};", dispatchLogin.DriverNumber, dispatchLogin.DeviceId));
                    _logger.LogInfoMessage(_logMessage.ToString());

                    return Ok(dispatchresponse);
                }

                TerminalDto terminal = _terminalService.GetTerminalDto(dispatchLogin.DeviceId);
                if (terminal.IsNull())
                {
                    dispatchresponse = CommonFunctions.FailedResponse(dispatchresponse, PaymentAPIResources.InvalidTerminal, dispatchLogin.DeviceId, dispatchLogin.CarNumber, dispatchLogin.DriverNumber);

                    _logMessage.AppendLine(string.Format("Failed GetTerminalDto. DeviceId:{0};", dispatchLogin.DeviceId));
                    _logger.LogInfoMessage(_logMessage.ToString());

                    return Ok(dispatchresponse);
                }

                _logMessage.AppendLine("Calling GetVehicleByTerminal method of _vehicleService class to get terminal with terminalId " + terminal.TerminalID);

                VehicleDto vehicle = _vehicleService.GetVehicleByTerminal(terminal.TerminalID);
                if (vehicle.IsNull())
                {
                    dispatchresponse = CommonFunctions.FailedResponse(dispatchresponse, PaymentAPIResources.InvalidTerminal, dispatchLogin.DeviceId, dispatchLogin.CarNumber, dispatchLogin.DriverNumber);

                    _logMessage.AppendLine(string.Format("Failed GetVehicleByTerminal. TerminalId:{0};", terminal.TerminalID));
                    _logger.LogInfoMessage(_logMessage.ToString());

                    return Ok(dispatchresponse);
                }

                _logMessage.AppendLine(string.Format("GetDriverDto() DriverNumber:{0};fk_FleetID:{1};", dispatchLogin.DriverNumber, vehicle.fk_FleetID));

                DriverDto driver = _driverService.GetDriverDto(dispatchLogin.DriverNumber, vehicle.fk_FleetID);
                if (driver.IsNull())
                {
                    dispatchresponse = CommonFunctions.FailedResponse(dispatchresponse, PaymentAPIResources.InvalidDriver, dispatchLogin.DeviceId, dispatchLogin.CarNumber, dispatchLogin.DriverNumber);

                    _logMessage.AppendLine(string.Format("Failed GetDriverDto. DriverNumber:{0};fk_FleetID:{1};", dispatchLogin.DriverNumber, vehicle.fk_FleetID));
                    _logger.LogInfoMessage(_logMessage.ToString());

                    return Ok(dispatchresponse);
                }

                _logMessage.AppendLine(string.Format("GetFleet() fleetId:{0};", vehicle.fk_FleetID));

                FleetDto fleet = _fleetService.GetFleet(vehicle.fk_FleetID);
                if (fleet.fk_MerchantID != terminal.fk_MerchantID)
                {
                    dispatchresponse = CommonFunctions.FailedResponse(dispatchresponse, PaymentAPIResources.FleetNotFound, dispatchLogin.DeviceId, dispatchLogin.CarNumber, dispatchLogin.DriverNumber);

                    _logMessage.AppendLine(string.Format("Failed. fleet.fk_MerchantID:{0};terminal.fk_MerchantID:{1};", fleet.fk_MerchantID, terminal.fk_MerchantID));
                    _logger.LogInfoMessage(_logMessage.ToString());

                    return Ok(dispatchresponse);
                }

                FleetDto fleetCurrency = _fleetService.GetCurrency(Convert.ToInt32(fleet.fK_CurrencyCodeID));
                dispatchresponse.Token = StringExtension.GenerateToken(dispatchLogin.DriverNumber, dispatchLogin.Pin);
                dispatchresponse.CarNumber = dispatchLogin.CarNumber;
                dispatchresponse.DriverNumber = dispatchLogin.DriverNumber;
                dispatchresponse.FleetId = Convert.ToString(fleet.FleetID);
                dispatchresponse.FleetName = fleet.FleetName;
                dispatchresponse.DeviceID = dispatchLogin.DeviceId;
                dispatchresponse.Message = "Success";
                dispatchresponse.CardSwiped = (fleet.IsCardSwiped == true) ? "1" : "0";

                // Contactless check needs to look at Fleet first, if it is enabled for fleet, then check for Terminal.
                bool isContactless = (fleet.IsContactless == true) ? (terminal.IsContactLess == true) : false;
                dispatchresponse.Contactless = (isContactless ? "1" : "0");

                dispatchresponse.ChipAndPin = (fleet.IsChipAndPin == true) ? "1" : "0";
                dispatchresponse.CurrencyCode = fleetCurrency.CurrencyCode;
                
                _commonService.SaveLogInDetails(dispatchLogin.DriverNumber, dispatchLogin.CarNumber, dispatchresponse.Token, true, fleet.FleetID);

                //Changes done for EMV Contact certification of Canada
                if (ConfigurationManager.AppSettings["Project"] == PaymentAPIResources.CanadaProject)
                {
                    PaymentController payment = new PaymentController(_transactionService, _merchantService, _driverService, _terminalService, _commonService, _logMessage, _logTracking);

                    _logMessage.Append(string.Format("GetFirstDataDetails() DeviceId:{0};FleetID:{1};RequestType:{2};", dispatchLogin.DeviceId, fleet.FleetID, RequestType.Dispatch));

                    var firstDataDetails = _commonService.GetFirstDataDetails(dispatchLogin.DeviceId, (string)null, (string)null, Convert.ToInt32(fleet.FleetID), RequestType.Dispatch.ToString());

                    _logMessage.Append(string.Format("GetMacDetails() DID:{0};TerminalID:{1};FDMerchantID:{2};Stan:{3};", firstDataDetails.DID, firstDataDetails.TerminalID, firstDataDetails.FDMerchantID, dispatchLogin.Stan));

                    MACVerification macDetails = payment.GetMacDetails(firstDataDetails, dispatchLogin.Stan);
                    
                    dispatchresponse.MACVerification = macDetails;
                    dispatchresponse.RCTerminal = firstDataDetails.RCTerminalId;

                    _logMessage.AppendLine(string.Format("Bit32:{0}-{1};Bit34:{2}-{3}-{4};Stan:{5};RCTerminalId:{6};", macDetails.MacData, macDetails.CheckDigit, macDetails.TMAC, macDetails.TKPE, macDetails.TKME, dispatchLogin.Stan, firstDataDetails.RCTerminalId));
                }

                _logTracking.AppendLine("DispatchSystemLogin process completed for DriverNo: " + dispatchLogin.DriverNumber + " and  DeviceID " + dispatchLogin.DeviceId + " at " + System.DateTime.Now.ToUniversalTime());

                _logger.LogInfoMessage(_logMessage.ToString());

                return Ok(dispatchresponse);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                dispatchresponse = CommonFunctions.FailedResponse(dispatchresponse, PaymentAPIResources.Failed, "0");
                dispatchresponse.Token = null;

                return Ok(dispatchresponse);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult DispatchSystemLogout(HttpRequestMessage request)
        {
            DispatchLogoutResponse dispatchresponse = new DispatchLogoutResponse();
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                DispatchSystemLogout dispatchLogout = new DispatchSystemLogout
                {
                    DriverNumber = xmlDoc.SelectSingleNode("DispatchLogoutRequest/DriverNumber").InnerText,
                    CarNumber = xmlDoc.SelectSingleNode("DispatchLogoutRequest/CarNumber").InnerText,
                    FleetId = xmlDoc.SelectSingleNode("DispatchLogoutRequest/FleetId").InnerText
                };

                FleetDto fleet = _fleetService.GetFleet(Convert.ToInt32(dispatchLogout.FleetId));

                _commonService.SaveLogOutDetails(dispatchLogout.DriverNumber, true, Convert.ToInt32(dispatchLogout.FleetId));

                dispatchresponse.DriverNumber = dispatchLogout.DriverNumber;
                dispatchresponse.CarNumber = dispatchLogout.CarNumber;
                dispatchresponse.FleetId = dispatchLogout.FleetId;
                dispatchresponse.FleetName = fleet.FleetName;
                dispatchresponse.Message = PaymentAPIResources.SuccessLogOut;
                
                _logger.LogInfoMessage(_logTracking.ToString());
                
                return Ok(dispatchresponse);
            }
            catch (Exception ex)
            {
                _logger.LogException(ex);
                dispatchresponse.DriverNumber = "0";
                dispatchresponse.CarNumber = "0";
                dispatchresponse.FleetId = "0";
                dispatchresponse.FleetName = "0";
                dispatchresponse.Message = PaymentAPIResources.Exception;

                return Ok(dispatchresponse);
            }
        }

        //zzz Chetu 357
        /// <summary>
        /// This is used to get new debit keys from FD
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult DebitKeyRequest(HttpRequestMessage request)
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            DebitKeyResponse debitKeyResponse = new DebitKeyResponse();

            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                DispatchSystemLogin debitKeyRequest = new DispatchSystemLogin
                {
                    Pin = xmlDoc.SelectSingleNode("DebitKeyRequest/Pin").InnerText,
                    DeviceId = xmlDoc.SelectSingleNode("DebitKeyRequest/DeviceId").InnerText,
                    Stan = xmlDoc.SelectSingleNode("DebitKeyRequest/StanNo").InnerText
                };

                _logMessage.AppendLine("Authenticating driver.");
                string configPin = WebConfigurationManager.AppSettings["DispatchPIN"];
                if (!debitKeyRequest.Pin.Equals(configPin))
                {
                    _logMessage.AppendLine("Failed authentication process.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    debitKeyResponse.Message = PaymentAPIResources.InvalidPin;
                    return Ok(debitKeyResponse);
                }

                _logMessage.AppendLine("executing GetTerminalDto method of _terminalService");
                TerminalDto terminal = _terminalService.GetTerminalDto(debitKeyRequest.DeviceId);
                if (terminal.IsNull())
                {
                    _logMessage.AppendLine("executing GetTerminalDto method of _terminalService");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    debitKeyResponse.Message = PaymentAPIResources.InvalidDeviceId;
                    return Ok(debitKeyResponse);
                }

                _logMessage.AppendLine("Calling GetVehicleByTerminal method of _vehicleService class to get terminal with terminalId " + terminal.TerminalID);
                VehicleDto vehicle = _vehicleService.GetVehicleByTerminal(terminal.TerminalID);
                if (vehicle.IsNull())
                {
                    _logMessage.AppendLine("Failed Calling GetVehicleByTerminal method of _vehicleService class to get terminal with terminalId " + terminal.TerminalID);
                    _logger.LogInfoMessage(_logMessage.ToString());

                    debitKeyResponse.Message = PaymentAPIResources.InvalidTerminal;
                    return Ok(debitKeyResponse);
                }

                PaymentController payment = new PaymentController(_transactionService, _merchantService, _driverService, _terminalService, _commonService, _logMessage, _logTracking);
                var firstDataDetails = _commonService.GetFirstDataDetails(debitKeyRequest.DeviceId, (string)null, (string)null, Convert.ToInt32(vehicle.fk_FleetID), RequestType.Dispatch.ToString());
                MACVerification macDetails = payment.GetMacDetails(firstDataDetails, debitKeyRequest.Stan);

                debitKeyResponse.MacData = macDetails.MacData;
                debitKeyResponse.CheckDigit = macDetails.CheckDigit;
                debitKeyResponse.TMAC = macDetails.TMAC;
                debitKeyResponse.TKPE = macDetails.TKPE;
                debitKeyResponse.TKME = macDetails.TKME;
                debitKeyResponse.RCTerminal = firstDataDetails.RCTerminalId;
                debitKeyResponse.Message = PaymentAPIResources.Success;

                _logMessage.AppendLine("Bit32 : " + macDetails.MacData + macDetails.CheckDigit + ", Bit34 : " + macDetails.TMAC + macDetails.TKPE + macDetails.TKME + ", Stan : " + debitKeyRequest.Stan);

                _logger.LogInfoMessage(_logMessage.ToString());

                return Ok(debitKeyResponse);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                debitKeyResponse.Message = PaymentAPIResources.Failed;

                return Ok(debitKeyResponse);
            }
        }

    }
}
