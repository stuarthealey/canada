IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CorporateDetailReportScheduler]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_CorporateDetailReportScheduler]
GO

CREATE PROCEDURE dbo.usp_CorporateDetailReportScheduler
-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <23-02-2016>
-- Description:	<Prepare and Send detail reports to corporate users>
-- =============================================
--	S.Healey	2016-06-06		- Changed company name reference to MTData, LLC
--
AS
BEGIN
	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SET NOCOUNT ON
		SET ANSI_WARNINGS OFF

		DECLARE	@Res INT,
				@UserId INT,
				@To NVARCHAR(50),
				@Body NVARCHAR(MAX),
				@Error NVARCHAR(200),
				@MailId INT,
				@Valid INT,
				@MsgXml NVARCHAR(40),
				@XmlData NVARCHAR(MAX),
				@UserName NVARCHAR(45),
				@ReportType INT,
				@RepType NVARCHAR(50),
				@RepHeading NVARCHAR(200),
				@FDate DATETIME,
				@TDate DATETIME,
				@TotalTransaction INT,
				@TotalActualFare NVARCHAR(30),
				@TotalTips NVARCHAR(30),
				@TotalFareAmount NVARCHAR(30),
				@TotalSurcharge NVARCHAR(30),
				@TotalFee  NVARCHAR(30),
				@NoChargeBack INT,
				@ChargeBackValue NVARCHAR(25),
				@NoVoid INT,
				@VoidValue NVARCHAR(25),
				@AmountOwing  NVARCHAR(30),
				@TotalTechFee NVARCHAR(30),
				@ParentUserId INT,
				@NextScheduleDateTime DATETIME,
				@IsSuperCorporate BIT,
				@Count INT,
				@Index INT

		DECLARE @NextScheduleData TABLE (
				Id INT IDENTITY(1, 1) PRIMARY KEY,
				ParentId INT,
				NextScheduleDate DATETIME
				)

		--Declaring cursor for fetching admin recipients one by one from database
		DECLARE Corporate_Cursor CURSOR FOR SELECT u.UserID, u.Email, u.FName, u.ParentUserID 
											FROM dbo.Users u 
												INNER JOIN dbo.Recipient rec ON u.UserID = rec.fk_UserID 
											WHERE u.fk_UserTypeID = 5 
											  AND u.IsActive = 1 
											  AND rec.fk_ReportID = 2 
											  AND rec.IsRecipient = 1
											  AND rec.fk_UserTypeID = 5

		OPEN Corporate_Cursor
		FETCH NEXT FROM Corporate_Cursor INTO @UserId, @To, @UserName, @ParentUserId

		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF (@ParentUserId IS NULL)
			BEGIN
				SELECT @NextScheduleDateTime = NextScheduleDateTime 
				FROM dbo.ScheduleReport 
				WHERE CorporateUserId = @UserId 
				  AND fk_ReportID = 2 
				  AND IsCreated = 1

				SET @IsSuperCorporate = 1
			END
			ELSE
			BEGIN
				SELECT @NextScheduleDateTime = NextScheduleDateTime
				FROM dbo.ScheduleReport 
				WHERE CorporateUserId = @ParentUserId 
				  AND fk_ReportID = 2 
				  AND IsCreated = 1

				SET @IsSuperCorporate = 0
			END

			IF (@NextScheduleDateTime >= DATEADD(SECOND, -900, GETDATE()) AND @NextScheduleDateTime < GETDATE())
			BEGIN
				--Exceuting store procedure usp_CorporateDetailReport to get data in xml format
				EXEC usp_CorporateDetailReport @UserId,@IsSuperCorporate, @XmlData OUTPUT,@ReportType OUTPUT, @FDate OUTPUT,@TDate OUTPUT,@NextScheduleDateTime OUTPUT

				IF (@IsSuperCorporate = 1 AND @NextScheduleDateTime IS NOT NULL)
				BEGIN
					INSERT INTO @NextScheduleData(ParentId, NextScheduleDate)
					SELECT @UserId, @NextScheduleDateTime
				END

				SET @RepType = CASE @ReportType 
									WHEN 1 THEN 'Daily Detailed Transaction Report'
									WHEN 2 THEN 'Weekly Detailed Transaction Report' 
									ELSE 'Monthly Detailed Transaction Report' 
								END

				SET @RepHeading = @RepType + ' ' + '(Dated from ' + CONVERT(VARCHAR(11), @FDate, 106) + ' to ' + CONVERT(VARCHAR(11), @TDate, 106) + ')'

				IF (@XmlData IS NULL OR @XmlData = '')
				BEGIN
					SET @Valid = 0
					INSERT INTO dbo.MailMessage(RepUserId, [Status], SendTo, Body, RepUserTypeId, [Date], ReportType) 
					VALUES (@UserId, -1, @To, 'No records in database', 5, GETDATE(), 'CorporateDetailReport')
				END
				ELSE
				BEGIN
					SET @Valid = 1
				END

				IF (@Valid = 1) 
				BEGIN
					SET @MsgXml = CAST((SELECT CONCAT('Dear', ' ', @UserName, ',') FOR XML PATH('p'), ELEMENTS ) AS NVARCHAR(MAX))

					--Set the xml data into html fromat
					SET @Body ='<html><style>div{font-size:13px;text-align:center;horizontal-align: middle;background-color:lightgrey}
								td{text-align:left;font-family:verdana;font-size:12px;}p{font-family:verdana;font-size:13px}span{font-family:verdana;font-size:13px}
								</style><body><p>Listed below is the transaction report containing details of all transactions done corresponding to merchants associated with you</p>
								<H5 style="color:grey;text-align:center;font-size:11px;font-family:verdana">' + @RepHeading + '</H5>
								<table border = 1 cellpadding="0" cellspacing="0" style="background-color:white;border-collapse:collapse"> <tr>
								<th style="color:grey;font-size:13px;width:900px;text-align:left">Merchant</th>
								<th style="color:grey;font-size:13px;width:700px;text-align:left">Report Date</th>
								<th style="color:grey;font-size:13px;width:120px;text-align:left">Trans Id</th>
								<th style="color:grey;font-size:13px;width:180px;text-align:left">Transaction Date</th>
								<th style="color:grey;font-size:13px;width:130px;text-align:left">Trans Type</th>
								<th style="color:grey;font-size:13px;width:140px;text-align:left">Card Type</th>
								<th style="color:grey;font-size:13px;width:180px;text-align:left">Last Four</th>
								<th style="color:grey;font-size:13px;width:150px;text-align:left">Auth Code</th>
								<th style="color:grey;font-size:13px;width:200px;text-align:left">Trip</th>
								<th style="color:grey;font-size:13px;width:500px;text-align:left">Driver</th>
								<th style="color:grey;font-size:13px;width:500px;text-align:left">Car Number</th>
								<th style="color:grey;font-size:13px;width:500px;text-align:left">Tech Fee</th>
								<th style="color:grey;font-size:13px;width:500px;text-align:left">Surcharge</th>
								<th style="color:grey;font-size:13px;width:150px;text-align:left">Gross Amt</th>
								<th style="color:grey;font-size:13px;width:150px;text-align:left">Fees</th>
								<th style="color:grey;font-size:13px;width:150px;text-align:left">Net Amt</th></tr>'    

					SET @Body = @MsgXml+@Body + @XmlData +'</table><br/><br/><span>Thanks & Regards,<br/><br/>MTData, LLC.<br/>www.mtdata.com<span>
								<h4 style="color:grey;font-size:12px;font-family:verdana">MTData-All rights reserved</h4>
								<p style="font-size:11px;color:black">Note:This is an automated message. Do not Reply or Reply All
								to this message as replies are not being accepted.</p></body></html>'

					INSERT INTO dbo.MailMessage(RepUserId, [Status], SendTo, Body, RepUserTypeId, [Date], ReportType) 
					VALUES (@UserId, 0, @To, @Body, 5, GETDATE(), 'CorporateDetailReport')

					SET @MailId = SCOPE_IDENTITY()

					--Exceute the store procedure sp_send_dbmail to send the detail report to corporate users
					EXEC @Res=msdb.dbo.sp_send_dbmail @profile_name = 'MTData', @recipients = @To, @body = @Body, @subject = 'Corporate Detailed Transaction Report', @body_format = 'HTML'
					IF (@Res != 0)
					BEGIN
						SET @Error = @@ERROR
						UPDATE dbo.MailMessage SET Error = @Error, [Date] = GETDATE() WHERE MailId = @MailId
					END
					ELSE
					BEGIN
						UPDATE dbo.MailMessage SET [Status] = 1, [Date] = GETDATE() WHERE MailId = @MailId
					END
				END
			END

			FETCH NEXT FROM Corporate_Cursor INTO @UserId, @To, @UserName, @ParentUserId
		END   
		CLOSE Corporate_Cursor   
		DEALLOCATE Corporate_Cursor

		SELECT @Count = COUNT(Id) FROM @NextScheduleData

		SET @Index = 1
		WHILE (@Index <= @Count)
		BEGIN
			SELECT	@NextScheduleDateTime = NextScheduleDate, 
					@ParentUserId = ParentId 
			FROM @NextScheduleData 
			WHERE Id = @Index

			UPDATE dbo.ScheduleReport 
			SET NextScheduleDateTime = @NextScheduleDateTime 
			WHERE CorporateUserId = @ParentUserId 
			  AND fk_ReportID = 2 
			  AND IsCreated = 1

			SET @Index = @Index + 1
		END
	END TRY
	BEGIN CATCH
		INSERT INTO dbo.ProcErrorHandler(ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, UserName, HostName, [TimeStamp])
		SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
	END CATCH
END
GO
