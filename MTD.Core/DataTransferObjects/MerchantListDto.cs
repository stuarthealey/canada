﻿using System;

namespace MTD.Core.DataTransferObjects
{
    public  class MerchantListDto
    {
        public int fk_MerchantID { get; set; }
        public Nullable<int> fk_UserID { get; set; }
        public int UserID { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Email { get; set; }
        public string Company { get; set; }
        public Nullable<bool> IsRecipient { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int fk_ReportID { get; set; }
        public string FirstName { get; set; }
    }
}
