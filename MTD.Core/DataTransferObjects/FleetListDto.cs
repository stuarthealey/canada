﻿namespace MTD.Core.DataTransferObjects
{
    public class FleetListDto
    {
        public int FleetID { get; set; }
        public string FleetName { get; set; }
    }
}