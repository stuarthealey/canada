﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    [XmlSerializerFormat]
    public class LoginCompany
    {
        //[DataMember(EmitDefaultValue = false)]
        //public string Fleet { get; set; }

        private string fleet;

        [DataMember]
        public string Fleet
        {
            get { return this.fleet ?? "null"; }
            set { this.fleet = value; }
        }
        
        //[DataMember(EmitDefaultValue = false)]
        //public string Vehicle { get; set; }

        private string vehicle;

        [DataMember]
        public string Vehicle
        {
            get { return this.vehicle ?? "null"; }
            set { this.vehicle = value; }
        }
        
        //[DataMember(EmitDefaultValue = false)]
        //public string Driver { get; set; }

        private string driver;

        [DataMember]
        public string Driver
        {
            get { return this.driver ?? "null"; }
            set { this.driver = value; }
        }
        
        //[DataMember(EmitDefaultValue = false)]
        //public string DriversTaxNumber { get; set; }

        private string driversTaxNumber;

        [DataMember]
        public string DriversTaxNumber
        {
            get { return this.driversTaxNumber ?? "null"; }
            set { this.driversTaxNumber = value; }
        }
        
        //[DataMember(EmitDefaultValue = true)]
        //public string CompanyTaxNumber { get; set; }

        private string companyTaxNumber;

        [DataMember]
        public string CompanyTaxNumber
        {
            get { return this.companyTaxNumber ?? "null"; }
            set { this.companyTaxNumber = value; }
        }
        
        
        //[DataMember(EmitDefaultValue = false)]
        //public string CompanyName { get; set; }

        private string companyName;

        [DataMember]
        public string CompanyName
        {
            get { return this.companyName ?? "null"; }
            set { this.companyName = value; }
        }
        
        //[DataMember(EmitDefaultValue = false)]
        //public string CompanyAddress { get; set; }

        private string companyAddress;

        [DataMember]
        public string CompanyAddress
        {
            get { return this.companyAddress ?? "null"; }
            set { this.companyAddress = value; }
        }
        
        //[DataMember(EmitDefaultValue = false)]
        //public string CompanyPhoneNumber { get; set; }

        private string companyPhoneNumber;

        [DataMember]
        public string CompanyPhoneNumber
        {
            get { return this.companyPhoneNumber ?? "null"; }
            set { this.companyPhoneNumber = value; }
        }
    }
}