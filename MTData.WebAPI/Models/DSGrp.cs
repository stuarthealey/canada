﻿namespace MTData.WebAPI.Models
{
    public class DSGrp
    {
        public string GatewayTxnId { get; set; }
        public string DiscNRID { get; set; }
        public string DiscTransQualifier { get; set; }
        public string DiscProcCode { get; set; }
        public string DiscPOSEntry { get; set; }
        public string DiscRespCode { get; set; }
        public string DiscPOSData { get; set; }
    }
}