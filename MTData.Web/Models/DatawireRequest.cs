﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.Web.Models
{
    public class DatawireRequest
    {
        public int MerchantId { get; set; }
        public string TerminalId { get; set; }
    }
}