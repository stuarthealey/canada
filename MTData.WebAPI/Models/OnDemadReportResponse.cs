﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Models
{
    public class OnDemadReportResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public string DriverNumber { get; set; }
        public string Token { get; set; } 

    }
}