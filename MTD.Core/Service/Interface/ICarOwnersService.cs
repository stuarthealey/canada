﻿using MTD.Core.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.Service.Interface
{
    public interface ICarOwnersService
    {
        bool Add(CarOwnersDto carOwnerDto);
        CarOwnersDto Read(int id);
        int GetOwnerCount(string name, int merchantId, string OwnerName);
        bool Remove(int id);
        bool Update(CarOwnersDto carOwnerDto);
        IEnumerable<CarOwnersDto> GetOwnerListByFilter(FilterSearchParametersDto filterSearch);
        List<CarOwnersDto> AddCarOwnewrList(List<CarOwnersDto> objCarOwnerDto);
    }
}
