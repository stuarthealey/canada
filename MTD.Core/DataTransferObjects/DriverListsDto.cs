﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
   public class DriverListsDto
    {
        public int DriverID { get; set; }
        public string DriverNumber { get; set; }
    }
}
