﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Globalization;
using System.Reflection;

using MTData.Utility;
using MTData.WebAPI.CanadaCommon;

namespace MTData.WebAPI.CanadaCert
{
    public class CnSendTransaction
    {
        readonly ILogger _logger = new Logger();

        /// <summary>
        /// Create the request message format for merchant provisioning
        /// </summary>
        /// <returns>Return the merchant provision request xml need to send on datawire</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string SendMessageToDatawire(string url, string xml, int timeOut = 35000, string requestType = "POST")
        {
            ILogger logger = new Logger();
            StringBuilder logMessage = new StringBuilder();

            logMessage.AppendLine("Controller Name:-CnSendTransaction; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            logMessage.AppendLine(string.Format("URL:{0};XML:{1};", (string.IsNullOrEmpty(url) ? "<NULL>" : url), (string.IsNullOrEmpty(xml) ? "<NULL>" : xml)));

            // Check we have a URL and XML passed in.
            if (string.IsNullOrEmpty(url) || string.IsNullOrEmpty(xml))
                return string.Empty;

            try
            {
                string xmlResponse = string.Empty;
                HttpWebRequest objWebrequest = (HttpWebRequest)WebRequest.Create(url);
                byte[] buf = Encoding.UTF8.GetBytes(xml);

                if (objWebrequest != null)
                {
                    // Header setting

                    // Header setting for the request start 
                    objWebrequest.UserAgent = Global.UserAgent;
                    objWebrequest.Method = requestType;
                    objWebrequest.ContentLength = buf.Length;
                    objWebrequest.KeepAlive = true;
                    objWebrequest.Headers.Add(HttpRequestHeader.CacheControl, "no-cache");
                    objWebrequest.ContentType = "text/xml";

                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    {
                        // Take the timeout value from the configuration.
                        int sendToDatawireTimeout = 35000;

                        if (Int32.TryParse(ConfigurationManager.AppSettings["SendToDatawireTimeout"], out sendToDatawireTimeout))
                            objWebrequest.Timeout = sendToDatawireTimeout;
                        else
                            objWebrequest.Timeout = timeOut;
                    }
                    else
                    {
                        // Take the timeout value from the method params.
                        objWebrequest.Timeout = timeOut;
                    }

                    //Header setting for the request ends 
                    //Allowing the service to authenticate on any of following SSL certificate 
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                         | SecurityProtocolType.Tls11
                                                         | SecurityProtocolType.Tls12
                                                         | SecurityProtocolType.Ssl3;
                    
                    // Allows for validation of SSL conversations
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    // request stream
                    //only when Request Type is post
                    if (requestType.Equals("POST"))
                    {
                        using (var stream = objWebrequest.GetRequestStream()) //get the request stream from request object created 
                        {
                            stream.Write(buf, 0, buf.Length);// write the content in the request stream                        
                        }
                    }

                    // Response stream

                    // get the response for the request
                    var response = (HttpWebResponse)objWebrequest.GetResponse();

                    logMessage.AppendLine(string.Format("Response.StatusCode:{0};StatusDescription:{1};", response.StatusCode, response.StatusDescription));

                    // Get the response in a stream.
                    var receiveStream = response.GetResponseStream();

                    // Pipes the stream to a higher level stream reader with the required encoding format. 
                    if (receiveStream != null)
                    {
                        var readStream = new StreamReader(receiveStream, Encoding.UTF8);

                        // Get the registration response
                        xmlResponse = readStream.ReadToEnd();
                        response.Close();
                        readStream.Close();
                    }
                }

                logMessage.AppendLine(string.Format("xmlResponse:{0};", xmlResponse));

                logger.LogInfoMessage(logMessage.ToString());

                return xmlResponse;
            }
            catch (WebException ex)
            {
                logger.LogInfoFatal(logMessage.ToString(), ex);
                return string.Empty;
            }
            catch (Exception ex)
            {
                logger.LogInfoFatal(logMessage.ToString(), ex);
                return string.Empty;
            }
        }
    }
}