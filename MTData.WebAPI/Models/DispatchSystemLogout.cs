﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Models
{
    public class DispatchSystemLogout
    {
        public string DriverNumber { get; set; }
        public string CarNumber { get; set; }
        public string FleetId { get; set; }
    }
}