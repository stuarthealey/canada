//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3615
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by wsdl, Version=2.0.50727.3038.
// 
namespace Firstdata.RapidConnect.Datawire.Soap
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "rcSoapBinding", Namespace = "http://securetransport.dw/rcservice/soap")]
    public partial class rcService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {

        private System.Threading.SendOrPostCallback rcTransactionOperationCompleted;

        /// <remarks/>
        public rcService()
        {
            this.Url = "https://stg.dw.us.fdcnet.biz/rc";
        }

        /// <remarks/>
        public event rcTransactionCompletedEventHandler rcTransactionCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://securetransport.dw/rcservice", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("Response", Namespace = "http://securetransport.dw/rcservice/soap")]
        public ResponseType rcTransaction([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://securetransport.dw/rcservice/soap")] RequestType Request)
        {
            object[] results = this.Invoke("rcTransaction", new object[] {
                        Request});
            return ((ResponseType)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginrcTransaction(RequestType Request, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("rcTransaction", new object[] {
                        Request}, callback, asyncState);
        }

        /// <remarks/>
        public ResponseType EndrcTransaction(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((ResponseType)(results[0]));
        }

        /// <remarks/>
        public void rcTransactionAsync(RequestType Request)
        {
            this.rcTransactionAsync(Request, null);
        }

        /// <remarks/>
        public void rcTransactionAsync(RequestType Request, object userState)
        {
            if ((this.rcTransactionOperationCompleted == null))
            {
                this.rcTransactionOperationCompleted = new System.Threading.SendOrPostCallback(this.OnrcTransactionOperationCompleted);
            }
            this.InvokeAsync("rcTransaction", new object[] {
                        Request}, this.rcTransactionOperationCompleted, userState);
        }

        private void OnrcTransactionOperationCompleted(object arg)
        {
            if ((this.rcTransactionCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.rcTransactionCompleted(this, new rcTransactionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://securetransport.dw/rcservice/soap")]
    public partial class RequestType
    {

        private ReqClientIDType reqClientIDField;

        private TransactionType transactionField;

        private string versionField;

        private string clientTimeoutField;

        public RequestType()
        {
            this.versionField = "3";
        }

        /// <remarks/>
        public ReqClientIDType ReqClientID
        {
            get
            {
                return this.reqClientIDField;
            }
            set
            {
                this.reqClientIDField = value;
            }
        }

        /// <remarks/>
        public TransactionType Transaction
        {
            get
            {
                return this.transactionField;
            }
            set
            {
                this.transactionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "integer")]
        public string ClientTimeout
        {
            get
            {
                return this.clientTimeoutField;
            }
            set
            {
                this.clientTimeoutField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://securetransport.dw/rcservice/soap")]
    public partial class ReqClientIDType
    {

        private string dIDField;

        private string appField;

        private string authField;

        private string clientRefField;

        /// <remarks/>
        public string DID
        {
            get
            {
                return this.dIDField;
            }
            set
            {
                this.dIDField = value;
            }
        }

        /// <remarks/>
        public string App
        {
            get
            {
                return this.appField;
            }
            set
            {
                this.appField = value;
            }
        }

        /// <remarks/>
        public string Auth
        {
            get
            {
                return this.authField;
            }
            set
            {
                this.authField = value;
            }
        }

        /// <remarks/>
        public string ClientRef
        {
            get
            {
                return this.clientRefField;
            }
            set
            {
                this.clientRefField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://securetransport.dw/rcservice/soap")]
    public partial class TransactionResponseType
    {

        private string returnCodeField;

        private PayloadType payloadField;

        /// <remarks/>
        public string ReturnCode
        {
            get
            {
                return this.returnCodeField;
            }
            set
            {
                this.returnCodeField = value;
            }
        }

        /// <remarks/>
        public PayloadType Payload
        {
            get
            {
                return this.payloadField;
            }
            set
            {
                this.payloadField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://securetransport.dw/rcservice/soap")]
    public partial class PayloadType
    {

        private PayloadTypeEncoding encodingField;

        private bool encodingFieldSpecified;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PayloadTypeEncoding Encoding
        {
            get
            {
                return this.encodingField;
            }
            set
            {
                this.encodingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EncodingSpecified
        {
            get
            {
                return this.encodingFieldSpecified;
            }
            set
            {
                this.encodingFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://securetransport.dw/rcservice/soap")]
    public enum PayloadTypeEncoding
    {

        /// <remarks/>
        xml_escape,

        /// <remarks/>
        cdata,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://securetransport.dw/rcservice/soap")]
    public partial class StatusType
    {

        private string statusCodeField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string StatusCode
        {
            get
            {
                return this.statusCodeField;
            }
            set
            {
                this.statusCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://securetransport.dw/rcservice/soap")]
    public partial class RespClientIDType
    {

        private string dIDField;

        private string clientRefField;

        /// <remarks/>
        public string DID
        {
            get
            {
                return this.dIDField;
            }
            set
            {
                this.dIDField = value;
            }
        }

        /// <remarks/>
        public string ClientRef
        {
            get
            {
                return this.clientRefField;
            }
            set
            {
                this.clientRefField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://securetransport.dw/rcservice/soap")]
    public partial class ResponseType
    {

        private RespClientIDType respClientIDField;

        private StatusType statusField;

        private TransactionResponseType transactionResponseField;

        private string versionField;

        public ResponseType()
        {
            this.versionField = "3";
        }

        /// <remarks/>
        public RespClientIDType RespClientID
        {
            get
            {
                return this.respClientIDField;
            }
            set
            {
                this.respClientIDField = value;
            }
        }

        /// <remarks/>
        public StatusType Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public TransactionResponseType TransactionResponse
        {
            get
            {
                return this.transactionResponseField;
            }
            set
            {
                this.transactionResponseField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://securetransport.dw/rcservice/soap")]
    public partial class TransactionType
    {

        private string serviceIDField;

        private PayloadType payloadField;

        /// <remarks/>
        public string ServiceID
        {
            get
            {
                return this.serviceIDField;
            }
            set
            {
                this.serviceIDField = value;
            }
        }

        /// <remarks/>
        public PayloadType Payload
        {
            get
            {
                return this.payloadField;
            }
            set
            {
                this.payloadField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void rcTransactionCompletedEventHandler(object sender, rcTransactionCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class rcTransactionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal rcTransactionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public ResponseType Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((ResponseType)(this.results[0]));
            }
        }
    }
}
