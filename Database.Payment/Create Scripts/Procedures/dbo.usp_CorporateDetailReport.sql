IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CorporateDetailReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_CorporateDetailReport]
GO

CREATE PROCEDURE dbo.usp_CorporateDetailReport
-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <23-02-2016>
-- Description:	<Prepare details reports for corporate users>
-- =============================================
-- S.Healey		2016-11-10		- Performance optimisation with use of indexes and improved queries.
(
	@UserId INT,
	@IsSuperCorporate BIT,
	@XmlData NVARCHAR(MAX) OUT,
	@ReportType INT OUT,
	@FDate DATE OUT,
	@TDate DATE OUT,
	@NextSchedule DATETIME OUT
)
AS
BEGIN
	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SET ANSI_WARNINGS OFF
		SET NOCOUNT ON

		DECLARE @NextScheduleDateTime DATETIME,
				@ScheduleId INT,
				@Frequency INT,
				@FrequencyType INT,
				@DateFrom DATE,
				@DateTo DATE,
				@XmlTotal NVARCHAR(200),
				@CardType NVARCHAR(25),
				@InnerXml NVARCHAR(MAX),
				@ParentCorporateId INT

		DECLARE @CorporateTable TABLE (
			FleetId INT,
			MerchantId INT
			)

		IF (@IsSuperCorporate = 1)
		BEGIN
			SELECT  @ScheduleId = ScheduleID,
					@Frequency = Frequency,
					@FrequencyType = FrequencyType,
					@NextScheduleDateTime = NextScheduleDateTime 
			FROM dbo.ScheduleReport 
			WHERE fk_ReportId = 2 
			  AND IsCreated = 1 
			  AND CorporateUserId = @UserId	  
		END
		ELSE
		BEGIN
			SELECT @ParentCorporateId = ParentUserID 
			FROM dbo.Users 
			WHERE UserID = @UserId 
			  AND IsActive = 1

			SELECT	@ScheduleId = ScheduleID,
					@Frequency = Frequency,
					@FrequencyType = FrequencyType,
					@NextScheduleDateTime = NextScheduleDateTime
			FROM dbo.ScheduleReport
			WHERE fk_ReportId = 2
			  AND IsCreated = 1
			  AND CorporateUserId = @ParentCorporateId
		END

		INSERT INTO @CorporateTable
		SELECT fk_FleetID, fk_MerchantId 
		FROM dbo.UserFleet 
		WHERE fk_UserTypeId = 5
		  AND IsActive = 1 
		  AND fk_UserID = @UserId

		--Set the dates in between transaction data to be prepared 
		IF (@Frequency = 1) 
		BEGIN       
			SET @DateFrom = GETDATE() - @FrequencyType
			SET @DateTo = GETDATE()
			SET @NextScheduleDateTime = @NextScheduleDateTime + @FrequencyType
		END
		ELSE IF (@Frequency = 2)
		BEGIN
			SET @DateFrom = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK, -@FrequencyType, GETDATE())), 0) - 1
			SET @DateTo = DATEADD(WEEK, DATEDIFF(WEEK, 0, GETDATE()), 0) - 1
			SET @NextScheduleDateTime = DATEADD(WK, @FrequencyType, @NextScheduleDateTime)
		END
		ELSE IF (@Frequency = 3)
		BEGIN
			SET @DateFrom = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH, -@FrequencyType, GETDATE())), 0)
			SET @DateTo = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)
			SET @NextScheduleDateTime = DATEADD(MONTH, @FrequencyType, @NextScheduleDateTime)
		END
		ELSE
		BEGIN
			RETURN 3
		END

		SET @XmlData = ''

		--Getting all the card types one by one 
		DECLARE @count INT = 1
		WHILE @count <= 5 
		BEGIN
			IF (@count = 1)
			BEGIN
				SET @CardType = 'AmericanExpress'
			END
			ELSE IF (@count = 2)
			BEGIN
				SET @CardType = 'Discover'
			END
			ELSE IF (@count = 3)
			BEGIN
				SET @CardType = 'MasterCard'
			END
			ELSE IF (@count = 4)
			BEGIN
				SET @CardType = 'Visa'
			END  
			ELSE 
			BEGIN
				SET @CardType = 'Amex'
			END   

			--Prepare the transaction data in xml format corresponding to a that specific card type                      
			SET @InnerXml = ISNULL(CAST((SELECT ISNULL(m.Company, '') AS 'td', '',
													  CAST(GETDATE() AS DATE) AS 'td', '',
													  t.Id AS 'td', '', ISNULL(CAST(t.TxnDate AS DATE), '') AS 'td','',
													  ISNULL(t.TxnType, '') AS 'td', '',
													  ISNULL(t.CardType, '')  AS 'td', '',
													  ISNULL(t.LastFourDigits, '')  AS 'td', '',
													  ISNULL(t.AuthId, '') AS 'td', '',
													  ISNULL(t.JobNumber, '') AS 'td', '',
													  ISNULL(t.DriverNo, '') AS 'td' , '',
													  ISNULL(t.VehicleNo, '') AS 'td', '',
													  CONCAT('$', CAST(ISNULL(t.TechFee, 0.00) AS DECIMAL(16, 2))) AS 'td', '',
													  CONCAT('$', ISNULL(CAST(t.Surcharge AS DECIMAL(18, 2)), 0.00)) AS 'td', '',
													  CONCAT('$', ISNULL(CAST(t.Amount AS DECIMAL(18, 2)), 0.00)) AS 'td', '',
													  CONCAT('$', CAST(ISNULL(t.FleetFee, 0.00) + ISNULL(t.Fee, 0.00) AS DECIMAL(16, 2))) AS 'td', '',
													  CONCAT('$', CAST(ISNULL(t.Amount, 0.00) - ISNULL(t.Fee, 0.00) - ISNULL(t.FleetFee, 0.00) AS DECIMAL(18, 2))) AS 'td'
										FROM dbo.MTDTransaction t WITH (INDEX(IX_MTDTransactionTxnDate))
											INNER JOIN dbo.Vehicle v  ON t.VehicleNo=v.VehicleNumber 
											INNER JOIN dbo.Fleet f ON v.fk_FleetID=f.FleetID 
											INNER JOIN dbo.Merchant m ON f.fk_MerchantID=m.MerchantID 
										WHERE t.TxnDate >= @DateFrom 
										  AND t.TxnDate < @DateTo 
										  AND t.fk_FleetId = v.fk_FleetID
										  AND t.CardType = @CardType 
										  AND t.ResponseCode IN ('000', '00', '002') 
										  AND t.fk_FleetId IN (SELECT DISTINCT ct.FleetId FROM @CorporateTable ct)
										  AND t.MerchantId IN (SELECT DISTINCT cl.MerchantId FROM @CorporateTable cl)
										FOR XML PATH('tr'), ELEMENTS ) AS NVARCHAR(MAX)), '')

			SET @XmlTotal = (SELECT ISNULL(SUM(tr.Amount),0.00)
							   FROM dbo.MTDTransaction tr WITH (INDEX(IX_MTDTransactionTxnDate))
							   WHERE tr.TxnDate >= @DateFrom 
								 AND tr.TxnDate < @DateTo
								 AND tr.TxnType IN ('Sale', 'Completion')
								 AND tr.IsRefunded = 0 
								 AND tr.IsVoided = 0
								 AND tr.CardType = @CardType 
								 AND tr.ResponseCode IN ('000', '00', '002') 
								 AND tr.fk_FleetId IN (SELECT DISTINCT ct.FleetId FROM @CorporateTable ct)
								 AND tr.MerchantId IN (SELECT DISTINCT cl.MerchantId FROM @CorporateTable cl)) -
						   (SELECT ISNULL(SUM(tr.Fee), 0) + ISNULL(SUM(tr.FleetFee), 0)
							   FROM dbo.MTDTransaction tr  WITH (INDEX(IX_MTDTransactionTxnDate))
							   WHERE tr.TxnDate >= @DateFrom 
								 AND tr.TxnDate < @DateTo
								 AND ((tr.TxnType = 'Sale' AND tr.IsRefunded = 0 AND tr.IsVoided = 0) OR (tr.TxnType = 'Authorization' AND tr.IsCompleted = 1))
								 AND tr.CardType = @CardType 
								 AND tr.ResponseCode IN ('000', '00', '002') 
								 AND tr.fk_FleetId IN (SELECT DISTINCT ct.FleetId FROM @CorporateTable ct)
								 AND tr.MerchantId IN (SELECT DISTINCT cl.MerchantId FROM @CorporateTable cl))				          							 

			IF (@InnerXml <>'' AND  @InnerXml IS NOT NULL) 
			BEGIN
				SET @XmlTotal= '<tr><td colspan=16 style="text-align:center;">
								<div>'+CONCAT('Total for ' + @CardType + '' + ' : ', CONCAT('$', CAST(@XmlTotal AS DECIMAL(18, 2)))) + ' </div>
								</td></tr>'
			END
			ELSE
			BEGIN
				SET @XmlTotal=''
			END 

			SET @XmlData = @XmlData + @InnerXml + @XmlTotal
			SET @count = @count + 1
		END

		SET @ReportType = @Frequency
		SET @FDate = @DateFrom
		SET @TDate = DATEADD(DAY, -1, @DateTo)

		SELECT @XmlData, @ReportType, @FDate, @TDate

		IF (@IsSuperCorporate = 1)
		BEGIN
			SET @NextSchedule = @NextScheduleDateTime
			SELECT @NextSchedule
		END
	END TRY
	BEGIN CATCH
		INSERT INTO dbo.ProcErrorHandler(ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, UserName, HostName, [TimeStamp])
		SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
	END CATCH
END
GO
