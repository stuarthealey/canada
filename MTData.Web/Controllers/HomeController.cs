﻿using System.Web.Mvc;
using MTData.Web.Resource;

namespace MTData.Web.Controllers
{
    public class HomeController : Controller
    {

        /// <summary>
        /// Purpose             :  Default Home Page
        /// Function Name       :   Index
        /// Created By          :   Umesh Kumar
        /// Created On          :   16/12/2014
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>         
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Purpose             :  Default About Page
        /// Function Name       :   About
        /// Created By          :   Umesh Kumar
        /// Created On          :   16/12/2014
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>         
        /// <returns></returns>
        public ActionResult About()
        {
            ViewBag.Message = MtdataResource.Application_description_page;   //"Your application description page.";

            return View();
        }
        /// <summary>
        /// Purpose             :  Default Contact Page
        /// Function Name       :   About
        /// Created By          :   Umesh Kumar
        /// Created On          :   16/12/2014
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>         
        /// <returns></returns>
        public ActionResult Contact()
        {
            ViewBag.Message = MtdataResource.Contact_page; //"Your contact page.";

            return View();
        }
    }
}