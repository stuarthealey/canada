IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MerchantTotalTransByVehicle]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_MerchantTotalTransByVehicle]
GO

CREATE PROCEDURE dbo.usp_MerchantTotalTransByVehicle
-- ============================================= 
-- Author:	<Naveen Kumar>
-- Create date: <10-03-2016>
-- Description:	<Prepare and Send total trans by vehicle reports to merchants>
-- ============================================= 
(
	@IsOnDemandReport BIT,
	@FromDate DATETIME = NULL,
	@ToDate DATETIME = NULL,
	@MerchantId INT,
	@ReportId INT,
	@XmlData NVARCHAR(MAX) OUT,
	@ReportType INT OUT,
	@FDate DATE OUT,
	@TDate DATE OUT,
	@TotalTransaction INT OUT,
	@TotalActualFare NVARCHAR(35) OUT,
	@TotalTips NVARCHAR(35) OUT,
	@TotalFareAmount NVARCHAR(35) OUT,
	@TotalSurcharge NVARCHAR(35) OUT,
	@TotalFee NVARCHAR(35) OUT,
	@NoChargeBack INT OUT,
	@ChargeBackValue NVARCHAR(35) OUT,
	@NoVoid INT OUT,
	@VoidValue NVARCHAR(35) OUT,
	@TotalTechFee NVARCHAR(35) OUT,
	@AmountOwing  NVARCHAR(35) OUT,
	@Slab1Message NVARCHAR(40) OUT,
	@Slab2Message NVARCHAR(40) OUT,
	@Slab3Message NVARCHAR(40) OUT,
	@Slab4Message NVARCHAR(40) OUT,
	@TotalSlab1 INT OUT,
	@TotalSlab2 INT OUT,
	@TotalSlab3 INT OUT,
	@TotalSlab4 INT OUT
)
AS
BEGIN
	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SET ANSI_WARNINGS OFF
		SET NOCOUNT ON

		DECLARE @NextScheduleDateTime DATETIME,
				@ScheduleId INT,
				@Frequency INT,
				@FrequencyType INT,
				@DateFrom DATE,
				@DateTo DATE,
				@TransCountSlablt1 INT,
				@TransCountSlabGt2 INT,
				@TransCountSlablt2 INT,
				@TransCountSlabGt3 INT,
				@TransCountSlablt3 INT,
				@TransCountSlabGt4 INT

		DECLARE @InnerTable TABLE (
				Id INT, 
				FareValue MONEY,
				Tip MONEY, 
				Amount MONEY, 
				Surcharge MONEY, 
				Fee MONEY,
				VehicleNo NVARCHAR(70), 
				TxnType NVARCHAR(25),
				ResponseCode NVARCHAR(30),
				fk_FleetId INT,
				IsRefunded BIT,
				IsVoided BIT,
				IsCompleted BIT,
				TechFee MONEY,
				FleetFee MONEY,
				MerchantId INT
			)  

		DECLARE @OuterTable TABLE (
				Id  INT, 
				FareValue MONEY, 
				Tip MONEY,
				Amount MONEY,
				Surcharge MONEY, 
				Fee MONEY, 
				TxnType NVARCHAR(25),
				ResponseCode NVARCHAR(30),
				fk_FleetId INT,
				TechFee MONEY,
				IsRefunded BIT,
				IsVoided BIT,
				IsCompleted BIT,
				FleetFee MONEY	
			)
		    
		IF (@ReportId = 7)
		BEGIN
			SELECT  @ScheduleId = ScheduleID,
					@Frequency = Frequency,
					@FrequencyType = FrequencyType,
					@NextScheduleDateTime = NextScheduleDateTime		 
			FROM dbo.ScheduleReport 
			WHERE fk_ReportId = 7 
			  AND IsCreated = 1 
			  AND fk_MerchantId IS NULL 
			  AND CorporateUserId IS NULL
		END

		IF (@ReportId IS NULL)
		BEGIN
			SELECT  @ScheduleId = ScheduleID,
					@Frequency = Frequency,
					@FrequencyType = FrequencyType,
					@NextScheduleDateTime = NextScheduleDateTime		 
			FROM dbo.ScheduleReport 
			WHERE fk_ReportId = 7 
			  AND IsCreated = 1 
			  AND fk_MerchantId = @MerchantId
		END

		IF (@IsOnDemandReport = 1)
		BEGIN
			SET @DateFrom = @FromDate
			SET @DateTo = @ToDate
		END
		ELSE
		BEGIN
			IF (@Frequency = 1)
			BEGIN
				SET @DateFrom = GETDATE() - @FrequencyType
				SET @DateTo = GETDATE()
				SET @NextScheduleDateTime = @NextScheduleDateTime + @FrequencyType
			END
			ELSE IF (@Frequency = 2)
			BEGIN
				SET @DateFrom = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK, -@FrequencyType, GETDATE())), 0) - 1
				SET @DateTo = DATEADD(WEEK, DATEDIFF(WEEK, 0, GETDATE()), 0) - 1
				SET @NextScheduleDateTime = DATEADD(WK, @FrequencyType, @NextScheduleDateTime)
			END
			ELSE IF (@Frequency = 3)
			BEGIN
				SET @DateFrom = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH, -@FrequencyType, GETDATE())), 0)
				SET @DateTo = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) 
				SET @NextScheduleDateTime = DATEADD(MONTH, @FrequencyType, @NextScheduleDateTime)
			END  
			ELSE
			BEGIN
				RETURN 1
			END 
		END

	    --Insert data into table variable InnnerTable for reuse                      
		INSERT INTO @InnerTable(Id, FareValue, Tip, Amount, Surcharge, Fee, VehicleNo, TxnType, ResponseCode, fk_FleetId, IsRefunded, IsVoided, IsCompleted, TechFee, FleetFee, MerchantId)                   
		SELECT mt.Id, mt.FareValue, mt.Tip, mt.Amount, mt.Surcharge, mt.Fee, mt.VehicleNo, mt.TxnType, mt.ResponseCode, mt.fk_FleetId, mt.IsRefunded, mt.IsVoided, mt.IsCompleted, mt.TechFee, mt.FleetFee, mt.MerchantId
		FROM (SELECT Id, FareValue, Tip, Amount, Surcharge, Fee, VehicleNo, TxnType, ResponseCode, fk_FleetId, TxnDate, IsRefunded, IsVoided, IsCompleted, TechFee, FleetFee, MerchantId
				FROM dbo.MTDTransaction)  mt
			INNER JOIN (SELECT VehicleNumber, fk_FleetID FROM dbo.Vehicle WHERE IsActive = 1) vh ON mt.VehicleNo = vh.VehicleNumber 
			INNER JOIN (SELECT FleetId, FleetName FROM dbo.Fleet WHERE IsActive = 1) fl ON vh.fk_FleetID = fl.FleetID
		WHERE CAST(mt.TxnDate AS DATE) >= @DateFrom 
		  AND CAST(mt.TxnDate AS DATE) < @DateTo    
		  AND mt.fk_FleetId = vh.fk_FleetID

		--Insert data into table variable OuterTable for reuse    
		INSERT INTO @OuterTable(Id, FareValue, Tip, Amount, Surcharge, Fee, TxnType, ResponseCode, fk_FleetId, TechFee, IsRefunded, IsVoided, IsCompleted, FleetFee) 
		SELECT t.Id, t.FareValue, t.Tip, t.Amount, t.Surcharge, t.Fee, t.TxnType, t.ResponseCode, t.fk_FleetId, t.TechFee, t.IsRefunded, t.IsVoided, t.IsCompleted, t.FleetFee
		FROM dbo.MTDTransaction t
		WHERE t.MerchantID = @MerchantId
		  AND CAST(t.TxnDate AS DATE) >= @DateFrom
		  AND CAST(t.TxnDate AS DATE) < @DateTo

		IF EXISTS (SELECT 1 FROM dbo.TransactionCountSlab WHERE Fk_MerchantId = @MerchantId)  
		BEGIN
			SELECT  @TransCountSlablt1 = TransCountSlablt1,
					@TransCountSlabGt2 = TransCountSlabGt2,
					@TransCountSlablt2 = TransCountSlablt2,
					@TransCountSlabGt3 = TransCountSlabGt3,
					@TransCountSlablt3 = TransCountSlablt3,
					@TransCountSlabGt4 = TransCountSlabGt4
			FROM dbo.TransactionCountSlab
			WHERE fk_MerchantId = @MerchantId
		END
		ELSE
		BEGIN
			SELECT  @TransCountSlablt1 = TransCountSlablt1,
					@TransCountSlabGt2 = TransCountSlabGt2,
					@TransCountSlablt2 = TransCountSlablt2,
					@TransCountSlabGt3 = TransCountSlabGt3,
					@TransCountSlablt3 = TransCountSlablt3,
					@TransCountSlabGt4 = TransCountSlabGt4
			FROM dbo.DefaultSetting 
			WHERE DefaultID = 7
		END

		SELECT COUNT(Id) AS 'Id',VehicleNo AS 'VehicleNo', fk_FleetId AS 'fk_FleetId' 
		INTO #temp
		FROM @InnerTable it 
		WHERE it.ResponseCode IN ('000', '00', '002')
		  AND it.MerchantId = @MerchantId
		GROUP BY it.VehicleNo, it.fk_FleetId	

		--Prepare data for summary report in xml format                            
		SET @XmlData=CAST((SELECT ROW_NUMBER() OVER (ORDER BY FleetName) AS 'td', '',
                                  ISNULL(vh.FleetName, '')  AS 'td', '',
							      (SELECT ISNULL(COUNT(Id), 0) FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND it.TxnType IN ('Sale', 'Completion') AND it.IsRefunded = 0 AND it.IsVoided = 0 AND it.ResponseCode IN ('000', '00', '002')) AS 'td', '',
							      CONCAT('$',(SELECT ISNULL(SUM(FareValue), '0.00') FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td', '',
                                  CONCAT('$',(SELECT ISNULL(SUM(Tip), '0.00') FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND  it.ResponseCode IN ('000', '00', '002'))) AS 'td', '',
								  CONCAT('$',(SELECT ISNULL(SUM(surcharge), '0.00')FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td', '',
								  CONCAT('$',(SELECT ISNULL(SUM(TechFee), '0.00')FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td', '',
							      (SELECT ISNULL(COUNT(id), 0) FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND it.TxnType='Refund' AND it.ResponseCode IN ('000', '00')) AS 'td', '',
							      CONCAT('$',(SELECT ISNULL(SUM(Amount), '0.00') FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND it.TxnType='Refund' AND it.ResponseCode IN ('000', '00'))) AS 'td', '',
							      (SELECT ISNULL(COUNT(id), 0) FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND it.TxnType='Void' AND it.ResponseCode IN ('000', '00')) AS 'td', '',
							      CONCAT('$',(SELECT ISNULL(SUM(Amount), '0.00') FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND it.TxnType='Void' AND it.ResponseCode IN ('000', '00'))) AS 'td', '',
								  CONCAT('$',(SELECT ISNULL(SUM(Amount), '0.00') FROM  @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND it.TxnType IN ('Sale', 'Completion') AND it.IsRefunded = 0 AND IT.IsVoided = 0 AND it.ResponseCode IN ('000', '00', '002'))) AS 'td', '',
								  CONCAT('$',(SELECT ISNULL(SUM(Fee), '0.00') + ISNULL(SUM(FleetFee), '0.00') FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td', '',
							      CONCAT('$',(SELECT ISNULL(SUM(Amount), '0.00') FROM  @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND it.TxnType IN ('Sale', 'Completion') AND it.IsRefunded = 0 AND IT.IsVoided = 0 AND it.ResponseCode IN ('000', '00', '002')) - 
										(SELECT ISNULL(SUM(Fee), '0.00') + ISNULL(SUM(FleetFee), '0.00') FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td', '',
								  (SELECT COALESCE(COUNT(t.VehicleNo), 0) FROM #temp t WHERE t.fk_FleetId = vh.FleetID AND t.Id<@TransCountSlablt1) as 'td', '',
							      (SELECT COALESCE(COUNT(t.VehicleNo), 0)  FROM #temp t WHERE t.fk_FleetId = vh.FleetID AND t.Id>=@TransCountSlabGt2 AND t.Id<@TransCountSlablt2) as 'td', '',
							      (SELECT COALESCE(COUNT(t.VehicleNo), 0)  FROM #temp t WHERE t.fk_FleetId = vh.FleetID AND t.Id>=@TransCountSlabGt3 AND t.Id<@TransCountSlablt3) as 'td', '',
							      (SELECT COALESCE(COUNT(t.VehicleNo), 0)  FROM #temp t WHERE t.fk_FleetId = vh.FleetID AND t.Id>=@TransCountSlabGt4) as 'td'
                        FROM dbo.MTDTransaction m 
							INNER JOIN (SELECT v.VehicleNumber AS 'VehicleNumber', v.fk_FleetID AS 'fk_FleetID', f.FleetID AS 'FleetID', f.FleetName AS 'FleetName', mer.Company AS 'Company'
										FROM (SELECT VehicleNumber, fk_FleetID FROM dbo.Vehicle WHERE IsActive = 1) v
											INNER JOIN (SELECT FleetID, FleetName, fk_MerchantID FROM dbo.Fleet WHERE IsActive = 1) f ON v.fk_FleetID = f.FleetID
											INNER JOIN (SELECT MerchantID, Company FROM dbo.Merchant WHERE IsActive = 1) mer ON f.fk_MerchantID = mer.MerchantID 
										WHERE mer.MerchantID = @MerchantId) vh ON m.VehicleNo = vh.VehicleNumber 
						WHERE m.ResponseCode IN ('000', '00', '002') 
						  AND CAST(m.TxnDate AS DATE) >= @DateFrom 
						  AND CAST(m.TxnDate AS DATE) < @DateTo 
						  AND m.fk_FleetId = vh.fk_FleetID
						  AND m.Id NOT IN (SELECT Id FROM dbo.MTDTransaction WHERE m.TxnType = 'Authorization' AND m.IsCompleted = 0)
						GROUP BY vh.FleetName, vh.FleetID, vh.Company
						ORDER BY FleetName
						FOR XML PATH('tr'), ELEMENTS ) AS NVARCHAR(MAX))

		SELECT @TotalTransaction = ISNULL(COUNT(ot.Id), 0) 
		FROM @OuterTable ot 
		WHERE ot.TxnType IN ('Sale', 'Completion') 
		  AND ot.ResponseCode IN ('000', '00', '002')
		  AND ot.IsRefunded = 0 
		  AND ot.IsVoided = 0

		SELECT  @TotalActualFare = CONCAT('$', CAST(ISNULL(SUM(ot.FareValue), '0.00') AS DECIMAL(18, 2))),
				@totalTips = CONCAT('$', CAST(ISNULL(SUM(ot.Tip), '0.00') AS DECIMAL(18, 2))),
				@TotalSurcharge = CONCAT('$', CAST(ISNULL(SUM(ot.Surcharge), '0.00') AS DECIMAL(18, 2))),
				@TotalFee = CAST(ISNULL(SUM(ot.Fee), '0.00') + ISNULL(SUM(ot.FleetFee), '0.00') AS DECIMAL(18, 2)),
				@TotalTechFee = CAST(ISNULL(SUM(ot.TechFee), '0.00') AS DECIMAL(18, 2))
		FROM @OuterTable ot 
		WHERE ot.ResponseCode IN ('000', '00', '002') 
		  AND ((ot.TxnType = 'Sale' AND ot.IsRefunded = 0 AND ot.IsVoided = 0) 
				OR (ot.TxnType = 'Authorization' AND ot.IsCompleted = 1))

		SELECT  @TotalFareAmount = CONCAT('$', CAST(ISNULL(SUM(ot.Amount), '0.00') AS DECIMAL(16, 2))),
				@AmountOwing = CONCAT('$', CAST(ISNULL(SUM(ot.Amount), '0.00') AS DECIMAL(18, 2)) - CAST(@TotalFee AS DECIMAL(18, 2)))
		FROM @OuterTable ot
		WHERE ot.ResponseCode IN ('000', '00', '002') 
		  AND ot.TxnType IN ('Sale', 'Completion') 
		  AND ot.IsRefunded = 0 
		  AND ot.IsVoided = 0 

		SELECT  @NoChargeBack = ISNULL(COUNT(ot.Id), '0'),
				@ChargeBackValue = CONCAT('$', CAST(ISNULL(SUM(ot.Amount), '0.00') AS DECIMAL(18, 2))) 
		FROM @OuterTable ot 
		WHERE ot.TxnType = 'Refund'
		  AND ot.ResponseCode IN ('000', '00')

		SELECT  @NoVoid = ISNULL(COUNT(Id), '0'),
				@VoidValue = CONCAT('$', CAST(ISNULL(SUM(Amount), '0.00') AS DECIMAL(18, 2))) 
		FROM @OuterTable ot 
		WHERE ot.TxnType = 'Void'
		  AND ot.ResponseCode IN ('000', '00')

		SET @ReportType = @Frequency
		SET @FDate = @DateFrom
		SET @TDate = DATEADD(DAY, -1, @DateTo)
		SET @TotalTechFee = CONCAT('$', @TotalTechFee)
		SET @Slab1Message = CONCAT('Trans less than ', @TransCountSlablt1)
		SET @Slab2Message = CONCAT('Trans between ', @TransCountSlabGt2, ' and ', @TransCountSlablt2) 
		SET @Slab3Message = CONCAT('Trans between ', @TransCountSlabGt3, ' and ', @TransCountSlablt3) 
		SET @Slab4Message = CONCAT('Trans greater than ', @TransCountSlabGt4)

		SELECT @TotalSlab1 = COUNT(t.VehicleNo) FROM #temp t WHERE t.Id < @TransCountSlablt1
		SELECT @TotalSlab2 = COUNT(t.VehicleNo) FROM #temp t WHERE t.Id >= @TransCountSlabGt2 AND t.Id < @TransCountSlablt2
		SELECT @TotalSlab3 = COUNT(t.VehicleNo) FROM #temp t WHERE t.Id >= @TransCountSlabGt3 AND t.Id < @TransCountSlablt3
		SELECT @TotalSlab4 = COUNT(t.VehicleNo) FROM #temp t WHERE t.Id >= @TransCountSlabGt4

		SET @TotalFee = CONCAT('$', @TotalFee)

		DROP TABLE #temp
	END TRY
	BEGIN CATCH
		INSERT INTO dbo.ProcErrorHandler (ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, UserName, HostName, [TimeStamp])
		SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
	END CATCH
END
GO
