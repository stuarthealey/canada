﻿using System.Collections.Generic;
using MTD.Core.DataTransferObjects;
namespace MTD.Core.Service.Interface
{
    public interface IMerchantGatewayService
    {
        bool Add(MerchantGatewayDto merchantGatewayDto);

        bool Update(MerchantGatewayDto merchantGatewayDto, int merchantGatewayId);

        IEnumerable<MerchantGatewayDto> GetMerchantGatewayDtoList(int merchantId);

        MerchantGatewayDto GetMerchantGatewayDto(int merchantGatewayId);

        void DeleteMerchantGateway(int merchantGatewayId);

        IEnumerable<MerchantGatewayDto> MerchantGateListByFilter(FilterSearchParametersDto filterSearch);

    }
}
