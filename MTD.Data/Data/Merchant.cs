//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MTD.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Merchant
    {
        public Merchant()
        {
            this.CarOwners = new HashSet<CarOwner>();
            this.CCPrefixes = new HashSet<CCPrefix>();
            this.Datawires = new HashSet<Datawire>();
            this.Fleets = new HashSet<Fleet>();
            this.Fleets1 = new HashSet<Fleet>();
            this.Fleets2 = new HashSet<Fleet>();
            this.LogConfigurations = new HashSet<LogConfiguration>();
            this.LogConfigurations1 = new HashSet<LogConfiguration>();
            this.LogConfigurations2 = new HashSet<LogConfiguration>();
            this.Merchant_Currency = new HashSet<Merchant_Currency>();
            this.Merchant_Gateway = new HashSet<Merchant_Gateway>();
            this.Merchant_Currency1 = new HashSet<Merchant_Currency>();
            this.Merchant_Currency2 = new HashSet<Merchant_Currency>();
            this.Recipients = new HashSet<Recipient>();
            this.ScheduleReports = new HashSet<ScheduleReport>();
            this.Terminals = new HashSet<Terminal>();
            this.Terminals1 = new HashSet<Terminal>();
            this.TransactionCountSlabs = new HashSet<TransactionCountSlab>();
            this.UserFleets = new HashSet<UserFleet>();
            this.UserMerchants = new HashSet<UserMerchant>();
            this.Users = new HashSet<User>();
            this.UserFleets1 = new HashSet<UserFleet>();
            this.UserFleets2 = new HashSet<UserFleet>();
            this.Users1 = new HashSet<User>();
        }
    
        public int MerchantID { get; set; }
        public string Company { get; set; }
        public Nullable<decimal> SetupFee { get; set; }
        public Nullable<decimal> MonthlyFee { get; set; }
        public string Disclaimer { get; set; }
        public string TermCondition { get; set; }
        public Nullable<decimal> StateTaxRate { get; set; }
        public Nullable<decimal> FederalTaxRate { get; set; }
        public Nullable<decimal> TipPerLow { get; set; }
        public Nullable<decimal> TipPerMedium { get; set; }
        public Nullable<decimal> TipPerHigh { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> IsAllowSale { get; set; }
        public Nullable<bool> IsAllowPreauth { get; set; }
        public Nullable<bool> IsAllowCapture { get; set; }
        public Nullable<bool> IsAllowRefund { get; set; }
        public Nullable<bool> IsAllowVoid { get; set; }
        public string CompanyTaxNumber { get; set; }
        public Nullable<bool> IsTaxInclusive { get; set; }
        public Nullable<int> Fk_FDId { get; set; }
        public string DisclaimerPlainText { get; set; }
        public string TermConditionPlainText { get; set; }
        public Nullable<bool> IsJobNoRequired { get; set; }
        public string TimeZone { get; set; }
        public string ZipCode { get; set; }
        public Nullable<bool> UseTransArmor { get; set; }   //PAY-13

        public virtual ICollection<CarOwner> CarOwners { get; set; }
        public virtual ICollection<CCPrefix> CCPrefixes { get; set; }
        public virtual ICollection<Datawire> Datawires { get; set; }
        public virtual FDMerchant FDMerchant { get; set; }
        public virtual ICollection<Fleet> Fleets { get; set; }
        public virtual ICollection<Fleet> Fleets1 { get; set; }
        public virtual ICollection<Fleet> Fleets2 { get; set; }
        public virtual ICollection<LogConfiguration> LogConfigurations { get; set; }
        public virtual ICollection<LogConfiguration> LogConfigurations1 { get; set; }
        public virtual ICollection<LogConfiguration> LogConfigurations2 { get; set; }
        public virtual ICollection<Merchant_Currency> Merchant_Currency { get; set; }
        public virtual ICollection<Merchant_Gateway> Merchant_Gateway { get; set; }
        public virtual ICollection<Merchant_Currency> Merchant_Currency1 { get; set; }
        public virtual ICollection<Merchant_Currency> Merchant_Currency2 { get; set; }
        public virtual ICollection<Recipient> Recipients { get; set; }
        public virtual ICollection<ScheduleReport> ScheduleReports { get; set; }
        public virtual ICollection<Terminal> Terminals { get; set; }
        public virtual ICollection<Terminal> Terminals1 { get; set; }
        public virtual ICollection<TransactionCountSlab> TransactionCountSlabs { get; set; }
        public virtual ICollection<UserFleet> UserFleets { get; set; }
        public virtual ICollection<UserMerchant> UserMerchants { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<UserFleet> UserFleets1 { get; set; }
        public virtual ICollection<UserFleet> UserFleets2 { get; set; }
        public virtual ICollection<User> Users1 { get; set; }
    }
}
