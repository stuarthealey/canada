﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    class Table32
    {
        #region Private Constants and Variables
        
        #region Constants
        private const string tblId = "|33|32";
       
        #endregion

        #region Variables
        
        //All these variables belongs to SMID – Security Management Information Data.
        private string mac = "|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20";
        private string msgAuthenticate ="|20|20|20|20";
        
        #endregion

        #endregion

        #region Public Properties
        
        /// <summary>
        /// ID of Table
        /// default X'33 32' Table 32
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }
              
        public string MAC
        {
            get { return mac; }
            set { mac = value; }
        }
         
        public string MsgAuthenticate
        {
            get { return msgAuthenticate; }
            set { msgAuthenticate = value; }
        }
       
        #endregion
    }
}