﻿namespace MTData.WebAPI.CnPrivateBit63
{
    public class TableSP_KeyUpdate
    {
        #region Constants

        private const string tblId = "|53|50";
        
        #endregion

        #region Variables

        private const string securityLevelTag = "|30|31";
        private const string securityLevelTagDataLength = "|30|30|31";
        private const string securityLevelTagValue = "|31";

        private const string encryptionTypeTag = "|30|32";
        private const string encryptionTypeTagDataLength = "|30|30|31";
        private const string encryptionTypeTagValue = "|31";

        private const string scrtyUpdateIndTag = "|30|38";
        private const string scrtyUpdateIndTagDataLength = "|30|30|31";
        private const string scrtyUpdateIndTagValue = "|55";

        private const string signingKeyIdTag = "|31|32";
        private const string signingKeyIdTagDataLength = "|30|30|32";
        private const string signingKeyIdTagValue = "|54|31";

        #endregion

        #region Public Properties

        /// <summary>
        /// ID of Table
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        public string SecurityLevelTag
        {
            get { return securityLevelTag; }
        }

        public string SecurityLevelTagDataLength
        {
            get { return securityLevelTagDataLength; }
        }

        /// <summary>
        /// Gets or Sets the value for DCC Transaction Time Zone
        /// </summary>
        public string SecurityLevelTagValue
        {
            get { return securityLevelTagValue; }
        }

        /// <summary>
        /// Gets or Sets the value for Foreign Conversion Rate
        /// </summary>
        public string EncryptionTypeTag
        {
            get { return encryptionTypeTag; }
        }

        public string EncryptionTypeTagDataLength
        {
            get { return encryptionTypeTagDataLength; }
        }

        /// <summary>
        /// Gets or Sets the value for Amount in Merchant Base Currency
        /// </summary>
        public string EncryptionTypeTagValue
        {
            get { return encryptionTypeTagValue; }
        }

        public string ScrtyUpdateIndTag
        {
            get { return scrtyUpdateIndTag; }
        }

        public string ScrtyUpdateIndTagDataLength
        {
            get { return scrtyUpdateIndTagDataLength; }
        }

        /// <summary>
        /// Gets or Sets the value for Amount in Merchant Base Currency
        /// </summary>
        public string ScrtyUpdateIndTagValue
        {
            get { return scrtyUpdateIndTagValue; }
        }

        /// <summary>
        /// Gets or Sets the signing key tag
        /// </summary>
        public string SigningKeyIdTag
        {
            get { return signingKeyIdTag; }
        }

        /// <summary>
        /// Gets or Sets the length of signing key
        /// </summary>
        public string SigningKeyIdTagDataLength
        {
            get { return signingKeyIdTagDataLength; }
        }

        /// <summary>
        /// Gets or Sets the value of signing key id
        /// </summary>
        public string SigningKeyIdTagValue
        {
            get { return signingKeyIdTagValue; }
        }

        #endregion
    }
}