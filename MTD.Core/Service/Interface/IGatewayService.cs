﻿using System.Collections.Generic;
using MTD.Core.DataTransferObjects;

namespace MTD.Core.Service.Interface
{
    public interface IGatewayService
    {
        bool Create(GatewayDto gatewayDto);

        bool Update(GatewayDto gatewayDto);

        IEnumerable<GatewayDto> GetGatewaysList();

        void DeleteGateway(int gatewayId);

        GatewayDto GetGateway(int gatewayId);

        IEnumerable<CurrencyDto> GetGatewayCurrency();

        IEnumerable<GatewayDto> GetGatewayByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting);
         int GatewaysListCount(string name);

    }
}
