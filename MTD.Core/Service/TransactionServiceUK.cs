﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTD.Data.Repository.Interface;
using Ninject;
using MTD.Core.Extension;

namespace MTD.Core.Service
{
    public class TransactionServiceUK: ITransactionServiceUK
    {
        private readonly ITransactionRepositoryUK _transactionReporsitoryuk;

        public TransactionServiceUK([Named("UkTxnRepository")] ITransactionRepositoryUK transactionReporsitoryuk)
        {
            _transactionReporsitoryuk = transactionReporsitoryuk;
        }
     
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mtdUK"></param>
        /// <returns></returns>
        public int AddSemiTxn(MtdtxnSemiUKDto mtdUK, MTDTransactionDto UsmtdDto)
        {
            var ukTxn = mtdUK.ToSemiIntigratedTxn();
            var uSTxn = UsmtdDto.ToMtdTransaction();
            int res = _transactionReporsitoryuk.AddSemiTxn(ukTxn, uSTxn);
            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mtdUK"></param>
        /// <returns></returns>
        public int AddSemiSattlementTxn(MtdtxnSattlementUKDto mtdUK)
        {
            var capk = mtdUK.ToSemiSattelementTxn();
            int res = _transactionReporsitoryuk.AddSemiSattlementTxn(capk);
            return res;
        }
    }
}
