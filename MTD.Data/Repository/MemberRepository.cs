﻿using System;
using System.Linq;

using MTD.Data.Data;
using MTD.Data.Repository.Interface;
using MTData.Utility;

namespace MTD.Data.Repository
{
    public class MemberRepository : BaseRepository, IMemberRepository
    {
        public MemberRepository(IMtDataDevlopmentsEntities dbMtData) : base(dbMtData)
        {
        }

        /// <summary>
        /// Purpose             :   To create login
        /// Function Name       :   CreateLogin
        /// Created By          :   Salil Gupta
        /// Created On          :   02/03/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        public SessionMerchantUser CreateLogin(Login loginModel)
        {
            try
            {
                string passCode = (from m in DbMtData.Users
                                    where m.Email == loginModel.UserName && m.IsActive && m.IsLockedOut==false
                                    select m.PCode).FirstOrDefault();

                bool isValidUser = StringExtension.ValidatePassCode(loginModel.PCode, passCode);
                var smu = new SessionMerchantUser();
                if (!isValidUser)
                {
                    var md = new MerchantDetails();
                    smu.SessionMerchant = md;
                    smu.SessionUser = new User();
                    return smu; 
                }

                var sUser =DbMtData.Users.FirstOrDefault(a => a.Email.Equals(loginModel.UserName) && a.PCode.Equals(passCode) && a.IsActive && (bool)!a.IsLockedOut);                

                if (sUser == null)
                {
                    var md = new MerchantDetails();
                    smu.SessionMerchant = md;
                    smu.SessionUser = new User();
                    return smu;
                }

                sUser.IsTempLocked = false;
                sUser.TempLockedTimePeriod = null;
                sUser.FailedAttemptTime = null;
                sUser.CountFailedAttempt = null;
                DbMtData.CommitChanges();

                if (sUser.fk_UserTypeID!=1)
                {
                    var merchantGateway = DbMtData.Merchant_Gateway.FirstOrDefault(a => a.fk_MerchantID == sUser.fk_MerchantID);
                    var terminal = DbMtData.Terminals.FirstOrDefault(a => a.fk_MerchantID== sUser.fk_MerchantID);
                    var merchant = DbMtData.Merchants.FirstOrDefault(a => a.MerchantID ==sUser.fk_MerchantID);
                    var gateway = new Gateway();

                    if (merchantGateway != null)
                    {
                        int gatewayId = merchantGateway.fk_GatewayID;
                        gateway = DbMtData.Gateways.FirstOrDefault(a => a.GatewayID==gatewayId);
                    }

                    var merchantDetails = AssignDetails.AssignProperties(gateway, merchantGateway, terminal, merchant, sUser);
                    return merchantDetails;
                }

                smu.SessionUser = sUser;
                var mdt=new MerchantDetails();
                smu.SessionMerchant = mdt;

                return smu;
            }
            catch
            {
                throw;
            }           
        }

        /// <summary>
        /// Purpose             :   To update password
        /// Function Name       :   UpdatePassword
        /// Created By          :   Salil Gupta
        /// Created On          :   02/03/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        public int UpdatePassword(Login loginModel)
        {
            try
            {
                int uId = Convert.ToInt32(loginModel.UserName);
                var userData = (from m in DbMtData.Users
                                where m.UserID == uId && m.IsActive && m.IsLockedOut == false
                                select m).FirstOrDefault();

                if (userData == null)
                    return 1;

                bool isValidUser = StringExtension.ValidatePassCode(loginModel.oldPCode, userData.PCode);
                if (!isValidUser)
                    return 2;

                string[] arrayCode = new string[5];
                arrayCode[0] = userData.PCode;
                arrayCode[1] = userData.FirstPCode;
                arrayCode[2] = userData.SecondPCode;
                arrayCode[3] = userData.ThirdPCode;
                arrayCode[4] = userData.FourthPCode;

                for (int index = 0; index < 5; index++)
                {
                    bool isSamePassCode = StringExtension.ValidatePassCode(loginModel.PCode, arrayCode[index]);
                    if (isSamePassCode)
                        return 3;
                }

                loginModel.PCode = loginModel.PCode.Hash();
                userData.FourthPCode = userData.ThirdPCode;
                userData.ThirdPCode = userData.SecondPCode;
                userData.SecondPCode = userData.FirstPCode;
                userData.FirstPCode = userData.PCode;
                userData.PCode = loginModel.PCode;
                userData.IsFirstLogin = false;
                userData.PCodeExpDate = DateTime.Now.AddDays(90);
                DbMtData.CommitChanges();

                return 4;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To make login as Admin
        /// Function Name       :   LoginAsAdmin
        /// Created By          :   Salil Gupta
        /// Created On          :   02/03/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        public SessionMerchantUser LoginAsAdmin(Login loginModel)
        {
            try
            {
                var smu = new SessionMerchantUser();
                int uid =Convert.ToInt32(loginModel.UserName);
                var sUser = DbMtData.Users.FirstOrDefault(a => a.UserID.Equals(uid) && a.Email.Equals(loginModel.PCode));

                if (sUser == null)
                {
                    var md = new MerchantDetails();
                    smu.SessionMerchant = md;
                    smu.SessionUser = new User();
                    return smu;
                }

                if (sUser.fk_UserTypeID != 1)
                {
                    var merchantGateway = DbMtData.Merchant_Gateway.FirstOrDefault(a => a.fk_MerchantID == sUser.fk_MerchantID);
                    var terminal = DbMtData.Terminals.FirstOrDefault(a => a.fk_MerchantID == sUser.fk_MerchantID);
                    var merchant = DbMtData.Merchants.FirstOrDefault(a => a.MerchantID == sUser.fk_MerchantID);
                     
                    var gateway = new Gateway();
                    if (merchantGateway != null)
                    {
                        int gatewayId = merchantGateway.fk_GatewayID;
                        gateway = DbMtData.Gateways.FirstOrDefault(a => a.GatewayID == gatewayId);
                    }

                    var merchantDetails = AssignDetails.AssignProperties(gateway, merchantGateway, terminal, merchant, sUser);
                    return merchantDetails;
                }

                smu.SessionUser = sUser;
                var mdt = new MerchantDetails();
                smu.SessionMerchant = mdt;

                return smu;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get password
        /// Function Name       :   GetPassword
        /// Created By          :   Salil Gupta
        /// Created On          :   02/03/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="emailId"></param>
        /// <returns></returns>
        public User GetPassword(string emailId)
        {
            var userDb = DbMtData.Users.FirstOrDefault(a => a.Email.Equals(emailId));
            return userDb;
        }

        /// <summary>
        /// Purpose             :   To reset password
        /// Function Name       :   Reset
        /// Created By          :   Salil Gupta
        /// Created On          :   02/03/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="email"></param>
        /// <param name="passwd"></param>
        /// <returns></returns>
        public bool Reset(string email, string passwd)
        {
            try
            {
                User userDb = DbMtData.Users.FirstOrDefault(s => s.Email == email && s.IsActive && !(bool)s.IsLockedOut);
                if (userDb != null)
                {
                    userDb.FourthPCode = userDb.ThirdPCode;
                    userDb.ThirdPCode = userDb.SecondPCode;
                    userDb.SecondPCode = userDb.FirstPCode;
                    userDb.FirstPCode = userDb.PCode;
                    userDb.PCode = passwd;
                    userDb.PCodeExpDate = DateTime.Now.AddDays(90);
                    DbMtData.CommitChanges();

                    return true;
                }

                return false;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool PassCodeExpiration(string email)
        {
            DateTime? passCodeExpDate = (from m in DbMtData.Users where m.Email == email && m.IsActive select m.PCodeExpDate).FirstOrDefault();
            if (DateTime.Now >= passCodeExpDate)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public int ResetExpiredPassword(Login login)
        {
            int uId = Convert.ToInt32(login.UserName);
            var userDb = DbMtData.Users.FirstOrDefault(
                           a => a.UserID == uId);

            if (userDb == null)
                return 1;

            bool isValidPassCode = StringExtension.ValidatePassCode(login.oldPCode, userDb.PCode);
            if (!isValidPassCode)
                return 2;

            string[] arrayPassCode = new string[5];
            arrayPassCode[0] = userDb.PCode;
            arrayPassCode[1] = userDb.FirstPCode;
            arrayPassCode[2] = userDb.SecondPCode;
            arrayPassCode[3] = userDb.ThirdPCode;
            arrayPassCode[4] = userDb.FourthPCode;
            
            for (int index = 0; index < 5; index++)
            {
                bool isSamePassCode = StringExtension.ValidatePassCode(login.PCode, arrayPassCode[index]);
                if (isSamePassCode)
                    return 3;
            }

            login.PCode = login.PCode.Hash();       
            userDb.FourthPCode = userDb.ThirdPCode;
            userDb.ThirdPCode = userDb.SecondPCode;
            userDb.SecondPCode = userDb.FirstPCode;
            userDb.FirstPCode = userDb.PCode;
            userDb.PCode = login.PCode;
            userDb.PCodeExpDate = DateTime.Now.AddDays(90);
            DbMtData.CommitChanges();

            return 4;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool IsLocked(string email)
        {
            var userDb = (from m in DbMtData.Users where m.Email == email && m.IsActive &&m.IsLockedOut==false select m).FirstOrDefault();
            
            if (userDb != null)
            {
                if (userDb.IsTempLocked == true && DateTime.Now <= userDb.TempLockedTimePeriod)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool CountFailedAttempt(string email)
        {
            User userDb = (from m in DbMtData.Users where m.Email == email && m.IsActive && m.IsLockedOut==false select m).FirstOrDefault();
            if (userDb != null)
            {
                double minutes = 0;
                TimeSpan? ts = (DateTime.Now - userDb.FailedAttemptTime);
                if (ts != null)
                    minutes = ts.Value.TotalMinutes;                            
         
                if (userDb.CountFailedAttempt >= 1 && userDb.CountFailedAttempt <= 2 && minutes <= 30 && minutes>0)
                {
                    userDb.CountFailedAttempt = userDb.CountFailedAttempt + 1;
                }
                else
                {
                    userDb.FailedAttemptTime = DateTime.Now;
                    userDb.CountFailedAttempt = 1;
                    userDb.IsTempLocked = false;
                    userDb.TempLockedTimePeriod = null;
                }

                if (userDb.CountFailedAttempt == 3)
                {
                    userDb.IsTempLocked = true;
                    userDb.TempLockedTimePeriod = DateTime.Now.AddMinutes(30);
                    DbMtData.CommitChanges();
                    return true;
                }

                DbMtData.CommitChanges();
            }

            return false;
        }

    }
}
