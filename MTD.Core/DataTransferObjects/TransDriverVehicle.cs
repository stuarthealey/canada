﻿namespace MTD.Core.DataTransferObjects
{
   public class TransDriverVehicleDto
    {
       public string VehicleNumber { get; set; }
       public string DriverNumber { get; set; }
    }
}
