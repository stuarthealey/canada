IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DriverSummaryReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_DriverSummaryReport]
GO

CREATE PROCEDURE dbo.usp_DriverSummaryReport
-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =  = 
-- Author:		<Naveen Kumar>
-- Create date: <14-07-2015>
-- Description:	<Prepare and send Daily, weekly and monthly summary reports to drivers as scheduled>
-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =  = 
--	S.Healey	2016-06-06		- Changed company name reference to MTData, LLC
--
(
	@ReportType INT     --1 is for weekly report, 2 is for monthly report                  
)
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON 
		SET ANSI_WARNINGS OFF;
		DECLARE	@EndDateTime DATETIME, 
				@BaseLogInTime DATETIME, 
				@BaseLogOutTime DATETIME, 
				@LogInDateTime DATETIME, 
				@LogOutDateTime DATETIME, 
				@DateFrom DATETIME, 
				@DateTo DATETIME, 
				@DriverNo NVARCHAR(50), 
				@DriverName NVARCHAR(50), 
				@DriverLName NVARCHAR(50), 
				@DriverEmail NVARCHAR(50), 
				@DateCommenced NVARCHAR(80), 
				@ReportName NVARCHAR(80), 
				@XmlData NVARCHAR(MAX), 
				@MsgXml VARCHAR(50), 
				@Body NVARCHAR(MAX), 
				@Error NVARCHAR(200), 
				@DailyReportHead NVARCHAR(200), 
				@InnerData NVARCHAR(MAX), 
				@TotalFareValue VARCHAR(30), 
				@TotalTip VARCHAR(30), 
				@TotalTax VARCHAR(30), 
				@TotalAmt VARCHAR(30), 
				@TotalFee VARCHAR(30), 
				@NetAmt VARCHAR(30), 
				@IsOwnerRecipient BIT, 
				@NoFares INT, 
				@MailId INT, 
				@Valid INT, 
				@Count  INT, 
				@Index INT, 
				@DriverId INT, 
				@FleetId INT, 
				@Res INT

		DECLARE @DriverRecTable TABLE(fk_DriverId INT, fk_FleetId INT)
		IF (@ReportType = 1)
		BEGIN
			SET @DateFrom = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK, -1, GETDATE())), 0) - 1
			SET @DateTo = DATEADD(WEEK, DATEDIFF(WEEK, 0, GETDATE()), 0) - 1
			SET @ReportName = 'Driver Weekly Report'

			INSERT INTO @DriverRecTable 
			SELECT fk_DriverId, fk_FleetId FROM dbo.DriverReportRecipient WHERE IsWeekly = 1 AND IsRecipient = 1

			SET @DateCommenced = 'Date Range : ' + CONVERT(VARCHAR(11), @DateFrom, 106) + ' To ' + CONVERT(VARCHAR(11), @DateTo - 1, 106) + '' 
		END
		ELSE IF (@ReportType = 2)
		BEGIN
			SET @DateFrom = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH, -1, GETDATE())), 0)
			SET @DateTo = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) 
			SET @ReportName = 'Driver Monthly Report'

			INSERT INTO @DriverRecTable 
			SELECT fk_DriverId, fk_FleetId FROM dbo.DriverReportRecipient WHERE IsMonthly = 1 AND IsRecipient = 1

			SET @DateCommenced = 'Month : ' + DATENAME(mm, @DateFrom) + ' ' + DATENAME(yy, @DateFrom) + ''
		END
		ELSE
		BEGIN
			RETURN 3
		END

		--Declaring cursor for fetching driver recipients one by one for summary report
		DECLARE Queue_Cursor CURSOR FOR SELECT d.DriverID, d.DriverNo, d.FName, d.LName, d.Email, d.fk_FleetID 
										FROM dbo.Driver d 
										WHERE d.DriverID IN (SELECT DISTINCT dr.fk_DriverId FROM @DriverRecTable dr) 
										  AND d.IsActive = 1

		OPEN Queue_Cursor 
		FETCH NEXT FROM Queue_Cursor INTO @DriverId, @DriverNo, @DriverName, @DriverLName, @DriverEmail, @FleetId
		WHILE @@FETCH_STATUS = 0  
		BEGIN  
			SET @IsOwnerRecipient = (SELECT TOP 1 IsOwnerRecipient  FROM DriverReportRecipient WHERE fk_DriverId = @DriverId)
			DECLARE @TableData TABLE(Id INT IDENTITY(1, 1) PRIMARY KEY, LogInTime DATETIME, LogOutTime DATETIME)
			SET @XmlData = ''
			INSERT INTO @TableData(LogInTime, LogOutTime)
			SELECT LogOnDateTime, LogOffDateTime FROM DriverTransactionHistory dh 
			WHERE dh.DriverNo =  @DriverNo 
			  AND FleetId = @FleetId 
			  AND LogOnDateTime > @DateFrom - 5
			  AND LogOnDateTime < @DateTo 
			  AND LogOffDateTime > @DateFrom 
			  AND LogOffDateTime < @DateTo 
			ORDER BY LogOnDateTime 

			SELECT @Count = COUNT(Id) FROM @TableData
			SET @Index = 1

			WHILE (@Index< = @Count)
			BEGIN
				SELECT @LogInDateTime = LogInTime, @LogOutDateTime = LogOutTime FROM @TableData WHERE Id = @Index

				SET @XmlData = @XmlData + COALESCE(CAST((SELECT @Index AS 'td', '', 
															COALESCE(CONVERT(VARCHAR(17), @LogInDateTime, 113), '') AS 'td', '', 
															COALESCE(CONVERT(VARCHAR(17), @LogOutDateTime, 113), '') AS 'td', '', 
															COALESCE(MAX(VehicleNo), '') AS 'td', '', 
															COALESCE(COUNT(Id), 0) AS 'td', '', 
															CONCAT('$', CAST(COALESCE(SUM(FareValue), 0.00) AS DECIMAL(18, 2)))  AS 'td', '', 
															CONCAT('$', CAST(COALESCE(SUM(Tip), 0.00) AS DECIMAL(18, 2)))  AS 'td', '', 
															CONCAT('$', CAST(COALESCE(SUM(Taxes), 0.00) AS DECIMAL(18, 2)))  AS 'td', '', 
															CONCAT('$', CAST(COALESCE(SUM(Amount), 0.00) AS DECIMAL(18, 2)))  AS 'td', '', 
															CONCAT('$', CAST(COALESCE(SUM(Fee), 0.00) AS DECIMAL(18, 2)))  AS 'td', '', 
															CONCAT('$', CAST(COALESCE(SUM(Amount), 0.00)-COALESCE(SUM(Fee), 0.00) AS DECIMAL(18, 2))) AS 'td' 
														FROM dbo.MTDTransaction 
														WHERE DriverNo = @DriverNo 
														  AND fk_FleetId = @FleetId
														  AND TxnDate >= @LogInDateTime AND TxnDate <= @LogOutDateTime 
														  AND ResponseCode IN ('000', '00', '002')
										FOR XML PATH('tr'), ELEMENTS) AS NVARCHAR(MAX)), '')
				SET @Index = @Index+1
			END

			DELETE @TableData

			IF (@XmlData = '' OR @XmlData IS NULL)
			BEGIN
				SET @Valid = 0
				INSERT INTO dbo.MailMessage(RepUserId, [Status], SendTo, Body, RepUserTypeId, [Date], ReportType) 
				VALUES (@DriverId, -1, @DriverEmail, 'No records in database', 4, GETDATE(), 'DriverSummaryReport')
			END
			ELSE 
			BEGIN
				SET @Valid = 1
			END

			IF (@Valid = 1 AND @DriverEmail IS NOT NULL) 
			BEGIN
				SELECT @BaseLogInTime = MIN(LogOnDateTime), @BaseLogOutTime = MAX(LogOffDateTime) 
				FROM dbo.DriverTransactionHistory 
				WHERE DriverNo = @DriverNo 
				  AND FleetId = @FleetId 
				  AND LogOnDateTime > @DateFrom - 5 
				  AND LogOnDateTime < @DateTo 
				  AND LogOffDateTime > @DateFrom 
				  AND LogOffDateTime < @DateTo 

				SELECT	@NoFares = COALESCE(COUNT(Id), 0), 
						@TotalFareValue = CONCAT('$', CAST(COALESCE(SUM(FareValue), 0.00) AS DECIMAL(18, 2))), 
						@TotalTip = CONCAT('$', CAST(COALESCE(SUM(Tip), 0.00) AS DECIMAL(18, 2))), 
						@TotalTax = CONCAT('$', CAST(COALESCE(SUM(Taxes), 0.00) AS DECIMAL(18, 2))), 
						@TotalAmt = CONCAT('$', CAST(COALESCE(SUM(Amount), 0.00) AS DECIMAL(18, 2))), 
						@TotalFee = CONCAT('$', CAST(COALESCE(SUM(Fee), 0.00) AS DECIMAL(18, 2))), 
						@NetAmt = CONCAT('$', CAST((COALESCE(SUM(Amount), 0.00) - COALESCE(SUM(Fee), 0.00)) AS DECIMAL(18, 2)))
				FROM dbo.MTDTransaction  
				WHERE DriverNo = @DriverNo 
				  AND fk_FleetId = @FleetId
				  AND TxnDate >= @BaseLogInTime 
				  AND TxnDate <= @BaseLogOutTime 
				  AND ResponseCode IN ('000', '00', '002')

				SET @MsgXml = CAST((SELECT CONCAT('Dear', ' ', @DriverName, ', ') AS 'p' FOR XML PATH('p'), ELEMENTS ) AS NVARCHAR(MAX))

				SET @Body  = '<html><style>td{text-align:left;font-family:verdana;font-size:12px}
							p{font-family:verdana;font-size:13px}span{font-family:verdana;font-size:13px}</style>
							<body><p>Listed below is the driver transaction report containing summarized details of transactions done in below mentioned shifts.</p>
							<H5 style = "color:black;text-align:center;font-size:11px;font-family:verdana"> ' + @ReportName + '</H5>
							<H5 style = "color:grey;text-align:center;font-size:11px;font-family:verdana">Driver No : ' + @DriverNo + '&nbsp;&nbsp;&nbsp;&nbsp;Driver Name : ' + @DriverName + ' ' + @DriverLName + '&nbsp;&nbsp;&nbsp;&nbsp;' + @DateCommenced + '</H5>
							<table border = 1 cellpadding = "0" cellspacing = "0" style = "background-color:white;border-collapse:collapse"> <tr>
							<th style = "color:grey;font-size:13px;width:220px;text-align:left">Seq No</th>
							<th style = "color:grey;font-size:13px;width:750px;text-align:left">Logon Date/Time</th>
							<th style = "color:grey;font-size:13px;width:750px;text-align:left">Logoff Date/Time</th>
							<th style = "color:grey;font-size:13px;width:500px;text-align:left">Car Number</th>
							<th style = "color:grey;font-size:13px;width:300px;text-align:left">No Fares</th>
							<th style = "color:grey;font-size:13px;width:240px;text-align:left">Fare Amt</th>
							<th style = "color:grey;font-size:13px;width:250px;text-align:left">Total Tips</th>
							<th style = "color:grey;font-size:13px;width:250px;text-align:left">Total Taxes</th>
							<th style = "color:grey;font-size:13px;width:250px;text-align:left">Total Amt</th>
							<th style = "color:grey;font-size:13px;width:250px;text-align:left">Total Fee</th>
							<th style = "color:grey;font-size:13px;width:250px;text-align:left">Net Amt</th></tr>'

				SET @Body  = @MsgXml+@Body + @XmlData + '<tfoot><tr style = "height:16px;font-weight:bold">
							<td colspan = 4 style = "font-size:11px;text-align:center">Total</td>
							<td style = "font-size:11px;text-align:left">' + CAST(@NoFares AS VARCHAR)+'</td>
							<td style = "font-size:11px;text-align:left">' + @TotalFareValue + '</td>
							<td style = "font-size:11px;text-align:left">' + @TotalTip + '</td>
							<td style = "font-size:11px;text-align:left">' + @TotalTax + '</td>
							<td style = "font-size:11px;text-align:left">' + @TotalAmt + '</td>
							<td style = "font-size:11px;text-align:left">' + @TotalFee + '</td>
							<td style = "font-size:11px;text-align:left">' + @NetAmt + '</td>
							</tr></tfoot></table><br/><br/>
							<span>Thanks & Regards, <br/><br/>MTData, LLC.<br/>www.mtdata.com<span>
							<h4 style = "color:grey;font-size:12px;font-family:verdana">MTData-All rights reserved</h4>
							<p style = "font-size:11px;color:black">Note:This is an automated message. Do not Reply or Reply All
							to this message as replies are not being accepted.</p> </body></html>'

				INSERT INTO dbo.MailMessage(RepUserId, [Status], SendTo, Body, [RepUserTypeId], [Date], [ReportType]) 
				VALUES (@DriverId, 0, @DriverEmail, @Body, 4, GETDATE(), 'DriverSummaryReport')

				SET @MailId = @@IDENTITY
				EXEC @Res = msdb.dbo.sp_send_dbmail
								@profile_name = 'MTData', 
								@recipients  = @DriverEmail, 
								@body  = @Body, 
								@subject = 'Driver Summary Report', 
								@body_format  = 'HTML'

				IF (@Res != 0)
				BEGIN
					SET @Error = @@ERROR
					UPDATE dbo.MailMessage SET Error = @Error, [Date] = GETDATE() WHERE MailId = @MailId
				END
				ELSE
				BEGIN
					UPDATE dbo.MailMessage SET [Status] = 1, [Date] = GETDATE() WHERE MailId = @MailId
				END
			END
  
			IF (@Valid = 1 AND @IsOwnerRecipient = 1)
			BEGIN
				EXEC dbo.usp_CarOwnerSummaryReport @DriverId, @DateFrom, @DateTo, @ReportName, @DateCommenced
			END

			FETCH NEXT FROM Queue_Cursor INTO @DriverId, @DriverNo, @DriverName, @DriverLName, @DriverEmail, @FleetId
		END

		CLOSE Queue_Cursor
		DEALLOCATE Queue_Cursor
	END TRY
	--use catch to log error in the table  ProcErrorHandler
	BEGIN CATCH
		INSERT INTO ProcErrorHandler(ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, UserName, HostName, [TimeStamp])
		SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
	END CATCH
END
GO
