﻿using System;

namespace MTD.Core.DataTransferObjects
{
    public class FleetDto
    {

        public string CurrencyCode { get; set; }
        public int FleetID { get; set; }
        public int fk_MerchantID { get; set; }
        public string FleetName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<decimal> FleetTransaction { get; set; }
        public Nullable<decimal> CarTransaction { get; set; }
        public Nullable<decimal> TransactionValue { get; set; }
        public bool? IsCardSwiped { get; set; }
        public bool? IsContactless { get; set; }
        public bool? IsChipAndPin { get; set; }
        public bool? IsAssigned { get; set; }
        public bool IsShowTip { get; set; }
        public Nullable<byte> PayType { get; set; }
        public Nullable<bool> IsReceipt { get; set; }
        public string currencyWithSymbol { get; set; }
        public Nullable<int> fK_CurrencyCodeID { get; set; }
        public Nullable<decimal> FleetFee { get; set; }
        public Nullable<byte> TechFeeType { get; set; }
        public Nullable<decimal> TechFeeFixed { get; set; }
        public Nullable<decimal> TechFeePer { get; set; }
        public Nullable<decimal> TechFeeMaxCap { get; set; }

        public Nullable<byte> FleetFeeType { get; set; }
        public Nullable<decimal> FleetFeeFixed { get; set; }
        public Nullable<decimal> FleetFeePer { get; set; }
        public Nullable<decimal> FleetFeeMaxCap { get; set; }

        public Nullable<int> DispatchFleetID { get; set; }
        public string BookingChannel { get; set; }
        public string UserName { get; set; }
        public string PCode { get; set; }
        public string URL { get; set; }
        public string Token { get; set; }
    }
}
