﻿using System;

namespace MTD.Core.DataTransferObjects
{
    public class MerchantUserDto
    {
        public bool IsTaxInclusive { get; set; }
        public int UserID { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string PCode { get; set; }
        public int fk_UserTypeID { get; set; }
        public Nullable<int> fk_MerchantID { get; set; }
        public Nullable<int> DriverTaxNo { get; set; }
        public bool IsTest { get; set; }
        public Nullable<bool> IsActivated { get; set; }
        public Nullable<bool> IsLockedOut { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public int MerchantID { get; set; }
        public string Company { get; set; }
        public Nullable<decimal> SetupFee { get; set; }
        public Nullable<decimal> MonthlyFee { get; set; }
        public string Disclaimer { get; set; }
        public string TermCondition { get; set; }
        public Nullable<decimal> StateTaxRate { get; set; }
        public Nullable<decimal> StateTaxMax { get; set; }
        public Nullable<decimal> FederalTaxRate { get; set; }
        public Nullable<decimal> FederalTaxMax { get; set; }
        public Nullable<decimal> TipPerLow { get; set; }
        public Nullable<decimal> TipPerMedium { get; set; }
        public Nullable<decimal> TipPerHigh { get; set; }
        public bool IsActive { get; set; }
        public string RolesAssigned { get; set; }
        public string CompanyTaxNumber { get; set; }
        public Nullable<bool> IsAllowSale { get; set; }
        public Nullable<bool> IsAllowPreauth { get; set; }
        public Nullable<bool> IsAllowCapture { get; set; }
        public Nullable<bool> IsAllowRefund { get; set; }
        public Nullable<bool> IsAllowVoid { get; set; }
        public Nullable<int> Fk_FDId { get; set; }
        public string GropuId { get; set; }
        public string RapidConnect { get; set; }
        public string TokenType { get; set; }
        public string MCCode { get; set; }
        public string EncryptionKey { get; set; }
        public string DisclaimerPlainText { get; set; }
        public string TermConditionPlainText { get; set; }
        public bool IsJobNoRequired { get; set; }
        public Nullable<byte> ProjectType { get; set; }
        public string EcommProjectId { get; set; }
        public string ProjectId { get; set; }
        public string TimeZone { get; set; }
        public string ZipCode { get; set; }

        public Nullable<bool> UseTransArmor { get; set; }   //PAY-13
    }
}
