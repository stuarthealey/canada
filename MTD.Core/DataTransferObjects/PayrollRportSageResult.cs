﻿using System;

namespace MTD.Core.DataTransferObjects
{
    public class PayrollRportSageResult
    {
        public string PaymentType { get; set; }
        public string SageAccount { get; set; }
        public string BankAccount { get; set; }
        public string Default1 { get; set; }
        public System.DateTime DefaultDate { get; set; }
        public string DefaultBACS { get; set; }
        public string DefaultPayment { get; set; }
        public Nullable<decimal> TransactionAmount { get; set; }
        public string DefaultTaxCode { get; set; }
        public string VAT { get; set; }         

    }
}
