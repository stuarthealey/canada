﻿using System;
using System.Collections.Generic;

namespace MTD.Data.Data
{
    public class RecipientMaster
    {
        public int RecipientID { get; set; }
        public Nullable<int> fk_MerchantID { get; set; }
        public bool IsRecipient { get; set; }
        public string Company { get; set; }
        public string ModifiedBy { get; set; }
        public int ReportId { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }
        public IEnumerable<MerchantRecipient> MerchantList { get; set; }

    }
}
