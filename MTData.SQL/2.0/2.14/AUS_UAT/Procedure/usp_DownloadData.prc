IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DownloadData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_DownloadData]
GO

/****** Object:  StoredProcedure [dbo].[usp_DownloadData]    Script Date: 11/2/2015 10:46:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Umesh Kumar>
-- Create date: <17-09-2015>
-- Description:	<Validate mac address and token>
-- =============================================
USE [MTDataDevlopments]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetTransactionData]    Script Date: 2/26/2016 5:16:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--exec [usp_GetTransactionData] 352,'2015-09-09 00:01:01','2015-09-16 00:01:01','','All',null,'510','253','EmvContact'

Create proc [dbo].[usp_GetTransactionData](	
	@fk_MerchantID int,
	@DateFrom datetime,
	@DateTo datetime = NULL,
	@TransType nvarchar(20) = NULL, 
	@DownloadStatus Char(3) = NULL,
	@VehicleId varchar(50) = NULL,
	@DriverId nvarchar(50) = NULL,
	@fk_fleetId int = NULL,
	@EntryMode NVARCHAR(50)=NULL)
	AS
BEGIN
	
	DECLARE @Sale varchar(50) = NULL, @Completion varchar(50) = NULL, @Refund varchar(50) = NULL, @IsDownloaded char(1),
			@swipe varchar(50) = NULL, @fswipe varchar(50) = NULL, @NewBatch int


	---Managing transaction type
	IF(UPPER(@TransType) = 'APPROVED')
	BEGIN
		SET @Sale='Sale'
		SET @Completion='Completion'
	END
	ELSE IF(UPPER(@TransType) = 'REFUND')
		SET @Refund='Refund'
	ELSE IF(UPPER(@TransType) = 'ALL' OR @TransType='' OR @TransType IS NULL)
	BEGIN
		SET @Sale='Sale'
		SET @Completion='Completion'
		SET @Refund='Refund'
	END


	---Managing EntryMode
	IF(UPPER(@EntryMode) = 'SWIPE')
	BEGIN
		SET @swipe='swiped'
		SET @fswipe='fswiped'
	END
	


	
	-- Managing DownloadStatus
	SELECT @IsDownloaded = case WHEN UPPER(@DownloadStatus)='NEW' THEN '0' 
								WHEN UPPER(@DownloadStatus)='OLD' THEN '1' 
								WHEN UPPER(@DownloadStatus)='ALL' OR @DownloadStatus ='' THEN  NULL END

	
	-- Preparing @DateTo if last date not supplied
	SET @DateTo = IsNull(@DateTo, cast(getdate() as datetime))


	SELECT ID,TerminalID ,DriverNo,VehicleNo,JobNumber,CrdHldrName,CrdHldrPhone,CrdHldrCity,CrdHldrState,CrdHldrZip,
		CrdHldrAddress,TransRefNo,PaymentType,TxnType,FareValue,Tip,Surcharge,Fee,Taxes,Toll,Amount,ExpiryDate,CardType,
		Industry,EntryMode,ResponseCode,AddRespData ,AuthId,SourceId,TransNote,IsCompleted,IsRefunded,PickAddress,DestinationAddress,
		TxnDate,FirstFourDigits,LastFourDigits,FlagFall,Extras,GatewayTxnId,fk_FleetId,RequestId ,IsVoided,IsDownloaded,TransSeqNo
	FROM [MTDTransaction] T
	where 
	(T.VehicleNo = @VehicleId OR COALESCE(@VehicleId,'') = '')
	AND (T.DriverNo = @DriverId OR COALESCE(@DriverId,'') = '')
	AND (T.TxnDate >= @DateFrom OR @DateFrom IS NULL)
	AND (T.TxnDate <= @DateTo OR COALESCE(@DateTo,'')='')
	AND (T.TxnType = @Sale OR T.TxnType= @Completion OR T.TxnType= @Refund)
	AND (T.MerchantId = @fk_MerchantID OR COALESCE(@fk_MerchantID,0) = 0)
	AND (T.fk_FleetId = @fk_fleetId OR COALESCE(@fk_fleetId,0) = 0)
	AND (EntryMode = @swipe OR EntryMode= @fswipe OR EntryMode= @EntryMode OR COALESCE(@EntryMode,'') = '')
	AND (T.IsDownloaded = @IsDownloaded OR @IsDownloaded IS NULL)
	AND ResponseCode in ('000','002') 


	-- Updating download status
	
	set @NewBatch=COALESCE((select max(batchnumber) from MTDTransaction),0)
	 
	UPDATE [MTDTransaction] SET IsDownloaded=1, BatchNumber= @NewBatch+1
	WHERE 
	(VehicleNo = @VehicleId OR COALESCE(@VehicleId,'') = '')
	AND (DriverNo = @DriverId OR COALESCE(@DriverId,'') = '')
	AND (TxnDate >= @DateFrom OR @DateFrom IS NULL)
	AND (TxnDate <= @DateTo)
	AND (TxnType = @Sale OR TxnType= @Completion OR TxnType= @Refund OR @TransType IS NULL)
	AND (MerchantId = @fk_MerchantID OR COALESCE(@fk_MerchantID,0) = 0)
	AND (fk_FleetId = @fk_fleetId OR COALESCE(@fk_fleetId,0) = 0)
	AND (EntryMode = @swipe OR EntryMode= @fswipe OR EntryMode= @EntryMode OR COALESCE(@EntryMode,'') = '')
	AND (IsDownloaded = @IsDownloaded OR @IsDownloaded IS NULL)
	AND ResponseCode in ('000','002') 
	
	
	OPTION (RECOMPILE)

END
GO