﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MTData.Web.ViewData
{
    public class TerminalExcelUploadViewData
    {
        public int TerminalID { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "Please enter the serial number")]
        [Display(Name = "Serial number")]
        public string SerialNo { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "Please choose the device name")]
        [Display(Name = "Device name")]
        public string DeviceName { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "Please enter the mac address")]
        [Display(Name = "Mac address")]
        public string MacAdd { get; set; }

        public string SoftVersion { get; set; }
        public string Description { get; set; }
        public int fk_MerchantID { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Nullable<bool> IsTerminalAssigned { get; set; }
        public string Stan { get; set; }
        public string RefNumber { get; set; }
        public DateTime? TransactionDate { get; set; }
        public Nullable<int> DatawireId { get; set; }
        public string TermId { get; set; }
        public string FailedError { get; set; }
    }
}