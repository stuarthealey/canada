﻿namespace MTData.WebAPI.Models
{
    public class RespGrp
    {
        public string  RespCode { get; set; }

        public string AthNtwkID { get; set; }

        public string ErrorData { get; set; }
        public FileDLGrp FileDLGrp { get; set; }
        public string AddtlRespData { get; set; }

        public string AuthID { get; set; }
        public EMVGrp EMVGrp { get; set; }

        public MCGrp MCGrp { get; set; } 

    }
}