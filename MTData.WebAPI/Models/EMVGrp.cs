﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Models
{
    public class EMVGrp
    {
        public string EMVData { get; set; }
        public string ServCode { get; set; }
        public string AppExpDate { get; set; }
    }
}