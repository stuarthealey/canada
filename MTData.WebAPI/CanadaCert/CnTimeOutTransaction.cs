﻿using System;
using System.Text;
using System.Configuration;
using System.Threading;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.WebAPI.Models;
using MTData.WebAPI.Controllers;
using MTData.WebAPI.Resources;
using MTData.WebAPI.Common;
using MTData.WebAPI.CanadaCommon;
using MTData.Utility;

namespace MTData.WebAPI.CanadaCert
{
    public class CnTimeOutTransaction : TransactionRequestController
    {
        private readonly ITransactionService _transactionService;
        private static ITerminalService _terminalService;
        private StringBuilder _logMessage;
        private Iso8583Response transResponse;
        private ILogger _logger = new Logger();

        public CnTimeOutTransaction(
            [Named("TransactionService")] ITransactionService transactionService,
            [Named("TerminalService")] ITerminalService terminalService,
            StringBuilder logMessage)
            : base(terminalService, logMessage)
        {
            _transactionService = transactionService;
            _terminalService = terminalService;
            _logMessage = logMessage;
        }

        /// <summary>
        /// Prepare and send time out transaction
        /// </summary>
        /// <returns>Return parsed response transResp</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public Iso8583Response TimeOut(TransactionRequest tr, int transId, string clientRef)
        {            
            Thread.Sleep(40000);//FD Keeps holding the line and Client initiates a TOR after a minimum gap of 40 Seconds

            transResponse = SendTimeoutTransaction(tr, transId, clientRef);

            _logMessage.Append("SendTimeoutTransaction executed successfully " + transId);

            if (Object.Equals(transResponse, null))
            {
                _logMessage.Append("checking for timeout");

                for (int i = 0; i <= 1; i++)
                {
                    Thread.Sleep(40000);
                        
                    transResponse = SendTimeoutTransaction(tr, transId, clientRef);

                    _logMessage.Append("timeout sent again");

                    if (!Object.Equals(transResponse, null))
                        break;
                }
            }

            return transResponse;
        }

        /// <summary>
        /// Send the timeout transaction to first data
        /// </summary>
        /// <returns>Return parsed response transResp</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public Iso8583Response SendTimeoutTransaction(TransactionRequest tr, int prevtransId, string clientRef)
        {
            // Debug
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
            {
                _logger.LogInfoMessage(string.Format("CnTimeOutTransaction.SendTimeoutTransaction() TerminalId:{0};prevTransId:{1};clientRef:{2};IndustryType:{3};EntryMode:{4};CardType:{5};ResponseBit63:{6};",
                                   tr.TerminalId, prevtransId, clientRef, tr.IndustryType, tr.EntryMode, tr.CardType, tr.ResponseBit63));
            }

            tr.IsReversal = true;   //SH 2018-07-25 Added at request of Conrad, to try and get Track2Data formatted correctly.
            tr.IsTORTransaction = true;

            CnTransactionEntities cnTrans = new CnTransactionEntities();

            if (tr.IndustryType == SigmaStaticValue.BookerAppToken || tr.IndustryType == SigmaStaticValue.BookerAppPayment)
            {
                tr.CnPayload = CnTransactionMessage.CnPrepareBookerAppRequest(tr, clientRef, out cnTrans);
            }
            else if (tr.IndustryType == PaymentIndustryType.Ecommerce.ToString())
            {
                tr.CnPayload = CnPreparePostTransaction.CheckAndPrepareEcommTrans(tr, clientRef, out cnTrans);
            }
            else if (tr.IndustryType == PaymentIndustryType.Retail.ToString())
            {
                if (tr.EntryMode == PaymentEntryMode.Swiped.ToString())
                {
                    tr.CnPayload = CnPreparePostTransaction.CheckAndPrepareRetailTrans(tr, clientRef, out cnTrans);
                }
                //zzz Added as part of Chetu 357
                else if (tr.EntryMode == PaymentEntryMode.MSRContactless.ToString())
                {
                    tr.CnPayload = CnPreparePostTransaction.PrepareMSRContactlessTransRequest(tr, clientRef, out cnTrans);
                }
                else if (tr.EntryMode == PaymentEntryMode.EmvContact.ToString())
                {
                    if (tr.CardType.ToString() == CardTypeType.Interac.ToString())
                        tr.CnPayload = CnPreparePostTransaction.PrepareInteracTransRequest(tr, clientRef, out cnTrans); //for Interac Contact transaction
                    else
                        tr.CnPayload = CnPreparePostTransaction.PrepareEMVTransRequest(tr, clientRef, out cnTrans);
                }
                //zzz Added as part of Chetu 357
                else if (tr.EntryMode == PaymentEntryMode.EmvContactless.ToString())
                {
                    if (tr.CardType.ToString() == CardTypeType.Interac.ToString())
                        tr.CnPayload = CnPreparePostTransaction.PrepareInteracContactlessTransRequest(tr, clientRef, out cnTrans); //for Interac Contactless
                    else
                        tr.CnPayload = CnPreparePostTransaction.PrepareEMVContactlessTrans(tr, clientRef, out cnTrans);
                }
            }

            // Debug
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                _logger.LogInfoMessage(string.Format("CnTimeOutTransaction.SendTimeoutTransaction() TerminalId:{0};prevTransId:{1};clientRef:{2};CnPayload:{3};", tr.TerminalId, prevtransId, clientRef, tr.CnPayload));

            tr.TerminalId = tr.TerminalId.FormatTerminalId();

            MTDTransactionDto transactionDto = CommonFunctions.XmlToTransactionDto(tr);

            transactionDto.TxnType = PaymentAPIResources.CanadaTimeOut;

            // Debug
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                _logger.LogInfoMessage(string.Format("CnTimeOutTransaction.SendTimeoutTransaction() TerminalId:{0};prevTransId:{1};clientRef:{2};TxnType:{3};", tr.TerminalId, prevtransId, clientRef, transactionDto.TxnType));

            int transId = _transactionService.Add(transactionDto);

            // Debug
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                _logger.LogInfoMessage(string.Format("CnTimeOutTransaction.SendTimeoutTransaction() TerminalId:{0};prevTransId:{1};clientRef:{2};transId:{3};", tr.TerminalId, prevtransId, clientRef, transId));

            _terminalService.UpdateStan(transactionDto.Stan, transactionDto.TransRefNo, tr.TerminalId, tr.MtdMerchantId);

            string transResponse = CnSendTransaction.SendMessageToDatawire(Global.ActiveUrl, tr.CnPayload);

            cnTrans.CompleteResponse = transResponse;

            // Debug
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                _logger.LogInfoMessage(string.Format("CnTimeOutTransaction.SendTimeoutTransaction() TerminalId:{0};prevTransId:{1};clientRef:{2};transResponse:{3};", tr.TerminalId, prevtransId, clientRef, transResponse));

            Iso8583Response transResp = ResponseHandler.ParseAndSaveResponse(cnTrans, transId);

            // Debug
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
            {
                _logger.LogInfoMessage(string.Format("CnTimeOutTransaction.SendTimeoutTransaction() TerminalId:{0};prevTransId:{1};clientRef:{2};transResp.ReturnCode:{3};transResp.ResponseCode:{4}",
                                   tr.TerminalId, prevtransId, clientRef, transResp.ReturnCode, transResp.ResponseCode));
            }

            transResp.SourceId = prevtransId;

            if (String.IsNullOrEmpty(transResponse) || CommonFunctions.IsFailed(transResp.ReturnCode))
            {
                SaveInDatabase(transResp, transId);
                return null;
            }

            SaveInDatabase(transResp, transId);

            return transResp;
        }

        /// <summary>
        /// Save time out transaction in database
        /// </summary>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public void SaveInDatabase(Iso8583Response iso8583Response, int transId)
        {
            string objResponse = Serialization.Serialize<Iso8583Response>(iso8583Response);
            
            iso8583Response.CompleteResponse = objResponse;
            
            MTDTransactionDto transDto = CnTransactionMessage.FillMerchantDto(iso8583Response);

            transDto.SourceId = Convert.ToString(iso8583Response.SourceId);
            transDto.TxnType = null;

            _transactionService.CnUpdate(transDto, transId);
        }

    }
}