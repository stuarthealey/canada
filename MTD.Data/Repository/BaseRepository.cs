﻿using MTD.Data.Data;

namespace MTD.Data.Repository
{
    public class BaseRepository
    {
        private IMtDataDevlopmentsEntities _dbMtData;

        public BaseRepository(IMtDataDevlopmentsEntities dbMtData)
        {
            _dbMtData = dbMtData;
        }

        public IMtDataDevlopmentsEntities DbMtData
        {
            get
            {
                return _dbMtData;
            }
        }
    }
}
