﻿using System.Collections.Generic;
using MTD.Core.DataTransferObjects;
using MTD.Core.Extension;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;
using Ninject;

namespace MTD.Core.Service
{
    public class PrefixService : IPrefixService
    {
        private readonly IPrefixRepository _prefixRepository;

        public PrefixService([Named("PrefixRepository")] IPrefixRepository prefixRepository)
        {
            _prefixRepository = prefixRepository;

        }

        /// <summary>
        /// Purpose             :   To Add the Prefix
        /// Function Name       :   Add
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/09/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="prefixDto"></param>
        /// <returns></returns>
        public bool Add(PrefixDto prefixDto)
        {
            try
            {
                CCPrefix prefix = prefixDto.ToPrefix();
                return _prefixRepository.Add(prefix);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Prefix List
        /// Function Name       :   GetPrefixDtoList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/13/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PrefixDto> GetPrefixDtoList(int merchantId,string name)
        {
            try
            {
                var prefixList = _prefixRepository.prefixList(merchantId, name);
                var prefixDtoList = prefixList.ToDTOPrefixList();
                return prefixDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Prefix  
        /// Function Name       :   GetPrefixDto
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/13/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="prefixId"></param>
        /// <returns></returns>
        public PrefixDto GetPrefixDto(int prefixId)
        {
            try
            {
                var prefix = _prefixRepository.GetPrefix(prefixId);
                var prefixDto = prefix.ToPrefixDto();
                return prefixDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update Prefix  
        /// Function Name       :   Update
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/13/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="prefixDto"></param>
        /// <param name="prefixId"></param>
        /// <returns></returns>
        public void Update(PrefixDto prefixDto, int prefixId)
        {
            try
            {
                CCPrefix prefix = prefixDto.ToPrefix();
                _prefixRepository.Update(prefix, prefixId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Delete the Prefix
        /// Function Name       :   DeletePrefix
        /// Created By          :   Salil Gupta Shafeeque
        /// Created On          :   02/13/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="prefixId"></param>
        public void DeletePrefix(int prefixId)
        {
            try
            {
                _prefixRepository.DeletePrefix(prefixId);
            }
            catch
            {
                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To get Prefixs List
        /// Function Name       :   PrefixListByFilter
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   02/13/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public IEnumerable<PrefixDto> PrefixListByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting,int merchantId)
        {
            try
            {
                var prefixList = _prefixRepository.PrefixListByFilter(name, jtStartIndex, jtPageSize, jtSorting, merchantId);
                var vehicleDtoList = prefixList.ToDTOPrefixList();
                return vehicleDtoList;
            }
            catch
            {
                throw;
            }
        }

    }
}
