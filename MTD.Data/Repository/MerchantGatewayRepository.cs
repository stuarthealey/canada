﻿using System;
using System.Collections.Generic;
using System.Linq;

using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Data.Repository
{
    public class MerchantGatewayRepository : BaseRepository, IMerchantGatewayRepository
    {
        public MerchantGatewayRepository(IMtDataDevlopmentsEntities dbMtData) : base(dbMtData)
        {
        }

        /// <summary>
        /// Purpose             :   To add merchant gateway
        /// Function Name       :   Add
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gateway"></param>
        /// <returns></returns>
        public bool Add(Merchant_Gateway gateway)
        {
            try
            {
                bool isExist = DbMtData.Merchant_Gateway.Any(m => m.IsDefault==true && m.fk_MerchantID == gateway.fk_MerchantID && m.IsActive==true && gateway.IsDefault==true);
                if (isExist)
                    return true;

                bool isGatewayExist=DbMtData.Merchant_Gateway.Any(m=>m.fk_MerchantID==gateway.fk_MerchantID && m.fk_GatewayID==gateway.fk_GatewayID && m.IsActive==true);
                if (isGatewayExist)
                    return true;
               
                gateway.IsActive = true;
                gateway.CreatedDate = DateTime.Now;
                DbMtData.Merchant_Gateway.Add(gateway);
                DbMtData.CommitChanges();

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get merchant gateway
        /// Function Name       :   GetMerchantGateway
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantGatewayId"></param>
        /// <returns></returns>
        public Merchant_Gateway GetMerchantGateway(int merchantGatewayId)
        {
            try
            {
                Merchant_Gateway gatewayDb = DbMtData.Merchant_Gateway.Find(merchantGatewayId);
                return gatewayDb;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To update merchant gateway
        /// Function Name       :   Update
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gateway"></param>
        /// <param name="merchantGatewayId"></param>
        /// <returns></returns>
        public bool Update(Merchant_Gateway gateway, int merchantGatewayId)
        {
            try
            {
                Merchant_Gateway gatewayDb = DbMtData.Merchant_Gateway.Find(merchantGatewayId);

                bool isExist = DbMtData.Merchant_Gateway.Any(m => m.IsDefault==true && m.fk_MerchantID==gateway.fk_MerchantID && m.IsActive==true && m.MerchantGatewayID!=merchantGatewayId && gateway.IsDefault==true);
                if (isExist)
                    return true;

                gatewayDb.fk_MerchantID = gateway.fk_MerchantID;
                gatewayDb.fk_GatewayID = gateway.fk_GatewayID;
                gatewayDb.IsMtdataGateway = gateway.IsMtdataGateway;
                gatewayDb.UserName = gateway.UserName;
                gatewayDb.PCode = gateway.PCode;
                gatewayDb.IsDefault = gateway.IsDefault;
                gatewayDb.ModifiedDate = DateTime.Now;
                gatewayDb.ModifiedBy = gateway.ModifiedBy;
            }
            catch (Exception)
            {
                throw;
            }

            DbMtData.CommitChanges();
            return false;
        }

        /// <summary>
        /// Purpose             :   To delete merchant gateway
        /// Function Name       :   DeleteMerchantGateway
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantGatewayId"></param>
        public void DeleteMerchantGateway(int merchantGatewayId)
        {
            try
            {
                Merchant_Gateway gatewayDb = DbMtData.Merchant_Gateway.Find(merchantGatewayId);
                gatewayDb.IsActive = false;
                DbMtData.CommitChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get list of merchant gateway
        /// Function Name       :   MerchantGatewayList
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<Merchant_Gateway> MerchantGatewayList(int merchantId)
        {
            try
            {
                IEnumerable<Merchant_Gateway> merchantGatewayList = (from m in DbMtData.Merchant_Gateway
                                                         join n in DbMtData.Gateways on m.fk_GatewayID
                                                             equals n.GatewayID
                                                         where m.IsActive == true && m.fk_MerchantID == merchantId
                                                         select
                                                             new { n.Name, m.IsDefault, m.MerchantGatewayID }).AsEnumerable().Select
                    (objNew => new Merchant_Gateway
                    {
                        Name = objNew.Name,

                        MerchantGatewayID = objNew.MerchantGatewayID,
                        IsDefault = objNew.IsDefault
                    }).ToList();

                foreach (Merchant_Gateway gateway in merchantGatewayList)
                {
                    gateway.Type = gateway.IsDefault == false ? "No" : "Yes";
                }

                return merchantGatewayList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get list of merchant by filter
        /// Function Name       :   MerchantGatewayList
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <param name="sorting"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<Merchant_Gateway> MerchantGateListByFilter(FilterSearchParameters fsParametes)
        {
            try
            {
                IEnumerable<Merchant_Gateway> gatewayList = (from m in DbMtData.Merchant_Gateway
                                                             join n in DbMtData.Gateways on m.fk_GatewayID
                                                                 equals n.GatewayID
                                                             where m.IsActive == true && m.fk_MerchantID == fsParametes.MerchantId
                                                             select
                                                                 new { n.Name, m.IsDefault, m.MerchantGatewayID, m.UserName }).AsEnumerable().Select
                        (objNew => new Merchant_Gateway
                        {
                            Name = objNew.Name,
                            UserName = objNew.UserName,
                            MerchantGatewayID = objNew.MerchantGatewayID,
                            IsDefault = objNew.IsDefault
                        }).ToList();
                foreach (Merchant_Gateway gateway in gatewayList)
                {
                    if (gateway.IsMtdataGateway == false)
                        gateway.Type = "Personal";
                    else
                        gateway.Type = "MTData";
                }

                if (!string.IsNullOrEmpty(fsParametes.Name))
                    gatewayList = gatewayList.Where(p => p.Name.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0);
                
                if (!string.IsNullOrEmpty(fsParametes.Sorting))
                    gatewayList = SortedMerchantGateway(fsParametes.Sorting, gatewayList);
                
                return fsParametes.PageSize > 0
                       ? gatewayList.Skip(fsParametes.StartIndex).Take(fsParametes.PageSize).ToList() //Paging
                       : gatewayList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="gatewayList"></param>
        /// <returns></returns>
        private IEnumerable<Merchant_Gateway> SortedMerchantGateway(string sorting, IEnumerable<Merchant_Gateway> gatewayList)
        {
            switch (sorting)
            {
                case "Name ASC":
                    gatewayList = gatewayList.OrderBy(p => p.Name);
                    break;
                case "Name DESC":
                    gatewayList = gatewayList.OrderByDescending(p => p.Name);
                    break;
                default:
                    gatewayList = gatewayList.OrderBy(p => p.Name); //Default!
                    break;
            }

            return gatewayList;
        }

    }
}
