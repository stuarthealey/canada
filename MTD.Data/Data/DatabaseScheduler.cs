﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Data.Data
{
    public  class DatabaseScheduler
    {
        public int ScheduleId { get; set; }
        public string Name { get; set; }
        public string New_Name{ get; set; } 
        public int Enabled { get; set; }
        public int Freq_Type { get; set; }
        public int Freq_Interval { get; set; }
        public int Freq_Subday_Type { get; set; }
        public int Freq_Subday_Interval{ get; set; }
        public int Freq_Relative_Interval { get; set; }
        public int Freq_Recurrence_Factor { get; set; }
        public int Active_Start_Date{ get; set; }
        public int Active_End_Date { get; set; }
        public int Active_Start_Time { get; set; }
        public int Active_End_Time { get; set; }
        public string Owner_Login_Name { get; set; }
        public int Automatic_Post { get; set; }
    }
}
