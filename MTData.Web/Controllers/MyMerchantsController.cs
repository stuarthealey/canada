﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Security;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.Web.Models;
using MTData.Web.Resource;
using MTData.Web.ViewData;

namespace MTData.Web.Controllers
{
    public class MyMerchantsController : Controller
    {
        private StringBuilder _logMessage;
        private StringBuilder _logTracking;
        private readonly ISubAdminService _subAdminService;
        readonly ILogger _logger = new Logger();
        private readonly ICommonService _commonService;
        private readonly IUserService _userService;
        private readonly ITransactionService _transactionService;
        private readonly IReportCommonService _reportcommonRepository;
        private readonly IDriverService _driverService;
        private const string Decline = "DECLINED-005";
        private const string Approved = "APPROVAL";
        private bool ChargeBack = true;
        private string Timezone = "UTC-11";
        private readonly IReportCommonService _reportcommonService;
        private readonly IScheduleService _scheduleService;
        private readonly IDefaultService _defaultService;

        public MyMerchantsController([Named("SubAdminService")] ISubAdminService subAdminService, StringBuilder logMessage, StringBuilder logTracking, [Named("CommonService")] ICommonService commonService, [Named("DriverService")] IDriverService driverService, [Named("TransactionService")] ITransactionService transactionService, [Named("UserService")] IUserService userService,
                                        [Named("ReportCommonService")] IReportCommonService reportcommonService, [Named("DefaultService")] IDefaultService defaultService, [Named("ScheduleService")] IScheduleService scheduleService)
        {
            _subAdminService = subAdminService;
            _logMessage = logMessage;
            _logTracking = logTracking;
            _commonService = commonService;
            _driverService = driverService;
            _userService = userService;
            _transactionService = transactionService;
            _reportcommonRepository = reportcommonService;
            _reportcommonService = reportcommonService;
            _defaultService = defaultService;
            _scheduleService = scheduleService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            IsFirstLoginViewData obj = new IsFirstLoginViewData();
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            
            if (Session["User"] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 5)
                return RedirectToAction("Index", "Member");

            if (Session["AllInfo"] != null)
                Session.Remove("AllInfo");
            
            if (!(bool)Session["Islogged"])
            {
                obj.IsFirstLogin = (bool)((SessionDto)Session["User"]).SessionUser.IsFirstLogin;
                obj.UserTypeId = Convert.ToString(((SessionDto)Session["User"]).SessionUser.fk_UserTypeID);
                Session.Remove("Islogged");
                Session["Islogged"] = true;
                obj.UserId = ((SessionDto)Session["User"]).SessionUser.UserID;
                return View(obj);
            }

            obj.UserId = ((SessionDto)Session["User"]).SessionUser.UserID;
            return View(obj);
        }

        /// <summary>
        /// Purpose             : apply page indexing on merchant list
        /// Function Name       : ViewMerchant
        /// Created By          : umesh
        /// Created On          :2/5/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult MyMerchantsListByFiter(string name = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetMerchantCountByFilter method of _merchantService class to count Merchant");

                int merchantUserListCount;
                name = name.Trim();
                int userId = ((SessionDto)Session["User"]).SessionUser.UserID;

                PageSizeSearchViewData pSizeSearch = new PageSizeSearchViewData()
                {
                    Name = name,
                    JtStartIndex = jtStartIndex,
                    JtPageSize = jtPageSize,
                    JtSorting = jtSorting,
                    UserId = userId
                };

                PageSizeSearchDto pSizeSearchDto = pSizeSearch.ToPageSizeSearch();
                var merchantUser = _subAdminService.GetMyMerchantByFilter(pSizeSearchDto);
                if (string.IsNullOrEmpty(name))
                {
                    _logMessage.Append("Calling MerchantList method of _merchantService class to count Merchants");

                    merchantUserListCount = _subAdminService.MyMerchantList(name, userId).ToList().Count;
                    
                    _logMessage.Append("MerchantList method of _merchantService executed successfully result = " + merchantUserListCount);
                    
                    return Json(new { Result = "OK", Records = merchantUser, TotalRecordCount = merchantUserListCount });
                }

                var merchantUserDtos = merchantUser as MerchantUserDto[] ?? merchantUser.ToArray();
                merchantUserListCount = _subAdminService.MyMerchantList(name, userId).ToList().Count;
                
                _logMessage.Append("GetMerchantByFilter method of _merchantService executed successfully");

                return Json(new { Result = "OK", Records = merchantUserDtos, TotalRecordCount = merchantUserListCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        [HttpGet]
        public ActionResult TxnReport(int page)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || Session["User"] == null) return RedirectToAction("Index", "Member");
            TxnSearch tvdData = new TxnSearch();
            tvdData.IsVarified = null;
            List<CarListViewData> carList = new List<CarListViewData>();
            List<DriverListViewData> driverList = new List<DriverListViewData>();
            var dto = (SessionDto)Session["User"];
            int userId = dto.SessionUser.UserID;
            tvdData.Myfleetlists = _subAdminService.GetUsersFleet(userId).ToViewDataList();
            tvdData.DriverList = driverList;
            tvdData.CarList = carList;
            tvdData.SourceId = Convert.ToString(page);
            tvdData.fk_FleetId = 0;
            tvdData.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            tvdData.Currency = ConfigurationManager.AppSettings["Currency"].ToString();

            return View(tvdData);
        }

        /// <summary>
        /// Purpose             :   Create transaction history on the basis of transaction ID
        /// Function Name       :   Create
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/15/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult TxnReport(TxnSearch tvd)
        {
            try
            {
                if (Session["User"] == null) return RedirectToAction("Index", "Member");
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            
                int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                tvd.Myfleetlists = _subAdminService.GetUsersFleet(userId).ToViewDataList();
                
                int fId = 0;
                tvd.SourceId = tvd.AId;
                
                if (tvd.fk_FleetId != null)
                    fId = (int)tvd.fk_FleetId;

                TimeZoneInfo timeZone = TimeZoneInfo.Local;
                Timezone = timeZone.StandardName;// use this time zone

                tvd.CarList = _commonService.GetCars((int)fId).ToViewDataList();
                tvd.DriverList = _commonService.GetDrivers((int)fId).ToViewDataList();
                if (tvd.startDateString != null && tvd.endDateString != null)
                {
                    if (tvd.startDateString.Length > 0 && tvd.endDateString.Length > 0)
                    {
                        bool isDate = false;
                        Regex r = new Regex(@"\d\d-\d\d-\d\d\d\d\s\d\d:\d\d");
                        if (((r.Match(tvd.startDateString).Success) && (r.Match(tvd.endDateString).Success)))
                        {
                            DateTime resultDate;

                            isDate = DateTime.TryParseExact(tvd.startDateString, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return View(tvd);
                            else
                                tvd.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones

                            isDate = DateTime.TryParseExact(tvd.endDateString, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return View(tvd);
                            else
                                tvd.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
                        }
                    }
                }

                if (tvd.fk_FleetId == null && string.IsNullOrEmpty(tvd.VNumber) && string.IsNullOrEmpty(tvd.DNumber) && string.IsNullOrEmpty(tvd.TType) && string.IsNullOrEmpty(tvd.CrdType) && tvd.startDate == null && tvd.endDate == null && tvd.Id == null && string.IsNullOrEmpty(tvd.ExpiryDate) && string.IsNullOrEmpty(tvd.FFD) && string.IsNullOrEmpty(tvd.LFD) && tvd.MinAmt == null && tvd.MaxAmt == null && tvd.JobNumber == null && tvd.AId == null && tvd.AddRespData == null && tvd.Industry == null && string.IsNullOrEmpty(tvd.SerialNo) && string.IsNullOrEmpty(tvd.TerminalId))
                {
                    ViewBag.Message = MtdataResource.Please_Select_field; //"Please enter or choose some field's";
                    return View(tvd);
                }

                if (!String.IsNullOrEmpty(tvd.ExpiryDate))
                    tvd.ExpiryDate = StringExtension.DateFormat(tvd.ExpiryDate);

                MtdTransactionsViewData td = new MtdTransactionsViewData
                {
                    PageSize = 0,
                    ExpiryDate = tvd.ExpiryDate,
                    VehicleNo = tvd.VNumber,
                    DriverNo = tvd.DNumber,
                    CardType = tvd.CrdType,
                    startDate = tvd.startDate,
                    endDate = tvd.endDate,
                    Id = tvd.Id,
                    TxnType = tvd.TType,
                    FirstFourDigits = tvd.FFD,
                    LastFourDigits = tvd.LFD,
                    MinAmount = tvd.MinAmt,
                    MaxAmount = tvd.MaxAmt,
                    AuthId = tvd.AId,
                    AddRespData = tvd.AddRespData,
                    MerchantId = null,
                    fk_FleetId = tvd.fk_FleetId,
                    EntryMode = tvd.Industry,
                    IsVarified = tvd.IsVarified,
                    UserId = userId,
                    SerialNo = tvd.SerialNo,
                    TerminalId = tvd.TerminalId,
                    TransNote = tvd.TransNote   // Ensure TransNote included
                };
               
                MTDTransactionDto transactionDto = td.ToDTO();
                int count = _subAdminService.GetTransactionCount(transactionDto);
                transactionDto.PageSize = count;
                IEnumerable<MTDTransactionDto> stvd = _subAdminService.GetTransaction(transactionDto);

                var userfleet = _userService.GetUsersFleet(userId);//2
                stvd = stvd.Where(x => x.fk_FleetId != null);
                stvd = stvd.Where(x => userfleet.Select(y => y.fk_FleetID).Contains((int)x.fk_FleetId)).ToList();

                IEnumerable<SearchTransactionViewData> mvData = stvd.ToViewData();
                var transactionViewDatas = mvData as SearchTransactionViewData[] ?? mvData.ToArray();

                if (transactionViewDatas.ToList().Count == 0)
                {
                    ViewBag.Message = MtdataResource.No_such_record_exist;  //"No such records exist";
                    return View(tvd);
                }

                string culture = ConfigurationManager.AppSettings["CultureInfo"].ToString();
                string currency = ConfigurationManager.AppSettings["Currency"].ToString();

                using (StringWriter output = new StringWriter())
                {
                    output.WriteLine("\"Id\",\"AuthCode\",\"Vehicle\",\"Driver\",\"Date\",\"Credit/Debit\",\"Transaction Type\",\"Tech Fee(" + currency + ")\",\"Fleet Fee(" + currency + ")\",\"Fee(" + currency + ")\",\"Surcharge(" + currency + ")\",\"Amount(" + currency + ")\",\"CardType\",\"Entry Mode\",\"Approval\"");
                    
                    foreach (SearchTransactionViewData item in transactionViewDatas)
                    {
                        DateTime dt1 = TimeZoneInfo.ConvertTime(Convert.ToDateTime(item.TxnDate), TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(Timezone));
                        
                        decimal tFee = Convert.ToDecimal(item.TechFee);
                        decimal fFee = Convert.ToDecimal(item.FleetFee);
                        decimal fee = Convert.ToDecimal(item.Fee);
                        decimal sur = Convert.ToDecimal(item.Surcharge);
                        decimal amt = Convert.ToDecimal(item.Amount);

                        output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\"",
                            //item.Id, item.AuthId, item.VehicleNo, item.DriverNo, dt1.ToString("MM/dd/yyyy HH:mm:ss"), item.PaymentType, item.TxnType,
                            item.Id, item.AuthId, item.VehicleNo, item.DriverNo, dt1.ToString("G", CultureInfo.GetCultureInfo(culture)), item.PaymentType, item.TxnType,
                            tFee.ToString("C", CultureInfo.GetCultureInfo(culture)), fFee.ToString("C", CultureInfo.GetCultureInfo(culture)),
                            fee.ToString("C", CultureInfo.GetCultureInfo(culture)), sur.ToString("C", CultureInfo.GetCultureInfo(culture)),
                            amt.ToString("C", CultureInfo.GetCultureInfo(culture)), item.CardType, item.EntryMode, item.AddRespData);
                    }

                    _logTracking.Append("Transaction Report Excel file Downloaded by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    //return File(new ASCIIEncoding().GetBytes(output.ToString()), "application/CSV", "Transaction_Report.csv");    // This wipes the currency symbol.
                    return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Transaction_Report.csv");
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View(tvd);
        }

        //<summary>
        //Purpose             :   Page indexing to display manual transaction
        //Function Name       :   MyTransactionListByFilter
        //Created By          :   Madhuri Tanwar
        //Created On          :   2/9/2015
        //</summary>
        //<param name="searchCriteria"></param>
        //<param name="jtStartIndex"></param>
        //<param name="jtPageSize"></param>
        //<param name="jtSorting"></param>
        //<returns></returns>
        [HttpPost]
        public JsonResult MyTransactionListByFilter(SearchParameters searchCriteria, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                IEnumerable<SearchTransactionDto> list = new List<SearchTransactionDto>();
                if (searchCriteria.approved == null && searchCriteria.rejected == null)
                {
                    searchCriteria.approved = false;
                    searchCriteria.rejected = false;
                }

                if (searchCriteria.IsVarifiedYes == (bool)false && searchCriteria.IsVarifiedNo == (bool)false && searchCriteria.IsVarifiedBoth == (bool)false)
                {
                    searchCriteria.IsVarified = null;
                }
                else
                {
                    if (searchCriteria.IsVarifiedYes == (bool)true)
                        searchCriteria.IsVarified = true;
                    if (searchCriteria.IsVarifiedNo == (bool)true)
                        searchCriteria.IsVarified = false;
                    if (searchCriteria.IsVarifiedBoth == (bool)true)
                        searchCriteria.IsVarified = null;
                }

                var sessionData = ((SessionDto)Session["User"]);
                int? merchantId = null;
                var fkMerchantId = sessionData.SessionUser.fk_MerchantID;

                int userId = sessionData.SessionUser.UserID;
                if (fkMerchantId != null)
                {
                    Timezone = _commonService.MerchantTimeZone((int)merchantId);
                    merchantId = fkMerchantId;
                    
                    var roles = _userService.GetUserRoles(userId).ToViewDataList();                    
                    if (roles.Any(x => x.RoleId == 1))
                        ChargeBack = true;
                    else
                        ChargeBack = false;
                }
                else
                {
                    TimeZoneInfo timeZone = TimeZoneInfo.Local;
                    Timezone = timeZone.StandardName;// use this time zone
                }

                if (searchCriteria.startDateString != null && searchCriteria.endDateString != null)
                {
                    bool isDate = false;
                    DateTime resultDate;

                    isDate = DateTime.TryParseExact(searchCriteria.startDateString, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                    if (isDate)
                        searchCriteria.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
                    else
                        return Json(new { Result = "OK", TotalRecordCount = 0, Records = list });

                    isDate = DateTime.TryParseExact(searchCriteria.endDateString, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                    if (isDate)
                        searchCriteria.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
                    else
                        return Json(new { Result = "OK", TotalRecordCount = 0, Records = list });
                }

                if (searchCriteria.fleetId == null && string.IsNullOrEmpty(searchCriteria.VNumber) && string.IsNullOrEmpty(searchCriteria.DNumber) && searchCriteria.approved == false && searchCriteria.rejected == false && searchCriteria.startDate == null && searchCriteria.endDate == null && searchCriteria.Id == null && searchCriteria.TType == null && string.IsNullOrEmpty(searchCriteria.AId) && searchCriteria.Industry == null && string.IsNullOrEmpty(searchCriteria.ExpiryDate) && string.IsNullOrEmpty(searchCriteria.FFD) && string.IsNullOrEmpty(searchCriteria.LFD) && string.IsNullOrEmpty(searchCriteria.CrdType) && searchCriteria.MinAmt == null && searchCriteria.MaxAmt == null && string.IsNullOrEmpty(searchCriteria.JobNumber) && string.IsNullOrEmpty(searchCriteria.SerialNo) && string.IsNullOrEmpty(searchCriteria.TerminalId))
                    return Json(new { Result = "OK", TotalRecordCount = 0, Records = list });

                if (sessionData == null)
                {
                    List<SearchTransactionDto> listd = new List<SearchTransactionDto>();
                    SearchTransactionDto st = new SearchTransactionDto();
                    st.SourceId = "-56";
                    listd.Add(st);
                    return Json(new { Result = "OK", TotalRecordCount = 0, Records = listd });
                }

                string gatewayMessage = "";
                if (searchCriteria.rejected != null && (bool)searchCriteria.rejected)
                    gatewayMessage = Decline;  //  "Decline";

                if (searchCriteria.approved != null && (bool)searchCriteria.approved)
                    gatewayMessage = Approved;   //"Approved";

                string modes = "";
                if (!string.IsNullOrEmpty(searchCriteria.Industry))
                {
                    switch (searchCriteria.Industry)
                    {
                        case "Manual":
                            modes = "Ecommerce";
                            break;
                        case "Swipe":
                            modes = "Retail";
                            break;
                        case "Chip":
                            modes = "Chip";
                            break;
                        case "Pin":
                            modes = "Pin";
                            break;
                    }
                }

                if (!String.IsNullOrEmpty(searchCriteria.ExpiryDate))
                    searchCriteria.ExpiryDate = StringExtension.DateFormat(searchCriteria.ExpiryDate);

                MtdTransactionsViewData td = new MtdTransactionsViewData
                {
                    ExpiryDate = searchCriteria.ExpiryDate,
                    VehicleNo = searchCriteria.VNumber,
                    DriverNo = searchCriteria.DNumber,
                    CardType = searchCriteria.CrdType,
                    startDate = searchCriteria.startDate,
                    endDate = searchCriteria.endDate,
                    Id = searchCriteria.Id,
                    AddRespData = gatewayMessage,
                    TxnType = searchCriteria.TType,
                    Industry = modes,
                    fk_FleetId = searchCriteria.fleetId, //entry mode treated as  fleetId
                    FirstFourDigits = searchCriteria.FFD,
                    LastFourDigits = searchCriteria.LFD,
                    MinAmount = searchCriteria.MinAmt,
                    MaxAmount = searchCriteria.MaxAmt,
                    AuthId = searchCriteria.AId,
                    MerchantId = merchantId,
                    JobNumber = searchCriteria.JobNumber,
                    PageSize = jtPageSize,
                    PageNumber = jtStartIndex,
                    Sorting = jtSorting,
                    EntryMode = searchCriteria.Industry,
                    IsVarified = searchCriteria.IsVarified,
                    UserId = userId,
                    SerialNo = searchCriteria.SerialNo,
                    TerminalId =  searchCriteria.TerminalId
                };

                _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");
                
                MTDTransactionDto transactionDto = td.ToDTO();
                
                _logMessage.Append("ToDTO method executed successfully");
                _logMessage.Append("calling GetTransactionCount method of _transactionService");
                
                int count = _subAdminService.GetTransactionCount(transactionDto);
                
                _logMessage.Append("GetTransactionCount method of _transactionService executed Successfully count = " + count);
                
                IEnumerable<MTDTransactionDto> stvd = _subAdminService.GetTransaction(transactionDto);

                stvd = stvd.Select(c => { c.IsChargeBackAllow = ChargeBack; return c; });
                stvd = stvd.Select(c => { c.AddRespData = c.AddRespData != null && c.AddRespData.Length > 12 ? c.AddRespData.Substring(0, 12).Insert(12, "...") : c.AddRespData; return c; });
                //stvd = stvd.Select(c => { c.StringDateTime = c.TxnDate != null ? TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(c.TxnDate), TimeZoneInfo.FindSystemTimeZoneById(Timezone)).ToString("MM/dd/yyyy HH:mm:ss") : TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(c.TxnDate), TimeZoneInfo.FindSystemTimeZoneById(Timezone)).ToString("MM/dd/yyyy HH:mm:ss"); return c; });

                stvd = stvd.Select(c => { c.StringDateTime = c.TxnDate != null 
                    ? TimeZoneInfo.ConvertTime(Convert.ToDateTime(c.TxnDate), TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(Timezone)).ToString("G", CultureInfo.GetCultureInfo(ConfigurationManager.AppSettings["CultureInfo"].ToString())) 
                    : TimeZoneInfo.ConvertTime(Convert.ToDateTime(c.TxnDate), TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(Timezone)).ToString("G", CultureInfo.GetCultureInfo(ConfigurationManager.AppSettings["CultureInfo"].ToString())); return c; });

                _logMessage.Append("GetTransaction method of _transactionService executed Successfully count");
                _logMessage.Append("assigning result in searchTransactionDtos from stvd");

                var searchTransactionDtos = stvd as MTDTransactionDto[] ?? stvd.ToArray();

                _logMessage.Append("result in searchTransactionDtos from stvd successfully executed");

                return Json(new { Result = "OK", Records = searchTransactionDtos, TotalRecordCount = count });
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        ///  Purpose            :  Fetching the Driverreport on the basis of its ID from the database.
        /// Function Name       :   DriverReport
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/19/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DriverReport()
        {
            try
            {
                if (Session["User"] == null) return RedirectToAction("Index", "Member");
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                    return RedirectToAction("index", "Member");

                var userId = (((SessionDto)Session["User"]).SessionUser.UserID);
                int? userTypeId = (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID);
                if (userTypeId == 5)
                {
                    IEnumerable<MerchantListDto> mvd = _subAdminService.GetMerchantList(userId);
                    List<MerchantListViewData> mld = mvd.Select(merchant => new MerchantListViewData
                    {
                        Company = merchant.Company,
                        fk_MerchantID = merchant.fk_MerchantID
                    }).ToList();

                    List<FleetListViewData> ob = new List<FleetListViewData>();
                    var driveTrx = new DriverTransaction { MerchantList = mld, FleetList = ob };
                    return View(driveTrx);
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :  Searching Driverreport 
        /// Function Name       :   DriverReport
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/19/2015
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DriverReport(DriverTransaction tvd)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

            int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
            if (userType == 1 || userType == 5)
                tvd.MerchantID = tvd.MerchantID;
            else
                tvd.MerchantID = (int)(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);

            // Now we know the merchant, hopefully, we can get a timezone.
            if (tvd.MerchantID != null)
                Timezone = _commonService.MerchantTimeZone((int)tvd.MerchantID);
            else
                Timezone = TimeZoneInfo.Local.StandardName;

            // We have to do the date Timezone manipulation after getting the Merchant details.
            if (tvd.startDateString != null && tvd.endDateString != null)
            {
                bool isDate = false;
                DateTime resultDate;
                string sDate = tvd.startDateString + " 00:00";
                string eDate = tvd.endDateString + " 23:59";

                isDate = DateTime.TryParseExact(sDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                if (isDate)
                    tvd.transaction.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones

                isDate = DateTime.TryParseExact(eDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                if (isDate)
                    tvd.transaction.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
            }

            if (tvd.transaction.DriverNo == "--Select Driver--")
                tvd.transaction.DriverNo = null;

            if (tvd.FleetID == 0)
                tvd.FleetID = null;

            MtdTransactionsViewData td = new MtdTransactionsViewData();

            td.FleetId = tvd.FleetID;
            td.PageSize = 0;
            td.MerchantId = tvd.MerchantID;
            td.VehicleNo = tvd.transaction.VehicleNo;
            td.DriverNo = tvd.transaction.DriverNo;
            td.startDate = tvd.transaction.startDate;
            td.endDate = tvd.transaction.endDate;

            _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");

            MTDTransactionDto transactionDto = td.ToDTO();
            
            _logMessage.Append("ToDTO method executed successfully");
            _logMessage.Append("calling GetDriverCount method of _transactionService");
            
            int count = _subAdminService.GetDriverCount(transactionDto);
            transactionDto.PageSize = count;
            
            _logMessage.Append("calling GetDriver method of _transactionService takes transaction dto");
            
            IEnumerable<DriverTransactionResult> stvd = _subAdminService.GetDriver(transactionDto);
            int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
            var userfleet = _userService.GetUsersFleet(userId);//2
            stvd = stvd.Where(x => userfleet.Select(y => y.fk_FleetID).Contains((int)x.FleetID)).ToList();
            
            _logMessage.Append("GetDriver method of _transactionService executed successfully");
            _logMessage.Append("assigning result in searchTransactionDtos from stvd");
            
            var searchTransactionDtos = stvd as DriverTransactionResult[] ?? stvd.ToArray();
            
            _logMessage.Append("result in searchTransactionDtos from stvd successfully executed");

            using (StringWriter output = new StringWriter())
            {
                //First line for column names
                output.WriteLine("\"Merchant Name\",\"Driver Name\",\"Driver No\",\"Car No\",\"Fleet\",\"Number Of Keyed\",\"Keyed Sale Amt\",\"No.Of Swiped\",\"Swiped Amt\",\"Tech Fee\",\"Fleet Fee\",\"Fee\",\"Surcharge\",\"Total Sale\",\"Total  Amount\"");

                foreach (DriverTransactionResult item in searchTransactionDtos)
                {
                    output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\"", item.MerchantName, item.DriverName, item.DriverNo, item.VehicleNo, item.Fleet, item.NumberOfKeyed, item.KeyedSaleAmount, item.NumberOfSwiped, item.SwipedSaleAmount,
                        item.TechFee, item.SumFleetFee, item.Fee, item.SumSurcharge, item.TotalNumberOfSale, item.TotalSaleAmount);
                }

                _logTracking.Append("MyMerchantsController.DriverReport() Driver Report Excel file Downloaded by " + (((SessionDto)Session["User"]).SessionUser.Email) + " at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());

                //return File(new ASCIIEncoding().GetBytes(output.ToString()), "application/CSV", "Driver_Transaction_Report.csv");
                return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Driver_Transaction_Report.csv");
            }
        }

        /// <summary>
        ///  Purpose            :  Page indexing on Driver report
        /// Function Name       :   DriverReportListByFiter
        ///Created By          :   Salil Gupta
        ///Created On          :   2/11/2015
        ///Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult DriverReportListByFiter(SearchParameters searchCriteria, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                int? mid = null;
                if (searchCriteria.MerchantId != null)
                    mid = searchCriteria.MerchantId;
                else
                    mid = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;

                if (searchCriteria.startDateString != null && searchCriteria.endDateString != null)
                {
                    if (searchCriteria.startDateString.Length > 0 && searchCriteria.endDateString.Length > 0)
                    {
                        // Now we know the merchant, hopefully, we can get a timezone.
                        if (mid != null)
                            Timezone = _commonService.MerchantTimeZone((int)mid);
                        else
                            Timezone = TimeZoneInfo.Local.StandardName;

                        bool isDate = false;
                        Regex r = new Regex(@"\d\d-\d\d-\d\d\d\d");
                        if (((r.Match(searchCriteria.startDateString).Success) && (r.Match(searchCriteria.endDateString).Success)))
                        {
                            DateTime resultDate;
                            string sDate = searchCriteria.startDateString + " 00:00";
                            string eDate = searchCriteria.endDateString + " 23:59";

                            isDate = DateTime.TryParseExact(sDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);                            
                            if (!isDate)
                                return Json(new { Result = "OK", Records = (IEnumerable<DriverTransactionResult>)null, TotalRecordCount = 0 });

                            searchCriteria.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones

                            isDate = DateTime.TryParseExact(eDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return Json(new { Result = "OK", Records = (IEnumerable<DriverTransactionResult>)null, TotalRecordCount = 0 });

                            searchCriteria.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
                        }
                    }
                }

                if (searchCriteria.DNumber == "--Select Driver--")
                    searchCriteria.DNumber = null;

                if (searchCriteria.VNumber == "--Select Car--")
                    searchCriteria.VNumber = null;

                if (searchCriteria.fleetId == 0)
                    searchCriteria.fleetId = null;

                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var dto = (SessionDto)Session["User"];
                int userId = dto.SessionUser.UserID;

                MtdTransactionsViewData td = new MtdTransactionsViewData
                {
                    MerchantId = mid,
                    FleetId = Convert.ToInt32(searchCriteria.fleetId),
                    VehicleNo = searchCriteria.VNumber,
                    DriverNo = searchCriteria.DNumber,
                    startDate = searchCriteria.startDate,
                    endDate = searchCriteria.endDate,
                    PageSize = jtPageSize,
                    PageNumber = jtStartIndex,
                    Sorting = jtSorting,
                    UserId = userId
                };

                _logMessage.Append( "calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");

                MTDTransactionDto transactionDto = td.ToDTO();
                
                _logMessage.Append("ToDTO method executed successfully");
                
                int count = _subAdminService.GetDriverCount(transactionDto);
                IEnumerable<DriverTransactionResult> stvd = _subAdminService.GetDriver(transactionDto);

                _logMessage.Append("GetDriver method of _transactionService executed successfully");
                _logMessage.Append("assigning result in searchTransactionDtos from stvd");
                
                var searchTransactionDtos = stvd as DriverTransactionResult[] ?? stvd.ToArray();
                
                _logMessage.Append("result in searchTransactionDtos from stvd successfully executed");

                return Json(new { Result = "OK", Records = searchTransactionDtos, TotalRecordCount = count });
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult TechnologyDetail()
        {
            try
            {
                if (Session["User"] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 5) return RedirectToAction("Index", "Member");
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                    return RedirectToAction("index", "Member");
                var userId = (((SessionDto)Session["User"]).SessionUser.UserID);
                IEnumerable<MerchantListDto> mvd = _subAdminService.GetMerchantList(userId);
                
                List<MerchantListViewData> mld = mvd.Select(merchant => new MerchantListViewData
                {
                    Company = merchant.Company,
                    fk_MerchantID = merchant.fk_MerchantID
                }).ToList();

                List<FleetListViewData> ob = new List<FleetListViewData>();
                var driveTrx = new TechDetailViewData { MerchantList = mld, FleetList = ob };
                return View(driveTrx);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// Purpose             :  Technology detail report
        /// Function Name       :   TechDetail
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/19/2015
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult TechnologyDetail(TechDetailViewData tvd)
        {
            if (Session["User"] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 5) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            if (tvd.FleetID == 0)
                tvd.FleetID = null;

            // Now we know the merchant, hopefully, we can get a timezone.
            if (tvd.MerchantID != null)
                Timezone = _commonService.MerchantTimeZone((int)tvd.MerchantID);
            else
                Timezone = TimeZoneInfo.Local.StandardName;

            // We have to do the date Timezone manipulation after getting the Merchant details.
            if (tvd.startDateString != null && tvd.endDateString != null)
            {
                bool isDate = false;
                DateTime resultDate;
                string sDate = tvd.startDateString + " 00:00";
                string eDate = tvd.endDateString + " 23:59";

                isDate = DateTime.TryParseExact(sDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                if (isDate)
                    tvd.transaction.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones

                isDate = DateTime.TryParseExact(eDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                if (isDate)
                    tvd.transaction.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
            }

            if (tvd.transaction.DriverNo == "--Select Driver--")
                tvd.transaction.DriverNo = null;

            if (tvd.transaction.VehicleNo == "--Select Car--")
                tvd.transaction.VehicleNo = null;

            int userId = (((SessionDto)Session["User"]).SessionUser.UserID);

            MtdTransactionsViewData td = new MtdTransactionsViewData();
            td.FleetId = tvd.FleetID;
            td.PageSize = 0;
            td.MerchantId = tvd.MerchantID;
            td.DriverNo = tvd.transaction.DriverNo;
            td.VehicleNo = tvd.transaction.VehicleNo;
            td.startDate = tvd.transaction.startDate;
            td.endDate = tvd.transaction.endDate;
            td.UserId = userId;

            _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");

            ////D#6082 Need to change the TxnDate to a local date\time for the Merchant\fleet for display.
            //if (td.MerchantId != null)
            //    Timezone = _commonService.MerchantTimeZone((int)td.MerchantId);
            //else
            //    Timezone = TimeZoneInfo.Local.StandardName;

            //td.TxnDate = TimeZoneInfo.ConvertTime(Convert.ToDateTime(td.TxnDate), TimeZoneInfo.Local, TimeZoneInfo.FindSystemTimeZoneById(Timezone));

            // Convert the transaction view data to data object.
            MTDTransactionDto transactionDto = td.ToDTO();

            _logMessage.Append("ToDTO method executed successfully");
            _logMessage.Append("calling GetDriverCount method of _transactionService");

            int count = _subAdminService.GetTechFeeDetailCount(transactionDto);
            transactionDto.PageSize = count;

            if (count == 0)
            {
                IEnumerable<MerchantListDto> mvd = _subAdminService.GetMerchantList(userId);
                List<MerchantListViewData> mld = mvd.Select(merchant => new MerchantListViewData
                {
                    Company = merchant.Company,
                    fk_MerchantID = merchant.fk_MerchantID
                }).ToList();

                List<FleetListViewData> ob = new List<FleetListViewData>();
                tvd.MerchantList = mld;
                tvd.FleetList = ob;
                ViewBag.Message = MtdataResource.No_such_record_exist;

                return View(tvd);
            }

            _logMessage.Append("GetTechFeeCount method of _transactionService executed Successfully count = " + count);
            _logMessage.Append("calling GetTechnologyDetail method of _transactionService takes transaction dto");
        
            IEnumerable<TechnologyDetailReportDto> stvd = _subAdminService.GetTechnologyDetail(transactionDto);
            
            _logMessage.Append("GetTechnologyDetail method of _transactionService executed successfully");
            _logMessage.Append("assigning result in searchTransactionDtos from stvd");
            
            var searchTransactionDtos = stvd as TechnologyDetailReportDto[] ?? stvd.ToArray();
            
            _logMessage.Append("result in searchTransactionDtos from stvd successfully executed");

            using (StringWriter output = new StringWriter())
            {
                string dateFromHeading = "DATE FROM :" + tvd.startDateString;
                string dateToHeading = "DATE TO :" + tvd.endDateString;

                output.WriteLine(dateFromHeading.PadLeft(80));
                output.WriteLine(dateToHeading.PadLeft(80));

                string merchants = "";
                string fleets = "";
                
                List<string> listMerchant = new List<string>();
                List<string> listFleet = new List<string>();
                foreach (TechnologyDetailReportDto item in searchTransactionDtos)
                {
                    listMerchant.Add(item.Merchant);
                    listFleet.Add(item.FleetName);
                }

                foreach (string merchant in listMerchant.Distinct())
                {
                    if (string.IsNullOrEmpty(merchants))
                        merchants = merchant;
                    else
                        merchants = merchants + ", " + merchant;
                }

                foreach (string fleet in listFleet.Distinct())
                {
                    if (string.IsNullOrEmpty(fleets))
                        fleets = fleet;
                    else
                        fleets = fleets + ", " + fleet;
                }

                string merchantList = "MERCHANT :" + merchants;
                string fleetList = "FLEET:" + fleets;

                output.WriteLine(merchantList);
                output.WriteLine(fleetList);
                output.WriteLine();
                output.WriteLine();

                string culture = ConfigurationManager.AppSettings["CultureInfo"].ToString();

                // First line for column names
                output.WriteLine("\"Trans Id\",\"Date\",\"Fleet\",\"Vehicle\",\"Driver\",\"Fare Amt\",\"Card Type\",\"Tech Fee\",\"Surcharge\",\"Fee\",\"Auth Code\"");
                foreach (TechnologyDetailReportDto item in searchTransactionDtos)
                {
                    if (item.PayType == 1 || item.PayType == 2)
                    {
                        item.FleetFee = "NA";
                    }

                    DateTime dt1 = TimeZoneInfo.ConvertTime(Convert.ToDateTime(item.TxnDate), TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(Timezone));

                    output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\"", item.TransId, 
                        dt1.ToString("G", CultureInfo.GetCultureInfo(culture)),
                        item.FleetName, item.VehicleNo, item.DriverNo, item.Amount, item.CardType, item.TechFee, item.Surcharge, item.Booking_Fee, item.AuthId);
                }

                _logTracking.Append("MyMerchantsController.TechnologyDetail() Driver Report Excel file Downloaded by " + (((SessionDto)Session["User"]).SessionUser.Email) + " at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());

                return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Technology_Detail_Report.csv");
            }
        }

        /// <summary>
        ///  Purpose            :   Technology report view
        /// Function Name       :   TechSummary
        /// Created By          :   Naveen Kumar
        /// Created On          :   6/10/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult TechnologySummary()
        {
            try
            {
                if (Session["User"] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 5) return RedirectToAction("Index", "Member");
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                    return RedirectToAction("index", "Member");
                var userId = (((SessionDto)Session["User"]).SessionUser.UserID);
                
                IEnumerable<MerchantListDto> mvd = _subAdminService.GetMerchantList(userId);
                List<MerchantListViewData> mld = mvd.Select(merchant => new MerchantListViewData
                {
                    Company = merchant.Company,
                    fk_MerchantID = merchant.fk_MerchantID
                }).ToList();

                List<FleetListViewData> ob = new List<FleetListViewData>();
                var driveTrx = new TechSummaryViewData { MerchantList = mld, FleetList = ob };
                return View(driveTrx);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :  Technology Summary Report
        /// Function Name       :   TechSummary
        /// Created By          :  Naveen Kumar
        /// Created On          :   1/19/2015
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult TechnologySummary(TechSummaryViewData tvd)
        {
            if (Session["User"] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 5) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            
            tvd.MerchantID = tvd.MerchantID;
            if (tvd.FleetID == 0)
                tvd.FleetID = null;

            // Now we know the merchant, hopefully, we can get a timezone.
            if (tvd.MerchantID != null)
                Timezone = _commonService.MerchantTimeZone((int)tvd.MerchantID);
            else
                Timezone = TimeZoneInfo.Local.StandardName;

            // We have to do the date Timezone manipulation after getting the Merchant details.
            if (tvd.startDateString != null && tvd.endDateString != null)
            {
                bool isDate = false;
                DateTime resultDate;
                string sDate = tvd.startDateString + " 00:00";
                string eDate = tvd.endDateString + " 23:59";

                isDate = DateTime.TryParseExact(sDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                if (isDate)
                    tvd.transaction.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones

                isDate = DateTime.TryParseExact(eDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                if (isDate)
                    tvd.transaction.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
            }

            var dto = (SessionDto)Session["User"];
            int userIds = dto.SessionUser.UserID;
            
            MtdTransactionsViewData td = new MtdTransactionsViewData();
            td.FleetId = tvd.FleetID;
            td.PageSize = 0;
            td.MerchantId = tvd.MerchantID;
            td.startDate = tvd.transaction.startDate;
            td.endDate = tvd.transaction.endDate;
            td.UserId = userIds;
            
            _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");
            
            MTDTransactionDto transactionDto = td.ToDTO();
            
            _logMessage.Append("ToDTO method executed successfully");
            _logMessage.Append("calling GetDriverCount method of _transactionService");
            
            int count = _subAdminService.GetTechFeeCount(transactionDto);
            transactionDto.PageSize = count;

            if (count == 0)
            {

                IEnumerable<MerchantListDto> mvd = _subAdminService.GetMerchantList(userIds);
                List<MerchantListViewData> mld = mvd.Select(merchant => new MerchantListViewData
                {
                    Company = merchant.Company,
                    fk_MerchantID = merchant.fk_MerchantID
                }).ToList();

                List<FleetListViewData> ob = new List<FleetListViewData>();
                tvd.MerchantList = mld;
                tvd.FleetList = ob;
                ViewBag.Message = MtdataResource.No_such_record_exist;
                return View(tvd);
            }

            _logMessage.Append("GetTechFeeCount method of _transactionService executed Successfully count = " + count);
            _logMessage.Append("calling GetTechnologySummary method of _transactionService takes transaction dto");

            IEnumerable<TechnologySummaryReportDto> stvd = _subAdminService.GetTechnologySummary(transactionDto);
            
            _logMessage.Append("GetTechnologySummary method of _transactionService executed successfully");
            _logMessage.Append("assigning result in searchTransactionDtos from stvd");
            
            var searchTransactionDtos = stvd as TechnologySummaryReportDto[] ?? stvd.ToArray();
            
            _logMessage.Append("result in searchTransactionDtos from stvd successfully executed");

            using (StringWriter output = new StringWriter())
            {
                string dateFromHeading = "DATE FROM :" + tvd.startDateString;
                string dateToHeading = "DATE TO :" + tvd.endDateString;
                output.WriteLine(dateFromHeading.PadLeft(80));
                output.WriteLine(dateToHeading.PadLeft(80));
                string merchants = "";
                string fleets = "";
                
                List<string> listMerchant = new List<string>();
                List<string> listFleet = new List<string>();

                foreach (TechnologySummaryReportDto item in searchTransactionDtos)
                {
                    listMerchant.Add(item.Merchant);
                    listFleet.Add(item.FleetName);
                }
                
                foreach (string merchant in listMerchant.Distinct())
                {
                    if (string.IsNullOrEmpty(merchants))
                        merchants = merchant;
                    else
                        merchants = merchants + ", " + merchant;
                }

                foreach (string fleet in listFleet.Distinct())
                {
                    if (string.IsNullOrEmpty(fleets))
                        fleets = fleet;
                    else
                        fleets = fleets + ", " + fleet;
                }

                string merchantList = "MERCHANT :" + merchants;
                string fleetList = "FLEET:" + fleets;
                output.WriteLine(merchantList);
                output.WriteLine(fleetList);
                output.WriteLine();
                output.WriteLine();

                //First line for column names
                output.WriteLine("\"Fleet\",\"Vehicle Transacting\",\"Automatic Transactions\",\"Manual Transactions\",\"Total Chargebacks\",\"Total Voids\",\"Total Trans\",\"Tech Fee\",\"Surcharge\",\"Fee\",\"Amount\"");
                foreach (TechnologySummaryReportDto item in searchTransactionDtos)
                {
                    if (item.PayType == 1 || item.PayType == 2)
                    {
                        item.FleetFee = "NA";
                    }

                    output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\"", item.FleetName, item.CountVehicle, item.AutomaticTrans, item.ManualTrans, item.CountRefund, item.CountVoid, item.TotalTrans, item.TechFee, item.Surcharge, item.Booking_Fee, item.TotalAmt);
                }

                _logTracking.Append("MyMerchantsController.TechnologySummary() Driver Report Excel file Downloaded by " + (((SessionDto)Session["User"]).SessionUser.Email) + " at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());

                return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Technology_Summary_Report.csv");
            }
        }

        /// <summary>
        ///  Purpose            :  Page indexing on Driver report
        /// Function Name       :   DriverReportListByFiter
        ///Created By          :   Salil Gupta
        ///Created On          :   2/11/2015
        ///Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult TechnologyFeeSummaryListByFiter(SearchParameters searchCriteria, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            int? mid = null;
            if (searchCriteria.MerchantId != null)
                mid = searchCriteria.MerchantId;
            else
                mid = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;

            try
            {
                if (searchCriteria.startDateString != null && searchCriteria.endDateString != null)
                {
                    if (searchCriteria.startDateString.Length > 0 && searchCriteria.endDateString.Length > 0)
                    {
                        // Now we know the merchant, hopefully, we can get a timezone.
                        if (mid != null)
                            Timezone = _commonService.MerchantTimeZone((int)mid);
                        else
                            Timezone = TimeZoneInfo.Local.StandardName;

                        bool isDate = false;
                        Regex r = new Regex(@"\d\d-\d\d-\d\d\d\d");
                        if (((r.Match(searchCriteria.startDateString).Success) && (r.Match(searchCriteria.endDateString).Success)))
                        {
                            DateTime resultDate;
                            string sDate = searchCriteria.startDateString + " 00:00";
                            string eDate = searchCriteria.endDateString + " 23:59";

                            isDate = DateTime.TryParseExact(sDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return Json(new { Result = "OK", Records = (IEnumerable<TechnologySummaryReportDto>)null, TotalRecordCount = 0 });

                            searchCriteria.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
                            
                            isDate = DateTime.TryParseExact(eDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return Json(new { Result = "OK", Records = (IEnumerable<TechnologySummaryReportDto>)null, TotalRecordCount = 0 });

                            searchCriteria.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
                        }
                    }
                }
        
                var dto = (SessionDto)Session["User"];
                int userId = dto.SessionUser.UserID;
                
                if (searchCriteria.fleetId == 0)
                    searchCriteria.fleetId = null;

                if (searchCriteria.fleetId == null && searchCriteria.MerchantId == null && searchCriteria.startDate == null && searchCriteria.endDate == null)
                    return Json(new { Result = "OK", Records = (IEnumerable<TechnologySummaryReportDto>)null, TotalRecordCount = 0 });

                MtdTransactionsViewData td = new MtdTransactionsViewData
                {
                    MerchantId = mid,
                    FleetId = searchCriteria.fleetId,
                    startDate = searchCriteria.startDate,
                    endDate = searchCriteria.endDate,
                    PageSize = jtPageSize,
                    PageNumber = jtStartIndex,
                    Sorting = jtSorting,
                    UserId = userId
                };

                _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");
                
                MTDTransactionDto transactionDto = td.ToDTO();
                
                _logMessage.Append("ToDTO method executed successfully");

                int count = _subAdminService.GetTechFeeCount(transactionDto);
                IEnumerable<TechnologySummaryReportDto> stvd = _subAdminService.GetTechnologySummary(transactionDto);

                foreach (TechnologySummaryReportDto item in stvd)
                {
                    if (item.PayType == 1 || item.PayType == 2)
                        item.FleetFee = "NA";
                }

                _logMessage.Append("GetTechnologySummary method of _transactionService executed successfully");
                _logMessage.Append("assigning result in TechnologySummaryReportDto from stvd");
                
                var searchTransactionDtos = stvd as TechnologySummaryReportDto[] ?? stvd.ToArray();
                
                _logMessage.Append("result in TechnologySummaryReportDto from stvd successfully executed");
                
                return Json(new { Result = "OK", Records = searchTransactionDtos, TotalRecordCount = count });
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        ///  Purpose            :  Page indexing on Driver report
        /// Function Name       :   DriverReportListByFiter
        ///Created By          :   Salil Gupta
        ///Created On          :   2/11/2015
        ///Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult TechnologyDetailListByFilter(SearchParameters searchCriteria, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                int? mid = null;
                if (searchCriteria.MerchantId != null)
                    mid = searchCriteria.MerchantId;
                else
                    mid = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;

                if (searchCriteria.startDateString != null && searchCriteria.endDateString != null)
                {
                    if (searchCriteria.startDateString.Length > 0 && searchCriteria.endDateString.Length > 0)
                    {
                        // Now we know the merchant, hopefully, we can get a timezone.
                        if (mid != null)
                            Timezone = _commonService.MerchantTimeZone((int)mid);
                        else
                            Timezone = TimeZoneInfo.Local.StandardName;

                        bool isDate = false;
                        Regex r = new Regex(@"\d\d-\d\d-\d\d\d\d");
                        if (((r.Match(searchCriteria.startDateString).Success) && (r.Match(searchCriteria.endDateString).Success)))
                        {
                            DateTime resultDate;
                            string sDate = searchCriteria.startDateString + " 00:00";
                            string eDate = searchCriteria.endDateString + " 23:59";

                            isDate = DateTime.TryParseExact(sDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return Json(new { Result = "OK", Records = (IEnumerable<TechnologyDetailReportDto>)null, TotalRecordCount = 0 });

                            searchCriteria.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
                            
                            isDate = DateTime.TryParseExact(eDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return Json(new { Result = "OK", Records = (IEnumerable<TechnologyDetailReportDto>)null, TotalRecordCount = 0 });

                            searchCriteria.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
                        }
                    }
                }

                if (searchCriteria.fleetId == 0)
                    searchCriteria.fleetId = null;
                
                if (searchCriteria.DNumber == "--Select Driver--" || searchCriteria.DNumber == "0")
                    searchCriteria.DNumber = null;
                
                if (searchCriteria.VNumber == "--Select Car--" || searchCriteria.VNumber == "0")
                    searchCriteria.VNumber = null;
                
                if (searchCriteria.fleetId == null && searchCriteria.MerchantId == null && searchCriteria.DNumber == null && searchCriteria.VNumber == null && searchCriteria.startDate == null && searchCriteria.endDate == null)
                    return Json(new { Result = "OK", Records = (IEnumerable<TechnologyDetailReportDto>)null, TotalRecordCount = 0 });
                
                var dto = (SessionDto)Session["User"];
                int userId = dto.SessionUser.UserID;
                MtdTransactionsViewData td = new MtdTransactionsViewData
                {
                    MerchantId = mid,
                    FleetId = searchCriteria.fleetId,
                    DriverNo = searchCriteria.DNumber,
                    VehicleNo = searchCriteria.VNumber,
                    startDate = searchCriteria.startDate,
                    endDate = searchCriteria.endDate,
                    PageSize = jtPageSize,
                    PageNumber = jtStartIndex,
                    Sorting = jtSorting,
                    UserId = userId
                };

                _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");
                
                MTDTransactionDto transactionDto = td.ToDTO();
                
                _logMessage.Append("ToDTO method executed successfully");
                _logMessage.Append("calling GetTechFeeDetailCount method of _transactionService");
                
                int count = _subAdminService.GetTechFeeDetailCount(transactionDto);
                IEnumerable<TechnologyDetailReportDto> stvd = _subAdminService.GetTechnologyDetail(transactionDto);

                string culture = ConfigurationManager.AppSettings["CultureInfo"].ToString();

                foreach (TechnologyDetailReportDto item in stvd)
                {
                    if (item.PayType == 1 || item.PayType == 2)
                        item.FleetFee = "NA";
                }

                //D#6082 Ensure TxnDate reflected as local timezone for merchant.
                stvd = stvd.Select(c =>
                {
                    c.StringDateTime = c.TxnDate != null
                        ? TimeZoneInfo.ConvertTime(Convert.ToDateTime(c.TxnDate), TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(Timezone)).ToString("G", CultureInfo.GetCultureInfo(ConfigurationManager.AppSettings["CultureInfo"].ToString()))
                        : TimeZoneInfo.ConvertTime(Convert.ToDateTime(c.TxnDate), TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(Timezone)).ToString("G", CultureInfo.GetCultureInfo(ConfigurationManager.AppSettings["CultureInfo"].ToString())); return c;
                });

                _logMessage.Append("GetTechnologySummary method of _transactionService executed successfully");
                _logMessage.Append("assigning result in TechnologySummaryReportDto from stvd");
                
                var searchTransactionDtos = stvd as TechnologyDetailReportDto[] ?? stvd.ToArray();
                
                _logMessage.Append("result in TechnologyDetailReportDto from stvd successfully executed");
                
                return Json(new { Result = "OK", Records = searchTransactionDtos, TotalRecordCount = count });
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        /// Purpose             :   To get the fleet
        /// Function Name       :   GetFleet
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   19/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>   
        /// <param name="id"></param>      
        /// <returns></returns>
        public JsonResult GetFleet(int id)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                _logMessage.Append("calling GetFleetList method of _reportcommonService takes id= " + id);

                var fleetDtoList = _reportcommonService.GetFleetList(id);

                _logMessage.Append("GetFleetList method of _reportcommonService class executed successfully");
                _logMessage.Append("taking records from fleetDtoList and adding to list<FleetListViewData>");

                List<FleetListViewData> objFleetlist = fleetDtoList.Select(fvd => new FleetListViewData { FleetName = fvd.FleetName, FleetID = fvd.FleetID }).ToList();

                var dto = (SessionDto)Session["User"];
                int userId = dto.SessionUser.UserID;
                
                IEnumerable<UserFleetViewData> Myfleetlists = _subAdminService.GetUsersFleet(userId).ToViewDataList();

                //SH 2016-07-18 T#6162 Delete following when proven working.
                //Compiler warning says this is not required.   Myfleetlists = Myfleetlists.Where(x => x.fk_FleetID != null);

                Myfleetlists = Myfleetlists.Where(x => objFleetlist.Select(y => y.FleetID).Contains((int)x.fk_FleetID)).ToList();
                objFleetlist = Myfleetlists.Select(Myfleetlistst => new FleetListViewData
                     {
                         FleetID = Myfleetlistst.fk_FleetID,
                         FleetName = Myfleetlistst.FleetName
                     }).ToList();

                _logMessage.Append("get records from fleetDtoList successfully");

                var fleet = new List<SelectListItem> { new SelectListItem { Text = MtdataResource.Select_Fleet, Value = "0" } };

                _logMessage.Append("taking records from objFleetlist and adding to list<SelectListItem>");
                
                fleet.AddRange(from fleetView in objFleetlist where true select new SelectListItem { Text = fleetView.FleetName, Value = Convert.ToString(fleetView.FleetID) });
                
                _logMessage.Append("get records from objFleetlist successfully");
                
                return Json(new SelectList(fleet, "Value", "Text"));
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

        /// <summary>
        /// Purpose             :   To get list of fleets of merchant
        /// Function Name       :   GetMerchantDriverRec
        /// Created By          :   Shishir Saunakia
        /// Created On          :   10/10/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetMerchantDriverRec()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 5)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                DriverRecListViewData driverObj = new DriverRecListViewData();
                
                _logMessage.Append("fetching fleet list and map to view data");
                
                if (userType == 2 || userType == 3)
                {
                    driverObj.fleetlist = _userService.FleetList(merchantId).ToViewDataList();
                    driverObj.ModifiedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                    driverObj.loggedUser = Convert.ToString(((SessionDto)Session["User"]).SessionUser.fk_UserTypeID);
                    return View(driverObj);
                }

                if (userType == 5)
                {
                    IEnumerable<UserFleetViewData> Myfleetlists = _subAdminService.GetUsersFleet(userId).ToViewDataList();
                    driverObj.fleetlist = Myfleetlists.Select(driver => new FleetViewData
                    {
                        FleetID = driver.fk_FleetID,
                        FleetName = driver.FleetName
                    }).ToList();
                }

                driverObj.ModifiedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                driverObj.loggedUser = Convert.ToString(((SessionDto)Session["User"]).SessionUser.fk_UserTypeID);
                
                return View(driverObj);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   To save and update driver and user list
        /// Function Name       :   GetMerchantDriverRec
        /// Created By          :   Shishir Saunakia
        /// Created On          :   10/10/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="DriverRecipientViewdata"></param>
        /// <returns></returns>
        [HttpPost]
        public string GetMerchantDriverRec(DriverRecListViewData DriverRecipientViewdata)
        {
            string Msg = string.Empty;
            if (Session["User"] == null) return string.Empty;
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null && ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 5)
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                
                bool response = false;
                try
                {
                    DriverRecListDto obj = new DriverRecListDto();
                    obj = DriverRecipientViewdata.toDto();
                    int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                    int loggeUser = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                    IEnumerable<DriverRecipientDto> DriverRecObj = DriverRecipientViewdata.UserList.TorecipientDtoList();
                    int fleetId = Convert.ToInt32(DriverRecipientViewdata.fk_FleetId);
                    int userType = Convert.ToInt32(DriverRecipientViewdata.UserType);
                    if (loggeUser == 2 || loggeUser == 3)
                        response = _driverService.MerchantDriverRecipient(obj, fleetId, userType, merchantId);
                    if (loggeUser == 5)
                        response = _driverService.UpdateDriverRecipient(obj, fleetId, loggeUser, merchantId);

                    if (response)
                    {
                        Msg = "Saved successfully";
                    }

                    return Msg;
                }
                catch (Exception exception)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), exception);
                }
            }

            return null;
        }

        /// <summary>
        /// Purpose             :   To get list of fleets of merchant and driver and user list
        /// Created By          :   Shishir Saunakia
        /// Created On          :   10/10/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetDriverRecipient(int fleetId)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            try
            {
                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                DriverRecListViewData driverObj = new DriverRecListViewData();
                _logMessage.Append("fetching driver recipient and map to view data");
                driverObj.UserList = _driverService.GetDriverReceipent(merchantId, fleetId).ToDriverViewDataList();
                driverObj.ModifiedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                return Json(driverObj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return Json(null);
        }

        /// <summary>
        /// Created By          :   Shishir Saunakia
        /// Created On          :   10/10/2015
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetAllDriverRecipient()
        {
            int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
            
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            
            try
            {
                DriverRecListViewData driverObj = new DriverRecListViewData();
                
                _logMessage.Append("fetching driver recipient and map to view data");

                int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                
                if (userType == 1)
                    driverObj.UserList = _driverService.GetDriverReceipent().ToDriverViewDataList().Distinct();
                
                if (userType == 2 || userType == 3)
                    driverObj.UserList = _driverService.GetMerchantDriverRecipient(merchantId).ToDriverViewDataList().Distinct();
                
                if (userType == 5)
                {
                    var merchantUserList = _subAdminService.GetUserMerchant(userId).ToList();
                    string merchantListID = string.Join(",", merchantUserList.Select(x => x.MerchantID));
                    var fleetlistId = _subAdminService.GetFleet(merchantListID, userType, userId).ToViewDataList();
                    string fleetListIds = string.Join(",", fleetlistId.Select(x => x.FleetID));
                    driverObj.UserList = _subAdminService.GetUserDriverReceipent(fleetListIds).ToDriverViewDataList().Distinct();
                }

                driverObj.ModifiedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                return Json(driverObj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return Json(null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OnDemandReport()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            var sDto = ((SessionDto)Session["User"]).SessionUser;
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || sDto.fk_UserTypeID != 5)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int cUsrId = sDto.UserID;
                OnDemandReportViewData sv = new OnDemandReportViewData();
                
                string v = Request.QueryString["sort"];
                string p = Request.QueryString["page"];

                if (v != null || p != null)
                {
                    sv.fk_ReportID = Convert.ToInt32(TempData["repValue"]);
                    sv.DateFrom = Convert.ToDateTime(TempData["DateFrom"]);
                    sv.DateTo = Convert.ToDateTime(TempData["DateTo"]);
                    sv.fk_MerchantId = Convert.ToInt32(TempData["MerValue"]);
                    sv.IsReport = true;
                    TempData.Keep("repValue");
                    TempData.Keep("DateFrom");
                    TempData.Keep("DateTo");
                    TempData.Keep("MerValue");

                    switch (sv.fk_ReportID)
                    {
                        case 22://Corp User Exception Report
                            break;

                        case 25://Corp User Card Type Report
                            IEnumerable<GetCardTypeReportDto> muCardDto = _transactionService.GetCorpUserCardTypeReport(sv.DateFrom, sv.DateTo, cUsrId);
                            IEnumerable<GetCardTypeReportViewData> muCardviewData = muCardDto.ToCardTypeList();
                            sv.GetCardTypeReport = muCardviewData;
                            break;

                        case 26://Corp User Total Transactions By Vehicle Report
                            IEnumerable<GetTotalTransByVehicleReportDto> muVehicleDto = _transactionService.CorpUserGetTotalTransByVehicleReport(sv.DateFrom, sv.DateTo, cUsrId);
                            IEnumerable<GetTotalTransByVehicleReportViewData> merchantuTotalView = muVehicleDto.ToMerchantTotalTransList();
                            sv.AdminTotalTransactionsByVehicle = merchantuTotalView;
                            break;
                    }
                }

                sv.ReportList = ReportList(23, 27);

                return View(sv);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sv"></param>
        /// <returns></returns>
        [ActionName("OnDemandReport")]
        [AcceptVerbs(HttpVerbs.Post)]
        [AttributeModel(Name = "UNumber", Data = "Email Report")]
        public ActionResult EmailUserReport(OnDemandReportViewData sv)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            var sDto = ((SessionDto)Session["User"]).SessionUser;
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || sDto.fk_UserTypeID != 5)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int runStatus = 0;
                sv.ReportList = ReportList(23, 27);
                
                ViewBag.Message = "Please configure admin or merchant first.";

                int? cUsrId = sDto.UserID;
                switch (sv.fk_ReportID)
                {
                    case 24://Corporate Exception Report
                        runStatus = _transactionService.CorpUserExceptionReportScheduler(true, sv.DateFrom, sv.DateTo, cUsrId);
                        break;

                    case 25://Corporate User Card Type Report
                        runStatus = _transactionService.CorpUserCardTypeReportScheduler(true, sv.DateFrom, sv.DateTo, cUsrId);
                        break;

                    case 26://Corporate Total Transactions By Vehicle Report
                        runStatus = _transactionService.CorpUserTotalTransByVehicleScheduler(true, sv.DateFrom, sv.DateTo, cUsrId);
                        break;
                }

                if (runStatus == 0)
                    ViewBag.Message = "Report run successfully";

                return View(sv);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            ViewBag.Message = "Report Run Successfully";
            return View(sv);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sv"></param>
        /// <returns></returns>
        [ActionName("OnDemandReport")]
        [AcceptVerbs(HttpVerbs.Post)]
        [AttributeModel(Name = "UNumber", Data = "View Report")]
        public ActionResult ShowUserReport(OnDemandReportViewData sv)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            var sDto = ((SessionDto)Session["User"]).SessionUser;
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || sDto.fk_UserTypeID != 5)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int? cUsrId = ((SessionDto)Session["User"]).SessionUser.UserID;
                TempData["repValue"] = sv.fk_ReportID;
                TempData["MerValue"] = sv.fk_MerchantId;
                TempData["DateFrom"] = sv.DateFrom;
                TempData["DateTo"] = sv.DateTo;
                sv.ReportList = ReportList(23, 27);

                switch (sv.fk_ReportID)
                {
                    case 24://Corp User Exception Report
                        IEnumerable<GetMerchantExceptionReportDto> merchantException = _transactionService.GetCorporateExceptionReport(sv.DateFrom, sv.DateTo, Convert.ToInt32(cUsrId));
                        IEnumerable<GetMerchantExceptionReportViewData> merchantExceptionView = merchantException.ToMerchantSummaryExceptionList();
                        sv.MerchantExceptionReport = merchantExceptionView;
                        break;

                    case 25://Corp User Card Type Report
                        IEnumerable<GetCardTypeReportDto> muCardDto = _transactionService.GetCorpUserCardTypeReport(sv.DateFrom, sv.DateTo, cUsrId);
                        IEnumerable<GetCardTypeReportViewData> muCardviewData = muCardDto.ToCardTypeList();
                        sv.GetCardTypeReport = muCardviewData;
                        break;

                    case 26://Corp User Total Transactions By Vehicle Report
                        IEnumerable<GetTotalTransByVehicleReportDto> muVehicleDto = _transactionService.CorpUserGetTotalTransByVehicleReport(sv.DateFrom, sv.DateTo, cUsrId);
                        IEnumerable<GetTotalTransByVehicleReportViewData> admVehicleviewData = muVehicleDto.ToVehicleReportList();
                        sv.AdminTotalTransactionsByVehicle = admVehicleviewData;
                        break;
                }
                
                TempData.Keep("repValue");
                TempData.Keep("DateFrom");
                TempData.Keep("DateTo");
                TempData.Keep("MerValue");

                return View(sv);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sv"></param>
        /// <returns></returns>
        [ActionName("OnDemandReport")]
        [AcceptVerbs(HttpVerbs.Post)]
        [AttributeModel(Name = "UNumber", Data = "Export")]
        public ActionResult ExportUserReport(OnDemandReportViewData sv)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            var sDto = ((SessionDto)Session["User"]).SessionUser;
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || sDto.fk_UserTypeID != 5)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int? cUsrId = ((SessionDto)Session["User"]).SessionUser.UserID;
                switch (sv.fk_ReportID)
                {
                    case 25://CorporateUser Card Type Report
                        IEnumerable<GetCardTypeReportDto> muCardDto = _transactionService.GetCorpUserCardTypeReport(sv.DateFrom, sv.DateTo, cUsrId);
                        using (StringWriter output = new StringWriter())
                        {
                            output.WriteLine("\"Sr No\",\"Merchant Name\",\"Fleet Name\",\"VISA Txn\",\"VISA\",\"Master Txn\",\"Master Card\",\"Amex Txn\",\"American Express\",\"DisTxn\",\"Discover\",\"JCB Txn\",\"JCB\",\"Diner Txn\",\"Diners Club\"");

                            foreach (GetCardTypeReportDto item in muCardDto)
                            {
                                output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\"",
                                     item.SrNo, item.MerchantName, item.FleetName, item.VISATxn, item.VISA, item.MasterTxn, item.MasterCard, item.AmexTxn, item.AmericanExpress, item.DisTxn, item.Discover, item.JCBTxn, item.JCB, item.DinerTxn, item.DinersClub);
                            }

                            return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "CorporateUser_CardTypeReport.csv");
                        }

                    case 24://Corp Exception Report
                        IEnumerable<GetMerchantExceptionReportDto> CorpReport = _transactionService.GetCorporateExceptionReport(sv.DateFrom, sv.DateTo, Convert.ToInt32(cUsrId));
                        using (StringWriter output = new StringWriter())
                        {
                            output.WriteLine("\"Fleet Name\",\"Vehicle No\",\"Fare Value\",\"Tip\",\"Surcharge\",\"TechFee\",\"Amount\",\"Fee\",\"AmountOwing\"");
                            
                            foreach (GetMerchantExceptionReportDto item in CorpReport)
                            {
                                output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\"",
                                     item.FleetName, item.VehicleNo, item.FareValue, item.Tip, item.Surcharge, item.TechFee, item.Amount, item.Fee,
                                     item.AmountOwing
                                     );
                            }

                            return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "CorporateUser_ExceptionReport.csv");
                        }

                    case 26://Corp User Total Transactions By Vehicle Report
                        IEnumerable<GetTotalTransByVehicleReportDto> admCardVehicleDto = _transactionService.CorpUserGetTotalTransByVehicleReport(sv.DateFrom, sv.DateTo, cUsrId);
                        int counter = 1;
                        using (StringWriter output = new StringWriter())
                        {
                            output.WriteLine("\"Sr No\",\"Company\",\"FleetName\",\"No Of Trans\",\"FareAmt\",\"Tips\",\"Surcharges\",\"TechFee\",\"No Of Chargebacks\",\"Charge back Value\",\"No Of Void\",\"Void Value\",\"Total Amount\",\"Fees\",\"AmountOwing\",\"NoOfVehilesForSlab1\",\"NoOfVehilesForSlab2\",\"NoOfVehilesForSlab3\",\"NoOfVehilesForSlab4\"");

                            foreach (GetTotalTransByVehicleReportDto item in admCardVehicleDto)
                            {
                                output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\",\"{16}\",\"{17}\",\"{18}\"",
                                     counter, item.MerchantName, item.FleetName, item.NoOfTrans, item.FareAmt, item.Tips, item.Surcharges, item.TechFee, item.NoOfChargebacks, item.ChargebackValue, item.NoOfVoid, item.VoidValue, item.TotalAmount, item.Fees, item.AmountOwing, item.NoOfVehilesForSlab1, item.NoOfVehilesForSlab2, item.NoOfVehilesForSlab3, item.NoOfVehilesForSlab4);

                                counter++;
                            }

                            return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "CorporateUser_TotalTransByVehicle.csv");
                        }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<ReportListViewData> ReportList(int to, int from)
        {
            var reportList = _scheduleService.ReportList(to, from);
            var reportViewData = reportList.ToReportList().OrderBy(x => x.DisplayName);
            return reportViewData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ConfigCorporateUserReportIndex()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 5) return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            
            try
            {
                int uID = (int)((SessionDto)Session["User"]).SessionUser.UserID;

                _logMessage.Append("calling GetDefaultDto method of _defaultService");
                
                var defaultDto = _defaultService.GetTransactionCountSlabCorp(uID);
                
                _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                
                var defaultTranCount = defaultDto.ToViewData();
                if (defaultDto == null)
                {
                    TransactionCountSlabViewData dSetViewData = new TransactionCountSlabViewData();
                    dSetViewData.TransCountSlablt3 = 0;
                    dSetViewData.TransCountSlablt2 = 0;
                    dSetViewData.TransCountSlablt1 = 0;
                    dSetViewData.TransCountSlabGt4 = 0;
                    dSetViewData.TransCountSlabGt3 = 0;
                    dSetViewData.TransCountSlabGt2 = 0;
                    return View(dSetViewData);
                }

                _logMessage.Append("ToDefaultViewData method executed successfully");
                
                return View(defaultTranCount);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ConfigCorporateUserReportUpdate()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 5) return RedirectToAction("index", "Member");
            
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            
            try
            {
                int uID = (int)((SessionDto)Session["User"]).SessionUser.UserID;
                
                _logMessage.Append("calling GetDefaultDto method of _defaultService");
                
                var defaultDto = _defaultService.GetTransactionCountSlabCorp(uID);
                
                _logMessage.Append("GetDefaultDto method of _defaultService class executed successfully");
                _logMessage.Append("calling ToDefaultViewData method to map DefaultSettingViewData to DefaultSettingDto and retrun DefaultSettingViewData");
                
                var defaultTranCount = defaultDto.ToViewData();
                
                _logMessage.Append("ToDefaultViewData method executed successfully");
                
                return View(defaultTranCount);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="defaultViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ConfigCorporateUserReportUpdate(TransactionCountSlabViewData slabViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 5) return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                if (slabViewData.TransCountSlablt1 == null)
                {
                    ModelState.AddModelError("TransCountSlablt1", MtdataResource.Please_Enter_Slab1);
                    return View(slabViewData);
                }
                else if (slabViewData.TransCountSlabGt2 == null)
                {
                    ModelState.AddModelError("TransCountSlabGt2", MtdataResource.Please_Enter_Slab2);
                    return View(slabViewData);
                }
                else if (slabViewData.TransCountSlablt2 == null)
                {
                    ModelState.AddModelError("TransCountSlablt2", MtdataResource.Please_Enter_Slab2);
                    return View(slabViewData);
                }
                else if (slabViewData.TransCountSlabGt3 == null)
                {
                    ModelState.AddModelError("TransCountSlabGt3", MtdataResource.Please_Enter_Slab3);
                    return View(slabViewData);
                }
                else if (slabViewData.TransCountSlablt3 == null)
                {
                    ModelState.AddModelError("TransCountSlablt3", MtdataResource.Please_Enter_Slab3);
                    return View(slabViewData);
                }
                else if (slabViewData.TransCountSlabGt4 == null)
                {
                    ModelState.AddModelError("TransCountSlabGt4", MtdataResource.Please_Enter_Slab4);
                    return View(slabViewData);
                }

                if (!ModelState.IsValid)
                    return View(slabViewData);

                _logMessage.Append("calling ToDefaultDto method to map DefaultSettingDto to DefaultSettingViewData and retrun DefaultSettingDto");
                
                var slabDto = slabViewData.ToDto();
                _logMessage.Append("ToDefaultDto method executed successfully");
                slabDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                int uID = (int)((SessionDto)Session["User"]).SessionUser.UserID;
                
                _defaultService.UpdateCorp(slabDto, uID);
                _logMessage.Append("Update method of _defaultService executed successfully");
                _logger.LogInfoMessage(_logTracking.ToString());

                return RedirectToAction("ConfigCorporateUserReportIndex");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

    }
}