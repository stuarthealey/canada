IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DownloadData]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_DownloadData]
GO

CREATE PROCEDURE dbo.usp_DownloadData
-- =============================================
-- Author:		<Umesh Kumar>
-- Create date: <17-09-2015>
-- Description:	<Validate mac address and token>
-- =============================================
(
	@DateFrom datetime = NULL,
	@DateTo datetime = NULL,
	@TransType nvarchar(20) = NULL, 
	@IsDownloaded bit = NULL,
	@VehicleId varchar(50) = NULL,
	@DriverId nvarchar(50) = NULL,
	@fk_MerchantID int = NULL,
	@SerialNo NVARCHAR(50) = NULL
)
AS
BEGIN
	SELECT Id, MerchantId, VehicleNo, DriverNo, JobNumber, Stan, TransRefNo, TransOrderNo, PaymentType, TxnType, LocalDateTime, TransmissionDateTime, 
	FareValue, Tip, Surcharge, Fee, Taxes, Toll, Amount, Currency, ExpiryDate, CardType, Industry, EntryMode, 
	ResponseCode, AddRespData, AuthId, AthNtwId, RequestXml, ResponseXml,  SourceId, 
	TransNote, CardToken, IsCompleted, IsRefunded, IsVoided, CreatedBy,  TxnDate , fk_FleetId, TechFee, FleetFee, IsVarified,TerminalId,SerialNo
	FROM [MTDTransaction] T
	where (T.VehicleNo = @VehicleId OR COALESCE(@VehicleId,'') = '')
	  AND (T.DriverNo = @DriverId OR COALESCE(@DriverId,'') = '')
	  AND (T.TxnDate >= @DateFrom OR @DateFrom IS NULL)
	  AND (T.TxnDate <= @DateTo OR @DateTo IS NULL)
	  AND (T.TxnType = @TransType OR COALESCE(@TransType,'') = '')
	  AND (T.MerchantId = @fk_MerchantID OR COALESCE(@fk_MerchantID,0) = 0)
	  AND (T.SerialNo = (SELECT TOP 1 MacAdd FROM Terminal WHERE SerialNo=@SerialNo AND IsActive=1) OR COALESCE(@SerialNo,'') = '')
END
GO
