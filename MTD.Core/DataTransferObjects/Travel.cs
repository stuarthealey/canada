﻿using System;

namespace MTD.Core.DataTransferObjects
{
    public class Travel
    {
        public int TravelId { get; set; }
        public int? FkVehicleId { get; set; }
        public int? FkDriverId { get; set; }
        public DateTime? TnxDate { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public double? StartLat { get; set; }
        public double? StartLong { get; set; }
        public double? EndLat { get; set; }
        public double? EndLong { get; set; }
        public decimal? TotalPayment { get; set; }
        public decimal? FlagFall { get; set; }
        public decimal? Extras { get; set; }
        public decimal? Fare { get; set; }
        public decimal? Toll { get; set; }
        public decimal? Tip { get; set; }
    }
}