﻿using System;
using System.Collections.Generic;
using System.Linq;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Data.Repository
{
    public class PrefixRepository : BaseRepository, IPrefixRepository
    {
        public PrefixRepository(IMtDataDevlopmentsEntities dbMtData)
            : base(dbMtData)
        {

        }
        
        /// <summary>
        /// Purpose             :   To add card prefix
        /// Function Name       :   Add
        /// Created By          :   Salil Gupta
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public bool Add(CCPrefix prefix)
        {
            try
            {
                if (DbMtData.CCPrefixes.Any(x => x.CCPrefix1 == prefix.CCPrefix1 && x.fk_MerchantId==prefix.fk_MerchantId && x.IsActive))
                    return false;
                prefix.IsActive = true;
                prefix.CreatedDate = DateTime.Now;
                DbMtData.CCPrefixes.Add(prefix);
                int result = DbMtData.CommitChanges();
                return Convert.ToBoolean(result);
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To get prefix of card
        /// Function Name       :   GetPrefix
        /// Created By          :   Salil Gupta
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="prefixId"></param>
        /// <returns></returns>
        public CCPrefix GetPrefix(int prefixId)
        {
            try
            {
                CCPrefix prefixDb = DbMtData.CCPrefixes.Find(prefixId);
                return prefixDb;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To update card prefix
        /// Function Name       :   Update
        /// Created By          :   Salil Gupta
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="prefixId"></param>
        /// <returns></returns>
        public void Update(CCPrefix prefix, int prefixId)
        {
            try
            {               
                CCPrefix prefixDb = DbMtData.CCPrefixes.Find(prefixId);
                prefixDb.fk_GatewayID = prefix.fk_GatewayID;
                prefixDb.ModifiedBy = prefix.ModifiedBy;
                prefixDb.ModifiedDate = DateTime.Now;
                DbMtData.CommitChanges();
               
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To delete card prefix
        /// Function Name       :   DeletePrefix
        /// Created By          :   Salil Gupta
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="prefixId"></param>
        public void DeletePrefix(int prefixId)
        {
            try
            {
                CCPrefix prefixDb = DbMtData.CCPrefixes.Find(prefixId);
                prefixDb.IsActive = false;
                DbMtData.CommitChanges();
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get list card prefix
        /// Function Name       :   prefixList
        /// Created By          :   Salil Gupta
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CCPrefix> prefixList(int merchantId,string name)
        {
            try
            {
                var prefixList = (from m in DbMtData.CCPrefixes join n in DbMtData.Gateways on m.fk_GatewayID equals n.GatewayID where m.IsActive && n.IsActive && m.fk_MerchantId==merchantId select new { m.PrefixID, m.CCPrefix1, n.Name }).AsEnumerable().Select(objNew => new CCPrefix { CCPrefix1 = objNew.CCPrefix1, PrefixID = objNew.PrefixID, Name = objNew.Name }).ToList();
                if (!string.IsNullOrEmpty(name))
                {
                    prefixList = prefixList.Where(p => p.Name.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                return prefixList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get list card prefix by filter
        /// Function Name       :   PrefixListByFilter
        /// Created By          :   Salil Gupta
        /// Created On          :   02/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <param name="sorting"></param>
        /// <returns></returns>
        public IEnumerable<CCPrefix> PrefixListByFilter(string name, int startIndex, int count, string sorting,int merchantId)
        {
            try
            {
                var prefixList = (from m in DbMtData.CCPrefixes
                                  join n in DbMtData.Gateways on m.fk_GatewayID equals n.GatewayID
                                  where m.IsActive && n.IsActive && m.fk_MerchantId==merchantId
                                  select new { m.PrefixID, m.CCPrefix1, n.Name }).AsEnumerable()
                    .Select(
                        objNew =>
                            new CCPrefix { CCPrefix1 = objNew.CCPrefix1, PrefixID = objNew.PrefixID, Name = objNew.Name });
                if (!string.IsNullOrEmpty(name))
                {
                    prefixList = prefixList.Where(p => p.Name.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sorting))
                {
                    prefixList = SortedprefixList(sorting, prefixList);
                }
                return count > 0
                           ? prefixList.Skip(startIndex).Take(count).ToList() //Paging
                           : prefixList.ToList();

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="prefixList"></param>
        /// <returns></returns>
        private IEnumerable<CCPrefix> SortedprefixList(string sorting, IEnumerable<CCPrefix> prefixList)
        {
                    switch (sorting)
                    {
                        case "Name ASC":
                            prefixList = prefixList.OrderBy(p => p.Name);
                            break;
                        case "Name DESC":
                            prefixList = prefixList.OrderByDescending(p => p.Name);
                            break;
                        default:
                            prefixList = prefixList.OrderBy(p => p.Name); //Default!
                            break;
                    }
            return prefixList;
        }

    }
}

