﻿using System;

namespace MTD.Core.DataTransferObjects
{
     public   class DriverDto
    {
        public int DriverID { get; set; }
        public string DriverNo { get; set; }
        public string DriverTaxNo { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string fk_Country { get; set; }
        public string Fk_State { get; set; }
        public string  Fk_City { get; set; }
        public string Address { get; set; }
        public string PIN { get; set; }
        public int fk_FleetID { get; set; }
        public Nullable<bool> IsLocked { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string ConfirmPIN { get; set; }
        public string GridCity { get; set; }
        public string GridState { get; set; }
        public string FleetName { get; set; }
        public string FailedError { get; set; }
        public string SageAccount { get; set; }
        public string BankName { get; set; }
        public string AccountName { get; set; }
        public string BSB { get; set; }
        public string Account { get; set; }
    }
}
