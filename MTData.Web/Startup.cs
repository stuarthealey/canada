﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MTData.Web.Startup))]
namespace MTData.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
