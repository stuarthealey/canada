﻿using System;
using System.ServiceModel;
using System.Text;

using MTData.Utility;
using MTData.WCFGW.Model;

namespace MTData.WCFGW
{
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    public class PaymentGatewayCreditCardService : IPaymentGatewayCreditCardService
    {
        ILogger _logger = new Logger();
        StringBuilder _logMessage = new StringBuilder();
        
        public Model.CreateRCCIResp CreateRCCI(Model.CreateRCCIReq request)
        {
            _logMessage.AppendLine("CreateRCCI: process start.");
            var rcciResp = new CreateRCCIResp();

            try
            {
                //Convert rquest into XML request.
                _logMessage.AppendLine("CreateRCCI: parsing CreateRCCIRequest into XML.");
                var requestPacket = ServiceHelper.ObjectToXML(request);

                //Call API
                _logMessage.AppendLine("CreateRCCI: Calling Web API.");
                var response = ServiceHelper.RequestAPI(requestPacket, "CreateRCI_ApiUrl".AppSetting());

                //Cast response into CreateRCCIResponse and return
                _logMessage.AppendLine("CreateRCCI: casting Web API response into CreateRCCIResponse type.");
                rcciResp = (CreateRCCIResp)ServiceHelper.XmlToObject(response, typeof(CreateRCCIResp));

                _logMessage.AppendLine("CreateRCCI: returning response to consumer. Process complete");
                _logger.LogInfoMessage(_logMessage.ToString());

                return rcciResp;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                ResultStatus rs = new ResultStatus() { Code = ResultCode.Error, ErrorDescription = "Exception occurred" };
                rcciResp.Result = rs;
                return rcciResp;
            }
        }

        public Model.ValidateRCCIResp ValidateRCCI(Model.ValidateRCCIReq request)
        {
            _logMessage.AppendLine("ValidteRCCI: process start.");
            var resp = new ValidateRCCIResp();
            try
            {
                //Convert rquest into XML request.
                _logMessage.AppendLine("ValidteRCCI: parsing CreateRCCIRequest into XML.");
                var requestPacket = ServiceHelper.ObjectToXML(request);

                //Call API
                _logMessage.AppendLine("ValidteRCCI: Calling Web API.");
                var response = ServiceHelper.RequestAPI(requestPacket, "ValidateRCCI_ApiUrl".AppSetting());

                //Cast response into ValidateRCCIResp and return
                _logMessage.AppendLine("ValidteRCCI: casting Web API response into CreateRCCIResponse type.");
                resp = (ValidateRCCIResp)ServiceHelper.XmlToObject(response, typeof(ValidateRCCIResp));

                _logMessage.AppendLine("ValidteRCCI: returning response to consumer. Process complete");
                _logger.LogInfoMessage(_logMessage.ToString());

                return resp;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                var cardStatus = new Model.CreditCardStatus() { IsValid = false, StatusDescription = "Exception occurred" };
                resp.CreditCardStatus = cardStatus;
                return resp;
            }           
        }
    }
}
