//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MTD.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class TransactionCountSlab
    {
        public int SlabID { get; set; }
        public Nullable<int> Fk_MerchantId { get; set; }
        public Nullable<int> fk_CorporateUserId { get; set; }
        public Nullable<int> TransCountSlablt1 { get; set; }
        public Nullable<int> TransCountSlabGt2 { get; set; }
        public Nullable<int> TransCountSlablt2 { get; set; }
        public Nullable<int> TransCountSlabGt3 { get; set; }
        public Nullable<int> TransCountSlablt3 { get; set; }
        public Nullable<int> TransCountSlabGt4 { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        public virtual Merchant Merchant { get; set; }
    }
}
