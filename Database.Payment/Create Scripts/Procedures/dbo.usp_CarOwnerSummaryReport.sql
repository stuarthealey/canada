IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CarOwnerSummaryReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_CarOwnerSummaryReport]
GO

CREATE PROCEDURE dbo.usp_CarOwnerSummaryReport
-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <09-09-2015>
-- Description:	<Prepare and send weekly and monhly driver summary report>
-- =============================================
--	S.Healey	2016-06-06		- Changed company name reference to MTData, LLC
-- S.Healey		2016-11-10		- Performance optimisation with use of indexes and improved queries.
--
(
	@DriverId INT,
	@DateFrom DATETIME,
	@DateTo DATETIME,
	@ReportName NVARCHAR(100),
	@DateCommenced NVARCHAR(100)
)		
AS
BEGIN	
	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SET NOCOUNT ON
		SET ANSI_WARNINGS OFF

		DECLARE	@DriverNo NVARCHAR(50),
				@VehicleNo NVARCHAR(50),
				@DriverEmail NVARCHAR(50),
				@XmlData NVARCHAR(MAX),
				@DriverName NVARCHAR(50),
				@FleetId NVARCHAR(50),
				@OwnerName NVARCHAR(80),
				@OwnerEmail NVARCHAR(50),
				@LogInDateTime DATETIME,
				@LogOutDateTime DATETIME,
				@BaseLogInTime DATETIME,
				@BaseLogOutTime DATETIME,
				@TotalFareValue VARCHAR(30),
				@TotalTip VARCHAR(30),
				@TotalTax VARCHAR(30),
				@TotalAmt VARCHAR(30),
				@TotalFee VARCHAR(30),
				@NetAmt VARCHAR(30),
				@MsgXml VARCHAR(50),
				@Body NVARCHAR(MAX),
				@DriverLName NVARCHAR(50),
				@Error NVARCHAR(200),
				@Res INT,
				@MailId INT,
				@NoFares INT,
				@OwnerId INT,
				@Valid INT,
				@Count  INT,
				@Index INT

		SELECT @DriverNo = DriverNo, @DriverName = FName, @FleetId = fk_FleetID, @DriverLName = LName 
		FROM dbo.Driver 
		WHERE DriverID = @DriverId

		DECLARE CarOwner_Cursor CURSOR FOR SELECT DISTINCT dh.VehicleNo 
											FROM dbo.DriverTransactionHistory dh 
											WHERE dh.DriverNo = @DriverNo 
											  AND FleetId = @FleetId 
											  AND LogOnDateTime > @DateFrom - 5 
											  AND LogOnDateTime < @DateTo 
											  AND LogOffDateTime > @DateFrom 
											  AND LogOffDateTime < @DateTo 

		OPEN CarOwner_Cursor 
		FETCH NEXT FROM CarOwner_Cursor INTO @VehicleNo
		WHILE @@FETCH_STATUS = 0  
		BEGIN
			SELECT @OwnerId = Id, @OwnerName = OwnerName, @OwnerEmail = EmailAddress 
			FROM dbo.CarOwners 
			WHERE Id = (SELECT TOP 1 fk_CarOwenerId 
						FROM dbo.Vehicle 
						WHERE VehicleNumber = @VehicleNo 
						  AND fk_FleetID = @FleetId
						  AND IsActive = 1)

			IF (@OwnerEmail IS NOT NULL)
			BEGIN
				DECLARE @TableData TABLE(Id INT IDENTITY(1, 1) PRIMARY KEY, LogInTime DATETIME, LogOutTime DATETIME)

				SET @XmlData = ''

				INSERT INTO @TableData(LogInTime, LogOutTime)
				SELECT LogOnDateTime, LogOffDateTime 
				FROM dbo.DriverTransactionHistory dh 
				WHERE dh.DriverNo = @DriverNo 
				  AND FleetId = @FleetId 
				  AND LogOnDateTime > @DateFrom - 5 
				  AND LogOnDateTime < @DateTo 
				  AND LogOffDateTime > @DateFrom 
				  AND LogOffDateTime < @DateTo 
				  AND dh.VehicleNo = @VehicleNo 
				ORDER BY LogOnDateTime 

				SELECT @Count = COUNT(Id) FROM @TableData
				SET @Index = 1

				WHILE (@Index <= @Count)
				BEGIN
					SELECT	@LogInDateTime = LogInTime, 
							@LogOutDateTime = LogOutTime 
					FROM @TableData 
					WHERE Id = @Index

					SET @XmlData = @XmlData + COALESCE(CAST((SELECT @Index AS 'td','',
																	COALESCE(CONVERT(VARCHAR(17), @LogInDateTime, 113), '') AS 'td', '',
																	COALESCE(CONVERT(VARCHAR(17), @LogOutDateTime, 113), '') AS 'td', '',
																	COALESCE(MAX(VehicleNo), '') AS 'td', '',
																	COALESCE(COUNT(Id), 0) AS 'td', '',
																	CONCAT('$',CAST(COALESCE(SUM(FareValue), 0.00) AS DECIMAL(18, 2)))  AS 'td', '',
																	CONCAT('$',CAST(COALESCE(SUM(Tip), 0.00) AS DECIMAL(18, 2)))  AS 'td', '',
																	CONCAT('$',CAST(COALESCE(SUM(Taxes), 0.00) AS DECIMAL(18, 2)))  AS 'td', '',
																	CONCAT('$',CAST(COALESCE(SUM(Amount), 0.00) AS DECIMAL(18, 2)))  AS 'td', '',
																	CONCAT('$',CAST(COALESCE(SUM(Fee), 0.00) AS DECIMAL(18, 2)))  AS 'td', '',
																	CONCAT('$',CAST(COALESCE(SUM(Amount), 0.00) - COALESCE(SUM(Fee), 0.00) AS DECIMAL(18, 2))) AS 'td' 
															FROM dbo.MTDTransaction 
															WHERE TxnDate >= @LogInDateTime 
															  AND TxnDate <= @LogOutDateTime 
															  AND DriverNo = @DriverNo 
															  AND fk_FleetId = @FleetId
															  AND ResponseCode IN ('000', '00', '002')
															  AND VehicleNo = @VehicleNo
															FOR XML PATH('tr'), ELEMENTS) AS NVARCHAR(MAX)), '')

					SET @Index = @Index + 1
				END																			  

				DELETE @TableData
				IF (@XmlData = '' OR @XmlData IS NULL)
				BEGIN
					SET @Valid = 0
					INSERT INTO dbo.MailMessage(RepUserId, [Status], SendTo, Body, RepUserTypeId, [Date], ReportType) 
					VALUES (@OwnerId, -1, @OwnerEmail, 'No records in database', 5, GETDATE(), 'CarOwnerSummaryReport')		
				END
				ELSE 
				BEGIN
					SET @Valid = 1
				END

				IF (@Valid = 1)
				BEGIN
					SELECT  @BaseLogInTime = MIN(LogOnDateTime),
							@BaseLogOutTime = MAX(LogOffDateTime) 
					FROM dbo.DriverTransactionHistory 
					WHERE DriverNo = @DriverNo 
					  AND FleetId = @FleetId 
					  AND LogOnDateTime > @DateFrom - 5 
					  AND LogOnDateTime < @DateTo 
					  AND LogOffDateTime > @DateFrom 
					  AND LogOffDateTime < @DateTo 
					  AND VehicleNo = @VehicleNo

					SELECT  @NoFares = COALESCE(COUNT(Id), 0),
							@TotalFareValue = CONCAT('$', CAST(COALESCE(SUM(FareValue), 0.00) AS DECIMAL(18, 2))),
							@TotalTip = CONCAT('$', CAST(COALESCE(SUM(Tip), 0.00) AS DECIMAL(18, 2))),
							@TotalTax = CONCAT('$', CAST(COALESCE(SUM(Taxes), 0.00) AS DECIMAL(18, 2))),
							@TotalAmt = CONCAT('$', CAST(COALESCE(SUM(Amount), 0.00) AS DECIMAL(18, 2))),
							@TotalFee = CONCAT('$', CAST(COALESCE(SUM(Fee), 0.00) AS DECIMAL(18, 2))),
							@NetAmt = CONCAT('$', CAST((COALESCE(SUM(Amount), 0.00) - COALESCE(SUM(Fee), 0.00)) AS DECIMAL(18, 2)))
					FROM dbo.MTDTransaction
					WHERE TxnDate >= @BaseLogInTime 
					  AND TxnDate <= @BaseLogOutTime 
					  AND fk_FleetId = @FleetId
					  AND DriverNo = @DriverNo 
					  AND ResponseCode IN ('000', '00', '002')
					  AND VehicleNo = @VehicleNo

					SET @MsgXml = CAST((SELECT CONCAT('Dear', ' ', @OwnerName, ',') AS 'p' FOR XML PATH('p'), ELEMENTS ) AS NVARCHAR(MAX))

					SET @Body = '<html><style>td{text-align:left;font-family:verdana;font-size:12px}
								p{font-family:verdana;font-size:13px}span{font-family:verdana;font-size:13px}</style>
								<body><p>Listed below is the driver transaction report containing summarized details of transactions done in below mentioned shifts.</p>
								<H5 style="color:black;text-align:center;font-size:11px;font-family:verdana"> ' + @ReportName + '</H5>
								<H5 style="color:grey;text-align:center;font-size:11px;font-family:verdana">Driver No : ' + @DriverNo + '&nbsp;&nbsp;&nbsp;&nbsp;Driver Name : ' + @DriverName + ' ' + @DriverLName + '&nbsp;&nbsp;&nbsp;&nbsp;Vehicle No :' + @VehicleNo + '&nbsp;&nbsp;&nbsp;&nbsp;' + @DateCommenced + '</H5>
								<table border = 1 cellpadding="0" cellspacing="0" style="background-color:white;border-collapse:collapse"> <tr>
								<th style="color:grey;font-size:13px;width:220px;text-align:left">Seq No</th>
								<th style="color:grey;font-size:13px;width:750px;text-align:left">Logon Date/Time</th>
								<th style="color:grey;font-size:13px;width:750px;text-align:left">Logoff Date/Time</th>
								<th style="color:grey;font-size:13px;width:500px;text-align:left">Car Number</th>
								<th style="color:grey;font-size:13px;width:300px;text-align:left">No Fares</th>
								<th style="color:grey;font-size:13px;width:240px;text-align:left">Fare Amt</th>
								<th style="color:grey;font-size:13px;width:250px;text-align:left">Total Tips</th>
								<th style="color:grey;font-size:13px;width:250px;text-align:left">Total Taxes</th>
								<th style="color:grey;font-size:13px;width:250px;text-align:left">Total Amt</th>
								<th style="color:grey;font-size:13px;width:250px;text-align:left">Total Fee</th>
								<th style="color:grey;font-size:13px;width:250px;text-align:left">Net Amt</th></tr>'    

					SET @Body = @MsgXml + @Body + @XmlData + '<tfoot><tr style="height:16px;font-weight:bold">
								<td colspan=4 style="font-size:11px;text-align:center">Total</td>
								<td style="font-size:11px;text-align:left">' + CAST(@NoFares AS VARCHAR) + '</td>
								<td style="font-size:11px;text-align:left">' + @TotalFareValue + '</td>
								<td style="font-size:11px;text-align:left">' + @TotalTip + '</td>
								<td style="font-size:11px;text-align:left">' + @TotalTax + '</td>
								<td style="font-size:11px;text-align:left">' + @TotalAmt + '</td>
								<td style="font-size:11px;text-align:left">' + @TotalFee + '</td>
								<td style="font-size:11px;text-align:left">' + @NetAmt + '</td>
								</tr></tfoot></table><br/><br/>
								<span>Thanks & Regards,<br/><br/>MTData, LLC.<br/>www.mtdata.com<span>
								<h4 style="color:grey;font-size:12px;font-family:verdana">MTData-All rights reserved</h4>
								<p style="font-size:11px;color:black">Note:This is an automated message. Do not Reply or Reply All
								to this message as replies are not being accepted.</p> </body></html>'      

					INSERT INTO dbo.MailMessage(RepUserId, [Status], SendTo, Body, [RepUserTypeId], [Date], [ReportType]) 
					VALUES (@OwnerId, 0, @OwnerEmail, @Body, 5, GETDATE(), 'CarOwnerSummaryReport')

					SELECT @MailId = @@IDENTITY

					EXEC @Res = msdb.dbo.sp_send_dbmail @profile_name = 'MTData', @recipients = @OwnerEmail, @body = @Body, @subject = 'Driver Summary Report', @body_format = 'HTML'

					IF (@Res != 0)
					BEGIN
						SET @Error = @@ERROR
						UPDATE dbo.MailMessage SET Error = @Error, [Date] = GETDATE() WHERE MailId = @MailId
					END
					ELSE
					BEGIN
						UPDATE dbo.MailMessage SET [Status] = 1, [Date] = GETDATE() WHERE MailId = @MailId
					END
				END
			END
			FETCH NEXT FROM CarOwner_Cursor INTO @VehicleNo
		END   
		CLOSE CarOwner_Cursor   
		DEALLOCATE CarOwner_Cursor
	END TRY
	BEGIN CATCH
		INSERT INTO dbo.ProcErrorHandler(ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, UserName, HostName, [TimeStamp])
		SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
	END CATCH
END
GO
