﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Models
{
    public class BasicAuthoriseRequest
    {
        public AuthenticationHeader AuthenticationHeader { get; set; }
        public BasicAuthorise BasicAuthorise { get; set; }
    }

    public class AuthenticationHeader
    {
        public string Password { get; set; }
        public string Token { get; set; }
        public string Username { get; set; }
    }

    public class BasicAuthorise
    {
        public string accountNumber { get; set; }
        public string cvv2 { get; set; }
        public string deviceId { get; set; }
        public decimal discountAmt { get; set; }
        public DateTime dropoffDate { get; set; }
        public decimal dropoffLatitude { get; set; }
        public decimal dropoffLongitude { get; set; }
        public string encryptedToken { get; set; }
        public byte encryptionAlgorithm { get; set; }
        public byte encryptionKeyVersion { get; set; }
        public string expiryDate { get; set; }
        public decimal extrasAmt { get; set; }
        public decimal fareAmt { get; set; }
        public decimal flagfallAmt { get; set; }
        public decimal jobDistance { get; set; }
        public decimal jobDuration { get; set; }
        public long? jobId { get; set; }
        public byte passengerCount { get; set; }
        public decimal paymentAmt { get; set; }
        public DateTime pickupDate { get; set; }
        public decimal pickupLatitude { get; set; }
        public decimal pickupLongitude { get; set; }
        public string postCode { get; set; }
        public string RCCI { get; set; }
        public string requestId { get; set; }
        public string swipeData { get; set; }
        public byte swipeMethod { get; set; }
        public decimal tipAmt { get; set; }
        public decimal tollAmt { get; set; }
        public string transactionId { get; set; }
        public string transactionType { get; set; }
        public int? userId { get; set; }
        public string Card { get; set; }
        public string zipCode { get; set; }
        public string streetAddress { get; set; }
    }
}