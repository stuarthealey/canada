﻿namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table14AdditionalMasterData
    {
        #region Private Constants and Variables

        #region Constants
        private const string tblId = "|31|34";
        private const string filler1space = "|20";
        private const string filler2space = "|20|20";
        private const string filler13space = "|20|20|20|20|20|20|20|20|20|20|20|20|20";
        #endregion

        #region Variables
        private string defaultACI = "|59"; // |59 represents hex of Y
        private string defaultBankNetDate = "|20|20|20|20";  // |20 represents hex of one blank space
        private string defaultBankNetRef = "|20|20|20|20|20|20|20|20|20";
        private string defaultCVCErrorCode = "|20";
        private string defaultPOSEntryModeChange = "|20";
        private string defaultTransEditErrorCode = "|20";
        private string defaultMSDI = "|20";
        private string defaultTotalAuthAmount = "|30|30|30|30|30|30|30|30|30|30|30|30"; // |30 represents hex of zero
        private string defaultAddMCSettlementDate = string.Empty;//Empty is Suggested by FirstData //"|20|20|20|20";//only set when ACI=I
        private string defaultAddMCBankNetRefNo = string.Empty;//Empty is Suggested by FirstData //"|20|20|20|20|20|20|20|20|20";//only set when ACI=I
        #endregion

        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'31 34' in Request
        /// </summary>
        public string TableID
        {
            // |31 and |34 represent hex of numeric 1 and 4
            get { return tblId; }
        }

        /// <summary>
        /// Authorization Characteristic Indicator
        /// default Y in Request
        /// </summary>
        public string ACI
        {
            get { return defaultACI; }
            set { defaultACI = value; }
        }

        /// <summary>
        /// Identifies the date, in MMDD format, 
        /// the authorization record was captured by the MasterCard's telecommunications network.
        /// default 4 Spaces(1 space= X'20') in Request 
        /// </summary>
        public string BankNetDate
        {
            get { return defaultBankNetDate; }
            set { defaultBankNetDate = value; }
        }

        /// <summary>
        /// Unique data element, assigned by MasterCard, which identifies an authorization transaction. 
        /// default 9 Spaces(1 space= X'20') in Request 
        /// </summary>
        public string BankNetReference
        {
            get { return defaultBankNetRef; }
            set { defaultBankNetRef = value; }
        }

        /// <summary>
        /// Filler. contains 2 Spaces(1 space= X'20') in Request 
        /// </summary>
        public string Filler2space
        {
            get { return filler2space; }
        }

        /// <summary>
        /// An indicator, provided by the Issuer in the authorization response, 
        /// to identify the presence of an invalid card verification code
        /// default 1 Spaces(1 space= X'20') in Request 
        /// If there is an error, the Issuer will respond with the 1-byte CVC Error Code (Y).
        /// </summary>
        public string CVCErrorCode
        {
            get { return defaultCVCErrorCode; }
            set { defaultCVCErrorCode = value; }
        }

        /// <summary>
        /// Entry Mode of Point of sales.
        /// default 1 Spaces(1 space= X'20') in Request 
        /// If the entry mode has changed, the Issuer will respond with the 1-byte POS Entry Mode Change (Y).
        /// </summary>
        public string POSEntryModeChange
        {
            get { return defaultPOSEntryModeChange; }
            set { defaultPOSEntryModeChange = value; }
        }

        /// <summary>
        /// Transaction Edit Error Code.
        /// default 1 Spaces(1 space= X'20') in Request 
        /// If the track data contains an edit error, a single one-position alphabetic error code is provided.
        /// </summary>
        public string TransEditErrorCode
        {
            get { return defaultTransEditErrorCode; }
            set { defaultTransEditErrorCode = value; }
        }

        /// <summary>
        /// Filler. contains 1 Spaces(1 space= X'20') in Request 
        /// </summary>
        public string Filler1space
        {
            get { return filler1space; }
        }

        /// <summary>
        /// Market Specific Data Indicator
        /// default 1 Spaces(1 space= X'20')
        /// For Auto: A, Bill Payment: B, Hotel: H, Healthcare(Medical): M,
        /// Transit: T, B2B Straight Through Transactions: J
        /// </summary>
        public string MSDI
        {
            get { return defaultMSDI; }
            set { defaultMSDI = value; }
        }

        /// <summary>
        /// Filler. contains 13 Spaces(1 space= X'20') in Request 
        /// </summary>
        public string Filler13space
        {
            get { return filler13space; }
        }

        /// <summary>
        /// Reflects the total amount authorized.
        /// default 12 Zero(1 Zero= X'30')
        /// </summary>
        public string TotalAuthAmount
        {
            get { return defaultTotalAuthAmount; }
            set { defaultTotalAuthAmount = value; }
        }

        /// <summary>
        /// Additional Master Card Settlement Date
        /// default 4 Zero(1 Zero= X'30')
        /// </summary>
        public string AdditionalMCSettlementDate
        {
            get { return defaultAddMCSettlementDate; }
            set { defaultAddMCSettlementDate = value; }
        }

        /// <summary>
        /// Additional Master Card BankNet Reference Number
        /// default 9 Zero(1 Zero= X'30')
        /// </summary>
        public string AdditionalMCBankNetRefNo
        {
            get { return defaultAddMCBankNetRefNo; }
            set { defaultAddMCBankNetRefNo = value; }
        }
        #endregion
    }
}