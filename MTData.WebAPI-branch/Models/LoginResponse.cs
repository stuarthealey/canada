﻿using System.Runtime.Serialization;
using System.Web;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class LoginResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public LoginHeader Header { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public LoginReceipt ReceiptFormat { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public LoginCompany CompanyDetails { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public LoginTips Tips { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public LoginSurcharge Surcharge { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public LoginTaxes Taxes { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Disclaimer { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string TermsConditions { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public bool? IsTaxIncluded { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Token { get; set; }

        private string fleetId;

        [DataMember]
        public string FleetId
        {
            get { return this.fleetId ?? "null"; }
            set { this.fleetId = value; }
        }
    }
}