﻿using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class ReceiptInfo
    {
        [DataMember]
        public string Show { get; set; }

        private int _sequence;
        [DataMember]
        public int Order
        {
            get { return this._sequence; }
            set { this._sequence = value; }
        }

        private string _title;

        [DataMember]
        public string Name
        {
            get { return this._title ?? "null"; }
            set { this._title = value; }
        }
        private string _position;

        [DataMember]
        public string Value
        {
            get { return this._position ?? "null"; }
            set { this._position = value; }
        }
    }
}