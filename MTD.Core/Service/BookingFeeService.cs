﻿using System.Collections.Generic;
using MTD.Core.DataTransferObjects;
using MTD.Core.Extension;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;
using Ninject;

namespace MTD.Core.Service
{
    public class BookingFeeService : IBookingFeeService
    {
        private readonly IBookingFeeRepository _feeRepository;
        public BookingFeeService([Named("BookigFeeRepository")] IBookingFeeRepository feeRepository)
        {
            _feeRepository = feeRepository;
        }

        /// <summary>
        /// Purpose             :   To Add the Bookig Fee
        /// Function Name       :   Add
        /// Created By          :   Umesh Kumar
        /// Created On          :   01/11/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="feeDto"></param>
        public void Add(BookigFeeDto feeDto)
        {
            try
            {
                BookingFee fee = feeDto.ToBookingFee();
                _feeRepository.Add(fee);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Fees List
        /// Function Name       :   GetFeeDtoList
        /// Created By          :   Salil Gupta
        /// Created On          :   01/12/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<BookigFeeDto> GetFeeDtoList()
        {
            try
            {
                var feeList = _feeRepository.FeeList();
                var feeDtoList = feeList.ToDTOBookingFeeList();
                return feeDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Fees
        /// Function Name       :   GetFeeDto
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   01/12/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="feeId"></param>
        /// <returns></returns>
        public BookigFeeDto GetFeeDto(int feeId)
        {
            try
            {
                var fee = _feeRepository.GetFee(feeId);
                var feeDto = fee.ToBookingFeeDto();
                return feeDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update Bookig Fee
        /// Function Name       :   Update
        /// Created By          :   Umesh Kumar
        /// Created On          :   01/19/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="feeDto"></param>
        /// <param name="feeId"></param>
        public void Update(BookigFeeDto feeDto, int feeId)
        {
            try
            {
                BookingFee fee = feeDto.ToBookingFee();
                _feeRepository.Update(fee, feeId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Delete the Fee
        /// Function Name       :   DeleteFee
        /// Created By          :   Salil Gupta
        /// Created On          :   01/19/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="feeId"></param>
        public void DeleteFee(int feeId)
        {
            try
            {
                _feeRepository.DeleteFee(feeId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Fees List
        /// Function Name       :   FeeListByFiter
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   01/19/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="filterSearch"></param>
        /// <returns></returns>
        public IEnumerable<BookigFeeDto> FeeListByFiter(FilterSearchParametersDto filterSearch)
        {
            try
            {
                var filterSearchClass = filterSearch.SearchParameters();
                var feeList = _feeRepository.FeeListByFiter(filterSearchClass);
                var feeDtoList = feeList.ToDTOBookingFeeList();
                return feeDtoList;
            }
            catch
            {
                throw;
            }
        }

    }
}