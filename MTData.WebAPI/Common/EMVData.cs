﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MTData.Utility;
using MTData.WebAPI.Resources;
using MTData.WebAPI.Models;

namespace MTData.WebAPI.Common
{
    public static class EMVData
    {
        private const string DebitAID1 = "A0000000043060";
        private const string DebitAID2 = "A0000000033010";
        private const string InteracAID = "A0000002771010";
        private const string tag9F53 = "9F53";
        private const string tag9F1E = "9F1E";

        /// <summary>
        /// Purpose             :   Parse EMV data from ingenico device, and return Track2,Track3,EMV data.
        /// Function Name       :   GetEMVdata
        /// Created By          :   Sunil Singh
        /// Created On          :   06-Aug-2015
        /// Modificatios Made   :   
        /// Modidified On       :   
        /// </summary>       
        /// <param name="value"></param>
        /// <param name="tagType"></param>
        /// <returns></returns>
        public static EmvDataValues GetEMVdata(this string value, EmvTagType tagType)
        {
            try
            {
                ILogger _logger = new Logger();

                // Load all EMV tags
                string[] tags = EMVTags.AllTags.Split(',');

                string track2 = string.Empty;
                string track3 = string.Empty;
                string emvData = string.Empty;
                string tranAmount = string.Empty;
                string pindata = string.Empty;
                string cSeq = string.Empty;
                string paymentType = string.Empty;
                string accountType = string.Empty;
                string tagAID = string.Empty;
                string deviceSerialNo = string.Empty;

                if (!string.IsNullOrEmpty(value))
                {
                    // Splitting Data1/Data2/Data3
                    value = value.Replace("\r", "").Replace("\n", "");
                    List<string> listEMVdata = new List<string>();
                    if (!string.IsNullOrEmpty(value))
                    {
                        listEMVdata = value.SplitString(tags);
                    }

                    //DEBUG output of the emv data list.
                    try
                    {
                        _logger.LogInfoMessage(string.Format("WebAPI.Common.EMVData.GetEMVData. Value:{0};listEMVData:{1};", value, string.Join(",", listEMVdata.ToArray())));
                    }
                    catch { }

                    // Storing Key-Value in dictionary
                    if (listEMVdata.Count > 0)
                    {
                        var emvDataDictionary = new Dictionary<string, Tuple<string, string>>();

                        // Picking value from Data2 section of ingenico data.                    
                        for (int data2Counter = 1; data2Counter < listEMVdata.Count; data2Counter++)
                        {
                            // Rmoving leading 'T' and storing
                            string key = listEMVdata[data2Counter].getTagName();

                            /*Remove code for T99, it is redundant*/
                            // Handling T99 (Pin Data)
                            if (key.Equals("99"))
                            {
                                if (!emvDataDictionary.ContainsKey(key))
                                {
                                    string[] val = listEMVdata[data2Counter + 1].Split(':');

                                    string tag99 = val[1].Trim().RemoveFS();

                                    emvDataDictionary.Add(key.Trim().RemoveFS(), Tuple.Create(val[0].Trim().RemoveFS(), tag99.Substring(1, tag99.Length - 1)));
                                }
                            }

                            //Changes done for EMV Contact certification of Canada
                            // Handling T84 to get PaymentType dynamically. Suggested by FD
                            if (key.Equals("84"))
                            {
                                if (!emvDataDictionary.ContainsKey(key))
                                {
                                    string[] val = listEMVdata[data2Counter + 1].Split(':');
                                    if (val[1].Length >= 15)
                                    {
                                        string tagValue = (val[1].Remove(0, 1)).Substring(0, 14);
                                        tagAID = tagValue;

                                        if (System.Configuration.ConfigurationManager.AppSettings["Project"] == PaymentAPIResources.CanadaProject)
                                        {
                                            if (tagValue == InteracAID)
                                                paymentType = PymtTypeType.Debit.ToString();
                                            else
                                                paymentType = PymtTypeType.Credit.ToString();
                                        }
                                        else
                                        {
                                            if (tagValue == DebitAID1 || tagValue == DebitAID2 || tagValue == InteracAID)
                                                paymentType = PymtTypeType.Debit.ToString();
                                            else
                                                paymentType = PymtTypeType.Credit.ToString();
                                        }
                                    }
                                }
                            }

                            // Storing Key,length & value in dictionary
                            if (!emvDataDictionary.ContainsKey(key))
                            {
                                // Spliting to get Length & Value string
                                if (listEMVdata[data2Counter + 1].Contains(":"))
                                {
                                    string[] val = listEMVdata[data2Counter + 1].Split(':');

                                    if (!val.Count().Equals(4))
                                    {
                                        emvDataDictionary.Add(key.Trim().RemoveFS(), Tuple.Create(val[0].Trim().RemoveFS(), val[1].Trim().RemoveFS().GetTagValue()));
                                    }
                                    else
                                    {
                                        track3 = val[1] + ":" + val[2] + ":" + val[3];
                                        emvDataDictionary.Add(key.Trim(), Tuple.Create(val[0].Trim().RemoveFS(), track3.Trim().RemoveFS().GetTagValue()));

                                        // Preparing Track3 data (ASCII) by removing leading char 'a' from string.
                                        track3 = track3.Substring(1, track3.Length - 1);
                                    }
                                }
                            }

                            // Handling 9F6E
                            if (key.Equals("9F6E"))
                            {
                                string[] val = listEMVdata[data2Counter + 1].Split(':');

                                string tag9F6Evalue = val[1].Trim().RemoveFS();

                                if (!tag9F6Evalue.KeepTag())
                                    emvDataDictionary.Remove(key);
                            }

                            //Changes done for EMV Contact certification of Canada
                            //To get account type based on emv tag D1000
                            if (key.Equals("1000:"))
                            {
                                string[] val = listEMVdata[data2Counter + 1].Split(':');
                                if (val[1].ToString() == "a1")
                                    accountType = CanadaCommon.AccountType.Saving.ToString();
                                else
                                    accountType = CanadaCommon.AccountType.Checking.ToString();
                            }

                            //getting value for tag LN
                            if (key.Equals(tag9F1E))
                            {
                                string[] val = listEMVdata[data2Counter + 1].Split(':');
                                deviceSerialNo = val[1].Substring(1).Trim().RemoveFS();
                            }

                            data2Counter++;
                        }

                        // Remove 9F53 tag if card type is not master
                        if (emvDataDictionary.ContainsKey("57"))
                        {
                            string track2Data = emvDataDictionary["57"].Item2;
                            track2Data = ";" + track2Data.Replace("D", "=") + "?";

                            MTData.Utility.CardInfo cardInfo = MTData.Utility.StringExtension.GetCardNumberData(track2Data);
                            string cardType = Convert.ToString(CreditCardType.GetCardType(cardInfo.CardNumber));

                            if (emvDataDictionary.ContainsKey(tag9F53) && !cardType.Equals(CardTypeType.MasterCard.ToString()))
                                emvDataDictionary.Remove(tag9F53);
                        }

                        // Getting Tag wise EMV data.
                        emvData = GetTagWiseEmvData(emvDataDictionary, tagType);

                        // Preparing Track2 data
                        if (emvDataDictionary.ContainsKey("57"))
                        {
                            track2 = emvDataDictionary["57"].Item2;
                            track2 = ";" + track2.Replace("D", "=") + "?";

                            // Preparing tag '5F24'
                            string tag5F24 = track2.Substring(track2.IndexOf('=') + 1, 4);
                            emvData += "5F2403" + tag5F24 + "31";
                            //emvData += "9F390107"; //Note: This line is commented as it is creating duplicate 9F39 tag.
                            if (!emvDataDictionary.ContainsKey("9F53"))
                                emvData += "9F530152";
                        }

                        if (emvDataDictionary.ContainsKey("9F02"))
                            tranAmount = emvDataDictionary["9F02"].Item2;

                        if (emvDataDictionary.ContainsKey("99"))
                            pindata = emvDataDictionary["99"].Item2;

                        if (emvDataDictionary.ContainsKey("5F34"))
                            cSeq = emvDataDictionary["5F34"].Item2;
                    }
                }

                //Changes done for EMV Contact certification of Canada
                EmvDataValues emvDataValues = new EmvDataValues
                {
                    Track2 = track2,
                    Track3 = track3,
                    Amount = tranAmount.FormatEMVAmount(),
                    EmvData = emvData,
                    PinData = pindata,
                    AccountType = accountType,
                    TagAID = tagAID,
                    DeviceSerialNo = deviceSerialNo,
                    CSequence = cSeq != string.Empty ? new string('0', 3 - cSeq.Length) + cSeq : string.Empty
                };

                if (!string.IsNullOrEmpty(paymentType))
                {
                    emvDataValues.PaymentType = (PymtTypeType)Enum.Parse(typeof(PymtTypeType), paymentType);
                }

                return emvDataValues;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Checks 9F6E tag, whether it should include or exclude.
        /// </summary>
        /// <returns></returns>
        private static bool KeepTag(this string tagValue)
        {
            if (!string.IsNullOrEmpty(tagValue) && tagValue.Length > 6)
            {
                int bitToCheck = Convert.ToInt32(tagValue.Substring(5, 1));

                if (bitToCheck >= 0 && bitToCheck <= 7)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        /// <summary>
        /// Purpose             :   Parse EMV response and in to Android readable format.
        /// Function Name       :   GetEMVResponse
        /// Created By          :   Sunil Singh
        /// Created On          :   25-Sep-2015
        /// Modificatios Made   :   
        /// Modidified On       :   
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetEMVResponse(this string value)
        {
            StringBuilder sbEmvResponse = new StringBuilder();
            string[] tags = EMVTags.AllTagsPlain.Split(',');

            if (!string.IsNullOrEmpty(value))
            {
                // Removing new line and blank spaces
                value = value.Replace("\r", "").Replace("\n", "");

                bool isAlive = true;

                while (isAlive)
                {
                    int lenghtToDiscard = 0;
                    int regEx = 0;
                    string tagLenght = string.Empty;
                    int valueLenght = 0;

                    if (value.Length > 0)
                    {
                        string tagInResp = value.Substring(0, 4);
                        string tagInResp2char = value.Substring(0, 2);

                        string tagValue = string.Empty;

                        if (tags.Contains(tagInResp))
                        {
                            tagLenght = value.Substring(4, 2);
                            lenghtToDiscard = 6;

                            valueLenght = Convert.ToInt32(tagLenght, 16) * 2;

                            tagValue = value.Substring(lenghtToDiscard, valueLenght);

                            string tag = "T" + tagInResp + ":" + tagLenght + ":h" + tagValue;

                            sbEmvResponse.Append(tag + " ");

                            lenghtToDiscard += valueLenght;
                        }
                        else if (tags.Contains(tagInResp2char))
                        {
                            tagLenght = value.Substring(2, 2);
                            lenghtToDiscard = 4;

                            valueLenght = Convert.ToInt32(tagLenght, 16) * 2;

                            tagValue = value.Substring(lenghtToDiscard, valueLenght);

                            string tag = string.Empty;
                            if (tagInResp2char == "91" && regEx > 0)
                            {
                                tag = "T" + tagInResp2char + ":" + tagLenght + ":" + tagValue;
                            }
                            else if (tagInResp2char == "8A")    //Handling tag 8A
                            {
                                tagValue = (tagValue == "3535") ? "3035" : tagValue;
                                tag = "T" + tagInResp2char + ":" + tagLenght + ":" + tagValue;
                            }
                            else
                            {
                                tag = "T" + tagInResp2char + ":" + tagLenght + ":" + tagValue;
                            }

                            // If we got a tag above, then add it to the response.
                            if (!string.IsNullOrEmpty(tag))
                                sbEmvResponse.Append(tag + " ");

                            lenghtToDiscard += valueLenght;
                        }

                        value = value.Substring(lenghtToDiscard, value.Length - lenghtToDiscard);

                        if (string.IsNullOrEmpty(value))
                            isAlive = false;
                    }
                }
            }

            return sbEmvResponse.ToString();
        }

        /// <summary>
        /// Get tag wise data
        /// </summary>
        /// <param name="emvDataDictionary"></param>
        /// <param name="tagType"></param>
        /// <returns></returns>
        public static string GetTagWiseEmvData(Dictionary<string, Tuple<string, string>> emvDataDictionary, EmvTagType tagType)
        {
            StringBuilder sbEmvData = new StringBuilder();
            switch (tagType)
            {
                case EmvTagType.AuthorizationRequestEMVTags:
                    string[] visaCreditSaleTags = EMVTags.AuthorizationRequestEMVTags.Split(',');

                    int tagCount = visaCreditSaleTags.Length;

                    // Preparing EMV data.
                    for (int i = 0; i < tagCount; i++)
                    {
                        string tag = visaCreditSaleTags[i];
                        if (emvDataDictionary.ContainsKey(tag))
                        {
                            string tagLength = emvDataDictionary[tag].Item1;
                            string tagValue = emvDataDictionary[tag].Item2.Trim();
                            string val = tag.Trim() + tagLength.Trim() + tagValue.Trim();
                            sbEmvData.Append(val.Trim());
                        }
                    }

                    break;
            }

            return sbEmvData.ToString();
        }

        /// <summary>
        /// Fromatting tag name
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string getTagName(this string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                string tagInitials = value.Substring(0, 1);
                if (tagInitials.Equals("T"))
                {
                    value = value.Substring(1, value.Length - 2).RemoveFS();
                }
                else
                {
                    value = value.Substring(1, value.Length - 1).RemoveFS();
                }
            }

            return value;
        }

        /// <summary>
        /// Split string extension method.
        /// </summary>
        /// <param name="searchStr"></param>
        /// <param name="separators"></param>
        /// <returns></returns>
        public static List<string> SplitString(this string searchStr, string[] separators)
        {
            List<string> result = new List<string>();
            int length = searchStr.Length;
            int lastMatchEnd = 0;
            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < separators.Length; j++)
                {
                    string str = separators[j];
                    int sepLen = str.Length;
                    if (((searchStr[i] == str[0]) && (sepLen <= (length - i))) && ((sepLen == 1) || (String.CompareOrdinal(searchStr, i, str, 0, sepLen) == 0)))
                    {
                        result.Add(searchStr.Substring(lastMatchEnd, i - lastMatchEnd));
                        result.Add(separators[j]);
                        i += sepLen - 1;
                        lastMatchEnd = i + 1;
                        break;
                    }
                }
            }

            if (lastMatchEnd != length)
                result.Add(searchStr.Substring(lastMatchEnd));

            return result;
        }

        /// <summary>
        /// Returns the value of Emv tag after taking care of ASCII & Hex values.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string GetTagValue(this string data)
        {
            if (!string.IsNullOrEmpty(data))
            {
                data = data.Trim();
                string valType = data.Substring(0, 1);
                if (valType == "a")
                {
                    data = data.Substring(1, data.Length - 1);
                    data = data.AsciiToHex();
                }
                else if (valType == "h")
                {
                    data = data.Substring(1, data.Length - 1);
                }
            }

            return data;
        }

        /// <summary>
        /// Convert ASCII value into HAX string.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string AsciiToHex(this string data)
        {
            StringBuilder sbhexchars = new StringBuilder();
            if (!string.IsNullOrEmpty(data))
            {
                var i = 0;
                var byteArray = Encoding.ASCII.GetBytes(data);
                while (i != byteArray.Length)
                {
                    sbhexchars.Append((byteArray[i]).ToString("X"));
                    i++;
                }
            }

            return sbhexchars.ToString();
        }

        /// <summary>
        /// Convert Hex to ASCII
        /// </summary>
        /// <param name="HexString"></param>
        /// <returns></returns>
        public static string HextoAscii(this string HexString)
        {
            StringBuilder sbASCIIString = new StringBuilder();
            if (!string.IsNullOrEmpty(HexString))
            {
                for (int i = 0; i < HexString.Length; i += 2)
                {
                    if (HexString.Length >= i + 2)
                    {
                        sbASCIIString.Append(Convert.ToChar(Convert.ToUInt32(HexString.Substring(i, 2), 16)).ToString());
                    }
                }
            }

            return sbASCIIString.ToString();
        }

        /// <summary>
        /// Remove file separateor 'FS' from a string. ASCII value of 1C is FS. FS means 'File Separator'
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string RemoveFS(this string data)
        {
            if (!string.IsNullOrEmpty(data))
            {
                data = data.Replace(((char)28).ToString(), "");
            }

            return data;
        }

        /// <summary>
        /// Append file separateor 'FS' to supplied string.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string AppendFS(this string data)
        {
            if (!string.IsNullOrEmpty(data))
            {
                data = ((char)28).ToString() + data;
            }

            return data;
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string FormatEMVAmount(this string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                double amount = Convert.ToDouble(value);
                value = (amount / 100).ToString();
            }

            return value;
        }
    }
}