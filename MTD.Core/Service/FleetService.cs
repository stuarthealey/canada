﻿using System.Collections.Generic;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Extension;
using MTD.Core.Factory.Interface;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Core.Service
{
    public class FleetService : IFleetService
    {
        private readonly IFleetRepository _fleetRepository;
        private readonly IFleetFactory _fleetFactory;

        public FleetService([Named("FleetRepository")] IFleetRepository fleetRepository, [Named("FleetFactory")] IFleetFactory fleetFactory) //("MerchantRepository") maped in class NinjectObjectMapper in MTD.Core.Infrastructure same for ("MerchantFactory")
        {
            try
            {
                _fleetRepository = fleetRepository; //need to initilize object only once due to ninject
                _fleetFactory = fleetFactory;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Add fleet
        /// Function Name       :   Add
        /// Created By          :   Umesh Kumar
        /// Created On          :   03/11/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetDto"></param>
        /// <param name="userType"></param>
        /// <param name="userId"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public bool Add(FleetDto fleetDto, int userType, int userId, int merchantId) // for adding fleet
        {
            try
            {
                Fleet fleet = _fleetFactory.CreateFleet(fleetDto);
                return _fleetRepository.Add(fleet, userType, userId, merchantId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Fleets List
        /// Function Name       :   GetAdminUserListByFilter
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   03/08/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="userType"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<FleetDto> FleetList(int merchantId, int userType, int userId)// get list of users
        {
            try
            {
                IEnumerable<Fleet> fleetlist = _fleetRepository.FleetList(merchantId, userType, userId);
                var fleetDtoList = fleetlist.ToDTOList();
                return fleetDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To GetFleet
        /// Function Name       :   GetFleet
        /// Created By          :   Asif Shafeeque
        /// Created On          :   03/08/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public FleetDto GetFleet(int fleetId, int merchantId) // get user on the basis of id
        {
            try
            {
                Fleet obj = _fleetRepository.GetFleet(fleetId, merchantId);
                FleetDto fleetDto = obj.ToDtoFleet();
                return fleetDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update the Fleet
        /// Function Name       :   Update
        /// Created By          :   Salil Gupta
        /// Created On          :   03/05/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetDto"></param>
        /// <param name="fleetId"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public bool Update(FleetDto fleetDto, int fleetId, int merchantId) //
        {
            try
            {
                Fleet fleet = _fleetFactory.CreateFleet(fleetDto);
                bool result = _fleetRepository.Update(fleet, fleetId, merchantId);
                return result;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Delete the Fleet
        /// Function Name       :   DeleteFleet
        /// Created By          :   Asif Shafeeque
        /// Created On          :   03/03/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public bool DeleteFleet(int fleetId) // deleting user
        {
            try
            {
                var result = _fleetRepository.DeleteFleet(fleetId);
                return result;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get User Fleets List
        /// Function Name       :   GetUserFleetListByFilter
        /// Created By          :   Umesh Kumar
        /// Created On          :   03/01/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="filterSearch"></param>
        /// <returns></returns>
        public IEnumerable<FleetDto> GetUserFleetListByFilter(FilterSearchParametersDto filterSearch, int logOnByUserId, int logOnByUserType)// get list of users
        {
            try
            {
                var filterSearchClass = filterSearch.SearchParameters();
                IEnumerable<Fleet> fleetlist = _fleetRepository.GetUserFleetListByFilter(filterSearchClass, logOnByUserId, logOnByUserType);
                var fleetDtoList = fleetlist.ToDTOList();
                return fleetDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To GetFleet
        /// Function Name       :   GetFleet
        /// Created By          :   Asif Shafeeque
        /// Created On          :   03/08/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public FleetDto GetFleet(int fleetId) // get user on the basis of id
        {
            try
            {
                Fleet obj = _fleetRepository.GetFleet(fleetId);
                FleetDto fleetDto = obj.ToDtoFleet();
                return fleetDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Add fleet settings
        /// Function Name       :   Add
        /// Created By          :   Umesh Kumar
        /// Created On          :   06/08/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetSettingDto"></param>
        /// <returns></returns>
        public bool Add(FleetSettingDto fleetSettingDto)
        {
            try
            {
                Fleet fleet = fleetSettingDto.ToFleet();
                bool result = _fleetRepository.Add(fleet);
                return result;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get list of users
        /// Function Name       :   FleetSettingList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   06/08/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public IEnumerable<FleetSettingDto> FleetSettingList(FilterSearchParametersDto filterSearch)// get list of users
        {
            try
            {
                FilterSearchParameters filterSearchClass = filterSearch.SearchParameters();
                IEnumerable<Fleet> fleetList = _fleetRepository.FleetSettingListByFiter(filterSearchClass);
                var fleetSettingList = fleetList.ToFleetSettingDtoList();
                return fleetSettingList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Fleet Setting
        /// Function Name       :   GetFleetSetting
        /// Created By          :   Asif Shafeeque
        /// Created On          :   06/08/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public FleetSettingDto GetFleetSetting(int fleetId)
        {
            return _fleetRepository.GetFleetSetting(fleetId).ToFleetSettingDto();
        }

        /// <summary>
        /// Purpose             :   To Delete the Fleet setting
        /// Function Name       :   DeleteFleetSetting
        /// Created By          :   Asif Shafeeque
        /// Created On          :   06/08/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public bool DeleteFleetSetting(int fleetId)
        {
            try
            {
                return _fleetRepository.DeleteFleetSetting(fleetId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Setting Fleet List
        /// Function Name       :   GetSettingFleetList
        /// Created By          :   Asif
        /// Created On          :   06/23/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<FleetDto> GetSettingFleetList(int merchantId)
        {
            IEnumerable<Fleet> fleetList = _fleetRepository.GetSettingFleetList(merchantId);
            var fleetSettingList = fleetList.ToDTOList();
            return fleetSettingList;
        }

        /// <summary>
        /// Purpose             :   To get currency
        /// Function Name       :   GetCurrency
        /// Created By          :  Madhuri Tanwar
        /// Created On          :   06/23/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public FleetDto GetCurrency(int CurrencyCodeID) // get Currency on the basis of id
        {
            try
            {
                Fleet obj = _fleetRepository.GetCurrency(CurrencyCodeID);
                FleetDto CurrencyDto = obj.ToDtoFleet();
                return CurrencyDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Currency List
        /// Function Name       :   CurrencyList
        /// Created By          :  Madhuri Tanwar
        /// Created On          :   06/23/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CurrencyDto> CurrencyList()
        {
            try
            {
                IEnumerable<Currency> currencyList = _fleetRepository.CurrencyList();
                return currencyList.ToDtoCurrencyList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fsParametes"></param>
        /// <param name="logOnByUserId"></param></param>
        /// <param name="logOnByUserType"></param>
        /// <returns></returns>
        public int FleetListCount(FilterSearchParametersDto fsParametes, int logOnByUserId, int logOnByUserType)
        {
            try
            {
                return _fleetRepository.FleetListCount(fsParametes.SearchParameters(), logOnByUserId, logOnByUserType);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fsParametes"></param>
        /// <returns></returns>
        public int FleetSettingListCount(FilterSearchParametersDto fsParametes)
        {
            try
            {
                return _fleetRepository.FleetSettingListCount(fsParametes.SearchParameters());
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="userType"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<FleetDto> FeeFleetList(int merchantId, int userType, int userId)
        {
            try
            {
                IEnumerable<Fleet> fleetlist = _fleetRepository.FeeFleetList(merchantId, userType, userId);
                var fleetDtoList = fleetlist.ToDTOList();
                return fleetDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleetDto"></param>
        /// <param name="userType"></param>
        /// <param name="userId"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public bool AddFleetFee(FleetDto fleetDto) // for adding fleet fee
        {
            try
            {
                Fleet fleet = _fleetFactory.CreateFleet(fleetDto);
                return _fleetRepository.AddFleetFee(fleet);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fsParametes"></param>
        /// <returns></returns>
        public IEnumerable<SurchargeDto> FleetFeetListByFilter(FilterSearchParametersDto fsParametes)
        {
            IEnumerable<Surcharge> fleetFeetList = _fleetRepository.FleetFeetListByFilter(fsParametes.SearchParameters());
            var fleetfeeDtoList = fleetFeetList.ToDTOSurchargeList();
            return fleetfeeDtoList;
        }

        public SurchargeDto GetFleetFee(int surchargeId)
        {
            try
            {
                SurchargeDto surchargeDto = _fleetRepository.GetFleetFee(surchargeId).ToSurchargeDto();
                return surchargeDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fsParametes"></param>
        /// <returns></returns>
        public int FleetFeeListCount(FilterSearchParametersDto fsParametes)
        {
            try
            {
                return _fleetRepository.FleetFeeListCount(fsParametes.SearchParameters());
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="surchargeDto"></param>
        /// <returns></returns>
        public bool UpdateFleetFee(SurchargeDto surchargeDto)
        {
            try
            {
                return _fleetRepository.UpdateFleetFee(surchargeDto.ToSurcharge());
            }
            catch
            {
                throw;
            }
        }

        public void DeleteFleetFee(int surchargeId)
        {
            try
            {
                _fleetRepository.DeleteFleetFee(surchargeId);
            }
            catch
            {
                throw;
            }
        }

        public List<int> GetAccesibleFleet(int logOnByUserId, int merchantId)
        {
            return _fleetRepository.GetAccesibleFleet(logOnByUserId, merchantId);
        }

        public string Authenticate(string SystemID, string userName, string pCode)
        {
            return _fleetRepository.Authenticate(SystemID, userName, pCode);
        }

        public bool tokenValidation(string token, int fid)
        {
            return _fleetRepository.tokenValidation(token, fid);
        }

        public bool tokenReset(string token)
        {
            return _fleetRepository.tokenReset(token);
        }

        /// <summary>
        /// Purpose             :   To get the currency code by fleet
        /// Function Name       :   GetCurrencyByFleet
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/27/2016
        /// </summary>
        ///<param name="fleetId"></param>
        /// <returns>int</returns>
        public int GetCurrencyByFleet(int fleetId)
        {
            return _fleetRepository.GetCurrencyByFleet(fleetId);
        }
    }
}
