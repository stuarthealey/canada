IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Datawire]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_Datawire]
GO

CREATE PROCEDURE dbo.usp_Datawire
(
	@merchantId INT = null,
	@id INT = null
)
AS
BEGIN
	SELECT * 
	FROM dbo.Datawire 
	WHERE MerchantId = @merchantId 
	  AND ID = @id 
	  AND IsActive = 1
	  AND IsAssigned = 1
END
GO
