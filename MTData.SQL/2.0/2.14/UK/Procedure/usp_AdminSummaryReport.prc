
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_AdminSummaryReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_AdminSummaryReport]
GO

/****** Object:  StoredProcedure [dbo].[usp_AdminSummaryReport]    Script Date: 11/2/2015 10:59:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
 
/****** Object:  StoredProcedure [dbo].[usp_AdminSummaryReport]    Script Date: 11/23/2015 4:45:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <10-06-2015>
-- Description:	<Prepare summary reports for admin users>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AdminSummaryReport]
(
  @XmlData NVARCHAR(MAX) OUT,
  @ReportType INT OUT,
  @FDate DATE OUT,
  @TDate DATE OUT,
  @TotalTransaction INT OUT,
  @TotalActualFare NVARCHAR(30) OUT,
  @TotalTips NVARCHAR(30) OUT,
  @TotalFareAmount NVARCHAR(30) OUT,
  @TotalSurcharge NVARCHAR(30) OUT,
  @TotalFee NVARCHAR(30) OUT,
  @NoChargeBack INT OUT,
  @ChargeBackValue NVARCHAR(30) OUT,
  @NoVoid INT OUT,
  @VoidValue NVARCHAR(30) OUT,
  @TotalTechFee NVARCHAR(35) OUT,
  @AmountOwing  NVARCHAR(30) OUT
)
AS
BEGIN TRY
  SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF
  DECLARE @Frequency INT,
          @FrequencyType INT,
          @DateFrom DATE,
          @DateTo DATE,
		  @ScheduleId INT,
		  @NextScheduleDateTime DATETIME
  DECLARE @InnerTable TABLE
   (
     Id INT, 
     FareValue MONEY,
     Tip MONEY, 
     Amount MONEY, 
     Surcharge MONEY, 
     Fee MONEY, 
     FleetID INT, 
     TxnType VARCHAR(15),
     ResponseCode VARCHAR(25),
	 fk_FleetId INT,
	 IsRefunded BIT,
	 IsVoided BIT,
	 IsCompleted BIT,
	 TechFee MONEY
   )  
  DECLARE @OuterTable TABLE
   (
	  Id  INT, 
	  FareValue MONEY, 
	  Tip MONEY,Amount MONEY,
	  Surcharge MONEY, 
	  Fee MONEY, 
	  TxnType VARCHAR(15),
	  ResponseCode NVARCHAR(25),
	  fk_FleetId INT,
	  TechFee MONEY,
	  IsRefunded BIT,
      IsVoided BIT,
	  IsCompleted BIT
   )   
   SELECT @ScheduleId=ScheduleID,
          @Frequency=Frequency,
          @FrequencyType=FrequencyType,
		  @NextScheduleDateTime=NextScheduleDateTime 
   FROM ScheduleReport 
   WHERE fk_ReportId=1 AND IsCreated=1 AND fk_MerchantId IS NULL
   IF(@Frequency=1)
   BEGIN
      SET @DateFrom=GETDATE()-@FrequencyType
      SET @DateTo=GETDATE()
	  SET @NextScheduleDateTime=@NextScheduleDateTime+@FrequencyType
   END
   ELSE IF(@Frequency=2)
   BEGIN
      SET @DateFrom = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-@FrequencyType,GETDATE())), 0)-1
      SET @DateTo= DATEADD(WEEK, DATEDIFF(WEEK, 0, GETDATE()), 0)-1
	  SET @NextScheduleDateTime=DATEADD(WK,@FrequencyType,@NextScheduleDateTime)
   END
   ELSE IF(@Frequency=3)
   BEGIN
      SET @DateFrom =DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH,-@FrequencyType,GETDATE())), 0)
      SET @DateTo=DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) 
	  SET @NextScheduleDateTime=DATEADD(MONTH,@FrequencyType,@NextScheduleDateTime)
   END   
   ELSE
   BEGIN
      RETURN 3
   END 
   UPDATE ScheduleReport SET NextScheduleDateTime=@NextScheduleDateTime WHERE ScheduleID=@ScheduleId
   --insert data into table variable InnerTable for reuse*/                         
   INSERT INTO @InnerTable(Id,FareValue,Tip,Amount,Surcharge,Fee,FleetID,TxnType,ResponseCode,IsRefunded,IsVoided,IsCompleted,TechFee)                   
   SELECT  mt.Id
          ,mt.FareValue
		  ,mt.Tip
		  ,mt.Amount
		  ,mt.Surcharge
		  ,mt.Fee
		  ,fl.FleetID
		  ,mt.TxnType
		  ,mt.ResponseCode
		  ,mt.IsRefunded
		  ,mt.IsVoided
		  ,mt.IsCompleted
		  ,mt.TechFee		
   FROM MTDTransaction mt 
   INNER JOIN Vehicle vh ON mt.VehicleNo=vh.VehicleNumber 
   INNER JOIN Fleet fl ON vh.fk_FleetID=fl.FleetID
   WHERE CAST(mt.TxnDate AS DATE)>=@DateFrom 
         AND CAST(mt.TxnDate AS DATE)<@DateTo 
		 AND mt.fk_FleetId=vh.fk_FleetID
   --insert data into table variable OuterTable for reuse*/    
   INSERT INTO @OuterTable(Id,FareValue,Tip,Amount,Surcharge,Fee,TxnType,ResponseCode,fk_FleetId,TechFee,IsRefunded,IsVoided,IsCompleted) 
   SELECT  t.Id
          ,t.FareValue
		  ,t.Tip,t.Amount
		  ,t.Surcharge
		  ,t.Fee
		  ,t.TxnType
		  ,t.ResponseCode
		  ,t.fk_FleetId
		  ,t.TechFee
	      ,t.IsRefunded
	      ,t.IsVoided
		  ,t.IsCompleted
   FROM  MTDTransaction t 
   WHERE CAST(t.TxnDate AS DATE)>=@DateFrom AND
	     CAST(t.TxnDate AS DATE)<@DateTo                           
   SET @XmlData = CAST((SELECT  ROW_NUMBER() OVER (ORDER BY Company) AS 'td',''
                               ,ISNULL(mer.Company,'') AS 'td',''
							   ,ISNULL(f.FleetName,'')  AS 'td',''
							   ,(SELECT ISNULL(COUNT(id),0) FROM @InnerTable it WHERE  it.FleetID=f.FleetID AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND it.IsVoided=0 AND it.ResponseCode IN ('000','002')) AS 'td',''
							   ,CONCAT('$',(SELECT ISNULL(SUM(FareValue),'0.00') FROM @InnerTable it WHERE  it.FleetID=f.FleetID  AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002'))) AS 'td',''
							   ,CONCAT('$',(SELECT ISNULL(SUM(Tip),'0.00') FROM @InnerTable it WHERE  it.FleetID=f.FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND  it.ResponseCode IN ('000','002'))) AS 'td',''
							   ,CONCAT('$',(SELECT ISNULL(SUM(Surcharge),'0.00') FROM @InnerTable it WHERE  it.FleetID=f.FleetID  AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002'))) AS 'td',''						
							   ,CONCAT('$',(SELECT ISNULL(SUM(Fee),'0.00') FROM @InnerTable it WHERE  it.FleetID=f.FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002'))) AS 'td',''
							   ,(SELECT ISNULL(COUNT(id),0) FROM @InnerTable it  WHERE  it.FleetID=f.FleetID AND it.TxnType='Refund'AND it.ResponseCode='000') AS 'td',''
							   ,CONCAT('$',(SELECT ISNULL(SUM(Amount),'0.00') FROM @InnerTable it WHERE  it.FleetID=f.FleetID AND it.TxnType='Refund'AND it.ResponseCode ='000')) AS 'td',''
							   ,(SELECT ISNULL(COUNT(id),0) FROM @InnerTable it WHERE  it.FleetID=f.FleetID AND it.TxnType='Void'AND it.ResponseCode='000') AS 'td',''
							   ,CONCAT('$',(SELECT ISNULL(SUM(Amount),'0.00') FROM @InnerTable it WHERE  it.FleetID=f.FleetID AND it.TxnType='Void'AND it.ResponseCode='000')) AS 'td',''
							   ,CONCAT('$',(SELECT ISNULL(SUM(Amount),'0.00') FROM @InnerTable it WHERE  it.FleetID=f.FleetID AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND IT.IsVoided=0 AND it.ResponseCode IN ('000','002'))) AS 'td',''  
							   ,CONCAT('$',(SELECT ISNULL(SUM(it.TechFee),'0.00') FROM @InnerTable it WHERE  it.FleetID=f.FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002'))) AS 'td',''	              							 						
							   ,CONCAT('$',(SELECT ISNULL(SUM(Amount),0.00) FROM @InnerTable it WHERE  it.FleetID=f.FleetID AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND IT.IsVoided=0 AND it.ResponseCode IN ('000','002'))-
							   (SELECT ISNULL(SUM(TechFee),'0.00') FROM @InnerTable it WHERE  it.FleetID=f.FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002'))) AS 'td'
                        FROM MTDTransaction m 
					    INNER JOIN  Vehicle v ON m.VehicleNo=v.VehicleNumber 
					    INNER JOIN fleet f ON v.fk_FleetID=f.FleetID 
                        INNER JOIN merchant mer ON f.fk_MerchantID=mer.MerchantID 
					    WHERE  m.ResponseCode IN ('000','002')   
						       AND CAST(m.TxnDate AS DATE)>=@DateFrom
							   AND CAST(m.TxnDate AS DATE)<@DateTo    
							   AND m.fk_FleetId=v.fk_FleetID
							   AND m.Id NOT IN (SELECT Id FROM dbo.MTDTransaction WHERE m.TxnType='Authorization' AND m.IsCompleted=0)
                        GROUP BY f.FleetName,f.FleetID,mer.Company 
					    ORDER BY mer.Company 
					    FOR XML PATH('tr'), ELEMENTS ) AS NVARCHAR(MAX))
   SELECT @TotalTransaction=ISNULL(COUNT(Id),0) 
   FROM @OuterTable ot 
   WHERE ot.TxnType IN ('Sale','Completion') 
         AND ot.ResponseCode IN ('000','002')
		 AND ot.IsRefunded=0 AND ot.IsVoided=0
   SELECT @TotalActualFare=CONCAT('$',CAST(ISNULL(SUM(ot.FareValue),'0.00') AS DECIMAL(18,2)))
         ,@TotalTips=CONCAT('$',CAST(ISNULL(SUM(ot.Tip),'0.00') AS DECIMAL(18,2)))
		 ,@TotalSurcharge=CONCAT('$',CAST(ISNULL(SUM(ot.Surcharge),'0.00') AS DECIMAL(18,2)))
		 ,@TotalFee=CONCAT('$',CAST(ISNULL(SUM(ot.Fee),'0.00') AS DECIMAL(18,2)))
		 ,@TotalTechFee=CAST(ISNULL(SUM(ot.TechFee),'0.00') AS DECIMAL(16,2))	
   FROM  @OuterTable ot 
   WHERE ot.ResponseCode IN ('000','002') 
	     AND ((ot.TxnType='Sale' AND ot.IsRefunded=0 AND ot.IsVoided=0) 
		 OR (ot.TxnType='Authorization' AND ot.IsCompleted=1)) 
   SELECT  @TotalFareAmount=CONCAT('$',CAST(ISNULL(SUM(ot.Amount),'0.00') AS DECIMAL(16,2))),
	       @AmountOwing=CONCAT('$',CAST(ISNULL(SUM(ot.Amount),0.00) AS DECIMAL(18,2))-CAST(@TotalTechFee AS DECIMAL(18,2)))
   FROM    @OuterTable ot
   WHERE   ot.ResponseCode IN ('000','002') AND ot.TxnType IN ('Sale','Completion') AND ot.IsRefunded=0 AND ot.IsVoided=0 
   SELECT @NoChargeBack=ISNULL(COUNT(ot.Id),'0')
         ,@ChargeBackValue=CONCAT('$',CAST(ISNULL(SUM(ot.Amount),'0.00') AS DECIMAL(18,2))) 
   FROM   @OuterTable ot 
   WHERE ot.TxnType ='Refund' AND ot.ResponseCode='000'
   SELECT @NoVoid=ISNULL(COUNT(Id),'0')
         ,@VoidValue=CONCAT('$',CAST(ISNULL(SUM(Amount),'0.00') AS DECIMAL(18,2))) 
   FROM   @OuterTable ot 
   WHERE ot.TxnType ='Void' AND ot.ResponseCode='000'
   SET   @ReportType=@Frequency 
   SET   @FDate=@DateFrom
   SET   @TDate= dateadd(day,-1,@DateTo)
   SET   @TotalTechFee=CONCAT('$',@TotalTechFee)
   SELECT @XmlData
         ,@ReportType
		 ,@FDate
		 ,@TDate
		 ,@TotalTransaction
		 ,@TotalActualFare
		 ,@TotalTips
		 ,@TotalFareAmount
		 ,@TotalSurcharge
		 ,@TotalFee
		 ,@NoChargeBack
		 ,@ChargeBackValue
		 ,@NoVoid
		 ,@VoidValue
		 ,@AmountOwing
		 ,@TotalTechFee
END TRY
--use catch to log error with details in ProcErrorHandler table
BEGIN CATCH
      INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
      SELECT ERROR_NUMBER()
	        ,ERROR_SEVERITY()
			,ERROR_STATE()
			,ERROR_PROCEDURE()
			,ERROR_LINE()
			,ERROR_MESSAGE()
			,SUSER_SNAME()
			,HOST_NAME()
			,GETDATE()
END CATCH
GO

