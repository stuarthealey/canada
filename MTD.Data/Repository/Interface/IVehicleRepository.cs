﻿using System.Collections.Generic;
using MTD.Data.Data;

namespace MTD.Data.Repository.Interface
{
    public interface IVehicleRepository
    {

        int Add(Vehicle vehicle);
        IEnumerable<Vehicle> VehicleList(int merchantId);
        int Update(Vehicle vehicle, int vehicleId,int terminalId);
        Vehicle GetVehicle(int vehicleId);
        Vehicle GetVehicleByTerminal(int terminalId);
        void DeleteVehicle(int vehicleId);
        IEnumerable<Vehicle> GetVehileListByFilter(FilterSearchParameters fsParametes);
        IEnumerable<Vehicle> GetVehileList(FilterSearchParameters fsParametes);
        int IsValidVehicle(Vehicle vehicle, int merchantId, int userId, int userType);
        Vehicle VehicelExcel(Vehicle vehicle, int merchantId);
        IEnumerable<Vehicle> UserVehicleList(FilterSearchParameters fsParametes);
        int UserVehicleCount(FilterSearchParameters fsParametes);
         IEnumerable<CarOwner> CarOwnerList(int merchantId);
         IEnumerable<CarOwner> PayCarOwnerList(int FleetId);
         Vehicle VehicleListInFleet(string token, string carNumber, int fleetID);

         bool IsDuplicateCarNumber(int fleetId, string CarNumber);

         bool FindFleet(int fleetId);

         Vehicle AddDespetchVehicle(Vehicle vehicle, string token);
    }
}
