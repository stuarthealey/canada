IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SearchTransactionReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SearchTransactionReport]
 GO
/****** Object:  StoredProcedure [dbo].[usp_SearchTransactionReport]    Script Date: 05-11-2015 4:18:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SearchTransactionReport]
	-- Add the parameters for the stored procedure here
	@TransId int = NULL, -- Pass Null in case no trans ID
	@VehicleId varchar(50) = NULL,
	@DriverId nvarchar(50) = NULL,
	@DateFrom datetime = NULL,
	@DateTo datetime = NULL,
	@CardType nvarchar(20) = NULL,
	@EntryMode nvarchar(20) = NULL, 
	@AuthorizeCode nvarchar(20) = NULL,
	@PaymentType nvarchar(20) = NULL,
	@TransType nvarchar(20) = NULL, 
	@fk_MerchantID int = NULL,
	@ExpDate varchar(50) = NULL,
	@MinAmount money = NULL,
	@MaxAmount money = NULL,
	@FirstFour varchar(10) = NULL,
	@LastFour varchar(10) = NULL,
	@Approval nvarchar(20)=NULL,
	@JobNumber varchar(15) = NULL,
	@fk_FleetId nvarchar(20) = NULL,
	@IsVarified bit = NULL,
	@RCTerminalId  NVARCHAR(20)=NULL,
	@SerialNo NVARCHAR(50)=NULL
AS
BEGIN
	SELECT Id, MerchantId, VehicleNo, DriverNo, JobNumber, Stan, TransRefNo, TransOrderNo, PaymentType, TxnType, LocalDateTime, TransmissionDateTime, 
	FareValue, Tip, Surcharge, Fee, Taxes, Toll, Amount, Currency, ExpiryDate, CardType, Industry, EntryMode, 
	ResponseCode, AddRespData, AuthId, AthNtwId, RequestXml, ResponseXml,  SourceId, 
	TransNote, CardToken, IsCompleted, IsRefunded, IsVoided, CreatedBy,  TxnDate , fk_FleetId, TechFee, FleetFee, IsVarified,TerminalId,SerialNo, IsDownloaded
	FROM [MTDTransaction] T
	where (T.Id = @TransId OR COALESCE(@TransId,0) = 0)
	AND (T.VehicleNo = @VehicleId OR COALESCE(@VehicleId,'') = '')
	AND (T.DriverNo = @DriverId OR COALESCE(@DriverId,'') = '')
	AND (T.TxnDate >= @DateFrom OR @DateFrom IS NULL)
	AND (T.TxnDate <= @DateTo OR @DateTo IS NULL)
	AND (T.CardType = @CardType OR COALESCE(@CardType,'') = '')
	AND (T.EntryMode = @EntryMode OR COALESCE(@EntryMode,'') = '')
	AND (T.AuthId = @AuthorizeCode OR COALESCE(@AuthorizeCode,'') = '')
	AND (T.Industry = @PaymentType OR COALESCE(@PaymentType,'') = '')
	AND (T.TxnType = @TransType OR COALESCE(@TransType,'') = '')
	AND (T.MerchantId = @fk_MerchantID OR COALESCE(@fk_MerchantID,0) = 0)
	AND (T.ExpiryDate = @ExpDate OR COALESCE(@ExpDate,'') = '')
	AND (T.Amount >= @MinAmount OR COALESCE(@MinAmount,0) = 0)
	AND (T.Amount <= @MaxAmount OR COALESCE(@MaxAmount,0) = 0)
	AND (T.FirstFourDigits = @FirstFour OR COALESCE(@FirstFour,'') = '')
	AND (T.LastFourDigits = @LastFour OR COALESCE(@LastFour,'') = '')
	AND (T.AddRespData = @Approval OR COALESCE(@Approval,'') = '')
	AND (T.JobNumber = @JobNumber OR COALESCE(@JobNumber,'') = '')
	AND (T.fk_FleetId = @fk_FleetId OR COALESCE(@fk_FleetId,'') = '')
	AND (T.IsVarified = @IsVarified OR @IsVarified IS NULL)
	AND (T.TerminalId = @RCTerminalId OR  COALESCE(@RCTerminalId,'') = '')
	AND (T.SerialNo = (SELECT TOP 1 MacAdd FROM Terminal WHERE SerialNo=@SerialNo AND IsActive=1) OR COALESCE(@SerialNo,'') = '')
	OPTION (RECOMPILE)
END
return
GO
 

