﻿using System;
using System.Globalization;
using log4net;

namespace MTData.Utility
{
    public  class Logger : ILogger
    {
        #region Datamembers

        private static ILog _log = null;

        #endregion

        #region Class Initializer

        public Logger()
        {
            _log = LogManager.GetLogger(typeof(Logger));
            GlobalContext.Properties["host"] = Environment.MachineName;
        }

        #endregion

        #region ILogger Members

        public void EnterMethod(string methodName)
        {
            if (_log.IsInfoEnabled)
                _log.Info(String.Format(CultureInfo.InvariantCulture, "Entering Method {0}", methodName));
        }

        public void LeaveMethod(string methodName)
        {
            if (_log.IsInfoEnabled)
                _log.Info(String.Format(CultureInfo.InvariantCulture, "Leaving Method {0}", methodName));
        }

        public void LogException(Exception exception)
        {
            if (_log.IsErrorEnabled)
                _log.Error(String.Format(CultureInfo.InvariantCulture, "{0}", exception.Message), exception);
        }

        public void LogError(string message)
        {
            if ((_log.IsErrorEnabled) && !string.IsNullOrEmpty(message))
                _log.Error(String.Format(CultureInfo.InvariantCulture, "{0}", message));
        }

        public void LogWarningMessage(string message)
        {
            if ((_log.IsWarnEnabled) && !string.IsNullOrEmpty(message))
                _log.Warn(String.Format(CultureInfo.InvariantCulture, "{0}", message));
        }

        public void LogInfoMessage(string message)
        {
            if ((_log.IsInfoEnabled) && !string.IsNullOrEmpty(message))
                _log.Info(String.Format(CultureInfo.InvariantCulture, "{0}", message));
        }

        public void LogInfoFatal(string message, Exception exception)
        {
             if (_log.IsInfoEnabled)
                 _log.Fatal(message,exception);
        }

        #endregion
    }
}
