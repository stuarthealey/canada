﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Models
{
    public class EmvDataValues
    {
        public string Track2 { get; set; }
        public string Track3 { get; set; }
        public string EmvData { get; set; }
        public string Amount { get; set; }
        public string PinData { get; set; }
        public string CSequence { get; set; }
        public PymtTypeType PaymentType { get; set; }
        public string AccountType { get; set; }
        public string TagAID { get; set; }
        public string DeviceSerialNo { get; set; }
    }

}