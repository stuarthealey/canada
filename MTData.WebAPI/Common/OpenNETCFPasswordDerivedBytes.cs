﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace MTData.WebAPI.Common
{
    /// <summary>
    /// Required for MJM implementation.
    /// MJM for some strange reason used a third-party implimentation rather than the System.Security.Cryptography.PasswordDerivedBytes
    /// The following code is deived from a decompilation of the DLL used by MJM
    /// </summary>
    public class OpenNETCFPasswordDerivedBytes
    {
        #region Attributes

        private string _Password;
        private byte[] _Salt;
        private string _HashName;
        private int _Iterations;

        #endregion


        #region Constructor

        public OpenNETCFPasswordDerivedBytes(string password, byte[] salt, string hashName, int iterations)
        {
            _Password = password;
            _Salt = salt;
            _HashName = hashName;
            _Iterations = iterations;
        }

        #endregion


        #region Public Methods

        public byte[] GetBytes(int cb)
        {
            byte[] buffer = ASCIIEncoder(_Password);
            if (_Salt != null)
            {
                byte[] numArray = new byte[buffer.Length + _Salt.Length];
                Array.Copy(buffer, 0, numArray, 0, buffer.Length);
                Array.Copy(_Salt, 0, numArray, buffer.Length, _Salt.Length);
                buffer = numArray;
            }
            HashAlgorithm hashAlgorithm = GetHashAlgorithm(_HashName);
            if (_Iterations <= 0)
                _Iterations = 1;
            int destinationIndex = 0;
            byte[] numArray1 = new byte[cb];
            while (destinationIndex < cb)
            {
                for (int index = 0; index < _Iterations; ++index)
                {
                    buffer = hashAlgorithm.ComputeHash(buffer);
                }
                int num = cb - destinationIndex;
                int length = num < buffer.Length ? num : buffer.Length;
                Array.Copy(buffer, 0, numArray1, destinationIndex, length);
                destinationIndex += length;
            }
            return numArray1;
        }

        #endregion


        #region Private Methods

        private HashAlgorithm GetHashAlgorithm(string halg)
        {
            if (halg.ToLower().IndexOf("hmacsha1") != -1)
                return new HMACSHA1();
            if (halg.ToLower().IndexOf("mactripledes") != -1)
                return new MACTripleDES();
            if (halg.ToLower().IndexOf("md5") != -1)
                return new MD5CryptoServiceProvider();
            if (halg.ToLower().IndexOf("sha1") != -1)
                return new SHA1CryptoServiceProvider();
            if (halg.ToLower().IndexOf("sha") != -1)
                return new SHA1CryptoServiceProvider();
            throw new Exception("unknown hash algorithm");
        }

        private static byte[] ASCIIEncoder(string s)
        {
            byte[] numArray = new byte[s.Length];
            for (int index = 0; index < s.Length; ++index)
                numArray[index] = (byte)s[index];
            return numArray;
        }

        #endregion
    }
}

