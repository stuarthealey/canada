﻿namespace MTData.WebAPI.Models
{
    public class CreateRCCIReq
    {
        public GPSLocation GPSLocation { get; set; }

        public ValueNode MGID { get; set; }

        public CustomerDetails CustomerDetails { get; set; }

        public EncryptionMethod Encryption { get; set; }

        public CreditCardDetails FullCreditCardDetails { get; set; }

        public CustomerAVSDetails AVSDetails { get; set; }  //PAY-12
    }

    public class GPSLocation
    {
        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }

        public decimal HorizontalAccuracyMeters { get; set; }
    }

    public class ValueNode
    {
        public string Value { get; set; }
    }

    public partial class CustomerDetails
    {
        public ValueNode CustomerId { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string ContactEmail { get; set; }

        public string ContactPhone { get; set; }

        public string ZipCode { get; set; } //changes for card registration      //zzz Need to try and exclude this from the payload.
    }

    public class EncryptionMethod
    {
        public int Method { get; set; }

        public int KeyVersion { get; set; }

        public string Token { get; set; }
    }

    public class CreditCardDetails
    {
        public string PAN { get; set; }

        public string ExpiryMonth { get; set; }

        public string ExpiryYear { get; set; }

        public string NameOnCard { get; set; }

        public string CVV2 { get; set; }
    }

    /// <summary>
    /// PAY-12 Include customer address details from RCCI request.
    /// </summary>
    public class CustomerAVSDetails
    {
        public string Unit { get; set; }
        public string StreetNumber { get; set; }
        public string Street { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
    }
}
