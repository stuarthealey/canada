﻿using System.Collections.Generic;
using MTD.Data.Data;

namespace MTD.Data.Repository.Interface
{
    public interface ISurchargeRepository
    {
        bool Add(Surcharge surcharge);
        IEnumerable<Surcharge> SurchargeList();
        bool Update(Surcharge surcharge, int surchargeId);
        Surcharge GetSurcharge(int surchargeId);
        void DeleteSurcharge(int surchargeId);
        bool FindSurcharge(int gatewayId);
        IEnumerable<Surcharge> SurchargeListByFiter(FilterSearchParameters searchParameters);
        IEnumerable<SurchargeList> SurchargeListByMerchantId(int merchantId);
        int SurchargeListCount(FilterSearchParameters fsParametes);
    }
}
