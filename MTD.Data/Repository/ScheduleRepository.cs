﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Configuration;
using System.Text;
using System.Reflection;
using System.Globalization;

using MTD.Data.Data;
using MTD.Data.Repository.Interface;
using MTData.Utility;

namespace MTD.Data.Repository
{
    public class ScheduleRepository : BaseRepository, IScheduleRepository
    {
        private readonly ILogger _logger = new Logger();
        private StringBuilder _logMessage = new StringBuilder();
        int summaryScheduleId;
        int ExceptionScheduleId;

        public ScheduleRepository(IMtDataDevlopmentsEntities dbMtData) : base(dbMtData)
        {
        }

        /// <summary>
        /// Purpose             :   To get the special offers list
        /// Function Name       :   Add
        /// Created By          :   Navin Kumar
        /// Created On          :   02/19/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="scheduleReport"></param>
        public void Add(ScheduleReport scheduleReport)
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    scheduleReport.CreatedDate = DateTime.Now;
                    scheduleReport.IsCreated = true;

                    DbMtData.ScheduleReports.Add(scheduleReport);
                    DbMtData.CommitChanges();
                    
                    Report report = DbMtData.Reports.FirstOrDefault(x => x.ReportId == scheduleReport.fk_ReportID);
                    if (scheduleReport.fk_MerchantId == null)
                        report.IsCreated = true;
                    
                    DbMtData.CommitChanges();
                    DatabaseScheduler database = DataScheduler(scheduleReport);
                    summaryScheduleId = Convert.ToInt32(ConfigurationManager.AppSettings["SummaryReportScheduleId"]);
                    ExceptionScheduleId = Convert.ToInt32(ConfigurationManager.AppSettings["ExceptionReportScheduleId"]);
                    
                    int totalTxnByVehicleScheduleID = Convert.ToInt32(ConfigurationManager.AppSettings["TotalTxnByVehicleScheduleID"]);
                    int cardTypeTxn = Convert.ToInt32(ConfigurationManager.AppSettings["CardTypeScheduleID"]);

                    try
                    {
                        switch (scheduleReport.fk_ReportID)
                        {
                            case 1:
                                DbMtData.usp_Schedule(summaryScheduleId, null, null, 1, database.Freq_Type, database.Freq_Interval, database.Freq_Subday_Type, database.Freq_Subday_Interval, database.Freq_Relative_Interval, database.Freq_Recurrence_Factor, database.Active_Start_Date, database.Active_End_Date, database.Active_Start_Time, database.Active_End_Time, null, null);
                                break;
                            case 3:
                                DbMtData.usp_Schedule(ExceptionScheduleId, null, null, 1, database.Freq_Type, database.Freq_Interval, database.Freq_Subday_Type, database.Freq_Subday_Interval, database.Freq_Relative_Interval, database.Freq_Recurrence_Factor, database.Active_Start_Date, database.Active_End_Date, database.Active_Start_Time, database.Active_End_Time, null, null);
                                break;
                            case 7:
                                DbMtData.usp_Schedule(totalTxnByVehicleScheduleID, null, null, 1, database.Freq_Type, database.Freq_Interval, database.Freq_Subday_Type, database.Freq_Subday_Interval, database.Freq_Relative_Interval, database.Freq_Recurrence_Factor, database.Active_Start_Date, database.Active_End_Date, database.Active_Start_Time, database.Active_End_Time, null, null);
                                break;
                            case 8:
                                DbMtData.usp_Schedule(cardTypeTxn, null, null, 1, database.Freq_Type, database.Freq_Interval, database.Freq_Subday_Type, database.Freq_Subday_Interval, database.Freq_Relative_Interval, database.Freq_Recurrence_Factor, database.Active_Start_Date, database.Active_End_Date, database.Active_Start_Time, database.Active_End_Time, null, null);
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogInfoFatal(_logMessage.ToString(), ex);
                    }

                    DbMtData.CommitChanges();
                    transactionScope.Complete();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Purpose             :   To get schedule report
        /// Function Name       :   GetScheduleReport
        /// Created By          :   Navin Kumar
        /// Created On          :   02/19/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public ScheduleReport GetScheduleReport(int scheduleId)
        {
            ScheduleReport scheduleDb = DbMtData.ScheduleReports.FirstOrDefault(x => x.ScheduleID == scheduleId);
            return scheduleDb;
        }

        /// <summary>
        /// Purpose             :   To update schedule report
        /// Function Name       :   Update
        /// Created By          :   Navin Kumar
        /// Created On          :   02/19/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="scheduleReport"></param>
        public void Update(ScheduleReport scheduleReport)
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    ScheduleReport scheduleDb = DbMtData.ScheduleReports.FirstOrDefault(x => x.ScheduleID == scheduleReport.ScheduleID);
                    scheduleDb.DateFrom = scheduleReport.DateFrom;
                    scheduleDb.DateTo = scheduleReport.DateTo;
                    scheduleDb.Frequency = scheduleReport.Frequency;
                    scheduleDb.FrequencyType = scheduleReport.FrequencyType;
                    scheduleDb.StartTime = scheduleReport.StartTime;
                    scheduleDb.TimeZone = scheduleReport.TimeZone;
                    scheduleDb.ModifiedDate = DateTime.Now;
                    scheduleDb.ModifiedBy = scheduleReport.ModifiedBy;
                    scheduleDb.WeekDay = scheduleReport.WeekDay;
                    scheduleDb.MonthDay = scheduleReport.MonthDay;
                    scheduleDb.DisplayStartTime = scheduleReport.DisplayStartTime;
                    scheduleDb.NextScheduleDateTime = scheduleReport.NextScheduleDateTime;
                    
                    DbMtData.CommitChanges();
                    DatabaseScheduler database = DataScheduler(scheduleReport);
                    summaryScheduleId = Convert.ToInt32(ConfigurationManager.AppSettings["SummaryReportScheduleId"]);
                    ExceptionScheduleId = Convert.ToInt32(ConfigurationManager.AppSettings["ExceptionReportScheduleId"]);
                    
                    int totalTxnByVehicleScheduleID = Convert.ToInt32(ConfigurationManager.AppSettings["TotalTxnByVehicleScheduleID"]);
                    int cardTypeTxn = Convert.ToInt32(ConfigurationManager.AppSettings["CardTypeScheduleID"]);

                    try
                    {
                        switch (scheduleReport.fk_ReportID)
                        {
                            case 1:
                                DbMtData.usp_Schedule(summaryScheduleId, null, null, 1, database.Freq_Type, database.Freq_Interval, database.Freq_Subday_Type, database.Freq_Subday_Interval, database.Freq_Relative_Interval, database.Freq_Recurrence_Factor, database.Active_Start_Date, database.Active_End_Date, database.Active_Start_Time, database.Active_End_Time, null, null);
                                break;
                            case 3:
                                DbMtData.usp_Schedule(ExceptionScheduleId, null, null, 1, database.Freq_Type, database.Freq_Interval, database.Freq_Subday_Type, database.Freq_Subday_Interval, database.Freq_Relative_Interval, database.Freq_Recurrence_Factor, database.Active_Start_Date, database.Active_End_Date, database.Active_Start_Time, database.Active_End_Time, null, null);
                                break;
                            case 7:
                                DbMtData.usp_Schedule(totalTxnByVehicleScheduleID, null, null, 1, database.Freq_Type, database.Freq_Interval, database.Freq_Subday_Type, database.Freq_Subday_Interval, database.Freq_Relative_Interval, database.Freq_Recurrence_Factor, database.Active_Start_Date, database.Active_End_Date, database.Active_Start_Time, database.Active_End_Time, null, null);
                                break;
                            case 8:
                                DbMtData.usp_Schedule(cardTypeTxn, null, null, 1, database.Freq_Type, database.Freq_Interval, database.Freq_Subday_Type, database.Freq_Subday_Interval, database.Freq_Relative_Interval, database.Freq_Recurrence_Factor, database.Active_Start_Date, database.Active_End_Date, database.Active_Start_Time, database.Active_End_Time, null, null);
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogInfoFatal(_logMessage.ToString(), ex);
                    }

                    DbMtData.CommitChanges();
                    transactionScope.Complete();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Purpose             :   To update receipents
        /// Function Name       :   UpdateRecipient
        /// Created By          :   Navin Kumar
        /// Created On          :   02/19/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="recipient"></param>
        /// <returns></returns>
        public bool UpdateRecipient(RecipientMaster recipient)
        {
            Recipient[] recipientObj = new Recipient[recipient.MerchantList.Count()];
            try
            {
                int count = 0;
                foreach (var item in recipient.MerchantList)
                {
                    recipientObj[count] = new Recipient();
                    recipientObj[count] = (from m in DbMtData.Recipients where m.fk_MerchantID == item.fk_MerchantID && m.fk_ReportID == item.fk_ReportID && m.fk_UserTypeID == 2 select m).FirstOrDefault();
                    recipientObj[count].IsRecipient = item.IsRecipient;
                    recipientObj[count].ModifiedBy = item.ModifiedBy;
                    recipientObj[count].ModifiedDate = DateTime.Now;
                    DbMtData.CommitChanges();
                    count++;
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   Data schedular
        /// Function Name       :   DataScheduler
        /// Created By          :   Navin Kumar
        /// Created On          :   02/19/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns></returns>
        public DatabaseScheduler DataScheduler(ScheduleReport schedule)
        {
            DatabaseScheduler database = new DatabaseScheduler();
            if (schedule.Frequency == 1)
            {
                database.Freq_Type = 4;
                database.Freq_Interval = Convert.ToInt32(schedule.FrequencyType);
                string strTime = SetTimeFormat((TimeSpan)(schedule.StartTime));
                database.Active_Start_Time = Convert.ToInt32(strTime);
                database.Freq_Subday_Type = 1;
                database.Freq_Subday_Interval = 24;
                database.Freq_Relative_Interval = 0;
                database.Freq_Recurrence_Factor = 0;
                string strFromDate = SetDateFromFormat(Convert.ToDateTime(schedule.DateFrom));
                string strToDate = SetDateToFormat(Convert.ToDateTime(schedule.DateTo));
                database.Active_Start_Date = Convert.ToInt32(strFromDate);
                database.Active_End_Date = Convert.ToInt32(strToDate);
                return database;
            }
            else if (schedule.Frequency == 2)
            {
                database.Freq_Type = 8;
                database.Freq_Interval = Convert.ToInt32(schedule.WeekDay);
                string strTime = SetTimeFormat((TimeSpan)schedule.StartTime);
                database.Active_Start_Time = Convert.ToInt32(strTime);
                database.Freq_Subday_Type = 1;
                database.Freq_Subday_Interval = 24;
                database.Freq_Relative_Interval = 0;
                database.Freq_Recurrence_Factor = Convert.ToInt32(schedule.FrequencyType);
                string strFromDate = SetDateFromFormat(Convert.ToDateTime(schedule.DateFrom));
                string strToDate = SetDateToFormat(Convert.ToDateTime(schedule.DateTo));
                database.Active_Start_Date = Convert.ToInt32(strFromDate);
                database.Active_End_Date = Convert.ToInt32(strToDate);
                return database;
            }
            else if (schedule.Frequency == 3)
            {
                database.Freq_Type = 16;
                database.Freq_Interval = Convert.ToInt32(schedule.MonthDay);
                string strTime = SetTimeFormat((TimeSpan)schedule.StartTime);
                database.Active_Start_Time = Convert.ToInt32(strTime);
                database.Freq_Subday_Type = 1;
                database.Freq_Subday_Interval = 24;
                database.Freq_Relative_Interval = 0;
                database.Freq_Recurrence_Factor = Convert.ToInt32(schedule.FrequencyType);
                string strFromDate = SetDateFromFormat(Convert.ToDateTime(schedule.DateFrom));
                string strToDate = SetDateToFormat(Convert.ToDateTime(schedule.DateTo));
                database.Active_Start_Date = Convert.ToInt32(strFromDate);
                database.Active_End_Date = Convert.ToInt32(strToDate);
                return database;
            }

            return database;
        }

        /// <summary>
        /// Purpose             :   To set date time format
        /// Function Name       :   SetTimeFormat
        /// Created By          :   Naveen Kumar
        /// Created On          :   02/19/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="startTime"></param>
        /// <returns></returns>
        public static string SetTimeFormat(TimeSpan startTime)
        {
            string strFirst = startTime.ToString();
            string[] strArray = strFirst.Split(':');
            string strFormat = strArray[0] + strArray[1] + strArray[2];
            return strFormat;
        }

        /// <summary>
        /// Purpose             :   To set date time fromformat
        /// Function Name       :   SetDateFromFormat
        /// Created By          :   Naveen Kumar
        /// Created On          :   02/19/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string SetDateFromFormat(DateTime date)
        {
            string strFirst = String.Format("{0:yyyy-MM-dd}", date);
            string[] strArray = strFirst.Split('-');
            string strDateFormat = strArray[0] + strArray[1] + strArray[2];
            return strDateFormat;
        }

        /// <summary>
        /// Purpose             :   To set date time Toformat
        /// Function Name       :   SetDateToFormat
        /// Created By          :   Naveen Kumar
        /// Created On          :   02/19/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string SetDateToFormat(DateTime date)
        {
            string strFirst = String.Format("{0:yyyy-MM-dd}", date);
            string[] strArray = strFirst.Split('-');
            string strDateFormat = strArray[0] + strArray[1] + strArray[2];
            return strDateFormat;
        }

        /// <summary>
        /// Purpose             :   To update AdminReceipents
        /// Function Name       :   UpdateAdminRecipient
        /// Created By          :   Ravit Chaudhary
        /// Created On          :   06/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="adminRecipientList"></param>
        /// <returns></returns>
        public bool UpdateAdminRecipient(AdminRecipientList adminRecipientList)
        {
            Recipient[] recipientObj = new Recipient[adminRecipientList.AdminList.Count()];
            try
            {
                int count = 0;
                foreach (var item in adminRecipientList.AdminList)
                {
                    recipientObj[count] = new Recipient();
                    recipientObj[count] = (from m in DbMtData.Recipients
                                           where m.fk_UserID == item.UserID && m.fk_ReportID == item.fk_ReportID
                                               && m.fk_UserTypeID == 1
                                           select m).FirstOrDefault();
                    recipientObj[count].IsRecipient = (bool)item.IsRecipient;
                    recipientObj[count].ModifiedBy = item.ModifiedBy;
                    recipientObj[count].ModifiedDate = DateTime.Now;
                    DbMtData.CommitChanges();
                    count++;
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ScheduleReport> ScheduleReportList()
        {
            try
            {
                IEnumerable<ScheduleReport> query = DbMtData.ScheduleReports;
                return query;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <param name="sorting"></param>
        /// <returns></returns>
        public IEnumerable<ScheduleReport> ScheduleReportListByFilter(string name, int startIndex, int count, string sorting)
        {
            try
            {
                IEnumerable<ScheduleReport> query = (from m in DbMtData.ScheduleReports
                                                     join n in DbMtData.Reports on m.fk_ReportID
                                                         equals n.ReportId
                                                     where (bool)m.IsCreated && m.fk_MerchantId == null && m.CorporateUserId == null
                                                     select new
                                                     {
                                                         m.DateFrom,
                                                         m.DateTo,
                                                         m.Frequency,
                                                         m.FrequencyType,
                                                         m.WeekDay,
                                                         m.MonthDay,
                                                         m.StartTime,
                                                         m.TimeZone,
                                                         m.ScheduleID,
                                                         m.IsCreated,
                                                         m.fk_ReportID,
                                                         n.DisplayName,
                                                         m.DisplayStartTime
                                                     }).AsEnumerable().Select(objNew => new
                                                    ScheduleReport
                                                     {
                                                         DateFrom = objNew.DateFrom,
                                                         DateTo = objNew.DateTo,
                                                         Frequency = objNew.Frequency,
                                                         FrequencyType = objNew.FrequencyType,
                                                         WeekDay = objNew.WeekDay,
                                                         MonthDay = objNew.MonthDay,
                                                         StartTime = objNew.StartTime,
                                                         TimeZone = objNew.TimeZone,
                                                         ScheduleID = objNew.ScheduleID,
                                                         IsCreated = objNew.IsCreated,
                                                         fk_ReportID = objNew.fk_ReportID,
                                                         DisplayName = objNew.DisplayName,
                                                         DisplayStartTime = objNew.DisplayStartTime
                                                     }).ToList();

                if (!string.IsNullOrEmpty(name))
                {
                    query = query.Where(p => p.DisplayName.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0);
                }

                if (!string.IsNullOrEmpty(sorting))
                {
                    query = SortedScheduleReport(sorting, query);
                }

                return query;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Report> ReportList()
        {
            try
            {
                IEnumerable<Report> query = DbMtData.Reports.Where(x => !(bool)x.IsCreated && x.ReportId < 4);
                query = query.Concat(DbMtData.Reports.Where(x => !(bool)x.IsCreated && x.ReportId > 6 && x.ReportId < 9));
                return query;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Report> ReportList(int merchantId)
        {
            try
            {
                IEnumerable<ScheduleReport> temp = DbMtData.ScheduleReports.Where(x => x.fk_MerchantId == merchantId && (bool)x.IsCreated).ToList();
                var query = DbMtData.Reports.Where(x => x.ReportId < 4).ToList();
                query = query.Concat(DbMtData.Reports.Where(x => x.ReportId > 6 && x.ReportId < 9)).ToList();
                query = query.Where(x => !temp.Select(y => y.fk_ReportID).Contains(x.ReportId)).ToList();
                return query;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Report> ReportListReadOnly()
        {
            try
            {
                IEnumerable<Report> query = DbMtData.Reports;
                return query;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<ScheduleReport> SortedScheduleReport(string sorting, IEnumerable<ScheduleReport> query)
        {
            switch (sorting)
            {
                case "DisplayName ASC":
                    query = query.OrderBy(p => p.DisplayName);
                    break;
                case "DisplayName DESC":
                    query = query.OrderByDescending(p => p.DisplayName);
                    break;
                case "DateFrom ASC":
                    query = query.OrderBy(p => p.DateFrom);
                    break;
                case "DateFrom DESC":
                    query = query.OrderByDescending(p => p.DateFrom);
                    break;
                case "DateTo ASC":
                    query = query.OrderBy(p => p.DateTo);
                    break;
                case "DateTo DESC":
                    query = query.OrderByDescending(p => p.DateTo);
                    break;
                case "FrequencyName ASC":
                    query = query.OrderBy(p => p.Frequency);
                    break;
                case "FrequencyName DESC":
                    query = query.OrderByDescending(p => p.Frequency);
                    break;
            }

            return query;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ScheduleID"></param>
        public void Delete(int ScheduleID)
        {
            try
            {
                var scheduledb = DbMtData.ScheduleReports.Find(ScheduleID);
                scheduledb.IsCreated = false;
                var reportdb = DbMtData.Reports.Find(scheduledb.fk_ReportID);
                if (scheduledb.fk_MerchantId == null)
                    reportdb.IsCreated = false;
                DbMtData.CommitChanges();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public IEnumerable<ScheduleReport> ScheduleReportListByFilter(FilterSearchParameters filterSearchParameters, string sorting)
        {
            try
            {
                IEnumerable<ScheduleReport> query;
                if (filterSearchParameters.CorporateId == null)
                {
                    query = (from m in DbMtData.ScheduleReports
                             join n in DbMtData.Reports on m.fk_ReportID
                                 equals n.ReportId
                             where (bool)m.IsCreated && m.fk_MerchantId == filterSearchParameters.MerchantId
                             select new
                             {
                                 m.DateFrom,
                                 m.DateTo,
                                 m.Frequency,
                                 m.FrequencyType,
                                 m.WeekDay,
                                 m.MonthDay,
                                 m.StartTime,
                                 m.TimeZone,
                                 m.ScheduleID,
                                 m.IsCreated,
                                 m.fk_ReportID,
                                 n.DisplayName,
                                 m.DisplayStartTime
                             }).AsEnumerable().Select(objNew => new
                                                   ScheduleReport
                                                         {
                                                             DateFrom = objNew.DateFrom,
                                                             DateTo = objNew.DateTo,
                                                             Frequency = objNew.Frequency,
                                                             FrequencyType = objNew.FrequencyType,
                                                             WeekDay = objNew.WeekDay,
                                                             MonthDay = objNew.MonthDay,
                                                             StartTime = objNew.StartTime,
                                                             TimeZone = objNew.TimeZone,
                                                             ScheduleID = objNew.ScheduleID,
                                                             IsCreated = objNew.IsCreated,
                                                             fk_ReportID = objNew.fk_ReportID,
                                                             DisplayName = objNew.DisplayName,
                                                             DisplayStartTime = objNew.DisplayStartTime
                                                         }).ToList();
                }
                else
                {
                    query = (from m in DbMtData.ScheduleReports
                             join n in DbMtData.Reports on m.fk_ReportID
                                 equals n.ReportId
                             where (bool)m.IsCreated && m.CorporateUserId == filterSearchParameters.CorporateId
                             select new
                             {
                                 m.DateFrom,
                                 m.DateTo,
                                 m.Frequency,
                                 m.FrequencyType,
                                 m.WeekDay,
                                 m.MonthDay,
                                 m.StartTime,
                                 m.TimeZone,
                                 m.ScheduleID,
                                 m.IsCreated,
                                 m.fk_ReportID,
                                 n.DisplayName,
                                 m.DisplayStartTime
                             }).AsEnumerable().Select(objNew => new
                                                    ScheduleReport
                                                          {
                                                              DateFrom = objNew.DateFrom,
                                                              DateTo = objNew.DateTo,
                                                              Frequency = objNew.Frequency,
                                                              FrequencyType = objNew.FrequencyType,
                                                              WeekDay = objNew.WeekDay,
                                                              MonthDay = objNew.MonthDay,
                                                              StartTime = objNew.StartTime,
                                                              TimeZone = objNew.TimeZone,
                                                              ScheduleID = objNew.ScheduleID,
                                                              IsCreated = objNew.IsCreated,
                                                              fk_ReportID = objNew.fk_ReportID,
                                                              DisplayName = objNew.DisplayName,
                                                              DisplayStartTime = objNew.DisplayStartTime
                                                          }).ToList();
                }

                if (!string.IsNullOrEmpty(filterSearchParameters.Name))
                {
                    query = query.Where(p => p.DisplayName.IndexOf(filterSearchParameters.Name, StringComparison.OrdinalIgnoreCase) >= 0);
                }

                if (!string.IsNullOrEmpty(sorting))
                {
                    query = SortedScheduleReport(sorting, query);
                }

                return query;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void UpdateCorporateUserRecipient(RecipientMaster recipient, int corporateUserId, string modifiedBy)
        {
            try
            {
                bool isParentCorporateExist = DbMtData.Recipients.Any(m => m.fk_UserID == corporateUserId && m.fk_ReportID == recipient.ReportId && m.fk_UserTypeID == 5);
                if (isParentCorporateExist)
                {
                    var parentCorporateUser = (from m in DbMtData.Recipients
                                               where m.fk_UserID == corporateUserId && m.fk_ReportID == recipient.ReportId
                                               && m.fk_UserTypeID == 5
                                               select m).FirstOrDefault();
                    parentCorporateUser.IsRecipient = true;
                    parentCorporateUser.ModifiedBy = modifiedBy;
                    parentCorporateUser.ModifiedDate = DateTime.Now;
                }
                else
                {
                    Recipient newCorporateRecipient = new Recipient();
                    newCorporateRecipient.fk_UserTypeID = 5;
                    newCorporateRecipient.fk_ReportID = recipient.ReportId;
                    newCorporateRecipient.ModifiedBy = modifiedBy;
                    newCorporateRecipient.ModifiedDate = DateTime.Now;
                    newCorporateRecipient.IsRecipient = true;
                    newCorporateRecipient.fk_UserID = corporateUserId;
                    DbMtData.Recipients.Add(newCorporateRecipient);
                }

                DbMtData.CommitChanges();
                Recipient[] recipientObj = new Recipient[recipient.MerchantList.Count()];
                
                int count = 0;
                foreach (var item in recipient.MerchantList)
                {
                    recipientObj[count] = new Recipient();
                    bool isExist = DbMtData.Recipients.Any(m => m.fk_UserID == item.fk_UserID && m.fk_ReportID == item.fk_ReportID && m.fk_UserTypeID == 5);

                    if (isExist)
                    {
                        recipientObj[count] = (from m in DbMtData.Recipients
                                               where m.fk_UserID == item.fk_UserID && m.fk_ReportID == item.fk_ReportID
                                                   && m.fk_UserTypeID == 5
                                               select m).FirstOrDefault();
                        recipientObj[count].IsRecipient = (bool)item.IsRecipient;
                        recipientObj[count].ModifiedBy = item.ModifiedBy;
                        recipientObj[count].ModifiedDate = DateTime.Now;
                    }
                    else
                    {
                        recipientObj[count].fk_UserTypeID = 5;
                        recipientObj[count].fk_ReportID = item.fk_ReportID;
                        recipientObj[count].ModifiedBy = item.ModifiedBy;
                        recipientObj[count].ModifiedDate = DateTime.Now;
                        recipientObj[count].IsRecipient = (bool)item.IsRecipient;
                        recipientObj[count].fk_UserID = item.fk_UserID;
                        DbMtData.Recipients.Add(recipientObj[count]);
                    }
                    DbMtData.CommitChanges();
                    count++;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void AddCorporateUser(ScheduleReport scheduleReport)
        {
            try
            {
                scheduleReport.fk_MerchantId = null;
                scheduleReport.CreatedDate = DateTime.Now;
                scheduleReport.IsCreated = true;
                DbMtData.ScheduleReports.Add(scheduleReport);
                DbMtData.CommitChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void UpdateCorporate(ScheduleReport scheduleReport)
        {
            try
            {
                ScheduleReport scheduleDb = DbMtData.ScheduleReports.FirstOrDefault(x => x.ScheduleID == scheduleReport.ScheduleID);
                scheduleDb.fk_MerchantId = null;
                scheduleDb.DateFrom = scheduleReport.DateFrom;
                scheduleDb.DateTo = scheduleReport.DateTo;
                scheduleDb.Frequency = scheduleReport.Frequency;
                scheduleDb.FrequencyType = scheduleReport.FrequencyType;
                scheduleDb.StartTime = scheduleReport.StartTime;
                scheduleDb.TimeZone = scheduleReport.TimeZone;
                scheduleDb.ModifiedDate = DateTime.Now;
                scheduleDb.ModifiedBy = scheduleReport.ModifiedBy;
                scheduleDb.WeekDay = scheduleReport.WeekDay;
                scheduleDb.MonthDay = scheduleReport.MonthDay;
                scheduleDb.DisplayStartTime = scheduleReport.DisplayStartTime;
                scheduleDb.NextScheduleDateTime = scheduleReport.NextScheduleDateTime;
                scheduleDb.CorporateUserId = scheduleReport.CorporateUserId;
                DbMtData.CommitChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Report> CorporateReportList(int corporateId)
        {
            try
            {
                IEnumerable<ScheduleReport> temp = DbMtData.ScheduleReports.Where(x => x.CorporateUserId == corporateId && (bool)x.IsCreated).ToList();
                var query = DbMtData.Reports.Where(x => x.ReportId <= 3 || x.ReportId == 7 || x.ReportId == 8).ToList();
                query = query.Where(x => !temp.Select(y => y.fk_ReportID).Contains(x.ReportId)).OrderBy(x => x.Name).ToList();
                return query;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteCorporate(int ScheduleID)
        {
            var scheduledb = DbMtData.ScheduleReports.Find(ScheduleID);
            scheduledb.IsCreated = false;
            DbMtData.CommitChanges();
        }

        public IEnumerable<Report> ReportList(int to, int from)
        {
            try
            {
                IEnumerable<Report> query = DbMtData.Reports.Where(x => !(bool)x.IsCreated && x.ReportId > to && x.ReportId <  from);
                return query;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Purpose             :   Data schedular
        /// Function Name       :   WeeklySummary
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   06/27/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>

        public IEnumerable<usp_GetAdminWeeklySummaryCount_Result> WeeklySummary(FilterSearchParameters fs)
        {
            IEnumerable<usp_GetAdminWeeklySummaryCount_Result> query = DbMtData.usp_GetAdminWeeklySummaryCount();
            if(!string.IsNullOrEmpty(fs.Name))
                query = query.Where(p => p.FleetName.IndexOf(fs.Name, StringComparison.OrdinalIgnoreCase) >= 0);
            if (!string.IsNullOrEmpty(fs.Sorting))
            {
                query = WeeklySummarySorted(query, fs.Sorting);
            }
            return query;
        }

        /// <summary>
        /// Purpose             :   Data schedular
        /// Function Name       :   WeeklySummary
        /// Created By          :  Madhuri Tanwar
        /// Created On          :   06/27/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>

        private IEnumerable<usp_GetAdminWeeklySummaryCount_Result> WeeklySummarySorted(IEnumerable<usp_GetAdminWeeklySummaryCount_Result> query, string sorting)
        {
            switch (sorting)
            {
                case "FleetName ASC":
                    query = query.OrderBy(p => p.FleetName);
                    break;
                case "FleetName DESC":
                    query = query.OrderByDescending(p => p.FleetName);
                    break;
                case "TransCountThisWeek ASC":
                    query = query.OrderBy(p => p.TransCountThisWeek);
                    break;
                case "TransCountWeekFirst DESC":
                    query = query.OrderByDescending(p => p.TransCountWeekFirst);
                    break;
                case "TransCountWeekSecond ASC":
                    query = query.OrderBy(p => p.TransCountWeekSecond);
                    break;
                case "TransCountWeekThird DESC":
                    query = query.OrderByDescending(p => p.TransCountWeekThird);
                    break;
                case "TransCountWeekFourth ASC":
                    query = query.OrderBy(p => p.TransCountWeekFourth);
                    break;
                case "TransCountWeekFifth DESC":
                    query = query.OrderByDescending(p => p.TransCountWeekFifth);
                    break;
                case "TransCountWeekSixth DESC":
                    query = query.OrderByDescending(p => p.TransCountWeekFifth);
                    break;
            }
            return query;
        }


        /// <summary>
        /// Purpose             :   Data schedular
        /// Function Name       :   WeeklySummary
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   06/27/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>

        public int WeeklySummaryCount(FilterSearchParameters fs)
        {
            IEnumerable<usp_GetAdminWeeklySummaryCount_Result> query = DbMtData.usp_GetAdminWeeklySummaryCount();
            if (!string.IsNullOrEmpty(fs.Name))
                query = query.Where(p => p.FleetName.IndexOf(fs.Name, StringComparison.OrdinalIgnoreCase) >= 0);
            
            return query.Count();
        }

        /// <summary>
        /// Purpose             :  To get and sort weekly dollar volume report data
        /// Function Name       :   WeeklySummary
        /// Created By          :   Naveen Kumar
        /// Created On          :   07/01/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>

        public IEnumerable<usp_GetAdminWeeklyDollarVolume_Result> WeeklyDollarVolumeReport(FilterSearchParameters fs)
        {
            IEnumerable<usp_GetAdminWeeklyDollarVolume_Result> query = DbMtData.usp_GetAdminWeeklyDollarVolume();
            if (!string.IsNullOrEmpty(fs.Name))
                query = query.Where(p => p.FleetName.IndexOf(fs.Name, StringComparison.OrdinalIgnoreCase) >= 0);
            if (!string.IsNullOrEmpty(fs.Sorting))
            {
                query = WeeklyDollarVolumeSorted(query, fs.Sorting);
            }
            return query;
        }

        /// <summary>
        /// Purpose             :   To sort weekly dollar volume report data
        /// Function Name       :   WeeklyDollarVolumeSorted
        /// Created By          :  Naveen Kumar
        /// Created On          :   07/01/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>

        private IEnumerable<usp_GetAdminWeeklyDollarVolume_Result> WeeklyDollarVolumeSorted(IEnumerable<usp_GetAdminWeeklyDollarVolume_Result> query, string sorting)
        {
            switch (sorting)
            {
                case "FleetName ASC":
                    query = query.OrderBy(p => p.FleetName);
                    break;
                case "FleetName DESC":
                    query = query.OrderByDescending(p => p.FleetName);
                    break;
                case "TransAmtThisWeek ASC":
                    query = query.OrderBy(p => p.TransAmtThisWeek);
                    break;
                case "TransAmtThisWeek DESC":
                    query = query.OrderByDescending(p => p.TransAmtThisWeek);
                    break;
                case "TransAmtWeekOne ASC":
                    query = query.OrderBy(p => p.TransAmtWeekOne);
                    break;
                case "TransAmtWeekOne DESC":
                    query = query.OrderByDescending(p => p.TransAmtWeekOne);
                    break;
                case "TransAmtWeekTwo ASC":
                    query = query.OrderBy(p => p.TransAmtWeekTwo);
                    break;
                case "TransAmtWeekTwo DESC":
                    query = query.OrderByDescending(p => p.TransAmtWeekTwo);
                    break;
                case "TransAmtWeekThree ASC":
                    query = query.OrderBy(p => p.TransAmtWeekThree);
                    break;
                case "TransAmtWeekThree DESC":
                    query = query.OrderByDescending(p => p.TransAmtWeekThree);
                    break;
                case "TransAmtWeekFour ASC":
                    query = query.OrderBy(p => p.TransAmtWeekFour);
                    break;
                case "TransAmtWeekFour DESC":
                    query = query.OrderByDescending(p => p.TransAmtWeekFour);
                    break;
                case "TransAmtWeekFive ASC":
                    query = query.OrderBy(p => p.TransAmtWeekFive);
                    break;
                case "TransAmtWeekFive DESC":
                    query = query.OrderByDescending(p => p.TransAmtWeekFive);
                    break;
                case "TransAmtWeekSix ASC":
                    query = query.OrderBy(p => p.TransAmtWeekSix);
                    break;
                case "TransAmtWeekSix DESC":
                    query = query.OrderByDescending(p => p.TransAmtWeekSix);
                    break;
            }
            return query;
        }


        /// <summary>
        /// Purpose             :   To count the records
        /// Function Name       :   WeeklyDollarVolumeReportCount
        /// Created By          :  Naveen Kumar
        /// Created On          :   07/01/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>

        public int WeeklyDollarVolumeReportCount(FilterSearchParameters fs)
        {
            IEnumerable<usp_GetAdminWeeklyDollarVolume_Result> query = DbMtData.usp_GetAdminWeeklyDollarVolume();
            if (!string.IsNullOrEmpty(fs.Name))
                query = query.Where(p => p.FleetName.IndexOf(fs.Name, StringComparison.OrdinalIgnoreCase) >= 0);

            return query.Count();
        }

    }

}


