IF OBJECT_ID('dbo.TransactionCountSlab', 'U') IS NULL
BEGIN
	CREATE TABLE [dbo].[TransactionCountSlab](
		[SlabID] [int] IDENTITY(1,1) NOT NULL,
		[Fk_MerchantId] [int] NULL,
		[TransCountSlablt1] [int] NULL,
		[TransCountSlabGt2] [int] NULL,
		[TransCountSlablt2] [int] NULL,
		[TransCountSlabGt3] [int] NULL,
		[TransCountSlablt3] [int] NULL,
		[TransCountSlabGt4] [int] NULL,
		[CreatedBy] [nvarchar](100) NOT NULL,
		[CreatedDate] [datetime] NOT NULL,
		[ModifiedBy] [nvarchar](50) NULL,
		[ModifiedDate] [datetime] NULL,
	PRIMARY KEY CLUSTERED 
	(
		[SlabID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	PRINT 'Table TransactionCountSlab created.'
END
ELSE
BEGIN
	PRINT 'Table TransactionCountSlab already exists.'
END

ALTER TABLE [dbo].[TransactionCountSlab] WITH CHECK ADD FOREIGN KEY([Fk_MerchantId])
	REFERENCES [dbo].[Merchant] ([MerchantID])
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TransactionCountSlab' AND COLUMN_NAME = 'fk_CorporateUserId')
	ALTER TABLE dbo.TransactionCountSlab ADD fk_CorporateUserId INT
GO