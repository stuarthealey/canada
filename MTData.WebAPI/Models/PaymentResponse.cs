﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class PaymentResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public LoginHeader Header { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public TransactionDetails TransactionDetails { get; set; }
    }
}