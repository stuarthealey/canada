﻿using MTD.Core.DataTransferObjects;

using System.Collections.Generic;

namespace MTD.Core.Service.Interface
{
    public interface IReportCommonService
    {
        IEnumerable<FleetListDto> GetFleetList();

        IEnumerable<FleetListDto> GetFleetList(int id);

        IEnumerable<MerchantListDto> GetMerchantList();

        IEnumerable<VehicleListDto> GetVehicleList();

        IEnumerable<VehicleListDto> GetVehicleList(int id);

        IEnumerable<VehicleListDto> GetVehicleList(string vehicleNumber, int fleetId);

        IEnumerable<DriverListDto> GetDriverList(string driverNumber, int fleetId);

        IEnumerable<DrivernamelistDto> GetDriverNameList(int fleetId);

        IEnumerable<UserDto> MerchantUserList(int mId);
    }
}
