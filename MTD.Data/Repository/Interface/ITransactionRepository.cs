﻿using System;
using System.Collections.Generic;

using MTD.Data.Data;

namespace MTD.Data.Repository.Interface
{
    public interface ITransactionRepository
    {
        int Create(MTDTransaction transaction);
        int ManageCardToken(CardToken cardToken);
        void Update(MTDTransaction transaction, int transId, string rapidConnectAuthId);
        MTDTransaction GetTransaction(int transId);
        MTDTransaction GetTransaction(int userType, int uId, int mId, int transId, string project);
        MTDTransaction GetTransaction(int transId, int? mid);
        IEnumerable<usp_SearchTransactionReport_Result> GetTransaction(MTDTransaction transaction);
        IEnumerable<usp_GetDriversTransaction_Result> GetDriver(MTDTransaction transaction);
        IEnumerable<usp_TechnologySummaryReport_Result> GetTechnologySummary(MTDTransaction tvd);
        IEnumerable<usp_TechnologyDetailReport_Result> GetTechnologyDetail(MTDTransaction tvd);
        int GetDriverCount(MTDTransaction transaction);
        int GetTransactionCount(MTDTransaction transaction);
        int GetTechFeeCount(MTDTransaction tvd);
        int GetTechFeeDetailCount(MTDTransaction tvd);
        bool DriverVhecileExist(string driverNo, string vehicleNo, int merchantId);
        int GetMerchantId(string driverNo, string vehicleNo);
        int Add(MTDTransaction transaction);
        void Update(MTDTransaction transaction);
        string GetDriverDevice(string vehicleNumber, int fleetId);
        TxnTaxSurcharges GetTaxSurchages(int merchantId);
        TxnTaxSurcharges GetTaxSurchages(int merchantId, int vehicleNumber);
        TxnTaxSurcharges GetTaxSurchages();
        IEnumerable<usp_GetDrivers_Result> GetDrivers(int merchantId);
        IEnumerable<usp_GetVehicles_Result> GetVehicle(int merchantId);
		//POD - need customerid for token..
        //string GetCardTypeByToken(string cardToken);
        int GetPayrollSageCount(MTDTransaction transaction);
        IEnumerable<usp_GetPayrollSAGETransaction_Result> GetpayrollSage(MTDTransaction transaction);
        int GetPayrollPaymentCount(MTDTransaction transaction);
        IEnumerable<usp_GetPayrollPaymentTransaction_Result> GetpayrollPayment(MTDTransaction transaction);
        bool Varified(int txnId, bool isVerified);
        bool VarifiedMany(ManyTxnClass[] ManyTxnVerified);
        Fleet GetFleetTechFee(int fleetId);
        IEnumerable<usp_GetTransactionData_Result> DownloadTransactionData(DownloadParameters dParam);
        int ValidateDownloadRequest(string uId, string pCode, int mCode);
        bool JobNumberRequired(int merchantId);
        CardToken GetCardToken(string customerId, string cardToken);
        //CardToken GetCardToken(string cardToken);
        int SaveCapKey(CapKey capK);
        CapKey GetCapKey();
        int GetMerchantId(int fleetId);
        void CnUpdate(MTDTransaction mtdTransaction, int transId);
        string GetTransactionUrl();

        //On Demand Report
        IEnumerable<usp_GetAdminSummaryReport_Result> GetadminSummary(DateTime? dateFrom, DateTime? dateTo);
        IEnumerable<usp_GetCardTypeReport_Result> AdminCardypeReport(DateTime? dateFrom, DateTime? dateTo);
        IEnumerable<usp_GetAdminExceptionReport_Result> AdminExceptionReport(DateTime? dateFrom, DateTime? dateTo);
        IEnumerable<usp_GetTotalTransByVehicleReport_Result> TransByVehicleReport(DateTime? dateFrom, DateTime? dateTo);

        IEnumerable<usp_GetMerchantSummaryReport_Result> MerchantSummaryReport(DateTime? dateFrom, DateTime? dateTo, int merchantId);
        IEnumerable<usp_GetDetailReport_Result> GetDetailsReport(DateTime? dateFrom, DateTime? dateTo, int merchantId);
        IEnumerable<usp_GetMerchantExceptionReport_Result> GetMerchantExceptionReport(DateTime? dateFrom, DateTime? dateTo, int merchantId);
        IEnumerable<usp_GetMerchantTotalTransByVehicleReport_Result> GetMerchantTotalTransByVehicle(DateTime? dateFrom, DateTime? dateTo, int merchantId);
        IEnumerable<usp_GetMerchantCardTypeReport_Result> GetMerchantCardTypeReport(DateTime? dateFrom, DateTime? dateTo, int merchantId);
        IEnumerable<usp_GetMerchantUserExceptionReport_Result> GetMerchantUserExceptionReport(DateTime? dateFrom, DateTime? dateTo, int? merchantId);
        //On Demand Merchat Users
        IEnumerable<usp_GetMerchantUserCardTypeReport_Result> GetMerchantUserCardTypeReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantId, Nullable<int> merchantUserId);
        IEnumerable<usp_GetMerchantUserDetailReport_Result> GetMerchantUserDetailReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantId, Nullable<int> merchantUserId);
        IEnumerable<usp_GetMerchantUserSummaryReport_Result> GetMerchantUserSummaryReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantId, Nullable<int> merchantUserId);
        IEnumerable<usp_GetMerchantUserTotalTransByVehicleReport_Result> GetMerchantUserTotalTransByVehicleReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> merchantId, Nullable<int> merchantUserId);

        //On Demand Corporate Users
        IEnumerable<usp_CorpUserGetTotalTransByVehicleReport_Result> CorpUserGetTotalTransByVehicleReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> corpUserUserId);
        IEnumerable<usp_GetCorpUserCardTypeReport_Result> GetCorpUserCardTypeReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> corpUserUserId);
        IEnumerable<usp_GetCorpUserExceptionReport_Result> GetCorporateExceptionReport(DateTime? dateFrom, DateTime? dateTo, int cUserId);

        //On Demand Run Admin
        int AdminExceptionReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate);
        int TotalTransByVehicleScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate);
        int CardTypeReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate);
        int AdminSummaryReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate);


        int MerchantSummaryReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, int? merchantId);
        int MerchantTotalTransByVehicleScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, int? merchantId);
        int MerchantExceptionReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate);
        int DetailReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, int? merchantId);
        int MerchantCardTypeReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, int? merchantId);

        //On Demand Run Merchant User
        int MerchantUserCardTypeReportScheduler(bool? isOnDemandReport, DateTime? fromDate, DateTime? toDate, int? parentMerchantId, int? merchantUserId);
        int MerchantUserDetailReportScheduler(bool? isOnDemandReport, DateTime? fromDate, DateTime? toDate, int? parentMerchantId, int? merchantUserId);
        int MerchantUserTotalTransByVehicleScheduler(bool? isOnDemandReport, DateTime? fromDate, DateTime? toDate, int? parentMerchantId, int? merchantUserId);
        int MerchantUserSummaryReportScheduler(bool? isOnDemandReport, DateTime? fromDate, DateTime? toDate, int? parentMerchantId, int? merchantUserId);
        int MerchantUserExceptionReportScheduler(bool? isOnDemandReport, DateTime? fromDate, DateTime? toDate, int? merchantUserId);

        //Corporate Run Report
        int CorpUserTotalTransByVehicleScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> corpUserId);
        int CorpUserCardTypeReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> corpUserId);
        int CorpUserExceptionReportScheduler(Nullable<bool> isOnDemandReport, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> corpUserId);
        string GetRefNumber();
        string GetFastestUrl(string failOverUrls, List<string> urlList);
    
        MTDTransaction GetTransaction(int? mid, string tid, string stan);

        string GetTransactionId(string requestId);
        bool IsMacVerificationFailed(string transId);
    }
}