﻿namespace MTD.Core.DataTransferObjects
{
    public class GatewayDto
    {
        public int GatewayId { get; set; }
        public string Name { get; set; }
        public bool? IsActive { get; set; }
        public string GatewayUrl { get; set; }
        public string GatewayOffset { get; set; }
        public bool? IsSaleSupported { get; set; }
        public bool? IsPreauthSupported { get; set; }
        public bool? IsCaptureSupported { get; set; }
        public bool? IsCancelSupported { get; set; }
        public bool? IsRefundSupported { get; set; }
        public bool IsDefault { get; set; }
        public string UserName { get; set; }
        public string PCode { get; set; }
        public string GatewayCurrency { get; set; }
    }
}