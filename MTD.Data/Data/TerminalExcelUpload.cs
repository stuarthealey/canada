﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Data.Data
{
    public class TerminalExcelUpload
    {
        public int TerminalID { get; set; }
        public string SerialNo { get; set; }
        public string DeviceName { get; set; }
        public string MacAdd { get; set; }
        public string SoftVersion { get; set; }
        public string Description { get; set; }
        public int fk_MerchantID { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Nullable<bool> IsTerminalAssigned { get; set; }
        public string Stan { get; set; }
        public string RefNumber { get; set; }
        public DateTime? TransactionDate { get; set; }
        public Nullable<int> DatawireId { get; set; }
        public string TermId { get; set; }
    }
}
