﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Data.Data
{
  public class DriverRecipient
    {
        public int DriverRcptId { get; set; }
        public Nullable<int> fk_FleetId { get; set; }
        public Nullable<int> fk_DriverId { get; set; }
        public Nullable<int> UserType { get; set; }
        public Nullable<bool> IsRecipient { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public int DriverID { get; set; }
        public string DriverNo { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public Nullable<bool> IsDaily { get; set; }
        public Nullable<bool> IsWeekly { get; set; }
        public Nullable<bool> IsMonthly { get; set; }
        public Nullable<bool> IsOwnerRecipient { get; set; }
       
    }
}
