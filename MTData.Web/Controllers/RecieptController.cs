﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using Ninject;

using MTData.Web.Resource;
using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Web.ViewData;
using MTData.Utility;

namespace MTData.Web.Controllers
{
    public class RecieptController : Controller
    {
        readonly ILogger _logger = new Logger();
        private readonly IReceiptService _receiptService;
        private StringBuilder _logMessage;
        private readonly IFleetService _fleetService;
        private StringBuilder _logTracking;

        public RecieptController([Named("FleetService")] IFleetService fleetService, [Named("ReceiptService")] IReceiptService receiptService, [Named("UserService")] IUserService userService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _receiptService = receiptService;
            _logMessage = logMessage;
            _fleetService = fleetService;
            _logTracking = logTracking;
        }

        /// <summary>
        ///  Purpose            : to display the reciept
        /// Function Name       :   Index
        /// Created By          :  Salil Gupta 
        /// Created On          :  1/28/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            return View();
        }

        /// <summary>
        ///  Purpose            : to get the updated  reciept on te basis of fleet ID
        /// Function Name       :  Update
        /// Created By          :  Salil Gupta 
        /// Created On          :  1/28/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleeId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Update(int fleeId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            var obj = new ReceiptFormatViewData();
            try
            {
                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);

                _logMessage.Append("calling ToreceiptViewData method to map receiptFormatDto to receiptFormatViewData and retrun receiptFormatViewData");

                obj.fleetReceipt = _receiptService.GetReceiptFleet(fleeId).ToViewDataList();
                if (obj.fleetReceipt.Count() == 0)
                {
                    Session.Remove("User");
                    return RedirectToAction("index", "Member");
                }

                obj.fleetlist = _receiptService.GetFleetList(merchantId).ToViewDataList();

                _logMessage.Append("ToreceiptViewData method executed successfully");

                obj.fk_FleetID = fleeId;
                obj.FleetName = _fleetService.GetFleet(fleeId, merchantId).ToViewData().FleetName;

                _logMessage.Append("calling GetUserFleet method of _receiptService with merhant id = " + merchantId + " then calling ToViewDataList method to map FleetDto to fleetViewData and retrun fleetViewData");
                _logMessage.Append("method executed successfully");
            }
            catch (Exception exception)
            {
                _logger.LogError(exception.ToString());
            }
            return View(obj);
        }

        /// <summary>
        ///  Purpose            : to retrieve the updated reciept
        /// Function Name       : Update
        /// Created By          :  Salil Gupta 
        /// Created On          :  1/29/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="viewData"></param>
        /// <param name="fleeId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(ReceiptFormatViewData viewData, int fleeId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1) 
                return RedirectToAction("index", "Member");

            try
            {
                int receiptId = 0;
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);

                _logMessage.Append("calling ToRecieptDto method to map receiptFormatViewData to manageRecieptDto and retrun receiptFormatdto");

                ReceiptFormatDto objDto = viewData.ToDTO();

                _logMessage.Append("ToRecieptDto method executed successfully");

                objDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                objDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                objDto.fk_MerchantID = merchantId;
                objDto.ModifiedDate = DateTime.Now;

                _logMessage.Append("Checing is Fleet Exist");

                foreach (var item in objDto.fleetReceipt)
                {
                    receiptId = item.FleetReceiptID;
                }

                var res = _receiptService.IsExistFleetForUpdate((int)viewData.fk_FleetID, receiptId);
                if (res)
                {
                    _logMessage.Append("Fleet Exist");
                    _logMessage.Append("calling ToreceiptViewData method to map receiptFormatdto to receiptFormatViewData and retrun receiptFormatViewData");

                    ReceiptFormatViewData obj = new ReceiptFormatViewData
                    {
                        fleetReceipt = _receiptService.GetReceiptFleet(fleeId).ToViewDataList(),
                        fleetlist = _receiptService.GetFleetList(merchantId).ToViewDataList(),
                        fk_FleetID = fleeId
                    };

                    _logMessage.Append("ToreceiptViewData method executed successfully");
                    _logMessage.Append("calling GetUserFleet method of _receiptService with merhant id = " + merchantId + " then calling ToViewDataList method to map FleetDto to fleetViewData and retrun fleetViewData");
                    _logMessage.Append(" method executed successfully");

                    ViewBag.IsExistFleet = MtdataResource.Fleeet_already_receipt_format_choose_another; //"This fleet has already receipt format,choose another.";

                    return View(obj);
                }

                _receiptService.Update(objDto, fleeId);
                string userName = ((SessionDto)Session["User"]).SessionUser.Email;

                _logTracking.Append("User " + userName + " update receipt for fleet ID " + fleeId + " at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return RedirectToAction("Index", "Reciept");
        }

        /// <summary>
        ///  Purpose            : to get the reciept
        /// Function Name       :  Create
        /// Created By          :  Salil Gupta 
        /// Created On          :  1/29/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                int logOnByUserId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserId));
                int logOnByUserType = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserType));
                ReceiptFormatViewData masterView = new ReceiptFormatViewData();
                masterView.fleetReceipt = _receiptService.GetReceiptFleet(0).ToViewDataList();

                if (masterView.fleetReceipt.Count() == 0)
                    masterView.recceiptMaster = _receiptService.GetReceiptFields().MasterToViewDataList(); 
                else
                    return RedirectToAction("GetDefaultFleetReciept"); 

                _logMessage.Append("Calling FleetList method of _userService class takes merchantId= " + merchantId);
                _logMessage.Append("calling ToViewDataList method to map FleetDto to fleetViewData and retrun fleetViewData");
                
                masterView.fleetlist = _receiptService.GetFleetList(merchantId).ToViewDataList();
                if (logOnByUserType == 5)
                {
                    List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                    List<FleetViewData> fleetViewResult = masterView.fleetlist.Where(m => fleetListAccessed.Contains((int)m.FleetID)).Select(m => m).Distinct().ToList();
                    masterView.fleetlist = fleetViewResult.AsEnumerable();
                }

                _logMessage.Append("ToViewDataList method executed successfully");

                return View(masterView);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            : to retrieve the reciept from database
        /// Function Name       :  Create
        /// Created By          :  Salil Gupta 
        /// Created On          :  1/30/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="reciptView"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(ReceiptFormatViewData reciptView)
        {
            try
            {
                if (Session["User"] == null) return RedirectToAction("Index", "Member");
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                    return RedirectToAction("index", "Member");

                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                _logMessage.Append("calling ToRecieptDto method to map receiptFormatdto to receiptFormatViewData and retrun receiptFormatdto");

                ReceiptFormatDto receiptFormatDto = reciptView.ToDTO();

                _logMessage.Append("ToRecieptDto method executed successfully");

                receiptFormatDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                receiptFormatDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                int logOnByUserId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserId));
                int logOnByUserType = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserType));
                var res = _receiptService.IsExistFleet(receiptFormatDto.fk_FleetID);

                if (res)
                {
                    _logMessage.Append("Fleet Exist");

                    ReceiptFormatViewData masterView = new ReceiptFormatViewData
                    {
                        recceiptMaster = _receiptService.GetReceiptFields().MasterToViewDataList()
                    };

                    _logMessage.Append("Calling FleetList method of _userService class takes merchantId= " + merchantId);
                    _logMessage.Append("calling ToViewDataList method to map FleetDto to fleetViewData and retrun fleetViewData");

                    masterView.fleetlist = _receiptService.GetFleetList(merchantId).ToViewDataList();
                    if (logOnByUserType == 5)
                    {
                        List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                        List<FleetViewData> fleetViewResult = masterView.fleetlist.Where(m => fleetListAccessed.Contains((int)m.FleetID)).Select(m => m).Distinct().ToList();
                        masterView.fleetlist = fleetViewResult.AsEnumerable();
                    }

                    _logMessage.Append("ToViewDataList method executed successfully");

                    ViewBag.IsExistFleet = MtdataResource.Fleeet_already_receipt_format_choose_another; //"This fleet has already receipt format,choose another.";

                    return View(masterView);
                }

                _receiptService.AddReciept(receiptFormatDto);
                string userName = ((SessionDto)Session["User"]).SessionUser.Email;

                _logTracking.Append("User " + userName + " create receipt for fleetID " + receiptFormatDto .fk_FleetID+ " at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return RedirectToAction("index", "Reciept");
        }

        /// <summary>
        ///  Purpose            : apply page indexing to display the list of receipt
        /// Function Name       :   ReceiptListByFiter
        /// Created By          :  Umesh Kumar
        /// Created On          :  2/6/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult ReceiptListByFiter(string name = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling ReceiptListByFilter method of _receiptService class to get Receipts");

                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                int logOnByUserId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserId));
                int logOnByUserType = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserType));
                int recListCount;

                name = name.Trim();

                FilterSearchParametersDto filterSearch = new FilterSearchParametersDto()
                {
                    Name = name,
                    StartIndex = jtStartIndex,
                    PageSize = jtPageSize,
                    Sorting = jtSorting,
                    MerchantId = merchantId
                };

                var receiptList = _receiptService.ReceiptListByFilter(filterSearch,logOnByUserId,logOnByUserType);
                if (string.IsNullOrEmpty(name))
                {
                    _logMessage.Append("Calling UserList method of _receiptService class to count fee");

                    recListCount = _receiptService.ReceiptList(merchantId, name, logOnByUserId, logOnByUserType).Count();

                    _logMessage.Append("UserList method of _receiptService executed successfully result = " + recListCount);

                    return Json(new { Result = "OK", Records = receiptList, TotalRecordCount = recListCount });
                }

                var manageRecieptDtos = receiptList as ReceiptFormatDto[] ?? receiptList.ToArray();
                recListCount = _receiptService.ReceiptList(merchantId, name,logOnByUserId,logOnByUserType).Count();

                return Json(new { Result = "OK", Records = manageRecieptDtos, TotalRecordCount = recListCount });
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);

                return Json(new { Result = "ERROR", exception.Message });
            }
        }

        /// <summary>
        ///  Purpose            : apply page indexing to delete the list of receipt
        /// Function Name       :   DeleteReceipt
        /// Created By          :  Umesh Kumar
        /// Created On          :  2/6/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteReceipt(int fleetId)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                try
                {
                    int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);

                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    _logMessage.Append("Calling DeleteReceipt method of _receiptService class takes fleetId= " + fleetId);

                    _receiptService.DeleteReceipt(fleetId, merchantId);

                    _logMessage.Append("DeleteReceipt method of _receiptService class executed successfully");

                    Thread.Sleep(50);
                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;

                    _logTracking.Append("User " + userName + " delete receipt for fleet ID" + fleetId + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return Json(new { Result = "OK" });
                }
                catch (Exception exception)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), exception);
                }
            }

            return Json(new { Result = "No" });
        }

        /// <summary>
        ///  Purpose            :to get format of receipt
        /// Function Name       :   ReceiptFormat
        /// Created By          : Salil Gupta
        /// Created On          :  2/6/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ReceiptFormat()
        {
            try
            {
                if (Session["User"] == null) return RedirectToAction("Index", "Member");
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 2 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 3 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 4)
                    return RedirectToAction("index", "Member");

                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                IEnumerable<ReceiptMasterViewData> masterView = _receiptService.GetReceiptFields().MasterToViewDataList();

                return View(masterView);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            :to retrieve the format of receipt
        /// Function Name       :  ReceiptFormat
        /// Created By          : Salil Gupta
        /// Created On          :  2/9/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="viewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ReceiptFormat(ReceiptMasterViewData viewData)
        {
            try
            {
                if (Session["User"] == null) return RedirectToAction("Index", "Member");
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 2 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 3 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 4)
                    return RedirectToAction("index", "Member");

                viewData.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                viewData.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                ReceiptMasterDto receiptMaterDto = viewData.ToReceiptMastertDto();
                _receiptService.UpdateReceiptMaster(receiptMaterDto);

                string userName = ((SessionDto)Session["User"]).SessionUser.Email;

                _logTracking.Append("User " + userName + " update receipt format  at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());

                IEnumerable<ReceiptMasterViewData> masterView = _receiptService.GetReceiptFields().MasterToViewDataList();

                return View(masterView);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            :to add the field of receipt
        /// Function Name       :  AddField
        /// Created By          : Salil Gupta
        /// Created On          :  2/9/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="field"></param>
        /// <param name="viewData"></param>
        /// <returns></returns>
        public ActionResult AddField(string field, ReceiptMasterViewData viewData)
        {
            try
            {
                if (Session["User"] == null) return RedirectToAction("Index", "Member");
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 2 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 3 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 4)
                    return RedirectToAction("index", "Member");

                viewData.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                viewData.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                ReceiptMasterDto receiptMaterDto = viewData.ToReceiptMastertDto();

                _receiptService.AddRecieptField(field, receiptMaterDto);
                
                string userName = ((SessionDto)Session["User"]).SessionUser.Email;

                _logTracking.Append("User " + userName + " add receipt field at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());

                return Redirect("ReceiptFormat");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return Redirect("ReceiptFormat");
        }

        /// <summary>
        ///  Purpose            :to delete the field of receipt
        /// Function Name       : DeleteField
        /// Created By          : Salil Gupta
        /// Created On          :  2/9/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fieldId"></param>
        /// <returns></returns>
        public bool DeleteField(int fieldId)
        {
            var res = false;
            try
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 2 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 3 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 4)
                    return false;

                IEnumerable<ReceiptMasterViewData> masterView;
                res = _receiptService.IsFieldExist(fieldId);
                if (res)
                {
                    masterView = _receiptService.GetReceiptFields().MasterToViewDataList();
                    ViewBag.IsFieldExist = MtdataResource.Fleet_cannot_deleted_already_in_use;   //"This field can not be deleted as it is in use.";
                    ViewData.Model = masterView;
                }
                else
                {
                    _receiptService.DeleteReceiptField(fieldId);

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;

                    _logTracking.Append("User " + userName + " delete receipt field at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return res;
        }

        /// <summary>
        ///  Purpose            :to check whether fleet is exist or not
        /// Function Name       :  IsFleetExist
        /// Created By          : Salil Gupta
        /// Created On          :  2/10/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public JsonResult IsFleetExist(int fleetId)
        {
            var status = _receiptService.IsExistFleet(fleetId);
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        ///  Purpose            :to check whether fleet is update or not
        /// Function Name       :IsFleetExistforUpdate
        /// Created By          : Salil Gupta
        /// Created On          :  2/10/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <param name="receiptId"></param>
        /// <returns></returns>
        public JsonResult IsFleetExistforUpdate(int fleetId, int receiptId)
        {
            var status = _receiptService.IsExistFleetForUpdate(fleetId, receiptId);
            return Json(status, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        ///Purpose            :to display default reciept
        /// Function Name       :DefaultReciept
        /// Created By          : Asif Shafeeque
        /// Created On          :  6/7/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DefaultReciept()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 2 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 3 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 4)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            
            try
            {
                _logMessage.Append("calling ToreceiptViewData method to map receiptFormatDto to receiptFormatViewData and retrun receiptFormatViewData");

                ReceiptFormatViewData objrec = new ReceiptFormatViewData();
                objrec.fleetReceipt = _receiptService.GetReceiptFleet(0).ToViewDataList();

                if (objrec.fleetReceipt.Count()==0)
                    objrec.recceiptMaster = _receiptService.GetReceiptFields().MasterToViewDataList();
                else
                    return RedirectToAction("GetDefaultReciept");

                _logMessage.Append("method executed successfully");

                return View(objrec);
            }
            catch (Exception exception)
            {
                _logger.LogError(exception.ToString());
            }

            return View();
        }

        /// <summary>
        ///Purpose            :to set default reciept
        /// Function Name       :DefaultReciept
        /// Created By          : Asif Shafeeque
        /// Created On          :  6/7/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="reciptView"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DefaultReciept(ReceiptFormatViewData reciptView)
        {
            try
            {
                if (Session["User"] == null) return RedirectToAction("Index", "Member");
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 2 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 3)
                    return RedirectToAction("index", "Member");

                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                _logMessage.Append("calling ToRecieptDto method to map receiptFormatdto to receiptFormatViewData and retrun receiptFormatdto");

                ReceiptFormatDto receiptFormatDto = reciptView.ToDTO();

                _logMessage.Append("ToRecieptDto method executed successfully");

                receiptFormatDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                receiptFormatDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                _receiptService.AddReciept(receiptFormatDto);
                TempData["IsUpdated"] = MtdataResource.Successfully_saved;

                string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                _logTracking.Append("User " + userName + " add default receipt at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());

                return RedirectToAction("GetDefaultReciept");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View(reciptView);
        }

        /// <summary>
        ///Purpose            :to get default reciept
        /// Function Name     :GetDefaultReciept
        /// Created By        : Asif Shafeeque
        /// Created On        :  6/9/2015
        /// Modification Made :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetDefaultReciept()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 2 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 3 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 4)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                _logMessage.Append("calling ToreceiptViewData method to map receiptFormatDto to receiptFormatViewData and retrun receiptFormatViewData");

                ReceiptFormatViewData objrec = new ReceiptFormatViewData();
                objrec.fleetReceipt = _receiptService.GetReceiptFleet(0).ToViewDataList();

                _logMessage.Append("method executed successfully");

                return View(objrec);
            }
            catch (Exception exception)
            {
                _logger.LogError(exception.ToString());
            }

            return View();
        }

        /// <summary>
        ///Purpose            :to set default reciept
        /// Function Name     :GetDefaultReciept
        /// Created By        : Asif Shafeeque
        /// Created On        :  6/9/2015
        /// Modification Made :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="viewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetDefaultReciept(ReceiptFormatViewData viewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                _logMessage.Append("calling ToRecieptDto method to map receiptFormatViewData to manageRecieptDto and retrun receiptFormatdto");

                ReceiptFormatDto objDto = viewData.ToDTO();

                _logMessage.Append("ToRecieptDto method executed successfully");

                objDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                objDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                objDto.ModifiedDate = DateTime.Now;
                _receiptService.Update(objDto, 0);
                TempData["IsUpdated"] = MtdataResource.Successfully_saved;

                string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                _logTracking.Append("User " + userName + " update default receipt at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());

                return View(viewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View(viewData);
        }

        /// <summary>
        ///Purpose            :to get default reciept which is set by admin
        /// Function Name     :GetDefaultFleetReciept
        /// Created By        : Asif Shafeeque
        /// Created On        :  6/9/2015
        /// Modification Made :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetDefaultFleetReciept()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                int logOnByUserId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserId));
                int logOnByUserType = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserType));
                ReceiptFormatViewData masterView = new ReceiptFormatViewData();

                masterView.fleetReceipt = _receiptService.GetReceiptFleet(0).ToViewDataList();

                _logMessage.Append("Calling FleetList method of _userService class takes merchantId= " + merchantId);
                _logMessage.Append("calling ToViewDataList method to map FleetDto to fleetViewData and retrun fleetViewData");
                
                masterView.fleetlist = _receiptService.GetFleetList(merchantId).ToViewDataList();
                if (logOnByUserType == 5)
                {
                    List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                    List<FleetViewData> fleetViewResult = masterView.fleetlist.Where(m => fleetListAccessed.Contains((int)m.FleetID)).Select(m => m).Distinct().ToList();
                    masterView.fleetlist = fleetViewResult.AsEnumerable();
                }

                _logMessage.Append("ToViewDataList method executed successfully");

                return View(masterView);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///Purpose            :to set default reciept which is set by admin for user
        /// Function Name     :GetDefaultFleetReciept
        /// Created By        : Asif Shafeeque
        /// Created On        :  6/9/2015
        /// Modification Made :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="reciptView"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetDefaultFleetReciept(ReceiptFormatViewData reciptView)
        {
            try
            {
                if (Session["User"] == null) return RedirectToAction("Index", "Member");
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                    return RedirectToAction("index", "Member");

                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("calling ToRecieptDto method to map receiptFormatdto to receiptFormatViewData and retrun receiptFormatdto");

                IEnumerable<FleetReceiptViewData> rec = reciptView.fleetReceipt;
                IEnumerable<ReceiptMasterViewData> obj = rec.ToReceipMasterList();
                reciptView.recceiptMaster = obj;
                ReceiptFormatDto receiptFormatDto = reciptView.ToDTO();

                _logMessage.Append("ToRecieptDto method executed successfully");

                receiptFormatDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                receiptFormatDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                int logOnByUserId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserId));
                int logOnByUserType = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserType));
                var res = _receiptService.IsExistFleet(receiptFormatDto.fk_FleetID);
                if (res)
                {
                    _logMessage.Append("Fleet Exist");

                    ReceiptFormatViewData masterView = new ReceiptFormatViewData
                    {
                        recceiptMaster = _receiptService.GetReceiptFields().MasterToViewDataList()
                    };

                    _logMessage.Append("Calling FleetList method of _userService class takes merchantId= " + merchantId);
                    _logMessage.Append("calling ToViewDataList method to map FleetDto to fleetViewData and retrun fleetViewData");

                    masterView.fleetlist = _receiptService.GetFleetList(merchantId).ToViewDataList();
                    if (logOnByUserType == 5)
                    {
                        List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                        List<FleetViewData> fleetViewResult = masterView.fleetlist.Where(m => fleetListAccessed.Contains((int)m.FleetID)).Select(m => m).Distinct().ToList();
                        masterView.fleetlist = fleetViewResult.AsEnumerable();
                    }

                    _logMessage.Append("ToViewDataList method executed successfully");

                    ViewBag.IsExistFleet = MtdataResource.Fleeet_already_receipt_format_choose_another; //"This fleet has already receipt format,choose another.";

                    return RedirectToAction("index", "Reciept");
                }

                _receiptService.AddReciept(receiptFormatDto);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return RedirectToAction("index", "Reciept");
        }

    }
}