IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PayDriverOwnerNetwork]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PayDriverOwnerNetwork]
GO

/****** Object:  StoredProcedure [dbo].[usp_PayDriverOwnerNetwork]    Script Date: 11/2/2015 10:59:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <08-10-2015>
-- Description:	<Get data in xml format when pay driver or pay car owner is selected>
-- =============================================
CREATE PROCEDURE [dbo].[usp_PayDriverOwnerNetwork]
	-- Add the parameters for the stored procedure here
    @DriverNo NVARCHAR(60),
	@FleetId INT,
	@DateFrom DATETIME,
	@DateTo DATETIME,
	@ReportType INT=NULL,
	@XmlData NVARCHAR(MAX) OUT
AS
BEGIN TRY	
  -- SET NOCOUNT ON added to prevent extra result sets from
  SET NOCOUNT ON;
  DECLARE @Index INT,
          @Count INT,
	      @LogInDateTime DATETIME,
          @LogOutDateTime DATETIME,
	      @CarNo NVARCHAR(60),
	      @DailyReportHead NVARCHAR(200),
		  @InnerData NVARCHAR(MAX),
		  @ManualRepHead NVARCHAR(200),
		  @ManualData NVARCHAR(MAX), 
		  @Seq INT=0  
  DECLARE @TotalData TABLE
    (
	   Id INT,
	   JobNumber  NVARCHAR(50),
	   PickAddress NVARCHAR(100),
	   DestinationAddress NVARCHAR(100),
	   FareValue MONEY,
	   Taxes MONEY,
	   Fee MONEY,
	   Tip MONEY,
	   Amount MONEY,
	   FleetFee MONEY,  
	   TxnType NVARCHAR(30),
	   VehicleNo NVARCHAR(60),
	   TxnDate DATETIME,
	   EntryMode NVARCHAR(30),
	   Surcharge MONEY
    )
	SET @XmlData=''
	INSERT INTO @TotalData 
	SELECT   tblSaleCompletion.Id,tblSaleCompletion.JobNumber,tblSaleCompletion.PickAddress,tblSaleCompletion.DestinationAddress,
	         tblSaleCompletion.FareValue,tblSaleCompletion.Taxes,tblSaleCompletion.Fee,tblSaleCompletion.Tip,tblSaleCompletion.Amount,
			 tblSaleCompletion.FleetFee,tblSaleCompletion.TxnType,tblSaleCompletion.VehicleNo,tblSaleCompletion.TxnDate,
			 tblSaleCompletion.EntryMode,tblSaleCompletion.Surcharge        			
     FROM
       (
	    SELECT 
		      Id AS 'Id'
			 ,JobNumber AS 'JobNumber'
			 ,PickAddress AS 'PickAddress'
			 ,DestinationAddress AS 'DestinationAddress'
			 ,FareValue AS 'FareValue'
			 ,Taxes AS 'Taxes'
			 ,Fee AS 'Fee'
			 ,Tip AS 'Tip'
			 ,Amount AS 'Amount'
			 ,FleetFee AS 'FleetFee'
			 ,TxnType AS 'TxnType'
			 ,VehicleNo AS 'VehicleNo'
			 ,TxnDate AS 'TxnDate'
			 ,EntryMode	AS 'EntryMode'
			 ,Surcharge AS 'Surcharge'		                                                                                                                                  
	     FROM    dbo.MTDTransaction 								 													 								
		 WHERE   TxnType='Sale'
				 AND IsRefunded=0
			     AND IsVoided=0				    
			     AND DriverNo=@DriverNo
		         AND fk_FleetId=@FleetId
		         AND ResponseCode IN ('000','002')
		  UNION ALL
		  SELECT  tblId.Id,
		          tblId.JobNumber,
			      tblId.PickAddress,
				  tblId.DestinationAddress,
			      tblId.FareValue,tblId.Taxes,
			      tblId.Fee,tblId.Tip,tblSrc.Amount,
			      tblId.FleetFee,
			      tblSrc.TxnType,tblSrc.VehicleNo,
			      tblSrc.TxnDate,tblId.EntryMode,
				  tblId.Surcharge	                                                                
	      FROM dbo.MTDTransaction tblSrc 
	      INNER JOIN dbo.MTDTransaction tblId
		  ON tblSrc.SourceId=tblId.Id						 								
		  WHERE  tblSrc.TxnType='Completion'
				 AND tblSrc.DriverNo=@DriverNo
		         AND tblSrc.fk_FleetId=@FleetId
		         AND tblSrc.ResponseCode IN ('000','002')
		) tblSaleCompletion	
	DECLARE @TableData TABLE(Id INT IDENTITY(1,1) PRIMARY KEY,LogInTime DATETIME,LogOutTime DATETIME)
	INSERT INTO @TableData(LogInTime,LogOutTime)
	SELECT LogOnDateTime,LogOffDateTime FROM DriverTransactionHistory WHERE DriverNo= @DriverNo 
	                                                                        AND FleetId=@FleetId 
																	        AND LogOnDateTime>@DateFrom-5
                                                                            AND LogOnDateTime<@DateTo 
																	        AND LogOffDateTime>@DateFrom 
																	        AND LogOffDateTime<@DateTo 
										                                ORDER BY LogOnDateTime
     SELECT @Count=COUNT(Id) FROM @TableData
	 SET @Index=1
	 IF(@ReportType=1)
     BEGIN
	 WHILE(@Index<=@Count)
         BEGIN
             SELECT @LogInDateTime=LogInTime,@LogOutDateTime=LogOutTime  FROM @TableData WHERE Id=@Index
			 SET @CarNo=(SELECT TOP 1 VehicleNo  FROM @TotalData WHERE TxnDate>= @LogInDateTime 
			                                                           AND TxnDate<=@LogOutDateTime)																			   																																		
	         SET @DailyReportHead='<tr><td colspan=10 style="text-align:center;"><div>Car Number:  '+@CarNo+' &nbsp;&nbsp;&nbsp;Shift from '
		     +CONVERT(VARCHAR(17),@LogInDateTime,113)+' To '+CONVERT(VARCHAR(17),@LogOutDateTime,113)+'</div></td></tr>' 
			 SET @InnerData = COALESCE(CAST( (
			     	                   SELECT   COALESCE(JobNumber,'') AS 'td','',
			                                    COALESCE(PickAddress,'') AS 'td','',
                                                COALESCE(DestinationAddress,'') AS 'td','',
												COALESCE(TxnType,'') AS 'td','',
			     					            CONCAT('$',CAST(COALESCE(FareValue,0.00) AS DECIMAL(18,2))) AS 'td','',
			     					            CONCAT('$',CAST(COALESCE(Taxes,0.00) AS DECIMAL(18,2))) AS 'td','',
			     					            CONCAT('$',CAST(COALESCE(Tip,0.00) AS DECIMAL(18,2))) AS 'td','',
												CONCAT('$',CAST(COALESCE(Amount,0.00)-COALESCE(Surcharge,0.00) AS DECIMAL(18,2))) AS 'td','',
												CONCAT('$',CAST(COALESCE(FleetFee,0.00)+COALESCE(Fee,0.00)  AS DECIMAL(18,2))) AS 'td','',
			     					            CONCAT('$',CAST(COALESCE(Amount,0.00)-COALESCE(FleetFee,0.00)-COALESCE(Fee,0.00)-COALESCE(Surcharge,0.00) AS DECIMAL(18,2))) AS 'td' 
										FROM @TotalData
										WHERE TxnDate>=@LogInDateTime 
			     		                      AND TxnDate<=@LogOutDateTime	
			     					          AND EntryMode NOT IN('Keyed')		   															 
			     			            FOR XML PATH('tr'), ELEMENTS) AS NVARCHAR(MAX)),'')	                                                            
			IF(@InnerData<>'' AND @InnerData IS NOT NULL) 
		    BEGIN
                SET @InnerData=@DailyReportHead+@InnerData
				SET @XmlData=@XmlData+@InnerData
            END		   
            SET @Index=@Index+1		
		END
		 SET @ManualRepHead='<tr><td colspan=10 style="text-align:center;"><div>Manual Transactions</div></td></tr>'									    
		 SET @ManualData=COALESCE(CAST( ( SELECT    COALESCE(JobNumber,'') AS 'td','',
			                                        COALESCE(PickAddress,'') AS 'td','',
                                                    COALESCE(DestinationAddress,'') AS 'td','',
												    COALESCE(TxnType,'') AS 'td','',
													CONCAT('$',CAST(COALESCE(FareValue,0.00) AS DECIMAL(18,2))) AS 'td','',
													CONCAT('$',CAST(COALESCE(Taxes,0.00) AS DECIMAL(18,2))) AS 'td','',
		                                            CONCAT('$',CAST(COALESCE(Tip,0.00) AS DECIMAL(18,2))) AS 'td','',     
													CONCAT('$',CAST(COALESCE(Amount,0.00)-COALESCE(Surcharge,0.00) AS DECIMAL(18,2))) AS 'td','', 
													CONCAT('$',CAST(COALESCE(FleetFee,0.00)+COALESCE(Fee,0.00) AS DECIMAL(18,2))) AS 'td','',  
													CONCAT('$',CAST(COALESCE(Amount,0.00)-COALESCE(FleetFee,0.00)-COALESCE(Fee,0.00)-COALESCE(Surcharge,0.00) AS DECIMAL(18,2))) AS 'td' 
		                                      FROM @TotalData 
											  WHERE TxnDate>@DateFrom 
		                                      AND TxnDate<@DateTo 											
											  AND EntryMode='Keyed'
		                                      FOR XML PATH('tr'), ELEMENTS) AS NVARCHAR(MAX)),'')	
		IF(@ManualData<>'' AND @ManualData IS NOT NULL)
		BEGIN
		   SET @ManualData= @ManualRepHead+@ManualData
		   SET @XmlData=@XmlData+@ManualData
		END	
	  END
	 ELSE  
     BEGIN
         WHILE(@Index<=@Count)
         BEGIN
		     SELECT @LogInDateTime=LogInTime,@LogOutDateTime=LogOutTime  FROM @TableData WHERE Id=@Index  
			 IF EXISTS(SELECT 1 FROM @TotalData WHERE  TxnDate>=@LogInDateTime 
													   AND TxnDate<=@LogOutDateTime
													   AND EntryMode NOT IN ('Keyed'))	
			 BEGIN
			 SET @Seq=@Seq+1
			 SET @XmlData=@XmlData+COALESCE(CAST((SELECT @Seq AS 'td','',
			                                             COALESCE(CONVERT(VARCHAR(17),@LogInDateTime,113),'') AS 'td','',
                                                         COALESCE(CONVERT(VARCHAR(17),@LogOutDateTime,113),'') AS 'td','', 
														 COALESCE(MAX(VehicleNo),'') AS 'td','',
														 COALESCE(COUNT(Id),0) AS 'td','',
                                                         CONCAT('$',CAST(COALESCE(SUM(FareValue),0.00) AS DECIMAL(18,2)))  AS 'td','',
														 CONCAT('$',CAST(COALESCE(SUM(Taxes),0.00) AS DECIMAL(18,2)))  AS 'td','',													
														 CONCAT('$',CAST(COALESCE(SUM(Tip),0.00) AS DECIMAL(18,2)))  AS 'td','', 
														 CONCAT('$',CAST(COALESCE(SUM(Amount),0.00)-COALESCE(SUM(Surcharge),0.00) AS DECIMAL(18,2)))  AS 'td','', 
														 CONCAT('$',CAST(COALESCE(SUM(FleetFee),0.00)+COALESCE(SUM(Fee),0.00) AS DECIMAL(18,2)))  AS 'td','', 
                                                         CONCAT('$',CAST(COALESCE(SUM(Amount),0.00)-COALESCE(SUM(FleetFee),0.00)-COALESCE(SUM(Fee),0.00)-COALESCE(SUM(Surcharge),0.00) AS DECIMAL(18,2))) AS 'td' 
		                                           FROM @TotalData  
												   WHERE  TxnDate>=@LogInDateTime 
												          AND TxnDate<=@LogOutDateTime 												      
														  AND EntryMode NOT IN ('Keyed')
                                                  FOR XML PATH('tr'),ELEMENTS) AS NVARCHAR(MAX)),'')																		  																   	
             END
              SET @Index=@Index+1
		 END
		 SET @Seq=@Seq+1
		 IF EXISTS(SELECT 1 FROM @TotalData WHERE  TxnDate>@DateFrom 
												   AND TxnDate<@DateTo
												   AND EntryMode='Keyed')
		 BEGIN
			 SET @XmlData=@XmlData+COALESCE(CAST((SELECT @Seq AS 'td','',
			                                             'NA (Manual)' AS 'td','',
                                                         'NA (Manual)' AS 'td','',
														 COALESCE(MAX(VehicleNo),'') AS 'td','',
														 COALESCE(COUNT(Id),0) AS 'td','',
                                                         CONCAT('$',CAST(COALESCE(SUM(FareValue),0.00) AS DECIMAL(18,2)))  AS 'td','',
														 CONCAT('$',CAST(COALESCE(SUM(Taxes),0.00) AS DECIMAL(18,2)))  AS 'td','',														
		                                                 CONCAT('$',CAST(COALESCE(SUM(Tip),0.00) AS DECIMAL(18,2)))  AS 'td','',
														 CONCAT('$',CAST(COALESCE(SUM(Amount),0.00)-COALESCE(SUM(Surcharge),0.00) AS DECIMAL(18,2)))  AS 'td','',
														 CONCAT('$',CAST(COALESCE(SUM(Fee),0.00)+COALESCE(SUM(FleetFee),0.00) AS DECIMAL(18,2)))  AS 'td','',														 	                                                													 														 	                                                
                                                         CONCAT('$',CAST(COALESCE(SUM(Amount),0.00)-COALESCE(SUM(FleetFee),0.00)-COALESCE(SUM(Fee),0.00)-COALESCE(SUM(Surcharge),0.00) AS DECIMAL(18,2))) AS 'td' 
		                                           FROM @TotalData  
												   WHERE  TxnDate>@DateFrom 
												          AND TxnDate<@DateTo 
														  AND EntryMode='Keyed'
                                                  FOR XML PATH('tr'),ELEMENTS) AS NVARCHAR(MAX)),'')
		END
      END
	  SELECT @XmlData
END TRY
--use catch to log error in the table  ProcErrorHandler
BEGIN CATCH    		
       INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
       SELECT ERROR_NUMBER()
	         ,ERROR_SEVERITY()
			 ,ERROR_STATE()
			 ,ERROR_PROCEDURE()
			 ,ERROR_LINE()
			 ,ERROR_MESSAGE()
			 ,SUSER_SNAME()
			 ,HOST_NAME()
			 ,GETDATE()
END CATCH

GO

