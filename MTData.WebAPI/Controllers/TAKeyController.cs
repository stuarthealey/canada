﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Web.Http;
using System.Xml;
using Antlr.Runtime.Tree;
using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.WebAPI.Models;
using Ninject;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.Serialization;
using MTData.WebAPI.Resources;
using MTData.WebAPI.Common;
using System.Web;
using System.Web.Configuration;
using System.Net;
using System.Security;
using System.Collections.Generic;
using System.Net.Http.Headers;


namespace MTData.WebAPI.Controllers
{
    public class TAKeyController : TransactionRequestController
    {
        #region (Constructor Loading)
        private readonly ITransactionService _transactionService;
        private readonly ITerminalService _terminalService;
        private readonly IMerchantService _merchantService;
        private readonly IDriverService _driverService;
        private readonly ICommonService _commonService;

        private LocalVariables objLocalVars = new LocalVariables();
        private readonly StringBuilder _logMessage;
        private StringBuilder _logTracking;
        readonly ILogger _logger = new Logger();
        public TAKeyController(
            [Named("TransactionService")] ITransactionService transactionService,
            [Named("MerchantService")] IMerchantService merchantService,
            [Named("DriverService")] IDriverService driverService,
            [Named("TerminalService")] ITerminalService terminalService,
            [Named("CommonService")] ICommonService commonService,
            StringBuilder logMessage, StringBuilder logTracking)
            : base(terminalService, logMessage)
        {
            _merchantService = merchantService;
            _transactionService = transactionService;
            _terminalService = terminalService;
            _driverService = driverService;
            _logMessage = logMessage;
            _commonService = commonService;
            _logTracking = logTracking;
        }

        fDMerchantDto _fdMerchantobj;
        DatawireDto _datawireObj;
        PaymentResponse response = new PaymentResponse();
        EmvCapkResponse capkresponse = new EmvCapkResponse();
        decimal fleetFee = 0;
        decimal techFee = 0;
        decimal txnFee = 0;
        #endregion


        #region TAkey Download

        /// <summary>
        /// Purpose             :   To download CapK xml file from server.
        /// Function Name       :   TAKeyDownloadFile
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   4th-feb--2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>

        [HttpPost]
        public IHttpActionResult TAKeyDownloadFile(HttpRequestMessage request)
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            TAKeyResponse resp = new TAKeyResponse();
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                _logMessage.AppendLine("Loading request XML data.");
                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                _logTracking.AppendLine("TAKey process start. DriverNo: " + objLocalVars._driverNo + " and  DeviceId " + objLocalVars._deviceId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());

                _logMessage.AppendLine("Initializing local variables from XML request data");
                #region Initialize capk local variable from XML Request.
                if (!SetLocalVarFromTAKeyXMLRequest(xmlDoc))
                {
                    _logMessage.AppendLine("Failed to initializ TAKey local variables from request XML data.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    resp = TAKeyResponse(false, PaymentAPIResources.InvalidRequest);
                    return Ok(resp);
                }
                #endregion

                #region Validate TA Key Request
                dynamic firstDataDetails = string.Empty;
                _logMessage.AppendLine("Validating capk request xml data");
                firstDataDetails = _commonService.GetFirstDataDetails(objLocalVars._deviceId, objLocalVars._driverNo, objLocalVars._token, Convert.ToInt32(objLocalVars.FleetId), objLocalVars._requestType);
                if (firstDataDetails == null)
                {
                    _logMessage.AppendLine("Failed to capk validate request xml data.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    resp = TAKeyResponse(false, PaymentAPIResources.UnauthorizedAccess);
                    return Ok(resp);
                }
                #endregion

                _logMessage.AppendLine("Returning file URL");

                string filePath = HttpContext.Current.Request.MapPath(@"~\Resources\MTDWELLSIPP.TGZ");

                string path = WebConfigurationManager.AppSettings["TAKeyFilePath"];
                resp = TAKeyResponse(true, path);

                _logMessage.AppendLine("DownloadTAKeyFile process completed successfully.");
                _logger.LogInfoMessage(_logMessage.ToString());

                return Ok(resp);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
                resp = TAKeyResponse(false, PaymentAPIResources.Exception);
                return Ok(resp);
            }
        }


        /// <summary>
        /// Purpose             :   Extract the value from XMLrequest and set to Local variables
        /// Function Name       :   SetLocalVarFromTAKeyXMLRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   04/feb/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private bool SetLocalVarFromTAKeyXMLRequest(XmlDocument xmlDoc)
        {
            try
            {
                var dvcID = xmlDoc.SelectSingleNode("PaymentRequest/Header/DeviceId");
                if (!dvcID.IsNull())
                    objLocalVars._deviceId = dvcID.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/DeviceId".MissingElementMessage());
                    return false;
                }


                var reqID = xmlDoc.SelectSingleNode("PaymentRequest/Header/RequestId");
                if (!reqID.IsNull())
                    objLocalVars._requestId = reqID.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/RequestId".MissingElementMessage());
                    return false;
                }

                var drvrID = xmlDoc.SelectSingleNode("PaymentRequest/Header/DriverId");
                if (!drvrID.IsNull())
                    objLocalVars._driverNo = drvrID.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/DriverId".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var token = xmlDoc.SelectSingleNode("PaymentRequest/Header/Token");
                if (!token.IsNull())
                    objLocalVars._token = token.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/Token".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var requestType = xmlDoc.SelectSingleNode("PaymentRequest/Header/RequestType");
                if (!requestType.IsNull())
                    objLocalVars._requestType = requestType.InnerText;
                else
                {
                    objLocalVars._requestType = null;
                }


                var fleetId = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/FleetId");
                if (!fleetId.IsNull())
                    objLocalVars.FleetId = fleetId.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/FleetId".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }
                return true;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Handling CapKey response
        /// </summary>
        /// <param name="status"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static TAKeyResponse TAKeyResponse(bool status, string message)
        {
            TAKeyResponse resp = new TAKeyResponse();
            ResponseHeader hdr = new ResponseHeader();
            if (status)
                hdr.Status = PaymentAPIResources.Success;
            else
                hdr.Status = PaymentAPIResources.Failed;

            hdr.Message = message;
            resp.Header = hdr;

            return resp;
        }

        #endregion

        #region TAKey Update

        /// <summary>
        /// Purpose             :   To download CapK xml file from server.
        /// Function Name       :   UpdateCapKeyStatus
        /// Created By          :   Madhuri tanwar
        /// Created On          :   4-Feb-2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult UpdateTAKeyStatus(HttpRequestMessage request)
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            CapKeyResponse resp = new CapKeyResponse();
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                _logMessage.AppendLine("Loading request XML data.");
                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                _logTracking.AppendLine("Payment process start. DriverNo: " + objLocalVars._driverNo + " and  DeviceId " + objLocalVars._deviceId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());

                _logMessage.AppendLine("Initializing local variables from XML request data");
                #region Initialize capk local variable from XML Request.
                if (!LoadCapKeyVariables(xmlDoc))
                {
                    _logMessage.AppendLine("Failed to initializ capk local variables from request XML data.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    resp = CapkUtil.CapKeyResponse(false, PaymentAPIResources.InvalidRequest);
                    return Ok(resp);
                }
                #endregion

                #region Validate Capk Request
                dynamic firstDataDetails = string.Empty;
                _logMessage.AppendLine("Validating capk request xml data");
                firstDataDetails = _commonService.GetFirstDataDetails(objLocalVars._deviceId, objLocalVars._driverNo, objLocalVars._token, Convert.ToInt32(objLocalVars.FleetId), objLocalVars._requestType);
                if (firstDataDetails == null)
                {
                    _logMessage.AppendLine("Failed to capk validate request xml data.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    resp = CapkUtil.CapKeyResponse(false, PaymentAPIResources.UnauthorizedAccess);
                    return Ok(resp);
                }
                #endregion

                if (objLocalVars.CapKStatus.ToUpper() == "Y")
                {
                    if (_terminalService.UpdateCapVersion(objLocalVars._deviceId))
                        resp = CapkUtil.CapKeyResponse(true, PaymentAPIResources.CapkUdateSuccess + "for DeviceId " + objLocalVars._deviceId);
                    else
                        resp = CapkUtil.CapKeyResponse(false, PaymentAPIResources.CapkUdateFailed + "for DeviceId " + objLocalVars._deviceId);
                }
                else
                    resp = CapkUtil.CapKeyResponse(false, PaymentAPIResources.CapkUdateFailed + "for DeviceId " + objLocalVars._deviceId);

                _logMessage.AppendLine("DownloadCapKeyFile process completed successfully.");
                _logger.LogInfoMessage(_logMessage.ToString());

                return Ok(resp);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
                resp = CapkUtil.CapKeyResponse(false, PaymentAPIResources.Exception);
                return Ok(resp);
            }
        }

        /// <summary>
        /// Purpose             :   Extract the value from XMLrequest and set to Local variables
        /// Function Name       :   SetLocalVarFromCapkXMLRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   4/feb/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private bool LoadCapKeyVariables(XmlDocument xmlDoc)
        {
            try
            {
                var dvcID = xmlDoc.SelectSingleNode("PaymentRequest/Header/DeviceId");
                if (!dvcID.IsNull())
                    objLocalVars._deviceId = dvcID.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/DeviceId".MissingElementMessage());
                    return false;
                }

                var reqID = xmlDoc.SelectSingleNode("PaymentRequest/Header/RequestId");
                if (!reqID.IsNull())
                    objLocalVars._requestId = reqID.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/RequestId".MissingElementMessage());
                    return false;
                }

                var drvrID = xmlDoc.SelectSingleNode("PaymentRequest/Header/DriverId");
                if (!drvrID.IsNull())
                    objLocalVars._driverNo = drvrID.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/DriverId".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var token = xmlDoc.SelectSingleNode("PaymentRequest/Header/Token");
                if (!token.IsNull())
                    objLocalVars._token = token.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/Token".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var capkStats = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/CapKeyUpdated");
                if (!capkStats.IsNull())
                    objLocalVars.CapKStatus = capkStats.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/CapKeyUpdated".MissingElementMessage(), objLocalVars._deviceId, objLocalVars._requestId);
                    return false;
                }

                var fleetId = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/FleetId");
                if (!fleetId.IsNull())
                    objLocalVars.FleetId = fleetId.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/FleetId".MissingElementMessage());
                    return false;
                }


                return true;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Load first request value from database
        /// </summary>
        /// <param name="capkDBResp"></param>
        /// <returns></returns>
        private void LoadCapkRequestFromDB()
        {
            try
            {
                var capkDto = _transactionService.GetCapKey();
                if (capkDto != null)
                {
                    objLocalVars.CapkFileCreationDt = capkDto.FileCreationDate;
                    objLocalVars.fileSize = capkDto.FileSize;
                    objLocalVars.fileCRC16 = capkDto.FileCRC16;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion







    }

}




