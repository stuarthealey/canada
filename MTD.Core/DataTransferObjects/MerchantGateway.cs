﻿namespace MTD.Core.DataTransferObjects
{
    public class MerchantGateway
    {
        public int ComId { get; set; }
        public int? FkMerchantId { get; set; }
        public int? FkGatewayId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}