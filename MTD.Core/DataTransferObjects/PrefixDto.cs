﻿using System;

namespace MTD.Core.DataTransferObjects
{
  public  class PrefixDto
    {
        public int PrefixID { get; set; }
        public int CCPrefix1 { get; set; }
        public int fk_GatewayID { get; set; }
        public int? fk_MerchantId { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

    }
}
