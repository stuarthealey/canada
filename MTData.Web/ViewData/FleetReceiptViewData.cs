﻿using System;
using System.Collections.Generic;

namespace MTData.Web.ViewData
{
    public class FleetReceiptViewData
    {
        public int FleetReceiptID { get; set; }
        public int fk_FleetID { get; set; }
        public int fk_FieldID { get; set; }
        public string FieldName { get; set; }
        public string fk_FieldText { get; set; }
        public int Postion { get; set; }
        public Nullable<bool> IsShow { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public int FieldID { get; set; }
        public List<string> RecieptCol { get; set; }
        public int Position { get; set; }
      
     
    }
}