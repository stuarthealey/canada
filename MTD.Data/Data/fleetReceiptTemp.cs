﻿using System;
using System.Collections.Generic;

namespace MTD.Data.Data
{
   public class fleetReceiptTemp
    {
        public int FleetReceiptID { get; set; }
        public int? fk_FleetID { get; set; }
        public int fk_FieldID { get; set; }
        public string FieldName { get; set; }
        public string fk_FieldText { get; set; }
        public int Postion { get; set; }
        public Nullable<bool> IsShow { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public int FieldID { get; set; }
        public List<string> RecieptCol { get; set; }
        public int Position { get; set; }
        public virtual Fleet Fleet { get; set; }
        public virtual ReceiptMaster ReceiptMaster { get; set; }
    }
}
