IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetFirstDataValue]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_GetFirstDataValue]
GO

CREATE PROCEDURE dbo.usp_GetFirstDataValue
-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <15-06-2015>
-- Description:	<Get first data details>
-- =============================================
--	S.Healey	2016-11-03		- T#6488 Additional paramater for @IncludeDatawire, to handle UK region.
(
	@deviceId VARCHAR(100) = NULL,
	@ResultType INT,
	@IncludeDatawire BIT = 1
)
AS 
BEGIN 
	IF (@ResultType = 1)
	BEGIN 
		IF (@IncludeDatawire = 1)
		BEGIN
			SELECT	tmnl.TerminalID, 
					tmnl.SerialNo, 
					tmnl.DeviceName, 
					tmnl.MacAdd, 
					tmnl.fk_MerchantID, 
					tmnl.IsTerminalAssigned, 
					tmnl.DatawireId, 
					fdMer.FDId,
					fdMer.FDMerchantID, 
					fdMer.GroupId, 
					fdMer.serviceId, 
					fdMer.ProjectId,
					fdMer.EcommProjectId,
					fdmer.ProjectType,
					fdMer.App, 
					fdMer.IsAssigned,
					fdMer.TokenType,
					fdMer.EncryptionKey,
					fdMer.MCCode, 
					dwr.ID, 
					dwr.DID, 
					dwr.DatewireXml, 
					dwr.MerchantId, 
					dwr.RCTerminalId, 
					dwr.Stan, 
					dwr.RefNumber
			FROM dbo.Terminal tmnl 
				INNER JOIN dbo.Merchant mer ON mer.MerchantID = tmnl.fk_MerchantID 
				INNER JOIN dbo.FDMerchant fdMer ON fdMer.FDId = mer.Fk_FDId 
				INNER JOIN dbo.Datawire dwr ON dwr.ID = tmnl.DatawireId 
			WHERE tmnl.MacAdd = @deviceId 
			  AND tmnl.IsActive = 1
			  AND mer.IsActive = 1
		END
		ELSE
		BEGIN
			SELECT	tmnl.TerminalID, 
					tmnl.SerialNo, 
					tmnl.DeviceName, 
					tmnl.MacAdd, 
					tmnl.fk_MerchantID, 
					tmnl.IsTerminalAssigned, 
					tmnl.DatawireId, 
					1 AS FDId,
					'' AS FDMerchantID,
					'' AS GroupId, 
					'' AS serviceId, 
					'' AS ProjectId,
					'' AS EcommProjectId, 
					CONVERT(TINYINT, 0) AS ProjectType,
					'' AS App, 
					CONVERT(BIT, 0) AS IsAssigned,
					'' AS TokenType,
					'' AS EncryptionKey,
					'' AS MCCode, 
					CONVERT(INT, 0) AS ID, 
					'' AS DID, 
					'' AS DatewireXml, 
					CONVERT(INT, 0) AS MerchantId, 
					'' AS RCTerminalId, 
					'' AS Stan, 
					'' AS RefNumber
			FROM dbo.Terminal tmnl 
				INNER JOIN dbo.Merchant mer ON mer.MerchantID = tmnl.fk_MerchantID 
			WHERE tmnl.MacAdd = @deviceId 
			  AND tmnl.IsActive = 1 
			  AND mer.IsActive = 1
		END
	END
	ELSE IF (@ResultType = 0)
	BEGIN 
		SELECT	tmnl.TerminalID, 
				tmnl.SerialNo, 
				tmnl.DeviceName, 
				tmnl.MacAdd, 
				tmnl.fk_MerchantID, 
				tmnl.IsTerminalAssigned, 
				tmnl.DatawireId, 
				fdMer.FDId,
				fdMer.FDMerchantID, 
				fdMer.GroupId, 
				fdMer.serviceId, 
				fdMer.ProjectId,
				fdMer.EcommProjectId,
				fdmer.ProjectType,
				fdMer.App, 
				fdMer.IsAssigned,
				fdMer.TokenType,
				fdMer.EncryptionKey,
				fdMer.MCCode, 
				dwr.ID, 
				dwr.DID, 
				dwr.DatewireXml, 
				dwr.MerchantId, 
				dwr.RCTerminalId, 
				dwr.Stan, 
				dwr.RefNumber
		FROM dbo.Terminal tmnl 
			INNER JOIN dbo.Merchant mer ON mer.MerchantID = tmnl.fk_MerchantID 
			INNER JOIN dbo.FDMerchant fdMer ON fdMer.FDId = mer.Fk_FDId 
			INNER JOIN dbo.Datawire dwr ON dwr.ID = tmnl.DatawireId 
		WHERE tmnl.MacAdd = '-420'
		  AND tmnl.IsActive = 1
		  AND mer.IsActive = 1
	END 
END
GO
