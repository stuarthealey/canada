IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CorpUserTotalTransByVehicle]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_CorpUserTotalTransByVehicle]
GO

CREATE PROCEDURE [dbo].[usp_CorpUserTotalTransByVehicle]
(
	@IsOnDemandReport BIT,
	@FromDate DATETIME = NULL,
	@ToDate DATETIME = NULL,
	@CorpUserId INT = NULL,
	@XmlData NVARCHAR(MAX) OUT,
	@ReportType INT OUT,
	@FDate DATE OUT,
	@TDate DATE OUT,
	@TotalTransaction INT OUT,
	@TotalActualFare NVARCHAR(30) OUT,
	@TotalTips NVARCHAR(30) OUT,
	@TotalFareAmount NVARCHAR(30) OUT,
	@TotalSurcharge NVARCHAR(30) OUT,
	@TotalFee NVARCHAR(30) OUT,
	@NoChargeBack INT OUT,
	@ChargeBackValue NVARCHAR(30) OUT,
	@NoVoid INT OUT,
	@VoidValue NVARCHAR(30) OUT,
	@TotalTechFee NVARCHAR(35) OUT,
	@AmountOwing  NVARCHAR(40) OUT,
	@Slab1Message NVARCHAR(40) OUT,
	@Slab2Message NVARCHAR(40) OUT,
	@Slab3Message NVARCHAR(40) OUT,
	@Slab4Message NVARCHAR(40) OUT,
	@TotalSlab1 INT OUT,
	@TotalSlab2 INT OUT,
	@TotalSlab3 INT OUT,
	@TotalSlab4 INT OUT,
	@NextScheduleDateTime DATETIME OUT
)
AS
BEGIN
	BEGIN TRY
	  SET NOCOUNT ON;
	  SET ANSI_WARNINGS OFF

	  DECLARE @Frequency INT,
			  @FrequencyType INT,
			  @DateFrom DATE,
			  @DateTo DATE,
			  @ScheduleId INT,
			  @TransCountSlablt1 INT,
			  @TransCountSlabGt2 INT,
			  @TransCountSlablt2 INT,
			  @TransCountSlabGt3 INT,
			  @TransCountSlablt3 INT,
			  @TransCountSlabGt4 INT,
			  @ParentCorporateUserId INT

	  DECLARE @InnerTable TABLE
	   (
		 Id INT, 
		 FareValue MONEY,
		 Tip MONEY, 
		 Amount MONEY, 
		 Surcharge MONEY, 
		 Fee MONEY, 
		 FleetID INT, 
		 TxnType VARCHAR(15),
		 ResponseCode VARCHAR(25),
		 fk_FleetId INT,
		 IsRefunded BIT,
		 IsVoided BIT,
		 IsCompleted BIT,
		 TechFee MONEY,
		 FleetFee MONEY,
		 VehicleNo NVARCHAR(50)
	   )  

	  DECLARE @OuterTable TABLE
	   (
		  Id  INT, 
		  FareValue MONEY, 
		  Tip MONEY,Amount MONEY,
		  Surcharge MONEY, 
		  Fee MONEY, 
		  TxnType VARCHAR(15),
		  ResponseCode NVARCHAR(25),
		  fk_FleetId INT,
		  TechFee MONEY,
		  IsRefunded BIT,
		  IsVoided BIT,
		  IsCompleted BIT,
		  FleetFee MONEY
	   )   

	   IF EXISTS(SELECT 1 FROM Users WHERE UserID=@CorpUserId AND ParentUserID IS NULL AND IsActive=1)
	   BEGIN
		SET @ParentCorporateUserId=@CorpUserId	
	   END
	   ELSE
	   BEGIN
		 SELECT @ParentCorporateUserId=ParentUserID from Users WHERE ParentUserID IS NOT NULL AND IsActive=1
	   END
	   SELECT @ScheduleId=ScheduleID,
			  @Frequency=Frequency,
			  @FrequencyType=FrequencyType,
			  @NextScheduleDateTime=NextScheduleDateTime 
	   FROM ScheduleReport 
	   WHERE fk_ReportId=7 AND IsCreated = 1 AND fk_MerchantId IS NULL AND CorporateUserId=@ParentCorporateUserId
	   IF(@IsOnDemandReport=1)
		  BEGIN
			SET @DateFrom=@FromDate
			SET @DateTo=@ToDate
		  END
		ELSE
		 BEGIN
		  IF(@Frequency=1)
		  BEGIN
			SET @DateFrom=GETDATE()-@FrequencyType
			SET @DateTo=GETDATE()
			SET @NextScheduleDateTime=@NextScheduleDateTime+@FrequencyType
		  END
		  ELSE IF(@Frequency=2)
		  BEGIN
			SET @DateFrom = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-@FrequencyType,GETDATE())), 0)-1
			SET @DateTo= DATEADD(WEEK, DATEDIFF(WEEK, 0, GETDATE()), 0)-1
			SET @NextScheduleDateTime=DATEADD(WK,@FrequencyType,@NextScheduleDateTime)
		  END
		  ELSE IF(@Frequency=3)
		  BEGIN
			SET @DateFrom =DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH,-@FrequencyType,GETDATE())), 0)
			SET @DateTo=DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) 
			SET @NextScheduleDateTime=DATEADD(MONTH,@FrequencyType,@NextScheduleDateTime)
		  END 
		  ELSE
		  BEGIN
		   RETURN 1
		  END 
		 END  

	   --insert data into table variable InnerTable for reuse*/                         
	   INSERT INTO @InnerTable(Id,FareValue,Tip,Amount,Surcharge,Fee,FleetID,TxnType,ResponseCode,IsRefunded,IsVoided,IsCompleted,TechFee,FleetFee,VehicleNo)                   
	   SELECT  mt.Id
			  ,mt.FareValue
			  ,mt.Tip
			  ,mt.Amount
			  ,mt.Surcharge
			  ,mt.Fee
			  ,fl.FleetID
			  ,mt.TxnType
			  ,mt.ResponseCode
			  ,mt.IsRefunded
			  ,mt.IsVoided
			  ,mt.IsCompleted
			  ,mt.TechFee
			  ,mt.FleetFee
			  ,mt.VehicleNo		
	   FROM MTDTransaction mt 
	   INNER JOIN Vehicle vh ON mt.VehicleNo=vh.VehicleNumber 
	   INNER JOIN Fleet fl ON vh.fk_FleetID = fl.FleetID
	   WHERE CAST(mt.TxnDate AS DATE)>=@DateFrom 
			 AND CAST(mt.TxnDate AS DATE)<@DateTo 
			 AND mt.fk_FleetId=vh.fk_FleetID
			 and mt.fk_FleetId in (select distinct fk_FleetID from UserFleet where fk_UserTypeId=5 and IsActive=1 and fk_UserID=@CorpUserId)

	   --insert data into table variable OuterTable for reuse*/    
	   INSERT INTO @OuterTable(Id,FareValue,Tip,Amount,Surcharge,Fee,TxnType,ResponseCode,fk_FleetId,TechFee,IsRefunded,IsVoided,IsCompleted,FleetFee) 
	   SELECT  t.Id
			  ,t.FareValue
			  ,t.Tip,t.Amount
			  ,t.Surcharge
			  ,t.Fee
			  ,t.TxnType
			  ,t.ResponseCode
			  ,t.fk_FleetId
			  ,t.TechFee
			  ,t.IsRefunded
			  ,t.IsVoided
			  ,t.IsCompleted
			  ,t.FleetFee
	   FROM  MTDTransaction t 
	   WHERE CAST(t.TxnDate AS DATE)>=@DateFrom AND
			 CAST(t.TxnDate AS DATE)<@DateTo    
			and t.fk_FleetId in (select distinct fk_FleetID from UserFleet where fk_UserTypeId=5 and IsActive=1 and fk_UserID=@CorpUserId)   

	   SELECT @TransCountSlablt1 = TransCountSlablt1,
			  @TransCountSlabGt2 = TransCountSlabGt2,
			  @TransCountSlablt2 = TransCountSlablt2,
			  @TransCountSlabGt3 = TransCountSlabGt3,
			  @TransCountSlablt3 = TransCountSlablt3,
			  @TransCountSlabGt4 = TransCountSlabGt4
	   FROM TransactionCountSlab 
	   WHERE fk_CorporateUserId = @CorpUserId

	   SELECT COUNT(Id) AS 'Id', VehicleNo AS'VehicleNo', FleetID AS 'fk_FleetId' INTO #temp 
		FROM @InnerTable it WHERE it.ResponseCode IN ('000', '00', '002')
		GROUP BY it.VehicleNo,it.FleetID
										 				   
	   SET @XmlData = CAST((SELECT  ROW_NUMBER() OVER (ORDER BY Company) AS 'td', ''
								   ,COALESCE(mer.Company, '') AS 'td', ''
								   ,COALESCE(f.FleetName, '')  AS 'td', ''
								   ,(SELECT COALESCE(COUNT(id),0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND it.TxnType IN ('Sale', 'Completion') AND it.IsRefunded = 0 AND it.IsVoided = 0 AND it.ResponseCode IN ('000', '00', '002')) AS 'td',''
								   ,CONCAT('$', (SELECT COALESCE(SUM(FareValue), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID  AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td',''
								   ,CONCAT('$', (SELECT COALESCE(SUM(Tip), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND  it.ResponseCode IN ('000', '00', '002'))) AS 'td',''
								   ,CONCAT('$', (SELECT COALESCE(SUM(Surcharge), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID  AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td',''
								   ,CONCAT('$', (SELECT COALESCE(SUM(TechFee), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td',''
								   ,(SELECT COALESCE(COUNT(id),0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND it.TxnType = 'Refund' AND it.ResponseCode = '000') AS 'td',''
								   ,CONCAT('$', (SELECT COALESCE(SUM(Amount), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND it.TxnType = 'Refund' AND it.ResponseCode  IN ('000', '00'))) AS 'td',''
								   ,(SELECT COALESCE(COUNT(id),0) FROM @InnerTable it WHERE  it.FleetID = f.FleetID AND it.TxnType = 'Void' AND it.ResponseCode = '000') AS 'td',''
								   ,CONCAT('$', (SELECT COALESCE(SUM(Amount), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND it.TxnType = 'Void' AND it.ResponseCode = '000')) AS 'td',''
								   ,CONCAT('$', (SELECT COALESCE(SUM(Amount), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND it.TxnType IN ('Sale', 'Completion') AND it.IsRefunded = 0 AND IT.IsVoided = 0 AND it.ResponseCode IN ('000', '00', '002'))) AS 'td',''
								   ,CONCAT('$', (SELECT COALESCE(SUM(it.Fee), 0) + COALESCE(SUM(it.FleetFee), '0.00') FROM @InnerTable it WHERE  it.FleetID = f.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td',''
								   ,CONCAT('$', (SELECT COALESCE(SUM(Amount), 0) FROM @InnerTable it WHERE  it.FleetID = f.FleetID AND it.TxnType IN ('Sale', 'Completion') AND it.IsRefunded = 0 AND IT.IsVoided = 0 AND it.ResponseCode IN ('000', '00', '002'))-
								   (SELECT COALESCE(SUM(Fee),0) + COALESCE(SUM(FleetFee), 0) FROM @InnerTable it WHERE  it.FleetID = f.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td',''
								  ,(SELECT COALESCE(COUNT(t.VehicleNo), 0) FROM #temp t WHERE t.fk_FleetID = f.FleetID AND t.Id < @TransCountSlablt1) AS 'td',''
								  ,(SELECT COALESCE(COUNT(t.VehicleNo), 0) FROM #temp t WHERE t.fk_FleetID = f.FleetID AND t.Id >= @TransCountSlabGt2 AND t.Id < @TransCountSlablt2) AS 'td',''
								  ,(SELECT COALESCE(COUNT(t.VehicleNo), 0) FROM #temp t WHERE t.fk_FleetID = f.FleetID AND t.Id >= @TransCountSlabGt3 AND t.Id < @TransCountSlablt3) AS 'td',''
								  ,(SELECT COALESCE(COUNT(t.VehicleNo), 0) FROM #temp t WHERE t.fk_FleetID = f.FleetID AND t.Id >= @TransCountSlabGt4) AS 'td'
							FROM MTDTransaction m 
							INNER JOIN  Vehicle v ON m.VehicleNo=v.VehicleNumber 
							INNER JOIN fleet f ON v.fk_FleetID = f.FleetID 
							INNER JOIN merchant mer ON f.fk_MerchantID=mer.MerchantID 
							WHERE  m.ResponseCode IN ('000', '00', '002')   
								   AND CAST(m.TxnDate AS DATE)>=@DateFrom
								   AND CAST(m.TxnDate AS DATE)<@DateTo    
								   AND m.fk_FleetId=v.fk_FleetID
								   AND m.Id NOT IN (SELECT Id FROM dbo.MTDTransaction WHERE m.TxnType = 'Authorization' AND m.IsCompleted = 0)
								   and m.fk_FleetId  in (select distinct fk_FleetID from UserFleet where fk_UserTypeId=5 and IsActive=1 and fk_UserID=@CorpUserId)
							GROUP BY f.FleetName,f.FleetID,mer.Company 
							ORDER BY mer.Company 
							FOR XML PATH('tr'), ELEMENTS ) AS NVARCHAR(MAX))

	   SELECT @TotalTransaction=COALESCE(COUNT(Id),0) 
	   FROM @OuterTable ot 
	   WHERE ot.TxnType IN ('Sale', 'Completion') 
			 AND ot.ResponseCode IN ('000', '00', '002')
			 AND ot.IsRefunded = 0 AND ot.IsVoided = 0

	   SELECT @TotalActualFare=CONCAT('$',CAST(COALESCE(SUM(ot.FareValue),0) AS DECIMAL(18,2)))
			 ,@TotalTips=CONCAT('$',CAST(COALESCE(SUM(ot.Tip),0) AS DECIMAL(18,2)))
			 ,@TotalSurcharge=CONCAT('$',CAST(COALESCE(SUM(ot.Surcharge),0) AS DECIMAL(18,2)))
			 ,@TotalFee=CAST(COALESCE(SUM(ot.Fee),0)+COALESCE(SUM(ot.FleetFee),0) AS DECIMAL(18,2))
			 ,@TotalTechFee=CAST(COALESCE(SUM(ot.TechFee),0) AS DECIMAL(16,2))	
	   FROM  @OuterTable ot 
	   WHERE ot.ResponseCode IN ('000', '00', '002') 
		AND ((ot.TxnType = 'Sale' AND ot.IsRefunded = 0 AND ot.IsVoided = 0) 
		OR (ot.TxnType = 'Authorization' AND ot.IsCompleted = 1)) 

	   SELECT  @TotalFareAmount = CONCAT('$', CAST(COALESCE(SUM(ot.Amount), 0) AS DECIMAL(18, 2))),
			   @AmountOwing = CONCAT('$', CAST(COALESCE(SUM(ot.Amount), 0) AS DECIMAL(18,2)) - CAST(@TotalFee AS DECIMAL(18, 2)))
	   FROM    @OuterTable ot
	  WHERE  ot.ResponseCode IN ('000', '00', '002') AND ot.TxnType IN ('Sale', 'Completion') AND ot.IsRefunded = 0 AND ot.IsVoided = 0 

	   SELECT @NoChargeBack=COALESCE(COUNT(ot.Id),0)
			 ,@ChargeBackValue=CONCAT('$',CAST(COALESCE(SUM(ot.Amount),0) AS DECIMAL(18,2))) 
	   FROM   @OuterTable ot 
	   WHERE ot.TxnType ='Refund' AND ot.ResponseCode IN ('000', '00')

	   SELECT @NoVoid=COALESCE(COUNT(Id),0)
			 ,@VoidValue=CONCAT('$',CAST(COALESCE(SUM(Amount),0) AS DECIMAL(18,2))) 
	   FROM   @OuterTable ot 
	   WHERE ot.TxnType ='Void' AND ot.ResponseCode IN ('000', '00')

	   SET @ReportType = @Frequency 
	   SET @FDate = @DateFrom
	   SET @TDate = DATEADD(day, -1, @DateTo)
	   SET @TotalTechFee =CONCAT('$', @TotalTechFee)
	   SET @Slab1Message = CONCAT('Trans less than ', @TransCountSlablt1)
	   SET @Slab2Message = CONCAT('Trans between ', @TransCountSlabGt2, ' and ', @TransCountSlablt2) 
	   SET @Slab3Message = CONCAT('Trans between ', @TransCountSlabGt3, ' and ', @TransCountSlablt3) 
	   SET @Slab4Message = CONCAT('Trans greater than ', @TransCountSlabGt4)
	   SET @TotalFee = CONCAT('$', @TotalFee)

	   SELECT @TotalSlab1=COUNT(t.VehicleNo) FROM #temp t WHERE  t.Id < @TransCountSlablt1
	   SELECT @TotalSlab2=COUNT(t.VehicleNo) FROM #temp t WHERE t.Id >= @TransCountSlabGt2 AND t.Id < @TransCountSlablt2
	   SELECT @TotalSlab3=COUNT(t.VehicleNo) FROM #temp t WHERE t.Id >= @TransCountSlabGt3 AND t.Id < @TransCountSlablt3
	   SELECT @TotalSlab4=COUNT(t.VehicleNo) FROM #temp t WHERE t.Id >= @TransCountSlabGt4

	   DROP TABLE #temp
	END TRY
	BEGIN CATCH
		  INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
		  SELECT ERROR_NUMBER()
				,ERROR_SEVERITY()
				,ERROR_STATE()
				,ERROR_PROCEDURE()
				,ERROR_LINE()
				,ERROR_MESSAGE()
				,SUSER_SNAME()
				,HOST_NAME()
				,GETDATE()
	END CATCH
END
GO