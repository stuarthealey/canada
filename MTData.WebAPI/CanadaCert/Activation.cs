﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTData.WebAPI.CanadaCert
{
    /// <summary>
    /// Class for merchant activation
    /// </summary>
    public class Activation
    {
        public string ServiceID { get; set; }
    }
}
