﻿using MTD.Core.DataTransferObjects;
using MTD.Core.Factory.Interface;
using MTD.Data.Data;

namespace MTD.Core.Factory
{
    public class FleetFactory : IFleetFactory
    {
        public Fleet CreateFleet(FleetDto fleetDto)
        {
            Fleet fleet = new Fleet
            {
               FleetID = fleetDto.FleetID,
               fk_MerchantID = fleetDto.fk_MerchantID,
               FleetName = fleetDto.FleetName,
               Description = fleetDto.Description,
               IsActive = fleetDto.IsActive,
               CreatedBy = fleetDto.CreatedBy,
               CreatedDate = fleetDto.CreatedDate,
               ModifiedBy = fleetDto.ModifiedBy,
               ModifiedDate = fleetDto.ModifiedDate,
               fK_CurrencyCodeID = fleetDto.fK_CurrencyCodeID,
               FleetFee = fleetDto.FleetFee,
               PayType = fleetDto.PayType,
               FleetFeeType = fleetDto.FleetFeeType,
               FleetFeeFixed = fleetDto.FleetFeeFixed,
               FleetFeePer = fleetDto.FleetFeePer,
               FleetFeeMaxCap = fleetDto.FleetFeeMaxCap,
               DispatchFleetID = fleetDto.DispatchFleetID,
               UserName = fleetDto.UserName,
               PCode = fleetDto.PCode,
               URL = fleetDto.URL,
               BookingChannel = fleetDto.BookingChannel
            };
            return fleet;
        }
    }
}
