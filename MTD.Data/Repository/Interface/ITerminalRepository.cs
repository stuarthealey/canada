﻿using System.Collections.Generic;

using MTD.Data.Data;

namespace MTD.Data.Repository.Interface
{
    public interface ITerminalRepository
    {
        bool Add(Terminal terminal, string termId);
        IEnumerable<Terminal> TerminalList(int merchantId);
        void Update(Terminal terminal, int terminalId, string termId);
        Terminal GetTerminal(int terminalId);
        Terminal GetTerminal(string deviceId);
        string DeleteTerminal(int terminalId, string modifiedBy);
        IEnumerable<Terminal> TerminalListByFiter(FilterSearchParameters searchParameters);
        Datawire GetTerminalStanRef(int merchantId, string serialNumber);
        bool IsRegistered(int merchantId, string serialNumber);
        MTDTransaction Completion(string transId, string serialNo, int merchantId);
        void UpdateStan(string stan, string transRefNo, string serialNo, int merchantId);
        bool IsValid(string serialNumber);
        bool IsDatawireRegistered(string serialNumber, string terminalId);
        bool IsRcExist(string rcTerminalId);
        void AddRcTerminalId(Datawire datawire);
        string GetRcTerminalId(int terminalId);
        IEnumerable<PaymentTerminal> GetDeviceList();
        PaymentTerminal GetDevice(int deviceId);
        bool AddDevicename(PaymentTerminal deviceName);
        void UpdateDevicename(PaymentTerminal deviceName, int deviceId);
        bool DeleteDevicename(int deviceId);
        IEnumerable<PaymentTerminal> DeviceListByFiter(string name, int index, int pageSize, string sorting);

        bool IsMacAddressRegistered(string macAddress);
        bool PaymentDeviceIsExist(string deviceName);
        bool RcTerminalIdIsExist(string rcTerminalId, int merchantId);
        void AddTermialDevice(TerminalExcelUpload objTerminal);
        IEnumerable<Terminal> GetUserTerminal(FilterSearchParameters filterSearch);
        int IsValidTerminalExcel(TerminalExcelUpload terminal);
        int GetUserTerminalCount(FilterSearchParameters filterSearch);
        int GetTerminalCount(FilterSearchParameters fsParametes);
        int GetDeviceListCount(string name);
        bool IsMacAddReg(int terminalId, string macAdd);
        bool UpdateCapVersion(string macAdd);
        void SaveServiceDiscoveryUrls(ServiceDiscoveryParametersData serviceData);
        List<string> GetServiceDiscoveryUrls();
        Datawire GetRandomMerchantDetail();
        FDMerchant GetFDMerchant(int merchantId);
        List<string> SaveServiceProviderUrls(ServiceProviderUrlData serviceProviderUrlData);
        bool IsClsCapKey(int terminalId);


    }
}
