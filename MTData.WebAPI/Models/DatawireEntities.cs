﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace MTData.WebAPI.Models
{

    [Serializable()]
    public class DatawireEntities
    {
        private Collection<string> discoveryUrls;
        private Collection<DatawireServiceUrls> serviceProviderUrls;
        public DatawireEntities()
        {
            discoveryUrls = new Collection<string>();
            serviceProviderUrls = new Collection<DatawireServiceUrls>();
        }

        public string TerminalIdentifier
        {
            get;
            set;
        }

        public string DID
        {
            get;
            set;
        }

        public string AppID
        {
            get;
            set;
        }

        public string MID
        {
            get;
            set;
        }

        public string ServiceId
        {
            get;
            set;
        }

        public string TID
        {
            get;
            set;
        }

        public string AuthID
        {
            get;
            set;
        }

        [XmlArray]
        [XmlArrayItem(ElementName = "Url")]

        public Collection<string> DiscoveryUrls
        {
            get { return discoveryUrls; }
            set { ;}
        }

        public Collection<DatawireServiceUrls> ServiceProviderUrls
        {
            get { return serviceProviderUrls; }
        }

        public string ClientRefId
        {
            get;
            set;
        }

        public string ActiveUrl
        {
            get;
            set;
        }

        public int TransactionCount
        {
            get;
            set;
        }

        public DateTime TransactionTime
        {
            get;
            set;
        }

        public int MaxNumberOfTransaction
        {
            get;
            set;
        }

        public void AssignServiceUrl(Collection<DatawireServiceUrls> item)
        {
            if (item != null)
                serviceProviderUrls = item;
        }

        public void AssignDiscoveryUrls(Collection<string> item)
        {
            if (item != null)
                discoveryUrls = item;
        }

    }

}
