﻿using System;
using System.Net.Http;
using System.Text;
using System.Xml;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.WebAPI.Models;
using MTData.WebAPI.Resources;
using MTData.WebAPI.Controllers;

namespace MTData.WebAPI
{
    public class DatawireRegistration : TransactionRequestController
    {
        //zzz Chetu 357 : private readonly ITerminalService _terminalService;
        private static ITerminalService _terminalService;
        private readonly IMerchantService _merchantService;
        private StringBuilder _logMessage;

        public DatawireRegistration(
            [Named("TerminalService")] ITerminalService terminalService,
            [Named("MerchantService")] IMerchantService merchantService,
            StringBuilder logMessage) : base(terminalService, logMessage)
        {
            _terminalService = terminalService;
            _merchantService = merchantService;
            _logMessage = logMessage;
        }

        /// <summary>
        /// Purpose             :   Datwire registration method for terminal registration from web portal
        /// Function Name       :   DatawireRegistration
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   04/14/2015
        /// Modificatios Made   :   Moved in separate class.
        /// Modidified On       :   2015-08-14 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public object Register(HttpRequestMessage request, DatawireResponse res, fDMerchantDto _fdMerchantobj, ILogger _logger)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();

                _logMessage.AppendLine("Loading request xml");

                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                _logMessage.AppendLine("Creating transaction request instance");

                TransactionRequest tr = new TransactionRequest
                {
                    MtdMerchantId = Convert.ToInt32(xmlDoc.SelectSingleNode("DatawireRequest/MerchantId").InnerText),
                    TerminalId = xmlDoc.SelectSingleNode("DatawireRequest/TerminalId").InnerText
                };

                _logMessage.AppendLine("Executing _merchantService.GetFDMerchant(Convert.ToInt32(tr.MtdMerchantId));");

                _fdMerchantobj = _merchantService.GetFDMerchant(Convert.ToInt32(tr.MtdMerchantId));

                if (_fdMerchantobj != null)
                {
                    if (String.IsNullOrEmpty(_fdMerchantobj.ProjectId))
                        _fdMerchantobj.ProjectId = _fdMerchantobj.EcommProjectId;
                }               

                _logMessage.AppendLine("Sending registration request to Rappid Connect");

                res = SendRegistrationRequest(_fdMerchantobj, tr.TerminalId.FormatTerminalId());
                
                _logMessage.AppendLine("SendRegistrationRequest method for datawire registration executed successfully");
                _logger.LogInfoMessage(_logMessage.ToString());

                return res;
            }
            catch (Exception ex)
            {
                res.Status = PaymentAPIResources.Failed;
                res.Value = ex.Message;
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return res;
            }
        }

        /// <summary>
        /// Purpose             :   Datwire registration method for terminal registration from web portal
        /// Function Name       :   DatawireRegistration
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   04/14/2015
        /// Modificatios Made   :   Moved in separate class.
        /// Modidified On       :   2015-08-14 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public string RegisterCN(HttpRequestMessage request, DatawireResponse res, fDMerchantDto _fdMerchantobj, ILogger _logger)
        {
            string register = string.Empty;
            try
            {
                XmlDocument xmlDoc = new XmlDocument();

                _logMessage.AppendLine("Loading request xml");
                
                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                _logMessage.AppendLine("Creating transaction request instance");

                TransactionRequest tr = new TransactionRequest
                {
                    MtdMerchantId = Convert.ToInt32(xmlDoc.SelectSingleNode("DatawireRequest/MerchantId").InnerText),
                    TerminalId = xmlDoc.SelectSingleNode("DatawireRequest/TerminalId").InnerText
                };

                _logMessage.AppendLine(string.Format("Executing _merchantService.GetFDMerchant({0});", tr.MtdMerchantId));

                _fdMerchantobj = _merchantService.GetFDMerchant(Convert.ToInt32(tr.MtdMerchantId));
                if (_fdMerchantobj != null)
                {
                    if (String.IsNullOrEmpty(_fdMerchantobj.ProjectId))
                        _fdMerchantobj.ProjectId = _fdMerchantobj.EcommProjectId;
                }

                _logMessage.AppendLine("Sending registration request");

                register = SendRegistrationRequestCN(_fdMerchantobj, tr.TerminalId.FormatTerminalId(),tr.MtdMerchantId);

                _logMessage.AppendLine(string.Format("SendRegistrationRequest method for datawire registration executed. Result:{0};", register));
                _logger.LogInfoMessage(_logMessage.ToString());

                return register;
            }
            catch (Exception ex)
            {
                res.Status = PaymentAPIResources.Failed;
                res.Value = ex.Message;
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return register;
            }
        }

        /// <summary>
        /// Purpose             :   service registration
        /// Function Name       :   ServiceRegister
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   04/4/2015
        /// Modificatios Made   :  **********
        /// Modidified On       :   ********
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        //zzz Chetu 357 : public string ServiceRegister(HttpRequestMessage request, DatawireResponse res, ILogger _logger)
        public string ServiceRegister(HttpRequestMessage request, DatawireResponse res, fDMerchantDto _fdMerchantobj, ILogger _logger)
        {
            string service = string.Empty;

            try
            {
                XmlDocument xmlDoc = new XmlDocument();

                _logMessage.AppendLine("Loading request xml");

                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                _logMessage.AppendLine("Sending service request ");

                service = ServiceDiscovery();

                _logMessage.AppendLine("SendRegistrationRequest method for datawire service executed successfully");
                _logger.LogInfoMessage(_logMessage.ToString());

                return service;
            }
            catch (Exception ex)
            {
                res.Status = PaymentAPIResources.Failed;
                res.Value = ex.Message;

                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                return service;
            }
        }

        /// <summary>
        /// Purpose             :   Ping registration
        /// Function Name       :   Pingregister
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   04/4/2016
        /// Modificatios Made   :   *********
        /// Modidified On       :  *************
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public string Pingregister(HttpRequestMessage request, DatawireResponse res, fDMerchantDto _fdMerchantobj, ILogger _logger)
        {
            string ping = string.Empty;

            try
            {
                XmlDocument xmlDoc = new XmlDocument();

                _logMessage.AppendLine("Loading request xml");

                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                _logMessage.AppendLine("Creating transaction request instance");

                TransactionRequest tr = new TransactionRequest
                {
                    MtdMerchantId = Convert.ToInt32(xmlDoc.SelectSingleNode("DatawireRequest/MerchantId").InnerText),
                    TerminalId = xmlDoc.SelectSingleNode("DatawireRequest/TerminalId").InnerText,
                    Did = xmlDoc.SelectSingleNode("DatawireRequest/DID").InnerText,
                    ServiceUrl = xmlDoc.SelectSingleNode("DatawireRequest/Url").InnerText
                };

                _logMessage.AppendLine("Executing _merchantService.GetFDMerchant(Convert.ToInt32(tr.MtdMerchantId));");

                _fdMerchantobj = _merchantService.GetFDMerchant(Convert.ToInt32(tr.MtdMerchantId));
                if (_fdMerchantobj != null)
                {
                    if (String.IsNullOrEmpty(_fdMerchantobj.ProjectId))
                        _fdMerchantobj.ProjectId = _fdMerchantobj.EcommProjectId;
                }

                _logMessage.AppendLine("Sending registration request");

                string pingurl = tr.ServiceUrl;
                ping = SendPingRequest(pingurl, _fdMerchantobj, tr.TerminalId.FormatTerminalId(),tr.Did);

                _logMessage.AppendLine("SendRegistrationRequest method for datawire ping executed successfully");
                _logger.LogInfoMessage(_logMessage.ToString());

                return ping;
            }
            catch (Exception ex)
            {
                res.Status = PaymentAPIResources.Failed;
                res.Value = ex.Message;

                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                return ping;
            }
        }
    }

}
