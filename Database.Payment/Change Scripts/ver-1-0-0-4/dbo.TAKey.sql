IF OBJECT_ID('dbo.TAKey', 'U') IS NULL
BEGIN
	CREATE TABLE [dbo].[TAKey](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[DownloadDate] [datetime] NULL,
		[TAFileVersion] [decimal](6, 1) NULL CONSTRAINT [DF_TAFileVersion]  DEFAULT ('0'),
		[IsNew] [bit] NULL
	) ON [PRIMARY]
END
GO


