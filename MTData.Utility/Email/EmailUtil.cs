﻿using System;
using System.Net.Mail;

namespace MTData.Utility
{
    /// <summary>
    /// contains email sending logic
    /// </summary>
    public class EmailUtil : IDisposable
    {
        readonly string _userName;
        bool IsMailSent = false;

        /// <summary>
        /// Constructor Used initialize member variable of class
        /// </summary>
        public EmailUtil()
        {
            _userName = System.Configuration.ConfigurationManager.AppSettings["SmtpUsername"];
        }

        /// <summary>
        /// <summary>
        /// Purpose : contains email sending logic
        /// Function Name : SendMail
        /// Created By : Pankaj Singh
        /// Created On : 04/29/2011 "MM/DD/YYYY"
        /// Modificatios Made : ************************
        /// Modidified On : ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// </summary>
        /// <param name="emailId">Email ID</param>
        /// <param name="mailDetails">Message Body</param>
        /// <param name="displayName"></param>
        /// <param name="subject">Subject</param>
        /// <returns></returns>
        public bool SendMail(string emailId, string mailDetails, string displayName, string subject)
        {
            try
            {
                using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient())
                {
                    using (MailMessage message = new MailMessage())
                    {
                        AlternateView avHtml = AlternateView.CreateAlternateViewFromString(mailDetails, null, "text/html");
                        message.From = new MailAddress(_userName, displayName);
                        message.To.Add(new MailAddress(emailId));
                        message.Subject = subject;
                        message.AlternateViews.Add(avHtml);

                        smtp.Send(message);

                        IsMailSent = true;

                        return IsMailSent;
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public void Dispose()
        {
            IsMailSent = false;
        }
    }
}
