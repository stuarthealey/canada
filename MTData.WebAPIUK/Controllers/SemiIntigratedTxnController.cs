﻿using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Web;
using System.Web.Configuration;
using System.Net;
using System.Security;
using System.Text;
using System.Web.Http;
using System.Net.Http;
using System;
using System.Xml;
using MTData.Utility;
using MTData.WebAPIUK.Common;
using MTData.WebAPIUK.Resources;
using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.WebAPIUK.Models;
using Ninject;


namespace MTData.WebAPIUK.Controllers
{
    public class SemiIntigratedTxnController : BaseController
    {
        private readonly ITransactionServiceUK _transactionService;
        private LocalVariables objLocalVars = new LocalVariables();
        private readonly StringBuilder _logMessage;
        private StringBuilder _logTracking;
        readonly ILogger _logger = new Logger();

        public SemiIntigratedTxnController(
            [Named("TxnServiceUk")] ITransactionServiceUK transactionService, StringBuilder logMessage, StringBuilder logTracking)
            : base(transactionService, logMessage, logTracking)
        {
            _transactionService = transactionService;
            _logMessage = logMessage;
            _logTracking = logTracking;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        public IHttpActionResult SaveTxnRequestResponse(HttpRequestMessage request)
        {
            MtdtxnSemiUKDto semiIntigratedTxn = new MtdtxnSemiUKDto();
            PaymentResponse response = new PaymentResponse();
            try
            {
                XmlDocument xmlDocumentRequest = new XmlDocument();
                xmlDocumentRequest.Load(request.Content.ReadAsStreamAsync().Result);
                semiIntigratedTxn = SemiIntigratedTxn(xmlDocumentRequest);
                int id = _transactionService.AddSemiTxn(semiIntigratedTxn);

                response.Header = CommonFunctions.FailedResponse("Transaction Id: " + id, objLocalVars._deviceId, objLocalVars._requestId, status: "Success");
                return Ok(response);

            }
            catch (Exception ex)
            {
                response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.Failed, objLocalVars._deviceId, objLocalVars._requestId, status: "Failed");
                return Ok(response);
            }
            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDocumentRequest"></param>
        /// <param name="xmlDocumentResponse"></param>
        /// <returns></returns>
        private MtdtxnSemiUKDto SemiIntigratedTxn(XmlDocument xmlDocumentRequest)
        {
            var selectSingleNode = xmlDocumentRequest.SelectSingleNode("PaymentRequest");
            if (selectSingleNode.IsNull())
            {
                //return Ok(logOutResponse);
            }
            MtdtxnSemiUKDto semiDto = new MtdtxnSemiUKDto();
            semiDto.MessageNumber = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/MessageNumber").InnerText;
            semiDto.TransactionStatus = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/TransactionStatus").InnerText;
            semiDto.EntryMethod = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/EntryMethod").InnerText;
            semiDto.ReceiptNumber = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/ReceiptNumber").InnerText;
            semiDto.AcquirerMerchantID = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/AcquirerMerchantID").InnerText;
            semiDto.DateTime = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/DateTime").InnerText;
            semiDto.Currency = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/Currency").InnerText;
            semiDto.CardSchemeName = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/CardSchemeName").InnerText;
            semiDto.PAN = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/PAN").InnerText;
            semiDto.ExpiryDate = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/ExpiryDate").InnerText;
            semiDto.GemsReceiptID = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/GemsReceiptID").InnerText;
            semiDto.AuthorizationCode = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/AuthorizationCode").InnerText;
            semiDto.AcquirerResponseCode = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/AcquirerResponseCode").InnerText;
            semiDto.Reference = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/Reference").InnerText;
            semiDto.MerchantName = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/MerchantName").InnerText;
            semiDto.MerchantAddress1 = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/MerchantAddress1").InnerText;
            semiDto.MerchantAddress2 = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/MerchantAddress2").InnerText;
            semiDto.AID = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/AID").InnerText;
            semiDto.PANSequenceNum = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/PANSequenceNum").InnerText;
            semiDto.StartDate = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/StartDate").InnerText;
            semiDto.TerminalIdentity = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/TerminalIdentity").InnerText;
            semiDto.TransactionAmount = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/TransactionAmount").InnerText);
            semiDto.IsDCCTxn = Convert.ToBoolean(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/IsDCCTxn").InnerText);
            semiDto.IsLoyaltyTxn = Convert.ToBoolean(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/IsLoyaltyTxn").InnerText);
            semiDto.DCCAmount = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/DCCAmount").InnerText);
            semiDto.DonationAmount = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/DonationAmount").InnerText);
            semiDto.RedeemedAmount = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/RedeemedAmount").InnerText);
            semiDto.DCCCurrency = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/DCCCurrency").InnerText;
            semiDto.FXRateApplied = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/FXRateApplied").InnerText;
            semiDto.FXExponentApplied = Convert.ToBoolean(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/FXExponentApplied").InnerText);
            semiDto.DCCCurrencyExponent = Convert.ToInt32(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/DCCCurrencyExponent").InnerText);
            semiDto.MessageHost = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/MessageHost").InnerText;
            semiDto.TransactionType = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/TransactionType").InnerText;
            semiDto.GratuityAmount = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/GratuityAmount").InnerText);
            semiDto.CashAmount = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/CashAmount").InnerText);
            semiDto.TotalTransactionAmount = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/TotalTransactionAmount").InnerText);
            semiDto.ICCApplicationFileName = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/ICCApplicationFileName").InnerText;
            semiDto.ICCApplicationPreferredName = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/ICCApplicationPreferredName").InnerText;
            semiDto.eCardVerificationMethod = Convert.ToInt32(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/eCardVerificationMethod").InnerText);
            semiDto.TransactionID = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/TransactionID").InnerText;
            semiDto.RequestXml = xmlDocumentRequest.ToString();
            return semiDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult SaveSattlementTxn(HttpRequestMessage request)
        {
            MtdtxnSattlementUKDto semiIntigratedTxn = new MtdtxnSattlementUKDto();
            PaymentResponse response = new PaymentResponse();
            try
            {
                XmlDocument xmlDocumentRequest = new XmlDocument();
                xmlDocumentRequest.Load(request.Content.ReadAsStreamAsync().Result);
                semiIntigratedTxn = SemiIntigratedSattlementTxn(xmlDocumentRequest);
                int id = _transactionService.AddSemiSattlementTxn(semiIntigratedTxn);

                response.Header = CommonFunctions.FailedResponse("Transaction Id: " + id, objLocalVars._deviceId, objLocalVars._requestId, status: "Success");
                return Ok(response);

            }
            catch (Exception ex)
            {
                response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.Failed, objLocalVars._deviceId, objLocalVars._requestId, status: "Failed");
                return Ok(response);
            }
            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDocumentRequest"></param>
        /// <param name="xmlDocumentResponse"></param>
        /// <returns></returns>
        private MtdtxnSattlementUKDto SemiIntigratedSattlementTxn(XmlDocument xmlDocumentRequest)
        {
            var selectSingleNode = xmlDocumentRequest.SelectSingleNode("PaymentRequest");
            if (selectSingleNode.IsNull())
            {
                //return Ok(logOutResponse);
            }
            MtdtxnSattlementUKDto semiDto = new MtdtxnSattlementUKDto();
            semiDto.Revision = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/Revision").InnerText;
            semiDto.MerchantIndex = Convert.ToInt32(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/MerchantIndex").InnerText);
            semiDto.settlementResult = Convert.ToInt32(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/settlementResult").InnerText);
            semiDto.AcquirerName = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/AcquirerName").InnerText;
            semiDto.MerchantAddress1 = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/MerchantAddress1").InnerText;
            semiDto.MerchantAddress2 = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/MerchantAddress2").InnerText;
            semiDto.AcquirerMerchantID = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/AcquirerMerchantID").InnerText;
            semiDto.TerminalIdentity = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/TerminalIdentity").InnerText;
            semiDto.HostMessage = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/HostMessage").InnerText;
            semiDto.FirstMessageNumber = Convert.ToInt16(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/FirstMessageNumber").InnerText);
            semiDto.LastMessageNumber = Convert.ToInt16(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/LastMessageNumber").InnerText);
            semiDto.DateTime = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/DateTime").InnerText;
            semiDto.NumScheme = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/NumScheme").InnerText;
            semiDto.QuantityDebits = Convert.ToInt32(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/QuantityDebits").InnerText);
            semiDto.ValueDebits = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/ValueDebits").InnerText);
            semiDto.QuantityCredits = Convert.ToInt32(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/QuantityCredits").InnerText);
            semiDto.ValueCredits = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/ValueCredits").InnerText);
            semiDto.QuantityCash = Convert.ToInt32(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/QuantityCash").InnerText);
            semiDto.ValueCash =Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/ValueCash").InnerText);
            semiDto.QuantityClessCredits = Convert.ToInt32(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/QuantityClessCredits").InnerText);
            semiDto.ValueClessCredits = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/ValueClessCredits").InnerText);
            semiDto.QuantityClessDebits = Convert.ToInt32(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/QuantityClessDebits").InnerText);
            semiDto.ValueClessDebits = Convert.ToDecimal(xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/ValueClessDebits").InnerText);
            semiDto.CardSchemeName = xmlDocumentRequest.SelectSingleNode("PaymentRequest/TransactionDetail/CardSchemeName").InnerText;
            semiDto.RequestXml = xmlDocumentRequest.ToString();
            return semiDto;
        }

        [HttpGet]
        public IHttpActionResult Go(HttpRequestMessage request)
        {
            MTDTransactionDto dt=new MTDTransactionDto();
            return Ok(dt);
        }
    }
}
