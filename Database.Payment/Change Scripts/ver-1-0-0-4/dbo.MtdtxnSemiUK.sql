IF OBJECT_ID('dbo.MtdtxnSemiUK', 'U') IS NULL
BEGIN 
	CREATE TABLE [dbo].[MtdtxnSemiUK](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[Revision] [nvarchar](3) NULL,
		[SessionId] [nvarchar](33) NULL,
		[MessageNumber] [nvarchar](3) NULL,
		[TransactionStatus] [nvarchar](2) NULL,
		[EntryMethod] [nvarchar](2) NULL,
		[ReceiptNumber] [nvarchar](5) NULL,
		[AcquirerMerchantID] [nvarchar](15) NULL,
		[DateTime] [nvarchar](13) NULL,
		[Currency] [nvarchar](3) NULL,
		[CardSchemeName] [nvarchar](25) NULL,
		[PAN] [nvarchar](19) NULL,
		[ExpiryDate] [nvarchar](4) NULL,
		[GemsReceiptID] [nvarchar](15) NULL,
		[AuthorizationCode] [nvarchar](9) NULL,
		[AcquirerResponseCode] [nvarchar](2) NULL,
		[Reference] [nvarchar](20) NULL,
		[MerchantName] [nvarchar](25) NULL,
		[MerchantAddress1] [nvarchar](25) NULL,
		[MerchantAddress2] [nvarchar](25) NULL,
		[AID] [nvarchar](32) NULL,
		[PANSequenceNum] [nvarchar](3) NULL,
		[StartDate] [nvarchar](25) NULL,
		[TerminalIdentity] [nvarchar](8) NULL,
		[TransactionAmount] [money] NULL,
		[IsDCCTxn] [bit] NULL,
		[IsLoyaltyTxn] [bit] NULL,
		[DCCAmount] [money] NULL,
		[DonationAmount] [money] NULL,
		[RedeemedAmount] [money] NULL,
		[DCCCurrency] [nvarchar](4) NULL,
		[FXRateApplied] [nvarchar](12) NULL,
		[FXExponentApplied] [bit] NULL,
		[DCCCurrencyExponent] [int] NULL,
		[MessageHost] [nvarchar](35) NULL,
		[TransactionType] [nvarchar](2) NULL,
		[GratuityAmount] [money] NULL,
		[CashAmount] [money] NULL,
		[TotalTransactionAmount] [money] NULL,
		[ICCApplicationFileName] [nvarchar](16) NULL,
		[ICCApplicationPreferredName] [nvarchar](16) NULL,
		[eCardVerificationMethod] [int] NULL,
		[TransactionID] [nvarchar](56) NULL,
		[RequestXml] [xml] NULL,
	 CONSTRAINT [PK_MtdtxnSemiUK] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
END
GO