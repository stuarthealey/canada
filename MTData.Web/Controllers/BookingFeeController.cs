﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Web.ViewData;
using Ninject;
using System.Text;
using System.Globalization;
using System.Reflection;
using System.Web.Security;
using MTData.Utility;

namespace MTData.Web.Controllers
{
    public class BookingFeeController : Controller
    {
        readonly ILogger _logger = new Logger();
        //private readonly IBookingFeeService _feeService;
        private readonly ICommonService _commonService;
        private StringBuilder _logMessage;

        public BookingFeeController([Named("CommonService")] ICommonService commomService, StringBuilder logMessage)
        {
            _feeService = feeService;
            _commonService = commomService;
            _logMessage = logMessage;
        }

        /// <summary>
        /// Purpose             :   To display booking fee applied to merchants
        /// Function Name       :   Index
        /// Created By          :   Salil Gupta
        /// Created On          :   20/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                             MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            _logMessage.Append(
               "using object initilizar for UserViewData and getting list of roles, counrtylist, statelist and citylist");
            _logMessage.Append("calling GetFeeDtoList method of _feeService to get fee list");
            var feeDtoList = _feeService.GetFeeDtoList();
            _logMessage.Append("_feeService method of _feeService executed successfully");
            _logMessage.Append("calling ToFeeViewDataList to convert BookigFeeDto into BookingFeeViewData");
            var viewDataList = feeDtoList.ToFeeViewDataList();
            _logMessage.Append("ToFeeViewDataList to convert BookigFeeDto into BookingFeeViewData excecuted successfully returing to view");
            return View(viewDataList);
        }

        /// <summary>
        /// Purpose             :   To create booking fee
        /// Function Name       :   Create
        /// Created By          :   Salil Gupta
        /// Created On          :   22/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                            MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            _logMessage.Append(
               "using object initilizar for BookingFeeViewData and getting list of Merchants, Gateways, Types");
            var feeView = new BookingFeeViewData
            {
                MerchantList = MerchantList(),
                GatewayList = GatewayList(),
                TypeList = TypeList()
            };
            _logMessage.Append(
              "object initilizar for BookingFeeViewData completed returing to view");
            return View(feeView);
        }

        /// <summary>
        /// Purpose             :   To create booking fee
        /// Function Name       :   Create
        /// Created By          :   Salil Gupta
        /// Created On          :   23/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="feeViewData"></param>      
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(BookingFeeViewData feeViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                              MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (ModelState.IsValid)
                {
                    _logMessage.Append("calling ToFeeDto to convert BookingFeeViewData into BookigFeeDto");
                    var feeDto = feeViewData.ToFeeDto();
                    _logMessage.Append("ToFeeDto to convert BookingFeeViewData into BookigFeeDto excecuted successfully");
                    feeDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                    _logMessage.Append("Add method of _feeService takes feeDto");
                    _feeService.Add(feeDto);
                    _logMessage.Append("Add method of _feeService executed successfully returing to view");
                    return RedirectToAction("Index");
                }
                _logMessage.Append(
                 "using object initilizar for BookingFeeViewData and getting list of Merchants, Gateways, Types");
                feeViewData.GatewayList = GatewayList();
                feeViewData.MerchantList = MerchantList();
                feeViewData.TypeList = TypeList();
                _logMessage.Append(
            "object initilizar for BookingFeeViewData completed returing to view");
                return View(feeViewData);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
            }
            return View();

        }

        /// <summary>
        /// Purpose             :   To update booking fee
        /// Function Name       :   Update
        /// Created By          :   Salil Gupta
        /// Created On          :   26/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="feeId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Update(int feeId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                          MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            _logMessage.Append("calling GetFeeDto method of _feeService takes parameter feeid " + feeId);
            var feeDto = _feeService.GetFeeDto(feeId);
            _logMessage.Append("GetFeeDto method of _feeService executed successfully");
            _logMessage.Append("calling ToFeeViewData to convert BookingFeeViewData into BookigFeeDto");
            BookingFeeViewData feeView = feeDto.ToFeeViewData();
            _logMessage.Append("ToFeeDto to convert BookingFeeViewData into BookigFeeDto excecuted successfully");
            _logMessage.Append(
              "assign MerchantList, GatewayList,  TypeList to model properties");
            feeView.MerchantList = MerchantList();
            feeView.GatewayList = GatewayList();
            feeView.TypeList = TypeList();
            _logMessage.Append(
              "assign MerchantList, GatewayList,  TypeList to model properties success returing to view");
            return View(feeView);
        }

        /// <summary>
        /// Purpose             :   To update booking fee applied to merchant
        /// Function Name       :   Update
        /// Created By          :   Salil Gupta
        /// Created On          :   27/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="feeId"></param>
        /// <param name="feeViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(int feeId, BookingFeeViewData feeViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                             MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                if (ModelState.IsValid)
                {
                    _logMessage.Append("calling ToFeeDto to convert BookingFeeViewData into BookigFeeDto");
                    BookigFeeDto feeDto = feeViewData.ToFeeDto();
                    _logMessage.Append("ToFeeDto to convert BookingFeeViewData into BookigFeeDto excecuted successfully");

                    feeDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                    _logMessage.Append("calling Update method of _feeService takes feeDto and email= " + feeDto.ModifiedBy);
                    _feeService.Update(feeDto, feeId);
                    _logMessage.Append("Update method of _feeService executed successfully returing to view");
                    return RedirectToAction("Index");
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), exception);
            }
            _logMessage.Append(
             "assign MerchantList, GatewayList,  TypeList to model properties");
            feeViewData.MerchantList = MerchantList();
            feeViewData.GatewayList = GatewayList();
            feeViewData.TypeList = TypeList();
            _logMessage.Append(
             "assign MerchantList, GatewayList,  TypeList to model properties success returing to view");
            return View(feeViewData);
        }

        /// <summary>
        /// Purpose             :   To delete booking fee applied to merchant
        /// Function Name       :   Delete
        /// Created By          :   Salil Gupta
        /// Created On          :   28/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="feeId"></param>   
        /// <returns></returns>
        [HttpGet]
        public ActionResult Delete(int feeId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                             MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("DeleteFee method of _feeService takes feeId and email= " + feeId);
                _feeService.DeleteFee(feeId);
                _logMessage.Append("DeleteFee method of _feeService executed successfully returing to view");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), exception);
            }
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Purpose             :   To get the list of gateways
        /// Function Name       :   GatewayList
        /// Created By          :   Salil Gupta
        /// Created On          :   28/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<GatewayListViewData> GatewayList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                                  MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                var gatewayDtoList = _commonService.GetGatewayList();
                List<GatewayListViewData> listGateway = gatewayDtoList.ToGatewayViewDataList().ToList();
                return listGateway;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), exception);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :   To get type of surcharge or booking fee
        /// Function Name       :   TypeList
        /// Created By          :   Salil Gupta
        /// Created On          :   29/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<SurchargeTypeViewData> TypeList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                               MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                List<SurchargeTypeViewData> listSurchargeType = new List<SurchargeTypeViewData>
                {
                    new SurchargeTypeViewData { TypeID = 0, TypeName = "Fixed" }, new SurchargeTypeViewData { TypeID = 1, TypeName = "Percentage" }
                };

                _logMessage.Append("adding two surcharges to list of SurchargeTypeViewData");
                _logMessage.Append("surcharges added successfully in list of SurchargeTypeViewData");
                return listSurchargeType;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), exception);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :   To get the list of merchants
        /// Function Name       :   MerchantList
        /// Created By          :   Salil Gupta
        /// Created On          :   30/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<MerchantListViewData> MerchantList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                                              MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("calling GetMerchantList method of _feeService to get GetMerchantList");
                var merchantDtoList = _commonService.GetMerchantList();
                _logMessage.Append("GetMerchantList method of _feeService executed successfully returing to view");
                List<MerchantListViewData> listMerchant = merchantDtoList.ToMerchantViewDataSelectList().ToList();
                return listMerchant;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), exception);
            }
            return null;
        }

    }
}