@echo off
REM: Command File Created by Microsoft Visual Database Tools
REM: Authentication type: Windows NT
REM: Usage: CommandFilename [Server] [Database]

if '%1' == '' goto usage
if '%2' == '' goto usage

if '%1' == '/?' goto usage
if '%1' == '-?' goto usage
if '%1' == '?' goto usage
if '%1' == '/help' goto usage

REM: ====================================
REM: Ordered scripts
REM: ====================================

echo.
echo "dbo.MTDTransaction.sql"
osql -S %1 -d %2 -E -b -i "dbo.MTDTransaction.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.MTDTransaction_IX.sql"
osql -S %1 -d %2 -E -b -i "dbo.MTDTransaction_IX.sql"
if %ERRORLEVEL% NEQ 0 goto errors

echo.
echo "dbo.UKMTDTransaction.sql"
osql -S %1 -d %2 -E -b -i "dbo.UKMTDTransaction.sql"
if %ERRORLEVEL% NEQ 0 goto errors


REM: ====================================
REM: Reference Data Scripts
REM: ====================================

echo.
echo "dbo.ReferenceDataChanges.sql"
osql -S %1 -d %2 -E -b -i "dbo.ReferenceDataChanges.sql"
if %ERRORLEVEL% NEQ 0 goto errors

goto finish

REM: How to use screen
:usage
echo.
echo Usage: MyScript Server Database
echo Server: the name of the target SQL Server
echo Database: the name of the target database
echo.
echo Example: MyScript.cmd MainServer MainDatabase
echo.
echo.
goto done

REM: error handler
:errors
echo.
echo WARNING! Error(s) were detected!
echo --------------------------------
echo Please evaluate the situation and, if needed,
echo restart this command file. You may need to
echo supply command parameters when executing
echo this command file.
echo.
pause
goto done

REM: finished execution
:finish
echo.
echo Script execution is complete!
:done
@echo on