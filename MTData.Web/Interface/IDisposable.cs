﻿namespace MTData.Web.Interface
{
    interface IDisposable
    {
        void Dispose();
    }
}
