﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web;

namespace MTDataDispatchSystemSync.Models
{
    [XmlSerializerFormat]
    public class ESIDriverResult
    {
        public string Error { get; set; }
        public string ErrorMessage { get; set; }
        public  Driver Driver { get; set; }
        public bool Succeeded { get; set; }
    }
}