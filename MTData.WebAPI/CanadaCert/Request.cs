﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CanadaCert
{
    [System.SerializableAttribute()]
    public class Request
    {
        public Request()
        {
            this.Version = "3"; // Initialize the Request Version to default [Version="3"]
        }

        #region Common property

        [System.Xml.Serialization.XmlElementAttribute("ReqClientID", typeof(ReqClientIDType))]
        public ReqClientIDType ReqClientID { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Transaction", typeof(TransactionClientType))]
        public TransactionClientType Transaction { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Ping", typeof(PingRequest))]
        public PingRequest Ping { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Registration", typeof(Registration))]
        public Registration Registration { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Activation", typeof(Activation))]
        public Activation Activation { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ClientTimeout { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Version { get; set; }

        #endregion //Common property
    }
}