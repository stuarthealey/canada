﻿using System;

namespace MTD.Core.DataTransferObjects
{
    public class ScheduleReportDto
    {
        public int ScheduleID { get; set; }
        public DateTime DateFrom { get; set; }
        public Nullable<System.DateTime> DateTo { get; set; }
        public Nullable<byte> Frequency { get; set; }
        public Nullable<byte> FrequencyType { get; set; }
        public Nullable<System.TimeSpan> StartTime { get; set; }
        public string TimeZone { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string TimeInString { get; set; }
        public System.Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> IsCreated { get; set; }
        public Nullable<int> WeekDay { get; set; }
        public Nullable<int> MonthDay { get; set; }
        public Nullable<int> fk_ReportID { get; set; }
        public string WeekName { get; set; }
        public string FrequencyName { get; set; }
        public string DisplayName { get; set; }
        public int? fk_MerchantId { get; set; }
        public Nullable<System.DateTime> NextScheduleDateTime { get; set; }
        public Nullable<System.TimeSpan> DisplayStartTime { get; set; }
        public Nullable<int> CorporateUserId { get; set; }
    }
}
