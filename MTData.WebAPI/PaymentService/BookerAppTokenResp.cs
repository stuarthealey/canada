﻿using MTData.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using MTData.WebAPI.Common;

namespace MTData.WebAPI
{
    public class BookerAppTokenResp
    {
        public XmlDocument reponseDoc { get; set; }

        public XmlElement responseElement { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public BookerAppTokenResp()
        {
            reponseDoc = new XmlDocument();
            reponseDoc.LoadXml("<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\"><soap12:Body></soap12:Body> </soap12:Envelope>");
            XmlElement rootElement = reponseDoc.DocumentElement;

            XmlNode Root = reponseDoc.GetElementsByTagName("soap12:Body").Item(0);

            XmlElement requestTypeElement = reponseDoc.CreateElement("CreateRCCIResponse");
            XmlAttribute requestTypeNameSpaceAttibute = reponseDoc.CreateAttribute("xmlns");
            requestTypeNameSpaceAttibute.Value = "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService";
            requestTypeElement.Attributes.Append(requestTypeNameSpaceAttibute);
            Root.AppendChild(requestTypeElement);
            responseElement = reponseDoc.CreateElement("CreateRCCIResult");
            requestTypeElement.AppendChild(responseElement);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resp"></param>
        /// <returns></returns>
        public XmlDocument ConstructDoc(CreateRCCIResult resp)
        {
            //Result 
            XmlDocumentFragment result = reponseDoc.CreateDocumentFragment();
            result.InnerXml = CommonFunctions.Serialize(resp.Result);
            responseElement.AppendChild(result.FirstChild as XmlElement);

            //CreditCardStatus
            XmlDocumentFragment creditCardStatus = reponseDoc.CreateDocumentFragment();
            creditCardStatus.InnerXml = CommonFunctions.Serialize(resp.CreditCardStatus);
            responseElement.AppendChild(creditCardStatus.FirstChild as XmlElement);

            //RCCI
            XmlDocumentFragment rCCI = reponseDoc.CreateDocumentFragment();
            rCCI.InnerXml = CommonFunctions.Serialize(resp.RCCI);
            responseElement.AppendChild(rCCI.FirstChild as XmlElement);

            //FullCreditCardDetails
            XmlDocumentFragment truncatedCreditCardDetails = reponseDoc.CreateDocumentFragment();
            truncatedCreditCardDetails.InnerXml = CommonFunctions.Serialize(resp.TruncatedCreditCardDetails);
            responseElement.AppendChild(truncatedCreditCardDetails.FirstChild as XmlElement);

            return reponseDoc;
        }

    }
}