IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Validation]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_Validation]
GO

CREATE PROCEDURE dbo.usp_Validation 
(
	@deviceId VARCHAR(100) = NULL,
	@driverNo VARCHAR(300) = NULL,
	@token VARCHAR(MAX) = NULL
)
AS
BEGIN
	DECLARE @Permission INT

	SET @Permission = 1

	IF NOT EXISTS (SELECT 1 FROM dbo.Terminal WHERE MacAdd = @deviceId AND IsActive = 1)
	BEGIN
		SET @Permission = -1
		SELECT @Permission
	END

	IF NOT EXISTS (SELECT 1 FROM dbo.DriverLogin WHERE DriverNo = @driverNo AND Token = @token)
	BEGIN
		SET @Permission = -2
		SELECT @Permission
		RETURN
	END

	IF NOT EXISTS (SELECT 1 FROM dbo.Driver WHERE DriverNo = @driverNo AND IsActive = 1)
	BEGIN
		SET @Permission = -3
		SELECT @Permission 
		RETURN
	END

	SET  @Permission = 1
	SELECT @Permission
END
GO
