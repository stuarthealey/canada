﻿using MTD.Core.DataTransferObjects;
using MTD.Core.Factory.Interface;
using MTD.Data.Data;

namespace MTD.Core.Factory
{
    public class UserFactory : IUserFactory
    {
        public User CreateUser(UserDto userDto)
        {
            User user = new User
            {
                UserID = userDto.UserID,
                FName = userDto.FName,
                LName = userDto.LName,
                Email = userDto.Email,
                Phone = userDto.Phone,
                fk_Country = userDto.fk_Country,
                fk_State = userDto.fk_State,
                fk_City = userDto.fk_City,
                Address = userDto.Address,
                PCode = userDto.PCode,
                fk_UserTypeID = userDto.fk_UserTypeID,
                fk_MerchantID = userDto.fk_MerchantID,
                IsLockedOut = userDto.IsLockedOut,
                IsActive = userDto.IsActive,
                CreatedBy = userDto.CreatedBy,
                CreatedDate = userDto.CreatedDate,
                ModifiedBy = userDto.ModifiedBy,
                ModifiedDate = userDto.ModifiedDate,
                RolesAssigned = userDto.RolesAssigned,
                fleetListId = userDto.fleetListId,
                MerchantListId = userDto.MerchantListId,
                ParentUserID = userDto.ParentUserID

            };
            return user;
        }
    }
}
