﻿namespace MTD.Core.DataTransferObjects
{
    public class TxnTaxSurchargesDto
    {
        public byte? SurchargeType { get; set; }
        public decimal? SurchargeFixed { get; set; }
        public decimal? SurchargePer { get; set; }
        public decimal? SurMaxCap { get; set; }
        public decimal? StateTaxRate { get; set; }
        public decimal? FederalTaxRate { get; set; }
        public decimal? TipPerLow { get; set; }
        public decimal? TipPerMedium { get; set; }
        public decimal? TipPerHigh { get; set; }
        public bool IsTaxInclusive { get; set; }

        public byte? FeeType { get; set; }
        public decimal? FeeFixed { get; set; }
        public decimal? FeePer { get; set; }
        public decimal? FeeMaxCap { get; set; }
    }
}
