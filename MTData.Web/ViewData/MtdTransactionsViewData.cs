﻿using System;

namespace MTData.Web.ViewData
{
    public class MtdTransactionsViewData
    {
        public int? Id { get; set; }
        public Nullable<int> MerchantId { get; set; }
        public string TerminalId { get; set; }
        public string VehicleNo { get; set; }
        public string DriverNo { get; set; }
        public string JobNumber { get; set; }
        public string CrdHldrName { get; set; }
        public string CrdHldrPhone { get; set; }
        public string CrdHldrCity { get; set; }
        public string CrdHldrState { get; set; }
        public string CrdHldrZip { get; set; }
        public string CrdHldrAddress { get; set; }
        public string Stan { get; set; }
        public string TransRefNo { get; set; }
        public string TransOrderNo { get; set; }
        public string PaymentType { get; set; }
        public string TxnType { get; set; }
        public string LocalDateTime { get; set; }
        public string TransmissionDateTime { get; set; }
        public Nullable<decimal> FareValue { get; set; }
        public Nullable<decimal> Tip { get; set; }
        public Nullable<decimal> Surcharge { get; set; }
        public Nullable<decimal> Fee { get; set; }
        public Nullable<decimal> Taxes { get; set; }
        public Nullable<decimal> Toll { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string Currency { get; set; }
        //public string AccountNumber { get; set; }
        public string ExpiryDate { get; set; }
        public string CardType { get; set; }
        public string Industry { get; set; }
        public string EntryMode { get; set; }
        public string ResponseCode { get; set; }
        public string AddRespData { get; set; }
        public string CardLvlResult { get; set; }
        public string SrcReasonCode { get; set; }
        public string GatewayTxnId { get; set; }
        public string AuthId { get; set; }
        public string AthNtwId { get; set; }
        public string RequestXml { get; set; }
        public string ResponseXml { get; set; }
        public string SerialNo { get; set; }
        public string SourceId { get; set; }
        //public Nullable<System.DateTime> DateOfTransaction { get; set; }
        public string TransNote { get; set; }
        //public Nullable<bool> IsMoto { get; set; }
        public string CardToken { get; set; }
        public Nullable<bool> IsCompleted { get; set; }
        public Nullable<bool> IsRefunded { get; set; }
        public Nullable<bool> IsVoided { get; set; }
        public string CreatedBy { get; set; }
        public string PickAddress { get; set; }
        public string DestinationAddress { get; set; }
        public Nullable<double> StartLatitude { get; set; }
        public Nullable<double> EndLatitude { get; set; }
        public Nullable<double> StartLongitude { get; set; }
        public Nullable<double> EndLongitude { get; set; }
        public Nullable<System.DateTime> TxnDate { get; set; }
        public string FirstFourDigits { get; set; }
        public string LastFourDigits { get; set; }
        public Nullable<decimal> FlagFall { get; set; }
        public Nullable<decimal> Extras { get; set; }
        public Nullable<decimal> GateFee { get; set; }
        public Nullable<decimal> OthersFee { get; set; }
        public Nullable<System.DateTime> startDate { get; set; }
        public Nullable<System.DateTime> endDate { get; set; }
        public string Sorting { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int? FleetId { get; set; }
        public decimal? MinAmount { get; set; }
        public decimal? MaxAmount { get; set; }
        public Nullable<int> fk_FleetId { get; set; }
        public string RequestId { get; set; }
        public Nullable<bool> IsDispatchRequest { get; set; }
        public Nullable<decimal> TechFee { get; set; }
        public Nullable<decimal> FleetFee { get; set; }
        public Nullable<bool> IsVarified { get; set; }
        public string payerType { get; set; }
        public int UserId { get; set; }
        public Nullable<bool> IsDownloaded { get; set; }
        public string TransSeqNo { get; set; }
        public Nullable<int> NetworkType { get; set; }
        public string StringDateTime { get; set; }
        public Nullable<int> BatchNumber { get; set; }
        public string CountryCodes { get; set; }
        public string ResponseBit63 { get; set; }
    }

}