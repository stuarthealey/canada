﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Web.ViewData;
using MTData.Web.Resource;
using MTData.Utility;

namespace MTData.Web.Controllers
{
    public class FleetController : Controller
    {
        private readonly ILogger _logger = new Logger();
        private StringBuilder _logMessage;
        private readonly IFleetService _fleetService;
        private StringBuilder _logTracking;

        public FleetController([Named("FleetService")] IFleetService fleetService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _fleetService = fleetService;
            _logMessage = logMessage;
            _logTracking = logTracking;
        }

        /// <summary>
        /// Purpose             :   To create the fleet
        /// Function Name       :   Create
        /// Created By          :   Asif Shafeeque
        /// Created On          :   13/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            FleetViewData fleetViewdata = new FleetViewData();
            fleetViewdata.currencyList = _fleetService.CurrencyList().ToCurrencyViewDataList();

            return View(fleetViewdata);
        }

        /// <summary>
        /// Purpose             :   To create the fleet
        /// Function Name       :   Create
        /// Created By          :   Asif Shafeeque
        /// Created On          :   13/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(FleetViewData fleetViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            if (fleetViewData.PayType == 3)
            {
                if (fleetViewData.FleetFeeType == null)
                    ModelState.AddModelError("FleetFeeType", MtdataResource.FleetFee_type_Required);

                if (fleetViewData.FleetFeeType == 1)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(fleetViewData.FleetFeeFixed)))
                        ModelState.AddModelError("FleetFeeFixed", MtdataResource.FleetFee_fix_Required);
                }

                if (fleetViewData.FleetFeeType == 2)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(fleetViewData.FleetFeePer)))
                        ModelState.AddModelError("FleetFeePer", MtdataResource.FleetFee_Per_Required);

                    if (string.IsNullOrEmpty(Convert.ToString(fleetViewData.FleetFeeMaxCap)))
                        ModelState.AddModelError("FleetFeeMaxCap", MtdataResource.FleetFee_maxCap_Required);
                }

                if (fleetViewData.FleetFeeType == 3)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(fleetViewData.FleetFeeFixed)))
                        ModelState.AddModelError("FleetFeeFixed", MtdataResource.FleetFee_fix_Required);

                    if (string.IsNullOrEmpty(Convert.ToString(fleetViewData.FleetFeePer)))
                        ModelState.AddModelError("FleetFeePer", MtdataResource.FleetFee_Per_Required);

                    if (string.IsNullOrEmpty(Convert.ToString(fleetViewData.FleetFeeMaxCap)))
                        ModelState.AddModelError("FleetFeeMaxCap", MtdataResource.FleetFee_maxCap_Required);
                }
            }

            if (!ModelState.IsValid)
            {
                fleetViewData.currencyList = _fleetService.CurrencyList().ToCurrencyViewDataList();
                return View(fleetViewData);
            }
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("calling ToDTO method to map FleetDto to fleetViewData and retrun FleetDto");

                var fleetDto = fleetViewData.ToDTO();
                
                _logMessage.Append("ToDTO method executed successfully");
                
                int merchantId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.fk_MerchantID));
                fleetDto.fk_MerchantID = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                int userType = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_UserTypeID);
                int userId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.UserID);
                fleetDto.CreatedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                fleetDto.ModifiedBy = fleetDto.CreatedBy;
                
                _logMessage.Append("Taking First name, userid, user type and merchant id from session the are : " + fleetDto.CreatedBy + ", " + userId + ", " + userType + ", " + fleetDto.fk_MerchantID);
                _logMessage.Append("Calling Add method of _fleetService takes fleet dto and data from session");

                if (fleetDto.PayType == 1 || fleetDto.PayType == 2)
                    fleetDto.FleetFee = null;

                if (!string.IsNullOrEmpty(fleetDto.PCode))
                    fleetDto.PCode = fleetDto.PCode.Hash();

                bool response = _fleetService.Add(fleetDto, userType, userId, merchantId);
                if (response)
                {
                    ViewBag.IsExitFleet = MtdataResource.fleet_already_exist; //"A fleet already exist with this name.Please select another.";
                    fleetViewData.currencyList = _fleetService.CurrencyList().ToCurrencyViewDataList();

                    return View(fleetViewData);
                }

                _logTracking.Append("Fleet " + fleetViewData.FleetName + " was Creadted by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                _logMessage.Append("Add method of _fleetService executed successfully");
                
                return RedirectToAction("Index", "Fleet");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View(fleetViewData);
        }

        /// <summary>
        /// Purpose             :   To update the fleet
        /// Function Name       :   Update
        /// Created By          :   Asif Shafeeque
        /// Created On          :   15/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Update(int fleetId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            _logMessage.Append("Calling GetFleet method of service _fleetService takes parameter Fleet id =" + fleetId);

            try
            {
                var fkMerchantId = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;
                if (fkMerchantId != null)
                {
                    int merchantId = (int)fkMerchantId;
                    var fleetdto = _fleetService.GetFleet(fleetId, merchantId);
                    
                    _logMessage.Append("Calling GetFleet method of _fleetService executed successfully");
                    _logMessage.Append("calling ToViewData method to map fleetViewData to FleetDto and retrun fleetViewData");
                    
                    var fleetviewData = fleetdto.ToViewData();
                    
                    _logMessage.Append("ToViewData method executed successfully");
                    
                    fleetviewData.currencyList = _fleetService.CurrencyList().ToCurrencyViewDataList();
                    
                    if (fleetviewData != null)
                        return View(fleetviewData);
                }

                Session.Abandon();

                return RedirectToAction("Index", "Member");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   To display the fleets
        /// Function Name       :   Index
        /// Created By          :   Asif Shafeeque
        /// Created On          :   14/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>    
        /// <returns></returns>
        public ActionResult Index()
        {
            IsFirstLoginViewData obj = new IsFirstLoginViewData();
            if ((SessionDto)Session["User"] != null && ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1 && ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 4)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                {
                    if (!(bool)Session["Islogged"])
                    {
                        obj.IsFirstLogin = (bool)((SessionDto)Session["User"]).SessionUser.IsFirstLogin;
                        obj.UserTypeId = Convert.ToString(((SessionDto)Session["User"]).SessionUser.fk_UserTypeID);
                        Session.Remove("Islogged");
                        Session["Islogged"] = true;
                        obj.UserId = ((SessionDto)Session["User"]).SessionUser.UserID;

                        return View(obj);
                    }
                }

                return View(obj);
            }

            return RedirectToAction("index", "Member");
        }

        /// <summary>
        /// Purpose             :   To update the fleet
        /// Function Name       :   Update
        /// Created By          :   Asif Shafeeque
        /// Created On          :   15/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>    
        /// <param name="fleetviewData"></param>
        /// /// <param name="fleetId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(FleetViewData fleetviewData, int fleetId)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            if (fleetviewData.PayType == 3)
            {
                if (fleetviewData.FleetFeeType == null)
                {
                    ModelState.AddModelError("FleetFeeType", MtdataResource.FleetFee_type_Required);
                }

                if (fleetviewData.FleetFeeType == 1)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(fleetviewData.FleetFeeFixed)))
                        ModelState.AddModelError("FleetFeeFixed", MtdataResource.FleetFee_fix_Required);
                }
                
                if (fleetviewData.FleetFeeType == 2)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(fleetviewData.FleetFeePer)))
                        ModelState.AddModelError("FleetFeePer", MtdataResource.FleetFee_Per_Required);
                    
                    if (string.IsNullOrEmpty(Convert.ToString(fleetviewData.FleetFeeMaxCap)))
                        ModelState.AddModelError("FleetFeeMaxCap", MtdataResource.FleetFee_maxCap_Required);
                }

                if (fleetviewData.FleetFeeType == 3)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(fleetviewData.FleetFeeFixed)))
                        ModelState.AddModelError("FleetFeeFixed", MtdataResource.FleetFee_fix_Required);
                    
                    if (string.IsNullOrEmpty(Convert.ToString(fleetviewData.FleetFeePer)))
                        ModelState.AddModelError("FleetFeePer", MtdataResource.FleetFee_Per_Required);
                    
                    if (string.IsNullOrEmpty(Convert.ToString(fleetviewData.FleetFeeMaxCap)))
                        ModelState.AddModelError("FleetFeeMaxCap", MtdataResource.FleetFee_maxCap_Required);
                }
            }

            if (!ModelState.IsValid)
            {
                fleetviewData.currencyList = _fleetService.CurrencyList().ToCurrencyViewDataList();
                return View(fleetviewData);
            }

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int merchantId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.fk_MerchantID));
                
                _logMessage.Append("calling ToDTO method to map FleetDto to fleetViewData and retrun FleetDto");
                
                FleetDto fleeDto = fleetviewData.ToDTO();
                fleeDto.CreatedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                fleeDto.ModifiedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                
                _logMessage.Append("ToDTO method executed successfully");
                _logMessage.Append("Calling Update method of service _fleetService takes parameter Fleet id =" + fleetId + " and fleet dto contains fleet name= " + fleeDto.FleetName + ", fleet description= " + fleeDto.Description);

                if (fleeDto.PayType == 1 || fleeDto.PayType == 2)
                    fleeDto.FleetFee = null;

                if (!string.IsNullOrEmpty(fleeDto.PCode))
                    fleeDto.PCode = fleeDto.PCode.Hash();

                bool res = _fleetService.Update(fleeDto, fleetId, merchantId);
                if (res)
                {
                    fleetviewData.currencyList = _fleetService.CurrencyList().ToCurrencyViewDataList();
                    ViewBag.IsExitFleet = MtdataResource.fleet_already_exist; //"A fleet already exist with this name.Please select another.";
                    return View(fleetviewData);
                }

                _logTracking.Append("Fleet " + fleetId + " was Update by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                _logMessage.Append("Update method of service _fleetService excuted successfully result :true");
                
                return RedirectToAction("Index", "Fleet");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View(fleetviewData);
        }

        /// <summary>
        /// Purpose             :   To filter the user fleets
        /// Function Name       :   GetUserFleetListByFilter
        /// Created By          :   Asif Shafeeque
        /// Created On          :   16/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>    
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult GetUserFleetListByFilter(string name = null, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                int fleetListCount;
                name = name.Trim();
                
                _logMessage.Append("Taking userid, user type and merchant id from session");
                
                int userType = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.fk_UserTypeID));
                int userId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.UserID));
                int merchantId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.fk_MerchantID));
                int logOnByUserId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserId));
                int logOnByUserType = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserType));
                
                _logMessage.Append("userid, user type and merchant id are : " + userId + ", " + userType + ", " + merchantId);
                _logMessage.Append("Calling GetUserFleetListByFilter method of service _fleetService");
                
                FilterSearchParametersDto filterSearch = new FilterSearchParametersDto()
                {
                    Name = name,
                    StartIndex = jtStartIndex,
                    PageSize = jtPageSize,
                    Sorting = jtSorting,
                    MerchantId = merchantId,
                    UserType = userType,
                    UserId = userId
                };

                var userFleet = _fleetService.GetUserFleetListByFilter(filterSearch, logOnByUserId, logOnByUserType);
                fleetListCount = _fleetService.FleetListCount(filterSearch, logOnByUserId, logOnByUserType);
                
                if (string.IsNullOrEmpty(name))
                    return Json(new { Result = "OK", Records = userFleet, TotalRecordCount = fleetListCount });
                
                var fleetDtos = userFleet as FleetDto[] ?? userFleet.ToArray();
                
                _logMessage.Append("GetUserFleetListByFilter method of _fleetService executed successfully");
                
                return Json(new { Result = "OK", Records = fleetDtos, TotalRecordCount = fleetListCount });
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);

                return Json(new { Result = "ERROR", exception.Message });
            }
        }

        /// <summary>
        /// Purpose             :   To delete the fleet
        /// Function Name       :   DeleteFleet
        /// Created By          :   Asif Shafeeque
        /// Created On          :   19/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>    
        /// <param name="fleetId"></param>    
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteFleet(int fleetId)
        {
            var sessionDto = (SessionDto)Session["User"];
            if (sessionDto == null || Request.Cookies[FormsAuthentication.FormsCookieName] == null || sessionDto.SessionUser.fk_UserTypeID == 1)
                return Json(new { Result = "NO", Message = "Not Authorized" });

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                _logMessage.Append("Calling DeleteFleet method of _fleetService takes parameter fleet Id = " + fleetId);

                var result = _fleetService.DeleteFleet(fleetId); //delete user by id
                if (result)
                {
                    var msg = MtdataResource.fleet_cannot_deleted_associated_with_driver_vehicle_users; //"This fleet can not be deleted as it is associated with driver or vehicle";
                    return Json(new { Result = "No", Message = msg });
                }

                _logMessage.Append("DeleteFleet method of _fleetService executed successfully");
                _logTracking.Append("Fleet " + fleetId + " was deleted by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                
                return Json(new { Result = "OK" });
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return Json(new { Result = "Ok" });
        }

        /// <summary>
        /// Purpose             :   To to get fleet fee
        /// Function Name       :   FleetFee
        /// Created By          :   Asif Shafeeque
        /// Created On          :   10/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult FleetFee()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                var fkMerchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                int userType = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_UserTypeID);
                int userId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.UserID);
                FleetViewData obj = new FleetViewData();
                
                if (userType == 2 || userType == 3)
                {
                    obj.fleetlist = _fleetService.FeeFleetList(fkMerchantId, userType, userId).ToViewDataList();
                    _logMessage.Append("FeeFleetList method of service _fleetService excuted successfully.");
                }

                if (userType == 4)
                {
                    obj.fleetlist = _fleetService.FeeFleetList(fkMerchantId, userType, userId).ToViewDataList();
                    _logMessage.Append("FeeFleetList method of service _fleetService excuted successfully.");
                }

                return View(obj);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleetViewdata"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult FleetFee(FleetViewData fleetViewdata)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                bool isvalid = true;
                if (fleetViewdata.FleetFee == null)
                {
                    ModelState.AddModelError("FleetFee", MtdataResource.FleetFee_Required);
                    isvalid = false;
                }

                if (fleetViewdata.FleetID == 0)
                {
                    ModelState.AddModelError("FleetID", MtdataResource.Please_select_fleet);
                    isvalid = false;
                }

                fleetViewdata.fk_MerchantID = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                int userType = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_UserTypeID);
                int userId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.UserID);
                
                if (userType == 2 || userType == 3)
                {
                    fleetViewdata.fleetlist = _fleetService.FeeFleetList(fleetViewdata.fk_MerchantID, userType, userId).ToViewDataList();

                    _logMessage.Append("FeeFleetList method of service _fleetService excuted successfully.");
                }

                if (userType == 4)
                {
                    fleetViewdata.fleetlist = _fleetService.FeeFleetList(fleetViewdata.fk_MerchantID, userType, userId).ToViewDataList();

                    _logMessage.Append("FeeFleetList method of service _fleetService excuted successfully.");
                }

                if (isvalid)
                {
                    fleetViewdata.CreatedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                    bool response = _fleetService.AddFleetFee(fleetViewdata.ToDTO());
                    
                    if (response)
                    {
                        _logTracking.Append("Fleet Fee is added for fleet " + fleetViewdata.FleetID + "  by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                        _logger.LogInfoMessage(_logTracking.ToString());
                        _logMessage.Append("AddFleetFee method of service _fleetService excuted successfully.");
                        
                        return RedirectToAction("FleetFeeIndex", "Fleet");
                    }
                    else
                    {
                        ViewBag.Message = MtdataResource.Fleet_fee_already_exist;
                    }
                }

                return View(fleetViewdata);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult FleetFeetListByFilter(string name = null, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                int fleetfeeListCount;
                name = name.Trim();

                _logMessage.Append("Taking userid, user type and merchant id from session");
                
                int userType = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.fk_UserTypeID));
                int userId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.UserID));
                int merchantId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.fk_MerchantID));
                
                _logMessage.Append("userid, user type and merchant id are : " + userId + ", " + userType + ", " + merchantId);
                _logMessage.Append("Calling FleetFeetListByFilter method of service _fleetService");

                FilterSearchParametersDto filterSearch = new FilterSearchParametersDto()
                {
                    Name = name,
                    StartIndex = jtStartIndex,
                    PageSize = jtPageSize,
                    Sorting = jtSorting,
                    MerchantId = merchantId,
                    UserType = userType,
                    UserId = userId
                };

                var feeFleet = _fleetService.FleetFeetListByFilter(filterSearch);
                fleetfeeListCount = _fleetService.FleetFeeListCount(filterSearch);

                if (string.IsNullOrEmpty(name))
                    return Json(new { Result = "OK", Records = feeFleet, TotalRecordCount = fleetfeeListCount });

                _logMessage.Append("FleetFeetListByFilter method of _fleetService executed successfully");

                return Json(new { Result = "OK", Records = feeFleet, TotalRecordCount = fleetfeeListCount });
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
                return Json(new { Result = "ERROR", exception.Message });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult FleetFeeIndex()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="surchage"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult FleetFeeUpdate(int surchargeId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                var fleetfee = _fleetService.GetFleetFee(surchargeId).ToSurchargeViewData();
                return View(fleetfee);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="surchargeViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult FleetFeeUpdate(SurchargeViewData surchargeViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            surchargeViewData.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);
            try
            {
                bool isvalid = true;
                if (surchargeViewData.FleetFee == null)
                {
                    ModelState.AddModelError("FleetFee", MtdataResource.FleetFee_Required);
                    isvalid = false;
                }
                
                if (surchargeViewData.FleetID == 0)
                {
                    ModelState.AddModelError("FleetID", MtdataResource.Please_select_fleet);
                    isvalid = false;
                }
                
                if (isvalid)
                {
                    bool response = _fleetService.UpdateFleetFee(surchargeViewData.ToSurchareDto());
                    if (response)
                    {
                        _logTracking.Append("Fleet Fee is updated for fleet " + surchargeViewData.FleetID + "  by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                        _logger.LogInfoMessage(_logTracking.ToString());
                        _logMessage.Append("AddFleetFee method of service _fleetService excuted successfully.");
                        
                        return RedirectToAction("FleetFeeIndex", "Fleet");
                    }
                }

                return View(surchargeViewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SurID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteFleetFee(int surID)
        {

            var sessionDto = (SessionDto)Session["User"];
            if (sessionDto == null || Request.Cookies[FormsAuthentication.FormsCookieName] == null || sessionDto.SessionUser.fk_UserTypeID == 1)
                return Json(new { Result = "NO", Message = "Not Authorized" });

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                _logMessage.Append("Calling DeleteFleetFee method of _fleetService takes parameter surchargeId Id = " + surID);

                _fleetService.DeleteFleetFee(surID);

                _logMessage.Append("DeleteFleetFee method of _fleetService executed successfully");
                _logTracking.Append("Fleet Fee of surhargeId" + surID + " was deleted by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                
                return Json(new { Result = "OK" });
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return Json(new { Result = "Ok" });
        }
    }
}