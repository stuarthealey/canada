﻿using System;
using System.Web.Configuration;
using System.Net;
using Firstdata.RapidConnect.Datawire.Soap;

using MTData.Utility;
using MTData.WebAPI.Resources;

/* The below class shows the way to send transaction request data to data wire,
 * and receive response from data wire using SOAP protocol.  
 * */
namespace MTData.WebAPI.Models
{
    class SoapHandler
    {
        public SoapHandler()
        {
        }

        /* The below method will take the XML request and returns the XML response received from Data wire.
	     * */
        public string SendMessage(string gmfRequest, string clientRef, TransactionRequest transactionRequest)
        {
            ILogger logger = new Logger();

            try
            {
                //string response = "";
                /* Create the instance of the RequestType that is a class generated from the Rapid connect Transaction 
                 * Service WSDL file [rc.wsdl]*/
                RequestType requestType = new RequestType();
                
                /* Set Client timeout*/
                requestType.ClientTimeout = WebConfigurationManager.AppSettings["DatawireClientTimeout"]; ;
                
                /* Create the instance of the RequestType that is a class generated from the Rapid connect Transaction 
                 * Service WSDL file [rc.wsdl]*/
                ReqClientIDType reqClientIdType = new ReqClientIDType();
                
                reqClientIdType.App = transactionRequest.App;
                reqClientIdType.Auth = transactionRequest.GroupId + "" + transactionRequest.FdMerchantId + "|" + transactionRequest.TerminalId;
                reqClientIdType.ClientRef = clientRef;
                reqClientIdType.DID = transactionRequest.Did;

                requestType.ReqClientID = reqClientIdType;
                
                /* Create the instance of the TransactionType that is a class generated from the Rapid connect Transaction 
                 * Service WSDL file [rc.wsdl]*/
                TransactionType transactionType = new TransactionType();
                
                /* Create the instance of the PayloadType that is a class generated from the Rapid connect Transaction 
                 * Service WSDL file [rc.wsdl]*/
                PayloadType payloadType = new PayloadType();
                
                /* Set pay load data*/
                payloadType.Encoding = PayloadTypeEncoding.cdata;
                payloadType.EncodingSpecified = true;
                
                /* Set pay load type as the actual XML request*/
                payloadType.Value = gmfRequest;     // Set payload - actual xml request

                /*set pay load of the transaction type object */
                transactionType.Payload = payloadType;
                
                /* Set Service ID of the tranasction type object*/
                transactionType.ServiceID = Convert.ToString(transactionRequest.ServiceId);
                
                /* Set transction value of the requet type object*/
                requestType.Transaction = transactionType;
                
                /* Set version of the request type object */
                requestType.Version = "3";
             
                String gmfResponse = null;
                
                /* Create the instance of the rcService that is a class generated from the Rapid connect Transaction 
                 * Service WSDL file [rc.wsdl]*/
                string responseCode = string.Empty;
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                ServiceURL.RcSoapServiceUrl = WebConfigurationManager.AppSettings["RcURL"];

                using (rcService service = new rcService())
                {
                    /* set the URL*/
                    service.Url = WebConfigurationManager.AppSettings["RcURL"];
                    service.UserAgent = WebConfigurationManager.AppSettings["UserAgent"];//"MTDATA v1.0";
                    
                    /*Execute the transaction to send the data.*/
                    ResponseType responseType = service.rcTransaction(requestType);

                    //responseType = null;
                    /* Parse the response*/
                    if ((responseType != null && responseType.Status != null) && (responseType.Status.StatusCode != null))
                    {
                        if (responseType.Status.StatusCode.Equals("OK"))
                        {
                            if ((responseType.TransactionResponse != null) && (responseType.TransactionResponse.Payload != null))
                            {
                                responseCode = responseType.TransactionResponse.ReturnCode;

                                if (responseType.TransactionResponse.Payload.Encoding == PayloadTypeEncoding.cdata)
                                {
                                    gmfResponse = responseType.TransactionResponse.Payload.Value;
                                }
                                else if (responseType.TransactionResponse.Payload.Encoding == PayloadTypeEncoding.xml_escape)
                                {
                                    if (responseType.TransactionResponse.Payload.Value != null)
                                    {
                                        gmfResponse = responseType.TransactionResponse.Payload.Value
                                            .Replace("&gt;", ">")
                                            .Replace("&lt;", "<")
                                            .Replace("&amp;", "&");
                                    }
                                    else
                                    {
                                        //203, 204, 205, 206, 405, 505, 008
                                        if (responseCode.Equals("203")
                                            || responseCode.Equals("204")
                                            || responseCode.Equals("205")
                                            || responseCode.Equals("206")
                                            || responseCode.Equals("405")
                                            || responseCode.Equals("505")
                                            || responseCode.Equals("008"))
                                        {
                                            gmfResponse = null;
                                        }
                                    }
                                }
                            }
                        }
                        else if (responseType.Status.StatusCode.Equals("AuthenticationError"))
                        {
                            gmfResponse = PaymentAPIResources.AuthenticationErrResponse;
                        }
                    }

                }

                /*Return the response*/
                return gmfResponse;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
