﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml;
using System.Web;
using System.Data;
using System.IO;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Web.Models;
using MTData.Web.Resource;
using MTData.Web.ViewData;
using MTData.Utility;
using MTData.Utility.Extension;

namespace MTData.Web.Controllers
{
    public class TerminalController : Controller
    {
        private readonly ILogger _logger = new Logger();
        private readonly ITerminalService _terminalService;
        private readonly ICommonService _commonService;
        private StringBuilder _logMessage;
        private string _datawireId;
        private string _status;
        private StringBuilder _logTracking;

        public TerminalController([Named("TerminalService")] ITerminalService terminalService,
            [Named("CommonService")] ICommonService commonService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _terminalService = terminalService;
            _logMessage = logMessage;
            _commonService = commonService;
            _logTracking = logTracking;
        }

        /// <summary>
        /// Purpose             :   to display the terminals 
        /// Function Name       :   Index
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/26/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            IsFirstLoginViewData obj = new IsFirstLoginViewData();
            if (!(bool)Session["Islogged"])
            {
                obj.IsFirstLogin = (bool)((SessionDto)Session["User"]).SessionUser.IsFirstLogin;
                obj.UserTypeId = Convert.ToString(((SessionDto)Session["User"]).SessionUser.fk_UserTypeID);

                Session.Remove("Islogged");
                Session["Islogged"] = true;

                obj.UserId = ((SessionDto)Session["User"]).SessionUser.UserID;
            }

            return View(obj);
        }

        /// <summary>
        /// Purpose             :   To export list in excel file
        /// Function Name       :   Index
        /// Created By          :   Umesh Kumar
        /// Created On          :   01/28/2016
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="deviceName"></param>
        /// <param name="serialNumber"></param>
        /// <param name="rapidConnectID"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(string deviceName, string serialNumber, string rapidConnectID)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling TerminalListByFiter method of _terminalService class to get terminal List");

                int listCount;
                var dto = (SessionDto)Session["User"];
                int merchantId = Convert.ToInt32(dto.SessionUser.fk_MerchantID);
                int userId = dto.SessionUser.UserID;
                int userType = dto.SessionUser.fk_UserTypeID;

                FilterSearchParametersDto filterSearch = new FilterSearchParametersDto();
                filterSearch.Name = deviceName;
                filterSearch.StartIndex = 0;
                filterSearch.PageSize = 0;
                filterSearch.Sorting = string.Empty;
                filterSearch.MerchantId = merchantId;
                filterSearch.UserId = userId;
                filterSearch.UserType = userType;
                filterSearch.RapidConnectID = rapidConnectID;
                filterSearch.SerialNumber = serialNumber;
                listCount = _terminalService.GetTerminalCount(filterSearch);
                filterSearch.PageSize = listCount;

                var viewDataList = _terminalService.TerminalListByFiter(filterSearch);
                
                if (userType == 4)
                {
                    listCount = _terminalService.GetUserTerminalCount(filterSearch);
                    filterSearch.PageSize = listCount;
                    viewDataList = _terminalService.GetUserTerminal(filterSearch);
                }

                if (listCount >= 1)
                {
                    using (StringWriter output = new StringWriter())
                    {
                        output.WriteLine("\"Device Name\",\"Serial No\",\"RC Terminal\",\"Android Mac Address\"");

                        foreach (TerminalDto item in viewDataList)
                        {
                            output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\"", item.DeviceName, item.SerialNo, item.Description, item.MacAdd);
                        }

                        _logTracking.Append("Transaction Report Excel file Downloaded by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                        _logger.LogInfoMessage(_logTracking.ToString());

                        return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Terminal.csv");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   creating terminal
        /// Function Name       :   Create
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/28/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            var terminalViewData = new TerminalViewData
            {
                DeviceList = _terminalService.GetDeviceList().ToDeviceViewData(),
                RegisteredTerminalList = RegTerminalList()
            };

            ViewBag.Project = ConfigurationManager.AppSettings["Project"];

            return View(terminalViewData);
        }

        /// <summary>
        /// Purpose             :   creating terminal
        /// Function Name       :   Create
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/28/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="terminalViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(TerminalViewData terminalViewData, HttpPostedFileBase uploadFile)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
            terminalViewData.RegisteredTerminalList = RegTerminalList();
            terminalViewData.DeviceList = _terminalService.GetDeviceList().ToDeviceViewData();
            var result = new ModelClientValidationRule();

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                
                if (terminalViewData.Contact == "1")
                    terminalViewData.IsContactLess = false;
                else if (terminalViewData.Contact == "2")
                    terminalViewData.IsContactLess = true;

                string project = ConfigurationManager.AppSettings["Project"];
                if (terminalViewData.TermId == null && project != MtdataResource.UKProject.ToString())
                    ModelState.AddModelError("TermId", MtdataResource.TerminalController_Create_Please_select_rapid_connect_terminal_id);

                if (uploadFile != null)
                {
                    if (!IsValidExcel(uploadFile))
                    {
                        terminalViewData.IsTerminalError = true;
                        List<TerminalExcelUploadViewData> db = new List<TerminalExcelUploadViewData>();
                        terminalViewData.TerminalExcelUploadViewData = db;
                        ViewBag.UploadMessage = MtdataResource.invalid_Excel_Fromat;
                        
                        ModelState.Clear();

                        return View(terminalViewData);
                    }
                }

                if (uploadFile != null)
                {
                    _logMessage.Append("Try to get device list from Excel file.");

                    List<TerminalExcelUploadViewData> lstTerminalViewData = UploadExcelFile(uploadFile);

                    _logMessage.Append("Get Device list from Excel file successfully.");
                    _logMessage.Append("Calling Add method of service _terminalService takes terminalDto parameters list.");

                    if (lstTerminalViewData != null && lstTerminalViewData.Count() > 0)
                    {
                        List<TerminalExcelUploadViewData> objTerminalExcelUploadViewData = _terminalService.AddDeviceList(lstTerminalViewData.ToTerminalDtoList()).ToTerminalViewDataList();
                        
                        _logMessage.Append("Get list of unauthorize device from uploaded excel file.");

                        terminalViewData.IsTerminalError = true;
                        terminalViewData.TerminalExcelUploadViewData = objTerminalExcelUploadViewData;

                        if (lstTerminalViewData != null && objTerminalExcelUploadViewData != null)
                        {
                            int uplodedTerminal = lstTerminalViewData.Count() - objTerminalExcelUploadViewData.Count();
                            if (uplodedTerminal > 0)
                            {
                                ViewBag.UploadMessage = uplodedTerminal + " terminal has been created from selected file. Below records have not been uploaded";
                                                                
                                string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                                _logTracking.Append("User " + userName + " create terminal through excel file" + uploadFile.FileName + " at " + System.DateTime.Now.ToUniversalTime());
                                _logger.LogInfoMessage(_logTracking.ToString());
                            }
                            else
                            {
                                ViewBag.UploadMessage = MtdataResource.Not_valid_File;
                            }
                        }
                    }
                    else
                    {
                        terminalViewData.IsTerminalError = true;
                        List<TerminalExcelUploadViewData> db = new List<TerminalExcelUploadViewData>();
                        terminalViewData.TerminalExcelUploadViewData = db;
                        ViewBag.UploadMessage = MtdataResource.No_recordFound;
                    }

                    ModelState.Remove("DeviceName");
                    ModelState.Remove("SerialNo");
                    ModelState.Remove("TermId");
                    ModelState.Remove("MacAdd");
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        bool isExist = _terminalService.IsValid(terminalViewData.MacAdd);
                        if (isExist)
                        {
                            ViewBag.Message = MtdataResource.Mac_Address_already_exists;
                            return View(terminalViewData);
                        }

                        _logMessage.Append("returning view");
                        _logMessage.Append("calling ToTerminalDto method to map TerminalDto to TerminalViewData and retrun TerminalDto");
                        
                        var terminalDto = terminalViewData.ToTerminalDto();
                        
                        _logMessage.Append("ToTerminalDto method executed successfully");
                        _logMessage.Append("taking CreatedBy and MerchantID from session");

                        terminalDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                        terminalDto.fk_MerchantID = merchantId;
                        
                        _logMessage.Append("CreatedBy=" + terminalDto.CreatedBy + " and MerchantID and " + terminalDto.fk_MerchantID + " from session");
                        _logMessage.Append("Calling Add method of service _terminalService takes terminalDto parameters are Device Name =" +
                            terminalViewData.DeviceName + ", IsActive= " + terminalViewData.IsActive + ", MacAdd= " +
                            terminalViewData.MacAdd + ",SerialNo= " + terminalViewData.SerialNo + ",MerchantID= " +
                            terminalViewData.fk_MerchantID + ", Description= " + terminalViewData.Description);

                        terminalDto.FileVersion = 0;
                        terminalDto.TAKeyFileVersion = 0;

                        bool response = _terminalService.Add(terminalDto, terminalViewData.TermId);
                        if (!response)
                        {
                            ViewBag.Message = MtdataResource.Terminal_could_not_be_configured;
                            return View(terminalViewData);
                        }

                        _logMessage.Append("Add method of service _terminalService excuted successfully");
                        
                        string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                        
                        _logTracking.Append("User " + userName + " create terminal" + terminalDto.SerialNo + " at " + System.DateTime.Now.ToUniversalTime());
                        _logger.LogInfoMessage(_logTracking.ToString());
                        
                        return Redirect("Index");
                    }

                    return View(terminalViewData);
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
                ModelState.Clear();
            }

            return View(terminalViewData);
        }

        /// <summary>
        /// Purpose              :   To update the terminal
        /// Function Name       :   Update
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/6/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Update(int terminalId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetTerminalDto method of _terminalService takes parameter terminalId =" + terminalId);
                
                var terminalDto = _terminalService.GetTerminalDto(terminalId);
                if (terminalDto == null)
                {
                    Session.Remove("User");
                    return RedirectToAction("index", "Member");
                }

                var fkMerchantId = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;
                if (fkMerchantId != null)
                {
                    var merchantId = (int)fkMerchantId;
                    if (terminalDto.fk_MerchantID != merchantId)
                    {
                        Session.Remove("User");
                        return RedirectToAction("index", "Member");
                    }
                }

                _logMessage.Append("Add GetTerminalDto of service _terminalService excuted successfully");
                _logMessage.Append("calling ToTerminalViewData method to map TerminalViewData to TerminalDto and retrun TerminalViewData");

                var terminal = terminalDto.ToTerminalViewData();

                _logMessage.Append("ToTerminalViewData method executed successfully");

                string rcTerminalId = _terminalService.GetRcTerminalId(terminalId);
                terminal.TermId = rcTerminalId;
                terminal.RegisteredTerminalList = RegTerminalListForUpdate(terminal.TermId);
                terminal.DeviceList = _terminalService.GetDeviceList().ToDeviceViewData();

                ViewBag.Project = ConfigurationManager.AppSettings["Project"];

                return View(terminal);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   To update the terminal
        /// Function Name       :   Update
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/06/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="terminalViewData"></param>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(TerminalViewData terminalViewData, int terminalId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                
                string project = ConfigurationManager.AppSettings["Project"];

                if (terminalViewData.TermId == null && project != MtdataResource.UKProject.ToString())
                    ModelState.AddModelError("TermId", MtdataResource.TerminalController_Create_Please_select_rapid_connect_terminal_id);

                if (terminalViewData.Contact == "1")
                    terminalViewData.IsContactLess = false;
                else if (terminalViewData.Contact == "2")
                    terminalViewData.IsContactLess = true;

                if (ModelState.IsValid)
                {
                    if (terminalViewData.TermId == null && project != MtdataResource.UKProject.ToString())
                    {
                        string rcTerminalId = _terminalService.GetRcTerminalId(terminalId);
                        terminalViewData.TermId = rcTerminalId;
                        terminalViewData.RegisteredTerminalList = RegTerminalListForUpdate(terminalViewData.TermId);
                        terminalViewData.DeviceList = _terminalService.GetDeviceList().ToDeviceViewData();

                        ModelState.AddModelError("TermId", MtdataResource.TerminalController_Create_Please_select_rapid_connect_terminal_id);
                        return View(terminalViewData);
                    }

                    bool isExist = _terminalService.IsMacAddReg(terminalId, terminalViewData.MacAdd);
                    if (isExist)
                    {
                        ViewBag.Message = MtdataResource.Mac_Address_already_exists;
                        terminalViewData.RegisteredTerminalList = RegTerminalListForUpdate(terminalViewData.TermId);
                        terminalViewData.DeviceList = _terminalService.GetDeviceList().ToDeviceViewData();
                        return View(terminalViewData);
                    }

                    _logMessage.Append("calling ToTerminalDto method to map TerminalDto to TerminalViewData and retrun TerminalDto");

                    var terminalDto = terminalViewData.ToTerminalDto();

                    _logMessage.Append("ToTerminalDto method executed successfully");

                    terminalDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                    terminalDto.fk_MerchantID = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);

                    _logMessage.Append("ModifiedBy=" + terminalDto.ModifiedBy + " from session");
                    _logMessage.Append(
                        "Calling Update method of service _terminalService takes terminalDto parameters are Device Name =" +
                        terminalViewData.DeviceName + ", IsActive= " + terminalViewData.IsActive + ", MacAdd= " +
                        terminalViewData.MacAdd + ",SerialNo= " + terminalViewData.SerialNo + ",MerchantID= " +
                        terminalViewData.fk_MerchantID + ", Description= " + terminalViewData.Description +
                        " and terminal id =" + terminalId);
                    
                    _terminalService.Update(terminalDto, terminalId, terminalViewData.TermId);

                    _logMessage.Append("Update method of service _terminalService excuted successfully");

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + " update terminal" + terminalDto.SerialNo + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                    
                    return Redirect("Index");
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            string rcTerminal = _terminalService.GetRcTerminalId(terminalId);
            terminalViewData.TermId = rcTerminal;
            terminalViewData.RegisteredTerminalList = RegTerminalListForUpdate(terminalViewData.TermId);
            terminalViewData.DeviceList = _terminalService.GetDeviceList().ToDeviceViewData();

            return View(terminalViewData);
        }

        /// <summary>
        /// Purpose             :   To filter the terminal list
        /// Function Name       :   TerminalListByFiter
        /// Created By          :   Naveen Kumar
        /// Created On          :   2/4/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult TerminalListByFiter(string name = null, string rapidConnectID = "", string serialNumber = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling TerminalListByFiter method of _terminalService class to get terminal List");

                int listCount;
                name = name.Trim();

                _logMessage.Append(string.Format("Name:{0};RapidConnectID:{1};SerialNumber:{2};", name, rapidConnectID, serialNumber));
                _logMessage.Append(string.Format("MerchantID:{0};UserID:{1};UserType:{2};", (((SessionDto)Session["User"]).SessionUser.fk_MerchantID) == null ? "<NULL>" : (((SessionDto)Session["User"]).SessionUser.fk_MerchantID).ToString(),
                                                                                            (((SessionDto)Session["User"]).SessionUser.UserID) == null ? "<NULL>" : (((SessionDto)Session["User"]).SessionUser.UserID).ToString(),
                                                                                            (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID) == null ? "<NULL>" : (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID).ToString()));

                int merchantId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.fk_MerchantID));
                int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                
                FilterSearchParametersDto filterSearch = new FilterSearchParametersDto()
                {
                    Name = name,
                    StartIndex = jtStartIndex,
                    PageSize = jtPageSize,
                    Sorting = jtSorting,
                    MerchantId = merchantId,
                    UserId = userId,
                    UserType = userType,
                    RapidConnectID = rapidConnectID,
                    SerialNumber = serialNumber
                };
                
                var viewDataList = _terminalService.TerminalListByFiter(filterSearch);
                listCount = _terminalService.GetTerminalCount(filterSearch);

                if (userType == 4)
                {
                    viewDataList = _terminalService.GetUserTerminal(filterSearch);
                    listCount = _terminalService.GetUserTerminalCount(filterSearch);
                }

                if (string.IsNullOrEmpty(name))
                {
                    _logMessage.Append("Calling GetTerminalDtoList method of _terminalService class to count fee");
                    _logMessage.Append("GetTerminalDtoList method of _terminalService executed successfully result = " + listCount);

                    return Json(new { Result = "OK", Records = viewDataList, TotalRecordCount = listCount });
                }

                var terminalDtos = viewDataList as TerminalDto[] ?? viewDataList.ToArray();
                
                return Json(new { Result = "OK", Records = terminalDtos, TotalRecordCount = listCount });
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
                return Json(new { Result = "ERROR", exception.Message });
            }
        }

        /// <summary>
        /// Purpose             :   To delete the vehicle
        /// Function Name       :   Delete
        /// Created By          :   Naveen Kumar
        /// Created On          :  02/06/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(int terminalId)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                try
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    _logMessage.Append("Calling DeleteTerminal method of _terminalService class takes terminalId= " + terminalId);

                    string modifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                    string lblMessage = _terminalService.DeleteTerminal(terminalId, modifiedBy);

                    if (lblMessage != null)
                    {
                        _logMessage.Append("You can't delete this terminal because this terminal is assigned to Vehicle Number:" + lblMessage);
                        var msg = "You can't delete this terminal because this terminal is assigned to Vehicle Number:" + lblMessage;
                        return Json(new { Result = "No", Message = msg });
                    }

                    _logMessage.Append("DeleteTerminal method of _terminalService class executed successfully");

                    Thread.Sleep(50);

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + " delete terminal ID" + terminalId + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return Json(new { Result = "OK" });
                }
                catch (Exception exception)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), exception);
                }
            }

            return Json(new { Result = "No" });
        }

        /// <summary>
        /// Purpose             :   To get unregistered terminal list
        /// Function Name       :   UnRegTerminalList
        /// Created By          :   Naveen Kumar
        /// Created On          :    08/04/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<RapidConnectViewData> RegTerminalList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetRegisteredTerminalList method of _commonService class to get registered terminal list");

                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                var terminalListDto = _commonService.GetRegisteredTerminalList(merchantId);

                _logMessage.Append("GetUnRegisteredTerminalList method of _commonService executed successfully");
                _logMessage.Append("calling ToUnRegisteredTerminalViewDataList method and retrun UnRegisteredTerminalViewData list");

                var terminalViewDataList = terminalListDto.ToRegisteredTerminalViewDataList();

                _logMessage.Append("ToCountryViewDataList method executed successfully");

                return terminalViewDataList;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

        /// <summary>
        /// Purpose             :   To add rapid connect terminal Id for datawire registration
        /// Function Name       :   AddRcTerminal
        /// Created By          :   Naveen Kumar
        /// Created On          :    09/04/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public ActionResult AddRcTerminal()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            return View();
        }

        /// <summary>
        /// Purpose             :   To add rapid connect terminal Id for datawire registration
        /// Function Name       :   AddRcTerminal
        /// Created By          :   Naveen Kumar
        /// Created On          :    09/04/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="rapidConnectViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddRcTerminal(RapidConnectViewData rapidConnectViewData)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (Session["User"] == null) return RedirectToAction("Index", "Member");
                int merchantId = 0;
                if (rapidConnectViewData.RCTerminalId == null)
                {
                    ModelState.AddModelError("RCTerminalId", MtdataResource.TerminalController_AddRcTerminal_Please_enter_rapid_connect_terminal_id);

                    _logger.LogInfoMessage(_logMessage.ToString());

                    return View();
                }

                rapidConnectViewData.RCTerminalId = StringExtension.FormatTerminalId(rapidConnectViewData.RCTerminalId);
                _logMessage.Append("Calling IsExist method of service _terminalService to check rapid connect terminal id exists or not.");

                bool isRcExist = _terminalService.IsExist(rapidConnectViewData.RCTerminalId);
                if (isRcExist)
                {
                    _logMessage.Append("Rapid connect terminal id already registered in the database");

                    ViewBag.Message = MtdataResource.Merchant_already_provisioned;

                    _logger.LogInfoMessage(_logMessage.ToString());

                    return View();
                }

                var httpCookie = Request.Cookies["CookieViewAs"];
                if (httpCookie != null)
                    merchantId = Convert.ToInt32(httpCookie["merchantId"]);

                var datawireRequest = new DatawireRequest();
                datawireRequest.TerminalId = rapidConnectViewData.RCTerminalId;
                datawireRequest.MerchantId = merchantId;
                string serviceUrl = ConfigurationManager.AppSettings["MyAccountBaseUrl"];

                _logMessage.Append("Calling DatawireRegistration method in the Payment Controller of web API service to register the rapid connect terminal with datawire.");

                dynamic httpResponse;
                if (ConfigurationManager.AppSettings["Project"] == MtdataResource.CanadaProject)
                {
                    httpResponse = WebClient.DoPostDatawireRegister<DatawireRequest>(String.Format("{0}api/Payment/DatawireRegistrationCN", serviceUrl), datawireRequest);

                    _logMessage.Append(string.Format("Response RawText:{0};", httpResponse.RawText));

                    XmlDocument xmlDocm = new XmlDocument();
                    xmlDocm.LoadXml(httpResponse.RawText);

                    _logMessage.Append(string.Format("xmlDocm.OuterXml:{0};", xmlDocm.OuterXml));

                    var error = xmlDocm.FirstChild;
                    if (error != null)
                    {
                        string text = error.InnerText;
                        if (ListContainsAnyError(text))
                        {
                            _logMessage.Append(string.Format("Datawire Registration error:{0};", text));
                            _logger.LogInfoMessage(_logMessage.ToString());

                            ViewBag.Message = string.Format("Registration Error: {0}.", text);
                            return View();
                        }
                    }
                }
                else
                {
                    httpResponse = WebClient.DoPostDatawireRegister<DatawireRequest>(String.Format("{0}api/Payment/DatawireRegistration", serviceUrl), datawireRequest);
                }

                var datawireResponse = httpResponse.RawText;
                _logMessage.Append("Raw Text of the http response :" + datawireResponse.ToString());

                string dataResp = String.Empty;

                if (datawireResponse != null)
                {
                    if (ConfigurationManager.AppSettings["Project"] != MtdataResource.CanadaProject)
                    {
                        datawireResponse = Regex.Replace(datawireResponse, "xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"", string.Empty);

                        XmlDocument xmlDoc = new XmlDocument();
                        using (MemoryStream stream = new MemoryStream(Encoding.Default.GetBytes(datawireResponse.ToString())))
                        {
                            XmlReaderSettings settings = new XmlReaderSettings();
                            settings.DtdProcessing = DtdProcessing.Prohibit;
                            using (XmlReader reader = XmlReader.Create(stream, settings))
                            {
                                try
                                {
                                    xmlDoc.Load(reader);
                                }
                                catch (XmlException exception)
                                {
                                    _logger.LogInfoFatal(_logMessage.ToString(), exception);
                                }
                            }
                        }

                        dataResp = xmlDoc.InnerXml;
                        var status = xmlDoc.SelectSingleNode("DatawireResponse/Status");
                        
                        if (status != null)
                            _status = status.InnerText;
                        
                        var did = xmlDoc.SelectSingleNode("DatawireResponse/Did");
                        
                        if (did != null)
                            _datawireId = did.InnerText;
                    }
                    else
                    {
                        var cnDatawireResp = GetCnDatawireResponse.GetDatawireResponse(datawireResponse);
                        _status = cnDatawireResp.StatusCode;
                        _datawireId = cnDatawireResp.DID;
                        dataResp = cnDatawireResp.CompleteResponse;
                    }

                    if (_status == MtdataResource.AuthenticationError)
                    {
                        _logMessage.Append("Checking status tag in the datawire response.The status is :" + _status);

                        ViewBag.Message = MtdataResource.Merchant_not_authroized;

                        _logger.LogInfoMessage(_logMessage.ToString());

                        return View();
                    }
                    else if (_status == MtdataResource.AccessDenied)
                    {
                        _logMessage.Append("Checking status tag in the datawire response.The status is :" + _status);

                        ViewBag.Message = MtdataResource.Access_Denied;

                        _logger.LogInfoMessage(_logMessage.ToString());

                        return View();
                    }
                    else if (_status != MtdataResource.OK)
                    {
                        _logMessage.Append("Checking status tag in the datawire response.The status is :" + _status);

                        ViewBag.Message = MtdataResource.Problem_in_registering_terminal;

                        _logger.LogInfoMessage(_logMessage.ToString());

                        return View();
                    }
                    else if (_status == MtdataResource.OK && ConfigurationManager.AppSettings["Project"] == MtdataResource.CanadaProject)
                    {
                        ViewBag.Message = MtdataResource.CnTerminalRegistered;
                        return View();
                    }

                    _logMessage.Append("Getting datawire id from the Did tag.The DID is :" + _datawireId);

                    rapidConnectViewData.MerchantId = merchantId;
                    rapidConnectViewData.CreatedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                    rapidConnectViewData.DatewireXml = dataResp;
                    rapidConnectViewData.DID = _datawireId;

                    _logMessage.Append("Calling ToRcTerminalDto method to convert RapidConnectViewData to RapidConnectDto");

                    var rapidConnectDto = rapidConnectViewData.ToRcTerminalDto();

                    _logMessage.Append("Calling AddRcTerminal method to add the datawire details in the database.");

                    _terminalService.AddRcTerminal(rapidConnectDto);

                    _logMessage.Append("AddRcTerminal in the terminal controller executed successfully.");

                    ViewBag.Message = MtdataResource.Rapid_connect_terminal_registered;
                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;

                    _logMessage.Append("User " + userName + " registered RCTerminal" + rapidConnectDto.RCTerminalId + " at " + System.DateTime.Now.ToUniversalTime());

                    _logger.LogInfoMessage(_logMessage.ToString());

                    return View();
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            ViewBag.Message = MtdataResource.Problem_in_registering_terminal;

            _logMessage.Append("ViewBag Message:" + MtdataResource.Problem_in_registering_terminal);
            _logger.LogInfoMessage(_logMessage.ToString());

            return View();
        }

        /// <summary>
        /// Purpose             :   To get list of registered terminal id's including current rapid connect terminal id 
        /// Function Name       :   RegTerminalListForUpdate
        /// Created By          :   Naveen Kumar
        /// Created On          :    04/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="termId"></param>
        /// <returns></returns>
        public IEnumerable<RapidConnectViewData> RegTerminalListForUpdate(string termId)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetRegisteredTerminalList method of _commonService class to get registered terminal list");

                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                var terminalListDto = _commonService.GetRegisteredTerminalList(merchantId);

                _logMessage.Append("GetUnRegisteredTerminalList method of _commonService executed successfully");
                _logMessage.Append("calling ToUnRegisteredTerminalViewDataList method and retrun UnRegisteredTerminalViewData list");

                List<RapidConnectViewData> terminalViewDataList = terminalListDto.ToRegisteredTerminalViewDataList().ToList();

                _logMessage.Append("ToCountryViewDataList method executed successfully");

                terminalViewDataList.Add(new RapidConnectViewData { RCTerminalId = termId });

                return terminalViewDataList;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DeviceIndex()
        {
            if (Session["User"] != null)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                    return View();
            }

            return RedirectToAction("index", "Member");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DeviceCreate()
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("index", "Member");

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeviceCreate(DeviceNameListViewData deviceViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("index", "Member");

            try
            {
                if (ModelState.IsValid)
                {
                    deviceViewData.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                    var deviceDto = deviceViewData.ToDeviceDto();
                    bool result = _terminalService.AddDevicename(deviceDto);

                    if (result)
                    {
                        string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                        _logTracking.Append("User " + userName + " create device" + deviceDto.DeviceName + " at " + System.DateTime.Now.ToUniversalTime());
                        _logger.LogInfoMessage(_logTracking.ToString());

                        return RedirectToAction("DeviceIndex");
                    }

                    ViewBag.Message = MtdataResource.Device_already_exists;

                    return View();
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DeviceUpdate(int deviceId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("index", "Member");

            var deviceDto = _terminalService.GetDevice(deviceId);
            var deviceViewData = deviceDto.ToDeviceView();
            return View(deviceViewData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceViewData"></param>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeviceUpdate(DeviceNameListViewData deviceViewData, int deviceId)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("index", "Member");
            try
            {
                if (ModelState.IsValid)
                {
                    deviceViewData.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                    var deviceDto = deviceViewData.ToDeviceDto();
                    _terminalService.UpdateDevicename(deviceDto, deviceId);

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + " update device" + deviceDto.DeviceName + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return RedirectToAction("DeviceIndex");
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="payTerminalId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeviceDelete(int payTerminalId)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                try
                {
                    bool result = _terminalService.DeleteDevicename(payTerminalId);
                    if (result)
                    {
                        var msg = "Device name already in use";
                        return Json(new { Result = "No", Message = msg });
                    }

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + " delete device ID" + payTerminalId + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return Json(new { Result = "OK" });
                }
                catch (Exception exception)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), exception);
                }
            }

            return Json(new { Result = "No" });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult GetDeviceListByFilter(string name = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling DeviceListByFiter method of _terminalService class to get Admin Users List");

                int viewDataListCount;
                var terminalList = _terminalService.DeviceListByFiter(name, jtStartIndex, jtPageSize, jtSorting);
                if (string.IsNullOrEmpty(name))
                {
                    _logMessage.Append("Calling GetDeviceList method of _terminalService class to count devices");

                    viewDataListCount = _terminalService.GetDeviceListCount(name);

                    _logMessage.Append("GetDeviceList method of _terminalService executed successfully result = " + viewDataListCount);

                    return Json(new { Result = "OK", Records = terminalList, TotalRecordCount = viewDataListCount });
                }

                var terminalDtos = terminalList as DeviceNameListDto[] ?? terminalList.ToArray();
                viewDataListCount = _terminalService.GetDeviceListCount(name);

                return Json(new { Result = "OK", Records = terminalDtos, TotalRecordCount = viewDataListCount });
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
                return Json(new { Result = "ERROR", exception.Message });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uploadFile"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public List<TerminalExcelUploadViewData> UploadExcelFile(HttpPostedFileBase uploadFile)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
            List<TerminalExcelUploadViewData> objTerminalViewData = new List<TerminalExcelUploadViewData>();

            try
            {
                if (uploadFile.ContentLength > 0)
                {
                    if (!Directory.Exists(Server.MapPath("../Uploads")))
                        Directory.CreateDirectory(Server.MapPath("../Uploads"));

                    string filePath = Path.Combine(HttpContext.Server.MapPath("../Uploads"), Path.GetFileName(uploadFile.FileName));
                    uploadFile.SaveAs(filePath);

                    string line = string.Empty;
                    string[] strArray = null;

                    // work out where we should split on comma, but not in a sentance
                    Regex r = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

                    //Set the filename in to our stream
                    StreamReader sr = new StreamReader(filePath);

                    //Read the first line and split the string at , with our regular express in to an array
                    line = sr.ReadLine();
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (!string.IsNullOrEmpty(line))
                            strArray = r.Split(line);

                        TerminalExcelUploadViewData Obj = new TerminalExcelUploadViewData();
                        Obj.DeviceName = strArray[0].Trim();
                        Obj.SerialNo = strArray[1].Trim();
                        Obj.TermId = strArray[2].Trim();
                        Obj.MacAdd = strArray[3].Trim();
                        Obj.Description = strArray[4].Trim();
                        Obj.fk_MerchantID = merchantId;
                        Obj.CreatedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                        Obj.ModifiedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                        objTerminalViewData.Add(Obj);
                    }

                    sr.Dispose();
                }

                return objTerminalViewData;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return null;
            }
        }

        /// <summary>
        /// Process the file supplied and process the CSV to a dynamic datatable
        /// </summary>
        /// <param name="fileName">String</param>
        /// <returns>DataTable</returns>
        private DataTable ProcessCSV(string fileName)
        {
            DataTable dt = new DataTable();
            try
            {
                //Set up our variables 
                string line = string.Empty;
                string[] strArray = null;

                DataRow row;

                // work out where we should split on comma, but not in a sentance
                Regex r = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

                //Set the filename in to our stream
                StreamReader sr = new StreamReader(fileName);

                //Read the first line and split the string at , with our regular express in to an array
                line = sr.ReadLine();
                if (!string.IsNullOrEmpty(line))
                    strArray = r.Split(line);

                //For each item in the new split array, dynamically builds our Data columns. Save us having to worry about it.
                Array.ForEach(strArray, s => dt.Columns.Add(new DataColumn()));

                //Read each line in the CVS file until it's empty
                while ((line = sr.ReadLine()) != null)
                {
                    row = dt.NewRow();

                    row.ItemArray = r.Split(line);
                    dt.Rows.Add(row);
                }

                sr.Dispose();

                return dt;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return dt;
        }

        /// <summary>
        /// Method for download excel file format for uploading terminal list
        /// </summary>
        /// <param name="fileName">String</param>
        /// <returns>DataTable</returns>
        public FileResult Download(string file)
        {
            string CurrentFileName = (from fls in GetFiles()
                                      where fls.FileName == file
                                      select fls.FilePath).First();

            string contentType = string.Empty;

            if (CurrentFileName.Contains(".pdf"))
                contentType = "application/pdf";
            else if (CurrentFileName.Contains(".docx"))
                contentType = "application/docx";
            else if (CurrentFileName.Contains(".csv"))
                contentType = "application/csv";

            return File(CurrentFileName, contentType, file);
        }

        /// <summary>
        /// Get All file list
        /// </summary>
        /// <param name="fileName">String</param>
        /// <returns>DataTable</returns>
        public List<DownloadFileViewData> GetFiles()
        {
            List<DownloadFileViewData> lstFiles = new List<DownloadFileViewData>();
            DirectoryInfo dirInfo = new DirectoryInfo(Server.MapPath("../Assets/TemplateFiles"));

            int i = 0;
            foreach (var item in dirInfo.GetFiles())
            {
                lstFiles.Add(new DownloadFileViewData()
                {
                    FileId = i + 1,
                    FileName = item.Name,
                    FilePath = dirInfo.FullName + @"\" + item.Name
                });

                i = i + 1;
            }

            return lstFiles;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uploadFile"></param>
        /// <returns></returns>
        private bool IsValidExcel(HttpPostedFileBase uploadFile)
        {
            string line = string.Empty;
            string filePath = string.Empty;
            if (uploadFile.ContentLength > 0)
            {
                if (!Directory.Exists(Server.MapPath("../Uploads")))
                    Directory.CreateDirectory(Server.MapPath("../Uploads"));

                filePath = Path.Combine(HttpContext.Server.MapPath("../Uploads"),
                Path.GetFileName(uploadFile.FileName));
                uploadFile.SaveAs(filePath);
            }

            StreamReader sr = new StreamReader(filePath);
            string CarExcel = "DeviceName,SerialNumber,TerminalId,MacAddress,Description";
            
            line = sr.ReadLine();
            sr.Dispose();
            if (!string.IsNullOrEmpty(line))
            {
                if (CarExcel != line)
                    return false;
                else
                    return true;
            }

            return false;
        }

        private bool ListContainsAnyError(string error)
        {
            List<string> list = new List<string> { MtdataResource.AccessDenied, MtdataResource.AuthenticationError, MtdataResource.TransactionCancel, MtdataResource.NoTicketProvisioned };
            if (list.Contains(error))
                return true;

            return false;
        }

    }
}


