//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MTD.Data.Data
{
    using System;
    
    public partial class usp_CardTypeReportScheduler_Result
    {
        public string Column1 { get; set; }
        public Nullable<int> Column2 { get; set; }
        public Nullable<System.DateTime> Column3 { get; set; }
        public Nullable<System.DateTime> Column4 { get; set; }
        public Nullable<int> Column5 { get; set; }
        public Nullable<int> Column6 { get; set; }
        public Nullable<int> Column7 { get; set; }
        public Nullable<int> Column8 { get; set; }
        public Nullable<int> Column9 { get; set; }
        public Nullable<int> Column10 { get; set; }
        public string Column11 { get; set; }
        public string Column12 { get; set; }
        public string Column13 { get; set; }
        public string Column14 { get; set; }
        public string Column15 { get; set; }
        public string Column16 { get; set; }
        public Nullable<System.DateTime> Column17 { get; set; }
    }
}
