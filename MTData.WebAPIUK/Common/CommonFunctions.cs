﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MTData.WebAPIUK.Models;
using MTData.Utility;
using MTData.WebAPIUK.Resources;

namespace MTData.WebAPIUK.Common
{
    public sealed class CommonFunctions
    {
        public static LoginHeader FailedResponse(string responseMessage, string deviceID = null, string requestID = null, string status = null)
        {
            try
            {
                LoginHeader mHeader = new LoginHeader();
                if (!string.IsNullOrEmpty(responseMessage))
                {
                    mHeader.Status = status.IsNull() ? PaymentAPIResources.Failed : status;
                    mHeader.DeviceId = deviceID;
                    mHeader.RequestId = requestID;
                    mHeader.Message = responseMessage;
                }
                return mHeader;
            }
            catch
            {
                throw;
            }
        }
    }
}