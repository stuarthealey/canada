﻿using System;
using System.Text;
using System.Web;

using MTData.Utility;

namespace MTData.WCFGW.Model
{
    public class SoapActionRewriter : IHttpModule
    {
        ILogger _logger = new Logger();
        StringBuilder _logMessage = new StringBuilder();

        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += context_BeginRequest;
        }

        void context_BeginRequest(object sender, EventArgs e)
        {
            try
            {
                _logMessage.AppendLine("Entered in SoapActionRewriter class");

                HttpApplication application = (HttpApplication)sender;
                HttpRequest request = application.Context.Request;

                if (!string.IsNullOrWhiteSpace(request.Url.Query) && request.Url.Query.Contains("op=CreateRCCI"))
                {
                    _logMessage.AppendLine("SOAP12 request with op=CreateRCCI");
                    if (request.ContentType.StartsWith("application/soap+xml;") && !request.ContentType.Contains(" action="))
                    {
                        if (!request.ContentType.EndsWith(";"))
                            request.ContentType = request.ContentType + ";";

                        request.ContentType = request.ContentType + " action=\"http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService/IPaymentGatewayCreditCardService/CreateRCCI\"";

                        _logMessage.AppendLine("handling CreateRCCI soap12 request with op=CreateRCCI");
                    }
                }

                if (!string.IsNullOrWhiteSpace(request.Url.Query) && request.Url.Query.Contains("op=ValidateRCCI"))
                {
                    _logMessage.AppendLine("SOAP12 request with op=ValidateRCCI");
                    if (request.ContentType.StartsWith("application/soap+xml;") && !request.ContentType.Contains(" action="))
                    {
                        if (!request.ContentType.EndsWith(";"))
                            request.ContentType = request.ContentType + ";";

                        request.ContentType = request.ContentType + "action=\"http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService/IPaymentGatewayCreditCardService/ValidateRCCI\"";
                        _logMessage.AppendLine("handling CreateRCCI soap12 request with op=ValidateRCCI");
                    }
                }

                if (request.ContentType.Contains("text/xml"))
                {
                    if (!request.Path.Contains("soap11"))
                    {
                        _logMessage.AppendLine("soap11 request");
                        string soapAction = request.Headers["SOAPAction"];
                        if (soapAction != null)
                        {
                            if (soapAction.Contains("CreateRCCI"))
                            {
                                _logMessage.AppendLine("CreateRCCI soap11 request");
                                application.Context.RewritePath(application.Context.Request.Path + "/soap11");
                                request.Headers.Remove("SOAPAction");
                                request.Headers.Add("SOAPAction", "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService/IPaymentGatewayCreditCardService/CreateRCCI");
                                _logMessage.AppendLine("CreateRCCI soap11 request, URL mapped");
                            }
                            else if (soapAction.Contains("ValidateRCCI"))
                            {
                                _logMessage.AppendLine("ValidateRCCI soap11 request");
                                application.Context.RewritePath(application.Context.Request.Path + "/soap11");
                                request.Headers.Remove("SOAPAction");
                                request.Headers.Add("SOAPAction", "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService/IPaymentGatewayCreditCardService/ValidateRCCI");
                                _logMessage.AppendLine("ValidateRCCI soap11 request, URL mapped");
                            }
                        }
                    }
                }
                else if (request.ContentType.Contains("application/soap+xml"))
                {
                    _logMessage.AppendLine("soap12 request");
                    string soapAction = request.Headers["SOAPAction"];
                    if (soapAction != null)
                    {
                        if (soapAction.Contains("CreateRCCI"))
                        {
                            _logMessage.AppendLine("CreateRCCI soap12 request");
                            request.ContentType = "application/soap+xml;charset=UTF-8;action=\"http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService/IPaymentGatewayCreditCardService/CreateRCCI\"";
                            _logMessage.AppendLine("CreateRCCI soap12 request, URL mapped");
                        }
                        else if (soapAction.Contains("ValidateRCCI"))
                        {
                            _logMessage.AppendLine("ValidateRCCI soap12 request");
                            request.ContentType = "application/soap+xml;charset=UTF-8;action=\"http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService/IPaymentGatewayCreditCardService/ValidateRCCI\"";
                            _logMessage.AppendLine("ValidateRCCI soap12 request, URL mapped");
                        }
                    }
                }

                _logMessage.AppendLine("Process complete. Exiting from SoapActionRewriter class");
                _logger.LogInfoMessage(_logMessage.ToString());
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }
        }
    }
}