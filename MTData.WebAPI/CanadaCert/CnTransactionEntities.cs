﻿using System;

using MTData.WebAPI.CanadaCommon;

namespace MTData.WebAPI.CanadaCert
{    
    public class CnTransactionEntities : ICloneable
    {
        #region public properties

        #region Bitmap Public Properties
        /// <summary>
        /// Bitmap 2
        /// </summary>
        public string PrimaryAccountNumber { get; set; }

        /// <summary>
        /// Bitmap 3
        /// </summary>
        public string ProcessingCode { get; set; }

        /// <summary>
        /// Bitmap 4
        /// </summary>
        public string AmountOfTransaction { get; set; }

        /// <summary>
        /// Bitmap 7 format (MMDDhhmmss)
        /// </summary>
        public string TransmissionDateTime { get; set; }

        /// <summary>
        /// Bitmap 11
        /// </summary>
        public string SystemTrace { get; set; }

        /// <summary>
        /// Bitmap 12 format (hhmmss)
        /// </summary>
        public string TimeLocalTransmission { get; set; }

        /// <summary>
        /// Bitmap 13 format (MMDD)
        /// </summary>
        public string DateLocalTrans { get; set; }

        /// <summary>
        /// Bitmap 14 format (YYMM) default 0000
        /// </summary>
        public string CardExpirationDate { get; set; }

        /// <summary>
        /// Bitmap 18
        /// </summary>
        public string MerchantCategoryCode { get; set; }

        /// <summary>
        /// Bitmap 22
        /// </summary>
        public string PosEntryModePinCapability { get; set; }

        /// <summary>
        /// Bitmap 23 range 000 -099
        /// </summary>
        public string CardSequenceNumber { get; set; }

        /// <summary>
        /// Bitmap 24
        /// </summary>
        public string NetworkInternationId { get; set; }

        /// <summary>
        /// Bitmap 25
        /// </summary>
        public string PosConditionCode { get; set; }

        /// <summary>
        /// Bitmap 31
        /// </summary>
        public string AcquirerReference { get; set; }

        /// <summary>
        /// Bitmap 32 depricated 
        /// Default value is 000000000001
        /// </summary>
        public string AcquiringId { get; set; }

        /// <summary>
        /// Bitmap 35
        /// </summary>
        public string Track2Data { get; set; }

        /// <summary>
        /// Bitmap 37
        /// </summary>
        public string RetrievalRefNumber { get; set; }

        /// <summary>
        /// Bitmap 38
        /// </summary>
        public string AuthIdentificationResponse { get; set; }

        /// <summary>
        /// Bitmap 39
        /// </summary>
        public string ResponseCode { get; set; }

        /// <summary>
        /// Bitmap 41
        /// </summary>
        public string TerminalId { get; set; }

        /// <summary>
        /// Bitmap 42
        /// </summary>
        public string MerchantId { get; set; }

        /// <summary>
        /// Bitmap 43
        /// </summary>
        public string AltMerchantNameOrLocation { get; set; }

        /// <summary>
        /// Bitmap 44
        /// </summary>
        public string AdditionalResponseData { get; set; }

        /// <summary>
        /// Bitmap 45
        /// </summary>
        public string Track1Data { get; set; }

        /// <summary>
        /// Bitmap 48
        /// </summary>
        public string FDPrivateUsageData48 { get; set; }

        /// <summary>
        /// Bitmap 49
        /// </summary>
        public string TransactionCurrencyCode { get; set; }

        /// <summary>
        /// Bitmap 52
        /// </summary>
        public string EncryptedPinData { get; set; }

        /// <summary>
        /// Bitmap 54
        /// </summary>
        public string AdditionalAmount { get; set; }

        /// <summary>
        /// Bitmap 55
        /// </summary>
        public string ICCData { get; set; }

        /// <summary>
        /// Bitmap 59
        /// </summary>
        public string MerchantZipCode { get; set; }

        /// <summary>
        /// Bitmap 60
        /// </summary>
        public string AdditionalPosData { get; set; }

        /// <summary>
        /// Bitmap 62
        /// </summary>
        public string FDPrivateUsageData62 { get; set; }

        /// <summary>
        /// Bitmap 63
        /// </summary>
        public string FDPrivateUsageData63 { get; set; }

        /// <summary>
        /// Bitmap 70 mandatory for 0800/08100
        /// </summary>
        public string NetworkInfoCode { get; set; }

        /// <summary>
        /// Bitmap 93 mandatory for 0810
        /// </summary>
        public string ResponseIndicator { get; set; }

        /// <summary>
        /// Bitmap 96
        /// </summary>
        public string KeyManagementData { get; set; }

        /// <summary>
        /// Bitmap 100
        /// </summary>
        public string ReceivingInstitutionId { get; set; }

        #endregion

        #region Other Properties

        /// <summary>
        /// gets the card type 
        /// Credit/Debit/EBT/None(Cash)
        /// </summary>
        public CardType CardType { get; set; }

        /// <summary>
        /// gets or sets the card cvv information
        /// </summary>
        public string CVVCode { get; set; }

        /// <summary>
        /// gets or sets the Card Issuer type.
        /// </summary>
        public CardIssuer CardIssuer { get; set; }

        /// <summary>
        /// gets or sets the complete transaction request.
        /// </summary>
        public string CompleteRequest { get; set; }

        /// <summary>
        /// gets or sets the complete transaction response.
        /// </summary>
        public string CompleteResponse { get; set; }

        /// <summary>
        /// gets or sets the total authorized amount.
        /// </summary>
        public string TotalAuthAmount { get; set; }

        /// <summary>
        /// gets or sets the bit63 reversal data.
        /// </summary>
        public string ReversalBit63 { get; set; }

        /// <summary>
        /// Get and set the MOTO/Bill Payment/Electronic Commerce Type Indicator.
        /// </summary>
        public string MotoBillECommIndicator { get; set; }

        /// <summary>
        /// gets or sets the value of MarketSpecificDataIndicator.
        /// </summary>
        public string MarketSpecificDataIndicator { get; set; }

        /// <summary>
        /// gets or sets the RequestTransactionId.
        /// </summary>
        public long RequestTransactionId { get; set; }

        /// <summary>
        /// gets or sets the TransactionId.
        /// </summary>
        public long TransactionId { get; set; }

        /// <summary>
        /// gets or sets the Token.
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Get and set OriginalTransactionId.
        /// Required for subsequent transactions.
        /// </summary>
        public long OriginalTransactionId { get; set; }

        /// <summary>
        /// Get and set whether Merchant support Existing Debt Indicator.
        /// </summary>
        public bool IsSupprotExistingDebtIndicator { get; set; }

        /// <summary>
        /// Get and set whether Existing Debt Indicator sent or not.
        /// </summary>
        public bool IsSentExistingDebtIndicator { get; set; }

        /// <summary>
        /// Get and set whether the transaction is a reversal transaction or not.
        /// </summary>
        public bool IsReversalTransaction { get; set; }

        /// <summary>
        /// get and set whether the transaction is a full reversal or not.
        /// </summary>
        public bool IsFullReversal { get; set; }

        /// <summary>
        /// get and set the Industry type.
        /// </summary>
        public IndustryType IndustryType { get; set; }

        /// <summary>
        /// get and set transaction counter.
        /// </summary>
        public string TransactionCounter { get; set; }

        /// <summary>
        /// get and set whether transaction is cash back type or not.
        /// </summary>
        public bool IsCashBack { get; set; }

        /// <summary>
        /// get and set whether pin less debit transaction or not.
        /// </summary>
        public bool IsPinLessDebit { get; set; }

        /// <summary>
        /// get and set whether TOR transaction or not.
        /// Required to set when processing TOR.
        /// </summary>
        public bool IsTORTransaction { get; set; }

        public string TransType { get; set; }
        public string MaxAmount { get; set; }

        /// <summary>
        /// get and set TOR sequence number.
        /// </summary>
        public int TORSequenceNumber { get; set; }

        public decimal CreditCashBackAmount { get; set; }

        public string Table14Response { get; set; }

        public string Table49Response { get; set; }

        public string TableVIResponse { get; set; }

        public string TableMCResponse { get; set; }

        public string TableDSResponse { get; set; }

        public string TableSPResponse { get; set; }

        public string TokenType { get; set; }

        public string BookerAppCardExpiry { get; set; }

        public string SourceTransaction { get; set; }

        public string Track3Data { get; set; }

        public string KeyId { get; set; }

        public string KeySerialNumber { get; set; }

        public bool IsCapKey { get; set; }

        public string EntryMode { get; set; }

        public string ServiceCode { get; set; }

        public string MacData { get; set; }

        public string CheckDigit { get; set; }

        public string TerminalLaneNo { get; set; }

        public bool PortalEmvRefundVoidReq { get; set; } //to identify EMV refund/void transaction request from portal

        public bool IsMAcVerificationFailed { get; set; }

        //PAY-13 Used to indicate the transaction is a TransArmor one, and to run the 
        // TransArmor clode blocks.  This is set from the Merchant UseTransArmor flag.
        public bool IsTransArmor { get; set; }

        #endregion

        #endregion

        #region Member function

        public object Clone()
        {
            return this.MemberwiseClone();
        }
        
        #endregion //end member function
    }
}