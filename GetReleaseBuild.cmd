REM *** Try and make the Build folder, in case it does not exist.
MD .\Build

REM ** Remove the current contents of the Build folder and sub-folders.
RD .\Build\MTDataPayments /S /Q
RD .\Build\MTDataPaymentsApp /S /Q
RD .\Build\MTDataPaymentsTerminal /S /Q
RD .\Build\Regions /S /Q

DEL .\Build\*.* /S /Q

REM *** Ensure the necessary Web App folders exist.
MD .\Build\MTDataPayments
MD .\Build\MTDataPaymentsApp
MD .\Build\MTDataPaymentsTerminal
MD .\Build\Regions

@ECHO OFF

:PopulateBuildDirectory

@ECHO ON

REM ** Copy the MTDataPayments files & folders.

XCOPY .\MTData.Web\App_Data\*.* .\Build\MTDataPayments\App_Data /E /Y /I
XCOPY .\MTData.Web\Assets\*.* .\Build\MTDataPayments\Assets /E /Y /I
XCOPY .\MTData.Web\bin\*.* .\Build\MTDataPayments\bin /E /Y /I
XCOPY .\MTData.Web\Content\*.* .\Build\MTDataPayments\Content /E /Y /I
XCOPY .\MTData.Web\css\*.* .\Build\MTDataPayments\css /E /Y /I
XCOPY .\MTData.Web\fonts\*.* .\Build\MTDataPayments\fonts /E /Y /I
XCOPY .\MTData.Web\HtmlTemplate\*.* .\Build\MTDataPayments\HtmlTemplate /E /Y /I
XCOPY .\MTData.Web\images\*.* .\Build\MTDataPayments\images /E /Y /I
XCOPY .\MTData.Web\js\*.* .\Build\MTDataPayments\js /E /Y /I
XCOPY .\MTData.Web\Merchant\*.* .\Build\MTDataPayments\Merchant /E /Y /I
XCOPY .\MTData.Web\screen\*.* .\Build\MTDataPayments\screen /E /Y /I
XCOPY .\MTData.Web\Scripts\*.* .\Build\MTDataPayments\Scripts /E /Y /I
XCOPY .\MTData.Web\"Service References"\*.* .\Build\MTDataPayments\"Service References" /E /Y /I
XCOPY .\MTData.Web\Uploads\*.* .\Build\MTDataPayments\Uploads /E /Y /I
XCOPY .\MTData.Web\Views\*.* .\Build\MTDataPayments\Views /E /Y /I

COPY .\MTData.Web\datetimepicker.jquery.json .\Build\MTDataPayments /Y
COPY .\MTData.Web\error.html .\Build\MTDataPayments /Y
COPY .\MTData.Web\favicon.ico .\Build\MTDataPayments /Y
COPY .\MTData.Web\Global.asax .\Build\MTDataPayments /Y
COPY .\MTData.Web\jquery.datetimepicker.css .\Build\MTDataPayments /Y
COPY .\MTData.Web\jquery.datetimepicker.js .\Build\MTDataPayments /Y
COPY .\MTData.Web\jquery.js .\Build\MTDataPayments /Y
COPY .\MTData.Web\MTData.Web.snk .\Build\MTDataPayments /Y
COPY .\MTData.Web\packages.config .\Build\MTDataPayments /Y
rem COPY .\MTData.Web\log4net.config .\Build\MTDataPayments /Y
rem COPY .\MTData.Web\log4net.config .\Build\MTDataPayments\log4net.PROD.config /Y
rem COPY .\MTData.Web\log4net.config .\Build\MTDataPayments\log4net.UAT.config /Y
rem COPY .\MTData.Web\Web.config .\Build\MTDataPayments /Y
rem COPY .\MTData.Web\Web.config .\Build\MTDataPayments\Web.UAT.config /Y
rem COPY .\MTData.Web\Web.config .\Build\MTDataPayments\Web.PROD.config /Y


REM ** Copy the MTDataPaymentsApp files & folders.

XCOPY .\MTData.GW\bin\*.* .\Build\MTDataPaymentsApp\bin /E /Y /I

COPY .\MTData.GW\PaymentGatewayCreditCardService.svc .\Build\MTDataPaymentsApp /Y
COPY .\MTData.GW\PaymentGatewayTransactionService.svc .\Build\MTDataPaymentsApp /Y

rem COPY .\MTData.GW\log4net.config .\Build\MTDataPaymentsApp\log4net.PROD.config /Y
rem COPY .\MTData.GW\log4net.config .\Build\MTDataPaymentsApp\log4net.UAT.config /Y
rem COPY .\MTData.GW\log4net.config .\Build\MTDataPaymentsApp /Y
rem COPY .\MTData.GW\Web.config .\Build\MTDataPaymentsApp\Web.PROD.config /Y
rem COPY .\MTData.GW\Web.config .\Build\MTDataPaymentsApp\Web.UAT.config /Y
rem COPY .\MTData.GW\Web.config .\Build\MTDataPaymentsApp /Y


REM ** Copy the MTDataPaymentsTerminal files & folders.

XCOPY .\MTData.WebAPI\Areas\*.* .\Build\MTDataPaymentsTerminal\Areas /E /Y /I
XCOPY .\MTData.WebAPI\bin\*.* .\Build\MTDataPaymentsTerminal\bin /E /Y /I
XCOPY .\MTData.WebAPI\HtmlTemplate\*.* .\Build\MTDataPaymentsTerminal\HtmlTemplate /E /Y /I
XCOPY .\MTData.WebAPI\images\*.* .\Build\MTDataPaymentsTerminal\images /E /Y /I
XCOPY .\MTData.WebAPI\RequestXML\*.* .\Build\MTDataPaymentsTerminal\RequestXML /E /Y /I
XCOPY .\MTData.WebAPI\Resources\*.* .\Build\MTDataPaymentsTerminal\Resources /E /Y /I
XCOPY .\MTData.WebAPI\Views\*.* .\Build\MTDataPaymentsTerminal\Views /E /Y /I

COPY .\MTData.WebAPI\favicon.ico .\Build\MTDataPaymentsTerminal /Y
COPY .\MTData.WebAPI\Global.asax .\Build\MTDataPaymentsTerminal /Y
COPY .\MTData.WebAPI\MTData.WebAPI.snk .\Build\MTDataPaymentsTerminal /Y
COPY .\MTData.WebAPI\packages.config .\Build\MTDataPaymentsTerminal /Y
rem COPY .\MTData.WebAPI\log4net.config .\Build\MTDataPaymentsTerminal\log4net.PROD.config /Y
rem COPY .\MTData.WebAPI\log4net.config .\Build\MTDataPaymentsTerminal\log4net.UAT.config /Y
rem COPY .\MTData.WebAPI\log4net.config .\Build\MTDataPaymentsTerminal /Y
rem COPY .\MTData.WebAPI\web.config .\Build\MTDataPaymentsTerminal\Web.PROD.config /Y
rem COPY .\MTData.WebAPI\web.config .\Build\MTDataPaymentsTerminal\Web.UAT.config /Y
rem COPY .\MTData.WebAPI\web.config .\Build\MTDataPaymentsTerminal /Y

REM ** Copy the Database project.
XCOPY .\Database.Payment\*.* .\Build\Database.Payment /E /Y /I /exclude:ExcludeFilesFromGet.txt

REM ** Copy the Reports.
XCOPY .\Reports\*.* .\Build\Reports /E /Y /I

REM ** Copy the Region config files.
XCOPY .\Regions\*.* .\Build\Regions /E /Y /I


GOTO End


:End
@ECHO ON
REM PAUSE