﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace MTData.Web.ViewData
{
    public class DriverViewData
    {
        public int DriverID { get; set; }
        [StringLength(100)]
        [Required(ErrorMessage = "Please enter driver Number")]
        [RegularExpression(@"^[A-Za-z,0-9, - , : , .]+$", ErrorMessage = "Driver number is not valid")]
        [Display(Name = "Driver Number")]

        public string DriverNo { get; set; }
        [StringLength(100)]
        //[Required(ErrorMessage = "Please enter driver tax number")]
        [RegularExpression(@"^[A-Za-z,0-9, - , : , .]+$", ErrorMessage = "Driver tax number is not valid")]
        [Display(Name = "Driver Tax Number")]

        public string DriverTaxNo { get; set; }
        [StringLength(100)]
        [Required(ErrorMessage = "Please enter first name")]
        [RegularExpression(@"^[A-Za-z 0-9]+$", ErrorMessage = "First name  is not valid")]
        [Display(Name = "First Name")]

        public string FName { get; set; }
        [StringLength(100)]
        [Required(ErrorMessage = "Please enter last name")]
        [RegularExpression(@"^[A-Za-z 0-9]+$", ErrorMessage = "Last name  is not valid")]
        [Display(Name = "Last Name")]

        public string LName { get; set; }
        [StringLength(100)]

       // [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Email is not valid")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Email is not valid")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please enter phone number")]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Phone  is not valid")]
        [DataType(DataType.PhoneNumber)]
        [MinLength(6, ErrorMessage = "Phone  must be minimum length of 6")]
        [Display(Name = "Phone")]
        public string Phone { get; set; }
        
        [StringLength(100)]
        [Required(ErrorMessage = "Please enter country name")]
        public string Fk_Country { get; set; }
        
        //[Required(ErrorMessage = "Please select state")]
        public string Fk_State { get; set; }
        
        //[Required(ErrorMessage = "Please select city")]
        public string Fk_City { get; set; }
        
        [StringLength(100)]
        [Required(ErrorMessage = "Please enter address")]

        public string Address { get; set; }
        [DataType(DataType.Password)]
      

        //[Required(ErrorMessage = "Please enter PIN")]
        public string PIN { get; set; }
        [DataType(DataType.Password)]
        [Display(Name = "Confirm PIN")]
        [Compare("PIN", ErrorMessage = "The PIN does not match")]
        //[Required(ErrorMessage = "Please confirm PIN")]
        public string ConfirmPIN { get; set; }

        public string FleetName { get; set; }
       [Required(ErrorMessage = "Please select fleet")]
        public int fk_FleetID { get; set; }
        public Nullable<bool> IsLocked { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public string GridCity { get; set; }
        public string GridState { get; set; }
        public IEnumerable<FleetListViewData> FleetList { get; set; }
        public IEnumerable<CountryViewData> CountryList { get; set; }
        public IEnumerable<StateViewData> StateList { get; set; }
        public IEnumerable<CityViewData> CityList { get; set; }

        //Ravit : Upload Excel file when create driver
        public HttpPostedFileBase uploadFile { get; set; }

        public List<DriverViewData> driverListViewData { get; set; }
        public bool IsDriverError { get; set; }
        public string FailedError { get; set; }
        public string SageAccount { get; set; }
        public string BankName { get; set; }
        public string AccountName { get; set; }
        public string BSB { get; set; }
        public string Account { get; set; }
    }

}