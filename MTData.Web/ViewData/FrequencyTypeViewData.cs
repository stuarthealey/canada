﻿namespace MTData.Web.ViewData
{
    public class FrequencyTypeViewData
    {
        public int TypeId { get; set; }
        public string Name { get; set; }
    }
}