﻿using System.Collections.Generic;
using MTD.Core.DataTransferObjects;

namespace MTD.Core.Service.Interface
{
    public interface IAdminUserService
    {
        bool Add(UserDto adminUserDto);

        bool Update(UserDto adminUserDto, int adminUserId);

        bool UpdateProfile(UserDto adminUserDto, int adminUserId);

        IEnumerable<UserDto> GetAdminUserList(string name);

        IEnumerable<UserDto> UserProfiles();

        UserDto GetAdminUserDto(int adminUserId);

        void DeleteAdminUser(int adminUserId);

        IEnumerable<RoleDto> GetAdminRoles();

        IEnumerable<UserDto> GetAdminUserListByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting);
       
    }
}
