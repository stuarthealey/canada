﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Configuration;
using System.Configuration;
using Ninject;

using MTD.Core.Service.Interface;
using MTD.Core.DataTransferObjects;
using MTData.Utility;
using MTData.Web.Resource;
using MTData.Web.ViewData;

namespace MTData.Web.Controllers
{
    public class MemberController : Controller
    {
        private readonly IUserService _userService;
        readonly ILogger _logger = new Logger();
        private readonly IMemberService _loginService;
        private StringBuilder _logMessage;
        private StringBuilder _logTracking;

        public MemberController([Named("LoginService")] IMemberService loginService, [Named("UserService")] IUserService userService, StringBuilder logMessage, StringBuilder logTracking)// need to register
        {
            _loginService = loginService;
            _userService = userService;
            _logMessage = logMessage;
            _logTracking = logTracking;
        }

        /// <summary>
        /// Purpose             :   To login the user
        /// Function Name       :   Index
        /// Created By          :   Umesh Kumar
        /// Created On          :   16/12/2014
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>         
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            bool doEncrypt = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["isEncrypt"]);
            if (doEncrypt)
                Encrypt();
            return View();
        }

        /// <summary>
        /// Purpose             :   To provide the password when user have forgotted password and do log in
        /// Function Name       :   ForgotPassword
        /// Created By          :   Umesh Kumar
        /// Created On          :   17/12/2014
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>         
        /// <returns></returns>
        [HttpGet]
        public ActionResult ForgotPassword()
        {
            return View("ForgotPassword");
        }

        /// <summary>
        /// Purpose             :   To provide the password when user have forgotted password and do log in
        /// Function Name       :   ForgotPassword
        /// Created By          :   Umesh Kumar
        /// Created On          :   17/12/2014
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary> 
        /// <param name="loginView"></param>        
        /// <returns></returns>
        [HttpPost]
        public ActionResult ForgotPassword(LoginViewData loginView)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (loginView.UserName == null)
                {
                    ModelState.AddModelError("UserName", MtdataResource.Please_Enter_Email);
                    return View();
                }

                _logMessage.Append("calling GetPassword method of _loginService takes parameter username as email= " + loginView.UserName + " to get password");

                var userDto = _loginService.GetPassword(loginView.UserName);

                _logMessage.Append("calling GetPassword method of _loginService executed successfully");

                if (userDto == null)
                {
                    TempData["PasswordSent"] = MtdataResource.invalid_Email_Forgot_Password; //"Invalid account, please enter valid email adderss.";
                    return View();
                }

                _logMessage.Append("calling CreateRandomString method of StringExtension class to genrate a random Password");

                string resetPCode = StringExtension.CreateRandomString(10);

                _logMessage.Append("CreateRandomString method of StringExtension class to genrate a random Password= " + resetPCode);
                _logMessage.Append("calling Hash method of StringExtension class to encrypt Password");

                string encryptPCode = StringExtension.Hash(resetPCode);

                _logMessage.Append("Hash method of StringExtension class to genrate a Hash Password= " + encryptPCode);
                _logMessage.Append("getting html file ~/HtmlTemplate/password.html from server");

                string filename = System.Web.HttpContext.Current.Server.MapPath("~/HtmlTemplate/password.html");

                try
                {
                    _logMessage.Append("Mail Sending begin and reading file ~/HtmlTemplate/password.html");

                    using (var myFile = new StreamReader(filename))
                    {
                        string msgfile = myFile.ReadToEnd();
                        var msgBody = new StringBuilder(msgfile);
                        msgBody.Replace("[##UserName##]", userDto.FName);
                        msgBody.Replace("[##email##]", loginView.UserName);
                        msgBody.Replace("[##password##]", resetPCode);

                        var utility = new EmailUtil();

                        _logMessage.Append("calling SendMail method of utility class to send a mail with various parameters " + loginView.UserName + ", " + msgBody + ", MTData , MTData member password");
                        _logMessage.Append("calling Reset method of _loginService class takes email and encrypt password= " + userDto.Email + ", " + encryptPCode);

                        bool updateUser = _loginService.Reset(userDto.Email, encryptPCode);

                        _logMessage.Append("calling Reset method of _loginService executed successfully");

                        if (updateUser)
                        {
                            _logMessage.Append("password Reset successfully");

                            TempData["PasswordSent"] = MtdataResource.invalid_Email_Forgot_Password; //  "Your passwrod has been reset successfully, please check your email.";

                            _logMessage.Append("calling SendMail method of utility class");

                            utility.SendMail(loginView.UserName, msgBody.ToString(), "MTI Admin", "MTI member password");
                        }
                        else
                            TempData["PasswordSent"] = MtdataResource.invalid_Email_Forgot_Password;

                        _logMessage.Append("returning to view");
                        return View();
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), ex);
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
                HttpContext.AddError(new HttpException(949, MtdataResource.Invalid_ID)); //"Invalid Id"));
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   To login the user
        /// Function Name       :   Index
        /// Created By          :   Umesh Kumar
        /// Created On          :   18/12/2014
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary> 
        /// <param name="loginView"></param>        
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(LoginViewData loginView)
        {
            if (!string.IsNullOrEmpty(loginView.UserName) && !string.IsNullOrEmpty(loginView.PCode))
            {
                try
                {
                    Session.Remove("Islogged");
                    Session.Remove("User");
                    Session.Remove("isAdmin");

                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                    _logMessage.Append("calling Hash method to encrypt the password");
                    _logMessage.Append("Hash method to encrypt the password executed successfully");

                    LoginDto loginDto = ViewDataExtention.CreateLoginDto(loginView);

                    _logMessage.Append("calling Login method of _loginService takes loginDto parameters are: username as email= " + loginView.UserName + " and password= " + loginView.PCode);

                    bool isLocked = _loginService.IsLocked(loginDto.UserName);

                    if (isLocked)
                    {
                        ViewBag.Message = MtdataResource.User_Locked;
                        return View();
                    }

                    SessionDto user = _loginService.Login(loginDto);
                    if (user != null)
                    {
                        Session["User"] = user;
                        Session["Islogged"] = false;
                        FormsAuthentication.SetAuthCookie(user.SessionUser.Email, false);

                        _logMessage.Append("session user is not null");

                        Session["isAdmin"] = "Admin";

                        if (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 0)
                        {
                            bool isLockedUser = _loginService.CountFailedAttempt(loginDto.UserName);
                            if (isLockedUser)
                            {
                                ViewBag.Message = MtdataResource.User_Locked;
                                return View();
                            }

                            ViewBag.Message = MtdataResource.Invalid_username_password; //"Invalid username or Password";
                            return View();
                        }
                        
                        bool isPasswordExpired = _loginService.PassCodeExpiration(loginDto.UserName);
                        if (isPasswordExpired)
                        {
                            //Session.Remove("User");
                            ViewBag.IsPasswordExpired = true;
                            return View();
                        }

                        _logTracking.Append("User " + loginView.UserName + " Logged in at " + System.DateTime.Now.ToUniversalTime());
                        if (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                        {
                            _logger.LogInfoMessage(_logTracking.ToString());
                            _logMessage.Append("Redirecting to Merchant/index if user type=1");

                            return Redirect("Merchant/Index");
                        }
                        
                        if (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 5)
                        {
                            _logger.LogInfoMessage(_logTracking.ToString());
                            _logMessage.Append("Redirecting to MyMerchants/index if user type=5");

                            return Redirect("MyMerchants/Index");
                        }

                        if (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 4 || !((SessionDto)Session["User"]).SessionUser.IsActive)
                        {
                            _logger.LogInfoMessage(_logTracking.ToString());
                            _logMessage.Append("Redirecting to Fleet/index if user type= 3 and 4");

                            return RedirectToAction("index", "Fleet");
                        }

                        int userId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.UserID));
                        new UserViewData { UserRoles = _userService.GetUserRoles(userId).ToViewDataList() };

                        _logMessage.Append("Redirecting to Terminal/index if user type=3");

                        return RedirectToAction("index", "Terminal");
                    }

                    ViewBag.Message = MtdataResource.Invalid_username_password; //"Invalid username or Password";
                }
                catch (Exception exception)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), exception);
                    HttpContext.AddError(new HttpException(949, MtdataResource.Invalid_ID)); //"Invalid Id"));
                }
            }

            if (string.IsNullOrEmpty(loginView.UserName))
                ModelState.AddModelError("UserName", MtdataResource.Please_Enter_Email);

            if (string.IsNullOrEmpty(loginView.PCode))
                ModelState.AddModelError("PCode", MtdataResource.Please_enter_password);

            return View();
        }

        /// <summary>
        /// Purpose             :   To logout the user
        /// Function Name       :   Logout
        /// Created By          :   Umesh Kumar
        /// Created On          :   19/12/2014
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>             
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Logout()
        {
            HttpCookie loginCookie = Response.Cookies["username"];
            HttpCookie httpCookie = Response.Cookies["CookieViewAs"];

            if (httpCookie != null)
                httpCookie.Expires = DateTime.Now.AddDays(-1);

            if (loginCookie != null)
                loginCookie.Expires = DateTime.Now.AddDays(-1);

            if (Session["User"] != null)
            {
                _logTracking.Append("User " + ((SessionDto)Session["User"]).SessionUser.Email + " Logged out at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
            }

            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

            FormsAuthentication.SignOut();

            return RedirectToAction("index");
        }

        /// <summary>
        /// Purpose             :   To provide different dashboards on different users
        /// Function Name       :   _MerchantUsers
        /// Created By          :   Umesh Kumar
        /// Created On          :   04/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>             
        /// <returns></returns>
        public ActionResult _MerchantUsers()
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("Logout", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                int userId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.UserID);

                _logMessage.Append("got user Id from session=" + userId);
                _logMessage.Append("using object initilizer to UserViewData and calling GetUserRoles method of _userService takes userId =" + userId + " and converting to view data");

                var objUserViewData = new UserViewData { UserRoles = _userService.GetUserRoles(userId).ToViewDataList() };

                _logMessage.Append("successed object initilizer to UserViewData and got GetUserRoles and converted viewdata");

                ViewData.Model = objUserViewData;

                return View("_MerchantUsers");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
                return View();
            }
        }

        /// <summary>
        /// Purpose             :   To provide different screens based on different users
        /// Function Name       :   _SettingMerchant
        /// Created By          :   Umesh Kumar
        /// Created On          :   04/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>             
        /// <returns></returns>
        public ActionResult _SettingMerchant()
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("Logout", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("getting user Id from session");

                int userId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.UserID);

                _logMessage.Append("got user Id from session it's= " + userId);
                _logMessage.Append("using object initilizer to  UserViewData and calling GetUserRoles method of _userService takes userId =" + userId + " and converting to view data");

                var objUserViewData = new UserViewData { UserRoles = _userService.GetUserRoles(userId).ToViewDataList() };

                _logMessage.Append("successed object initilizer to UserViewData and got GetUserRoles and converted viewdata");

                ViewData.Model = objUserViewData;

                return View("_SettingMerchant");
            }
            catch (Exception exception)
            {
                ViewBag.Message = "";
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View("_SchedularReport");
        }

        /// <summary>
        /// Purpose             :   To do the profile settings
        /// Function Name       :   _SettingProfile
        /// Created By          :   Umesh Kumar
        /// Created On          :   05/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>             
        /// <returns></returns>
        public ActionResult _SettingProfile()
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("Logout", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("getting user Id from session");

                int userId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.UserID);

                _logMessage.Append("got user Id from session it's= " + userId);
                _logMessage.Append("using object initilizer to  UserViewData and calling GetUserRoles method of _userService takes userId =" + userId + " and converting to view data");

                var objUserViewData = new UserViewData { UserRoles = _userService.GetUserRoles(userId).ToViewDataList() };

                _logMessage.Append("successed object initilizer to UserViewData and got GetUserRoles and converted viewdata");

                ViewData.Model = objUserViewData;

                return View("_SettingProfile");
            }
            catch (Exception exception)
            {
                ViewBag.Message = "";
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View("_SchedularReport");
        }

        /// <summary>
        /// Purpose             :   To search the transactions
        /// Function Name       :   _TransactionSearch
        /// Created By          :   Umesh Kumar
        /// Created On          :   06/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>             
        /// <returns></returns>
        public ActionResult _TransactionSearch()
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("Logout", "Member");

            try
            {
                _logMessage.Append("getting user Id from session");

                int userId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.UserID);

                _logMessage.Append("got user Id from session it's= " + userId);
                _logMessage.Append("using object initilizer to  UserViewData and calling GetUserRoles method of _userService takes userId =" + userId + " and converting to view data");

                var objUserViewData = new UserViewData { UserRoles = _userService.GetUserRoles(userId).ToViewDataList() };

                _logMessage.Append("successed object initilizer to UserViewData and got GetUserRoles and converted viewdata");

                ViewData.Model = objUserViewData;

                return View("_TransactionSearch");
            }
            catch (Exception exception)
            {
                ViewBag.Message = "";
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View("_SchedularReport");
        }

        /// <summary>
        /// Purpose             :   To schedule the reports
        /// Function Name       :   _SchedularReport
        /// Created By          :   Umesh Kumar
        /// Created On          :   07/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>             
        /// <returns></returns>
        public ActionResult _SchedularReport()
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("Logout", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("getting user Id from session");

                int userId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.UserID);

                _logMessage.Append("got user Id from session it's= " + userId);
                _logMessage.Append("using object initilizer to  UserViewData and calling GetUserRoles method of _userService takes userId =" + userId + " and converting to view data");

                var objUserViewData = new UserViewData { UserRoles = _userService.GetUserRoles(userId).ToViewDataList() };

                _logMessage.Append("successed object initilizer to UserViewData and got GetUserRoles and converted viewdata");

                ViewData.Model = objUserViewData;

                return View("_SchedularReport");
            }
            catch (Exception exception)
            {
                ViewBag.Message = "";
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View("_SchedularReport");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult _UserReset()
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("Logout", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("getting user Id from session");

                int userId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.UserID);

                _logMessage.Append("got user Id from session it's= " + userId);
                _logMessage.Append("using object initilizer to  UserViewData and calling GetUserRoles method of _userService takes userId =" + userId + " and converting to view data");

                var objUserViewData = new UserViewData { UserRoles = _userService.GetUserRoles(userId).ToViewDataList() };

                _logMessage.Append("successed object initilizer to UserViewData and got GetUserRoles and converted viewdata");

                ViewData.Model = objUserViewData;

                return View("_UserReset");
            }
            catch (Exception exception)
            {
                ViewBag.Message = "";
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View("_UserReset");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult _Manage()
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("Logout", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("getting user Id from session");

                int userId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.UserID);

                _logMessage.Append("got user Id from session it's= " + userId);
                _logMessage.Append("using object initilizer to  UserViewData and calling GetUserRoles method of _userService takes userId =" + userId + " and converting to view data");

                var objUserViewData = new UserViewData { UserRoles = _userService.GetUserRoles(userId).ToViewDataList() };

                _logMessage.Append("successed object initilizer to UserViewData and got GetUserRoles and converted viewdata");

                ViewData.Model = objUserViewData;

                return View("_Manage");
            }
            catch (Exception exception)
            {
                ViewBag.Message = "";
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View("_Manage");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult _ViewAsLayout()
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("Logout", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("getting user Id from session");

                int userId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.UserID);

                _logMessage.Append("got user Id from session it's= " + userId);
                _logMessage.Append("using object initilizer to  UserViewData and calling GetUserRoles method of _userService takes userId =" + userId + " and converting to view data");

                var objUserViewData = new UserViewData { UserRoles = _userService.GetUserRoles(userId).ToViewDataList() };

                _logMessage.Append("successed object initilizer to UserViewData and got GetUserRoles and converted viewdata");

                ViewData.Model = objUserViewData;

                return View("_ViewAsLayout");
            }
            catch (Exception exception)
            {
                ViewBag.Message = "";
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View("_Manage");
        }

        /// <summary>
        /// 
        /// </summary>
        [NonAction]
        public void Encrypt()
        {
            ProtectSection("connectionStrings", "RSAProtectedConfigurationProvider");
            ProtectSection("appSettings", "RSAProtectedConfigurationProvider");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sectionName"></param>
        /// <param name="provider"></param>
        [NonAction]
        private void ProtectSection(string sectionName, string provider)
        {
            Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
            ConfigurationSection section = config.GetSection(sectionName);

            if (section != null && !section.SectionInformation.IsProtected)
            {
                section.SectionInformation.ProtectSection(provider);
                config.Save();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ResetExpiredPassword()
        {
            var loginViewData = new LoginViewData();
            return View(loginViewData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loginView"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ResetExpiredPassword(LoginViewData loginView)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            try
            {
                if (string.IsNullOrEmpty(loginView.UserName))
                    ModelState.AddModelError("UserName", MtdataResource.ProfileController_ChangePassword_Please_enter_old_password);

                if (string.IsNullOrEmpty(loginView.PCode))
                    ModelState.AddModelError("PCode", MtdataResource.ProfileController_ChangePassword_Please_enter_new_password);

                if (string.IsNullOrEmpty(loginView.ConfirmPCode))
                    ModelState.AddModelError("ConfirmPCode", MtdataResource.ProfileController_ChangePassword_Please_re_enter_new_password);

                if (!ModelState.IsValid)
                    return View(loginView);

                string confirmpass = loginView.ConfirmPCode;
                if (confirmpass == loginView.PCode)
                {
                    loginView.ConfirmPCode = loginView.UserName;
                    loginView.UserName = Convert.ToString(((SessionDto)Session["User"]).SessionUser.UserID);
                    LoginDto loginDto = ViewDataExtention.CreateLoginDto(loginView);

                    int result = _loginService.ResetExpiredPassword(loginDto);

                    if (result == 2)
                    {
                        ViewBag.Message = MtdataResource.Old_Password_Incorrect;
                        return View(loginView);
                    }

                    if (result == 3)
                    {
                        ViewBag.Message = MtdataResource.NewPassword_Match_OldPassword;
                        return View(loginView);
                    }

                    if (result == 4)
                    {
                        ViewBag.Success = 1;
                        return View();
                    }

                    ViewBag.Message = MtdataResource.Invalid_Information;
                    return View(loginView);
                }

                ModelState.AddModelError("ConfirmPCode", MtdataResource.Password_Mismatch);

                return View(loginView);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   To provide different dashboards on different users
        /// Function Name       :   _MerchantUsers
        /// Created By          :   Umesh Kumar
        /// Created On          :   04/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>             
        /// <returns></returns>
        public ActionResult _SubAdmin()
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("Logout", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("getting user Id from session");

                int userId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.UserID);

                _logMessage.Append("got user Id from session it's= " + userId);
                _logMessage.Append("using object initilizer to  UserViewData and calling GetUserRoles method of _userService takes userId =" + userId + " and converting to view data");

                var objUserViewData = new UserViewData { UserRoles = _userService.GetUserRoles(userId).ToViewDataList() };

                _logMessage.Append("successed object initilizer to UserViewData and got GetUserRoles and converted viewdata");

                ViewData.Model = objUserViewData;

                return View("_SubAdmin");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
                return View();
            }
        }

        [HttpPost]
        public JsonResult ValidRequest()
        {
            if (Session["User"] != null)
                return Json(true, JsonRequestBehavior.AllowGet);

            return Json(false, JsonRequestBehavior.AllowGet);
        }
    }
}