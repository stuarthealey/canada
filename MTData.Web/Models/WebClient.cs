﻿using System;
using System.Linq;
using System.Net;
using EasyHttp.Http;

using MTData.Web.ViewData;

namespace MTData.Web.Models
{
    public class WebClient
    {
        public static string DoPost<T>(string uri, PaymentRequest obj)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

            var httpClient = new HttpClient();
            try
            {
                var response = httpClient.Post(uri, obj, HttpContentTypes.ApplicationXml);
                if (response != null)
                {
                    return response.RawText;
                }

                throw new Exception(response.RawText);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("DoPost Request Failed for Uri : {0} and Message : {1}", uri, ex.Message));
            }
        }

        public static HttpResponse DoPostDatawireRegister<T>(string uri, DatawireRequest objRequest)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

            var httpClient = new HttpClient();
            try
            {
                var response = httpClient.Post(uri, objRequest, HttpContentTypes.ApplicationXml);
                if (response != null)
                {
                    return response;
                }

                throw new Exception(response.RawText);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("DoPost Request Failed for Uri : {0} and Message : {1}", uri, ex.Message));
            }
        }
     
    }
}