﻿using System.Collections.Generic;
using System.Linq;

using Ninject;

using MTD.Core.Extension;
using MTD.Core.Factory.Interface;
using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Core.Service
{
    public class SubAdminService : ISubAdminService
    {
        private readonly ISubAdminRepository _subAdminRepository;
        private readonly IUserFactory _userFactory;
        private readonly ITransactionRepository _transactionReporsitory;

        public SubAdminService([Named("SubAdminRepository")] ISubAdminRepository subAdminRepository, [Named("UserFactory")] IUserFactory userFactory, [Named("TransactionRepository")] ITransactionRepository transactionReporsitory)
        {
            _subAdminRepository = subAdminRepository;
            _userFactory = userFactory;
            _transactionReporsitory = transactionReporsitory;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageSizeSearchdto"></param>
        /// <returns></returns>
        public IEnumerable<UserDto> GetSusbAdminByFilter(PageSizeSearchDto pageSizeSearchdto)
        {
            var pageSizeSearch = pageSizeSearchdto.ToPageSizeSearch();
            var subAdminList = _subAdminRepository.GetMerchantByFilter(pageSizeSearch);
            var subAdminDtoList = subAdminList.ToDTOList();
            return subAdminDtoList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subAdminId"></param>
        /// <returns></returns>
        public bool Remove(int subAdminId)
        {
            return _subAdminRepository.Remove(subAdminId);
        }

        /// <summary>
        /// Purpose             :   To get the Merchant List
        /// Function Name       :   MerchantList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/02/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UserDto> SubAdminList(PageSizeSearchDto pageSizeSearchdto)
        {
            try
            {
                var pageSizeSearch = pageSizeSearchdto.ToPageSizeSearch();
                IEnumerable<User> subList = _subAdminRepository.SubAdminList(pageSizeSearch);
                var SubAdminDtoList = subList.ToDTOList();
                return SubAdminDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Merchant List
        /// Function Name       :   GetMerchantByFilter
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   02/02/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public IEnumerable<MerchantUserDto> GetMyMerchantByFilter(PageSizeSearchDto pageSizeSearchdto)
        {
            try
            {
                var pageSizeSearch = pageSizeSearchdto.ToPageSizeSearch();
                var merchantList = _subAdminRepository.GetMyMerchantByFilter(pageSizeSearch);
                var merchantDtoList = merchantList.ToDTOList();
                return merchantDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get the Merchant List
        /// Function Name       :   MerchantList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/02/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MerchantUserDto> MyMerchantList(string name, int userId)
        {
            try
            {
                IEnumerable<MerchantUser> merchantList = _subAdminRepository.MyMerchantList(name, userId);
                var merchantDtoList = merchantList.ToDTOList();
                return merchantDtoList;
            }
            catch
            {
                throw;
        }
        }

        /// <summary>
        /// Purpose             :   To Get Merchants List
        /// Function Name       :   GetMerchantList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/22/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MerchantListDto> GetMerchantList(int userId)
        {
            try
            {
                IEnumerable<AllMerchants> merchantList = _subAdminRepository.GetUserMerchant(userId);
                return merchantList.Select(merchant => new MerchantListDto
                {
                    Company = merchant.Company,
                    fk_MerchantID = merchant.MerchantID
                }).ToList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Users Fleet
        /// Function Name       :   GetUsersFleet
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/26/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<UserFleetDto> GetUsersFleet(int userId) // for accessing roles of user
        {
            try
            {
                IEnumerable<UserFleet> fleets = _subAdminRepository.GetUsersFleet(userId);
                IEnumerable<UserFleetDto> fleetDto = fleets.ToDtoUserFleetList();
                return fleetDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Numberes of Transaction
        /// Function Name       :   GetTransactionCount
        /// Created By          :   Salil Gupta
        /// Created On          :   12/28/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public int GetTransactionCount(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToTransaction();
                int obj = _subAdminRepository.GetTransactionCount(trans);
                return obj;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get all merchants
        /// Function Name       :   MerchantList
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   10/27/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AllMerchantsDto> MerchantList()
        {
            try
            {
                var merchantList = _subAdminRepository.MerchantList();
                return merchantList.ToDTOList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get all merchants
        /// Function Name       :   GetFleet
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   10/27/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantListId"></param>
        /// <returns></returns>
        public IEnumerable<FleetDto> GetFleet(string merchantListId, int userType, int userId)
        {
            try
            {
                var fleetList = _subAdminRepository.GetFleet(merchantListId,userType, userId);
                return fleetList.ToDTOList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To add user
        /// Function Name       :   Add
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   10/27/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userDto"></param>
        /// <returns></returns>
        public bool Add(UserDto userDto)
        {

            try
            {
                User user = _userFactory.CreateUser(userDto);
                return _subAdminRepository.Add(user);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get user merchant
        /// Function Name       :   GetUserMerchant
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   10/27/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AllMerchantsDto> GetUserMerchant(int userId)
        {
            try
            {
                var merchantList = _subAdminRepository.GetUserMerchant(userId);
                return merchantList.ToDTOList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To update user
        /// Function Name       :   Update
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   10/27/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userDto"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string Update(UserDto userDto, int userId) // updating user
        {
            try
            {
                User user = _userFactory.CreateUser(userDto);
                return _subAdminRepository.Update(user, userId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Function Name       :   GetUser
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   10/27/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDto GetUser(int userId) 
        {
            try
            {
                return _subAdminRepository.GetUser(userId).ToDtoUser();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Transactions list
        /// Function Name       :   GetTransaction
        /// Created By          :   Salil Gupta
        /// Created On          :   12/28/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public IEnumerable<MTDTransactionDto> GetTransaction(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToMtdTransaction();
                IEnumerable<usp_SearchTransactionReport_Result> obj = _subAdminRepository.GetTransaction(trans);
                IEnumerable<MTDTransactionDto> std = obj.ToTransaction();
                return std;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Numberes of Driver
        /// Function Name       :   GetDriverCount
        /// Created By          :   Salil Gupta
        /// Created On          :   12/28/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public int GetDriverCount(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToTransaction();
                int obj1 = _subAdminRepository.GetDriverCount(trans);
                return obj1;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Driver list
        /// Function Name       :   GetDriver
        /// Created By          :   Salil Gupta
        /// Created On          :   12/28/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public IEnumerable<DriverTransactionResult> GetDriver(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToTransaction();
                IEnumerable<usp_GetDriversTransaction_Result> obj1 = _subAdminRepository.GetDriver(trans);
                IEnumerable<DriverTransactionResult> std = obj1.ToDriverReport();
                return std;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get numbers of tech fee detail records
        /// Function Name       :   GetTechFeeDetailCount
        /// Created By          :   Naveen Kumar
        /// Created On          :   10/06/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public int GetTechFeeDetailCount(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToTransaction();
                int obj1 = _subAdminRepository.GetTechFeeDetailCount(trans);
                return obj1;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get technology fee detail list
        /// Function Name       :   GetTechnologyDetail
        /// Created By          :   Naveen Kumar
        /// Created On          :   6/10/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public IEnumerable<TechnologyDetailReportDto> GetTechnologyDetail(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToTransaction();
                IEnumerable<usp_TechnologyDetailReport_Result> obj1 = _subAdminRepository.GetTechnologyDetail(trans);
                IEnumerable<TechnologyDetailReportDto> std = obj1.ToTechDetailReport();
                return std;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Numberes of tech fee records
        /// Function Name       :   GetTechFeeCount
        /// Created By          :   Naveen Kumar
        /// Created On          :   10/06/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public int GetTechFeeCount(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToTransaction();
                int obj1 = _subAdminRepository.GetTechFeeCount(trans);
                return obj1;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get technology fee list
        /// Function Name       :   GetTechnologySummary
        /// Created By          :   Naveen Kumar
        /// Created On          :   6/10/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public IEnumerable<TechnologySummaryReportDto> GetTechnologySummary(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToTransaction();
                IEnumerable<usp_TechnologySummaryReport_Result> obj1 = _subAdminRepository.GetTechnologySummary(trans);
                IEnumerable<TechnologySummaryReportDto> std = obj1.ToTechSummaryReport();
                return std;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get all Drivers
        /// Function Name       :   GetUserDriverReceipent
        /// Created By          :   Shishir Saunakia 
        /// Created On          :   10/29/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantListId"></param>
        /// <returns></returns>
        public IEnumerable<DriverRecipientDto> GetUserDriverReceipent(string fleelistId)
        {
            try
            {
                var driverRec = _subAdminRepository.GetUserDriverReceipent(fleelistId);
                return driverRec.ToDtoList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<AllMerchantsDto> GetCorpUserMerchant(int userId)
        {
            try
            {
                var merchantList = _subAdminRepository.GetCorpUserMerchant(userId);
                return merchantList.ToDTOList();
            }
            catch
            {
                throw;
            }
        }

    }
}
