-- Create indexes on MTDTransaction for performance.
--==================================================

IF NOT EXISTS (SELECT * FROM sysindexes si INNER JOIN sysobjects so ON so.id = si.id WHERE si.name = 'IX_MTDTransactionBatchNumber' AND OBJECTPROPERTY(so.id, N'IsUserTable') = 1 AND so.name = 'MTDTransaction')
BEGIN
	PRINT 'Creating Index IX_MTDTransactionBatchNumber on MTDTransaction'

	CREATE NONCLUSTERED INDEX IX_MTDTransactionBatchNumber ON dbo.MTDTransaction (BatchNumber, Id)
		WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
ELSE
BEGIN
	PRINT 'Index IX_MTDTransactionBatchNumber on MTDTransaction already exists'
END