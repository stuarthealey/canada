-- Add a timestamp column to the UKMTDTransaction table, as there is no date\time element.
------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'UKMTDTransaction' AND COLUMN_NAME = '_Timestamp')
BEGIN
	ALTER TABLE UKMTDTransaction ADD _Timestamp DATETIME NULL

	PRINT 'UKMTDTransaction table added _Timestamp'
END
ELSE
BEGIN
	PRINT 'UKMTDTransaction table already has _Timestamp'
END

-- Set the _Timestamp value on all existing UKMTDTransaction records before adding constraint.
----------------------------------------------------------------------------------------------
DECLARE @SQL NVARCHAR(2000)

SET @SQL = 'UPDATE dbo.UKMTDTransaction '
SET @SQL = @SQL + 'SET _Timestamp = T.TxnDate '
SET @SQL = @SQL + 'FROM dbo.MTDTransaction T '
SET @SQL = @SQL + 'WHERE T.Id = UKMTDTransaction.Fk_MtdTxnId '
SET @SQL = @SQL + '  AND _Timestamp IS NULL '

EXEC (@SQL)

-- Catch-all update of any NULL _Timestamp before adding the constraint.
SET @SQL = 'UPDATE dbo.UKMTDTransaction SET _Timestamp = GETDATE() WHERE _Timestamp IS NULL'

EXEC (@SQL)

-- Add constraint on _Timestamp column.
---------------------------------------
IF NOT EXISTS (SELECT * FROM sys.default_constraints WHERE name = 'DF_UKMTDTransaction_Timestamp')
BEGIN
	ALTER TABLE dbo.UKMTDTransaction 
		ADD CONSTRAINT DF_UKMTDTransaction_Timestamp DEFAULT GETDATE() FOR _Timestamp

	PRINT 'UKMTDTransaction Adding _Timestamp default constraint.'
END
ELSE
BEGIN
	PRINT 'UKMTDTransaction _Timestamp default constraint already exists.'
END
