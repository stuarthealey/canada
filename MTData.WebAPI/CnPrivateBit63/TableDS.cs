﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class TableDS
    {
        #region Private Constants and Variables
        private const string tblId = "|44|53";
        #endregion

        #region Public Properties
        /// <summary>
        /// get the table id of DS table
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }
        #endregion
    }
}