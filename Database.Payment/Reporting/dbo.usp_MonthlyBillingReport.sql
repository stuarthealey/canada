IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MonthlyBillingReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_MonthlyBillingReport]
GO

CREATE PROCEDURE dbo.usp_MonthlyBillingReport
-- =======================================================
-- Author:		S.Healey
-- Create date: 03-Nov-2018
-- Description:	Monthly Billing data extract for accounts.
-- =======================================================
--
(
	@System NVARCHAR(20),
	@ReportMonth DATE = NULL,
	@EmailTo NVARCHAR(250) = NULL
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @Today DATE
	DECLARE @ReportStartDate DATE
	DECLARE @ReportEndDate DATE

	IF (@ReportMonth IS NULL)
	BEGIN
		SET @Today = GETDATE()
		SET @ReportStartDate = DATEADD(month, -1, DATEADD(day, -(DATEPART(day, @Today) - 1), @Today))
		SET @ReportEndDate = DATEADD(day, -(DATEPART(day, @Today) - 1), @Today)
	END
	ELSE
	BEGIN
		SET @ReportStartDate = DATEADD(day, -(DATEPART(day, @ReportMonth) - 1), @ReportMonth)
		SET @ReportEndDate = DATEADD(month, 1, @ReportStartDate)
	END

	IF OBJECT_ID('tempdb..#Monthly') IS NOT NULL 
	BEGIN 
		DROP TABLE #Monthly
	END

	CREATE TABLE #Monthly (FleetName NVARCHAR(50), TransAmt DECIMAL(18, 2), TransCount INT)

	INSERT INTO #Monthly (FleetName)
	SELECT DISTINCT F.FleetName
	FROM dbo.Fleet F
	WHERE F.FleetID IN (SELECT CAST(Value AS INT) AS FleetID FROM dbo._MonthlyReportFilter WHERE Type IN ('FV', 'FT'))

	-- $ Value Query
	UPDATE #Monthly 
	SET TransAmt = TxnAmt.TransactionsAmount
	FROM (SELECT F.FleetName AS FName, SUM(T.Amount) As TransactionsAmount
		FROM dbo.MTDTransaction T
			INNER JOIN dbo.Fleet F ON F.FleetID = T.fk_FleetId
		WHERE F.FleetID IN (SELECT CAST(Value AS INT) AS FleetID FROM dbo._MonthlyReportFilter WHERE Type = 'FV')
		  AND TxnDate >= @ReportStartDate AND TxnDate < @ReportEndDate
		  AND AddRespData IN (SELECT Value AS AddRespData FROM dbo._MonthlyReportFilter WHERE Type = 'AV')
		  AND VehicleNo NOT IN (SELECT Value AS VehicleNo FROM dbo._MonthlyReportFilter WHERE Type = 'V')
		GROUP BY F.FleetName) AS TxnAmt
	WHERE FleetName = TxnAmt.FName COLLATE database_default

	-- Transactions
	UPDATE #Monthly
	SET TransCount = TxnCount.TransactionCount
	FROM (SELECT F.FleetName AS FName, COUNT(T.AddRespData) AS TransactionCount
		FROM dbo.MTDTransaction T
			INNER JOIN dbo.Fleet F ON F.FleetID = T.fk_FleetId
		WHERE F.FleetID IN (SELECT CAST(Value AS INT) AS FleetID FROM dbo._MonthlyReportFilter WHERE Type = 'FT')
		  AND TxnDate >= @ReportStartDate AND TxnDate < @ReportEndDate
		  AND AddRespData NOT IN (SELECT Value AS AddRespData FROM dbo._MonthlyReportFilter WHERE Type = 'AT')
		  AND AddRespData NOT LIKE '%XML%'
		  AND AddRespData NOT LIKE '%CRYPT%'
		  AND VehicleNo NOT IN (SELECT Value AS VehicleNo FROM dbo._MonthlyReportFilter WHERE Type = 'V')
		GROUP BY F.FleetName) AS TxnCount
	WHERE FleetName = TxnCount.FName COLLATE database_default

	-- Email the report results
	---------------------------
	DECLARE @MailTo VARCHAR(50)
	DECLARE @Subject VARCHAR(100)

	IF (@EmailTo IS NULL)
		SET @MailTo = 'shealey@mtidispatch.com'
	ELSE 
		SET @MailTo = @EmailTo

	SET @Subject = 'Monthly billing numbers for ' + @System + ' from ' + CONVERT(VARCHAR, @ReportStartDate, 20) + ' to ' + CONVERT(VARCHAR, @ReportEndDate, 20)

	DECLARE @xml NVARCHAR(MAX)
	DECLARE @HTMLBody NVARCHAR(MAX)

	SET @xml = CAST((SELECT FleetName AS 'td', '', '$' + CAST(ISNULL(TransAmt, 0) AS NVARCHAR) AS 'td', '', ISNULL(TransCount, 0) AS 'td', ''
					FROM #Monthly WHERE TransAmt IS NOT NULL OR TransCount IS NOT NULL
				FOR XML PATH('tr'), ELEMENTS ) AS NVARCHAR(MAX))

	SET @HTMLBody = '<html><head>' 
					+ '<style>' 
					+ 'th {border: solid black 1px;padding-left:5px;padding-right:5px;padding-top:1px;padding-bottom:1px;font-size:11pt;} ' 
					+ 'td {border: solid black 1px;padding-left:5px;padding-right:5px;padding-top:1px;padding-bottom:1px;font-size:11pt;} ' 
					+ '</style>' 
					+ '</head>' 
					+ '<body>'
					+ '<h4>Payment system transaction values and counts for the month</h4>'
					+ '<table border="1">'
					+ '<tr>'
					+ '<th>Fleet</th>'
					+ '<th>TransAmt</th>'
					+ '<th>TransCount</th>'
					+ '</tr>'

	SET @HTMLBody = @HTMLBody + @xml +'</table></body></html>'+'<font face="Verdana" size="2" color="#333333"><p><br />Thanks,<br />MTI</p><p>'
	
	EXEC msdb.dbo.sp_send_dbmail
		@profile_name = 'MTData',
		@recipients = @MailTo,
		@subject = @Subject,
		@body_format = 'HTML',
		@body = @HTMLBody

	-- Cleanup
	DROP TABLE #Monthly

END
GO
