﻿namespace MTData.Web.ViewData
{
    public class RoleViewData
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
    }
}