﻿namespace MTData.Web.ViewData
{
    public class DriverListViewData
    {
        public int DriverID { get; set; }
        public string DriverNumber { get; set; }
    }
}