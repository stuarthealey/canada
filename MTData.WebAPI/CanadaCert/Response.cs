﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CanadaCert
{
    [System.SerializableAttribute()]
    //[System.Diagnostics.DebuggerStepThroughAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    // [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://securetransport.dw/rcservice/xml")]
    //[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://securetransport.dw/rcservice/xml", IsNullable = false)]
    public class Response
    {
        public Response()
        {
            this.Version = "3";  // Initialize the Response Version to default [Version="3"]
        }

        #region Common property

        [System.Xml.Serialization.XmlElementAttribute("RespClientID", typeof(RespClientIDType))]
        public RespClientIDType RespClientIDField { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Status", typeof(StatusType))]
        public StatusType StatusTypeField { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Version { get; set; }

        #endregion //end Common property

        #region Property related to Merchant response

        [System.Xml.Serialization.XmlElementAttribute("RegistrationResponse", typeof(RegistrationResponse))]
        public RegistrationResponse RegistrationResponse { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("ActivationResponse", typeof(string))]
        public string ActivationResponse { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("PingResponse", typeof(PingResponse))]
        public PingResponse PingResponse { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("ServiceDiscoveryResponse", typeof(ServiceDiscoveryResponse))]
        public ServiceDiscoveryResponse ServiceDiscoveryResponse { get; set; }

        #endregion //end Property related to Merchant response

        #region Property related to Simple Transaction

        [System.Xml.Serialization.XmlElementAttribute("TransactionResponse", typeof(TransactionResponce))]
        public TransactionResponce TransactionResponseField { get; set; }

        #endregion //end property related to simple transaction

    }   
}