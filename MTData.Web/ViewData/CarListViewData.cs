﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.Web.ViewData
{
    public class CarListViewData
    {
        public int FleetId { get; set; }
        public string VehicleNumber { get; set; }
    }
}
