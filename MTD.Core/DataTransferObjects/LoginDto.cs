﻿namespace MTD.Core.DataTransferObjects
{
   public class LoginDto
    {
       public string UserName { get; set; }
       public string PCode { get; set; }
       public string ConfirmPCode { get; set; }
    }
}
