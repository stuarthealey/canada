﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.Web.ViewData
{
    public class GetDetailReportViewData
    {
        public string Company { get; set; }
        public Nullable<System.DateTime> ReportDate { get; set; }
        public Nullable<int> TransId { get; set; }
        public Nullable<System.DateTime> TransDate { get; set; }
        public string TxnType { get; set; }
        public string CardType { get; set; }
        public string LastFour { get; set; }
        public string AuthCode { get; set; }
        public string Trip { get; set; }
        public string DriverNo { get; set; }
        public string VehicleNo { get; set; }
        public string TechFee { get; set; }
        public string Surcharge { get; set; }
        public string GrossAmt { get; set; }
        public string Fees { get; set; }
        public string NetAmt { get; set; }
    }
}