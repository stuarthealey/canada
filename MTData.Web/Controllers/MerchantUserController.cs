﻿using System;
using System.Web.Mvc;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Web.ViewData;

namespace MTData.Web.Controllers
{
    public class MerchantUserController : Controller
    {
        UserViewData _objUserViewData;
        private readonly IUserService _userService;

        public MerchantUserController([Named("UserService")] IUserService userService)
        {
            _userService = userService;
        }

        //
        // GET: /MerchantUser/
        public ActionResult Index()
        {
            int userId = Convert.ToInt32((((UserDto)Session["User"]).UserID));
            _objUserViewData = new UserViewData { UserRoles = _userService.GetUserRoles(userId).ToViewDataList() };

            return View(_objUserViewData);
        }
	}
}