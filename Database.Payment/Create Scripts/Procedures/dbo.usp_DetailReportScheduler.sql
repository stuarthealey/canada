IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DetailReportScheduler]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_DetailReportScheduler]
GO

CREATE PROCEDURE dbo.usp_DetailReportScheduler
-- =============================================
-- Author:	<Naveen Kumar>
-- Create date: <10-03-2016>
-- Description:	<Prepare and Send detail reports to merchants>
-- =============================================
--	S.Healey	2016-06-06		- Changed company name reference to MTData, LLC
--
(
	@IsOnDemandReport BIT = 0,
	@FromDate DATETIME = NULL,
	@ToDate DATETIME = NULL,
	@MerchId INT = NULL
)
AS
BEGIN
	BEGIN TRY
	 SET NOCOUNT ON
	 SET ANSI_WARNINGS OFF

	 DECLARE @Res INT,
		@MerchantId INT,
		@To VARCHAR(50),
		@Body NVARCHAR(MAX),
		@Error VARCHAR(200),
		@MailId INT,
		@Valid INT,
		@MsgXml NVARCHAR(40),
		@XmlData NVARCHAR(MAX),
		@MerchantName VARCHAR(50),    
		@Company VARCHAR(100),
		@ReportType INT,
		@RepType VARCHAR(50),
		@RepHeading VARCHAR(200),
		@FDate DATETIME,
		@TDate DATETIME,
		@ReportId INT,
		@NextScheduleDateTime DATETIME,
		@Result INT

		DECLARE @CursorData TABLE
		 (
		   MerchantId INT,
		   Email NVARCHAR(80),
		   Company NVARCHAR(80),
		   ReportId  INT NULL
		 )

		IF (@IsOnDemandReport = 1)
		BEGIN
			IF (@MERCHID IS NULL)
			BEGIN
				INSERT INTO @CursorData (MerchantId, Email, Company)
				SELECT DISTINCT m.MerchantID, u.Email, m.Company
				FROM dbo.Merchant m 
					INNER JOIN dbo.recipient r ON m.MerchantID = r.fk_MerchantID 
					INNER JOIN dbo.Users u ON m.MerchantID = u.fk_MerchantID 
				WHERE r.IsRecipient = 1 
				  AND r.fk_UserTypeId = 2 
				  AND r.fk_ReportId = 2
				  AND m.IsActive = 1 
				  AND u.fk_UserTypeID = 2 AND u.IsActive = 1
			END
			ELSE
			BEGIN
				INSERT INTO @CursorData (MerchantId, Email, Company)
				SELECT DISTINCT m.MerchantID, u.Email, m.Company
				FROM dbo.Merchant m 
					INNER JOIN dbo.recipient r ON m.MerchantID = r.fk_MerchantID 
					INNER JOIN dbo.Users u ON m.MerchantID = u.fk_MerchantID 
				WHERE r.IsRecipient = 1 
				  AND r.fk_UserTypeId = 2 
				  AND r.fk_ReportId = 2
				  AND m.IsActive = 1 
				  AND u.fk_UserTypeID = 2 
				  AND u.IsActive = 1 
				  AND m.MerchantID = @MerchId
			END     
		END
		ELSE
		BEGIN
			SELECT @NextScheduleDateTime = NextScheduleDateTime 
			FROM dbo.ScheduleReport 
			WHERE fk_MerchantId IS NULL 
			  AND fk_ReportID = 2 
			  AND IsCreated = 1 
			  AND CorporateUserId IS NULL

			INSERT INTO @CursorData (MerchantId, Email, Company, ReportId)
			SELECT DISTINCT m.MerchantID, u.Email, m.Company, NULLIF (2,sc.fk_ReportID)
			FROM dbo.Merchant m 
				INNER JOIN recipient r ON m.MerchantID = r.fk_MerchantID 
				INNER JOIN  Users u ON m.MerchantID = u.fk_MerchantID 
				LEFT JOIN ScheduleReport sc on sc.fk_MerchantId=m.MerchantID
			WHERE r.IsRecipient = 1 
			  AND r.fk_UserTypeId = 2 
			  AND r.fk_ReportId = 2
			  AND m.IsActive = 1 
			  AND u.fk_UserTypeID = 2
			  AND ((sc.fk_ReportID = 2 AND sc.IsCreated = 1 AND sc.NextScheduleDateTime >= DATEADD(SECOND, -900, GETDATE()) AND sc.NextScheduleDateTime < GETDATE())
			   OR (sc.fk_ReportID IS NULL AND @NextScheduleDateTime >= DATEADD(SECOND, -900, GETDATE()) AND @NextScheduleDateTime < GETDATE())
			   OR (sc.fk_ReportID IN (1, 3, 7, 8) AND 1 <> (SELECT COUNT(1) FROM dbo.ScheduleReport WHERE fk_MerchantId = sc.fk_MerchantId AND IsCreated = 1 AND fk_ReportID = 2) 
			  AND @NextScheduleDateTime >= DATEADD(SECOND, -900, GETDATE()) AND @NextScheduleDateTime < GETDATE()))
		END

		--Declaring cursor for fetching all the merchant recipients for detail report
		DECLARE queue_cursor CURSOR FOR SELECT MerchantId, Email, Company, ReportId FROM @CursorData

		OPEN queue_cursor
		FETCH NEXT FROM queue_cursor INTO @MerchantId, @To, @Company, @ReportId
		WHILE @@FETCH_STATUS = 0   
		BEGIN   
			SELECT @MerchantName=FName FROM Users  WHERE fk_MerchantID=@MerchantId AND fk_UserTypeID=2 AND IsActive=1

			--Exceuting store procedure DetailReport to get data in xml format
			EXEC @Result = dbo.usp_DetailReport @IsOnDemandReport, @FromDate, @ToDate, @MerchantId, @ReportId, @XmlData OUTPUT, @ReportType OUTPUT, @FDate OUTPUT, @TDate OUTPUT
			IF (@Result <> 0)
			BEGIN
				SET @Valid=0
				RETURN
			END

			IF (@IsOnDemandReport = 1)
			BEGIN
				SET @RepType='On Demand Detailed Transaction Report'
			END
			ELSE
			BEGIN
				SET  @RepType=CASE @ReportType WHEN 1 THEN 'Daily Detailed Transaction Report'
												WHEN 2 THEN 'Weekly Detailed Transaction Report' 
												ELSE 'Monthly Detailed Transaction Report' END
			END

			SET @RepHeading=@RepType+' '+'(Dated from'+' '+ CONVERT(VARCHAR(11),@FDate,106)+' '+'to'+' '+CONVERT(VARCHAR(11),@TDate,106)+')'
			IF (@XmlData IS NULL or @XmlData = '') 
			BEGIN
				SET @Valid=0
				INSERT INTO MailMessage(RepUserId,[Status],SendTo,Body,RepUserTypeId,[Date],ReportType) 
				VALUES(@MerchantId,-1,@To,'No records in database',2,GETDATE(),'DetailReport')
			END
			ELSE 
			BEGIN
				SET @Valid=1
			END

			IF (@Valid=1) 
			BEGIN
				--Set the xml data into html fromat
				SET @Body ='<html>
							<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
							<style>td{text-align:left;font-family:verdana;font-size:12px}
							p{font-family:verdana;font-size:13px}span{font-family:verdana;font-size:13px}</style>
							<body><table style="background-color:#e0e0e0; padding:20px; height:100%;"><tr><td><p style="font-size:11px;font-weight:bold">'+CONCAT('Dear',' ',@MerchantName,',')+'</p>
							<p style="font-size:11px;font-weight:bold">Listed below is the detailed transaction report containing details of all transactions.</p>
							<H5 style="color:#003366;text-align:center;font-size:11px;font-family:verdana; padding:10px;"> Merchant  :  '+@Company+'</H5>
							<h5 style="color:#003366;text-align:center;font-size:11px;font-family:verdana; padding:10px;">'+@RepHeading+'</h5>
							<table border = 1 cellpadding="0" cellspacing="0" style="background-color:#aed6eb; border:2px solid #003366; border-collapse:collapse; padding:2px;">
							<thead style="background-color:#2b79c7; padding:10px; "><tr>
							<th style="color:grey;font-size:10px;width:900px;text-align:left;font-family:verdana; color:#fff; padding:5px;">Merchant</th>
							<th style="color:grey;font-size:10px;width:700px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Report Date</th>
							<th style="color:grey;font-size:10px;width:120px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Trans Id</th>
							<th style="color:grey;font-size:10px;width:180px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Transaction Date</th>
							<th style="color:grey;font-size:10px;width:130px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Trans Type</th>
							<th style="color:grey;font-size:10px;width:140px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Card Type</th>
							<th style="color:grey;font-size:10px;width:180px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Last Four</th>
							<th style="color:grey;font-size:10px;width:150px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Auth Code</th>
							<th style="color:grey;font-size:10px;width:200px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Trip</th>
							<th style="color:grey;font-size:10px;width:500px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Driver</th>
							<th style="color:grey;font-size:10px;width:500px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Car Number</th>
							<th style="color:grey;font-size:10px;width:500px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Tech Fee</th>
							<th style="color:grey;font-size:10px;width:500px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Surcharge</th>
							<th style="color:grey;font-size:10px;width:150px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Gross Amt</th>
							<th style="color:grey;font-size:10px;width:150px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Fees</th>
							<th style="color:grey;font-size:10px;width:150px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Net Amt</th></tr></thead>'    

				SET @Body = @Body + @XmlData +'</table><br/><br/>
													<span>Thanks & Regards,<br/><br/>MTData, LLC.<br/>www.mtdata.us<span>
													<h4 style="color:grey;font-size:12px;font-family:verdana">MTData-All rights reserved</h4>
														<img src="D:\logo-1.png" width="138" height="44" alt="logo">
													<p style="font-size:11px;color:black">Note:This is an automated message. Do not Reply or Reply All
													to this message as replies are not being accepted.</p></body></html>'

				INSERT INTO MailMessage(RepUserId,[Status],SendTo,Body,RepUserTypeId,[Date],ReportType) VALUES(@MerchantId,0,@To,@Body,2,GETDATE(),'DetailReport')

				SET @MailId=SCOPE_IDENTITY()

				--Exceute the store procedure sp_send_dbmail to send the detail report to corresponding merchant
				EXEC @Res=msdb.dbo.sp_send_dbmail
					@profile_name = 'MTData',
					@recipients =@To,
					@body =@Body,
					@subject = 'Merchant Detailed Transaction Report',
					@body_format ='HTML'

				IF (@Res != 0)
				BEGIN
					SET @Error =@@ERROR
					UPDATE MailMessage SET Error=@Error,[Date]=GETDATE() WHERE MailId=@MailId
				END
				ELSE
				BEGIN
					UPDATE MailMessage SET [Status]=1,[Date]=GETDATE() WHERE MailId=@MailId
				END
			END

			FETCH NEXT FROM queue_cursor INTO @MerchantId,@To,@Company,@ReportId
		END   
		CLOSE queue_cursor   
		DEALLOCATE queue_cursor
		SELECT 0
	END TRY
	BEGIN CATCH
		INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
		SELECT ERROR_NUMBER()
				,ERROR_SEVERITY()
				,ERROR_STATE()
				,ERROR_PROCEDURE()
				,ERROR_LINE()
				,ERROR_MESSAGE()
				,SUSER_SNAME()
				,HOST_NAME()
				,GETDATE()

		SELECT 1 
	END CATCH
END
GO
