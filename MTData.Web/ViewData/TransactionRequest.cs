﻿using System.Collections.Generic;

namespace MTData.Web.ViewData
{
    public class TransactionRequest
    {
        public int? TransID { get; set; }
        public string PaymentType { get; set; }
        public string TransactionAmount { get; set; }
        public string CardNumber { get; set; }
        public string CardType { get; set; }
        public string ExpiryDate { get; set; }
        public string CCVData { get; set; }
        public string ZipCode { get; set; }
        public string StreetAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EMailId { get; set; }
        public string Description { get; set; }
        public string CompanyName { get; set; }
        public string MerchantCategoryCode { get; set; }
        public string TransactionType { get; set; }
        public string TransType { get; set; }
        public string TerminalID { get; set; }
        public string FdMerchantId { get; set; }
        public int MtdMerchantId { get; set; }
        public int UserID { get; set; }
        public string EntryType { get; set; }
        public string Track2Data { get; set; }
        public string IndustryType { get; set; }
        public string SerialNumber { get; set; }
        public string TppId { get; set; }
        public string GroupId { get; set; }
        public string App { get; set; }
        public string Did { get; set; }
        public string ServiceUrl { get; set; }
        public string ServiceId { get; set; }
        public string CCVIndType { get; set; }
        public string DriverNumber { get; set; }
        public string VehicleNumber { get; set; }
        public string SerialNo { get; set; }
        public string RapidConnectAuthId { get; set; }
        public string PinData { get; set; }
        public string KeySerNumData { get; set; }
        public string AdditionalAmout { get; set; }
        public string ReversalIndType { get; set; }
        public string DeviceID { get; set; }
        public string TransNote { get; set; }
        public bool Ismoto { get; set; }
        public string Token { get; set; }
        public bool? IsCompleted { get; set; }
        public string CreatedBy { get; set; }

        public IEnumerable<TipViewData> TaxList { get; set; }
        public IEnumerable<DriverTxnListViewData> DriverList { get; set; }
        public IEnumerable<VehicleTxnListViewData> VehicleList { get; set; }

        public decimal? StateTax { get; set; }
        public decimal? StateTaxPer { get; set; }
        public decimal? FedralTax { get; set; }
        public decimal? FedralTaxPer { get; set; }
        public bool IsTaxInclusive { get; set; }
        public bool IsJobNoRequired { get; set; }
        public double Surcharge { get; set; }

        public byte? SurchargeType { get; set; }
        public decimal? SurchargeFixed { get; set; }
        public decimal?  SurchargePer { get; set; }
        public decimal? SurMaxCap { get; set; }
        public string MtDataJobNumber { get; set; }
        public byte? FeeType { get; set; }
        public decimal? FeeFixed { get; set; }
        public decimal? FeePer { get; set; }
        public decimal? FeeMaxCap { get; set; }
        public decimal? Fee { get; set; }

        public string FirstFourDigits { get; set; }
        public string LastFourDigits { get; set; }
        public string FareValue { get; set; }
        public string Tip { get; set; }
        public string Taxes { get; set; }
        public string Toll { get; set; }
        public string FlagFall { get; set; }
        public string Extras { get; set; }
        public string GateFee { get; set; }
        public string Others { get; set; }
        public string Email { get; set; }
        public string FleetId { get; set; }

        public IEnumerable<FleetViewData> fleetlist { get; set; }

        public string Currency { get; set; }
    }
}