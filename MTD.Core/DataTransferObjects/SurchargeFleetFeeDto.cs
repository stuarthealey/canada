﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
    public class SurchargeFleetFeeDto
    {
        public Nullable<byte> TechFeeType { get; set; }
        public Nullable<decimal> TechFeeFixed { get; set; }
        public Nullable<decimal> TechFeePer { get; set; }
        public Nullable<decimal> TechFeeMaxCap { get; set; }

        public Nullable<byte> FleetFeeType { get; set; }
        public Nullable<decimal> FleetFeeFixed { get; set; }
        public Nullable<decimal> FleetFeePer { get; set; }
        public Nullable<decimal> FleetFeeMaxCap { get; set; }

        public Nullable<byte> BookingFeeType { get; set; }
        public Nullable<decimal> BookingFeeFixed { get; set; }
        public Nullable<decimal> BookingFeePer { get; set; }
        public Nullable<decimal> BookingFeeMaxCap { get; set; }
        public Nullable<byte> PayType { get; set; }
    }
}
