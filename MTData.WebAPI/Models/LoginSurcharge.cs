﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class LoginSurcharge
    {
        //[DataMember]
        //public List<CardPrefix> CardNumber { get; set; }

        private int? _surchargeType;
        [DataMember]
        public int? SurchargeType
        {
            get { return _surchargeType ?? default(int); }
            set { _surchargeType = value; }
        }

        private decimal? _surchargeFixed;
        [DataMember]
        public decimal? SurchargeFixed
        {
            get { return _surchargeFixed ?? default(decimal); }
            set { _surchargeFixed = value; }
        }

        private decimal? _surchargePercentage;
        [DataMember]
        public decimal? SurchargePercentage
        {
            get { return _surchargePercentage ?? default(decimal); }
            set { _surchargePercentage = value; }
        }

        private decimal? _surchargeMaxCap;
        [DataMember]
        public decimal? SurchargeMaxCap
        {
            get { return _surchargeMaxCap ?? default(decimal); }
            set { _surchargeMaxCap = value; }
        }

        private int? _bookingFeeType;
        [DataMember]
        public int? BookingFeeType
        {
            get { return _bookingFeeType ?? default(int); }
            set { _bookingFeeType = value; }
        }

        private decimal? _feeFixed;
        [DataMember]
        public decimal? FeeFixed
        {
            get { return _feeFixed ?? default(decimal); }
            set { _feeFixed = value; }
        }

        private decimal? _feePercentage;
        [DataMember]
        public decimal? FeePercentage
        {
            get { return _feePercentage ?? default(decimal); }
            set { _feePercentage = value; }
        }

        private decimal? _feeMaxCap;
        [DataMember]
        public decimal? FeeMaxCap
        {
            get { return _feeMaxCap ?? default(decimal); }
            set { _feeMaxCap = value; }
        }
    }
}