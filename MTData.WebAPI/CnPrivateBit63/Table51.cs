﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table51
    {
        #region Private Constants and Variables
        #region Constants
        private const string tblId = "|35|31";
        #endregion

        #region Variables
        private string defaultIndicator = "9";//9 refers to payment on existing debt.
        #endregion
        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'35 31' in Request
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        /// <summary>
        /// Indicator
        /// used to indicate Payment on existing debt
        /// </summary>
        public string Indicator
        {
            get { return defaultIndicator; }
            set { defaultIndicator = value; }
        }
        #endregion
    }
}