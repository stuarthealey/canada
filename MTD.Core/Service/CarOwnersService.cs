﻿using Ninject;
using System.Collections.Generic;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTD.Data.Repository.Interface;
using MTData.Utility;
using MTD.Core.Extension;
using MTD.Data.Data;
using ResourceData = MTD.Core.Resource.Resource;

namespace MTD.Core.Service
{
    public class CarOwnersService : ICarOwnersService
    {
        private readonly ICarOwnersRepository _carOwnerRepository;

        public CarOwnersService([Named("CarOwnersRepository")] ICarOwnersRepository ownerRepository)
        {
            _carOwnerRepository = ownerRepository;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="carownerDto"></param>
        /// <returns></returns>
        public bool Add(CarOwnersDto carownerDto)
        {
            var carowner = carownerDto.ToCarOwner();
            return _carOwnerRepository.Add(carowner);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CarOwnersDto Read(int id)
        {
            CarOwner carOwner = _carOwnerRepository.Read(id);
            CarOwnersDto cdto = carOwner.ToCarOwner();
            return cdto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="carownerDto"></param>
        /// <returns></returns>
        public bool Update(CarOwnersDto carownerDto)
        {
            var carowner = carownerDto.ToCarOwner();
            return _carOwnerRepository.Update(carowner);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Remove(int id)
        {
            return _carOwnerRepository.Remove(id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public int GetOwnerCount(string name ,int merchantId, string ownerName)
        {
            try
            {
                return _carOwnerRepository.GetOwnerCount(name, merchantId, ownerName);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objCarOwnerDto"></param>
        /// <returns></returns>
        public List<CarOwnersDto> AddCarOwnewrList(List<CarOwnersDto> objCarOwnerDto)
        {
            try
            {
                string _Error = string.Empty;
                List<CarOwnersDto> CarOwnerExcelUpload = new List<CarOwnersDto>();
                int response = 0;
                bool isAdd = false;

                foreach (CarOwnersDto item in objCarOwnerDto)
                {
                    CarOwner carOwner = item.ToCarOwner();
                    response = _carOwnerRepository.IsValidCarOwner(carOwner);
                    switch (response)
                    {
                        case 2:
                            _Error = ResourceData.Email_Id_Required; // "Email Id is required.";
                            break;
                        case 3:
                            _Error = ResourceData.Address_Required; // "Address is required.";
                            break;
                        case 4:
                            _Error = ResourceData.Country_not_exist; // "Country does not exists.";
                            break;
                        case 5:
                            _Error = ResourceData.Email_already_exist; // "Email Id already exists.";
                            break;
                        case 6:
                            _Error = ResourceData.Country_required; // "Country is required.";
                            break;
                        case 7:
                            _Error = ResourceData.Contact_Number_minimum_length; // "Contact Number must be minimum length of 6.";
                            break;
                        case 8:
                            _Error = ResourceData.Contact_Person_required; // "Contact Person is required.";
                            break;
                        case 9:
                            _Error = ResourceData.Invalid_Email_Id; // "Invalid Email Id.";
                            break;
                        case 10:
                            _Error = ResourceData.Invalid_Owner_Name; // "Invalid Owner Name.";
                            break;
                        case 11:
                            _Error = ResourceData.Owner_Name_max_length; // "Owner Name must not exceed the length of 100.";
                            break;
                        case 12:
                            _Error = ResourceData.Invalid_Contact_person; // "Invalid Contact person.";
                            break;
                        case 13:
                            _Error = ResourceData.Invalid_Contact; // "Invalid Contact.";
                            break;
                        case 14:
                            _Error = ResourceData.Contact_max_length; // "Contact must not exceed the length of 20.";
                            break;
                        case 15:
                            _Error = ResourceData.Invalid_State; // "Invalid State.";
                            break;
                        case 16:
                            _Error = ResourceData.State_max_length; // "State must not exceed the length of 40.";
                            break;
                        case 17:
                            _Error = ResourceData.Invalid_City; // "Invalid City.";
                            break;
                        case 18:
                            _Error = ResourceData.City_max_length; // "City must not exceed the length of 40.";
                            break;
                        case 19:
                            _Error = ResourceData.Invalid_Sage_Account; // "Invalid Sage Account.";
                            break;
                        case 20:
                            _Error = ResourceData.Sage_Account_max_length; // "Sage Account must not exceed the length of 50.";
                            break;
                        case 21:
                            _Error = ResourceData.Invalid_Bank_Name; // "Invalid Bank Name.";
                            break;
                        case 22:
                            _Error = ResourceData.Bank_Name_max_length; // "Bank Name must not exceed the length of 50.";
                            break;
                        case 23:
                            _Error = ResourceData.Invalid_Account_Name; // "Invalid Account Name.";
                            break;
                        case 24:
                            _Error = ResourceData.Account_Name_max_length; // "Account Name must not exceed the length of 50.";
                            break;
                        case 25:
                            _Error = ResourceData.Invalid_BSB; // "Invalid BSB.";
                            break;
                        case 26:
                            _Error = ResourceData.BSB_max_length; // "BSB  must not exceed the length of 50.";
                            break;
                        case 27:
                            _Error = ResourceData.Invalid_Account; // "Invalid Account.";
                            break;
                        case 28:
                            _Error = ResourceData.Account_max_length; // "Account must not exceed the length of 50.";
                            break;
                        case 29:
                            _Error = ResourceData.Contact_Person_max_length; // "Contact Person must not exceed the length of 100.";
                            break;
                    }

                    if (response == 0)
                    {
                        CarOwner carOwnerDb = _carOwnerRepository.CarOwnerExcel(carOwner);

                        if (!string.IsNullOrEmpty(carOwnerDb.Account))
                            carOwnerDb.Account = StringExtension.EncryptDriver(carOwnerDb.Account);

                        if (!string.IsNullOrEmpty(carOwnerDb.SageAccount))
                            carOwnerDb.SageAccount = StringExtension.EncryptDriver(carOwnerDb.SageAccount);

                        isAdd = _carOwnerRepository.Add(carOwnerDb);
                       
                        if (!isAdd)
                            CarOwnerExcelUpload.Add(new CarOwnersDto() { OwnerName = item.OwnerName, Contact = item.Contact,ContactPerson = item.ContactPerson,EmailAddress = item.EmailAddress, Country = item.Country, State = item.State, City = item.City, Address = item.Address,SageAccount = item.SageAccount, BankName = item.BankName, AccountName = item.AccountName, BSB = item.BSB, Account = item.Account, FailedError = _Error });
                    }

                    if (response != 0)
                        CarOwnerExcelUpload.Add(new CarOwnersDto() { OwnerName = item.OwnerName, Contact = item.Contact, ContactPerson = item.ContactPerson, EmailAddress = item.EmailAddress, Country = item.Country, State = item.State, City = item.City, Address = item.Address, SageAccount = item.SageAccount, BankName = item.BankName, AccountName = item.AccountName, BSB = item.BSB, Account = item.Account,FailedError = _Error });
                }

                return CarOwnerExcelUpload;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterSearch"></param>
        /// <returns></returns>
        public IEnumerable<CarOwnersDto> GetOwnerListByFilter(FilterSearchParametersDto filterSearch)
        {
            var filterSearchClass = filterSearch.SearchParameters();
            var ownerList = _carOwnerRepository.GetOwnerListByFilter(filterSearchClass);
            var ownerListDto = ownerList.ToCarOwnerList();
            return ownerListDto;
        }
        
    }
}
