﻿using System;
using System.Net.Http;
using System.Text;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.WebAPI.Models;
using MTData.WebAPI.Resources;
using MTData.WebAPI.Common;
using MTData.WebAPI.CanadaCert;
using MTData.WebAPI.CanadaCommon;
using MTData.WebAPI.CnPrivateBit63;

namespace MTData.WebAPI.Controllers
{
    public class CapKeyController : TransactionRequestController
    {
        #region (Constructor Loading)

        private readonly ITransactionService _transactionService;
        private readonly ITerminalService _terminalService;
        private readonly IMerchantService _merchantService;
        private readonly ICommonService _commonService;

        private LocalVariables objLocalVars = new LocalVariables();
        private readonly StringBuilder _logMessage;
        private StringBuilder _logTracking;
        readonly ILogger _logger = new Logger();

        public CapKeyController(
            [Named("TransactionService")] ITransactionService transactionService,
            [Named("MerchantService")] IMerchantService merchantService,
            [Named("TerminalService")] ITerminalService terminalService,
            [Named("CommonService")] ICommonService commonService,
            StringBuilder logMessage, StringBuilder logTracking) : base(terminalService, logMessage)
        {
            _merchantService = merchantService;
            _transactionService = transactionService;
            _terminalService = terminalService;
            _logMessage = logMessage;
            _commonService = commonService;
            _logTracking = logTracking;
        }

        PaymentResponse response = new PaymentResponse();
        EmvCapkResponse capkresponse = new EmvCapkResponse();

        private const string tableFBId4642 = "4642";
        private const string tableFBId4631 = "4631";
        private const int successCode = 1;
        private const int downloadAvailable = 703;
        private const string fbSeqContinue = "C";
        private const string fbSeqLast = "L";       

        #endregion

        #region (Capk Process)

        /// <summary>
        /// Purpose             :   service to generate CAPK for POS
        /// Function Name       :   SendPayment
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   10/26/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>Sending data...
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult CapkUpdate(HttpRequestMessage request)
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            _logMessage.AppendLine("Creating object of LoginHeader Class as mHeader");

            LoginHeader mHeader = new LoginHeader();
            try
            {
                XmlDocument xmlDoc = new XmlDocument();

                _logMessage.AppendLine("Loading request XML data.");

                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                _logTracking.AppendLine("Payment process start. DriverNo: " + objLocalVars.DriverNo + " and  DeviceId " + objLocalVars.DeviceId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());

                _logMessage.AppendLine("Initializing local variables from XML request data");

                // Initialize capk local variable from XML Request.
                if (!SetLocalVarFromCapkXMLRequest(xmlDoc))
                {
                    _logMessage.AppendLine("Failed to initializ capk local variables from request XML data.");
                    _logger.LogInfoMessage(_logMessage.ToString());
                    return Ok(capkresponse);
                }

                // Validate Capk Request
                dynamic firstDataDetails = string.Empty;
                _logMessage.AppendLine("Validating capk request xml data");
                if (!ValidateCapkRequest(capkresponse, mHeader, out firstDataDetails))
                {
                    _logMessage.AppendLine("Failed to capk validate request xml data.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    return Ok(capkresponse);
                }

                // Creating Transaction Request as per UMF specification.
                _logMessage.AppendLine("Loading XML request data into TransactionRequest");
                TransactionRequest tr;
                try
                {
                    tr = InitiateCapkTransaction(firstDataDetails);
                }
                catch (Exception ex)
                {
                    _logMessage.AppendLine("Failed to load XML request data into TransactionRequest");
                    _logger.LogInfoFatal(_logMessage.ToString(), ex);

                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidTransactionRequest, objLocalVars.DeviceId, objLocalVars.RequestId);
                    return Ok(capkresponse);
                }

                _logMessage.AppendLine("Creating transaction request.");
                CreateTransactionRequest(tr);

                string xmlSerializedTransReq = GetXmlData();
                _logMessage.AppendLine("GetXMLData exceuted successfully");

                // Validating Capk XML request
                _logMessage.AppendLine("Validationg XML request");

                int transId;
                XmlDocument ValidateXml = new XmlDocument();
                ValidateXml.LoadXml(xmlSerializedTransReq);
                XmlElement elem = (XmlElement)ValidateXml.DocumentElement.FirstChild;

                if (elem.IsNull())
                {
                    _logMessage.AppendLine("Failed to validationg XML request.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidTransactionRequest, objLocalVars.DeviceId, objLocalVars.RequestId);
                    return Ok(capkresponse);
                }

                // Save transaction into MTDTransaction Table
                _logMessage.AppendLine("calling XmlToTransactionDto takes xmlSerializedTransReq, tr");

                MTDTransactionDto transactionDto = CommonFunctions.CapkXmlToTransactionDto(tr);

                _logMessage.AppendLine("Inserting Transaction.");

                transId = _transactionService.Add(transactionDto);

                _logMessage.AppendLine("Update stan parameters" + transactionDto.Stan + "--" + transactionDto.TransRefNo + "--" + tr.TerminalId + "--" + tr.MtdMerchantId);

                _terminalService.UpdateStan(transactionDto.Stan, transactionDto.TransRefNo, tr.TerminalId, tr.MtdMerchantId);

                string clientRef = GetClientRef(firstDataDetails.ProjectId);

                _logMessage.AppendLine("GetClientRef executed successfully" + clientRef);
                //end Save transaction into MTDTransaction Table 

                /* Send data using SOAP protocol to Datawire*/
                _logMessage.AppendLine("Sending transaction request to rapid conect");

                string xmlSerializedTransResp = new SoapHandler().SendMessage(xmlSerializedTransReq, clientRef, tr);
                _logMessage.AppendLine("SendMessage executed successfully" + xmlSerializedTransResp);

                // update transaction response into database
                if (string.IsNullOrEmpty(xmlSerializedTransResp))
                {
                    _logMessage.AppendLine("xmlSerializedTransResp is null");
                    MTDTransactionDto responsetransactionDto = CommonFunctions.XmlEmvCapkResponseToTransactionDto(xmlSerializedTransResp, true, tr.TransType.ToString());

                    _transactionService.Update(responsetransactionDto, transId, tr.RapidConnectAuthId);
                    _logMessage.AppendLine("xmlSerializedTransResp is null updated into database");
                }
                else
                {
                    if (tr.IndustryType == "CAPKDownload")
                    {
                        _logMessage.AppendLine("xmlSerializedTransResp is not null");
                        MTDTransactionDto responsetransactionDto = CommonFunctions.XmlEmvCapkResponseToTransactionDto(xmlSerializedTransResp, false, tr.TransType.ToString());

                        _transactionService.Update(responsetransactionDto, transId, tr.RapidConnectAuthId);
                        _logMessage.AppendLine("xmlSerializedTransResp is not null updated");

                        XmlDocument xmlDocu = new XmlDocument();
                        xmlSerializedTransResp = xmlSerializedTransResp.HandleCdata();
                        xmlDocu.LoadXml(xmlSerializedTransResp);
                        string xmlString = CommonFunctions.GetXmlAsString(xmlDocu);
                        var response = CommonFunctions.GetCapkResponse(xmlString);
                        xmlDocu.LoadXml(response);

                        StringBuilder cap = new StringBuilder();
                        StringBuilder cap12 = new StringBuilder();
                        var fbseq = xmlDocu.SelectSingleNode("AdminResponse/FileDLGrp/FunCode");
                        {
                            if (fbseq.InnerText == "C")
                            {
                                var capkData1 = xmlDocu.SelectSingleNode("AdminResponse/FileDLGrp/FBData");
                                {
                                    cap.Append(capkData1.InnerText).Append(" \n ");
                                }

                                DeviceCapk objCapk = new DeviceCapk(_transactionService, _terminalService, _merchantService, _logMessage);
                                for (int i = 0; i <= 14; i++)
                                {
                                    xmlSerializedTransResp = objCapk.CapkFile(tr, transId, clientRef, xmlSerializedTransResp);
                                    string xmlSerializedTransResp1 = xmlSerializedTransResp;

                                    XmlDocument xmlDocum = new XmlDocument();
                                    xmlSerializedTransResp1 = xmlSerializedTransResp1.HandleCdata();
                                    xmlDocum.LoadXml(xmlSerializedTransResp1);

                                    string xmlString1 = CommonFunctions.GetXmlAsString(xmlDocum);
                                    var response1 = CommonFunctions.GetCapkResponse(xmlString1);
                                    xmlDocum.LoadXml(response1);
                                    var capkData12 = xmlDocum.SelectSingleNode("AdminResponse/FileDLGrp/FBData");
                                    {
                                        cap12.Append(capkData12.InnerText).Append("\n");
                                        cap.Append(cap12.ToString());
                                    }
                                }

                                tr.Capkdata = cap.ToString();
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }
                }

                // Sending timeout
                if (string.IsNullOrEmpty(xmlSerializedTransResp))
                {
                    _logMessage.AppendLine("creating timeout trasnaction after 35 seconds");
                    TimeoutTransaction objTimeout = new TimeoutTransaction(_transactionService, _terminalService, _merchantService, _logMessage);
                    xmlSerializedTransResp = objTimeout.TimeOut(tr, transId, clientRef, xmlSerializedTransResp);
                }

                EmvCapkResponse CapkupdateResponse = new EmvCapkResponse();
                if (!string.IsNullOrEmpty(xmlSerializedTransResp))
                    CapkupdateResponse = CommonFunctions.CapkTransactionResponse(tr, xmlSerializedTransResp, tr.IndustryType, transId);

                _logTracking.AppendLine("Payment process completed. DriverNo: " + objLocalVars.DriverNo + " and  DeviceId " + objLocalVars.DeviceId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());
                _logMessage.AppendLine("SendPayment process completed successfully.");
                _logger.LogInfoMessage(_logTracking.ToString());
                _logger.LogInfoMessage(_logMessage.ToString());

                return Ok(CapkupdateResponse);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                capkresponse.Header = CommonFunctions.FailedResponse(PaymentAPIResources.Exception, objLocalVars.DeviceId, objLocalVars.RequestId);

                return Ok(capkresponse);
            }
        }

        /// <summary>
        /// Purpose             :   Extract the value from XMLrequest and set to Local variables
        /// Function Name       :   SetLocalVarFromCapkXMLRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   10/26/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private bool SetLocalVarFromCapkXMLRequest(XmlDocument xmlDoc)
        {
            try
            {
                var dvcID = xmlDoc.SelectSingleNode("PaymentRequest/Header/DeviceId");
                if (!dvcID.IsNull())
                    objLocalVars.DeviceId = dvcID.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/DeviceId".MissingElementMessage());
                    return false;
                }

                var reqID = xmlDoc.SelectSingleNode("PaymentRequest/Header/RequestId");
                if (!reqID.IsNull())
                    objLocalVars.RequestId = reqID.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/RequestId".MissingElementMessage());
                    return false;
                }

                var drvrID = xmlDoc.SelectSingleNode("PaymentRequest/Header/DriverId");
                if (!drvrID.IsNull())
                    objLocalVars.DriverNo = drvrID.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/DriverId".MissingElementMessage(), objLocalVars.DeviceId, objLocalVars.RequestId);
                    return false;
                }

                var token = xmlDoc.SelectSingleNode("PaymentRequest/Header/Token");
                if (!token.IsNull())
                    objLocalVars.Token = token.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/Token".MissingElementMessage(), objLocalVars.DeviceId, objLocalVars.RequestId);
                    return false;
                }

                var requestType = xmlDoc.SelectSingleNode("PaymentRequest/Header/RequestType");
                if (!requestType.IsNull())
                    objLocalVars.RequestType = requestType.InnerText;
                else
                    objLocalVars.RequestType = null;

                var rapidConnectAuthId = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/RapidConnectAuthId");
                if (!rapidConnectAuthId.IsNull())
                    objLocalVars.RapidConnectAuthId = rapidConnectAuthId.InnerText;
                else
                    objLocalVars.RapidConnectAuthId = null;

                var transType = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/TransactionType");
                if (!transType.IsNull())
                    objLocalVars.TxnType = transType.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/TransactionType".MissingElementMessage(), objLocalVars.DeviceId, objLocalVars.RequestId);
                    return false;
                }

                if ((objLocalVars.TxnType != PaymentTransactionType.Void.ToString()) && (!transType.IsNull()))
                    objLocalVars.TransType = (TxnTypeType)Enum.Parse(typeof(TxnTypeType), transType.InnerText);

                var indType = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/IndustryType");
                if (!indType.IsNull())
                {
                    objLocalVars.IndustryType = indType.InnerText;
                    if (string.IsNullOrEmpty(objLocalVars.IndustryType))
                    {
                        capkresponse.Header = CommonFunctions.FailedResponse(PaymentAPIResources.InvalidIndustryType, objLocalVars.DeviceId, objLocalVars.RequestId);
                        return false;
                    }
                }
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/IndustryType".MissingElementMessage(), objLocalVars.DeviceId, objLocalVars.RequestId);
                    return false;
                }

                var fleetId = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/FleetId");
                if (!fleetId.IsNull())
                    objLocalVars.FleetId = fleetId.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/FleetId".MissingElementMessage(), objLocalVars.DeviceId, objLocalVars.RequestId);
                    return false;
                }

                return true;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To initialize capk TransactionRequest.
        /// Function Name       :   InitiateCapkTransaction
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   10/26/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private TransactionRequest InitiateCapkTransaction(dynamic firstDataDetails)
        {
            TransactionRequest CApktr = new TransactionRequest
            {
                SerialNumber = objLocalVars.DeviceId,
                RequestedId = objLocalVars.RequestId,
                TransactionType = objLocalVars.TxnType,
                TransType = objLocalVars.TransType,
                IndustryType = objLocalVars.IndustryType,
                DriverNumber = objLocalVars.DriverNo,
                RapidConnectAuthId = objLocalVars.RapidConnectAuthId,
                FleetId = objLocalVars.FleetId,
                LocalDateTime = DateTime.Now.ToString("yyyyMMddHHmmss"),
                TrnmsnDateTime = DateTime.UtcNow.ToString("yyyyMMddHHmmss"),
                FdMerchantId = firstDataDetails.FDMerchantID,
                MtdMerchantId = firstDataDetails.fk_MerchantID,
                TerminalId = firstDataDetails.RCTerminalId,
                TppId = firstDataDetails.ProjectId,
                GroupId = firstDataDetails.GroupId,
                TokenType = firstDataDetails.TokenType,
                MCCode = firstDataDetails.MCCode,
                EncryptionKey = firstDataDetails.EncryptionKey,
                Did = firstDataDetails.DID,
                App = firstDataDetails.App,
                FbSeq = objLocalVars.FbSeq,
                Capkdata = objLocalVars.CapData,
                ReqFileOff = objLocalVars.ReqFileOff,
                ServiceId = firstDataDetails.serviceId,
                ServiceUrl = string.Empty,
                CapkFileCreationDt = objLocalVars.CapkFileCreationDt,
                fileSize = objLocalVars.FileSize,
                fileCRC16 = objLocalVars.FileCRC16
            };

            return CApktr;
        }

        /// <summary>
        /// Validating payment request.
        /// Purpose             :   Validate capk reqest
        /// Function Name       :   ValidateCapkRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   10/26/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :    "MM/DD/YYYY" 
        /// <param name="response"></param>
        /// <param name="mHeader"></param>
        /// <param name="firstDataDetails"></param>
        /// <param name="isFailed"></param>
        /// <returns>bool</returns>
        private bool ValidateCapkRequest(EmvCapkResponse capkresponse, LoginHeader mHeader, out dynamic firstDataDetails)
        {
            bool isPass = true;
            try
            {
                if (string.IsNullOrEmpty(objLocalVars.DeviceId))
                {
                    isPass = false;
                    capkresponse.Header = CommonFunctions.FailedResponse(PaymentAPIResources.DeviceIdRequired, objLocalVars.DeviceId, objLocalVars.RequestId);
                }

                bool isToken = string.IsNullOrEmpty(objLocalVars.Token);
                if (isToken)
                    firstDataDetails = _commonService.GetFirstDataDetails(objLocalVars.DeviceId, objLocalVars.DriverNo, (string)null, Convert.ToInt32(objLocalVars.FleetId), objLocalVars.RequestType);
                else
                    firstDataDetails = _commonService.GetFirstDataDetails(objLocalVars.DeviceId, objLocalVars.DriverNo, objLocalVars.Token, Convert.ToInt32(objLocalVars.FleetId), objLocalVars.RequestType);

                if (firstDataDetails == null)
                {
                    isPass = false;
                    capkresponse.Header = CommonFunctions.FailedResponse(PaymentAPIResources.UnauthorizedAccess, objLocalVars.DeviceId, objLocalVars.RequestId);
                }

                return isPass;
            }
            catch
            {
                throw;
            }
        }

        //------------------ New Code ------------------//

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Purpose             :   To check CapKey daily and update EMVContact.xml if update available.
        /// Function Name       :   ManageCapk
        /// Created By          :   Sunil Singh
        /// Created On          :   28-Jan-2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult ManageCapk()
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                string projectType = WebConfigurationManager.AppSettings["Project"].ToString();
                if (projectType == PaymentAPIResources.CanadaProject)
                {
                    _logMessage.AppendLine("CapKey download process started for Canada.");
                    CnDownloadCapk();
                }
                else if (projectType == PaymentAPIResources.USProject)
                {
                    _logMessage.AppendLine("CapKey dowwnload process started for US.");
                    _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                    _logMessage.AppendLine("ManageCapk process start. Loading local variables.");

                    objLocalVars.DeviceId = WebConfigurationManager.AppSettings["BookerAppDeviceId"];
                    objLocalVars.DriverNo = string.Empty;
                    dynamic firstDataDetails = string.Empty;

                    //objLocalVars.TxnType = "FileDownload";
                    //objLocalVars.TransType = (TxnTypeType)Enum.Parse(typeof(TxnTypeType), "FileDownload");
                    objLocalVars.TxnType = SigmaStaticValue.FileDownload;
                    objLocalVars.TransType = (TxnTypeType)Enum.Parse(typeof(TxnTypeType), SigmaStaticValue.FileDownload);

                    // Check whether download available
                    _logMessage.AppendLine("Checking whether CapKey available.");
                    var resp = CheckForCapkUpdate(out firstDataDetails);
                    if (resp[0] == successCode)//i.e success response
                    {
                        _logMessage.AppendLine("Checking check process completed.");
                        if (resp[1] == downloadAvailable)   //i.e Capk update required
                        {
                            // Process to download CapK
                            _logMessage.AppendLine("CapKey available for download.");

                            CapkData capkData = DownloadCapK(firstDataDetails);

                            _logMessage.AppendLine("CapKey downloaded.");

                            if (capkData.Status)
                            {
                                _logMessage.AppendLine("Reading raw capkeys for EMV Contact.");

                                var rawCapKeys = capkData.CapK.ReadRawCapKey();
                                if (!rawCapKeys.IsNull())
                                {
                                    _logMessage.AppendLine("Updating CapKeys into XML file.");

                                    capkData.XMLFileData = rawCapKeys.UpdateXMLFile();

                                    _logMessage.AppendLine("CapKeys updated.");

                                    // Save data
                                    _logMessage.AppendLine("Initializing CapKeyDto to save CapKey into databse.");

                                    CapKeyDto cpkDto = new CapKeyDto();
                                    cpkDto.DownloadDate = DateTime.Now;
                                    cpkDto.IsNew = true;
                                    cpkDto.RawData = capkData.CapK;
                                    cpkDto.RequestXML = capkData.Request;
                                    cpkDto.ResponseXML = capkData.Response;
                                    cpkDto.XmlFile = capkData.XMLFileData.OuterXml;
                                    cpkDto.FileCreationDate = capkData.CurrFileCreationDt;
                                    cpkDto.FileCRC16 = capkData.FileCRC16;
                                    cpkDto.FileSize = capkData.FileSize;
                                    cpkDto.FileType = capkData.FileType;
                                    cpkDto.FunCode = capkData.FunCode;
                                    cpkDto.IsContactLess = false;

                                    _logMessage.AppendLine("Saving CapKey into database");

                                    int transId = _transactionService.SaveCapKey(cpkDto);

                                    _logMessage.AppendLine("CapKey saved into database");
                                }

                                _logMessage.AppendLine("Reading raw capkeys for EMV ContactLess.");

                                var rawCapKeysCLS = capkData.CapK.ClessReadRawCapKey();
                                if (!rawCapKeys.IsNull())
                                {
                                    _logMessage.AppendLine("Updating CLS CapKeys into XML file.");

                                    capkData.XMLFileData = rawCapKeysCLS.UpdateXMLFileCless();

                                    _logMessage.AppendLine("CLS CapKeys updated.");

                                    //Save data
                                    _logMessage.AppendLine("Initializing CapKeyDto to save CapKey into databse.");

                                    CapKeyDto cpkDto = new CapKeyDto();
                                    cpkDto.DownloadDate = DateTime.Now;
                                    cpkDto.IsNew = true;
                                    cpkDto.RawData = capkData.CapK;
                                    cpkDto.RequestXML = capkData.Request;
                                    cpkDto.ResponseXML = capkData.Response;
                                    cpkDto.XmlFile = capkData.XMLFileData.OuterXml;
                                    cpkDto.FileCreationDate = capkData.CurrFileCreationDt;
                                    cpkDto.FileCRC16 = capkData.FileCRC16;
                                    cpkDto.FileSize = capkData.FileSize;
                                    cpkDto.FileType = capkData.FileType;
                                    cpkDto.FunCode = capkData.FunCode;
                                    cpkDto.IsContactLess = true;

                                    _logMessage.AppendLine("Saving CapKey into database");
                                    int transId = _transactionService.SaveCapKey(cpkDto);
                                    _logMessage.AppendLine("CapKey saved into database");
                                }
                            }
                        }
                        else
                        {
                            _logMessage.AppendLine("CapKey not available for download.");
                        }
                    }
                    else
                    {
                        _logMessage.AppendLine("CheckForCapkUpdate execution failed");
                    }
                }
                else
                {
                    _logMessage.AppendLine("Unknown Request.");
                }

                _logMessage.AppendLine("ManageCapk process completed successfully.");

                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Ok();
            }
            finally
            {
                _logger.LogInfoMessage(_logMessage.ToString());
            }
        }

        /// <summary>
        /// Purpose             :   To check whether CapK update is available.
        /// Function Name       :   CheckForCapkUpdate
        /// Created By          :   Sunil Singh
        /// Created On          :   28-Jan-2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="firstDataDetails"></param>
        /// <returns></returns>
        private int[] CheckForCapkUpdate(out dynamic firstDataDetails)
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int[] resp = { 0, 0 };
                objLocalVars.IndustryType = "CAPKUpdate";

                // Fetching Terminal and Merchant detail from database
                firstDataDetails = _commonService.GetFirstDataDetails(objLocalVars.DeviceId, objLocalVars.DriverNo, (string)null, Convert.ToInt32(objLocalVars.FleetId), objLocalVars.RequestType);
                if (firstDataDetails == null)
                {
                    _logMessage.AppendLine(string.Format("DeviceID:{0};DriverID:{1};FleetID:{2};RequestType:{3};", objLocalVars.DeviceId, objLocalVars.DriverNo, Convert.ToInt32(objLocalVars.FleetId), objLocalVars.RequestType));
                    _logger.LogWarningMessage(_logMessage.ToString());

                    resp[0] = 0; //i.e process failed.
                    return resp;
                }

                LoadCapkRequestFromDB();

                #region Creating Transaction Request as per UMF specification.
                _logMessage.AppendLine("Loading XML request data into TransactionRequest");
                TransactionRequest tr = new TransactionRequest();

                try
                {
                    tr = InitiateCapkTransaction(firstDataDetails);
                }
                catch (Exception ex)
                {
                    _logMessage.AppendLine("Failed to load XML request data into TransactionRequest");
                    _logger.LogInfoFatal(_logMessage.ToString(), ex);

                    resp[0] = 0;    //i.e process failed.
                    return resp;
                }

                _logMessage.AppendLine("Creating transaction request.");
                CreateTransactionRequest(tr);

                string xmlSerializedTransReq = GetXmlData();
                _logMessage.AppendLine("GetXMLData exceuted successfully");
                #endregion

                #region Validating Capk XML request
                _logMessage.AppendLine("Validationg XML request");
                XmlDocument ValidateXml = new XmlDocument();
                ValidateXml.LoadXml(xmlSerializedTransReq);
                XmlElement elem = (XmlElement)ValidateXml.DocumentElement.FirstChild;
                if (elem.IsNull())
                {
                    _logMessage.AppendLine("Failed to validationg XML request.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    resp[0] = 0;//  i.e process failed.
                    return resp;
                }
                #endregion

                string clientRef = GetClientRef(firstDataDetails.ProjectId);

                /* Send data using SOAP protocol to Datawire*/
                _logMessage.AppendLine("Sending transaction request to rapid conect");
                string xmlSerializedTransResp = new SoapHandler().SendMessage(xmlSerializedTransReq, clientRef, tr);
                _logMessage.AppendLine("SendMessage executed successfully" + xmlSerializedTransResp);

                if (!string.IsNullOrEmpty(xmlSerializedTransResp))
                    resp = CommonFunctions.CapkResponse(xmlSerializedTransResp);

                return resp;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To download CapK from server.
        /// Function Name       :   DownloadCapK
        /// Created By          :   Sunil Singh
        /// Created On          :   28-Jan-2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="firstDataDetails"></param>
        /// <returns></returns>
        private CapkData DownloadCapK(dynamic firstDataDetails)
        {
            CapkData capkDto = new CapkData();
            objLocalVars.IndustryType = "CAPKDownload";

            // Creating Transaction Request as per UMF specification.
            _logMessage.AppendLine("Loading XML request data into TransactionRequest");
            TransactionRequest tr = new TransactionRequest();
            try
            {
                tr = InitiateCapkTransaction(firstDataDetails);
            }
            catch (Exception ex)
            {
                _logMessage.AppendLine("Failed to load XML request data into TransactionRequest");
                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                capkDto.Status = false;
                return capkDto;
            }

            _logMessage.AppendLine("Creating transaction request.");
            CreateTransactionRequest(tr);

            string xmlSerializedTransReq = GetXmlData();
            _logMessage.AppendLine("GetXMLData exceuted successfully");

            // Validating Capk XML request
            _logMessage.AppendLine("Validationg XML request");
            XmlDocument ValidateXml = new XmlDocument();
            ValidateXml.LoadXml(xmlSerializedTransReq);
            XmlElement elem = (XmlElement)ValidateXml.DocumentElement.FirstChild;
            if (elem.IsNull())
            {
                _logMessage.AppendLine("Failed to validationg XML request.");
                _logger.LogInfoMessage(_logMessage.ToString());

                capkDto.Status = false;
                return capkDto;
            }

            string clientRef = GetClientRef(firstDataDetails.ProjectId);

            /* Send data using SOAP protocol to Datawire*/
            _logMessage.AppendLine("Sending transaction request to rapid conect");
            string xmlSerializedTransResp = new SoapHandler().SendMessage(xmlSerializedTransReq, clientRef, tr);
            _logMessage.AppendLine("SendMessage executed successfully" + xmlSerializedTransResp);

            if (!string.IsNullOrEmpty(xmlSerializedTransResp))
            {
                StringBuilder sbReqXML = new StringBuilder();
                StringBuilder sbRespXML = new StringBuilder();
                StringBuilder sbCapK = new StringBuilder();

                sbReqXML.AppendLine(xmlSerializedTransReq);
                sbRespXML.AppendLine(xmlSerializedTransResp);

                // Checking Response type and Response code
                var resp = CommonFunctions.CapkResponse(xmlSerializedTransResp);
                if (resp[0] == successCode) //i.e success response
                {
                    if (resp[1] == downloadAvailable)   //Download available
                    {
                        // Process to download CapK
                        _logMessage.AppendLine("xmlSerializedTransResp is not null");

                        XmlDocument xmlDocu = new XmlDocument();
                        XNamespace ns = "com/firstdata/Merchant/gmfV4.02";
                        xmlSerializedTransResp = xmlSerializedTransResp.HandleCdata();//To handle invalid request error data
                        xmlDocu.LoadXml(xmlSerializedTransResp);
                        var requestXML = XDocument.Parse(xmlDocu.InnerXml);
                        var fileDLGrp = requestXML.Descendants(ns + "FileDLGrp").FirstOrDefault();
                        var fbseq = (string)fileDLGrp.Element(ns + "FunCode");
                        var capkData1 = (string)fileDLGrp.Element(ns + "FBData");

                        sbCapK.AppendLine(capkData1);

                        if (fbseq.ToUpper() == fbSeqContinue)
                        {
                            DeviceCapk objCapk = new DeviceCapk(_transactionService, _terminalService, _merchantService, _logMessage);
                            while (true)
                            {
                                var cpKData = objCapk.SendCapkTransactionNew(tr, clientRef, xmlSerializedTransResp);
                                xmlSerializedTransResp = cpKData.Response;

                                XmlDocument xmlDocum = new XmlDocument();
                                xmlSerializedTransResp = xmlSerializedTransResp.HandleCdata();
                                xmlDocum.LoadXml(xmlSerializedTransResp);

                                sbReqXML.AppendLine(cpKData.Request);
                                sbRespXML.AppendLine(cpKData.Response);

                                requestXML = XDocument.Parse(xmlDocum.InnerXml);
                                fileDLGrp = requestXML.Descendants(ns + "FileDLGrp").FirstOrDefault();
                                if (fileDLGrp != null)
                                {
                                    sbCapK.AppendLine((string)fileDLGrp.Element(ns + "FBData"));
                                    fbseq = (string)fileDLGrp.Element(ns + "FunCode");
                                }

                                if (fbseq.ToUpper() == fbSeqLast)
                                {
                                    capkDto.CurrFileCreationDt = (string)fileDLGrp.Element(ns + "CurrFileCreationDt");
                                    capkDto.FileCRC16 = (string)fileDLGrp.Element(ns + "FileCRC16");
                                    capkDto.FileSize = (string)fileDLGrp.Element(ns + "FileSize");
                                    capkDto.FileType = (string)fileDLGrp.Element(ns + "FileType");
                                    capkDto.FunCode = (string)fileDLGrp.Element(ns + "FunCode");
                                    break;
                                }
                            }

                            //preparing result
                            capkDto.Request = sbReqXML.ToString();
                            capkDto.Response = sbRespXML.ToString();
                            capkDto.CapK = sbCapK.ToString();
                            capkDto.Status = true;
                        }
                    }
                }
            }
            else
            {
                _logMessage.AppendLine("xmlSerializedTransResp is null");
                _logger.LogInfoMessage(_logMessage.ToString());

                capkDto.Status = false;
                return capkDto;
            }

            return capkDto;
        }

        /// <summary>
        /// Purpose             :   To download CapK xml file from server.
        /// Function Name       :   DownloadCapKeyFile
        /// Created By          :   Sunil Singh
        /// Created On          :   28-Jan-2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult DownloadCapKeyFile(HttpRequestMessage request)
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            
            CapKeyResponse resp = new CapKeyResponse();
            try
            {
                XmlDocument xmlDoc = new XmlDocument();

                _logMessage.AppendLine("Loading request XML data.");

                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                _logTracking.AppendLine("Payment process start. DriverNo: " + objLocalVars.DriverNo + " and  DeviceId " + objLocalVars.DeviceId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());

                _logMessage.AppendLine("Initializing local variables from XML request data");
                
                // Initialize capk local variable from XML Request.
                if (!SetLocalVarFromCapkXMLRequest(xmlDoc))
                {
                    _logMessage.AppendLine("Failed to initializ capk local variables from request XML data.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    resp = CapkUtil.CapKeyResponse(false, PaymentAPIResources.InvalidRequest);
                    return Ok(resp);
                }

                // Validate Capk Request
                dynamic firstDataDetails = string.Empty;
                _logMessage.AppendLine("Validating capk request xml data");
                firstDataDetails = _commonService.GetFirstDataDetails(objLocalVars.DeviceId, objLocalVars.DriverNo, objLocalVars.Token, Convert.ToInt32(objLocalVars.FleetId), objLocalVars.RequestType);
                if (firstDataDetails == null)
                {
                    _logMessage.AppendLine("Failed to capk validate request xml data.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    resp = CapkUtil.CapKeyResponse(false, PaymentAPIResources.UnauthorizedAccess);
                    return Ok(resp);
                }

                int terminalId = firstDataDetails.TerminalId;

                bool isCls = _terminalService.IsClsCapKey(terminalId);

                _logMessage.AppendLine("Returning file URL");

                string filePath = string.Empty;

                if (!isCls)
                    filePath = HttpContext.Current.Request.MapPath(@"~\Resources\EMVCONTACT.XML");
                else
                    filePath = HttpContext.Current.Request.MapPath(@"~\Resources\EMVCLESS.XML");

                var capKeyDTO = _transactionService.GetCapKey();
                var capkXML = capKeyDTO.XmlFile;
                XmlDocument cakXMLFile = new XmlDocument();
                cakXMLFile.LoadXml(capkXML);
                cakXMLFile.Save(filePath);

                string path = WebConfigurationManager.AppSettings["CapkFilePath"];
                resp = CapkUtil.CapKeyResponse(true, path);

                _logMessage.AppendLine("DownloadCapKeyFile process completed successfully.");
                _logger.LogInfoMessage(_logMessage.ToString());

                return Ok(resp);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                resp = CapkUtil.CapKeyResponse(false, PaymentAPIResources.Exception);

                return Ok(resp);
            }
        }

        /// <summary>
        /// Purpose             :   To download CapK xml file from server.
        /// Function Name       :   UpdateCapKeyStatus
        /// Created By          :   Sunil Singh
        /// Created On          :   28-Jan-2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult UpdateCapKeyStatus(HttpRequestMessage request)
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            CapKeyResponse resp = new CapKeyResponse();
            try
            {
                XmlDocument xmlDoc = new XmlDocument();

                _logMessage.AppendLine("Loading request XML data.");

                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                _logTracking.AppendLine("Payment process start. DriverNo: " + objLocalVars.DriverNo + " and  DeviceId " + objLocalVars.DeviceId + " and Time Stamp: " + System.DateTime.Now.ToUniversalTime());

                _logMessage.AppendLine("Initializing local variables from XML request data");
                
                // Initialize capk local variable from XML Request.
                if (!LoadCapKeyVariables(xmlDoc))
                {
                    _logMessage.AppendLine("Failed to initializ capk local variables from request XML data.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    resp = CapkUtil.CapKeyResponse(false, PaymentAPIResources.InvalidRequest);
                    return Ok(resp);
                }

                // Validate Capk Request
                dynamic firstDataDetails = string.Empty;
                _logMessage.AppendLine("Validating capk request xml data");
                firstDataDetails = _commonService.GetFirstDataDetails(objLocalVars.DeviceId, objLocalVars.DriverNo, objLocalVars.Token, Convert.ToInt32(objLocalVars.FleetId), objLocalVars.RequestType);
                if (firstDataDetails == null)
                {
                    _logMessage.AppendLine("Failed to capk validate request xml data.");
                    _logger.LogInfoMessage(_logMessage.ToString());

                    resp = CapkUtil.CapKeyResponse(false, PaymentAPIResources.UnauthorizedAccess);
                    return Ok(resp);
                }

                if (objLocalVars.CapKStatus.ToUpper() == "Y")
                {
                    if (_terminalService.UpdateCapVersion(objLocalVars.DeviceId))
                        resp = CapkUtil.CapKeyResponse(true, PaymentAPIResources.CapkUdateSuccess + "for DeviceId " + objLocalVars.DeviceId);
                    else
                        resp = CapkUtil.CapKeyResponse(false, PaymentAPIResources.CapkUdateFailed + "for DeviceId " + objLocalVars.DeviceId);
                }
                else
                    resp = CapkUtil.CapKeyResponse(false, PaymentAPIResources.CapkUdateFailed + "for DeviceId " + objLocalVars.DeviceId);

                _logMessage.AppendLine("DownloadCapKeyFile process completed successfully.");
                _logger.LogInfoMessage(_logMessage.ToString());

                return Ok(resp);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                resp = CapkUtil.CapKeyResponse(false, PaymentAPIResources.Exception);

                return Ok(resp);
            }
        }

        /// <summary>
        /// Purpose             :   Extract the value from XMLrequest and set to Local variables
        /// Function Name       :   SetLocalVarFromCapkXMLRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   10/26/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private bool LoadCapKeyVariables(XmlDocument xmlDoc)
        {
            try
            {
                var dvcID = xmlDoc.SelectSingleNode("PaymentRequest/Header/DeviceId");
                if (!dvcID.IsNull())
                    objLocalVars.DeviceId = dvcID.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/DeviceId".MissingElementMessage());
                    return false;
                }

                var reqID = xmlDoc.SelectSingleNode("PaymentRequest/Header/RequestId");
                if (!reqID.IsNull())
                    objLocalVars.RequestId = reqID.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/RequestId".MissingElementMessage());
                    return false;
                }

                var drvrID = xmlDoc.SelectSingleNode("PaymentRequest/Header/DriverId");
                if (!drvrID.IsNull())
                    objLocalVars.DriverNo = drvrID.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/DriverId".MissingElementMessage(), objLocalVars.DeviceId, objLocalVars.RequestId);
                    return false;
                }

                var token = xmlDoc.SelectSingleNode("PaymentRequest/Header/Token");
                if (!token.IsNull())
                    objLocalVars.Token = token.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/Header/Token".MissingElementMessage(), objLocalVars.DeviceId, objLocalVars.RequestId);
                    return false;
                }

                var capkStats = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/CapKeyUpdated");
                if (!capkStats.IsNull())
                    objLocalVars.CapKStatus = capkStats.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/CapKeyUpdated".MissingElementMessage(), objLocalVars.DeviceId, objLocalVars.RequestId);
                    return false;
                }

                var fleetId = xmlDoc.SelectSingleNode("PaymentRequest/TransactionDetail/FleetId");
                if (!fleetId.IsNull())
                    objLocalVars.FleetId = fleetId.InnerText;
                else
                {
                    capkresponse.Header = CommonFunctions.FailedResponse("PaymentRequest/TransactionDetail/FleetId".MissingElementMessage());
                    return false;
                }

                return true;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Load first request value from database
        /// </summary>
        /// <param name="capkDBResp"></param>
        /// <returns></returns>
        private void LoadCapkRequestFromDB()
        {
            try
            {
                var capkDto = _transactionService.GetCapKey();
                if (capkDto != null)
                {
                    objLocalVars.CapkFileCreationDt = capkDto.FileCreationDate;
                    objLocalVars.FileSize = capkDto.FileSize;
                    objLocalVars.FileCRC16 = capkDto.FileCRC16;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Purpose             :   To check CapKey daily and update EMVContact.xml if update available.
        /// Function Name       :   ManageCapk
        /// Created By          :   Sunil Singh
        /// Created On          :   28-Jan-2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <returns></returns>
        private void CnDownloadCapk()
        {
            _logMessage.AppendLine("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                CapkData capKeyData = new CapkData();
                StringBuilder capKeyFirstDataResponse = new StringBuilder();
                StringBuilder capKeyFirstDataRequest = new StringBuilder();
                StringBuilder capKeyDownloadFile = new StringBuilder();
                var capKey = string.Empty;
                var capKeyResponseStatus = string.Empty;

                _logMessage.AppendLine("CnDownloadCapk process start. Loading local variables.");
                _logMessage.AppendLine("Fetching canada device id from the config file .");
                objLocalVars.DeviceId = WebConfigurationManager.AppSettings["CnBookerAppDeviceId"];
                objLocalVars.DriverNo = string.Empty;
                dynamic firstDataDetails = string.Empty;

                firstDataDetails = _commonService.GetFirstDataDetails(objLocalVars.DeviceId, objLocalVars.DriverNo, (string)null, Convert.ToInt32(objLocalVars.FleetId), objLocalVars.RequestType);

                // Added for testing the canada capkey.Will be updated later--By Naveen Kumar
                CapkData capkDto = new CapkData();
                LoadCapkRequestFromDB();

                TransactionRequest transactionRequest = new TransactionRequest();
                transactionRequest = InitiateCapkTransaction(firstDataDetails);
                transactionRequest.IndustryType = SigmaStaticValue.CapkeyIndustry;
                transactionRequest.TransactionType = Convert.ToString(TxnTypeType.FileDownload);

                int capKeyCounter = 1;
                var firstDataBlockResponse = string.Empty;
                var bit63CapKeyData = string.Empty;

                for (int index = 0; index < capKeyCounter; index++)
                {
                    string cnClientRef = CnTransactionMessage.GetCanadaClientRef();
                    string referenceNumber = _transactionService.GetRefNumber();

                    transactionRequest.TransRefNo = FormatFields.FormatReferenceNo(transactionRequest.TerminalId, transactionRequest.Stan, Convert.ToString(transactionRequest.PaymentType), referenceNumber);

                    transactionRequest.Stan = BaseController.GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalId);

                    CnTransactionEntities cnTransRequest = CnTransactionMessage.PrepareFieldsForCapKeyDownload(transactionRequest);
                    CnTransactionMessage transactionMessage = new CnTransactionMessage();
                    MessageType messageType = MessageType.CapKeyDownload;
                    string dataElements = transactionMessage.GetMessage(messageType, cnTransRequest, index);
                    string payload = string.Concat(SigmaStaticValue.MessageType_TokenOnly, dataElements);
                    string payloadRequest = CnTransactionMessage.SetTransactionXml(transactionRequest, payload, cnClientRef);

                    transactionRequest.TerminalId = transactionRequest.TerminalId.FormatTerminalId();
                    MTDTransactionDto transactionDto = CommonFunctions.XmlToTransactionDto(transactionRequest);
                    _terminalService.UpdateStan(transactionDto.Stan, transactionDto.TransRefNo, transactionRequest.TerminalId, transactionRequest.MtdMerchantId);

                    Global.ActiveUrl = _transactionService.GetTransactionUrl();
                    firstDataBlockResponse = CnSendTransaction.SendMessageToDatawire(Global.ActiveUrl, payloadRequest);

                    CnTransactionEntities cnTrans = new CnTransactionEntities();
                    cnTrans.IsCapKey = true;
                    cnTrans.CompleteResponse = firstDataBlockResponse;
                    Iso8583Response response = PaymentController.HandleResponse(cnTrans, 0);

                    bit63CapKeyData = response.FDPrivateUsageData63.Substring(4);

                    TableFB objTableFB = new TableFB();
                    var tableFBId = bit63CapKeyData.Substring(4, 4);

                    if (tableFBId == tableFBId4642)
                    {
                        objTableFB.ReqFile_Block = CnGenericHandler.ConvertHexToAscii(Convert.ToString(bit63CapKeyData.Substring(34, 8)));
                        objTableFB.Req_FileBlockSize = CnGenericHandler.ConvertHexToAscii(Convert.ToString(bit63CapKeyData.Substring(42, 6)));
                        objTableFB.Req_FileOffset = CnGenericHandler.ConvertHexToAscii(Convert.ToString(bit63CapKeyData.Substring(48, 14)));
                        objTableFB.FileBlock_StatusCode = CnGenericHandler.ConvertHexToAscii(Convert.ToString(bit63CapKeyData.Substring(62, 2)));
                        capKey = CnGenericHandler.ConvertHexToAscii(Convert.ToString(bit63CapKeyData.Substring(64, Convert.ToInt32(objTableFB.Req_FileBlockSize) * 2)));
                    }

                    TableF1 objTableF1 = new TableF1();
                    var tableF1Data = bit63CapKeyData.Substring(64 + Convert.ToInt32(objTableFB.Req_FileBlockSize) * 2);

                    if (!string.IsNullOrEmpty(tableF1Data))
                    {
                        var tableF1Id = tableF1Data.Substring(4, 4);
                        if (tableF1Id == tableFBId4631)
                        {
                            objTableF1.Table_Version = CnGenericHandler.ConvertHexToAscii(Convert.ToString(tableF1Data.Substring(8, 6)));
                            objTableF1.Download_File = CnGenericHandler.ConvertHexToAscii(Convert.ToString(tableF1Data.Substring(14, 20)));
                            objTableF1.File_CreationDate = CnGenericHandler.ConvertHexToAscii(Convert.ToString(tableF1Data.Substring(34, 28)));

                            objTableF1.File_Size = CnGenericHandler.ConvertHexToAscii(Convert.ToString(tableF1Data.Substring(62, 10)));
                            objTableF1.File_CRC = Convert.ToString(tableF1Data.Substring(72, 4));
                            objTableF1.Function_Code = CnGenericHandler.ConvertHexToAscii(Convert.ToString(tableF1Data.Substring(76, 2)));

                            capKeyData.CurrFileCreationDt = objTableF1.File_CreationDate;
                            capKeyData.FileType = objTableF1.Download_File.Substring(0, 7);
                            capKeyData.FileSize = objTableF1.File_Size;
                            capKeyData.FileCRC16 = objTableF1.File_CRC;
                            capKeyData.FunCode = objTableF1.Function_Code;
                        }

                        if ((objTableF1.Function_Code == "N"))
                            //capKeyResponseStatus = "No update available";
                            capKeyResponseStatus = SigmaStaticValue.NoUpdateAvailable;

                        if ((objTableF1.Function_Code == "S"))
                            //capKeyResponseStatus = "File transfer successful";
                            capKeyResponseStatus = SigmaStaticValue.FileTransferSuccess;
                    }

                    capKeyDownloadFile.Append(capKey);
                    capKeyFirstDataRequest.Append(payloadRequest);
                    capKeyFirstDataResponse.Append(firstDataBlockResponse);

                    if (objTableFB.FileBlock_StatusCode == "C")
                        capKeyCounter++;

                    if (objTableFB.FileBlock_StatusCode == "L")
                        break;

                    if (objTableFB.FileBlock_StatusCode == "E")
                        //capKeyResponseStatus = "Error";
                        capKeyResponseStatus = SigmaStaticValue.Error;
                }

                _logMessage.AppendLine("File Download Response Status : " + capKeyResponseStatus);

                _logMessage.AppendLine("Reading raw capkeys for EMV Contact.");
                var rawCapKeys = Convert.ToString(capKeyDownloadFile).ReadRawCapKey();

                if (!rawCapKeys.IsNull())
                {
                    _logMessage.AppendLine("Updating CapKeys into XML file.");
                    capKeyData.XMLFileData = rawCapKeys.UpdateXMLFile();
                    _logMessage.AppendLine("CapKeys updated.");

                    CapKeyDto cpkDto = new CapKeyDto();
                    cpkDto.DownloadDate = DateTime.Now;
                    cpkDto.IsNew = true;
                    cpkDto.RawData = Convert.ToString(capKeyDownloadFile);
                    cpkDto.RequestXML = Convert.ToString(capKeyFirstDataRequest);
                    cpkDto.ResponseXML = Convert.ToString(capKeyFirstDataResponse);
                    cpkDto.XmlFile = capKeyData.XMLFileData.OuterXml;
                    cpkDto.FileCreationDate = capKeyData.CurrFileCreationDt;
                    cpkDto.FileCRC16 = capKeyData.FileCRC16;
                    cpkDto.FileSize = capKeyData.FileSize;
                    cpkDto.FileType = capKeyData.FileType;
                    cpkDto.FunCode = capKeyData.FunCode;
                    cpkDto.IsContactLess = false;

                    _logMessage.AppendLine("Saving CapKey into database");
                    int transId = _transactionService.SaveCapKey(cpkDto);
                    _logMessage.AppendLine("CapKey saved into database");
                }

                _logMessage.AppendLine("Reading raw capkeys for EMV ContactLess.");
                var rawCapKeysCLS = Convert.ToString(capKeyDownloadFile).ClessReadRawCapKey();

                if (!rawCapKeys.IsNull())
                {
                    _logMessage.AppendLine("Updating CLS CapKeys into XML file.");
                    capKeyData.XMLFileData = rawCapKeysCLS.UpdateXMLFileCless();
                    _logMessage.AppendLine("CLS CapKeys updated.");

                    // Save data
                    _logMessage.AppendLine("Initializing CapKeyDto to save CapKey into databse.");
                    CapKeyDto cpkDto = new CapKeyDto();
                    cpkDto.DownloadDate = DateTime.Now;
                    cpkDto.IsNew = true;
                    cpkDto.RawData = Convert.ToString(capKeyDownloadFile);
                    cpkDto.RequestXML = Convert.ToString(capKeyFirstDataRequest);
                    cpkDto.ResponseXML = Convert.ToString(capKeyFirstDataResponse);
                    cpkDto.XmlFile = capKeyData.XMLFileData.OuterXml;
                    cpkDto.FileCreationDate = capKeyData.CurrFileCreationDt;
                    cpkDto.FileCRC16 = capKeyData.FileCRC16;
                    cpkDto.FileSize = capKeyData.FileSize;
                    cpkDto.FileType = capKeyData.FileType;
                    cpkDto.FunCode = capKeyData.FunCode;
                    cpkDto.IsContactLess = true;

                    _logMessage.AppendLine("Saving CapKey into database");
                    int transId = _transactionService.SaveCapKey(cpkDto);
                    _logMessage.AppendLine("CapKey saved into database");
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }
            finally
            {
                _logMessage.AppendLine("CnDownloadCapk process complete.");
            }
        }

        #endregion
    }
}
