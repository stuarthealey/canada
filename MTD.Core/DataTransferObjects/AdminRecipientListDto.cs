﻿using System;
using System.Collections.Generic;

namespace MTD.Core.DataTransferObjects
{

    public class AdminRecipientListDto
    {
        public int fk_UserId { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public Nullable<bool> IsRecipient { get; set; }
        public IEnumerable<AdminRecipienMastertListDto> AdminList { get; set; }
    }
}
