﻿using System.Collections.Generic;
using MTD.Data.Data;

namespace MTD.Data.Repository.Interface
{
    public interface IAdminUserRepository
    {
        bool Add(User adminUser);

        IEnumerable<User> AdminList(string name);

        IEnumerable<User> UserProfiles();

        bool Update(User adminUser, int adminUserId);

        bool UpdateProfile(User adminUser, int adminUserId);

        User GetAdminUser(int adminUserId);

        void DeleteAdminUser(int adminUserId);

        IEnumerable<Role> GetAdminRole();

        IEnumerable<User> GetAdminUserListByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting);
    }
}
