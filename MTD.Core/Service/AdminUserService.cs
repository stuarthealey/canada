﻿using System.Collections.Generic;
using MTD.Core.DataTransferObjects;
using MTD.Core.Extension;
using MTD.Core.Factory.Interface;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;
using Ninject;

namespace MTD.Core.Service
{
    class AdminUserService : IAdminUserService
    {
        private readonly IAdminUserRepository _adminUserRepository;
        private readonly IAdminUserFactory _adminUserFactory;

        public AdminUserService([Named("AdminUserRepository")] IAdminUserRepository adminUserRepository, [Named("AdminUserFactory")] IAdminUserFactory adminUserFactory)
        {
            _adminUserRepository = adminUserRepository;
            _adminUserFactory = adminUserFactory;
        }

        /// <summary>
        /// Purpose             :   To Add Admin User
        /// Function Name       :   Add
        /// Created By          :   Umesh Kumar
        /// Created On          :   12/18/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="adminUserDto"></param>
        /// <returns></returns>
        public bool Add(UserDto adminUserDto)
        {
            try
            {
                User adminUser = _adminUserFactory.CreateAdminUser(adminUserDto);
                bool isExist = _adminUserRepository.Add(adminUser);
                return isExist;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get the Admin User List
        /// Function Name       :   GetAdminUserList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/18/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UserDto> GetAdminUserList(string name)
        {
            try
            {
                var userList = _adminUserRepository.AdminList(name);
                var adminDtoList = userList.ToDTOUserList();
                return adminDtoList;
            }
            catch
            {
                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To get the User Profiles
        /// Function Name       :   UserProfiles
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/19/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UserDto> UserProfiles()
        {
            try
            {
                var userList = _adminUserRepository.UserProfiles();
                var adminDtoList = userList.ToDTOUserList();
                return adminDtoList;
            }
            catch
            {
                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To get the List of Admin User Profile
        /// Function Name       :   GetAdminUserProfileList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/19/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UserDto> GetAdminUserProfileList()
        {
            try
            {
                var userList = _adminUserRepository.UserProfiles();
                var adminDtoList = userList.ToDTOUserList();
                return adminDtoList;
            }
            catch
            {
                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To do the Update
        /// Function Name       :   Update
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/22/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="adminUserDto"></param>
        /// <param name="adminUserId"></param>
        /// <returns></returns>
        public bool Update(UserDto adminUserDto, int adminUserId)
        {
            try
            {
                User adminUser = _adminUserFactory.CreateAdminUser(adminUserDto);
                return _adminUserRepository.Update(adminUser, adminUserId);
            }
            catch
            {
                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To Update Profile
        /// Function Name       :   UpdateProfile
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="adminUserDto"></param>
        /// <param name="adminUserId"></param>
        /// <returns></returns>
        public bool UpdateProfile(UserDto adminUserDto, int adminUserId)
        {
            try
            {
                User adminUser = _adminUserFactory.CreateAdminUser(adminUserDto);
                return _adminUserRepository.UpdateProfile(adminUser, adminUserId);
            }
            catch
            {
                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To get the Admin User
        /// Function Name       :   GetAdminUserDto
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="adminUserId"></param>
        /// <returns></returns>
        public UserDto GetAdminUserDto(int adminUserId)
        {
            try
            {
                var adminUser = _adminUserRepository.GetAdminUser(adminUserId);
                var userDto = adminUser.ToDtoUser();
                return userDto;
            }
            catch
            {
                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To delete the Admin User
        /// Function Name       :   DeleteAdminUser
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="adminUserId"></param>
        public void DeleteAdminUser(int adminUserId)
        {
            try
            {
                _adminUserRepository.DeleteAdminUser(adminUserId);
            }
            catch
            {
                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To get the Admin Roles
        /// Function Name       :   GetAdminRoles
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<RoleDto> GetAdminRoles() // for accessing roles of user
        {

            try
            {
                IEnumerable<Role> roles = _adminUserRepository.GetAdminRole();
                IEnumerable<RoleDto> roleDto = roles.ToDTOList();
                return roleDto;
            }
            catch
            {
                throw;
            }

        }

        /// <summary>
        /// Purpose             :   To get the Admin User List
        /// Function Name       :   GetAdminUserListByFilter
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public IEnumerable<UserDto> GetAdminUserListByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting)
        {
            try
            {
                var merchantList = _adminUserRepository.GetAdminUserListByFilter(name, jtStartIndex, jtPageSize, jtSorting);
                var merchantDtoList = merchantList.ToDTOList();
                return merchantDtoList;
            }
            catch
            {
                throw;
            }

        }

    }
}
