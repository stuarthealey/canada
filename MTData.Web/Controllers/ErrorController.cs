﻿using System.Web.Mvc;

namespace MTData.Web.Controllers
{
    public class ErrorController : Controller
    {
        /// <summary>
        /// Purpose             :   To get the error and display that error
        /// Function Name       :   NotResponding
        /// Created By          :   Umesh Kumar
        /// Created On          :   12/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public ActionResult NotResponding()
        {
            Response.StatusCode = 404;
            Response.TrySkipIisCustomErrors = true;
            return View();
        }

        /// <summary>
        /// Purpose             :   To get the error and display that error
        /// Function Name       :   LoginError
        /// Created By          :   Umesh Kumar
        /// Created On          :   13/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public ActionResult LoginError()
        {
            Response.StatusCode = 090;
            Response.TrySkipIisCustomErrors = true;
            return View();
        }

        public ActionResult CustomError()
        {
           
            return View();
        }
        
	}
}