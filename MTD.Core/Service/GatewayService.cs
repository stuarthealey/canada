﻿using System;
using System.Collections.Generic;
using MTD.Core.DataTransferObjects;
using MTD.Core.Extension;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;
using Ninject;

namespace MTD.Core.Service
{
    public class GatewayService : IGatewayService
    {
        private readonly IGatewayReporsitory _gatewayReporsitory;

        public GatewayService([Named("GatewayRepository")] IGatewayReporsitory gatewayReporsitory)
        {
            _gatewayReporsitory = gatewayReporsitory;
        }

        /// <summary>
        /// Purpose             :   To Create the new gateway
        /// Function Name       :   Create
        /// Created By          :   Umesh kumar
        /// Created On          :   12/22/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gatewayDto"></param>
        /// <returns></returns>
        public bool Create(GatewayDto gatewayDto)
        {
            try
            {
                Gateway gateway = DtoExtension.ToGateway(gatewayDto);
                int result = _gatewayReporsitory.Create(gateway);
                return Convert.ToBoolean(result);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Gateways List List
        /// Function Name       :   GetGatewaysList
        /// Created By          :   Umesh kumar
        /// Created On          :   12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<GatewayDto> GetGatewaysList()
        {
            try
            {
                var gatewaysList = _gatewayReporsitory.GatewaysList();
                var gatewaysDtoList = gatewaysList.ToDtoGatewaysList();
                return gatewaysDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Delete the Gateway
        /// Function Name       :   DeleteGateway
        /// Created By          :   Umesh kumar
        /// Created On          :   12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gatewayId"></param>
        public void DeleteGateway(int gatewayId)
        {
            try
            {
                _gatewayReporsitory.DeleteGateway(gatewayId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Gateway
        /// Function Name       :   GetGateway
        /// Created By          :   Umesh kumar
        /// Created On          :   12/24/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gatewayId"></param>
        /// <returns></returns>
        public GatewayDto GetGateway(int gatewayId)
        {
            try
            {
                Gateway obj = _gatewayReporsitory.GetGateway(gatewayId);
                GatewayDto gatewayDto = obj.ToGatewayDto();
                return gatewayDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update the existing Gateway
        /// Function Name       :   Update
        /// Created By          :   Umesh kumar
        /// Created On          :   12/25/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gatewayDto"></param>
        /// <returns></returns>
        public bool Update(GatewayDto gatewayDto)
        {
            try
            {
                Gateway gateway = DtoExtension.ToGateway(gatewayDto);
                bool result = _gatewayReporsitory.Update(gateway);
                return result;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CurrencyDto> GetGatewayCurrency()
        {
            try
            {
                var currenciesList = _gatewayReporsitory.GetGatewayCurrencyList();
                var currenciesDtoList = currenciesList.ToDtoCurrencyList();
                return currenciesDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Gateway List
        /// Function Name       :   GetGatewayByFilter
        /// Created By          :   Umesh kumar
        /// Created On          :   12/28/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public IEnumerable<GatewayDto> GetGatewayByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting)
        {
            try
            {
                var gatewaysList = _gatewayReporsitory.GetGatewayByFilter(name, jtStartIndex, jtPageSize, jtSorting);
                var gatewaysDtoList = gatewaysList.ToDtoGatewaysList();
                return gatewaysDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int GatewaysListCount(string name)
        {
            try
            {
                return _gatewayReporsitory.GatewaysListCount(name);
            }
            catch
            {
                throw;
            }
        }
    
    }
}
