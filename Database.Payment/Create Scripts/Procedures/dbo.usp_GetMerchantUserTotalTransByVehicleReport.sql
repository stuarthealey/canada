IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetMerchantUserTotalTransByVehicleReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetMerchantUserTotalTransByVehicleReport]
GO

CREATE PROCEDURE dbo.usp_GetMerchantUserTotalTransByVehicleReport
( 
	@DateFrom DATETIME,
	@DateTo DATETIME,
	@MerchantId INT = NULL,
	@MerchantUserId INT = NULL
)
AS
BEGIN
	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SET ANSI_WARNINGS OFF
		SET NOCOUNT ON

		DECLARE  @TransCountSlablt1 INT,
				 @TransCountSlabGt2 INT,
				 @TransCountSlablt2 INT,
				 @TransCountSlabGt3 INT,
				 @TransCountSlablt3 INT,
				 @TransCountSlabGt4 INT

		 DECLARE @InnerTable TABLE (
				Id INT, 
				FareValue MONEY,
				Tip MONEY, 
				Amount MONEY, 
				Surcharge MONEY, 
				Fee MONEY,
				VehicleNo NVARCHAR(70), 
				TxnType NVARCHAR(25),
				ResponseCode NVARCHAR(30),
				fk_FleetId INT,
				IsRefunded BIT,
				IsVoided BIT,
				IsCompleted BIT,
				TechFee MONEY,
				FleetFee MONEY,
				MerchantId INT
			)

		IF (@MerchantId IS NULL)
		BEGIN
			SET @MerchantId = (SELECT fk_merchantid FROM dbo.Users WHERE UserID = @MerchantUserId)
		END

		--Insert data into table variable InnnerTable for reuse
		INSERT INTO @InnerTable(Id, FareValue, Tip, Amount, Surcharge, Fee, VehicleNo, TxnType, ResponseCode, fk_FleetId, IsRefunded, IsVoided, IsCompleted, TechFee, FleetFee, MerchantId)                   
		SELECT mt.Id, mt.FareValue, mt.Tip, mt.Amount, mt.Surcharge, mt.Fee, mt.VehicleNo, mt.TxnType, mt.ResponseCode, mt.fk_FleetId, mt.IsRefunded, mt.IsVoided, mt.IsCompleted, mt.TechFee, mt.FleetFee, mt.MerchantId
		FROM (SELECT Id, FareValue, Tip, Amount, Surcharge, Fee, VehicleNo, TxnType, ResponseCode, fk_FleetId, TxnDate, IsRefunded, IsVoided, IsCompleted, TechFee, FleetFee, MerchantId
			 FROM dbo.MTDTransaction)  mt
			INNER JOIN (SELECT VehicleNumber, fk_FleetID FROM dbo.Vehicle WHERE IsActive = 1) vh ON mt.VehicleNo = vh.VehicleNumber 
			INNER JOIN (SELECT FleetId, FleetName FROM dbo.Fleet WHERE IsActive = 1) fl ON vh.fk_FleetID = fl.FleetID
		WHERE CAST(mt.TxnDate AS DATE) >= @DateFrom
		  AND CAST(mt.TxnDate AS DATE) < @DateTo
		  AND mt.fk_FleetId = vh.fk_FleetID

		SELECT  @TransCountSlablt1 = TransCountSlablt1,
				@TransCountSlabGt2 = TransCountSlabGt2,
				@TransCountSlablt2 = TransCountSlablt2,
				@TransCountSlabGt3 = TransCountSlabGt3,
				@TransCountSlablt3 = TransCountSlablt3,
				@TransCountSlabGt4 = TransCountSlabGt4
		FROM dbo.TransactionCountSlab
		WHERE fk_MerchantId = @MerchantId

		SELECT COUNT(Id) AS 'Id', VehicleNo AS 'VehicleNo', fk_FleetId AS 'fk_FleetId' 
		INTO #temp
		FROM @InnerTable it 
		WHERE it.ResponseCode IN ('000', '00', '002') 
		  AND it.MerchantId = @MerchantId
		GROUP BY it.VehicleNo, it.fk_FleetId	

		--Prepare data for summary report in xml format                            
		SELECT   ROW_NUMBER() OVER (ORDER BY FleetName) AS 'SrNo',
									ISNULL(vh.FleetName, '')  AS 'FleetName',
									(SELECT ISNULL(COUNT(Id), 0) FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded = 0 AND it.IsVoided = 0 AND it.ResponseCode IN ('000', '00', '002')) AS 'NoOfTrans',
									CONCAT('$', (SELECT ISNULL(SUM(FareValue),'0.00') FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'FareAmt',
									CONCAT('$', (SELECT ISNULL(SUM(Tip),'0.00') FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted=1)) AND  it.ResponseCode IN ('000', '00', '002'))) AS 'Tips',
									CONCAT('$', (SELECT ISNULL(SUM(surcharge),'0.00')FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'Surcharges',
									CONCAT('$', (SELECT ISNULL(SUM(TechFee),'0.00')FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'TechFee',
									(SELECT ISNULL(COUNT(id), 0) FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND it.TxnType = 'Refund' AND it.ResponseCode IN ('000', '00')) AS 'NoOfChargebacks',
									CONCAT('$', (SELECT ISNULL(SUM(Amount),'0.00') FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND it.TxnType = 'Refund' AND it.ResponseCode IN ('000', '00'))) AS 'ChargebackValue',
									(SELECT ISNULL(COUNT(id), 0) FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND it.TxnType = 'Void' AND it.ResponseCode IN ('000', '00')) AS 'NoOfVoid',
									CONCAT('$', (SELECT ISNULL(SUM(Amount),'0.00') FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND it.TxnType = 'Void' AND it.ResponseCode IN ('000', '00'))) AS 'VoidValue',
									CONCAT('$', (SELECT ISNULL(SUM(Amount),'0.00') FROM  @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded = 0 AND IT.IsVoided = 0 AND it.ResponseCode IN ('000', '00', '002'))) AS 'TotalAmount',
									CONCAT('$', (SELECT ISNULL(SUM(Fee),'0.00') + ISNULL(SUM(FleetFee),'0.00') FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'Fees',

									CONCAT('$', (SELECT ISNULL(SUM(Amount),'0.00') FROM  @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded = 0 AND IT.IsVoided = 0 AND it.ResponseCode IN ('000', '00', '002'))-
										(SELECT ISNULL(SUM(Fee),'0.00')+ISNULL(SUM(FleetFee),'0.00') FROM @InnerTable it WHERE it.fk_FleetId = vh.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'AmountOwing',

									(SELECT COALESCE(COUNT(t.VehicleNo), 0) FROM #temp t WHERE t.fk_FleetId = vh.FleetID AND t.Id < @TransCountSlablt1) AS 'TransCountForSlab1',
									(SELECT COALESCE(COUNT(t.VehicleNo), 0) FROM #temp t WHERE t.fk_FleetId = vh.FleetID AND t.Id >= @TransCountSlabGt2 AND t.Id < @TransCountSlablt2) AS 'TransCountForSlab2',
									(SELECT COALESCE(COUNT(t.VehicleNo), 0) FROM #temp t WHERE t.fk_FleetId = vh.FleetID AND t.Id >= @TransCountSlabGt3 AND t.Id < @TransCountSlablt3) AS 'TransCountForSlab3',
									(SELECT COALESCE(COUNT(t.VehicleNo), 0) FROM #temp t WHERE t.fk_FleetId = vh.FleetID AND t.Id >= @TransCountSlabGt4) AS 'TransCountForSlab4'
		FROM dbo.MTDTransaction m 
			INNER JOIN (SELECT v.VehicleNumber AS 'VehicleNumber', v.fk_FleetID AS 'fk_FleetID', f.FleetID AS 'FleetID', f.FleetName AS 'FleetName', mer.Company AS 'Company' 									
						FROM (SELECT VehicleNumber, fk_FleetID FROM dbo.Vehicle WHERE IsActive = 1) v  
							INNER JOIN (SELECT FleetID, FleetName, fk_MerchantID FROM dbo.Fleet WHERE IsActive = 1) f ON v.fk_FleetID = f.FleetID
							INNER JOIN (SELECT MerchantID, Company FROM dbo.Merchant WHERE IsActive = 1) mer ON f.fk_MerchantID = mer.MerchantID 
						WHERE mer.MerchantID = @MerchantId) vh ON m.VehicleNo = vh.VehicleNumber 
		WHERE m.ResponseCode IN ('000', '00', '002') 
		  AND CAST(m.TxnDate AS DATE) >= @DateFrom 
		  AND CAST(m.TxnDate AS DATE) < @DateTo 
		  AND m.fk_FleetId = vh.fk_FleetID
		  AND m.Id NOT IN (SELECT Id FROM dbo.MTDTransaction WHERE m.TxnType = 'Authorization' AND m.IsCompleted = 0)
		  AND m.fk_FleetId IN (SELECT DISTINCT fk_FleetId FROM dbo.UserFleet WHERE fk_UserTypeId = 4 AND IsActive = 1 AND fk_UserID = @MerchantUserId)
		GROUP BY vh.FleetName, vh.FleetID, vh.Company	                    					   					        											    
		ORDER BY FleetName
	END TRY
	BEGIN CATCH
		INSERT INTO dbo.ProcErrorHandler(ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, UserName, HostName, [TimeStamp])
		SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
	END CATCH
END
GO