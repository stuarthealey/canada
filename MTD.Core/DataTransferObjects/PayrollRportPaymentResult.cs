﻿using System;

namespace MTD.Core.DataTransferObjects
{
    public class PayrollRportPaymentResult
    {
        public System.DateTime Date { get; set; }
        public string Name { get; set; }
        public string BankName { get; set; }
        public string BSB { get; set; }
        public string AccountNumber { get; set; }
        public Nullable<decimal> AmountPayable { get; set; }       
    }
}
