﻿namespace MTData.WebAPI.Models
{
    public class CreditResponse
    {
        public CommonGrp CommonGrp { get; set; }
        public CardGrp CardGrp { get; set; }
        public TAGrp TAGrp { get; set; }
        public OrigAuthGrp OrigAuthGrp { get; set; }
        //public AddtlAmtGrp AddtlAmtGrp { get; set; }
        public MCGrp MCGrp { get; set; }
        public AmexGrp AmexGrp { get; set; }
        public DSGrp DSGrp { get; set; }
        public VisaGrp VisaGrp { get; set; }
        public RespGrp RespGrp { get; set; }
        public EMVGrp EMVGrp { get; set; }
    }
}