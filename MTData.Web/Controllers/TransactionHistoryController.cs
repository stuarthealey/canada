﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using System.Configuration;
using System.Text.RegularExpressions;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Web.ViewData;
using MTData.Web.Resource;
using MTData.Utility;

namespace MTData.Web.Controllers
{
    public class TransactionHistoryController : Controller
    {
        readonly ILogger _logger = new Logger();
        private readonly ITransactionService _transactionService;
        private readonly IUserService _userService;
        private StringBuilder _logMessage;
        private readonly ICommonService _commonService;
        private const string Decline = "DECLINED-005";
        private const string Approved = "APPROVAL";
        private readonly IDriverService _driverService;
        private StringBuilder _logTracking;
        private string Timezone = "UTC-11";

        public TransactionHistoryController([Named("TransactionService")] ITransactionService transactionService, StringBuilder logMessage, [Named("UserService")] IUserService userService, [Named("CommonService")] ICommonService commonService, [Named("DriverService")] IDriverService driverService, StringBuilder logTracking)
        {
            _driverService = driverService;
            _transactionService = transactionService;
            _logMessage = logMessage;
            _userService = userService;
            _commonService = commonService;
            _logTracking = logTracking;
        }

        /// <summary>
        /// Purpose             :  to display the transaction 
        /// Function Name       :   Index
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/6/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index(int tId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                if (tId <= 0)
                {
                    TempData["Dne"] = MtdataResource.Enter_valid_transaction_id; //"Please Enter the Valid Tranasaction ID";
                    return RedirectToAction("Create", "TransactionHistory");
                }

                int? mid = (((SessionDto)Session["User"]).SessionUser.fk_MerchantID);

                _logMessage.Append("Calling GetTransaction method of _transactionService class takes 2 parameters are = transaction Id " + tId + " and Merchant Id= " + mid);

                MTDTransactionDto transactionDto;
                transactionDto = mid == null ? _transactionService.GetTransaction(tId) : _transactionService.GetTransaction(tId, mid);

                _logMessage.Append("GetTransaction method of _transactionService class executed successfully");

                if (transactionDto != null)
                {
                    //DEBUG
                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("TransactionHistoryController.Index(). tId:{0};transactionDto.TransNote:{1};", tId, transactionDto.TransNote));

                    //D#6082 Need to change the TxnDate to a local date\time for the Merchant\fleet for display.
                    if (transactionDto.MerchantId != null)
                        Timezone = _commonService.MerchantTimeZone((int)transactionDto.MerchantId);
                    else
                        Timezone = TimeZoneInfo.Local.StandardName;

                    // TxnDate is stored in the DB as UTC time, not local time, so need to convert from UTC.
                    transactionDto.TxnDate = TimeZoneInfo.ConvertTime(Convert.ToDateTime(transactionDto.TxnDate), TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(Timezone));

                    var trnx = transactionDto.ToViewData();
                    trnx.Currency = ConfigurationManager.AppSettings["Currency"].ToString();

                    //DEBUG
                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    {
                        _logger.LogInfoMessage(string.Format("TransactionHistoryController.Index(). tId:{0};transactionDto.TransNote:{1};trnx.TransNote:{2};", tId, transactionDto.TransNote, trnx.TransNote));
                        _logger.LogInfoMessage(string.Format("TransactionHistoryController.Index(). MerchantId:{0};Timezone:{1};TxnDate:{2};DtoTxnDate:{3};", transactionDto.MerchantId, Timezone, transactionDto.TxnDate, trnx.TxnDate));
                    }

                    return View(trnx);
                }

                TempData["Dne"] = MtdataResource.Transaction_id_does_not_exist;  //"This transaction id does not exist";

                if (mid == null)
                    return RedirectToAction("Create", "TransactionHistory");

                return RedirectToAction("Search", "TransactionHistory");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            finally
            {
                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    _logger.LogInfoMessage(_logger.ToString());
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   Fetching the Transaction History from the database
        /// Function Name       :   Create
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/15/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || Session["User"] == null) return RedirectToAction("Index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            TxnSearch tvdData = new TxnSearch();
            tvdData.IsVarified = null;

            try
            {
                List<CarListViewData> carList = new List<CarListViewData>();
                List<DriverListViewData> driverList = new List<DriverListViewData>();

                var fkMerchantId = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;
                if (fkMerchantId != null)
                {
                    int mid = (int)fkMerchantId;
                    var dto = (SessionDto)Session["User"];
                    int userType = dto.SessionUser.fk_UserTypeID;
                    int userId = dto.SessionUser.UserID;
                    int merchantId = (int)dto.SessionUser.fk_MerchantID;
                    int logOnByUserId = dto.SessionUser.LogOnByUserId;
                    int loggedbyUserType = dto.SessionUser.LogOnByUserType;

                    if (loggedbyUserType == 5) // For 5
                    {
                        tvdData.fleetlist = _userService.FleetList(merchantId).ToViewDataList();
                        IEnumerable<UserFleetDto> accessedFleet = _userService.GetUsersFleet(logOnByUserId);
                        tvdData.fleetlist = tvdData.fleetlist.Where(x => accessedFleet.Select(y => y.fk_FleetID).Contains((int)x.FleetID)).ToList();
                    }
                    else
                    {
                        if (userType == 4)
                            tvdData.fleetlist = _userService.UserFleetLists(userId).ToViewDataList();
                        else if (userType == 2 || userType == 3)
                            tvdData.fleetlist = _userService.FleetList(mid).ToViewDataList();
                    }
                }
                else
                {
                    tvdData.fleetlist = _driverService.AllFleets().ToViewDataList();
                }

                tvdData.DriverList = driverList;
                tvdData.CarList = carList;
                tvdData.fk_FleetId = 0;
                tvdData.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
                tvdData.Currency = ConfigurationManager.AppSettings["Currency"].ToString();

                if ((TempData != null) && (TempData["back"] != null) && (Convert.ToInt32(TempData["back"]) == 1))
                {
                    tvdData = Session["BackData"] as TxnSearch;
                    ViewBag.IsBack = true;
                    tvdData.DriverList = driverList;
                    tvdData.CarList = carList;
                    tvdData.fleetlist = _driverService.AllFleets().ToViewDataList();
                    tvdData.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
                    tvdData.Currency = ConfigurationManager.AppSettings["Currency"].ToString();
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            finally
            {
                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    _logger.LogInfoMessage(_logger.ToString());
            }

            return View(tvdData);
        }

        /// <summary>
        /// Purpose             :   Create transaction history on the basis of transaction ID
        /// Function Name       :   Create
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/15/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(TxnSearch tvd)
        {
            try
            {
                if (Session["User"] == null) return RedirectToAction("Index", "Member");
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

                var sessionDto = ((SessionDto)Session["User"]);
                int userType = sessionDto.SessionUser.fk_UserTypeID;
                int merchantIds = 0;
                
                if (userType != 1)
                {
                    merchantIds = (int)sessionDto.SessionUser.fk_MerchantID;
                    Timezone = _commonService.MerchantTimeZone(merchantIds);
                }
                else
                {
                    // Timezone = TimeZoneInfo.Local.StandardName;
                    //D#6082 Determine the correct Timezone for the Merchant, if one is specified, or the selected Fleet.
                    if (merchantIds != 0)
                    {
                        Timezone = _commonService.MerchantTimeZone(merchantIds);
                    }
                    else
                    {
                        // Get the Merchant for the selected Fleet.
                        if (tvd.fk_FleetId != null)
                        {
                            merchantIds = _transactionService.GetMerchantId((int)tvd.fk_FleetId);
                            Timezone = _commonService.MerchantTimeZone(merchantIds);
                        }
                        else
                        {
                            Timezone = TimeZoneInfo.Local.StandardName;
                        }
                    }
                }

                int loggedbyUserType = sessionDto.SessionUser.LogOnByUserType;
                if (loggedbyUserType == 5) // For 5
                {
                    Timezone = _commonService.MerchantTimeZone(merchantIds);
                    int logOnByUserId = sessionDto.SessionUser.LogOnByUserId;
                    tvd.fleetlist = _userService.FleetList(merchantIds).ToViewDataList();
                    IEnumerable<UserFleetDto> accessedFleet = _userService.GetUsersFleet(logOnByUserId);
                    tvd.fleetlist = tvd.fleetlist.Where(x => accessedFleet.Select(y => y.fk_FleetID).Contains((int)x.FleetID)).ToList();
                }
                else
                {
                    tvd.fleetlist = _driverService.AllFleets().ToViewDataList();
                }

                int fId = 0;
                if (tvd.fk_FleetId != null)
                    fId = (int)tvd.fk_FleetId;

                tvd.CarList = _commonService.GetCars((int)fId).ToViewDataList();
                tvd.DriverList = _commonService.GetDrivers((int)fId).ToViewDataList();

                //DEBUG
                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    _logger.LogInfoMessage(string.Format("THC.Create(). UserType:{0};Timezone:{1};MerchantID:{2};LoggedByUserType:{3};FleetID:{4};", userType, Timezone, merchantIds, loggedbyUserType, tvd.fk_FleetId));

                if (tvd.startDateString != null && tvd.endDateString != null)
                {
                    //DEBUG
                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("THC.Create(). StartDateString:{0};EndDateString:{1};", tvd.startDateString, tvd.endDateString));

                    if (tvd.startDateString.Length > 0 && tvd.endDateString.Length > 0)
                    {
                        bool isDate = false;
                        Regex r = new Regex(@"\d\d-\d\d-\d\d\d\d\s\d\d:\d\d");

                        if (((r.Match(tvd.startDateString).Success) && (r.Match(tvd.endDateString).Success)))
                        {
                            DateTime resultDate;
                            isDate = DateTime.TryParseExact(tvd.startDateString, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return View(tvd);
                            else
                                tvd.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones

                            isDate = DateTime.TryParseExact(tvd.endDateString, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return View(tvd);
                            else
                                tvd.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
                        }
                    }
                }

                //DEBUG
                _logger.LogInfoMessage(string.Format("THC.Create(). StartDate:{0};EndDate:{1};", tvd.startDate, tvd.endDate));

                if (tvd.fk_FleetId == null 
                    && string.IsNullOrEmpty(tvd.VNumber) 
                    && string.IsNullOrEmpty(tvd.DNumber) 
                    && string.IsNullOrEmpty(tvd.TType) 
                    && string.IsNullOrEmpty(tvd.CrdType) 
                    && tvd.startDate == null 
                    && tvd.endDate == null 
                    && tvd.Id == null 
                    && string.IsNullOrEmpty(tvd.ExpiryDate) 
                    && string.IsNullOrEmpty(tvd.FFD) 
                    && string.IsNullOrEmpty(tvd.LFD) 
                    && tvd.MinAmt == null 
                    && tvd.MaxAmt == null 
                    && tvd.JobNumber == null 
                    && tvd.AId == null 
                    && tvd.AddRespData == null 
                    && tvd.Industry == null 
                    && string.IsNullOrEmpty(tvd.SerialNo) 
                    && string.IsNullOrEmpty(tvd.TerminalId))
                {
                    ViewBag.Message = MtdataResource.Please_Select_field; //"Please enter or choose some field's";
                    return View(tvd);
                }

                int? merchantId = null;
                var fkMerchantId = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;
                if (fkMerchantId != null)
                    merchantId = fkMerchantId;

                if (!String.IsNullOrEmpty(tvd.ExpiryDate))
                    tvd.ExpiryDate = StringExtension.DateFormat(tvd.ExpiryDate);

                MtdTransactionsViewData td = new MtdTransactionsViewData
                {
                    PageSize = 0,
                    ExpiryDate = tvd.ExpiryDate,
                    VehicleNo = tvd.VNumber,
                    DriverNo = tvd.DNumber,
                    CardType = tvd.CrdType,
                    startDate = tvd.startDate,
                    endDate = tvd.endDate,
                    Id = tvd.Id,
                    TxnType = tvd.TType,
                    FirstFourDigits = tvd.FFD,
                    LastFourDigits = tvd.LFD,
                    MinAmount = tvd.MinAmt,
                    MaxAmount = tvd.MaxAmt,
                    AuthId = tvd.AId,
                    AddRespData = tvd.AddRespData,
                    MerchantId = merchantId,
                    fk_FleetId = tvd.fk_FleetId,
                    EntryMode = tvd.Industry,
                    IsVarified = tvd.IsVarified,
                    SerialNo = tvd.SerialNo,
                    TerminalId = tvd.TerminalId,
                    TransNote = tvd.TransNote   // Ensure TransNote included
                };

                MTDTransactionDto transactionDto = td.ToDTO();
                int count = _transactionService.GetTransactionCount(transactionDto);
                transactionDto.PageSize = count;
                IEnumerable<MTDTransactionDto> stvd = _transactionService.GetTransaction(transactionDto);
                IEnumerable<SearchTransactionViewData> mvData = stvd.ToViewData();
                var transactionViewDatas = mvData as SearchTransactionViewData[] ?? mvData.ToArray();

                if (transactionViewDatas.ToList().Count == 0)
                {
                    ViewBag.Message = MtdataResource.No_such_record_exist;  //"No such records exist";
                    return View(tvd);
                }

                string culture = ConfigurationManager.AppSettings["CultureInfo"].ToString();
                string currency = ConfigurationManager.AppSettings["Currency"].ToString();

                using (StringWriter output = new StringWriter())
                {
                    output.WriteLine("\"Id\",\"AuthCode\",\"Vehicle\",\"Driver\",\"Date\",\"Credit/Debit\",\"Transaction Type\",\"Tech Fee(" + currency + ")\",\"Fleet Fee(" + currency + ")\",\"Fee(" + currency + ")\",\"Surcharge(" + currency + ")\",\"Amount(" + currency + ")\",\"CardType\",\"Entry Mode\",\"Approval\"");

                    foreach (SearchTransactionViewData item in transactionViewDatas)
                    {
                        DateTime dt1 = TimeZoneInfo.ConvertTime(Convert.ToDateTime(item.TxnDate), TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(Timezone));
                        decimal tFee = Convert.ToDecimal(item.TechFee);
                        decimal fFee = Convert.ToDecimal(item.FleetFee);
                        decimal fee = Convert.ToDecimal(item.Fee);
                        decimal sur = Convert.ToDecimal(item.Surcharge);
                        decimal amt = Convert.ToDecimal(item.Amount);

                        output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\"",
                            item.Id, item.AuthId, item.VehicleNo, item.DriverNo, dt1.ToString("G", CultureInfo.GetCultureInfo(culture)), item.PaymentType, item.TxnType,
                            tFee.ToString("C", CultureInfo.GetCultureInfo(culture)), fFee.ToString("C", CultureInfo.GetCultureInfo(culture)), 
                            fee.ToString("C", CultureInfo.GetCultureInfo(culture)), sur.ToString("C", CultureInfo.GetCultureInfo(culture)), 
                            amt.ToString("C", CultureInfo.GetCultureInfo(culture)), item.CardType, item.EntryMode, item.AddRespData);
                    }

                    _logTracking.Append(string.Format("Transaction Report Excel file Downloaded by {0} at {1}. TransactionHistoryController.Create() ", (((SessionDto)Session["User"]).SessionUser.Email), System.DateTime.Now.ToUniversalTime()));
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Transaction_Report.csv");
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View(tvd);
        }

        /// <summary>
        /// Purpose             :   Fetching the Transaction History from the database
        /// Function Name       :   Create
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/15/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Search()
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || Session["User"] == null) return RedirectToAction("Index", "Member");
            int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
            TxnSearch tvdData = new TxnSearch();
            tvdData.IsVarified = null;
            List<CarListViewData> carList = new List<CarListViewData>();
            List<DriverListViewData> driverList = new List<DriverListViewData>();

            int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
            int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
            
            if (userType == 4)
                tvdData.fleetlist = _userService.UserFleetLists(userId).ToViewDataList();

            if (userType == 2 || userType == 3)
                tvdData.fleetlist = _userService.FleetList(merchantId).ToViewDataList();
            
            tvdData.DriverList = driverList;
            tvdData.CarList = carList;
            tvdData.fk_FleetId = 0;
            tvdData.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            tvdData.Currency = ConfigurationManager.AppSettings["Currency"].ToString();

            return View(tvdData);
        }

        /// <summary>
        /// Purpose             :   Create transaction history on the basis of transaction ID
        /// Function Name       :   Create
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/15/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Search(TxnSearch tvd)
        {
            try
            {
                if (Session["User"] == null) return RedirectToAction("Index", "Member");
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

                int? merchantId = null;
                var fkMerchantId = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;
                if (fkMerchantId != null)
                {
                    merchantId = fkMerchantId;
                    Timezone = _commonService.MerchantTimeZone((int)merchantId);
                }
                else
                {
                    Timezone = TimeZoneInfo.Local.StandardName;// use this time zone
                }

                int fId = 0;
                if (tvd.fk_FleetId != null)
                    fId = (int)tvd.fk_FleetId;

                tvd.fleetlist = _userService.FleetList((int)merchantId).ToViewDataList();
                tvd.CarList = _commonService.GetCars((int)fId).ToViewDataList();
                tvd.DriverList = _commonService.GetDrivers((int)fId).ToViewDataList();
                if (tvd.startDateString != null && tvd.endDateString != null)
                {
                    if (tvd.startDateString.Length > 0 && tvd.endDateString.Length > 0)
                    {
                        bool isDate = false;
                        Regex r = new Regex(@"\d\d-\d\d-\d\d\d\d\s\d\d:\d\d");
                        if (((r.Match(tvd.startDateString).Success) && (r.Match(tvd.endDateString).Success)))
                        {
                            DateTime resultDate;
                            isDate = DateTime.TryParseExact(tvd.startDateString, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return View(tvd);
                            else
                                tvd.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones

                            isDate = DateTime.TryParseExact(tvd.endDateString, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return View(tvd);
                            else
                                tvd.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
                        }
                    }
                }

                if (tvd.fk_FleetId == null && string.IsNullOrEmpty(tvd.VNumber) && string.IsNullOrEmpty(tvd.DNumber) && tvd.startDate == null && tvd.endDate == null && tvd.Id == null && tvd.TType == null && string.IsNullOrEmpty(tvd.AId) && tvd.Industry == null && string.IsNullOrEmpty(tvd.ExpiryDate) && string.IsNullOrEmpty(tvd.FFD) && string.IsNullOrEmpty(tvd.LFD) && string.IsNullOrEmpty(tvd.CrdType) && tvd.MinAmt == null && tvd.MaxAmt == null && string.IsNullOrEmpty(tvd.JobNumber) && tvd.Industry == null && string.IsNullOrEmpty(tvd.SerialNo) && string.IsNullOrEmpty(tvd.TerminalId))
                {
                    ViewBag.Message = MtdataResource.Please_Select_field; //"Please enter or choose some field's";
                    return View(tvd);
                }

                if (!String.IsNullOrEmpty(tvd.ExpiryDate))
                    tvd.ExpiryDate = StringExtension.DateFormat(tvd.ExpiryDate);

                MtdTransactionsViewData td = new MtdTransactionsViewData
                {
                    PageSize = 0,
                    ExpiryDate = tvd.ExpiryDate,
                    VehicleNo = tvd.VNumber,
                    DriverNo = tvd.DNumber,
                    CardType = tvd.CrdType,
                    startDate = tvd.startDate,
                    endDate = tvd.endDate,
                    Id = tvd.Id,
                    TxnType = tvd.TType,
                    FirstFourDigits = tvd.FFD,
                    LastFourDigits = tvd.LFD,
                    MinAmount = tvd.MinAmt,
                    MaxAmount = tvd.MaxAmt,
                    AuthId = tvd.AId,
                    MerchantId = merchantId,
                    AddRespData = tvd.AddRespData,
                    fk_FleetId = tvd.fk_FleetId,
                    EntryMode = tvd.Industry,
                    IsVarified = tvd.IsVarified,
                    SerialNo = tvd.SerialNo,
                    TerminalId = tvd.TerminalId,
                    TransNote = tvd.TransNote   // Ensure TransNote included
                };

                MTDTransactionDto transactionDto = td.ToDTO();
                int count = _transactionService.GetTransactionCount(transactionDto);
                transactionDto.PageSize = count;
                IEnumerable<MTDTransactionDto> stvd = _transactionService.GetTransaction(transactionDto);
                
                int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                var userfleet = _userService.GetUsersFleet(userId);
                int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;

                if (userType == 4)
                    stvd = stvd.Where(x => userfleet.Select(y => y.fk_FleetID).Contains((int)x.fk_FleetId)).ToList();

                IEnumerable<SearchTransactionViewData> mvData = stvd.ToViewData();
                var transactionViewDatas = mvData as SearchTransactionViewData[] ?? mvData.ToArray();

                if (transactionViewDatas.ToList().Count == 0)
                {
                    ViewBag.Message = MtdataResource.No_such_record_exist;
                    return View(tvd);
                }

                string culture = ConfigurationManager.AppSettings["CultureInfo"].ToString();
                string currency = ConfigurationManager.AppSettings["Currency"].ToString();

                using (StringWriter output = new StringWriter())
                {
                    output.WriteLine("\"Id\",\"AuthCode\",\"Vehicle\",\"Driver\",\"Date\",\"Credit/Debit\",\"Transaction Type\",\"Tech Fee(" + currency + ")\",\"Fleet Fee(" + currency + ")\",\"Surcharge(" + currency + ")\",\"Fee(" + currency + ")\",\"Amount(" + currency + ")\",\"CardType\",\"Entry Mode\",\"Approval\"");

                    foreach (SearchTransactionViewData item in transactionViewDatas)
                    {
                        DateTime dt1 = TimeZoneInfo.ConvertTime(Convert.ToDateTime(item.TxnDate), TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(Timezone));
                        decimal tFee = Convert.ToDecimal(item.TechFee);
                        decimal fFee = Convert.ToDecimal(item.FleetFee);
                        decimal fee = Convert.ToDecimal(item.Fee);
                        decimal sur = Convert.ToDecimal(item.Surcharge);
                        decimal amt = Convert.ToDecimal(item.Amount);

                        output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\"",
                            //item.Id, item.AuthId, item.VehicleNo, item.DriverNo, dt1.ToString("MM/dd/yyyy HH:mm:ss"), item.PaymentType, item.TxnType,
                            item.Id, item.AuthId, item.VehicleNo, item.DriverNo, dt1.ToString("G", CultureInfo.GetCultureInfo(culture)), item.PaymentType, item.TxnType,
                            tFee.ToString("C", CultureInfo.GetCultureInfo(culture)), fFee.ToString("C", CultureInfo.GetCultureInfo(culture)),
                            sur.ToString("C", CultureInfo.GetCultureInfo(culture)), fee.ToString("C", CultureInfo.GetCultureInfo(culture)),
                            amt.ToString("C", CultureInfo.GetCultureInfo(culture)), item.CardType, item.EntryMode, item.AddRespData);
                    }

                    _logTracking.Append(string.Format("Transaction Report Excel file Downloaded by {0} at {1}. TransactionHistoryController.Search() ", (((SessionDto)Session["User"]).SessionUser.Email), System.DateTime.Now.ToUniversalTime()));
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Transaction_Report.csv");
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View(tvd);
        }

        /// <summary>
        ///  Created by : Umesh K3
        /// </summary>
        /// <returns></returns>
        public ActionResult BackUp()
        {
            TempData["back"] = 1;
            return RedirectToAction("Create");
        }

    }

}