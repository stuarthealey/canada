IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetUsersCredentials]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_GetUsersCredentials]
GO

CREATE PROCEDURE dbo.usp_GetUsersCredentials
(
	@UserId INT
)
AS 
BEGIN
	SELECT U.UserName, U.[Password], V.fk_DeviceID 
	FROM dbo.[User] U
		INNER JOIN dbo.Vehicle V ON V.fk_UserID = @UserId
END
GO
