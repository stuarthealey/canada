﻿using System;
using System.Collections.Generic;

namespace MTData.Web.ViewData
{
    public class ManageRecieptViewData
    {
        public int FleetID { get; set; }
        public int fk_MerchantID { get; set; }
        public string FleetName { get; set; }
        public string Description { get; set; }  
        public int ReceiptID { get; set; }
        public int fk_FleetID { get; set; }
        public bool IsFleet { get; set; }
        public bool IsVehicle { get; set; }
        public bool IsDriver { get; set; }
        public bool IsDriverTaxNo { get; set; }
        public bool IsCompName { get; set; }
        public bool IsCompAdd { get; set; }
        public bool IsComPhone { get; set; }
        public bool IsPickAdd { get; set; }
        public bool IsDestAdd { get; set; }
        public bool IsFlagFall { get; set; }
        public bool IsFare { get; set; }
        public bool IsExtra { get; set; }
        public bool IsTolls { get; set; }
        public bool IsTip { get; set; }
        public bool IsStateTaxPer { get; set; }
        public bool IsStateTaxAmt { get; set; }
        public bool IsFederalTaxPer { get; set; }
        public bool IsFederalTaxAmt { get; set; }
        public bool IsSurPer { get; set; }
        public bool IsSurAmt { get; set; }
        public bool IsSubTotal { get; set; }
        public bool IsTotal { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool IsTermsAndCondition { get; set; }
        public bool IsDisclaimer { get; set; }
        public bool IsCompanyTaxNo { get; set; }
        public Nullable<int> IsFleetPos { get; set; }
        public Nullable<int> IsVehiclePos { get; set; }
        public Nullable<int> IsDriverPos { get; set; }
        public Nullable<int> IsDriverTaxNoPos { get; set; }
        public Nullable<int> IsCompNamePos { get; set; }
        public Nullable<int> IsCompPhonePos { get; set; }
        public Nullable<int> IsPickAddPos { get; set; }
        public Nullable<int> IsDestAddPos { get; set; }
        public Nullable<int> IsFlagFallPos { get; set; }
        public Nullable<int> IsFarePos { get; set; }
        public Nullable<int> ISExtraPos { get; set; }
        public Nullable<int> IsTollsPos { get; set; }
        public Nullable<int> IsTipPos { get; set; }
        public Nullable<int> IsStateTaxPerPos { get; set; }
        public Nullable<int> IsFederalTaxPerPos { get; set; }
        public Nullable<int> IsFederalTaxAmtPos { get; set; }
        public Nullable<int> IsSurPerPos { get; set; }
        public Nullable<int> IsSurAmtPos { get; set; }
        public Nullable<int> IsSubTotalPos { get; set; }
        public Nullable<int> IsTotalpos { get; set; }
        public Nullable<int> IsTermsConditionsPos { get; set; }
        public Nullable<int> IsDisclaimerPos { get; set; }
        public Nullable<int> IsCompanyTaxNoPos { get; set; }
        public Nullable<int> IsCompAddPos { get; set; }
        public Nullable<int> IsStateTaxAmtPos { get; set; }    
        public IEnumerable<FleetViewData> fleetlist { get; set; }
       
    }

 
}