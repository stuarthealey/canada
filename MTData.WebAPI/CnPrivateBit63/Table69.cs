﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
   public class Table69
    {
        #region Private Constants and Variables

        #region Constants
        private const string tblId = "|36|39";
        private const string version = "|30|31";//Version 01
        private const string visaId1 = "|0B";//For First Occurence
        #endregion

        #region Variables
        private string defaultNumberOfEntries = "|31";//As we have one TPP Id and multiple third party are not involved.//can be 1,2 or 3
        //For First Occurence
        private string defaultVisaBID1 = "|20|20|20|20|20";//If the card type is not a Visa; spaces should be submitted.
        private string defaultVisaAUAR1 = "|00|00|00|00|00|00";//If the AUAR is not present, 6 bytes (12-digits) of Hex 0 should be submitted.
        private string defaultTPPID1 = "|56|4d|54|30|30|31";//Provided by First Data. VMT001
        #endregion

        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'36 39' in Request
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        /// <summary>
        /// Version of the table
        /// default X'30 31' in Request for Version 01
        /// </summary>
        public string Version
        {
            get { return version; }
        }

        /// <summary>
        /// Number of Servicers contained in the table.Valid values are 1, 2 or 3.
        /// default 1 (1= X'31')
        /// </summary>
        public string NumberOfEntries
        {
            get { return defaultNumberOfEntries; }
            set { defaultNumberOfEntries = value; }
        }

        #region First Occurence
        /// <summary>
        /// Visa ID (First Occurrence)
        /// Constant value of Hex "0B" for all card types
        /// </summary>
        public string VisaID1
        {
            get { return visaId1; }
        }

        /// <summary>
        /// Visa Business Identifier (BID) (First Occurrence)
        /// </summary>
        public string VisaBID1
        {
            get { return defaultVisaBID1; }
            set { defaultVisaBID1 = value; }
        }

        /// <summary>
        /// Visa’s Agent Unique Account Result (AUAR). (First Occurrence)
        /// </summary>
        public string VisaAUAR1
        {
            get { return defaultVisaAUAR1; }
            set { defaultVisaAUAR1 = value; }
        }

        /// <summary>
        /// Third Party Processor ID provided by First Data 
        /// to all TPP and vendors. (First Occurrence)
        /// </summary>
        public string TPPID1
        {
            get { return defaultTPPID1; }
            set { defaultTPPID1 = value; }
        }

        #endregion

        //Second and Third occurence is not applicable for this application
        #endregion
    }
}
