﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using MTDataDispatchSystemSync.Models;

namespace MTDataDispatchSystemSync
{
    [ServiceContract]
    public interface IESIWebService
    {
        [OperationContract]
        string Authenticate(string SystemID, string userName, string pCode);

        [OperationContract]
        string GetVehicleByCarNumberForFleet(string token, string carNumber, int fleetID);

        [OperationContract]
        string SaveVehicle(string token, Vehicle vehicle);


        [OperationContract]
        string GetDriverByID(string authenticationToken, int fleetID, string driverNumber);

        [OperationContract]
        string SaveDriver(string authenticationToken, Driver driver);
    }
}
