﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MTD.Data.Data;

namespace MTD.Data.Repository.Interface
{
   public interface ICarOwnersRepository
    {
       bool Add(CarOwner carOwner);
       CarOwner Read(int id);
       IEnumerable<CarOwner> CarOwnerList();
       bool Update(CarOwner carOwner);
       bool Remove(int id);
       int GetOwnerCount(string name, int merchantId, string ownerName);
       IEnumerable<CarOwner> GetOwnerListByFilter(FilterSearchParameters searchParameters);
       int IsValidCarOwner(CarOwner carOwner);
       CarOwner CarOwnerExcel(CarOwner CarOwnerExcel);
    }
}
