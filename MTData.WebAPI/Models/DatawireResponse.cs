﻿using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class DatawireResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string Status { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Value { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ClientRef { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Did { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string Version { get; set; }
    }
}