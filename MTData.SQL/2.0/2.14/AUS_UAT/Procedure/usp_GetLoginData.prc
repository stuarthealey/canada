
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspGetLoginData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspGetLoginData]
GO

/****** Object:  StoredProcedure [dbo].[uspGetLoginData]    Script Date: 11/2/2015 10:59:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[uspGetLoginData]
(
@DeviceId varchar(100),
@DriverId varchar(50),
@UserType int
)
AS
BEGIN


DECLARE 
@Error nvarchar(max),
@TerminalID int,
@fk_MerchantID int,
@fleetId int,
@VehicleNumber varchar(50),
@PIN nvarchar(300),
@DriverNo nvarchar(50),
@DriverTaxNo nvarchar(50),

--Fleet
@fK_CurrencyCodeID int,
@FleetName nvarchar(100),
@IsCardSwiped bit,
@IsContactless bit,
@IsChipAndPin bit,
@IsShowTip bit,
@CurrencyCode char(3),

--Merchant
@CompanyTaxNumber nvarchar(20),
@Company nvarchar(100),
@TipPerLow decimal(4,2),
@TipPerMedium decimal(4,2),
@TipPerHigh decimal(4,2),
@FederalTaxRate decimal(4,2),
@StateTaxRate decimal(4,2),
@Disclaimer nvarchar(max),
@TermCondition nvarchar(max),
@IsTaxInclusive bit,

--User
@Address nvarchar(250),
@fk_City varchar(50),
@fk_State varchar(50),
@fk_Country nvarchar(40),
@Phone nvarchar(20),
@fleet_MechantId int


SET @Error=''
IF EXISTS(SELECT 1 FROM Terminal WHERE MacAdd=@DeviceId AND IsActive=1)
BEGIN
	select @TerminalID=TerminalID, @fk_MerchantID=fk_MerchantID FROM Terminal WHERE MacAdd=@DeviceId AND IsActive=1

	IF EXISTS(SELECT 1 FROM Vehicle WHERE fk_TerminalID=@TerminalID AND IsActive=1)
	BEGIN
		select @fleetId=fk_FleetID,@VehicleNumber=VehicleNumber FROM Vehicle WHERE fk_TerminalID=@TerminalID AND IsActive=1

		IF EXISTS(SELECT 1 FROM Driver WHERE DriverNo=@DriverId and fk_FleetID=@fleetId and IsActive=1)
		BEGIN
			SELECT @PIN=PIN, @DriverNo=DriverNo,@DriverTaxNo=DriverTaxNo 
			FROM Driver WHERE DriverNo=@DriverId and fk_FleetID=@fleetId and IsActive=1

			IF EXISTS(SELECT 1 FROM Fleet WHERE FleetID=@fleetId AND IsActive=1)
			BEGIN
				SELECT @FleetName=FleetName,@IsCardSwiped=IsCardSwiped,@IsContactless=IsContactless,
				@IsChipAndPin=IsChipAndPin,@IsShowTip=IsShowTip,@fK_CurrencyCodeID=fK_CurrencyCodeID,
				@fleet_MechantId=fk_MerchantID
				FROM Fleet WHERE FleetID=@fleetId AND IsActive=1

				IF(@fleet_MechantId<>@fk_MerchantID)
				BEGIN
					SET @Error='Invalid driver configuration, please contact support'
				END
				ELSE
				BEGIN

						SELECT @CurrencyCode=CurrencyCode FROM Currency WHERE CurrencyCodeID=@fK_CurrencyCodeID	
				
						IF EXISTS(SELECT 1 FROM Merchant WHERE MerchantID=@fk_MerchantID AND IsActive=1)
						BEGIN
							SELECT	@CompanyTaxNumber=CompanyTaxNumber,
									@Company=Company,
									@TipPerLow=TipPerLow,
									@TipPerMedium=TipPerMedium,
									@TipPerHigh=TipPerHigh,
									@FederalTaxRate=FederalTaxRate,
									@StateTaxRate=StateTaxRate,
									@Disclaimer=DisclaimerPlainText,
									@TermCondition=TermConditionPlainText,
									@IsTaxInclusive=IsTaxInclusive
									FROM Merchant WHERE MerchantID=@fk_MerchantID AND IsActive=1

								

							IF EXISTS(SELECT 1 FROM Users WHERE fk_MerchantID=@fk_MerchantID AND fk_UserTypeID=@UserType AND IsActive=1)
							BEGIN
								SELECT	@Address=[Address],
										@fk_City=fk_City,
										@fk_State=fk_State,
										--@fk_Country=fk_Country,
									   @fk_Country= (select top 1 Title from countrycodes cc join users us on cc.CountryCodesID=us.fk_Country)  ,
										@Phone=Phone
										FROM Users WHERE fk_MerchantID=@fk_MerchantID AND fk_UserTypeID=@UserType AND IsActive=1
							END
							ELSE
								SET @Error='Invalid driver configuration, please contact support'
						END
				ELSE
					SET @Error='Invalid driver configuration, please contact support'	
				END		
			END
			ELSE
				SET @Error='Invalid driver configuration, please contact support'

		END
		ELSE
			SET @Error='Invalid driver configuration, please contact support'
	END
	ELSE
		SET @Error='Invalid driver configuration, please contact support'
END
ELSE
	SET @Error='Invalid terminal configuration, please contact support'

select 
@Error 'Error',
@TerminalID 'TerminalID',
@fk_MerchantID 'fk_MerchantID',
@fleetId 'fleetId',
@VehicleNumber 'VehicleNumber',
@PIN 'PIN',
@DriverNo 'DriverNo',
@DriverTaxNo 'DriverTaxNo',
@FleetName 'FleetName',
@IsCardSwiped 'IsCardSwiped',
@IsContactless 'IsContactless',
@IsChipAndPin 'IsChipAndPin',
@IsShowTip 'IsShowTip',
@CurrencyCode 'CurrencyCode',
@CompanyTaxNumber 'CompanyTaxNumber',
@Company 'Company',
@TipPerLow 'TipPerLow',
@TipPerMedium 'TipPerMedium',
@TipPerHigh 'TipPerHigh',
@FederalTaxRate 'FederalTaxRate',
@StateTaxRate 'StateTaxRate',
@Disclaimer 'DisclaimerPlainText',
@TermCondition 'TermConditionPlainText',
@IsTaxInclusive 'IsTaxInclusive',
@Address 'Address',
@fk_City 'fk_City',
@fk_State 'fk_State',
@fk_Country 'fk_Country',
@Phone 'Phone'

END

--exec uspGetLoginData '7c:F9:0E:1F:89:AD','501',2


