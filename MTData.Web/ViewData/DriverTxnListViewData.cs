﻿namespace MTData.Web.ViewData
{
    public class DriverTxnListViewData
    {
        public string DriverText { get; set; }
        public string DriverValue { get; set; }
    }
}