﻿using System;

namespace MTD.Core.DataTransferObjects
{
    public class ReceiptListDto
    {
      
        public string fk_FieldText { get; set; }
        public int Postion { get; set; }
        public Nullable<bool> IsShow { get; set; }
        public string FieldName { get; set; }
        
    }
}
