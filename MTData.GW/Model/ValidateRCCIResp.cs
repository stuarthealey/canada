﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MTData.WCFGW.Model
{
	[DataContract(Namespace = "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService")]
	public class ValidateRCCIResp
	{
		[DataMember]
		public CreditCardStatus CreditCardStatus { get; set; }

		[DataMember]
		public TruncatedCreditCardDetails TruncatedCreditCardDetails { get; set; }
	}
}