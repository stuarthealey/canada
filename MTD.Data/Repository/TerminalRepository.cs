﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Configuration;
using System.Xml;

using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Data.Repository
{
    public class TerminalRepository : BaseRepository, ITerminalRepository
    {
        public TerminalRepository(IMtDataDevlopmentsEntities dbMtData) : base(dbMtData)
        {
        }

        Regex regexNum = new Regex(@"^[0-9]+$");
        Regex alphaNum = new Regex(@"^[a-zA-Z0-9 ]+$");
        Regex alphaNumOnly = new Regex(@"^[a-zA-Z0-9]+$");
        Regex serialNO = new Regex(@"^[A-Za-z0-9 -:,.]+$");

        /// <summary>
        /// Purpose             :   to add the terminal
        /// Function Name       :   Add
        /// Created By          :   Salil gupta
        /// Created On          :   12/19/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="terminal"></param>
        /// <param name="termId"></param>
        public bool Add(Terminal terminal, string termId)
        {
            try
            {
                Datawire datawire = (from m in DbMtData.Datawires where m.RCTerminalId == termId && m.MerchantId == terminal.fk_MerchantID && m.IsActive select m).FirstOrDefault();
                if (datawire != null)
                {
                    datawire.IsAssigned = true;
                    datawire.ModifiedBy = terminal.CreatedBy;
                    datawire.ModifiedDate = DateTime.Now;
                    terminal.DatawireId = datawire.ID;
                    terminal.IsActive = true;
                    terminal.IsTerminalAssigned = false;
                    terminal.CreatedDate = DateTime.Now;
                    DbMtData.Terminals.Add(terminal);
                    DbMtData.CommitChanges();
                    return true;
                }
                string project = ConfigurationManager.AppSettings["Project"];
                if (project == "UK" && datawire == null)
                {
                    terminal.IsActive = true;
                    terminal.IsTerminalAssigned = false;
                    terminal.CreatedDate = DateTime.Now;
                    DbMtData.Terminals.Add(terminal);
                    DbMtData.CommitChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   to get the terminal
        /// Function Name       :   GetTerminal
        /// Created By          :   Salil gupta
        /// Created On          :   12/20/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        /// 
        public Terminal GetTerminal(int terminalId)
        {
            try
            {
                Terminal terminalDb = DbMtData.Terminals.Find(terminalId);
                return terminalDb;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   to update the terminal
        /// Function Name       :   Update
        /// Created By          :   Salil gupta
        /// Created On          :   12/22/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="terminal"></param>
        /// <param name="terminalId"></param>
        /// <param name="termId"></param>
        public void Update(Terminal terminal, int terminalId, string termId)
        {
            try
            {
                Terminal terminalDb = DbMtData.Terminals.Find(terminalId);
                var datawire = DbMtData.Datawires.Find(terminalDb.DatawireId);
                if(datawire!=null)
                {
                    datawire.IsAssigned = false;
                    datawire.ModifiedBy = terminal.CreatedBy;
                    datawire.ModifiedDate = DateTime.Now;
                }

                Datawire newDatawire = (from m in DbMtData.Datawires where m.RCTerminalId == termId && m.MerchantId == terminal.fk_MerchantID && m.IsActive select m).FirstOrDefault();
                if (newDatawire != null)
                {
                    newDatawire.IsAssigned = true;
                    newDatawire.ModifiedBy = terminal.CreatedBy;
                    newDatawire.ModifiedDate = DateTime.Now;
                    terminalDb.DatawireId = newDatawire.ID;
                    terminalDb.SerialNo = terminal.SerialNo;
                    terminalDb.DeviceName = terminal.DeviceName;
                    terminalDb.MacAdd = terminal.MacAdd;
                    terminalDb.SoftVersion = terminal.SoftVersion;
                    terminalDb.ModifiedBy = terminal.ModifiedBy;
                    terminalDb.Description = terminal.Description;
                    terminalDb.ModifiedDate = DateTime.Now;
                    terminalDb.IsContactLess = terminal.IsContactLess;
                    DbMtData.CommitChanges();
                }

                string project = ConfigurationManager.AppSettings["Project"];
                if (project == "UK" && datawire == null)
                {
                    terminalDb.SerialNo = terminal.SerialNo;
                    terminalDb.DeviceName = terminal.DeviceName;
                    terminalDb.MacAdd = terminal.MacAdd;
                    terminalDb.SoftVersion = terminal.SoftVersion;
                    terminalDb.ModifiedBy = terminal.ModifiedBy;
                    terminalDb.Description = terminal.Description;
                    terminalDb.ModifiedDate = DateTime.Now;
                    terminalDb.IsContactLess = terminal.IsContactLess;
                    DbMtData.CommitChanges();
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   to delete the terminal
        /// Function Name       :   DeleteTerminal
        /// Created By          :   Salil gupta
        /// Created On          :   12/23/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="terminalId"></param>
        /// <param name="modifiedBy"></param>
        /// <returns></returns>
        public string DeleteTerminal(int terminalId, string modifiedBy)
        {
            try
            {
                Terminal terminalDb = DbMtData.Terminals.Find(terminalId);
                var vehicleDb = (from m in DbMtData.Vehicles where terminalId == m.fk_TerminalID && m.IsActive select m).FirstOrDefault();
                if (vehicleDb != null)
                {
                    string vehicleNumber = vehicleDb.VehicleNumber;
                    return vehicleNumber;
                }
                terminalDb.IsActive = false;
                var datawire = DbMtData.Datawires.Find(terminalDb.DatawireId);
                if (datawire != null)
                {
                    datawire.IsAssigned = false;
                    datawire.ModifiedDate = DateTime.Now;
                    datawire.ModifiedBy = modifiedBy;
                    terminalDb.DatawireId = null;
                }
                DbMtData.CommitChanges();
                return null;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   to get the list of terminal
        /// Function Name       :   TerminalList
        /// Created By          :   Salil gupta
        /// Created On          :   12/23/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<Terminal> TerminalList(int merchantId)
        {
            try
            {
                var terminalList = from m in DbMtData.Terminals where m.IsActive && m.fk_MerchantID == merchantId select m;
                return terminalList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   to filter the terminal list
        /// Function Name       :   TerminalListByFiter
        /// Created By          :   Salil gupta
        /// Created On          :   12/23/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="fsParametes"></param>
        /// <returns></returns>
        public IEnumerable<Terminal> TerminalListByFiter(FilterSearchParameters fsParametes)
        {
            try
            {
                IEnumerable<Terminal> query = (from m in DbMtData.Terminals join n in DbMtData.PaymentTerminals on m.DeviceName equals n.PayTerminalId.ToString()
                                               join dwire in DbMtData.Datawires on  m.DatawireId equals dwire.ID into g
                                               from rt in g.DefaultIfEmpty() 
                                               where m.IsActive && m.fk_MerchantID == fsParametes.MerchantId select new { m.MacAdd, n.DeviceName, m.SerialNo, m.TerminalID, rt.RCTerminalId }).AsEnumerable().Select(objNew => new Terminal
                {
                    DeviceName = objNew.DeviceName,
                    MacAdd = objNew.MacAdd,
                    SerialNo = objNew.SerialNo,
                    TerminalID = objNew.TerminalID,
                    Description = (string.IsNullOrEmpty(objNew.RCTerminalId) ? "" : objNew.RCTerminalId) //Description representing RCTermainlID. Handle NULL RCTerminalID value coming back.
                }).ToList();

                query = query.OrderBy(p => p.DeviceName); //Default!

                if (!string.IsNullOrEmpty(fsParametes.Name))
                    query = query.Where(p => p.DeviceName.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();

                if (!string.IsNullOrEmpty(fsParametes.RapidConnectID))
                    query = query.Where(p => p.Description.IndexOf(fsParametes.RapidConnectID, StringComparison.OrdinalIgnoreCase) >= 0).ToList();

                if (!string.IsNullOrEmpty(fsParametes.SerialNumber))
                    query = query.Where(p => p.SerialNo.IndexOf(fsParametes.SerialNumber, StringComparison.OrdinalIgnoreCase) >= 0).ToList();

                if (!string.IsNullOrEmpty(fsParametes.Name) && !string.IsNullOrEmpty(fsParametes.RapidConnectID) && !string.IsNullOrEmpty(fsParametes.SerialNumber))
                    query = query.Where(p => p.DeviceName.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0 && p.SerialNo.IndexOf(fsParametes.SerialNumber, StringComparison.OrdinalIgnoreCase) >= 0).ToList();

                if (!string.IsNullOrEmpty(fsParametes.Sorting))
                    query = SortedTerminal(fsParametes.Sorting, query);

                return fsParametes.PageSize > 0
                           ? query.Skip(fsParametes.StartIndex).Take(fsParametes.PageSize).ToList() //Paging
                           : query.ToList(); //No paging
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   to get terminal
        /// Function Name       :   GetTerminal
        /// Created By          :   Salil gupta
        /// Created On          :   12/24/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public Terminal GetTerminal(string deviceId)
        {
            var terminalDb = DbMtData.Terminals.FirstOrDefault(a => a.MacAdd.Equals(deviceId) && a.IsActive == true);
            return terminalDb;
        }

        /// <summary>
        /// Purpose             :   to get the terminal stan reference
        /// Function Name       :   GetTerminalStanRef
        /// Created By          :   Salil gupta
        /// Created On          :   12/24/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="serialNumber"></param>
        /// <returns></returns>
        public Datawire GetTerminalStanRef(int merchantId, string serialNumber)
        {
            var transParam = DbMtData.Datawires.FirstOrDefault(a => a.MerchantId == merchantId && a.RCTerminalId == serialNumber);
            return transParam;
        }

        /// <summary>
        /// Purpose             :   To check whether it is registered or not
        /// Function Name       :   IsRegistered
        /// Created By          :   Salil gupta
        /// Created On          :   12/26/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="serialNumber"></param>
        /// <returns></returns>
        public bool IsRegistered(int merchantId, string serialNumber)
        {
            Terminal transParam = DbMtData.Terminals.FirstOrDefault(a => a.fk_MerchantID == merchantId && a.SerialNo == serialNumber);
            if (transParam != null)
            {
                if (transParam.DatawireId != null)
                {
                    int did = (int)transParam.DatawireId;
                    return DbMtData.Datawires.Any(x => x.ID == did);
                }
            }

            return false;
        }

        /// <summary>
        /// Purpose             :   For completion transaction
        /// Function Name       :   Completion
        /// Created By          :   Salil gupta
        /// Created On          :   12/26/2014
        /// Modificatios Made   :   ****************************
        /// Modified By         :   Sunil Singh
        /// Modidified On       :   21-Jan-2016
        /// Modification Detail :   Removed 'SerialNo' from transaction fetching criteria to fix Refund/Void problem
        /// </summary>
        /// <param name="transId"></param>
        /// <param name="serialNo"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public MTDTransaction Completion(string transId, string serialNo, int merchantId)
        {
            int trxId = Convert.ToInt32(transId);

            MTDTransaction mtd = DbMtData.MTDTransactions.FirstOrDefault(a => a.MerchantId == merchantId && a.Id == trxId);
            return mtd;
        }

        /// <summary>
        /// Purpose             :   To update stan
        /// Function Name       :   UpdateStan
        /// Created By          :   Salil gupta 
        /// Created On          :   12/26/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="stan"></param>
        /// <param name="transRefNo"></param>
        /// <param name="serialNo"></param>
        public void UpdateStan(string stan, string transRefNo, string serialNo, int merchantId)
        {
            try
            {
                Datawire tr = DbMtData.Datawires.FirstOrDefault(a => a.RCTerminalId == serialNo && a.MerchantId == merchantId);
                if (tr != null && stan != null && transRefNo != null)
                {
                    tr.RefNumber = transRefNo;
                    tr.Stan = stan;
                    DbMtData.CommitChanges();
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To check IsValid serial no
        /// Function Name       :   IsValid
        /// Created By          :   Salil gupta 
        /// Created On          :   12/26/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <returns></returns>
        public bool IsValid(string macAddress)
        {
            return DbMtData.Terminals.Any(m => m.MacAdd == macAddress && m.IsActive);
        }

        /// <summary>
        /// Purpose             :   To check the rapid connect terminal id is registered or not
        /// Function Name       :   IsDatawireRegistered
        /// Created By          :   Naveen Kumar 
        /// Created On          :    04/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        public bool IsDatawireRegistered(string serialNumber, string terminalId)
        {
            return DbMtData.Datawires.Any(m => m.IsAssigned == true && m.IsActive);
        }

        /// <summary>
        /// Purpose             :   To check whether rapid connect terminal id already exists or not
        /// Function Name       :   IsRcExist
        /// Created By          :   Naveen Kumar
        /// Created On          :   04/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="rcTerminalId"></param>
        /// <returns></returns>
        public bool IsRcExist(string rcTerminalId)
        {
            return DbMtData.Datawires.Any(m => m.IsActive && m.RCTerminalId == rcTerminalId);
        }

        /// <summary>
        /// Purpose             :   To add rapid connect terminal Id
        /// Function Name       :   AddRcTerminalId
        /// Created By          :   Naveen Kumar
        /// Created On          :   04/14/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="datawire"></param>
        /// <returns></returns>
        public void AddRcTerminalId(Datawire datawire)
        {
            datawire.IsActive = true;
            datawire.IsAssigned = false;
            datawire.CreatedDate = DateTime.Now;
            DbMtData.Datawires.Add(datawire);
            DbMtData.CommitChanges();
        }

        /// <summary>
        /// Purpose             :   To get rapid connect terminal id of selected terminal
        /// Function Name       :   GetRcTerminalId
        /// Created By          :   Naveen Kumar
        /// Created On          :   04/13/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        public string GetRcTerminalId(int terminalId)
        {
            var terminal = DbMtData.Terminals.Find(terminalId);
            var datawire = DbMtData.Datawires.Find(terminal.DatawireId);
            if (datawire != null)
                return datawire.RCTerminalId;
            return string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PaymentTerminal> GetDeviceList()
        {
            var deviceList = from m in DbMtData.PaymentTerminals where (bool)m.IsActive select m;
            return deviceList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public PaymentTerminal GetDevice(int deviceId)
        {
            var device = DbMtData.PaymentTerminals.Find(deviceId);
            return device;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public bool AddDevicename(PaymentTerminal device)
        {
            bool isExist = DbMtData.PaymentTerminals.Any(x => x.DeviceName == device.DeviceName);
            if (!isExist)
            {
                device.IsActive = true;
                device.CreatedDate = DateTime.Now;
                DbMtData.PaymentTerminals.Add(device);
                DbMtData.CommitChanges();
                return true;
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceName"></param>
        /// <param name="deviceId"></param>
        public void UpdateDevicename(PaymentTerminal deviceName, int deviceId)
        {
            var deviceDb = DbMtData.PaymentTerminals.Find(deviceId);
            deviceDb.DeviceName = deviceName.DeviceName;
            deviceDb.ModifiedBy = deviceName.ModifiedBy;
            deviceDb.ModifiedDate = DateTime.Now;
            DbMtData.CommitChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public bool DeleteDevicename(int deviceId)
        {
            var deviceDb = DbMtData.PaymentTerminals.Find(deviceId);
            bool isUsed = DbMtData.Terminals.Any(m => m.DeviceName == deviceId.ToString() && m.IsActive);
            if (isUsed)
            {
                return true;
            }
            deviceDb.IsActive = false;
            DbMtData.CommitChanges();
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="index"></param>
        /// <param name="page"></param>
        /// <param name="sorting"></param>
        /// <returns></returns>
        public IEnumerable<PaymentTerminal> DeviceListByFiter(string name, int index, int page, string sorting)
        {
            try
            {
                var query = from m in DbMtData.PaymentTerminals where m.IsActive == true select m;
                query = query.OrderBy(p => p.DeviceName); //Default!
                
                if (!string.IsNullOrEmpty(name))
                {
                    query = query.Where(p => p.DeviceName.Contains(name));
                }

                if (!string.IsNullOrEmpty(sorting))
                {
                    switch (sorting)
                    {
                        case "DeviceName ASC":
                            query = query.OrderBy(p => p.DeviceName);
                            break;
                        case "DeviceName DESC":
                            query = query.OrderByDescending(p => p.DeviceName);
                            break;
                    }
                }

                return page > 0
                             ? query.Skip(index).Take(page).ToList() //Paging
                             : query.ToList(); //No paging
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceName"></param>
        /// <returns></returns>
        public bool PaymentDeviceIsExist(string deviceName)
        {
            var res = DbMtData.PaymentTerminals.Any(m => (bool)m.IsActive && m.DeviceName == deviceName);
            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rcTerminalId"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public bool RcTerminalIdIsExist(string rcTerminalId, int merchantId)
        {
            string project = ConfigurationManager.AppSettings["Project"];
            if (String.IsNullOrEmpty(rcTerminalId) && project == "UK")
            {
                return true;
            }
            var res = DbMtData.Datawires.Any(m => (bool)m.IsActive && m.RCTerminalId == rcTerminalId && !(bool)m.IsAssigned && m.MerchantId == merchantId);
            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objTerminal"></param>
        public void AddTermialDevice(TerminalExcelUpload objTerminal)
        {
            try
            {
                if (objTerminal != null)
                {
                    Terminal dbTerminal = new Terminal();

                    PaymentTerminal paymentTerminal = (from pt in DbMtData.PaymentTerminals where pt.DeviceName == objTerminal.DeviceName && (bool)pt.IsActive select pt).FirstOrDefault();
                    Datawire datawire = (from m in DbMtData.Datawires where m.RCTerminalId == objTerminal.TermId && m.IsActive && m.MerchantId == objTerminal.fk_MerchantID && !(bool)m.IsAssigned select m).FirstOrDefault();
                    
                    if (datawire != null)
                    {
                        datawire.IsAssigned = true;
                        datawire.ModifiedBy = objTerminal.CreatedBy;
                        datawire.ModifiedDate = DateTime.Now;
                        dbTerminal.SerialNo = objTerminal.SerialNo;
                        dbTerminal.DeviceName = Convert.ToString(paymentTerminal.PayTerminalId);
                        dbTerminal.MacAdd = objTerminal.MacAdd;
                        dbTerminal.SoftVersion = objTerminal.SoftVersion;
                        dbTerminal.Description = objTerminal.Description;
                        dbTerminal.fk_MerchantID = objTerminal.fk_MerchantID;
                        dbTerminal.IsActive = true;
                        dbTerminal.CreatedBy = objTerminal.CreatedBy;
                        dbTerminal.CreatedDate = DateTime.Now;
                        dbTerminal.ModifiedBy = objTerminal.ModifiedBy;
                        dbTerminal.ModifiedDate = DateTime.Now;
                        dbTerminal.IsTerminalAssigned = false;
                        dbTerminal.Stan = objTerminal.Stan;
                        dbTerminal.RefNumber = objTerminal.RefNumber;
                        dbTerminal.TransactionDate = DateTime.Now;
                        dbTerminal.DatawireId = datawire.ID;

                        DbMtData.Terminals.Add(dbTerminal);
                        DbMtData.CommitChanges();
                    }
                    else
                    {
                        string project = ConfigurationManager.AppSettings["Project"];
                        if (project == "UK" && datawire == null)
                        {
                            dbTerminal.SerialNo = objTerminal.SerialNo;
                            dbTerminal.DeviceName = Convert.ToString(paymentTerminal.PayTerminalId);
                            dbTerminal.MacAdd = objTerminal.MacAdd;
                            dbTerminal.SoftVersion = objTerminal.SoftVersion;
                            dbTerminal.Description = objTerminal.Description;
                            dbTerminal.fk_MerchantID = objTerminal.fk_MerchantID;
                            dbTerminal.IsActive = true;
                            dbTerminal.CreatedBy = objTerminal.CreatedBy;
                            dbTerminal.CreatedDate = DateTime.Now;
                            dbTerminal.ModifiedBy = objTerminal.ModifiedBy;
                            dbTerminal.ModifiedDate = DateTime.Now;
                            dbTerminal.IsTerminalAssigned = false;
                            dbTerminal.Stan = objTerminal.Stan;
                            dbTerminal.RefNumber = objTerminal.RefNumber;
                            dbTerminal.TransactionDate = DateTime.Now;

                            DbMtData.Terminals.Add(dbTerminal);
                            DbMtData.CommitChanges();
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To check whether it is registered or not
        /// Function Name       :   IsRegistered
        /// Created By          :   Salil gupta
        /// Created On          :   12/26/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="serialNumber"></param>
        /// <returns></returns>
        public bool IsMacAddressRegistered(string macAddress)
        {
            Terminal transParam = DbMtData.Terminals.FirstOrDefault(a => a.MacAdd == macAddress && a.IsActive);
            if (transParam != null)
            {
                if (transParam.DatawireId != null)
                {
                    int did = (int)transParam.DatawireId;
                    return DbMtData.Datawires.Any(x => x.ID == did);
                }
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<Terminal> SortedTerminal(string sorting, IEnumerable<Terminal> query)
        {
            switch (sorting)
            {
                case "DeviceName ASC":
                    query = query.OrderBy(p => p.DeviceName);
                    break;
                case "DeviceName DESC":
                    query = query.OrderByDescending(p => p.DeviceName);
                    break;
                case "SerialNo ASC":
                    query = query.OrderBy(p => p.SerialNo);
                    break;
                case "SerialNo DESC":
                    query = query.OrderByDescending(p => p.SerialNo);
                    break;
                case "MacAdd ASC":
                    query = query.OrderBy(p => p.MacAdd);
                    break;
                case "MacAdd DESC":
                    query = query.OrderByDescending(p => p.MacAdd);
                    break;
            }

            return query;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterSearch"></param>
        /// <returns></returns>
        public IEnumerable<Terminal> GetUserTerminal(FilterSearchParameters filterSearch)
        {
            try
            {
                var results = DbMtData.Terminals.Where(t => DbMtData.Vehicles.Where(v => DbMtData.UserFleets.Where(x => x.fk_UserID == filterSearch.UserId).Select(x => x.fk_FleetID).Contains(v.fk_FleetID)).Select(x => x.fk_TerminalID).Contains(t.TerminalID) && t.fk_MerchantID == filterSearch.MerchantId && (bool)t.IsActive || !(bool)t.IsTerminalAssigned).ToList();
                IEnumerable<Terminal> query = (from m in results
                                               join n in DbMtData.PaymentTerminals on m.DeviceName equals n.PayTerminalId.ToString()
                                               join k in DbMtData.Datawires on m.DatawireId equals k.ID
                                               where m.IsActive && m.fk_MerchantID == filterSearch.MerchantId
                                               select new { m.MacAdd, n.DeviceName, m.SerialNo, m.TerminalID, k.RCTerminalId }).AsEnumerable().Select(objNew => new Terminal
                {

                    DeviceName = objNew.DeviceName,
                    MacAdd = objNew.MacAdd,
                    SerialNo = objNew.SerialNo,
                    TerminalID = objNew.TerminalID,
                    Description = objNew.RCTerminalId
                }).ToList();

                query = query.OrderBy(p => p.DeviceName); //Default!
                if (!string.IsNullOrEmpty(filterSearch.Name) && string.IsNullOrEmpty(filterSearch.RapidConnectID) && string.IsNullOrEmpty(filterSearch.SerialNumber))
                {
                    query = query.Where(p => p.DeviceName.ToUpper().IndexOf(filterSearch.Name.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                else if (!string.IsNullOrEmpty(filterSearch.RapidConnectID) && string.IsNullOrEmpty(filterSearch.Name) && string.IsNullOrEmpty(filterSearch.SerialNumber))
                {
                    query = query.Where(p => p.Description.ToUpper().IndexOf(filterSearch.RapidConnectID.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                else if (!string.IsNullOrEmpty(filterSearch.SerialNumber) && string.IsNullOrEmpty(filterSearch.Name) && string.IsNullOrEmpty(filterSearch.RapidConnectID))
                {
                    query = query.Where(p => p.SerialNo.ToUpper().IndexOf(filterSearch.SerialNumber.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                else if (!string.IsNullOrEmpty(filterSearch.Name) && !string.IsNullOrEmpty(filterSearch.RapidConnectID) && string.IsNullOrEmpty(filterSearch.SerialNumber))
                {
                    query = query.Where(p => p.DeviceName.ToUpper().IndexOf(filterSearch.Name.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0 && p.Description.ToUpper().IndexOf(filterSearch.RapidConnectID.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                else if (!string.IsNullOrEmpty(filterSearch.Name) && string.IsNullOrEmpty(filterSearch.RapidConnectID) && !string.IsNullOrEmpty(filterSearch.SerialNumber))
                {
                    query = query.Where(p => p.DeviceName.ToUpper().IndexOf(filterSearch.Name.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0 && p.SerialNo.ToUpper().IndexOf(filterSearch.SerialNumber.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                else if (string.IsNullOrEmpty(filterSearch.Name) && !string.IsNullOrEmpty(filterSearch.RapidConnectID) && !string.IsNullOrEmpty(filterSearch.SerialNumber))
                {
                    query = query.Where(p => p.SerialNo.ToUpper().IndexOf(filterSearch.SerialNumber.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0 && p.Description.ToUpper().IndexOf(filterSearch.RapidConnectID.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                else if (!string.IsNullOrEmpty(filterSearch.Name) && !string.IsNullOrEmpty(filterSearch.RapidConnectID) && !string.IsNullOrEmpty(filterSearch.SerialNumber))
                {
                    query = query.Where(p => p.DeviceName.ToUpper().IndexOf(filterSearch.Name.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0 && p.SerialNo.ToUpper().IndexOf(filterSearch.SerialNumber.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0 && p.Description.ToUpper().IndexOf(filterSearch.RapidConnectID.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }

                if (!string.IsNullOrEmpty(filterSearch.Sorting))
                {
                    switch (filterSearch.Sorting)
                    {
                        case "DeviceName ASC":
                            query = query.OrderBy(p => p.DeviceName);
                            break;
                        case "DeviceName DESC":
                            query = query.OrderByDescending(p => p.DeviceName);
                            break;
                    }
                }

                return filterSearch.PageSize > 0
                             ? query.Skip(filterSearch.StartIndex).Take(filterSearch.PageSize).ToList() //Paging
                             : query.ToList(); //No paging
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="terminal"></param>
        /// <returns></returns>
        public int IsValidTerminalExcel(TerminalExcelUpload terminal)
        {
            int response = 0;

            if (terminal.DeviceName == string.Empty)
                return 5;
            if (terminal.SerialNo == string.Empty)
                return 6;
            
            //terminal not mendatory for uk CountryCode
            string countryCode = Convert.ToString(ConfigurationManager.AppSettings["Project"]);
            if (!countryCode.Equals("UK"))
            {
                if (terminal.TermId == string.Empty)
                    return 7;
            }
            
            if (terminal.MacAdd == string.Empty)
                return 8;
            
            if (terminal.Description.Length >= 200)
                return 12;
            
            if (terminal.DeviceName.Length >= 100)
                return 13;
            
            Match exp = serialNO.Match(terminal.SerialNo);
            if (!exp.Success)
                return 14;
            
            exp = serialNO.Match(terminal.MacAdd);
            
            if (!exp.Success)
                return 16;
            
            if (terminal.SerialNo.Length >= 50)
                return 17;
            
            if (terminal.MacAdd.Length >= 100)
                return 18;

            var IsterminalExists = DbMtData.PaymentTerminals.Any(m => (bool)m.IsActive && m.DeviceName == terminal.DeviceName);
            if (!IsterminalExists)
            {
                response = 2;// Device does not exists.
                return response;
            }

            if (!countryCode.Equals("UK"))
            {
                var IsTerminal = DbMtData.Datawires.Any(m => m.RCTerminalId == terminal.TermId);
                if (!IsTerminal)
                {
                    // Invalid Rapid Connect terminal.
                    return 15;
                }

                var IsTerminalExists = DbMtData.Datawires.Any(m => (bool)m.IsActive && m.RCTerminalId == terminal.TermId && !(bool)m.IsAssigned && m.MerchantId == terminal.fk_MerchantID);
                if (!IsTerminalExists)
                {
                    response = 3;// Rapid Connect terminal is already assigned.
                    return response;
                }
            }

            bool macExist = DbMtData.Terminals.Any(a => a.MacAdd == terminal.MacAdd && a.IsActive);
            if (macExist)
            {
                return 9;//Mac Address is alreay exist
            }

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterSearch"></param>
        /// <returns></returns>
        public int GetUserTerminalCount(FilterSearchParameters filterSearch)
        {
            try
            {
                var results = DbMtData.Terminals.Where(t => DbMtData.Vehicles.Where(v => DbMtData.UserFleets.Where(x => x.fk_UserID == filterSearch.UserId).Select(x => x.fk_FleetID).Contains(v.fk_FleetID)).Select(x => x.fk_TerminalID).Contains(t.TerminalID) && t.fk_MerchantID == filterSearch.MerchantId && (bool)t.IsActive || !(bool)t.IsTerminalAssigned).ToList();
                IEnumerable<Terminal> query = (from m in results
                                               join n in DbMtData.PaymentTerminals on m.DeviceName equals n.PayTerminalId.ToString()
                                               join k in DbMtData.Datawires on m.DatawireId equals k.ID
                                               where m.IsActive && m.fk_MerchantID == filterSearch.MerchantId
                                               select new { m.MacAdd, n.DeviceName, m.SerialNo, m.TerminalID, k.RCTerminalId }).AsEnumerable().Select(objNew => new Terminal
                {

                    DeviceName = objNew.DeviceName,
                    MacAdd = objNew.MacAdd,
                    SerialNo = objNew.SerialNo,
                    TerminalID = objNew.TerminalID,
                    Description = objNew.RCTerminalId
                }).ToList();

                if (!string.IsNullOrEmpty(filterSearch.Name) && string.IsNullOrEmpty(filterSearch.RapidConnectID) && string.IsNullOrEmpty(filterSearch.SerialNumber))
                {
                    query = query.Where(p => p.DeviceName.ToUpper().IndexOf(filterSearch.Name.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                else if (!string.IsNullOrEmpty(filterSearch.RapidConnectID) && string.IsNullOrEmpty(filterSearch.Name) && string.IsNullOrEmpty(filterSearch.SerialNumber))
                {
                    query = query.Where(p => p.Description.ToUpper().IndexOf(filterSearch.RapidConnectID.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                else if (!string.IsNullOrEmpty(filterSearch.SerialNumber) && string.IsNullOrEmpty(filterSearch.Name) && string.IsNullOrEmpty(filterSearch.RapidConnectID))
                {
                    query = query.Where(p => p.SerialNo.ToUpper().IndexOf(filterSearch.SerialNumber.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                else if (!string.IsNullOrEmpty(filterSearch.Name) && !string.IsNullOrEmpty(filterSearch.RapidConnectID) && string.IsNullOrEmpty(filterSearch.SerialNumber))
                {
                    query = query.Where(p => p.DeviceName.ToUpper().IndexOf(filterSearch.Name.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0 && p.Description.ToUpper().IndexOf(filterSearch.RapidConnectID.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                else if (!string.IsNullOrEmpty(filterSearch.Name) && string.IsNullOrEmpty(filterSearch.RapidConnectID) && !string.IsNullOrEmpty(filterSearch.SerialNumber))
                {
                    query = query.Where(p => p.DeviceName.ToUpper().IndexOf(filterSearch.Name.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0 && p.SerialNo.ToUpper().IndexOf(filterSearch.SerialNumber.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                else if (string.IsNullOrEmpty(filterSearch.Name) && !string.IsNullOrEmpty(filterSearch.RapidConnectID) && !string.IsNullOrEmpty(filterSearch.SerialNumber))
                {
                    query = query.Where(p => p.SerialNo.ToUpper().IndexOf(filterSearch.SerialNumber.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0 && p.Description.ToUpper().IndexOf(filterSearch.RapidConnectID.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                else if (!string.IsNullOrEmpty(filterSearch.Name) && !string.IsNullOrEmpty(filterSearch.RapidConnectID) && !string.IsNullOrEmpty(filterSearch.SerialNumber))
                {
                    query = query.Where(p => p.DeviceName.ToUpper().IndexOf(filterSearch.Name.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0 && p.SerialNo.ToUpper().IndexOf(filterSearch.SerialNumber.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0 && p.Description.ToUpper().IndexOf(filterSearch.RapidConnectID.ToUpper(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                
                if (!string.IsNullOrEmpty(filterSearch.Name))
                {
                    query = query.Where(p => p.DeviceName.IndexOf(filterSearch.Name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }

                return query.Count();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fsParametes"></param>
        /// <returns></returns>
        public int GetTerminalCount(FilterSearchParameters fsParametes)
        {
            try
            {
                IEnumerable<Terminal> query = (from m in DbMtData.Terminals
                                               join n in DbMtData.PaymentTerminals on m.DeviceName equals n.PayTerminalId.ToString()
                                               join dwire in DbMtData.Datawires on m.DatawireId equals dwire.ID into g
                                               from rt in g.DefaultIfEmpty()
                                               where m.IsActive && m.fk_MerchantID == fsParametes.MerchantId
                                               select new { m.MacAdd, n.DeviceName, m.SerialNo, m.TerminalID, rt.RCTerminalId }).AsEnumerable().Select(objNew => new Terminal
                {
                    DeviceName = objNew.DeviceName,
                    MacAdd = objNew.MacAdd,
                    SerialNo = objNew.SerialNo,
                    TerminalID = objNew.TerminalID,
                    Description = objNew.RCTerminalId //Description representing RCTermainlID
                }).ToList();

                query = query.OrderBy(p => p.DeviceName); //Default!
                
                if (!string.IsNullOrEmpty(fsParametes.Name))
                    query = query.Where(p => p.DeviceName.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();

                if (!string.IsNullOrEmpty(fsParametes.RapidConnectID))
                    query = query.Where(p => p.Description.IndexOf(fsParametes.RapidConnectID, StringComparison.OrdinalIgnoreCase) >= 0).ToList();

                if (!string.IsNullOrEmpty(fsParametes.SerialNumber))
                    query = query.Where(p => p.SerialNo.IndexOf(fsParametes.SerialNumber, StringComparison.OrdinalIgnoreCase) >= 0).ToList();

                if (!string.IsNullOrEmpty(fsParametes.Name) && !string.IsNullOrEmpty(fsParametes.RapidConnectID) && !string.IsNullOrEmpty(fsParametes.SerialNumber))
                    query = query.Where(p => p.DeviceName.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0 && p.SerialNo.IndexOf(fsParametes.SerialNumber, StringComparison.OrdinalIgnoreCase) >= 0).ToList();

                return query.Count();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public int GetDeviceListCount(string name)
        {
            var deviceList = DbMtData.PaymentTerminals.Where(x => (bool)x.IsActive).ToList();
            if (!string.IsNullOrEmpty(name))
            {
                deviceList = deviceList.Where(p => p.DeviceName.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
            }
            return deviceList.Count();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="terminalId"></param>
        /// <param name="macAdd"></param>
        /// <returns></returns>
        public bool IsMacAddReg(int terminalId, string macAdd)
        {
            return DbMtData.Terminals.Any(m => m.MacAdd == macAdd && m.TerminalID != terminalId && (bool)m.IsActive);
        }

        /// <summary>
        /// Purpose             :   To update Cap Key version in terminal for device.
        /// Function Name       :   UpdateCapVersion
        /// Created By          :   Salil Gupta
        /// Created On          :   02-Feb-2016
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="macAdd"></param>
        /// <returns></returns>
        public bool UpdateCapVersion(string macAdd)
        {
            var termnl = DbMtData.Terminals.FirstOrDefault(o => o.MacAdd == macAdd && o.IsActive);
            if (termnl != null)
            {
                var capK = DbMtData.CapKeys.FirstOrDefault(o => o.IsNew == true);
                if (capK != null)
                {
                    termnl.FileVersion = capK.FileVersion;
                    DbMtData.CommitChanges();
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        public void SaveServiceDiscoveryUrls(ServiceDiscoveryParametersData serviceData)
        {
            XmlDocument xmlDoc=new XmlDocument();
            xmlDoc.LoadXml(serviceData.XmlResponse);
            Datawire datawire = new Datawire();
            datawire.DID = serviceData.DID;
            datawire.RCTerminalId = serviceData.TID;
            datawire.DatewireXml = xmlDoc.LastChild.InnerXml;
            datawire.MerchantId = serviceData.MerchantId;
            datawire.IsActive = true;
            datawire.IsAssigned = false;

            string email = (from m in DbMtData.Users where m.fk_MerchantID == serviceData.MerchantId && m.fk_UserTypeID == 2 && m.IsActive select m.Email).FirstOrDefault();
            
            datawire.CreatedBy = email;
            datawire.CreatedDate = DateTime.Now;
            
            var serviceDiscoveryDeleted = DbMtData.ServiceDiscoveryProviders.Where(m => m.IsServiceDiscovery == true).ToList();
            DbMtData.ServiceDiscoveryProviders.RemoveRange(serviceDiscoveryDeleted);
            ServiceDiscoveryProvider serviceDiscoveryProvider;
            List<ServiceDiscoveryProvider> serviceList = new List<ServiceDiscoveryProvider>();

            foreach (string item in serviceData.DiscoveryUrls)
            {
                serviceDiscoveryProvider = new ServiceDiscoveryProvider();
                serviceDiscoveryProvider.IsServiceDiscovery = true;
                serviceDiscoveryProvider.Url = item;
                serviceList.Add(serviceDiscoveryProvider);
            }

            DbMtData.ServiceDiscoveryProviders.AddRange(serviceList.AsEnumerable());
            DbMtData.Datawires.Add(datawire);
            DbMtData.CommitChanges();
        }

        public List<string> GetServiceDiscoveryUrls()
        {
            var discoveryUrls = (from m in DbMtData.ServiceDiscoveryProviders where m.IsServiceDiscovery == true select m.Url).ToList();
            return discoveryUrls;
        }

        public Datawire GetRandomMerchantDetail()
        {
            var randomMerchant = (from m in DbMtData.Datawires join n in DbMtData.MTDTransactions on m.RCTerminalId equals n.TerminalId orderby n.Id descending select m).FirstOrDefault();

            if (randomMerchant != null)
                return randomMerchant;
            else
            {
                var randomDatawireDetail = (from m in DbMtData.Datawires where m.IsActive orderby m.ID descending select m).FirstOrDefault();
                return randomDatawireDetail;
            }
        }

        public FDMerchant GetFDMerchant(int merchantId)
        {
            var fdMerchant = (from fdmerchant in DbMtData.FDMerchants
                              join merchant in DbMtData.Merchants on fdmerchant.FDId equals merchant.Fk_FDId into result
                              from resTemp in result
                              where resTemp.MerchantID == merchantId
                              select fdmerchant).FirstOrDefault();
            return fdMerchant;
        }

        public List<string> SaveServiceProviderUrls(ServiceProviderUrlData serviceProviderUrls)
        {
            var serviceProviderDeleted = DbMtData.ServiceDiscoveryProviders.Where(m => m.IsServiceDiscovery == false).ToList();
            DbMtData.ServiceDiscoveryProviders.RemoveRange(serviceProviderDeleted);
            DbMtData.CommitChanges();

            ServiceDiscoveryProvider serviceDiscoveryProvider;
            List<ServiceDiscoveryProvider> serviceList = new List<ServiceDiscoveryProvider>();
            
            foreach (DatawireServiceUrlData item in serviceProviderUrls.ServiceProviderUrls)
            {
                serviceDiscoveryProvider = new ServiceDiscoveryProvider();
                serviceDiscoveryProvider.IsServiceDiscovery = false;
                serviceDiscoveryProvider.TransactionTime = item.TransactionTime.ToString();
                serviceDiscoveryProvider.Url = item.Url;
                serviceList.Add(serviceDiscoveryProvider);
            }

            DbMtData.ServiceDiscoveryProviders.AddRange(serviceList.AsEnumerable());
            DbMtData.CommitChanges();

            var activeUrlData = (from m in DbMtData.ServiceDiscoveryProviders where m.IsServiceDiscovery == false && m.Url == serviceProviderUrls.ActiveUrl select m).FirstOrDefault();
            if (activeUrlData != null)

            activeUrlData.IsSetActive = true;
            DbMtData.CommitChanges();

            var urls = (from m in DbMtData.ServiceDiscoveryProviders where m.IsServiceDiscovery == false select m.Url).ToList();
            return urls;
        }

        public bool IsClsCapKey(int terminalId)
        {
            bool? isCls = (from m in DbMtData.Terminals
                           where m.TerminalID == terminalId
                           select m.IsContactLess).FirstOrDefault();

            return (isCls == null) ? false : true;
        }
    }
}

