﻿namespace MTD.Core.Factory.Interface
{
   public interface ISystemSetting
    {
       string SiteTitle { get; set; }
       string RootUrl { get; set; }
    }
}
