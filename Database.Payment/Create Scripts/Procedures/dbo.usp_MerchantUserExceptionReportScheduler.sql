IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MerchantUserExceptionReportScheduler]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_MerchantUserExceptionReportScheduler]
GO

CREATE PROCEDURE dbo.usp_MerchantUserExceptionReportScheduler
(
	@IsOnDemandReport BIT,
	@FromDate DATETIME = NULL,
	@ToDate DATETIME = NULL,
	@MerchantUserId INT = null
)
AS
BEGIN
	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SET NOCOUNT ON
		SET ANSI_WARNINGS OFF

		DECLARE	@Res INT,
				@MerchantId INT,
				@To VARCHAR(50),
				@Body NVARCHAR(MAX),
				@Error VARCHAR(200),
				@MailId INT,
				@Valid INT,
				@MsgXml NVARCHAR(40),
				@XmlData NVARCHAR(MAX),
				@MerchantName VARCHAR(50),
				@Company VARCHAR(100),   
				@ReportType INT,
				@RepType VARCHAR(50),
				@RepHeading VARCHAR(200),
				@FDate DATETIME,
				@TDate DATETIME,
				@NextScheduleDateTime DATETIME,
				@ReportId INT,
				@Result INT

		DECLARE @CursorData TABLE (
				MerchantId INT,
				Email NVARCHAR(80),
				Company NVARCHAR(80),
				ReportId  INT NULL
			)
		
		IF (@IsOnDemandReport = 1)
		BEGIN
			INSERT INTO @CursorData(MerchantId, Email, Company)			
			SELECT DISTINCT m.MerchantID, u.Email, m.Company
			FROM dbo.Merchant m 
				INNER JOIN dbo.recipient r ON m.MerchantID = r.fk_MerchantID 
				INNER JOIN dbo.Users u ON m.MerchantID = u.fk_MerchantID 
			WHERE r.IsRecipient = 1
			  AND r.fk_UserTypeId = 4
			  AND r.fk_ReportId = 3
			  AND m.IsActive = 1 
			  AND u.fk_UserTypeID = 4 
			  AND u.IsActive = 1
		END
		ELSE
		BEGIN
			SELECT @NextScheduleDateTime = NextScheduleDateTime 
			FROM dbo.ScheduleReport 
			WHERE fk_MerchantId IS NULL 
			  AND fk_ReportID = 3 
			  AND IsCreated = 1

			INSERT INTO @CursorData (MerchantId, Email, Company, ReportId)
			SELECT DISTINCT m.MerchantID, u.Email, m.Company, NULLIF(3, sc.fk_ReportID)
			FROM dbo.Merchant m 
				INNER JOIN dbo.recipient r ON m.MerchantID = r.fk_MerchantID 
				INNER JOIN dbo.Users u ON m.MerchantID = u.fk_MerchantID 
				LEFT OUTER JOIN dbo.ScheduleReport sc ON sc.fk_MerchantId = m.MerchantID
			WHERE r.IsRecipient = 1 
			  AND r.fk_UserTypeId = 4 
			  AND r.fk_ReportId = 3
			  AND m.IsActive = 1 
			  AND u.fk_UserTypeID = 4 
			  AND ((sc.fk_ReportID = 3 AND sc.IsCreated = 1 AND sc.NextScheduleDateTime >= DATEADD(SECOND, -900, GETDATE()) AND sc.NextScheduleDateTime < GETDATE())
					OR (sc.fk_ReportID IS NULL AND @NextScheduleDateTime >= DATEADD(SECOND, -900, GETDATE()) AND @NextScheduleDateTime < GETDATE())
					OR (sc.fk_ReportID IN (1, 2) AND 1 <> (SELECT COUNT(1) FROM dbo.ScheduleReport WHERE fk_MerchantId = sc.fk_MerchantId AND IsCreated = 1 AND fk_ReportID = 3) 
			  AND @NextScheduleDateTime >= DATEADD(SECOND, -900, GETDATE()) AND @NextScheduleDateTime < GETDATE()))
		END

		--Declaring cursor to fetch merchant recipients for exception report one by one
		DECLARE Queue_Cursor CURSOR FOR SELECT MerchantId, Email, Company, ReportId FROM @CursorData
		OPEN Queue_Cursor

		FETCH NEXT FROM Queue_Cursor INTO @MerchantId, @To, @Company, @ReportId
		WHILE @@FETCH_STATUS = 0   
		BEGIN   
			SELECT @MerchantName = FName 
			FROM dbo.Users 
			WHERE fk_MerchantID = @MerchantId 
			  AND fk_UserTypeID = 4 
			  AND IsActive = 1

			--Executing store procedure MerchantExceptionReport to get report data in xml format
			EXEC @Result = dbo.usp_MerchantUserExceptionReport @IsOnDemandReport, @FromDate, @ToDate, @MerchantUserId, @ReportId, @XmlData OUTPUT, @ReportType OUTPUT, @FDate OUTPUT, @TDate OUTPUT
			IF (@Result <> 0)
			BEGIN
				SET @Valid = 0
			END

			IF (@IsOnDemandReport = 1)
			BEGIN
				SET @RepType = 'On Demand Exception Report'
			END
			ELSE
			BEGIN
				SET @RepType = CASE @ReportType 
									WHEN 1 THEN 'Daily Exception Report'
									WHEN 2 THEN 'Weekly Exception Report'
									ELSE 'Monthly Exception Report' 
								END
			END
      
			SET @RepHeading = @RepType + ' ' + '(Dated from' + ' ' + CONVERT(VARCHAR(11), @FDate, 106) + ' ' + 'to' + ' ' + CONVERT(VARCHAR(11), @TDate, 106) + ')'
			IF (@XmlData IS NULL OR @XmlData = '') 
			BEGIN
				SET @Valid = 0

				INSERT INTO MailMessage (RepUserId, [Status], SendTo, Body, RepUserTypeId, [Date], ReportType) 
				VALUES (@MerchantId, -1, @To, 'No records in database', 2, GETDATE(), 'MerchantUserExceptionReport')
			END
			ELSE 
			BEGIN
				SET @Valid = 1
			END

			IF (@Valid = 1) 
			BEGIN
				--Set data in html format
				SET @Body = '<html>
							 <meta http-equiv = "Content-Type" content = "text/html; charset = utf-8" />
							 <style>td{text-align:left;font-family:verdana;font-size:12px}
							 p{font-family:verdana;font-size:13px}span{font-family:verdana;font-size:13px}</style>
							 <body><table style = "background-color:#e0e0e0; padding:20px; height:100%;"><tr><td><p style = "font-size:11px;font-weight:bold">' + CONCAT('Dear', ' ', @MerchantName, ',') + '</p>
							 <p style = "font-size:11px;font-weight:bold">Listed below is the exception report containing exception details corresponding to fleets.</p>
							 <H5 style = "color:#003366;text-align:center;font-size:11px;font-family:verdana; padding:10px"> Merchant  :  ' + @Company + '</H5>
							 <h5 style = "color:#003366;text-align:center;font-size:11px;font-family:verdana; padding:10px;">' + @RepHeading + '</h5>
							 <table border = 1 cellpadding = "0" cellspacing = "0" style = "background-color:#aed6eb; border:2px solid #003366; border-collapse:collapse; padding:2px;">
							 <thead style = "background-color:#2b79c7; padding:10px; "><tr>
							<th style = "color:grey;font-size:10px;width:500px;text-align:left;font-family:verdana; color:#fff; padding:5px;">Fleet</th>
							<th style = "color:grey;font-size:10px;width:500px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Car Number</th>
							<th style = "color:grey;font-size:10px;width:500px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Fare Amt</th>
							<th style = "color:grey;font-size:10px;width:500px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Tip</th>
							<th style = "color:grey;font-size:10px;width:500px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Surcharge</th>
							<th style = "color:grey;font-size:10px;width:500px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Tech Fee</th>
							<th style = "color:grey;font-size:10px;width:500px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Amount</th>
							<th style = "color:grey;font-size:10px;width:500px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Fees</th>
							<th style = "color:grey;font-size:10px;width:200px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Amount Owing</th></tr></thead>'    

				 SET @Body = @Body + @XmlData +'</table><br/><br/><span>Thanks & Regards,
												<br/><br/>MTData, LLC.<br/>www.mtdata.us<span>
												<h4 style = "color:grey;font-size:12px;font-family:verdana">MTData-All rights reserved</h4>
												<img src = "D:\logo-1.png" width = "138" height = "44" alt = "logo">
												<p style = "font-size:11px;color:black">Note:This is an automated message. Do not Reply or Reply All to this message 
												as replies are not being accepted.</p></body></html>'

				INSERT INTO dbo.MailMessage(RepUserId, [Status], SendTo, Body, RepUserTypeId, [Date], ReportType) 
				VALUES (@MerchantId, 0, @To, @Body, 2, GETDATE(), 'MerchantUserExceptionReport')

				SET @MailId = SCOPE_IDENTITY()

				--Exceute store procedure to send exception report to corresponding merchant
				EXEC @Res = msdb.dbo.sp_send_dbmail @profile_name = 'MTData', @recipients = @To, @body  = @Body, @subject = 'Merchant User Exception Report', @body_format = 'HTML'
				IF (@Res !=  0)
				BEGIN
					SET @Error  = @@ERROR
					UPDATE dbo.MailMessage SET Error = @Error, [Date] = GETDATE() WHERE MailId = @MailId
				END
				ELSE
				BEGIN
					UPDATE dbo.MailMessage SET [Status] = 1, [Date] = GETDATE() WHERE MailId = @MailId
				END
			END

			FETCH NEXT FROM Queue_Cursor INTO @MerchantId, @To, @Company, @ReportId
		END   
		CLOSE Queue_Cursor   
		DEALLOCATE Queue_Cursor

		SELECT 0
	END TRY
	BEGIN CATCH
		INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
		SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
		SELECT 1
	END CATCH
END
GO
