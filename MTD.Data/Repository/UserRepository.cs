﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Data.Repository
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public UserRepository(IMtDataDevlopmentsEntities dbMtData) : base(dbMtData)
        {
        }

        /// <summary>
        /// Purpose             :   to encode data
        /// Function Name       :   base64Encode
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/19/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string Base64Encode(string data)
        {
            try
            {
                byte[] encDataByte = System.Text.Encoding.UTF8.GetBytes(data);
                var encodedData = Convert.ToBase64String(encDataByte);
                return encodedData;
            }
            catch (Exception e)
            {
                throw new Exception("Error in base64Encode" + e.Message);
            }
        }

        /// <summary>
        /// Purpose             :   to add user
        /// Function Name       :   Add
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/20/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool Add(User user)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    var userDb = new User
                    {
                        FName = user.FName,
                        LName = user.LName,
                        Email = user.Email,
                        Phone = user.Phone,
                        fk_Country = user.fk_Country,
                        fk_State = user.fk_State,
                        fk_City = user.fk_City,
                        Address = user.Address,
                        PCode = user.PCode,
                        fk_MerchantID = user.fk_MerchantID,
                        fk_UserTypeID = user.fk_UserTypeID,
                        IsLockedOut = user.IsLockedOut,
                        IsActive = Convert.ToBoolean(1),
                        CreatedBy = user.CreatedBy,
                        CreatedDate = DateTime.Now,
                        ModifiedBy = user.ModifiedBy,
                        ModifiedDate = DateTime.Now,
                        IsFirstLogin = true,
                        RolesAssigned = user.RolesAssigned,
                        PCodeExpDate = DateTime.Now.AddDays(90)
                    };

                    DbMtData.Users.Add(userDb);
                    DbMtData.CommitChanges();
                    var roleList = user.RolesAssigned.Split(',');
                    var roles = new UserRole[roleList.Length];

                    for (int i = 0; i < roleList.Length; i++)
                    {
                        roles[i] = new UserRole();
                        roles[i].fk_UserID = userDb.UserID;
                        roles[i].fk_RoleID = Convert.ToInt32(roleList[i]);
                        roles[i].IsActive = true;
                        roles[i].CreatedDate = DateTime.Now;
                        roles[i].CreatedBy = userDb.CreatedBy;
                        DbMtData.UserRoles.Add(roles[i]);
                    }

                    var report = DbMtData.Reports.Where(x => x.ReportId < 4).ToList();
                    report = report.Concat(DbMtData.Reports.Where(x => x.ReportId > 6 && x.ReportId < 9)).ToList();
                    Recipient[] recObj = new Recipient[report.Count()];
                    int count = 0;

                    foreach (var item in report)
                    {
                        recObj[count] = new Recipient();
                        recObj[count].fk_MerchantID = userDb.fk_MerchantID;
                        recObj[count].IsRecipient = true;
                        recObj[count].fk_UserID = userDb.UserID;
                        recObj[count].fk_UserTypeID = userDb.fk_UserTypeID;
                        recObj[count].fk_ReportID = item.ReportId;
                        recObj[count].ModifiedBy = user.ModifiedBy;
                        recObj[count].ModifiedDate = DateTime.Now; ;
                        DbMtData.Recipients.Add(recObj[count]);
                        count++;
                    }

                    DbMtData.CommitChanges();
                    var fleetList = user.fleetListId.Split(',');
                    var userFleetDb = new UserFleet[fleetList.Length];

                    for (int i = 0; i < fleetList.Length; i++)
                    {
                        if ("multiselect-all" != fleetList[i] && fleetList[i] != "-1")
                        {
                            userFleetDb[i] = new UserFleet
                            {
                                fk_UserID = userDb.UserID,
                                fk_FleetID = Convert.ToInt32(fleetList[i]),
                                IsActive = true,
                                fk_MerchantId = userDb.fk_MerchantID,
                                fk_UserTypeId = userDb.fk_UserTypeID
                            };

                            DbMtData.UserFleets.Add(userFleetDb[i]);
                        }
                    }

                    DbMtData.CommitChanges();
                    transactionScope.Complete();

                    return true;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Purpose             :   to get the users role
        /// Function Name       :   GetUsersRole
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/20/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<Role> GetUsersRole(int userId)
        {
            try
            {
                IEnumerable<Role> userRoles =
                    (from role in DbMtData.Roles
                     join uRoles in DbMtData.UserRoles on role.RoleID equals uRoles.fk_RoleID into result
                     from resTemp in result
                     where resTemp.fk_UserID == userId

                     select new { role.RoleName, role.RoleID }).AsEnumerable()
                        .Select(objNew =>
                            new Role
                            {
                                RoleID = objNew.RoleID,
                                RoleName = objNew.RoleName

                            }).ToList();

                return userRoles;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   to get the list of users's fleet
        /// Function Name       :   GetUsersFleet
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/22/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<UserFleet> GetUsersFleet(int userId)
        {
            try
            {
                IEnumerable<UserFleet> getFleet =
                    (from fleet in DbMtData.UserFleets
                     where fleet.fk_UserID == userId

                     select new { fleet.fk_FleetID, fleet.fk_UserID }).AsEnumerable()
                        .Select(objNew =>
                            new UserFleet
                            {
                                fk_FleetID = objNew.fk_FleetID,
                                fk_UserID = objNew.fk_UserID

                            }).ToList();

                return getFleet;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   to get the list of users
        /// Function Name       :   UserList
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/22/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<User> UserList(int userId, string name, int logOnByUserId, int logOnByUserType)
        {
            try
            {
                IEnumerable<User> usersList = (from users in DbMtData.Users
                                               join countries in DbMtData.CountryCodes on users.fk_Country equals countries.CountryCodesID
                                               select new { Users = users, CountryCodes = countries }).AsEnumerable().Select(objNew =>

                new User
                {
                    FName = objNew.Users.FName,
                    LName = objNew.Users.LName,
                    Email = objNew.Users.Email,
                    Address = objNew.Users.Address,
                    GridCity = objNew.Users.fk_City,
                    GridState = objNew.Users.fk_State,
                    fk_Country = objNew.CountryCodes.Title,
                    Phone = objNew.Users.Phone,
                    IsActive = objNew.Users.IsActive,
                    IsLockedOut = objNew.Users.IsLockedOut,
                    UserID = objNew.Users.UserID,
                    fk_UserTypeID = objNew.Users.fk_UserTypeID,
                    fk_MerchantID = objNew.Users.fk_MerchantID
                }).ToList();

                if (logOnByUserType != 5)
                {
                    usersList = from m in usersList where ((m.fk_UserTypeID == 3 || m.fk_UserTypeID == 4) && m.fk_MerchantID == userId && m.IsActive) select m;
                }
                else
                {
                    usersList = from m in usersList where ((m.fk_UserTypeID == 4) && m.fk_MerchantID == userId && m.IsActive) select m;
                    List<User> AccessibleUserList = new List<User>();
                    List<int> fleetAccessed = GetAccesibleFleet(logOnByUserId, userId);
                    List<int> merchantUserId = GetAccesibleUserFleet(fleetAccessed);

                    foreach (User user in usersList)
                    {
                        if (merchantUserId.Contains(user.UserID))
                            AccessibleUserList.Add(user);
                    }

                    usersList = AccessibleUserList;
                }

                if (!string.IsNullOrEmpty(name))
                    usersList = usersList.Where(p => p.FName.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();

                return usersList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   to get the users
        /// Function Name       :   GetUser
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/23/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public User GetUserUpdate(int userId, int merchantId)
        {
            try
            {
                User userLi = (from users in DbMtData.Users
                               where users.UserID == userId
                               select users).SingleOrDefault();

                if (userLi != null)
                {
                    var countryName = (from m in DbMtData.CountryCodes where m.CountryCodesID == userLi.fk_Country select m.Title).FirstOrDefault();

                    if (userLi != null)
                    {
                        userLi.CountryName = countryName;

                        User userDb = (from user in DbMtData.Users where user.UserID == userId && user.fk_MerchantID == merchantId select user).FirstOrDefault();
                        if (userDb != null)
                        {
                            var role = (from roles in DbMtData.UserRoles
                                        where roles.fk_UserID == userId
                                        select new { roles.fk_RoleID }).ToList();

                            string temp = "";
                            for (int i = 0; i < role.Count; i++)
                            {
                                if (i == role.Count - 1)
                                    temp = temp + role[i].fk_RoleID;
                                else
                                    temp = temp + role[i].fk_RoleID + ",";
                            }

                            userDb.RolesAssigned = temp;
                        }

                        return userDb;
                    }
                }
            }
            catch
            {
                throw;
            }

            return null;
        }

        /// <summary>
        /// Purpose             :   to update the users
        /// Function Name       :   Update
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/24/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="user"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool Update(User user, int userId)
        {
            //if (userId == user.UserID)
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    var data = DbMtData.Users.Find(userId); ;
                    if (data != null)
                    {
                        data.FName = user.FName;
                        data.LName = user.LName;
                        data.Email = user.Email;
                        data.Phone = user.Phone;
                        data.Address = user.Address;
                        data.fk_City = user.fk_City;
                        data.fk_State = user.fk_State;
                        data.fk_Country = user.fk_Country;
                        data.RolesAssigned = user.RolesAssigned;
                        data.IsLockedOut = user.IsLockedOut;
                        data.fk_UserTypeID = user.fk_UserTypeID;
                        
                        //updating roles to the database

                        DbMtData.CommitChanges();

                        //Finding all records to delete
                        var deleteRecords = from tempRecords in DbMtData.UserRoles
                                            where tempRecords.fk_UserID == data.UserID
                                            select tempRecords;

                        foreach (var item in deleteRecords)
                        {
                            DbMtData.UserRoles.Remove(item);
                        }

                        //updating records on the basis of roleId
                        string[] roleList = data.RolesAssigned.Split(',');
                        var roles = new UserRole[roleList.Length];
                        for (int i = 0; i < roleList.Length; i++)
                        {
                            roles[i] = new UserRole();
                            roles[i].fk_UserID = data.UserID;
                            roles[i].fk_RoleID = Convert.ToInt32(roleList[i]);
                            roles[i].IsActive = true;
                            roles[i].CreatedDate = DateTime.Now;
                            roles[i].CreatedBy = data.CreatedBy;
                            DbMtData.UserRoles.Add(roles[i]);
                        }

                        DbMtData.CommitChanges();

                        //End 

                        //Implementing update for fleet of users
                        //Finding all records to delete
                        var deleteFleets = from tempFleet in DbMtData.UserFleets
                                           where tempFleet.fk_UserID == data.UserID
                                           select tempFleet;

                        foreach (var item in deleteFleets)
                        {
                            DbMtData.UserFleets.Remove(item);
                        }

                        var fleetList = user.fleetListId.Split(',');
                        var userFleetDb = new UserFleet[fleetList.Length];

                        for (int i = 0; i < fleetList.Length; i++)
                        {
                            if ("multiselect-all" != fleetList[i] && fleetList[i] != "-1")
                            {
                                userFleetDb[i] = new UserFleet();
                                userFleetDb[i].fk_UserID = data.UserID;
                                userFleetDb[i].fk_FleetID = Convert.ToInt32(fleetList[i]);
                                userFleetDb[i].IsActive = true;
                                userFleetDb[i].fk_MerchantId = data.fk_MerchantID;
                                userFleetDb[i].fk_UserTypeId = data.fk_UserTypeID;
                                DbMtData.UserFleets.Add(userFleetDb[i]);
                            }
                        }

                        DbMtData.CommitChanges();
                        transactionScope.Complete();

                        return true;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return true;
        }

        /// <summary>
        /// Purpose             :   To delete the users
        /// Function Name       :   DeleteUser
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/24/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        public void DeleteUser(int userId)
        {
            try
            {
                var data = DbMtData.Users.Find(userId); ;
                if (data != null) data.IsActive = false;
                var tempFleet = DbMtData.UserFleets.Where(x => x.fk_UserID == userId).ToList();

                if (tempFleet != null)
                    tempFleet.ForEach(x => x.IsActive = false);

                DbMtData.CommitChanges();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   to get the list of fleet
        /// Function Name       :   FleetList
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/25/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<Fleet> FleetList(int merchantId)
        {
            try
            {
                IEnumerable<Fleet> fleetList =
                    (from fleet in DbMtData.Fleets where fleet.fk_MerchantID == merchantId && fleet.IsActive select new { fleet.FleetID, fleet.FleetName }).AsEnumerable()
                        .Select(objNew =>
                            new Fleet
                            {
                                FleetID = objNew.FleetID,
                                FleetName = objNew.FleetName

                            }).ToList().OrderBy(x => x.FleetName);

                return fleetList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To update the users profile
        /// Function Name       :   UpdateUserProfile
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/26/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="user"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool UpdateUserProfile(User user, int userId)
        {
            if (userId == user.UserID)
            {
                try
                {
                    var data = DbMtData.Users.Find(userId);
                    if (data != null)
                    {
                        data.FName = user.FName;
                        data.LName = user.LName;
                        data.Email = user.Email;
                        data.Phone = user.Phone;
                        data.Address = user.Address;
                        data.fk_City = user.fk_City;
                        data.fk_State = user.fk_State;
                        data.fk_Country = user.fk_Country;
                        data.IsLockedOut = user.IsLockedOut;
                        data.RolesAssigned = user.RolesAssigned;
                        data.ModifiedDate = DateTime.Now;
                        data.ModifiedBy = user.ModifiedBy;
                    }
                }
                catch
                {
                    throw;
                }
            }

            DbMtData.CommitChanges();

            return false;
        }

        /// <summary>
        /// Purpose             :   To get the users
        /// Function Name       :   GetUser
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/29/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public User GetUser(int userId, int val)
        {
            try
            {
                var userDb = DbMtData.Users.FirstOrDefault(a => a.fk_MerchantID == userId && a.fk_UserTypeID == val);
                return userDb;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To filter the users list
        /// Function Name       :   UserListByFilter
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/29/2014
        /// Modified By         :   Sunil Singh 
        /// Created On          :   06/25/2015 
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fsParametes"></param>
        /// <returns></returns>
        public IEnumerable<User> UserListByFilter(FilterSearchParameters fsParametes, int logOnByUserId, int logOnByUserType)
        {
            try
            {
                //Get users list
                var userList = GetUserList(fsParametes, logOnByUserId, logOnByUserType);

                //Get sorted list of users
                userList = GetSortedUser(userList, fsParametes);

                List<User> mUsers = new List<User>();
                foreach (var mu in userList)
                {
                    mu.Address = mu.Address.Replace(", , ,", ", ");
                    mu.Address = mu.Address.Replace(", ,", ", ");
                    mUsers.Add(mu);
                }

                return fsParametes.PageSize > 0
                          ? userList.Skip(fsParametes.StartIndex).Take(fsParametes.PageSize).ToList() //Paging
                          : userList.ToList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To filter the users list
        /// Function Name       :   GetUserList
        /// Created By          :   Sunil Singh 
        /// Created On          :   06/25/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fsParametes"></param>
        /// <returns></returns>
        public IEnumerable<User> GetUserList(FilterSearchParameters fsParametes, int logOnByUserId, int logOnByUserType)
        {
            try
            {
                IEnumerable<User> usersList = (from users in DbMtData.Users
                                               join countries in DbMtData.CountryCodes on users.fk_Country equals countries.CountryCodesID
                                               select new { Users = users, CountryCodes = countries }).AsEnumerable().Select(objNew =>
                                new User
                                {
                                    FName = objNew.Users.FName + " " + objNew.Users.LName,
                                    Email = objNew.Users.Email,
                                    Address = objNew.Users.Address + ", " + objNew.Users.fk_City + ", " + objNew.Users.fk_State + ", " + objNew.CountryCodes.Title,
                                    Phone = objNew.Users.Phone,
                                    IsActive = objNew.Users.IsActive,
                                    IsLockedOut = objNew.Users.IsLockedOut,
                                    UserID = objNew.Users.UserID,
                                    fk_UserTypeID = objNew.Users.fk_UserTypeID,
                                    fk_MerchantID = objNew.Users.fk_MerchantID,
                                    CreatedDate = objNew.Users.CreatedDate
                                }).ToList();

                if (logOnByUserType != 5)
                {
                    usersList = from m in usersList where ((m.fk_UserTypeID == 3 || m.fk_UserTypeID == 4) && m.fk_MerchantID == fsParametes.MerchantId && m.IsActive) select m;
                }
                else
                {
                    usersList = from m in usersList where ((m.fk_UserTypeID == 4) && m.fk_MerchantID == fsParametes.MerchantId && m.IsActive) select m;
                    List<User> AccessibleUserList = new List<User>();
                    List<int> fleetAccessed = GetAccesibleFleet(logOnByUserId, fsParametes.MerchantId);
                    List<int> merchantUserId = GetAccesibleUserFleet(fleetAccessed);

                    foreach (User user in usersList)
                    {
                        if (merchantUserId.Contains(user.UserID))
                            AccessibleUserList.Add(user);
                    }

                    usersList = AccessibleUserList;
                }

                return usersList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To sort users list
        /// Function Name       :   GetSortedUser
        /// Created By          :   Sunil Singh 
        /// Created On          :   06/25/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// /// </summary>
        /// <param name="userList"></param>
        /// <param name="fsParametes"></param>
        /// <returns></returns>
        public IEnumerable<User> GetSortedUser(IEnumerable<User> userList, FilterSearchParameters fsParametes)
        {
            if (!string.IsNullOrEmpty(fsParametes.Name))
                userList = userList.Where(p => p.FName.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0);

            if (!string.IsNullOrEmpty(fsParametes.Sorting))
            {
                switch (fsParametes.Sorting)
                {
                    case "FName ASC":
                        userList = userList.OrderBy(p => p.FName);
                        break;
                    case "FName DESC":
                        userList = userList.OrderByDescending(p => p.FName);
                        break;
                    case "LName ASC":
                        userList = userList.OrderBy(p => p.LName);
                        break;
                    case "LName DESC":
                        userList = userList.OrderByDescending(p => p.LName);
                        break;
                    case "Email ASC":
                        userList = userList.OrderBy(p => p.Email);
                        break;
                    case "Email DESC":
                        userList = userList.OrderByDescending(p => p.Email);
                        break;
                    case "CreatedDate ASC":
                        userList = userList.OrderBy(p => p.CreatedDate);
                        break;
                    case "CreatedDate DESC":
                        userList = userList.OrderByDescending(p => p.CreatedDate);
                        break;
                }
            }

            return userList;
        }

        /// <summary>
        /// Purpose             :   to get the users
        /// Function Name       :   GetUser
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/23/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public User GetUser(int userId)
        {
            try
            {
                User userLi = (from users in DbMtData.Users
                               where users.UserID == userId
                               select users).SingleOrDefault();

                if (userLi != null)
                {
                    var countryName = (from m in DbMtData.CountryCodes where m.CountryCodesID == userLi.fk_Country select m.Title).FirstOrDefault();

                    if (userLi != null)
                    {
                        userLi.CountryName = countryName;

                        var role = (from roles in DbMtData.UserRoles where roles.fk_UserID == userId select new { roles.fk_RoleID }).ToList();
                        string temp = "";

                        for (int i = 0; i < role.Count; i++)
                        {
                            if (i == role.Count - 1)
                                temp = temp + role[i].fk_RoleID;
                            else
                                temp = temp + role[i].fk_RoleID + ",";
                        }

                        userLi.RolesAssigned = temp;

                        return userLi;
                    }
                }
            }
            catch
            {
                throw;
            }

            return null;
        }

        /// <summary>
        /// Purpose             : for changing status for first login
        /// Function Name       :  ChangeStatus
        /// Created By          :  Asif Shafeeque
        /// Created On          :  8/17/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="UserId"></param>
        public void ChangeStatus(int UserId)
        {
            try
            {
                var data = DbMtData.Users.Find(UserId); ;
                if (data != null) data.IsFirstLogin = false;

                DbMtData.CommitChanges();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public IEnumerable<FleetList> UserFleetList(int UserId)
        {
            try
            {
                IEnumerable<FleetList> fleetLi = new List<FleetList>();
                fleetLi = (from fleet in DbMtData.Fleets
                           join ufleet in DbMtData.UserFleets on fleet.FleetID equals ufleet.fk_FleetID
                           where ufleet.fk_UserID == UserId && fleet.IsActive
                           select new { fleet }).Distinct().AsEnumerable().Select(objNew =>
                                               new FleetList { FleetName = objNew.fleet.FleetName, FleetID = objNew.fleet.FleetID }).ToList();

                return fleetLi;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public IEnumerable<Fleet> UserFleetLists(int UserId)
        {
            try
            {
                IEnumerable<Fleet> fleetLi = new List<Fleet>();
                fleetLi = (from fleet in DbMtData.Fleets
                           join ufleet in DbMtData.UserFleets on fleet.FleetID equals ufleet.fk_FleetID
                           where ufleet.fk_UserID == UserId && fleet.IsActive
                           select new { fleet }).Distinct().AsEnumerable().Select(objNew =>
                                               new Fleet { FleetName = objNew.fleet.FleetName, FleetID = objNew.fleet.FleetID }).ToList();

                return fleetLi;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   to get accessible fleets
        /// Function Name       :   GetAccesibleUserFleet
        /// Created By          :   Naveen Kumar 
        /// Created On          :   10/15/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public List<int> GetAccesibleUserFleet(List<int> fleets)
        {
            List<int> merchantUserList = DbMtData.UserFleets.Where(m => fleets.Contains((int)m.fk_FleetID) && m.fk_UserTypeId == 4 && m.IsActive == true).Select(a => (int)a.fk_UserID).Distinct().ToList();
            return merchantUserList;
        }

        /// <summary>
        /// Purpose             :   to get the accessible fleets 
        /// Function Name       :   GetAccesibleFleet
        /// Created By          :   Naveen Kumar
        /// Created On          :   10/15/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public List<int> GetAccesibleFleet(int logOnByUserId, int merchantId)
        {
            List<int> accessibleFleets = (from m in DbMtData.UserFleets
                                          where m.fk_UserID == logOnByUserId && m.fk_MerchantId == merchantId
                                          && m.fk_UserTypeId == 5 && m.IsActive == true
                                          select (int)m.fk_FleetID).Distinct().ToList();
            return accessibleFleets;
        }

        /// <summary>
        /// Purpose             :   to get the list of users's fleet based on Payer type
        /// Function Name       :   GetUsersPayerFleet
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/22/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<UserFleet> GetUsersPayerFleet(int userId, int PayerId)
        {
            try
            {
                IEnumerable<UserFleet> getFleet =
                    (from fleet in DbMtData.UserFleets
                     join flt in DbMtData.Fleets on fleet.fk_FleetID equals flt.FleetID
                     where fleet.fk_UserID == userId && flt.PayType == PayerId

                     select new { fleet.fk_FleetID, fleet.fk_UserID }).AsEnumerable()
                        .Select(objNew =>
                            new UserFleet
                            {
                                fk_FleetID = objNew.fk_FleetID,
                                fk_UserID = objNew.fk_UserID
                            }).ToList();

                return getFleet;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   to get the list of fleet Based on PayerType
        /// Function Name       :   PayerFleetList
        /// Created By          :   Shishir Saunakia 
        /// Created On          :   10/15/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<Fleet> PayerFleetList(int PayerId, int merchantId)
        {
            try
            {
                IEnumerable<Fleet> fleetList =
                    (from fleet in DbMtData.Fleets where fleet.PayType == PayerId && fleet.fk_MerchantID == merchantId && fleet.IsActive select new { fleet.FleetID, fleet.FleetName }).AsEnumerable()
                        .Select(objNew =>
                            new Fleet
                            {
                                FleetID = objNew.FleetID,
                                FleetName = objNew.FleetName
                            }).ToList().OrderBy(x => x.FleetName);

                return fleetList;
            }
            catch
            {
                throw;
            }
        }

    }
}
