﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
    public class TAKeyDto
    {
        public int Id { get; set; }
        public Nullable<System.DateTime> DownloadDate { get; set; }
        public Nullable<decimal> TAFileVersion { get; set; }
        public Nullable<bool> IsNew { get; set; }
       
    }
}
