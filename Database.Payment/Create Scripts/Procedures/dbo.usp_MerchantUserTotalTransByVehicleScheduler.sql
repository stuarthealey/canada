IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MerchantUserTotalTransByVehicleScheduler]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_MerchantUserTotalTransByVehicleScheduler]
GO

CREATE PROCEDURE dbo.usp_MerchantUserTotalTransByVehicleScheduler
(
	@IsOnDemandReport BIT,
	@FromDate DATETIME = NULL,
	@ToDate DATETIME = NULL,
	@ParentMerchantId INT = NULL,
	@MerchantUserId INT = NULL
)
AS
BEGIN
	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SET NOCOUNT ON
		SET ANSI_WARNINGS OFF

		DECLARE @Res INT,
				@MerchantId INT,
				@To VARCHAR(50),
				@Body NVARCHAR(MAX),
				@Error VARCHAR(200),
				@MailId INT,
				@Valid INT,
				@MsgXml NVARCHAR(MAX),
				@XmlData NVARCHAR(MAX),
				@UserName NVARCHAR(50),
				@Company NVARCHAR(100),
				@ReportType INT,
				@RepType VARCHAR(50),
				@RepHeading VARCHAR(200),
				@FDate DATETIME,
				@TDate DATETIME,
				@TotalTransaction INT,
				@TotalActualFare NVARCHAR(30),
				@TotalTips NVARCHAR(30),
				@TotalFareAmount NVARCHAR(30),
				@TotalTechFee NVARCHAR(30),
				@TotalSurcharge NVARCHAR(30),
				@TotalFee   NVARCHAR(30),
				@NoChargeBack INT,
				@ChargeBackValue NVARCHAR(25),
				@NoVoid INT,
				@VoidValue NVARCHAR(25),
				@AmountOwing  NVARCHAR(30),
				@NextScheduleDateTime DATETIME,
				@ReportId INT,
				@Slab1Message NVARCHAR(40),
				@Slab2Message NVARCHAR(40),
				@Slab3Message NVARCHAR(40),
				@Slab4Message NVARCHAR(40),
				@TotalSlab1 INT,
				@TotalSlab2 INT,
				@TotalSlab3 INT,
				@TotalSlab4 INT,
				@Result INT

		DECLARE @CursorData TABLE (
			MerchantUserId INT,
			Email NVARCHAR(80),
			Company NVARCHAR(80),
			ReportId INT NULL,
			UserName NVARCHAR(100)
			)

		IF (@IsOnDemandReport = 1 AND @MerchantUserId IS NOT NULL)
		BEGIN
			INSERT INTO @CursorData (MerchantUserId, Email, Company, UserName)
			SELECT DISTINCT u.UserID, u.Email, m.Company, U.FName
			FROM dbo.Users u 
				INNER JOIN dbo.Merchant m ON u.fk_MerchantID = m.MerchantID 
				INNER JOIN dbo.Recipient r ON u.UserID = r.fk_UserID 
			where u.IsActive = 1
			  AND u.fk_UserTypeID = 4
			  AND m.IsActive = 1
			  AND r.IsRecipient = 1
			  AND r.fk_UserTypeID = 4 
			  AND r.fk_ReportID = 7 
			  AND r.fk_UserID = @MerchantUserId
		END
		ELSE IF (@ParentMerchantId IS NOT NULL AND @IsOnDemandReport = 1 AND @MerchantUserId IS NULL)
		BEGIN
			INSERT INTO @CursorData (MerchantUserId, Email, Company, UserName)
			SELECT DISTINCT u.UserID, u.Email, m.Company, U.FName
			FROM dbo.Users u 
				inner join dbo.Merchant m ON u.fk_MerchantID = m.MerchantID 
				inner join dbo.Recipient r ON u.UserID = r.fk_UserID 
			where u.IsActive = 1
			  AND u.fk_UserTypeID = 4
			  AND m.IsActive = 1
			  AND r.IsRecipient = 1
			  AND r.fk_UserTypeID = 4 
			  AND r.fk_ReportID = 7
		END
		ELSE
		BEGIN
			SELECT @MerchantId = fk_MerchantID 
			FROM dbo.Users 
			WHERE UserID = @MerchantUserId 
			  AND IsActive = 1
			  AND fk_UserTypeID = 4

			SELECT @NextScheduleDateTime = NextScheduleDateTime 
			FROM dbo.ScheduleReport 
			WHERE fk_MerchantId IS NULL 
			  AND CorporateUserId IS NULL 
			  AND fk_ReportID = 7 
			  AND IsCreated = 1

			INSERT INTO @CursorData (MerchantUserId, Email, Company, ReportId, UserName)
			SELECT DISTINCT u.UserID, u.Email, m.Company, NULLIF(7, sc.fk_ReportID), u.FName
			FROM dbo.Users u 
				INNER JOIN dbo.Merchant m ON u.fk_MerchantID = m.MerchantID
				INNER JOIN dbo.Recipient r ON u.UserID = r.fk_UserID
				LEFT OUTER JOIN dbo.ScheduleReport sc ON sc.fk_MerchantId = m.MerchantID
			WHERE u.IsActive = 1 
			  AND u.fk_UserTypeID = 4 
			  AND m.IsActive = 1 
			  AND r.IsRecipient = 1 
			  AND r.fk_UserTypeID = 4 
			  AND r.fk_ReportID = 7
			  AND m.MerchantID = @MerchantId
			  AND ((sc.fk_ReportID = 7 AND sc.IsCreated = 1 AND sc.NextScheduleDateTime >= DATEADD(SECOND, -900, GETDATE()) AND sc.NextScheduleDateTime < GETDATE())
					OR (sc.fk_ReportID IS NULL AND @NextScheduleDateTime >= DATEADD(SECOND, -900, GETDATE()) AND @NextScheduleDateTime < GETDATE())
					OR (sc.fk_ReportID IN (1, 2, 3, 8) AND 1 <> (SELECT COUNT(1) FROM dbo.ScheduleReport WHERE fk_MerchantId = @MerchantId AND IsCreated = 1 AND fk_ReportID = 7) 
					AND @NextScheduleDateTime >= DATEADD(SECOND, -900, GETDATE()) AND @NextScheduleDateTime < GETDATE()))
		END

		--Declaring cursor for fetching merchant recipients one by one for summary report
		DECLARE Queue_Cursor CURSOR FOR SELECT MerchantUserId, Email, Company, ReportId, UserName FROM @CursorData
		OPEN Queue_Cursor

		FETCH NEXT FROM Queue_Cursor INTO @MerchantUserId, @To, @Company, @ReportId, @UserName
		WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @XmlData = ''

			--Calling store procedure SummaryReport to get the data in xml format
			EXEC @Result = dbo.usp_MerchantUserTotalTransByVehicle @IsOnDemandReport, @FromDate, @ToDate, @MerchantUserId, @ReportId, @XmlData OUTPUT, @ReportType OUTPUT, @FDate OUTPUT, @TDate OUTPUT,
												@TotalTransaction OUTPUT, @TotalActualFare OUTPUT, @TotalTips OUTPUT, @TotalFareAmount OUTPUT,
												@TotalSurcharge OUTPUT, @TotalFee  OUTPUT, @NoChargeBack OUTPUT, @ChargeBackValue OUTPUT,
												@NoVoid OUTPUT, @VoidValue OUTPUT, @TotalTechFee OUTPUT, @AmountOwing OUTPUT, @Slab1Message OUTPUT,
												@Slab2Message OUTPUT, @Slab3Message OUTPUT, @Slab4Message OUTPUT, @TotalSlab1 OUTPUT,
												@TotalSlab2 OUTPUT, @TotalSlab3 OUTPUT, @TotalSlab4 OUTPUT

			IF (@Result <> 0)
			BEGIN
				SET @Valid = 0
			END
			IF (@IsOnDemandReport = 1)
			BEGIN
				SET @RepType = 'On Demand User Total Trans By vehicle Report'
			END
			ELSE
			BEGIN
				SET @RepType = CASE @ReportType 
									WHEN 1 THEN 'Daily User Total Trans By vehicle Report' 
									WHEN 2 THEN 'Weekly User Total Trans By vehicle Report'
									ELSE 'Monthly User Total Trans By vehicle Report' 
								END
			END

			SET @RepHeading = @RepType + ' ' + '(Dated from' + ' ' + CONVERT(VARCHAR(11), @FDate, 106) + ' ' + 'to' + ' ' + CONVERT(VARCHAR(11), @TDate, 106) + ')'

			IF (@XmlData IS NULL OR @XmlData = '') 
			BEGIN 
				SET @Valid = 0

				INSERT INTO MailMessage (RepUserId, [Status], SendTo, Body, RepUserTypeId, [Date], ReportType) 
				VALUES (@MerchantUserId, -1, @To, 'No records in database', 4, GETDATE(), 'MerchantUserTotalTransByVehicle')
			END
			ELSE 
			BEGIN
				SET @Valid=1
			END

			IF (@Valid = 1)
			BEGIN
				--Set the xml data in the html format 
				SET @Body = '<html>
							<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
							<style>td{text-align:left;font-family:verdana;font-size:12px}
							p{font-family:verdana;font-size:13px}span{font-family:verdana;font-size:13px}</style>
							<body><table style="background-color:#e0e0e0; padding:20px; height:100%;"><tr><td><p style="font-size:11px;font-weight:bold">' + CONCAT('Dear', ' ', @UserName, ',') + '</p>
							<p style="font-size:11px;font-weight:bold">Listed below is the total transactions by vehicle report containing summarized details of transactions.</p>
							<H5 style="color:#003366;text-align:center;font-size:11px;font-family:verdana; padding:10px"> Merchant  :  ' + @Company + '</H5>
							<h5 style="color:#003366;text-align:center;font-size:11px;font-family:verdana; padding:10px;">' + @RepHeading + '</h5>
							<table border = 1 cellpadding="0" cellspacing="0" style="background-color:#aed6eb; border:2px solid #003366; border-collapse:collapse; padding:2px;">
							<thead style="background-color:#2b79c7; padding:10px; "><tr>
							<th style="color:grey;font-size:10px;width:90px;text-align:left;font-family:verdana; color:#fff; padding:5px;">Sr.No</th>
							<th style="color:grey;font-size:10px;width:600px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Fleet</th>
							<th style="color:grey;font-size:10px;width:140px;text-align:left;font-family:verdana; color:#fff; padding:2px;">No of Trans</th>
							<th style="color:grey;font-size:10px;width:150px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Fare Amt</th>
							<th style="color:grey;font-size:10px;width:100px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Tips</th>
							<th style="color:grey;font-size:10px;width:120px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Surcharges</th>
							<th style="color:grey;font-size:10px;width:100px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Tech Fee</th>
							<th style="color:grey;font-size:10px;width:100px;text-align:left;font-family:verdana; color:#fff; padding:2px;">No of Chargebacks</th>
							<th style="color:grey;font-size:10px;width:100px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Chargeback Value</th>
							<th style="color:grey;font-size:10px;width:200px;text-align:left;font-family:verdana; color:#fff; padding:2px;">No of void</th>
							<th style="color:grey;font-size:10px;width:130px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Void value</th>
							<th style="color:grey;font-size:10px;width:150px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Total Amount</th>
							<th style="color:grey;font-size:10px;width:150px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Fees</th>
							<th style="color:grey;font-size:10px;width:100px;text-align:left;font-family:verdana; color:#fff; padding:2px;">Amount Owing</th>
							<th style="color:grey;font-size:10px;width:100px;text-align:left;font-family:verdana; color:#fff; padding:2px;">NoOfVehicles(' + @Slab1Message + ')</th>
							<th style="color:grey;font-size:10px;width:100px;text-align:left;font-family:verdana; color:#fff; padding:2px;">NoOfVehicles(' + @Slab2Message + ')</th>
							<th style="color:grey;font-size:10px;width:100px;text-align:left;font-family:verdana; color:#fff; padding:2px;">NoOfVehicles(' + @Slab3Message + ')</th>
							<th style="color:grey;font-size:10px;width:100px;text-align:left;font-family:verdana; color:#fff; padding:2px;">NoOfVehicles(' + @Slab4Message + ')</th></tr></thead>'    

				SET @Body = @Body + @XmlData + '<tfoot><tr style="height:25px;font-weight:bold">
												<td colspan=2 style="font-size:11px;text-align:center">
												Total</td><td style="font-size:11px">' + CAST(@TotalTransaction AS NVARCHAR(50)) + '</td>
												<td style="font-size:11px">' + @TotalActualFare + '</td>
												<td style="font-size:11px">' + @TotalTips + '</td>
												<td style="font-size:11px">' + @TotalSurcharge + '</td>
												<td style="font-size:11px">' + @TotalTechFee + '</td>
												<td style="font-size:11px">' + CAST(@NoChargeBack AS NVARCHAR(50)) + '</td>
												<td style="font-size:11px">' + @ChargeBackValue + '</td>
												<td style="font-size:11px">' + CAST(@NoVoid AS NVARCHAR(50)) + '</td>
												<td style="font-size:11px">' + @VoidValue + '</td>
												<td style="font-size:11px">' + @totalFareAmount + '</td>
												<td style="font-size:11px">' + @TotalFee + '</td>
												<td style="font-size:11px">' + @AmountOwing + '</td>
												<td style="font-size:11px">' + CAST(@TotalSlab1 AS NVARCHAR(50)) + '</td>
												<td style="font-size:11px">' + CAST(@TotalSlab2 AS NVARCHAR(50)) + '</td>
												<td style="font-size:11px">' + CAST(@TotalSlab3 AS NVARCHAR(50)) + '</td>
												<td style="font-size:11px">' + CAST(@TotalSlab4 AS NVARCHAR(50)) + '</td>
												</tr></tfoot></table><br/><br/>
												<span>Thanks & Regards,<br/><br/>MTData, LLC.<br/>www.mtdata.us<span>
												<h4 style="color:grey;font-size:12px;font-family:verdana">MTData-All rights reserved</h4>
												<img src="D:\logo-1.png" width="138" height="44" alt="logo">
												<p style="font-size:11px;color:black">Note:This is an automated message. Do not Reply or Reply All
												to this message as replies are not being accepted.</p> </body></html>'

				INSERT INTO dbo.MailMessage (RepUserId, [Status], SendTo, Body, [RepUserTypeId], [Date], [ReportType])
				VALUES (@MerchantUserId, 0, @To, @Body, 4, GETDATE(), 'MerchantUserTotalTransByVehicle')

				SET @MailId = SCOPE_IDENTITY()

				--Execute the sp_send_dbmail to send the summary report in html format 
				EXEC @Res = msdb.dbo.sp_send_dbmail @profile_name = 'MTData', @recipients = @To, @body = @Body, @subject = 'Merchant User Total Trans By Vehicle Report', @body_format = 'HTML'

				IF (@Res != 0)
				BEGIN
					SET @Error = @@ERROR
					UPDATE dbo.MailMessage SET Error = @Error, [Date] = GETDATE() WHERE MailId = @MailId
				END
				ELSE
				BEGIN
					UPDATE dbo.MailMessage SET [Status] = 1, [Date] = GETDATE() WHERE MailId = @MailId
				END
			END

			FETCH NEXT FROM Queue_Cursor INTO @MerchantUserId, @To, @Company, @ReportId, @UserName
		END
		CLOSE Queue_Cursor
		DEALLOCATE Queue_Cursor

		SELECT 0
	END TRY
	--use catch to log error in the table  ProcErrorHandler
	BEGIN CATCH
		INSERT INTO dbo.ProcErrorHandler (ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, UserName, HostName, [TimeStamp])
		SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()

		SELECT 1
	END CATCH
END
GO
