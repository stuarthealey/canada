﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTDataDispatchSystemSync.Models
{
    public class Lease
    {
        public string LeaseType { get; set; }
        public string LeaseStatus { get; set; }
        public string LeaseVehicle { get; set; }
        public string LeaseExpiryGracePeriod { get; set; }
    }
}