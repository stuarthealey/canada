﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web.Mvc;
using System.Resources;
using System.Configuration;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Extension;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Core.Service
{
    public class CommonService : ICommonService
    {
        private readonly ICommonRepository _commonRepository;

        public CommonService([Named("CommonRepository")] ICommonRepository commonRepository)
        {
            _commonRepository = commonRepository;
        }

        /// <summary>
        /// Purpose             :   To Get Countrys List
        /// Function Name       :   GetCountryList
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/02/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CountryCodeDto> GetCountryList()
        {
            try
            {
                var countryList = _commonRepository.CountryList();
                var countryDtoList = countryList.ToDTOCountryList();
                List<CountryCodeDto> objcountrylist = countryDtoList.ToList();
                ResourceManager rsm = new ResourceManager("MTD.Core.Resource.Resource", this.GetType().Assembly);
                string pleaseSelectCountry = rsm.GetString("Please_Select_a_Country");
                var li = new List<SelectListItem> { new SelectListItem { Text = pleaseSelectCountry, Value = "0" } };
                li.AddRange(objcountrylist.Select(item => new SelectListItem { Text = item.Title, Value = Convert.ToString(item.CountryCodesID) }));
                return objcountrylist;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get States List
        /// Function Name       :   GetStateList
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/02/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="stateId"></param>
        /// <param name="mtch"></param>
        /// <returns></returns>
        public IEnumerable<CountryStateDto> GetStateList(string stateId, string mtch)
        {
            try
            {
                var stateList = _commonRepository.GetStateList(stateId, mtch);
                var stateDtoList = stateList.ToDtoStateList();
                return stateDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get city List
        /// Function Name       :   GetCityList
        /// Created By          :   Naveen Kumar
        /// Created On          :   02/04/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="mtch"></param>
        /// <returns></returns>
        public IEnumerable<CityDto> GetCityList(string mtch)
        {
            try
            {
                var cityList = _commonRepository.GetCityList(mtch);
                var cityDtoList = cityList.ToDtoCityList();
                return cityDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Fleets List
        /// Function Name       :   GetFleetList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/02/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<FleetListDto> GetFleetList(int merchantId)
        {
            var fleetList = _commonRepository.FleetList(merchantId);
            var fleetDtoList = fleetList.ToDTOFleetSelectList();
            return fleetDtoList;
        }

        /// <summary>
        /// Purpose             :   To Get Terminals List
        /// Function Name       :   GetTerminalList
        /// Created By          :   Salil Gupta
        /// Created On          :   02/04/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TerminalListDto> GetTerminalList(int merchantId)
        {
            try
            {
                var terminalList = _commonRepository.TerminalList(merchantId);
                var terminalDtoList = terminalList.ToDTOTerminalSelectList();
                return terminalDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Merchants List
        /// Function Name       :   GetMerchantList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/04/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MerchantListDto> GetMerchantList()
        {
            try
            {
                var merchantList = _commonRepository.MerchantList();
                var merchantDtoList = merchantList.ToDTOMerchantSelectList();
                return merchantDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To GetGateways List
        /// Function Name       :   GetGatewayList
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/04/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<GatewayListDto> GetGatewayList()
        {
            try
            {
                var gatewayList = _commonRepository.GatewayList();
                var gatewayDtoList = gatewayList.ToDTOGatewayList();
                return gatewayDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To check  does email Exist
        /// Function Name       :   IsExist
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/08/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="emailId"></param>
        /// <returns></returns>
        public bool IsExist(string emailId)
        {
            try
            {
                var result = _commonRepository.IsExist(emailId);
                return result;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Tax Merchant
        /// Function Name       :   GetTaxMerchantList
        /// Created By          :   Salil Gupta
        /// Created On          :   02/04/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MerchantListDto> GetTaxMerchantList()
        {
            try
            {
                var merchantList = _commonRepository.TaxMerchantList();
                var merchantDtoList = merchantList.ToDTOMerchantSelectList();
                return merchantDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Term condition
        /// Function Name       :   GetTermDto
        /// Created By          :   Naveen kumar
        /// Created On          :   02/06/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="termId"></param>
        /// <returns></returns>
        public TCDto GetTermDto(int termId)
        {
            try
            {
                var term = _commonRepository.GetTerm(termId);
                var termDto = term.ToTCDto();
                return termDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To check Is Locked Member
        /// Function Name       :   IsLockedMember
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   02/09/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool IsLockedMember(int id, bool status)
        {
            try
            {
                return _commonRepository.IsLockedMember(id, status);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Recipients List
        /// Function Name       :   GetRecipientList
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   02/08/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MerchantListDto> GetRecipientList()
        {
            try
            {
                var recipientList = _commonRepository.RecipientList();
                var recipientListDto = recipientList.ToDtoRecipientList();
                return recipientListDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To unregistered terminal list
        /// Function Name       :   GetUnRegisteredTerminalList
        /// Created By          :   Naveen Kumar
        /// Created On          :   04/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<RapidConnectTerminalDto> GetRegisteredTerminalList(int merchantId)
        {
            var terminalList = _commonRepository.RegisteredTerminalList(merchantId);
            var terminalListDto = terminalList.ToDtoRegisteredTerminal();
            return terminalListDto;
        }

        /// <summary>
        /// Purpose             :   To Get AdminRecipients List
        /// Function Name       :   GetRecipientList
        /// Created By          :   Ravit Chaudhary
        /// Created On          :   06/05/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="reportId"></param>
        /// <param name="userType"></param>
        /// <returns></returns>
        public IEnumerable<MerchantListDto> GetRecipientList(int reportId, int userType)
        {
            try
            {
                var recipientList = _commonRepository.RecipientList(reportId, userType);
                var recipientListDto = recipientList.ToDtoRecipientList();
                return recipientListDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Recipient List Distinctly
        /// Function Name       :   GetRecipientListDistinct
        /// Created By          :   Ravit Chaudhary
        /// Created On          :   06/08/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MerchantListDto> GetRecipientListDistinct()
        {
            try
            {
                var recipientList = _commonRepository.RecipientListDistinct();
                var recipientListDto = recipientList.ToDtoRecipientList();
                return recipientListDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Admin Recipient List Distinctly
        /// Function Name       :   GetAdminRecipientListDistinct
        /// Created By          :   Ravit Chaudhary
        /// Created On          :   06/08/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MerchantListDto> GetAdminRecipientListDistinct()
        {
            try
            {
                var adminRecipientList = _commonRepository.AdminRecipientListDistinct();
                var adminRecipientListDto = adminRecipientList.ToDtoRecipientList();
                return adminRecipientListDto;
            }
            catch
            {
                throw;
            }
        }

        public void SaveLogInDetails(string driverId, string vehicleId, string token, bool isDispatchRequest, int fleetId)
        {
            _commonRepository.SaveLogInDetails(driverId, vehicleId, token, isDispatchRequest, fleetId);
        }

        public int SaveLogOutDetails(string driverNo, bool isDispatchRequest, int fleetId)
        {
            return _commonRepository.SaveLogOutDetails(driverNo, isDispatchRequest, fleetId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driverNo"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public bool IsTokenValid(string driverNo, string token)
        {
            return _commonRepository.IsTokenValid(driverNo, token);
        }

        public FirstDataDetailsResultDto GetFirstDataDetails(string deviceId, string driverNo, string token, int fleetId, string requestType)
        {
            //T#6488 Determine if this is for the UK region, to set the IncludeDatawire parameter.
            bool includeDatawire = true;

            if (ConfigurationManager.AppSettings["Project"] == "UK")
                includeDatawire = false;

            var details = _commonRepository.GetFirstDataDetails(deviceId, driverNo, token, fleetId, requestType, includeDatawire);
            var fDetails = details.ToFirstDataDto();

            return fDetails;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driverId"></param>
        /// <param name="reportType"></param>
        /// <param name="reportDate"></param>
        /// <returns></returns>
        public int OnDemandReport(int driverId, int reportType, DateTime reportDate)
        {
            try
            {
                int response = _commonRepository.OnDemandReport(driverId, reportType, reportDate);
                return response;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public IEnumerable<DriverListsDto> GetDrivers(int fleetId)
        {
            try
            {
                var driverList = _commonRepository.GetDrivers(fleetId);
                var drivers = driverList.ToDtoList();
                return drivers;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public IEnumerable<CarListDto> GetCars(int fleetId)
        {
            try
            {
                var carList = _commonRepository.GetCars(fleetId);
                var cars = carList.ToDtoList();
                return cars;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="macAdd"></param>
        /// <param name="driverNo"></param>
        /// <param name="token"></param>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public bool IsValidateTokenMacAdd(string macAdd, string driverNo, string token, int fleetId)
        {
            return _commonRepository.IsValidateTokenMacAdd(macAdd, driverNo, token, fleetId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public Tuple<string, string> GetCardAndPlate(int requestId)
        {
            return _commonRepository.GetCardAndPlate(requestId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public SurchargeFleetFeeDto SurchargeFees(int fleetId)
        {
            SurchargeFleetFee result = _commonRepository.SurchargeFees(fleetId);
            return result.ToSurchargeFleetFee();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="corporateId"></param>
        /// <returns></returns>
        public IEnumerable<MerchantListDto> GetCorporateUserList(int corporateId)
        {
            var corporateRecipientList = _commonRepository.GetCorporateUserList(corporateId);
            var corporateRecipientListDto = corporateRecipientList.ToDtoRecipientList();
            return corporateRecipientListDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reportId"></param>
        /// <param name="corporateUserId"></param>
        /// <returns></returns>
        public IEnumerable<MerchantListDto> GetUpdatedCorporateUserList(int reportId, int corporateUserId)
        {
            try
            {
                var recipientList = _commonRepository.GetUpdatedCorporateUserList(reportId, corporateUserId);
                var recipientListDto = recipientList.ToDtoRecipientList();
                return recipientListDto;
            }
            catch
            {
                throw;
            }
        }

        public string MerchantTimeZone(int merchantId)
        {
            return _commonRepository.MerchantTimeZone(merchantId);
        }

        public IEnumerable<MerchantListDto> WeeklyReportRecipients(int reportId)
        {
            try
            {
                var adminRecipientList = _commonRepository.WeeklyReportRecipients(reportId);
                var adminRecipientListDto = adminRecipientList.ToDtoRecipientList();
                return adminRecipientListDto;
            }
            catch
            {
                throw;
            }
        }
    }
}
