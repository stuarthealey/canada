﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MTData.Web.ViewData
{
    public class TransactionViewData
    {
        public int TransID { get; set; }
        public string PaymentType { get; set; }
        public string TransactionAmount { get; set; }
        public string CardNumber { get; set; }
        public string CardType { get; set; }
        public string ExpiryDate { get; set; }
        public string CardCVV { get; set; }
        public string ZipCode { get; set; }
        public string StreetAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EMailId { get; set; }
        public string Description { get; set; }
        public string CompanyName { get; set; }
        public string MerchantCategoryCode { get; set; }
        public string TransactionType { get; set; }
        public string TransType { get; set; }
        public string TerminalID { get; set; }
        public string FdMerchantId { get; set; }
        public int MtdMerchantId { get; set; }
        public int UserID { get; set; }
        public string EntryType { get; set; }
        public string Track2Data { get; set; }
        public string IndustryType { get; set; }
        public string SerialNumber { get; set; }
        public string TppId { get; set; }
        public string GroupId { get; set; }
        public string App { get; set; }
        public string Did { get; set; }
        public string ServiceUrl { get; set; }
        public string ServiceId { get; set; }
        public string CCVIndType { get; set; }
        public string DriverNumber { get; set; }
        public string VehicleNumber { get; set; }
        public string SerialNo { get; set; }
        public string RapidConnectAuthId { get; set; }
        public string PinData { get; set; }
        public string KeySerNumData { get; set; }
        public string AdditionalAmout { get; set; }
        public string ReversalIndType { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public string Sorting { get; set; }
        public int FleetId { get; set; }
    }
}