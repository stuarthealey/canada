IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_AdminWeeklySummaryCountScheduler]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_AdminWeeklySummaryCountScheduler]
GO

-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <23-06-2016>
-- Description:	<Prepare and Send weekly summary count reports to admin users>
-- =============================================
-- S.Healey		2016-11-10		- Performance optimisation with use of indexes and improved queries.
--
CREATE PROCEDURE dbo.usp_AdminWeeklySummaryCountScheduler
AS
BEGIN
	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SET NOCOUNT ON
		SET ANSI_WARNINGS OFF

		DECLARE @Res INT,
				@UserId INT,
				@To NVARCHAR(50),
				@Body NVARCHAR(MAX),
				@Error NVARCHAR(200),
				@MailId INT,
				@Valid INT,
				@XmlData NVARCHAR(MAX),
				@UserName NVARCHAR(45),
				@RepHeading NVARCHAR(200),            
				@TransCountThisWeek INT,
				@TransCountOneWeek INT,
				@TransCountTwoWeeks INT,
				@TransCountThreeWeeks INT,
				@TransCountFourWeeks INT,
				@TransCountFiveWeeks INT,
				@TransCountSixWeeks INT

			--Declaring cursor for fetching admin recipients one by one from database
		DECLARE WeeklyCount_Cursor CURSOR FOR SELECT u.UserID, u.Email, u.FName 
											FROM dbo.Users u 
												INNER JOIN dbo.Recipient r ON u.UserID = r.fk_UserId 
											WHERE r.IsRecipient = 1 
											  AND r.fk_UserTypeId = 1 
											  AND r.fk_ReportId = 27 
											  AND u.IsActive = 1 
											  AND u.fk_UserTypeID = 1 

		OPEN WeeklyCount_Cursor
		FETCH NEXT FROM WeeklyCount_Cursor INTO @UserId, @To, @UserName
		WHILE @@FETCH_STATUS = 0   
		BEGIN   
			SET @XmlData=''

			--Calling store procedure usp_AdminWeeklySummaryCount to get the data in xml format
			EXEC dbo.usp_AdminWeeklySummaryCount @XmlData OUTPUT,
												@TransCountThisWeek OUTPUT, @TransCountOneWeek OUTPUT,
												@TransCountTwoWeeks OUTPUT, @TransCountThreeWeeks OUTPUT,
												@TransCountFourWeeks OUTPUT, @TransCountFiveWeeks  OUTPUT,
												@TransCountSixWeeks OUTPUT

			SET @RepHeading = 'Weekly Trans Count Volume Report' 

			IF (@XmlData IS NULL OR @XmlData = '') 
			BEGIN
				SET @Valid=0
				INSERT INTO dbo.MailMessage(RepUserId, [Status], SendTo, Body, RepUserTypeId, [Date], ReportType) 
				VALUES(@UserId, -1, @To, 'No records in database', 1, GETDATE(), 'WeeklySummaryCount')
			END
			ELSE 
			BEGIN
				SET @Valid = 1
			END

			IF (@Valid = 1)
			BEGIN
				 --Set the xml data in the html format 
				SET @Body = '<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
							<style>td{text-align:left;font-family:verdana;font-size:12px}
							p{font-family:verdana;font-size:13px}span{font-family:verdana;font-size:13px}</style>
							<body><table style="background-color:#e0e0e0; padding:20px; height:100%;"><tr><td><p style="font-size:11px;font-weight:bold">' + CONCAT('Dear', ' ', @UserName, ',') + '</p>
							<p style="font-size:11px;font-weight:bold">Listed below is the weekly trans count volume report showing the transactions count upto six weeks.</p>
							<h5 style="color:#003366;text-align:center;font-size:11px;font-family:verdana; padding:10px;">' + @RepHeading + '</h5>
							<table border = 1 cellpadding="0" cellspacing="0" style="background-color:#aed6eb; border:2px solid #003366; border-collapse:collapse; padding:2px;">
							<thead style="background-color:#2b79c7; padding:10px; "><tr>
							<th style="color:grey;font-size:10px;width:90px;text-align:left; font-family:verdana; color:#fff; padding:5px;">Sr.No</th>
							<th style="color:grey;font-size:10px;width:140px;text-align:left; color:#fff;  font-family:verdana;padding:2px;">Fleet</th>
							<th style="color:grey;font-size:10px;width:140px;text-align:left; color:#fff;  font-family:verdana;padding:2px;">Trans Count(This week)</th>
							<th style="color:grey;font-size:10px;width:140px;text-align:left; color:#fff;  font-family:verdana;padding:2px;">Trans Count(One week ago)</th>
							<th style="color:grey;font-size:10px;width:140px;text-align:left; color:#fff;  font-family:verdana;padding:2px;">Trans Count(Two weeks ago)</th>
							<th style="color:grey;font-size:10px;width:150px;text-align:left; color:#fff;  font-family:verdana;padding:2px;">Trans Count(Three weeks ago)</th>
							<th style="color:grey;font-size:10px;width:100px;height:19px;text-align:left;font-family:verdana;padding:2px; color:#fff;">Trans Count(Four weeks ago)</th>
							<th style="color:grey;font-size:10px;width:120px;text-align:left; color:#fff;font-family:verdana;padding:2px;">Trans Count(Five weeks ago)</th>
							<th style="color:grey;font-size:10px;width:100px;text-align:left; color:#fff;font-family:verdana;padding:2px;">Trans Count(Six weeks ago)</th></tr></thead>'

				SET @Body = @Body + @XmlData + '<tfoot><tr style="height:25px;font-weight:bold">
												<td colspan=2 style="font-size:11px;text-align:center;padding:5px;">Total</td>
												<td style="font-size:11px">' + CAST(@TransCountThisWeek AS NVARCHAR(50)) + '</td>
												<td style="font-size:11px">' + CAST(@TransCountOneWeek AS NVARCHAR(50)) + '</td>
												<td style="font-size:11px">' + CAST(@TransCountTwoWeeks AS NVARCHAR(50)) + '</td>
												<td style="font-size:11px">' + CAST(@TransCountThreeWeeks AS NVARCHAR(50)) + '</td>
												<td style="font-size:11px">' + CAST(@TransCountFourWeeks AS NVARCHAR(50)) + '</td>
												<td style="font-size:11px">' + CAST(@TransCountFiveWeeks AS NVARCHAR(50)) + '</td>
												<td style="font-size:11px">' + CAST(@TransCountSixWeeks AS NVARCHAR(50)) + '</td>
												</tr></tfoot></table><br/><br/><span>Thanks & Regards,<br/><br/>MTData LLC.<br/>www.mtdata.au<span>
												<h4 style="color:grey;font-size:12px;font-family:verdana">MTData-All rights reserved</h4>                                              
												<p style="font-size:11px;color:black">Note:This is an automated message. 
												Do not Reply or Reply All to this message as replies are not being accepted.</p><tr><td></table></body></html>'

				INSERT INTO dbo.MailMessage(RepUserId, [Status], SendTo, Body, RepUserTypeId, [Date], ReportType) 
				VALUES (@UserId, 0, @To, @Body, 1, GETDATE(), 'AdminWeeklySummaryCount')

				SET @MailId = SCOPE_IDENTITY()

				--Execute the sp_send_dbmail to send the summary report in html format 
				EXEC @Res = msdb.dbo.sp_send_dbmail @profile_name = 'MTData', @recipients = @To, @body = @Body, @subject = 'Admin Weekly Summary Count', @body_format ='HTML'

				IF (@Res != 0)
				BEGIN
					SET @Error = @@ERROR
					UPDATE dbo.MailMessage SET Error = @Error, [Date] = GETDATE() WHERE MailId = @MailId
				END
				ELSE
				BEGIN
					UPDATE dbo.MailMessage SET [Status] = 1, [Date] = GETDATE() WHERE MailId = @MailId
				END
			END

			FETCH NEXT FROM WeeklyCount_Cursor INTO @UserId, @To, @UserName
		END

		CLOSE WeeklyCount_Cursor   
		DEALLOCATE WeeklyCount_Cursor
	END TRY
	--use catch to log error with details in ProcErrorHandler table
	BEGIN CATCH
		INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
		SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
	END CATCH
END
GO