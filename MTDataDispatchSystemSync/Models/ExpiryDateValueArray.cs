﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTDataDispatchSystemSync.Models
{
    public class ExpiryDateValueArray
    {
        public List<ExpiryDateValueItem> ExpiryDateValueItem { get; set; }
    }
}