IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAdminSummaryReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetAdminSummaryReport]
GO

CREATE  PROCEDURE [dbo].[usp_GetAdminSummaryReport]
(
  @DateFrom DATETIME,
  @DateTo DATETIME
)
AS
BEGIN TRY
  SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF
  DECLARE @InnerTable TABLE
   (
     Id INT, 
     FareValue MONEY,
     Tip MONEY, 
     Amount MONEY, 
     Surcharge MONEY, 
     Fee MONEY, 
     FleetID INT, 
     TxnType VARCHAR(15),
     ResponseCode VARCHAR(25),
	 fk_FleetId INT,
	 IsRefunded BIT,
	 IsVoided BIT,
	 IsCompleted BIT,
	 TechFee MONEY,
	 FleetFee MONEY
   )  
   --insert data into table variable InnerTable for reuse*/                         
   INSERT INTO @InnerTable(Id,FareValue,Tip,Amount,Surcharge,Fee,FleetID,TxnType,ResponseCode,IsRefunded,IsVoided,IsCompleted,TechFee,FleetFee)                   
   SELECT  mt.Id
          ,mt.FareValue
		  ,mt.Tip
		  ,mt.Amount
		  ,mt.Surcharge
		  ,mt.Fee
		  ,fl.FleetID
		  ,mt.TxnType
		  ,mt.ResponseCode
		  ,mt.IsRefunded
		  ,mt.IsVoided
		  ,mt.IsCompleted
		  ,mt.TechFee
		  ,mt.FleetFee		
   FROM MTDTransaction mt 
   INNER JOIN Vehicle vh ON mt.VehicleNo=vh.VehicleNumber 
   INNER JOIN Fleet fl ON vh.fk_FleetID=fl.FleetID
   WHERE CAST(mt.TxnDate AS DATE)>=@DateFrom 
         AND CAST(mt.TxnDate AS DATE)<@DateTo 
		 AND mt.fk_FleetId=vh.fk_FleetID                        
   SELECT  ROW_NUMBER() OVER (ORDER BY Company) AS 'Sr No.'
                               ,ISNULL(mer.Company,'') AS 'Company'
							   ,ISNULL(f.FleetName,'') AS 'FleetName'
							   ,(SELECT ISNULL(COUNT(id),0) FROM @InnerTable it WHERE  it.FleetID=f.FleetID AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND it.IsVoided=0 AND it.ResponseCode IN ('000','002')) AS 'NoOfTrans'
							   ,CONCAT('$',(SELECT ISNULL(SUM(FareValue),'0.00') FROM @InnerTable it WHERE  it.FleetID=f.FleetID  AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002'))) AS 'FareAmt'
							   ,CONCAT('$',(SELECT ISNULL(SUM(Tip),'0.00') FROM @InnerTable it WHERE  it.FleetID=f.FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND  it.ResponseCode IN ('000','002'))) AS 'Tips'
							   ,CONCAT('$',(SELECT ISNULL(SUM(Surcharge),'0.00') FROM @InnerTable it WHERE  it.FleetID=f.FleetID  AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002'))) AS 'Surcharges'						
							   ,CONCAT('$',(SELECT ISNULL(SUM(TechFee),'0.00') FROM @InnerTable it  WHERE  it.FleetID=f.FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002')))  AS 'TechFee'	             							 						
							   ,(SELECT ISNULL(COUNT(id),0) FROM @InnerTable it  WHERE  it.FleetID=f.FleetID AND it.TxnType='Refund'AND it.ResponseCode='000') AS 'NoOfChargebacks'
							   ,CONCAT('$',(SELECT ISNULL(SUM(Amount),'0.00') FROM @InnerTable it WHERE  it.FleetID=f.FleetID AND it.TxnType='Refund'AND it.ResponseCode ='000')) AS 'ChargebackValue'
							   ,(SELECT ISNULL(COUNT(id),0) FROM @InnerTable it WHERE  it.FleetID=f.FleetID AND it.TxnType='Void'AND it.ResponseCode='000')  AS 'NoOfVoid'
							   ,CONCAT('$',(SELECT ISNULL(SUM(Amount),'0.00') FROM @InnerTable it WHERE  it.FleetID=f.FleetID AND it.TxnType='Void'AND it.ResponseCode='000')) AS 'VoidValue'
							   ,CONCAT('$',(SELECT ISNULL(SUM(Amount),'0.00') FROM @InnerTable it WHERE  it.FleetID=f.FleetID AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND IT.IsVoided=0 AND it.ResponseCode IN ('000','002'))) AS 'TotalAmount'
							   ,CONCAT('$',(SELECT ISNULL(SUM(it.Fee),'0.00')+ISNULL(SUM(it.FleetFee),'0.00') FROM @InnerTable it WHERE  it.FleetID=f.FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002')))   AS 'Fees'            							 						
							   ,CONCAT('$',(SELECT ISNULL(SUM(Amount),0.00) FROM @InnerTable it WHERE  it.FleetID=f.FleetID AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND IT.IsVoided=0 AND it.ResponseCode IN ('000','002'))-
							   (SELECT ISNULL(SUM(Fee),'0.00')+ISNULL(SUM(FleetFee),'0.00') FROM @InnerTable it WHERE  it.FleetID=f.FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002'))) AS 'AmountOwing'
                        FROM MTDTransaction m 
					    INNER JOIN  Vehicle v ON m.VehicleNo=v.VehicleNumber 
					    INNER JOIN fleet f ON v.fk_FleetID=f.FleetID 
                        INNER JOIN merchant mer ON f.fk_MerchantID=mer.MerchantID 
					    WHERE  m.ResponseCode IN ('000','002')   
						       AND CAST(m.TxnDate AS DATE)>=@DateFrom
							   AND CAST(m.TxnDate AS DATE)<@DateTo    
							   AND m.fk_FleetId=v.fk_FleetID
							   AND m.Id NOT IN (SELECT Id FROM dbo.MTDTransaction WHERE m.TxnType='Authorization' AND m.IsCompleted=0)
                        GROUP BY f.FleetName,f.FleetID,mer.Company 
					    ORDER BY mer.Company 

END TRY
--use catch to log error with details in ProcErrorHandler table
BEGIN CATCH
      INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
      SELECT ERROR_NUMBER()
	        ,ERROR_SEVERITY()
			,ERROR_STATE()
			,ERROR_PROCEDURE()
			,ERROR_LINE()
			,ERROR_MESSAGE()
			,SUSER_SNAME()
			,HOST_NAME()
			,GETDATE()
END CATCH
GO
