﻿using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Extension;
using MTD.Core.Factory;
using MTD.Core.Service.Interface;
using MTD.Data.Repository.Interface;
using MTD.Data.Data;

namespace MTD.Core.Service
{
    class MemberService : IMemberService
    {
        private readonly IMemberRepository _loginRepository;

        public MemberService([Named("LoginRepository")] IMemberRepository loginRepository) //("MerchantRepository") maped in class NinjectObjectMapper in MTD.Core.Infrastructure same for ("MerchantFactory")
        {
            _loginRepository = loginRepository; //need to initilize object only once due to ninject
        }

        /// <summary>
        /// Purpose             :   To Login User
        /// Function Name       :   Login
        /// Created By          :   Naveen 
        /// Created On          :   12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="loginDto"></param>
        /// <returns></returns>
        public SessionDto Login(LoginDto loginDto)
        {
            try
            {
                var lgLogin = new Login { UserName = loginDto.UserName, PCode = loginDto.PCode };
                var user = _loginRepository.CreateLogin(lgLogin);
                SessionDto sd = AssignDetailsDto.AssignProperties(user);
                return sd;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update the Password
        /// Function Name       :   UpdatePassword
        /// Created By          :   Umesh Kumar
        /// Created On          :   12/31/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="loginDto"></param>
        /// <returns></returns>
        public int UpdatePassword(LoginDto loginDto)
        {
            try
            {
                var lgLogin = new Login { PCode = loginDto.PCode, UserName = loginDto.UserName, oldPCode = loginDto.ConfirmPCode };
                return _loginRepository.UpdatePassword(lgLogin);         
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Login As Admin
        /// Function Name       :   LoginAsAdmin
        /// Created By          :   Salil Gupta
        /// Created On          :   01/23/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="loginDto"></param>
        /// <returns></returns>
        public SessionDto LoginAsAdmin(LoginDto loginDto)
        {
            try
            {
                var lgLogin = new Login { UserName = loginDto.UserName, PCode = loginDto.PCode };
                var user = _loginRepository.LoginAsAdmin(lgLogin);
                SessionDto sd = AssignDetailsDto.AssignProperties(user);
                return sd;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Password
        /// Function Name       :   GetPassword
        /// Created By          :   Madhuri tanwar
        /// Created On          :   12/02/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="emailId"></param>
        /// <returns></returns>
        public UserDto GetPassword(string emailId)
        {
            try
            {
                var user = _loginRepository.GetPassword(emailId);
                var userdto = user.ToDtoUser();
                return userdto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Reset the Password
        /// Function Name       :   Reset
        /// Created By          :   Umesh Kumar
        /// Created On          :   12/31/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="email"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public bool Reset(string email, string newPassword)
        {
            try
            {
                return _loginRepository.Reset(email, newPassword); // add method exist in MTD.Data.Repository to save a new merchant into database Merchant
            }
            catch
            {
                throw;
            }
        }

        public bool PassCodeExpiration(string email)
        {
            return _loginRepository.PassCodeExpiration(email);
        }

        public int ResetExpiredPassword(LoginDto loginDto)
        {
            var login = new Login { PCode = loginDto.PCode, UserName = loginDto.UserName, oldPCode = loginDto.ConfirmPCode };
            return _loginRepository.ResetExpiredPassword(login);
        }

        public bool IsLocked(string email)
        {
            return _loginRepository.IsLocked(email);
        }

        public bool CountFailedAttempt(string email)
        {
            return _loginRepository.CountFailedAttempt(email);
        }
    }
}
