﻿using System;
using System.Collections.Generic;

using MTD.Data.Data;

namespace MTD.Data.Repository.Interface
{
   public interface ICommonRepository
    {
       IEnumerable<CountryCode> CountryList();
       IEnumerable<CountryState> GetStateList(string stateId, string mtch);
       IEnumerable<StateCity> GetCityList(string mtch);
       IEnumerable<Fleet> FleetList(int merchantId);
       IEnumerable<Terminal> TerminalList(int merchantId);
       IEnumerable<Merchant> MerchantList();
       IEnumerable<Gateway> GatewayList();
       bool IsExist(string emailId);
       IEnumerable<Merchant> TaxMerchantList();
       DefaultSetting GetTerm(int termId);
       bool IsLockedMember(int id, bool status);
       IEnumerable<Recipient> RecipientList();
       IEnumerable<Datawire> RegisteredTerminalList(int merchantId); 
       IEnumerable<Recipient> RecipientList(int reportId,int userType); 
       IEnumerable<Recipient> RecipientListDistinct(); 
       IEnumerable<Recipient> AdminRecipientListDistinct(); 
       void SaveLogInDetails(string driverId, string vehicleId, string token, bool isDispatchRequest, int fleetId); 
       int SaveLogOutDetails(string driverNo, bool isDispatchRequest,int fleetId); 
       bool IsTokenValid(string driverNo, string token);
       usp_FirstDataDetails_Result GetFirstDataDetails(string deviceId, string driverNo, string token, int fleetId, string requestType, bool includeDatawire);
       int OnDemandReport(int driverId, int reportType, DateTime reportDate); 
       IEnumerable<DriverList> GetDrivers(int fleetId); 
       IEnumerable<CarList> GetCars(int fleetId); 
       bool IsValidateTokenMacAdd(string macAdd, string driverNo, string token, int fleetId); 
       Tuple<string, string> GetCardAndPlate(int requestId); 
       SurchargeFleetFee SurchargeFees(int fleetId); 
       IEnumerable<Recipient> GetCorporateUserList(int corporateUserId); 
       IEnumerable<Recipient> GetUpdatedCorporateUserList(int reportId, int corporateUserId);
       string MerchantTimeZone(int merchantId);
       IEnumerable<Recipient> WeeklyReportRecipients(int reportId);
    }
}
