﻿using MTD.Core.DataTransferObjects;
using MTD.Data.Data;

namespace MTD.Core.Factory.Interface
{
  public  interface IMerchantFactory
  {
      Merchant CreateMerchant(MerchantDto merchantDto);
      MerchantUser ToMerchantUser(MerchantUserDto merchantuserdto);
      ManageReciept ToManageReciept(ManageRecieptDto recieptDto);
      ReceiptMaster ToReceiptMaster(ReceiptMasterDto recieptDto);
  }
}
