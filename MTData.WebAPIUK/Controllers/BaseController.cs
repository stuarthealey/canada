﻿using System;
using System.Text;
using System.Web.Http;
using MTD.Core.Service.Interface;
using MTData.Utility;
using Ninject;

namespace MTData.WebAPIUK.Controllers
{
    public class BaseController : ApiController
    {
        private static ITransactionServiceUK _transactionServiceUK;
        private static StringBuilder _logMessage;
        static ILogger _logger = new Logger();
        public BaseController ([Named("TxnServiceUk")] ITransactionServiceUK transactionService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _transactionServiceUK = transactionService;
            _logMessage = logMessage;
        }


    }
}
