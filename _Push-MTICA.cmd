ECHO ** Push release to Canada Web Apps
PAUSE

XCOPY "C:\developmentSV\Payment\canada\build\MTDataPayments" "C:\inetpub\wwwroot\CAMTIPayments" /S /Y
XCOPY "C:\developmentSV\Payment\canada\build\MTDataPaymentsApp" "C:\inetpub\wwwroot\CAMTIPaymentsApp" /S /Y
XCOPY "C:\developmentSV\Payment\canada\build\MTDataPaymentsTerminal" "C:\inetpub\wwwroot\CAMTIPaymentsTerminal" /S /Y

ECHO ** Push to Canada Web APPS completed
PAUSE

IISRESET
PAUSE

rem ** force file change to test push