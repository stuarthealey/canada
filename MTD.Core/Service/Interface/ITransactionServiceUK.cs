﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MTD.Core.DataTransferObjects;

namespace MTD.Core.Service.Interface
{
    public interface ITransactionServiceUK
    {
        int AddSemiTxn(MtdtxnSemiUKDto ukDto, MTDTransactionDto UsmtdDto);

        int AddSemiSattlementTxn(MtdtxnSattlementUKDto ukDto);
    }
}
