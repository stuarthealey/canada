﻿using System.Collections.Generic;
using System.Data;
using MTD.Data.Data;

namespace MTD.Data.Repository.Interface
{
     public interface IDriverRepository
    {
        bool Add(Driver driver);
        bool IsEmailExist(string emailId, int fleetId,int driverId);
        IEnumerable<Driver> DriverList(FilterSearchParameters filterSearch);
        bool Update(Driver driver, int driverId);
        Driver GetDriver(int driverId, int merchantId);
        void DeleteDriver(int driverId);
        IEnumerable<Driver> DrivGetDeriverListByFiltererList(FilterSearchParameters searchParameters);
        Driver GetDriverDto(string driverNo, string driverPin);
        Driver GetDriverDto(string driverNo, int fleetId);
        bool IsDriverNoValid(string driverNo, string driverPin);
        Driver GetDriverDto(string driverNo);
        bool AddDriverList(Driver objDriver,int merchantId);
        int IsDriverNoExist(Driver driver, int merchantId, int userId, int userType);
        bool ResetPin(int driverId, string pin);
        IEnumerable<DriverReportRecipient> GetDriverReceipent(int merchantId, int fleetId);
        bool UpdateDriverRecipient(DriverRecList driverRecipient, int fleetId, int userType, int merchantId);
        string GetDriverCompany(int merchantId);
        bool ScheduleDriverReport(ScheduleReport scheduleDto);
        IEnumerable<Report> DriverReportList();
        IEnumerable<ScheduleReport> ScheduleReportListByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting);
        ScheduleReport GetScheduleReport(int scheduleId);
        bool ScheduleDriverReportUpdate(ScheduleReport scheduleDto);
        IEnumerable<Fleet> AllFleets();
        IEnumerable<ScheduleReport> ScheduleDriverReportList();
        bool MerchantDriverRecipient(DriverRecList driverRecipient, int fleetId, int userType, int merchantId);
        IEnumerable<DriverReportRecipient> GetDriverReceipent();
        IEnumerable<DriverReportRecipient> GetMerchantDriverReceipent(int merchantId);
        IEnumerable<usp_GetLoginData_Result> GetDriverLoginDto(string DeviceId, string DriverId, int UserType);
        bool UpdatePin(int id, string hashPin);
        IEnumerable<Driver> UserDriverList(FilterSearchParameters filterSearch);
        bool IsExist(int driverID);
        bool IsDupligate(int fleetId, string driverNo);
        Driver AddDispatchDriver(Driver driver);
         
    }
}
