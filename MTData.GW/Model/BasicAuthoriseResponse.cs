﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WCFGW.Model
{
	public class BasicAuthoriseResponse
	{
		public BasicAuthoriseResult BasicAuthoriseResult { get; set; }
	}

	public class BasicAuthoriseResult
	{
		public string AuthorisationCode { get; set; }
		public DateTime AuthorisationDate { get; set; }
		public decimal BalanceAmt { get; set; }
		public string CardType { get; set; }
		public string DeclinedReason { get; set; }
		public string ErrorMessage { get; set; }
		public string ExpiryDate { get; set; }
		public long JobId { get; set; }
		public decimal PaymentAmt { get; set; }
		public string RequestID { get; set; }
		public int ResponseType { get; set; }
		public string TransactionId { get; set; }
		public string TruncatedPAN { get; set; }
		public decimal PaymentGatewayProcessingFee { get; set; }
	}

}