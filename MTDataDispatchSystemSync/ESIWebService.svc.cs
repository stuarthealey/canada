﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTDataDispatchSystemSync.Models;

namespace MTDataDispatchSystemSync
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ESIWebService : IESIWebService
    {
        readonly ILogger _logger = new Logger();
        private readonly IDriverService _driverService;
        private StringBuilder _logMessage;
        private readonly IVehicleService _vehicleService;
        private readonly IFleetService _fleetService;
        private StringBuilder _logTracking;

        public ESIWebService([Named("DriverService")] IDriverService driverService, [Named("VehicleService")] IVehicleService vehicleService, [Named("FleetService")] IFleetService fleetService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _driverService = driverService;
            _logMessage = logMessage;
            _vehicleService = vehicleService;
            _logTracking = logTracking;
            _fleetService = fleetService;
        }

        public string Authenticate(string SystemID, string userName, string pCode)
        {
            var respxml = string.Empty;
            bool isAuthenticatd = true;
            string successToken = string.Empty;
            ESIAuthenticateResult esi = new ESIAuthenticateResult();
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(pCode))
            {
                esi.Error = Resource.ESIResource.InvalidCredentials;
                isAuthenticatd = false;
            }
            if (string.IsNullOrEmpty(SystemID))
            {
                esi.Error = Resource.ESIResource.WebMethodAccessDenied;
                isAuthenticatd = false;            
            }

            if (isAuthenticatd)
                successToken = _fleetService.Authenticate(SystemID, userName, pCode);

            if (!string.IsNullOrEmpty(successToken))
            {
                if (successToken == "WebMethodAccessDenied")
                {
                    esi.Error = Resource.ESIResource.WebMethodAccessDenied;
                    esi.Succeeded = false;
                    esi.Token = string.Empty;
                    esi.ErrorMessage = string.Empty;
                    respxml = CreateXml(esi);
                    return respxml;
                }
                if (successToken == "InvalidCredentials")
                {
                    esi.Error = Resource.ESIResource.InvalidCredentials;
                    esi.Succeeded = false;
                    esi.Token = string.Empty;
                    esi.ErrorMessage = string.Empty;
                    respxml = CreateXml(esi);
                    return respxml;
                }

                esi.Error = Resource.ESIResource.None;
                esi.Succeeded = true;
                esi.ErrorMessage = string.Empty;
                esi.Token = successToken;
                respxml = CreateXml(esi);
                return respxml;
            }
            esi.Succeeded = false;
            esi.Token = string.Empty;
            esi.ErrorMessage = string.Empty;
            respxml = CreateXml(esi);
            return respxml;
        }

        public string GetVehicleByCarNumberForFleet(string token, string carNumber, int fleetID)
        {
            ESIVehicleResult esiVehicle = new ESIVehicleResult();
            bool isAuthenticatd = true;
            if (string.IsNullOrEmpty(token))
            {
                esiVehicle.Succeeded = false;
                esiVehicle.Error = Resource.ESIResource.InvalidCredentials;
                isAuthenticatd = false;
                esiVehicle.ErrorMessage = string.Empty;
            }
            else
            {
                bool IstokenValid = _fleetService.tokenValidation(token, fleetID);
                if (!IstokenValid)
                {

                    isAuthenticatd = false;
                    esiVehicle.Succeeded = false;
                    esiVehicle.Error = Resource.ESIResource.InvalidCredentials;
                    esiVehicle.ErrorMessage = string.Empty;
                }
            }

            if (isAuthenticatd)
            {
                VehicleDto fleetVehicleList = _vehicleService.VehicleListInFleet(token, carNumber, fleetID);
                List<Vehicle> lV = new List<Vehicle>();
                
                Vehicle vehicleObj = new Vehicle();
                if (fleetVehicleList.VehicleID == 0)
                {
                    esiVehicle.Succeeded = false;
                    esiVehicle.Error = Resource.ESIResource.VehicleNotFound;
                    esiVehicle.ErrorMessage = string.Empty;
                    _fleetService.tokenReset(token);
                }
                else
                {
                    vehicleObj.VehicleID = fleetVehicleList.VehicleID;
                    vehicleObj.CarNumber = fleetVehicleList.VehicleNumber;
                    vehicleObj.PrimaryFleet = Convert.ToString(fleetID);
                    vehicleObj.PrimaryCity = string.Empty;
                    vehicleObj.Registration = fleetVehicleList.VehicleRegNo;
                    vehicleObj.TaxiLicence = fleetVehicleList.PlateNumber;
                    vehicleObj.ValidationReason = string.Empty;
                    vehicleObj.InhibitedReason = string.Empty;
                    vehicleObj.InsurancePolicy = string.Empty;
                    vehicleObj.InsurancePolicyCompany = string.Empty;
                    vehicleObj.IsActive = fleetVehicleList.IsActive;
                    vehicleObj.VehicleMake = string.Empty;
                    vehicleObj.VehicleModel = string.Empty;
                    vehicleObj.VehicleMonth = string.Empty;
                    vehicleObj.VehicleYear = string.Empty;
                    vehicleObj.VehicleType = string.Empty;
                    vehicleObj.FuelType = string.Empty;
                    vehicleObj.Comments = string.Empty;
                    vehicleObj.VIN = string.Empty;
                    vehicleObj.Lease = new List<Lease>();
                    vehicleObj.PurchaseCost = 0;
                    vehicleObj.LabourCosts = 0;
                    vehicleObj.PartsCosts = 0;
                    vehicleObj.TotalCosts = 0;
                    vehicleObj.Assets = new List<Assets>();
                    vehicleObj.VehicleFleets = new List<Fleets>();;
                    vehicleObj.VehicleConditions = new List<Condition>();
                    vehicleObj.StandbyAction = string.Empty;
                    vehicleObj.ExpiryDateValueArray = new List<ExpiryDateValueItem>();
                    lV.Add(vehicleObj);
                    esiVehicle.Error = "None";
                    esiVehicle.ErrorMessage = string.Empty;
                    esiVehicle.Succeeded = true;
                    esiVehicle.Vehicles = lV;
                    _fleetService.tokenReset(token);
                }
            }

            var respxml = CreateXml(esiVehicle);
            return respxml;
        }

        public string SaveVehicle(string token, Vehicle vehicle)
        {
            Vehicle esiVehicle = new Vehicle();
            ESIVehicleResult esiVehicleResult = new ESIVehicleResult();
            int fid = Convert.ToInt32(vehicle.PrimaryFleet);
            bool isAuthenticatd = true;
            
            if (string.IsNullOrEmpty(token))
            {
                esiVehicleResult.Succeeded = false;
                esiVehicleResult.Error = Resource.ESIResource.InvalidCredentials;
                isAuthenticatd = false;
                esiVehicleResult.ErrorMessage = string.Empty;
            }
            else
            {
                bool IstokenValid = _fleetService.tokenValidation(token, fid);
                if (!IstokenValid)
                {
                    isAuthenticatd = false;
                    esiVehicleResult.Succeeded = false;
                    esiVehicleResult.Error = Resource.ESIResource.InvalidCredentials;
                    esiVehicleResult.ErrorMessage = string.Empty;
                }
            }

            if (isAuthenticatd)
            {
                bool isError = false;
                bool isDuplicate = false;
                if (vehicle.VehicleID <= 0)
                {
                    isDuplicate = _vehicleService.IsDuplicateCarNumber(Convert.ToInt32(fid), vehicle.CarNumber);
                    if (isDuplicate)
                    {
                        _fleetService.tokenReset(token);
                        esiVehicleResult.Error = Resource.ESIResource.DuplicateCarNumber;
                        isError = true;
                    }
                }
                bool isFleetExist = _vehicleService.FindFleet(Convert.ToInt32(fid));
                if (!isFleetExist)
                {
                    _fleetService.tokenReset(token);
                    esiVehicleResult.Error = Resource.ESIResource.InvalidPrimaryFleet;
                    isError = true;
                }
                if (!string.IsNullOrEmpty(vehicle.Registration) && vehicle.Registration.Length > 20)
                {
                    _fleetService.tokenReset(token);
                    esiVehicleResult.Error = Resource.ESIResource.RegistrationNumberTooLong;
                    isError = true;
                }
                if (!string.IsNullOrEmpty(vehicle.TaxiLicence) && vehicle.TaxiLicence.Length > 20)
                {
                    _fleetService.tokenReset(token);
                    esiVehicleResult.Error = Resource.ESIResource.TaxiLicenceNumberTooLong;
                    isError = true;
                }
                if (!isError)
                {
                    VehicleDto vehicleDto = new VehicleDto();
                    vehicleDto.VehicleID = vehicle.VehicleID;
                    vehicleDto.VehicleNumber = vehicle.CarNumber;
                    vehicleDto.fk_FleetID = fid;
                    vehicleDto.VehicleRegNo = vehicle.Registration;
                    vehicleDto.PlateNumber = vehicle.TaxiLicence;
                    vehicleDto.IsActive = vehicle.IsActive;
                    vehicleDto.CreatedBy = "MTI Dispatch System";
                    VehicleDto result = _vehicleService.AddDespetchVehicle(vehicleDto, token);
                    esiVehicle.VehicleID = result.VehicleID;
                    esiVehicle.CarNumber = result.VehicleNumber;
                    esiVehicle.PrimaryFleet = vehicle.PrimaryFleet;
                    esiVehicle.Registration = result.VehicleRegNo;
                    esiVehicle.TaxiLicence = result.PlateNumber;
                    esiVehicle.IsActive = result.IsActive;
                    esiVehicleResult.Error = "None";
                    esiVehicleResult.ErrorMessage = string.Empty;
                    esiVehicleResult.Succeeded = true;
                    esiVehicle.PrimaryCity = string.Empty;
                    esiVehicle.ValidationReason = string.Empty;
                    esiVehicle.InhibitedReason = string.Empty;
                    esiVehicle.InsurancePolicy = string.Empty;
                    esiVehicle.InsurancePolicyCompany = string.Empty;
                    esiVehicle.IsActive = vehicle.IsActive;
                    esiVehicle.VehicleMake = string.Empty;
                    esiVehicle.VehicleModel = string.Empty;
                    esiVehicle.VehicleMonth = string.Empty;
                    esiVehicle.VehicleYear = string.Empty;
                    esiVehicle.VehicleType = string.Empty;
                    esiVehicle.FuelType = string.Empty;
                    esiVehicle.Comments = string.Empty;
                    esiVehicle.VIN = string.Empty;
                    esiVehicle.Lease = new List<Lease>();
                    esiVehicle.PurchaseCost = 0;
                    esiVehicle.LabourCosts = 0;
                    esiVehicle.PartsCosts = 0;
                    esiVehicle.TotalCosts = 0;
                    esiVehicle.Assets = new List<Assets>();
                    esiVehicle.VehicleFleets = new List<Fleets>();
                    esiVehicle.VehicleConditions = new List<Condition>();
                    esiVehicle.StandbyAction = string.Empty;
                    esiVehicle.ExpiryDateValueArray = new List<ExpiryDateValueItem>();
                    esiVehicleResult.Vehicle = esiVehicle;
                    _fleetService.tokenReset(token);
                    var respxml = CreateXml(esiVehicleResult);
                    return respxml;
                }
            }

            esiVehicleResult.ErrorMessage = string.Empty;
            esiVehicleResult.Succeeded = false;

            var respxmlErr = CreateXml(esiVehicleResult);
            return respxmlErr;
        }

        public string GetDriverByID(string token, int fleetID, string driverNo)
        {
            ESIDriverResult esiDriver = new ESIDriverResult();
            bool isAuthenticatd = true;
            if (string.IsNullOrEmpty(token))
            {
                esiDriver.Succeeded = false;
                esiDriver.Error = Resource.ESIResource.InvalidCredentials;
                isAuthenticatd = false;
                esiDriver.ErrorMessage = string.Empty;
            }
            else
            {
                bool IstokenValid = _fleetService.tokenValidation(token, fleetID);
                if (!IstokenValid)
                {
                    isAuthenticatd = false;
                    esiDriver.Succeeded = false;
                    esiDriver.Error = Resource.ESIResource.InvalidCredentials;
                    esiDriver.ErrorMessage = string.Empty;
                }
            }

            try
            {
                if (isAuthenticatd)
                {
                    DriverDto dDto = _driverService.GetDriverDto(driverNo, fleetID);
                    if (dDto == null)
                    {
                        _fleetService.tokenReset(token);
                        esiDriver.Succeeded = false;
                        esiDriver.ErrorMessage = string.Empty;
                        esiDriver.Error = Resource.ESIResource.DriverNotFound;
                    }
                    else
                    {
                        esiDriver.Driver = new Driver();
                        esiDriver.Succeeded = true;
                        esiDriver.Error = Resource.ESIResource.None;
                        esiDriver.ErrorMessage = string.Empty;
                        esiDriver.Driver.DriverID = dDto.DriverID;
                        esiDriver.Driver.DriverNumber = dDto.DriverNo;
                        esiDriver.Driver.City = dDto.Fk_City;
                        esiDriver.Driver.DriverPIN = Convert.ToInt32(dDto.PIN);
                        esiDriver.Driver.DriverName = dDto.FName + " " + dDto.LName;
                        esiDriver.Driver.DriverSurname = string.Empty;
                        esiDriver.Driver.DisplayName = string.Empty;
                        esiDriver.Driver.Address = new List<Address>();
                        esiDriver.Driver.Comments = string.Empty;
                        esiDriver.Driver.DateOfBirth = string.Empty;
                        esiDriver.Driver.BirthCountry = string.Empty;
                        esiDriver.Driver.DriverLicenceCountry = string.Empty;
                        esiDriver.Driver.DriverLicenceState = string.Empty;
                        esiDriver.Driver.DriverLicenceExpiryDate = string.Empty;
                        esiDriver.Driver.Email = dDto.Email;
                        esiDriver.Driver.HomePhone = dDto.Phone;
                        esiDriver.Driver.MobilePhone = dDto.Phone;
                        esiDriver.Driver.ValidationReason = string.Empty;
                        esiDriver.Driver.InhibitedReason = string.Empty;
                        esiDriver.Driver.IsActive = dDto.IsActive;
                        esiDriver.Driver.StartDate = string.Empty;
                        esiDriver.Driver.TaxiLicence = string.Empty;
                        esiDriver.Driver.TaxNumber = string.Empty;
                        esiDriver.Driver.PoliceCheck = string.Empty;
                        esiDriver.Driver.Medical = string.Empty;
                        esiDriver.Driver.DefensiveDriving = string.Empty;
                        esiDriver.Driver.FindgerPrintExpiry = string.Empty;
                        esiDriver.Driver.SMSCapable = false;
                        esiDriver.Driver.OwnerOp = false;
                        esiDriver.Driver.DriverHeight = string.Empty;
                        esiDriver.Driver.Weight = string.Empty;
                        esiDriver.Driver.Sex = string.Empty;
                        esiDriver.Driver.HairColor = string.Empty;
                        esiDriver.Driver.EyeColor = string.Empty;
                        esiDriver.Driver.Ethnicity = string.Empty;
                        esiDriver.Driver.Lease = new List<Lease>();
                        esiDriver.Driver.NextOfKinName = string.Empty;
                        esiDriver.Driver.NextOfKinSurName = string.Empty;
                        esiDriver.Driver.NextOfKinRelationship = string.Empty;
                        esiDriver.Driver.NextOfKinPhone = string.Empty;
                        esiDriver.Driver.NextOfKinMobile = string.Empty;
                        esiDriver.Driver.NextOfKinAddress = new List<NextOfKinAddress>();
                        esiDriver.Driver.SSN = string.Empty;
                        esiDriver.Driver.LastLogonTime = string.Empty;
                        esiDriver.Driver.LastLogoffTime = string.Empty;
                        List<Fleets> flist = new List<Fleets>();
                        Fleets flds = new Fleets();
                        flds.Fleet = Convert.ToString(fleetID);
                        flist.Add(flds);
                        esiDriver.Driver.DriverFleets = flist;
                        esiDriver.Driver.DriverConditions = new List<Condition>();
                        esiDriver.Driver.ExpiryDateValueArray = new List<ExpiryDateValueArray>();
                        _fleetService.tokenReset(token);
                    }
                }
            }
            catch 
            {
            }

            var respxml = CreateXml(esiDriver);
            return respxml;
        }

        public string SaveDriver(string token, Driver driver)
        {
            ESIDriverResult esiDriver = new ESIDriverResult();
            int fid = Convert.ToInt32(driver.DriverFleets.FirstOrDefault().Fleet);
            bool isAuthenticatd = true;

            if (string.IsNullOrEmpty(token))
            {
                esiDriver.Succeeded = false;
                esiDriver.Error = Resource.ESIResource.InvalidCredentials;
                isAuthenticatd = false;
                esiDriver.ErrorMessage = string.Empty;
            }
            else
            {
                bool IstokenValid = _fleetService.tokenValidation(token, fid);
                if (!IstokenValid)
                {
                    isAuthenticatd = false;
                    esiDriver.Succeeded = false;
                    esiDriver.Error = Resource.ESIResource.InvalidCredentials;
                    esiDriver.ErrorMessage = string.Empty;
                }
            }
            if (isAuthenticatd)
            {
                bool isError = false;
                bool isDuplicate = false;
                bool isFound = false;
                if (driver.DriverID > 0)
                {
                    isFound = _driverService.IsExist(driver.DriverID);
                    if (!isFound)
                    {
                        _fleetService.tokenReset(token);
                        esiDriver.Error = Resource.ESIResource.DriverNotFound;
                        isError = true;
                    }
                }
                if (string.IsNullOrEmpty(driver.DriverNumber) || driver.DriverNumber.Length <= 0)
                {
                    esiDriver.Error = Resource.ESIResource.InvalidDriverNumber;
                    isError = true;
                }
                int driverFleet = Convert.ToInt32(driver.DriverFleets.FirstOrDefault().Fleet);
                if (driver.DriverID <= 0)
                {
                    isDuplicate = _driverService.IsDupligate(driverFleet, driver.DriverNumber);
                    if (isDuplicate)
                    {
                        _fleetService.tokenReset(token);
                        esiDriver.Error = Resource.ESIResource.DuplicateDriverNumber;
                        isError = true;
                    }
                }

                if (driver.DriverPIN < 999)
                {
                    _fleetService.tokenReset(token);
                    esiDriver.Error = Resource.ESIResource.InvalidDriverPIN;
                    isError = true;
                }

                if (!isError)
                {
                    DriverDto drvDto = new DriverDto();
                    drvDto.DriverID = driver.DriverID;
                    drvDto.DriverNo = driver.DriverNumber;
                    drvDto.Fk_City = driver.City;
                    drvDto.fk_FleetID = fid;
                    drvDto.FleetName = string.Empty;
                    drvDto.Phone = driver.MobilePhone;
                    drvDto.FName = driver.DriverName;
                    drvDto.Email = driver.Email;
                    drvDto.LName = string.Empty;
                    drvDto.IsActive = driver.IsActive;
                    drvDto.PIN = Convert.ToString(driver.DriverPIN);
                    drvDto.CreatedBy = "Dispatch System";
                    DriverDto resultDto = _driverService.AddDispatchDriver(drvDto);
                    //Createing xml response fields
                    esiDriver.Error = "None";
                    esiDriver.Succeeded = true;
                    esiDriver.ErrorMessage = string.Empty;
                    esiDriver.Driver = new Driver();
                    esiDriver.Driver.DriverID = resultDto.DriverID;
                    esiDriver.Driver.DriverNumber = resultDto.DriverNo;
                    esiDriver.Driver.City = resultDto.Fk_City;
                    if (!string.IsNullOrEmpty(resultDto.PIN))
                        esiDriver.Driver.DriverPIN = Convert.ToInt32(resultDto.PIN);
                    esiDriver.Driver.DriverName = resultDto.FName;
                    esiDriver.Driver.DriverSurname = string.Empty;
                    esiDriver.Driver.DisplayName = string.Empty;
                    esiDriver.Driver.Address = new List<Address>();
                    esiDriver.Driver.Comments = string.Empty;
                    esiDriver.Driver.DateOfBirth = string.Empty;
                    esiDriver.Driver.BirthCountry = string.Empty;
                    esiDriver.Driver.DriverLicenceCountry = string.Empty;
                    esiDriver.Driver.DriverLicenceState = string.Empty;
                    esiDriver.Driver.DriverLicence = string.Empty;
                    esiDriver.Driver.Email = resultDto.Email;
                    esiDriver.Driver.HomePhone = string.Empty;
                    esiDriver.Driver.MobilePhone = resultDto.Phone;
                    esiDriver.Driver.ValidationReason = string.Empty;
                    esiDriver.Driver.InhibitedReason = string.Empty;
                    esiDriver.Driver.IsActive = resultDto.IsActive;
                    esiDriver.Driver.StartDate = string.Empty;
                    esiDriver.Driver.TaxiLicence = string.Empty;
                    esiDriver.Driver.TaxNumber = string.Empty;
                    esiDriver.Driver.PoliceCheck = string.Empty;
                    esiDriver.Driver.Medical = string.Empty;
                    esiDriver.Driver.DefensiveDriving = string.Empty;
                    esiDriver.Driver.FindgerPrintExpiry = string.Empty;
                    esiDriver.Driver.SMSCapable = false; //
                    esiDriver.Driver.OwnerOp = false; //
                    esiDriver.Driver.DriverHeight = string.Empty;
                    esiDriver.Driver.Weight = string.Empty;
                    esiDriver.Driver.Sex = string.Empty;
                    esiDriver.Driver.HairColor = string.Empty;
                    esiDriver.Driver.EyeColor = string.Empty;
                    esiDriver.Driver.Ethnicity = string.Empty;
                    esiDriver.Driver.Lease = new List<Lease>();
                    esiDriver.Driver.NextOfKinName = string.Empty;
                    esiDriver.Driver.NextOfKinSurName = string.Empty;
                    esiDriver.Driver.NextOfKinRelationship = string.Empty;
                    esiDriver.Driver.NextOfKinPhone = string.Empty;
                    esiDriver.Driver.NextOfKinMobile = string.Empty;
                    esiDriver.Driver.NextOfKinAddress = new List<NextOfKinAddress>();
                    esiDriver.Driver.SSN = string.Empty;
                    esiDriver.Driver.LastLogonTime = string.Empty;
                    esiDriver.Driver.LastLogoffTime = string.Empty;
                    List<Fleets> flist = new List<Fleets>();
                    Fleets fld = new Fleets();
                    fld.Fleet = Convert.ToString(fid);
                    flist.Add(fld);
                    esiDriver.Driver.DriverFleets = flist;
                    esiDriver.Driver.DriverConditions = new List<Condition>();
                    esiDriver.Driver.ExpiryDateValueArray = new List<ExpiryDateValueArray>();
                    _fleetService.tokenReset(token);
                    var respxmlSuccess = CreateXml(esiDriver);
                    return respxmlSuccess;
                }
            }

            esiDriver.ErrorMessage = string.Empty;
            esiDriver.Succeeded = false;
            var respxmlErr = CreateXml(esiDriver);
            return respxmlErr;
        }

        public static string CreateXml(Object yourClassObject)
        {
            XmlDocument xmlDoc = new XmlDocument();   //Represents an XML document, 
            // Initializes a new instance of the XmlDocument class.          
            XmlSerializer xmlSerializer = new XmlSerializer(yourClassObject.GetType());
            // Creates a stream whose backing store is memory. 
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, yourClassObject);
                xmlStream.Position = 0;
                //Loads the XML document from the specified string.
                xmlDoc.Load(xmlStream);
                return xmlDoc.InnerXml;
            }
        }

    }
}
