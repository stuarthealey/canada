﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table14Diners
    {
        #region Private Constants and Variables

        #region Constants
        private const string tblId = "|31|34";
        private const string filler6Space = "|20|20|20|20|20|20";
        private const string filler12Zero = "|30|30|30|30|30|30|30|30|30|30|30|30";
        #endregion

        #region Variables
        private string defaultDinerIndicator = "|58"; // |58 represents hex of X
        private string defaultTranID = "|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20"; // |20 represents hex of one blank space
        private string defaultTotalAuthAmt = "|30|30|30|30|30|30|30|30|30|30|30|30";
        #endregion

        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'31 34' in Request
        /// </summary>
        public string TableID
        {
            // |31 and |34 represent hex of numeric 1 and 4
            get { return tblId; }
        }

        /// <summary>
        /// American Express Indicator
        /// default X is set in request
        /// </summary>
        public string DinerIndicator
        {
            get { return defaultDinerIndicator; }
            set { defaultDinerIndicator = value; }
        }

        /// <summary>
        /// Tran ID (Provided by Issuer)
        /// default 15 Spaces(1 space= X'20') in Request 
        /// </summary>
        public string TranID
        {
            get { return defaultTranID; }
            set { defaultTranID = value; }
        }

        /// <summary>
        /// Filler. contains 6 Spaces(1 space= X'20') in Request 
        /// </summary>
        public string Filler6Space
        {
            get { return filler6Space; }
        }

       

        /// <summary>
        /// Filler. contains 12 Spaces(1 space= X'20') in Request 
        /// </summary>
        public string Filler12Zero
        {
            get { return filler12Zero; }
        }

        /// <summary>
        /// Total authorized amount after all incremental authorizations
        /// default 12 Zero(1 Zero= X'30')
        /// </summary>
        public string TotalAuthorizedAmount
        {
            get { return defaultTotalAuthAmt; }
            set { defaultTotalAuthAmt = value; }
        }


       
        #endregion
    }
}