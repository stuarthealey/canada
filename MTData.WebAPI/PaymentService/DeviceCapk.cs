﻿
using System;
using System.Text;
using System.Threading;
using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.WebAPI.Models;
using Ninject;
using MTData.WebAPI.Resources;
using MTData.WebAPI.Common;
using MTData.WebAPI.Controllers;
using System.Xml;
using System.Xml.Serialization;

namespace MTData.WebAPI
{
    public class DeviceCapk : TransactionRequestController
    {
        private readonly ITransactionService _transactionService;
        private static ITerminalService _terminalService;
        private readonly IMerchantService _merchantService;
        private StringBuilder _logMessage;

        public DeviceCapk(
            [Named("TransactionService")] ITransactionService transactionService,
            [Named("TerminalService")] ITerminalService terminalService,
            [Named("MerchantService")] IMerchantService merchantService,
            StringBuilder logMessage)
            : base(terminalService, logMessage)
        {
            _transactionService = transactionService;
            _terminalService = terminalService;
            _merchantService = merchantService;
            _logMessage = logMessage;
        }

        public string CapkFile(TransactionRequest tr, int transId, string clientRef, string xmlSerializedTransResp)
        {
            if (string.IsNullOrEmpty(xmlSerializedTransResp))
            {
                xmlSerializedTransResp = SendCapkTransaction(tr, transId, clientRef, xmlSerializedTransResp);
                _logMessage.Append("SendcapkTransaction executed successfully " + transId);
            }
            else if (!string.IsNullOrEmpty(xmlSerializedTransResp))
            {
                xmlSerializedTransResp = SendCapkTransaction(tr, transId, clientRef, xmlSerializedTransResp);
            }
            return xmlSerializedTransResp;
        }

        public string SendCapkTransaction(TransactionRequest tr, int prevtransId, string clientRef, string xmlSerializedTransResp)
        {
            string XMLresponse = xmlSerializedTransResp;
            tr.TransactionType = "FileDownload";
            tr.RapidConnectAuthId = Convert.ToString(prevtransId);
            tr.LocalDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
            tr.TrnmsnDateTime = DateTime.UtcNow.ToString("yyyyMMddHHmmss");
            if (!string.IsNullOrEmpty(XMLresponse))
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(XMLresponse);
                string xmlString = CommonFunctions.GetXmlAsString(xmlDoc);
                var response = CommonFunctions.GetCapkResponse(xmlString);
                xmlDoc.LoadXml(response);
                var fbseq = xmlDoc.SelectSingleNode("AdminResponse/FileDLGrp/FBSeq");
                {
                    if (fbseq != null)
                    {
                        var fbseqPre = fbseq.InnerText;
                        int fbseqnext = int.Parse(fbseqPre);
                        fbseqnext = fbseqnext + 1;
                        string fbseqfinal = String.Format("{0:0000}", fbseqnext);
                        tr.FbSeq = fbseqfinal;
                    }
                }
                var reqFileOff = xmlDoc.SelectSingleNode("AdminResponse/FileDLGrp/NextFileDLOffset");
                {
                    if (reqFileOff != null)
                    { tr.ReqFileOff = reqFileOff.InnerText; }
                }
            }

            CreateTransactionRequest(tr);
            string xmlSerializedTimeoutReq = GetXmlData();
            int transId;

            #region Save transaction into MTDTransaction Table
            _logMessage.AppendLine("calling CapkXmlToTransactionDto takes xmlSerializedTransReq, tr");
            MTDTransactionDto transactionDto = CommonFunctions.CapkXmlToTransactionDto(tr);
            _logMessage.AppendLine("Inserting Transaction.");
            transId = _transactionService.Add(transactionDto);
            _logMessage.AppendLine("Update stan parameters" + transactionDto.Stan + "--" + transactionDto.TransRefNo + "--" + tr.TerminalId + "--" + tr.MtdMerchantId);
            _terminalService.UpdateStan(transactionDto.Stan, transactionDto.TransRefNo, tr.TerminalId, tr.MtdMerchantId);
            #endregion


            /* Send data using SOAP protocol to Datawire*/
            string xmlSerializedCapkTransResp = new SoapHandler().SendMessage(xmlSerializedTimeoutReq, clientRef, tr);
            _logMessage.AppendLine("xmlSerializedCapkTransResp is not null");

            MTDTransactionDto responsetransactionDto = CommonFunctions.XmlEmvCapkResponseToTransactionDto(
                xmlSerializedCapkTransResp, false, tr.TransType.ToString());

            _transactionService.Update(responsetransactionDto, transId, tr.RapidConnectAuthId);
            _logMessage.AppendLine("xmlSerializedCapkTransResp is not null updated");

            return xmlSerializedCapkTransResp;
        }

        public CapkData SendCapkTransactionNew(TransactionRequest tr, string clientRef, string xmlSerializedTransResp)
        {
            string XMLresponse = xmlSerializedTransResp;
            tr.TransactionType = "FileDownload";
            tr.RapidConnectAuthId = DateTime.Now.ToString("yyMdHHmmssff");
            tr.LocalDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
            tr.TrnmsnDateTime = DateTime.UtcNow.ToString("yyyyMMddHHmmss");
            if (!string.IsNullOrEmpty(XMLresponse))
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(XMLresponse);
                string xmlString = CommonFunctions.GetXmlAsString(xmlDoc);
                var response = CommonFunctions.GetCapkResponse(xmlString);
                xmlDoc.LoadXml(response);
                var fbseq = xmlDoc.SelectSingleNode("AdminResponse/FileDLGrp/FBSeq");
                {
                    if (fbseq != null)
                    {
                        var fbseqPre = fbseq.InnerText;
                        int fbseqnext = int.Parse(fbseqPre);
                        fbseqnext = fbseqnext + 1;
                        string fbseqfinal = String.Format("{0:0000}", fbseqnext);
                        tr.FbSeq = fbseqfinal;
                    }
                }
                var reqFileOff = xmlDoc.SelectSingleNode("AdminResponse/FileDLGrp/NextFileDLOffset");
                {
                    if (reqFileOff != null)
                    { tr.ReqFileOff = reqFileOff.InnerText; }
                }
            }

            CreateTransactionRequest(tr);
            string xmlSerializedTimeoutReq = GetXmlData();
            _logMessage.AppendLine("calling CapkXmlToTransactionDto takes xmlSerializedTransReq, tr");
            
            //int transId;
            MTDTransactionDto transactionDto = CommonFunctions.CapkXmlToTransactionDto(tr);
            _logMessage.AppendLine("Update stan parameters" + transactionDto.Stan + "--" + transactionDto.TransRefNo + "--" + tr.TerminalId + "--" + tr.MtdMerchantId);
            _terminalService.UpdateStan(transactionDto.Stan, transactionDto.TransRefNo, tr.TerminalId, tr.MtdMerchantId);
          
            /* Send data using SOAP protocol to Datawire*/
            string xmlSerializedCapkTransResp = new SoapHandler().SendMessage(xmlSerializedTimeoutReq, clientRef, tr);
            _logMessage.AppendLine("xmlSerializedCapkTransResp is not null");

            CapkData ck = new CapkData() { Request = xmlSerializedTimeoutReq, Response = xmlSerializedCapkTransResp };
            return ck;
        }
    }
}


