﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
   public class KapKeyFileVersionDto
    {
        public int Id { get; set; }
        public Nullable<System.DateTime> DownloadDate { get; set; }
        public string FileVersion { get; set; }
        public string XmlFile { get; set; }
    }
}
