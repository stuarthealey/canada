﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml;
using System.Text.RegularExpressions;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.Web.Models;
using MTData.Web.Resource;
using MTData.Web.ViewData;

enum EmailField
{
    CompanyName,
    CompanyPhone,
    CompanyAddress,
    CompanyTaxNumber,
    Driver,
    DriverTaxNumber,
    Extra,
    Fare,
    FederalTaxAmount,
    FederalTaxPercentage,
    Flagfall,
    Fleet,
    PickAddress,
    StateTaxAmount,
    StateTaxPercentage,
    Subtotal,
    SurchargeAmount,
    SurchargePercentage,
    TermsandConditions,
    Tip,
    Tolls,
    Total,
    Vehicle,
    Disclaimer,
    DestinationAddress,
};

namespace MTData.Web.Controllers
{
    public class TransactionController : Controller
    {
        private const string DeviceId = "08:08:C2:11:5E:D2";
        private const string Credit = "Credit";
        private const string Debit = "Debit";
        private const string Decline = "DECLINED-005";
        private const string Approved = "APPROVAL";
        private const string Sale = "Sale";
        private const string Ecommerce = "Ecommerce";
        private const string Void = "Void";
        private string Timezone = "UTC-11";
        private const string Keyed = "Keyed";
        private readonly ILogger _logger = new Logger();
        private StringBuilder _logMessage;
        private readonly IUserService _userService;
        private readonly ITransactionService _transactionService;
        private readonly IReportCommonService _reportcommonRepository;
        private readonly IDriverService _driverService;
        private readonly IFleetService _fleetService;
        private readonly IReceiptService _receiptService;
        private readonly ICommonService _commonService;
        private StringBuilder _logTracking;
        private bool ChargeBack = true;
        private readonly IVehicleService _vehicleService;
        private readonly ISubAdminService _subAdminService;
        private readonly IScheduleService _scheduleService;

        public TransactionController([Named("ReceiptService")] IReceiptService receiptService, [Named("FleetService")] IFleetService fleetService, [Named("DriverService")] IDriverService driverService, [Named("TransactionService")] ITransactionService transactionService,
            StringBuilder logMessage, [Named("ReportCommonService")] IReportCommonService reportcommonService, [Named("UserService")] IUserService userService, [Named("CommonService")] ICommonService commonService, [Named("VehicleService")] IVehicleService vehicleService, StringBuilder logTracking,
            [Named("SubAdminService")] ISubAdminService subAdminService, [Named("ScheduleService")] IScheduleService scheduleService)
        {
            _transactionService = transactionService;
            _logMessage = logMessage;
            _reportcommonRepository = reportcommonService;
            _driverService = driverService;
            _fleetService = fleetService;
            _receiptService = receiptService;
            _userService = userService;
            _commonService = commonService;
            _logTracking = logTracking;
            _vehicleService = vehicleService;
            _subAdminService = subAdminService;
            _scheduleService = scheduleService;
        }

        /// <summary>
        /// Purpose             :   To create new transaction
        /// Function Name       :   Create
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");

            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("index", "Member");
            
            TransactionRequest txnRequest = new TransactionRequest();
            var fkMerchantId = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;
            List<VehicleTxnListViewData> carList = new List<VehicleTxnListViewData>();
            List<DriverTxnListViewData> driverList = new List<DriverTxnListViewData>();
            
            if (fkMerchantId != null)
            {
                txnRequest.IsJobNoRequired = _transactionService.JobNumberRequired(Convert.ToInt32(fkMerchantId));
                int mid = (int)fkMerchantId;
                var dto = (SessionDto)Session["User"];
                int userType = dto.SessionUser.fk_UserTypeID;
                int userId = dto.SessionUser.UserID;
                int loggedbyUserType = dto.SessionUser.LogOnByUserType;
                int logOnByUserId = dto.SessionUser.LogOnByUserId;

                if (loggedbyUserType == 5) // For 5
                {
                    txnRequest.fleetlist = _userService.FleetList(mid).ToViewDataList();
                    IEnumerable<UserFleetDto> accessedFleet = _userService.GetUsersFleet(logOnByUserId);
                    txnRequest.fleetlist = txnRequest.fleetlist.Where(x => accessedFleet.Select(y => y.fk_FleetID).Contains((int)x.FleetID)).ToList();
                }
                else
                {
                    if (userType == 4)
                        txnRequest.fleetlist = _userService.UserFleetLists(userId).ToViewDataList();

                    if (userType == 2 || userType == 3)
                        txnRequest.fleetlist = _userService.FleetList(mid).ToViewDataList();
                }

                txnRequest.VehicleList = carList;
                txnRequest.DriverList = driverList;

                TxnTaxSurchargesDto ttSurchage = _transactionService.GetTaxSurchages(mid);
                var lowTip = ttSurchage.TipPerLow;
                var midTip = ttSurchage.TipPerMedium;
                var highTip = ttSurchage.TipPerHigh;

                txnRequest.SurchargeType = 1;
                txnRequest.SurchargeFixed = 0;
                txnRequest.SurchargePer = 1;
                txnRequest.SurMaxCap = 0;
                txnRequest.FeeType = 1;
                txnRequest.FeeFixed = 0;
                txnRequest.FeePer = 1;
                txnRequest.FeeMaxCap = 0;
                txnRequest.IsTaxInclusive = ttSurchage.IsTaxInclusive;
                txnRequest.FedralTax = ttSurchage.FederalTaxRate;
                txnRequest.StateTax = ttSurchage.StateTaxRate;

                List<TipViewData> tvd = new List<TipViewData>();
                for (int s = 0; s <= 4; s++)
                {
                    TipViewData tview = new TipViewData();
                    if (s == 0)
                    {
                        tview.TipText = "--No Tip--";
                        tview.TipValue = 0;
                    }

                    if (s == 1)
                    {
                        tview.TipText = "Low Tip " + lowTip + " %";
                        tview.TipValue = lowTip;
                    }

                    if (s == 2)
                    {
                        tview.TipText = "Medium Tip " + midTip + " %";
                        tview.TipValue = midTip;
                    }

                    if (s == 3)
                    {
                        tview.TipText = "High Tip " + highTip + " %";
                        tview.TipValue = highTip;
                    }

                    if (s == 4)
                    {
                        tview.TipText = "Enter manually";
                        tview.TipValue = 0;
                    }

                    tvd.Add(tview);
                }

                txnRequest.TaxList = tvd;
                txnRequest.Currency = ConfigurationManager.AppSettings["Currency"].ToString();

                return View(txnRequest);
            }

            return RedirectToAction("Search", "Transaction");
        }

        /// <summary>
        /// Purpose             :   To update the response of the transaction into database
        /// Function Name       :   Update
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/2/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="viewData"></param>
        /// <param name="transId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return RedirectToAction("Search", "Transaction");
        }

        /// <summary>
        ///  Purpose            :   To fetch the transaction from database
        /// Function Name       :   Update
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/2/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="transId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Update(int transId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 4)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            _logMessage.Append("calling GetTransaction method of _transactionService sercive with transactionId is that is :" + transId);

            try
            {
                MTDTransactionDto mrTransaction = _transactionService.GetTransaction(transId);
                
                _logMessage.Append("calling ToViewData method to map TransactionViewData to TransactionDto and retrun TransactionViewData");
                
                MtdTransactionsViewData mvData = mrTransaction.ToViewData();

                _logMessage.Append("ToViewData method executed successfully");

                ViewData.Model = mvData;

                return View();
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            :   Searching the transaction 
        /// Function Name       :   Search
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/8/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Search()
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || Session["User"] == null) return RedirectToAction("Index", "Member");
            int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
            TxnSearch tvdData = new TxnSearch();
            tvdData.IsVarified = null;
            List<CarListViewData> carList = new List<CarListViewData>();
            List<DriverListViewData> driverList = new List<DriverListViewData>();
            
            var dto = (SessionDto)Session["User"];
            int userType = dto.SessionUser.fk_UserTypeID;
            int userId = dto.SessionUser.UserID;
            int logOnByUserId = dto.SessionUser.LogOnByUserId;
            int loggedbyUserType = dto.SessionUser.LogOnByUserType;

            if (loggedbyUserType == 5) // For 5
            {
                tvdData.fleetlist = _userService.FleetList(merchantId).ToViewDataList();
                IEnumerable<UserFleetDto> accessedFleet = _userService.GetUsersFleet(logOnByUserId);
                tvdData.fleetlist = tvdData.fleetlist.Where(x => accessedFleet.Select(y => y.fk_FleetID).Contains((int)x.FleetID)).ToList();
            }
            else
            {
                if (userType == 2 || userType == 3)
                    tvdData.fleetlist = _userService.FleetList(merchantId).ToViewDataList();

                if (userType == 1)
                    tvdData.fleetlist = _driverService.AllFleets().ToViewDataList();

                if (userType == 4)
                    tvdData.fleetlist = _userService.UserFleetLists(userId).ToViewDataList();
            }

            tvdData.DriverList = driverList;
            tvdData.CarList = carList;
            tvdData.fk_FleetId = 0;
            tvdData.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            tvdData.Currency = ConfigurationManager.AppSettings["Currency"].ToString();

            return View(tvdData);
        }

        /// <summary>
        ///  Purpose            :   Searching the transaction 
        /// Function Name       :   Search
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/8/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Search(TxnSearch tvd)
        {
            try
            {
                if (Session["User"] == null) return RedirectToAction("Index", "Member");
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
                var sessionData = ((SessionDto)Session["User"]);
                int userType = sessionData.SessionUser.fk_UserTypeID;
                int merchantIds = sessionData.SessionMerchant.MerchantID;

                int loggedbyUserType = sessionData.SessionUser.LogOnByUserType;
                if (loggedbyUserType == 5) // For 5
                {
                    int logOnByUserId = sessionData.SessionUser.LogOnByUserId;
                    tvd.fleetlist = _userService.FleetList(merchantIds).ToViewDataList();
                    IEnumerable<UserFleetDto> accessedFleet = _userService.GetUsersFleet(logOnByUserId);
                    tvd.fleetlist = tvd.fleetlist.Where(x => accessedFleet.Select(y => y.fk_FleetID).Contains((int)x.FleetID)).ToList();
                }
                else
                {
                    tvd.fleetlist = _driverService.AllFleets().ToViewDataList();
                }

                int fId = 0;
                if (tvd.fk_FleetId != null)
                    fId = (int)tvd.fk_FleetId;

                int? merchantId = null;
                var fkMerchantId = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;
                if (fkMerchantId != null)
                {
                    merchantId = fkMerchantId;
                    Timezone = _commonService.MerchantTimeZone((int)merchantId);
                }
                else
                {
                    TimeZoneInfo timeZone = TimeZoneInfo.Local;
                    Timezone = timeZone.StandardName;
                }

                tvd.CarList = _commonService.GetCars((int)fId).ToViewDataList();
                tvd.DriverList = _commonService.GetDrivers((int)fId).ToViewDataList();

                if (tvd.startDateString != null && tvd.endDateString != null)
                {
                    if (tvd.startDateString.Length > 0 && tvd.endDateString.Length > 0)
                    {
                        bool isDate = false;
                        Regex r = new Regex(@"\d\d-\d\d-\d\d\d\d\s\d\d:\d\d");

                        if (((r.Match(tvd.startDateString).Success) && (r.Match(tvd.endDateString).Success)))
                        {
                            DateTime resultDate;
                            isDate = DateTime.TryParseExact(tvd.startDateString, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);

                            if (!isDate)
                                return View(tvd);
                            else
                                tvd.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);    //D#6082 Handle Timezones

                            isDate = DateTime.TryParseExact(tvd.endDateString, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return View(tvd);
                            else
                                tvd.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);      //D#6082 Handle Timezones
                        }
                    }
                }

                if (tvd.fk_FleetId == null && string.IsNullOrEmpty(tvd.VNumber) && string.IsNullOrEmpty(tvd.DNumber) && string.IsNullOrEmpty(tvd.TType) && string.IsNullOrEmpty(tvd.CrdType) && tvd.startDate == null && tvd.endDate == null && tvd.Id == null && string.IsNullOrEmpty(tvd.ExpiryDate) && string.IsNullOrEmpty(tvd.FFD) && string.IsNullOrEmpty(tvd.LFD) && tvd.MinAmt == null && tvd.MaxAmt == null && tvd.JobNumber == null && tvd.AId == null && tvd.AddRespData == null && tvd.Industry == null && string.IsNullOrEmpty(tvd.SerialNo) && string.IsNullOrEmpty(tvd.TerminalId))
                {
                    ViewBag.Message = MtdataResource.Please_Select_field;
                    return View(tvd);
                }

                if (!String.IsNullOrEmpty(tvd.ExpiryDate))
                    tvd.ExpiryDate = StringExtension.DateFormat(tvd.ExpiryDate);

                MtdTransactionsViewData td = new MtdTransactionsViewData
                {
                    PageSize = 0,
                    ExpiryDate = tvd.ExpiryDate,
                    VehicleNo = tvd.VNumber,
                    DriverNo = tvd.DNumber,
                    CardType = tvd.CrdType,
                    startDate = tvd.startDate,
                    endDate = tvd.endDate,
                    Id = tvd.Id,
                    TxnType = tvd.TType,
                    FirstFourDigits = tvd.FFD,
                    LastFourDigits = tvd.LFD,
                    MinAmount = tvd.MinAmt,
                    MaxAmount = tvd.MaxAmt,
                    AddRespData = tvd.AddRespData,
                    AuthId = tvd.AId,
                    MerchantId = merchantId,
                    fk_FleetId = tvd.fk_FleetId,
                    EntryMode = tvd.Industry,
                    IsVarified = tvd.IsVarified,
                    SerialNo = tvd.SerialNo,
                    TerminalId = tvd.TerminalId,
                    TransNote = tvd.TransNote       // Ensure TransNote included
                };

                MTDTransactionDto transactionDto = td.ToDTO();
                int count = _transactionService.GetTransactionCount(transactionDto);
                transactionDto.PageSize = count;
                IEnumerable<MTDTransactionDto> stvd = _transactionService.GetTransaction(transactionDto);

                int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                var userfleet = _userService.GetUsersFleet(userId);
                if (userType == 4)
                {
                    stvd = stvd.Where(x => userfleet.Select(y => y.fk_FleetID).Contains((int)x.fk_FleetId)).ToList();
                }

                IEnumerable<SearchTransactionViewData> mvData = stvd.ToViewData();
                var transactionViewDatas = mvData as SearchTransactionViewData[] ?? mvData.ToArray();

                if (transactionViewDatas.ToList().Count == 0)
                {
                    ViewBag.Message = MtdataResource.No_such_record_exist;  //"No such records exist";
                    return View(tvd);
                }

                string culture = ConfigurationManager.AppSettings["CultureInfo"].ToString();
                string currency = ConfigurationManager.AppSettings["Currency"].ToString();

                using (StringWriter output = new StringWriter())
                {
                    output.WriteLine("\"Id\",\"AuthCode\",\"Vehicle\",\"Driver\",\"Date\",\"Credit/Debit\",\"Transaction Type\",\"Tech Fee(" + currency + ")\",\"Fleet Fee(" + currency + ")\",\"Fee(" + currency + ")\",\"Surcharge(" + currency + ")\",\"Amount(" + currency + ")\",\"CardType\",\"Entry Mode\",\"Approval\"");

                    foreach (SearchTransactionViewData item in transactionViewDatas)
                    {
                        DateTime dt1 = TimeZoneInfo.ConvertTime(Convert.ToDateTime(item.TxnDate), TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(Timezone));
                        decimal tFee = Convert.ToDecimal(item.TechFee);
                        decimal fFee = Convert.ToDecimal(item.FleetFee);
                        decimal fee = Convert.ToDecimal(item.Fee);
                        decimal sur = Convert.ToDecimal(item.Surcharge);
                        decimal amt = Convert.ToDecimal(item.Amount);

                        output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\"",
                            item.Id, item.AuthId, item.VehicleNo, item.DriverNo, dt1.ToString("G", CultureInfo.GetCultureInfo(culture)), item.PaymentType, item.TxnType,
                            tFee.ToString("C", CultureInfo.GetCultureInfo(culture)), fFee.ToString("C", CultureInfo.GetCultureInfo(culture)),
                            fee.ToString("C", CultureInfo.GetCultureInfo(culture)), sur.ToString("C", CultureInfo.GetCultureInfo(culture)),
                            amt.ToString("C", CultureInfo.GetCultureInfo(culture)), item.CardType, item.EntryMode, item.AddRespData);
                    }

                    _logTracking.Append("Transaction Report Excel file Downloaded by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Transaction_Report.csv");
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View(tvd);
        }

        /// <summary>
        ///  Purpose            :   Technology fee detail 
        /// Function Name       :   TechDetail
        /// Created By          :   Naveen Kumar
        /// Created On          :   10/07/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult TechDetail()
        {
            try
            {
                if (Session["User"] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("Index", "Member");
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                    return RedirectToAction("index", "Member");

                List<FleetListViewData> ob = new List<FleetListViewData>();
                var driveTrx = new TechDetailViewData { MerchantList = MerchantList(), FleetList = ob };
                return View(driveTrx);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :  Technology detail report
        /// Function Name       :   TechDetail
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/19/2015
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult TechDetail(TechDetailViewData tvd)
        {
            if (Session["User"] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

            if (tvd.FleetID == 0)
                tvd.FleetID = null;

            if (tvd.transaction.DriverNo == "--Select Driver--")
                tvd.transaction.DriverNo = null;

            if (tvd.transaction.VehicleNo == "--Select Car--")
                tvd.transaction.VehicleNo = null;

            MtdTransactionsViewData td = new MtdTransactionsViewData();
            td.FleetId = tvd.FleetID;
            td.PageSize = 0;
            td.MerchantId = tvd.MerchantID;
            td.DriverNo = tvd.transaction.DriverNo;
            td.VehicleNo = tvd.transaction.VehicleNo;
            td.startDate = tvd.transaction.startDate;
            td.endDate = tvd.transaction.endDate;

            _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");

            MTDTransactionDto transactionDto = td.ToDTO();

            _logMessage.Append("ToDTO method executed successfully");
            _logMessage.Append("calling GetDriverCount method of _transactionService");
            
            int count = _transactionService.GetTechFeeDetailCount(transactionDto);
            transactionDto.PageSize = count;

            if (count == 0)
            {
                List<FleetListViewData> ob = new List<FleetListViewData>();
                tvd.MerchantList = MerchantList();
                tvd.FleetList = ob;
                ViewBag.Message = MtdataResource.No_such_record_exist;
                return View(tvd);
            }

            _logMessage.Append("GetTechFeeCount method of _transactionService executed Successfully count = " + count);
            _logMessage.Append("calling GetTechnologyDetail method of _transactionService takes transaction dto");

            IEnumerable<TechnologyDetailReportDto> stvd = _transactionService.GetTechnologyDetail(transactionDto);

            _logMessage.Append("GetTechnologyDetail method of _transactionService executed successfully");
            _logMessage.Append("assigning result in searchTransactionDtos from stvd");

            var searchTransactionDtos = stvd as TechnologyDetailReportDto[] ?? stvd.ToArray();

            _logMessage.Append("result in searchTransactionDtos from stvd successfully executed");
            
            using (StringWriter output = new StringWriter())
            {
                string dateFromHeading = "DATE FROM :" + tvd.startDateString;
                string dateToHeading = "DATE TO :" + tvd.endDateString;
                output.WriteLine(dateFromHeading.PadLeft(80));
                output.WriteLine(dateToHeading.PadLeft(80));
                string merchants = "";
                string fleets = "";
                List<string> listMerchant = new List<string>();
                List<string> listFleet = new List<string>();

                foreach (TechnologyDetailReportDto item in searchTransactionDtos)
                {
                    listMerchant.Add(item.Merchant);
                    listFleet.Add(item.FleetName);
                }

                foreach (string merchant in listMerchant.Distinct())
                {
                    if (string.IsNullOrEmpty(merchants))
                        merchants = merchant;
                    else
                        merchants = merchants + ", " + merchant;
                }

                foreach (string fleet in listFleet.Distinct())
                {
                    if (string.IsNullOrEmpty(fleets))
                        fleets = fleet;
                    else
                        fleets = fleets + ", " + fleet;
                }

                string merchantList = "MERCHANT :" + merchants;
                string fleetList = "FLEET:" + fleets;
                output.WriteLine(merchantList);
                output.WriteLine(fleetList);
                output.WriteLine();
                output.WriteLine();

                string culture = ConfigurationManager.AppSettings["CultureInfo"].ToString();

                //First line for column names
                output.WriteLine("\"Trans Id\",\"Date\",\"Fleet\",\"Vehicle\",\"Driver\",\"Fare Amt\",\"Card Type \",\"Tech Fee\",\"Fleet Fee\",\"Surcharge\",\"Fee\",\"Auth Code\"");
                foreach (TechnologyDetailReportDto item in searchTransactionDtos)
                {
                    DateTime dt1 = TimeZoneInfo.ConvertTime(Convert.ToDateTime(item.TxnDate), TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(Timezone));

                    output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\"", 
                        item.TransId, 
                        dt1.ToString("G", CultureInfo.GetCultureInfo(culture)),
                        item.FleetName, item.VehicleNo, item.DriverNo, item.Amount, item.CardType, item.TechFee, item.FleetFee, item.Surcharge, item.Booking_Fee, item.AuthId);
                }

                _logTracking.Append("TransactionController.TechDetail() Driver Report Excel file Downloaded by " + (((SessionDto)Session["User"]).SessionUser.Email) + " at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());

                return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Technology_Detail_Report.csv");
            }
        }

        /// <summary>
        ///  Purpose            :   Technology report view
        /// Function Name       :   TechSummary
        /// Created By          :   Naveen Kumar
        /// Created On          :   6/10/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult TechSummary()
        {
            try
            {
                if (Session["User"] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("Index", "Member");
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                    return RedirectToAction("index", "Member");

                List<FleetListViewData> ob = new List<FleetListViewData>();
                var driveTrx = new TechSummaryViewData { MerchantList = MerchantList(), FleetList = ob };

                return View(driveTrx);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :  Technology Summary Report
        /// Function Name       :   TechSummary
        /// Created By          :  Naveen Kumar
        /// Created On          :   1/19/2015
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult TechSummary(TechSummaryViewData tvd)
        {
            if (Session["User"] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

            if (tvd.FleetID == 0)
                tvd.FleetID = null;

            // Now we know the merchant, hopefully, we can get a timezone.
            if (tvd.MerchantID != null)
                Timezone = _commonService.MerchantTimeZone((int)tvd.MerchantID);
            else
                Timezone = TimeZoneInfo.Local.StandardName;

            // We have to do the date Timezone manipulation after getting the Merchant details.
            if (tvd.startDateString != null && tvd.endDateString != null)
            {
                bool isDate = false;
                DateTime resultDate;
                string sDate = tvd.startDateString + " 00:00";
                string eDate = tvd.endDateString + " 23:59";

                isDate = DateTime.TryParseExact(sDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                if (isDate)
                    tvd.transaction.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones

                isDate = DateTime.TryParseExact(eDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                if (isDate)
                    tvd.transaction.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
            }

            MtdTransactionsViewData td = new MtdTransactionsViewData();
            td.FleetId = tvd.FleetID;
            td.PageSize = 0;
            td.MerchantId = tvd.MerchantID;
            td.startDate = tvd.transaction.startDate;
            td.endDate = tvd.transaction.endDate;

            _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");
            
            MTDTransactionDto transactionDto = td.ToDTO();
            
            _logMessage.Append("ToDTO method executed successfully");
            _logMessage.Append("calling GetDriverCount method of _transactionService");
            
            int count = _transactionService.GetTechFeeCount(transactionDto);            
            transactionDto.PageSize = count;

            if (count == 0)
            {
                List<FleetListViewData> ob = new List<FleetListViewData>();
                tvd.MerchantList = MerchantList();
                tvd.FleetList = ob;
                ViewBag.Message = MtdataResource.No_such_record_exist;
                return View(tvd);
            }

            _logMessage.Append("GetTechFeeCount method of _transactionService executed Successfully count = " + count);
            _logMessage.Append("calling GetTechnologySummary method of _transactionService takes transaction dto");
            
            IEnumerable<TechnologySummaryReportDto> stvd = _transactionService.GetTechnologySummary(transactionDto);
            
            _logMessage.Append("GetTechnologySummary method of _transactionService executed successfully");
            _logMessage.Append("assigning result in searchTransactionDtos from stvd");
            
            var searchTransactionDtos = stvd as TechnologySummaryReportDto[] ?? stvd.ToArray();
            
            _logMessage.Append("result in searchTransactionDtos from stvd successfully executed");
            
            using (StringWriter output = new StringWriter())
            {
                string dateFromHeading = "DATE FROM :" + tvd.startDateString;
                string dateToHeading = "DATE TO :" + tvd.endDateString;
                output.WriteLine(dateFromHeading.PadLeft(80));
                output.WriteLine(dateToHeading.PadLeft(80));
                string merchants = "";
                string fleets = "";
                List<string> listMerchant = new List<string>();
                List<string> listFleet = new List<string>();

                foreach (TechnologySummaryReportDto item in searchTransactionDtos)
                {
                    listMerchant.Add(item.Merchant);
                    listFleet.Add(item.FleetName);
                }

                foreach (string merchant in listMerchant.Distinct())
                {
                    if (string.IsNullOrEmpty(merchants))
                        merchants = merchant;
                    else
                        merchants = merchants + ", " + merchant;
                }

                foreach (string fleet in listFleet.Distinct())
                {
                    if (string.IsNullOrEmpty(fleets))
                        fleets = fleet;
                    else
                        fleets = fleets + ", " + fleet;
                }

                string merchantList = "MERCHANT :" + merchants;
                string fleetList = "FLEET:" + fleets;
                output.WriteLine(merchantList);
                output.WriteLine(fleetList);
                output.WriteLine();
                output.WriteLine();

                //First line for column names
                output.WriteLine("\"Fleet\",\"Vehicle Trans\",\"Auto Trans\",\"Manual Trans\",\"Total Chargebacks\",\"Total Voids\",\"Total Trans\",\"Tech Fee\",\"Fleet Fee\",\"Surcharge\",\"BookingFee\",\"Amount\"");
                foreach (TechnologySummaryReportDto item in searchTransactionDtos)
                {
                    output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\"", 
                        item.FleetName, item.CountVehicle, item.AutomaticTrans, item.ManualTrans, item.CountRefund, item.CountVoid, item.TotalTrans, item.TechFee, item.FleetFee, item.Surcharge, item.Booking_Fee, item.TotalAmt);
                }

                _logTracking.Append("TransactionController.TechSummary() Driver Report Excel file Downloaded by " + (((SessionDto)Session["User"]).SessionUser.Email) + " at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                
                return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Technology_Summary_Report.csv");
            }
        }

        /// <summary>
        ///  Purpose            :  Fetching the Driverreport on the basis of its ID from the database.
        /// Function Name       :   DriverReport
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/19/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public ActionResult DriverReport()
        {
            try
            {
                if (Session["User"] == null) return RedirectToAction("Index", "Member");
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                    return RedirectToAction("index", "Member");

                var dto = (SessionDto)Session["User"];
                var merchantId = Convert.ToInt32(dto.SessionUser.fk_MerchantID);
                int userTypeId = dto.SessionUser.fk_UserTypeID;
                int userId = dto.SessionUser.UserID;
                int logOnByUserId = dto.SessionUser.LogOnByUserId;
                int loggedbyUserType = dto.SessionUser.LogOnByUserType;
                
                if (userTypeId == 1)
                {
                    List<FleetListViewData> ob = new List<FleetListViewData>();
                    var driveTrx = new DriverTransaction { MerchantList = MerchantList(), FleetList = ob };
                    return View(driveTrx);
                }
                else
                {
                    DriverTransaction driveTrx = new DriverTransaction();
                    if (loggedbyUserType == 5) // For 5
                    {
                        IEnumerable<FleetViewData> fleetDto = _userService.FleetList(merchantId).ToViewDataList();
                        IEnumerable<UserFleetDto> accessedFleet = _userService.GetUsersFleet(logOnByUserId);
                        List<FleetListViewData> fleets = new List<FleetListViewData>();
                        foreach (FleetViewData fleet in fleetDto)
                        {
                            FleetListViewData fd = new FleetListViewData();
                            fd.FleetName = fleet.FleetName;
                            fd.FleetID = fleet.FleetID;
                            fleets.Add(fd);
                        }

                        driveTrx.FleetList = fleets;
                        driveTrx.FleetList = driveTrx.FleetList.Where(x => accessedFleet.Select(y => y.fk_FleetID).Contains((int)x.FleetID)).ToList();
                    }
                    else
                    {
                        if (userTypeId == 4)
                        {
                            IEnumerable<FleetViewData> fleetDto = _userService.UserFleetLists(userId).ToViewDataList();
                            List<FleetListViewData> fleets = new List<FleetListViewData>();
                            foreach (FleetViewData fleet in fleetDto)
                            {
                                FleetListViewData fd = new FleetListViewData();
                                fd.FleetName = fleet.FleetName;
                                fd.FleetID = fleet.FleetID;
                                fleets.Add(fd);
                            }

                            driveTrx.FleetList = fleets;
                        }

                        if (userTypeId == 2 || userTypeId == 3)
                        {
                            IEnumerable<FleetViewData> fleetDto = _userService.FleetList(merchantId).ToViewDataList();
                            List<FleetListViewData> fleets = new List<FleetListViewData>();
                            foreach (FleetViewData fleet in fleetDto)
                            {
                                FleetListViewData fd = new FleetListViewData();
                                fd.FleetName = fleet.FleetName;
                                fd.FleetID = fleet.FleetID;
                                fleets.Add(fd);
                            }

                            driveTrx.FleetList = fleets;
                        }
                    }

                    List<CarListViewData> carList = new List<CarListViewData>();
                    driveTrx.CarList = carList;
                    return View(driveTrx);
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :  Searching Driverreport 
        /// Function Name       :   DriverReport
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/19/2015
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DriverReport(DriverTransaction tvd)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

            int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
            if (userType == 1)
                tvd.MerchantID = tvd.MerchantID;
            else
                tvd.MerchantID = (int)(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);

            // Now we know the merchant, hopefully, we can get a timezone.
            if (tvd.MerchantID != null)
                Timezone = _commonService.MerchantTimeZone((int)tvd.MerchantID);
            else
                Timezone = TimeZoneInfo.Local.StandardName;

            // We have to do the date Timezone manipulation after getting the Merchant details.
            if (tvd.startDateString != null && tvd.endDateString != null)
            {
                bool isDate = false;
                DateTime resultDate;
                string sDate = tvd.startDateString + " 00:00";
                string eDate = tvd.endDateString + " 23:59";

                isDate = DateTime.TryParseExact(sDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                if (isDate)
                    tvd.transaction.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);    //D#6082 Handle Timezones

                isDate = DateTime.TryParseExact(eDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                if (isDate)
                    tvd.transaction.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);      //D#6082 Handle Timezones
            }

            if (tvd.transaction.DriverNo == "--Please Select a Driver--")
                tvd.transaction.DriverNo = null;
            
            if (tvd.FleetID == 0)
                tvd.FleetID = null;

            MtdTransactionsViewData td = new MtdTransactionsViewData();

            td.FleetId = tvd.FleetID;
            td.PageSize = 0;
            td.MerchantId = tvd.MerchantID;
            td.VehicleNo = tvd.transaction.VehicleNo;
            td.DriverNo = tvd.transaction.DriverNo;
            td.startDate = tvd.transaction.startDate;
            td.endDate = tvd.transaction.endDate;

            _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");

            MTDTransactionDto transactionDto = td.ToDTO();

            _logMessage.Append("ToDTO method executed successfully");
            _logMessage.Append("calling GetDriverCount method of _transactionService");

            int count = _transactionService.GetDriverCount(transactionDto);
            transactionDto.PageSize = count;

            _logMessage.Append("GetDriverCount method of _transactionService executed Successfully count = " + count);
            _logMessage.Append("calling GetDriver method of _transactionService takes transaction dto");

            IEnumerable<DriverTransactionResult> stvd = _transactionService.GetDriver(transactionDto);

            int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
            var userfleet = _userService.GetUsersFleet(userId);
            if (userType == 4)
            {
                stvd = stvd.Where(x => userfleet.Select(y => y.fk_FleetID).Contains((int)x.FleetID)).ToList();
            }

            _logMessage.Append("GetDriver method of _transactionService executed successfully");
            _logMessage.Append("assigning result in searchTransactionDtos from stvd");

            var searchTransactionDtos = stvd as DriverTransactionResult[] ?? stvd.ToArray();

            _logMessage.Append("result in searchTransactionDtos from stvd successfully executed");

            using (StringWriter output = new StringWriter())
            {
                //First line for column names
                output.WriteLine("\"Merchant Name\",\"Fleet\",\"Driver Name\",\"Driver No\",\"Number Of Keyed\",\"Keyed Sale Amt\",\"No.Of Swiped\",\"Swiped Amt\",\"Tech Fee\",\"Fleet Fee\",\"Fee\",\"Surcharge\",\"Total Sale\",\"Total  Amount\"");

                foreach (DriverTransactionResult item in searchTransactionDtos)
                {
                    output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\"", item.MerchantName, item.Fleet, item.DriverName, item.DriverNo, item.NumberOfKeyed, String.Format("{0:C2}", item.KeyedSaleAmount), item.NumberOfSwiped, String.Format("{0:C2}", item.SwipedSaleAmount),
                        String.Format("{0:C2}", item.TechFee), String.Format("{0:C2}", item.SumFleetFee), String.Format("{0:C2}", item.Fee), String.Format("{0:C2}", item.SumSurcharge), item.TotalNumberOfSale, String.Format("{0:C2}", item.TotalSaleAmount));
                }

                _logTracking.Append("TransactionController.DriverReport() Driver Report Excel file Downloaded by " + (((SessionDto)Session["User"]).SessionUser.Email) + " at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());

                return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Driver_Transaction_Report.csv");
            }
        }

        //<summary>
        //Purpose             :   Page indexing to display manual transaction
        //Function Name       :   TransactionListByFilter
        //Created By          :   Madhuri Tanwar
        //Created On          :   2/9/2015
        //</summary>
        //<param name="searchCriteria"></param>
        //<param name="jtStartIndex"></param>
        //<param name="jtPageSize"></param>
        //<param name="jtSorting"></param>
        //<returns></returns>
        [HttpPost]
        public JsonResult TransactionListByFilter(SearchParameters searchCriteria, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                TxnSearch txn = new TxnSearch
                {
                    fk_FleetId = searchCriteria.fleetId,
                    VNumber = searchCriteria.VNumber,
                    DNumber = searchCriteria.DNumber,
                    TType = searchCriteria.TType,
                    Industry = searchCriteria.Industry,
                    CrdType = searchCriteria.CrdType,
                    Id = searchCriteria.Id,
                    AId = searchCriteria.AId,
                    FFD = searchCriteria.FFD,
                    LFD = searchCriteria.LFD,
                    startDate = searchCriteria.startDate,
                    endDate = searchCriteria.endDate,
                    ExpiryDate = searchCriteria.ExpiryDate,
                    MinAmt = searchCriteria.MinAmt,
                    MaxAmt = searchCriteria.MaxAmt,
                    JobNumber = searchCriteria.JobNumber,
                    AddRespData = searchCriteria.AddRespData,
                    PageSize = searchCriteria.PageSize,
                    IsVarified = searchCriteria.IsVarified,
                    TerminalId = searchCriteria.TerminalId,
                    SerialNo = searchCriteria.SerialNo,
                    startDateString = searchCriteria.startDateString,
                    endDateString = searchCriteria.endDateString,
                };

                if (searchCriteria.approved == true)
                    txn.Approval = 1;
                else if (searchCriteria.rejected == true)
                    txn.Approval = 2;
                else if (searchCriteria.approved == false && searchCriteria.rejected == false)
                    txn.Approval = 3;
                if (searchCriteria.IsVarifiedYes == true)
                    txn.Varify = 1;
                else if (searchCriteria.IsVarifiedNo == true)
                    txn.Varify = 2;
                else if (searchCriteria.IsVarifiedBoth == true)
                    txn.Varify = 3;
                else if (searchCriteria.IsVarifiedYes == false && searchCriteria.IsVarifiedNo == false && searchCriteria.IsVarifiedBoth == false)
                    txn.Varify = 4;

                Session["BackData"] = txn;

                IEnumerable<SearchTransactionDto> list = new List<SearchTransactionDto>();
                if (searchCriteria.approved == null && searchCriteria.rejected == null)
                {
                    searchCriteria.approved = false;
                    searchCriteria.rejected = false;
                }

                if (searchCriteria.IsVarifiedYes == (bool)false && searchCriteria.IsVarifiedNo == (bool)false && searchCriteria.IsVarifiedBoth == (bool)false)
                {
                    searchCriteria.IsVarified = null;
                }
                else
                {
                    if (searchCriteria.IsVarifiedYes == true)
                        searchCriteria.IsVarified = true;
                    
                    if (searchCriteria.IsVarifiedNo == true)
                        searchCriteria.IsVarified = false;

                    if (searchCriteria.IsVarifiedBoth == true)
                        searchCriteria.IsVarified = null;
                }

                int? merchantId = null;
                var sessionData = ((SessionDto)Session["User"]);        //zzz This comes back as NULL when the session has timed out.  How to handle and send back to Login page??? //SH 20161130
                var fkMerchantId = sessionData.SessionUser.fk_MerchantID;
                if (fkMerchantId != null)
                {
                    merchantId = fkMerchantId;
                    Timezone = _commonService.MerchantTimeZone((int)merchantId);
                    
                    int userId = sessionData.SessionUser.UserID;
                    var roles = _userService.GetUserRoles(userId).ToViewDataList();

                    if (roles.Any(x => x.RoleId == 1))
                        ChargeBack = true;
                    else
                        ChargeBack = false;
                }
                else
                {
                    // Check if we have a fleet specified. If there is a FleetID, then we can find the MerchantID
                    // for that fleet, and then get the Timezone for that Merchant.
                    var fleetdto = _fleetService.GetFleet((int)searchCriteria.fleetId);
                    if (fleetdto != null)
                        Timezone = _commonService.MerchantTimeZone((int)fleetdto.fk_MerchantID);
                    else
                        Timezone = TimeZoneInfo.Local.StandardName;
                }

                if (searchCriteria.startDateString != null && searchCriteria.endDateString != null)
                {
                    //D#6082 Fixed incorrect Timezone handling.
                    bool isDate = false;
                    DateTime resultDate;
                    isDate = DateTime.TryParseExact(searchCriteria.startDateString, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                    if (isDate)
                        searchCriteria.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);
                    else
                        return Json(new { Result = "OK", TotalRecordCount = 0, Records = list });

                    isDate = DateTime.TryParseExact(searchCriteria.endDateString, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                    if (isDate)
                        searchCriteria.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);
                    else
                        return Json(new { Result = "OK", TotalRecordCount = 0, Records = list });
                }

                if (searchCriteria.fleetId == null && string.IsNullOrEmpty(searchCriteria.VNumber) && string.IsNullOrEmpty(searchCriteria.DNumber) && searchCriteria.approved == false && searchCriteria.rejected == false && searchCriteria.startDate == null && searchCriteria.endDate == null && searchCriteria.Id == null && searchCriteria.TType == null && string.IsNullOrEmpty(searchCriteria.AId) && searchCriteria.Industry == null && string.IsNullOrEmpty(searchCriteria.ExpiryDate) && string.IsNullOrEmpty(searchCriteria.FFD) && string.IsNullOrEmpty(searchCriteria.LFD) && string.IsNullOrEmpty(searchCriteria.CrdType) && searchCriteria.MinAmt == null && searchCriteria.MaxAmt == null && string.IsNullOrEmpty(searchCriteria.JobNumber) && string.IsNullOrEmpty(searchCriteria.TerminalId) && string.IsNullOrEmpty(searchCriteria.SerialNo))
                    return Json(new { Result = "OK", TotalRecordCount = 0, Records = list });

                if (sessionData == null)
                {
                    List<SearchTransactionDto> listd = new List<SearchTransactionDto>();
                    SearchTransactionDto st = new SearchTransactionDto();
                    st.SourceId = "-56";
                    listd.Add(st);
                    return Json(new { Result = "OK", TotalRecordCount = 0, Records = listd });
                }

                string gatewayMessage = "";
                if (searchCriteria.rejected != null && (bool)searchCriteria.rejected)
                    gatewayMessage = Decline;  //  "Decline";

                if (searchCriteria.approved != null && (bool)searchCriteria.approved)
                    gatewayMessage = Approved;   //"Approved";

                string modes = "";

                if (!string.IsNullOrEmpty(searchCriteria.Industry))
                {
                    switch (searchCriteria.Industry)
                    {
                        case "Manual":
                            modes = "Ecommerce";
                            break;
                        case "Swipe":
                            modes = "Retail";
                            break;
                        case "Chip":
                            modes = "Chip";
                            break;
                        case "Pin":
                            modes = "Pin";
                            break;
                    }
                }
                
                if (!String.IsNullOrEmpty(searchCriteria.ExpiryDate))
                    searchCriteria.ExpiryDate = StringExtension.DateFormat(searchCriteria.ExpiryDate);

                MtdTransactionsViewData td = new MtdTransactionsViewData
                {
                    ExpiryDate = searchCriteria.ExpiryDate,
                    VehicleNo = searchCriteria.VNumber,
                    DriverNo = searchCriteria.DNumber,
                    CardType = searchCriteria.CrdType,
                    startDate = searchCriteria.startDate,
                    endDate = searchCriteria.endDate,
                    Id = searchCriteria.Id,
                    AddRespData = gatewayMessage,
                    TxnType = searchCriteria.TType,
                    Industry = modes,
                    fk_FleetId = searchCriteria.fleetId, //entry mode treated as  fleetId
                    FirstFourDigits = searchCriteria.FFD,
                    LastFourDigits = searchCriteria.LFD,
                    MinAmount = searchCriteria.MinAmt,
                    MaxAmount = searchCriteria.MaxAmt,
                    AuthId = searchCriteria.AId,
                    MerchantId = merchantId,
                    JobNumber = searchCriteria.JobNumber,
                    PageSize = jtPageSize,
                    PageNumber = jtStartIndex,
                    Sorting = jtSorting,
                    EntryMode = searchCriteria.Industry,
                    IsVarified = searchCriteria.IsVarified,
                    SerialNo = searchCriteria.SerialNo,
                    TerminalId = searchCriteria.TerminalId
                };

                _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");
                
                int userType = sessionData.SessionUser.fk_UserTypeID;
                int count = 0;
                MTDTransactionDto transactionDto = td.ToDTO();
                IEnumerable<MTDTransactionDto> stvd = new List<MTDTransactionDto>();

                if (userType == 1)  //Admin 
                {
                    _logMessage.Append("ToDTO method executed successfully");
                    _logMessage.Append("calling GetTransactionCount method of _transactionService");
                    
                    count = _transactionService.GetTransactionCount(transactionDto);
                    
                    _logMessage.Append("GetTransactionCount method of _transactionService executed Successfully count = " + count);

                    stvd = _transactionService.GetTransaction(transactionDto);
                    stvd = stvd.Select(c => { c.IsChargeBackAllow = ChargeBack; return c; });
                    stvd = stvd.Select(c => { c.AddRespData = c.AddRespData != null && c.AddRespData.Length > 12 ? c.AddRespData.Substring(0, 12).Insert(12, "...") : c.AddRespData; return c; });

                    //D#6082 Display transaction date\time in Merchant local timezone, not server timezone.
                    stvd = stvd.Select(c => { c.StringDateTime = c.TxnDate != null 
                        ? TimeZoneInfo.ConvertTime(Convert.ToDateTime(c.TxnDate), TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(Timezone)).ToString("G", CultureInfo.GetCultureInfo(ConfigurationManager.AppSettings["CultureInfo"].ToString()))
                        : TimeZoneInfo.ConvertTime(Convert.ToDateTime(c.TxnDate), TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(Timezone)).ToString("G", CultureInfo.GetCultureInfo(ConfigurationManager.AppSettings["CultureInfo"].ToString())); return c;
                    });
                }

                if (userType == 3 || userType == 2) //merchant and merchant admin db time
                {
                    _logMessage.Append("ToDTO method executed successfully");
                    _logMessage.Append("calling GetTransactionCount method of _transactionService");

                    count = _transactionService.GetTransactionCount(transactionDto);

                    _logMessage.Append("GetTransactionCount method of _transactionService executed Successfully count = " + count);

                    stvd = _transactionService.GetTransaction(transactionDto);
                    stvd = stvd.Select(c => { c.IsChargeBackAllow = ChargeBack; return c; });
                    stvd = stvd.Select(c => { c.AddRespData = c.AddRespData != null && c.AddRespData.Length > 12 ? c.AddRespData.Substring(0, 12).Insert(12, "...") : c.AddRespData; return c; });

                    //D#6082 Display transaction date\time in Merchant local timezone, not server timezone.
                    stvd = stvd.Select(c => { c.StringDateTime = c.TxnDate != null
                        ? TimeZoneInfo.ConvertTime(Convert.ToDateTime(c.TxnDate), TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(Timezone)).ToString("G", CultureInfo.GetCultureInfo(ConfigurationManager.AppSettings["CultureInfo"].ToString()))
                        : TimeZoneInfo.ConvertTime(Convert.ToDateTime(c.TxnDate), TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(Timezone)).ToString("G", CultureInfo.GetCultureInfo(ConfigurationManager.AppSettings["CultureInfo"].ToString())); return c;
                    });
                }

                if (userType == 4) //merchant user time zone db
                {
                    int userId = sessionData.SessionUser.UserID;

                    transactionDto.UserId = userId;
                    count = _subAdminService.GetTransactionCount(transactionDto);
                    
                    stvd = _subAdminService.GetTransaction(transactionDto);
                    stvd = stvd.Select(c => { c.IsChargeBackAllow = ChargeBack; return c; });
                    stvd = stvd.Select(c => { c.AddRespData = c.AddRespData != null && c.AddRespData.Length > 12 ? c.AddRespData.Substring(0, 12).Insert(12, "...") : c.AddRespData; return c; });

                    //D#6082 Display transaction date\time in Merchant local timezone, not server timezone.
                    stvd = stvd.Select(c =>
                    {
                        c.StringDateTime = c.TxnDate != null 
                        ? TimeZoneInfo.ConvertTime(Convert.ToDateTime(c.TxnDate), TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(Timezone)).ToString("G", CultureInfo.GetCultureInfo(ConfigurationManager.AppSettings["CultureInfo"].ToString()))
                        : TimeZoneInfo.ConvertTime(Convert.ToDateTime(c.TxnDate), TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(Timezone)).ToString("G", CultureInfo.GetCultureInfo(ConfigurationManager.AppSettings["CultureInfo"].ToString())); return c;
                    });
                }

                _logMessage.Append("GetTransaction method of _transactionService executed Successfully count");
                _logMessage.Append("assigning result in searchTransactionDtos from stvd");

                var searchTransactionDtos = stvd as MTDTransactionDto[] ?? stvd.ToArray();

                _logMessage.Append("result in searchTransactionDtos from stvd successfully executed");

                return Json(new { Result = "OK", Records = searchTransactionDtos, TotalRecordCount = count });
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        ///  Purpose            :  Page indexing on Driver report
        /// Function Name       :   DriverReportListByFiter
        ///Created By          :   Salil Gupta
        ///Created On          :   2/11/2015
        ///Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult DriverReportListByFiter(SearchParameters searchCriteria, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                // Set the MerchantID first, as it is then required for Timezone stuff.
                int? mid = null;

                if (searchCriteria.fleetId == 0)
                    searchCriteria.fleetId = null;

                if (searchCriteria.MerchantId != null)
                    mid = searchCriteria.MerchantId;
                else
                    mid = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;

                // Now we know the merchant, hopefully, we can get a timezone.
                if (mid != null)
                    Timezone = _commonService.MerchantTimeZone((int)mid);
                else
                    Timezone = TimeZoneInfo.Local.StandardName;

                if (searchCriteria.startDateString != null && searchCriteria.endDateString != null)
                {
                    if (searchCriteria.startDateString.Length > 0 && searchCriteria.endDateString.Length > 0)
                    {
                        bool isDate = false;
                        Regex r = new Regex(@"\d\d-\d\d-\d\d\d\d");
                        if (((r.Match(searchCriteria.startDateString).Success) && (r.Match(searchCriteria.endDateString).Success)))
                        {
                            DateTime resultDate;
                            string sDate = searchCriteria.startDateString + " 00:00";
                            string eDate = searchCriteria.endDateString + " 23:59";

                            isDate = DateTime.TryParseExact(sDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return Json(new { Result = "OK", Records = (IEnumerable<DriverTransactionResult>)null, TotalRecordCount = 0 });

                            searchCriteria.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones

                            isDate = DateTime.TryParseExact(eDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return Json(new { Result = "OK", Records = (IEnumerable<DriverTransactionResult>)null, TotalRecordCount = 0 });

                            searchCriteria.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
                        }
                    }
                }

                if ((searchCriteria.DNumber == "--Please Select a Driver--") || (searchCriteria.DNumber == "0"))
                    searchCriteria.DNumber = null;
                
                if ((searchCriteria.VNumber == "--Please Select a Car--") || (searchCriteria.VNumber == "0"))
                    searchCriteria.VNumber = null;
                
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 2 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 3)
                {
                    MtdTransactionsViewData td = new MtdTransactionsViewData
                    {
                        MerchantId = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID,
                        FleetId = Convert.ToInt32(searchCriteria.fleetId),
                        VehicleNo = searchCriteria.VNumber,
                        DriverNo = searchCriteria.DNumber,
                        startDate = searchCriteria.startDate,
                        endDate = searchCriteria.endDate,
                        PageSize = jtPageSize,
                        PageNumber = jtStartIndex,
                        Sorting = jtSorting
                    };

                    _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");
                    
                    MTDTransactionDto transactionDto = td.ToDTO();
                    
                    _logMessage.Append("ToDTO method executed successfully");
                    _logMessage.Append("calling GetDriverCount method of _transactionService");
                    
                    int count = _transactionService.GetDriverCount(transactionDto);
                    
                    _logMessage.Append("GetDriverCount method of _transactionService executed Successfully count = " + count);
                    _logMessage.Append("calling GetDriver method of _transactionService takes transaction dto");
                    
                    IEnumerable<DriverTransactionResult> stvd = _transactionService.GetDriver(transactionDto);
                    
                    int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                    var userfleet = _userService.GetUsersFleet(userId);//2
                    int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                    if (userType == 4)
                        count = stvd.Count();
                    
                    _logMessage.Append("GetDriver method of _transactionService executed successfully");
                    _logMessage.Append("assigning result in searchTransactionDtos from stvd");
                    
                    var searchTransactionDtos = stvd as DriverTransactionResult[] ?? stvd.ToArray();
                    
                    _logMessage.Append("result in searchTransactionDtos from stvd successfully executed");

                    //zzz Delete once we have tracked down the timezone problem with Search but not Export.
                    _logger.LogInfoMessage(_logMessage.ToString());

                    return Json(new { Result = "OK", Records = searchTransactionDtos, TotalRecordCount = count });
                }
                else
                {
                    MtdTransactionsViewData td = new MtdTransactionsViewData
                    {
                        MerchantId = mid,
                        FleetId = Convert.ToInt32(searchCriteria.fleetId),
                        VehicleNo = searchCriteria.VNumber,
                        DriverNo = searchCriteria.DNumber,
                        startDate = searchCriteria.startDate,
                        endDate = searchCriteria.endDate,
                        PageSize = jtPageSize,
                        PageNumber = jtStartIndex,
                        Sorting = jtSorting
                    };

                    _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");
            
                    MTDTransactionDto transactionDto = td.ToDTO();
                    
                    _logMessage.Append("ToDTO method executed successfully");
                    _logMessage.Append("calling GetDriverCount method of _transactionService");
                    
                    int count = _transactionService.GetDriverCount(transactionDto);
                    
                    _logMessage.Append("GetDriverCount method of _transactionService executed Successfully count = " + count);
                    _logMessage.Append("calling GetDriver method of _transactionService takes transaction dto");
                    
                    IEnumerable<DriverTransactionResult> stvd = _transactionService.GetDriver(transactionDto);

                    int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                    var userfleet = _userService.GetUsersFleet(userId);
                    int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                    if (userType == 4)
                    {
                        stvd = stvd.Where(x => userfleet.Select(y => y.fk_FleetID).Contains((int)x.FleetID)).ToList();
                        count = stvd.Count();
                    }

                    _logMessage.Append("GetDriver method of _transactionService executed successfully");
                    _logMessage.Append("assigning result in searchTransactionDtos from stvd");
                    
                    var searchTransactionDtos = stvd as DriverTransactionResult[] ?? stvd.ToArray();
                    
                    _logMessage.Append("result in searchTransactionDtos from stvd successfully executed");

                    //zzz Delete once we have tracked down the timezone problem with Search but not Export.
                    _logger.LogInfoMessage(_logMessage.ToString());

                    return Json(new { Result = "OK", Records = searchTransactionDtos, TotalRecordCount = count });
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        ///  Purpose            :  Page indexing on Driver report
        /// Function Name       :   DriverReportListByFiter
        ///Created By          :   Salil Gupta
        ///Created On          :   2/11/2015
        ///Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult TechnologyFeeSummaryListByFiter(SearchParameters searchCriteria, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                int? mid;
                if (searchCriteria.MerchantId != null)
                    mid = searchCriteria.MerchantId;
                else
                    mid = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;

                if (searchCriteria.startDateString != null && searchCriteria.endDateString != null)
                {
                    if (searchCriteria.startDateString.Length > 0 && searchCriteria.endDateString.Length > 0)
                    {
                        // Now we know the merchant, hopefully, we can get a timezone.
                        if (mid != null)
                            Timezone = _commonService.MerchantTimeZone((int)mid);
                        else
                            Timezone = TimeZoneInfo.Local.StandardName;

                        bool isDate = false;
                        Regex r = new Regex(@"\d\d-\d\d-\d\d\d\d");
                        if (((r.Match(searchCriteria.startDateString).Success) && (r.Match(searchCriteria.endDateString).Success)))
                        {
                            DateTime resultDate;
                            string sDate = searchCriteria.startDateString + " 00:00";
                            string eDate = searchCriteria.endDateString + " 23:59";

                            isDate = DateTime.TryParseExact(sDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return Json(new { Result = "OK", Records = (IEnumerable<TechnologySummaryReportDto>)null, TotalRecordCount = 0 });

                            searchCriteria.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones

                            isDate = DateTime.TryParseExact(eDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return Json(new { Result = "OK", Records = (IEnumerable<TechnologySummaryReportDto>)null, TotalRecordCount = 0 });

                            searchCriteria.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
                        }
                    }
                }
                
                if (searchCriteria.fleetId == 0)
                    searchCriteria.fleetId = null;
                
                if (searchCriteria.fleetId == null && searchCriteria.MerchantId == null && searchCriteria.startDate == null && searchCriteria.endDate == null)
                    return Json(new { Result = "OK", Records = (IEnumerable<TechnologySummaryReportDto>)null, TotalRecordCount = 0 });

                MtdTransactionsViewData td = new MtdTransactionsViewData
                    {
                        MerchantId = mid,
                        FleetId = searchCriteria.fleetId,
                        startDate = searchCriteria.startDate,
                        endDate = searchCriteria.endDate,
                        PageSize = jtPageSize,
                        PageNumber = jtStartIndex,
                        Sorting = jtSorting
                    };

                _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");
                
                MTDTransactionDto transactionDto = td.ToDTO();
                
                _logMessage.Append("ToDTO method executed successfully");
                _logMessage.Append("calling GetDriverCount method of _transactionService");
                
                int count = _transactionService.GetTechFeeCount(transactionDto);
                
                _logMessage.Append("GetTechFeeCount method of _transactionService executed Successfully count = " + count);
                _logMessage.Append("calling GetTechnologySummary method of _transactionService takes transaction dto");
                
                IEnumerable<TechnologySummaryReportDto> stvd = _transactionService.GetTechnologySummary(transactionDto);
                
                _logMessage.Append("GetTechnologySummary method of _transactionService executed successfully");
                _logMessage.Append("assigning result in TechnologySummaryReportDto from stvd");
                
                var searchTransactionDtos = stvd as TechnologySummaryReportDto[] ?? stvd.ToArray();
                
                _logMessage.Append("result in TechnologySummaryReportDto from stvd successfully executed");
                
                return Json(new { Result = "OK", Records = searchTransactionDtos, TotalRecordCount = count });
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        ///  Purpose            :  Page indexing on Driver report
        /// Function Name       :   DriverReportListByFiter
        ///Created By          :   Salil Gupta
        ///Created On          :   2/11/2015
        ///Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult TechDetailListByFilter(SearchParameters searchCriteria, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                int? mid;
                if (searchCriteria.MerchantId != null)
                    mid = searchCriteria.MerchantId;
                else
                    mid = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;

                if (searchCriteria.startDateString != null && searchCriteria.endDateString != null)
                {
                    if (searchCriteria.startDateString.Length > 0 && searchCriteria.endDateString.Length > 0)
                    {
                        // Now we know the merchant, hopefully, we can get a timezone.
                        if (mid != null)
                            Timezone = _commonService.MerchantTimeZone((int)mid);
                        else
                            Timezone = TimeZoneInfo.Local.StandardName;
                        
                        bool isDate = false;
                        Regex r = new Regex(@"\d\d-\d\d-\d\d\d\d");
                        if (((r.Match(searchCriteria.startDateString).Success) && (r.Match(searchCriteria.endDateString).Success)))
                        {
                            DateTime resultDate;
                            string sDate = searchCriteria.startDateString + " 00:00";
                            string eDate = searchCriteria.endDateString + " 23:59";

                            isDate = DateTime.TryParseExact(sDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return Json(new { Result = "OK", Records = (IEnumerable<TechnologyDetailReportDto>)null, TotalRecordCount = 0 });

                            searchCriteria.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones

                            isDate = DateTime.TryParseExact(eDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return Json(new { Result = "OK", Records = (IEnumerable<TechnologyDetailReportDto>)null, TotalRecordCount = 0 });

                            searchCriteria.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
                        }
                    }
                }
                
                if (searchCriteria.fleetId == 0)
                    searchCriteria.fleetId = null;
                
                if (searchCriteria.DNumber == "--Select Driver--" || searchCriteria.DNumber == "0")
                    searchCriteria.DNumber = null;
                
                if (searchCriteria.VNumber == "--Select Car--" || searchCriteria.VNumber == "0")
                    searchCriteria.VNumber = null;
                
                if (searchCriteria.fleetId == null && searchCriteria.MerchantId == null && searchCriteria.DNumber == null && searchCriteria.VNumber == null && searchCriteria.startDate == null && searchCriteria.endDate == null)
                    return Json(new { Result = "OK", Records = (IEnumerable<TechnologyDetailReportDto>)null, TotalRecordCount = 0 });

                MtdTransactionsViewData td = new MtdTransactionsViewData
                    {
                        MerchantId = mid,
                        FleetId = searchCriteria.fleetId,
                        DriverNo = searchCriteria.DNumber,
                        VehicleNo = searchCriteria.VNumber,
                        startDate = searchCriteria.startDate,
                        endDate = searchCriteria.endDate,
                        PageSize = jtPageSize,
                        PageNumber = jtStartIndex,
                        Sorting = jtSorting
                    };

                _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");
                
                MTDTransactionDto transactionDto = td.ToDTO();
                
                _logMessage.Append("ToDTO method executed successfully");
                _logMessage.Append("calling GetTechFeeDetailCount method of _transactionService");
                
                int count = _transactionService.GetTechFeeDetailCount(transactionDto);
                
                _logMessage.Append("GetTechFeeDetailCount method of _transactionService executed Successfully count = " + count);
                _logMessage.Append("calling GetTechnologySummary method of _transactionService takes transaction dto");
                
                IEnumerable<TechnologyDetailReportDto> stvd = _transactionService.GetTechnologyDetail(transactionDto);
                
                _logMessage.Append("GetTechnologySummary method of _transactionService executed successfully");
                _logMessage.Append("assigning result in TechnologySummaryReportDto from stvd");
                
                var searchTransactionDtos = stvd as TechnologyDetailReportDto[] ?? stvd.ToArray();
                
                _logMessage.Append("result in TechnologyDetailReportDto from stvd successfully executed");
                
                return Json(new { Result = "OK", Records = searchTransactionDtos, TotalRecordCount = count });
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        /// Purpose            :  To Refund or Void Transaction
        /// Function Name      :   RefundVoid
        /// Created By         :   Umesh Kumar
        /// Created On         :   04/20/2015
        /// Modifications Made :   ****************************
        /// Modified On        :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="transId"></param>
        /// <param name="tType"></param>
        /// <param name="transNote"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public JsonResult RefundVoid(int transId, string tType, string transNote, string amount)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                if (Session["User"] == null)
                    return Json("Session Expired", JsonRequestBehavior.AllowGet);

                PaymentRequest transactionRequest = new PaymentRequest();
                transactionRequest.Header = new Header();
                transactionRequest.TransactionDetail = new TransactionDetail();
                MTDTransactionDto dbRequest = _transactionService.GetTransaction(transId);

                if (dbRequest == null)
                {
                    int id = 0;
                    return Json(id);
                }

                if (string.IsNullOrEmpty(dbRequest.SerialNo))
                {
                    string invalidDriver = MtdataResource.terminal_not_linked;
                    return Json(invalidDriver, JsonRequestBehavior.AllowGet);
                }

                transactionRequest.Header.DeviceId = dbRequest.SerialNo;
                transactionRequest.Header.DriverId = dbRequest.DriverNo;
                transactionRequest.Header.RequestId = string.Empty;
                transactionRequest.Header.Token = string.Empty;
                transactionRequest.TransactionDetail.PaymentType = dbRequest.PaymentType == Credit ? dbRequest.PaymentType : Debit;
                transactionRequest.TransactionDetail.TransNote = transNote;
                transactionRequest.TransactionDetail.CardType = dbRequest.CardType;
                transactionRequest.TransactionDetail.EntryMode = Keyed;   //"Keyed";
                transactionRequest.TransactionDetail.ExpiryDate = StringExtension.Decrypt(dbRequest.ExpiryDate);
                string expDate = StringExtension.Decrypt(dbRequest.ExpiryDate);
                transactionRequest.TransactionDetail.ExpiryDate = expDate;
                transactionRequest.TransactionDetail.TransactionType = tType;
                transactionRequest.TransactionDetail.IndustryType = Ecommerce; //"Ecommerce";
                transactionRequest.TransactionDetail.RapidConnectAuthId = Convert.ToString(transId);
                transactionRequest.TransactionDetail.VehicleNumber = dbRequest.VehicleNo;
                transactionRequest.TransactionDetail.Fare = string.Empty;
                transactionRequest.TransactionDetail.Tip = string.Empty;
                transactionRequest.TransactionDetail.Taxes = string.Empty;
                transactionRequest.TransactionDetail.Surcharge = string.Empty;
                transactionRequest.TransactionDetail.Fee = string.Empty;
                transactionRequest.TransactionDetail.Tolls = string.Empty;
                transactionRequest.TransactionDetail.JobNumber = string.Empty;
                transactionRequest.TransactionDetail.FlagFall = string.Empty;
                transactionRequest.TransactionDetail.Extras = string.Empty;
                transactionRequest.TransactionDetail.GateFee = string.Empty;
                transactionRequest.TransactionDetail.Others = string.Empty;
                transactionRequest.TransactionDetail.KeyId = string.Empty;
                transactionRequest.TransactionDetail.CcvData = string.Empty;
                transactionRequest.TransactionDetail.EmvData = string.Empty;
                transactionRequest.TransactionDetail.FleetId = Convert.ToString(dbRequest.fk_FleetId);
                transactionRequest.TransactionDetail.CurrencyCode = dbRequest.Currency;
                transactionRequest.TransactionDetail.PinData = string.Empty;
                transactionRequest.TransactionDetail.CardNumber = string.Empty;

                if (Session["User"] != null)
                    transactionRequest.TransactionDetail.CreatedBy = ((SessionDto)Session["User"]).SessionUser.Email;

                // Ensure that we have a CreatedBy property set, otherwise we get an Xml element missing error.
                if (string.IsNullOrEmpty(transactionRequest.TransactionDetail.CreatedBy))
                    transactionRequest.TransactionDetail.CreatedBy = dbRequest.CreatedBy;

                double decimalAmount = Convert.ToDouble(amount);
                decimalAmount = Math.Round(decimalAmount, 2, MidpointRounding.AwayFromZero);
                transactionRequest.TransactionDetail.TransactionAmount = Convert.ToString(decimalAmount);

                string transactionSubmitedAt = dbRequest.LocalDateTime;
                string timeNow = DateTime.Now.ToString("yyyyMMddHHmmss");
                string dateoftxn = transactionSubmitedAt.Substring(0, 8);
                string currentDate = timeNow.Substring(0, 8);

                if (tType == Void)
                {
                    if (dateoftxn == currentDate)
                    {
                        int dateoftxnMin = Convert.ToInt32(transactionSubmitedAt.Substring(10, 2));
                        int currentDateMin = Convert.ToInt32(timeNow.Substring(10, 2));

                        if (currentDateMin - dateoftxnMin > 25)
                        {
                            int id = 0;
                            return Json(id);
                        }
                    }
                    else
                    {
                        int id = 0;
                        return Json(id);
                    }
                }

                transactionRequest.TransactionDetail.StreetAddress = dbRequest.CrdHldrAddress;
                transactionRequest.TransactionDetail.PickUpAdd = string.Empty;
                transactionRequest.TransactionDetail.DropOffAdd = string.Empty;
                transactionRequest.TransactionDetail.PickUpLat = string.Empty;
                transactionRequest.TransactionDetail.DropOffLat = string.Empty;
                transactionRequest.TransactionDetail.PickUpLng = string.Empty;
                transactionRequest.TransactionDetail.DropOffLng = string.Empty;
                transactionRequest.TransactionDetail.Track1Data = string.Empty;
                transactionRequest.TransactionDetail.Track2Data = string.Empty;
                transactionRequest.TransactionDetail.Track3Data = string.Empty;
                transactionRequest.TransactionDetail.ZipCode = dbRequest.CrdHldrZip;

                string serviceUrl = ConfigurationManager.AppSettings["MyAccountBaseUrl"];

                if (ConfigurationManager.AppSettings["Project"].Equals(MtdataResource.CanadaProject, StringComparison.OrdinalIgnoreCase))
                {
                    if (dbRequest.EntryMode == "EmvContact" || dbRequest.EntryMode == "EmvContactless")
                        transactionRequest.TransactionDetail.EmvRefundVoid = true;
                    else
                        transactionRequest.TransactionDetail.EmvRefundVoid = false;
                }

                dynamic response = WebClient.DoPost<PaymentRequest>(String.Format("{0}api/Payment/SendPayment", serviceUrl), transactionRequest);

                if (response != null)
                {
                    try
                    {
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(response);

                        if (ConfigurationManager.AppSettings["Project"] == MtdataResource.CanadaProject)
                        {
                            var innerXml = xmlDoc.InnerXml;
                            int startTransIndex = innerXml.IndexOf("<TransactionId>");
                            int endTransIndex = innerXml.IndexOf("</TransactionId>");
                            string id = innerXml.Substring(startTransIndex + 15, endTransIndex - (startTransIndex + 15));

                            return Json(id, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
                        }  

                        var xmlNode = xmlDoc.SelectSingleNode("PaymentResponse/TransactionDetails/TransactionId");
                        if (xmlNode != null)
                        {
                            _logTracking.Append("Refund Transaction Performed for transaction id " + transId + " and transaction performed by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                            _logger.LogInfoMessage(_logTracking.ToString());

                            string id = xmlNode.InnerText;
                            return Json(id, "applicaiton/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            var xmlNodeMessage = xmlDoc.SelectSingleNode("PaymentResponse/Header/Message");
                            return Json(xmlNodeMessage.InnerText, JsonRequestBehavior.AllowGet);
                        }
                    }
                    catch (Exception exception)
                    {
                        _logger.LogInfoFatal(_logMessage.ToString(), exception);
                        return Json(0, "applicaiton/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            return null;
        }

        /// <summary>
        ///  Purpose             :  Get List of driver names
        /// Function Name        :   GetDriverListName
        ///Created By            :   Asif Shafeeque
        ///Created On            :   04/16/2015
        ///Modifications Made    :   ****************************
        /// Modified On          :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public JsonResult GetDriverListName(int fleetId)
        {
            var result = _reportcommonRepository.GetDriverNameList(fleetId).ToViewDataList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Purpose             :   To create new transaction
        /// Function Name       :   Create
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transactionView"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ManualTransaction(TransactionRequest transactionView)
        {
            if (Session["User"] == null)
                return Json("Session Expired", JsonRequestBehavior.AllowGet);

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            if (ModelState.IsValid)
            {
                try
                {
                    _logTracking.Append("User " + ((SessionDto)Session["User"]).SessionUser.Email + " trying to perform a Manual Transaction with vehicle no " + transactionView.VehicleNumber);
                    _logMessage.Append("creating PaymentRequest class object");

                    PaymentRequest pRequest = new PaymentRequest();
                    pRequest.Header = new Header();
                    pRequest.TransactionDetail = new TransactionDetail();
                    pRequest.Header.RequestId = string.Empty;
                    pRequest.Header.Token = string.Empty;
                    
                    if (!string.IsNullOrEmpty(transactionView.DriverNumber))
                        pRequest.Header.DriverId = transactionView.DriverNumber;
                    else
                        pRequest.Header.DriverId = string.Empty;

                    _logMessage.Append("PaymentRequest object created successfully");

                    if (Session["User"] != null)
                        pRequest.TransactionDetail.CreatedBy = ((SessionDto)Session["User"]).SessionUser.Email;

                    string payType = transactionView.PaymentType;
                    pRequest.TransactionDetail.CardType = payType == "Debit" ? string.Empty : transactionView.CardType;

                    decimal amount = Convert.ToDecimal(transactionView.TransactionAmount);
                    amount = Math.Round(amount, 2, MidpointRounding.AwayFromZero);
                    
                    _logMessage.Append("transaction amount " + amount);
                    
                    if (!string.IsNullOrEmpty(transactionView.VehicleNumber))
                    {
                        string driverDevice = _transactionService.GetDriverDevice(transactionView.VehicleNumber, Convert.ToInt32(transactionView.FleetId));
                        if (string.IsNullOrEmpty(driverDevice))
                        {
                            string invalidDriver = MtdataResource.vehicle_not_configured;
                            return Json(invalidDriver, JsonRequestBehavior.AllowGet);
                        }

                        pRequest.Header.DeviceId = driverDevice;

                        _logMessage.Append("device associated with vehicle is " + driverDevice);
                    }

                    _logMessage.Append("Creation transaction Request");

                    decimal fareValue = Convert.ToDecimal(transactionView.FareValue);
                    decimal tipAmount = Convert.ToDecimal(transactionView.Tip);
                    decimal taxex = Convert.ToDecimal(transactionView.Taxes);
                    decimal fee = 0;
                    decimal surcharge = Convert.ToDecimal(transactionView.Surcharge);
                    string expDate = transactionView.ExpiryDate;
                    string year = expDate.Substring(0, 4);
                    string month = transactionView.ExpiryDate.Substring(4, 2);
                    int days = DateTime.DaysInMonth(Convert.ToInt32(year), Convert.ToInt32(month));

                    if (!string.IsNullOrEmpty(transactionView.Email))   //added to validate email address
                    {
                        var regex = new Regex(MtdataResource.EmailRegex);
                        if (!regex.IsMatch(transactionView.Email))
                            return Json(MtdataResource.InvalidEmail, JsonRequestBehavior.AllowGet);
                    }

                    int currencyCode = _fleetService.GetCurrencyByFleet(Convert.ToInt32(transactionView.FleetId));  //added to get currency code by fleet
                    string projectType = ConfigurationManager.AppSettings["Project"];

                    if (projectType == MtdataResource.USProject && Convert.ToString(currencyCode) != MtdataResource.Currency_US)    //added to validate currency code
                        return Json(MtdataResource.IncorrectCurrencyCode, JsonRequestBehavior.AllowGet);
                    else if (projectType == MtdataResource.CanadaProject && Convert.ToString(currencyCode) != MtdataResource.Currency_CN)
                        return Json(MtdataResource.IncorrectCurrencyCode, JsonRequestBehavior.AllowGet);

                    pRequest.TransactionDetail.PaymentType = payType;
                    pRequest.TransactionDetail.EntryMode = Keyed;
                    pRequest.TransactionDetail.CardNumber = transactionView.CardNumber;
                    pRequest.TransactionDetail.ExpiryDate = year + month + Convert.ToString(days);
                    pRequest.TransactionDetail.CcvData = transactionView.CCVData;
                    pRequest.TransactionDetail.RapidConnectAuthId = string.Empty;
                    pRequest.TransactionDetail.FleetId = transactionView.FleetId;
                    pRequest.TransactionDetail.TransactionType = transactionView.TransactionType;
                    pRequest.TransactionDetail.IndustryType = Ecommerce;
                    pRequest.TransactionDetail.TransactionAmount = Convert.ToString(amount, CultureInfo.InvariantCulture);
                    pRequest.TransactionDetail.VehicleNumber = transactionView.VehicleNumber;
                    pRequest.TransactionDetail.StreetAddress = transactionView.StreetAddress;
                    pRequest.TransactionDetail.Fare = Convert.ToString(fareValue, CultureInfo.InvariantCulture);
                    pRequest.TransactionDetail.Tip = Convert.ToString(tipAmount, CultureInfo.InvariantCulture);
                    pRequest.TransactionDetail.Taxes = Convert.ToString(taxex, CultureInfo.InvariantCulture);
                    pRequest.TransactionDetail.Surcharge = Convert.ToString(surcharge, CultureInfo.InvariantCulture);
                    pRequest.TransactionDetail.Fee = Convert.ToString(fee, CultureInfo.InvariantCulture);
                    pRequest.TransactionDetail.Tolls = string.Empty;
                    pRequest.TransactionDetail.FlagFall = string.Empty;
                    pRequest.TransactionDetail.Extras = string.Empty;
                    pRequest.TransactionDetail.GateFee = string.Empty;
                    pRequest.TransactionDetail.Others = string.Empty;
                    pRequest.TransactionDetail.PickUpAdd = string.Empty;
                    pRequest.TransactionDetail.DropOffAdd = string.Empty;
                    pRequest.TransactionDetail.PickUpLat = string.Empty;
                    pRequest.TransactionDetail.KeyId = string.Empty;
                    pRequest.TransactionDetail.DropOffLat = string.Empty;
                    pRequest.TransactionDetail.PickUpLng = string.Empty;
                    pRequest.TransactionDetail.DropOffLng = string.Empty;
                    pRequest.TransactionDetail.Track1Data = string.Empty;
                    pRequest.TransactionDetail.Track2Data = string.Empty;
                    pRequest.TransactionDetail.Track3Data = string.Empty;
                    pRequest.TransactionDetail.EmvData = string.Empty;
                    pRequest.TransactionDetail.CurrencyCode = Convert.ToString(currencyCode);
                    pRequest.TransactionDetail.PinData = string.Empty;
                    pRequest.TransactionDetail.ZipCode = transactionView.ZipCode;

                    if (!string.IsNullOrEmpty(transactionView.TransNote))
                        pRequest.TransactionDetail.TransNote = transactionView.TransNote;
                    else
                        pRequest.TransactionDetail.TransNote = string.Empty;

                    if (!string.IsNullOrEmpty(transactionView.MtDataJobNumber))
                        pRequest.TransactionDetail.JobNumber = transactionView.MtDataJobNumber;
                    else
                        pRequest.TransactionDetail.JobNumber = string.Empty;

                    _logMessage.Append("getting service url from config");

                    string serviceUrl = ConfigurationManager.AppSettings["MyAccountBaseUrl"];

                    _logMessage.Append("service url is " + serviceUrl);

                    dynamic trxResponse = WebClient.DoPost<PaymentRequest>(String.Format("{0}api/Payment/SendPayment", serviceUrl), pRequest);

                    if (trxResponse != "")
                    {
                        try
                        {
                            string tansId = string.Empty;
                            string message1 = string.Empty;

                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(trxResponse);

                            if (projectType == MtdataResource.CanadaProject)
                            {
                                var innerXml = xmlDoc.InnerXml;
                                
                                int startTransIndex = innerXml.IndexOf(MtdataResource.TranasactionIdStartTag);
                                int endTransIndex = innerXml.IndexOf(MtdataResource.TranasactionIdEndTag);
                                if (startTransIndex != -1 && endTransIndex != -1)
                                    tansId = innerXml.Substring(startTransIndex + 15, endTransIndex - (startTransIndex + 15));
                                
                                int startMsgIndex = innerXml.IndexOf(MtdataResource.MessageStartTag);
                                int endMsgIndex = innerXml.IndexOf(MtdataResource.MessageEndTag);
                                if (startMsgIndex != -1 && endMsgIndex != -1)
                                    message1 = innerXml.Substring(startMsgIndex + 9, endMsgIndex - (startMsgIndex + 9));

                                SendMail(pRequest, transactionView, message1);
                                string id = tansId;
                                
                                return Json(id, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
                            }

                            var xmlNode = xmlDoc.SelectSingleNode("PaymentResponse/TransactionDetails/TransactionId");
                            var message = xmlDoc.SelectSingleNode("PaymentResponse/Header/Message").InnerText;
                            if (xmlNode != null)
                            {
                                _logTracking.Append("Manaul Transaction Performed for " + transactionView.VehicleNumber + "vehicle no and transaction performed by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                                _logger.LogInfoMessage(_logTracking.ToString());

                                SendMail(pRequest, transactionView, message);
                                string id = xmlNode.InnerText;
                                
                                return Json(id, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
                            }

                            var xmlNodeMessage = xmlDoc.SelectSingleNode("PaymentResponse/Header/Message");
                            SendMail(pRequest, transactionView, message);

                            return Json(xmlNodeMessage.InnerText, JsonRequestBehavior.AllowGet);
                        }
                        catch (Exception ex)
                        {
                            _logger.LogInfoMessage(_logTracking.ToString());
                            _logger.LogInfoFatal(_logMessage.ToString(), ex);

                            return Json("An error occurred please try later.", "applicaiton/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
                        }
                    }

                    string wentWrong = MtdataResource.Something_went_wrong_try_after_sometimes; //"Something gone wrong please try after sometime";
                    
                    return Json(wentWrong, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    _logger.LogInfoMessage(_logTracking.ToString());
                    _logger.LogInfoFatal(_logMessage.ToString(), ex);
                    
                    return Json("An error occurred please try later.", "applicaiton/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
                }
            }

            return Json("TimeOut", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetSurcharges(string vehicle)
        {
            if (Session["User"] != null)
            {
                int vehicleToGetFleet = Convert.ToInt32(vehicle);
                int fkMerchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                TxnTaxSurchargesDto ttSurchage = _transactionService.GetTaxSurchages(fkMerchantId, vehicleToGetFleet);

                return Json(ttSurchage);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetSurcharge()
        {
            if (Session["User"] != null)
            {
                TxnTaxSurchargesDto ttSurchage = _transactionService.GetTaxSurchages();
                return Json(ttSurchage, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pRequest"></param>
        /// <param name="transactionView"></param>
        protected void SendMail(PaymentRequest pRequest, TransactionRequest transactionView, string message)
        {
            try
            {
                _logMessage.Append("send mail begin after transaction");

                DriverDto driverDto = _driverService.GetDriverDto(transactionView.DriverNumber, Convert.ToInt32(transactionView.FleetId));
                List<ReceiptListDto> receipt = _receiptService.ReceiptToShowByfleetId(Convert.ToInt32(transactionView.FleetId)).ToList();

                if (transactionView.Email != null)
                {
                    var fleetDto = _fleetService.GetFleet(Convert.ToInt32(transactionView.FleetId));
                    var tablestring = string.Empty;

                    tablestring = "<html lang='en' xmlns='http://www.w3.org/1999/xhtml>'<head><style>.bod  { border-bottom: 1px solid black;width:300px;overflow: hidden; white-space: nowrap;word-wrap: break-word;}</style></head><body>";
                    tablestring += "<p> You have made Transaction with following details:</p></br>";
                    tablestring += "<table width='400' style='font-size:12px; font-family:verdana;border-top: 1px solid black;border-right: 1px solid black;border-left: 1px solid black;height:500px;overflow: hidden; white-space: nowrap;width:400px;table-layout:fixed;'>";

                    var companyName = ((SessionDto)Session["User"]).SessionMerchant.Company;
                    var companyTaxNo = ((SessionDto)Session["User"]).SessionMerchant.CompanyTaxNumber;
                    var cPhone = ((SessionDto)Session["User"]).SessionUser.Phone;
                    var cAddress = ((SessionDto)Session["User"]).SessionUser.Address;
                    var termsConditions = ((SessionDto)Session["User"]).SessionMerchant.TermCondition;
                    var disclaimer = ((SessionDto)Session["User"]).SessionMerchant.Disclaimer;

                    tablestring += "<tr style='font-weight: bold;align=center;'> <td colspan='2' class='bod'>Transaction Detail</td></tr>";
                    tablestring += "<tr style='font-weight: bold;align=center;'> <td colspan='2' class='bod' >Staus: " + message + "</td></tr>";

                    foreach (ReceiptListDto t in receipt)
                    {
                        var value = (EmailField)Enum.Parse(typeof(EmailField), t.FieldName.Replace(" ", String.Empty), true);
                        switch (value)
                        {
                            case EmailField.CompanyName:// "CompanyName":
                                tablestring += "<tr ><td class='bod'>" + t.fk_FieldText + "</td><td  class='bod' >" + companyName + "</td></tr>";
                                break;
                            case EmailField.CompanyPhone://"CompanyPhone":
                                tablestring += "<tr ><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + cPhone + "</td></tr>";
                                break;
                            case EmailField.CompanyAddress://"CompanyAddress":
                                tablestring += "<tr ><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + cAddress + "</td></tr>";
                                break;
                            case EmailField.CompanyTaxNumber://"CompanyTaxNumber":
                                tablestring += "<tr ><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + companyTaxNo + "</td></tr>";
                                break;
                            case EmailField.Driver://"Driver":
                                if (driverDto != null)
                                    tablestring += "<tr ><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + driverDto.DriverNo + "</td></tr>";
                                break;
                            case EmailField.DriverTaxNumber://"DriverTaxNumber":
                                if (driverDto != null)
                                    tablestring += "<tr ><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + driverDto.DriverTaxNo + "</td></tr>";
                                break;
                            case EmailField.Extra://"Extra":
                                tablestring += "<tr ><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + pRequest.TransactionDetail.Extras + "</td></tr>";
                                break;
                            case EmailField.Fare://"Fare":
                                tablestring += "<tr ><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + pRequest.TransactionDetail.Fare + "</td></tr>";
                                break;
                            case EmailField.FederalTaxAmount://"FederalTaxAmount":
                                tablestring += "<tr ><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + transactionView.FedralTax + "</td></tr>";
                                break;
                            case EmailField.FederalTaxPercentage://"FederalTaxPercentage":
                                tablestring += "<tr ><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + transactionView.FedralTaxPer + "</td></tr>";
                                break;
                            case EmailField.Flagfall://"Flagfall":
                                tablestring += "<tr ><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + pRequest.TransactionDetail.FlagFall + "</td></tr>";
                                break;
                            case EmailField.Fleet://"Fleet":
                                tablestring += "<tr ><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + fleetDto.FleetName + "</td></tr>";
                                break;
                            case EmailField.PickAddress://"PickAddress":
                                tablestring += "<tr ><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + pRequest.TransactionDetail.PickUpAdd + "</td></tr>";
                                break;
                            case EmailField.StateTaxAmount://"StateTaxAmount":
                                tablestring += "<tr ><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + transactionView.StateTax + "</td></tr>";
                                break;
                            case EmailField.StateTaxPercentage://"StateTaxPercentage":
                                tablestring += "<tr ><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + transactionView.StateTaxPer + "</td></tr>";
                                break;
                            case EmailField.Subtotal://"Subtotal":
                                tablestring += "<tr><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + Convert.ToString(Convert.ToDouble(transactionView.TransactionAmount) - Convert.ToDouble(transactionView.Tip)) + "</td></tr>";
                                break;
                            case EmailField.SurchargeAmount://"SurchargeAmount":
                                tablestring += "<tr ><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + transactionView.Surcharge + "</td></tr>";
                                break;
                            case EmailField.SurchargePercentage://"SurchargePercentage":
                                tablestring += "<tr ><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + transactionView.SurchargePer + "</td></tr>";
                                break;
                            case EmailField.TermsandConditions://"TermsandConditions":
                                tablestring += "<tr ><td  class='bod' >" + t.fk_FieldText + "</td><td  class='bod'>" + termsConditions + "</td></tr>";
                                break;
                            case EmailField.Tip://"Tip":
                                tablestring += "<tr ><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + transactionView.Tip + "</td></tr>";
                                break;
                            case EmailField.Tolls://"Tolls":
                                tablestring += "<tr><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + transactionView.Toll + "</td></tr>";
                                break;
                            case EmailField.Total://"Total":
                                tablestring += "<tr ><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + transactionView.TransactionAmount + "</td></tr>";
                                break;
                            case EmailField.Vehicle://"Vehicle":
                                tablestring += "<tr ><td  class='bod'>" + t.fk_FieldText + "</td><td  class='bod'>" + transactionView.VehicleNumber + "</td></tr>";
                                break;
                            case EmailField.Disclaimer://"Disclaimer":
                                tablestring += "<tr ><td  class='bod' >" + t.fk_FieldText + "</td><td  class='bod'>" + disclaimer + "</td></tr>";
                                break;
                            case EmailField.DestinationAddress://"DestinationAddress":
                                tablestring += "<tr ><td>" + t.fk_FieldText + "</td><td>" + string.Empty + "</td></tr>";
                                break;
                        }
                    }
                    
                    tablestring = tablestring + "</table></br>";
                    tablestring += " <h4> Thanks and Regards<br /> " + companyName + "<br/></h4>";

                    _logMessage.Append("mail table created success fully that is " + tablestring);
                    
                    var em = new EmailUtil();
                    
                    _logMessage.Append("Calling SendMail method of EmailUtil to send mail taking parameters mail, tablestring, cName, Invoice =" + transactionView.Email + ", " + tablestring + ", " + "asifs@chetu.com" + ", Invoice");
                    
                    em.SendMail(transactionView.Email, tablestring, companyName, companyName + " Invoice");
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Report()
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || Session["User"] == null) return RedirectToAction("Index", "Member");

            int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
            TxnSearch tvdData = new TxnSearch();
            tvdData.IsVarified = null;
            List<CarListViewData> carList = new List<CarListViewData>();
            List<DriverListViewData> driverList = new List<DriverListViewData>();
            
            int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
            int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
            if (userType == 4)
                tvdData.fleetlist = _userService.UserFleetLists(userId).ToViewDataList();
            
            if (userType == 2 || userType == 3)
                tvdData.fleetlist = _userService.FleetList(merchantId).ToViewDataList();
            
            tvdData.DriverList = driverList;
            tvdData.CarList = carList;
            tvdData.fk_FleetId = 0;
            tvdData.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            tvdData.Currency = ConfigurationManager.AppSettings["Currency"].ToString();
            
            return View(tvdData);
        }

        /// <summary>
        ///  Purpose            :   Searching the transaction 
        /// Function Name       :   Search
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   1/8/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Report(TxnSearch tvd)
        {
            try
            {
                if (Session["User"] == null) return RedirectToAction("Index", "Member");
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
                int? merchantId = null;
                
                var fkMerchantId = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;
                if (fkMerchantId != null)
                {
                    merchantId = fkMerchantId;
                    Timezone = _commonService.MerchantTimeZone((int)merchantId);
                }
                else
                {
                    TimeZoneInfo timeZone = TimeZoneInfo.Local;
                    Timezone = timeZone.StandardName;// use this time zone
                }

                int fId = 0;
                if (tvd.fk_FleetId != null)
                    fId = (int)tvd.fk_FleetId;

                tvd.fleetlist = _userService.FleetList((int)merchantId).ToViewDataList();
                tvd.CarList = _commonService.GetCars((int)fId).ToViewDataList();
                tvd.DriverList = _commonService.GetDrivers((int)fId).ToViewDataList();
                if (tvd.startDateString != null && tvd.endDateString != null)
                {
                    if (tvd.startDateString.Length > 0 && tvd.endDateString.Length > 0)
                    {
                        //D#6082 Incorrect Timezone handling.
                        bool isDate = false;
                        Regex r = new Regex(@"\d\d-\d\d-\d\d\d\d\s\d\d:\d\d");
                        if (((r.Match(tvd.startDateString).Success) && (r.Match(tvd.endDateString).Success)))
                        {
                            DateTime resultDate;
                            isDate = DateTime.TryParseExact(tvd.startDateString, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return View(tvd);
                            else
                                tvd.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones

                            isDate = DateTime.TryParseExact(tvd.endDateString, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return View(tvd);
                            else
                                tvd.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
                        }
                    }
                }

                if (tvd.fk_FleetId == null && string.IsNullOrEmpty(tvd.VNumber) && string.IsNullOrEmpty(tvd.TType) && string.IsNullOrEmpty(tvd.DNumber) && string.IsNullOrEmpty(tvd.CrdType) && tvd.startDate == null && tvd.endDate == null && tvd.Id == null && string.IsNullOrEmpty(tvd.ExpiryDate) && string.IsNullOrEmpty(tvd.FFD) && string.IsNullOrEmpty(tvd.LFD) && tvd.MinAmt == null && tvd.MaxAmt == null && tvd.JobNumber == null && tvd.AId == null && tvd.AddRespData == null && tvd.Industry == null && string.IsNullOrEmpty(tvd.TerminalId) && string.IsNullOrEmpty(tvd.SerialNo))
                {
                    ViewBag.Message = MtdataResource.Please_Select_field; //"Please enter or choose some field's";
                    return View(tvd);
                }

                if (!String.IsNullOrEmpty(tvd.ExpiryDate))
                    tvd.ExpiryDate = StringExtension.DateFormat(tvd.ExpiryDate);

                MtdTransactionsViewData td = new MtdTransactionsViewData
                {
                    PageSize = 0,
                    ExpiryDate = tvd.ExpiryDate,
                    VehicleNo = tvd.VNumber,
                    DriverNo = tvd.DNumber,
                    CardType = tvd.CrdType,
                    startDate = tvd.startDate,
                    endDate = tvd.endDate,
                    Id = tvd.Id,
                    TxnType = tvd.TType,
                    FirstFourDigits = tvd.FFD,
                    LastFourDigits = tvd.LFD,
                    MinAmount = tvd.MinAmt,
                    MaxAmount = tvd.MaxAmt,
                    AuthId = tvd.AId,
                    AddRespData = tvd.AddRespData,
                    MerchantId = merchantId,
                    JobNumber = tvd.JobNumber,
                    fk_FleetId = tvd.fk_FleetId,
                    EntryMode = tvd.Industry,
                    IsVarified = tvd.IsVarified,
                    SerialNo = tvd.SerialNo,
                    TerminalId = tvd.TerminalId,
                    TransNote = tvd.TransNote   // Ensure TransNote included
                };

                MTDTransactionDto transactionDto = td.ToDTO();
                int count = _transactionService.GetTransactionCount(transactionDto);
                td.PageSize = count;
                IEnumerable<MTDTransactionDto> stvd = _transactionService.GetTransaction(transactionDto);
                int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                var userfleet = _userService.GetUsersFleet(userId);
                int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                if (userType == 4)
                    stvd = stvd.Where(x => userfleet.Select(y => y.fk_FleetID).Contains((int)x.fk_FleetId)).ToList();

                IEnumerable<SearchTransactionViewData> mvData = stvd.ToViewData();
                var transactionViewDatas = mvData as SearchTransactionViewData[] ?? mvData.ToArray();

                if (transactionViewDatas.ToList().Count == 0)
                {
                    ViewBag.Message = MtdataResource.No_such_record_exist;  //"No such records exist";
                    return View(tvd);
                }

                string culture = ConfigurationManager.AppSettings["CultureInfo"].ToString();
                string currency = ConfigurationManager.AppSettings["Currency"].ToString();

                using (StringWriter output = new StringWriter())
                {
                    output.WriteLine("\"Id\",\"AuthCode\",\"Vehicle\",\"Driver\",\"Date\",\"Credit/Debit\",\"Transaction Type\",\"Tech Fee(" + currency + ")\",\"Fleet Fee(" + currency + ")\",\"Fee(" + currency + ")\",\"Surcharge(" + currency + ")\",\"Amount(" + currency + ")\",\"CardType\",\"Entry Mode\",\"Approval\"");

                    foreach (SearchTransactionViewData item in transactionViewDatas)
                    {
                        DateTime dt1 = TimeZoneInfo.ConvertTime(Convert.ToDateTime(item.TxnDate), TimeZoneInfo.Utc, TimeZoneInfo.FindSystemTimeZoneById(Timezone));
                        decimal tFee = Convert.ToDecimal(item.TechFee);
                        decimal fFee = Convert.ToDecimal(item.FleetFee);
                        decimal fee = Convert.ToDecimal(item.Fee);
                        decimal sur = Convert.ToDecimal(item.Surcharge);
                        decimal amt = Convert.ToDecimal(item.Amount);

                        output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\"",
                            item.Id, item.AuthId, item.VehicleNo, item.DriverNo, dt1.ToString("G", CultureInfo.GetCultureInfo(culture)), item.PaymentType, item.TxnType,
                            tFee.ToString("C", CultureInfo.GetCultureInfo(culture)), fFee.ToString("C", CultureInfo.GetCultureInfo(culture)),
                            fee.ToString("C", CultureInfo.GetCultureInfo(culture)), sur.ToString("C", CultureInfo.GetCultureInfo(culture)),
                            amt.ToString("C", CultureInfo.GetCultureInfo(culture)), item.CardType, item.EntryMode, item.AddRespData);
                    }

                    _logTracking.Append("Transaction Report Excel file Downloaded by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Transaction_Report.csv");
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View(tvd);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetCarsDrivers(string id)
        {
            if (String.IsNullOrEmpty(id))
                return Json(null, JsonRequestBehavior.AllowGet);

            CarDriverViewData ob = new CarDriverViewData();
            ob.CarList = _commonService.GetCars(Convert.ToInt32(id)).ToViewDataList();
            ob.DriverList = _commonService.GetDrivers(Convert.ToInt32(id)).ToViewDataList();

            return Json(ob, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Purpose             :  Searching Payroll Report SAGE
        /// Function Name       :   PayrollReportSAGE
        /// Created By          :   Shishir Saunakia
        /// Created On          :   09/29/2015
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PayrollReportSAGE()
        {
            try
            {
                if (Session["User"] == null) return RedirectToAction("Index", "Member");
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                    return RedirectToAction("index", "Member");

                var merId = (((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                int? userTypeId = (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID);
                if (userTypeId == 1)
                {
                    List<FleetListViewData> ob = new List<FleetListViewData>();
                    var payrollTrx = new PayrollReportSAGE { MerchantList = MerchantList(), FleetList = ob };
                    return View(payrollTrx);
                }
                else
                {
                    int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                    int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                    PayrollReportSAGE payrollTrx = new PayrollReportSAGE();
                    int merchantId = merId ?? 0;
                    if (userType == 4)
                    {
                        IEnumerable<FleetViewData> fleetDto = _userService.UserFleetLists(userId).ToViewDataList();
                        List<FleetListViewData> fleets = new List<FleetListViewData>();
                        foreach (FleetViewData fleet in fleetDto)
                        {
                            FleetListViewData fd = new FleetListViewData();
                            fd.FleetName = fleet.FleetName;
                            fd.FleetID = fleet.FleetID;
                            fleets.Add(fd);
                        }

                        payrollTrx.FleetList = fleets;
                    }

                    if (userType == 2 || userType == 3)
                        payrollTrx.MerchantID = merchantId;

                    return View(payrollTrx);
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            :  Page indexing on Payroll Report SAGE
        /// Function Name       :   PayrollReportSAGEListByFiter
        ///Created By          :   Shishir Saunakia
        ///Created On          :   09/29/2015
        ///Modifications Made   :   ****************************      
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult PayrollReportSAGEListByFiter(SearchParameters searchCriteria, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                int? mid;
                if (searchCriteria.MerchantId != null)
                    mid = searchCriteria.MerchantId;
                else
                    mid = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;

                if (searchCriteria.startDateString != null && searchCriteria.endDateString != null)
                {
                    if (searchCriteria.startDateString.Length > 0 && searchCriteria.endDateString.Length > 0)
                    {
                        // Now we know the merchant, hopefully, we can get a timezone.
                        if (mid != null)
                            Timezone = _commonService.MerchantTimeZone((int)mid);
                        else
                            Timezone = TimeZoneInfo.Local.StandardName;

                        bool isDate = false;
                        Regex r = new Regex(@"\d\d-\d\d-\d\d\d\d");
                        if (((r.Match(searchCriteria.startDateString).Success) && (r.Match(searchCriteria.endDateString).Success)))
                        {
                            DateTime resultDate;
                            string sDate = searchCriteria.startDateString + " 00:00";
                            string eDate = searchCriteria.endDateString + " 23:59";

                            isDate = DateTime.TryParseExact(sDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return Json(new { Result = "OK", Records = (IEnumerable<PayrollRportSageResult>)null, TotalRecordCount = 0 });

                            searchCriteria.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones

                            isDate = DateTime.TryParseExact(eDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return Json(new { Result = "OK", Records = (IEnumerable<PayrollRportSageResult>)null, TotalRecordCount = 0 });

                            searchCriteria.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
                        }
                    }
                }

                if (searchCriteria.DNumber == "--Select Driver--")
                    searchCriteria.DNumber = null;
                
                if (searchCriteria.fleetId == 0)
                    searchCriteria.fleetId = null;
                
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                
                if (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 2 || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 3)
                {
                    MtdTransactionsViewData td = new MtdTransactionsViewData
                    {
                        MerchantId = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID,
                        FleetId = Convert.ToInt32(searchCriteria.fleetId),
                        payerType = searchCriteria.PayerType,
                        DriverNo = searchCriteria.DNumber,
                        startDate = searchCriteria.startDate,
                        endDate = searchCriteria.endDate,
                        NetworkType = searchCriteria.NetworkType,
                        PageSize = jtPageSize,
                        PageNumber = jtStartIndex,
                        Sorting = jtSorting
                    };
                    
                    _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");

                    MTDTransactionDto transactionDto = td.ToDTO();
                    
                    _logMessage.Append("ToDTO method executed successfully");
                    _logMessage.Append("calling GetPayrollSageCount method of _transactionService");
                    
                    int count = _transactionService.GetPayrollSageCount(transactionDto);
                    
                    _logMessage.Append("GetPayrollSageCount method of _transactionService executed Successfully count = " + count);
                    _logMessage.Append("calling GetpayrollSage method of _transactionService takes transaction dto");
                    
                    IEnumerable<PayrollRportSageResult> stvd = _transactionService.GetpayrollSage(transactionDto);
                    
                    int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                    var userfleet = _userService.GetUsersFleet(userId);//2
                    int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                    if (userType == 4)
                    {
                        count = stvd.Count();
                    }
                    
                    _logMessage.Append("GetpayrollSage method of _transactionService executed successfully");
                    _logMessage.Append("assigning result in searchTransactionDtos from stvd");
                    
                    var searchTransactionDtos = stvd as PayrollRportSageResult[] ?? stvd.ToArray();
                    foreach (PayrollRportSageResult item in searchTransactionDtos)
                    {
                        if (!string.IsNullOrWhiteSpace(item.SageAccount))
                        {
                            var decryptSage = StringExtension.DecryptDriver(item.SageAccount);
                            item.SageAccount = decryptSage;
                        }
                    }
                    
                    _logMessage.Append("result in searchTransactionDtos from stvd successfully executed");
                    
                    return Json(new { Result = "OK", Records = searchTransactionDtos, TotalRecordCount = count });
                }
                else
                {
                    MtdTransactionsViewData td = new MtdTransactionsViewData
                    {
                        MerchantId = mid,
                        FleetId = Convert.ToInt32(searchCriteria.fleetId),
                        payerType = searchCriteria.PayerType,
                        DriverNo = searchCriteria.DNumber,
                        startDate = searchCriteria.startDate,
                        endDate = searchCriteria.endDate,
                        PageSize = jtPageSize,
                        PageNumber = jtStartIndex,
                        Sorting = jtSorting
                    };

                    _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");

                    
                    MTDTransactionDto transactionDto = td.ToDTO();
                    
                    _logMessage.Append("ToDTO method executed successfully");
                    _logMessage.Append("calling GetPayrollSageCount method of _transactionService");
                    
                    int count = _transactionService.GetPayrollSageCount(transactionDto);
                    
                    _logMessage.Append("GetPayrollSageCount method of _transactionService executed Successfully count = " + count);
                    _logMessage.Append("calling GetpayrollSage method of _transactionService takes transaction dto");
                    
                    IEnumerable<PayrollRportSageResult> stvd = _transactionService.GetpayrollSage(transactionDto);
                    
                    int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                    var userfleet = _userService.GetUsersFleet(userId);//2
                    int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                    if (userType == 4)
                    {
                        count = stvd.Count();
                    }
                    
                    _logMessage.Append("GetpayrollSage method of _transactionService executed successfully");
                    _logMessage.Append("assigning result in searchTransactionDtos from stvd");
                    
                    var searchTransactionDtos = stvd as PayrollRportSageResult[] ?? stvd.ToArray();
                    foreach (PayrollRportSageResult item in searchTransactionDtos)
                    {
                        if (!string.IsNullOrWhiteSpace(item.SageAccount))
                        {
                            var decryptSage = StringExtension.DecryptDriver(item.SageAccount);
                            item.SageAccount = decryptSage;
                        }
                    }
                    
                    _logMessage.Append("result in searchTransactionDtos from stvd successfully executed");
                    
                    return Json(new { Result = "OK", Records = searchTransactionDtos, TotalRecordCount = count });
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        /// Purpose             :  Export to Excel Payroll Report SAGE
        /// Function Name       :   PayrollReportSAGE
        /// Created By          :   Shishir Saunakia
        /// Created On          :   09/29/2015
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PayrollReportSAGE(PayrollReportSAGE tvd)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

            int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
            if (userType == 1)
                tvd.MerchantID = tvd.MerchantID;
            else
                tvd.MerchantID = (int)(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);

            // Now we know the merchant, hopefully, we can get a timezone.
            if (tvd.MerchantID != null)
                Timezone = _commonService.MerchantTimeZone((int)tvd.MerchantID);
            else
                Timezone = TimeZoneInfo.Local.StandardName;

            // We have to do the date Timezone manipulation after getting the Merchant details.
            if (tvd.startDateString != null && tvd.endDateString != null)
            {
                bool isDate = false;
                DateTime resultDate;
                string sDate = tvd.startDateString + " 00:00";
                string eDate = tvd.endDateString + " 23:59";

                isDate = DateTime.TryParseExact(sDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                if (isDate)
                    tvd.transaction.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones

                isDate = DateTime.TryParseExact(eDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                if (isDate)
                    tvd.transaction.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
            }

            if (tvd.transaction.DriverNo == "--Select Driver--")
                tvd.transaction.DriverNo = null;
            
            if (tvd.FleetID == 0)
                tvd.FleetID = null;

            MtdTransactionsViewData td = new MtdTransactionsViewData();

            td.FleetId = tvd.FleetID;
            td.PageSize = 0;
            td.MerchantId = tvd.MerchantID;
            td.payerType = tvd.TType;
            td.DriverNo = tvd.transaction.DriverNo;
            td.startDate = tvd.transaction.startDate;
            td.endDate = tvd.transaction.endDate;
            td.NetworkType = tvd.transaction.NetworkType;

            _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");
            
            MTDTransactionDto transactionDto = td.ToDTO();
            
            _logMessage.Append("ToDTO method executed successfully");
            _logMessage.Append("calling GetPayrollSageCount method of _transactionService");
            
            int count = _transactionService.GetPayrollSageCount(transactionDto);
            transactionDto.PageSize = count;
            
            _logMessage.Append("GetPayrollSageCount method of _transactionService executed Successfully count = " + count);
            _logMessage.Append("calling GetpayrollSage method of _transactionService takes transaction dto");
            
            IEnumerable<PayrollRportSageResult> stvd = _transactionService.GetpayrollSage(transactionDto);
            int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
            var userfleet = _userService.GetUsersFleet(userId);//2
            if (userType == 4)
            {
                //stvd = stvd.Where(x => userfleet.Select(y => y.fk_FleetID).Contains((int)x.FleetID)).ToList();
            }
            
            _logMessage.Append("GetpayrollSage method of _transactionService executed successfully");
            _logMessage.Append("assigning result in searchTransactionDtos from stvd");
            
            var searchTransactionDtos = stvd as PayrollRportSageResult[] ?? stvd.ToArray();
            
            _logMessage.Append("result in searchTransactionDtos from stvd successfully executed");

            using (StringWriter output = new StringWriter())
            {
                foreach (PayrollRportSageResult item in searchTransactionDtos)
                {
                    var decryptSage = string.Empty;
                    var defaultDate = string.Empty;
                    if (!string.IsNullOrWhiteSpace(item.SageAccount))
                    {
                        decryptSage = StringExtension.DecryptDriver(item.SageAccount);
                        item.SageAccount = decryptSage;
                    }

                    if (Convert.ToString(item.DefaultDate) != string.Empty)
                        defaultDate = item.DefaultDate.ToString("MM-dd-yyyy");

                    output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\"", item.PaymentType, decryptSage, item.BankAccount, item.Default1, defaultDate, item.DefaultBACS, item.DefaultPayment, String.Format("{0:C2}", item.TransactionAmount), item.DefaultTaxCode, item.VAT);
                }

                _logTracking.Append("Payroll SAGE Report Excel file Downloaded by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                
                return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Payroll_Report_SAGE.csv");
            }
        }

        /// <summary>
        /// Purpose             :  Searching PayrollReport Payment 
        /// Function Name       :   PayrollReportPayment
        /// Created By          :   Shishir Saunakia
        /// Created On          :   10/06/2015
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PayrollReportPayment()
        {
            try
            {
                if (Session["User"] == null) return RedirectToAction("Index", "Member");
                if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                    return RedirectToAction("index", "Member");

                var merId = (((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                int? userTypeId = (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID);
                if (userTypeId == 1)
                {
                    List<FleetListViewData> ob = new List<FleetListViewData>();
                    var payrollTrx = new PayrollReportPayment { MerchantList = MerchantList(), FleetList = ob };
                    return View(payrollTrx);
                }
                else
                {
                    int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                    int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                    PayrollReportPayment payrollTrx = new PayrollReportPayment();
                    
                    int merchantId = merId ?? 0;
                    if (userType == 4)
                    {
                        IEnumerable<FleetViewData> fleetDto = _userService.UserFleetLists(userId).ToViewDataList();
                        List<FleetListViewData> fleets = new List<FleetListViewData>();
                        foreach (FleetViewData fleet in fleetDto)
                        {
                            FleetListViewData fd = new FleetListViewData();
                            fd.FleetName = fleet.FleetName;
                            fd.FleetID = fleet.FleetID;
                            fleets.Add(fd);
                        }

                        payrollTrx.FleetList = fleets;
                    }

                    if (userType == 2 || userType == 3)
                        payrollTrx.MerchantID = merchantId;

                    return View(payrollTrx);
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            :  Page indexing on Payroll Report Payment
        /// Function Name       :   PayrollReportPaymentListByFiter
        ///Created By          :   Shishir Saunakia
        ///Created On          :   10/06/2015
        ///Modifications Made   :   ****************************      
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult PayrollReportPaymentListByFiter(SearchParameters searchCriteria, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                int? mid;
                if (searchCriteria.MerchantId != null)
                    mid = searchCriteria.MerchantId;
                else
                    mid = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;

                if (searchCriteria.startDateString != null && searchCriteria.endDateString != null)
                {
                    if (searchCriteria.startDateString.Length > 0 && searchCriteria.endDateString.Length > 0)
                    {
                        // Now we know the merchant, hopefully, we can get a timezone.
                        if (mid != null)
                            Timezone = _commonService.MerchantTimeZone((int)mid);
                        else
                            Timezone = TimeZoneInfo.Local.StandardName;

                        bool isDate = false;
                        Regex r = new Regex(@"\d\d-\d\d-\d\d\d\d");
                        if (((r.Match(searchCriteria.startDateString).Success) && (r.Match(searchCriteria.endDateString).Success)))
                        {
                            DateTime resultDate;
                            string sDate = searchCriteria.startDateString + " 00:00";
                            string eDate = searchCriteria.endDateString + " 23:59";

                            isDate = DateTime.TryParseExact(sDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return Json(new { Result = "OK", Records = (IEnumerable<PayrollRportPaymentResult>)null, TotalRecordCount = 0 });

                            searchCriteria.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones

                            isDate = DateTime.TryParseExact(eDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                            if (!isDate)
                                return Json(new { Result = "OK", Records = (IEnumerable<PayrollRportPaymentResult>)null, TotalRecordCount = 0 });

                            searchCriteria.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
                        }
                    }
                }

                if (searchCriteria.DNumber == "--Select Driver--")
                    searchCriteria.DNumber = null;
                
                if (searchCriteria.fleetId == 0)
                    searchCriteria.fleetId = null;
                
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 2 ||
                    ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 3)
                {
                    MtdTransactionsViewData td = new MtdTransactionsViewData
                    {
                        MerchantId = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID,
                        FleetId = Convert.ToInt32(searchCriteria.fleetId),
                        payerType = searchCriteria.PayerType,
                        DriverNo = searchCriteria.DNumber,
                        startDate = searchCriteria.startDate,
                        endDate = searchCriteria.endDate,
                        PageSize = jtPageSize,
                        PageNumber = jtStartIndex,
                        Sorting = jtSorting,
                        NetworkType = searchCriteria.NetworkType
                    };

                    _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");

                    MTDTransactionDto transactionDto = td.ToDTO();
                    
                    _logMessage.Append("ToDTO method executed successfully");
                    _logMessage.Append("calling GetpayrollPaymentCount method of _transactionService");
                    
                    int count = _transactionService.GetPayrollPaymentCount(transactionDto);
                    
                    _logMessage.Append("GetpayrollPaymentCount method of _transactionService executed Successfully count = " + count);
                    _logMessage.Append("calling GetpayrollPayment method of _transactionService takes transaction dto");
                    
                    IEnumerable<PayrollRportPaymentResult> stvd = _transactionService.GetpayrollPayment(transactionDto);
                    int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                    var userfleet = _userService.GetUsersFleet(userId);//2
                    int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                    if (userType == 4)
                        count = stvd.Count();

                    _logMessage.Append("GetpayrollSage method of _transactionService executed successfully");
                    _logMessage.Append("assigning result in searchTransactionDtos from stvd");
                    
                    var searchTransactionDtos = stvd as PayrollRportPaymentResult[] ?? stvd.ToArray();
                    foreach (PayrollRportPaymentResult item in searchTransactionDtos)
                    {
                        if (!string.IsNullOrWhiteSpace(item.AccountNumber))
                        {
                            var decryptAccount = StringExtension.DecryptDriver(item.AccountNumber);
                            item.AccountNumber = decryptAccount;
                        }
                    }

                    _logMessage.Append("result in searchTransactionDtos from stvd successfully executed");
                    
                    return Json(new { Result = "OK", Records = searchTransactionDtos, TotalRecordCount = count });
                }
                else
                {
                    MtdTransactionsViewData td = new MtdTransactionsViewData
                    {
                        MerchantId = mid,
                        FleetId = Convert.ToInt32(searchCriteria.fleetId),
                        payerType = searchCriteria.PayerType,
                        DriverNo = searchCriteria.DNumber,
                        startDate = searchCriteria.startDate,
                        endDate = searchCriteria.endDate,
                        PageSize = jtPageSize,
                        PageNumber = jtStartIndex,
                        Sorting = jtSorting
                    };

                    _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");
                    
                    MTDTransactionDto transactionDto = td.ToDTO();
                    
                    _logMessage.Append("ToDTO method executed successfully");
                    _logMessage.Append("calling GetPayrollPaymentCount method of _transactionService");
                    
                    int count = _transactionService.GetPayrollPaymentCount(transactionDto);
                    
                    _logMessage.Append("GetPayrollPaymentCount method of _transactionService executed Successfully count = " + count);
                    _logMessage.Append("calling GetPayrollPayment method of _transactionService takes transaction dto");
                    
                    IEnumerable<PayrollRportPaymentResult> stvd = _transactionService.GetpayrollPayment(transactionDto);

                    int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                    var userfleet = _userService.GetUsersFleet(userId);//2
                    int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                    if (userType == 4)
                        count = stvd.Count();

                    _logMessage.Append("GetpayrollSage method of _transactionService executed successfully");
                    _logMessage.Append("assigning result in searchTransactionDtos from stvd");
                    
                    var searchTransactionDtos = stvd as PayrollRportPaymentResult[] ?? stvd.ToArray();
                    foreach (PayrollRportPaymentResult item in searchTransactionDtos)
                    {
                        if (!string.IsNullOrWhiteSpace(item.AccountNumber))
                        {
                            var decryptAccount = StringExtension.DecryptDriver(item.AccountNumber);
                            item.AccountNumber = decryptAccount;
                        }
                    }
                    
                    _logMessage.Append("result in searchTransactionDtos from stvd successfully executed");
                    
                    return Json(new { Result = "OK", Records = searchTransactionDtos, TotalRecordCount = count });
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        ///  Purpose            :  Export to Excel Payroll Report Payment
        /// Function Name       :   PayrollReportPayment
        ///Created By          :   Shishir Saunakia
        ///Created On          :   10/06/2015
        /// </summary>
        /// <param name="tvd"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PayrollReportPayment(PayrollReportPayment tvd)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

            int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
            if (userType == 1)
                tvd.MerchantID = tvd.MerchantID;
            else
                tvd.MerchantID = (int)(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);

            // Now we know the merchant, hopefully, we can get a timezone.
            if (tvd.MerchantID != null)
                Timezone = _commonService.MerchantTimeZone((int)tvd.MerchantID);
            else
                Timezone = TimeZoneInfo.Local.StandardName;

            // We have to do the date Timezone manipulation after getting the Merchant details.
            if (tvd.startDateString != null && tvd.endDateString != null)
            {
                bool isDate = false;
                DateTime resultDate;
                string sDate = tvd.startDateString + " 00:00";
                string eDate = tvd.endDateString + " 23:59";

                isDate = DateTime.TryParseExact(sDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                if (isDate)
                    tvd.transaction.startDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones

                isDate = DateTime.TryParseExact(eDate, "MM-dd-yyyy HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out resultDate);
                if (isDate)
                    tvd.transaction.endDate = TimeZoneInfo.ConvertTime(resultDate, TimeZoneInfo.FindSystemTimeZoneById(Timezone), TimeZoneInfo.Local);	//D#6082 Handle Timezones
            }

            if (tvd.transaction.DriverNo == "--Select Driver--")
                tvd.transaction.DriverNo = null;
            
            if (tvd.FleetID == 0)
                tvd.FleetID = null;
            
            MtdTransactionsViewData td = new MtdTransactionsViewData
            {
                FleetId = tvd.FleetID,
                PageSize = 0,
                MerchantId = tvd.MerchantID,
                payerType = tvd.TType,
                DriverNo = tvd.transaction.DriverNo,
                startDate = tvd.transaction.startDate,
                endDate = tvd.transaction.endDate,
                NetworkType = tvd.transaction.NetworkType
            };

            _logMessage.Append("calling ToDTO method to map TransactionDto to TransactionViewData and retrun TransactionDto");
            
            MTDTransactionDto transactionDto = td.ToDTO();
            
            _logMessage.Append("ToDTO method executed successfully");
            _logMessage.Append("calling GetpayrollPaymentCount method of _transactionService");
            
            int count = _transactionService.GetPayrollPaymentCount(transactionDto);
            transactionDto.PageSize = count;
            
            _logMessage.Append("GetpayrollPaymentCount method of _transactionService executed Successfully count = " + count);
            _logMessage.Append("calling GetpayrollPayment method of _transactionService takes transaction dto");
            
            IEnumerable<PayrollRportPaymentResult> stvd = _transactionService.GetpayrollPayment(transactionDto);
            int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
            var userfleet = _userService.GetUsersFleet(userId);//2
            if (userType == 4)
            {
                //stvd = stvd.Where(x => userfleet.Select(y => y.fk_FleetID).Contains((int)x.FleetID)).ToList();
            }
            
            _logMessage.Append("GetpayrollPayment method of _transactionService executed successfully");
            _logMessage.Append("assigning result in searchTransactionDtos from stvd");
            
            var searchTransactionDtos = stvd as PayrollRportPaymentResult[] ?? stvd.ToArray();
            
            _logMessage.Append("result in searchTransactionDtos from stvd successfully executed");

            using (StringWriter output = new StringWriter())
            {
                //First line for column names
                output.WriteLine("\"Date\",\"Name\",\"Bank Name\",\"BSB\",\"Account# \",\"Amount Payable \"");

                foreach (PayrollRportPaymentResult item in searchTransactionDtos)
                {
                    var decryptAccount = string.Empty;
                    if (!string.IsNullOrWhiteSpace(item.AccountNumber))
                    {
                        decryptAccount = StringExtension.DecryptDriver(item.AccountNumber);
                        item.AccountNumber = decryptAccount;
                    }

                    output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\"", item.Date.ToString("MM/dd/yyyy"), item.Name, item.BankName, item.BSB, decryptAccount, String.Format("{0:C2}", item.AmountPayable));
                }

                _logTracking.Append("Payroll Report Payment Excel file Downloaded by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                
                return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Payroll_Report_Payment.csv");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="FleetId"></param>
        /// <returns></returns>
        public JsonResult GetPayCarOwners(string fleetID)
        {
            if (String.IsNullOrEmpty(fleetID)) return 
                    Json(null, JsonRequestBehavior.AllowGet);

            IEnumerable<CarOwnersDto> db = new List<CarOwnersDto>();
            db = _vehicleService.PayCarOwnerList(Convert.ToInt32(fleetID));

            return Json(db, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        ///Purpose             :  Get single transactions for completion
        ///Function Name       :   Completion
        ///Created By          :   Umesh k3
        ///Created On          :   10/08/2015
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Completion()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("index", "Member");

            TransactionRequest txnRequest = new TransactionRequest();

            return View(txnRequest);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionView"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CompletionTransaction(TransactionRequest transactionView)
        {
            if (Session["User"] == null)
                return Json("Session Expired", JsonRequestBehavior.AllowGet);

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            
            PaymentRequest transactionRequest = new PaymentRequest();
            transactionRequest.Header = new Header();
            transactionRequest.TransactionDetail = new TransactionDetail();
            
            MTDTransactionDto dbRequest = _transactionService.GetTransaction((int)transactionView.TransID);
            if (dbRequest == null)
            {
                int id = 0;
                return Json(id);
            }

            transactionRequest.Header.DeviceId = dbRequest.SerialNo;
            transactionRequest.Header.DriverId = dbRequest.DriverNo;
            transactionRequest.Header.RequestId = string.Empty;
            transactionRequest.Header.Token = string.Empty;
            transactionRequest.TransactionDetail.PaymentType = dbRequest.PaymentType == Credit ? dbRequest.PaymentType : Debit;
            transactionRequest.TransactionDetail.TransNote = dbRequest.TransNote;
            transactionRequest.TransactionDetail.CardType = dbRequest.CardType;
            transactionRequest.TransactionDetail.EntryMode = Keyed;   //"Keyed";
            transactionRequest.TransactionDetail.ExpiryDate = StringExtension.Decrypt(dbRequest.ExpiryDate);
            string expDate = StringExtension.Decrypt(dbRequest.ExpiryDate);
            transactionRequest.TransactionDetail.ExpiryDate = expDate;
            transactionRequest.TransactionDetail.TransactionType = "Completion";
            transactionRequest.TransactionDetail.IndustryType = Ecommerce; //"Ecommerce";
            transactionRequest.TransactionDetail.RapidConnectAuthId = Convert.ToString(transactionView.TransID);
            transactionRequest.TransactionDetail.VehicleNumber = dbRequest.VehicleNo;
            transactionRequest.TransactionDetail.Fare = string.Empty;
            transactionRequest.TransactionDetail.Tip = string.Empty;
            transactionRequest.TransactionDetail.Taxes = string.Empty;
            transactionRequest.TransactionDetail.Surcharge = string.Empty;
            transactionRequest.TransactionDetail.Fee = string.Empty;
            transactionRequest.TransactionDetail.Tolls = string.Empty;
            transactionRequest.TransactionDetail.JobNumber = string.Empty;
            transactionRequest.TransactionDetail.FlagFall = string.Empty;
            transactionRequest.TransactionDetail.Extras = string.Empty;
            transactionRequest.TransactionDetail.GateFee = string.Empty;
            transactionRequest.TransactionDetail.Others = string.Empty;
            transactionRequest.TransactionDetail.KeyId = string.Empty;
            transactionRequest.TransactionDetail.CcvData = string.Empty;
            transactionRequest.TransactionDetail.EmvData = string.Empty;
            transactionRequest.TransactionDetail.FleetId = Convert.ToString(dbRequest.fk_FleetId);
            transactionRequest.TransactionDetail.CurrencyCode = dbRequest.Currency;
            transactionRequest.TransactionDetail.PinData = string.Empty;
            transactionRequest.TransactionDetail.CardNumber = string.Empty;

            if (Session["User"] != null)
                transactionRequest.TransactionDetail.CreatedBy = ((SessionDto)Session["User"]).SessionUser.Email;

            // Ensure that we have a CreatedBy property set, otherwise we get an Xml element missing error.
            if (string.IsNullOrEmpty(transactionRequest.TransactionDetail.CreatedBy))
                transactionRequest.TransactionDetail.CreatedBy = dbRequest.CreatedBy;

            double decimalAmount = Convert.ToDouble(transactionView.TransactionAmount);
            decimalAmount = Math.Round(decimalAmount, 2, MidpointRounding.AwayFromZero);
            transactionRequest.TransactionDetail.TransactionAmount = Convert.ToString(decimalAmount);
            
            transactionRequest.TransactionDetail.StreetAddress = dbRequest.CrdHldrAddress;
            transactionRequest.TransactionDetail.PickUpAdd = string.Empty;
            transactionRequest.TransactionDetail.DropOffAdd = string.Empty;
            transactionRequest.TransactionDetail.PickUpLat = string.Empty;
            transactionRequest.TransactionDetail.DropOffLat = string.Empty;
            transactionRequest.TransactionDetail.PickUpLng = string.Empty;
            transactionRequest.TransactionDetail.DropOffLng = string.Empty;
            transactionRequest.TransactionDetail.Track1Data = string.Empty;
            transactionRequest.TransactionDetail.Track2Data = string.Empty;
            transactionRequest.TransactionDetail.Track3Data = string.Empty;
            transactionRequest.TransactionDetail.ZipCode = dbRequest.CrdHldrZip;

            string serviceUrl = ConfigurationManager.AppSettings["MyAccountBaseUrl"];

            dynamic response = WebClient.DoPost<PaymentRequest>(String.Format("{0}api/Payment/SendPayment", serviceUrl), transactionRequest);

            if (response != null)
            {
                try
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(response);

                    if (ConfigurationManager.AppSettings["Project"] == MtdataResource.CanadaProject)
                    {
                        var innerXml = xmlDoc.InnerXml;
                        int startTransIndex = innerXml.IndexOf("<TransactionId>");
                        int endTransIndex = innerXml.IndexOf("</TransactionId>");
                        string id = innerXml.Substring(startTransIndex + 15, endTransIndex - (startTransIndex + 15));
                        return Json(id, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
                    }

                    var xmlNode = xmlDoc.SelectSingleNode("PaymentResponse/TransactionDetails/TransactionId");
                    if (xmlNode != null)
                    {
                        _logTracking.Append("Refund Transaction Performed for transaction id " + transactionView.TransID + " and transaction performed by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                        _logger.LogInfoMessage(_logTracking.ToString());
                        string id = xmlNode.InnerText;
                        return Json(id, "applicaiton/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var xmlNodeMessage = xmlDoc.SelectSingleNode("PaymentResponse/Header/Message");
                        return Json(xmlNodeMessage.InnerText, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception exception)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), exception);
                    return Json(0, "applicaiton/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
                }
            }

            return null;
        }

        /// <summary>
        ///  Purpose           :  Verify single transactions
        /// Function Name      :   Varified
        ///Created By          :   Umesh k3
        ///Created On          :   10/13/2015
        /// </summary>
        /// <param name="FleetId"></param>
        /// <param name="TxnId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetTransaction(int TxnId)
        {
            if (Session["User"] == null)
                return Json("Session Expired", JsonRequestBehavior.AllowGet);

            var dto = (SessionDto)Session["User"];
            int userType = dto.SessionUser.fk_UserTypeID;
            int userId = dto.SessionUser.UserID;
            int mId = (int)dto.SessionUser.fk_MerchantID;
            int loggedbyUserType = dto.SessionUser.LogOnByUserType;
            int loggedbyUserBy = dto.SessionUser.LogOnByUserId;

            string project = ConfigurationManager.AppSettings["Project"];
            
            if (loggedbyUserType == 5)
            {
                MTDTransactionDto dbRequest = _transactionService.GetTransaction(5, loggedbyUserBy, mId, TxnId, project);
                return Json(dbRequest, JsonRequestBehavior.AllowGet);
            }
            
            if (userType == 4)
            {
                MTDTransactionDto dbRequest = _transactionService.GetTransaction(userType, userId, mId, TxnId, project);
                return Json(dbRequest, JsonRequestBehavior.AllowGet);
            }
            
            if (userType == 2 || userType == 3)
            {
                MTDTransactionDto dbRequest = _transactionService.GetTransaction(userType, userId, mId, TxnId, project);
                return Json(dbRequest, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="custID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DownloadFile(string custID)
        {
            // load file content from db or file system
            string fileContents = "field1,field2,field3";

            // convert to byte array
            // use a different encoding if needed
            var encoding = new System.Text.UTF8Encoding();
            byte[] returnContent = encoding.GetBytes(fileContents);

            return File(returnContent, "application/CSV", "test.csv");
        }

        /// <summary>
        ///  Purpose           :  Verify single transactions
        /// Function Name      :   Varified
        ///Created By          :   Umesh k3
        ///Created On          :   10/11/2015
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Varified(int id, bool status)
        {
            if (Session["User"] != null)
            {
                bool isExist = _transactionService.Varified(id, status);
                return Json(isExist, JsonRequestBehavior.AllowGet);
            }

            return Json("");
        }

        /// <summary>
        ///  Purpose           :  Verify selected transactions
        /// Function Name      :   VarifyMany
        ///Created By          :   Umesh k3
        ///Created On          :   10/12/2015
        /// </summary>
        /// <param name="ManyTxnClass"></param>
        /// <returns></returns>
        public JsonResult VarifyMany(ManyTxnClass[] ManyTxnClass)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                try
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    
                    ManyTxnClassDto[] varifiedTxnDto = ManyTxnClass.ToDTO();
                    bool isAllVarified = _transactionService.VarifiedMany(varifiedTxnDto);
                    return Json(isAllVarified, JsonRequestBehavior.AllowGet);
                }
                catch (Exception exception)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), exception);
                    return null;
                }
            }

            return null;
        }

        /// <summary>
        /// Created by : Umesh K3
        /// </summary>
        /// <param name="PayerId"></param>
        /// /// <param name="MerchentId"></param>
        /// <returns></returns>
        public JsonResult GetPayFleetList(int PayerId, int merchantId)
        {
            if (String.IsNullOrEmpty(PayerId.ToString()) || String.IsNullOrEmpty(PayerId.ToString()))
                return Json(null, JsonRequestBehavior.AllowGet);

            IEnumerable<FleetDto> db = new List<FleetDto>();
            db = _userService.PayerFleetList(PayerId, merchantId);

            return Json(db, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult OnDemandReport()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            
            try
            {
                OnDemandReportViewData sv = new OnDemandReportViewData();
                string v = Request.QueryString["sort"];
                string p = Request.QueryString["page"];

                if (v != null || p != null)
                {
                    sv.fk_ReportID = Convert.ToInt32(TempData["repValue"]);
                    sv.DateFrom = Convert.ToDateTime(TempData["DateFrom"]);
                    sv.DateTo = Convert.ToDateTime(TempData["DateTo"]);
                    sv.fk_MerchantId = Convert.ToInt32(TempData["MerValue"]);
                    sv.IsReport = true;
                    TempData.Keep("repValue");
                    TempData.Keep("DateFrom");
                    TempData.Keep("DateTo");
                    TempData.Keep("MerValue");

                    switch (sv.fk_ReportID)
                    {
                        case 9:// Get admin Summary
                            IEnumerable<GetAdminSummaryReportDto> admSummery = _transactionService.GetadminSummary(sv.DateFrom, sv.DateTo);
                            IEnumerable<GetAdminSummaryReportViewData> admSummeryView = admSummery.ToAdminSummaryList();
                            sv.GetAdminSummaryReport = admSummeryView;
                            break;
                        
                        case 10://Admin Exception Report
                            IEnumerable<GetAdminExceptionReportDto> admCardExcDto = _transactionService.AdminExceptionReport(sv.DateFrom, sv.DateTo);
                            IEnumerable<GetAdminExceptionReportViewData> admExcviewData = admCardExcDto.ToExceptionList();
                            sv.GetAdminExceptionReport = admExcviewData;
                            break;
                        
                        case 11://Admin Card Type Report
                            IEnumerable<GetCardTypeReportDto> admCardDto = _transactionService.AdminCardypeReport(sv.DateFrom, sv.DateTo);
                            IEnumerable<GetCardTypeReportViewData> admCardviewData = admCardDto.ToCardTypeList();
                            sv.GetCardTypeReport = admCardviewData;
                            break;
                        
                        case 12://Admin Total Transactions By Vehicle Report
                            IEnumerable<GetTotalTransByVehicleReportDto> admCardVehicleDto = _transactionService.TransByVehicleReport(sv.DateFrom, sv.DateTo);
                            IEnumerable<GetTotalTransByVehicleReportViewData> admVehicleviewData = admCardVehicleDto.ToVehicleReportList();
                            sv.AdminTotalTransactionsByVehicle = admVehicleviewData;
                            break;
                        
                        case 13://Merchant Summary Report
                            IEnumerable<GetMerchantSummaryReportDto> merchantSummaryDto = _transactionService.MerchantSummaryReport(sv.DateFrom, sv.DateTo, Convert.ToInt32(sv.fk_MerchantId));
                            IEnumerable<GetMerchantSummaryReportViewData> mSummary = merchantSummaryDto.ToSummaryList();
                            sv.MerchantSummaryReport = mSummary;
                            break;
                        
                        case 14://Merchant Detail Report
                            IEnumerable<GetDetailReportDto> detailReport = _transactionService.GetDetailsReport(sv.DateFrom, sv.DateTo, Convert.ToInt32(sv.fk_MerchantId));
                            IEnumerable<GetDetailReportViewData> dReport = detailReport.ToDetailList();
                            sv.DetailReport = dReport;
                            break;
                        
                        case 15://Merchant Exception Report
                            IEnumerable<GetMerchantExceptionReportDto> merchantException = _transactionService.GetMerchantExceptionReport(sv.DateFrom, sv.DateTo, Convert.ToInt32(sv.fk_MerchantId));
                            IEnumerable<GetMerchantExceptionReportViewData> merchantExceptionView = merchantException.ToMerchantSummaryExceptionList();
                            sv.MerchantExceptionReport = merchantExceptionView;
                            break;
                        
                        case 16://Merchant Total Transactions By Vehicle Report
                            IEnumerable<GetMerchantTotalTransByVehicleReportDto> merchantTotalTransactions = _transactionService.GetMerchantTotalTransByVehicle(sv.DateFrom, sv.DateTo, Convert.ToInt32(sv.fk_MerchantId));
                            IEnumerable<GetMerchantTotalTransByVehicleReportViewData> merchantETransactionsView = merchantTotalTransactions.ToMerchantTotalTransList();
                            sv.MerchantTotalTransReport = merchantETransactionsView;
                            break;
                    }
                }

                sv.ReportList = ReportList(8, 18);
                sv.MerchantList = MerchantList();

                return View(sv);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sv"></param>
        /// <returns></returns>
        [ActionName("OnDemandReport")]
        [AcceptVerbs(HttpVerbs.Post)]
        [AttributeModel(Name = "Number", Data = "Email Report")]
        public ActionResult Run(OnDemandReportViewData sv)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                var sDto = ((SessionDto)Session["User"]).SessionUser;
                int runStatus = 0;

                sv.ReportList = ReportList(8, 18);
                sv.MerchantList = MerchantList();
                ViewBag.Message = "Please configure admin or merchant first.";

                int? mId = sDto.fk_MerchantID;
                switch (sv.fk_ReportID)
                {
                    case 9:// Get admin Summary
                        runStatus = _transactionService.AdminExceptionReportScheduler(true, sv.DateFrom, sv.DateTo);
                        break;
                    
                    case 10://Admin Exception Report
                        runStatus = _transactionService.AdminExceptionReportScheduler(true, sv.DateFrom, sv.DateTo);
                        break;
                    
                    case 11://Admin Card Type Report
                        runStatus = _transactionService.CardTypeReportScheduler(true, sv.DateFrom, sv.DateTo);
                        break;
                    
                    case 12://Admin Total Transactions By Vehicle Report
                        runStatus = _transactionService.TotalTransByVehicleScheduler(true, sv.DateFrom, sv.DateTo);
                        break;
                    
                    case 13://Merchant Summary Report
                        runStatus = _transactionService.MerchantSummaryReportScheduler(true, sv.DateFrom, sv.DateTo,  mId);
                        break;
                    
                    case 14://Merchant Detail Report
                        runStatus = _transactionService.DetailReportScheduler(true, sv.DateFrom, sv.DateTo, mId);
                        break;
                    
                    case 15://Merchant Exception Report
                        runStatus = _transactionService.MerchantExceptionReportScheduler(true, sv.DateFrom, sv.DateTo);
                        break;
                    
                    case 16://Merchant Total Transactions By Vehicle Report
                        runStatus = _transactionService.MerchantTotalTransByVehicleScheduler(true, sv.DateFrom, sv.DateTo, mId);
                        break;

                    case 17://Merchant card type
                        runStatus = _transactionService.MerchantCardTypeReportScheduler(true, sv.DateFrom, sv.DateTo, mId);
                        break;
                }

                if (runStatus == -1)
                    ViewBag.Message = "Report run successfully";

                return View(sv);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            ViewBag.Message = "Report Run Successfully";
            return View(sv);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sv"></param>
        /// <returns></returns>
        [ActionName("OnDemandReport")]
        [AcceptVerbs(HttpVerbs.Post)]
        [AttributeModel(Name = "Number", Data = "View Report")]
        public ActionResult GetReport(OnDemandReportViewData sv)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null ||
                ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                TempData["repValue"] = sv.fk_ReportID;
                TempData["MerValue"] = sv.fk_MerchantId;
                TempData["DateFrom"] = sv.DateFrom;
                TempData["DateTo"] = sv.DateTo;

                sv.ReportList = ReportList(8, 18);
                sv.MerchantList = MerchantList();

                switch (sv.fk_ReportID)
                {
                    case 9:// Get admin Summary
                        IEnumerable<GetAdminSummaryReportDto> admSummery = _transactionService.GetadminSummary(sv.DateFrom, sv.DateTo);
                        IEnumerable<GetAdminSummaryReportViewData> admSummeryView = admSummery.ToAdminSummaryList();
                        sv.GetAdminSummaryReport = admSummeryView;
                        break;

                    case 10://Admin Exception Report                    
                        IEnumerable<GetAdminExceptionReportDto> admCardExcDto = _transactionService.AdminExceptionReport(sv.DateFrom, sv.DateTo);
                        IEnumerable<GetAdminExceptionReportViewData> admExcviewData = admCardExcDto.ToExceptionList();
                        sv.GetAdminExceptionReport = admExcviewData;
                        break;
                    
                    case 11://Admin Card Type Report
                        IEnumerable<GetCardTypeReportDto> admCardDto = _transactionService.AdminCardypeReport(sv.DateFrom, sv.DateTo);
                        IEnumerable<GetCardTypeReportViewData> admCardviewData = admCardDto.ToCardTypeList();
                        sv.GetCardTypeReport = admCardviewData;
                        break;
                    
                    case 12://Admin Total Transactions By Vehicle Report
                        IEnumerable<GetTotalTransByVehicleReportDto> admCardVehicleDto = _transactionService.TransByVehicleReport(sv.DateFrom, sv.DateTo);
                        IEnumerable<GetTotalTransByVehicleReportViewData> admVehicleviewData = admCardVehicleDto.ToVehicleReportList();
                        sv.AdminTotalTransactionsByVehicle = admVehicleviewData;
                        break;
                    
                    case 13://Merchant Summary Report
                        IEnumerable<GetMerchantSummaryReportDto> merchantSummaryDto = _transactionService.MerchantSummaryReport(sv.DateFrom, sv.DateTo, Convert.ToInt32(sv.fk_MerchantId));
                        IEnumerable<GetMerchantSummaryReportViewData> mSummary = merchantSummaryDto.ToSummaryList();
                        sv.MerchantSummaryReport = mSummary;
                        break;
                    
                    case 14://Merchant Detail Report
                        IEnumerable<GetDetailReportDto> detailReport = _transactionService.GetDetailsReport(sv.DateFrom, sv.DateTo, Convert.ToInt32(sv.fk_MerchantId));
                        IEnumerable<GetDetailReportViewData> dReport = detailReport.ToDetailList();
                        sv.DetailReport = dReport;
                        break;
                    
                    case 15://Merchant Exception Report
                        IEnumerable<GetMerchantExceptionReportDto> merchantException = _transactionService.GetMerchantExceptionReport(sv.DateFrom, sv.DateTo, Convert.ToInt32(sv.fk_MerchantId));
                        IEnumerable<GetMerchantExceptionReportViewData> merchantExceptionView = merchantException.ToMerchantSummaryExceptionList();
                        sv.MerchantExceptionReport = merchantExceptionView;
                        break;

                    case 16://Merchant Total Transactions By Vehicle Report
                        IEnumerable<GetMerchantTotalTransByVehicleReportDto> merchantTotalTransactions = _transactionService.GetMerchantTotalTransByVehicle(sv.DateFrom, sv.DateTo, Convert.ToInt32(sv.fk_MerchantId));
                        IEnumerable<GetMerchantTotalTransByVehicleReportViewData> merchantTotalView = merchantTotalTransactions.ToMerchantTotalTransList();
                        sv.MerchantTotalTransReport = merchantTotalView;
                        break;

                    case 17://Merchant Card Type Report
                        IEnumerable<GetMerchantCardTypeReportDto> marcCardDto = _transactionService.GetMerchantCardTypeReport(sv.DateFrom, sv.DateTo, Convert.ToInt32(sv.fk_MerchantId));
                        IEnumerable<GetMerchantCardTypeReportViewData> merCardviewData = marcCardDto.ToMerchantCardTypeList();
                        sv.GetMerchantCardTypeReport = merCardviewData;
                        break;
                }

                TempData.Keep("repValue");
                TempData.Keep("DateFrom");
                TempData.Keep("DateTo");
                TempData.Keep("MerValue");

                return View(sv);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        /// <summary>
        /// Export report data to CSV file.
        /// </summary>
        /// <param name="sv"></param>
        /// <returns></returns>
        [ActionName("OnDemandReport")]
        [AcceptVerbs(HttpVerbs.Post)]
        [AttributeModel(Name = "Number", Data = "Export")]
        public ActionResult Export(OnDemandReportViewData sv)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");

            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                StringWriter output = new StringWriter();
                string filename = string.Empty;

                switch (sv.fk_ReportID)
                {
                    case 9:     // Get admin Summary
                        IEnumerable<GetAdminSummaryReportDto> admSummery = _transactionService.GetadminSummary(sv.DateFrom, sv.DateTo);
                        output.WriteLine("\"Sr No\",\"Company\",\"FleetName\",\"No Of Trans\",\"FareAmt\",\"Tips\",\"Surcharges\",\"TechFee\",\"No Of Chargebacks\",\"Charge back Value\",\"No Of Void\",\"Void Value\",\"Total Amount\",\"Fees\",\"AmountOwing\"");

                        foreach (GetAdminSummaryReportDto item in admSummery)
                        {
                            output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\"",
                                item.Sr_No_, item.Company, item.FleetName, item.NoOfTrans, item.FareAmt, item.Tips, item.Surcharges, item.TechFee, item.NoOfChargebacks, item.ChargebackValue, item.NoOfVoid, item.VoidValue, item.TotalAmount, item.Fees, item.AmountOwing);
                        }

                        filename = "Admin_Summary.csv";
                        break;

                    case 10:    //Admin Exception Report
                        IEnumerable<GetAdminExceptionReportDto> admCardExcDto = _transactionService.AdminExceptionReport(sv.DateFrom, sv.DateTo);
                        output.WriteLine("\"Merchant Name\",\"Fleet Name\",\"Exp Fleet Trans\",\"Act Fleet Trans\",\"Exp Car\",\"Act Car\",\"Exp Value\",\"ActValue\",\"ExpCase\"");

                        foreach (GetAdminExceptionReportDto item in admCardExcDto)
                        {
                            output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\"",
                                item.MerchantName, item.FleetName, item.ExpFleetTrans, item.ActFleetTrans, item.ExpCar, item.ActCar, item.ExpValue, item.ActValue, item.ExpCase);
                        }

                        filename = "Admin_Exception.csv";
                        break;

                    case 11:    //Admin Card Type Report
                        IEnumerable<GetCardTypeReportDto> admCardDto = _transactionService.AdminCardypeReport(sv.DateFrom, sv.DateTo);
                        output.WriteLine("\"Sr No\",\"Merchant Name\",\"Fleet Name\",\"VISA Txn\",\"VISA\",\"Master Txn\",\"Master Card\",\"Amex Txn\",\"American Express\",\"DisTxn\",\"Discover\",\"JCB Txn\",\"JCB\",\"Diner Txn\",\"Diners Club\"");
                        
                        foreach (GetCardTypeReportDto item in admCardDto)
                        {
                            output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\"",
                                item.SrNo, item.MerchantName, item.FleetName, item.VISATxn, item.VISA, item.MasterTxn, item.MasterCard, item.AmexTxn, item.AmericanExpress,
                                    item.DisTxn, item.Discover, item.JCBTxn, item.JCB, item.DinerTxn, item.DinersClub);
                        }
                        filename = "Admin_CardTypeReport.csv";
                        break;

                    case 12:    //Admin Total Transactions By Vehicle Report
                        IEnumerable<GetTotalTransByVehicleReportDto> admCardVehicleDto = _transactionService.TransByVehicleReport(sv.DateFrom, sv.DateTo);
                        int counter = 1;                        
                        output.WriteLine("\"Sr No\",\"Company\",\"FleetName\",\"No Of Trans\",\"FareAmt\",\"Tips\",\"Surcharges\",\"TechFee\",\"No Of Chargebacks\",\"Charge back Value\",\"No Of Void\",\"Void Value\",\"Total Amount\",\"Fees\",\"AmountOwing\",\"NoOfVehilesForSlab1\",\"NoOfVehilesForSlab2\",\"NoOfVehilesForSlab3\",\"NoOfVehilesForSlab4\"");
                        
                        foreach (GetTotalTransByVehicleReportDto item in admCardVehicleDto)
                        {
                            output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\",\"{16}\",\"{17}\",\"{18}\"",
                                counter, item.MerchantName, item.FleetName, item.NoOfTrans, item.FareAmt, item.Tips, item.Surcharges, item.TechFee, item.NoOfChargebacks, item.ChargebackValue, item.NoOfVoid, item.VoidValue, item.TotalAmount, item.Fees, item.AmountOwing, item.NoOfVehilesForSlab1, item.NoOfVehilesForSlab2, item.NoOfVehilesForSlab3, item.NoOfVehilesForSlab4);

                            counter++;
                        }

                        filename = "Admin_TotalTransByVehicle.csv";
                        break;

                    case 13:    //Merchant Summary Report
                        IEnumerable<GetMerchantSummaryReportDto> merchantSummaryDto = _transactionService.MerchantSummaryReport(sv.DateFrom, sv.DateTo, Convert.ToInt32(sv.fk_MerchantId));
                        int count = 1;
                        output.WriteLine("\"Sr No\",\"Fleet Name\",\"Vehicle Number\",\"No Of Trans\",\"FareAmt\",\"Tips\",\"Surcharges\",\"TechFee\",\"No Of Chargebacks\",\"Charge back Value\",\"No Of Void\",\"Void Value\",\"Total Amount\",\"Fees\",\"AmountOwing\"");

                        foreach (GetMerchantSummaryReportDto item in merchantSummaryDto)
                        {
                            output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\"",
                                count, item.FleetName, item.VehicleNumber, item.NoOfTrans, item.FareAmt, item.Tips, item.Surcharges, item.TechFee, item.NoOfChargebacks, item.ChargebackValue, item.NoOfVoid, item.VoidValue, item.TotalAmount, item.Fees, item.AmountOwing);

                            count++;
                        }

                        filename = "Merchant_Summary.csv";
                        break;

                    case 14:    //Merchant Detail Report
                        IEnumerable<GetDetailReportDto> detailReport = _transactionService.GetDetailsReport(sv.DateFrom, sv.DateTo, Convert.ToInt32(sv.fk_MerchantId));
                        output.WriteLine("\"Company\",\"Report Date\",\"Trans Id\",\"Trans Date\",\"Txn Type\",\"Card Type\",\"Last Four\",\"Auth Code\",\"Trip\",\"DriverNo\",\"VehicleNo\",\"TechFee\",\"Surcharge\",\"Gross Amt\",\"Fees\",\"NetAmt\"");

                        foreach (GetDetailReportDto item in detailReport)
                        {
                            output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\"",
                                    item.Company, item.ReportDate, item.TransId, item.TransDate, item.TxnType, item.CardType, item.LastFour, item.AuthCode, item.Trip, item.DriverNo, item.VehicleNo, item.TechFee, item.Surcharge, item.GrossAmt, item.Fees, item.NetAmt);
                        }

                        filename = "Merchant_details.csv";
                        break;

                    case 15:    //Merchant Exception Report
                        IEnumerable<GetMerchantExceptionReportDto> merchantReport = _transactionService.GetMerchantExceptionReport(sv.DateFrom, sv.DateTo, Convert.ToInt32(sv.fk_MerchantId));
                        output.WriteLine("\"Fleet Name\",\"Vehicle No\",\"Fare Value\",\"Tip\",\"Surcharge\",\"TechFee\",\"Amount\",\"Fee\",\"AmountOwing\"");

                        foreach (GetMerchantExceptionReportDto item in merchantReport)
                        {
                            output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\"",
                                    item.FleetName, item.VehicleNo, item.FareValue, item.Tip, item.Surcharge, item.TechFee, item.Amount, item.Fee, item.AmountOwing);
                        }

                        filename = "Merchant_Exception.csv";
                        break;

                    case 16:    //Merchant Total Transactions By Vehicle Report
                        IEnumerable<GetMerchantTotalTransByVehicleReportDto> merchantTotalTransactions = _transactionService.GetMerchantTotalTransByVehicle(sv.DateFrom, sv.DateTo, Convert.ToInt32(sv.fk_MerchantId));
                        int counterd = 1;
                        output.WriteLine("\"Sr No\",\"FleetName\",\"No Of Trans\",\"FareAmt\",\"Tips\",\"Surcharges\",\"TechFee\",\"No Of Chargebacks\",\"Charge back Value\",\"No Of Void\",\"Void Value\",\"Total Amount\",\"Fees\",\"AmountOwing\",\"TransCountForSlab1\",\"TransCountForSlab2\",\"TransCountForSlab3\",\"TransCountForSlab4\"");

                        foreach (GetMerchantTotalTransByVehicleReportDto item in merchantTotalTransactions)
                        {
                            output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\",\"{16}\",\"{17}\"",
                                counterd, item.FleetName, item.NoOfTrans, item.FareAmt, item.Tips, item.Surcharges, item.TechFee, item.NoOfChargebacks, item.ChargebackValue, item.NoOfVoid, item.VoidValue, item.TotalAmount, item.Fees, item.AmountOwing, item.TransCountForSlab1, item.TransCountForSlab2, item.TransCountForSlab3, item.TransCountForSlab4);

                            counterd++;
                        }

                        filename = "Merchant_TotalTransactionsByVehicleReport.csv";
                        break;

                    case 17:    //Merchant Card Type Report
                        IEnumerable<GetMerchantCardTypeReportDto> merCardDto = _transactionService.GetMerchantCardTypeReport(sv.DateFrom, sv.DateTo, Convert.ToInt32(sv.fk_MerchantId));
                        output.WriteLine("\"Sr No\",\"Fleet Name\",\"VISA Txn\",\"VISA\",\"Master Txn\",\"Master Card\",\"Amex Txn\",\"American Express\",\"DisTxn\",\"Discover\",\"JCB Txn\",\"JCB\",\"Diner Txn\",\"Diners Club\"");
                        
                        foreach (GetMerchantCardTypeReportDto item in merCardDto)
                        {
                            output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\"",
                                item.SrNo, item.FleetName, item.VISATxn, item.VISA, item.MasterTxn, item.MasterCard, item.AmexTxn, item.AmericanExpress, item.DisTxn, item.Discover, item.JCBTxn, item.JCB, item.DisTxn, item.DinersClub);
                        }

                        filename = "Merchant_CardTypeReport.csv";
                        break;
                }

                // If we have a filename to output to, then do so.
                if (filename != string.Empty)
                    return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", filename);

                // Finished with the stream now.
                output.Close();
                output.Dispose();
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [NonAction]
        private List<MerchantListViewData> MerchantList()
        {
            IEnumerable<MerchantListDto> mvd = _reportcommonRepository.GetMerchantList();
            List<MerchantListViewData> mld = mvd.Select(merchant => new MerchantListViewData
            {
                Company = merchant.Company,
                fk_MerchantID = merchant.fk_MerchantID
            }).ToList();

            return mld;
        }

        //Merchant On Demand Report
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult MerchantOnDemandReport()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            var sDto = ((SessionDto)Session["User"]).SessionUser;

            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || sDto.fk_UserTypeID != 2 && sDto.fk_UserTypeID != 3)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                OnDemandReportViewData sv = new OnDemandReportViewData();
                string v = Request.QueryString["sort"];
                string p = Request.QueryString["page"];
                int? mId = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;
                int mIds = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);

                if (v != null || p != null)
                {
                    sv.fk_ReportID = Convert.ToInt32(TempData["repValue"]);
                    sv.DateFrom = Convert.ToDateTime(TempData["DateFrom"]);
                    sv.DateTo = Convert.ToDateTime(TempData["DateTo"]);
                    sv.fk_MerchantId = Convert.ToInt32(TempData["MerValue"]);
                    sv.IsReport = true;
                    TempData.Keep("repValue");
                    TempData.Keep("DateFrom");
                    TempData.Keep("DateTo");
                    TempData.Keep("MerValue");

                    switch (sv.fk_ReportID)
                    {
                        case 17://Admin Card Type Report
                            IEnumerable<GetCardTypeReportDto> admCardDto = _transactionService.AdminCardypeReport(sv.DateFrom, sv.DateTo);
                            IEnumerable<GetCardTypeReportViewData> admCardviewData = admCardDto.ToCardTypeList();
                            sv.GetCardTypeReport = admCardviewData;
                            break;

                        case 13://Merchant Summary Report
                            IEnumerable<GetMerchantSummaryReportDto> merchantSummaryDto = _transactionService.MerchantSummaryReport(sv.DateFrom, sv.DateTo, mIds);
                            IEnumerable<GetMerchantSummaryReportViewData> mSummary = merchantSummaryDto.ToSummaryList();
                            sv.MerchantSummaryReport = mSummary;
                            break;

                        case 14://Merchant Detail Report
                            IEnumerable<GetDetailReportDto> detailReport = _transactionService.GetDetailsReport(sv.DateFrom, sv.DateTo, mIds);
                            IEnumerable<GetDetailReportViewData> dReport = detailReport.ToDetailList();
                            sv.DetailReport = dReport;
                            break;

                        case 15://Merchant Exception Report
                            IEnumerable<GetMerchantExceptionReportDto> merchantException = _transactionService.GetMerchantExceptionReport(sv.DateFrom, sv.DateTo, mIds);
                            IEnumerable<GetMerchantExceptionReportViewData> merchantExceptionView = merchantException.ToMerchantSummaryExceptionList();
                            sv.MerchantExceptionReport = merchantExceptionView;
                            break;

                        case 16://Merchant Total Transactions By Vehicle Report
                            IEnumerable<GetMerchantTotalTransByVehicleReportDto> merchantTotalTransactions = _transactionService.GetMerchantTotalTransByVehicle(sv.DateFrom, sv.DateTo, mIds);
                            IEnumerable<GetMerchantTotalTransByVehicleReportViewData> merchantETransactionsView = merchantTotalTransactions.ToMerchantTotalTransList();
                            sv.MerchantTotalTransReport = merchantETransactionsView;
                            break;

                        case 19://Merchant User Card Type Report
                            IEnumerable<GetCardTypeReportDto> muCardDto = _transactionService.GetMerchantUserCardTypeReport(sv.DateFrom, sv.DateTo, mId, sv.fk_MerchantId);
                            IEnumerable<GetCardTypeReportViewData> muCardviewData = muCardDto.ToCardTypeList();
                            sv.GetCardTypeReport = muCardviewData;
                            break;

                        case 20://Merchant User Summary Report
                            IEnumerable<GetMerchantSummaryReportDto> merchantuSummaryDto = _transactionService.GetMerchantUserSummaryReport(sv.DateFrom, sv.DateTo, mId, sv.fk_MerchantId);
                            IEnumerable<GetMerchantSummaryReportViewData> muSummary = merchantuSummaryDto.ToSummaryList();
                            sv.MerchantSummaryReport = muSummary;
                            break;

                        case 21://Merchant User Detail Report
                            IEnumerable<GetDetailReportDto> muDetailReport = _transactionService.GetMerchantUserDetailReport(sv.DateFrom, sv.DateTo, mId, sv.fk_MerchantId);//
                            IEnumerable<GetDetailReportViewData> mudReport = muDetailReport.ToDetailList();
                            sv.DetailReport = mudReport;
                            break;

                        case 22://Merchant User Exception Report
                            break;

                        case 23://Merchant User Total Transactions By Vehicle Report
                            IEnumerable<GetMerchantTotalTransByVehicleReportDto> merchantuTotalTransactions = _transactionService.GetMerchantUserTotalTransByVehicleReport(sv.DateFrom, sv.DateTo, mId, sv.fk_MerchantId);
                            IEnumerable<GetMerchantTotalTransByVehicleReportViewData> merchantuTotalView = merchantuTotalTransactions.ToMerchantTotalTransList();
                            sv.MerchantTotalTransReport = merchantuTotalView;
                            break;
                    }
                }

                sv.ReportList = ReportList(12, 24);
                sv.MerchantList = MerchantUserList(Convert.ToInt32(sDto.fk_MerchantID));
                return View(sv);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sv"></param>
        /// <returns></returns>
        [ActionName("MerchantOnDemandReport")]
        [AcceptVerbs(HttpVerbs.Post)]
        [AttributeModel(Name = "MNumber", Data = "Email Report")]
        public ActionResult EmailReport(OnDemandReportViewData sv)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            var sDto = ((SessionDto)Session["User"]).SessionUser;
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || sDto.fk_UserTypeID != 2 && sDto.fk_UserTypeID != 3)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int runStatus = 0;
                sv.ReportList = ReportList(12, 24);
                sv.MerchantList = MerchantUserList(Convert.ToInt32(sDto.fk_MerchantID));
                int? mId = sDto.fk_MerchantID;

                ViewBag.Message = "Please configure admin or merchant first.";

                switch (sv.fk_ReportID)
                {
                    case 17://Merchant Card Type Report
                        runStatus = _transactionService.MerchantCardTypeReportScheduler(true, sv.DateFrom, sv.DateTo, mId);
                        break;

                    case 13://Merchant Summary Report
                        runStatus = _transactionService.MerchantSummaryReportScheduler(true, sv.DateFrom, sv.DateTo, mId);
                        break;
                    
                    case 14://Merchant Detail Report
                        runStatus = _transactionService.DetailReportScheduler(true, sv.DateFrom, sv.DateTo, mId);
                        break;
                    
                    case 15://Merchant Exception Report
                        runStatus = _transactionService.MerchantExceptionReportScheduler(true, sv.DateFrom, sv.DateTo);
                        break;
                    
                    case 16://Merchant Total Transactions By Vehicle Report
                        runStatus = _transactionService.MerchantTotalTransByVehicleScheduler(true, sv.DateFrom, sv.DateTo, mId);
                        break;
                    
                    case 19://Merchant User Card Type Report
                        runStatus = _transactionService.MerchantUserCardTypeReportScheduler(true, sv.DateFrom, sv.DateTo, mId, sv.fk_MerchantId);
                        break;
                    
                    case 20://Merchant User Summary Report
                        runStatus = _transactionService.MerchantUserSummaryReportScheduler(true, sv.DateFrom, sv.DateTo, null, sv.fk_MerchantId);
                        break;
                    
                    case 21://Merchant User Detail Report
                        runStatus = _transactionService.MerchantUserDetailReportScheduler(true, sv.DateFrom, sv.DateTo, mId, sv.fk_MerchantId);
                        break;
                    
                    case 22://Merchant User Exception Report
                        runStatus = _transactionService.MerchantUserExceptionReportScheduler(true, sv.DateFrom, sv.DateTo, sv.fk_MerchantId);
                        break;
                    
                    case 23://Merchant User Total Transactions By Vehicle Report
                        runStatus = _transactionService.MerchantUserTotalTransByVehicleScheduler(true, sv.DateFrom, sv.DateTo, mId, sv.fk_MerchantId);
                        break;
                }

                if (runStatus == -1)
                    ViewBag.Message = "Report run successfully";

                return View(sv);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            ViewBag.Message = "Report Run Successfully";
            return View(sv);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sv"></param>
        /// <returns></returns>
        [ActionName("MerchantOnDemandReport")]
        [AcceptVerbs(HttpVerbs.Post)]
        [AttributeModel(Name = "MNumber", Data = "View Report")]
        public ActionResult ShowReport(OnDemandReportViewData sv)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            var sDto = ((SessionDto)Session["User"]).SessionUser;
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || sDto.fk_UserTypeID != 2 && sDto.fk_UserTypeID != 3)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int merchantId = (int)((SessionDto)Session["User"]).SessionUser.fk_MerchantID;
                int? mId = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;
                TempData["repValue"] = sv.fk_ReportID;

                if (sv.fk_MerchantId != null)
                    TempData["MerValue"] = sv.fk_MerchantId;
                
                TempData["DateFrom"] = sv.DateFrom;
                TempData["DateTo"] = sv.DateTo;
                TempData["IsDateSet"] = true;

                sv.ReportList = ReportList(12, 24);
                sv.MerchantList = MerchantUserList(Convert.ToInt32(sDto.fk_MerchantID));
                
                switch (sv.fk_ReportID)
                {
                    case 17://Merchant Card Type Report
                        IEnumerable<GetMerchantCardTypeReportDto> admCardDto = _transactionService.GetMerchantCardTypeReport(sv.DateFrom, sv.DateTo, merchantId);
                        IEnumerable<GetMerchantCardTypeReportViewData> merCardviewData = admCardDto.ToMerchantCardTypeList();
                        sv.GetMerchantCardTypeReport = merCardviewData;
                        break;

                    case 13://Merchant Summary Report
                        IEnumerable<GetMerchantSummaryReportDto> merchantSummaryDto = _transactionService.MerchantSummaryReport(sv.DateFrom, sv.DateTo, merchantId);
                        IEnumerable<GetMerchantSummaryReportViewData> mSummary = merchantSummaryDto.ToSummaryList();
                        sv.MerchantSummaryReport = mSummary;
                        break;
                    
                    case 14://Merchant Detail Report
                        IEnumerable<GetDetailReportDto> detailReport = _transactionService.GetDetailsReport(sv.DateFrom, sv.DateTo, merchantId);
                        IEnumerable<GetDetailReportViewData> dReport = detailReport.ToDetailList();
                        sv.DetailReport = dReport;
                        break;
                    
                    case 15://Merchant Exception Report
                        IEnumerable<GetMerchantExceptionReportDto> merchantException = _transactionService.GetMerchantExceptionReport(sv.DateFrom, sv.DateTo, merchantId);
                        IEnumerable<GetMerchantExceptionReportViewData> merchantExceptionView = merchantException.ToMerchantSummaryExceptionList();
                        sv.MerchantExceptionReport = merchantExceptionView;
                        break;
                    
                    case 16://Merchant Total Transactions By Vehicle Report
                        IEnumerable<GetMerchantTotalTransByVehicleReportDto> merchantTotalTransactions = _transactionService.GetMerchantTotalTransByVehicle(sv.DateFrom, sv.DateTo, merchantId);
                        IEnumerable<GetMerchantTotalTransByVehicleReportViewData> merchantTotalView = merchantTotalTransactions.ToMerchantTotalTransList();
                        sv.MerchantTotalTransReport = merchantTotalView;
                        break;
                    
                    case 19://Merchant User Card Type Report
                        IEnumerable<GetCardTypeReportDto> muCardDto = _transactionService.GetMerchantUserCardTypeReport(sv.DateFrom, sv.DateTo, mId, sv.fk_MerchantId);
                        IEnumerable<GetCardTypeReportViewData> muCardviewData = muCardDto.ToCardTypeList();
                        sv.GetCardTypeReport = muCardviewData;
                        break;
                    
                    case 20://Merchant User Summary Report
                        IEnumerable<GetMerchantSummaryReportDto> merchantuSummaryDto = _transactionService.GetMerchantUserSummaryReport(sv.DateFrom, sv.DateTo, mId, sv.fk_MerchantId);
                        IEnumerable<GetMerchantSummaryReportViewData> muSummary = merchantuSummaryDto.ToSummaryList();
                        sv.MerchantSummaryReport = muSummary;
                        break;
                    
                    case 21://Merchant User Detail Report
                        IEnumerable<GetDetailReportDto> muDetailReport = _transactionService.GetMerchantUserDetailReport(sv.DateFrom, sv.DateTo, mId, sv.fk_MerchantId);//
                        IEnumerable<GetDetailReportViewData> mudReport = muDetailReport.ToDetailList();
                        sv.DetailReport = mudReport;
                        break;
                    
                    case 22://Merchant User Exception Report
                        IEnumerable<GetMerchantExceptionReportDto> merchantUserException = _transactionService.GetMerchantUserExceptionReport(sv.DateFrom, sv.DateTo, sv.fk_MerchantId);
                        IEnumerable<GetMerchantExceptionReportViewData> merchantusrExceptionView = merchantUserException.ToMerchantSummaryExceptionList();
                        sv.MerchantExceptionReport = merchantusrExceptionView;
                        break;
                    
                    case 23://Merchant User Total Transactions By Vehicle Report
                        IEnumerable<GetMerchantTotalTransByVehicleReportDto> merchantuTotalTransactions = _transactionService.GetMerchantUserTotalTransByVehicleReport(sv.DateFrom, sv.DateTo, mId, sv.fk_MerchantId);
                        IEnumerable<GetMerchantTotalTransByVehicleReportViewData> merchantuTotalView = merchantuTotalTransactions.ToMerchantTotalTransList();
                        sv.MerchantTotalTransReport = merchantuTotalView;
                        break;
                }

                TempData.Keep("repValue");
                TempData.Keep("DateFrom");
                TempData.Keep("DateTo");
                TempData.Keep("MerValue");

                return View(sv);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sv"></param>
        /// <returns></returns>
        [ActionName("MerchantOnDemandReport")]
        [AcceptVerbs(HttpVerbs.Post)]
        [AttributeModel(Name = "MNumber", Data = "Export")]
        public ActionResult ExportReport(OnDemandReportViewData sv)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            var sDto = ((SessionDto)Session["User"]).SessionUser;
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || sDto.fk_UserTypeID != 2 && sDto.fk_UserTypeID != 3)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int merchantId = (int)((SessionDto)Session["User"]).SessionUser.fk_MerchantID;
                int? mId = ((SessionDto)Session["User"]).SessionUser.fk_MerchantID;
                switch (sv.fk_ReportID)
                {
                    case 17://Merchant Card Type Report
                        IEnumerable<GetMerchantCardTypeReportDto> merCardDto = _transactionService.GetMerchantCardTypeReport(sv.DateFrom, sv.DateTo, merchantId);
                        using (StringWriter output = new StringWriter())
                        {
                            output.WriteLine( "\"Sr No\",\"Fleet Name\",\"VISA Txn\",\"VISA\",\"Master Txn\",\"Master Card\",\"Amex Txn\",\"American Express\",\"DisTxn\",\"Discover\",\"JCB Txn\",\"JCB\",\"Diner Txn\",\"Diners Club\"");
                            foreach (GetMerchantCardTypeReportDto item in merCardDto)
                            {
                                output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\"",
                                        item.SrNo, item.FleetName, item.VISATxn, item.VISA, item.MasterTxn, item.MasterCard, item.AmexTxn, item.AmericanExpress,
                                        item.DisTxn, item.Discover, item.JCBTxn, item.JCB, item.DisTxn, item.DinersClub);
                            }

                            return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Merchant_CardTypeReport.csv");
                        }

                    case 13://Merchant Summary Report
                        IEnumerable<GetMerchantSummaryReportDto> merchantSummaryDto = _transactionService.MerchantSummaryReport(sv.DateFrom, sv.DateTo, merchantId);
                        using (StringWriter output = new StringWriter())
                        {
                            int count = 1;
                            output.WriteLine("\"Sr No\",\"Fleet Name\",\"Vehicle Number\",\"No Of Trans\",\"FareAmt\",\"Tips\",\"Surcharges\",\"TechFee\",\"No Of Chargebacks\",\"Charge back Value\",\"No Of Void\",\"Void Value\",\"Total Amount\",\"Fees\",\"AmountOwing\"");
                            foreach (GetMerchantSummaryReportDto item in merchantSummaryDto)
                            {
                                output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\"",
                                        count, item.FleetName, item.VehicleNumber, item.NoOfTrans, item.FareAmt, item.Tips, item.Surcharges, item.TechFee, item.NoOfChargebacks,
                                        item.ChargebackValue, item.NoOfVoid, item.VoidValue, item.TotalAmount, item.Fees, item.AmountOwing);

                                count++;
                            }

                            return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Merchant_SummaryReport.csv");
                        }

                    case 14://Merchant Detail Report
                        IEnumerable<GetDetailReportDto> detailReport = _transactionService.GetDetailsReport(sv.DateFrom, sv.DateTo, merchantId);
                        using (StringWriter output = new StringWriter())
                        {
                            output.WriteLine("\"Company\",\"Report Date\",\"Trans Id\",\"Trans Date\",\"Txn Type\",\"Card Type\",\"Last Four\",\"Auth Code\",\"Trip\",\"DriverNo\",\"VehicleNo\",\"TechFee\",\"Surcharge\",\"Gross Amt\",\"Fees\",\"NetAmt\"");
                            foreach (GetDetailReportDto item in detailReport)
                            {
                                output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\"",
                                     item.Company, item.ReportDate, item.TransId, item.TransDate, item.TxnType, item.CardType, item.LastFour, item.AuthCode,
                                     item.Trip, item.DriverNo, item.VehicleNo, item.TechFee, item.Surcharge, item.GrossAmt, item.Fees, item.NetAmt);
                            }

                            return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Merchant_detailReport.csv");
                        }

                    case 15://Merchant Exception Report
                        IEnumerable<GetMerchantExceptionReportDto> merchantReport = _transactionService.GetMerchantExceptionReport(sv.DateFrom, sv.DateTo, merchantId);
                        using (StringWriter output = new StringWriter())
                        {
                            output.WriteLine("\"Fleet Name\",\"Vehicle No\",\"Fare Value\",\"Tip\",\"Surcharge\",\"TechFee\",\"Amount\",\"Fee\",\"AmountOwing\"");
                            foreach (GetMerchantExceptionReportDto item in merchantReport)
                            {
                                output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\"",
                                     item.FleetName, item.VehicleNo, item.FareValue, item.Tip, item.Surcharge, item.TechFee, item.Amount, item.Fee,
                                     item.AmountOwing
                                     );
                            }

                            return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Merchant_ExceptionReport.csv");
                        }

                    case 16://Merchant Total Transactions By Vehicle Report
                        IEnumerable<GetMerchantTotalTransByVehicleReportDto> merchantTotalTransactions = _transactionService.GetMerchantTotalTransByVehicle(sv.DateFrom, sv.DateTo, merchantId);
                        using (StringWriter output = new StringWriter())
                        {
                            int counterd = 1;
                            output.WriteLine("\"Sr No\",\"FleetName\",\"No Of Trans\",\"FareAmt\",\"Tips\",\"Surcharges\",\"TechFee\",\"No Of Chargebacks\",\"Charge back Value\",\"No Of Void\",\"Void Value\",\"Total Amount\",\"Fees\",\"AmountOwing\",\"TransCountForSlab1\",\"TransCountForSlab2\",\"TransCountForSlab3\",\"TransCountForSlab4\"");
                            foreach (GetMerchantTotalTransByVehicleReportDto item in merchantTotalTransactions)
                            {
                                output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\",\"{16}\",\"{17}\"",
                                        counterd, item.FleetName, item.NoOfTrans, item.FareAmt, item.Tips, item.Surcharges, item.TechFee, item.NoOfChargebacks,
                                        item.ChargebackValue, item.NoOfVoid, item.VoidValue, item.TotalAmount, item.Fees, item.AmountOwing, item.TransCountForSlab1, item.TransCountForSlab2, item.TransCountForSlab3, item.TransCountForSlab4);
                                counterd++;
                            }

                            return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Merchant_TotalTransactionsByVehicleReport.csv");
                        }

                    case 19://Merchant user Card Type Report
                        IEnumerable<GetCardTypeReportDto> muCardDto = _transactionService.GetMerchantUserCardTypeReport(sv.DateFrom, sv.DateTo, mId, sv.fk_MerchantId);
                        using (StringWriter output = new StringWriter())
                        {
                            output.WriteLine("\"Sr No\",\"Merchant Name\",\"Fleet Name\",\"VISA Txn\",\"VISA\",\"Master Txn\",\"Master Card\",\"Amex Txn\",\"American Express\",\"DisTxn\",\"Discover\",\"JCB Txn\",\"JCB\",\"Diner Txn\",\"Diners Club\"");
                            foreach (GetCardTypeReportDto item in muCardDto)
                            {
                                output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\"",
                                        item.SrNo, item.MerchantName, item.FleetName, item.VISATxn, item.VISA, item.MasterTxn, item.MasterCard, item.AmexTxn, item.AmericanExpress,
                                        item.DisTxn, item.Discover, item.JCBTxn, item.JCB, item.DinerTxn, item.DinersClub);
                            }

                            return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Merchantuser_CardTypeReport.csv");
                        }

                    case 20://Merchant user Summary Report
                        IEnumerable<GetMerchantSummaryReportDto> merchantuSummaryDto = _transactionService.GetMerchantUserSummaryReport(sv.DateFrom, sv.DateTo, mId, sv.fk_MerchantId);
                        using (StringWriter output = new StringWriter())
                        {
                            int count = 1;
                            output.WriteLine("\"Sr No\",\"Fleet Name\",\"Vehicle Number\",\"No Of Trans\",\"FareAmt\",\"Tips\",\"Surcharges\",\"TechFee\",\"No Of Chargebacks\",\"Charge back Value\",\"No Of Void\",\"Void Value\",\"Total Amount\",\"Fees\",\"AmountOwing\"");
                            foreach (GetMerchantSummaryReportDto item in merchantuSummaryDto)
                            {
                                output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\"",
                                        count, item.FleetName, item.VehicleNumber, item.NoOfTrans, item.FareAmt, item.Tips, item.Surcharges, item.TechFee, item.NoOfChargebacks,
                                        item.ChargebackValue, item.NoOfVoid, item.VoidValue, item.TotalAmount, item.Fees, item.AmountOwing);
                                count++;
                            }

                            return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Merchantuser_SummaryReport.csv");
                        }

                    case 21://Merchant user Detail Report
                        IEnumerable<GetDetailReportDto> muDetailReport = _transactionService.GetMerchantUserDetailReport(sv.DateFrom, sv.DateTo, mId, sv.fk_MerchantId);//
                        using (StringWriter output = new StringWriter())
                        {
                            output.WriteLine("\"Company\",\"Report Date\",\"Trans Id\",\"Trans Date\",\"Txn Type\",\"Card Type\",\"Last Four\",\"Auth Code\",\"Trip\",\"DriverNo\",\"VehicleNo\",\"TechFee\",\"Surcharge\",\"Gross Amt\",\"Fees\",\"NetAmt\"");
                            foreach (GetDetailReportDto item in muDetailReport)
                            {
                                output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\"",
                                     item.Company, item.ReportDate, item.TransId, item.TransDate, item.TxnType, item.CardType, item.LastFour, item.AuthCode,
                                     item.Trip, item.DriverNo, item.VehicleNo, item.TechFee, item.Surcharge, item.GrossAmt, item.Fees, item.NetAmt);
                            }

                            return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Merchantuser_detailsReport.csv");
                        }

                    case 22://Merchant user Exception Report
                        IEnumerable<GetMerchantExceptionReportDto> merchantUserException = _transactionService.GetMerchantUserExceptionReport(sv.DateFrom, sv.DateTo, sv.fk_MerchantId);
                        using (StringWriter output = new StringWriter())
                        {
                            output.WriteLine("\"Fleet Name\",\"Vehicle No\",\"Fare Value\",\"Tip\",\"Surcharge\",\"TechFee\",\"Amount\",\"Fee\",\"AmountOwing\"");
                            foreach (GetMerchantExceptionReportDto item in merchantUserException)
                            {
                                output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\"",
                                     item.FleetName, item.VehicleNo, item.FareValue, item.Tip, item.Surcharge, item.TechFee, item.Amount, item.Fee,
                                     item.AmountOwing
                                     );
                            }

                            return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Merchantuser_ExceptionReport.csv");
                        }

                    case 23://Merchant User Total Transactions By Vehicle Report
                        IEnumerable<GetMerchantTotalTransByVehicleReportDto> merchantuTotalTransactions = _transactionService.GetMerchantUserTotalTransByVehicleReport(sv.DateFrom, sv.DateTo, mId, sv.fk_MerchantId);
                        using (StringWriter output = new StringWriter())
                        {
                            int counterd = 1;
                            output.WriteLine("\"Sr No\",\"FleetName\",\"No Of Trans\",\"FareAmt\",\"Tips\",\"Surcharges\",\"TechFee\",\"No Of Chargebacks\",\"Charge back Value\",\"No Of Void\",\"Void Value\",\"Total Amount\",\"Fees\",\"AmountOwing\",\"TransCountForSlab1\",\"TransCountForSlab2\",\"TransCountForSlab3\",\"TransCountForSlab4\"");
                            foreach (GetMerchantTotalTransByVehicleReportDto item in merchantuTotalTransactions)
                            {
                                output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\",\"{16}\",\"{17}\"",
                                        counterd, item.FleetName, item.NoOfTrans, item.FareAmt, item.Tips, item.Surcharges, item.TechFee, item.NoOfChargebacks,
                                        item.ChargebackValue, item.NoOfVoid, item.VoidValue, item.TotalAmount, item.Fees, item.AmountOwing, item.TransCountForSlab1, item.TransCountForSlab2, item.TransCountForSlab3, item.TransCountForSlab4);

                                counterd++;
                            }

                            return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "MerchantUser_TotalTransactionsByVehicleReport.csv");
                        }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        //Merchant User On Demand Report
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult MerchantUserOnDemandReport()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            var sDto = ((SessionDto)Session["User"]).SessionUser;
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || sDto.fk_UserTypeID != 4)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int mUsrId = sDto.UserID;
                OnDemandReportViewData sv = new OnDemandReportViewData();
                string v = Request.QueryString["sort"];
                string p = Request.QueryString["page"];
                int? mId = null;
                if (v != null || p != null)
                {
                    sv.fk_ReportID = Convert.ToInt32(TempData["repValue"]);
                    sv.DateFrom = Convert.ToDateTime(TempData["DateFrom"]);
                    sv.DateTo = Convert.ToDateTime(TempData["DateTo"]);
                    sv.fk_MerchantId = Convert.ToInt32(TempData["MerValue"]);
                    sv.IsReport = true;
                    TempData.Keep("repValue");
                    TempData.Keep("DateFrom");
                    TempData.Keep("DateTo");
                    TempData.Keep("MerValue");
                    switch (sv.fk_ReportID)
                    {
                        case 19://Merchant User Card Type Report
                            IEnumerable<GetCardTypeReportDto> muCardDto = _transactionService.GetMerchantUserCardTypeReport(sv.DateFrom, sv.DateTo, mId, mUsrId);
                            IEnumerable<GetCardTypeReportViewData> muCardviewData = muCardDto.ToCardTypeList();
                            sv.GetCardTypeReport = muCardviewData;
                            break;

                        case 20://Merchant User Summary Report
                            IEnumerable<GetMerchantSummaryReportDto> merchantuSummaryDto = _transactionService.GetMerchantUserSummaryReport(sv.DateFrom, sv.DateTo, mId, mUsrId);
                            IEnumerable<GetMerchantSummaryReportViewData> muSummary = merchantuSummaryDto.ToSummaryList();
                            sv.MerchantSummaryReport = muSummary;
                            break;
                        
                        case 21://Merchant User Detail Report
                            IEnumerable<GetDetailReportDto> muDetailReport = _transactionService.GetMerchantUserDetailReport(sv.DateFrom, sv.DateTo, mId, mUsrId);//
                            IEnumerable<GetDetailReportViewData> mudReport = muDetailReport.ToDetailList();
                            sv.DetailReport = mudReport;
                            break;
                        
                        case 22://Merchant User Exception Report
                            break;
                        
                        case 23://Merchant User Total Transactions By Vehicle Report
                            IEnumerable<GetMerchantTotalTransByVehicleReportDto> merchantuTotalTransactions = _transactionService.GetMerchantUserTotalTransByVehicleReport(sv.DateFrom, sv.DateTo, mId, mUsrId);
                            IEnumerable<GetMerchantTotalTransByVehicleReportViewData> merchantuTotalView = merchantuTotalTransactions.ToMerchantTotalTransList();
                            sv.MerchantTotalTransReport = merchantuTotalView;
                            break;
                    }
                }

                sv.ReportList = ReportList(18, 24);

                return View(sv);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sv"></param>
        /// <returns></returns>
        [ActionName("MerchantUserOnDemandReport")]
        [AcceptVerbs(HttpVerbs.Post)]
        [AttributeModel(Name = "UNumber", Data = "Email Report")]
        public ActionResult EmailUserReport(OnDemandReportViewData sv)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            var sDto = ((SessionDto)Session["User"]).SessionUser;
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || sDto.fk_UserTypeID != 4)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int runStatus = 0;
                sv.ReportList = ReportList(18, 24);

                ViewBag.Message = "Please configure admin or merchant first.";
                int? uId = sDto.UserID;
                switch (sv.fk_ReportID)
                {
                    case 19://Merchant User Card Type Report
                        runStatus = _transactionService.MerchantUserCardTypeReportScheduler(true, sv.DateFrom, sv.DateTo, null, uId);
                        break;

                    case 20://Merchant User Summary Report
                        runStatus = _transactionService.MerchantUserSummaryReportScheduler(true, sv.DateFrom, sv.DateTo, null, uId);
                        break;
                    
                    case 21://Merchant User Detail Report
                        runStatus = _transactionService.MerchantUserDetailReportScheduler(true, sv.DateFrom, sv.DateTo, null, uId);
                        break;
                    
                    case 22://Merchant User Exception Report
                        runStatus = _transactionService.MerchantUserExceptionReportScheduler(true, sv.DateFrom, sv.DateTo, uId);
                        break;
                    
                    case 23://Merchant User Total Transactions By Vehicle Report
                        runStatus = _transactionService.MerchantUserTotalTransByVehicleScheduler(true, sv.DateFrom, sv.DateTo, null, uId);
                        break;
                }

                if (runStatus == -1)
                    ViewBag.Message = "Report run successfully";

                return View(sv);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            ViewBag.Message = "Report Run Successfully";
            return View(sv);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sv"></param>
        /// <returns></returns>
        [ActionName("MerchantUserOnDemandReport")]
        [AcceptVerbs(HttpVerbs.Post)]
        [AttributeModel(Name = "UNumber", Data = "View Report")]
        public ActionResult ShowUserReport(OnDemandReportViewData sv)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            var sDto = ((SessionDto)Session["User"]).SessionUser;
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || sDto.fk_UserTypeID != 4)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int? merchantUserId = ((SessionDto)Session["User"]).SessionUser.UserID;
                int? mId = null;
                TempData["repValue"] = sv.fk_ReportID;
                TempData["MerValue"] = sv.fk_MerchantId;
                TempData["DateFrom"] = sv.DateFrom;
                TempData["DateTo"] = sv.DateTo;
                sv.ReportList = ReportList(18, 24);

                switch (sv.fk_ReportID)
                {
                    case 19://Merchant User Card Type Report
                        IEnumerable<GetCardTypeReportDto> muCardDto = _transactionService.GetMerchantUserCardTypeReport(sv.DateFrom, sv.DateTo, mId, merchantUserId);
                        IEnumerable<GetCardTypeReportViewData> muCardviewData = muCardDto.ToCardTypeList();
                        sv.GetCardTypeReport = muCardviewData;
                        break;

                    case 20://Merchant User Summary Report
                        IEnumerable<GetMerchantSummaryReportDto> merchantuSummaryDto = _transactionService.GetMerchantUserSummaryReport(sv.DateFrom, sv.DateTo, mId, merchantUserId);
                        IEnumerable<GetMerchantSummaryReportViewData> muSummary = merchantuSummaryDto.ToSummaryList();
                        sv.MerchantSummaryReport = muSummary;
                        break;
                    
                    case 21://Merchant User Detail Report
                        IEnumerable<GetDetailReportDto> muDetailReport = _transactionService.GetMerchantUserDetailReport(sv.DateFrom, sv.DateTo, mId, merchantUserId);//
                        IEnumerable<GetDetailReportViewData> mudReport = muDetailReport.ToDetailList();
                        sv.DetailReport = mudReport;
                        break;
                    
                    case 22://Merchant User Exception Report
                        IEnumerable<GetMerchantExceptionReportDto> merchantUserException = _transactionService.GetMerchantUserExceptionReport(sv.DateFrom, sv.DateTo, sv.fk_MerchantId);
                        IEnumerable<GetMerchantExceptionReportViewData> merchantusrExceptionView = merchantUserException.ToMerchantSummaryExceptionList();
                        sv.MerchantExceptionReport = merchantusrExceptionView;
                        break;
                    
                    case 23://Merchant User Total Transactions By Vehicle Report
                        IEnumerable<GetMerchantTotalTransByVehicleReportDto> merchantuTotalTransactions = _transactionService.GetMerchantUserTotalTransByVehicleReport(sv.DateFrom, sv.DateTo, mId, merchantUserId);
                        IEnumerable<GetMerchantTotalTransByVehicleReportViewData> merchantuTotalView = merchantuTotalTransactions.ToMerchantTotalTransListView();
                        sv.MerchantTotalTransReport = merchantuTotalView;
                        break;
                }

                TempData.Keep("repValue");
                TempData.Keep("DateFrom");
                TempData.Keep("DateTo");
                TempData.Keep("MerValue");

                return View(sv);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sv"></param>
        /// <returns></returns>
        [ActionName("MerchantUserOnDemandReport")]
        [AcceptVerbs(HttpVerbs.Post)]
        [AttributeModel(Name = "UNumber", Data = "Export")]
        public ActionResult ExportUserReport(OnDemandReportViewData sv)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            var sDto = ((SessionDto)Session["User"]).SessionUser;
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || sDto.fk_UserTypeID != 4)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int? merchantUserId = ((SessionDto)Session["User"]).SessionUser.UserID;
                int merchantId = (int)((SessionDto)Session["User"]).SessionUser.fk_MerchantID;
                int? mId = null;
                switch (sv.fk_ReportID)
                {
                    case 19://Merchant Card Type Report
                        IEnumerable<GetCardTypeReportDto> muCardDto = _transactionService.GetMerchantUserCardTypeReport(sv.DateFrom, sv.DateTo, mId, merchantUserId);
                        using (StringWriter output = new StringWriter())
                        {
                            output.WriteLine("\"Sr No\",\"Merchant Name\",\"Fleet Name\",\"VISA Txn\",\"VISA\",\"Master Txn\",\"Master Card\",\"Amex Txn\",\"American Express\",\"DisTxn\",\"Discover\",\"JCB Txn\",\"JCB\",\"Diner Txn\",\"Diners Club\"");
                            foreach (GetCardTypeReportDto item in muCardDto)
                            {
                                output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\"",
                                    item.SrNo, item.MerchantName, item.FleetName, item.VISATxn, item.VISA, item.MasterTxn, item.MasterCard, item.AmexTxn, item.AmericanExpress, item.DisTxn, item.Discover, item.JCBTxn, item.JCB, item.DinerTxn, item.DinersClub);
                            }

                            return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "MerchantUser_CardTypeReport.csv");
                        }

                    case 20://Merchant Summary Report
                        IEnumerable<GetMerchantSummaryReportDto> merchantuSummaryDto = _transactionService.GetMerchantUserSummaryReport(sv.DateFrom, sv.DateTo, mId, merchantUserId);
                        using (StringWriter output = new StringWriter())
                        {
                            int count = 1;
                            output.WriteLine("\"Sr No\",\"Fleet Name\",\"Vehicle Number\",\"No Of Trans\",\"FareAmt\",\"Tips\",\"Surcharges\",\"TechFee\",\"No Of Chargebacks\",\"Charge back Value\",\"No Of Void\",\"Void Value\",\"Total Amount\",\"Fees\",\"AmountOwing\"");
                            foreach (GetMerchantSummaryReportDto item in merchantuSummaryDto)
                            {
                                output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\"",
                                    count, item.FleetName, item.VehicleNumber, item.NoOfTrans, item.FareAmt, item.Tips, item.Surcharges, item.TechFee, item.NoOfChargebacks, item.ChargebackValue, item.NoOfVoid, item.VoidValue, item.TotalAmount, item.Fees, item.AmountOwing);

                                count++;
                            }

                            return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "MerchantUser_SummaryReport.csv");
                        }
                        
                    case 21://Merchant Detail Report
                        IEnumerable<GetDetailReportDto> muDetailReport = _transactionService.GetMerchantUserDetailReport(sv.DateFrom, sv.DateTo, mId, merchantUserId);//
                        using (StringWriter output = new StringWriter())
                        {
                            output.WriteLine("\"Company\",\"Report Date\",\"Trans Id\",\"Trans Date\",\"Txn Type\",\"Card Type\",\"Last Four\",\"Auth Code\",\"Trip\",\"DriverNo\",\"VehicleNo\",\"TechFee\",\"Surcharge\",\"Gross Amt\",\"Fees\",\"NetAmt\"");
                            foreach (GetDetailReportDto item in muDetailReport)
                            {
                                output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\"",
                                    item.Company, item.ReportDate, item.TransId, item.TransDate, item.TxnType, item.CardType, item.LastFour, item.AuthCode, item.Trip, item.DriverNo, item.VehicleNo, item.TechFee, item.Surcharge, item.GrossAmt, item.Fees, item.NetAmt);
                            }

                            return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "MerchantUser_detailsReport.csv");
                        }
                        
                    case 22://Merchant User Exception Report
                        IEnumerable<GetMerchantExceptionReportDto> merchantUserException = _transactionService.GetMerchantUserExceptionReport(sv.DateFrom, sv.DateTo, sv.fk_MerchantId);
                        using (StringWriter output = new StringWriter())
                        {
                            output.WriteLine("\"Fleet Name\",\"Vehicle No\",\"Fare Value\",\"Tip\",\"Surcharge\",\"TechFee\",\"Amount\",\"Fee\",\"AmountOwing\"");
                            foreach (GetMerchantExceptionReportDto item in merchantUserException)
                            {
                                output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\"",
                                     item.FleetName, item.VehicleNo, item.FareValue, item.Tip, item.Surcharge, item.TechFee, item.Amount, item.Fee, item.AmountOwing);
                            }

                            return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "MerchantUser_ExceptionReport.csv");
                        }
                        
                    case 23://Merchant User Total Transactions By Vehicle Report
                        IEnumerable<GetMerchantTotalTransByVehicleReportDto> merchantuTotalTransactions = _transactionService.GetMerchantUserTotalTransByVehicleReport(sv.DateFrom, sv.DateTo, mId, merchantUserId);
                        using (StringWriter output = new StringWriter())
                        {
                            int counterd = 1;
                            output.WriteLine("\"Sr No\",\"FleetName\",\"No Of Trans\",\"FareAmt\",\"Tips\",\"Surcharges\",\"TechFee\",\"No Of Chargebacks\",\"Charge back Value\",\"No Of Void\",\"Void Value\",\"Total Amount\",\"Fees\",\"AmountOwing\",\"TransCountForSlab1\",\"TransCountForSlab2\",\"TransCountForSlab3\",\"TransCountForSlab4\"");

                            foreach (GetMerchantTotalTransByVehicleReportDto item in merchantuTotalTransactions)
                            {
                                output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\",\"{16}\",\"{17}\"",
                                    counterd, item.FleetName, item.NoOfTrans, item.FareAmt, item.Tips, item.Surcharges, item.TechFee, item.NoOfChargebacks, item.ChargebackValue, item.NoOfVoid, item.VoidValue, item.TotalAmount, item.Fees, item.AmountOwing, item.TransCountForSlab1, item.TransCountForSlab2, item.TransCountForSlab3, item.TransCountForSlab4);

                                counterd++;
                            }

                            return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "MerchantUser_TotalTransactionsByVehicleReport.csv");
                        }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<ReportListViewData> ReportList(int to, int from)
        {
            var reportList = _scheduleService.ReportList(to, from);
            var reportViewData = reportList.ToReportList().OrderBy(x => x.DisplayName);
            return reportViewData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [NonAction]
        private List<MerchantListViewData> MerchantUserList(int mId)
        {
            IEnumerable<UserDto> mvd = _reportcommonRepository.MerchantUserList(mId);
            List<MerchantListViewData> mld = mvd.Select(merchant => new MerchantListViewData
            {
                Company = merchant.Email,
                fk_MerchantID = merchant.UserID
            }).ToList();

            return mld;
        }

    }
}
