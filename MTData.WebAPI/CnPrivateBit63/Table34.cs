﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table34
    {
        #region Private Constants and Variables

        #region Constants
        private const string tblID = "|33|34";      
        #endregion

        #endregion

        #region Public Properties

        /// <summary>
        /// ID of Table
        /// default X'33 34' in Request
        /// </summary>
        public string TableID
        {
            get { return tblID; }
        }
        
        #endregion
    }
}