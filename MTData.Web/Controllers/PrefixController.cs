﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Web.Resource;
using MTData.Web.ViewData;
using MTData.Utility;

namespace MTData.Web.Controllers
{
    public class PrefixController : Controller
    {
        readonly ILogger _logger = new Logger();
        private readonly ICommonService _commonService;
        private readonly IPrefixService _prefixService;
        private StringBuilder _logMessage;
        private StringBuilder _logTracking;

        public PrefixController([Named("PrefixService")] IPrefixService prefixService, [Named("CommonService")] ICommonService commonService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _prefixService = prefixService;
            _commonService = commonService;
            _logMessage = logMessage;
            _logTracking = logTracking;
        }

        /// <summary>
        ///  Purpose            :to display the list the credit card prefix 
        /// Function Name       :Index
        /// Created By          : Asif Shafeeque
        /// Created On          : 12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1) return RedirectToAction("index", "Member");

            return View();
        }
        /// <summary>
        ///  Purpose            :to create the credit card prefix
        /// Function Name       :Create
        /// Created By          : Asif Shafeeque
        /// Created On          : 12/24/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1) return RedirectToAction("index", "Member");

            var viewData = new PrefixViewData { GatewayList = GatewayList() };

            return View(viewData);
        }

        /// <summary>
        ///  Purpose            :to retrieve the credit card prefix
        /// Function Name       :Create
        /// Created By          : Asif Shafeeque
        /// Created On          : 12/24/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="prefixViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(PrefixViewData prefixViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1) return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (ModelState.IsValid)
                {
                    _logMessage.Append("calling ToPrefixDto method to map PrefixDto to PrefixViewData and retrun PrefixDto");

                    var prefixDto = prefixViewData.ToPrefixDto();

                    _logMessage.Append("ToPrefixDto method executed successfully");

                    prefixDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                    prefixDto.fk_MerchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);

                    _logMessage.Append("Calling Add method of _prefixService class takes prefixDto, prefixDto properties Name= " + prefixViewData.Name + ", CCPrefix= " + prefixViewData.CCPrefix1 + ", CreatedBy= " + prefixViewData.CreatedBy);

                    bool exist = _prefixService.Add(prefixDto);
                    if (!exist)
                    {
                        ViewBag.Message = MtdataResource.Card_prefix_already_exist; //"This card prefix already in use";
                        prefixViewData.GatewayList = GatewayList();
                        return View(prefixViewData);
                    }

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;

                    _logTracking.Append("User " + userName + " create prefix for merchant" + prefixDto .fk_MerchantId+ " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                    _logMessage.Append("Add method of _prefixService class executed successfully");

                    return RedirectToAction("Index");
                }

                prefixViewData.GatewayList = GatewayList();
                return View(prefixViewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            :to update credit card  prefix
        /// Function Name       :Update
        /// Created By          : Asif Shafeeque
        /// Created On          : 12/25/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="prefixId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Update(int prefixId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1) return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                _logMessage.Append("Calling Add method of _prefixService class takes prefixId = " + prefixId);

                var prefixDto = _prefixService.GetPrefixDto(prefixId);

                _logMessage.Append("calling prefixDto method to map PrefixViewData to PrefixDto and retrun PrefixViewData");

                PrefixViewData prefix = prefixDto.ToPrefixViewData();
                _logMessage.Append("prefixDto method executed successfully");
                _logMessage.Append("Calling GatewayList method of same class");

                prefix.GatewayList = GatewayList();

                _logMessage.Append("GatewayList method Executed Successfully");

                return View(prefix);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            :to update the credit card prefix
        /// Function Name       :Update
        /// Created By          : Asif Shafeeque
        /// Created On          : 12/26/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="prefixViewData"></param>
        /// <param name="prefixId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(PrefixViewData prefixViewData, int prefixId)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (ModelState.IsValid)
                {
                    _logMessage.Append("calling ToPrefixDto method to map PrefixDto to PrefixViewData and retrun PrefixDto");

                    var prefixDto = prefixViewData.ToPrefixDto();

                    _logMessage.Append("ToPrefixDto method executed successfully");

                    prefixDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                    _logMessage.Append("Calling Update method of _prefixService class takes prefixDto, prefixDto properties Name= " + prefixViewData.Name + ", CCPrefix= " + prefixViewData.CCPrefix1 + ", CreatedBy= " + prefixViewData.ModifiedBy + ", PrefixId= " + prefixId);

                    _prefixService.Update(prefixDto, prefixId);

                    _logMessage.Append("Update method of _prefixService class executed successfully");

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + " update prefix Id " + prefixId + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return RedirectToAction("Index");
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            prefixViewData.GatewayList = GatewayList();

            return View(prefixViewData);
        }

        /// <summary>
        ///  Purpose            :to delete the prefix
        /// Function Name       :Delete
        /// Created By          : Asif Shafeeque
        /// Created On          : 12/26/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="prefixId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(int prefixId)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null && ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
            {
                try
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    _logMessage.Append("Calling DeletePrefix method of _prefixService class takes prefixid= " + prefixId);

                    _prefixService.DeletePrefix(prefixId);

                    _logMessage.Append("DeletePrefix method of _prefixService class executed successfully");

                    Thread.Sleep(50);

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + " delete prefix Id" + prefixId + "at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return Json(new { Result = "OK" });
                }
                catch (Exception exception)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), exception);
                }
            }

            return Json(new { Result = "No" });
        }

        /// <summary>
        ///  Purpose            :apply page indexing on delete
        /// Function Name       :GetPrefixListByFilter
        /// Created By          : Asif Shafeeque
        /// Created On          : 4/2/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult GetPrefixListByFilter(string name = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
         {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                  return Json(new { Result = "NO" });

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                int prefixDtoListCount;
                name = name.Trim();

                _logMessage.Append("Calling GetPrefixDtoList method of _prefixService to get gatwaylist");
                _logMessage.Append("GetPrefixDtoList method executed successfully");

                int merchantId  = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);

                var pfix = _prefixService.PrefixListByFilter(name, jtStartIndex, jtPageSize, jtSorting,merchantId);
                if (string.IsNullOrEmpty(name))
                {
                    _logMessage.Append("Calling GetPrefixDtoList method of _prefixService class to count Prefix");

                    prefixDtoListCount = _prefixService.GetPrefixDtoList(merchantId,name).ToList().Count;

                    _logMessage.Append("GetPrefixDtoList method of _prefixService executed successfully result = " + prefixDtoListCount);

                    return Json(new { Result = "OK", Records = pfix, TotalRecordCount = prefixDtoListCount });
                }

                _logMessage.Append("converting prefix list to array");

                var prefixDtos = pfix as PrefixDto[] ?? pfix.ToArray();

                _logMessage.Append("conversion of  prefix list to array successfully");

                prefixDtoListCount = _prefixService.GetPrefixDtoList(merchantId, name).ToList().Count;

                return Json(new { Result = "OK", Records = prefixDtos, TotalRecordCount = prefixDtoListCount });
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        ///  Purpose            :to get the gateway list
        /// Function Name       :GatewayList
        /// Created By          : Asif Shafeeque
        /// Created On          :14/1/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<GatewayListViewData> GatewayList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetGatewayList method of _prefixService to get gatwaylist");

                var gatewayDtoList = _commonService.GetGatewayList();

                _logMessage.Append("GetGatewayList method of _prefixService class executed successfully");
                _logMessage.Append("calling ToGatewayViewDataList method to map GatewayListDto to GatewayListViewData and retrun GatewayListDto");

                List<GatewayListViewData> lst = gatewayDtoList.ToGatewayViewDataList().ToList();

                _logMessage.Append("ToGatewayViewDataList method executed successfully");

                return lst;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return null;
        }

    }
}