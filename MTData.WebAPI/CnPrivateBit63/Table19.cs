﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table19
    {
        #region Private Constants and Variables
        #region Constants
        private const string tblId = "|00|19";
        #endregion

        #region Variables
        private string defaultDCCConversionIndicator = "2";//2 represents not Convertible.
        private string defaultDCCTransactionTimeZone = "|20|20|20";
        private string defaultForeignConversionRate = "|30|30|30|30|30|30|30|30|30|30|30|30|30";
        private string defaultAmountInMerchantBaseCurrency = "|30|30|30|30|30|30|30|30|30|30|30|30";
        #endregion
        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'36 30' Table 60
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        /// <summary>
        /// Gets or Sets the value for DCC Conversion Indicator
        /// </summary>
        public string DCCConversionIndicator
        {
            get { return defaultDCCConversionIndicator; }
            set { defaultDCCConversionIndicator = value; }
        }

        /// <summary>
        /// Gets or Sets the value for DCC Transaction Time Zone
        /// </summary>
        public string DCCTransactionTimeZone
        {
            get { return defaultDCCTransactionTimeZone; }
            set { defaultDCCTransactionTimeZone = value; }
        }

        /// <summary>
        /// Gets or Sets the value for Foreign Conversion Rate
        /// </summary>
        public string ForeignConversionRate
        {
            get { return defaultForeignConversionRate; }
            set { defaultForeignConversionRate = value; }
        }

        /// <summary>
        /// Gets or Sets the value for Amount in Merchant Base Currency
        /// </summary>
        public string AmountInMerchantBaseCurrency
        {
            get { return defaultAmountInMerchantBaseCurrency; }
            set { defaultAmountInMerchantBaseCurrency = value; }
        }
        #endregion
    }
}