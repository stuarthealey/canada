IF NOT EXISTS (SELECT 1 FROM Report WHERE ReportID IN (7))
	INSERT INTO dbo.Report VALUES('Total Transactions By Vehicle','Total Transactions By Vehicle', 0)
GO

IF NOT EXISTS (SELECT 1 FROM Report WHERE ReportID IN (8))
	INSERT INTO dbo.Report VALUES('Card Type Report','Card Type Report', 0)
GO