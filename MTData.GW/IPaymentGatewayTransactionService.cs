﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MTData.WCFGW
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPaymentGatewayTransactionService" in both code and config file together.
	//[ServiceContract(Namespace = "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayTransactionService")]
	[ServiceContract(Namespace = "http://tempuri.org/", Name = "IMTDataGateway")]
	public interface IPaymentGatewayTransactionService
	{
		[OperationContract]
		string BasicAuthorisation(string requestPacket);
	}
}
