﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CanadaCert
{
    public class MerchantInfo : ICloneable
    {
        public long Token { get; set; }
        public string FirstDataID { get; set; }
        public string MerchantID { get; set; }
        public string TerminalID { get; set; }
        public string DatawireID { get; set; }
        public string MerchantZipCode { get; set; }
        public bool IsSupprotExistingDebtIndicator { get; set; }
        public string MerchantCategoryCode { get; set; }

        #region Member function
        public object Clone()
        {
            return this.MemberwiseClone();
        }
        #endregion//end member function
        public string MCCCode { get; set; }
        public bool IsNewGlobalBinAvailable { get; set; }

        public int? MerchantIndustry { get; set; }
    }
}