﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;

using MTData.Utility;
using MTData.WebAPI.CanadaCommon;
using MTData.WebAPI.Resources;

namespace MTData.WebAPI.CanadaCert
{
    public static class FormatFields
    {
        /// <summary>
        /// To apply formatting on various fields as per the iso8583 format
        /// </summary>
        /// <param name="entityProperty"></param>
        /// <param name="entityValue"></param>
        /// <param name="isTransArmor"></param>
        /// <returns>result</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static object ApplyISOFormatting(Element entityProperty, object entityValue, bool isTransArmor)
        {
            var result = entityValue;

            switch (entityProperty.ElementID)
            {
                case 2:     //format the PAN data as per the format specified in ISO8583 specs
                    result = FormatPAN(entityProperty, Convert.ToString(entityValue));
                    break;

                case 4:     //format the transaction amount as per the format specified in ISO8583 specs
                    result = FormatAmount(Convert.ToDouble(entityValue));
                    break;

                case 35:
                    result = FormatTrack2Data(entityProperty, Convert.ToString(entityValue));
                    break;

                case 41:    //format the Terminal ID as per the format specified in ISO8583 specs
                    result = Convert.ToString(entityValue).PadLeft(8, '0');
                    break;

                case 42:    //format the Merchant ID as per the format specified in ISO8583 specs
                    result = Convert.ToString(entityValue).PadLeft(15, '0');
                    break;

                case 44:
                    break;

                case 52:
                    break;

                case 54:    //format the Additional amount as per the format specified in ISO8583 specs
                    result = FormatAmountWithLengthIndicator(Convert.ToString(entityValue));
                    break;

                case 48:
                    break;

                case 55:    //format the ICC data as per the format specified in ISO8583 specs
                    result = FormatIccData(entityProperty, Convert.ToString(entityValue));
                    break;

                case 59:
                    break;

                default:    //format the numeric fields  as per the format specified in ISO8583 specs
                    if (entityProperty.Type == DataType.Numeric)
                        result = FormatNumeric(entityProperty, Convert.ToString(entityValue));

                    if (entityProperty.Type == DataType.Alphanumeric)
                        result = FormatAlphaNumeric(entityProperty, Convert.ToString(entityValue));
                    break;
            }

            return result;
        }
       
        /// <summary>
        /// Prepare processsing code as per ISO8583.
        /// </summary>
        /// <param name="cardType"></param>
        /// <param name="transType"></param>
        /// <returns>processingCode</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string PrepareProcessingCode(string paymentType,string transType,string entryMode)
        {            
            try
            {
               string processingCode;

               if (String.Equals(paymentType, CardType.Credit.ToString()) && (String.Equals(transType, TransactionType.Sale.ToString()) || String.Equals(transType, TransactionType.Authorization.ToString()) || String.Equals(transType, TransactionType.Completion.ToString()) || String.Equals(transType, TransactionType.Void.ToString())))
                   processingCode = SigmaStaticValue.ProcessingCodeForCreditSale;
               else if (String.Equals(paymentType, CardType.Credit.ToString()) && String.Equals(transType, TransactionType.Refund.ToString()))
                   processingCode = SigmaStaticValue.ProcessingCodeForCreditRefund;
               else if (String.Equals(paymentType, CardType.Debit.ToString()) && String.Equals(entryMode, EntryMode.Swiped.ToString()) && (String.Equals(transType, TransactionType.Sale.ToString()) || String.Equals(transType, TransactionType.Authorization.ToString()) || String.Equals(transType, TransactionType.Completion.ToString())))
                   processingCode = SigmaStaticValue.ProcessingCodeForPinDebitSale;
               else if (String.Equals(paymentType, CardType.Debit.ToString()) && String.Equals(entryMode, EntryMode.Keyed.ToString()) && (String.Equals(transType, TransactionType.Sale.ToString()) || String.Equals(transType, TransactionType.Authorization.ToString()) || String.Equals(transType, TransactionType.Completion.ToString())))
                   processingCode = SigmaStaticValue.ProcessingCodeForPinlessDebitSale;
               else if (String.Equals(paymentType, CardType.Debit.ToString()) && String.Equals(transType, TransactionType.Void.ToString()))
                   processingCode = SigmaStaticValue.ProcessingCodeForDebitReversal;
               else if (String.Equals(paymentType, CardType.Debit.ToString()) && String.Equals(transType, TransactionType.Refund.ToString()))
                   processingCode = SigmaStaticValue.ProcessingCodeForDebitRefund;
               else
                   processingCode = String.Empty;

               return processingCode;               
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Prepare bitmap as per ISO8583.
        /// </summary>
        /// <param name="objElements"></param>
        /// <returns>hexBitmapString</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string PrepareBitmap(Dictionary<Element, string> objElements)
        {
            string hexBitmapString = string.Empty;
            try
            {
                byte[] bitmap;

                //checks the max value in dictionary to set the length of byte array
                if (objElements.Max(x => x.Key.ElementID) > 63)
                {
                    bitmap = new byte[128];
                    bitmap[0] = 1;  //set bit to indicate presence of secondary bitmap
                }
                else
                {
                    bitmap = new byte[64];
                }

                //sets the bitmap if the value is set in dictionary
                foreach (var key in objElements.Keys)
                {
                    if (!string.IsNullOrEmpty(objElements[key]))
                        bitmap[key.ElementID - 1] = 1;
                }

                //generate the primary bitmap on the basis of values available in the dictionary
                var primaryBitmap = bitmap.Select(p => p.ToString()).Aggregate((x, y) => (x + y));
                for (int ind = 0; ind < primaryBitmap.Length; ind += 4)
                {
                    hexBitmapString += Convert.ToString(Convert.ToInt32(primaryBitmap.Substring(ind, 4), 2), 16).ToUpper(CultureInfo.CurrentCulture);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return hexBitmapString;
        }

        /// <summary>
        /// Format message type as per ISO8583.
        /// </summary>
        /// <param name="tranType"></param>
        /// <param name="paymentType"></param>
        /// <param name="IsTORTransaction"></param>
        /// <returns>messageType</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string FormatMessageType(string tranType, string paymentType, bool IsTORTransaction)
        { 
            string messageType;

            if (IsTORTransaction)
            {
                messageType = SigmaStaticValue.MessageType_ZeroFourZeroZero;
            }
            else
            {
                if (paymentType == CardType.Credit.ToString() && (tranType == TransactionType.Sale.ToString() || tranType == TransactionType.Authorization.ToString()) || tranType == TransactionType.Completion.ToString() || tranType == TransactionType.Refund.ToString())
                    messageType = SigmaStaticValue.MessageType_ZeroOneZeroZero;
                else if (paymentType == CardType.Debit.ToString() && (tranType == TransactionType.Sale.ToString() || tranType == TransactionType.Authorization.ToString() || tranType == TransactionType.Completion.ToString() || tranType == TransactionType.Refund.ToString()))
                    messageType = SigmaStaticValue.MessageType_ZeroTwoZeroZero;
                else if (tranType == TransactionType.Void.ToString())
                    messageType = SigmaStaticValue.MessageType_ZeroFourZeroZero;
                else
                    messageType = String.Empty;
            }
            
            return messageType;
        }

        /// <summary>
        /// Format trasmission date time
        /// </summary>
        /// <param name="transDateTime"></param>
        /// <returns></returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string FormatTransDateTime(string transDateTime)
        {
            if (transDateTime.Length < 14)
                return transDateTime;
            else
                return transDateTime.Substring(4, 10);
        }

        /// <summary>
        /// Format local time
        /// </summary>
        /// <param name="transDateTime"></param>
        /// <returns></returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string FormatLocalTime(string transDateTime)
        {
            if (transDateTime.Length < 14)
                return transDateTime;
            else
                return transDateTime.Substring(8, 6);
        }

        /// <summary>
        /// Format local date
        /// </summary>
        /// <param name="transDateTime"></param>
        /// <returns></returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string FormatLocalDate(string transDateTime)
        {
            if (transDateTime.Length < 14)
                return transDateTime;
            else
                return transDateTime.Substring(4, 4);
        }

        /// <summary>
        /// Format expiry date
        /// </summary>
        /// <param name="expiryDate"></param>
        /// <returns></returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string FormatExpiryDate(string expiryDate, string transType, string industryType)
        {
            string expiry = string.Empty;
            if ((industryType == IndustryType.Retail.ToString() && transType == TransactionType.Completion.ToString()) 
                || industryType == IndustryType.Ecommerce.ToString()
                || industryType == IndustryType.BookerAppPayment.ToString()     // Set Expiry Date for Token payments.
                )
            {
                if (expiryDate.Length < 6)
                    expiry = expiryDate;
                else
                    expiry = expiryDate.Substring(2, 4);
            }

            return expiry;
        }
        
        /// <summary>
        /// Prepare reference number
        /// </summary>
        /// <param name="terminalId"></param>
        /// <param name="stan"></param>
        /// <returns></returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string FormatReferenceNo(string terminalId, string stan, string paymentType, string refNumber)
        {
            string result = string.Empty;

            if (paymentType == PymtTypeType.Debit.ToString())
            {
                if (terminalId.Length < 8)
                    terminalId = terminalId.PadLeft(8, '0');

                result = String.Concat(SigmaStaticValue.FourZeroes, terminalId.Substring(6, 2), stan);
            }
            else
            {
                int referenceNumber = 1;

                if (Convert.ToInt32(refNumber) == Int32.MaxValue)
                    referenceNumber = 1;
                else
                    referenceNumber = Convert.ToInt32(refNumber) + 1;

                result = referenceNumber.ToString().PadLeft(12, '0');
            }

            return result;
        }

        /// <summary>
        /// Format track1 data
        /// </summary>
        /// <param name="track1Data"></param>
        /// <returns></returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string FormatTrack1Data(string track1Data)
        {
            track1Data = String.Concat("|", track1Data.Length, track1Data);
            return track1Data;
        }
 
        /// <summary>
        /// Format the numeric data as per ISO8583.
        /// Numeric data elements should be right justified and padded to left with zeros.
        /// </summary>
        /// <param name="entityProperty"></param>
        /// <param name="data"></param>
        /// <returns>string</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        private static string FormatNumeric(Element entityProperty, string data)
        {
            //if data length is odd then set the right-most half byte to X ‘0’.
            var length = data.Length;
            if ((length) % 2 > 0)
            {
                if (entityProperty.LengthType == LengthType.Variable)
                    data = data + Convert.ToString(SigmaStaticValue.Zero);

                if (entityProperty.LengthType == LengthType.Fixed)
                    data = Convert.ToString(SigmaStaticValue.Zero) + data;
            }

            data = data.PadLeft(entityProperty.LengthMax, SigmaStaticValue.Zero);

            string formattedNumeric = entityProperty.LentghIndicator == 1 ? FormatLengthIndicator(entityProperty, length.ToString()) + data : data;
            return formattedNumeric;
        }

        /// <summary>
        /// Format the alphanumeric data as per ISO8583.
        /// Alphanumeric data elements should be left-justified and padded to right with spaces.
        /// </summary>
        /// <param name="entityProperty"></param>
        /// <param name="data"></param>
        /// <returns>string</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string FormatAlphaNumeric(Element entityProperty, string data)
        {
            var length = FormatLengthIndicator(entityProperty, data.Length.ToString());

            //left justified and pad right with spaces.
            data = data.PadRight(entityProperty.LengthMax, ' ');
            data = data.Replace(SigmaStaticValue.SingleSpace, SigmaStaticValue.PI20);

            return entityProperty.LentghIndicator == 1 ? length + data : data;
        }

        /// <summary>
        /// Format the amount as per ISO8583.
        /// Amount should be right justified, zero filled(in left), and two assumed decimal places for US and most other countries.
        /// </summary>
        /// <param name="value"></param>
        /// <returns>string</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string FormatAmount(double value)
        {
            var sep = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToCharArray();

            //Split the string on the separator.
            var segments = value.ToString().Split(sep);
            string valueWithoutDecimal = string.Empty;
            switch (segments.Length)
            {
                case 1:     //Only one segment, so there is not fractional value.
                    break;
                case 2:     //Two segments, so round to two decimal value.
                    value = Math.Round(value, 2, MidpointRounding.AwayFromZero);
                    break;
                default:    //More than two segments means it's invalid, so throw an exception.
                    throw new InvalidOperationException("Something bad happened!");
            }

            valueWithoutDecimal = string.Format("{0:0.00}", value).Replace(SigmaStaticValue.Dot, string.Empty);

            return valueWithoutDecimal.PadLeft(12, SigmaStaticValue.Zero);
        }

        /// <summary>
        /// Format track data as per ISO8583.
        /// </summary>
        /// <param name="entityProperty"></param>
        /// <param name="data"></param>
        /// <returns>object</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        /// 
        public static string FormatTrack2Data(Element entityProperty, string trackData)
        {
            // Make the Track Data length even    
            if (trackData.Length % 2 > 0)
                trackData = trackData.PadRight(trackData.Length + 1, (char)SigmaStaticValue.Zero);

            // FormatLengthIndicator() method only add length with | for example if length is “7” then “|07” one byte data
            trackData = FormatLengthIndicator(entityProperty, trackData.Length.ToString()) + trackData;
            trackData = trackData.Replace(SigmaStaticValue.SplitEquals, 'D');

            return trackData;
        }

        /// <summary>
        /// Format Primary Account Number(PAN) as per ISO8583.
        /// </summary>
        /// <param name="entityProperty"></param>
        /// <param name="data"></param>
        /// <returns>strResult</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string FormatPAN(Element entityProperty, string data)
        {
            string strResult = data;

            //make the primary account number length even         
            var length = data.Length;
            if ((length % 2) > 0)
                strResult = strResult.PadRight(length + 1, SigmaStaticValue.Zero);

            //adding length to the PAN string
            strResult = FormatLengthIndicator(entityProperty, length.ToString()) + strResult;
            return strResult;
        }

       /// <summary>
        /// Gets the length of additional infornmation tables data.
        /// </summary>
        /// <param name="data"></param>
        /// <returns>string totalLength</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string SetAdditionalTableLength(string data)
        {
            string totalLength = string.Empty;
            int seperatorCount = data.Count(m => m == SigmaStaticValue.Pipe);
            int dataLength = data.Replace(Convert.ToString(SigmaStaticValue.Pipe), string.Empty).Length;

            totalLength = Convert.ToString(SigmaStaticValue.Pipe) + Convert.ToString(dataLength - seperatorCount).PadLeft(4, '0').Insert(2, Convert.ToString(SigmaStaticValue.Pipe));

            return totalLength;
        }

        /// <summary>
        /// Apply formatting of amount with the length indicator for the elements as per ISO8583.
        /// </summary>
        /// <param name="data"></param>
        /// <returns>string</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string FormatAmountWithLengthIndicator(string data)
        {
            //Format the data amount on the basis of currency code
            var amount = FormatAmount(Convert.ToDouble(data));

            //get the length indicator for the data amount.
            return SetAdditionalTableLength(amount);    //Get the length of data with | for example if length is 21 then |21.
        }

        /// <summary>
        /// Format length indicator as per ISO8583.
        /// </summary>
        /// <param name="entityProperty"></param>
        /// <param name="data"></param>
        /// <returns>string data</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        private static string FormatLengthIndicator(Element entityProperty, string data)
        {
            var length = data.Length;
            if ((length % 2) > 0)
                data = data.PadLeft(length + 1, SigmaStaticValue.Zero);

            //Chetu 357
            // Prepare bit 55 data length 3 byte as per ISO format 
            if (entityProperty.ElementID.Equals(55) && data.Length < 3)
                data = data.PadLeft(data.Length + 2, SigmaStaticValue.Zero);

            // If ascii format element has lenght indicator then apply datawire format explicitly
            if (entityProperty.Format == FormatType.ASCII)
                data = CnTransactionMessage.ApplyDatawireFormat(data);

            return data;
        }

        /// <summary>
        /// To prepare acquirer reference data
        /// </summary>
        /// <param name="transType"></param>
        /// <returns>acquireRef</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string PrepareAcquirerRef(string transType, string sourceTransaction)
        {
            string acquireRef = String.Empty;

            if (transType == TransactionType.Sale.ToString())
                acquireRef = SigmaStaticValue.AcquirerRefDataForAuthCapture;
            else if (transType == TransactionType.Void.ToString() && sourceTransaction == TransactionType.Completion.ToString())
                acquireRef = SigmaStaticValue.AcquirerRefDataForCaptureOnly;
            else if (transType == TransactionType.Void.ToString() && sourceTransaction == TransactionType.Authorization.ToString())
                acquireRef = SigmaStaticValue.AcquirerRefDataForAuthOnly;
            else if (transType == TransactionType.Void.ToString())
                acquireRef = SigmaStaticValue.AcquirerRefDataForAuthCapture;
            else if (transType == TransactionType.Authorization.ToString())
                acquireRef = SigmaStaticValue.AcquirerRefDataForAuthOnly;
            else if (transType == TransactionType.Completion.ToString() || transType == TransactionType.Refund.ToString())
                acquireRef = SigmaStaticValue.AcquirerRefDataForCaptureOnly;
            else
                acquireRef = String.Empty;

            return acquireRef;
        }

         /// <summary>
        /// To prepare acquirer reference data
        /// </summary>
        /// <param name="transType"></param>
        /// <returns>acquireRef</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static bool IsPinLessDebit(string encryptedPin, string paymentType)
        {
            if (String.IsNullOrEmpty(encryptedPin) && paymentType == CardType.Debit.ToString())
                return true;
            else
                return false;
        }

        /// <summary>
        /// To prepare pos condition code
        /// </summary>
        /// <param name="paymentType"></param>
        /// <param name="cardType"></param>
        /// <param name="paymentType"></param>
        /// <returns>bool</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string PreparePosConditionCode(string paymentType, string cardType, string encryptedPinData)
        {
            if (paymentType == PymtTypeType.Debit.ToString())
            {
                return SigmaStaticValue.PosConditionCodeZeroSeven;
            }
            else
            {
                if (cardType == CardTypeType.Visa.ToString() || cardType == CardTypeType.MasterCard.ToString() || cardType == CardTypeType.Amex.ToString() || (paymentType == CardType.Debit.ToString() && IsPinLessDebit(encryptedPinData, paymentType)))
                    return SigmaStaticValue.PosConditionCodeFiveNine;
                else if (cardType == CardTypeType.Discover.ToString() || cardType == CardTypeType.Diners.ToString() || cardType == CardTypeType.JCB.ToString())
                    return SigmaStaticValue.PosConditionCodeZeroEight;
                else
                    return String.Empty;
            }
        }

        /// <summary>
        /// To prepare acquirer reference data
        /// </summary>
        /// <param name="transType"></param>
        /// <returns>acquireRef</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string PrepareAuthId(string txnType, string authId)
        {
            string authIdResponse = string.Empty;

            if (txnType == TransactionType.Completion.ToString() || txnType == TransactionType.Void.ToString())
                authIdResponse = authId;
            else
                authIdResponse = string.Empty;

            return authIdResponse;
        }

        /// <summary>
        /// To prepare acquirer reference data
        /// </summary>
        /// <param name="transType"></param>
        /// <returns>acquireRef</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static Bit63Fields ParseBit63Resoponse(this string bit63Resonse, string cardType, string industryType)
        {
            string tableDSPrefix = string.Empty;
            int table14PrefixLength;
            int tableDSResponseLength;
            CardTypeType cardTypeType = (CardTypeType)Enum.Parse(typeof(CardTypeType), cardType);
            string inputResponse = String.Empty;
            Bit63Fields bit63Fields = new Bit63Fields();
            int startIndex = bit63Resonse.IndexOf(PaymentAPIResources.StartTag);
            int lastIndex = bit63Resonse.IndexOf(PaymentAPIResources.EndTag);
            int diffIndex = lastIndex - (startIndex + 15);

            if (startIndex > 0 && lastIndex > 0 && diffIndex > 0)
                inputResponse = bit63Resonse.Substring(startIndex + 15, diffIndex);

            if (industryType == IndustryType.Ecommerce.ToString())
            {
                tableDSPrefix = PaymentAPIResources.TableDSPrefix;
                tableDSResponseLength = 114;
                table14PrefixLength = 68;
            }
            else
            {
                tableDSPrefix = PaymentAPIResources.TableDSPrefixRetail;
                tableDSResponseLength = 108;
                table14PrefixLength = 66;
            }

            int table14Index = inputResponse.IndexOf(PaymentAPIResources.Table14Prefix);
            if (table14Index > 0)
            {
                switch (cardTypeType)
                {
                    case CardTypeType.Visa:
                        bit63Fields.Table14Response = inputResponse.Substring(table14Index, 56);
                        int tableVIIndex = inputResponse.IndexOf(PaymentAPIResources.TableVIPrefix);

                        //zzz Chetu 357
                        if (tableVIIndex < 0)
                            tableVIIndex = inputResponse.IndexOf(PaymentAPIResources.TableVIPrefixRetail); //for retail

                        if (tableVIIndex > 0)
                            bit63Fields.TableVIResponse = inputResponse.Substring(tableVIIndex, 14).Replace(PaymentAPIResources.TableVIResponseLength, PaymentAPIResources.TableVIReplacedLength);
                        break;

                    case CardTypeType.MasterCard:
                        bit63Fields.Table14Response = inputResponse.Substring(table14Index, table14PrefixLength);
                        int tableMCIndex = inputResponse.IndexOf(PaymentAPIResources.TableMCPrefix);
                        if (tableMCIndex > 0)
                            bit63Fields.TableMCResponse = inputResponse.Substring(tableMCIndex, 75);
                        break;

                    case CardTypeType.Amex:
                        bit63Fields.Table14Response = inputResponse.Substring(table14Index, 66);
                        bit63Fields.TableVIResponse = String.Empty;
                        bit63Fields.TableMCResponse = String.Empty;
                        bit63Fields.TableDSResponse = String.Empty;
                        break;

                    case CardTypeType.JCB:
                        bit63Fields.Table14Response = inputResponse.Substring(table14Index, 94);
                        bit63Fields.TableVIResponse = String.Empty;
                        bit63Fields.TableMCResponse = String.Empty;
                        bit63Fields.TableDSResponse = String.Empty;
                        break;

                    case CardTypeType.Discover:
                    case CardTypeType.Diners:
                        bit63Fields.Table14Response = inputResponse.Substring(table14Index, 64);
                        int tableDSIndex = inputResponse.IndexOf(tableDSPrefix);
                        if (tableDSIndex > 0)
                            bit63Fields.TableDSResponse = inputResponse.Substring(tableDSIndex, tableDSResponseLength); //114 in case of ecommerce and 108 in case of Retail
                        break;
                }
            }

            int table49Index = inputResponse.IndexOf(PaymentAPIResources.Table49Prefix);
            if (table49Index > 0)
                bit63Fields.Table49Response = inputResponse.Substring(table49Index, 9);

            if (cardType == CardTypeType.Amex.ToString())
            {
                int tableSPAmexIndex = inputResponse.IndexOf(PaymentAPIResources.TableSPAmexPrefix);
                if (tableSPAmexIndex > 0)
                {
                    bit63Fields.TableSPResponse = CnGenericHandler.ConvertToHex(inputResponse.Substring(tableSPAmexIndex + 5, 15));
                    bit63Fields.TableSPResponse = CnTransactionMessage.ApplyDatawireFormat(bit63Fields.TableSPResponse);
                }
            }
            else if (cardType == CardTypeType.Diners.ToString())
            {
                int tableSPIndex = inputResponse.IndexOf(PaymentAPIResources.TableSPDinersPrefix);
                if (tableSPIndex > 0)
                {
                    bit63Fields.TableSPResponse = CnGenericHandler.ConvertToHex(inputResponse.Substring(tableSPIndex + 5, 14));
                    bit63Fields.TableSPResponse = CnTransactionMessage.ApplyDatawireFormat(bit63Fields.TableSPResponse);
                }
            }
            else
            {
                int tableSPIndex = inputResponse.IndexOf(PaymentAPIResources.TableSPOthers);
                if (tableSPIndex > 0)
                {
                    bit63Fields.TableSPResponse = CnGenericHandler.ConvertToHex(inputResponse.Substring(tableSPIndex + 5, 16));
                    bit63Fields.TableSPResponse = CnTransactionMessage.ApplyDatawireFormat(bit63Fields.TableSPResponse);
                }
            }

            return bit63Fields;
        }

        /// <summary>
        /// To prepare sccount number
        /// </summary>
        /// <param name="transType"></param>
        /// <returns>string</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string PrepareAccountNumber(string transactionType, string cardNumber, bool emvRefundVoidReq)
        {
            if (!emvRefundVoidReq)
            {
                if ((transactionType == TransactionType.Completion.ToString()) || (transactionType == TransactionType.Void.ToString()) || (transactionType == TransactionType.Refund.ToString()))
                    cardNumber = String.Empty;
            }
            else
            {
                if (!string.IsNullOrEmpty(cardNumber))
                {
                    if (cardNumber.Length > 19)
                        cardNumber = cardNumber.Substring(0, 19);
                }
            }

            return cardNumber;
        }

        /// <summary>
        /// To prepare AVS
        /// </summary>
        /// <param name="transType"></param>
        /// <returns>string</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string PrepareAVS(string transactionType, string crdHldrZipCode, string crdHldrStreetAddress)
        {
            if ((transactionType == TransactionType.Sale.ToString()) || (transactionType == TransactionType.Authorization.ToString()))
                return FormatAvs(crdHldrZipCode, crdHldrStreetAddress);
            else
                return string.Empty;
        }

        /// <summary>
        /// To prepare AddRespdata
        /// </summary>
        /// <param name="addRespData"></param>
        /// <returns>string</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string PrepareAddRespData(string addRespData)
        {
            return CnTransactionMessage.ApplyDatawireFormat(PaymentAPIResources.AddRespDataLengthInd + CnGenericHandler.ConvertToHex(addRespData));
        }

        /// <summary>
        /// Purpose             :   Format avs as per iso8583 format
        /// Function Name       :   FormatAvs
        /// Created By          :   Naveen Kumar
        /// Created On          :   14/11/2016
        /// Modification Made   :   ****************************
        /// Modified On       :    "MM/DD/YYYY" 
        /// </summary>
        /// <param name=zipCode, streetAddress></param>
        /// <returns>string</returns>  
        public static string FormatAvs(string zipCode, string streetAddress)
        {
            if (!string.IsNullOrEmpty(zipCode))
            {
                zipCode = zipCode.Replace(SigmaStaticValue.SingleSpace, String.Empty);

                if (zipCode.Length > 9)
                    zipCode = zipCode.Substring(0, 9);

                zipCode = zipCode.PadRight(9, Convert.ToChar(SigmaStaticValue.SingleSpace));
            }
            else
            {
                zipCode = SigmaStaticValue.NineSpaces;
            }

            if (!string.IsNullOrEmpty(streetAddress))
            {
                streetAddress = streetAddress.Replace(SigmaStaticValue.SingleSpace, String.Empty);

                if (streetAddress.Length > 20)
                    streetAddress = streetAddress.Substring(0, 20);

                streetAddress = streetAddress.PadRight(20, Convert.ToChar(SigmaStaticValue.SingleSpace));
            }
            else
            {
                streetAddress = SigmaStaticValue.TwentySpaces;
            }

            return PaymentAPIResources.AvsLengthWithTranCode + zipCode + streetAddress;
        }

        /// <summary>
        /// Purpose             :   Format zip code as per iso8583 format
        /// Function Name       :   FormatMerchantZipCode
        /// Created By          :   Naveen Kumar
        /// Created On          :   14/11/2016
        /// Modification Made   :   ****************************
        /// Modified On       :    "MM/DD/YYYY" 
        /// </summary>
        /// <param name=merchantZipCode,cardType></param>
        /// <returns>string</returns>  
        public static string FormatMerchantZipCode(string merchantZipCode, string cardType)
        {
            string formattedMerchantZip = string.Empty;

            if (!string.IsNullOrEmpty(merchantZipCode))
            {
                merchantZipCode = merchantZipCode.Replace(SigmaStaticValue.SingleSpace, String.Empty);
                formattedMerchantZip = PaymentAPIResources.MerchantZipCodeLength + merchantZipCode.PadRight(9, Convert.ToChar(SigmaStaticValue.SingleSpace));
            }
            else
            {
                formattedMerchantZip = PaymentAPIResources.MerchantZipCodeLength + SigmaStaticValue.NineSpaces;
            }

            if (!string.IsNullOrEmpty(merchantZipCode))
            {
                if (cardType == CardTypeType.MasterCard.ToString() || cardType == CardTypeType.Visa.ToString())
                {
                    if (merchantZipCode.Length >= 6)
                    {
                        string firstThreeChars = merchantZipCode.Substring(0, 3);
                        string lastThreeChars = merchantZipCode.Substring(3, 3);
                        string finalzipChars = firstThreeChars + SigmaStaticValue.SingleSpace + lastThreeChars;
                        formattedMerchantZip = PaymentAPIResources.MerchantZipCodeLength + finalzipChars.PadRight(9, Convert.ToChar(SigmaStaticValue.SingleSpace));
                    }
                }
            }

            return formattedMerchantZip;
        }

        /// <summary>
        /// Purpose             :   Format cvv 
        /// Function Name       :   FormatCvvCode
        /// Created By          :   Naveen Kumar
        /// Created On          :   22/12/2016
        /// Modification Made   :   ****************************
        /// Modified On       :    "MM/DD/YYYY" 
        /// </summary>
        /// <param name=merchantZipCode,cardType></param>
        /// <returns>string</returns>  
        public static string FormatCvvCode(string cvv)
        {
            if (!string.IsNullOrEmpty(cvv))
            {
                if (cvv.Length > 4)
                    cvv = cvv.Substring(0, 4);
            }

            return cvv;
        }

        /// <summary>
        /// To get pos entry mode+pin capability based on the entry mode
        /// </summary>
        /// <param name="entryMode"></param>
        /// <returns></returns>
        public static string FormatPosEntryPinCapability(string entryMode)
        {
            return (entryMode == Convert.ToString(EntryMode.Swiped) ? SigmaStaticValue.POSEntryModePinCapabilityForSwipedWithPin : SigmaStaticValue.POSEntryModePinCapabilityForMSRContactless);
        }

        /// <summary>
        /// To format encrypted pin data
        /// </summary>
        /// <param name="paymentType"></param>
        /// <param name="cardType"></param>
        /// <returns></returns>
        public static string FormatEncryptedPinData(string paymentType, string cardType, string pinData)
        {
            ILogger logger = new Logger();

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                logger.LogInfoMessage(string.Format("FormatFields.FormatEncryptedPinData() paymentType:{0};cardType:{1};pinData:{2};", paymentType, cardType, pinData));

            string encryptedPinData = string.Empty;

            if (paymentType == Convert.ToString(CardType.Debit))
            {
                if (cardType == Convert.ToString(CardIssuer.Interac))
                    encryptedPinData = CnTransactionMessage.ApplyDatawireFormat(SigmaStaticValue.EncryptedPinDataForInterac);
                else
                    encryptedPinData = CnTransactionMessage.ApplyDatawireFormat(CnGenericHandler.ConvertToHex(pinData));
            }

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                logger.LogInfoMessage(string.Format("FormatFields.FormatEncryptedPinData() paymentType:{0};cardType:{1};pinData:{2};encryptedPinData:{3};", paymentType, cardType, pinData, encryptedPinData));

            return encryptedPinData;
        }

        /// <summary>
        /// To get additional pos data for MSR contact and contactless
        /// </summary>
        /// <param name="entryMode"></param>
        /// <returns></returns>
        public static string FormatAdditionalPosDataForRetail(string entryMode)
        {
            string additionalPosData = string.Empty;

            if (entryMode == Convert.ToString(EntryMode.Swiped) || entryMode == Convert.ToString(EntryMode.FSwiped))
                additionalPosData = SigmaStaticValue.AdditionalPossDataForRetailContact;
            else
                additionalPosData = SigmaStaticValue.AdditionalPossDataForMSRContactless;
            
            return additionalPosData;
        }

        /// <summary>
        /// To calculate and format track 3 data length
        /// </summary>
        /// <param name="track3Data"></param>
        /// <returns></returns>
        public static string FormatTrack3DataLength(string track3Data)
        {
            string track3DataLength = string.Empty;

            if (track3Data.Length > 0)
            {
                track3DataLength = Convert.ToString(track3Data.Length).PadLeft(3, SigmaStaticValue.Zero);
                track3DataLength = CnTransactionMessage.ApplyDatawireFormat(CnGenericHandler.ConvertToHex(track3DataLength));
            }
            
            return track3DataLength;
        }

        /// <summary>
        /// To format base derivation key id 
        /// </summary>
        /// <param name="keySerialNumber"></param>
        /// <returns></returns>
        public static string FormatBaseDerivationkeyId(string keySerialNumber)
        {
            string baseDerivationKeyId = string.Empty;

            if (!string.IsNullOrEmpty(keySerialNumber))
                baseDerivationKeyId = keySerialNumber.Remove(keySerialNumber.Length - 10).Substring(1).PadLeft(9, SigmaStaticValue.OneF);
            
            return baseDerivationKeyId;
        }

        /// <summary>
        /// To format Device Id
        /// </summary>
        /// <param name="keySerialNumber"></param>
        /// <returns></returns>
        public static string FormatDeviceId(string keySerialNumber)
        {
            string deviceId = string.Empty;

            if (!string.IsNullOrEmpty(keySerialNumber))
                deviceId = keySerialNumber.Substring(keySerialNumber.Length - 10, 5);

            return deviceId;
        }

        /// <summary>
        /// To format transcation counter
        /// </summary>
        /// <param name="keySerialNumber"></param>
        /// <returns></returns>
        public static string FormatTransactionCounter(string keySerialNumber)
        {
            string transactionCounter = string.Empty;

            if (!string.IsNullOrEmpty(keySerialNumber))
                transactionCounter = keySerialNumber.Substring(keySerialNumber.Length - 5);

            return transactionCounter;
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Implements the ISO 8583 format for the ICCData
        /// </summary>
        /// <param name="entityProperty"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string FormatIccData(Element entityProperty, string data)
        {
            string strResult = data.ToUpper();

            //make the primary account number length even         
            var length = data.Length;
            if ((length % 2) > 0)
                strResult = Convert.ToString(SigmaStaticValue.Zero) + strResult;

            //adding length to the ICCData string   
            strResult = FormatLengthIndicator(entityProperty, (strResult.Length / 2).ToString()) + strResult;

            return strResult;
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Format the track2 data
        /// </summary>
        /// <param name="tranType"></param>
        /// <param name="paymentType"></param>
        /// <param name="IsTORTransaction"></param>
        /// <returns></returns>
        public static string FormatTrack2Data(string track2Data, string transType, bool isTORTransaction)
        {
            if (transType != TxnTypeType.Completion.ToString())
            {
                if (!isTORTransaction)
                {
                    if (track2Data.StartsWith(SigmaStaticValue.SemiColon.ToString()))
                        track2Data = track2Data.Substring(1);

                    if (track2Data.EndsWith(SigmaStaticValue.QuestionMark.ToString()))
                        track2Data = track2Data.Substring(0, track2Data.Length - 1);
                }
            }

            return track2Data;
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// To get processing code for interac transaction
        /// </summary>
        /// <param name="accountType"></param>
        /// <returns></returns>
        public static string GetInteracProcessingCode(string transactionType, string accountType)
        {
            string processingCode = string.Empty;

            if (accountType == AccountType.Saving.ToString())
                processingCode = SigmaStaticValue.SaleSavingProcessingCode;
            else if (accountType == AccountType.Checking.ToString())
                processingCode = SigmaStaticValue.SaleCheckingProcessingCode;

            return processingCode;
        }

        //zzz Chetu 357
        /// <summary>
        /// To get formatted device type indicator value for 9F6E
        /// </summary>
        /// <param name="deviceTypeIndicator"></param>
        /// <returns>9F6E value</returns>
        public static string GetTag9F6EValue(string deviceTypeIndicator)
        {
            string tag9F6EValue = string.Empty;

            if (!string.IsNullOrEmpty(deviceTypeIndicator))
            {
                int dataLength = deviceTypeIndicator.Length / 2;
                string lenInHex = String.Format("{0:X}", dataLength).PadLeft(2, '0');
                tag9F6EValue = SigmaStaticValue.Tag9F6E + lenInHex + deviceTypeIndicator;
            }

            return tag9F6EValue;
        }

    }
}