IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CheckTAKeyFileVersion]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_CheckTAKeyFileVersion]
GO

CREATE PROCEDURE dbo.usp_CheckTAKeyFileVersion
(
	@CurrentVersion DECIMAL(10,2),
	@TAKeyUpdateRequired CHAR(3) OUT 
)
AS
BEGIN	
	DECLARE @TAFileVersion DECIMAL(10, 2)

	SELECT TOP 1 @TAFileVersion = TAFileVersion 
	FROM dbo.TAKey 
	ORDER BY Id DESC

	IF (@CurrentVersion < @TAFileVersion)
		SET @TAKeyUpdateRequired = 'Yes'
	ELSE
		SET @TAKeyUpdateRequired = 'No'
END
GO
