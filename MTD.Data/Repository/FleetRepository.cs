﻿using System;
using System.Collections.Generic;
using System.Linq;

using MTD.Data.Data;
using MTD.Data.Repository.Interface;
using MTData.Utility;

namespace MTD.Data.Repository
{
    public class FleetRepository : BaseRepository, IFleetRepository
    {
        readonly ILogger _logger = new Logger();

        public FleetRepository(IMtDataDevlopmentsEntities dbMtData) : base(dbMtData)
        {
        }

        /// <summary>
        /// Purpose             :   To add fleet
        /// Function Name       :   Add
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/23/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleet"></param>
        /// <param name="userType"></param>
        /// <param name="userId"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public bool Add(Fleet fleet, int userType, int userId, int merchantId)
        {
            try
            {
                Fleet fleetDb = new Fleet();
                var fleetLi = (from fleets in DbMtData.Fleets
                               where fleets.fk_MerchantID == merchantId && fleets.FleetName == fleet.FleetName
                                && fleets.IsActive
                               select fleets).ToList();

                if (fleetLi.Count == 0)
                {
                    if (userType == 2 || userType == 3)
                    {
                        fleetDb.fk_MerchantID = fleet.fk_MerchantID;
                        fleetDb.FleetName = fleet.FleetName;
                        fleetDb.Description = fleet.Description;
                        fleetDb.IsActive = true;
                        fleetDb.CreatedBy = fleet.CreatedBy;
                        fleetDb.CreatedDate = System.DateTime.Now;
                        fleetDb.ModifiedBy = fleet.ModifiedBy;
                        fleetDb.ModifiedDate = DateTime.Now;
                        fleetDb.IsAssigned = false;
                        fleetDb.IsReceipt = false;
                        fleetDb.PayType = fleet.PayType;
                        fleetDb.FleetFee = fleet.FleetFee;
                        fleetDb.fK_CurrencyCodeID = fleet.fK_CurrencyCodeID;
                        fleetDb.FleetFeeType = fleet.FleetFeeType;
                        fleetDb.FleetFeeFixed = fleet.FleetFeeFixed;
                        fleetDb.FleetFeePer = fleet.FleetFeePer;
                        fleetDb.FleetFeeMaxCap = fleet.FleetFeeMaxCap;
                        //Dispatch begin
                        fleetDb.DispatchFleetID = fleet.DispatchFleetID;
                        fleetDb.BookingChannel = fleet.BookingChannel;
                        fleetDb.UserName = fleet.UserName;
                        fleetDb.PCode = fleet.PCode;
                        fleetDb.URL = fleet.URL;

                        DbMtData.Fleets.Add(fleetDb);
                        DbMtData.CommitChanges();

                        DbMtData.CommitChanges();
                    }
                    if (userType == 4)
                    {
                        fleetDb.fk_MerchantID = fleet.fk_MerchantID;
                        fleetDb.FleetName = fleet.FleetName;
                        fleetDb.Description = fleet.Description;
                        fleetDb.IsActive = true;
                        fleetDb.CreatedBy = fleet.CreatedBy;
                        fleetDb.CreatedDate = System.DateTime.Now;
                        fleetDb.ModifiedBy = fleet.ModifiedBy;
                        fleetDb.ModifiedDate = DateTime.Now;
                        fleetDb.IsAssigned = false;
                        fleetDb.IsReceipt = false;
                        fleetDb.PayType = fleet.PayType;
                        fleetDb.fK_CurrencyCodeID = fleet.fK_CurrencyCodeID;
                        fleetDb.DispatchFleetID = fleet.DispatchFleetID;
                        fleetDb.BookingChannel = fleet.BookingChannel;
                        fleetDb.UserName = fleet.UserName;
                        fleetDb.PCode = fleet.PCode;
                        fleetDb.URL = fleet.URL;
                        DbMtData.Fleets.Add(fleetDb);
                        DbMtData.CommitChanges();
                        UserFleet ufleet = new UserFleet();
                        ufleet.fk_FleetID = fleetDb.FleetID;
                        ufleet.fk_UserID = userId;

                        DbMtData.UserFleets.Add(ufleet);
                        DbMtData.CommitChanges();
                    }
                    return false;
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get fleet based on fleet Id
        /// Function Name       :   GetFleet
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/23/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public Fleet GetFleet(int fleetId)
        {
            try
            {
                var fleetLi = (from fleet in DbMtData.Fleets where fleet.FleetID == fleetId select fleet).SingleOrDefault();
                return fleetLi;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get fleet based on fleet Id
        /// Function Name       :   GetFleet
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/23/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public Fleet GetFleet(int fleetId, int merchantId)
        {
            try
            {
                var fleetLi = (from fleet in DbMtData.Fleets where fleet.FleetID == fleetId select fleet).SingleOrDefault(x => x.fk_MerchantID == merchantId);
                return fleetLi;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get currency based on fleet Id
        /// Function Name       :   GetCurrency
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   6/23/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public Fleet GetCurrency(int CurrencyCodeID)
        {
            try
            {
                Fleet fleetCurrency = new Fleet();

                var currency = DbMtData.Currencies.Find(CurrencyCodeID);
                fleetCurrency.CurrencyCode = currency.CurrencyCode;
                return fleetCurrency;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To update fleet
        /// Function Name       :   Update
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/23/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleet"></param>
        /// <param name="fleetId"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public bool Update(Fleet fleet, int fleetId, int merchantId)
        {
            if (fleetId != fleet.FleetID) return false;

            try
            {
                var fleetLi = (from fleets in DbMtData.Fleets
                               where fleets.fk_MerchantID == merchantId && fleets.FleetName == fleet.FleetName && fleetId != fleets.FleetID
                                   && fleets.IsActive
                               select fleets).ToList();

                if (fleetLi.Count == 0)
                {
                    Fleet fleetdata = DbMtData.Fleets.Find(fleetId);
                    fleetdata.FleetName = fleet.FleetName;
                    fleetdata.Description = fleet.Description;
                    fleetdata.ModifiedBy = fleet.ModifiedBy;
                    fleetdata.fK_CurrencyCodeID = fleet.fK_CurrencyCodeID;
                    fleetdata.PayType = fleet.PayType;
                    fleetdata.FleetFee = fleet.FleetFee;
                    fleetdata.FleetFeeType = fleet.FleetFeeType;
                    fleetdata.FleetFeeFixed = fleet.FleetFeeFixed;
                    fleetdata.FleetFeePer = fleet.FleetFeePer;
                    fleetdata.FleetFeeMaxCap = fleet.FleetFeeMaxCap;
                    fleetdata.DispatchFleetID = fleet.DispatchFleetID;
                    fleetdata.BookingChannel = fleet.BookingChannel;
                    fleetdata.UserName = fleet.UserName;
                    fleetdata.PCode = fleet.PCode;
                    fleetdata.URL = fleet.URL;
                    DbMtData.CommitChanges();
                    return false;
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get the special offers list
        /// Function Name       :   DeleteFleet
        /// Created By          :   Asif 
        /// Created On          :   12/23/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public bool DeleteFleet(int fleetId)
        {
            bool isAssignedUser = false;
            var data = DbMtData.Fleets.Find(fleetId);
            var obj = (from item in DbMtData.Drivers.Where(x => x.fk_FleetID == fleetId && x.IsActive) select new { }).FirstOrDefault();
            isAssignedUser = DbMtData.Users.Any(x => DbMtData.UserFleets.Where(y => y.fk_FleetID == fleetId).Select(y => y.fk_UserID).Contains(x.UserID) && x.IsActive && x.fk_UserTypeID == 4);

            if (obj == null && !isAssignedUser)
            {
                data.IsActive = false;
                var receiptDb = DbMtData.FleetReceipts.Where(x => x.fk_FleetID == fleetId).ToList();
                receiptDb.ForEach(x => x.IsActive = false);
                DbMtData.CommitChanges();
                return false;
            }

            return true;
        }

        /// <summary>
        /// Purpose             :   To get list of fleet
        /// Function Name       :   GetUserFleetListByFilter
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/23/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <param name="sorting"></param>
        /// <param name="merchantId"></param>
        /// <param name="userType"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<Fleet> GetUserFleetListByFilter(FilterSearchParameters fsParametes, int logOnByUserId, int logOnByUserType)
        {
            try
            {
                IEnumerable<Fleet> fleetLi = new List<Fleet>();
                switch (fsParametes.UserType)
                {
                    case 2:
                        fleetLi = (from fleet in DbMtData.Fleets
                                   join currency in DbMtData.Currencies on fleet.fK_CurrencyCodeID equals currency.CurrencyCodeID
                                   where fleet.fk_MerchantID == fsParametes.MerchantId && fleet.IsActive
                                   select new { fleet, currency }).Distinct().AsEnumerable().Select(objNew =>
                                                new Fleet { FleetName = objNew.fleet.FleetName, FleetID = objNew.fleet.FleetID, currencyWithSymbol = objNew.currency.CurrencyAbv + "(" + objNew.currency.CurrencySymbol + ")", Description = objNew.fleet.Description }).ToList();
                        break;

                    case 3:
                        fleetLi = (from fleet in DbMtData.Fleets
                                   join currency in DbMtData.Currencies on fleet.fK_CurrencyCodeID equals currency.CurrencyCodeID
                                   where fleet.fk_MerchantID == fsParametes.MerchantId && fleet.IsActive
                                   select new { fleet, currency }).Distinct().AsEnumerable().Select(objNew =>
                                                new Fleet { FleetName = objNew.fleet.FleetName, FleetID = objNew.fleet.FleetID, currencyWithSymbol = objNew.currency.CurrencyAbv + "(" + objNew.currency.CurrencySymbol + ")", Description = objNew.fleet.Description }).ToList();
                        break;
                }

                if (logOnByUserType == 5)
                {
                    List<Fleet> AccessibleFleetList = new List<Fleet>();
                    List<int> fleetAccessed = GetAccesibleFleet(logOnByUserId, fsParametes.MerchantId);
                    foreach (Fleet fleet in fleetLi)
                    {
                        if (fleetAccessed.Contains(fleet.FleetID))
                        {
                            AccessibleFleetList.Add(fleet);
                        }
                    }

                    fleetLi = AccessibleFleetList;
                }

                if (!string.IsNullOrEmpty(fsParametes.Name))
                {
                    fleetLi = fleetLi.Where(p => p.FleetName.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0);
                }

                if (!string.IsNullOrEmpty(fsParametes.Sorting))
                {
                    switch (fsParametes.Sorting)
                    {
                        case "FleetName ASC":
                            fleetLi = fleetLi.OrderBy(p => p.FleetName);
                            break;
                        case "FleetName DESC":
                            fleetLi = fleetLi.OrderByDescending(p => p.FleetName);
                            break;
                        default:
                            fleetLi = fleetLi.OrderByDescending(p => p.FleetName);
                            break;
                    }
                }

                return fsParametes.PageSize > 0
                           ? fleetLi.Skip(fsParametes.StartIndex).Take(fsParametes.PageSize).ToList() //Paging
                           : fleetLi.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get list of fleet
        /// Function Name       :   GetUserFleetListByFilter
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/23/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <param name="sorting"></param>
        /// <param name="merchantId"></param>
        /// <param name="userType"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<Fleet> GetCorporateUserFleetListByFilter(FilterSearchParameters fsParametes)
        {
            try
            {
                IEnumerable<Fleet> fleetLi = new List<Fleet>();
                switch (fsParametes.UserType)
                {
                    case 2:
                        fleetLi = (from fleet in DbMtData.Fleets
                                   join currency in DbMtData.Currencies on fleet.fK_CurrencyCodeID equals currency.CurrencyCodeID
                                   where fleet.fk_MerchantID == fsParametes.MerchantId && fleet.IsActive
                                   select new { fleet, currency }).Distinct().AsEnumerable().Select(objNew =>
                                                new Fleet { FleetName = objNew.fleet.FleetName, FleetID = objNew.fleet.FleetID, currencyWithSymbol = objNew.currency.CurrencyAbv + "(" + objNew.currency.CurrencySymbol + ")", Description = objNew.fleet.Description }).ToList();
                        break;
                    
                    case 3:
                        fleetLi = (from fleet in DbMtData.Fleets
                                   join currency in DbMtData.Currencies on fleet.fK_CurrencyCodeID equals currency.CurrencyCodeID
                                   where fleet.fk_MerchantID == fsParametes.MerchantId && fleet.IsActive
                                   select new { fleet, currency }).Distinct().AsEnumerable().Select(objNew =>
                                                new Fleet { FleetName = objNew.fleet.FleetName, FleetID = objNew.fleet.FleetID, currencyWithSymbol = objNew.currency.CurrencyAbv + "(" + objNew.currency.CurrencySymbol + ")", Description = objNew.fleet.Description }).ToList();
                        break;
                }

                if (!string.IsNullOrEmpty(fsParametes.Name))
                {
                    fleetLi = fleetLi.Where(p => p.FleetName.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0);
                }

                if (!string.IsNullOrEmpty(fsParametes.Sorting))
                {
                    switch (fsParametes.Sorting)
                    {
                        case "FleetName ASC":
                            fleetLi = fleetLi.OrderBy(p => p.FleetName);
                            break;
                        case "FleetName DESC":
                            fleetLi = fleetLi.OrderByDescending(p => p.FleetName);
                            break;
                        default:
                            fleetLi = fleetLi.OrderByDescending(p => p.FleetName);
                            break;
                    }
                }

                return fsParametes.PageSize > 0
                           ? fleetLi.Skip(fsParametes.StartIndex).Take(fsParametes.PageSize).ToList() //Paging
                           : fleetLi.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleet"></param>
        /// <returns></returns>
        public bool Add(Fleet fleet)
        {
            try
            {
                var fleetDb = DbMtData.Fleets.Find(fleet.fk_FleetID);
                fleetDb.CarTransaction = fleet.CarTransaction;
                fleetDb.FleetTransaction = fleet.FleetTransaction;
                fleetDb.TransactionValue = fleet.TransactionValue;
                fleetDb.IsCardSwiped = fleet.IsCardSwiped;
                fleetDb.IsChipAndPin = fleet.IsChipAndPin;
                fleetDb.IsContactless = fleet.IsContactless;
                fleetDb.ModifiedBy = fleet.ModifiedBy;
                fleetDb.IsShowTip = fleet.IsShowTip;
                fleetDb.ModifiedDate = DateTime.Now;
                fleetDb.DispatchFleetID = fleet.DispatchFleetID;
                fleetDb.BookingChannel = fleet.BookingChannel;
                fleetDb.UserName = fleet.UserName;
                fleetDb.PCode = fleet.PCode;
                fleetDb.URL = fleet.URL;
                fleetDb.IsAssigned = true;
                DbMtData.CommitChanges();

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fsParametes"></param>
        /// <returns></returns>
        public IEnumerable<Fleet> FleetSettingListByFiter(FilterSearchParameters fsParametes)
        {
            try
            {
                IEnumerable<Fleet> fleetList = (from fleet in DbMtData.Fleets
                                                where fleet.fk_MerchantID == fsParametes.MerchantId
                                                       && (bool)fleet.IsActive && (bool)fleet.IsAssigned
                                                select fleet).ToList();

                if (!string.IsNullOrEmpty(fsParametes.Name))
                {
                    fleetList = fleetList.Where(p => p.FleetName.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0);
                }

                if (!string.IsNullOrEmpty(fsParametes.Sorting))
                {
                    fleetList = SortedFleet(fsParametes.Sorting, fleetList);
                }

                return fsParametes.PageSize > 0
                          ? fleetList.Skip(fsParametes.StartIndex).Take(fsParametes.PageSize).ToList() //Paging
                          : fleetList.ToList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public Fleet GetFleetSetting(int fleetId)
        {
            try
            {
                return DbMtData.Fleets.Find(fleetId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public bool DeleteFleetSetting(int fleetId)
        {
            try
            {
                var data = DbMtData.Fleets.Find(fleetId);
                data.IsAssigned = false;
                DbMtData.CommitChanges();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<Fleet> GetSettingFleetList(int merchantId)
        {
            try
            {
                IEnumerable<Fleet> fleetList =
                    (from fleet in DbMtData.Fleets where fleet.fk_MerchantID == merchantId && fleet.IsActive && !(bool)fleet.IsAssigned select new { fleet.FleetID, fleet.FleetName }).AsEnumerable()
                        .Select(objNew =>
                            new Fleet
                            {
                                FleetID = objNew.FleetID,
                                FleetName = objNew.FleetName
                            }).ToList();
                return fleetList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Currency> CurrencyList()
        {
            try
            {
                var currencyList = DbMtData.Currencies.ToList();
                return currencyList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="fleetList"></param>
        /// <returns></returns>
        private IEnumerable<Fleet> SortedFleet(string sorting, IEnumerable<Fleet> fleetList)
        {
            switch (sorting)
            {
                case "FleetName ASC":
                    fleetList = fleetList.OrderBy(p => p.FleetName);
                    break;
                case "FleetName DESC":
                    fleetList = fleetList.OrderByDescending(p => p.FleetTransaction);
                    break;
                case "FleetTransaction ASC":
                    fleetList = fleetList.OrderBy(p => p.FleetTransaction);
                    break;
                case "FleetTransaction DESC":
                    fleetList = fleetList.OrderByDescending(p => p.FleetTransaction);
                    break;
                case "CarTransaction ASC":
                    fleetList = fleetList.OrderBy(p => p.CarTransaction);
                    break;
                case "CarTransaction DESC":
                    fleetList = fleetList.OrderByDescending(p => p.CarTransaction);
                    break;
                case "TransactionValue ASC":
                    fleetList = fleetList.OrderBy(p => p.TransactionValue);
                    break;
                case "TransactionValue DESC":
                    fleetList = fleetList.OrderByDescending(p => p.TransactionValue);
                    break;
            }

            return fleetList;
        }

        /// <summary>
        /// Purpose             :   To get list of fleet
        /// Function Name       :   FleetList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/23/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="userType"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public IEnumerable<Fleet> FleetList(int merchantId, int userType, int UserId)
        {
            try
            {
                IEnumerable<Fleet> fleetLi = new List<Fleet>();
                if (userType == 2 || userType == 3)
                {
                    fleetLi = (from fleet in DbMtData.Fleets
                               join currency in DbMtData.Currencies on fleet.fK_CurrencyCodeID equals currency.CurrencyCodeID
                               where fleet.fk_MerchantID == merchantId && fleet.IsActive
                               select new { fleet, currency }).Distinct().AsEnumerable().Select(objNew =>
                                                    new Fleet { FleetName = objNew.fleet.FleetName, FleetID = objNew.fleet.FleetID, currencyWithSymbol = objNew.currency.CurrencyAbv + "(" + objNew.currency.CurrencySymbol + ")", Description = objNew.fleet.Description }).ToList();
                }

                if (userType == 4)
                {
                    fleetLi = (from fleet in DbMtData.Fleets
                               join ufleet in DbMtData.UserFleets on fleet.FleetID equals ufleet.fk_FleetID
                               join currency in DbMtData.Currencies on fleet.fK_CurrencyCodeID equals currency.CurrencyCodeID
                               where fleet.fk_MerchantID == merchantId && ufleet.fk_UserID == UserId && fleet.IsActive
                               select new { fleet, currency }).Distinct().AsEnumerable().Select(objNew =>
                                                   new Fleet { FleetName = objNew.fleet.FleetName, FleetID = objNew.fleet.FleetID, currencyWithSymbol = objNew.currency.CurrencyAbv + "(" + objNew.currency.CurrencySymbol + ")", Description = objNew.fleet.Description }).ToList();
                }

                return fleetLi;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fsParametes"></param>
        /// <returns></returns>
        public int FleetListCount(FilterSearchParameters fsParametes, int logOnByUserId, int logOnByUserType)
        {
            try
            {
                IEnumerable<Fleet> fleetLi = new List<Fleet>();
                if (fsParametes.UserType == 2 || fsParametes.UserType == 3)
                {
                    fleetLi = (from fleet in DbMtData.Fleets
                               join currency in DbMtData.Currencies on fleet.fK_CurrencyCodeID equals currency.CurrencyCodeID
                               where fleet.fk_MerchantID == fsParametes.MerchantId && fleet.IsActive
                               select new { fleet, currency }).Distinct().AsEnumerable().Select(objNew =>
                                                    new Fleet { FleetName = objNew.fleet.FleetName, FleetID = objNew.fleet.FleetID, currencyWithSymbol = objNew.currency.CurrencyAbv + "(" + objNew.currency.CurrencySymbol + ")", Description = objNew.fleet.Description }).ToList();
                }
                
                if (fsParametes.UserType == 4)
                {
                    fleetLi = (from fleet in DbMtData.Fleets
                               join ufleet in DbMtData.UserFleets on fleet.FleetID equals ufleet.fk_FleetID
                               join currency in DbMtData.Currencies on fleet.fK_CurrencyCodeID equals currency.CurrencyCodeID
                               where fleet.fk_MerchantID == fsParametes.MerchantId && ufleet.fk_UserID == fsParametes.UserId && fleet.IsActive
                               select new { fleet, currency }).Distinct().AsEnumerable().Select(objNew =>
                                                   new Fleet { FleetName = objNew.fleet.FleetName, FleetID = objNew.fleet.FleetID, currencyWithSymbol = objNew.currency.CurrencyAbv + "(" + objNew.currency.CurrencySymbol + ")", Description = objNew.fleet.Description }).ToList();
                }
                
                if (logOnByUserType == 5)
                {
                    List<Fleet> AccessibleFleetList = new List<Fleet>();
                    List<int> fleetAccessed = GetAccesibleFleet(logOnByUserId, fsParametes.MerchantId);
                    foreach (Fleet fleet in fleetLi)
                    {
                        if (fleetAccessed.Contains(fleet.FleetID))
                        {
                            AccessibleFleetList.Add(fleet);
                        }
                    }

                    fleetLi = AccessibleFleetList;
                }

                if (!string.IsNullOrEmpty(fsParametes.Name))
                {
                    fleetLi = fleetLi.Where(p => p.FleetName.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }

                return fleetLi.Count();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fsParametes"></param>
        /// <returns></returns>
        public int FleetSettingListCount(FilterSearchParameters fsParametes)
        {
            try
            {
                IEnumerable<Fleet> fleetList = (from fleet in DbMtData.Fleets
                                                where fleet.fk_MerchantID == fsParametes.MerchantId
                                                       && (bool)fleet.IsActive && (bool)fleet.IsAssigned
                                                select fleet).ToList();

                if (!string.IsNullOrEmpty(fsParametes.Name))
                {
                    fleetList = fleetList.Where(p => p.FleetName.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0);
                }

                return fleetList.Count();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="userType"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public IEnumerable<Fleet> FeeFleetList(int merchantId, int userType, int userId)
        {
            try
            {
                IEnumerable<Fleet> fleetLi = new List<Fleet>();
                if (userType == 2 || userType == 3)
                {
                    fleetLi = (from fleet in DbMtData.Fleets
                               join currency in DbMtData.Currencies on fleet.fK_CurrencyCodeID equals currency.CurrencyCodeID
                               where fleet.fk_MerchantID == merchantId && fleet.IsActive && fleet.PayType == 3
                               select new { fleet, currency }).Distinct().AsEnumerable().Select(objNew =>
                                                    new Fleet { FleetName = objNew.fleet.FleetName, FleetID = objNew.fleet.FleetID, currencyWithSymbol = objNew.currency.CurrencyAbv + "(" + objNew.currency.CurrencySymbol + ")", Description = objNew.fleet.Description }).ToList();
                }

                if (userType == 4)
                {
                    fleetLi = (from fleet in DbMtData.Fleets
                               join ufleet in DbMtData.UserFleets on fleet.FleetID equals ufleet.fk_FleetID
                               join currency in DbMtData.Currencies on fleet.fK_CurrencyCodeID equals currency.CurrencyCodeID
                               where fleet.fk_MerchantID == merchantId && ufleet.fk_UserID == userId && fleet.IsActive && fleet.PayType == 3
                               select new { fleet, currency }).Distinct().AsEnumerable().Select(objNew =>
                                                   new Fleet { FleetName = objNew.fleet.FleetName, FleetID = objNew.fleet.FleetID, currencyWithSymbol = objNew.currency.CurrencyAbv + "(" + objNew.currency.CurrencySymbol + ")", Description = objNew.fleet.Description }).ToList();
                }

                return fleetLi;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleet"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public bool AddFleetFee(Fleet fleet)
        {
            try
            {
                bool isExist = DbMtData.Surcharges.Any(x => x.FleetID == fleet.FleetID && x.MerchantId == fleet.fk_MerchantID && x.FleetFee != null && x.IsActive);
                if (isExist)
                {
                    return false;
                }

                Surcharge surcharge = new Surcharge();
                surcharge.IsActive = true;
                surcharge.FleetID = fleet.FleetID;
                surcharge.FleetFee = fleet.FleetFee;
                surcharge.MerchantId = fleet.fk_MerchantID;
                surcharge.CreatedBy = fleet.CreatedBy;
                surcharge.CreatedDate = DateTime.Now;
                DbMtData.Surcharges.Add(surcharge);

                return Convert.ToBoolean(DbMtData.CommitChanges());
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Surcharge> FleetFeetListByFilter(FilterSearchParameters fsParametes)
        {
            try
            {
                IEnumerable<Surcharge> fleetFeeList = (from m in DbMtData.Surcharges
                                                       join l in DbMtData.Fleets on m.FleetID equals l.FleetID
                                                       join k in DbMtData.Merchants on m.MerchantId
                                                        equals k.MerchantID
                                                       where m.IsActive && m.FleetFee != null && fsParametes.MerchantId == k.MerchantID
                                                       select
                                                            new
                                                            {
                                                                m.SurID,
                                                                l.FleetName,
                                                                m.FleetFee
                                                            }).AsEnumerable().Select
               (objNew => new Surcharge
               {
                   SurID = objNew.SurID,
                   FleetName = objNew.FleetName,
                   FleetFee = objNew.FleetFee
               }).ToList();

                if (!string.IsNullOrEmpty(fsParametes.Name))
                {
                    fleetFeeList = fleetFeeList.Where(p => p.FleetName.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0);
                }

                if (!string.IsNullOrEmpty(fsParametes.Sorting))
                {
                    switch (fsParametes.Sorting)
                    {
                        case "FleetName ASC":
                            fleetFeeList = fleetFeeList.OrderBy(p => p.FleetName);
                            break;
                        case "FleetName DESC":
                            fleetFeeList = fleetFeeList.OrderByDescending(p => p.FleetName);
                            break;
                        default:
                            fleetFeeList = fleetFeeList.OrderByDescending(p => p.FleetName);
                            break;
                    }
                }

                return fsParametes.PageSize > 0
                           ? fleetFeeList.Skip(fsParametes.StartIndex).Take(fsParametes.PageSize).ToList() //Paging
                           : fleetFeeList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="surchargeId"></param>
        /// <returns></returns>
        public Surcharge GetFleetFee(int surchargeId)
        {
            try
            {
                var surchargeDb = (from m in DbMtData.Surcharges
                                   join l in DbMtData.Fleets on m.FleetID equals l.FleetID
                                   where m.SurID == surchargeId
                                   select new { m.FleetFee, l.FleetName, m.SurID, m.FleetID }).FirstOrDefault();

                Surcharge obj = new Surcharge();
                obj.FleetFee = surchargeDb.FleetFee;
                obj.FleetName = surchargeDb.FleetName;
                obj.SurID = surchargeDb.SurID;
                obj.FleetID = surchargeDb.FleetID;

                return obj;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fsParametes"></param>
        /// <returns></returns>
        public int FleetFeeListCount(FilterSearchParameters fsParametes)
        {
            try
            {
                IEnumerable<Surcharge> fleetFeeList = (from m in DbMtData.Surcharges
                                                       join l in DbMtData.Fleets on m.FleetID equals l.FleetID
                                                       join k in DbMtData.Merchants on m.MerchantId
                                                        equals k.MerchantID
                                                       where m.IsActive && m.FleetFee != null && fsParametes.MerchantId == k.MerchantID
                                                       select
                                                            new
                                                            {
                                                                m.SurID,
                                                                l.FleetName,
                                                                m.FleetFee
                                                            }).AsEnumerable().Select
                (objNew => new Surcharge
                {
                    SurID = objNew.SurID,
                    FleetName = objNew.FleetName,
                    FleetFee = objNew.FleetFee
                }).ToList();

                if (!string.IsNullOrEmpty(fsParametes.Name))
                {
                    fleetFeeList = fleetFeeList.Where(p => p.FleetName.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                return fleetFeeList.Count();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateFleetFee(Surcharge surcharge)
        {
            try
            {
                Surcharge surchargeDb = DbMtData.Surcharges.Find(surcharge.SurID);
                surchargeDb.FleetFee = surcharge.FleetFee;
                surchargeDb.ModifiedDate = DateTime.Now;
                surchargeDb.ModifiedBy = surcharge.ModifiedBy;
                return Convert.ToBoolean(DbMtData.CommitChanges());
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="surchargeId"></param>
        public void DeleteFleetFee(int surchargeId)
        {
            try
            {
                Surcharge surchargeDb = DbMtData.Surcharges.Find(surchargeId);
                surchargeDb.IsActive = false;
                DbMtData.CommitChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<int> GetAccesibleFleet(int logOnByUserId, int merchantId)
        {
            List<int> accessibleFleets = (from m in DbMtData.UserFleets
                                          where m.fk_UserID == logOnByUserId && m.fk_MerchantId == merchantId
                                          && m.fk_UserTypeId == 5 && m.IsActive == true
                                          select (int)m.fk_FleetID).Distinct().ToList();
            return accessibleFleets;
        }

        public string Authenticate(string SystemID, string userName, string pCode)
        {
            try
            {
                int systemId = Convert.ToInt32(SystemID);
                string passCode = (from m in DbMtData.Fleets
                                    where m.UserName == userName && m.IsActive
                                    select m.PCode).FirstOrDefault();

                bool isValidUser = StringExtension.ValidatePassCode(pCode, passCode);
                if (!isValidUser)
                    return "InvalidCredentials";
                
                string token = StringExtension.GenerateToken(userName, pCode);
                Fleet fleetDb = DbMtData.Fleets.Where(x => x.UserName == userName && x.DispatchFleetID == systemId).FirstOrDefault();
                if (fleetDb != null)
                {
                    fleetDb.Token = token;
                    DbMtData.CommitChanges();
                    return token;
                }

                return "WebMethodAccessDenied";
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool tokenValidation(string token, int fid)
        {
            bool isFound = DbMtData.Fleets.Any(x => x.Token == token && x.DispatchFleetID == fid);
            if (isFound)
                return true;
            return false;
        }

        public bool tokenReset(string token)
        {
            bool isFound = DbMtData.Fleets.Any(x => x.Token == token);
            if (isFound)
            {
                var fleet = DbMtData.Fleets.Where(x => x.Token == token).FirstOrDefault();
                fleet.Token = null;
                DbMtData.CommitChanges();
            }
            return true;
        }

        /// <summary>
        /// Purpose             :   To get the currency code by fleet
        /// Function Name       :   GetCurrencyByFleet
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/27/2016
        /// </summary>
        ///<param name="fleetId"></param>
        /// <returns>int</returns>
        public int GetCurrencyByFleet(int fleetId)
        {
            var currencyCode = (from m in DbMtData.Fleets join n in DbMtData.Currencies on m.fK_CurrencyCodeID equals n.CurrencyCodeID where m.FleetID == fleetId select n.CurrencyCode).FirstOrDefault();
            return Convert.ToInt32(currencyCode);
        }
    }
}
