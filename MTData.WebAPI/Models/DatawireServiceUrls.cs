﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Models
{


    public class DatawireServiceUrls
    {
       
        public string Url
        {
            get;
            set;
        }
       
        public long TransactionTime
        {
            get;
            set;
        }
     
        public int MaximumTransactionInPackage
        {
            get;
            set;
        }
    }
}
