﻿using System.Collections.Generic;

using MTD.Core.DataTransferObjects;

namespace MTD.Core.Service.Interface
{
    public interface IFleetService
    {
        bool Add(FleetDto fleetDto, int userType, int userId, int merchantId);

        IEnumerable<FleetDto> FleetList(int merchantId, int userType, int userId);

        FleetDto GetFleet(int fleetId, int merchantId);

        bool Update(FleetDto fleetDto, int fleetId, int merchantId);
        
        FleetDto GetCurrency(int CurrencyCodeID);
        
        bool DeleteFleet(int fleetId);

        FleetDto GetFleet(int fleetId);

        IEnumerable<FleetDto> GetUserFleetListByFilter(FilterSearchParametersDto fsParametes, int logOnByUserId,int logOnByUserType);
        
        bool Add(FleetSettingDto fleetSettingDto);
        
        IEnumerable<FleetSettingDto> FleetSettingList(FilterSearchParametersDto filterSearch);
        
        FleetSettingDto GetFleetSetting(int fleetId);
        
        bool DeleteFleetSetting(int fleetId);
        
        IEnumerable<FleetDto> GetSettingFleetList(int merchantId);
        
        IEnumerable<CurrencyDto> CurrencyList();

        int FleetListCount(FilterSearchParametersDto fsParametes,int logOnByUserId, int logOnByUserType);

        int FleetSettingListCount(FilterSearchParametersDto fsParametes);

        IEnumerable<FleetDto> FeeFleetList(int merchantId, int userType, int userId);

        bool AddFleetFee(FleetDto fleetDto);

        IEnumerable<SurchargeDto> FleetFeetListByFilter(FilterSearchParametersDto fsParametes);

        SurchargeDto GetFleetFee(int surchargeId);

        int FleetFeeListCount(FilterSearchParametersDto fsParametes);

        bool UpdateFleetFee(SurchargeDto surchargeDto);

        void DeleteFleetFee(int surchargeId);

        List<int> GetAccesibleFleet(int logOnByUserId,int merchantId);

        string Authenticate(string SystemID, string userName, string pCode);

        bool tokenValidation(string token, int fid);

        bool tokenReset(string token);

        int GetCurrencyByFleet(int fleetId);
    }
}
