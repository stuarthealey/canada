IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_AdminSummaryReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_AdminSummaryReport]
GO

CREATE PROCEDURE dbo.usp_AdminSummaryReport
-- =============================================
-- Author:	<Naveen Kumar>
-- Create date: <10-03-2016>
-- Description:	<Prepare and Send summary reports to admin users>
-- =============================================
-- S.Healey		2016-11-08		- Performance optimisation with use of indexes and improved queries.
--
(
	@IsOnDemandReport BIT,
	@FromDate DATETIME = NULL,
	@ToDate DATETIME = NULL,
	@XmlData NVARCHAR(MAX) OUT,
	@ReportType INT OUT,
	@FDate DATE OUT,
	@TDate DATE OUT,
	@TotalTransaction INT OUT,
	@TotalActualFare NVARCHAR(40) OUT,
	@TotalTips NVARCHAR(40) OUT,
	@TotalFareAmount NVARCHAR(40) OUT,
	@TotalSurcharge NVARCHAR(40) OUT,
	@TotalFee NVARCHAR(40) OUT,
	@NoChargeBack INT OUT,
	@ChargeBackValue NVARCHAR(40) OUT,
	@NoVoid INT OUT,
	@VoidValue NVARCHAR(40) OUT,
	@TotalTechFee NVARCHAR(40) OUT,
	@AmountOwing  NVARCHAR(40) OUT,
	@NextScheduleDateTime DATETIME OUT
)
AS
BEGIN
	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SET NOCOUNT ON;
		SET ANSI_WARNINGS OFF

		DECLARE @Frequency INT,
				@FrequencyType INT,
				@DateFrom DATE,
				@DateTo DATE,
				@ScheduleId INT

		DECLARE @InnerTable TABLE (
			Id INT,
			FareValue MONEY,
			Tip MONEY,
			Amount MONEY,
			Surcharge MONEY,
			Fee MONEY,
			FleetID INT, 
			TxnType VARCHAR(15),
			ResponseCode VARCHAR(25),
			fk_FleetId INT,
			IsRefunded BIT,
			IsVoided BIT,
			IsCompleted BIT,
			TechFee MONEY,
			FleetFee MONEY
			)

		DECLARE @OuterTable TABLE (
			Id  INT,
			FareValue MONEY,
			Tip MONEY,Amount MONEY,
			Surcharge MONEY,
			Fee MONEY,
			TxnType VARCHAR(15),
			ResponseCode NVARCHAR(25),
			fk_FleetId INT,
			TechFee MONEY,
			IsRefunded BIT,
			IsVoided BIT,
			IsCompleted BIT,
			FleetFee MONEY
		)

		SELECT	@ScheduleId = ScheduleID,
				@Frequency = Frequency,
				@FrequencyType = FrequencyType,
				@NextScheduleDateTime = NextScheduleDateTime
		FROM dbo.ScheduleReport
		WHERE fk_ReportId = 1
		  AND IsCreated = 1
		  AND fk_MerchantId IS NULL
		  AND CorporateUserId IS NULL

		IF (@IsOnDemandReport = 1)
		BEGIN
			SET @DateFrom = @FromDate
			SET @DateTo = @ToDate
		END
		ELSE
		BEGIN
			IF (@Frequency = 1)
			BEGIN
				SET @DateFrom = GETDATE() - @FrequencyType
				SET @DateTo = GETDATE()
				SET @NextScheduleDateTime = @NextScheduleDateTime + @FrequencyType
			END
			ELSE IF (@Frequency = 2)
			BEGIN
				SET @DateFrom = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK, -@FrequencyType, GETDATE())), 0) - 1
				SET @DateTo = DATEADD(WEEK, DATEDIFF(WEEK, 0, GETDATE()), 0) - 1
				SET @NextScheduleDateTime = DATEADD(WK, @FrequencyType, @NextScheduleDateTime)
			END
			ELSE IF (@Frequency = 3)
			BEGIN
				SET @DateFrom = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH, -@FrequencyType, GETDATE())), 0)
				SET @DateTo = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)
				SET @NextScheduleDateTime = DATEADD(MONTH, @FrequencyType, @NextScheduleDateTime)
			END
			ELSE
			BEGIN
				RETURN 1
			END
		END

		--insert data into table variable InnerTable for reuse*/                         
		INSERT INTO @InnerTable(Id, FareValue, Tip, Amount, Surcharge, Fee, FleetID, TxnType, ResponseCode, IsRefunded, IsVoided, IsCompleted, TechFee, FleetFee)                   
		SELECT	mt.Id,
				mt.FareValue,
				mt.Tip,
				mt.Amount,
				mt.Surcharge,
				mt.Fee,
				fl.FleetID,
				mt.TxnType,
				mt.ResponseCode,
				mt.IsRefunded,
				mt.IsVoided,
				mt.IsCompleted,
				mt.TechFee,
				mt.FleetFee		
		FROM dbo.MTDTransaction mt WITH (INDEX(IX_MTDTransactionTxnDate))
			INNER JOIN dbo.Vehicle vh ON vh.VehicleNumber = mt.VehicleNo AND vh.fk_FleetID = mt.fk_FleetId
			INNER JOIN dbo.Fleet fl ON fl.FleetID = vh.fk_FleetID
		WHERE mt.TxnDate >= @DateFrom 
		  AND mt.TxnDate < @DateTo 

		--insert data into table variable OuterTable for reuse*/    
		INSERT INTO @OuterTable(Id, FareValue, Tip, Amount, Surcharge, Fee, TxnType, ResponseCode, fk_FleetId, TechFee, IsRefunded, IsVoided, IsCompleted, FleetFee) 
		SELECT	mt.Id,
				mt.FareValue,
				mt.Tip,
				mt.Amount,
				mt.Surcharge,
				mt.Fee,
				mt.TxnType,
				mt.ResponseCode,
				mt.fk_FleetId,
				mt.TechFee,
				mt.IsRefunded,
				mt.IsVoided,
				mt.IsCompleted,
				mt.FleetFee
		FROM  dbo.MTDTransaction mt WITH (INDEX(IX_MTDTransactionTxnDate))
		WHERE mt.TxnDate >= @DateFrom
		  AND mt.TxnDate < @DateTo

		SET @XmlData = CAST((SELECT  ROW_NUMBER() OVER (ORDER BY Company) AS 'td','',
								   COALESCE(mer.Company,'') AS 'td','',
								   COALESCE(f.FleetName,'')  AS 'td','',
								   (SELECT COALESCE(COUNT(id), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND it.TxnType IN ('Sale', 'Completion') AND it.IsRefunded = 0 AND it.IsVoided = 0 AND it.ResponseCode IN ('000', '00', '002')) AS 'td', '',
								   CONCAT('$',(SELECT COALESCE(SUM(FareValue), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID  AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td', '',
								   CONCAT('$',(SELECT COALESCE(SUM(Tip), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND  it.ResponseCode IN ('000', '00', '002'))) AS 'td', '',
								   CONCAT('$',(SELECT COALESCE(SUM(Surcharge), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID  AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td', '',
								   CONCAT('$',(SELECT COALESCE(SUM(TechFee), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td', '',
								   (SELECT COALESCE(COUNT(id), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND it.TxnType = 'Refund' AND it.ResponseCode IN ('000', '00')) AS 'td', '',
								   CONCAT('$',(SELECT COALESCE(SUM(Amount), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND it.TxnType = 'Refund'AND it.ResponseCode IN ('000', '00'))) AS 'td', '',
								   (SELECT COALESCE(COUNT(id), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND it.TxnType = 'Void' AND it.ResponseCode IN ('000', '00')) AS 'td', '',
								   CONCAT('$',(SELECT COALESCE(SUM(Amount), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND it.TxnType = 'Void'AND it.ResponseCode IN ('000', '00'))) AS 'td', '',
								   CONCAT('$',(SELECT COALESCE(SUM(Amount), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND it.TxnType IN ('Sale', 'Completion') AND it.IsRefunded = 0 AND it.IsVoided = 0 AND it.ResponseCode IN ('000', '00', '002'))) AS 'td', '',
								   CONCAT('$',(SELECT COALESCE(SUM(it.Fee), 0)+COALESCE(SUM(it.FleetFee), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td', '',
								   CONCAT('$',(SELECT COALESCE(SUM(Amount), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND it.TxnType IN ('Sale', 'Completion') AND it.IsRefunded = 0 AND IT.IsVoided = 0 AND it.ResponseCode IN ('000', '00', '002')) -
										(SELECT COALESCE(SUM(Fee), 0) + COALESCE(SUM(FleetFee), 0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td'

							FROM dbo.MTDTransaction m WITH (INDEX(IX_MTDTransactionTxnDate))
								INNER JOIN dbo.Vehicle v ON v.VehicleNumber = m.VehicleNo AND v.fk_FleetID = m.fk_FleetId
								INNER JOIN dbo.fleet f ON f.FleetID = v.fk_FleetID
								INNER JOIN dbo.merchant mer ON mer.MerchantID = f.fk_MerchantID

							WHERE m.TxnDate >= @DateFrom
							  AND m.TxnDate < @DateTo    
							  AND m.ResponseCode IN ('000', '00', '002')   
							  AND m.Id NOT IN (SELECT Id FROM dbo.MTDTransaction WHERE m.TxnType = 'Authorization' AND m.IsCompleted = 0)

							GROUP BY f.FleetName, f.FleetID, mer.Company 
							ORDER BY mer.Company 
							FOR XML PATH('tr'), ELEMENTS ) AS NVARCHAR(MAX))

		SELECT @TotalTransaction = ISNULL(COUNT(Id), 0)
		FROM  @OuterTable ot 
		WHERE ot.TxnType IN ('Sale', 'Completion') 
		  AND ot.ResponseCode IN ('000', '00', '002')
		  AND ot.IsRefunded = 0 
		  AND ot.IsVoided = 0

		SELECT	@TotalActualFare = CONCAT('$',CAST(COALESCE(SUM(ot.FareValue), 0) AS DECIMAL(18, 2))),
				@TotalTips = CONCAT('$',CAST(COALESCE(SUM(ot.Tip), 0) AS DECIMAL(18, 2))),
				@TotalSurcharge = CONCAT('$',CAST(COALESCE(SUM(ot.Surcharge), 0) AS DECIMAL(18, 2))),
				@TotalFee = CAST(COALESCE(SUM(ot.Fee), 0) + COALESCE(SUM(ot.FleetFee), 0) AS DECIMAL(18, 2)),
				@TotalTechFee = CAST(COALESCE(SUM(ot.TechFee), 0) AS DECIMAL(18, 2))	
		FROM  @OuterTable ot 
		WHERE ot.ResponseCode IN ('000', '00', '002') 
		  AND ((ot.TxnType = 'Sale' AND ot.IsRefunded = 0 AND ot.IsVoided = 0) OR (ot.TxnType = 'Authorization' AND ot.IsCompleted = 1)) 

		SELECT  @TotalFareAmount = CONCAT('$', CAST(COALESCE(SUM(ot.Amount), 0) AS DECIMAL(18, 2))),
				@AmountOwing = CONCAT('$', CAST(COALESCE(SUM(ot.Amount), 0) AS DECIMAL(18, 2)) - CAST(@TotalFee AS DECIMAL(18, 2)))
		FROM @OuterTable ot
		WHERE ot.ResponseCode IN ('000', '00', '002') 
		  AND ot.TxnType IN ('Sale', 'Completion') 
		  AND ot.IsRefunded = 0 
		  AND ot.IsVoided = 0 

		SELECT	@NoChargeBack = COALESCE(COUNT(ot.Id), 0),
				@ChargeBackValue = CONCAT('$', CAST(COALESCE(SUM(ot.Amount), 0) AS DECIMAL(18, 2))) 
		FROM @OuterTable ot 
		WHERE ot.TxnType ='Refund' 
		  AND ot.ResponseCode IN ('000', '00')

		SELECT	@NoVoid = ISNULL(COUNT(Id), 0), 
				@VoidValue = CONCAT('$', CAST(ISNULL(SUM(Amount), 0) AS DECIMAL(18, 2))) 
		FROM @OuterTable ot 
		WHERE ot.TxnType = 'Void' 
		  AND ot.ResponseCode IN ('000', '00')

		SET @TotalFee = CONCAT('$', @TotalFee)
		SET @ReportType = @Frequency 
		SET @FDate = @DateFrom
		SET @TDate = DATEADD(DAY, -1, @DateTo)
		SET @TotalTechFee = CONCAT('$', @TotalTechFee)
	END TRY
	BEGIN CATCH
		  INSERT INTO ProcErrorHandler(ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, UserName, HostName, [TimeStamp])
		  SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()	 
	END CATCH
END
GO
