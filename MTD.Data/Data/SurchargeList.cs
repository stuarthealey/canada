﻿using System;

namespace MTD.Data.Data
{
    public class SurchargeList
    {
        public int SurID { get; set; }
        public Nullable<int> CCPrefix { get; set; }
        public Nullable<byte> SurchargeType { get; set; }
        public Nullable<decimal> SurchargeFixed { get; set; }
        public Nullable<decimal> SurchargePer { get; set; }
        public Nullable<decimal> SurMaxCap { get; set; }
        public Nullable<byte> BookingFeeType { get; set; }
        public Nullable<decimal> BookingFeeFixed { get; set; }
        public Nullable<decimal> BookingFeePer { get; set; }
        public Nullable<decimal> BookingFeeMaxCap { get; set; }
        public Nullable<byte> TechFeeType { get; set; }
        public Nullable<decimal> TechFeeFixed { get; set; }
        public Nullable<decimal> TechFeePer { get; set; }
        public Nullable<decimal> TechFeeMaxCap { get; set; }
        public string CCType { get; set; }
        public string FleetName { get; set; }
        public int FleetID { get; set; }
        public Nullable<int> fk_GatewayID { get; set; }
        public Nullable<bool> IsDefault { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }
        public string FeeType { get; set; }
        public string SurType { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Company { get; set; }
        public Nullable<decimal> FleetFee { get; set; }
    }
}
