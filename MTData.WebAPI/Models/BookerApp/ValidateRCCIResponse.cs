﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Models
{
    public class ValidateRCCIResp
    {
        public CreditCardStatus CreditCardStatus { get; set; }

        public TruncatedCreditCardDetails TruncatedCreditCardDetails { get; set; }
    }
}