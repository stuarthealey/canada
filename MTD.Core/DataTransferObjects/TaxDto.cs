﻿using System;

namespace MTD.Core.DataTransferObjects
{
   public class TaxDto
    {
        public int MerchantID { get; set; }
        public Nullable<int> fk_UserID { get; set; }
        public Nullable<decimal> StateTaxRate { get; set; }
        public Nullable<decimal> FederalTaxRate { get; set; }
        public string Company { get; set; }
        public Nullable<bool> IsTaxInclusive { get; set; }
        public string DisclaimerPlainText { get; set; }
        public string TermConditionPlainText { get; set; }

        public Nullable<bool> UseTransArmor { get; set; }       //PAY-13
    }
}
