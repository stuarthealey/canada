﻿namespace MTData.Web.ViewData
{
    public class TipViewData
    {
        public string  TipText { get; set; }
        public decimal? TipValue { get; set; }
    }
}