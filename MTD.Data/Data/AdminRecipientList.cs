﻿using System;
using System.Collections.Generic;

namespace MTD.Data.Data
{
    public class AdminRecipientList
    {
        public int fk_UserId { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public Nullable<bool> IsRecipient { get; set; }
        public IEnumerable<AdminRecipientMaster> AdminList { get; set; }        
    }
}
