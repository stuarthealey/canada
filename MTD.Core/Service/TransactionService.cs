﻿using System.Collections.Generic;
using System.Linq;
using System;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Extension;
using MTD.Core.Factory.Interface;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Core.Service
{
    public class TransactionService : ITransactionService
    {
        private readonly ITransactionRepository _transactionReporsitory;
        private readonly ITransactionFactory _transactionFactory;

        public TransactionService([Named("TransactionRepository")] ITransactionRepository transactionReporsitory, ITransactionFactory transactionFactory)
        {
            _transactionReporsitory = transactionReporsitory;
            _transactionFactory = transactionFactory;
        }

        /// <summary>
        /// Purpose             :   To Create the transaction
        /// Function Name       :   Create
        /// Created By          :   Salil Gupta
        /// Created On          :   12/23/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="transactionDto"></param>
        /// <returns></returns>
        public int Create(MTDTransactionDto transactionDto)
        {
            try
            {
                MTDTransaction transaction = transactionDto.ToTransaction();
                int result = _transactionReporsitory.Create(transaction);
                return result;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To manage card token
        /// Function Name       :   ManageCardToken
        /// Created By          :   Sunil Singh
        /// Created On          :   06/26/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="ManageCardToken"></param>
        /// <returns></returns>
        public int ManageCardToken(CardTokenDto cardTokenDto)
        {
            try
            {
                CardToken cardToken = cardTokenDto.ToCardToken();
                int result = _transactionReporsitory.ManageCardToken(cardToken);
                return result;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update Transaction
        /// Function Name       :   Update
        /// Created By          :   Salil Gupta
        /// Created On          :   12/25/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="transactionDto"></param>
        /// <param name="transId"></param>
        public void Update(MTDTransactionDto transactionDto, int transId, string rapidConnectAuthId)
        {
            try
            {
                MTDTransaction transaction = transactionDto.ToMtdTransaction();
                _transactionReporsitory.Update(transaction, transId, rapidConnectAuthId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get the Transaction
        /// Function Name       :   GetTransaction
        /// Created By          :   Salil Gupta
        /// Created On          :   12/25/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="transId"></param>
        /// <returns></returns>
        public MTDTransactionDto GetTransaction(int transId)
        {
            try
            {
                MTDTransaction obj = _transactionReporsitory.GetTransaction(transId);
                MTDTransactionDto transactionDto = obj.ToDtoTransaction();
                return transactionDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get the Transaction
        /// Function Name       :   GetTransaction
        /// Created By          :   Salil Gupta
        /// Created On          :   12/25/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="transId"></param>
        /// <returns></returns>
        public MTDTransactionDto GetTransaction(int userType, int uId, int mId, int transId, string project)
        {
            try
            {
                MTDTransaction obj = _transactionReporsitory.GetTransaction(userType, uId, mId, transId, project);
                MTDTransactionDto transactionDto = obj.ToDtoTransaction();
                return transactionDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get the Transaction
        /// Function Name       :   GetTransaction
        /// Created By          :   Salil Gupta
        /// Created On          :   12/25/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="transId"></param>
        /// <param name="mid"></param>
        /// <returns></returns>
        public MTDTransactionDto GetTransaction(int transId, int? mid)
        {
            try
            {
                MTDTransaction obj = _transactionReporsitory.GetTransaction(transId, mid);
                MTDTransactionDto transactionDto = obj.ToDtoTransaction();
                return transactionDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Transactions list
        /// Function Name       :   GetTransaction
        /// Created By          :   Salil Gupta
        /// Created On          :   12/28/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public IEnumerable<MTDTransactionDto> GetTransaction(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToMtdTransaction();
                IEnumerable<usp_SearchTransactionReport_Result> obj = _transactionReporsitory.GetTransaction(trans);
                IEnumerable<MTDTransactionDto> std = obj.ToTransaction();
                return std;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Driver list
        /// Function Name       :   GetDriver
        /// Created By          :   Salil Gupta
        /// Created On          :   12/28/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public IEnumerable<DriverTransactionResult> GetDriver(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToTransaction();
                IEnumerable<usp_GetDriversTransaction_Result> obj1 = _transactionReporsitory.GetDriver(trans);
                IEnumerable<DriverTransactionResult> std = obj1.ToDriverReport();
                return std;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get technology fee list
        /// Function Name       :   GetTechnologySummary
        /// Created By          :   Naveen Kumar
        /// Created On          :   6/10/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public IEnumerable<TechnologySummaryReportDto> GetTechnologySummary(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToTransaction();
                IEnumerable<usp_TechnologySummaryReport_Result> obj1 = _transactionReporsitory.GetTechnologySummary(trans);
                IEnumerable<TechnologySummaryReportDto> std = obj1.ToTechSummaryReport();
                return std;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get technology fee detail list
        /// Function Name       :   GetTechnologyDetail
        /// Created By          :   Naveen Kumar
        /// Created On          :   6/10/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public IEnumerable<TechnologyDetailReportDto> GetTechnologyDetail(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToTransaction();
                IEnumerable<usp_TechnologyDetailReport_Result> obj1 = _transactionReporsitory.GetTechnologyDetail(trans);
                IEnumerable<TechnologyDetailReportDto> std = obj1.ToTechDetailReport();
                return std;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Numberes of Driver
        /// Function Name       :   GetDriverCount
        /// Created By          :   Salil Gupta
        /// Created On          :   12/28/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public int GetDriverCount(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToTransaction();
                int obj1 = _transactionReporsitory.GetDriverCount(trans);
                return obj1;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Numberes of Transaction
        /// Function Name       :   GetTransactionCount
        /// Created By          :   Salil Gupta
        /// Created On          :   12/28/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public int GetTransactionCount(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToTransaction();
                int obj = _transactionReporsitory.GetTransactionCount(trans);
                return obj;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Numberes of tech fee records
        /// Function Name       :   GetTechFeeCount
        /// Created By          :   Naveen Kumar
        /// Created On          :   10/06/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public int GetTechFeeCount(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToTransaction();
                int obj1 = _transactionReporsitory.GetTechFeeCount(trans);
                return obj1;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get numbers of tech fee detail records
        /// Function Name       :   GetTechFeeDetailCount
        /// Created By          :   Naveen Kumar
        /// Created On          :   10/06/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public int GetTechFeeDetailCount(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToTransaction();
                int obj1 = _transactionReporsitory.GetTechFeeDetailCount(trans);
                return obj1;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To check Driver Vhecile Exist
        /// Function Name       :   DriverVhecileExist
        /// Created By          :   Salil Gupta
        /// Created On          :   12/30/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverNo"></param>
        /// <param name="vehicleNo"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public bool DriverVhecileExist(string driverNo, string vehicleNo, int merchantId)
        {
            return _transactionReporsitory.DriverVhecileExist(driverNo, vehicleNo, merchantId);
        }

        /// <summary>
        /// Purpose             :   To get the Merchant Id
        /// Function Name       :   GetMerchantId
        /// Created By          :   Salil Gupta
        /// Created On          :   12/30/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverNo"></param>
        /// <param name="vehicleNo"></param>
        /// <returns></returns>
        public int GetMerchantId(string driverNo, string vehicleNo)
        {
            return _transactionReporsitory.GetMerchantId(driverNo, vehicleNo);
        }

        /// <summary>
        /// Purpose             :   To Add the Transaction
        /// Function Name       :   Add
        /// Created By          :   Salil Gupta
        /// Created On          :   12/30/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="transactionDto"></param>
        /// <returns></returns>
        public int Add(MTDTransactionDto transactionDto)
        {
            try
            {
                MTDTransaction transaction = transactionDto.ToTransaction();
                int result = _transactionReporsitory.Add(transaction);
                return result;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update the existing Transaction
        /// Function Name       :   Update
        /// Created By          :   Salil Gupta
        /// Created On          :   12/31/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="transactionDto"></param>
        public void Update(MTDTransactionDto transactionDto)
        {
            try
            {
                MTDTransaction transaction = transactionDto.ToTransaction();
                _transactionReporsitory.Update(transaction);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vehicleNumber"></param>
        /// <returns></returns>
        public string GetDriverDevice(string vehicleNumber, int fleetId)
        {
            return _transactionReporsitory.GetDriverDevice(vehicleNumber, fleetId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public TxnTaxSurchargesDto GetTaxSurchages(int merchantId)
        {
            TxnTaxSurcharges tTax = _transactionReporsitory.GetTaxSurchages(merchantId);
            TxnTaxSurchargesDto txnTax = tTax.ToTxnSurchages();
            return txnTax;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public TxnTaxSurchargesDto GetTaxSurchages()
        {
            TxnTaxSurcharges tTax = _transactionReporsitory.GetTaxSurchages();
            TxnTaxSurchargesDto txnTax = tTax.ToTxnSurchages();
            return txnTax;
        }

        /// <summary>
        /// Purpose             :   To Get Tax Surchages
        /// Function Name       :   GetTaxSurchages
        /// Created By          :   Umesh Kumar
        /// Created On          :   06/10/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="cardNumber"></param>
        /// <param name="vehicleNumber"></param>
        public TxnTaxSurchargesDto GetTaxSurchages(int merchantId, int vehicleNumber)
        {
            TxnTaxSurcharges tTax = _transactionReporsitory.GetTaxSurchages(merchantId, vehicleNumber);
            TxnTaxSurchargesDto txnTax = tTax.ToTxnSurchages();
            return txnTax;
        }

        /// <summary>
        /// Purpose             :   To Get Drivers
        /// Function Name       :   GetDrivers
        /// Created By          :   Asif Saffiq
        /// Created On          :   06/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        public IEnumerable<DriverTxnListDto> GetDrivers(int merchantId)
        {
            try
            {
                IEnumerable<usp_GetDrivers_Result> obj1 = _transactionReporsitory.GetDrivers(merchantId);
                return obj1.Select(driver => new DriverTxnListDto
                {
                    DriverText = driver.DriverNo,
                    DriverValue = driver.DriverNo
                }).ToList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Drivers
        /// Function Name       :   GetDrivers
        /// Created By          :   Asif Saffiq
        /// Created On          :   06/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        public IEnumerable<VehicleTxnListDto> GetVehicle(int merchantId)
        {
            try
            {
                IEnumerable<usp_GetVehicles_Result> obj1 = _transactionReporsitory.GetVehicle(merchantId);
                return obj1.Select(vehicle => new VehicleTxnListDto
                {
                    VehicleText = vehicle.VehicleNumber,
                    FleetId = vehicle.fk_FleetID
                }).ToList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cardToken"></param>
        /// <returns></returns>
		/// POD - Cannot get card details without customerid..
        //public string GetCardTypeByToken(string cardToken)
        //{
        //    return _transactionReporsitory.GetCardTypeByToken(cardToken);
        //}

        /// <summary>
        /// Purpose             :   To Payroll SAGE list
        /// Function Name       :   GetPayrollSAGE
        /// Created By          :   Shishir Saunakia
        /// Created On          :   09/28/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public IEnumerable<PayrollRportSageResult> GetpayrollSage(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToTransaction();
                IEnumerable<usp_GetPayrollSAGETransaction_Result> obj1 = _transactionReporsitory.GetpayrollSage(trans);
                IEnumerable<PayrollRportSageResult> std = obj1.ToPayrollSageReport();
                return std;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Numberes of PayrollSage
        /// Function Name       :   GetPayrollSageCount
        /// Created By          :   Shishir Saunakia
        /// Created On          :   10/06/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public int GetPayrollSageCount(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToTransaction();
                int obj1 = _transactionReporsitory.GetPayrollSageCount(trans);
                return obj1;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Payroll Payment list
        /// Function Name       :   GetpayrollPayment
        /// Created By          :   Shishir Saunakia
        /// Created On          :   09/28/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public IEnumerable<PayrollRportPaymentResult> GetpayrollPayment(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToTransaction();
                IEnumerable<usp_GetPayrollPaymentTransaction_Result> obj1 = _transactionReporsitory.GetpayrollPayment(trans);
                IEnumerable<PayrollRportPaymentResult> std = obj1.ToPayrollPaymentReport();
                return std;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Numberes of PayrollSage
        /// Function Name       :   GetPayrollSageCount
        /// Created By          :   Shishir Saunakia
        /// Created On          :   10/06/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="tDto"></param>
        /// <returns></returns>
        public int GetPayrollPaymentCount(MTDTransactionDto tDto)
        {
            try
            {
                MTDTransaction trans = tDto.ToTransaction();
                int obj1 = _transactionReporsitory.GetPayrollPaymentCount(trans);
                return obj1;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="txnId"></param>
        /// <param name="isVerified"></param>
        /// <returns></returns>
        public bool Varified(int txnId, bool isVerified)
        {
            return _transactionReporsitory.Varified(txnId, isVerified);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="manyTxnDto"></param>
        /// <returns></returns>
        public bool VarifiedMany(ManyTxnClassDto[] manyTxnDto)
        {
            ManyTxnClass[] manyTxn = manyTxnDto.ToVarifiedMany();
            return _transactionReporsitory.VarifiedMany(manyTxn);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public FleetDto GetFleetTechFee(int fleetId)
        {
            var fleet = _transactionReporsitory.GetFleetTechFee(fleetId);
            FleetDto fDto = fleet.ToDtoFleet();
            return fDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dParamDto"></param>
        /// <returns></returns>
        public IEnumerable<DownloadDriverDataDto> DownloadTransactionData(DownloadParametersDto dParamDto)
        {
            DownloadParameters dParam = dParamDto.ToDownloadDriver();
            IEnumerable<DownloadDriverDataDto> downloadList = _transactionReporsitory.DownloadTransactionData(dParam).ToDTOList();
            return downloadList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uId"></param>
        /// <param name="pCode"></param>
        /// <param name="mCode"></param>
        /// <returns></returns>
        public int ValidateDownloadRequest(string uId, string pCode, int mCode)
        {
            return _transactionReporsitory.ValidateDownloadRequest(uId, pCode, mCode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public bool JobNumberRequired(int merchantId)
        {
            return _transactionReporsitory.JobNumberRequired(merchantId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="cardToken"></param>
        /// <returns></returns>
        public CardTokenDto GetCardToken(string customerId, string cardToken)
        {
            try
            {
                var crdTkn = _transactionReporsitory.GetCardToken(customerId, cardToken);
                CardTokenDto crdTknDto = crdTkn.ToCardToken();
                return crdTknDto;
            }
            catch
            {
                throw;
            }
        }

        //public CardTokenDto GetCardToken(string cardToken)
        //{
        //    try
        //    {
        //        var crdTkn = _transactionReporsitory.GetCardToken(cardToken);
        //        CardTokenDto crdTknDto = crdTkn.ToCardToken();
        //        return crdTknDto;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="capK"></param>
        /// <returns></returns>
        public int SaveCapKey(CapKeyDto capK)
        {
            try
            {
                CapKey cpK = capK.ToCapKey();
                int result = _transactionReporsitory.SaveCapKey(cpK);
                return result;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public CapKeyDto GetCapKey()
        {
            try
            {
                var capk = _transactionReporsitory.GetCapKey();
                CapKeyDto capkDTO = capk.ToCapKeyDto();
                return capkDTO;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public int GetMerchantId(int fleetId)
        {
            return _transactionReporsitory.GetMerchantId(fleetId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public IEnumerable<GetAdminSummaryReportDto> GetadminSummary(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo)
        {
            try
            {
                IEnumerable<usp_GetAdminSummaryReport_Result> dbData = _transactionReporsitory.GetadminSummary(dateFrom, dateTo);
                IEnumerable<GetAdminSummaryReportDto> adminSummaryReport = dbData.ToAdminSummaryList();
                return adminSummaryReport;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public IEnumerable<GetCardTypeReportDto> AdminCardypeReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo)
        {
            try
            {
                IEnumerable<usp_GetCardTypeReport_Result> dbData = _transactionReporsitory.AdminCardypeReport(dateFrom, dateTo);
                IEnumerable<GetCardTypeReportDto> cardDto = dbData.ToCardTypeList();
                return cardDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public IEnumerable<GetAdminExceptionReportDto> AdminExceptionReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo)
        {
            try
            {
                IEnumerable<usp_GetAdminExceptionReport_Result> dbData = _transactionReporsitory.AdminExceptionReport(dateFrom, dateTo);
                IEnumerable<GetAdminExceptionReportDto> adminSummaryReport = dbData.ToAdminExceptionList();
                return adminSummaryReport;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public IEnumerable<GetTotalTransByVehicleReportDto> TransByVehicleReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo)
        {
            IEnumerable<usp_GetTotalTransByVehicleReport_Result> dbData = _transactionReporsitory.TransByVehicleReport(dateFrom, dateTo);
            IEnumerable<GetTotalTransByVehicleReportDto> cardDto = dbData.ToTransByVehicleList();
            return cardDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="mId"></param>
        /// <returns></returns>
        public IEnumerable<GetMerchantSummaryReportDto> MerchantSummaryReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int mId)
        {
            try
            {
                IEnumerable<usp_GetMerchantSummaryReport_Result> dbData = _transactionReporsitory.MerchantSummaryReport(dateFrom, dateTo, mId);
                IEnumerable<GetMerchantSummaryReportDto> adminSummaryReport = dbData.ToMerchantSummaryList();
                return adminSummaryReport;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="mId"></param>
        /// <returns></returns>
        public IEnumerable<GetDetailReportDto> GetDetailsReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int mId)
        {
            try
            {
                IEnumerable<usp_GetDetailReport_Result> dbData = _transactionReporsitory.GetDetailsReport(dateFrom, dateTo, mId);
                IEnumerable<GetDetailReportDto> adminSummaryReport = dbData.ToDetailsList();
                return adminSummaryReport;
            }
            catch
            {
                throw;
            }
        }
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="mId"></param>
        /// <returns></returns>
        public IEnumerable<GetMerchantExceptionReportDto> GetMerchantExceptionReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int mId)
        {
            try
            {
                IEnumerable<usp_GetMerchantExceptionReport_Result> dbData = _transactionReporsitory.GetMerchantExceptionReport(dateFrom, dateTo, mId);
                IEnumerable<GetMerchantExceptionReportDto> adminSummaryReport = dbData.ToMerchantExecptionReportList();
                return adminSummaryReport;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="mId"></param>
        /// <returns></returns>
        public IEnumerable<GetMerchantExceptionReportDto> GetMerchantUserExceptionReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int? muId)
        {
            try
            {
                IEnumerable<usp_GetMerchantUserExceptionReport_Result> dbData = _transactionReporsitory.GetMerchantUserExceptionReport(dateFrom, dateTo, muId);
                IEnumerable<GetMerchantExceptionReportDto> adminSummaryReport = dbData.ToMerchantExecptionReportList();
                return adminSummaryReport;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="mId"></param>
        /// <returns></returns>
        public IEnumerable<GetMerchantTotalTransByVehicleReportDto> GetMerchantTotalTransByVehicle(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int mId)
        {
            try
            {
                IEnumerable<usp_GetMerchantTotalTransByVehicleReport_Result> dbData = _transactionReporsitory.GetMerchantTotalTransByVehicle(dateFrom, dateTo, mId);
                IEnumerable<GetMerchantTotalTransByVehicleReportDto> adminSummaryReport = dbData.ToMerchantVehicleList();
                return adminSummaryReport;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="mId"></param>
        /// <returns></returns>
        public IEnumerable<GetMerchantCardTypeReportDto> GetMerchantCardTypeReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int mId)
        {
            try
            {
                IEnumerable<usp_GetMerchantCardTypeReport_Result> dbData = _transactionReporsitory.GetMerchantCardTypeReport(dateFrom, dateTo, mId);
                IEnumerable<GetMerchantCardTypeReportDto> cardDto = dbData.ToCardTypeListMerchant();
                return cardDto;
            }
            catch
            {
                throw;
            }
        }

        //On Demand Merchat User Section
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="mId"></param>
        /// <param name="merchantUserId"></param>
        /// <returns></returns>
        public IEnumerable<GetCardTypeReportDto> GetMerchantUserCardTypeReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int? mId, int? merchantUserId)
        {
            IEnumerable<usp_GetMerchantUserCardTypeReport_Result> dbaData = _transactionReporsitory.GetMerchantUserCardTypeReport(dateFrom, dateTo, mId, merchantUserId);
            IEnumerable<GetCardTypeReportDto> cardDto = dbaData.ToMuCardTypeList();
            return cardDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="mId"></param>
        /// <param name="merchantUserId"></param>
        /// <returns></returns>
        public IEnumerable<GetDetailReportDto> GetMerchantUserDetailReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int? mId, int? merchantUserId)
        {
            try
            {
                IEnumerable<usp_GetMerchantUserDetailReport_Result> dbData = _transactionReporsitory.GetMerchantUserDetailReport(dateFrom, dateTo, mId, merchantUserId);
                IEnumerable<GetDetailReportDto> adminSummaryReport = dbData.TomuDetailsList();
                return adminSummaryReport;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="mId"></param>
        /// <param name="merchantUserId"></param>
        /// <returns></returns>
        public IEnumerable<GetMerchantSummaryReportDto> GetMerchantUserSummaryReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int? mId, int? merchantUserId)
        {
            try
            {
                IEnumerable<usp_GetMerchantUserSummaryReport_Result> dbData = _transactionReporsitory.GetMerchantUserSummaryReport(dateFrom, dateTo, mId, merchantUserId);
                IEnumerable<GetMerchantSummaryReportDto> adminSummaryReport = dbData.ToMerchantUserSummaryList();
                return adminSummaryReport;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="mId"></param>
        /// <param name="merchantUserId"></param>
        /// <returns></returns>
        public IEnumerable<GetMerchantTotalTransByVehicleReportDto> GetMerchantUserTotalTransByVehicleReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int? mId, int? merchantUserId)
        {
            try
            {
                IEnumerable<usp_GetMerchantUserTotalTransByVehicleReport_Result> dbtData = _transactionReporsitory.GetMerchantUserTotalTransByVehicleReport(dateFrom, dateTo, mId, merchantUserId);
                IEnumerable<GetMerchantTotalTransByVehicleReportDto> adminSummaryReport = dbtData.ToMerchantUserVehicleList();
                return adminSummaryReport;
            }
            catch
            {
                throw;
            }
        }

        //On Demand Merchat Corporate Section
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="corpUserId"></param>
        /// <returns></returns>
        public IEnumerable<GetTotalTransByVehicleReportDto> CorpUserGetTotalTransByVehicleReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int? corpUserId)
        {
            try
            {
                IEnumerable<usp_CorpUserGetTotalTransByVehicleReport_Result> dbtData = _transactionReporsitory.CorpUserGetTotalTransByVehicleReport(dateFrom, dateTo, corpUserId);
                IEnumerable<GetTotalTransByVehicleReportDto> corpSummaryReport = dbtData.ToCorpUserVehicleList();
                return corpSummaryReport;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="corpUserId"></param>
        /// <returns></returns>
        public IEnumerable<GetCardTypeReportDto> GetCorpUserCardTypeReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int? corpUserId)
        {
            IEnumerable<usp_GetCorpUserCardTypeReport_Result> dbaData = _transactionReporsitory.GetCorpUserCardTypeReport(dateFrom, dateTo, corpUserId);
            IEnumerable<GetCardTypeReportDto> corpCardDto = dbaData.ToCorpCardTypeList();
            return corpCardDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="cUserId"></param>
        /// <returns></returns>
        public IEnumerable<GetMerchantExceptionReportDto> GetCorporateExceptionReport(Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int cUserId)
        {
            try
            {
                IEnumerable<usp_GetCorpUserExceptionReport_Result> dbData = _transactionReporsitory.GetCorporateExceptionReport(dateFrom, dateTo, cUserId);
                IEnumerable<GetMerchantExceptionReportDto> adminSummaryReport = dbData.ToCorpExecptionReportList();
                return adminSummaryReport;
            }
            catch
            {
                throw;
            }
        }

        //Reprot run Admin
        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRun"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public int AdminExceptionReportScheduler(bool? isRun, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo)
        {
            try
            {
                int exceptionReport = _transactionReporsitory.AdminExceptionReportScheduler(isRun, dateFrom, dateTo);
                return exceptionReport;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRun"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public int TotalTransByVehicleScheduler(bool? isRun, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo)
        {
            try
            {
                int vehicleScheduler = _transactionReporsitory.TotalTransByVehicleScheduler(isRun, dateFrom, dateTo);
                return vehicleScheduler;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRun"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public int CardTypeReportScheduler(bool? isRun, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo)
        {
            try
            {
                int reportScheduler = _transactionReporsitory.CardTypeReportScheduler(isRun, dateFrom, dateTo);
                return reportScheduler;
            }
            catch
            {
                throw;
            }
        }   

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRun"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public int AdminSummaryReportScheduler(bool? isRun, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo)
        {
            try
            {
                int reportScheduler = _transactionReporsitory.AdminSummaryReportScheduler(isRun, dateFrom, dateTo);
                return reportScheduler;
            }
            catch
            {
                throw;
            }
        }

        //Report Run Merchnat
        /// <summary>
        /// Report Run Merchnat
        /// </summary>
        /// <param name="isRun"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public int MerchantSummaryReportScheduler(bool? isRun, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int? merchantId)
        {
            try
            {
                int getMerchantTotal = _transactionReporsitory.MerchantSummaryReportScheduler(isRun, dateFrom, dateTo, merchantId);
                return getMerchantTotal;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRun"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public int MerchantTotalTransByVehicleScheduler(bool? isRun, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int? merchantId)
       {
           try
           {
                int getMerchantTotal = _transactionReporsitory.MerchantTotalTransByVehicleScheduler(isRun, dateFrom, dateTo, merchantId);
               return getMerchantTotal;
           }
           catch
           {
               throw;
           }
       }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRun"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public int MerchantExceptionReportScheduler(bool? isRun, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo)
        {
            try
            {
                int getMerchantTotal = _transactionReporsitory.MerchantExceptionReportScheduler(isRun, dateFrom, dateTo);
                return getMerchantTotal;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRun"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public int DetailReportScheduler(bool? isRun, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int? merchantId)
        {
            try
            {
                int getMerchantTotal = _transactionReporsitory.DetailReportScheduler(isRun, dateFrom, dateTo, merchantId);
                return getMerchantTotal;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRun"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public int MerchantCardTypeReportScheduler(bool? isRun, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, int? merchantId)
        {
            try
            {
                int getMerchantTotal = _transactionReporsitory.MerchantCardTypeReportScheduler(isRun, dateFrom, dateTo, merchantId);
                return getMerchantTotal;
            }
            catch
            {
                throw;
            }
        }

        //Merchant User run report
        /// <summary>
        /// 
        /// </summary>
        /// <param name="isOnDemandReport"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="parentMerchantId"></param>
        /// <param name="merchantUserId"></param>
        /// <returns></returns>
        public int MerchantUserCardTypeReportScheduler(bool? isOnDemandReport, DateTime? fromDate, DateTime? toDate, int? parentMerchantId, int? merchantUserId)
        {
            try
            {
                int result = _transactionReporsitory.MerchantUserCardTypeReportScheduler(isOnDemandReport, fromDate, toDate, parentMerchantId, merchantUserId);
                return result;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isOnDemandReport"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="parentMerchantId"></param>
        /// <param name="merchantUserId"></param>
        /// <returns></returns>
        public int MerchantUserDetailReportScheduler(bool? isOnDemandReport, DateTime? fromDate, DateTime? toDate, int? parentMerchantId, int? merchantUserId)
        {
            try
            {
                int result = _transactionReporsitory.MerchantUserDetailReportScheduler(isOnDemandReport, fromDate, toDate, parentMerchantId, merchantUserId);
                return result;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isOnDemandReport"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="parentMerchantId"></param>
        /// <param name="merchantUserId"></param>
        /// <returns></returns>
        public int MerchantUserTotalTransByVehicleScheduler(bool? isOnDemandReport, DateTime? fromDate, DateTime? toDate, int? parentMerchantId, int? merchantUserId)
        {
            try
            {
                int result = _transactionReporsitory.MerchantUserTotalTransByVehicleScheduler(isOnDemandReport, fromDate, toDate, parentMerchantId, merchantUserId);
                return result;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isOnDemandReport"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="parentMerchantId"></param>
        /// <param name="merchantUserId"></param>
        /// <returns></returns>
        public int MerchantUserSummaryReportScheduler(bool? isOnDemandReport, DateTime? fromDate, DateTime? toDate, int? parentMerchantId, int? merchantUserId)
        {
            try
            {
                int result = _transactionReporsitory.MerchantUserSummaryReportScheduler(isOnDemandReport, fromDate, toDate, parentMerchantId, merchantUserId);
                return result;
            }
            catch
            {
                throw;
            }
        }

        public int MerchantUserExceptionReportScheduler(bool? isOnDemandReport, DateTime? fromDate, DateTime? toDate, int? merchantUserId)
        {
            try
            {
                int result = _transactionReporsitory.MerchantUserExceptionReportScheduler(isOnDemandReport, fromDate, toDate, merchantUserId);
                return result;
            }
            catch
            {
                throw;
            }
        }

        //Corporate RUN
        public int CorpUserTotalTransByVehicleScheduler(bool? isOnDemandReport, DateTime? fromDate, DateTime? toDate, int? cUserId)
        {
            try
            {
                int result = _transactionReporsitory.CorpUserTotalTransByVehicleScheduler(isOnDemandReport, fromDate, toDate, cUserId);
                return result;
            }
            catch
            {
                throw;
            }
        }

        public int CorpUserCardTypeReportScheduler(bool? isOnDemandReport, DateTime? fromDate, DateTime? toDate, int? cUserId)
        {
            try
            {
                int result = _transactionReporsitory.CorpUserCardTypeReportScheduler(isOnDemandReport, fromDate, toDate, cUserId);
                return result;
            }
            catch
            {
                throw;
            }
        }

        public int CorpUserExceptionReportScheduler(bool? isOnDemandReport, DateTime? fromDate, DateTime? toDate, int? cUserId)
        {
            try
            {
                int result = _transactionReporsitory.CorpUserExceptionReportScheduler(isOnDemandReport, fromDate, toDate, cUserId);
                return result;
            }
            catch
            {
                throw;
            }
        }

        public void CnUpdate(MTDTransactionDto mtdTransDto, int transId)
        {
            MTDTransaction transaction = mtdTransDto.ToMtdTransaction();
            _transactionReporsitory.CnUpdate(transaction, transId);
        }

        public string GetTransactionUrl()
        {
            return _transactionReporsitory.GetTransactionUrl();
        }

        public string GetRefNumber()
        {
            return _transactionReporsitory.GetRefNumber();
        }

        public string GetFastestUrl(string failOverUrls, List<string> urlList)
        {
            return _transactionReporsitory.GetFastestUrl(failOverUrls, urlList);
        }

        /// <summary>
        /// Get transaction details based on stan no.
        /// </summary>
        /// <param name="mid"></param>
        /// <param name="tid"></param>
        /// <param name="stan"></param>
        /// <returns></returns>
        public MTDTransactionDto GetTransaction(int? mid, string tid, string stan)
        {
            try
            {
                MTDTransaction obj = _transactionReporsitory.GetTransaction(mid, tid, stan);
                MTDTransactionDto transactionDto = obj.ToDtoTransaction();
                return transactionDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get transaction id based on request id
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public string GetTransactionId(string requestId)
        {
            try
            {
                return _transactionReporsitory.GetTransactionId(requestId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// To get transaction to decide mac verification failed or not
        /// </summary>
        /// <param name="transId"></param>
        /// <returns></returns>
        public bool IsMacVerificationFailed(string transId)
        {
            try
            {
                return _transactionReporsitory.IsMacVerificationFailed(transId);
            }
            catch
            {
                throw;
            }
        }

    }
}