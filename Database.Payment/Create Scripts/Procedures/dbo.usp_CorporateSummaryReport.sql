IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CorporateSummaryReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_CorporateSummaryReport]
GO

CREATE PROCEDURE dbo.usp_CorporateSummaryReport
-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <22-02-2016>
-- Description:	<Prepare summary reports for corporate users>
-- =============================================
(
	@UserId INT,
	@IsSuperCorporate BIT,
	@XmlData NVARCHAR(MAX) OUT,
	@ReportType INT OUT,
	@FDate DATE OUT,
	@TDate DATE OUT,
	@TotalTransaction INT OUT,
	@TotalActualFare NVARCHAR(30) OUT,
	@TotalTips NVARCHAR(30) OUT,
	@TotalFareAmount NVARCHAR(30) OUT,
	@TotalSurcharge NVARCHAR(30) OUT,
	@TotalFee NVARCHAR(30) OUT,
	@NoChargeBack INT OUT,
	@ChargeBackValue NVARCHAR(30) OUT,
	@NoVoid INT OUT,
	@VoidValue NVARCHAR(30) OUT,
	@TotalTechFee NVARCHAR(35) OUT,
	@AmountOwing  NVARCHAR(30) OUT,
	@NextSchedule DATETIME OUT
)
AS
BEGIN
	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SET NOCOUNT ON
		SET ANSI_WARNINGS OFF

	   DECLARE @Frequency INT,
			   @FrequencyType INT,
			   @DateFrom DATE,
			   @DateTo DATE,
			   @ScheduleId INT,
			   @NextScheduleDateTime DATETIME,
			   @ParentCorporateId INT

	  DECLARE @InnerTable TABLE (
		 Id INT, 
		 FareValue MONEY,
		 Tip MONEY, 
		 Amount MONEY, 
		 Surcharge MONEY, 
		 Fee MONEY, 
		 FleetID INT, 
		 TxnType VARCHAR(15),
		 ResponseCode VARCHAR(25),
		 fk_FleetId INT,
		 IsRefunded BIT,
		 IsVoided BIT,
		 IsCompleted BIT,
		 TechFee MONEY,
		 FleetFee MONEY
		)

	  DECLARE @OuterTable TABLE (
		  Id  INT, 
		  FareValue MONEY, 
		  Tip MONEY,Amount MONEY,
		  Surcharge MONEY, 
		  Fee MONEY, 
		  TxnType VARCHAR(15),
		  ResponseCode NVARCHAR(25),
		  fk_FleetId INT,
		  TechFee MONEY,
		  IsRefunded BIT,
		  IsVoided BIT,
		  IsCompleted BIT,
		  FleetFee MONEY,
		  MerchantId INT
			)

		DECLARE @CorporateTable TABLE (FleetId INT, MerchantId INT)

		IF (@IsSuperCorporate = 1)
		BEGIN
			SELECT	@ScheduleId = ScheduleID,
					@Frequency = Frequency,
					@FrequencyType = FrequencyType,
					@NextScheduleDateTime = NextScheduleDateTime 
			FROM dbo.ScheduleReport 
			WHERE fk_ReportId = 1 
			  AND IsCreated = 1 
			  AND CorporateUserId = @UserId	  
		END
		ELSE
		BEGIN
			SELECT @ParentCorporateId = ParentUserID FROM dbo.Users WHERE UserID =@UserId AND IsActive = 1

			SELECT	@ScheduleId = ScheduleID,
					@Frequency = Frequency,
					@FrequencyType = FrequencyType,
					@NextScheduleDateTime = NextScheduleDateTime 
		   FROM dbo.ScheduleReport 
		   WHERE fk_ReportId = 1
		     AND IsCreated = 1 
			 AND CorporateUserId = @ParentCorporateId	  
		END

		INSERT INTO @CorporateTable
		SELECT fk_FleetID, fk_MerchantId FROM dbo.UserFleet WHERE fk_UserTypeId = 5 AND IsActive = 1 AND fk_UserID = @UserId

		IF (@Frequency = 1)
		BEGIN
			SET @DateFrom = GETDATE() - @FrequencyType
			SET @DateTo = GETDATE()
			SET @NextScheduleDateTime = @NextScheduleDateTime + @FrequencyType
		END
		ELSE IF (@Frequency = 2)
		BEGIN
			SET @DateFrom = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK, -@FrequencyType, GETDATE())), 0) - 1
			SET @DateTo = DATEADD(WEEK, DATEDIFF(WEEK, 0, GETDATE()), 0) - 1
			SET @NextScheduleDateTime = DATEADD(WK, @FrequencyType, @NextScheduleDateTime)
		END
		ELSE IF (@Frequency = 3)
		BEGIN
			SET @DateFrom = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH, -@FrequencyType, GETDATE())), 0)
			SET @DateTo = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) 
			SET @NextScheduleDateTime = DATEADD(MONTH, @FrequencyType, @NextScheduleDateTime)
		END   
		ELSE
		BEGIN
			RETURN 3
		END

		--insert data into table variable InnerTable for reuse*/                         
		INSERT INTO @InnerTable(Id, FareValue, Tip, Amount, Surcharge, Fee, FleetID, TxnType, ResponseCode, IsRefunded, IsVoided, IsCompleted, TechFee, FleetFee)                   
		SELECT	mt.Id,
				mt.FareValue,
				mt.Tip,
				mt.Amount,
				mt.Surcharge,
				mt.Fee,
				fl.FleetID,
				mt.TxnType,
				mt.ResponseCode,
				mt.IsRefunded,
				mt.IsVoided,
				mt.IsCompleted,
				mt.TechFee,
				mt.FleetFee		
		FROM dbo.MTDTransaction mt 
			INNER JOIN Vehicle vh ON mt.VehicleNo=vh.VehicleNumber 
			INNER JOIN Fleet fl ON vh.fk_FleetID = fl.FleetID
		WHERE CAST(mt.TxnDate AS DATE) >= @DateFrom 
		  AND CAST(mt.TxnDate AS DATE) < @DateTo 
		  AND mt.fk_FleetId = vh.fk_FleetID

		--insert data into table variable OuterTable for reuse*/    
		INSERT INTO @OuterTable(Id, FareValue, Tip, Amount, Surcharge, Fee, TxnType, ResponseCode, fk_FleetId, TechFee, IsRefunded, IsVoided, IsCompleted, FleetFee, MerchantId) 
		SELECT  t.Id,
				t.FareValue,
				t.Tip,t.Amount,
				t.Surcharge,
				t.Fee,
				t.TxnType,
				t.ResponseCode,
				t.fk_FleetId,
				t.TechFee,
				t.IsRefunded,
				t.IsVoided,
				t.IsCompleted,
				t.FleetFee,
				t.MerchantId
		FROM dbo.MTDTransaction t 
		WHERE CAST(t.TxnDate AS DATE) >= @DateFrom 
		  AND CAST(t.TxnDate AS DATE) < @DateTo

		SET @XmlData = CAST((SELECT  ROW_NUMBER() OVER (ORDER BY Company) AS 'td', '',
				ISNULL(mer.Company, '') AS 'td', '',
				ISNULL(f.FleetName, '')  AS 'td', '',
				(SELECT ISNULL(COUNT(id),0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND it.TxnType IN ('Sale', 'Completion') AND it.IsRefunded = 0 AND it.IsVoided = 0 AND it.ResponseCode IN ('000', '00', '002')) AS 'td', '',
				CONCAT('$',(SELECT ISNULL(SUM(FareValue), '0.00') FROM @InnerTable it WHERE it.FleetID = f.FleetID  AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td', '',
				CONCAT('$',(SELECT ISNULL(SUM(Tip), '0.00') FROM @InnerTable it WHERE it.FleetID = f.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND  it.ResponseCode IN ('000', '00', '002'))) AS 'td', '',
				CONCAT('$',(SELECT ISNULL(SUM(Surcharge), '0.00') FROM @InnerTable it WHERE it.FleetID = f.FleetID  AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td', '',
				CONCAT('$',(SELECT ISNULL(SUM(TechFee), '0.00') FROM @InnerTable it  WHERE it.FleetID = f.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td', '',
				(SELECT ISNULL(COUNT(id),0) FROM @InnerTable it  WHERE it.FleetID = f.FleetID AND it.TxnType = 'Refund'AND it.ResponseCode IN ('000', '00')) AS 'td', '',
				CONCAT('$',(SELECT ISNULL(SUM(Amount), '0.00') FROM @InnerTable it WHERE it.FleetID = f.FleetID AND it.TxnType = 'Refund'AND it.ResponseCode IN ('000', '00'))) AS 'td', '',
				(SELECT ISNULL(COUNT(id),0) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND it.TxnType = 'Void'AND it.ResponseCode IN ('000', '00')) AS 'td', '',
				CONCAT('$',(SELECT ISNULL(SUM(Amount), '0.00') FROM @InnerTable it WHERE it.FleetID = f.FleetID AND it.TxnType = 'Void'AND it.ResponseCode IN ('000', '00'))) AS 'td', '',
				CONCAT('$',(SELECT ISNULL(SUM(Amount), '0.00') FROM @InnerTable it WHERE it.FleetID = f.FleetID AND it.TxnType IN ('Sale', 'Completion') AND it.IsRefunded = 0 AND it.IsVoided = 0 AND it.ResponseCode IN ('000', '00', '002'))) AS 'td', '',
				CONCAT('$',(SELECT ISNULL(SUM(it.Fee), '0.00')+ISNULL(SUM(it.FleetFee), '0.00') FROM @InnerTable it WHERE it.FleetID = f.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td', '',
				CONCAT('$',(SELECT ISNULL(SUM(Amount),0.00) FROM @InnerTable it WHERE it.FleetID = f.FleetID AND it.TxnType IN ('Sale', 'Completion') AND it.IsRefunded = 0 AND IT.IsVoided = 0 AND it.ResponseCode IN ('000', '00', '002')) -
					(SELECT ISNULL(SUM(Fee), '0.00') + ISNULL(SUM(FleetFee), '0.00') FROM @InnerTable it WHERE it.FleetID = f.FleetID AND ((it.TxnType = 'Sale' AND it.IsRefunded = 0 AND it.IsVoided = 0) OR (it.TxnType = 'Authorization' AND it.IsCompleted = 1)) AND it.ResponseCode IN ('000', '00', '002'))) AS 'td'
		FROM dbo.MTDTransaction m 
			INNER JOIN dbo.Vehicle v ON m.VehicleNo = v.VehicleNumber 
			INNER JOIN dbo.Fleet f ON v.fk_FleetID = f.FleetID 
			INNER JOIN dbo.Merchant mer ON f.fk_MerchantID = mer.MerchantID 
		WHERE m.ResponseCode IN ('000', '00', '002')   
		  AND CAST(m.TxnDate AS DATE) >= @DateFrom
		  AND CAST(m.TxnDate AS DATE) < @DateTo    
		  AND m.fk_FleetId = v.fk_FleetID
		  AND m.Id NOT IN (SELECT Id FROM dbo.MTDTransaction WHERE m.TxnType = 'Authorization' AND m.IsCompleted = 0)
		  AND mer.MerchantID IN (SELECT DISTINCT cb.MerchantId FROM @CorporateTable cb)
		  AND f.FleetID IN (SELECT DISTINCT ct.FleetId FROM @CorporateTable ct)
		GROUP BY f.FleetName, f.FleetID, mer.Company 
		ORDER BY mer.Company 
		FOR XML PATH('tr'), ELEMENTS ) AS NVARCHAR(MAX))

		SELECT @TotalTransaction = ISNULL(COUNT(Id), 0)
		FROM @OuterTable ot 
		WHERE ot.TxnType IN ('Sale', 'Completion') 
		  AND ot.ResponseCode IN ('000', '00', '002')
		  AND ot.IsRefunded = 0 
		  AND ot.IsVoided = 0
		  AND ot.fk_FleetId IN (SELECT DISTINCT ct.FleetId FROM @CorporateTable ct)
		  AND ot.MerchantId IN (SELECT DISTINCT cl.MerchantId FROM @CorporateTable cl)

		SELECT	@TotalActualFare = CONCAT('$', CAST(ISNULL(SUM(ot.FareValue), '0.00') AS DECIMAL(18, 2))),
				@TotalTips = CONCAT('$', CAST(ISNULL(SUM(ot.Tip), '0.00') AS DECIMAL(18, 2))),
				@TotalSurcharge = CONCAT('$', CAST(ISNULL(SUM(ot.Surcharge), '0.00') AS DECIMAL(18, 2))),
				@TotalFee = CAST(ISNULL(SUM(ot.Fee), '0.00') + ISNULL(SUM(ot.FleetFee), '0.00') AS DECIMAL(18, 2)),
				@TotalTechFee = CAST(ISNULL(SUM(ot.TechFee), '0.00') AS DECIMAL(16, 2))	
		FROM @OuterTable ot 
		WHERE ot.ResponseCode IN ('000', '00', '002') 
		  AND ((ot.TxnType = 'Sale' AND ot.IsRefunded = 0 AND ot.IsVoided = 0) OR (ot.TxnType = 'Authorization' AND ot.IsCompleted = 1)) 
		  AND ot.fk_FleetId IN (SELECT DISTINCT ct.FleetId FROM @CorporateTable ct)
		  AND ot.MerchantId IN (SELECT DISTINCT cl.MerchantId FROM @CorporateTable cl)

		SELECT @TotalFareAmount = CONCAT('$', CAST(ISNULL(SUM(ot.Amount), '0.00') AS DECIMAL(16, 2))),
				@AmountOwing = CONCAT('$', CAST(ISNULL(SUM(ot.Amount),0.00) AS DECIMAL(18, 2)) - CAST(@TotalFee AS DECIMAL(18, 2)))
		FROM @OuterTable ot
		WHERE ot.ResponseCode IN ('000', '00', '002') 
		  AND ot.TxnType IN ('Sale', 'Completion') 
		  AND ot.IsRefunded = 0 
		  AND ot.IsVoided = 0 
		  AND ot.fk_FleetId IN (SELECT DISTINCT ct.FleetId FROM @CorporateTable ct)
		  AND ot.MerchantId IN (SELECT DISTINCT cl.MerchantId FROM @CorporateTable cl)

		SELECT	@NoChargeBack = ISNULL(COUNT(ot.Id), '0'),
				@ChargeBackValue = CONCAT('$', CAST(ISNULL(SUM(ot.Amount), '0.00') AS DECIMAL(18, 2))) 
		FROM @OuterTable ot 
		WHERE ot.TxnType ='Refund' 
		  AND ot.ResponseCode IN ('000', '00')
		  AND ot.fk_FleetId IN (SELECT DISTINCT ct.FleetId FROM @CorporateTable ct)
		  AND ot.MerchantId IN (SELECT DISTINCT cl.MerchantId FROM @CorporateTable cl)

		SELECT  @NoVoid = ISNULL(COUNT(Id), '0'),
				@VoidValue = CONCAT('$', CAST(ISNULL(SUM(Amount), '0.00') AS DECIMAL(18, 2))) 
		FROM @OuterTable ot 
		WHERE ot.TxnType ='Void' 
		  AND ot.ResponseCode IN ('000', '00')
		  AND ot.fk_FleetId IN (SELECT DISTINCT ct.FleetId FROM @CorporateTable ct)
		  AND ot.MerchantId IN (SELECT DISTINCT cl.MerchantId FROM @CorporateTable cl)

		SET @ReportType = @Frequency 
		SET @FDate = @DateFrom
		SET @TDate = DATEADD(day,-1, @DateTo)
		SET @TotalTechFee = CONCAT('$', @TotalTechFee)
		SET @TotalFee = CONCAT('$', @TotalFee)

		SELECT	@XmlData,
				@ReportType,
				@FDate,
				@TDate,
				@TotalTransaction,
				@TotalActualFare,
				@TotalTips,
				@TotalFareAmount,
				@TotalSurcharge,
				@TotalFee,
				@NoChargeBack,
				@ChargeBackValue,
				@NoVoid,
				@VoidValue,
				@AmountOwing,
				@TotalTechFee

		IF (@IsSuperCorporate = 1)
		BEGIN
			SET @NextSchedule = @NextScheduleDateTime
			SELECT @NextSchedule
		END
	END TRY
	BEGIN CATCH
		INSERT INTO dbo.ProcErrorHandler(ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, UserName, HostName, [TimeStamp])
		SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
	END CATCH
END
GO
