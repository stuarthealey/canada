﻿using System.Collections.Generic;

namespace MTData.Web.ViewData
{
    public class PagingViewData
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public double TotalPages { get; set; }
        public int SortBy { get; set; }
        public bool IsAsc { get; set; }
        public string Search { get; set; }
        public int IsLastRecord { get; set; } // 0 = Not a last record, 1 = Last Record, 2 = Number of records less than page size
        public int Count { get; set; }
        public IEnumerable<MerchantUserViewData> ListOfMerchantUser  { get; set; }
        public IEnumerable<UserViewData> UserList { get; set; }
        public IEnumerable<GatewayViewData> GatewayList { get; set; }
        
    }
}