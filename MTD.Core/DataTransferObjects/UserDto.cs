﻿using System;

namespace MTD.Core.DataTransferObjects
{
    public class UserDto
    {
        public int UserID { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string fk_Country { get; set; }
        public string fk_State { get; set; }
        public string fk_City { get; set; }
        public string Address { get; set; }
        public string PCode { get; set; }
        public int fk_UserTypeID { get; set; }
        public int? fk_MerchantID { get; set; }
        public Nullable<bool> IsLockedOut { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string RolesAssigned { get; set; }
        public string fleetListId { get; set; }
        public string ConfirmPCode { get; set; }
        public string GridCity { get; set; }
        public string GridState { get; set; }
        public string CountryName { get; set; }
        public Nullable<bool> IsFirstLogin { get; set; }
        public string MerchantListId { get; set; }
        public int LogOnByUserType { get; set; }
        public int LogOnByUserId { get; set; }
        public Nullable<int> ParentUserID { get; set; }
        public string ErrorMessage { get; set; }

        public Nullable<bool> UseTransArmor { get; set; }       //PAY-13
    }
}