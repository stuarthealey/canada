﻿using System.Collections.Generic;
using MTD.Core.DataTransferObjects;
using MTD.Core.Extension;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;
using Ninject;

namespace MTD.Core.Service
{
    class MerchantGatewayService : IMerchantGatewayService
    {
        private readonly IMerchantGatewayRepository _merchantGatewayRepository;

        public MerchantGatewayService([Named("MerchantGatewayRepository")] IMerchantGatewayRepository merchantGatewayRepository)
        {
            _merchantGatewayRepository = merchantGatewayRepository;
        }

        /// <summary>
        /// Purpose             :   To Add the Merchant Gateway
        /// Function Name       :   Add
        /// Created By          :   Umesh Kumar
        /// Created On          :   01/23/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gatewayDto"></param>
        /// <returns></returns>
        public bool Add(MerchantGatewayDto gatewayDto)
        {

            try
            {
                Merchant_Gateway gateway = gatewayDto.ToMerchantGateway();
                bool isExist = _merchantGatewayRepository.Add(gateway);
                return isExist;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Merchant Gateway List
        /// Function Name       :   GetMerchantGatewayDtoList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/23/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<MerchantGatewayDto> GetMerchantGatewayDtoList(int merchantId)
        {
            try
            {
                var gatewayList = _merchantGatewayRepository.MerchantGatewayList(merchantId);
                var gatewayDtoList = gatewayList.ToDTOGatewayList();
                return gatewayDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Merchant Gateway
        /// Function Name       :   GetMerchantGatewayDto
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/23/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantGatewayId"></param>
        /// <returns></returns>
        public MerchantGatewayDto GetMerchantGatewayDto(int merchantGatewayId)
        {
            try
            {
                var gateway = _merchantGatewayRepository.GetMerchantGateway(merchantGatewayId);
                var gatewayDto = gateway.ToMerchantGatewayDto();
                return gatewayDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update the Merchant Gateway
        /// Function Name       :   Update
        /// Created By          :   Umesh Kumar
        /// Created On          :   01/27/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="gatewayDto"></param>
        /// <param name="merchantGatewayId"></param>
        /// <returns></returns>
        public bool Update(MerchantGatewayDto gatewayDto, int merchantGatewayId)
        {
            try
            {
                Merchant_Gateway gateway = gatewayDto.ToMerchantGateway();
                return _merchantGatewayRepository.Update(gateway, merchantGatewayId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Delete the Merchant Gateway
        /// Function Name       :   DeleteMerchantGateway
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/27/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantGatewayId"></param>
        public void DeleteMerchantGateway(int merchantGatewayId)
        {
            try
            {
                _merchantGatewayRepository.DeleteMerchantGateway(merchantGatewayId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Merchant Gateway List
        /// Function Name       :   MerchantGateListByFilter
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/29/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="filterSearch"></param>
        /// <returns></returns>
        public IEnumerable<MerchantGatewayDto> MerchantGateListByFilter(FilterSearchParametersDto filterSearch)
        {
            try
            {
                var filterSearchClass = filterSearch.SearchParameters();
                var gatewayList = _merchantGatewayRepository.MerchantGateListByFilter(filterSearchClass);
                var gatewayDtoList = gatewayList.ToDTOGatewayList();
                return gatewayDtoList;
            }
            catch
            {
                throw;
            }
        }

    }
}
