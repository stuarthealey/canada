﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.Web.ViewData
{
    public class GetTotalTransByVehicleReportViewData
    {
        public Nullable<long> SrNo { get; set; }
        public string MerchantName { get; set; }
        public string FleetName { get; set; }
        public int NoOfTrans { get; set; }
        public string FareAmt { get; set; }
        public string Tips { get; set; }
        public string Surcharges { get; set; }
        public string TechFee { get; set; }
        public int NoOfChargebacks { get; set; }
        public string ChargebackValue { get; set; }
        public int NoOfVoid { get; set; }
        public string VoidValue { get; set; }
        public string TotalAmount { get; set; }
        public string Fees { get; set; }
        public string AmountOwing { get; set; }
        public int NoOfVehilesForSlab1 { get; set; }
        public int NoOfVehilesForSlab2 { get; set; }
        public int NoOfVehilesForSlab3 { get; set; }
        public int NoOfVehilesForSlab4 { get; set; }
    }
}