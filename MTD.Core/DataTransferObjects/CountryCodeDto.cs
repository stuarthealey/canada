﻿namespace MTD.Core.DataTransferObjects
{
    public class CountryCodeDto
    {
        public string CountryCodesID { get; set; }
        public string Title { get; set; }
    }
}
