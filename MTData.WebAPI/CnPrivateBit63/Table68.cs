﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table68
    {
        #region Private Constants and Variables
        #region Constants
        private const string tblId = "|36|38";
        private const string version = "|32";//Version 2
        #endregion

        #region Variables
        private string defaultBalInfoCapability = "|30";//Balance information not supported in responses.
        private string defaultPartialAuthAppCapability = "|30";//Partial authorization not supported.
        #endregion
        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'36 38' in Request
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        /// <summary>
        /// Version of the table
        /// default X'32' in Request for Version 2
        /// </summary>
        public string Version
        {
            get { return version; }
        }

        /// <summary>
        /// POS device’s capability to handle balance information in response messages.
        /// default X'30' in Request 
        /// X'30' — Balance information not supported in responses 
        /// X'31' — Balance information is supported in responses
        /// </summary>
        public string BalanceInfoCapability
        {
            get { return defaultBalInfoCapability; }
            set { defaultBalInfoCapability = value; }
        }

        /// <summary>
        /// POS device’s capability to accept response messages
        /// in which only a portion of the requested amount was approved.
        /// default X'31' in Request 
        /// X'30' — Partial authorization approvals are not supported. 
        /// X'31' — Partial authorization approvals are supported.
        /// </summary>
        public string PartialAuthApprovalCapability
        {
            get { return defaultPartialAuthAppCapability; }
            set { defaultPartialAuthAppCapability = value; }
        }
        #endregion
    }
}