﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    [XmlSerializerFormat]
    public class EmvCapkDetails
    {
        private string _driver;

        [DataMember]
        public string DriverNumber
        {
            get { return string.IsNullOrEmpty(this._driver) ? string.Empty : this._driver; }
            set { this._driver = value; }
        }

        private string _emvcapk;

        [DataMember]
        public string CapkData
        {
            get { return string.IsNullOrEmpty(this._emvcapk) ? string.Empty : this._emvcapk; }
            set { this._emvcapk = value; }
        }

        private string _currentCreationDate;

        [DataMember]
        public string CreationDate
        {
            get { return string.IsNullOrEmpty(this._currentCreationDate) ? string.Empty : this._currentCreationDate; }
            set { this._currentCreationDate = value; }
        }
         
    }
}