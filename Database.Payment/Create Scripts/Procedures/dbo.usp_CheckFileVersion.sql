IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CheckFileVersion]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_CheckFileVersion]
GO

CREATE PROCEDURE dbo.usp_CheckFileVersion
(
	@IsClsCap BIT,
	@CurrentVersion DECIMAL(10,2),
	@UpdateRequired CHAR(3) OUT 
)
AS
BEGIN
	DECLARE @FileVersion DECIMAL(10, 2)

	SELECT TOP 1 @FileVersion = FileVersion 
	FROM dbo.CapKey
	WHERE IsContactLess = @IsClsCap 
	ORDER BY Id DESC

	IF (@CurrentVersion < @FileVersion)
		SET @UpdateRequired = 'Yes'
	ELSE
		SET @UpdateRequired = 'No'
END
GO
