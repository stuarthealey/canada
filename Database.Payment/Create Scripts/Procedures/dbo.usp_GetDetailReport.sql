IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetDetailReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_GetDetailReport]
GO

CREATE PROCEDURE dbo.usp_GetDetailReport
-- =============================================
-- Author:	<Naveen Kumar>
-- Create date: <10-03-2016>
-- Description:	<Get merchant detailed report>
-- =============================================
(
	@DateFrom DATETIME,
	@DateTo DATETIME,
	@MerchantId INT
)
AS
BEGIN
	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SET ANSI_WARNINGS OFF
		SET NOCOUNT ON

		DECLARE @CardType NVARCHAR(25)

		DECLARE @TableData TABLE (
			Company NVARCHAR(80),
			ReportDate DATE,
			TransId INT,
			TransDate DATE,
			TxnType NVARCHAR(20),
			CardType NVARCHAR(20),
			LastFour NVARCHAR(6),
			AuthCode NVARCHAR(20),
			Trip NVARCHAR(40),
			DriverNo NVARCHAR(40),
			VehicleNo NVARCHAR(40),
			TechFee NVARCHAR(40),
			Surcharge NVARCHAR(40),
			GrossAmt NVARCHAR(40),
			Fees NVARCHAR(40),
			NetAmt NVARCHAR(40)
		)

		--Getting all the card types one by one 
		DECLARE @count INT = 1
		WHILE @count <= 5 
		BEGIN
			IF (@count = 1) 
			BEGIN
				SET @CardType = 'AmericanExpress'
			END
			ELSE IF (@count = 2)
			BEGIN
				SET @CardType = 'Discover'
			END
			ELSE IF (@count = 3)
			BEGIN
				SET @CardType = 'MasterCard'
			END
			ELSE IF (@count = 4)
			BEGIN
				SET @CardType = 'Visa'
			END
			ELSE
			BEGIN
				SET @CardType = 'Amex'
			END

			--Prepare the transaction data in xml format corresponding to a that specific card type   
			INSERT INTO @TableData(Company, ReportDate, TransId, TransDate, TxnType, CardType, LastFour, AuthCode, Trip, DriverNo, VehicleNo, TechFee, Surcharge, GrossAmt, Fees, NetAmt)                   
			SELECT	ISNULL(m.Company, ''),
					CAST(GETDATE() AS DATE),
					t.Id,
					ISNULL(CAST(t.TxnDate AS DATE), ''),
					ISNULL(t.TxnType, ''),
					ISNULL(t.CardType, ''),
					ISNULL(t.LastFourDigits, ''),
					ISNULL(t.AuthId, ''),
					ISNULL(t.JobNumber, ''),
					ISNULL(t.DriverNo, ''),
					ISNULL(t.VehicleNo, ''),
					CONCAT('$', CAST(ISNULL(t.TechFee, 0.00) AS DECIMAL(16, 2))),
					CONCAT('$', ISNULL(CAST(t.Surcharge AS DECIMAL(18, 2)),0.00)),
					CONCAT('$', ISNULL(CAST(t.Amount AS DECIMAL(18, 2)),0.00)),
					CONCAT('$', CAST(ISNULL(t.FleetFee, 0.00)+ISNULL(t.Fee,0.00) AS DECIMAL(16, 2))),
					CONCAT('$', CAST(ISNULL(t.Amount, 0.00)-ISNULL(t.Fee ,0.00)-ISNULL(t.FleetFee ,0.00) AS DECIMAL(18, 2)))
			FROM dbo.MTDTransaction t 
				INNER JOIN dbo.Vehicle v ON t.VehicleNo = v.VehicleNumber 
				INNER JOIN dbo.Fleet f ON v.fk_FleetID = f.FleetID 
				INNER JOIN dbo.Merchant m ON f.fk_MerchantID = m.MerchantID 
			WHERE m.MerchantId = @MerchantId
			  AND CAST(t.TxnDate AS DATE) >= @DateFrom 
			  AND CAST(t.TxnDate AS DATE) < @DateTo 
			  AND t.CardType = @CardType 
			  AND t.ResponseCode IN ('000', '00', '002') 
			  AND t.fk_FleetId = v.fk_FleetID

			SET @count = @count + 1
		END

		SELECT	Company AS 'Company',
				ReportDate AS 'ReportDate',
				TransId AS 'TransId',
				TransDate AS 'TransDate',
				TxnType AS 'TxnType',
				CardType AS 'CardType',
				LastFour AS 'LastFour',
				AuthCode AS 'AuthCode',
				Trip AS 'Trip',
				DriverNo AS 'DriverNo',
				VehicleNo AS 'VehicleNo',
				TechFee AS 'TechFee',
				Surcharge AS 'Surcharge',
				GrossAmt AS 'GrossAmt',
				Fees AS 'Fees',
				NetAmt AS 'NetAmt'
		FROM @TableData
	END TRY
	BEGIN CATCH
		INSERT INTO dbo.ProcErrorHandler(ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, UserName, HostName, [TimeStamp])
		SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
	END CATCH
END
GO
