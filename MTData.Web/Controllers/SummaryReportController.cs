﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Text;
using System.Globalization;
using System.Reflection;
using System.Collections.Generic;
using System.Web.Security;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.Web.Resource;
using MTData.Web.ViewData;

namespace MTData.Web.Controllers
{
    public class SummaryReportController : Controller
    {
        private readonly ILogger _logger = new Logger();
        private readonly IScheduleService _scheduleService;
        private readonly ICommonService _commonService;
        private StringBuilder _logMessage;
        private StringBuilder _logTracking;

        public SummaryReportController([Named("CommonService")] ICommonService commonService,
            [Named("ScheduleService")] IScheduleService scheduleService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _scheduleService = scheduleService;
            _commonService = commonService;
            _logMessage = logMessage;
            _logTracking = logTracking;
        }

        /// <summary>
        ///  Purpose            : To schedule reports
        /// Function Name       :  Create
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   06/24/2016
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Display()
        {
            if ((SessionDto)Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                return View();
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        [HttpGet]
        public ActionResult DollarVolumeDisplay()
        {
            if ((SessionDto)Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                return View();
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            : To schedule reports
        /// Function Name       :  Create
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   06/24/2016
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("index", "Member");
            try

            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var recipientDto = _commonService.GetRecipientListDistinct();
                foreach (var rec in recipientDto)
                {
                    rec.IsRecipient = true;
                }

                var recipient = recipientDto.ToMerchantViewDataSelectList();
                var adminRecipientDto = _commonService.GetAdminRecipientListDistinct();
                foreach (var recipnt in adminRecipientDto)
                {
                    recipnt.IsRecipient = true;
                }

                var adminRecipient = adminRecipientDto.ToMerchantViewDataSelectList();
                ScheduleReportViewData scheduleReport = new ScheduleReportViewData
                { 
                    AdminList = adminRecipient,
                };

                _logMessage.Append("calling FrequencyList method of same class");
                _logMessage.Append("FrequencyList methods executed successfully");

                scheduleReport.DateFrom = DateTime.Now.Date;

                return View(scheduleReport);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }


        /// <summary>
        ///  Purpose            : To schedule reports
        /// Function Name       :  WeeklyCountReportListByFiter
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   06/24/2016
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public JsonResult WeeklyCountReportListByFiter(string name = null, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            FilterSearchParametersDto filterSearch = new FilterSearchParametersDto()
            {
                Name = name,
                StartIndex = jtStartIndex,
                PageSize = jtPageSize,
                Sorting = jtSorting,
            };

            var reportData = _scheduleService.WeeklySummary(filterSearch);
            int adminUserCount = _scheduleService.WeeklySummaryCount(filterSearch);
          
            if (string.IsNullOrEmpty(name))
            {
                _logMessage.Append("Calling UserList method of _userService class to count fee");
                
                adminUserCount = _scheduleService.WeeklySummary(filterSearch).Count();
                 
                return Json(new { Result = "OK", Records = reportData, TotalRecordCount = adminUserCount });
            }

            var reportDatas = reportData as GetAdminWeeklySummaryCountDto[] ?? reportData.ToArray();            
            return Json(new { Result = "OK", Records = reportDatas, TotalRecordCount = adminUserCount });
        }

        /// <summary>
        ///  Purpose            : To get the weekly dollar volume data to display in the grid
        /// Function Name       :  WeeklyDollarVolumeReportListByFiter
        /// Created By          :   Naveen Kumar
        /// Created On          :   07/01/2016
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public JsonResult WeeklyDollarVolumeReportListByFiter(string name = null, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            FilterSearchParametersDto filterSearch = new FilterSearchParametersDto()
            {
                Name = name,
                StartIndex = jtStartIndex,
                PageSize = jtPageSize,
                Sorting = jtSorting
            };

            var reportData = _scheduleService.WeeklyDollarVolumeReport(filterSearch);
            int recordsCount = _scheduleService.WeeklyDollarVolumeReportCount(filterSearch);

            if (string.IsNullOrEmpty(name))
            {
                _logMessage.Append("Calling UserList method of _userService class to count fee");

                recordsCount = _scheduleService.WeeklyDollarVolumeReport(filterSearch).Count();

                return Json(new { Result = "OK", Records = reportData, TotalRecordCount = recordsCount });
            }

            var reportDatas = reportData as WeeklyDollarVolumeDto[] ?? reportData.ToArray();
            return Json(new { Result = "OK", Records = reportDatas, TotalRecordCount = recordsCount });
        }
	}
}