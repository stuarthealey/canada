﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Data.Data
{
   public class FilterSearchParameters
   {
       public string Name { get; set; }
       public string DriverNo { get; set; }
       public string OwnerName { get; set; }
       public int StartIndex { get; set; }
       public int PageSize { get; set; }
       public string Sorting { get; set; }
       public int MerchantId { get; set; }
       public string RapidConnectID { get; set; }
       public string SerialNumber { get; set; }
       public int UserType  { get; set; }
       public int UserId { get; set; }
       public int LogOnByUserId { get; set; }
       public int LogOnByUserType { get; set; }
       public int? CorporateId { get; set; }

       public IEnumerable<UserFleet> UserFleet { get; set; }
    }
}
