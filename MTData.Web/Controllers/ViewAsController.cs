﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.Web.Resource;
using MTData.Web.ViewData;

namespace MTData.Web.Controllers
{
    public class ViewAsController : Controller
    {
        readonly ILogger _logger = new Logger();
        private readonly IMerchantService _merchantService;
        private readonly ITaxService _taxService;
        private readonly ISurchargeService _surchargeService;
        private readonly ICommonService _commonService;
        private StringBuilder _logMessage;
        private readonly IDefaultService _defaultService;
        private readonly IFleetService _fleetService;
        private readonly IUserService _userService;
        private readonly IReportCommonService _reportcommonRepository;
        private StringBuilder _logTracking;
        private readonly IScheduleService _scheduleService;

        public ViewAsController([Named("UserService")] IUserService userService, [Named("ScheduleService")] IScheduleService scheduleService, [Named("FleetService")] IFleetService fleetService, [Named("MerchantService")] IMerchantService merchantService, [Named("CommonService")] ICommonService commomService, [Named("SurchargeService")] ISurchargeService surchargeService, [Named("TaxService")] ITaxService taxService, StringBuilder logMessage, [Named("DefaultService")] IDefaultService defaultService, [Named("ReportCommonService")] IReportCommonService reportcommonService, StringBuilder logTracking)
        {
            _merchantService = merchantService;
            _surchargeService = surchargeService;
            _commonService = commomService;
            _taxService = taxService;
            _logMessage = logMessage;
            _defaultService = defaultService;
            _fleetService = fleetService;
            _userService = userService;
            _reportcommonRepository = reportcommonService;
            _logTracking = logTracking;
            _scheduleService = scheduleService;
        }

        /// <summary>
        /// ///Purpose          : To display the Merchants
        /// Function Name       :  Index
        /// Created By          :  umesh kumar
        /// Created On          :  12/23/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var httpCookie = Request.Cookies["CookieViewAs"];
                if (httpCookie != null)
                {
                    _logMessage.Append("Getting merchant and user id from cookie");
                    var merchantId = Convert.ToInt32(httpCookie["merchantId"]);
                    int userId = Convert.ToInt32(httpCookie["userId"]);
                    _logMessage.Append("Got merchant and user id from cookie merchant Id=" + merchantId + " ,and user Id" + userId);

                    MerchantUserDto mrMerchant = _merchantService.GetMerchantViewAs(merchantId, userId);
                    MerchantUserViewData mvData = mrMerchant.ToViewData();
                    
                    mvData.TimeZones = TimeZoneList();
                    mvData.UserRoles = _merchantService.GetUserRoles().ToViewDataList();
                    ViewData.Model = mvData;
                    
                    return View(mvData);
                }
                return View();
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        /// <summary>
        ///Purpose              : to retrieve the list of merchant
        /// Function Name       :   Merchant
        /// Created By          : Asif Shafeeque 
        /// Created On          :  14/1/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Merchant()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var httpCookie = Request.Cookies["CookieViewAs"];
                int merchantId = 0;
                if (httpCookie != null)
                    merchantId = Convert.ToInt32(httpCookie["merchantId"]);

                var surchargeDtoList = _surchargeService.GetSurchargeDtoList();
                IEnumerable<SurchargeViewData> viewDataList = surchargeDtoList.ToSurchargeViewDataList().Where(x => x.FleetID == merchantId);
                var surchargeViewDatas = viewDataList as SurchargeViewData[] ?? viewDataList.ToArray();
                FindSurcharge(surchargeViewDatas);

                if (surchargeViewDatas.ToList().Count >= 1)
                {
                    return RedirectToAction("MerchantSurcharges");
                }

                return RedirectToAction("SurchargeDisplay");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return RedirectToAction("SurchargeDisplay");
        }

        /// <summary>
        ///Purpose              : to get the surcharge list
        /// Function Name       :   Surcharge
        /// Created By          : Naveen Kumar
        /// Created On          :   17/1/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Surcharge()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            SurchargeViewData surcharge = new SurchargeViewData();
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                var defaultDto = _defaultService.GetDefaultDto(2);
                
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var httpCookie = Request.Cookies["CookieViewAs"];
                if (httpCookie != null)
                {
                    int merchantId = Convert.ToInt32(httpCookie["merchantId"]);
                    IEnumerable<FleetListDto> fvd = _reportcommonRepository.GetFleetList(merchantId);
                    List<FleetListViewData> fld = fvd.Select(fleet => new FleetListViewData { FleetName = fleet.FleetName, FleetID = fleet.FleetID }).ToList();

                    var surchargeView = new SurchargeViewData
                    {
                        GatewayList = GatewayList(),
                        TypeList = TypeList(),
                        Fleets = fld
                    };

                    surchargeView.SurchargeType = defaultDto.SurchargeType;//per
                    surchargeView.Surcharge = defaultDto.SurchargeFixed;
                    surchargeView.SurchargePer = defaultDto.SurchargePer;
                    surchargeView.SurMaxCap = defaultDto.SurchargeMaxCap;

                    surchargeView.BookingFeeType = defaultDto.BookingFeeType;//both
                    surchargeView.BookingFee = defaultDto.BookingFeeFixed;
                    surchargeView.FeePercentage = defaultDto.BookingFeePer;
                    surchargeView.BookingFeeMaxCap = defaultDto.BookingMaxCap;

                    surchargeView.TechFee = defaultDto.TechFeeFixed;
                    surchargeView.TechFeeType = defaultDto.TechFeeType;
                    surchargeView.TechFeePer = defaultDto.TechFeePer;
                    surchargeView.TechFeeMaxCap = defaultDto.TechFeeMaxCap;
                    surchargeView.TechFeeFixed = defaultDto.TechFeeFixed;
                    return View(surchargeView);
                }

                surcharge.GatewayList = GatewayList();
                surcharge.TypeList = TypeList();
                surcharge.Surcharge = defaultDto.SurchargeFixed;
                surcharge.SurchargePercentage = defaultDto.SurchargePer;
                surcharge.SurMaxCap = defaultDto.SurchargeMaxCap;
                surcharge.BookingFee = defaultDto.BookingFeeFixed;
                surcharge.FeePercentage = defaultDto.BookingFeePer;
                surcharge.BookingFeeMaxCap = defaultDto.BookingMaxCap;
                surcharge.SurchargeType = defaultDto.SurchargeType;//per
                surcharge.BookingFeeType = defaultDto.BookingFeeType;//both
                surcharge.TechFeeType = defaultDto.TechFeeType;
                surcharge.TechFeePer = defaultDto.TechFeePer;
                surcharge.TechFeeMaxCap = defaultDto.TechFeeMaxCap;
                surcharge.TechFeeFixed = defaultDto.TechFeeFixed;
                surcharge.TechFee = defaultDto.TechFeeFixed;

                return View(surcharge);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///Purpose              : to retrieve the surcharge list
        /// Function Name       :   Surcharge
        /// Created By          : Naveen Kumar
        /// Created On          :   17/1/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="surchargeViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Surcharge(SurchargeViewData surchargeViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +                              MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (surchargeViewData.SurchargeType == 2)
                {
                    if (surchargeViewData.SurchargePer == null)
                        ModelState.AddModelError("SurchargePer", MtdataResource.Please_enter_surcharge_percentage);

                    if (surchargeViewData.SurMaxCap == null)
                        ModelState.AddModelError("SurMaxCap", MtdataResource.SurchargeViewData_SurMaxCap_Please_enter_the_surcharge_s_max_cap);
                }

                if (surchargeViewData.SurchargeType == 1)
                {
                    if (surchargeViewData.Surcharge == null)
                        ModelState.AddModelError("Surcharge", MtdataResource.Please_enter_surcharge);
                }

                if (surchargeViewData.SurchargeType == 3)
                {
                    if (surchargeViewData.SurchargePer == null)
                        ModelState.AddModelError("SurchargePer", MtdataResource.Please_enter_surcharge_percentage);

                    if (surchargeViewData.Surcharge == null)
                        ModelState.AddModelError("Surcharge", MtdataResource.Please_enter_surcharge);

                    if (surchargeViewData.SurMaxCap == null)
                        ModelState.AddModelError("SurMaxCap", MtdataResource.SurchargeViewData_SurMaxCap_Please_enter_the_surcharge_s_max_cap);
                }

                if (surchargeViewData.BookingFeeType == 1)
                {
                    if (surchargeViewData.BookingFee == null)
                        ModelState.AddModelError("BookingFee", MtdataResource.Please_txnFee_surcharge);
                }

                if (surchargeViewData.BookingFeeType == 2)
                {
                    if (surchargeViewData.FeePercentage == null)
                        ModelState.AddModelError("FeePercentage", MtdataResource.Please_enter_txnFee_percentage);

                    if (surchargeViewData.BookingFeeMaxCap == null)
                        ModelState.AddModelError("BookingFeeMaxCap", MtdataResource.SurchargeViewData_txnFee_Please_enter_the_surcharge_s_max_cap);
                }

                if (surchargeViewData.BookingFeeType == 3)
                {
                    if (surchargeViewData.FeePercentage == null)
                        ModelState.AddModelError("FeePercentage", MtdataResource.Please_enter_txnFee_percentage);

                    if (surchargeViewData.BookingFee == null)
                        ModelState.AddModelError("BookingFee", MtdataResource.Please_txnFee_surcharge);

                    if (surchargeViewData.BookingFeeMaxCap == null)
                        ModelState.AddModelError("BookingFeeMaxCap", MtdataResource.SurchargeViewData_txnFee_Please_enter_the_surcharge_s_max_cap);
                }

                if (surchargeViewData.TechFeeType == 1)
                {
                    if (surchargeViewData.TechFeeFixed == null)
                        ModelState.AddModelError("TechFeeFixed", MtdataResource.Please_enter_technology_fee);
                }

                if (surchargeViewData.TechFeeType == 2)
                {
                    if (surchargeViewData.TechFeePer == null)
                        ModelState.AddModelError("TechFeePer", MtdataResource.Please_enter_technology_Fee_Percentage);

                    if (surchargeViewData.TechFeeMaxCap == null)
                        ModelState.AddModelError("TechFeeMaxCap", MtdataResource.Please_enter_technology_feemax_cap);
                }

                if (surchargeViewData.TechFeeType == 3)
                {
                    if (surchargeViewData.TechFeePer == null)
                        ModelState.AddModelError("TechFeePer", MtdataResource.Please_enter_technology_Fee_Percentage);

                    if (surchargeViewData.TechFeeFixed == null)
                        ModelState.AddModelError("TechFeeFixed", MtdataResource.Please_enter_technology_fee);

                    if (surchargeViewData.TechFeeMaxCap == null)
                        ModelState.AddModelError("TechFeeMaxCap", MtdataResource.Please_enter_technology_feemax_cap);
                }

                if (ModelState.IsValid)
                {
                    var httpCookie = Request.Cookies["CookieViewAs"];
                    int mId = Convert.ToInt32(httpCookie["merchantId"]);
                    surchargeViewData.MerchantId = mId;
                    surchargeViewData.FleetID = surchargeViewData.FleetID;
                    FindSurchargeType(surchargeViewData);
                    FindFeeType(surchargeViewData);
                    FindTechnologyType(surchargeViewData);
                    var surchargeDto = surchargeViewData.ToSurchareDto();
                    surchargeDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                    bool isExistOrDefault = _surchargeService.Add(surchargeDto);
                    if (isExistOrDefault)
                    {
                        if (httpCookie != null)
                        {
                            int merchantId = Convert.ToInt32(httpCookie["merchantId"]);

                            IEnumerable<FleetListDto> fvd = _reportcommonRepository.GetFleetList(merchantId);
                            List<FleetListViewData> fld = fvd.Select(fleet => new FleetListViewData { FleetName = fleet.FleetName, FleetID = fleet.FleetID }).ToList();

                            ViewBag.Message = MtdataResource.Credit_card_prefix_already_exist; // "Credit card prefix already exists";
                            surchargeViewData.GatewayList = GatewayList();
                            surchargeViewData.TypeList = TypeList();
                            surchargeViewData.Fleets = fld;

                            return View(surchargeViewData);
                        }
                    }

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + " add surcharge  at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return RedirectToAction("MerchantSurcharges");
                }

                var defaultDto = _defaultService.GetDefaultDto(2);
                surchargeViewData.Surcharge = defaultDto.SurchargePer;
                surchargeViewData.BookingFee = defaultDto.BookingFeePer;
                surchargeViewData.BookingFeeType = defaultDto.BookingFeeType;

                var httpCookies = Request.Cookies["CookieViewAs"];
                if (httpCookies != null)
                {
                    int merchantId = Convert.ToInt32(httpCookies["merchantId"]);

                    IEnumerable<FleetListDto> fvd = _reportcommonRepository.GetFleetList(merchantId);
                    List<FleetListViewData> fld = fvd.Select(fleet => new FleetListViewData { FleetName = fleet.FleetName, FleetID = fleet.FleetID }).ToList();

                    surchargeViewData.GatewayList = GatewayList();
                    surchargeViewData.TypeList = TypeList();
                    surchargeViewData.Fleets = fld;

                    return View(surchargeViewData);
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View(surchargeViewData);
        }

        /// <summary>
        ///Purpose              : to update the surcharge list
        /// Function Name       :  Update
        /// Created By          :  Naveen kumar
        /// Created On          :   1/19/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="surchargeId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Update(int surchargeId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (Session["User"] == null) return RedirectToAction("index", "Member");
                var surchargeDto = _surchargeService.GetSurchargeDto(surchargeId);
                var surchargeView = surchargeDto.ToSurchargeViewData();

                FindSurchargeId(surchargeView);
                FindFeeId(surchargeView);
                surchargeView.GatewayList = GatewayList();
                surchargeView.TypeList = TypeList();
                var httpCookie = Request.Cookies["CookieViewAs"];
                if (httpCookie != null)
                {
                    int merchantId = Convert.ToInt32(httpCookie["merchantId"]);
                    IEnumerable<FleetListDto> fvd = _reportcommonRepository.GetFleetList(merchantId);
                    List<FleetListViewData> fld = fvd.Select(fleet => new FleetListViewData { FleetName = fleet.FleetName, FleetID = fleet.FleetID }).ToList();
                    surchargeView.Fleets = fld;
                    surchargeView.FleetID = surchargeView.FleetID;
                    return View(surchargeView);
                }
                return View(surchargeView);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        ///Purpose              : to update the surcharge list
        /// Function Name       : Update
        /// Created By          : Naveen kumar
        /// Created On          : 1/19/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="surchargeId"></param>
        /// <param name="surchargeViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(int surchargeId, SurchargeViewData surchargeViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (surchargeViewData.SurchargeType == 2)
                {
                    if (surchargeViewData.SurchargePer == null)
                    {
                        ModelState.AddModelError("SurchargePer", MtdataResource.Please_enter_surcharge_percentage);
                    }
                    if (surchargeViewData.SurMaxCap == null)
                    {
                        ModelState.AddModelError("SurMaxCap", MtdataResource.SurchargeViewData_SurMaxCap_Please_enter_the_surcharge_s_max_cap);
                    }
                }

                if (surchargeViewData.SurchargeType == 1)
                {
                    if (surchargeViewData.Surcharge == null)
                    {
                        ModelState.AddModelError("Surcharge", MtdataResource.Please_enter_surcharge);
                    }
                }

                if (surchargeViewData.SurchargeType == 3)
                {
                    if (surchargeViewData.SurchargePer == null)
                    {
                        ModelState.AddModelError("SurchargePer", MtdataResource.Please_enter_surcharge_percentage);
                    }
                    if (surchargeViewData.Surcharge == null)
                    {
                        ModelState.AddModelError("Surcharge", MtdataResource.Please_enter_surcharge);
                    }
                    if (surchargeViewData.SurMaxCap == null)
                    {
                        ModelState.AddModelError("SurMaxCap", MtdataResource.SurchargeViewData_SurMaxCap_Please_enter_the_surcharge_s_max_cap);
                    }
                }
                if (surchargeViewData.BookingFeeType == 1)
                {
                    if (surchargeViewData.BookingFee == null)
                    {
                        ModelState.AddModelError("BookingFee", MtdataResource.Please_txnFee_surcharge);
                    }
                }

                if (surchargeViewData.BookingFeeType == 2)
                {
                    if (surchargeViewData.FeePercentage == null)
                    {
                        ModelState.AddModelError("FeePercentage", MtdataResource.Please_enter_txnFee_percentage);
                    }
                    if (surchargeViewData.BookingFeeMaxCap == null)
                    {
                        ModelState.AddModelError("BookingFeeMaxCap", MtdataResource.SurchargeViewData_txnFee_Please_enter_the_surcharge_s_max_cap);
                    }
                }

                if (surchargeViewData.BookingFeeType == 3)
                {
                    if (surchargeViewData.FeePercentage == null)
                    {
                        ModelState.AddModelError("FeePercentage", MtdataResource.Please_enter_txnFee_percentage);
                    }
                    if (surchargeViewData.BookingFee == null)
                    {
                        ModelState.AddModelError("BookingFee", MtdataResource.Please_txnFee_surcharge);
                    }
                    if (surchargeViewData.BookingFeeMaxCap == null)
                    {
                        ModelState.AddModelError("BookingFeeMaxCap", MtdataResource.SurchargeViewData_txnFee_Please_enter_the_surcharge_s_max_cap);
                    }
                }

                if (surchargeViewData.TechFeeType == 1)
                {
                    if (surchargeViewData.TechFeeFixed == null)
                    {
                        ModelState.AddModelError("TechFeeFixed", MtdataResource.Please_enter_technology_fee);
                    }
                }

                if (surchargeViewData.TechFeeType == 2)
                {
                    if (surchargeViewData.TechFeePer == null)
                    {
                        ModelState.AddModelError("TechFeePer", MtdataResource.Please_enter_technology_Fee_Percentage);
                    }
                    if (surchargeViewData.TechFeeMaxCap == null)
                    {
                        ModelState.AddModelError("TechFeeMaxCap", MtdataResource.Please_enter_technology_feemax_cap);
                    }
                }

                if (surchargeViewData.TechFeeType == 3)
                {
                    if (surchargeViewData.TechFeePer == null)
                    {
                        ModelState.AddModelError("TechFeePer", MtdataResource.Please_enter_technology_Fee_Percentage);
                    }
                    if (surchargeViewData.TechFeeFixed == null)
                    {
                        ModelState.AddModelError("TechFeeFixed", MtdataResource.Please_enter_technology_fee);
                    }
                    if (surchargeViewData.TechFeeMaxCap == null)
                    {
                        ModelState.AddModelError("TechFeeMaxCap", MtdataResource.Please_enter_technology_feemax_cap);
                    }
                }

                if (ModelState.IsValid)
                {
                    var httpCookie = Request.Cookies["CookieViewAs"];
                    if (httpCookie != null)
                        surchargeViewData.MerchantId = Convert.ToInt32(httpCookie["merchantId"]);

                    surchargeViewData.FleetID = surchargeViewData.FleetID;
                    FindSurchargeType(surchargeViewData);
                    FindFeeType(surchargeViewData);
                    SurchargeDto surchargeDto = surchargeViewData.ToSurchareDto();
                    surchargeDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                    
                    bool isExist = _surchargeService.Update(surchargeDto, surchargeId);
                    if (!isExist)
                    {
                        string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                        _logTracking.Append("User " + userName + "  update  surcharge ID " + surchargeId + " at " + System.DateTime.Now.ToUniversalTime());
                        _logger.LogInfoMessage(_logTracking.ToString());
                        return RedirectToAction("MerchantSurcharges");
                    }

                    if (httpCookie != null)
                    {
                        int merchantId = Convert.ToInt32(httpCookie["merchantId"]);
                        IEnumerable<FleetListDto> fvd = _reportcommonRepository.GetFleetList(merchantId);
                        List<FleetListViewData> fld = fvd.Select(fleet => new FleetListViewData { FleetName = fleet.FleetName, FleetID = fleet.FleetID }).ToList();
                        surchargeViewData.Fleets = fld;
                        ViewBag.Message = MtdataResource.Credit_card_prefix_already_exist;
                        surchargeViewData.GatewayList = GatewayList();
                        surchargeViewData.TypeList = TypeList();
                        return View(surchargeViewData);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            var httpCooki = Request.Cookies["CookieViewAs"];
            if (httpCooki != null)
            {
                int merchantId = Convert.ToInt32(httpCooki["merchantId"]);
                IEnumerable<FleetListDto> fvd = _reportcommonRepository.GetFleetList(merchantId);
                List<FleetListViewData> fld = fvd.Select(fleet => new FleetListViewData { FleetName = fleet.FleetName, FleetID = fleet.FleetID }).ToList();
                surchargeViewData.Fleets = fld;
                surchargeViewData.GatewayList = GatewayList();
                surchargeViewData.TypeList = TypeList();
                return View(surchargeViewData);
            }

            return View(surchargeViewData);
        }

        /// <summary>
        ///Purpose              : to display the tax screen on portal
        /// Function Name       : TaxDisplay
        /// Created By          : Umesh kumar
        /// Created On          : 1/21/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult TaxDisplay()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                
                var httpCookie = Request.Cookies["CookieViewAs"];

                if (httpCookie != null)
                {
                    int merchantId = Convert.ToInt32(httpCookie["merchantId"]);
                    var taxDto = _taxService.GetTaxDto(merchantId);
                    var tax = taxDto.ToTaxViewData();
                    if (tax.StateTaxRate != null && tax.FederalTaxRate != null)
                    {
                        tax.MerchantName = httpCookie["merchantName"];
                        return View(tax);
                    }
                    return RedirectToAction("Tax");
                }
                return View();
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }
            return View();
        }

        /// <summary>
        ///Purpose                 : to get the Tax 
        /// Function Name       : Tax
        /// Created By          : umesh kumar
        /// Created On          : 1/21/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public ActionResult Tax()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var httpCookie = Request.Cookies["CookieViewAs"];
                if (httpCookie != null)
                {
                    int merchantId = Convert.ToInt32(httpCookie["merchantId"]);
                    var taxDto = _taxService.GetTaxDto(merchantId);
                    var tax = taxDto.ToTaxViewData();
                    tax.Company = httpCookie["merchantName"];
                    return View(tax);
                }
                return View();
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }
            return View();
        }

        /// <summary>
        /// ///Purpose          : To display the tax
        /// Function Name       :   GetVehicleListByFilter
        /// Created By          : umesh kumar
        /// Created On          :   1/22/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="taxViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Tax(TaxViewData taxViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (ModelState.IsValid)
                {
                    var httpCookie = Request.Cookies["CookieViewAs"];
                    if (httpCookie != null)
                    {
                        taxViewData.MerchantID = Convert.ToInt32(httpCookie["merchantId"]);
                        var taxDto = taxViewData.ToTaxDto();
                        bool result = _taxService.Add(taxDto);
                        if (!result)
                        {
                            ViewBag.Message = MtdataResource.Internal_server_error;  //"Server Error";
                            return View(taxViewData);
                        }
                        string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                        _logTracking.Append("User " + userName + " set tax at " + System.DateTime.Now.ToUniversalTime());
                        _logger.LogInfoMessage(_logTracking.ToString());
                    }

                    return RedirectToAction("TaxDisplay");
                }

                return View(taxViewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// ///Purpose          : to fetch the updated tax
        /// Function Name       : TaxUpdate
        /// Created By          : umesh kumar
        /// Created On          :   1/23/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public ActionResult TaxUpdate()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var httpCookie = Request.Cookies["CookieViewAs"];
                if (httpCookie != null)
                {
                    int merchantId = Convert.ToInt32(httpCookie["merchantId"]);
                    var taxDto = _taxService.GetTaxDto(merchantId);
                    var tax = taxDto.ToTaxViewData();

                    return View(tax);
                }
                return View();
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// ///Purpose          : to post the updated tax
        /// Function Name       :  TaxUpdate
        /// Created By          : umesh kumar
        /// Created On          :   1/23/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="taxViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult TaxUpdate(TaxViewData taxViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (ModelState.IsValid)
                {
                    var httpCookie = Request.Cookies["CookieViewAs"];
                    if (httpCookie != null)
                    {
                        taxViewData.MerchantID = Convert.ToInt32(httpCookie["merchantId"]);
                        var taxDto = taxViewData.ToTaxDto();
                        bool result = _taxService.Add(taxDto);
                        if (!result)
                        {
                            ViewBag.Message = MtdataResource.Internal_server_error; //"Server Error";
                            return View(taxViewData);
                        }
                        string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                        _logTracking.Append("User " + userName + " update tax  at " + System.DateTime.Now.ToUniversalTime());
                        _logger.LogInfoMessage(_logTracking.ToString());
                    }

                    return RedirectToAction("TaxDisplay");
                }

                return View(taxViewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// ///Purpose          : to get the list of gateway
        /// Function Name       :  GatewayList
        /// Created By          :  Madhuri Tanwar
        /// Created On          :  1/19/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<GatewayListViewData> GatewayList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var gatewayDtoList = _commonService.GetGatewayList();
                List<GatewayListViewData> lst = gatewayDtoList.ToGatewayViewDataList().ToList();
                return lst;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

        /// <summary>
        /// ///Purpose         : to get the list of surcharge
        /// Function Name       :TypeList
        /// Created By          :Madhuri tanwar
        /// Created On          :20/1/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<SurchargeTypeViewData> TypeList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +                              MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var listSurchargeType = new List<SurchargeTypeViewData>
                {
                    new SurchargeTypeViewData { TypeID = 1, TypeName = MtdataResource.Fixed }, new SurchargeTypeViewData { TypeID = 2, TypeName = MtdataResource.Percentage }, new SurchargeTypeViewData { TypeID = 3, TypeName = "Both" }
                };

                return listSurchargeType;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);

            }
            return null;
        }

        /// <summary>
        /// ///Purpose         : to get the list of surcharge
        /// Function Name       :TypeList
        /// Created By          :Madhuri tanwar
        /// Created On          :20/1/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<SurchargeTypeViewData> TechnologyTypeList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +                              MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var listSurchargeType = new List<SurchargeTypeViewData>
                {
                    new SurchargeTypeViewData { TypeID = 1, TypeName = MtdataResource.Fixed },new SurchargeTypeViewData { TypeID = 2, TypeName = MtdataResource.Percentage },new SurchargeTypeViewData { TypeID = 3, TypeName = "Both" }
                };

                return listSurchargeType;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);

            }
            return null;
        }

        /// <summary>
        /// ///Purpose          : to find the type of surcharge
        /// Function Name       : FindSurchargeType
        /// Created By          : Madhuri tanwar
        /// Created On          :1/23/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="surchargeViewData"></param>
        [NonAction]
        public static void FindSurchargeType(SurchargeViewData surchargeViewData)
        {
            if (surchargeViewData.SurchargeType == 1)
            {
                surchargeViewData.SurchargeFixed = surchargeViewData.Surcharge;
                surchargeViewData.SurchargePer = null;
            }
            if (surchargeViewData.SurchargeType == 2)
            {
                surchargeViewData.SurchargePer = surchargeViewData.SurchargePer;
                surchargeViewData.SurchargeFixed = null;
            }
            if (surchargeViewData.SurchargeType == 3)
            {
                surchargeViewData.SurchargeFixed = surchargeViewData.Surcharge;
                surchargeViewData.SurchargePer = surchargeViewData.SurchargePer;
            }
        }

        [NonAction]
        public static void FindTechnologyType(SurchargeViewData surchargeViewData)
        {
            if (surchargeViewData.TechFeeType == 1)
            {
                surchargeViewData.TechFeeFixed = surchargeViewData.TechFeeFixed;
                surchargeViewData.TechFeePer = null;
            }
            if (surchargeViewData.TechFeeType == 2)
            {
                surchargeViewData.TechFeePer = surchargeViewData.TechFeePer;
                surchargeViewData.TechFeeFixed = null;
            }
            if (surchargeViewData.TechFeeType == 3)
            {
                surchargeViewData.TechFeeFixed = surchargeViewData.TechFeeFixed;
                surchargeViewData.TechFeePer = surchargeViewData.TechFeePer;
            }
        }
        /// <summary>
        /// ///Purpose          : to find the surcharge on the basis of its ID
        /// Function Name       : FindSurchargeId
        /// Created By          : umesh kumar
        /// Created On          : 23/1/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="surchargeViewData"></param>
        [NonAction]
        public void FindSurchargeId(SurchargeViewData surchargeViewData)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (surchargeViewData.SurchargeType == 3)
                {
                    surchargeViewData.Surcharge = surchargeViewData.SurchargeFixed;
                    surchargeViewData.SurchargePercentage = surchargeViewData.SurchargePer;
                    surchargeViewData.SurType = MtdataResource.both_percentage_fixed;   //"Both (Fixed and Percentage)";
                }

                if (surchargeViewData.SurchargeType == 1)
                {
                    surchargeViewData.Surcharge = surchargeViewData.SurchargeFixed;
                    surchargeViewData.SurType = MtdataResource.Fixed;   //"Fixed";
                }

                if (surchargeViewData.SurchargeType == 2)
                {
                    surchargeViewData.SurchargePer = surchargeViewData.SurchargePer;
                    surchargeViewData.SurType = MtdataResource.Percentage;  // "Percentage";
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }
        }

        /// <summary>
        /// ///Purpose          : to find the fee ID
        /// Function Name       : FindFeeId
        /// Created By          : Asif Shafeeque 
        /// Created On          : 29/1/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="defaultViewData"></param>
        [NonAction]
        public void FindFeeId(SurchargeViewData defaultViewData)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (defaultViewData.BookingFeeType == 3)
                {
                    defaultViewData.BookingFee = defaultViewData.BookingFeeFixed;
                    defaultViewData.FeePercentage = defaultViewData.BookingFeePer;
                    defaultViewData.FeeType = MtdataResource.both_percentage_fixed;   // "Both (Percentage and Fixed)";
                }

                if (defaultViewData.BookingFeeType == 1)
                {
                    defaultViewData.BookingFee = defaultViewData.BookingFeeFixed;
                    defaultViewData.FeeType = MtdataResource.Fixed;   //"Fixed";
                }

                if (defaultViewData.BookingFeeType == 2)
                {
                    defaultViewData.FeePercentage = defaultViewData.BookingFeePer;
                    defaultViewData.FeeType = MtdataResource.Percentage;   //"Percentage";
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }
        }

        /// <summary>
        ///Purpose               : to find the fee type
        /// Function Name       :FindFeeType
        /// Created By          :Asif Shafeeque 
        /// Created On          :
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="defaultViewData"></param>
        [NonAction]
        public void FindFeeType(SurchargeViewData defaultViewData)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +                              MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (defaultViewData.BookingFeeType == 1)
                {
                    defaultViewData.BookingFeeFixed = defaultViewData.BookingFee;
                    defaultViewData.BookingFeePer = null;
                }
                if (defaultViewData.BookingFeeType == 2)
                {
                    defaultViewData.BookingFeePer = defaultViewData.FeePercentage;
                    defaultViewData.BookingFeeFixed = null;
                }
                if (defaultViewData.BookingFeeType == 3)
                {
                    defaultViewData.BookingFeeFixed = defaultViewData.BookingFee;
                    defaultViewData.BookingFeePer = defaultViewData.FeePercentage;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }
        }

        /// <summary>
        /// ///Purpose          :to find booking fee
        /// Function Name       : FindBook
        /// Created By          : Naveen
        /// Created On          : 1/20/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="viewData"></param>
        [NonAction]
        public void FindBook(IEnumerable<SurchargeViewData> viewData)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                foreach (SurchargeViewData defaultViewData in viewData)
                {
                    if (defaultViewData.BookingFeeType == 1)
                    {
                        defaultViewData.BookingFee = defaultViewData.BookingFeeFixed;
                    }
                    if (defaultViewData.BookingFeeType == 2)
                    {
                        defaultViewData.FeePercentage = defaultViewData.BookingFeePer;
                    }
                    if (defaultViewData.BookingFeeType == 3)
                    {
                        defaultViewData.BookingFee = defaultViewData.BookingFeeFixed;
                        defaultViewData.FeePercentage = defaultViewData.BookingFeePer;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }
        }

        /// <summary>
        /// ///Purpose           : to find the type of surcharge
        /// Function Name       : FindSurcharge
        /// Created By          : Naveen
        /// Created On          : 21/1/2105
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="viewData"></param>
        [NonAction]
        public void FindSurcharge(IEnumerable<SurchargeViewData> viewData)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                foreach (SurchargeViewData defaultViewData in viewData)
                {
                    if (defaultViewData.SurchargeType == 1)
                    {
                        defaultViewData.Surcharge = defaultViewData.SurchargeFixed;
                    }
                    if (defaultViewData.SurchargeType == 2)
                    {
                        defaultViewData.SurchargePercentage = defaultViewData.SurchargePer;
                    }
                    if (defaultViewData.SurchargeType == 3)
                    {
                        defaultViewData.Surcharge = defaultViewData.SurchargeFixed;
                        defaultViewData.SurchargePercentage = defaultViewData.SurchargePer;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }
        }

        /// <summary>
        /// ///Purpose           Applying page indexing to display the surcharge list
        /// Function Name       : SurchargeListByFiter
        /// Created By          : umesh kumar
        /// Created On          :   2/52015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult SurchargeListByFiter(string name = null, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                int listCount;
                var httpCookie = Request.Cookies["CookieViewAs"];
                int merchantId = 0;
                if (httpCookie != null)
                    merchantId = Convert.ToInt32(httpCookie["merchantId"]);

                FilterSearchParametersDto filterSearch = new FilterSearchParametersDto()
                {
                    Name = name,
                    StartIndex = jtStartIndex,
                    PageSize = jtPageSize,
                    Sorting = jtSorting,
                    MerchantId = merchantId
                };

                var viewDataList = _surchargeService.SurchargeListByFiter(filterSearch);
                var dataList = viewDataList as SurchargeDto[] ?? viewDataList.ToArray();
                FindTypeList(dataList);
                if (string.IsNullOrEmpty(name))
                {
                    listCount = _surchargeService.GetSurchargeDtoList().Count(x => x.MerchantId == merchantId);
                    return Json(new { Result = "OK", Records = dataList, TotalRecordCount = listCount });
                }

                listCount = dataList.ToList().Count();
                return Json(new { Result = "OK", Records = dataList, TotalRecordCount = listCount });
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        /// ///Purpose          : to find the type of list
        /// Function Name       :   FindTypeList
        /// Created By          : Naveen
        /// Created On          :  23/1/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="viewDataList"></param>
        private static void FindTypeList(IEnumerable<SurchargeDto> viewDataList)
        {
            foreach (var surcharge in viewDataList)
            {
                surcharge.SurType = surcharge.SurchargeType.ToString();
                surcharge.FeeType = surcharge.BookingFeeType.ToString();
            }
        }

        /// <summary>
        /// ///Purpose          : Applying page indexing 
        /// Function Name       :  Delete
        /// Created By          : umesh kumar
        /// Created On          :   2/4/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="surId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(int surId)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                _surchargeService.DeleteSurcharge(surId);
                string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                _logTracking.Append("User " + userName + " delete surcharge ID " + surId + "at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                
                Thread.Sleep(50);
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        ///Purpose              : to get the surcharge of merchant
        /// Function Name       : MerchantSurcharges
        /// Created By          :Asif Shafeeque 
        /// Created On          : 1/12/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult MerchantSurcharges()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            return View();
        }

        /// <summary>
        /// ///Purpose           : to display the surcharge
        /// Function Name       :   SurchargeDisplay
        /// Created By          : Asif Shafeeque 
        /// Created On          :  1/14/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SurchargeDisplay()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ResetCredentials()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loginView"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ResetCredentials(LoginViewData loginView)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

            try
            {
                int userId = 0;
                var httpCookie = Request.Cookies["CookieViewAs"];
                var httpCookieUser = Request.Cookies["CookieViewAsUser"];
                if (httpCookie != null)
                {
                    if (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                        userId = Convert.ToInt32(httpCookie["userId"]);
                }

                if (httpCookieUser != null)
                {
                    if (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 2)
                        userId = Convert.ToInt32(httpCookieUser["userId"]);
                }

                bool isValid = true;
                if (string.IsNullOrEmpty(loginView.PCode) && string.IsNullOrEmpty(loginView.ConfirmPCode))
                {
                    ModelState.AddModelError("PCode", MtdataResource.ProfileController_ChangePassword_Please_enter_new_password);
                    ModelState.AddModelError("ConfirmPCode", MtdataResource.ProfileController_ChangePassword_Please_enter_confirm_password);
                    return View();
                }

                if (string.IsNullOrEmpty(loginView.PCode))
                {
                    ModelState.AddModelError("PCode", MtdataResource.ProfileController_ChangePassword_Please_enter_new_password);
                    isValid = false;
                }

                if (string.IsNullOrEmpty(loginView.ConfirmPCode))
                {
                    ModelState.AddModelError("ConfirmPCode", MtdataResource.ProfileController_ChangePassword_Please_enter_confirm_password);
                    isValid = false;
                }

                if (isValid)
                {
                    string olderEmail = _merchantService.GetEmail(userId);
                    string confirmpass = loginView.ConfirmPCode;
                    if (confirmpass == loginView.PCode)
                    {
                        confirmpass = loginView.ConfirmPCode;
                        bool isUpdate = _merchantService.UpdatePassword(userId, confirmpass);
                        if (!isUpdate)
                        {
                            ViewBag.Message = MtdataResource.NewPassword_Match_OldPassword;
                            return View(loginView);
                        }

                        if (isUpdate)
                        {
                            try
                            {
                                _logMessage.Append("Mail Sending begin and reading file ~/HtmlTemplate/password.html");
                                var filename = System.Web.HttpContext.Current.Server.MapPath("~/HtmlTemplate/confirmMail.html");
                                var myFile = new System.IO.StreamReader(filename);
                                string msgfile = myFile.ReadToEnd();
                                var msgBody = new StringBuilder(msgfile);
                                myFile.Close();
                                string cMail = olderEmail;
                                string cName = "Not Specifiend";
                                string cuserName = "";
                                
                                string appUrl = System.Configuration.ConfigurationManager.AppSettings["ApplicationURL"];
                                msgBody.Replace("[##cMail##]", olderEmail);
                                msgBody.Replace("[##cName##]", cName);
                                msgBody.Replace("[##cPassword##]", loginView.PCode);
                                msgBody.Replace("[##cuserName##]", cuserName);
                                msgBody.Replace("[##AppURL##]", appUrl);
                                
                                var sendEmail = new EmailUtil();
                                _logMessage.Append("calling SendMail method of utility class");
                                sendEmail.SendMail(cMail, msgBody.ToString(), cName, "Password Reset");
                                ViewBag.Message = MtdataResource.Password_updated_successfully;
                                
                                string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                                _logTracking.Append("User " + userName + " reset crediantials for " + loginView.UserName + " at " + System.DateTime.Now.ToUniversalTime());
                                _logger.LogInfoMessage(_logTracking.ToString());
                                ViewBag.Message = MtdataResource.Password_updated_successfully;

                                return View();
                            }
                            catch (Exception ex)
                            {
                                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                            }
                        }

                        if (isUpdate)
                        {
                            ViewBag.Message = MtdataResource.Password_updated_successfully;
                            return View();
                        }

                        ViewBag.Message = MtdataResource.Invalid_Information;
                        return View();
                    }

                    ModelState.AddModelError("ConfirmPassword", MtdataResource.Password_Mismatch);
                    return View();
                }

                return View();
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return RedirectToAction("index", "Member");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UserReceipent()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 4) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                int merchantId = 0;
                int userTypeId = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                
                var httpCookie = Request.Cookies["CookieViewAs"];
                if (httpCookie != null)
                    merchantId = Convert.ToInt32(httpCookie["merchantId"]);
                
                if (userTypeId == 2 || userTypeId == 3)
                    merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                
                var receipentDto = _merchantService.GetUserReceipent(merchantId);
                if (receipentDto.Count() == 0)
                {
                    return RedirectToAction("NoRecepient");
                }

                RecipientViewData recipientViewData = new RecipientViewData();
                recipientViewData.MerchantList = receipentDto.ToMerchantViewDataSelectList();
                recipientViewData.ReportList = _merchantService.GetReportList().ToReportList();

                return View(recipientViewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recipient"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UserReceipent(RecipientViewData recipient)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("calling ToRecipientDto method to map RecipientDto to RecipientViewData and retrun RecipientDto");

                var recipientDto = recipient.ToRecipientDto();
                _logMessage.Append("ToRecipientDto method executed successfully");
                recipientDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                int merchantId = 0;
                int userTypeId = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                
                var httpCookie = Request.Cookies["CookieViewAs"];
                if (httpCookie != null)
                    merchantId = Convert.ToInt32(httpCookie["merchantId"]);
                
                if (userTypeId == 2 || userTypeId == 3)
                    merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                
                bool result = _merchantService.UpdateUserRecipient(recipientDto);
                if (result)
                {
                    ViewBag.Message = MtdataResource.Recipient_list_updated;
                    _logMessage.Append("calling GetRecipientList of  _commonService");
                    
                    var receipentDto = _merchantService.GetUserReceipent(merchantId);
                    _logMessage.Append("GetRecipientList of  _commonService executed successfully");
                    _logMessage.Append("calling ToMerchantViewDataSelectList method to map MerchantListViewData to MerchantListDto and retrun MerchantListViewData");
                    
                    var recipientList = receipentDto.ToMerchantViewDataSelectList();
                    _logMessage.Append("ToMerchantViewDataSelectList method executed successfully");

                    ViewBag.saved = "saved successfully.";
                    recipient.ReportList = _merchantService.GetReportList().ToReportList();
                    
                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + "  updated  user recepient at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                    
                    return View(recipient);
                }

                return RedirectToAction("UserReceipent");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult NoRecepient()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 4) return RedirectToAction("index", "Member");
            return View();
        }

        /// <summary>
        /// ///Purpose           : to get fleet setting for update
        /// Function Name       :   FleetSetting
        /// Created By          : Asif Shafeeque 
        /// Created On          :  6/8/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult FleetSetting()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            try
            {
                var httpCookie = Request.Cookies["CookieViewAs"];
                var merchantId = Convert.ToInt32(httpCookie["merchantId"]);
                
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling FleetSetting method of fleetservice class to get fleet setting");

                FleetSettingViewData fleetSettingViewData = new FleetSettingViewData();
                fleetSettingViewData.fleetlist = _fleetService.GetSettingFleetList(merchantId).TofleetSettingViewdataList();
                _logMessage.Append("FleetSetting method executed successfully");
                ViewBag.Status = "Create";
                
                return View(fleetSettingViewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// ///Purpose           : to set fleet setting
        /// Function Name       :   FleetSetting
        /// Created By          : Asif Shafeeque 
        /// Created On          :  6/8/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetSettingViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult FleetSetting(FleetSettingViewData fleetSettingViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var httpCookie = Request.Cookies["CookieViewAs"];
                var merchantId = Convert.ToInt32(httpCookie["merchantId"]);
                fleetSettingViewData.fleetlist = _fleetService.GetSettingFleetList(merchantId).TofleetSettingViewdataList();
                _logMessage.Append("Calling GetSettingFleetList method of fleetservice class to set fleet List");
                
                if (!ModelState.IsValid)
                {
                    ViewBag.Status = "Create";
                    return View(fleetSettingViewData);
                }

                FleetSettingDto fleetSettingDto = fleetSettingViewData.toFleetSettingDto();
                _fleetService.Add(fleetSettingDto);
                _logMessage.Append("FleetSetting method executed successfully");
                
                string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                _logTracking.Append("User " + userName + "  add  fleet setting for fleet " + fleetSettingDto.FleetName + " at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                
                return RedirectToAction("FleetSettingIndex");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            ViewBag.Status = "Create";
            return View();
        }

        /// <summary>
        /// ///Purpose           : to get feet setting
        /// Function Name       :   GetFleetSetting
        /// Created By          : Asif Shafeeque 
        /// Created On          :  6/8/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fleetSettingViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetFleetSetting(FleetSettingViewData fleetSettingViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            try
            {
                var httpCookie = Request.Cookies["CookieViewAs"];
                var merchantId = Convert.ToInt32(httpCookie["merchantId"]);

                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetSettingFleetList method of fleetservice class to get fleet List");

                fleetSettingViewData.fleetlist = _fleetService.GetSettingFleetList(merchantId).TofleetSettingViewdataList();
                _logMessage.Append("fleetlist method executed successfully");
                if (!ModelState.IsValid)
                {
                    _logMessage.Append("Calling fleetlist method of fleetservice class to get fleet List");
                    fleetSettingViewData.fleetlist = _userService.FleetList(merchantId).ToViewDataList();
                    _logMessage.Append("fleetlist method executed successfully");
                    return View("FleetSetting", fleetSettingViewData);
                }

                FleetSettingDto fleetSettingDto = fleetSettingViewData.toFleetSettingDto();
                _fleetService.Add(fleetSettingDto);
                
                string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                _logTracking.Append("User " + userName + "  updated  fleet setting for fleet" + fleetSettingDto.FleetName + " at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                _logMessage.Append("FleetSetting method executed successfully");
                
                return RedirectToAction("FleetSettingIndex");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return RedirectToAction("FleetSettingIndex");
        }

        /// <summary>
        /// ///Purpose           : to display fleet setting index
        /// Function Name       :   FleetSettingIndex
        /// Created By          : Asif Shafeeque 
        /// Created On          :  6/8/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult FleetSettingIndex()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            return View();
        }

        /// <summary>
        /// Purpose             :  To display fleet setting list
        /// Function Name       :  FleetSettingListByFiter
        /// Created By          :  Asif Shafeeque 
        /// Created On          :  6/8/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult FleetSettingListByFiter(string name = null, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling FleetSettingListByFiter method of fleetservice class to get fleet List");

                var httpCookie = Request.Cookies["CookieViewAs"];
                var merchantId = Convert.ToInt32(httpCookie["merchantId"]);

                FilterSearchParametersDto filterSearch = new FilterSearchParametersDto()
                {
                    Name = name.Trim(),
                    StartIndex = jtStartIndex,
                    PageSize = jtPageSize,
                    Sorting = jtSorting,
                    MerchantId = merchantId
                };

                var fleetSettingList = _fleetService.FleetSettingList(filterSearch);
                var count = _fleetService.FleetSettingListCount(filterSearch);
                if (string.IsNullOrEmpty(name))
                {
                    _logMessage.Append("Calling Fleetlist method of _fleetService class to count fee");
                    _logMessage.Append("Fleetlist method of _fleetService executed successfully result = " + _logMessage);

                    return Json(new { Result = "OK", Records = fleetSettingList, TotalRecordCount = count });
                }

                var fleetList = fleetSettingList as FleetSettingDto[] ?? fleetSettingList.ToArray();
                return Json(new { Result = "OK", Records = fleetList, TotalRecordCount = count });
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
                return Json(new { Result = "ERROR", exception.Message });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetFleetSetting(int fleetId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            FleetSettingViewData fleetSettingViewData = new FleetSettingViewData();
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            try
            {
                var httpCookie = Request.Cookies["CookieViewAs"];
                var merchantId = Convert.ToInt32(httpCookie["merchantId"]);

                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetFleetSetting method of fleetservice class takes fleetId= " + fleetId);

                fleetSettingViewData = _fleetService.GetFleetSetting(fleetId).toFleetSettingViewData();
                _logMessage.Append("GetFleetSetting method of fleetservice class executed successfully");
                fleetSettingViewData.fk_FleetID = fleetId;
                fleetSettingViewData.fleetlist = _userService.FleetList(merchantId).ToViewDataList();
                return View("FleetSetting", fleetSettingViewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View("FleetSetting", fleetSettingViewData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteFleetSetting(int fleetId)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return Json(new { Result = "No", Message = "Not Authorized" });
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling DeleteFleetSetting method of fleetservice class takes fleetId= " + fleetId);
                
                _fleetService.DeleteFleetSetting(fleetId);
                string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                _logTracking.Append("User " + userName + "  deleted fleet setting for fleet Id" + fleetId + " at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                _logMessage.Append("DeleteFleetSetting method of fleetservice class executed successfully");
                Thread.Sleep(50);
                return Json(new { Result = "OK" });
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return Json(new { Result = "No" });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reportId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetUsersList(int reportId)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                int merchantId = 0;
                int userTypeId = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                var httpCookie = Request.Cookies["CookieViewAs"];
                if (httpCookie != null)
                    merchantId = Convert.ToInt32(httpCookie["merchantId"]);
                if (userTypeId == 2 || userTypeId == 3)
                    merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                var receipentDto = _merchantService.GetUserReceipent(merchantId, reportId);
                
                RecipientViewData recipientViewData = new RecipientViewData();
                
                if (reportId == 0)
                {
                    receipentDto = _merchantService.GetUserReceipent(merchantId);
                    recipientViewData.MerchantList = receipentDto.ToMerchantViewDataSelectList();
                    recipientViewData.ReportList = _merchantService.GetReportList().ToReportList();
                    return Json(recipientViewData, JsonRequestBehavior.AllowGet);
                }

                receipentDto = _merchantService.GetUserReceipent(merchantId, reportId);
                recipientViewData.MerchantList = receipentDto.ToMerchantViewDataSelectList();
                recipientViewData.ReportList = _merchantService.GetReportList().ToReportList();
             
                return Json(recipientViewData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return Json(null);
        }
 
        [HttpGet]
        public ActionResult ExternalUsers()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

            var httpCookie = Request.Cookies["CookieViewAs"];
            if (httpCookie != null)
            {
                _logMessage.Append("Getting merchant and user id from cookie");
                var merchantId = Convert.ToInt32(httpCookie["merchantId"]);
                string isAllReady = _merchantService.IsAllReadySet(merchantId);
                if (!string.IsNullOrEmpty(isAllReady))
                {
                    LoginViewData lVData = new LoginViewData()
                    {
                        UserName = isAllReady
                    };

                    TempData["IsAllReady"] = isAllReady;
                    TempData.Keep("IsAllReady");

                    return View(lVData);
                }
            }

            TempData["IsAllReady"] = null;
            return View();
        }

        [HttpPost]
        public JsonResult IsAvailale(string userName)
        {
            bool isAvail = _merchantService.IsUserAvailable(userName);
            return Json(isAvail, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UserScheduleReport()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                var httpCookie = Request.Cookies["CookieViewAs"];
                if (httpCookie != null)
                {
                    _logMessage.Append("Getting merchant and user id from cookie");
                    merchantId = Convert.ToInt32(httpCookie["merchantId"]);
                }

                int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                ScheduleReportViewData scheduleReport = new ScheduleReportViewData
                {
                    FrequencyList = FrequencyList(),
                    TimeZones = TimeZoneList(),
                    DayList = WeekDayList(),
                    MonthDayList = MonthDayList(),
                    ReportList = ReportList(merchantId),
                    Frequency = Convert.ToByte(1),
                    MerchantList = _merchantService.GetUserReceipent(merchantId).ToMerchantViewDataSelectList()
                };

                _logMessage.Append("calling FrequencyList method of same class");
                scheduleReport.FrequencyTyeList = FrequencyTypeList();
                _logMessage.Append("FrequencyList methods executed successfully");
                scheduleReport.DateFrom = DateTime.Now.Date;
                scheduleReport.UserType = userType;
                
                return View(scheduleReport);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scheduleReportViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UserScheduleReport(ScheduleReportViewData scheduleViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("index", "Member");

            int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
            var httpCookie = Request.Cookies["CookieViewAs"];
            if (httpCookie != null)
            {
                _logMessage.Append("Getting merchant and user id from cookie");
                merchantId = Convert.ToInt32(httpCookie["merchantId"]);
            }

            try
            {
                if (scheduleViewData.Frequency == 2)
                {
                    if (scheduleViewData.WeekDay == null)
                    {
                        ModelState.AddModelError("WeekDay", MtdataResource.Please_select_week_day);
                    }
                }

                if (scheduleViewData.Frequency == 3)
                {
                    if (scheduleViewData.MonthDay == null)
                    {
                        ModelState.AddModelError("MonthDay", MtdataResource.Please_select_month_day);
                    }
                }

                if (scheduleViewData.DateFrom > scheduleViewData.DateTo)
                {
                    ModelState.AddModelError("DateFrom", MtdataResource.Date_from_validation);
                }

                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (ModelState.IsValid)
                {
                    scheduleViewData.DisplayStartTime = scheduleViewData.StartTime;
                    DateTime dateTime = (DateTime)(DateTime.Now.Date + scheduleViewData.StartTime);
                    string timeZoneID = scheduleViewData.TimeZone;
                    TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneID);
                    DateTime dat = TimeZoneInfo.ConvertTime(dateTime, timeZone);
                    string strTime = dat.TimeOfDay.ToString();
                    TimeSpan ts = TimeSpan.Parse(strTime.Substring(0, 8));
                    scheduleViewData.StartTime = ts;
                    ScheduleReportViewData scheduleView = ClearDay(scheduleViewData);
                    _logMessage.Append("calling ToScheduleDto method to map ScheduleReportDto to ScheduleReportViewData and return ScheduleReportDto");

                    var scheduleDto = scheduleView.ToScheduleDto();
                    _logMessage.Append("ToScheduleDto method executed successfully");
                    
                    scheduleDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                    
                    _logMessage.Append("calling Add method of  _scheduleService takes scheduleDto parameters are " +
                                       scheduleDto.WeekDay + ", " + scheduleDto.ScheduleID + ", " + scheduleDto.MonthDay +
                                       ", " + scheduleDto + ", " + scheduleDto.ModifiedDate + ", " +
                                       scheduleDto.IsCreated + ", " + scheduleDto.Frequency + ", " + scheduleDto.DateTo +
                                       ", " + scheduleDto.CreatedDate);

                    RecipientDto recipientDto = new RecipientDto();
                    recipientDto.ReportId = (int)scheduleViewData.fk_ReportID;
                    recipientDto.MerchantList = scheduleViewData.MerchantList.ToMerchantListDto();
                    scheduleDto.fk_MerchantId = merchantId;

                    ScheduleReportViewData scheduleViewReport = new ScheduleReportViewData();
                    scheduleViewReport = scheduleViewData;
                    scheduleViewReport.StartTime = scheduleViewData.DisplayStartTime;
                    scheduleDto.NextScheduleDateTime = GetNotification(scheduleViewReport).Date + scheduleViewData.DisplayStartTime;
                    bool result = _merchantService.UpdateUserRecipient(recipientDto);
                    _scheduleService.Add(scheduleDto);
                    _logMessage.Append("Add method of  _scheduleService executed successfully");
                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + " add schedule report Id" + scheduleDto.fk_ReportID + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                    return RedirectToAction("Display");
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            _logMessage.Append("calling FrequencyList,  FrequencyTypeListmethod,TimeZoneList method of same class");
            
            scheduleViewData.FrequencyList = FrequencyList();
            scheduleViewData.FrequencyTyeList = FrequencyTypeList();
            scheduleViewData.TimeZones = TimeZoneList();
            scheduleViewData.ReportList = ReportList(merchantId);
            scheduleViewData.DayList = WeekDayList();
            scheduleViewData.MonthDayList = MonthDayList();
            
            _logMessage.Append("FrequencyList,  FrequencyTypeListmethod,TimeZoneList methods executed successfully");
            
            return View(scheduleViewData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UpdateScheduleReport(int scheduleId, int reportId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                var httpCookie = Request.Cookies["CookieViewAs"];
                if (httpCookie != null)
                {
                    _logMessage.Append("Getting merchant and user id from cookie");
                    merchantId = Convert.ToInt32(httpCookie["merchantId"]);
                }

                ScheduleReportViewData scheduleReport = _scheduleService.GetScheduleDto(scheduleId).ToScheduleViewData();
                
                scheduleReport.FrequencyList = FrequencyList();
                scheduleReport.FrequencyTyeList = FrequencyTypeList();
                scheduleReport.TimeZones = TimeZoneList();
                scheduleReport.DayList = WeekDayList();
                scheduleReport.MonthDayList = MonthDayList();
                scheduleReport.ReportList = ReportListReadOnly(); 
                scheduleReport.MerchantList = _merchantService.GetUserReceipent(merchantId, reportId).ToMerchantViewDataSelectList();
                _logMessage.Append("calling FrequencyList method of same class");
                scheduleReport.FrequencyTyeList = FrequencyTypeList();
                _logMessage.Append("FrequencyList methods executed successfully");
                scheduleReport.DateFrom = DateTime.Now.Date;
                scheduleReport.StartTime = scheduleReport.DisplayStartTime;
                
                return View(scheduleReport);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scheduleViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateScheduleReport(ScheduleReportViewData scheduleViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("index", "Member");
            
            int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
            var httpCookie = Request.Cookies["CookieViewAs"];
            if (httpCookie != null)
            {
                _logMessage.Append("Getting merchant and user id from cookie");
                merchantId = Convert.ToInt32(httpCookie["merchantId"]);
            }
            try
            {
                if (scheduleViewData.Frequency == 2)
                {
                    if (scheduleViewData.WeekDay == null)
                    {
                        ModelState.AddModelError("WeekDay", MtdataResource.Please_select_week_day);
                    }
                }

                if (scheduleViewData.Frequency == 3)
                {
                    if (scheduleViewData.MonthDay == null)
                    {
                        ModelState.AddModelError("MonthDay", MtdataResource.Please_select_month_day);
                    }
                }

                if (scheduleViewData.DateFrom > scheduleViewData.DateTo)
                {
                    ModelState.AddModelError("DateFrom", MtdataResource.Date_from_validation);
                }

                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (ModelState.IsValid)
                {
                    scheduleViewData.DisplayStartTime = scheduleViewData.StartTime;
                    DateTime dateTime = (DateTime)(DateTime.Now.Date + scheduleViewData.StartTime);
                    string timeZoneID = scheduleViewData.TimeZone;
                    TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneID);
                    DateTime dat = TimeZoneInfo.ConvertTime(dateTime, timeZone);
                    string strTime = dat.TimeOfDay.ToString();
                    TimeSpan ts = TimeSpan.Parse(strTime.Substring(0, 8));
                    scheduleViewData.StartTime = ts;
                    ScheduleReportViewData scheduleView = ClearDay(scheduleViewData);
                    _logMessage.Append("calling ToScheduleDto method to map ScheduleReportDto to ScheduleReportViewData and return ScheduleReportDto");

                    var scheduleDto = scheduleView.ToScheduleDto();
                    _logMessage.Append("ToScheduleDto method executed successfully");
                    
                    scheduleDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                    
                    _logMessage.Append("calling Add method of  _scheduleService takes scheduleDto parameters are " +
                                       scheduleDto.WeekDay + ", " + scheduleDto.ScheduleID + ", " + scheduleDto.MonthDay +
                                       ", " + scheduleDto + ", " + scheduleDto.ModifiedDate + ", " +
                                       scheduleDto.IsCreated + ", " + scheduleDto.Frequency + ", " + scheduleDto.DateTo +
                                       ", " + scheduleDto.CreatedDate);

                    RecipientDto recipientDto = new RecipientDto();
                    recipientDto.ReportId = (int)scheduleViewData.fk_ReportID;
                    recipientDto.MerchantList = scheduleViewData.MerchantList.ToMerchantListDto();
                    scheduleDto.fk_MerchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);

                    ScheduleReportViewData scheduleViewReport = new ScheduleReportViewData();
                    scheduleViewReport = scheduleViewData;
                    scheduleViewReport.StartTime = scheduleViewData.DisplayStartTime;
                    scheduleDto.NextScheduleDateTime = GetNotification(scheduleViewReport).Date + scheduleViewData.DisplayStartTime;
                    
                    bool result = _merchantService.UpdateUserRecipient(recipientDto);
                    _scheduleService.Update(scheduleDto);
                    
                    _logMessage.Append("Add method of  _scheduleService executed successfully");
                    
                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    
                    _logTracking.Append("User " + userName + " add schedule report Id" + scheduleDto.fk_ReportID + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                    
                    return RedirectToAction("Display");
                }
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            _logMessage.Append("calling FrequencyList,  FrequencyTypeListmethod,TimeZoneList method of same class");
            
            scheduleViewData.FrequencyList = FrequencyList();
            scheduleViewData.FrequencyTyeList = FrequencyTypeList();
            scheduleViewData.TimeZones = TimeZoneList();
            scheduleViewData.ReportList = ReportList(merchantId);
            scheduleViewData.DayList = WeekDayList();
            scheduleViewData.MonthDayList = MonthDayList();
            
            _logMessage.Append("FrequencyList,  FrequencyTypeListmethod,TimeZoneList methods executed successfully");
            
            return View(scheduleViewData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<ReportListViewData> ReportList()
        {
            var reportList = _scheduleService.ReportList();
            var reportViewData = reportList.ToReportList();
            return reportViewData;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<ReportListViewData> ReportListReadOnly()
        {
            var reportList = _scheduleService.ReportListReadOnly();
            var reportViewData = reportList.ToReportList();
            return reportViewData;
        }

        /// <summary>
        ///  Purpose            : To get the frequency list
        /// Function Name       :   FrequencyList
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/3/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<FrequencyListViewData> FrequencyList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("creating frequencyList with assigned FrequencyListViewData ");

                List<FrequencyListViewData> frequencyList = new List<FrequencyListViewData>
                {
                    new FrequencyListViewData{FrequencyId=0,Name="--Select Frequency--"},
                    new FrequencyListViewData {FrequencyId = 1, Name = "Daily"},
                    new FrequencyListViewData {FrequencyId = 2, Name = "Weekly"},
                    new FrequencyListViewData {FrequencyId = 3, Name = "Monthly"}
                };

                _logMessage.Append("returning frequencyList");
                return frequencyList;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

        /// <summary>
        ///  Purpose            : To get the list of types of frequency 
        /// Function Name       :   FrequencyTypeList
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/4/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<FrequencyTypeViewData> FrequencyTypeList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                List<FrequencyTypeViewData> frequencyList = new List<FrequencyTypeViewData>();
                return frequencyList;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

        /// <summary>
        ///  Purpose            : to get the list of time zone
        /// Function Name       :   TimeZoneList
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/4/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public List<TimeZoneViewData> TimeZoneList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("geting GetSystemTimeZones from TimeZoneInfo class and assign it into TimeZoneViewData and creating a list into timeZone");

                var timeZone = TimeZoneInfo.GetSystemTimeZones()
                        .Select(z => new TimeZoneViewData { TimeZoneName = z.DisplayName, TimeZoneTime = z.Id })
                        .ToList();

                _logMessage.Append("list of timeZone created successfully");
                _logMessage.Append("returing list of timezones");

                return timeZone;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

        /// <summary>
        ///  Purpose            : to find the frequency 
        /// Function Name       :   FindFrequency
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/5/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="scheduleViewdata"></param>
        [NonAction]
        public void FindFrequency(ScheduleReportDto scheduleDto)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("assinging frequency to scheduleViewdata");

                switch (scheduleDto.Frequency)
                {
                    case 1:
                        scheduleDto.FrequencyName = "Daily";
                        break;
                    case 2:
                        scheduleDto.FrequencyName = "Weekly";
                        break;
                    case 3:
                        scheduleDto.FrequencyName = "Monthly";
                        break;
                    case 4:
                        scheduleDto.FrequencyName = "Yearly";
                        break;
                }

                _logMessage.Append("frequencies assigned to scheduleViewdata");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
        }

        /// <summary>
        ///  Purpose            : to get the list of week day
        /// Function Name       :   WeekDayList
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/5/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<WeekDayViewData> WeekDayList()
        {
            List<WeekDayViewData> dayList = new List<WeekDayViewData>
            {
                new WeekDayViewData {DayID = 1, DayName = "Sunday"},
                new WeekDayViewData {DayID = 2, DayName = "Monday"},
                new WeekDayViewData {DayID = 4, DayName = "Tuesday"},
                new WeekDayViewData {DayID = 8, DayName = "Wednesday"},
                new WeekDayViewData {DayID = 16, DayName = "Thursday"},
                new WeekDayViewData {DayID = 32, DayName = "Friday"},
                new WeekDayViewData {DayID = 64, DayName = "Saturday"}
            };

            return dayList;
        }

        /// <summary>
        ///  Purpose            : to get the list of month days
        /// Function Name       :   MonthDayList
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/5/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public static IEnumerable<MonthDayViewData> MonthDayList()
        {
            List<MonthDayViewData> monthDayList = new List<MonthDayViewData>();
            for (int index = 1; index <= 31; index++)
            {
                monthDayList.Add(new MonthDayViewData { MonthID = index, MonthName = index.ToString(CultureInfo.InvariantCulture) });
            }

            return monthDayList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<ReportListViewData> ReportList(int merchantId)
        {
            var reportList = _scheduleService.ReportList(merchantId);
            var reportViewData = reportList.ToReportList();
            return reportViewData;
        }

        /// <summary>
        ///  Purpose            : to clear week day and month day
        /// Function Name       :   ClearDay
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/6/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns></returns>
        [NonAction]
        public static ScheduleReportViewData ClearDay(ScheduleReportViewData schedule)
        {
            if (schedule.Frequency == 1)
            {
                schedule.WeekDay = null;
                schedule.MonthDay = null;
            }

            if (schedule.Frequency == 2)
                schedule.MonthDay = null;

            if (schedule.Frequency == 3)
                schedule.WeekDay = null;

            return schedule;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns></returns>
        [NonAction]
        public DateTime GetNotification(ScheduleReportViewData schedule)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            DateTime nextDay = DateTime.Now;
            try
            {
                DateTime dateTime = (DateTime)(schedule.DateFrom + schedule.StartTime);
                string timeZoneID = schedule.TimeZone;
                TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneID);
                DateTime startTime = (DateTime)(schedule.DateFrom + schedule.StartTime);
                DateTime currentTime = DateTime.Now;
                currentTime = TimeZoneInfo.ConvertTime(currentTime, timeZone);
                int result = DateTime.Compare(currentTime, startTime);

                string setfrequency = string.Empty.ToString();

                string containDatefrom = schedule.DateFrom.ToString();
                string containDateTo = schedule.DateTo.ToString();
                DayOfWeek next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), "Sunday", true);
                string day = string.Empty;

                switch (schedule.Frequency)
                {
                    case 1:
                        if (result > 0)//past
                        {
                            if (currentTime.TimeOfDay < (TimeSpan)schedule.StartTime)
                            {
                                setfrequency = "every day";
                                nextDay = currentTime;
                                containDatefrom = nextDay.AddDays(-((int)schedule.FrequencyType)).ToString("MM/dd/yyyy");
                                containDateTo = nextDay.AddDays(Convert.ToDouble(-1)).ToString("MM/dd/yyyy");
                            }
                            if (currentTime.TimeOfDay > (TimeSpan)schedule.StartTime)
                            {
                                setfrequency = "every day";
                                nextDay = currentTime.AddDays(1);
                                containDatefrom = nextDay.AddDays(-((int)schedule.FrequencyType)).ToString("MM/dd/yyyy");
                                containDateTo = nextDay.AddDays(-1).ToString("MM/dd/yyyy");
                            }
                        }

                        if (result <= 0)//future
                        {
                            if ((currentTime.Date == startTime.Date))
                            {
                                if (currentTime.TimeOfDay < (TimeSpan)schedule.StartTime)
                                {
                                    setfrequency = "every day";
                                    nextDay = startTime;
                                    containDatefrom = startTime.AddDays(-((int)schedule.FrequencyType)).ToString("MM/dd/yyyy");
                                    containDateTo = nextDay.AddDays(-1).ToString("MM/dd/yyyy");
                                }
                                if (currentTime.TimeOfDay > (TimeSpan)schedule.StartTime)
                                {
                                    setfrequency = "every day";
                                    nextDay = startTime.AddDays(1);
                                    containDatefrom = startTime.AddDays(-((int)schedule.FrequencyType)).ToString("MM/dd/yyyy");
                                    containDateTo = nextDay.AddDays(-1).ToString("MM/dd/yyyy");
                                }
                            }
                            if ((currentTime.Date < startTime.Date))
                            {
                                setfrequency = "every day";
                                nextDay = startTime;
                                containDatefrom = nextDay.AddDays(-((int)schedule.FrequencyType)).ToString("MM/dd/yyyy");
                                containDateTo = nextDay.AddDays(-1).ToString("MM/dd/yyyy");
                            }
                        }

                        break;
                    case 2:

                        if (result > 0)//past
                        {
                            if (currentTime.TimeOfDay > (TimeSpan)schedule.StartTime)
                            {
                                setfrequency = "every week";
                                day = schedule.WeekName;
                                next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), day, true);
                                currentTime = currentTime.AddDays(1);
                                nextDay = GetNextWeekday(Convert.ToDateTime(currentTime.Date), next);
                                DateTime lastSaturday = nextDay.AddDays(-((int)nextDay.DayOfWeek) - 1);
                                DateTime lastSunday = lastSaturday.AddDays(-6);
                                containDatefrom = lastSunday.AddDays(-Convert.ToDouble(7 * (schedule.FrequencyType - 1))).ToString("MM/dd/yyyy");
                                if (schedule.FrequencyType == 1)
                                    containDatefrom = lastSunday.ToString("MM/dd/yyyy");
                                containDateTo = lastSaturday.ToString("MM/dd/yyyy");
                            }
                            else
                            {
                                setfrequency = "every week";
                                day = schedule.WeekName;
                                next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), day, true);
                                nextDay = GetNextWeekday(Convert.ToDateTime(currentTime.Date), next);
                                DateTime lastSaturday = nextDay.AddDays(-((int)nextDay.DayOfWeek) - 1);
                                DateTime lastSunday = lastSaturday.AddDays(-6);
                                containDatefrom = lastSunday.AddDays(-Convert.ToDouble(7 * (schedule.FrequencyType - 1))).ToString("MM/dd/yyyy");
                                if (schedule.FrequencyType == 1)
                                    containDatefrom = lastSunday.ToString("MM/dd/yyyy");
                                containDateTo = lastSaturday.ToString("MM/dd/yyyy");
                            }
                        }

                        if (result <= 0)//future
                        {
                            if ((currentTime.Date == startTime.Date))
                            {
                                if (currentTime.TimeOfDay > (TimeSpan)schedule.StartTime)
                                {
                                    setfrequency = "every week";
                                    day = schedule.WeekName;
                                    next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), day, true);
                                    currentTime = currentTime.AddDays(1);
                                    nextDay = GetNextWeekday(Convert.ToDateTime(currentTime.Date), next);
                                    DateTime lastSaturday = nextDay.AddDays(-((int)nextDay.DayOfWeek) - 1);
                                    DateTime lastSunday = lastSaturday.AddDays(-6);
                                    containDatefrom = lastSunday.AddDays(-Convert.ToDouble(7 * (schedule.FrequencyType - 1))).ToString("MM/dd/yyyy");
                                    if (schedule.FrequencyType == 1)
                                        containDatefrom = lastSunday.ToString("MM/dd/yyyy");
                                    containDateTo = lastSaturday.ToString("MM/dd/yyyy");
                                }
                                else
                                {
                                    if (currentTime.TimeOfDay < (TimeSpan)schedule.StartTime && currentTime.Date == startTime.Date)
                                    {
                                        setfrequency = "every week";
                                        day = schedule.WeekName;
                                        next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), day, true);
                                        nextDay = startTime;
                                        DateTime lastSaturday = nextDay.AddDays(-((int)nextDay.DayOfWeek) - 1);
                                        DateTime lastSunday = lastSaturday.AddDays(-6);
                                        containDatefrom = lastSunday.AddDays(-Convert.ToDouble(7 * (schedule.FrequencyType - 1))).ToString("MM/dd/yyyy");
                                        if (schedule.FrequencyType == 1)
                                            containDatefrom = lastSunday.ToString("MM/dd/yyyy");
                                        containDateTo = lastSaturday.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        setfrequency = "every week";
                                        day = schedule.WeekName;
                                        next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), day, true);
                                        nextDay = GetNextWeekday(Convert.ToDateTime(currentTime.Date), next);
                                        DateTime lastSaturday = nextDay.AddDays(-((int)nextDay.DayOfWeek) - 1);
                                        DateTime lastSunday = lastSaturday.AddDays(-6);
                                        containDatefrom = lastSunday.AddDays(-Convert.ToDouble(7 * (schedule.FrequencyType - 1))).ToString("MM/dd/yyyy");
                                        if (schedule.FrequencyType == 1)
                                            containDatefrom = lastSunday.ToString("MM/dd/yyyy");
                                        containDateTo = lastSaturday.ToString("MM/dd/yyyy");
                                    }
                                }
                            }

                            if ((currentTime.Date < startTime.Date))
                            {
                                setfrequency = "every week";
                                day = schedule.WeekName;
                                next = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), day, true);
                                nextDay = GetNextWeekday(Convert.ToDateTime(startTime.Date), next);
                                DateTime lastSaturday = nextDay.AddDays(-((int)nextDay.DayOfWeek) - 1);
                                DateTime lastSunday = lastSaturday.AddDays(-6);
                                containDatefrom = lastSunday.AddDays(-Convert.ToDouble(7 * (schedule.FrequencyType - 1))).ToString("MM/dd/yyyy");
                                if (schedule.FrequencyType == 1)
                                    containDatefrom = lastSunday.ToString("MM/dd/yyyy");
                                containDateTo = lastSaturday.ToString("MM/dd/yyyy");
                            }
                        }

                        break;

                    case 3:
                        setfrequency = "every month";
                        int days = (int)schedule.MonthDay;
                        int totalDays = NoOFDays(currentTime);
                        int monthFrequency = (int)schedule.FrequencyType;

                        if (result > 0)//past
                        {
                            if (currentTime.TimeOfDay > (TimeSpan)schedule.StartTime)
                            {
                                if (days <= totalDays)
                                {
                                    if (days > currentTime.Day)
                                    {
                                        nextDay = dateTime;
                                        nextDay = new DateTime(currentTime.Year, currentTime.Month, days) + (TimeSpan)schedule.StartTime;
                                        DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - 1, 1);
                                        containDatefrom = new DateTime(nextDay.Year, nextDay.Month - monthFrequency, 1).ToString("MM/dd/yyyy");
                                        int lastMonthDays = NoOFDays(lastMonth.Date);
                                        containDateTo = new DateTime(nextDay.Year, nextDay.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        nextDay = dateTime;
                                        nextDay = new DateTime(currentTime.Year, currentTime.Month + 1, days) + (TimeSpan)schedule.StartTime;
                                        DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - 1, 1);
                                        containDatefrom = new DateTime(nextDay.Year, nextDay.Month - monthFrequency, 1).ToString("MM/dd/yyyy");
                                        int lastMonthDays = NoOFDays(lastMonth.Date);
                                        containDateTo = new DateTime(nextDay.Year, nextDay.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                                    }
                                }
                            }
                            else// go for next month////
                            {
                                nextDay = dateTime;
                                nextDay = new DateTime(currentTime.Year, currentTime.Month + 1, days) + (TimeSpan)schedule.StartTime;
                                DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - monthFrequency, 1);
                                containDatefrom = new DateTime(nextDay.Year, nextDay.Month - 1, 1).ToString("MM/dd/yyyy");
                                int lastMonthDays = NoOFDays(lastMonth.Date);
                                containDateTo = new DateTime(nextDay.Year, nextDay.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                            }
                        }

                        if (result <= 0) //future
                        {
                            if (days == startTime.Day && currentTime.TimeOfDay < (TimeSpan)schedule.StartTime)
                            {
                                if (days <= totalDays)
                                {
                                    if (days > currentTime.Day)
                                    {
                                        nextDay = dateTime;
                                        nextDay = new DateTime(startTime.Year, currentTime.Month, days) + (TimeSpan)schedule.StartTime;
                                        DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - 1, 1);
                                        containDatefrom = new DateTime(nextDay.Year, startTime.Month - monthFrequency, 1).ToString("MM/dd/yyyy");
                                        int lastMonthDays = NoOFDays(lastMonth.Date);
                                        containDateTo = new DateTime(nextDay.Year, nextDay.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        nextDay = dateTime;
                                        nextDay = startTime;
                                        DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - 1, 1);
                                        containDatefrom = new DateTime(nextDay.Year, startTime.Month - monthFrequency, 1).ToString("MM/dd/yyyy");
                                        int lastMonthDays = NoOFDays(lastMonth.Date);
                                        containDateTo = new DateTime(nextDay.Year, startTime.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                                    }
                                }
                            }
                            else if (days > startTime.Day)
                            {
                                nextDay = dateTime;
                                nextDay = new DateTime(currentTime.Year, startTime.Month, days) + (TimeSpan)schedule.StartTime;
                                DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - 1, 1);
                                containDatefrom = new DateTime(startTime.Year, startTime.Month - monthFrequency, 1).ToString("MM/dd/yyyy");
                                int lastMonthDays = NoOFDays(lastMonth.Date);
                                containDateTo = new DateTime(nextDay.Year, nextDay.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                            }
                            else // go for next month
                            {
                                nextDay = dateTime;
                                nextDay = new DateTime(currentTime.Year, startTime.Month + 1, days) + (TimeSpan)schedule.StartTime;
                                DateTime lastMonth = new DateTime(nextDay.Year, nextDay.Month - 1, 1);
                                containDatefrom = new DateTime(startTime.Year, startTime.Month - monthFrequency, 1).ToString("MM/dd/yyyy");
                                int lastMonthDays = NoOFDays(lastMonth.Date);
                                containDateTo = new DateTime(nextDay.Year, nextDay.Month - 1, lastMonthDays).ToString("MM/dd/yyyy");
                            }
                        }
                        break;
                }

                nextDay = nextDay.Date;
                return nextDay;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return nextDay;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="start"></param>
        /// <param name="day"></param>
        /// <returns></returns>
        public static DateTime GetNextWeekday(DateTime start, DayOfWeek day)
        {
            int daysToAdd = ((int)day - (int)start.DayOfWeek + 7) % 7;
            return start.AddDays(daysToAdd);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static int NoOFDays(DateTime date)
        {
            int month = date.Month;
            int year = date.Year;
            int daysInMonth = DateTime.DaysInMonth(year, month);
            return daysInMonth;
        }

        /// <summary>
        ///  Purpose            : To display the schedule report details
        /// Function Name       :   Display
        /// Created By          :   Asif Shafeeque
        /// Created On          :   11/20/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Display()
        {
            if ((SessionDto)Session["User"] == null) return RedirectToAction("Index", "Member");
            int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
            
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || userType == 4 || userType == 5) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                return View();
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult ScheduleReportListByFiter(string name = null, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling ScheduleReportListByFiter method of adminUserService class to get Admin Users List");

                int adminUserCount;
                name = name.Trim();
                int merchantId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.fk_MerchantID));
                var httpCookie = Request.Cookies["CookieViewAs"];
                if (httpCookie != null)
                {
                    _logMessage.Append("Getting merchant and user id from cookie");
                    merchantId = Convert.ToInt32(httpCookie["merchantId"]);
                }

                FilterSearchParametersDto filterSearch = new FilterSearchParametersDto()
                {
                    Name = name,
                    StartIndex = jtStartIndex,
                    PageSize = jtPageSize,
                    Sorting = jtSorting,
                    MerchantId = merchantId
                };

                var scheduleReport = _scheduleService.ScheduleReportListByFilter(filterSearch, jtSorting);

                foreach (var sc in scheduleReport)
                {
                    sc.StartTime = sc.DisplayStartTime;
                    TimeSpan ts = (TimeSpan)sc.StartTime;
                    int hours = ts.Hours;
                    double min = ts.Minutes;
                    string str = hours.ToString() + ':' + min.ToString("00");
                    sc.TimeInString = str;
                    TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(sc.TimeZone);
                    sc.TimeZone = timeZone.ToString();
                    FindFrequency(sc);
                    if (sc.Frequency == 2)
                    {
                        FindWeekDay(sc);
                    }
                }

                _logMessage.Append("GetAdminUserListByFilter of adminUserService executed successfully");

                if (string.IsNullOrEmpty(name))
                {
                    _logMessage.Append("Calling AdminUserListCount method of adminUserService class to count adminUsers");
                    adminUserCount = scheduleReport.ToList().Count;
                    _logMessage.Append("AdminUserListCount method of adminUserService executed successfully result = " + adminUserCount);

                    return Json(new { Result = "OK", Records = scheduleReport, TotalRecordCount = adminUserCount });
                }

                var userDtos = scheduleReport as ScheduleReportDto[] ?? scheduleReport.ToArray();
                adminUserCount = userDtos.ToList().Count();
                return Json(new { Result = "OK", Records = userDtos, TotalRecordCount = adminUserCount });
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        ///  Purpose            : To find week day
        /// Function Name       :   FindWeekDay
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/6/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="scheduleViewdata"></param>
        [NonAction]
        public static void FindWeekDay(ScheduleReportDto scheduleDto)
        {
            switch (scheduleDto.WeekDay)
            {
                case 1:
                    scheduleDto.WeekName = "Sunday";
                    break;
                case 2:
                    scheduleDto.WeekName = "Monday";
                    break;
                case 4:
                    scheduleDto.WeekName = "Tuesday";
                    break;
                case 8:
                    scheduleDto.WeekName = "Wednesday";
                    break;
                case 16:
                    scheduleDto.WeekName = "Thursday";
                    break;
                case 32:
                    scheduleDto.WeekName = "Friday";
                    break;
                case 64:
                    scheduleDto.WeekName = "Saturday";
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public JsonResult DeleteReport(int ScheduleID)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return Json(new { Result = "No", Message = "Not Authorized" });
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling Delete method of scheduleService class takes ScheduleID= " + ScheduleID);

                _scheduleService.Delete(ScheduleID);
                _logMessage.Append("Delete method of scheduleService class executed successfully");

                string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                
                _logTracking.Append("User " + userName + " delete schedule Id " + ScheduleID + " at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                
                return Json(new { Result = "OK" });
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return Json(new { Result = "No" });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loginView"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExternalUsers(LoginViewData loginView)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return RedirectToAction("index", "Member");

            try
            {
                int merchantId = 0;
                var httpCookie = Request.Cookies["CookieViewAs"];
                if (httpCookie != null)
                {
                    _logMessage.Append("Getting merchant and user id from cookie");
                      merchantId = Convert.ToInt32(httpCookie["merchantId"]);
                }

                bool isValid = true;
                if (string.IsNullOrEmpty(loginView.PCode) && string.IsNullOrEmpty(loginView.ConfirmPCode))
                {
                    ModelState.AddModelError("PCode", MtdataResource.ProfileController_ChangePassword_Please_enter_new_password);
                    ModelState.AddModelError("ConfirmPCode", MtdataResource.ProfileController_ChangePassword_Please_enter_confirm_password);
                    return View();
                }

                if (string.IsNullOrEmpty(loginView.PCode))
                {
                    ModelState.AddModelError("PCode", MtdataResource.ProfileController_ChangePassword_Please_enter_new_password);
                    isValid = false;
                }

                if (string.IsNullOrEmpty(loginView.ConfirmPCode))
                {
                    ModelState.AddModelError("ConfirmPCode", MtdataResource.ProfileController_ChangePassword_Please_enter_confirm_password);
                    isValid = false;
                }

                if (isValid)
                {
                    string confirmpass = loginView.ConfirmPCode;
                    if (confirmpass == loginView.PCode)
                    {
                        confirmpass = loginView.ConfirmPCode;
                        bool isUpdate = _merchantService.UpdateTCPassword(merchantId, loginView.UserName, confirmpass);
                        if (!isUpdate)
                        {
                            ViewBag.Message = MtdataResource.NewPassword_Match_OldPassword;
                            return View(loginView);
                        }

                        if (isUpdate)
                        {
                            ViewBag.Message = MtdataResource.Password_set_successfully;
                            return View();
                        }

                        ViewBag.Message = MtdataResource.Invalid_Information;
                        return View();
                    }

                    ModelState.AddModelError("ConfirmPassword", MtdataResource.Password_Mismatch);
                    return View();
                }

                return View();
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return RedirectToAction("index", "Member");
        }
    }
}