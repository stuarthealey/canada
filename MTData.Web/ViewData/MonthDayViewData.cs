﻿namespace MTData.Web.ViewData
{
    public class MonthDayViewData
    {
        public int MonthID { get; set; }
        public string MonthName { get; set; }
    }
}