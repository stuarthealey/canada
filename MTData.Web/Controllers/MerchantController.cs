﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Configuration;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.Web.Resource;
using MTData.Web.ViewData;
using MTData.Web.Interface;

namespace MTData.Web.Controllers
{
    public class MerchantController : Controller
    {
        readonly ILogger _logger = new Logger();
        private readonly IMerchantService _merchantService;
        private readonly ICommonService _commonService;
        private readonly IMemberService _loginService;
        private readonly IUserService _userService;
        private StringBuilder _logMessage;
        private readonly IDefaultService _defaultService;
        private StringBuilder _logTracking;

        public MerchantController([Named("MerchantService")] IMerchantService merchantService, [Named("CommonService")] ICommonService commonService, [Named("LoginService"),] IMemberService loginService, [Named("UserService")] IUserService userService, StringBuilder logMessage, [Named("DefaultService")] IDefaultService defaultService, StringBuilder logTracking)
        {
            _loginService = loginService;
            _commonService = commonService;
            _userService = userService;
            _logMessage = logMessage;
            _merchantService = merchantService;
            _defaultService = defaultService;
            _logTracking = logTracking;
        }

        public int UserId;

        /// <summary>
        /// Purpose             :   to update the merchant on the basis of its ID
        /// Function Name       :   Update
        /// Created By          :   Asif Shafeeque 
        /// Created On          :    12/18/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Update(int merchantId, int userId)
        {
            if (Session["User"] == null)
                return RedirectToAction("Index", "Member");

            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("Index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                PersonalDetails pds = new PersonalDetails();
                pds.Mid = merchantId;
                pds.UserID = userId;

                return RedirectToAction("PersonalInfo", pds);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);

                return View();
            }
        }

        /// <summary>
        /// Purpose             :  to get the list of country
        /// Function Name       :  CountryList
        /// Created By          :  Asif Shafeeque 
        /// Created On          : 1/6/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<CountryViewData> CountryList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetCountryList");

                var countryDtoList = _commonService.GetCountryList();

                _logMessage.Append("calling ToCountryViewDataList");

                var countryViewDataList = countryDtoList.ToCountryViewDataList();

                _logMessage.Append("converting countryViewDataList into objcountrylist");

                List<CountryViewData> objcountrylist = countryViewDataList.ToList();

                _logMessage.Append("convert countryViewDataList into objcountrylist successful");

                var li = new List<SelectListItem> { new SelectListItem { Text = MtdataResource.Please_Select_a_Country, Value = "0" } };
                li.AddRange(objcountrylist.Select(item => new SelectListItem { Text = item.Title, Value = Convert.ToString(item.CountryCodesID) }));

                _logMessage.Append("returning country list");

                return objcountrylist;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);

                return null;
            }
        }

        /// <summary>
        /// Purpose             :  to send email after successful login
        /// Function Name       :  TakeControl
        /// Created By          :  Asif Shafeeque 
        /// Created On          : 8/1/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="userEmail"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ActionResult TakeControl(string userEmail, string userId, int? userType = null)
        {
            if (Session["User"] == null || Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("Index", "Member");

            try
            {
                var sessionDto = ((SessionDto)Session["User"]);
                int logbyUserId = sessionDto.SessionUser.UserID;
                int logbyType = sessionDto.SessionUser.fk_UserTypeID;
                int parentLogOnUserType = sessionDto.SessionUser.LogOnByUserType;
                int parentLogOnUserId = sessionDto.SessionUser.LogOnByUserId;

                if (parentLogOnUserType == 5 && logbyType == 2)
                {
                    logbyUserId = parentLogOnUserId;
                    logbyType = parentLogOnUserType;
                }

                _logTracking.Append("User " + ((SessionDto)Session["User"]).SessionUser.Email + " attempting to Log on as " + userEmail + " at " + System.DateTime.Now.ToUniversalTime() + " attempt");

                Session.Remove("User");

                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                
                var loginView = new LoginViewData { UserName = userId, PCode = userEmail };
                var loginDto = ViewDataExtention.CreateLoginDto(loginView);
                 
                _logMessage.Append("Calling LoginAsAdmin. User Email:" + User + ";UserID:" + userId);

                var user = _loginService.LoginAsAdmin(loginDto);

                _logMessage.Append("LoginAsAdmin method of loginService class executed successfully");

                if (user != null)
                {
                    user.SessionUser.LogOnByUserId = logbyUserId;
                    user.SessionUser.LogOnByUserType = logbyType;

                    _logTracking.Append(" Successful");
                    _logger.LogInfoMessage(_logTracking.ToString());

                    Session["User"] = user;
                    if (user.SessionUser.fk_UserTypeID == 5 && userType != null)
                        return Redirect("~/MyMerchants/Index");

                    if (user.SessionUser.fk_UserTypeID == 1)
                        return Redirect("Index");

                    if (user.SessionUser.fk_UserTypeID == 4)
                    {
                        new UserViewData { UserRoles = _userService.GetUserRoles(user.SessionUser.fk_UserTypeID).ToViewDataList() };
                        return Redirect("~/Terminal/Index");
                    }

                    if (user.SessionUser.fk_UserTypeID == 5)
                    {
                        user.SessionUser.fk_UserTypeID = 2;
                        return RedirectToAction("index", "Fleet");
                    }

                    return RedirectToAction("index", "Fleet");
                }

                _logTracking.Append(" failed");
                _logger.LogInfoMessage(_logTracking.ToString());
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Purpose             :  to view merchant
        /// Function Name       :  ViewMerchant
        /// Created By          :  Asif Shafeeque 
        /// Created On          : 13/1/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="userId"></param>
        /// <param name="merchantName"></param>
        /// <returns></returns>
        public ActionResult ViewMerchant(string merchantId, string userId, string merchantName)
        {
            if (Session["User"] == null)
                return RedirectToAction("Index", "Member");

            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("Index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("setting cookies for the merchant");

                var cookie = new HttpCookie("CookieViewAs");
                cookie.HttpOnly = true;
                cookie.Values.Add("merchantId", merchantId);
                cookie.Values.Add("userId", userId);
                cookie.Values.Add("merchantName", merchantName);
                Response.Cookies.Add(cookie);

                _logMessage.Append("setting cookies for the merchant successfully");

                var httpCookie = Response.Cookies["CookieViewAs"];
                if (httpCookie != null)
                    httpCookie.Expires = DateTime.Now.AddDays(1);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return RedirectToAction("Index", "ViewAs");
        }

        /// <summary>
        /// Purpose             : apply page indexing on merchant list
        /// Function Name       : ViewMerchant
        /// Created By          : umesh
        /// Created On          :2/5/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult MerchantListByFiter(string name = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetMerchantCountByFilter method of _merchantService class to count Merchant");

                int merchantUserListCount;
                name = name.Trim();

                var merchantUser = _merchantService.GetMerchantByFilter(name, jtStartIndex, jtPageSize, jtSorting);
                if (string.IsNullOrEmpty(name))
                {
                    _logMessage.Append("Calling MerchantList");

                    merchantUserListCount = _merchantService.MerchantList(name).ToList().Count;

                    _logMessage.Append("MerchantList result = " + merchantUserListCount);

                    return Json(new { Result = "OK", Records = merchantUser, TotalRecordCount = merchantUserListCount });
                }

                var merchantUserDtos = merchantUser as MerchantUserDto[] ?? merchantUser.ToArray();
                merchantUserListCount = _merchantService.MerchantList(name).ToList().Count;

                _logMessage.Append("GetMerchantByFilter executed successfully");
                _logger.LogInfoMessage(_logMessage.ToString());

                return Json(new { Result = "OK", Records = merchantUserDtos, TotalRecordCount = merchantUserListCount });
            }
            catch (Exception ex)
            {
                _logger.LogException(ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        ///  Purpose             :  To find out merchant is locked or not 
        /// Function Name       : Islocked
        /// Created By          : umesh
        /// Created On          : 23/1/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Islocked(int id, bool status)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || Session["User"] == null)
                return RedirectToAction("Index", "Member");

            var sessionData = ((SessionDto)Session["User"]);
            if (status)
            {
                _logTracking.Append("User " + sessionData.SessionUser.Email + " attempting to locked " + id + " at " + System.DateTime.Now.ToUniversalTime() + " attempt");

                if (sessionData.SessionUser.UserID == id)
                {
                    string js = @"document.getElementById(" + id + ").checked = false; alert('Cant locked currently logged in user!');";
                    return JavaScript(js);       
                }
            }

            _logTracking.Append("User " + sessionData.SessionUser.Email + " attempting to Unlocked " + id + " at " + System.DateTime.Now.ToUniversalTime() + " attempt");

            bool isLocked = _commonService.IsLockedMember(id, status);
            if (isLocked)
            {
                _logTracking.Append("Successful");
                _logger.LogInfoMessage(_logTracking.ToString());
                return RedirectToAction("Index");
            }

            _logTracking.Append(" Faild");
            _logger.LogInfoMessage(_logTracking.ToString());

            return RedirectToAction("Index");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [CustomActionFilter]
        public ActionResult Index()
        {
            IsFirstLoginViewData obj = new IsFirstLoginViewData();
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Session["User"] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("Index", "Member");

            if (Session["AllInfo"] != null)
                Session.Remove("AllInfo");

            if (!(bool)Session["Islogged"])
            {
                obj.IsFirstLogin = (bool)((SessionDto)Session["User"]).SessionUser.IsFirstLogin;
                obj.UserTypeId = Convert.ToString(((SessionDto)Session["User"]).SessionUser.fk_UserTypeID);
                Session.Remove("Islogged");
                Session["Islogged"] = true;
                obj.UserId = ((SessionDto)Session["User"]).SessionUser.UserID;
                return View(obj);
            }

            obj.UserId = ((SessionDto)Session["User"]).SessionUser.UserID;
            return View(obj);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pdetails"></param>
        /// <returns></returns>
        [HttpGet]
        public ViewResult PersonalInfo(PersonalDetails pdetails)
        {
            if (Session["AllInfo"] != null)
            {
                pdetails.Mid = Convert.ToInt32((((MerchantUserViewData)Session["AllInfo"]).MerchantID));
                pdetails.UserID = Convert.ToInt32((((MerchantUserViewData)Session["AllInfo"]).UserID));
            }

            //if (pdetails.isPersonalUpdate == 0) //zzz Chetu 357 - This is crap code and stops the 'Edit' of a Merchant from the merchant list page.
            if (pdetails.UserID == 0 && pdetails.Mid == 0)
            {
                pdetails.FdMerchantList = GetFdMerchantList();
                TempData["status"] = "Create";
                TempData.Keep("status");

                RemoveAllInfo();

                return View(pdetails);
            }
            else
            {
                MerchantUserViewData muserObj = GetBaseInfo();

                var mvData = new MerchantUserViewData();
                if (!muserObj.IsFirstTime)
                {
                    mvData.IsOtherDetailSaved = false;
                    MerchantUserDto mrMerchantDto = _merchantService.GetMerchant(pdetails.Mid, pdetails.UserID);
                    mvData = mrMerchantDto.ToViewData();
                    mvData.UserRoles = _merchantService.GetUserRoles().ToViewDataList();
                }
                else
                {
                    mvData = GetBaseInfo();
                    mvData.IsOtherDetailSaved = true;
                }

                ViewData.Model = mvData;
                var countryDtoList = _commonService.GetCountryList();
                mvData.CountryList = countryDtoList.ToCountryViewDataList();

                UserId = mvData.UserID;

                Session["AllInfo"] = mvData;

                PersonalDetails pd = new PersonalDetails();
                pd.Company = mvData.Company;
                pd.CompanyTaxNumber = mvData.CompanyTaxNumber;
                pd.FName = mvData.FName;
                pd.LName = mvData.LName;
                pd.Email = mvData.Email;
                pd.isPersonalUpdate = 1;
                pd.Mid = pdetails.Mid;

                pd.Phone = mvData.Phone;

                pd.UseTransArmor = mvData.UseTransArmor;        //PAY-13

                if (mvData.Fk_FDId != null)
                    pd.Fk_FDId = (int)mvData.Fk_FDId;

                TempData["status"] = "Update";
                TempData.Keep("status");

                return View(pd);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult AddressInfo()
        {
            AddressDetails objUserViewData = new AddressDetails
            {
                TimeZones = TimeZoneList(),
                CountryList = CountryList(),
                Country = "AU"
            };

            MerchantUserViewData muvd = GetBaseInfo();
            objUserViewData.Country = muvd.Country;

            if (string.IsNullOrEmpty(muvd.Country))
            {
                objUserViewData.Country = "AU";
            }

            objUserViewData.State = muvd.State;
            objUserViewData.City = muvd.City;
            objUserViewData.Address = muvd.Address;
            objUserViewData.TimeZone = muvd.TimeZone;
            objUserViewData.ZipCode = muvd.ZipCode;

            return View(objUserViewData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult OtherDetails()
        {
            if (Session["AllInfo"] != null)
            {
                MerchantUserViewData muvd = GetBaseInfo();

                if (muvd.UserID != 0 || muvd.IsOtherDetailSaved)
                {
                    TaxRateOther oldDetails = new TaxRateOther();

                    oldDetails.FederalTaxRate = muvd.FederalTaxRate;
                    oldDetails.StateTaxRate = muvd.StateTaxRate;
                    oldDetails.TipPerLow = muvd.TipPerLow;
                    oldDetails.TipPerMedium = muvd.TipPerMedium;
                    oldDetails.TipPerHigh = muvd.TipPerHigh;
                    oldDetails.isTaxInclusive = muvd.IsTaxInclusive;
                    oldDetails.UserRoles = _merchantService.GetUserRoles().ToViewDataList();
                    oldDetails.Disclaimer = muvd.Disclaimer;
                    oldDetails.TermCondition = muvd.TermCondition;
                    oldDetails.IsJobNoRequired = muvd.IsJobNoRequired;

                    var roles = string.Join(", ", muvd.RolesAssigned);
                    oldDetails.RolesAssigned = roles;

                    return View(oldDetails);
                }
                else
                {
                    TaxRateOther objUserViewData = new TaxRateOther();
                    objUserViewData.isPersonalUpdate = 1;
                    objUserViewData.UserRoles = _merchantService.GetUserRoles().ToViewDataList();

                    var roles = string.Join(", ", muvd.RolesAssigned);
                    objUserViewData.RolesAssigned = roles;

                    if (objUserViewData.TipPerHigh > 0)
                    {
                        objUserViewData.Disclaimer = muvd.Disclaimer;
                        objUserViewData.TermCondition = muvd.TermCondition;
                    }
                    else
                    {
                        var defaultDto = _defaultService.GetDefaultDto(1);
                        objUserViewData.FederalTaxRate = defaultDto.FederalStateRate;
                        objUserViewData.StateTaxRate = defaultDto.StateTaxRate;
                        objUserViewData.TipPerLow = defaultDto.TipLow;
                        objUserViewData.TipPerMedium = defaultDto.TipMedium;
                        objUserViewData.TipPerHigh = defaultDto.TipHigh;
                        objUserViewData.isTaxInclusive = muvd.IsTaxInclusive;
                        var termDto = _commonService.GetTermDto(4);
                        var termViewData = termDto.ToTermViewData();
                        objUserViewData.TCondition = termViewData;
                        objUserViewData.Disclaimer = objUserViewData.TCondition.Disclaimer;
                        objUserViewData.TermCondition = objUserViewData.TCondition.TermCondition;
                        objUserViewData.UserRoles = _merchantService.GetUserRoles().ToViewDataList();

                        roles = string.Join(", ", objUserViewData.RolesAssigned);
                        objUserViewData.RolesAssigned = roles;
                    }

                    return View(objUserViewData);
                }
            }

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="detailsData"></param>
        /// <param name="btnPrevious"></param>
        /// <param name="btnNext"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PersonalInfo(PersonalDetails detailsData, string btnNext)
        {
            if (btnNext != null)
            {
                detailsData.Phone = detailsData.Phone.Trim();
                if (ModelState.IsValid)
                {
                    MerchantUserViewData merchantObj = GetBaseInfo();
                    merchantObj.Company = detailsData.Company;
                    merchantObj.CompanyTaxNumber = detailsData.CompanyTaxNumber;
                    merchantObj.FName = detailsData.FName;
                    merchantObj.Fk_FDId = detailsData.Fk_FDId;
                    merchantObj.LName = detailsData.LName;
                    merchantObj.Email = detailsData.Email;
                    merchantObj.Phone = detailsData.Phone;

                    merchantObj.UseTransArmor = detailsData.UseTransArmor;      //PAY-13

                    return RedirectToAction("AddressInfo");
                }

                if (detailsData.isPersonalUpdate == 1)
                {
                    TempData["status"] = "Update";
                    return View(detailsData);
                }

                TempData["status"] = "Create";
            }

            return View(detailsData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="addrssDetails"></param>
        /// <param name="btnPrevious"></param>
        /// <param name="btnNext"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddressInfo(AddressDetails addrssDetails, string btnPrevious, string btnNext)
        {
            MerchantUserViewData muserObj = GetBaseInfo();

            if (btnPrevious != null)
            {
                PersonalDetails detailsObj = new PersonalDetails();
                detailsObj.Company = muserObj.Company;
                detailsObj.CompanyTaxNumber = muserObj.CompanyTaxNumber;
                detailsObj.FName = muserObj.FName;
                detailsObj.LName = muserObj.LName;
                detailsObj.Email = muserObj.Email;
                detailsObj.Phone = muserObj.Phone;

                detailsObj.UseTransArmor = muserObj.UseTransArmor;      //PAY-13

                return RedirectToAction("PersonalInfo", detailsObj);
            }

            if (btnNext != null)
            {
                muserObj.Country = addrssDetails.Country;
                muserObj.State = addrssDetails.State;
                muserObj.City = addrssDetails.City;
                muserObj.Address = addrssDetails.Address;
                muserObj.TimeZone = addrssDetails.TimeZone;
                muserObj.ZipCode = addrssDetails.ZipCode;

                return RedirectToAction("OtherDetails");
            }

            AddressDetails objUserViewData = new AddressDetails
            {
                TimeZones = TimeZoneList(),
                CountryList = CountryList(),
                Country = "AU"
            };

            return View(objUserViewData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tro"></param>
        /// <param name="btnPrevious"></param>
        /// <param name="btnNext"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult OtherDetails(TaxRateOther tro, string btnPrevious, string btnNext)
        {
            MerchantUserViewData muserObj = GetBaseInfo();
            muserObj.UserRoles = _merchantService.GetUserRoles().ToViewDataList();
            
            if (btnPrevious != null)
            {
                AddressDetails detailsObj = new AddressDetails();
                detailsObj.CountryList = CountryList();
                detailsObj.TimeZones = TimeZoneList();
                detailsObj.Country = muserObj.Country;
                detailsObj.State = muserObj.State;
                detailsObj.City = muserObj.City;
                detailsObj.Address = muserObj.Address;
                detailsObj.TimeZone = muserObj.TimeZone;
                detailsObj.ZipCode = muserObj.ZipCode;

                return RedirectToAction("AddressInfo", detailsObj);
            }

            if (btnNext != null)
            {
                if (ModelState.IsValid)
                {
                    muserObj.FederalTaxRate = tro.FederalTaxRate;
                    muserObj.StateTaxRate = tro.StateTaxRate;
                    muserObj.TipPerLow = tro.TipPerLow;
                    muserObj.TipPerMedium = tro.TipPerMedium;
                    muserObj.TipPerHigh = tro.TipPerHigh;
                    muserObj.IsTaxInclusive = tro.isTaxInclusive;
                    muserObj.IsJobNoRequired = tro.IsJobNoRequired;
                    muserObj.TermCondition = tro.TermCondition;
                    muserObj.Disclaimer = tro.Disclaimer;
                    muserObj.UserRoleIdList = tro.UserRoleIdList;
                    muserObj.DisclaimerPlainText = tro.DisclaimerPlainText;
                    muserObj.TermConditionPlainText = tro.TermConditionPlainText;
                    
                    var roles = string.Join(", ", tro.UserRoleIdList);
                    muserObj.RolesAssigned = roles;
                    muserObj.IsOtherDetailSaved = true;
                    muserObj.IsFirstTime = true;
                    
                    if (muserObj.UserID != 0)
                    {
                        return RedirectToAction("RcMerchant");
                    }
                    else
                    {
                        try
                        {
                            var res = _commonService.IsExist(muserObj.Email);
                            if (res)
                            {
                                muserObj.FederalTaxRate = tro.FederalTaxRate;
                                muserObj.StateTaxRate = tro.StateTaxRate;
                                muserObj.TipPerLow = tro.TipPerLow;
                                muserObj.TipPerMedium = tro.TipPerMedium;
                                muserObj.TipPerHigh = tro.TipPerHigh;
                                muserObj.IsTaxInclusive = tro.isTaxInclusive;
                                muserObj.TermCondition = tro.TermCondition;
                                muserObj.Disclaimer = tro.Disclaimer;
                                muserObj.UserRoleIdList = tro.UserRoleIdList;
                                roles = string.Join(", ", tro.UserRoleIdList);
                                muserObj.RolesAssigned = roles;
                                muserObj = GetBaseInfo();
                                muserObj.Disclaimer = tro.Disclaimer;
                                muserObj.TermCondition = tro.TermCondition;
                                TempData["ExitUser"] = "Account for '" + muserObj.Email + "' already exists please contact to your administrator";
                                
                                return RedirectToAction("PersonalInfo", muserObj);
                            }

                            return RedirectToAction("RcMerchant");
                        }
                        catch (Exception exception)
                        {
                            _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, "{0} Method Not executed successfully.", MethodBase.GetCurrentMethod().Name) + "; Method Result:- False;");
                            _logger.LogInfoFatal(_logMessage.ToString(), exception);
                        }

                        RemoveAllInfo();

                        return View();
                    }
                }
                else
                {
                    TaxRateOther objUserViewData = new TaxRateOther
                    {
                        UserRoles = _merchantService.GetUserRoles().ToViewDataList()
                    };

                    var termDto = _commonService.GetTermDto(4);
                    var termViewData = termDto.ToTermViewData();
                    objUserViewData.TCondition = termViewData;
                    objUserViewData.Disclaimer = objUserViewData.TCondition.Disclaimer;
                    objUserViewData.TermCondition = objUserViewData.TCondition.TermCondition;

                    return View(objUserViewData);
                }
            }

            return View("Index");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private MerchantUserViewData GetBaseInfo()
        {
            if (Session["AllInfo"] == null)
                Session["AllInfo"] = new MerchantUserViewData();

            return (MerchantUserViewData)Session["AllInfo"];
        }

        /// <summary>
        /// 
        /// </summary>
        private void RemoveAllInfo()
        {
            Session.Remove("AllInfo");
        }

        /// <summary>
        ///  Purpose            : To get the list fo fd merchants
        /// Function Name       : FDIndex
        /// Created By          : Naveen Kumar
        /// Created On          : 04/15/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>    
        /// <returns></returns>
        [HttpGet]
        public ActionResult FdIndex()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult RcMerchant()
        {
            MerchantUserViewData muvd = GetBaseInfo();
            FDMerchantViewData fvd = new FDMerchantViewData();
            string project = ConfigurationManager.AppSettings["Project"];
            ViewBag.Project = project;

            if (muvd != null)
            {
                fDMerchantDto fdMerchant = _merchantService.FdMerchantExist(Convert.ToInt32(muvd.Fk_FDId));
                if (fdMerchant != null)
                {
                    fvd.GroupId = fdMerchant.GroupId;
                    fvd.FDMerchantID = fdMerchant.FDMerchantID;
                    fvd.TokenType = fdMerchant.TokenType;
                    fvd.MCCode = fdMerchant.MCCode;
                    fvd.EncryptionKey = fdMerchant.EncryptionKey;
                    fvd.ProjectType = fdMerchant.ProjectType;
                   
                    return View(fvd);
                }
            }

            return View(fvd);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fdMerchantView"></param>
        /// <param name="btnPrevious"></param>
        /// <param name="btnNext"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RcMerchant(FDMerchantViewData fdMerchantView, string btnPrevious, string btnNext)
        {
            if (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1 && ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 5)
                return RedirectToAction("Index", "Member");

            if (btnPrevious != null)
            {
                MerchantUserViewData muserObj = GetBaseInfo();
                TaxRateOther tOther = new TaxRateOther();

                tOther.FederalTaxRate = muserObj.FederalTaxRate;
                tOther.StateTaxRate = muserObj.StateTaxRate;
                tOther.TipPerLow = muserObj.TipPerLow;
                tOther.TipPerMedium = muserObj.TipPerMedium;
                tOther.TipPerHigh = muserObj.TipPerHigh;
                tOther.isTaxInclusive = muserObj.IsTaxInclusive;
                tOther.TermCondition = muserObj.TermCondition;
                tOther.Disclaimer = muserObj.Disclaimer;
                tOther.UserRoleIdList = muserObj.UserRoleIdList;
                string roles = string.Join(", ", tOther.UserRoleIdList);
                tOther.RolesAssigned = roles;
                tOther.Disclaimer = null;
                tOther.TermCondition = null;

                return RedirectToAction("OtherDetails", tOther);
            }

            if (btnNext != null)
            {
                try
                {
                    MerchantUserViewData muserObj = GetBaseInfo();
                    string roles;
                    MerchantUserDto merchantDto = muserObj.ToDTO();

                    var sDto = (SessionDto)Session["User"];
                    merchantDto.fk_MerchantID = sDto.SessionUser.fk_MerchantID;
                    merchantDto.CreatedBy = sDto.SessionUser.Email;
                    merchantDto.ModifiedBy = sDto.SessionUser.Email;

                    int id = muserObj.MerchantID;

                    merchantDto.GropuId = fdMerchantView.GroupId;
                    merchantDto.RapidConnect = fdMerchantView.FDMerchantID;
                    merchantDto.TokenType = fdMerchantView.TokenType;
                    merchantDto.MCCode = fdMerchantView.MCCode;
                    merchantDto.EncryptionKey = fdMerchantView.EncryptionKey;

                    if (muserObj.UserID != 0)
                    {
                        if (ConfigurationManager.AppSettings["Project"] == MtdataResource.CanadaProject)
                        {
                            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                _logger.LogInfoMessage(string.Format("MerchantController.RcMerchant-1. EcommProjectId:{0};", ConfigurationManager.AppSettings["CnEcommProjectId"]));

                            merchantDto.EcommProjectId = ConfigurationManager.AppSettings["CnEcommProjectId"];
                            merchantDto.ProjectType = 1;
                        }
                        else
                        {
                            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                _logger.LogInfoMessage(string.Format("MerchantController.RcMerchant-2. ProjectType:{0};", fdMerchantView.ProjectType));

                            switch (fdMerchantView.ProjectType)
                            {
                                case 1:
                                    merchantDto.EcommProjectId = ConfigurationManager.AppSettings["EcommProjectId"];
                                    merchantDto.ProjectType = Convert.ToByte(fdMerchantView.ProjectType);
                                    break;
                                case 2:
                                    merchantDto.ProjectId = ConfigurationManager.AppSettings["ProjectId"];
                                    merchantDto.ProjectType = Convert.ToByte(fdMerchantView.ProjectType);
                                    break;
                                case 3:
                                    merchantDto.EcommProjectId = ConfigurationManager.AppSettings["EcommProjectId"];
                                    merchantDto.ProjectId = ConfigurationManager.AppSettings["ProjectId"];
                                    merchantDto.ProjectType = Convert.ToByte(fdMerchantView.ProjectType);
                                    break;
                                default:
                                    merchantDto.EcommProjectId = String.Empty;
                                    merchantDto.ProjectId = String.Empty;
                                    merchantDto.ProjectType = null;
                                    break;
                            }
                        }

                        if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                            _logger.LogInfoMessage(string.Format("MerchantController.RcMerchant. MerchantID:{0};RapidConnect:{1};Fk_FDId:{2};", id, merchantDto.RapidConnect, merchantDto.Fk_FDId));

                        int already = _merchantService.Update(merchantDto, id);

                        if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                            _logger.LogInfoMessage(string.Format("MerchantController.RcMerchant-3. Already:{0};", already));

                        if (already == 1)
                        {
                            roles = string.Join(", ", muserObj.UserRoleIdList);
                            muserObj.RolesAssigned = roles;
                            TempData["ExistCompany"] = MtdataResource.Merchant_Already_Exists;

                            PersonalDetails pd = new PersonalDetails();
                            pd.Company = muserObj.Company;
                            pd.CompanyTaxNumber = muserObj.CompanyTaxNumber;

                            pd.FName = muserObj.FName;
                            pd.LName = muserObj.LName;
                            pd.Email = muserObj.Email;
                            pd.Phone = muserObj.Phone;

                            pd.UseTransArmor = muserObj.UseTransArmor;      //PAY-13

                            if (muserObj.Fk_FDId != null)
                                pd.Fk_FDId = (int)muserObj.Fk_FDId;

                            muserObj = GetBaseInfo();

                            return RedirectToAction("PersonalInfo", pd);
                        }

                        if (already == 2)
                        {
                            if (ConfigurationManager.AppSettings["Project"] == MtdataResource.CanadaProject)
                                ViewData["Msg"] = MtdataResource.FirstDataMerchantExists;
                            else
                                ViewData["Msg"] = MtdataResource.Rapid_connect_merchant_id_already_exists;
                            
                            ViewBag.Project = ConfigurationManager.AppSettings["Project"];

                            return View();
                        }

                        int mId = _merchantService.GetMerchantId(muserObj.Email);
                        var cookie = new HttpCookie("CookieViewAs");
                        cookie.HttpOnly = true;
                        cookie.Values.Add("merchantId", Convert.ToString(mId));
                        cookie.Values.Add("userId", "");
                        cookie.Values.Add("merchantName", "");
                        Response.Cookies.Add(cookie);

                        _logMessage.Append("setting cookies for the merchnat successfully.");

                        var httpCookie = Response.Cookies["CookieViewAs"];
                        if (httpCookie != null)
                            httpCookie.Expires = DateTime.Now.AddDays(1);

                        _logTracking.Append("User " + ((SessionDto)Session["User"]).SessionUser.Email + " Created a Merchant " + muserObj.Email + " at " + System.DateTime.Now.ToUniversalTime());

                        RemoveAllInfo();

                        _logger.LogInfoMessage(_logTracking.ToString());

                        if (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 5)
                            return Redirect("~/MyMerchants/Index");

                        return Redirect("~/Merchant/Index");
                    }

                    fdMerchantView.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                    fDMerchantDto fDMerchantDto = fdMerchantView.ToDTO();

                    if (ConfigurationManager.AppSettings["Project"] == MtdataResource.CanadaProject)
                    {
                        fDMerchantDto.ServiceId = ConfigurationManager.AppSettings["CnServiceId"];
                        fDMerchantDto.App = ConfigurationManager.AppSettings["CnApp"];
                    }
                    else
                    {
                        fDMerchantDto.ServiceId = ConfigurationManager.AppSettings["ServiceId"];
                        fDMerchantDto.App = ConfigurationManager.AppSettings["App"];
                    }

                    fDMerchantDto.GroupId = fdMerchantView.GroupId;
                    fDMerchantDto.FDMerchantID = fdMerchantView.FDMerchantID;

                    if (ConfigurationManager.AppSettings["Project"] == MtdataResource.CanadaProject)
                    {
                        fDMerchantDto.EcommProjectId = ConfigurationManager.AppSettings["CnEcommProjectId"];
                        fDMerchantDto.ProjectType = 1;
                    }
                    else
                    {
                        switch (fdMerchantView.ProjectType)
                        {
                            case 1:
                                fDMerchantDto.EcommProjectId = ConfigurationManager.AppSettings["EcommProjectId"];
                                fDMerchantDto.ProjectType = Convert.ToByte(fdMerchantView.ProjectType);
                                break;
                            case 2:
                                fDMerchantDto.ProjectId = ConfigurationManager.AppSettings["ProjectId"];
                                fDMerchantDto.ProjectType = Convert.ToByte(fdMerchantView.ProjectType);
                                break;
                            case 3:
                                fDMerchantDto.EcommProjectId = ConfigurationManager.AppSettings["EcommProjectId"];
                                fDMerchantDto.ProjectId = ConfigurationManager.AppSettings["ProjectId"];
                                fDMerchantDto.ProjectType = Convert.ToByte(fdMerchantView.ProjectType);
                                break;
                            default:
                                fDMerchantDto.EcommProjectId = String.Empty;
                                fDMerchantDto.ProjectId = String.Empty;
                                fDMerchantDto.ProjectType = null;
                                break;
                        }
                    }

                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("MerchantController.RcMerchant. MerchantID:{0};MV.GroupId:{1};MV.FDMerchantID:{2};", id, fdMerchantView.GroupId, fdMerchantView.FDMerchantID));


                    if (string.IsNullOrEmpty(fdMerchantView.GroupId) && string.IsNullOrEmpty(fdMerchantView.FDMerchantID))
                    {
                        muserObj.Fk_FDId = null;
                    }
                    else
                    {
                        if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                            _logger.LogInfoMessage(string.Format("MerchantController.RcMerchant. MerchantID:{0};MDTO.MerchantID:{1};MDTO.FDId:{2};", id, fDMerchantDto.FDMerchantID, fDMerchantDto.FDId));

                        // Needs to also check that the fDMerchantDto is not linked to this Merchant.
                        // This is why it is giving the 'already exists' error.
                        
                        int response = _merchantService.AddFdMerchant(fDMerchantDto);
                        if (response == 1)
                        {
                            string project = ConfigurationManager.AppSettings["Project"];

                            if (project == MtdataResource.CanadaProject)
                                ViewData["Msg"] = MtdataResource.FirstDataMerchantExists;
                            else
                                ViewData["Msg"] = MtdataResource.rapid_connect_merchnat_id_already_exist;
                            
                            ViewBag.Project = project;

                            return View();
                        }

                        muserObj.Fk_FDId = response;
                    }

                    _logMessage.Append("Appending HtmlTemplate for confirm mail");

                    string cPassword = Membership.GeneratePassword(10, 1);
                        
                    _logMessage.Append("calling Hash method to encrypt the password");

                    muserObj.PCode = cPassword.Hash();

                    _logMessage.Append("joing all the roles assigned to user in varible result");
                        
                    var result = string.Join(", ", muserObj.UserRoleIdList);

                    _logMessage.Append("calling ToDTO method to map MerchantUserDto to MerchantUserViewData and retrun MerchantUserDto");
                    _logMessage.Append("ToDTO method executed successfully");

                    MerchantUserDto merchantuserDto = muserObj.ToDTO();
                    merchantuserDto.fk_MerchantID = (((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                    merchantuserDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                    merchantuserDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                    merchantuserDto.RolesAssigned = result;

                    _logMessage.Append("Calling Add method of MarchnatService class takes MerchantUserDto, MerchantUserDto properties Frist Name= " + merchantuserDto.FName + ", Last Name= " + merchantuserDto.LName + ", ");
                        
                    bool isExists = _merchantService.Add(merchantuserDto);
                    if (isExists)
                    {
                        ViewData["Msg"] = "Account for '" + merchantDto.Email + "'already exists please contact to your administrator";
                        ViewBag.Project = ConfigurationManager.AppSettings["Project"];

                        return View();
                    }

                    try
                    {
                        _logMessage.Append("Mail Sending begin and reading file ~/HtmlTemplate/confirmMail.html");

                        var filename = System.Web.HttpContext.Current.Server.MapPath("~/HtmlTemplate/confirmMail.html");
                        var myFile = new System.IO.StreamReader(filename);
                        string msgfile = myFile.ReadToEnd();
                        var msgBody = new StringBuilder(msgfile);

                        myFile.Close();

                        string cMail = muserObj.Email;
                        string cName = muserObj.Company;
                        string cuserName = muserObj.FName;
                        string appUrl = ConfigurationManager.AppSettings["ApplicationURL"];

                        msgBody.Replace("[##cMail##]", cMail);
                        msgBody.Replace("[##cName##]", cName);
                        msgBody.Replace("[##cPassword##]", cPassword);
                        msgBody.Replace("[##cuserName##]", cuserName);
                        msgBody.Replace("[##AppURL##]", appUrl);

                        _logMessage.Append("calling SendMail method of utility class");

                        var sendEmail = new EmailUtil();
                        sendEmail.SendMail(cMail, msgBody.ToString(), cName, "Registered User");

                        myFile.Dispose();
                    }
                    catch (Exception ex)
                    {
                        _logger.LogInfoFatal(_logMessage.ToString(), ex);
                    }

                    var mIds = _merchantService.GetMerchantId(muserObj.Email);
                    var cookies = new HttpCookie("CookieViewAs");
                        
                    cookies.HttpOnly = true;
                    cookies.Values.Add("merchantId", Convert.ToString(mIds));
                    cookies.Values.Add("userId", "");
                    cookies.Values.Add("merchantName", "");

                    Response.Cookies.Add(cookies);
                        
                    _logMessage.Append("setting cookies for the merchnat successfully.");

                    var httpCookies = Response.Cookies["CookieViewAs"];
                    if (httpCookies != null)
                        httpCookies.Expires = DateTime.Now.AddDays(1);

                    _logTracking.Append("User " + ((SessionDto)Session["User"]).SessionUser.Email + " Created a Merchant " + muserObj.Email + " at " + System.DateTime.Now.ToUniversalTime());

                    RemoveAllInfo();

                    _logger.LogInfoMessage(_logTracking.ToString());

                    return Redirect("~/Merchant/Index");
                }
                catch (Exception exception)
                {
                    _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, "{0} Method Not executed successfully.", MethodBase.GetCurrentMethod().Name) + "; Method Result:- False;");
                    _logger.LogInfoFatal(_logMessage.ToString(), exception);
                    
                    ViewData["Msg"] = MtdataResource.Merchant_Could_Not_be_Added;
                    ViewBag.Project = ConfigurationManager.AppSettings["Project"];

                    return View();
                }
            }

            ViewBag.Project = ConfigurationManager.AppSettings["Project"];
            return View();
        }

        /// <summary>
        ///  Purpose            : To create rapid connect merchant details
        /// Function Name       : FDMerchant
        /// Created By          : Naveen Kumar
        /// Created On          : 04/15/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>    
        /// <returns></returns>
        [HttpGet]
        public ActionResult FDMerchant()
        {
            return View();
        }

        /// <summary>
        ///  Purpose            : To create rapid connect merchant details
        /// Function Name       : FDMerchant
        /// Created By          : Naveen Kumar
        /// Created On          : 04/15/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>    
        /// <param name="fdMerchantView"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult FDMerchant(FDMerchantViewData fdMerchantView)
        {
            if (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("Index", "Member");

            try
            {
                if (ModelState.IsValid)
                {
                    fdMerchantView.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                    fDMerchantDto fDMerchantDto = fdMerchantView.ToDTO();
                    bool response = _merchantService.FdMerchantAdd(fDMerchantDto);
                    if (response)
                    {
                        ViewBag.Msg = MtdataResource.Rapid_connect_merchant_id_already_exists;
                        return View();
                    }
                    return Redirect("FDIndex");
                }
                return View(fdMerchantView);
            }
            catch (Exception exception)
            {
                _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, "{0} Method Not executed successfully.", MethodBase.GetCurrentMethod().Name) + "; Method Result:- False;");
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            : To get list fo fd merchants
        /// Function Name       : GetFDMerchantList
        /// Created By          : Naveen Kumar
        /// Created On          : 04/15/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>    
        /// <returns></returns>
        [NonAction]
        public IEnumerable<FDMerchantListViewData> GetFdMerchantList()
        {
            var result = _merchantService.GetFDMerchantList().ToViewDataList();
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fdId"></param>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<FDMerchantListViewData> GetFdMerchantListForUpdate(int fdId)
        {
            List<FDMerchantListViewData> result = _merchantService.GetFDMerchantList().ToViewDataList().ToList();
            var fdMerchantDto = _merchantService.GetFdMerchantForUpdate(fdId);
            var fdMerchantViewData = fdMerchantDto.ToFdMerchantViewData();
            result.Insert(0, new FDMerchantListViewData { FDId = fdId, FdMerchantId = fdMerchantViewData.FDMerchantID });
            return result;
        }

        /// <summary>
        ///  Purpose            : To update rapid connect merchant details
        /// Function Name       : UpdateFdMerchant
        /// Created By          : Naveen Kumar
        /// Created On          : 04/16/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>    
        /// <param name="fdid"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UpdateFdMerchant(int fdid)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("index", "Member");

            fDMerchantDto mrMerchant = _merchantService.GetFDMerchantUpdate(fdid);
            FDMerchantViewData mvData = mrMerchant.ToFdMerchantViewData();

            return View(mvData);
        }

        /// <summary>
        ///  Purpose            : To update rapid connect merchant details
        /// Function Name       : UpdateFdMerchant
        /// Created By          : Naveen Kumar
        /// Created On          : 04/16/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>    
        /// <param name="fdMerchantViewData"></param>
        /// <param name="fdid"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateFdMerchant(FDMerchantViewData fdMerchantViewData, int fdid)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                if (ModelState.IsValid)
                {
                    fdMerchantViewData.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                    var fdMerchantDto = fdMerchantViewData.ToDTO();
                    _merchantService.FdMerchantUpdate(fdMerchantDto, fdid);

                    _logMessage.Append("First Data Merchant id " + fdid + " updated by " + fdMerchantViewData.ModifiedBy + "at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return Redirect("FDIndex");
                }

                return View();
            }
            catch (Exception exception)
            {
                _logMessage.AppendLine(String.Format(CultureInfo.InvariantCulture, "{0} Method Not executed successfully.", MethodBase.GetCurrentMethod().Name) + "; Method Result:- False;");
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            : To filter the list of fd merchants
        /// Function Name       : FdMerchantListByFiter
        /// Created By          : Naveen Kumar
        /// Created On          : 04/17/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>    
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult FdMerchantListByFiter(string name = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetFdMerchantListByFilter");
                
                int listCount;
                name = name.Trim();
                var viewDataList = _merchantService.GetFdMerchantListByFilter(name, jtStartIndex, jtPageSize, jtSorting);

                if (string.IsNullOrEmpty(name))
                {
                    _logMessage.Append("Calling GetFDMerchantList");

                    listCount = _merchantService.GetFDMerchantList().Count();

                    _logMessage.Append("GetFDMerchantList executed successfully result = " + listCount);

                    return Json(new { Result = "OK", Records = viewDataList, TotalRecordCount = listCount });
                }

                var fdDtos = viewDataList as fDMerchantDto[] ?? viewDataList.ToArray();
                listCount = fdDtos.ToList().Count();

                return Json(new { Result = "OK", Records = fdDtos, TotalRecordCount = listCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        ///  Purpose            : To delete fd merchants
        /// Function Name       : Delete
        /// Created By          : Naveen Kumar
        /// Created On          : 04/17/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>    
        /// <param name="fdid"></param>     
        /// <returns></returns>
        public JsonResult Delete(int fdid)
        {
            if (Session["User"] != null)
            {
                try
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    _logMessage.Append("Calling DeleteFdMerchant FDId= " + fdid);

                    bool isAssigned = _merchantService.DeleteFdMerchant(fdid);
                    if (isAssigned)
                    {
                        var msg = "This first data merchant is assigned to specific merchant";
                        return Json(new { Result = "No", Message = msg });
                    }

                    _logMessage.Append("First Data Merchant id " + fdid + " deleted by " + (((SessionDto)Session["User"]).SessionUser.Email) + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                    _logMessage.Append("DeleteFdMerchant method of _merchantService class executed successfully");

                    Thread.Sleep(50);

                    return Json(new { Result = "OK" });
                }
                catch (Exception exception)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), exception);
                }
            }

            return Json(new { Result = "No" });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteMerchant(int merchantId)
        {
            if (Session["User"] != null)
            {
                try
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    _logMessage.Append("Calling DeleteMerchant UserId= " + merchantId);

                    _merchantService.DeleteMerchant(merchantId); //delete merchant by id

                    _logMessage.Append("Merchant id " + merchantId + " was deleted by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    return Json(new { Result = "OK" });
                }
                catch (Exception exception)
                {
                    _logger.LogError(exception.ToString());
                }
            }

            return Json(new { Result = "No" });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CompanyExsit(string company)
        {
            if (Session["User"] != null)
            {
                bool isExist = _merchantService.CompanyExsit(company);
                return Json(isExist, JsonRequestBehavior.AllowGet);
            }
            return Json("");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult EmailExist(string email)
        {
            if (Session["User"] != null)
            {
                bool isExist = _merchantService.EmailExist(email);
                return Json(isExist, JsonRequestBehavior.AllowGet);
            }
            return Json("");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ActionResult ViewUser(string userId)
        {
            if (Session["User"] == null) return 
                    RedirectToAction("Index", "Member");

            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return RedirectToAction("Index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var cookie = new HttpCookie("CookieViewAsUser");
                cookie.HttpOnly = true;
                cookie.Values.Add("merchantId", "");
                cookie.Values.Add("userId", userId);
                cookie.Values.Add("merchantName", "");

                Response.Cookies.Add(cookie);

                _logMessage.Append("Setting cookies for merchant successfully");

                var httpCookie = Response.Cookies["CookieViewAsUser"];
                if (httpCookie != null)
                    httpCookie.Expires = DateTime.Now.AddDays(1);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return RedirectToAction("ResetCredentials", "ViewAs");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="company"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CompanyExsits(string company, string merchantId)
        {
            if (Session["User"] != null)
            {
                bool isExist = _merchantService.CompanyExsit(company, Convert.ToInt32((merchantId)));
                return Json(isExist, JsonRequestBehavior.AllowGet);
            }
            return Json("");
        }

        /// <summary>
        /// Purpose             : to get the list of time zone
        /// Function Name       :   TimeZoneList
        /// Created By          :   Naveen Kumar
        /// Created On          :  2/4/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public List<TimeZoneViewData> TimeZoneList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                
                var timeZone = TimeZoneInfo.GetSystemTimeZones()
                        .Select(z => new TimeZoneViewData { TimeZoneName = z.DisplayName, TimeZoneTime = z.Id })
                        .ToList();

                _logMessage.Append("TimeZone list created successfully.");

                return timeZone;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }
    }
}
