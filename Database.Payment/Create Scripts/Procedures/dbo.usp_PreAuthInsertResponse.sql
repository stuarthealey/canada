IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PreAuthInsertResponse]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_PreAuthInsertResponse]
GO

CREATE PROCEDURE dbo.usp_PreAuthInsertResponse
(
	@XMLData XML
)
AS 
BEGIN
	SELECT	t.value('(authCodeField/text())[1]', 'NVARCHAR(120)') AS AuthCode,
			t.value('(extDataField/text())[1]', 'NVARCHAR(220)') AS [Extended Data],
			t.value('(getAVSResultField/text())[1]', 'NVARCHAR(120)') AS AVSResultField ,
			t.value('(getAVSResultTXTField/text())[1]', 'NVARCHAR(120)') AS AVSResultTXTField,
			t.value('(getCAVVResultField/text())[1]', 'NVARCHAR(120)') AS CAVVResultField,
			t.value('(getCVResultField/text())[1]', 'NVARCHAR(120)') AS CVResultField,
			t.value('(getCVResultTXTField/text())[1]', 'NVARCHAR(120)') AS CVResultTXTField ,
			t.value('(getCommercialCardField/text())[1]', 'NVARCHAR(120)') AS CommercialCardField,
			t.value('(getStreetMatchTXTField/text())[1]', 'NVARCHAR(120)') AS StreetMatchTXTField,
			t.value('(getZipMatchTXTField/text())[1]', 'NVARCHAR(120)') AS ZipMatchTXTField,
			t.value('(messageField/text())[1]', 'NVARCHAR(120)') AS messageField,
			t.value('(pNRefField/text())[1]', 'INT') AS pNRefField ,
			t.value('(respMSGField/text())[1]', 'NVARCHAR(120)') AS ResponseMSGField,
			t.value('(resultField/text())[1]', 'BIT') AS ResultField
	FROM @XMLData.nodes('/Response') AS PreAuth_XMLResponseTable(t)
END
GO
