﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;

using MTData.WebAPI.Resources;

namespace MTData.WebAPI.Models
{
    public static class CapkUtil
    {
        /// <summary>
        /// Purpose             :   To read available CapKeys.
        /// Function Name       :   ReadRawCapKey
        /// Created By          :   Sunil Singh
        /// Created On          :   28-Jan-2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="rawData"></param>
        /// <returns></returns>
        public static Dictionary<string, CapKeyNodes> ReadRawCapKey(this string rawData)
        {
            //Handling unwanted string
            if (rawData.Contains("</m_StringValue>"))
                rawData = rawData.Replace("</m_StringValue>", "");

            string[] dlmtr = { ",01,01," };
            string[] strKeyList = rawData.Split(dlmtr, StringSplitOptions.None);

            Dictionary<string, CapKeyNodes> newCapKey = new Dictionary<string, CapKeyNodes>();
            for (int i = 1; i < strKeyList.Length; i++)
            {
                string[] keys = strKeyList[i].Split(',');
                CapKeyNodes cK = new CapKeyNodes();
                cK.x9F06 = keys[0];
                cK.x9F22 = keys[1];
                cK.x9F8123 = keys[2];
                cK.x9F8122 = keys[3];
                cK.SearchKey = keys[0] + keys[1];

                if (i != strKeyList.Length - 1)
                    cK.x9F8121 = keys[4].Substring(0, keys[4].Length - 9);
                else
                    cK.x9F8121 = keys[4];

                if (!newCapKey.ContainsKey(cK.SearchKey))
                    newCapKey.Add(cK.SearchKey, cK);
            }

            return newCapKey;
        }

        /// <summary>
        /// Purpose             :   To update CapKey XML file to be used in ingenico device.
        /// Function Name       :   UpdateXMLFile
        /// Created By          :   Sunil Singh
        /// Created On          :   28-Jan-2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="rawCapkey"></param>
        /// <returns></returns>
        public static XmlDocument UpdateXMLFile(this Dictionary<string, CapKeyNodes> rawCapkey)
        {
            Dictionary<string, CapKeyNodes> extngKey = new Dictionary<string, CapKeyNodes>();

            //Loading EMVCONTACT.XML
            string path = HttpContext.Current.Request.MapPath(@"~\Resources\EMVCONTACT.XML");
            string xmlFileData = File.ReadAllText(path);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlFileData);

            //Loading CapKey Parent node
            XmlNode capKeyNode = doc.SelectSingleNode("//node[@tag='0x1100']");

            //Managing serial number
            List<int> serial = new List<int>();

            //Loading All CapKey child nodes
            XmlNodeList xnList = capKeyNode.SelectNodes("node[@tag]");
            for (int i = 0; i < xnList.Count; i++)
            {
                // Reading ALL Nodes from XML file

                var srNo = xnList[i].Attributes["tag"].Value;
                serial.Add(Convert.ToInt32(srNo, 16));

                //Loading CapKey values
                XmlNodeList xnList1 = xnList[i].SelectNodes("node[@tag]");

                string node9F06 = string.Empty;
                string node9F22 = string.Empty;

                //Loading existing keys from XML file
                for (int k = 0; k < xnList1.Count; k++)
                {
                    if (xnList1[k] != null && xnList1[k].Attributes["tag"] != null)
                    {
                        var tagName = xnList1[k].Attributes["tag"].Value;
                        var key = tagName.Substring(1, tagName.Length - 1);

                        switch (key)
                        {
                            case "x9F06":
                                node9F06 = xnList1[k].InnerXml.FormatString();
                                break;
                            case "x9F22":
                                node9F22 = xnList1[k].InnerXml.FormatString();
                                break;
                        }
                    }
                }

                // Updating XML Nodes in XML file
                //Create serch key
                string xmlSerchKey = node9F06 + node9F22;

                //Find CapKey in new CapKey values
                if (rawCapkey.ContainsKey(xmlSerchKey))
                {
                    //If exist - Update xml file from new capkey
                    var nwCapKey = rawCapkey[xmlSerchKey];

                    for (int k = 0; k < xnList1.Count; k++)
                    {
                        if (xnList1[k] != null && xnList1[k].Attributes["tag"] != null)
                        {
                            var tagName = xnList1[k].Attributes["tag"].Value;
                            var key = tagName.Substring(1, tagName.Length - 1);

                            switch (key)
                            {
                                case "x9F06":
                                    xnList1[k].InnerXml = nwCapKey.x9F06.AddSpace();
                                    break;
                                case "x9F22":
                                    xnList1[k].InnerXml = nwCapKey.x9F22.AddSpace();
                                    break;
                                case "x9F8123":
                                    xnList1[k].InnerXml = nwCapKey.x9F8123.AddSpace();
                                    break;
                                case "x9F8122":
                                    xnList1[k].InnerXml = nwCapKey.x9F8122.AddSpace();
                                    break;
                                case "x9F8121":
                                    xnList1[k].InnerXml = nwCapKey.x9F8121.AddSpace();
                                    break;
                            }
                        }
                    }

                    //Create list of existing Keys
                    if (!extngKey.ContainsKey(xmlSerchKey))
                        extngKey.Add(xmlSerchKey, nwCapKey);
                }
            }

            // Adding New Node in XML File
            //Check for new keys
            var newCapKeys = rawCapkey.Keys.Except(extngKey.Keys);
            if (newCapKeys.Count() > 0)
            {
                var maxSerialCount = serial.Max(o => o);
                maxSerialCount += 1;
                serial.Add(maxSerialCount);

                foreach (var cKey in newCapKeys)
                {
                    //Copy node schema
                    XmlNode newXMLNodeSchema = xnList[0].Clone();

                    //Update new serial number
                    XmlAttribute serialNumber = newXMLNodeSchema.Attributes["tag"];
                    maxSerialCount = serial.Max(o => o);
                    serialNumber.Value = "0x" + maxSerialCount.ToString("X");

                    //Create new CapKey node
                    XmlNodeList xl = newXMLNodeSchema.SelectNodes("node[@tag]");

                    var capKeyData = rawCapkey[cKey];

                    for (int k = 0; k < xl.Count; k++)
                    {
                        if (xl[k] != null && xl[k].Attributes["tag"] != null)
                        {
                            var tagName = xl[k].Attributes["tag"].Value;
                            var key = tagName.Substring(1, tagName.Length - 1);

                            switch (key)
                            {
                                case "x9F06":
                                    xl[k].InnerXml = " " + capKeyData.x9F06.AddSpace();
                                    break;
                                case "x9F22":
                                    xl[k].InnerXml = " " + capKeyData.x9F22.AddSpace();
                                    break;
                                case "x9F8123":
                                    xl[k].InnerXml = " " + capKeyData.x9F8123.AddSpace();
                                    break;
                                case "x9F8122":
                                    xl[k].InnerXml = " " + capKeyData.x9F8122.AddSpace();
                                    break;
                                case "x9F8121":
                                    xl[k].InnerXml = " " + capKeyData.x9F8121.AddSpace();
                                    break;
                            }
                        }
                    }

                    capKeyNode.AppendChild(newXMLNodeSchema);

                    //incrementing serial
                    maxSerialCount += 1;
                    serial.Add(maxSerialCount);
                }
            }

            //Changes done for EMV Contact certification of Canada
            // Update Currency Code for Canada
            XmlNodeList xnList5F2A = doc.SelectNodes("//node[@tag='0x5F2A']");
            XmlNodeList xnList9F1A = doc.SelectNodes("//node[@tag='0x9F1A']");

            for (int i = 0; i < xnList5F2A.Count; i++)
            {
                if (ConfigurationManager.AppSettings["Project"] == PaymentAPIResources.CanadaProject)
                    xnList5F2A[i].InnerXml = " 01 24 ";
                else
                    xnList5F2A[i].InnerXml = " 08 40 ";
            }

            for (int i = 0; i < xnList9F1A.Count; i++)
            {
                if (ConfigurationManager.AppSettings["Project"] == PaymentAPIResources.CanadaProject)
                    xnList9F1A[i].InnerXml = " 01 24 ";
                else
                    xnList9F1A[i].InnerXml = " 08 40 ";
            }

            // save the XmlDocument back to disk
            doc.Save(path);
            return doc;
        }

        /// <summary>
        /// To remove un-necessary chars
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string FormatString(this string val)
        {
            if (!string.IsNullOrEmpty(val))
                return val.Replace("\r", "").Replace("\n", "").Replace(" ", "");
            else
                return val;
        }

        /// <summary>
        /// Purpose             :   To add space between node values.
        /// Function Name       :   AddSpace
        /// Created By          :   Sunil Singh
        /// Created On          :   28-Jan-2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string AddSpace(this string val)
        {
            if (!string.IsNullOrEmpty(val))
            {
                val = val.FormatString();
                char[] inputChars = val.ToCharArray();
                StringBuilder sbNewValue = new StringBuilder();

                for (int i = 0; i < inputChars.Length; i++)
                {
                    string fistChar = inputChars[i].ToString();
                    i++;

                    string secondChar = string.Empty;
                    if (i < inputChars.Length)
                    {
                        secondChar = inputChars[i].ToString();
                        sbNewValue.Append(fistChar + secondChar + " ");
                    }
                }

                return sbNewValue.Insert(0, ' ').ToString();
            }
            else
                return val;
        }

        /// <summary>
        /// Handling CapKey response
        /// </summary>
        /// <param name="status"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static CapKeyResponse CapKeyResponse(bool status, string message)
        {
            CapKeyResponse resp = new CapKeyResponse();
            ResponseHeader hdr = new ResponseHeader();
            if (status)
                hdr.Status = PaymentAPIResources.Success;
            else
                hdr.Status = PaymentAPIResources.Failed;
            
            hdr.Message = message;
            resp.Header = hdr;

            return resp;
        }

        /// <summary>
        /// Purpose             :   To read available CapKeys.
        /// Function Name       :   ClessReadRawCapKey
        /// Created By          :   Madhuri
        /// Created On          :   20-July-2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="rawData"></param>
        /// <returns></returns>
        public static Dictionary<string, CapKeyNodes> ClessReadRawCapKey(this string rawData)
        {
            //Handling unwanted string
            if (rawData.Contains("</m_StringValue>"))
                rawData = rawData.Replace("</m_StringValue>", "");

            string[] dlmtr = { ",01,01," };
            string[] strKeyList = rawData.Split(dlmtr, StringSplitOptions.None);

            Dictionary<string, CapKeyNodes> newCapKey = new Dictionary<string, CapKeyNodes>();
            for (int i = 1; i < strKeyList.Length; i++)
            {
                string[] keys = strKeyList[i].Split(',');
                CapKeyNodes cK = new CapKeyNodes();
                cK.x9F06 = keys[0];
                cK.x9F22 = keys[1];
                cK.x9F918704 = keys[2];
                cK.x9F918703 = keys[3];
                cK.SearchKey = keys[0] + keys[1];

                if (i != strKeyList.Length - 1)
                    cK.x9F918702 = keys[4].Substring(0, keys[4].Length - 9);
                else
                    cK.x9F918702 = keys[4];

                if (!newCapKey.ContainsKey(cK.SearchKey))
                    newCapKey.Add(cK.SearchKey, cK);
            }
            return newCapKey;
        }

        /// <summary>
        /// Purpose             :   To read available CapKeys.
        /// Function Name       :   UpdateXMLFileCless
        /// Created By          :   Madhuri
        /// Created On          :   20-July-2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   "MM/DD/YYYY" 
        /// </summary>
        /// <param name="rawCapkey"></param>
        /// <returns></returns>
        public static XmlDocument UpdateXMLFileCless(this Dictionary<string, CapKeyNodes> rawCapkey)
        {
            Dictionary<string, CapKeyNodes> extngKey = new Dictionary<string, CapKeyNodes>();

            //Loading EMVCONTACT.XML
            string path = HttpContext.Current.Request.MapPath(@"~\Resources\EMVCLESS.XML");
            string xmlFileData = File.ReadAllText(path);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlFileData);

            //Loading CapKey Parent node
            XmlNode capKeyNode = doc.SelectSingleNode("//node[@tag='0xBF918801']");

            //Managing serial number
            List<int> serial = new List<int>();

            //Loading All CapKey child nodes
            XmlNodeList xnList = capKeyNode.SelectNodes("node[@tag]");
            for (int i = 0; i < xnList.Count; i++)
            {
                // Reading ALL Nodes from XML file

                var srNo = xnList[i].Attributes["tag"].Value;
                serial.Add(Convert.ToInt32(srNo, 16));

                //Loading CapKey values
                XmlNodeList xnList1 = xnList[i].SelectNodes("node[@tag]");

                string node9F06 = string.Empty;
                string node9F22 = string.Empty;

                //Loading existing keys from XML file
                for (int k = 0; k < xnList1.Count; k++)
                {
                    if (xnList1[k] != null && xnList1[k].Attributes["tag"] != null)
                    {
                        var tagName = xnList1[k].Attributes["tag"].Value;
                        var key = tagName.Substring(1, tagName.Length - 1);

                        switch (key)
                        {
                            case "x9F06":
                                node9F06 = xnList1[k].InnerXml.FormatString();
                                break;
                            case "x9F22":
                                node9F22 = xnList1[k].InnerXml.FormatString();
                                break;
                        }
                    }
                }

                // Updating XML Nodes in XML file
                //Create serch key
                string xmlSerchKey = node9F06 + node9F22;

                //Find CapKey in new CapKey values
                if (rawCapkey.ContainsKey(xmlSerchKey))
                {
                    //If exist - Update xml file from new capkey
                    var nwCapKey = rawCapkey[xmlSerchKey];

                    for (int k = 0; k < xnList1.Count; k++)
                    {
                        if (xnList1[k] != null && xnList1[k].Attributes["tag"] != null)
                        {
                            var tagName = xnList1[k].Attributes["tag"].Value;
                            var key = tagName.Substring(1, tagName.Length - 1);

                            switch (key)
                            {
                                case "x9F06":
                                    xnList1[k].InnerXml = nwCapKey.x9F06.AddSpace();
                                    break;
                                case "x9F22":
                                    xnList1[k].InnerXml = nwCapKey.x9F22.AddSpace();
                                    break;
                                case "x9F918704":
                                    xnList1[k].InnerXml = nwCapKey.x9F918704.AddSpace();
                                    break;
                                case "x9F918703":
                                    xnList1[k].InnerXml = nwCapKey.x9F918703.AddSpace();
                                    break;
                                case "x9F918702":
                                    xnList1[k].InnerXml = nwCapKey.x9F918702.AddSpace();
                                    break;
                            }
                        }
                    }

                    //Create list of existing Keys
                    if (!extngKey.ContainsKey(xmlSerchKey))
                        extngKey.Add(xmlSerchKey, nwCapKey);
                }
            }

            // Adding New Node in XML File
            //Check for new keys
            var newCapKeys = rawCapkey.Keys.Except(extngKey.Keys);
            if (newCapKeys.Count() > 0)
            {
                var maxSerialCount = serial.Max(o => o);
                maxSerialCount += 1;
                serial.Add(maxSerialCount);

                foreach (var cKey in newCapKeys)
                {
                    //Copy node schema
                    XmlNode newXMLNodeSchema = xnList[0].Clone();

                    //Update new serial number
                    XmlAttribute serialNumber = newXMLNodeSchema.Attributes["tag"];
                    maxSerialCount = serial.Max(o => o);
                    serialNumber.Value = "0x" + maxSerialCount.ToString("X");

                    //Create new CapKey node
                    XmlNodeList xl = newXMLNodeSchema.SelectNodes("node[@tag]");

                    var capKeyData = rawCapkey[cKey];

                    for (int k = 0; k < xl.Count; k++)
                    {
                        if (xl[k] != null && xl[k].Attributes["tag"] != null)
                        {
                            var tagName = xl[k].Attributes["tag"].Value;
                            var key = tagName.Substring(1, tagName.Length - 1);

                            switch (key)
                            {
                                case "x9F06":
                                    xl[k].InnerXml = " " + capKeyData.x9F06.AddSpace();
                                    break;
                                case "x9F22":
                                    xl[k].InnerXml = " " + capKeyData.x9F22.AddSpace();
                                    break;
                                case "x9F918704":
                                    xl[k].InnerXml = " " + capKeyData.x9F918704.AddSpace();
                                    break;
                                case "x9F918703":
                                    xl[k].InnerXml = " " + capKeyData.x9F918703.AddSpace();
                                    break;
                                case "x9F918702":
                                    xl[k].InnerXml = " " + capKeyData.x9F918702.AddSpace();
                                    break;
                            }
                        }
                    }

                    capKeyNode.AppendChild(newXMLNodeSchema);

                    //incrementing serial
                    maxSerialCount += 1;
                    serial.Add(maxSerialCount);
                }
            }

            //Changes done for EMV Contact certification of Canada
            // Update Currency Code for Canada
            XmlNodeList xnList5F2A = doc.SelectNodes("//node[@tag='0x5F2A']");
            XmlNodeList xnList9F1A = doc.SelectNodes("//node[@tag='0x9F1A']");

            for (int i = 0; i < xnList5F2A.Count; i++)
            {
                if (ConfigurationManager.AppSettings["Project"] == PaymentAPIResources.CanadaProject)
                    xnList5F2A[i].InnerXml = " 01 24 ";
                else
                    xnList5F2A[i].InnerXml = " 08 40 ";
            }

            for (int i = 0; i < xnList9F1A.Count; i++)
            {
                if (ConfigurationManager.AppSettings["Project"] == PaymentAPIResources.CanadaProject)
                    xnList9F1A[i].InnerXml = " 01 24 ";
                else
                    xnList9F1A[i].InnerXml = " 08 40 ";
            }
            
            // save the XmlDocument back to disk
            doc.Save(path);
            return doc;
        }
    }
}