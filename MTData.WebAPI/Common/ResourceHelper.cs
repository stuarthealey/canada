﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace MTData.WebAPI.Common
{
    /// <summary>
    /// Class is used as the helper for getting resource string 
    /// </summary>
    public static class ResourceHelper
    {
        private static ResourceManager _resource = null;
        static ResourceHelper()
        {
            _resource = new ResourceManager("Resource.resource", Assembly.GetExecutingAssembly());
        }

        /// <summary>
        /// Get the resource string based on the key specified 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetResourceString(string key)
        {
            return _resource.GetString(key);
        }
    }
}
