﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
   public  class TechnologyDetailReportDto
    {
        public string Merchant { get; set; }
        public string FleetName { get; set; }
        public Nullable<byte> PayType { get; set; }
        public int TransId { get; set; }
        public Nullable<System.DateTime> TxnDate { get; set; }
        public string StringDateTime { get; set; }
        public string VehicleNo { get; set; }
        public string DriverNo { get; set; }
        public string TxnType { get; set; }
        public string Amount { get; set; }
        public string CardType { get; set; }
        public string TechFee { get; set; }
        public string FleetFee { get; set; }
        public string Surcharge { get; set; }
        public string Booking_Fee { get; set; }
        public string AuthId { get; set; }
        public Nullable<int> fk_FleetId { get; set; }
    }
}
