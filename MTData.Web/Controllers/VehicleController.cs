﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using System.Web;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.Web.ViewData;
using MTData.Web.Resource;

namespace MTData.Web.Controllers
{
    public class VehicleController : Controller
    {
        readonly ILogger _logger = new Logger();
        private readonly IVehicleService _vehicleService;
        private readonly ICommonService _commonService;
        private readonly ITerminalService _terminalService;
        private readonly IUserService _userService;
        private readonly IFleetService _fleetService;
        private StringBuilder _logMessage;
        private StringBuilder _logTracking;

        public VehicleController([Named("UserService")] IUserService userService, [Named("VehicleService")] IVehicleService vehicleService, [Named("CommonService")] ICommonService commomService, [Named("TerminalService")] ITerminalService terminalService, [Named("FleetService")] IFleetService fleetService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _vehicleService = vehicleService;
            _commonService = commomService;
            _terminalService = terminalService;
            _logMessage = logMessage;
            _userService = userService;
            _logTracking = logTracking;
            _fleetService = fleetService;
        }

        /// <summary>
        ///  Purpose            :   to display the vehicle
        /// Function Name       :   Index
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/6/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            return View();
        }

        /// <summary>
        /// Purpose             :   To export list in excel file
        /// Function Name       :   Index
        /// Created By          :   Umesh Kumar
        /// Created On          :   01/28/2016
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="carOwner"></param>
        /// <param name="vehicleNumber"></param>
        /// <param name="androidMac"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(string carOwner, string vehicleNumber, string androidMac)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetAdminUserListByFilter method of adminUserService class to get Admin Users List");
                
                int viewDataListCount = 0;
                IEnumerable<VehicleDto> userVechile = new List<VehicleDto>();

                var sessionData = (SessionDto)Session["User"];
                int merchantId = Convert.ToInt32(sessionData.SessionUser.fk_MerchantID);
                int logOnByUserId = Convert.ToInt32(sessionData.SessionUser.LogOnByUserId);
                int logOnByUserType = Convert.ToInt32(sessionData.SessionUser.LogOnByUserType);
                int userType = sessionData.SessionUser.fk_UserTypeID;

                FilterSearchParametersDto searchDto = new FilterSearchParametersDto()
                {
                    Name = vehicleNumber,
                    StartIndex = 0,
                    PageSize = 0,
                    Sorting = string.Empty,
                    MerchantId = merchantId,
                    LogOnByUserId = logOnByUserId,
                    LogOnByUserType = logOnByUserType,
                    OwnerName = carOwner,
                    SerialNumber = androidMac
                };

                if (userType != 4)
                {
                    userVechile = _vehicleService.GetVehileListByFilter(searchDto);
                    viewDataListCount = _vehicleService.GetVehileCount(searchDto).Count();
                }

                int userId = sessionData.SessionUser.UserID;
                var userfleet = _userService.GetUsersFleet(userId);//2

                if (userType == 4)
                {
                    FilterSearchParametersDto filterSearch = new FilterSearchParametersDto()
                    {
                        Name = vehicleNumber,
                        StartIndex = 0,
                        PageSize = 0,
                        Sorting = string.Empty,
                        MerchantId = merchantId,
                        UserId = userId,
                        UserType = userType,
                        userFleet = userfleet,
                        OwnerName = carOwner,
                        LogOnByUserType = logOnByUserType,
                        LogOnByUserId = logOnByUserId,
                        SerialNumber = androidMac
                    };

                    viewDataListCount = _vehicleService.UserVehicleCount(filterSearch);
                    filterSearch.PageSize = viewDataListCount;
                    userVechile = _vehicleService.UserVehicleList(filterSearch);
                }

                if (viewDataListCount > 1)
                {
                    using (StringWriter output = new StringWriter())
                    {
                        output.WriteLine("\"Car Owner\",\"Vehicle Number\",\"Vehicle Reg No\",\"Plate Number\",\"Fleet Name\",\"Mac Add\"");

                        foreach (VehicleDto item in userVechile)
                        {
                            output.WriteLine("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\"",
                               item.CarOwnerName, item.VehicleNumber, item.VehicleRegNo, item.PlateNumber, item.FleetName, item.DeviceName);
                        }

                        _logTracking.Append("Transaction Report Excel file Downloaded by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                        _logger.LogInfoMessage(_logTracking.ToString());

                        return File(new UTF8Encoding().GetBytes(output.ToString()), "application/CSV", "Vehicles.csv");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            :  Fetching the Vehicle on the basis of its ID from the database.
        /// Function Name       :   Create
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/6/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            _logMessage.Append("using object intilizer to populate fleetlist and terminal list");

            int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
            int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
            int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
            int logOnByUserId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserId));
            int logOnByUserType = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserType));

            try
            {
                var vehicleView = new VehicleViewData();
                vehicleView.TerminalList = TerminalList();

                if (userType == 2 || userType == 3)
                {
                    vehicleView.FleetList = FleetList(merchantId);

                    if (logOnByUserType == 5)
                    {
                        List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);                       
                        List<FleetListViewData> fleetViewResult = vehicleView.FleetList.Where(m => fleetListAccessed.Contains((int)m.FleetID)).Select(m => m).Distinct().ToList();                       
                        vehicleView.FleetList = fleetViewResult.AsEnumerable();
                    }

                    vehicleView.CarOwnerList = _vehicleService.CarOwnerList(merchantId).ToCarOwnerList();
                }

                if (userType == 4)
                {
                    vehicleView.FleetList = _userService.UserFleetList(userId).ToViewDataList();

                    if (logOnByUserType == 5)
                    {
                        List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                        List<FleetListViewData> fleetViewResult = vehicleView.FleetList.Where(m => fleetListAccessed.Contains((int)m.FleetID)).Select(m => m).Distinct().ToList();
                        vehicleView.FleetList = fleetViewResult.AsEnumerable();
                    }

                    vehicleView.CarOwnerList = _vehicleService.CarOwnerList(merchantId).ToCarOwnerList();
                }

                _logMessage.Append("fleetlist and terminal list populate successfully");

                return View(vehicleView);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose            :  Retrieve Vehicle on the basis of its ID from the database.
        /// Function Name       :   Create
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/6/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="vehicleViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(VehicleViewData vehicleViewData, HttpPostedFileBase uploadFile)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1) return RedirectToAction("index", "Member");

            try
            {
                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                int userId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.UserID);
                int userType = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_UserTypeID);
                int logOnByUserId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserId));
                int logOnByUserType = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserType));

                // Ecel Upload begin
                if (uploadFile != null)
                {
                    if (!IsValidExcel(uploadFile))
                    {
                        vehicleViewData.IsVehicleError = true;
                        List<VehicleViewData> db = new List<VehicleViewData>();
                        vehicleViewData.VehicleListViewData = db;
                        ViewBag.UploadMessage = MtdataResource.invalid_Excel_Fromat;
                        vehicleViewData.FleetList = FleetList(merchantId);
                        
                        if (logOnByUserType == 5)
                        {
                            List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                            List<FleetListViewData> fleetViewResult = vehicleViewData.FleetList.Where(m => fleetListAccessed.Contains((int)m.FleetID)).Select(m => m).Distinct().ToList();
                            vehicleViewData.FleetList = fleetViewResult.AsEnumerable();
                        }

                        vehicleViewData.TerminalList = TerminalList();
                        vehicleViewData.CarOwnerList = _vehicleService.CarOwnerList(merchantId).ToCarOwnerList();
                        ModelState.Clear();

                        return View(vehicleViewData);
                    }

                    _logMessage.Append("Try to get device list from Excel file.");

                    List<VehicleViewData> lstVehicleViewData = UploadExcelFile(uploadFile);

                    _logMessage.Append("Get Device list from Excel file successfully.");
                    _logMessage.Append("Calling Add method of service _terminalService takes terminalDto parameters list.");

                    if (lstVehicleViewData != null && lstVehicleViewData.Count() > 0)
                    {
                        string Company = (((SessionDto)Session["User"]).SessionMerchant.Company);
                        List<VehicleViewData> objVehicelExcelUploadViewData = _vehicleService.AddVehicleList(lstVehicleViewData.ToDtoList(), merchantId, userId, userType).ToDriverViewDataList();

                        _logMessage.Append("Get list of unauthorize vehicle from uploaded excel file.");

                        vehicleViewData.IsVehicleError = true;
                        vehicleViewData.VehicleListViewData = objVehicelExcelUploadViewData;

                        if (lstVehicleViewData != null && objVehicelExcelUploadViewData != null)
                        {
                            int uplodedVehicle = lstVehicleViewData.Count() - objVehicelExcelUploadViewData.Count();

                            if (uplodedVehicle > 0)
                            {
                                ViewBag.UploadMessage = uplodedVehicle + " Vehicle has been created from selected file.";

                                string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                                _logTracking.Append("User " + userName + " add car through excel file " + uploadFile.FileName + " at " + System.DateTime.Now.ToUniversalTime());
                                _logger.LogInfoMessage(_logTracking.ToString());
                            }
                            else
                            {
                                ViewBag.UploadMessage = MtdataResource.No_vehicle_Created;
                            }
                        }
                    }
                    else
                    {
                        vehicleViewData.IsVehicleError = true;
                        List<VehicleViewData> db = new List<VehicleViewData>();
                        vehicleViewData.VehicleListViewData = db;
                        ViewBag.UploadMessage = MtdataResource.No_recordFound;
                    }

                    ModelState.Remove("VehicleNumber");
                    ModelState.Remove("VehicleRegNo");
                    ModelState.Remove("PlateNumber");
                    ModelState.Remove("fk_FleetID");
                    ModelState.Remove("fk_TerminalID");
                    ModelState.Remove("FleetName");
                    ModelState.Remove("DeviceName");
                }
                // Excel Upload code end
                else
                {
                    if (ModelState.IsValid)
                    {
                        _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                        _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                        _logMessage.Append("calling ToVehicleDto method to map VehicleDto to VehicleViewData and retrun VehicleDto");

                        var vehicleDto = vehicleViewData.ToVehicleDto();

                        _logMessage.Append("ToVehicleDto method executed successfully");

                        vehicleDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                        _logMessage.Append("Calling Add method of service _vehicleService takes parameter Vehicle Number =" + vehicleViewData.VehicleNumber + ", Plate Number= " + vehicleViewData.PlateNumber + ", fleet Id= " + vehicleViewData.fk_FleetID + ", Terminal ID= " + vehicleViewData.fk_TerminalID + ", fleet description= " + vehicleViewData.Description);

                        int result = _vehicleService.Add(vehicleDto);
                        if (result != 0)
                        {
                            if (result == 1)
                                ViewBag.Message = MtdataResource.CarNo_Already_Exist;

                            if (result == 2)
                                ViewBag.Message = MtdataResource.CarRegNo_Already_Exist;

                            if (result == 3)
                                ViewBag.Message = MtdataResource.Plate_Already_Exist;
                            
                            vehicleViewData.FleetList = FleetList(merchantId);
                            
                            if (logOnByUserType == 5)
                            {
                                List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                                List<FleetListViewData> fleetViewResult = vehicleViewData.FleetList.Where(m => fleetListAccessed.Contains((int)m.FleetID)).Select(m => m).Distinct().ToList();
                                vehicleViewData.FleetList = fleetViewResult.AsEnumerable();
                            }

                            vehicleViewData.TerminalList = TerminalList();
                            vehicleViewData.CarOwnerList = _vehicleService.CarOwnerList(merchantId).ToCarOwnerList();

                            _logMessage.Append("fleetlist and terminal list populate successfully");

                            return View(vehicleViewData);
                        }

                        string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                        _logTracking.Append("User " + userName + " add carNo." + vehicleDto.VehicleNumber + " at " + System.DateTime.Now.ToUniversalTime());
                        _logger.LogInfoMessage(_logTracking.ToString());
                        _logMessage.Append("Add method of service _vehicleService excuted successfully");

                        return Redirect("Index");
                    }
                }

                _logMessage.Append("populating fleetlist and terminal list");

                vehicleViewData.FleetList = FleetList(merchantId);
                if (userType == 4)
                    vehicleViewData.FleetList = _userService.UserFleetList(userId).ToViewDataList();

                if (logOnByUserType == 5)
                {
                    List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                    List<FleetListViewData> fleetViewResult = vehicleViewData.FleetList.Where(m => fleetListAccessed.Contains((int)m.FleetID)).Select(m => m).Distinct().ToList();
                    vehicleViewData.FleetList = fleetViewResult.AsEnumerable();
                }

                vehicleViewData.TerminalList = TerminalList();
                vehicleViewData.CarOwnerList = _vehicleService.CarOwnerList(merchantId).ToCarOwnerList();

                _logMessage.Append("fleetlist and terminal list populate successfully");

                return View(vehicleViewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose           :  updating the vehicle details
        /// Function Name       :   Update
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/9/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Update(int vehicleId)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
            int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
            int logOnByUserId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserId));
            int logOnByUserType = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserType));
            int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);

            try
            {
                _logMessage.Append("calling GetVehicleDto method of _vehicleService takes parameter vehicleid= " + vehicleId);

                var vehicleDto = _vehicleService.GetVehicleDto(vehicleId);
                _logMessage.Append("GetVehicleDto method executed successfully");
                _logMessage.Append("calling ToVehicleViewData method to map VehicleViewData to VehicleDto and retrun VehicleViewData");

                var vehicleView = vehicleDto.ToVehicleViewData();

                _logMessage.Append("ToVehicleViewData method executed successfully");

                if (userType == 2 || userType == 3)
                {
                    vehicleView.FleetList = FleetList(merchantId);
                    if (logOnByUserType == 5)
                    {
                        List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                        List<FleetListViewData> fleetViewResult = vehicleView.FleetList.Where(m => fleetListAccessed.Contains((int)m.FleetID)).Select(m => m).Distinct().ToList();
                        vehicleView.FleetList = fleetViewResult.AsEnumerable();
                    }

                    vehicleView.CarOwnerList = _vehicleService.CarOwnerList(merchantId).ToCarOwnerList();
                    vehicleView.TerminalList = TerminalList();
                }

                if (userType == 4)
                {
                    vehicleView.FleetList = _userService.UserFleetList(userId).ToViewDataList();

                    if (logOnByUserType == 5)
                    {
                        List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                        List<FleetListViewData> fleetViewResult = vehicleView.FleetList.Where(m => fleetListAccessed.Contains((int)m.FleetID)).Select(m => m).Distinct().ToList();
                        vehicleView.FleetList = fleetViewResult.AsEnumerable();
                    }

                    vehicleView.CarOwnerList = _vehicleService.CarOwnerList(merchantId).ToCarOwnerList();
                    vehicleView.TerminalList = TerminalList();
                }

                _logMessage.Append("calling UpdateTerminalList method of _vehicleService takes parameter TerminalID= " + vehicleView.fk_TerminalID);

                if (vehicleView.fk_TerminalID != null)
                    vehicleView.TerminalList = UpdateTerminalList((int)vehicleView.fk_TerminalID);
                else
                    vehicleView.TerminalList = TerminalList();
                
                _logMessage.Append("UpdateTerminalList method of _vehicleService executed successfully");

                return View(vehicleView);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        ///  Purpose              :  Updating the vehicle records
        /// Function Name       :   Update
        /// Created By          :  Naveen Kumar
        /// Created On          :   1/9/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="vehicleViewData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(int vehicleId, VehicleViewData vehicleViewData)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID == 1)
                return RedirectToAction("index", "Member");
            
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                int userId = ((SessionDto)Session["User"]).SessionUser.UserID;
                int userType = ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID;
                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                int logOnByUserId = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserId));
                int logOnByUserType = Convert.ToInt32((((SessionDto)Session["User"]).SessionUser.LogOnByUserType));

                if (ModelState.IsValid)
                {
                    _logMessage.Append("calling GetVehicleDto method of _vehicleService takes parameter vehicleid= " + vehicleId);
                    _logMessage.Append("GetVehicleDto method executed successfully");
                    _logMessage.Append("calling ToVehicleViewData method to map VehicleViewData to VehicleDto and retrun VehicleViewData");
                    _logMessage.Append("ToVehicleViewData method executed successfully");
                    
                    var prevVehicleDto = _vehicleService.GetVehicleDto(vehicleId);
                    int terminalId = Convert.ToInt32(prevVehicleDto.fk_TerminalID);

                    _logMessage.Append("calling ToVehicleViewData method to map VehicleViewData to VehicleDto and retrun VehicleViewData");

                    VehicleDto vehicleDto = vehicleViewData.ToVehicleDto();

                    _logMessage.Append("ToVehicleViewData method executed successfully");

                    vehicleDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                    _logMessage.Append("calling Update method of _vehicleService takes parameter TerminalID, vehicleId = " + terminalId + ", " + vehicleId + " and vehcleDto parameters are Vehicle Number =" + vehicleViewData.VehicleNumber + ", Plate Number= " + vehicleViewData.PlateNumber + ", fleet Id= " + vehicleViewData.fk_FleetID + ", Terminal ID= " + vehicleViewData.fk_TerminalID + ", fleet description= " + vehicleViewData.Description);

                    int result = _vehicleService.Update(vehicleDto, vehicleId, terminalId);
                    if (result != 0)
                    {
                        if (result == 1)
                            ViewBag.Message = MtdataResource.CarNo_Already_Exist;
                        if (result == 2)
                            ViewBag.Message = MtdataResource.CarRegNo_Already_Exist;
                        if (result == 3)
                            ViewBag.Message = MtdataResource.Plate_number_already_exists;
                        
                        vehicleViewData.FleetList = FleetList(merchantId);
                        
                        if (logOnByUserType == 5)
                        {
                            List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                            List<FleetListViewData> fleetViewResult = vehicleViewData.FleetList.Where(m => fleetListAccessed.Contains((int)m.FleetID)).Select(m => m).Distinct().ToList();
                            vehicleViewData.FleetList = fleetViewResult.AsEnumerable();
                        }

                        vehicleViewData.TerminalList = TerminalList();
                        vehicleViewData.CarOwnerList = _vehicleService.CarOwnerList(merchantId).ToCarOwnerList();

                        _logMessage.Append("fleetlist and terminal list populate successfully");

                        return View(vehicleViewData);
                    }

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + " update car" + vehicleDto.VehicleNumber + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                    _logMessage.Append("UpdateTerminalList method of _vehicleService executed successfully");

                    return Redirect("Index");
                }
                
                _logMessage.Append("populating fleetlist and terminal list");

                vehicleViewData.FleetList = FleetList(merchantId);
                
                if (userType == 4)
                    vehicleViewData.FleetList = _userService.UserFleetList(userId).ToViewDataList();
                
                if (logOnByUserType == 5)
                {
                    List<int> fleetListAccessed = _fleetService.GetAccesibleFleet(logOnByUserId, merchantId);
                    List<FleetListViewData> fleetViewResult = vehicleViewData.FleetList.Where(m => fleetListAccessed.Contains((int)m.FleetID)).Select(m => m).Distinct().ToList();
                    vehicleViewData.FleetList = fleetViewResult.AsEnumerable();
                }

                vehicleViewData.TerminalList = TerminalList();
                vehicleViewData.CarOwnerList = _vehicleService.CarOwnerList(merchantId).ToCarOwnerList();

                _logMessage.Append("fleetlist and terminal list populate successfully");

                return View(vehicleViewData);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return View();
        }

        /// <summary>
        /// ///  Purpose        :  to get the list of fleet
        /// Function Name       :   FleetList
        /// Created By          :  Naveen Kumar
        /// Created On          :   1/19/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<FleetListViewData> FleetList(int merchantId)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetFleetList method of _commonService class to get Fleet List");

                var fleetDtoList = _commonService.GetFleetList(merchantId);

                _logMessage.Append("GetFleetList method of _commonService executed successfully");
                _logMessage.Append("calling ToFleetViewDataSelectList method to map FleetListViewData to FleetListDto and retrun FleetListViewData");

                var fleetList = fleetDtoList.ToFleetViewDataSelectList();

                _logMessage.Append("ToFleetViewDataSelectList method executed successfully");

                return fleetList;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

        /// <summary>
        ///Purpose              :  to get the list of terminal
        /// Function Name       :   TerminalList
        /// Created By          :  Naveen Kumar
        /// Created On          :   1/19/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<TerminalListViewData> TerminalList()
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            _logMessage.Append("Calling GetTerminalList method of _commonService class to get Fleet List");

            int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
            var terminalDtoList = _commonService.GetTerminalList(merchantId);

            _logMessage.Append("GetTerminalList method of _commonService executed successfully");
            _logMessage.Append("calling ToTerminalViewDataSelectList method to map TerminalListViewData to TerminalListDto and retrun TerminalListViewData");

            var terminalList = terminalDtoList.ToTerminalViewDataSelectList();

            _logMessage.Append("ToTerminalViewDataSelectList method executed successfully");

            return terminalList;
        }

        /// <summary>
        ///Purpose              :  to update the list of terminal
        /// Function Name       :  UpdateTerminalList
        /// Created By          :  Naveen Kumar
        /// Created On          :  1/21/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<TerminalListViewData> UpdateTerminalList(int terminalId)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                _logMessage.Append("Calling GetTerminalList method of _commonService class to get terminal List");

                int merchantId = Convert.ToInt32(((SessionDto)Session["User"]).SessionUser.fk_MerchantID);
                var terminalDtoList = _commonService.GetTerminalList(merchantId);

                _logMessage.Append("GetTerminalList method of _commonService executed successfully");

                List<TerminalListViewData> terminalList = terminalDtoList.ToTerminalViewDataSelectList().ToList();

                _logMessage.Append("Calling GetTerminalDto method of _terminalService class to get terminal with terminal id =" + terminalId);

                var terminalDto = _terminalService.GetTerminalDto(terminalId);

                _logMessage.Append("GetTerminalDto method of _terminalService executed successfully ");
                _logMessage.Append("calling ToTerminalViewData method to map TerminalViewData to TerminalDto and retrun TerminalViewData");

                var viewData = terminalDto.ToTerminalViewData();
                
                _logMessage.Append("ToTerminalViewData method executed successfully");
                _logMessage.Append("using object intilizer to assign TerminalID, DeviceName to TerminalListViewData thats are " + viewData.TerminalID + ", " + viewData.DeviceName);

                var terminal = new TerminalListViewData
                {
                    TerminalID = viewData.TerminalID,
                    MacAdd = viewData.MacAdd
                };

                _logMessage.Append("adding terminal to terminalList");

                terminalList.Add(terminal);

                return terminalList;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return null;
        }

        /// <summary>
        ///Purpose              : Applying page indexing to display the vehicle screen
        /// Function Name       :   GetVehicleListByFilter
        /// Created By          : umesh kumar
        /// Created On          :   2/4/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult GetVehicleListByFilter(string name = "", string carOwner = "", string androidMac = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetAdminUserListByFilter method of adminUserService class to get Admin Users List");
                
                int viewDataListCount = 0;
                IEnumerable<VehicleDto> userVechile = new List<VehicleDto>();
                
                var sessionData = (SessionDto)Session["User"];
                int merchantId = Convert.ToInt32(sessionData.SessionUser.fk_MerchantID);
                int logOnByUserId = Convert.ToInt32(sessionData.SessionUser.LogOnByUserId);
                int logOnByUserType = Convert.ToInt32(sessionData.SessionUser.LogOnByUserType);
                int userType = sessionData.SessionUser.fk_UserTypeID;
                
                FilterSearchParametersDto searchDto = new FilterSearchParametersDto()
                {
                    Name = name,
                    StartIndex = jtStartIndex,
                    PageSize = jtPageSize,
                    Sorting = jtSorting,
                    MerchantId = merchantId,
                    LogOnByUserId = logOnByUserId,
                    LogOnByUserType = logOnByUserType,
                    OwnerName = carOwner,
                    SerialNumber = androidMac
                };

                if (userType != 4)
                {
                    userVechile = _vehicleService.GetVehileListByFilter(searchDto);
                    viewDataListCount = _vehicleService.GetVehileCount(searchDto).Count();
                }

                int userId = sessionData.SessionUser.UserID;
                var userfleet = _userService.GetUsersFleet(userId);//2

                if (userType == 4)
                {
                    FilterSearchParametersDto filterSearch = new FilterSearchParametersDto()
                    {
                        Name = name,
                        StartIndex = jtStartIndex,
                        PageSize = jtPageSize,
                        Sorting = jtSorting,
                        MerchantId = merchantId,
                        UserId = userId,
                        UserType = userType,
                        userFleet = userfleet,
                        OwnerName = carOwner,
                        LogOnByUserType = logOnByUserType,
                        LogOnByUserId = logOnByUserId,
                        SerialNumber = androidMac
                    };

                    userVechile = _vehicleService.UserVehicleList(filterSearch);
                    viewDataListCount = _vehicleService.UserVehicleCount(filterSearch);
                }

                if (string.IsNullOrEmpty(name))
                {
                    _logMessage.Append("Calling GetVehicleDtoList method of _vehicleService class to count fee");
                    _logMessage.Append("GetVehicleDtoList method of _vehicleService executed successfully result = " + viewDataListCount);

                    return Json(new { Result = "OK", Records = userVechile, TotalRecordCount = viewDataListCount });
                }

                var vehicleDtos = userVechile as VehicleDto[] ?? userVechile.ToArray();
                return Json(new { Result = "OK", Records = vehicleDtos, TotalRecordCount = viewDataListCount });
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
                return Json(new { Result = "ERROR", exception.Message });
            }
        }

        /// <summary>
        ///Purpose              : To Delete vehicle
        /// Function Name       : Delete
        /// Created By          : umesh kumar
        /// Created On          : 2/4/2015
        /// Modifications Made   : ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(int vehicleId)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                try
                {
                    _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                    _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                    _logMessage.Append("Calling DeleteVehicle method of _vehicleService class takes vehicleId= " + vehicleId);

                    _vehicleService.DeleteVehicle(vehicleId);

                    _logMessage.Append("DeleteVehicle method of _vehicleService class executed successfully");

                    string userName = ((SessionDto)Session["User"]).SessionUser.Email;
                    _logTracking.Append("User " + userName + " delete car ID" + vehicleId + " at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());

                    Thread.Sleep(50);

                    return Json(new { Result = "OK" });
                }
                catch (Exception exception)
                {
                    _logger.LogInfoFatal(_logMessage.ToString(), exception);
                }
            }

            return Json(new { Result = "No" });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public List<VehicleViewData> UploadExcelFile(HttpPostedFileBase uploadFile)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            List<VehicleViewData> objVehicleDataView = new List<VehicleViewData>();

            try
            {
                if (uploadFile.ContentLength > 0)
                {
                    if (!Directory.Exists(Server.MapPath("../Uploads")))
                        Directory.CreateDirectory(Server.MapPath("../Uploads"));

                    string filePath = Path.Combine(HttpContext.Server.MapPath("../Uploads"), Path.GetFileName(uploadFile.FileName));
                    uploadFile.SaveAs(filePath);
                    string line = string.Empty;
                    string[] strArray = null;

                    // work out where we should split on comma, but not in a sentance
                    Regex r = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

                    //Set the filename in to our stream
                    StreamReader sr = new StreamReader(filePath);

                    //Read the first line and split the string at , with our regular express in to an array
                    line = sr.ReadLine();
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (!string.IsNullOrEmpty(line))
                            strArray = r.Split(line);

                        VehicleViewData Obj = new VehicleViewData();
                        Obj.VehicleNumber = strArray[0].Trim();
                        Obj.VehicleRegNo = strArray[1].Trim();
                        Obj.PlateNumber = strArray[2].Trim();
                        Obj.FleetName = strArray[3].Trim();
                        Obj.DeviceName = strArray[4].Trim();
                        Obj.Description = strArray[5].Trim();
                        Obj.CarOwnerEmail = strArray[6].Trim();
                        Obj.CreatedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                        Obj.ModifiedBy = ((SessionDto)Session["User"]).SessionUser.Email;
                        objVehicleDataView.Add(Obj);
                    }

                    sr.Dispose();
                }

                return objVehicleDataView;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return null;
            }
        }

        /// <summary>
        /// Process the file supplied and process the CSV to a dynamic datatable
        /// </summary>
        /// <param name="fileName">String</param>
        /// <returns>DataTable</returns>
        private DataTable ProcessCSV(string fileName)
        {
            DataTable dt = new DataTable();
            try
            {
                //Set up our variables 
                string line = string.Empty;
                string[] strArray = null;
                DataRow row;

                // work out where we should split on comma, but not in a sentance
                Regex r = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

                //Set the filename in to our stream
                StreamReader sr = new StreamReader(fileName);

                //Read the first line and split the string at , with our regular express in to an array
                line = sr.ReadLine();
                if (!string.IsNullOrEmpty(line))
                    strArray = r.Split(line);       // For each item in the new split array, dynamically builds our Data columns. Save us having to worry about it.

                Array.ForEach(strArray, s => dt.Columns.Add(new DataColumn()));
                while ((line = sr.ReadLine()) != null)
                {
                    row = dt.NewRow();

                    row.ItemArray = r.Split(line);
                    dt.Rows.Add(row);
                }

                sr.Dispose();

                // Return the new DataTable
                return dt;
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return dt;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public FileResult Download(string file)
        {
            string CurrentFileName = (from fls in GetFiles()
                                      where fls.FileName == file
                                      select fls.FilePath).First();

            string contentType = string.Empty;

            if (CurrentFileName.Contains(".pdf"))
                contentType = "application/pdf";
            else if (CurrentFileName.Contains(".docx"))
                contentType = "application/docx";
            else if (CurrentFileName.Contains(".csv"))
                contentType = "application/csv";

            return File(CurrentFileName, contentType, file);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<DownloadFileViewData> GetFiles()
        {
            List<DownloadFileViewData> lstFiles = new List<DownloadFileViewData>();
            DirectoryInfo dirInfo = new DirectoryInfo(Server.MapPath("../Assets/TemplateFiles"));

            int i = 0;
            foreach (var item in dirInfo.GetFiles())
            {
                lstFiles.Add(new DownloadFileViewData()
                {

                    FileId = i + 1,
                    FileName = item.Name,
                    FilePath = dirInfo.FullName + @"\" + item.Name
                });
                i = i + 1;
            }

            return lstFiles;
        }

        /// <summary>
        ///  Created By : Umesh K3
        /// </summary>
        /// <param name="uploadFile"></param>
        /// <returns></returns>
        private bool IsValidExcel(HttpPostedFileBase uploadFile)
        {
            string line = string.Empty;
            string filePath = string.Empty;

            if (uploadFile.ContentLength > 0)
            {
                if (!Directory.Exists(Server.MapPath("../Uploads")))
                    Directory.CreateDirectory(Server.MapPath("../Uploads"));

                filePath = Path.Combine(HttpContext.Server.MapPath("../Uploads"),
                Path.GetFileName(uploadFile.FileName));
                uploadFile.SaveAs(filePath);
            }

            //Set the filename in to our stream
            StreamReader sr = new StreamReader(filePath);
            string CarExcel = "Car Number,Car Registration No,Plate Number,Fleet,Terminal,Description,Car Owner Email";
            
            //Read the first line 
            line = sr.ReadLine();
            sr.Dispose();

            if (!string.IsNullOrEmpty(line))
            {
                if (CarExcel != line)
                    return false;
                else
                    return true;
            }

            return false;
        }

    }
}