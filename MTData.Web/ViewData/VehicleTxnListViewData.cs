﻿namespace MTData.Web.ViewData
{
    public class VehicleTxnListViewData
    {
        public string VehicleText { get; set; }
        public int FleetId { get; set; }
    }
}