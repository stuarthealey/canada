﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTDataDispatchSystemSync.Models
{
    public class ESIExpiryDateDefinitions
    {
        public int OwnerID { get; set; }
        public int ExpiryID { get; set; }
        public string DateLabel { get; set; }
        public DateTime Date { get; set; }
        public string FieldLabel { get; set; }
        public string FieldText { get; set; }
    }
}