﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CanadaCert
{
    public class PingResponse
    {
        [System.Xml.Serialization.XmlElementAttribute("ServiceCost", typeof(ServiceCost))]
        public List<ServiceCost> ServiceCost { get; set; }
    }
}