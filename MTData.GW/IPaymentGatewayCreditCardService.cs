﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using MTData.WCFGW.Model;

namespace MTData.WCFGW
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
	[System.Web.Services.WebServiceBindingAttribute(Name = "PaymentGatewayCreditCardService", Namespace = "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService")]
	[System.Xml.Serialization.XmlIncludeAttribute(typeof(CreateRCCIReq))]
	[System.Xml.Serialization.XmlIncludeAttribute(typeof(CreateRCCIResp))]
	[System.Xml.Serialization.XmlIncludeAttribute(typeof(ValidateRCCIReq))]
	[System.Xml.Serialization.XmlIncludeAttribute(typeof(ValidateRCCIResp))]
	[ServiceContract(Namespace = "http://schemas.mtdata.com.au/external/2.26.0/PaymentGatewayCreditCardService")]
	public interface IPaymentGatewayCreditCardService
	{
		[OperationContract]
		CreateRCCIResp CreateRCCI(CreateRCCIReq request);

		[OperationContract]
		ValidateRCCIResp ValidateRCCI(ValidateRCCIReq request);
	}
}
