IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MonthlyGraphDollarVolumeReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_MonthlyGraphDollarVolumeReport]
GO

-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <15-07-2016>
-- Description:	<Prepare monthly graph dollar volume report for admin users>
-- =============================================
CREATE PROCEDURE dbo.usp_MonthlyGraphDollarVolumeReport
AS
BEGIN
	BEGIN TRY
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SET NOCOUNT ON
		SET ANSI_WARNINGS OFF

		DECLARE @DateFromThisMonth DATETIME,
				@DateToThisMonth DATETIME,
				@DateFromMonth1 DATETIME,
				@DateToMonth1 DATETIME,
				@DateFromMonth2 DATETIME,
				@DateToMonth2 DATETIME,
				@DateFromMonth3 DATETIME,
				@DateToMonth3 DATETIME,
				@DateFromMonth4 DATETIME,
				@DateToMonth4 DATETIME,
				@DateFromMonth5 DATETIME,
				@DateToMonth5 DATETIME,
				@DateFromMonth6 DATETIME,
				@DateToMonth6 DATETIME

		SELECT  @DateFromThisMonth = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH, 0, GETDATE())), 0),
				@DateToThisMonth = GETDATE(),
				@DateFromMonth1 =DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH, -1, GETDATE())), 0),
				@DateToMonth1 = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH, 0, GETDATE())), 0),
				@DateFromMonth2 = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH, -2, GETDATE())), 0),
				@DateToMonth2 = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH, -1, GETDATE())), 0),
				@DateFromMonth3 = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH,-3, GETDATE())), 0),
				@DateToMonth3 = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH, -2, GETDATE())), 0),
				@DateFromMonth4 = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH,-4, GETDATE())), 0),
				@DateToMonth4 = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH, -3, GETDATE())), 0),
				@DateFromMonth5 = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH,-5, GETDATE())), 0),
				@DateToMonth5 = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH, -4, GETDATE())), 0),
				@DateFromMonth6 = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH,-6, GETDATE())), 0),
				@DateToMonth6 = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH, -5, GETDATE())), 0)

		DECLARE @DataTable TABLE (
			Id INT,
			FleetId INT,
			Txndate DATETIME,
			Amount Decimal(18, 2)
			)

		INSERT INTO @DataTable
		SELECT Id, fk_FleetId, TxnDate, Amount 
		FROM dbo.MTDTransaction 
		WHERE ResponseCode IN ('00', '000', '002') 
		  AND TxnType IN ('Sale', 'Completion') 
		  AND IsRefunded = 0 
		  AND IsVoided = 0                    

		SELECT  (SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE Txndate >= @DateFromThisMonth AND Txndate < @DateToThisMonth) AS 'TransAmtThisMonth',
									(SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE Txndate >= @DateFromMonth1 AND Txndate < @DateToMonth1) AS 'TransAmtOneMonthAgo',
									(SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE Txndate >= @DateFromMonth2 AND Txndate < @DateToMonth2) AS 'TransAmtTwoMonthsAgo',
									(SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE Txndate >= @DateFromMonth3 AND Txndate < @DateToMonth3) AS 'TransAmtThreeMonthsAgo',
									(SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE Txndate >= @DateFromMonth4 AND Txndate < @DateToMonth4) AS 'TransAmtFourMonthsAgo',
									(SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE Txndate >= @DateFromMonth5 AND Txndate < @DateToMonth5) AS 'TransAmtFiveMonthsAgo',
									(SELECT CAST(COALESCE(SUM(Amount), 0) AS DECIMAL(16, 2)) FROM @DataTable WHERE Txndate >= @DateFromMonth6 AND Txndate < @DateToMonth6) AS 'TransAmtSixMonthsAgo'						  
	END TRY
	BEGIN CATCH
		  INSERT INTO dbo.ProcErrorHandler(ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, UserName, HostName, [TimeStamp])
		  SELECT ERROR_NUMBER(), ERROR_SEVERITY(), ERROR_STATE(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_MESSAGE(), SUSER_SNAME(), HOST_NAME(), GETDATE()
	END CATCH
END
GO