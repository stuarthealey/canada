//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MTD.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Datawire
    {
        public Datawire()
        {
            this.Terminals = new HashSet<Terminal>();
        }
    
        public int ID { get; set; }
        public string DID { get; set; }
        public string DatewireXml { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> IsAssigned { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> MerchantId { get; set; }
        public string URL { get; set; }
        public string RCTerminalId { get; set; }
        public string Stan { get; set; }
        public string RefNumber { get; set; }
        public Nullable<System.DateTime> TransactionDate { get; set; }
    
        public virtual ICollection<Terminal> Terminals { get; set; }
        public virtual Merchant Merchant { get; set; }
    }
}
