﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;


namespace MTData.WCFGW
{
    public static class ServiceHelper
    {
        /// <summary>
        /// Call web api for payment
        /// </summary>
        /// <param name="requestPacket"></param>
        /// <returns></returns>
        public static string RequestAPI(string requestPacket, string ActionUrl)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ActionUrl);
            request.ContentType = "application/soap+xml";
            request.Method = "POST";
            request.KeepAlive = true;

            var requestStream = request.GetRequestStream();
            var bytes = Encoding.UTF8.GetBytes(requestPacket);
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();

            var response = (HttpWebResponse)request.GetResponse();
            var responseContent = new StreamReader(response.GetResponseStream()).ReadToEnd();
            return responseContent;
        }

        /// <summary>
        /// Convert Object into XML without UTF Encoding
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ObjectToXML(object obj)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(obj.GetType());
                using (MemoryStream xmlStream = new MemoryStream())
                {
                    xmlSerializer.Serialize(xmlStream, obj);
                    xmlStream.Position = 0;
                    xmlDoc.Load(xmlStream);
                    return xmlDoc.InnerXml;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Convert XML string into Object.
        /// </summary>
        /// <param name="XMLstring">XML string.</param>
        /// <param name="ObjectType">Required object type.</param>
        /// <returns></returns>
        public static Object XmlToObject(string XMLstring, Type ObjectType)
        {

            try
            {
                StringReader StrReader = new StringReader(XMLstring);
                XmlTextReader XmlReader = new XmlTextReader(StrReader);
                XmlSerializer serializer = new XmlSerializer(ObjectType);
                Object AnObject = serializer.Deserialize(XmlReader);
                StrReader.Close();
                return AnObject;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Remove extra characters from response.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static string FormatRequest(string data)
        {
            data = data.Replace("\\", "");
            int fi, li;
            fi = data.IndexOf("\"");
            li = data.LastIndexOf("\"");
            data = data.Substring(fi + 1, li - 1);
            return data;
        }

        /// <summary>
        /// Returns an application setting based on the passed in string. Used primarily to cut down on typing.
        /// </summary>
        /// <param name="Key">The name of the key</param>
        /// <returns>The value of the app setting in the web.Config or String.Empty if no setting found</returns>
        public static string AppSetting(this string Key)
        {
            string Value = string.Empty;
            if (ConfigurationManager.AppSettings[Key] != null)
                Value = ConfigurationManager.AppSettings[Key];
            return Value;
        }
    }
}