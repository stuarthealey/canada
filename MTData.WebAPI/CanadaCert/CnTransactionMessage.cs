﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Configuration;
using System.ComponentModel;
using System.Text.RegularExpressions;

using MTD.Core.DataTransferObjects;
using MTData.Utility;
using MTData.WebAPI.Models;
using MTData.WebAPI.CnPrivateBit63;
using MTData.WebAPI.CanadaCommon;
using MTData.WebAPI.Common;
using MTData.WebAPI.Resources;

namespace MTData.WebAPI.CanadaCert
{
    public class CnTransactionMessage
    {
        private string _motoBillECommIndicator = string.Empty;
        private string _marketSpecificDataIndicator = string.Empty;
        readonly ILogger _logger = new Logger();

        /// <summary>
        /// Purpose             :   To create payload in iso8583 message format
        /// Function Name       :   CreateMessage
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/25/2016
        /// Modification Made   :   ****************************
        /// Modified On       :    "MM/DD/YYYY" 
        /// </summary>
        /// <param name="objTransactionEntities"></param>
        /// <returns></returns>
        public string CreateMessage(CnTransactionEntities objTransactionEntities)
        {
            try
            {
                string messageID = string.Empty;
                string dataElements = string.Empty;

                // Set the Message ID on the basis of Transaction Type
                messageID = FormatFields.FormatMessageType(objTransactionEntities.TransType.ToString(), objTransactionEntities.CardType.ToString(), objTransactionEntities.IsTORTransaction);
                MessageType type = (MessageType)Enum.Parse(typeof(MessageType), objTransactionEntities.CardType.ToString() + objTransactionEntities.TransType.ToString());

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.CreateMessage() messageID:{0};dataElements:{1};type:{2};IsTOR:{3};IsTA:{4};", messageID, dataElements, type, objTransactionEntities.IsTORTransaction, objTransactionEntities.IsTransArmor));

                // Get Message with Bitmap and Data Elements.
                dataElements = GetMessage(type, objTransactionEntities);    // pass payment type with transaction typr like CreditSale

                string finalMessage = string.Concat(messageID, dataElements);

                return finalMessage;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal("CnTransactionMessage.CreateMessage();", ex);

                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To fill the data elements in dictionary that will be used for doing transactions 
        /// Function Name       :   FillData
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/25/2016
        /// Modification Made   :   ****************************
        /// Modified On       :    "MM/DD/YYYY" 
        /// </summary>
        /// <param name="objTransactionEntities"></param>
        /// <returns></returns>
        private Dictionary<Element, string> FillData(CnTransactionEntities objTransactionEntities)
        {
            Dictionary<Element, string> objDataDictionary = new Dictionary<Element, string>();
            try
            {
                var objTransactionProperties = objTransactionEntities.GetType().GetProperties();
                foreach (var item in objTransactionProperties)
                {
                    var bitmapProperty = typeof(CnBitmapDefinition).GetProperty(item.Name);
                    if (!object.Equals(bitmapProperty, null))
                    {
                        var bitmapDefination = (Element)bitmapProperty.GetValue(null);

                        //PAY-13 If this is a TransArmor transaction, then we must EXCLUDE certain elements.
                        bool excludeElement = false;
                        string excludeReason = string.Empty;

                        if (objTransactionEntities.IsTransArmor && (bitmapDefination.ElementID == 2) && (objTransactionEntities.CardIssuer == CardIssuer.Interac))
                        {
                            excludeElement = true;
                            excludeReason = "CnTransactionMessage.FillData() ElementID:2 PAN excluded from TransArmor Data for Interac card type;";
                        }

                        // Exclude Element 2 for TransArmor refund
                        if (objTransactionEntities.IsTransArmor && (bitmapDefination.ElementID == 2) && (objTransactionEntities.TransType == TransactionType.Refund.ToString()))
                        {
                            excludeElement = true;
                            excludeReason = "CnTransactionMessage.FillData() ElementID:2 PAN excluded from TransArmor Data for Refund TransType;";
                        }

                        // Exclude Element 14 (Expiry Date) for TransArmor Interac
                        if (objTransactionEntities.IsTransArmor && (bitmapDefination.ElementID == 14) && (objTransactionEntities.CardIssuer == CardIssuer.Interac))
                        {
                            excludeElement = true;
                            excludeReason = "CnTransactionMessage.FillData() ElementID:14 Expiry Date excluded from TransArmor Interac transaction;";
                        }

                        // Contact EMV to NOT include ElementID 14 ExpDate for Interac, as this is the card number.
                        if (objTransactionEntities.IsTransArmor && (objTransactionEntities.EntryMode == EntryMode.EmvContact.ToString()) 
                            && (bitmapDefination.ElementID == 14) && (objTransactionEntities.CardIssuer == CardIssuer.Interac))
                        {
                            excludeElement = true;
                            excludeReason = "CnTransactionMessage.FillData() ElementID:14 excluded from TransArmor Data for Interac Contact data;";
                        }

                        if (objTransactionEntities.IsTransArmor && (bitmapDefination.ElementID == 35))
                        {
                            excludeElement = true;
                            excludeReason = "CnTransactionMessage.FillData() ElementID:35 excluded from TransArmor Data;";
                        }

                        // Contactless EMV to NOT include ElementID 2, as this is the card number.
                        if ((objTransactionEntities.EntryMode == EntryMode.EmvContactless.ToString()) && (bitmapDefination.ElementID == 2))
                        {
                            excludeElement = true;
                            excludeReason = "CnTransactionMessage.FillData() ElementID:2 excluded from EmvContactless data;";
                        }

                        if (!excludeElement)
                        {
                            var transactionEntitiesValues = item.GetValue(objTransactionEntities);
                            if (!string.IsNullOrEmpty(Convert.ToString(transactionEntitiesValues)))
                            {
                                //Debug output of elements collection.
                                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.FillData() ElementID:{0};Value:{1};IsTA:{2};IsTOR:{3};", bitmapDefination.ElementID, transactionEntitiesValues, objTransactionEntities.IsTransArmor, objTransactionEntities.IsTORTransaction));

                                //apply ISO 8583 message format
                                transactionEntitiesValues = FormatFields.ApplyISOFormatting(bitmapDefination, transactionEntitiesValues, objTransactionEntities.IsTransArmor);

                                //apply datawire format for BCD format type elements
                                if ((bitmapDefination.Format == FormatType.BCD || bitmapDefination.Format == FormatType.HD) && bitmapDefination.ElementID != 48 && bitmapDefination.ElementID != 52)
                                    transactionEntitiesValues = ApplyDatawireFormat(Convert.ToString(transactionEntitiesValues));

                                //add the formatted elements to the dictionary
                                objDataDictionary.Add(bitmapDefination, Convert.ToString(transactionEntitiesValues));
                            }
                        }
                        else
                        {
                            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                _logger.LogInfoMessage(excludeReason);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal("CnTransactionMessage.FillData();", ex);

                throw;
            }

            return objDataDictionary;
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Function Name       :   GetMessage
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/25/2016
        /// Modification Made   :   ****************************
        /// Modified On       :    "MM/DD/YYYY" 
        /// </summary>
        /// <param name="objMessageType"></param>
        /// <param name="objTransactionEntities"></param>
        /// <returns></returns>
        public string GetMessage(MessageType objMessageType, CnTransactionEntities objTransactionEntities, int index = 0, string entryMode = null)
        {
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                _logger.LogInfoMessage(string.Format("CnTransactionMessage.GetMessage() msgType:{0};IndustryType:{1};IsTA:{2};TokenType:{3};TxnType:{4};", objMessageType, objTransactionEntities.IndustryType, objTransactionEntities.IsTransArmor, objTransactionEntities.TokenType, objTransactionEntities.TransType));

            string bitMessage = string.Empty;
            string dataElement = string.Empty;

            try
            {
                bool isBitmap70 = false;
                string networkInfoCode = string.Empty;

                // Fill the dictionary with value of Transaction Entities.
                Dictionary<Element, string> objElements = FillData(objTransactionEntities);

                // Sets the additional data for the cards as per ISO(bit 63 tables data)
                objElements = SetAdditionalData(objMessageType, objElements, objTransactionEntities, index, entryMode);

                switch (Convert.ToString(objTransactionEntities.IndustryType))
                {
                    case SigmaStaticValue.BookerAppToken:
                        networkInfoCode = SigmaStaticValue.NetworkInfoCodeForTokenOnly;
                        isBitmap70 = true;
                        break;
                    case SigmaStaticValue.CapkeyIndustry:
                        networkInfoCode = SigmaStaticValue.NetworkInfoCodeForCapKeyDownload;
                        isBitmap70 = true;
                        break;
                    case SigmaStaticValue.KeyUpdateRequest:
                        networkInfoCode = SigmaStaticValue.NetworkInfoCodeForKeyUpdateRequest;
                        isBitmap70 = true;
                        break;
                    case SigmaStaticValue.NewKeyRequest:
                        networkInfoCode = SigmaStaticValue.NetworkInfoCodeForNewKeyRequest;
                        isBitmap70 = true;
                        break;
                }

                if (isBitmap70)
                {
                    Element element = new Element();
                    element.ElementID = 70;
                    element.Format = FormatType.BCD;
                    element.LengthType = LengthType.Fixed;
                    element.Type = DataType.Numeric;
                    objElements.Add(element, networkInfoCode);
                }

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.GetMessage() ElementCount:{0};", objElements.Count));

                // Append all the values of Element from Dictionary to String variable.
                if (objElements.Count > 0)
                {
                    dataElement = objElements.Select(a => a.Value).Aggregate((x, y) => x + y);

                    // Debug output of elements collection.
                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    {
                        foreach (KeyValuePair<Element, string> e in objElements)
                        {
                            Element key = e.Key;
                            _logger.LogInfoMessage(string.Format("CnTransactionMessage.GetMessage() ElementID:{0};Value:{1};", key.ElementID, e.Value));
                        }
                    }
                }

                // Set the Bitmap Message in HexaDecimal format.
                var PrimaryBitmap = FormatFields.PrepareBitmap(objElements);
                PrimaryBitmap = ApplyDatawireFormat(PrimaryBitmap);

                // Return the string with bitmap and its associated data.
                bitMessage = string.Concat(PrimaryBitmap, dataElement);

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.GetMessage() PrimaryBitmap:{0};dataElement:{1};bitMsg:{2};", PrimaryBitmap, dataElement, bitMessage));
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(string.Format("CnTransactionMessage.GetMessage() Ex:{0};", ex.ToString()), ex);
                throw;
            }

            return bitMessage;
        }

        /// <summary>
        /// Purpose             :   To apply data wire format
        /// Function Name       :   ApplyDatawireFormat
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/25/2016
        /// Modification Made   :   ****************************
        /// Modified On       :    "MM/DD/YYYY" 
        /// </summary>
        /// <param name="dataToFormat"></param>
        /// <returns></returns>       
        public static string ApplyDatawireFormat(string dataToFormat)
        {
            string result = string.Empty;
            if (dataToFormat.Length > 2)
            {
                var dataArray = GetChunks(dataToFormat, 2);
                result = Convert.ToString(SigmaStaticValue.Pipe) + string.Join(Convert.ToString(SigmaStaticValue.Pipe), dataArray);
            }
            else
                result = Convert.ToString(SigmaStaticValue.Pipe) + dataToFormat;

            return result;
        }

        /// <summary>
        /// Purpose             :   To break data in to chunks
        /// Function Name       :   GetChunks
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/25/2016
        /// Modification Made   :   ****************************
        /// Modified On       :    "MM/DD/YYYY" 
        /// </summary>
        /// <param name="str"></param> 
        /// <param name="maxChunkSize"></param>
        /// <returns></returns>
        /// 
        private static IEnumerable<string> GetChunks(string str, int maxChunkSize)
        {
            for (int iCount = 0; iCount < str.Length; iCount += maxChunkSize)
            {
                yield return str.Substring(iCount, Math.Min(maxChunkSize, str.Length - iCount));
            }
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Purpose             :   Set additional data for ecomerce and retail
        /// Function Name       :   SetAdditionalData
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/25/2016
        /// Modification Made   :   ****************************
        /// Modified On       :    "MM/DD/YYYY" 
        /// </summary>
        /// <param name="objMessageType"></param>
        /// <param name="objElements"></param>
        /// <param name="objTransactionEntities"></param> 
        /// <returns></returns>      
        private Dictionary<Element, string> SetAdditionalData(MessageType objMessageType, Dictionary<Element, string> objElements, CnTransactionEntities objTransactionEntities, int index, string entryMode)
        {
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetAdditionalData() objMessageType:{0};IndustryType:{1};IsTA:{2};TokenType:{3};", objMessageType, objTransactionEntities.IndustryType, objTransactionEntities.IsTransArmor, objTransactionEntities.TokenType));

            try
            {
                switch (objTransactionEntities.IndustryType)
                {
                    case IndustryType.Ecommerce:
                    case IndustryType.BookerAppPayment:
                        objElements = SetEcommAdditionalData(objMessageType, objElements, objTransactionEntities);
                        break;
                    case IndustryType.Retail:
                        if (entryMode == EntryMode.EmvContact.ToString() || entryMode == EntryMode.EmvContactless.ToString() || entryMode == EntryMode.FSwiped.ToString())
                            objElements = SetEmvAdditionalData(objMessageType, objElements, objTransactionEntities);
                        else
                            objElements = SetRetailAdditionalData(objMessageType, objElements, objTransactionEntities);
                        break;
                    case IndustryType.BookerAppToken:
                        objElements = SetAdditionalDataForTokenOnly(objElements, objTransactionEntities);
                        break;
                    case IndustryType.KeyUpdateRequest:
                        objElements = SetAdditionalDataForKeyUpdate(objMessageType, objElements, objTransactionEntities);
                        break;
                    case IndustryType.CAPKDownload:
                        objElements = SetAdditionalDataForCapKey(objMessageType, objElements, objTransactionEntities, index);
                        break;
                    case IndustryType.NewKeyRequest:
                        objElements = SetAdditionalDataForNewKeyRequest(objElements, objTransactionEntities);
                        break;
                }

                return objElements;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(string.Format("CnTransactionMessage.SetEmvAdditionalData() Ex:{0};", ex.ToString()), ex);
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   Set additional table lenght
        /// Function Name       :   SetAdditionalTableLength
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/25/2016
        /// Modification Made   :   ****************************
        /// Modified On       :    "MM/DD/YYYY" 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static string SetAdditionalTableLength(string data)
        {
            string totalLength = string.Empty;
            int seperatorCount = data.Count(m => m == SigmaStaticValue.Pipe);
            int dataLength = data.Replace(Convert.ToString(SigmaStaticValue.Pipe), string.Empty).Length;
            totalLength = Convert.ToString(SigmaStaticValue.Pipe) + Convert.ToString(dataLength - seperatorCount).PadLeft(4, '0').Insert(2, Convert.ToString(SigmaStaticValue.Pipe));
            return totalLength;
        }

        /// <summary>
        /// Purpose             :   Set private bit 63 table properties  
        /// Function Name       :   SetTableProperties
        /// Created By          :   Naveen Kumar
        /// Created On          :   02/05/2016
        /// Modification Made   :   ****************************
        /// Modified On       :    "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// 
        private static string SetTableProperties(object tableToFill)
        {
            var tableType = tableToFill.GetType();
            var objProperties = tableType.GetProperties();
            string tableValues = string.Empty;

            foreach (var item in objProperties)
            {
                var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                if (!String.IsNullOrEmpty(propertyValue))
                    tableValues += propertyValue;
            }

            tableValues = SetAdditionalTableLength(tableValues) + tableValues;

            return tableValues;
        }

        /// <summary>
        /// Purpose             :   Get enum description
        /// Function Name       :   GetEnumDescription
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/25/2016
        /// Modification Made   :   ****************************
        /// Modified On       :    "MM/DD/YYYY" 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// 
        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        /// <summary>
        /// Purpose             :   Pass transaction data in CnTransactionEnities class 
        /// Function Name       :   MakeRetailTansRequest
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/25/2016
        /// Modification Made   :   ****************************
        /// Modified On       :    "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        /// 
        public static CnTransactionEntities MakeRetailTansRequest(TransactionRequest transactionRequest)
        {
            ILogger logger = new Logger();

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                logger.LogInfoMessage(string.Format("CnTransactionMessage.MakeRetailTransRequest() paymentType:{0};cardType:{1};industryType:{2};txnType:{3};", transactionRequest.PaymentType, transactionRequest.CardType, transactionRequest.IndustryType, transactionRequest.TransactionType));

            return new CnTransactionEntities
            {
                CardType = (CardType)Enum.Parse(typeof(CardType), transactionRequest.PaymentType.ToString()),
                TransType = transactionRequest.TransactionType,
                CardIssuer = (CardIssuer)Enum.Parse(typeof(CardIssuer), transactionRequest.CardType.ToString()),
                IndustryType = (IndustryType)Enum.Parse(typeof(IndustryType), transactionRequest.IndustryType),
                ProcessingCode = FormatFields.PrepareProcessingCode(transactionRequest.PaymentType.ToString(), transactionRequest.TransactionType.ToString(), transactionRequest.EntryMode.ToString()),
                AmountOfTransaction = transactionRequest.TransactionAmount,
                TransmissionDateTime = FormatFields.FormatTransDateTime(transactionRequest.TrnmsnDateTime),
                SystemTrace = transactionRequest.Stan,
                TimeLocalTransmission = FormatFields.FormatLocalTime(transactionRequest.LocalDateTime),
                DateLocalTrans = FormatFields.FormatLocalDate(transactionRequest.LocalDateTime),
                CardExpirationDate = FormatFields.FormatExpiryDate(transactionRequest.ExpiryDate, transactionRequest.TransactionType, transactionRequest.IndustryType),
                MerchantCategoryCode = SigmaStaticValue.MCCForRetail,
                PosEntryModePinCapability = FormatFields.FormatPosEntryPinCapability(transactionRequest.EntryMode),
                NetworkInternationId = SigmaStaticValue.NetworkInternationId,
                PosConditionCode = SigmaStaticValue.PosConditionCodeZeroZero,
                AcquirerReference = FormatFields.PrepareAcquirerRef(transactionRequest.TransactionType, transactionRequest.SourceTransaction),

                //Chetu 357
                Track2Data = transactionRequest.Track2Data,

                RetrievalRefNumber = transactionRequest.TransRefNo,
                AuthIdentificationResponse = FormatFields.PrepareAuthId(transactionRequest.TransactionType, transactionRequest.AuthIdentResponse),
                ResponseCode = transactionRequest.ResponseCode,
                TerminalId = transactionRequest.TerminalId.PadLeft(8, '0'),
                MerchantId = transactionRequest.FdMerchantId.PadLeft(15, '0'),
                AdditionalResponseData = transactionRequest.AddRespdata,
                TransactionCurrencyCode = SigmaStaticValue.CurrencyCode,
                EncryptedPinData = FormatFields.FormatEncryptedPinData(Convert.ToString(transactionRequest.PaymentType), Convert.ToString(transactionRequest.CardType), transactionRequest.DebitPin),
                MerchantZipCode = FormatFields.FormatMerchantZipCode(transactionRequest.MerchantZipCode, Convert.ToString(transactionRequest.CardType)),
                AdditionalPosData = SigmaStaticValue.AdditionalPossDataForRetail,
                Table14Response = transactionRequest.RespTable14,
                Table49Response = transactionRequest.RespTable49,
                TableVIResponse = transactionRequest.RespTableVI,
                TableSPResponse = transactionRequest.RespTableSP,
                TableDSResponse = transactionRequest.RespTableDS,
                TableMCResponse = transactionRequest.RespTableMC,
                IsTORTransaction = transactionRequest.IsTORTransaction,
                ReversalBit63 = transactionRequest.ResponseBit63,
                TokenType = transactionRequest.TokenType,
                Token = transactionRequest.RCCIToken,
                SourceTransaction = transactionRequest.SourceTransaction,
                Track3Data = transactionRequest.Track3Data,
                KeyId = transactionRequest.KeyId,
                KeySerialNumber = transactionRequest.KeySerialNumber
            };
        }

        /// <summary>
        /// Purpose             :   Pass transaction data in CnTransactionEnities class 
        /// Function Name       :   MakeEcommTransRequest
        /// Created By          :   Naveen Kumar
        /// Created On          :   03/25/2016
        /// Modification Made   :   ****************************
        /// Modified On       :    "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        /// 
        public static CnTransactionEntities MakeEcommTransRequest(TransactionRequest transactionRequest)
        {
            ILogger logger = new Logger();

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                logger.LogInfoMessage(string.Format("CnTransactionMessage.MakeEcommTransRequest() paymentType:{0};cardType:{1};industryType:{2};txnType:{3};", transactionRequest.PaymentType, transactionRequest.CardType, transactionRequest.IndustryType, transactionRequest.TransactionType));

            return new CnTransactionEntities
            {
                CardType = (CardType)Enum.Parse(typeof(CardType), transactionRequest.PaymentType.ToString()),
                TransType = transactionRequest.TransactionType,
                CardIssuer = (CardIssuer)Enum.Parse(typeof(CardIssuer), transactionRequest.CardType.ToString()),
                IndustryType = (IndustryType)Enum.Parse(typeof(IndustryType), transactionRequest.IndustryType),
                PrimaryAccountNumber = FormatFields.PrepareAccountNumber(transactionRequest.TransactionType, transactionRequest.CardNumber, transactionRequest.PortalEmvRefundVoidReq),
                ProcessingCode = FormatFields.PrepareProcessingCode(transactionRequest.PaymentType.ToString(), transactionRequest.TransactionType.ToString(), transactionRequest.EntryMode.ToString()), //mandatory
                AmountOfTransaction = transactionRequest.TransactionAmount,
                TransmissionDateTime = FormatFields.FormatTransDateTime(transactionRequest.TrnmsnDateTime),
                SystemTrace = transactionRequest.Stan,
                TimeLocalTransmission = FormatFields.FormatLocalTime(transactionRequest.LocalDateTime),
                DateLocalTrans = FormatFields.FormatLocalDate(transactionRequest.LocalDateTime),
                CardExpirationDate = FormatFields.FormatExpiryDate(transactionRequest.ExpiryDate, transactionRequest.TransactionType, transactionRequest.IndustryType),
                MerchantCategoryCode = SigmaStaticValue.MCCForECom,
                PosEntryModePinCapability = SigmaStaticValue.POSEntryModePinCapabilityForManual,
                NetworkInternationId = SigmaStaticValue.NetworkInternationId,
                PosConditionCode = FormatFields.PreparePosConditionCode(transactionRequest.PaymentType.ToString(), transactionRequest.CardType.ToString(), transactionRequest.PinData),
                AcquirerReference = FormatFields.PrepareAcquirerRef(transactionRequest.TransactionType, transactionRequest.SourceTransaction),
                RetrievalRefNumber = transactionRequest.TransRefNo,
                TerminalId = transactionRequest.TerminalId.PadLeft(8, '0'),
                MerchantId = transactionRequest.FdMerchantId.PadLeft(15, '0'),
                TransactionCurrencyCode = SigmaStaticValue.CurrencyCode,
                ReversalBit63 = transactionRequest.ResponseBit63,
                MaxAmount = transactionRequest.MAXAmount,
                EncryptedPinData = transactionRequest.PinData,
                
                MerchantZipCode = FormatFields.FormatMerchantZipCode(transactionRequest.MerchantZipCode, Convert.ToString(transactionRequest.CardType)),
                
                IsPinLessDebit = FormatFields.IsPinLessDebit(transactionRequest.PinData, transactionRequest.PaymentType.ToString()),
                AdditionalPosData = SigmaStaticValue.AdditionalPossData,
                IsReversalTransaction = transactionRequest.IsReversal,
                AuthIdentificationResponse = FormatFields.PrepareAuthId(transactionRequest.TransactionType, transactionRequest.AuthIdentResponse),
                CVVCode = FormatFields.FormatCvvCode(transactionRequest.CardCvv),

                Table14Response = transactionRequest.RespTable14,
                Table49Response = transactionRequest.RespTable49,
                TableVIResponse = transactionRequest.RespTableVI,
                TableSPResponse = transactionRequest.RespTableSP,
                TableDSResponse = transactionRequest.RespTableDS,
                TableMCResponse = transactionRequest.RespTableMC,
                ResponseCode = transactionRequest.ResponseCode,

                FDPrivateUsageData48 = FormatFields.PrepareAVS(transactionRequest.TransactionType, transactionRequest.ZipCode, transactionRequest.StreetAddress),
                
                AdditionalResponseData = transactionRequest.AddRespdata,
                IsTORTransaction = transactionRequest.IsTORTransaction,
                TokenType = transactionRequest.TokenType,
                Token = transactionRequest.RCCIToken,
                SourceTransaction = transactionRequest.SourceTransaction
            };
        }

        /// <summary>
        /// Fill merchant details 
        /// </summary>
        /// <param name="request"></param>
        /// <returns>MerchantInfo object</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static MerchantInfo FillMerchantInfo(TransactionRequest request)
        {
            return new MerchantInfo
            {
                MerchantID = request.FdMerchantId,
                TerminalID = request.TerminalId,
                DatawireID = request.Did,
                FirstDataID = request.TppId,
                MerchantZipCode = request.ZipCode,
                MerchantCategoryCode = request.MerchantCategoryCode
            };
        }

        /// <summary>
        /// Create the complete transaction request in Xml.
        /// </summary>
        /// <param name="objMerchantData"></param>
        /// <param name="payload"></param>
        /// <param name="systemTraceNumber"></param>
        /// <returns>string RequestXml</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        public static string SetTransactionXml(TransactionRequest trans, string payload, string clientReference)
        {
            try
            {
                ReqClientIDType objRequestClientID = new ReqClientIDType();
                objRequestClientID.DID = trans.Did;
                objRequestClientID.App = Global.AppNameNVersion;
                objRequestClientID.Auth = trans.FdMerchantId + SigmaStaticValue.Pipe + trans.TerminalId;
                objRequestClientID.ClientRef = clientReference;

                CnTransactionRequest objTransactionRequest = new CnTransactionRequest();
                objTransactionRequest.ServiceID = Global.ServiceID;
                objTransactionRequest.Payload = payload;

                TransactionClientType transaction = new TransactionClientType();
                transaction.ServiceID = Global.ServiceID;
                transaction.Payload = payload;

                Request objRequest = new Request();
                objRequest.ReqClientID = objRequestClientID;
                objRequest.Transaction = transaction;

                //Create complete transaction request in XML by serializing the object.
                string requestXml = Serialization.Serialize<Request>(objRequest);
                return requestXml;
            }
            catch (Exception ex)
            {
                ILogger logger = new Logger();
                logger.LogInfoFatal("CnTransactionMessage.SetTransactionXml();", ex);

                throw;
            }
        }

        /// <summary>
        /// This method sets additional data for bit 63 private data fields for Ecomm.
        /// </summary>
        /// <param name="objMessageType"></param>
        /// <param name="objElements"></param>
        /// <param name="objTransactionEntities"></param>
        /// <returns>Dictionary<Element, string></returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        private Dictionary<Element, string> SetEcommAdditionalData(MessageType objMessageType, Dictionary<Element, string> objElements, CnTransactionEntities objTransactionEntities)
        {
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalData() objMessageType:{0};CardType:{1};IsTA:{2};", objMessageType, objTransactionEntities.CardType, objTransactionEntities.IsTransArmor));

            StringBuilder resultValue = new StringBuilder();
            try
            {
                // Process card type Credit and Debit
                switch (objTransactionEntities.CardType)
                {
                    // Credit Card
                    case CardType.Credit:
                        switch (objMessageType)
                        {
                            // Credit Sale
                            case MessageType.CreditSale:
                            case MessageType.CreditAuthorization:
                            case MessageType.CreditCompletion:
                                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalData()-1 objMessageType:{0};CardType:{1};", objMessageType, objTransactionEntities.CardType));

                                SetEcommAdditionalDataForCreditSale(resultValue, objTransactionEntities);
                                break;

                            case MessageType.CreditRefund:
                                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalData()-2 objMessageType:{0};CardType:{1};", objMessageType, objTransactionEntities.CardType));

                                SetEcommAdditionalDataForCreditRefund(resultValue, objTransactionEntities);
                                break;

                            // Credit Full and Partial Reversal
                            case MessageType.CreditReversal:
                            case MessageType.CreditPartialReversal:
                            case MessageType.CreditVoid:
                                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalData()-3 objMessageType:{0};CardType:{1};", objMessageType, objTransactionEntities.CardType));

                                SetEcommAdditionalDataForCreditReversalVoid(resultValue, objTransactionEntities);
                                break;

                            default:
                                break;
                        }

                        break;

                    // Debit Card
                    case CardType.Debit:
                        switch (objMessageType)
                        {
                            // Debit Sale
                            case MessageType.DebitSale:
                            case MessageType.DebitAuthorization:
                            case MessageType.DebitCompletion:
                            case MessageType.DebitRefund:
                                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalData()-4 objMessageType:{0};CardType:{1};", objMessageType, objTransactionEntities.CardType));

                                SetEcommAdditionalDataForDebitSale(resultValue, objTransactionEntities);
                                break;

                            default:
                                break;
                        }
                        break;

                    default:
                        if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                            _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalData()-5 objMessageType:{0};CardType:{1};", objMessageType, objTransactionEntities.CardType));
                        break;
                }

                if (!string.IsNullOrEmpty(resultValue.ToString()))
                {
                    var bit63length = SetAdditionalTableLength(resultValue.ToString());

                    //Set the length of bitmap 63 and append it with bitmap 63
                    resultValue = resultValue.Insert(0, bit63length);

                    //Add the bit 63 and realated tables
                    objElements.Add(CnBitmapDefinition.FDPrivateUsageData63, resultValue.ToString());

                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalData() FDPrivateUsageData63:{0};", resultValue.ToString()));

                    //Set Bit63 data in objTransactionEntities for saving it in Database.
                    objTransactionEntities.FDPrivateUsageData63 = resultValue.ToString();
                }
                else
                {
                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalData() ResultValue = <<NULL>>;"));
                }

                return objElements;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal("CnTransactionMessage.SetEcommAdditionalData();", ex);

                throw;
            }
        }

        /// <summary>
        /// This method sets additional data for bit 63 private data fields for MSR.
        /// </summary>
        /// <param name="objMessageType"></param>
        /// <param name="objElements"></param>
        /// <param name="objTransactionEntities"></param>
        /// <returns>Dictionary<Element, string></returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        private Dictionary<Element, string> SetRetailAdditionalData(MessageType objMessageType, Dictionary<Element, string> objElements, CnTransactionEntities objTransactionEntities)
        {
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetRetailAdditionalData() objMessageType:{0};CardType:{1};", objMessageType, objTransactionEntities.CardType));

            StringBuilder resultValue = new StringBuilder();
            try
            {
                // Process card type Credit and Debit
                switch (objTransactionEntities.CardType)
                {
                    case CardType.Credit:
                        switch (objMessageType)
                        {
                            case MessageType.CreditSale:
                            case MessageType.CreditAuthorization:
                            case MessageType.CreditCompletion:
                                if (objTransactionEntities.EntryMode == EntryMode.MSRContactless.ToString())
                                    SetMSRContactlessAdditionalDataForCreditSaleAuthCompletion(resultValue, objTransactionEntities);
                                else
                                    SetRetailAdditionalDataForCreditSaleAuthCompletion(resultValue, objTransactionEntities);
                                break;

                            default:
                                break;
                        }
                        break;

                    case CardType.Debit:
                        switch (objMessageType)
                        {
                            case MessageType.DebitSale:
                            case MessageType.DebitAuthorization:
                            case MessageType.DebitCompletion:
                                SetRetailAdditionalDataForDebitSaleAuthCompletion(resultValue, objTransactionEntities);
                                break;

                            default: break;
                        }
                        break;

                    default:
                        break;
                }

                if (!string.IsNullOrEmpty(resultValue.ToString()))
                {

                    var bit63length = SetAdditionalTableLength(resultValue.ToString());
                    //Set the length of bitmap 63 and append it with bitmap 63
                    resultValue = resultValue.Insert(0, bit63length);

                    //Add the bit 63 and realated tables
                    objElements.Add(CnBitmapDefinition.FDPrivateUsageData63, resultValue.ToString());

                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetRetailAdditionalData() FDPrivateUsageData63:{0};", resultValue.ToString()));

                    //Set Bit63 data in objTransactionEntities for saving it in Database.
                    objTransactionEntities.FDPrivateUsageData63 = resultValue.ToString();
                }
                else
                {
                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetRetailAdditionalData() ResultValue = <<NULL>>;"));
                }

                return objElements;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal("CnTransactionMessage.SetRetailAdditionalData();", ex);

                throw;
            }
        }

        /// <summary>
        /// This method sets additional data for credit sale for Ecomm.
        /// </summary>
        /// <param name="resultValue"></param>
        /// <param name="objTransactionEntities"></param>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        private void SetEcommAdditionalDataForCreditSale(StringBuilder resultValue, CnTransactionEntities objTransactionEntities)
        {
            try
            {
                string authorizedAmount = FormatFields.FormatAmount(Convert.ToDouble(objTransactionEntities.AmountOfTransaction));
                decimal transactionAmount = Convert.ToDecimal(objTransactionEntities.AmountOfTransaction);

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditSale() authAmt:{0};transAmt:{1};CardIssuer:{2};", authorizedAmount, transactionAmount, objTransactionEntities.CardIssuer));

                switch (objTransactionEntities.CardIssuer)
                {
                    // Set additional data for MasterCard Issuer
                    case CardIssuer.MasterCard:
                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            var strBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                            strBuilder.Remove(56, 12);
                            strBuilder.Insert(56, authorizedAmount);
                            objTransactionEntities.Table14Response = strBuilder.ToString();
                            resultValue.Append(objTransactionEntities.Table14Response);
                        }
                        else
                        {
                            var objMCTable14 = new Table14AdditionalMasterData();
                            string valueMCTable14 = SetTableProperties(objMCTable14);
                            resultValue.Append(valueMCTable14);
                        }

                        // Table49
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                resultValue.Append(objTransactionEntities.Table49Response);
                            }
                            else
                            {
                                if (objTransactionEntities.PosEntryModePinCapability.StartsWith(PaymentAPIResources.ZeroOne))
                                {
                                    var objMCTable49 = new Table49();
                                    if (!string.IsNullOrEmpty(objTransactionEntities.CVVCode))
                                    {
                                        var cvv = objTransactionEntities.CVVCode.Trim().PadLeft(4, Convert.ToChar(SigmaStaticValue.Hash));
                                        cvv = cvv.Replace(SigmaStaticValue.Hash, SigmaStaticValue.PI20);
                                        objMCTable49.CardCodeValue = cvv;
                                    }

                                    string valueMCTable49 = SetTableProperties(objMCTable49);
                                    resultValue.Append(valueMCTable49);
                                }
                            }
                        }

                        // Table60
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            var objMCTable60 = new Table60();
                            string valueMCTable60 = SetTableProperties(objMCTable60);
                            resultValue.Append(valueMCTable60);
                        }

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objMCTable69 = new Table69();
                            string valueMCTable69 = SetTableProperties(objMCTable69);
                            resultValue.Append(valueMCTable69);
                        }

                        // TableMC
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                resultValue.Append(objTransactionEntities.TableMCResponse);
                            }
                            else
                            {
                                var objTablesMC = new TableMC();
                                string valueTableMC = SetTableProperties(objTablesMC);
                                resultValue.Append(valueTableMC);
                            }
                        }

                        // Table36   
                        if (objTransactionEntities.TransType != TransactionType.Authorization.ToString() && !objTransactionEntities.IsTORTransaction)
                        {
                            var objMCTables36 = new Table36();
                            objMCTables36.OrderNo = objTransactionEntities.RetrievalRefNumber + PaymentAPIResources.ThreeSpaces;
                            objMCTables36.OrderNo = CnGenericHandler.ConvertToHex(objMCTables36.OrderNo);
                            objMCTables36.OrderNo = CnTransactionMessage.ApplyDatawireFormat(objMCTables36.OrderNo);
                            string valueMCTables36 = SetTableProperties(objMCTables36);
                            resultValue.Append(valueMCTables36);
                        }

                        //Chetu 357
                        // Transarmor
                        if (objTransactionEntities.IsTransArmor)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                tableToFill.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));

                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }

                                tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }

                            // Prepare TableSP for booker payment using token
                            else if ((objTransactionEntities.IndustryType == (IndustryType)Enum.Parse(typeof(IndustryType), SigmaStaticValue.BookerAppPayment)) && !string.IsNullOrEmpty(objTransactionEntities.Token))
                            {
                                var tableSpBookerApp = new TableSP(objTransactionEntities.TokenType);
                                var tableBookerAppType = tableSpBookerApp.GetType();
                                var objPropertiesApp = tableBookerAppType.GetProperties();
                                string tableValuesApp = string.Empty;
                                foreach (var item in objPropertiesApp)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableSpBookerApp));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValuesApp += propertyValue;
                                }

                                tableValuesApp = tableValuesApp + PaymentAPIResources.TableSP07016 + objTransactionEntities.Token;
                                tableValuesApp = SetAdditionalTableLength(tableValuesApp) + tableValuesApp;
                                resultValue.Append(tableValuesApp);
                            }
                            else
                            {
                                var objMCTableSp = new TableSP(objTransactionEntities.TokenType);
                                objMCTableSp.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));

                                string valueMCTableSp = SetTableProperties(objMCTableSp);
                                resultValue.Append(valueMCTableSp);
                            }
                        }
                        break;

                    // Set additional data for Visa Issuer
                    case CardIssuer.Visa:
                        if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                            _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditSale()-Visa;TransType:{0};", objTransactionEntities.TransType));

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditSale()-Visa Table14-Completion;"));

                            var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                            aStringBuilder.Remove(32, 12);
                            aStringBuilder.Insert(32, authorizedAmount);
                            aStringBuilder.Remove(44, 12);
                            aStringBuilder.Insert(44, authorizedAmount);
                            objTransactionEntities.Table14Response = aStringBuilder.ToString();
                            resultValue.Append(objTransactionEntities.Table14Response);
                        }
                        else
                        {
                            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditSale()-Visa Table14-NonCompletion;"));

                            var objVisaTable14 = new Table14AdditionalVisaData();
                            string valueVisaTable14 = SetTableProperties(objVisaTable14);
                            resultValue.Append(valueVisaTable14);
                        }

                        // Table49
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditSale()-Visa Table49 IsTOR=False;"));

                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditSale()-Visa Table49-Completion;"));

                                resultValue.Append(objTransactionEntities.Table49Response);
                            }
                            else
                            {
                                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditSale()-Visa Table14-NonCompletion;"));

                                if (objTransactionEntities.PosEntryModePinCapability.StartsWith(PaymentAPIResources.ZeroOne))
                                {
                                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                        _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditSale()-Visa Table14-Completion. StartsWithZeroOne;"));

                                    var objVITable49 = new Table49();
                                    if (!string.IsNullOrEmpty(objTransactionEntities.CVVCode))
                                    {
                                        var cvv = objTransactionEntities.CVVCode.Trim().PadLeft(4, Convert.ToChar(SigmaStaticValue.Hash));
                                        cvv = cvv.Replace(SigmaStaticValue.Hash, SigmaStaticValue.PI20);
                                        objVITable49.CardCodeValue = cvv;
                                    }

                                    string valueVITable49 = SetTableProperties(objVITable49);
                                    resultValue.Append(valueVITable49);
                                }
                            }
                        }

                        // Table60
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditSale()-Visa Table60;"));

                            var objVisaTable60 = new Table60();
                            string valueVisaTable60 = SetTableProperties(objVisaTable60);
                            resultValue.Append(valueVisaTable60);
                        }

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditSale()-Visa Table69;"));

                            var objVisaTable69 = new Table69();
                            string valueVisaTable69 = SetTableProperties(objVisaTable69);
                            resultValue.Append(valueVisaTable69);
                        }

                        // TableVI
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditSale()-Visa TableVI;"));

                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                resultValue.Append(objTransactionEntities.TableVIResponse);
                            }
                            else
                            {
                                var objVisaTableVI = new TableVI();
                                string valueVisaTableVI = SetTableProperties(objVisaTableVI);
                                resultValue.Append(valueVisaTableVI);
                            }
                        }

                        // Table36   
                        if (objTransactionEntities.TransType != TransactionType.Authorization.ToString() && !objTransactionEntities.IsTORTransaction)
                        {
                            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditSale()-Visa Table36;"));

                            var objVITables36 = new Table36();
                            objVITables36.OrderNo = objTransactionEntities.RetrievalRefNumber + PaymentAPIResources.ThreeSpaces;
                            objVITables36.OrderNo = CnGenericHandler.ConvertToHex(objVITables36.OrderNo);
                            objVITables36.OrderNo = CnTransactionMessage.ApplyDatawireFormat(objVITables36.OrderNo);
                            objVITables36.EcommUrl = PaymentAPIResources.VIEcommUrl;
                            string valueVITable36 = SetTableProperties(objVITables36);
                            resultValue.Append(valueVITable36);
                        }

                        // Transarmor
                        if (objTransactionEntities.IsTransArmor)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditSale()-Visa Completion;"));

                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                tableToFill.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));

                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }

                                tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            // Prepare TableSP for booker payment using token
                            else if ((objTransactionEntities.IndustryType == (IndustryType)Enum.Parse(typeof(IndustryType), SigmaStaticValue.BookerAppPayment)) && !string.IsNullOrEmpty(objTransactionEntities.Token))
                            {
                                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditSale()-Visa IndustryType={0};Token:{1};TokenType:{2};", objTransactionEntities.IndustryType, objTransactionEntities.Token, objTransactionEntities.TokenType));

                                var tableSpVisaBookerApp = new TableSP(objTransactionEntities.TokenType);
                                var tableBookeVisarAppType = tableSpVisaBookerApp.GetType();
                                var objPropertiesVisaApp = tableBookeVisarAppType.GetProperties();
                                string tableValuesVisaApp = string.Empty;

                                foreach (var item in objPropertiesVisaApp)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableSpVisaBookerApp));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValuesVisaApp += propertyValue;
                                }

                                tableValuesVisaApp = tableValuesVisaApp + PaymentAPIResources.TableSP07016 + objTransactionEntities.Token;
                                tableValuesVisaApp = SetAdditionalTableLength(tableValuesVisaApp) + tableValuesVisaApp;
                                resultValue.Append(tableValuesVisaApp);
                            }
                            else
                            {
                                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                                {
                                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditSale()-Visa IndustryType={0};Token:{1};", objTransactionEntities.IndustryType, objTransactionEntities.Token));

                                    if (objTransactionEntities.TokenType == null)
                                        _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditSale()-Visa IndustryType={0};Token:{1};TokenType=NULL;", objTransactionEntities.IndustryType, objTransactionEntities.Token));
                                    else
                                        _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditSale()-Visa IndustryType={0};Token:{1};TokenType:{2};", objTransactionEntities.IndustryType, objTransactionEntities.Token, objTransactionEntities.TokenType));
                                }

                                var objVITableSp = new TableSP(objTransactionEntities.TokenType);
                                objVITableSp.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));

                                string valueVITableSp = SetTableProperties(objVITableSp);
                                resultValue.Append(valueVITableSp);
                            }
                        }
                        break;

                    // Set additional data for American Express Issuer
                    case CardIssuer.Amex:
                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            resultValue.Append(objTransactionEntities.Table14Response);
                        }
                        else
                        {
                            var objAmexTable14 = new Table14AMEXData();
                            string valueAmexTable14 = SetTableProperties(objAmexTable14);
                            resultValue.Append(valueAmexTable14);
                        }

                        // Table49
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                resultValue.Append(objTransactionEntities.Table49Response);
                            }
                            else
                            {
                                if (objTransactionEntities.PosEntryModePinCapability.StartsWith(PaymentAPIResources.ZeroOne))
                                {
                                    if (!transactionAmount.Equals(0))
                                    {
                                        var objAmexTable49 = new Table49();
                                        if (!string.IsNullOrEmpty(objTransactionEntities.CVVCode))
                                        {
                                            var cvv = objTransactionEntities.CVVCode.Trim().PadLeft(4, Convert.ToChar(SigmaStaticValue.Hash));
                                            cvv = cvv.Replace(SigmaStaticValue.Hash, SigmaStaticValue.PI20);
                                            objAmexTable49.CardCodeValue = cvv;
                                        }
                                        string valueAmexTable49 = SetTableProperties(objAmexTable49);
                                        resultValue.Append(valueAmexTable49);
                                    }
                                }
                            }
                        }

                        // Table60
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            var objAmexTable60 = new Table60();
                            string valueAmexTable60 = SetTableProperties(objAmexTable60);
                            resultValue.Append(valueAmexTable60);
                        }

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objAmexTable69 = new Table69();
                            string valueAmexTable69 = SetTableProperties(objAmexTable69);
                            resultValue.Append(valueAmexTable69);
                        }

                        // Table36     
                        if (objTransactionEntities.TransType != TransactionType.Authorization.ToString() && !objTransactionEntities.IsTORTransaction)
                        {
                            var objAmexTables36 = new Table36();
                            objAmexTables36.OrderNo = objTransactionEntities.RetrievalRefNumber + PaymentAPIResources.ThreeSpaces;
                            objAmexTables36.OrderNo = CnGenericHandler.ConvertToHex(objAmexTables36.OrderNo);
                            objAmexTables36.OrderNo = CnTransactionMessage.ApplyDatawireFormat(objAmexTables36.OrderNo);
                            string valueAmexTables36 = SetTableProperties(objAmexTables36);
                            resultValue.Append(valueAmexTables36);
                        }

                        // Transarmor
                        if (objTransactionEntities.IsTransArmor)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                tableToFill.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));

                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }

                                tableValues = tableValues + PaymentAPIResources.TableSP07015 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            // Prepare TableSP for booker payment using token
                            else if ((objTransactionEntities.IndustryType == (IndustryType)Enum.Parse(typeof(IndustryType), SigmaStaticValue.BookerAppPayment)) && !string.IsNullOrEmpty(objTransactionEntities.Token))
                            {
                                var tableSpAmexBookerApp = new TableSP(objTransactionEntities.TokenType);
                                var tableBookeAmexAppType = tableSpAmexBookerApp.GetType();
                                var objPropertiesAmexApp = tableBookeAmexAppType.GetProperties();
                                string tableValuesAmexApp = string.Empty;
                                foreach (var item in objPropertiesAmexApp)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableSpAmexBookerApp));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValuesAmexApp += propertyValue;
                                }

                                tableValuesAmexApp = tableValuesAmexApp + PaymentAPIResources.TableSP07015 + objTransactionEntities.Token;
                                tableValuesAmexApp = SetAdditionalTableLength(tableValuesAmexApp) + tableValuesAmexApp;
                                resultValue.Append(tableValuesAmexApp);
                            }
                            else
                            {
                                var objAmexTableSp = new TableSP(objTransactionEntities.TokenType);
                                objAmexTableSp.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));

                                string valueAmexTableSp = SetTableProperties(objAmexTableSp);
                                resultValue.Append(valueAmexTableSp);
                            }
                        }
                        break;

                    // Set additional data for Discover,diners and JCB
                    case CardIssuer.Discover:
                    case CardIssuer.Diners:
                    case CardIssuer.JCB:
                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            if (objTransactionEntities.CardIssuer.ToString()==CardTypeType.Discover.ToString() || objTransactionEntities.CardIssuer.ToString()==CardTypeType.Diners.ToString())
                            {
                                var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                                aStringBuilder.Remove(52, 12);
                                aStringBuilder.Insert(52, authorizedAmount);
                                objTransactionEntities.Table14Response = aStringBuilder.ToString();
                                resultValue.Append(objTransactionEntities.Table14Response);
                            }
                            else
                            {
                                var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                                aStringBuilder.Remove(82, 12);
                                aStringBuilder.Insert(82, authorizedAmount);
                                objTransactionEntities.Table14Response = aStringBuilder.ToString();
                                resultValue.Append(objTransactionEntities.Table14Response);
                            }
                        }
                        else
                        {
                            var objDiscoverTable14 = new Table14AdditionalDiscoverData();
                            string valueDiscoverTable14 = SetTableProperties(objDiscoverTable14);
                            resultValue.Append(valueDiscoverTable14);
                        }

                        // Table49
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                resultValue.Append(objTransactionEntities.Table49Response);
                            }
                            else
                            {
                                if (objTransactionEntities.PosEntryModePinCapability.StartsWith(PaymentAPIResources.ZeroOne))
                                {
                                    var objDiscoverTable49 = new Table49();
                                    if (!string.IsNullOrEmpty(objTransactionEntities.CVVCode))
                                    {
                                        var cvv = objTransactionEntities.CVVCode.Trim().PadLeft(4, Convert.ToChar(SigmaStaticValue.Hash));
                                        cvv = cvv.Replace(SigmaStaticValue.Hash, SigmaStaticValue.PI20);
                                        objDiscoverTable49.CardCodeValue = cvv;
                                    }
                                    string valueDiscoverTable49 = SetTableProperties(objDiscoverTable49);
                                    resultValue.Append(valueDiscoverTable49);
                                }
                            }
                        }
                        
                        // Table60
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            var objDiscoverTable60 = new Table60();
                            string valueDiscoverTable60 = SetTableProperties(objDiscoverTable60);
                            resultValue.Append(valueDiscoverTable60);
                        }

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objDiscoverTable69 = new Table69();
                            string valueDiscoverTable69 = SetTableProperties(objDiscoverTable69);
                            resultValue.Append(valueDiscoverTable69);
                        }

                        // TableDS
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString() && objTransactionEntities.CardIssuer.ToString() != CardTypeType.JCB.ToString())
                            {
                                resultValue.Append(objTransactionEntities.TableDSResponse);
                            }
                            else if (objTransactionEntities.TransType != TransactionType.Completion.ToString() && objTransactionEntities.CardIssuer.ToString() != CardTypeType.JCB.ToString())
                            {
                                var objTableDS = new TableDS();
                                string valueTableDS = SetTableProperties(objTableDS);
                                resultValue.Append(valueTableDS);
                            }
                        }

                        // Table36   
                        if (objTransactionEntities.TransType != TransactionType.Authorization.ToString() && !objTransactionEntities.IsTORTransaction)
                        {
                            var objDSTables36 = new Table36();
                            objDSTables36.OrderNo = objTransactionEntities.RetrievalRefNumber + PaymentAPIResources.ThreeSpaces;
                            objDSTables36.OrderNo = CnGenericHandler.ConvertToHex(objDSTables36.OrderNo);
                            objDSTables36.OrderNo = CnTransactionMessage.ApplyDatawireFormat(objDSTables36.OrderNo);
                            string valueDSTables36 = SetTableProperties(objDSTables36);
                            resultValue.Append(valueDSTables36);
                        }

                        // Transarmor
                        if (objTransactionEntities.IsTransArmor)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                tableToFill.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));

                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }

                                if (objTransactionEntities.CardIssuer.ToString() == CardIssuer.Diners.ToString())
                                    tableValues = tableValues + PaymentAPIResources.TableSP07014 + objTransactionEntities.TableSPResponse;
                                else
                                    tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;

                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            // Prepare TableSP for booker payment using token
                            else if ((objTransactionEntities.IndustryType == (IndustryType)Enum.Parse(typeof(IndustryType), SigmaStaticValue.BookerAppPayment)) && !string.IsNullOrEmpty(objTransactionEntities.Token))
                            {
                                var tableSpDSBookerApp = new TableSP(objTransactionEntities.TokenType);
                                var tableBookerDSAppType = tableSpDSBookerApp.GetType();
                                var objPropertiesDSApp = tableBookerDSAppType.GetProperties();
                                string tableValuesDSApp = string.Empty;
                                foreach (var item in objPropertiesDSApp)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableSpDSBookerApp));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValuesDSApp += propertyValue;
                                }

                                if (objTransactionEntities.CardIssuer.ToString() == CardIssuer.Diners.ToString())
                                    tableValuesDSApp = tableValuesDSApp + PaymentAPIResources.TableSP07014 + objTransactionEntities.Token;
                                else
                                    tableValuesDSApp = tableValuesDSApp + PaymentAPIResources.TableSP07016 + objTransactionEntities.Token;

                                tableValuesDSApp = SetAdditionalTableLength(tableValuesDSApp) + tableValuesDSApp;
                                resultValue.Append(tableValuesDSApp);
                            }
                            else
                            {
                                var objDSTableSp = new TableSP(objTransactionEntities.TokenType);
                                objDSTableSp.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType != null ? objTransactionEntities.TokenType : string.Empty));

                                string valueDSTableSp = SetTableProperties(objDSTableSp);
                                resultValue.Append(valueDSTableSp);
                            }
                        }
                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditSale() Ex:{0};", ex.ToString()), ex);

                throw;
            }
        }

        /// <summary>
        /// Purpose             :   Create Refund txn.
        /// Function Name       :   SetEcommAdditionalDataForCreditRefund
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   05/11/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   05/16/2016
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private void SetEcommAdditionalDataForCreditRefund(StringBuilder resultValue, CnTransactionEntities objTransactionEntities)
        {
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditRefund() resultValue:{0};", resultValue));

            try
            {
                var bit63ResponseDictionary = ResponseHandler.SetTablesResponse(objTransactionEntities.ReversalBit63);
                var table14ReversalValue = bit63ResponseDictionary[SigmaStaticValue.TableNo14];

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditRefund() table14ReversalValue:{0};", table14ReversalValue));

                table14ReversalValue = ApplyDatawireFormat(table14ReversalValue);

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditRefund() table14ReversalValue:{0};CardIssuer:{1};PortalEmvRefundVoidReq:{2};ReversalBit63:{3};", table14ReversalValue, objTransactionEntities.CardIssuer, objTransactionEntities.PortalEmvRefundVoidReq, objTransactionEntities.ReversalBit63));

                switch (objTransactionEntities.CardIssuer)
                {
                    // Set additional data for MasterCard Issuer
                    case CardIssuer.MasterCard:
                        // Table60
                        var objMCTable60 = new Table60();
                        string valueMCTable60 = SetTableProperties(objMCTable60);
                        resultValue.Append(valueMCTable60);
                        _motoBillECommIndicator = objMCTable60.ECommerceIndicator;

                        // SP
                        var objMCTableSP = new TableSP_Reversal();
                        objMCTableSP.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));

                        if (!objTransactionEntities.PortalEmvRefundVoidReq)
                        {
                            if (objTransactionEntities.SourceTransaction == Convert.ToString(TxnTypeType.Completion))
                            {
                                objMCTableSP.Tag07Length = PaymentAPIResources.Mc_Visa_DS_Tag07Length;
                                int prefixIndex = table14ReversalValue.IndexOf(PaymentAPIResources.TableSP07016);
                                objMCTableSP.Tag07Value = table14ReversalValue.Substring(prefixIndex + 15, 48);
                            }
                            else
                            {
                                if (table14ReversalValue.Substring(378, 6) == PaymentAPIResources.Indicator39)
                                {
                                    objMCTableSP.Tag07Length = table14ReversalValue.Substring(420, 9);
                                    objMCTableSP.Tag07Value = table14ReversalValue.Substring(429, 48);
                                }
                                else if (table14ReversalValue.Substring(378, 6) == PaymentAPIResources.Indicator38)
                                {
                                    objMCTableSP.Tag07Length = table14ReversalValue.Substring(417, 9);
                                    objMCTableSP.Tag07Value = table14ReversalValue.Substring(426, 48);
                                }
                                else if (table14ReversalValue.Substring(378, 6) == PaymentAPIResources.Indicator37)
                                {
                                    objMCTableSP.Tag07Length = table14ReversalValue.Substring(414, 9);
                                    objMCTableSP.Tag07Value = table14ReversalValue.Substring(423, 48);
                                }
                                else if (table14ReversalValue.Substring(378, 6) == PaymentAPIResources.Indicator36)
                                {
                                    objMCTableSP.Tag07Length = table14ReversalValue.Substring(411, 9);
                                    objMCTableSP.Tag07Value = table14ReversalValue.Substring(420, 48);
                                }
                                else if (table14ReversalValue.Substring(378, 6) == PaymentAPIResources.Indicator35)
                                {
                                    objMCTableSP.Tag07Length = table14ReversalValue.Substring(408, 9);
                                    objMCTableSP.Tag07Value = table14ReversalValue.Substring(417, 48);
                                }
                                else if (table14ReversalValue.Substring(378, 6) == PaymentAPIResources.Indicator31)
                                {
                                    objMCTableSP.Tag07Length = table14ReversalValue.Substring(396, 9);
                                    objMCTableSP.Tag07Value = table14ReversalValue.Substring(405, 48);
                                }
                            }

                            string valueMCTableSP = SetTableProperties(objMCTableSP);
                            resultValue.Append(valueMCTableSP);
                        }
                        else
                        {
                            //Chetu 357
                            var bit63Data = CommonFunctions.GetTableSPDetails(objTransactionEntities.ReversalBit63);
                            // Ensure we have a "07" element in the collection before trying to use it.
                            if ((bit63Data.Count > 0) && (bit63Data["07"] != null))
                            {
                                string tag07val = (bit63Data["07"]);

                                objMCTableSP.Tag07Length = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.Length.ToString().PadLeft(3, '0')));
                                objMCTableSP.Tag07Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.ToString()));

                                resultValue.Append(SetTableProperties(objMCTableSP));
                            }
                        }

                        break;

                    // Set additional data for Visa Issuer
                    case CardIssuer.Visa:

                        // Table60
                        var objVisaTable60 = new Table60();
                        string valueVisaTable60 = SetTableProperties(objVisaTable60);
                        resultValue.Append(valueVisaTable60);
                        _motoBillECommIndicator = objVisaTable60.ECommerceIndicator;

                        var objVisaTableSP = new TableSP_Reversal();
                        objVisaTableSP.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));

                        if (!objTransactionEntities.PortalEmvRefundVoidReq)
                        {
                            // SP
                            if (objTransactionEntities.SourceTransaction == Convert.ToString(TxnTypeType.Completion))
                            {
                                objVisaTableSP.Tag07Length = PaymentAPIResources.Mc_Visa_DS_Tag07Length;
                                int prefixIndex = table14ReversalValue.IndexOf(PaymentAPIResources.TableSP07016);
                                objVisaTableSP.Tag07Value = table14ReversalValue.Substring(prefixIndex + 15, 48);
                            }
                            else
                            {
                                if (table14ReversalValue.Substring(261, 6) == PaymentAPIResources.Indicator37)
                                {
                                    objVisaTableSP.Tag07Length = table14ReversalValue.Substring(297, 9);
                                    objVisaTableSP.Tag07Value = table14ReversalValue.Substring(306, 48);
                                }
                                else if (table14ReversalValue.Substring(261, 6) == PaymentAPIResources.Indicator36)
                                {
                                    objVisaTableSP.Tag07Length = table14ReversalValue.Substring(303, 9);
                                    objVisaTableSP.Tag07Value = table14ReversalValue.Substring(313, 48);
                                }
                                else if (table14ReversalValue.Substring(261, 6) == PaymentAPIResources.Indicator35)
                                {
                                    objVisaTableSP.Tag07Length = table14ReversalValue.Substring(301, 9);
                                    objVisaTableSP.Tag07Value = table14ReversalValue.Substring(309, 48);
                                }
                                else if (table14ReversalValue.Substring(261, 6) == PaymentAPIResources.Indicator39)
                                {
                                    objVisaTableSP.Tag07Length = table14ReversalValue.Substring(303, 9);
                                    objVisaTableSP.Tag07Value = table14ReversalValue.Substring(312, 48);
                                }
                                else if (table14ReversalValue.Substring(261, 6) == PaymentAPIResources.Indicator38)
                                {
                                    objVisaTableSP.Tag07Length = table14ReversalValue.Substring(300, 9);
                                    objVisaTableSP.Tag07Value = table14ReversalValue.Substring(309, 48);
                                }
                                else if (table14ReversalValue.Substring(261, 6) == PaymentAPIResources.Indicator31)
                                {
                                    objVisaTableSP.Tag07Length = table14ReversalValue.Substring(279, 9);
                                    objVisaTableSP.Tag07Value = table14ReversalValue.Substring(288, 48);
                                }
                                //Chetu 357
                                else if (table14ReversalValue.Substring(249, 6) == PaymentAPIResources.Indicator37)
                                {
                                    objVisaTableSP.Tag07Length = table14ReversalValue.Substring(285, 9);
                                    objVisaTableSP.Tag07Value = table14ReversalValue.Substring(294, 48);
                                }
                                else if (table14ReversalValue.Substring(249, 6) == PaymentAPIResources.Indicator38)
                                {
                                    objVisaTableSP.Tag07Length = table14ReversalValue.Substring(289, 9);
                                    objVisaTableSP.Tag07Value = table14ReversalValue.Substring(298, 48);
                                }
                                else if (table14ReversalValue.Substring(249, 6) == PaymentAPIResources.Indicator39)
                                {
                                    objVisaTableSP.Tag07Length = table14ReversalValue.Substring(292, 9);
                                    objVisaTableSP.Tag07Value = table14ReversalValue.Substring(300, 48);
                                }
                            }

                            string valueVisaTableSP = SetTableProperties(objVisaTableSP);
                            resultValue.Append(valueVisaTableSP);
                        }
                        else
                        {
                            var bit63Data = CommonFunctions.GetTableSPDetails(objTransactionEntities.ReversalBit63);

                            // Ensure we have a "07" element in the collection before trying to use it.
                            if ((bit63Data.Count > 0) && (bit63Data["07"] != null))
                            {
                                string tag07val = (bit63Data["07"]);

                                objVisaTableSP.Tag07Length = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.Length.ToString().PadLeft(3, '0')));
                                objVisaTableSP.Tag07Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.ToString()));

                                resultValue.Append(SetTableProperties(objVisaTableSP));
                            }
                        }

                        break;

                    // Set additional data for American Express Issuer
                    case CardIssuer.Amex:

                        // Table60
                        var objAmexTable60 = new Table60();
                        string valueAmexTable60 = SetTableProperties(objAmexTable60);
                        resultValue.Append(valueAmexTable60);
                        _motoBillECommIndicator = objAmexTable60.ECommerceIndicator;

                        // SP
                        var objAmexTableSP = new TableSP_Reversal();
                        objAmexTableSP.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));

                        if (!objTransactionEntities.PortalEmvRefundVoidReq)
                        {
                            if (objTransactionEntities.SourceTransaction == Convert.ToString(TxnTypeType.Completion))
                            {
                                objAmexTableSP.Tag07Length = PaymentAPIResources.Amex_Tag07Length;
                                int prefixIndex = table14ReversalValue.IndexOf(PaymentAPIResources.TableSP07015);
                                objAmexTableSP.Tag07Value = table14ReversalValue.Substring(prefixIndex + 15, 45);
                            }
                            else
                            {
                                if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator37)
                                {
                                    objAmexTableSP.Tag07Length = table14ReversalValue.Substring(264, 9);
                                    objAmexTableSP.Tag07Value = table14ReversalValue.Substring(273, 45);
                                }
                                else if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator36)
                                {
                                    objAmexTableSP.Tag07Length = table14ReversalValue.Substring(261, 9);
                                    objAmexTableSP.Tag07Value = table14ReversalValue.Substring(270, 45);
                                }
                                else if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator35)
                                {
                                    objAmexTableSP.Tag07Length = table14ReversalValue.Substring(258, 9);
                                    objAmexTableSP.Tag07Value = table14ReversalValue.Substring(267, 45);
                                }
                                else if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator38)
                                {
                                    objAmexTableSP.Tag07Length = table14ReversalValue.Substring(267, 9);
                                    objAmexTableSP.Tag07Value = table14ReversalValue.Substring(276, 45);
                                }
                                else if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator39)
                                {
                                    objAmexTableSP.Tag07Length = table14ReversalValue.Substring(270, 9);
                                    objAmexTableSP.Tag07Value = table14ReversalValue.Substring(279, 45);
                                }
                                else if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator30)
                                {
                                    objAmexTableSP.Tag07Length = table14ReversalValue.Substring(243, 9);
                                    objAmexTableSP.Tag07Value = table14ReversalValue.Substring(252, 45);
                                }
                            }

                            string valueAmexTableSP = SetTableProperties(objAmexTableSP);
                            resultValue.Append(valueAmexTableSP);
                        }
                        else
                        {
                            var bit63Data = CommonFunctions.GetTableSPDetails(objTransactionEntities.ReversalBit63);

                            // Ensure we have a "07" element in the collection before trying to use it.
                            if ((bit63Data.Count > 0) && (bit63Data["07"] != null))
                            {
                                string tag07val = (bit63Data["07"]);

                                objAmexTableSP.Tag07Length = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.Length.ToString().PadLeft(3, '0')));
                                objAmexTableSP.Tag07Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.ToString()));

                                resultValue.Append(SetTableProperties(objAmexTableSP));
                            }
                        }

                        break;

                    // Set additional data for Discover
                    case CardIssuer.DCI:
                    case CardIssuer.Discover:

                        // Table60
                        var objDiscoverTable60 = new Table60();
                        string valueDiscoverTable60 = SetTableProperties(objDiscoverTable60);
                        resultValue.Append(valueDiscoverTable60);
                        _motoBillECommIndicator = objDiscoverTable60.ECommerceIndicator;

                        var objDisTableSP = new TableSP_Reversal();
                        objDisTableSP.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));

                        if (!objTransactionEntities.PortalEmvRefundVoidReq)
                        {
                            // SP
                            if (objTransactionEntities.SourceTransaction == Convert.ToString(TxnTypeType.Completion))
                            {
                                objDisTableSP.Tag07Length = PaymentAPIResources.Mc_Visa_DS_Tag07Length;
                                int prefixIndex = table14ReversalValue.IndexOf(PaymentAPIResources.TableSP07016);
                                objDisTableSP.Tag07Value = table14ReversalValue.Substring(prefixIndex + 15, 48);
                            }
                            else
                            {
                                if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator39)
                                {
                                    objDisTableSP.Tag07Length = table14ReversalValue.Substring(543, 9);
                                    objDisTableSP.Tag07Value = table14ReversalValue.Substring(552, 48);
                                }
                                else if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator38)
                                {
                                    objDisTableSP.Tag07Length = table14ReversalValue.Substring(540, 9);
                                    objDisTableSP.Tag07Value = table14ReversalValue.Substring(549, 48);
                                }
                                else if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator37)
                                {
                                    objDisTableSP.Tag07Length = table14ReversalValue.Substring(537, 9);
                                    objDisTableSP.Tag07Value = table14ReversalValue.Substring(546, 48);
                                }
                                else if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator36)
                                {
                                    objDisTableSP.Tag07Length = table14ReversalValue.Substring(534, 9);
                                    objDisTableSP.Tag07Value = table14ReversalValue.Substring(543, 48);
                                }
                                else if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator35)
                                {
                                    objDisTableSP.Tag07Length = table14ReversalValue.Substring(531, 9);
                                    objDisTableSP.Tag07Value = table14ReversalValue.Substring(540, 48);
                                }
                                else if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator31)
                                {
                                    objDisTableSP.Tag07Length = table14ReversalValue.Substring(519, 9);
                                    objDisTableSP.Tag07Value = table14ReversalValue.Substring(528, 48);
                                }
                            }

                            string valuedisTableSP = SetTableProperties(objDisTableSP);
                            resultValue.Append(valuedisTableSP);
                        }
                        else
                        {
                            var bit63Data = CommonFunctions.GetTableSPDetails(objTransactionEntities.ReversalBit63);

                            // Ensure we have a "07" element in the collection before trying to use it.
                            if ((bit63Data.Count > 0) && (bit63Data["07"] != null))
                            {
                                string tag07val = (bit63Data["07"]);

                                objDisTableSP.Tag07Length = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.Length.ToString().PadLeft(3, '0')));
                                objDisTableSP.Tag07Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.ToString()));

                                resultValue.Append(SetTableProperties(objDisTableSP));
                            }
                        }

                        break;

                    // Set additional data for Diners Club International Issuer

                    case CardIssuer.Diners:

                        // Table60
                        var objDITable60 = new Table60();
                        string valueDITable60 = SetTableProperties(objDITable60);
                        resultValue.Append(valueDITable60);
                        _motoBillECommIndicator = objDITable60.ECommerceIndicator;

                        // SP
                        var objDITableSP = new TableSP_Reversal();
                        objDITableSP.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));

                        if (!objTransactionEntities.PortalEmvRefundVoidReq)
                        {
                            if (objTransactionEntities.SourceTransaction == Convert.ToString(TxnTypeType.Completion))
                            {
                                objDITableSP.Tag07Length = PaymentAPIResources.Diners_Tag07Length;
                                int prefixIndex = table14ReversalValue.IndexOf(PaymentAPIResources.TableSP07014);
                                objDITableSP.Tag07Value = table14ReversalValue.Substring(prefixIndex + 15, 42);
                            }
                            else
                            {
                                if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator39)
                                {
                                    objDITableSP.Tag07Length = table14ReversalValue.Substring(549, 9);
                                    objDITableSP.Tag07Value = table14ReversalValue.Substring(558, 48);
                                }
                                else if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator38)
                                {
                                    objDITableSP.Tag07Length = table14ReversalValue.Substring(546, 9);
                                    objDITableSP.Tag07Value = table14ReversalValue.Substring(555, 48);
                                }
                                else if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator37)
                                {
                                    objDITableSP.Tag07Length = table14ReversalValue.Substring(543, 9);
                                    objDITableSP.Tag07Value = table14ReversalValue.Substring(552, 42);
                                }
                                else if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator36)
                                {
                                    objDITableSP.Tag07Length = table14ReversalValue.Substring(540, 9);
                                    objDITableSP.Tag07Value = table14ReversalValue.Substring(549, 42);
                                }
                                else if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator35)
                                {
                                    objDITableSP.Tag07Length = table14ReversalValue.Substring(537, 9);
                                    objDITableSP.Tag07Value = table14ReversalValue.Substring(546, 42);
                                }
                            }

                            string valueDITableSP = SetTableProperties(objDITableSP);
                            resultValue.Append(valueDITableSP);
                        }
                        else
                        {
                            var bit63Data = CommonFunctions.GetTableSPDetails(objTransactionEntities.ReversalBit63);

                            // Ensure we have a "07" element in the collection before trying to use it.
                            if ((bit63Data.Count > 0) && (bit63Data["07"] != null))
                            {
                                string tag07val = (bit63Data["07"]);

                                objDITableSP.Tag07Length = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.Length.ToString().PadLeft(3, '0')));
                                objDITableSP.Tag07Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.ToString()));

                                resultValue.Append(SetTableProperties(objDITableSP));
                            }
                        }

                        break;

                    // Set additional data for JCB
                    case CardIssuer.JCB:

                        // Table60
                        var objJCBTable60 = new Table60();
                        string valueJCBTable60 = SetTableProperties(objJCBTable60);
                        resultValue.Append(valueJCBTable60);
                        _motoBillECommIndicator = objJCBTable60.ECommerceIndicator;

                        // SP
                        var objJCBTableSP = new TableSP_Reversal();
                        objJCBTableSP.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));

                        if (!objTransactionEntities.PortalEmvRefundVoidReq)
                        {
                            if (objTransactionEntities.SourceTransaction == Convert.ToString(TxnTypeType.Completion))
                            {
                                objJCBTableSP.Tag07Length = PaymentAPIResources.Mc_Visa_DS_Tag07Length;
                                int prefixIndex = table14ReversalValue.IndexOf(PaymentAPIResources.TableSP07016);
                                objJCBTableSP.Tag07Value = table14ReversalValue.Substring(prefixIndex + 15, 48);
                            }
                            else
                            {
                                if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator39)
                                {
                                    objJCBTableSP.Tag07Length = table14ReversalValue.Substring(267, 9);
                                    objJCBTableSP.Tag07Value = table14ReversalValue.Substring(276, 48);
                                }
                                else if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator38)
                                {
                                    objJCBTableSP.Tag07Length = table14ReversalValue.Substring(264, 9);
                                    objJCBTableSP.Tag07Value = table14ReversalValue.Substring(273, 48);
                                }
                                else if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator37)
                                {
                                    objJCBTableSP.Tag07Length = table14ReversalValue.Substring(261, 9);
                                    objJCBTableSP.Tag07Value = table14ReversalValue.Substring(270, 48);
                                }
                                else if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator36)
                                {
                                    objJCBTableSP.Tag07Length = table14ReversalValue.Substring(258, 9);
                                    objJCBTableSP.Tag07Value = table14ReversalValue.Substring(267, 48);
                                }
                                else if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator35)
                                {
                                    objJCBTableSP.Tag07Length = table14ReversalValue.Substring(255, 9);
                                    objJCBTableSP.Tag07Value = table14ReversalValue.Substring(264, 48);
                                }
                            }

                            string valueJCBTableSP = SetTableProperties(objJCBTableSP);
                            resultValue.Append(valueJCBTableSP);
                        }
                        else
                        {
                            var bit63Data = CommonFunctions.GetTableSPDetails(objTransactionEntities.ReversalBit63);

                            // Ensure we have a "07" element in the collection before trying to use it.
                            if ((bit63Data.Count > 0) && (bit63Data["07"] != null))
                            {
                                string tag07val = (bit63Data["07"]);

                                objJCBTableSP.Tag07Length = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.Length.ToString().PadLeft(3, '0')));
                                objJCBTableSP.Tag07Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.ToString()));

                                resultValue.Append(SetTableProperties(objJCBTableSP));
                            }
                        }

                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal("CnTransactionMessage.SetEcommAdditionalDataForCreditRefund();", ex);

                throw;
            }
        }

        /// <summary>
        /// Purpose             :   Create reversal txn.
        /// Function Name       :   SetEcommAdditionalDataForCreditReversalVoid
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   04/28/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   05/16/2016 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private void SetEcommAdditionalDataForCreditReversalVoid(StringBuilder resultValue, CnTransactionEntities objTransactionEntities)
        {
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditReversalVoid() resultValue:{0};", resultValue));

            double num = Convert.ToDouble(objTransactionEntities.MaxAmount);
            string amt = num.ToString("0.00").Replace(".", string.Empty);

            var TxnAmt = Convert.ToString(amt);
            TxnAmt = TxnAmt.PadLeft(12, '0');

            try
            {
                var bit63ResponseDictionary = ResponseHandler.SetTablesResponse(objTransactionEntities.ReversalBit63);
                var table14ReversalValue = bit63ResponseDictionary[SigmaStaticValue.TableNo14];

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditReversalVoid() table14ReversalValue:{0};", table14ReversalValue));

                table14ReversalValue = ApplyDatawireFormat(table14ReversalValue);

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForCreditReversalVoid() table14ReversalValue:{0};CardIssuer:{1};PortalEmvRefundVoidReq:{2};ReversalBit63:{3};", table14ReversalValue, objTransactionEntities.CardIssuer, objTransactionEntities.PortalEmvRefundVoidReq, objTransactionEntities.ReversalBit63));

                switch (objTransactionEntities.CardIssuer)
                {
                    // Set additional data for MasterCard Issuer
                    case CardIssuer.MasterCard:
                        // Table14
                        var objMCTable14 = new Table14AdditionalMasterData();
                        objMCTable14.ACI = table14ReversalValue.Substring(12, 3);
                        objMCTable14.BankNetDate = table14ReversalValue.Substring(15, 12);
                        objMCTable14.BankNetReference = table14ReversalValue.Substring(27, 27);
                        objMCTable14.TotalAuthAmount = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(TxnAmt));
                        string valueMCTable14 = SetTableProperties(objMCTable14);
                        resultValue.Append(valueMCTable14);
                        _marketSpecificDataIndicator = objMCTable14.MSDI;

                        // Table60
                        var objMCTable60 = new Table60();
                        string valueMCTable60 = SetTableProperties(objMCTable60);
                        resultValue.Append(valueMCTable60);
                        _motoBillECommIndicator = objMCTable60.ECommerceIndicator;

                        // SP
                        var objMCTableSP = new TableSP_Reversal();
                        objMCTableSP.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));

                        if (!objTransactionEntities.PortalEmvRefundVoidReq)
                        {
                            if (objTransactionEntities.SourceTransaction == Convert.ToString(TxnTypeType.Completion))
                            {
                                objMCTableSP.Tag07Length = PaymentAPIResources.Mc_Visa_DS_Tag07Length;
                                int prefixIndex = table14ReversalValue.IndexOf(PaymentAPIResources.TableSP07016);
                                objMCTableSP.Tag07Value = table14ReversalValue.Substring(prefixIndex + 15, 48);
                            }
                            else
                            {
                                if (table14ReversalValue.Substring(378, 6) == PaymentAPIResources.Indicator39)
                                {
                                    objMCTableSP.Tag07Length = table14ReversalValue.Substring(420, 9);
                                    objMCTableSP.Tag07Value = table14ReversalValue.Substring(429, 48);
                                }
                                else if (table14ReversalValue.Substring(378, 6) == PaymentAPIResources.Indicator38)
                                {
                                    objMCTableSP.Tag07Length = table14ReversalValue.Substring(417, 9);
                                    objMCTableSP.Tag07Value = table14ReversalValue.Substring(426, 48);
                                }
                                else if (table14ReversalValue.Substring(378, 6) == PaymentAPIResources.Indicator37)
                                {
                                    objMCTableSP.Tag07Length = table14ReversalValue.Substring(414, 9);
                                    objMCTableSP.Tag07Value = table14ReversalValue.Substring(423, 48);
                                }
                                else if (table14ReversalValue.Substring(378, 6) == PaymentAPIResources.Indicator36)
                                {
                                    objMCTableSP.Tag07Length = table14ReversalValue.Substring(411, 9);
                                    objMCTableSP.Tag07Value = table14ReversalValue.Substring(420, 48);
                                }
                                else if (table14ReversalValue.Substring(378, 6) == PaymentAPIResources.Indicator35)
                                {
                                    objMCTableSP.Tag07Length = table14ReversalValue.Substring(408, 9);
                                    objMCTableSP.Tag07Value = table14ReversalValue.Substring(417, 48);
                                }
                                else if (table14ReversalValue.Substring(378, 6) == PaymentAPIResources.Indicator31)
                                {
                                    objMCTableSP.Tag07Length = table14ReversalValue.Substring(396, 9);
                                    objMCTableSP.Tag07Value = table14ReversalValue.Substring(405, 48);
                                }
                            }

                            string valueMCTableSP = SetTableProperties(objMCTableSP);
                            resultValue.Append(valueMCTableSP);
                        }
                        else
                        {
                            var bit63Data = CommonFunctions.GetTableSPDetails(objTransactionEntities.ReversalBit63);

                            // Ensure we have a "07" element in the collection before trying to use it.
                            if ((bit63Data.Count > 0) && (bit63Data["07"] != null))
                            {
                                string tag07val = (bit63Data["07"]);

                                objMCTableSP.Tag07Length = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.Length.ToString().PadLeft(3, '0')));
                                objMCTableSP.Tag07Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.ToString()));

                                resultValue.Append(SetTableProperties(objMCTableSP));
                            }
                        }

                        break;

                    // Set additional data for Visa Issuer
                    case CardIssuer.Visa:

                        // Table14
                        var objVisaTable14 = new Table14AdditionalVisaData();
                        objVisaTable14.ACI = table14ReversalValue.Substring(12, 3);
                        objVisaTable14.TransID = table14ReversalValue.Substring(15, 45);
                        objVisaTable14.ValidationCode = table14ReversalValue.Substring(60, 12);
                        objVisaTable14.FirstAuthorizedAmount = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(TxnAmt));
                        objVisaTable14.TotalAuthorizedAmount = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(TxnAmt));
                        string valueVisaTable14 = SetTableProperties(objVisaTable14);
                        resultValue.Append(valueVisaTable14);
                        _marketSpecificDataIndicator = objVisaTable14.MSDI;

                        // Table60
                        var objVisaTable60 = new Table60();
                        string valueVisaTable60 = SetTableProperties(objVisaTable60);
                        resultValue.Append(valueVisaTable60);
                        _motoBillECommIndicator = objVisaTable60.ECommerceIndicator;

                        // SP
                        var objVisaTableSP = new TableSP_Reversal();
                        objVisaTableSP.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));

                        if (!objTransactionEntities.PortalEmvRefundVoidReq)
                        {
                            if (objTransactionEntities.SourceTransaction == Convert.ToString(TxnTypeType.Completion))
                            {
                                objVisaTableSP.Tag07Length = PaymentAPIResources.Mc_Visa_DS_Tag07Length;
                                int prefixIndex = table14ReversalValue.IndexOf(PaymentAPIResources.TableSP07016);
                                objVisaTableSP.Tag07Value = table14ReversalValue.Substring(prefixIndex + 15, 48);
                            }
                            else
                            {
                                if (table14ReversalValue.Substring(261, 6) == PaymentAPIResources.Indicator37)
                                {
                                    objVisaTableSP.Tag07Length = table14ReversalValue.Substring(297, 9);
                                    objVisaTableSP.Tag07Value = table14ReversalValue.Substring(306, 48);
                                }
                                else if (table14ReversalValue.Substring(261, 6) == PaymentAPIResources.Indicator36)
                                {
                                    objVisaTableSP.Tag07Length = table14ReversalValue.Substring(303, 9);
                                    objVisaTableSP.Tag07Value = table14ReversalValue.Substring(313, 48);
                                }
                                else if (table14ReversalValue.Substring(261, 6) == PaymentAPIResources.Indicator35)
                                {
                                    objVisaTableSP.Tag07Length = table14ReversalValue.Substring(301, 9);
                                    objVisaTableSP.Tag07Value = table14ReversalValue.Substring(309, 48);
                                }
                                else if (table14ReversalValue.Substring(261, 6) == PaymentAPIResources.Indicator39)
                                {
                                    objVisaTableSP.Tag07Length = table14ReversalValue.Substring(303, 9);
                                    objVisaTableSP.Tag07Value = table14ReversalValue.Substring(312, 48);
                                }
                                else if (table14ReversalValue.Substring(261, 6) == PaymentAPIResources.Indicator38)
                                {
                                    objVisaTableSP.Tag07Length = table14ReversalValue.Substring(300, 9);
                                    objVisaTableSP.Tag07Value = table14ReversalValue.Substring(309, 48);
                                }
                                else if (table14ReversalValue.Substring(261, 6) == PaymentAPIResources.Indicator31)
                                {
                                    objVisaTableSP.Tag07Length = table14ReversalValue.Substring(279, 9);
                                    objVisaTableSP.Tag07Value = table14ReversalValue.Substring(288, 48);
                                }
                                //Chetu 357
                                else if (table14ReversalValue.Substring(249, 6) == PaymentAPIResources.Indicator37)
                                {
                                    objVisaTableSP.Tag07Length = table14ReversalValue.Substring(285, 9);
                                    objVisaTableSP.Tag07Value = table14ReversalValue.Substring(294, 48);
                                }
                                else if (table14ReversalValue.Substring(249, 6) == PaymentAPIResources.Indicator38)
                                {
                                    objVisaTableSP.Tag07Length = table14ReversalValue.Substring(289, 9);
                                    objVisaTableSP.Tag07Value = table14ReversalValue.Substring(298, 48);
                                }
                                else if (table14ReversalValue.Substring(249, 6) == PaymentAPIResources.Indicator39)
                                {
                                    objVisaTableSP.Tag07Length = table14ReversalValue.Substring(292, 9);
                                    objVisaTableSP.Tag07Value = table14ReversalValue.Substring(300, 48);
                                }
                            }

                            resultValue.Append(SetTableProperties(objVisaTableSP));
                        }
                        else
                        {
                            var bit63Data = CommonFunctions.GetTableSPDetails(objTransactionEntities.ReversalBit63);

                            // Ensure we have a "07" element in the collection before trying to use it.
                            if ((bit63Data.Count > 0) && (bit63Data["07"] != null))
                            {
                                string tag07val = (bit63Data["07"]);

                                objVisaTableSP.Tag07Length = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.Length.ToString().PadLeft(3, '0')));
                                objVisaTableSP.Tag07Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.ToString()));

                                resultValue.Append(SetTableProperties(objVisaTableSP));
                            }
                        }

                        break;

                    // Set additional data for American Express Issuer
                    case CardIssuer.Amex:

                        // Table14
                        var objAmexTable14 = new Table14AMEXData();
                        objAmexTable14.AmexIndicator = table14ReversalValue.Substring(12, 3);
                        objAmexTable14.TranID = table14ReversalValue.Substring(15, 45);
                        objAmexTable14.POSData = table14ReversalValue.Substring(78, 36);
                        string valueAmexTable14 = SetTableProperties(objAmexTable14);
                        resultValue.Append(valueAmexTable14);

                        // Table60
                        var objAmexTable60 = new Table60();
                        string valueAmexTable60 = SetTableProperties(objAmexTable60);
                        resultValue.Append(valueAmexTable60);
                        _motoBillECommIndicator = objAmexTable60.ECommerceIndicator;

                        // SP
                        var objAmexTableSP = new TableSP_Reversal();
                        objAmexTableSP.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));

                        if (!objTransactionEntities.PortalEmvRefundVoidReq)
                        {
                            if (objTransactionEntities.SourceTransaction == Convert.ToString(TxnTypeType.Completion))
                            {
                                objAmexTableSP.Tag07Length = PaymentAPIResources.Amex_Tag07Length;
                                int prefixIndex = table14ReversalValue.IndexOf(PaymentAPIResources.TableSP07015);
                                objAmexTableSP.Tag07Value = table14ReversalValue.Substring(prefixIndex + 15, 45);
                            }
                            else
                            {
                                if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator37)
                                {
                                    objAmexTableSP.Tag07Length = table14ReversalValue.Substring(264, 9);
                                    objAmexTableSP.Tag07Value = table14ReversalValue.Substring(273, 45);
                                }
                                else if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator36)
                                {
                                    objAmexTableSP.Tag07Length = table14ReversalValue.Substring(261, 9);
                                    objAmexTableSP.Tag07Value = table14ReversalValue.Substring(270, 45);
                                }
                                else if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator35)
                                {
                                    objAmexTableSP.Tag07Length = table14ReversalValue.Substring(258, 9);
                                    objAmexTableSP.Tag07Value = table14ReversalValue.Substring(267, 45);
                                }
                                else if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator38)
                                {
                                    objAmexTableSP.Tag07Length = table14ReversalValue.Substring(267, 9);
                                    objAmexTableSP.Tag07Value = table14ReversalValue.Substring(276, 45);
                                }
                                else if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator39)
                                {
                                    objAmexTableSP.Tag07Length = table14ReversalValue.Substring(270, 9);
                                    objAmexTableSP.Tag07Value = table14ReversalValue.Substring(279, 45);
                                }
                                else if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator30)
                                {
                                    objAmexTableSP.Tag07Length = table14ReversalValue.Substring(243, 9);
                                    objAmexTableSP.Tag07Value = table14ReversalValue.Substring(252, 45);
                                }
                            }

                            string valueAmexTableSP = SetTableProperties(objAmexTableSP);
                            resultValue.Append(valueAmexTableSP);
                        }
                        else
                        {
                            var bit63Data = CommonFunctions.GetTableSPDetails(objTransactionEntities.ReversalBit63);

                            // Ensure we have a "07" element in the collection before trying to use it.
                            if ((bit63Data.Count > 0) && (bit63Data["07"] != null))
                            {
                                string tag07val = (bit63Data["07"]);

                                objAmexTableSP.Tag07Length = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.Length.ToString().PadLeft(3, '0')));
                                objAmexTableSP.Tag07Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.ToString()));

                                resultValue.Append(SetTableProperties(objAmexTableSP));
                            }
                        }

                        break;

                    // Set additional data for Discover
                    case CardIssuer.DCI:
                    case CardIssuer.Discover:

                        // Table14
                        var objDiscoverTable14 = new Table14AdditionalDiscoverData();
                        objDiscoverTable14.DiscoverIndicator = table14ReversalValue.Substring(12, 3);
                        objDiscoverTable14.TransactionID = table14ReversalValue.Substring(15, 45);
                        objDiscoverTable14.TotalAuthAmount = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(TxnAmt));
                        string valueDiscoverTable14 = SetTableProperties(objDiscoverTable14);
                        resultValue.Append(valueDiscoverTable14);

                        // Table60
                        var objDiscoverTable60 = new Table60();
                        string valueDiscoverTable60 = SetTableProperties(objDiscoverTable60);
                        resultValue.Append(valueDiscoverTable60);
                        _motoBillECommIndicator = objDiscoverTable60.ECommerceIndicator;

                        // SP
                        var objDisTableSP = new TableSP_Reversal();
                        objDisTableSP.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));

                        if (!objTransactionEntities.PortalEmvRefundVoidReq)
                        {
                            if (objTransactionEntities.SourceTransaction == Convert.ToString(TxnTypeType.Completion))
                            {
                                objDisTableSP.Tag07Length = PaymentAPIResources.Mc_Visa_DS_Tag07Length;
                                int prefixIndex = table14ReversalValue.IndexOf(PaymentAPIResources.TableSP07016);
                                objDisTableSP.Tag07Value = table14ReversalValue.Substring(prefixIndex + 15, 48);
                            }
                            else
                            {
                                if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator39)
                                {
                                    objDisTableSP.Tag07Length = table14ReversalValue.Substring(543, 9);
                                    objDisTableSP.Tag07Value = table14ReversalValue.Substring(552, 48);
                                }
                                else if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator38)
                                {
                                    objDisTableSP.Tag07Length = table14ReversalValue.Substring(540, 9);
                                    objDisTableSP.Tag07Value = table14ReversalValue.Substring(549, 48);
                                }
                                else if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator37)
                                {
                                    objDisTableSP.Tag07Length = table14ReversalValue.Substring(537, 9);
                                    objDisTableSP.Tag07Value = table14ReversalValue.Substring(546, 48);
                                }
                                else if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator36)
                                {
                                    objDisTableSP.Tag07Length = table14ReversalValue.Substring(534, 9);
                                    objDisTableSP.Tag07Value = table14ReversalValue.Substring(543, 48);
                                }
                                else if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator35)
                                {
                                    objDisTableSP.Tag07Length = table14ReversalValue.Substring(531, 9);
                                    objDisTableSP.Tag07Value = table14ReversalValue.Substring(540, 48);
                                }
                                else if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator31)
                                {
                                    objDisTableSP.Tag07Length = table14ReversalValue.Substring(519, 9);
                                    objDisTableSP.Tag07Value = table14ReversalValue.Substring(528, 48);
                                }
                            }

                            string valuedisTableSP = SetTableProperties(objDisTableSP);
                            resultValue.Append(valuedisTableSP);
                        }
                        else
                        {
                            var bit63Data = CommonFunctions.GetTableSPDetails(objTransactionEntities.ReversalBit63);

                            // Ensure we have a "07" element in the collection before trying to use it.
                            if ((bit63Data.Count > 0) && (bit63Data["07"] != null))
                            {
                                string tag07val = (bit63Data["07"]);

                                objDisTableSP.Tag07Length = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.Length.ToString().PadLeft(3, '0')));
                                objDisTableSP.Tag07Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.ToString()));

                                resultValue.Append(SetTableProperties(objDisTableSP));
                            }
                        }

                        break;

                    // Set additional data for Diners Club International Issuer
                    case CardIssuer.Diners:

                        // Table14
                        var objDITable14 = new Table14AdditionalDiscoverData();
                        objDITable14.DiscoverIndicator = table14ReversalValue.Substring(12, 3);
                        objDITable14.TransactionID = table14ReversalValue.Substring(15, 45);
                        objDITable14.TotalAuthAmount = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(TxnAmt));
                        string valueDITable14 = SetTableProperties(objDITable14);
                        resultValue.Append(valueDITable14);

                        // Table60
                        //Setting Table 60 for Discover
                        var objDITable60 = new Table60();
                        string valueDITable60 = SetTableProperties(objDITable60);

                        //Add table 60 to resultValue
                        resultValue.Append(valueDITable60);
                        
                        //Assign MOTO/Bill Payment/Electronic Commerce Type Indicator.
                        _motoBillECommIndicator = objDITable60.ECommerceIndicator;

                        // SP
                        var objDITableSP = new TableSP_Reversal();
                        objDITableSP.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));

                        if (!objTransactionEntities.PortalEmvRefundVoidReq)
                        {
                            if (objTransactionEntities.SourceTransaction == Convert.ToString(TxnTypeType.Completion))
                            {
                                objDITableSP.Tag07Length = PaymentAPIResources.Diners_Tag07Length;
                                int prefixIndex = table14ReversalValue.IndexOf(PaymentAPIResources.TableSP07014);
                                objDITableSP.Tag07Value = table14ReversalValue.Substring(prefixIndex + 15, 42);
                            }
                            else
                            {
                                if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator39)
                                {
                                    objDITableSP.Tag07Length = table14ReversalValue.Substring(549, 9);
                                    objDITableSP.Tag07Value = table14ReversalValue.Substring(558, 48);
                                }
                                else if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator38)
                                {
                                    objDITableSP.Tag07Length = table14ReversalValue.Substring(546, 9);
                                    objDITableSP.Tag07Value = table14ReversalValue.Substring(555, 48);
                                }
                                else if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator37)
                                {
                                    objDITableSP.Tag07Length = table14ReversalValue.Substring(543, 9);
                                    objDITableSP.Tag07Value = table14ReversalValue.Substring(552, 42);
                                }
                                else if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator36)
                                {
                                    objDITableSP.Tag07Length = table14ReversalValue.Substring(540, 9);
                                    objDITableSP.Tag07Value = table14ReversalValue.Substring(549, 42);
                                }
                                else if (table14ReversalValue.Substring(501, 6) == PaymentAPIResources.Indicator35)
                                {
                                    objDITableSP.Tag07Length = table14ReversalValue.Substring(537, 9);
                                    objDITableSP.Tag07Value = table14ReversalValue.Substring(546, 42);
                                }
                            }

                            string valueDITableSP = SetTableProperties(objDITableSP);
                            resultValue.Append(valueDITableSP);
                        }
                        else
                        {
                            var bit63Data = CommonFunctions.GetTableSPDetails(objTransactionEntities.ReversalBit63);

                            // Ensure we have a "07" element in the collection before trying to use it.
                            if ((bit63Data.Count > 0) && (bit63Data["07"] != null))
                            {
                                string tag07val = (bit63Data["07"]);

                                objDITableSP.Tag07Length = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.Length.ToString().PadLeft(3, '0')));
                                objDITableSP.Tag07Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.ToString()));

                                resultValue.Append(SetTableProperties(objDITableSP));
                            }
                        }

                        break;

                    // Set additional data for JCB
                    case CardIssuer.JCB:
                        // Table14

                        //Setting Table 14 for Discover group credit card.
                        var objJCBTable14 = new Table14AdditionalDiscoverData();
                        objJCBTable14.DiscoverIndicator = table14ReversalValue.Substring(12, 3);
                        objJCBTable14.TransactionID = table14ReversalValue.Substring(15, 45);
                        objJCBTable14.TotalAuthAmount = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(TxnAmt));
                        string valueJCBTable14 = SetTableProperties(objJCBTable14);
                        resultValue.Append(valueJCBTable14);

                        // Table60
                        var objJCBTable60 = new Table60();
                        string valueJCBTable60 = SetTableProperties(objJCBTable60);
                        resultValue.Append(valueJCBTable60);
                        _motoBillECommIndicator = objJCBTable60.ECommerceIndicator;

                        // SP
                        var objJCBTableSP = new TableSP_Reversal();
                        objJCBTableSP.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));

                        if (!objTransactionEntities.PortalEmvRefundVoidReq)
                        {
                            if (objTransactionEntities.SourceTransaction == Convert.ToString(TxnTypeType.Completion))
                            {
                                objJCBTableSP.Tag07Length = PaymentAPIResources.Mc_Visa_DS_Tag07Length;
                                int prefixIndex = table14ReversalValue.IndexOf(PaymentAPIResources.TableSP07016);
                                objJCBTableSP.Tag07Value = table14ReversalValue.Substring(prefixIndex + 15, 48);
                            }
                            else
                            {
                                if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator39)
                                {
                                    objJCBTableSP.Tag07Length = table14ReversalValue.Substring(267, 9);
                                    objJCBTableSP.Tag07Value = table14ReversalValue.Substring(276, 48);
                                }
                                else if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator38)
                                {
                                    objJCBTableSP.Tag07Length = table14ReversalValue.Substring(264, 9);
                                    objJCBTableSP.Tag07Value = table14ReversalValue.Substring(273, 48);
                                }
                                else if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator37)
                                {
                                    objJCBTableSP.Tag07Length = table14ReversalValue.Substring(261, 9);
                                    objJCBTableSP.Tag07Value = table14ReversalValue.Substring(270, 48);
                                }
                                else if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator36)
                                {
                                    objJCBTableSP.Tag07Length = table14ReversalValue.Substring(258, 9);
                                    objJCBTableSP.Tag07Value = table14ReversalValue.Substring(267, 48);
                                }
                                else if (table14ReversalValue.Substring(225, 6) == PaymentAPIResources.Indicator35)
                                {
                                    objJCBTableSP.Tag07Length = table14ReversalValue.Substring(255, 9);
                                    objJCBTableSP.Tag07Value = table14ReversalValue.Substring(264, 48);
                                }
                            }

                            string valueJCBTableSP = SetTableProperties(objJCBTableSP);
                            resultValue.Append(valueJCBTableSP);
                        }
                        else
                        {
                            var bit63Data = CommonFunctions.GetTableSPDetails(objTransactionEntities.ReversalBit63);

                            // Ensure we have a "07" element in the collection before trying to use it.
                            if ((bit63Data.Count > 0) && (bit63Data["07"] != null))
                            {
                                string tag07val = (bit63Data["07"]);

                                objJCBTableSP.Tag07Length = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.Length.ToString().PadLeft(3, '0')));
                                objJCBTableSP.Tag07Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.ToString()));

                                resultValue.Append(SetTableProperties(objJCBTableSP));
                            }
                        }

                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal("CnTransactionMessage.SetEcommAdditionalDataForCreditReversalVoid();", ex);

                throw;
            }
        }

        /// <summary>
        /// This method sets Ecomm additional data for debit sale.
        /// </summary>
        /// <param name="resultValue"></param>
        /// <param name="objTransactionEntities"></param>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        private static void SetEcommAdditionalDataForDebitSale(StringBuilder resultValue, CnTransactionEntities objTransactionEntities)
        {
            ILogger logger = new Logger();

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
            {
                logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForDebitSale() EntryMode:{0};IsTA:{1};IsTOR:{2};", objTransactionEntities.EntryMode, objTransactionEntities.IsTransArmor, objTransactionEntities.IsTORTransaction));
                logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEcommAdditionalDataForDebitSale() ReversalBit63:{0};", objTransactionEntities.ReversalBit63));
            }
            try
            {
                switch (objTransactionEntities.CardIssuer)
                {
                    case CardIssuer.MasterCard:
                        break;
                    case CardIssuer.Visa:
                        break;
                    case CardIssuer.Amex:
                        break;
                    case CardIssuer.DCI:
                        break;
                    default:
                        break;
                }

                Table66 table66 = new Table66();
                string valueTable66 = SetTableProperties(table66);

                valueTable66 = "|00|08" + valueTable66;

                resultValue.Append(valueTable66);
            }
            catch (Exception ex)
            {
                logger.LogInfoFatal("CnTransactionMessage.SetEcommAdditionalDataForDebitSale();", ex);

                throw;
            }
        }

        /// <summary>
        /// Purpose             :   Generate Client Ref Number in the format STAN|TPPID, right justified and left padded with "0" in case of canada
        /// Function Name       :   GetCanadaClientRef 
        /// Created By          :   Naveen Kumar
        /// Created On          :   03/23/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY"  
        /// </summary>
        /// <returns></returns>
        public static string GetCanadaClientRef()
        {
            int transactionId = new Random(Guid.NewGuid().GetHashCode()).Next(10000, 99999);

            return "00" + transactionId + "V" + "003" + "000";
        }

        /// <summary>
        /// Function Name       :   FillMerchantDto
        /// Created By          :   Naveen Kumar
        /// Created On          :   03/23/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY"  
        /// </summary>
        /// <returns></returns>
        public static MTDTransactionDto FillMerchantDto(Iso8583Response isoResponse)
        {
            string re = @"<!DOCTYPE[^>[]*(\[[^]]*\])?>";
            isoResponse.CompleteResponse = Regex.Replace(isoResponse.CompleteResponse, re, String.Empty);
         
            return new MTDTransactionDto 
            {
                ResponseCode = isoResponse.ResponseCode,
                ResponseXml = isoResponse.CompleteResponse,
                AuthId = isoResponse.AuthIdentificationResponse,
                AddRespData = !string.IsNullOrEmpty(isoResponse.AdditionalResponseData) ? isoResponse.AdditionalResponseData : isoResponse.Message,
                ResponseBit63 = isoResponse.FDPrivateUsageData63,
                TxnType = isoResponse.TransactionType
            };
        }

        /// <summary>
        /// Purpose             :   Create token only request for canada booker app
        /// Function Name       :   CnPrepareTokenOnlyRequest
        /// Created By          :   Naveen Kumar
        /// Created On          :   11/05/2016
        /// </summary>
        /// <returns>string</returns>
        public static string CnPrepareBookerAppRequest(TransactionRequest transactionRequest, string clientRef, out CnTransactionEntities cnTransactionRequest)
        {
            ILogger logger = new Logger();

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
            {
                logger.LogInfoMessage(string.Format("CnTransactionMessage.CnPrepareBookerAppRequest() ClientRef:{0};IsTA:{1};RCCI:{2};TokenType:{3};",
                                        clientRef, transactionRequest.IsTransArmor, (!string.IsNullOrEmpty(transactionRequest.RCCIToken) ? transactionRequest.RCCIToken : "<NULL>"), transactionRequest.TokenType));
            }

            string payload = string.Empty;
            try
            {
                CnTransactionEntities cnTransactionEntities = new CnTransactionEntities();
                payload = CnPreparePostTransaction.PrepareBookerAppPayloadRequest(transactionRequest, clientRef, out cnTransactionEntities);
                cnTransactionRequest = cnTransactionEntities;

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                {
                    logger.LogInfoMessage(string.Format("CnTransactionMessage.CnPrepareBookerAppRequest() transReq.TokenType:{0};cnTransEnt.TokenType:{1};payload:{2};",
                                            transactionRequest.TokenType, cnTransactionEntities.TokenType, payload));
                }
            }
            catch (Exception ex)
            {
                logger.LogInfoFatal("CnTransactionMessage.CnPrepareBookerAppRequest();", ex);

                throw;
            }

            return payload;
        }

        /// <summary>
        /// Purpose             :   Prepare bitmap fields for token only request
        /// Function Name       :   PrepareFieldsForTokenOnlyRequest
        /// Created By          :   Naveen Kumar
        /// Created On          :   11/07/2016
        /// </summary>
        /// <returns>string</returns>
        public static CnTransactionEntities PrepareFieldsForTokenOnlyRequest(TransactionRequest transactionRequest)
        {
            return new CnTransactionEntities
            {
                IndustryType = (IndustryType)Enum.Parse(typeof(IndustryType), transactionRequest.IndustryType),
                CardType = (CardType)Enum.Parse(typeof(CardType), Convert.ToString(transactionRequest.PaymentType)),
                TransType = transactionRequest.TransactionType,
                PrimaryAccountNumber = transactionRequest.CardNumber,
                ProcessingCode = SigmaStaticValue.ProcessingCodeForCreditSale,
                TransmissionDateTime = FormatFields.FormatTransDateTime(transactionRequest.TrnmsnDateTime),
                SystemTrace = transactionRequest.Stan,
                RetrievalRefNumber = transactionRequest.TransRefNo,
                TerminalId = transactionRequest.TerminalId.PadLeft(8, '0'),
                MerchantId = transactionRequest.FdMerchantId.PadLeft(15, '0'),
                TimeLocalTransmission = FormatFields.FormatLocalTime(transactionRequest.LocalDateTime),
                DateLocalTrans = FormatFields.FormatLocalDate(transactionRequest.LocalDateTime),
                IsTORTransaction = transactionRequest.IsTORTransaction,
                BookerAppCardExpiry = transactionRequest.ExpiryDate,

                // Changes for card registration
                CVVCode = transactionRequest.CardCvv,
                MerchantZipCode = FormatFields.FormatMerchantZipCode(transactionRequest.ZipCode, transactionRequest.CardType.ToString()),
                TokenType = transactionRequest.TokenType    //PAY-13
            };
        }

        /// <summary>
        /// Purpose             :   Prepare additional data for token only
        /// Function Name       :   SetAdditionalDataForTokenOnly
        /// Created By          :   Naveen Kumar
        /// Created On          :   11/07/2016
        /// </summary>
        /// <returns>string</returns>
        private Dictionary<Element, string> SetAdditionalDataForTokenOnly(Dictionary<Element, string> objElements, CnTransactionEntities objTransactionEntities)
        {
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetAdditionalDataForTokenOnly();"));

            StringBuilder resultValue = new StringBuilder();
            try
            {
                var tableSP = new TableSP(objTransactionEntities.TokenType);
                string tableSPValues = SetTableProperties(tableSP);
                resultValue.Append(tableSPValues);

                if (!string.IsNullOrEmpty(resultValue.ToString()))
                {
                    var bit63length = SetAdditionalTableLength(resultValue.ToString());

                    // Set the length of bitmap 63 and append it with bitmap 63
                    resultValue = resultValue.Insert(0, bit63length);

                    // Add the bit 63 and realated tables
                    objElements.Add(CnBitmapDefinition.FDPrivateUsageData63, resultValue.ToString());

                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetAdditionalDataForTokenOnly() FDPrivateUsageData63:{0};", resultValue.ToString()));

                    // Set Bit63 data in objTransactionEntities for saving it in Database.
                    objTransactionEntities.FDPrivateUsageData63 = resultValue.ToString();
                }
                else
                {
                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetAdditionalDataForTokenOnly() ResultValue = <<NULL>>;"));
                }

                return objElements;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal("CnTransactionMessage.SetAdditionalDataForTokenOnly();", ex);

                throw;
            }
        }

        /// <summary>
        /// Purpose             :   Prepare bitmap fields for booker app payment request
        /// Function Name       :   PrepareFieldsForBookerAppPayment
        /// Created By          :   Naveen Kumar
        /// Created On          :   11/08/2016
        /// </summary>
        /// <returns>CnTransactionEntities</returns>
        public static CnTransactionEntities PrepareFieldsForBookerAppPayment(TransactionRequest transactionRequest)
        {
            return new CnTransactionEntities
            {
                IndustryType = (IndustryType)Enum.Parse(typeof(IndustryType), transactionRequest.IndustryType),
                PrimaryAccountNumber = transactionRequest.CardNumber,
                ProcessingCode = SigmaStaticValue.ProcessingCodeForCreditSale,
                TransmissionDateTime = FormatFields.FormatTransDateTime(transactionRequest.TrnmsnDateTime),
                SystemTrace = transactionRequest.Stan,
                RetrievalRefNumber = transactionRequest.TransRefNo,
                TerminalId = transactionRequest.TerminalId.PadLeft(8, '0'),
                MerchantId = transactionRequest.FdMerchantId.PadLeft(15, '0'),
                NetworkInfoCode = SigmaStaticValue.NetworkInfoCodeForTokenOnly,
                IsTORTransaction = transactionRequest.IsTORTransaction,
                TokenType = transactionRequest.TokenType    //PAY-13
            };
        }

        /// <summary>
        /// This method sets additional data for credit sale for Retail.
        /// </summary>
        /// <param name="resultValue"></param>
        /// <param name="objTransactionEntities"></param>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        private void SetRetailAdditionalDataForCreditSaleAuthCompletion(StringBuilder resultValue, CnTransactionEntities objTransactionEntities)
        {
            try
            {
                string authorizedAmount = FormatFields.FormatAmount(Convert.ToDouble(objTransactionEntities.AmountOfTransaction));

                switch (objTransactionEntities.CardIssuer)
                {
                    // Set additional data for MasterCard Issuer
                    case CardIssuer.MasterCard:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            var strBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                            strBuilder.Remove(54, 12);
                            strBuilder.Insert(54, authorizedAmount);
                            objTransactionEntities.Table14Response = strBuilder.ToString();
                            resultValue.Append(objTransactionEntities.Table14Response);
                        }
                        else
                        {
                            var objMCTable14 = new Table14AdditionalMasterData();
                            string valueMCTable14 = SetTableProperties(objMCTable14);
                            resultValue.Append(valueMCTable14);
                        }

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objMCTable69 = new Table69();
                            string valueMCTable69 = SetTableProperties(objMCTable69);
                            resultValue.Append(valueMCTable69);
                        }

                        // TableMC
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                resultValue.Append(objTransactionEntities.TableMCResponse);
                            }
                            else
                            {
                                var objTablesMC = new TableMC();
                                string valueTableMC = SetTableProperties(objTablesMC);
                                resultValue.Append(valueTableMC);
                            }
                        }

                        // Table36
                        if (objTransactionEntities.TransType != TransactionType.Authorization.ToString() && !objTransactionEntities.IsTORTransaction)
                        {
                            var objMCTables36 = new Table36();
                            objMCTables36.OrderNo = objTransactionEntities.RetrievalRefNumber + PaymentAPIResources.ThreeSpaces;
                            objMCTables36.OrderNo = CnGenericHandler.ConvertToHex(objMCTables36.OrderNo);
                            objMCTables36.OrderNo = CnTransactionMessage.ApplyDatawireFormat(objMCTables36.OrderNo);
                            string valueMCTables36 = SetTableProperties(objMCTables36);
                            resultValue.Append(valueMCTables36);
                        }

                        //Transarmor
                        if (objTransactionEntities.IsTransArmor)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }

                                tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objMCTableSp = new TableSP_Retail();
                                objMCTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));
                                objMCTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId));
                                objMCTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objMCTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueMCTableSp = SetTableProperties(objMCTableSp);
                                resultValue.Append(valueMCTableSp);
                            }
                        }

                        //TableSD
                        var objMCTableSD = new TableSD();
                        string valueMCTableSD = SetTableProperties(objMCTableSD);
                        resultValue.Append(valueMCTableSD);

                        break;

                    //Set additional data for Visa Issuer
                    case CardIssuer.Visa:

                        //Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                            aStringBuilder.Remove(32, 12);
                            aStringBuilder.Insert(32, authorizedAmount);
                            aStringBuilder.Remove(44, 12);
                            aStringBuilder.Insert(44, authorizedAmount);
                            objTransactionEntities.Table14Response = aStringBuilder.ToString();
                            resultValue.Append(objTransactionEntities.Table14Response);
                        }
                        else
                        {
                            var objVisaTable14 = new Table14AdditionalVisaData();
                            string valueVisaTable14 = SetTableProperties(objVisaTable14);
                            resultValue.Append(valueVisaTable14);
                        }

                        //Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objVisaTable69 = new Table69();
                            string valueVisaTable69 = SetTableProperties(objVisaTable69);
                            resultValue.Append(valueVisaTable69);
                        }

                        //TableVI
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                resultValue.Append(objTransactionEntities.TableVIResponse);
                            }
                            else
                            {
                                var objVisaTableVI = new TableVI();
                                string valueVisaTableVI = SetTableProperties(objVisaTableVI);
                                resultValue.Append(valueVisaTableVI);
                            }
                        }

                        //Table36
                        if (objTransactionEntities.TransType != TransactionType.Authorization.ToString() && !objTransactionEntities.IsTORTransaction)
                        {
                            var objVITables36 = new Table36();
                            objVITables36.OrderNo = objTransactionEntities.RetrievalRefNumber + PaymentAPIResources.ThreeSpaces;
                            objVITables36.OrderNo = CnGenericHandler.ConvertToHex(objVITables36.OrderNo);
                            objVITables36.OrderNo = CnTransactionMessage.ApplyDatawireFormat(objVITables36.OrderNo);
                            objVITables36.EcommUrl = PaymentAPIResources.VIEcommUrl;
                            string valueVITable36 = SetTableProperties(objVITables36);
                            resultValue.Append(valueVITable36);
                        }

                        //Transarmor
                        if (objTransactionEntities.IsTransArmor)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;

                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }

                                tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objVITableSp = new TableSP_Retail();
                                objVITableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));
                                objVITableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId));
                                objVITableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objVITableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueVITableSp = SetTableProperties(objVITableSp);
                                resultValue.Append(valueVITableSp);
                            }
                        }

                        //TableSD
                        var objVITableSD = new TableSD();
                        string valueVITableSD = SetTableProperties(objVITableSD);
                        resultValue.Append(valueVITableSD);

                        break;

                    //Set additional data for American Express Issuer
                    case CardIssuer.Amex:

                        //Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            resultValue.Append(objTransactionEntities.Table14Response);
                        }
                        else
                        {
                            var objAmexTable14 = new Table14AMEXData();
                            string valueAmexTable14 = SetTableProperties(objAmexTable14);
                            resultValue.Append(valueAmexTable14);
                        }

                        //Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objAmexTable69 = new Table69();
                            string valueAmexTable69 = SetTableProperties(objAmexTable69);
                            resultValue.Append(valueAmexTable69);
                        }

                        //Table36
                        if (objTransactionEntities.TransType != TransactionType.Authorization.ToString() && !objTransactionEntities.IsTORTransaction)
                        {
                            var objAmexTables36 = new Table36();
                            objAmexTables36.OrderNo = objTransactionEntities.RetrievalRefNumber + PaymentAPIResources.ThreeSpaces;
                            objAmexTables36.OrderNo = CnGenericHandler.ConvertToHex(objAmexTables36.OrderNo);
                            objAmexTables36.OrderNo = CnTransactionMessage.ApplyDatawireFormat(objAmexTables36.OrderNo);
                            string valueAmexTables36 = SetTableProperties(objAmexTables36);
                            resultValue.Append(valueAmexTables36);
                        }

                        //Transarmor
                        if (objTransactionEntities.IsTransArmor)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;

                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }

                                tableValues = tableValues + PaymentAPIResources.TableSP07015 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objAmexTableSp = new TableSP_Retail();
                                objAmexTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));
                                objAmexTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId));
                                objAmexTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objAmexTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueAmexTableSp = SetTableProperties(objAmexTableSp);
                                resultValue.Append(valueAmexTableSp);
                            }
                        }

                        //TableSD
                        var objAmexTableSD = new TableSD();
                        string valueAmexTableSD = SetTableProperties(objAmexTableSD);
                        resultValue.Append(valueAmexTableSD);

                        break;

                    //Set additional data for Discover,Diners and JCB
                    case CardIssuer.Discover:
                    case CardIssuer.Diners:
                    case CardIssuer.JCB:

                        //Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            if (objTransactionEntities.CardIssuer.ToString() == CardTypeType.Discover.ToString() || objTransactionEntities.CardIssuer.ToString() == CardTypeType.Diners.ToString())
                            {
                                var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                                aStringBuilder.Remove(52, 12);
                                aStringBuilder.Insert(52, authorizedAmount);
                                objTransactionEntities.Table14Response = aStringBuilder.ToString();
                                resultValue.Append(objTransactionEntities.Table14Response);
                            }
                            else
                            {
                                var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                                aStringBuilder.Remove(82, 12);
                                aStringBuilder.Insert(82, authorizedAmount);
                                objTransactionEntities.Table14Response = aStringBuilder.ToString();
                                resultValue.Append(objTransactionEntities.Table14Response);
                            }
                        }
                        else
                        {
                            var objDiscoverTable14 = new Table14AdditionalDiscoverData();
                            string valueDiscoverTable14 = SetTableProperties(objDiscoverTable14);
                            resultValue.Append(valueDiscoverTable14);
                        }

                        //Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objDiscoverTable69 = new Table69();
                            string valueDiscoverTable69 = SetTableProperties(objDiscoverTable69);
                            resultValue.Append(valueDiscoverTable69);
                        }

                        //TableDS
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString() && objTransactionEntities.CardIssuer.ToString() != CardTypeType.JCB.ToString())
                            {
                                resultValue.Append(objTransactionEntities.TableDSResponse);
                            }
                            else if (objTransactionEntities.TransType != TransactionType.Completion.ToString() && objTransactionEntities.CardIssuer.ToString() != CardTypeType.JCB.ToString())
                            {
                                var objTableDS = new TableDS();
                                string valueTableDS = SetTableProperties(objTableDS);
                                resultValue.Append(valueTableDS);
                            }
                        }

                        //Table36
                        if (objTransactionEntities.TransType != TransactionType.Authorization.ToString() && !objTransactionEntities.IsTORTransaction)
                        {
                            var objDSTables36 = new Table36();
                            objDSTables36.OrderNo = objTransactionEntities.RetrievalRefNumber + PaymentAPIResources.ThreeSpaces;
                            objDSTables36.OrderNo = CnGenericHandler.ConvertToHex(objDSTables36.OrderNo);
                            objDSTables36.OrderNo = CnTransactionMessage.ApplyDatawireFormat(objDSTables36.OrderNo);
                            string valueDSTables36 = SetTableProperties(objDSTables36);
                            resultValue.Append(valueDSTables36);
                        }

                        //Transarmor
                        if (objTransactionEntities.IsTransArmor)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }

                                if (objTransactionEntities.CardIssuer.ToString() == CardIssuer.Diners.ToString())
                                    tableValues = tableValues + PaymentAPIResources.TableSP07014 + objTransactionEntities.TableSPResponse;
                                else
                                    tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;

                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objDSTableSp = new TableSP_Retail();
                                objDSTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType));
                                objDSTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId));
                                objDSTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objDSTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueDSTableSp = SetTableProperties(objDSTableSp);
                                resultValue.Append(valueDSTableSp);
                            }
                        }

                        //TableSD

                        var objDSTableSD = new TableSD();
                        string valueDSTableSD = SetTableProperties(objDSTableSD);
                        resultValue.Append(valueDSTableSD);

                        break;

                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal("CnTransactionMessage.SetRetailAdditionalDataForCreditSaleAuthCompletion();", ex);

                throw;
            }
        }

        /// <summary>
        /// This method sets additional data for debit sale for Retail.
        /// </summary>
        /// <param name="resultValue"></param>
        /// <param name="objTransactionEntities"></param>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        private void SetRetailAdditionalDataForDebitSaleAuthCompletion(StringBuilder resultValue, CnTransactionEntities objTransactionEntities)
        {
            try
            {
                switch (objTransactionEntities.CardIssuer)
                {
                    // Set additional data for MasterCard Issuer
                    case CardIssuer.MasterCard:

                        // Table33
                        var table33MC = new Table33();
                        table33MC.BaseDerivationKeyID = FormatFields.FormatBaseDerivationkeyId(objTransactionEntities.KeySerialNumber);
                        table33MC.DeviceID = FormatFields.FormatDeviceId(objTransactionEntities.KeySerialNumber);
                        table33MC.TransactionCounter = FormatFields.FormatTransactionCounter(objTransactionEntities.KeySerialNumber);
                        string valueTable33MC = SetTableProperties(table33MC);
                        resultValue.Append(valueTable33MC);

                        // Table56
                        var table56MC = new Table56();
                        string valueTable56MC = SetTableProperties(table56MC);
                        resultValue.Append(valueTable56MC);

                        // Table66
                        var table66MC = new Table66();
                        string valueTable66MC = SetTableProperties(table66MC);
                        resultValue.Append(valueTable66MC);

                        break;

                    // Set additional data for Visa Issuer
                    case CardIssuer.Visa:

                        // Table33
                        var table33VI = new Table33();
                        table33VI.BaseDerivationKeyID = FormatFields.FormatBaseDerivationkeyId(objTransactionEntities.KeySerialNumber);
                        table33VI.DeviceID = FormatFields.FormatDeviceId(objTransactionEntities.KeySerialNumber);
                        table33VI.TransactionCounter = FormatFields.FormatTransactionCounter(objTransactionEntities.KeySerialNumber);
                        string valueTable33VI = SetTableProperties(table33VI);
                        resultValue.Append(valueTable33VI);

                        // Table56
                        var table56VI = new Table56();
                        string valueTable56VI = SetTableProperties(table56VI);
                        resultValue.Append(valueTable56VI);

                        // Table66
                        var table66VI = new Table66();
                        string valueTable66VI = SetTableProperties(table66VI);
                        resultValue.Append(valueTable66VI);

                        break;

                    // Set additional data for American Express Issuer
                    case CardIssuer.Amex:

                        // Table33
                        var table33Amex = new Table33();
                        table33Amex.BaseDerivationKeyID = FormatFields.FormatBaseDerivationkeyId(objTransactionEntities.KeySerialNumber);
                        table33Amex.DeviceID = FormatFields.FormatDeviceId(objTransactionEntities.KeySerialNumber);
                        table33Amex.TransactionCounter = FormatFields.FormatTransactionCounter(objTransactionEntities.KeySerialNumber);
                        string valueTable33Amex = SetTableProperties(table33Amex);
                        resultValue.Append(valueTable33Amex);

                        // Table56
                        var table56Amex = new Table56();
                        string valueTable56Amex = SetTableProperties(table56Amex);
                        resultValue.Append(valueTable56Amex);

                        // Table66
                        var table66Amex = new Table66();
                        string valueTable66Amex = SetTableProperties(table66Amex);
                        resultValue.Append(valueTable66Amex);

                        break;

                    // Set additional data for Discover
                    case CardIssuer.Discover:
                    case CardIssuer.Diners:
                    case CardIssuer.JCB:

                        // Table33
                        var table33DS = new Table33();
                        table33DS.BaseDerivationKeyID = FormatFields.FormatBaseDerivationkeyId(objTransactionEntities.KeySerialNumber);
                        table33DS.DeviceID = FormatFields.FormatDeviceId(objTransactionEntities.KeySerialNumber);
                        table33DS.TransactionCounter = FormatFields.FormatTransactionCounter(objTransactionEntities.KeySerialNumber);
                        string valueTable33DS = SetTableProperties(table33DS);
                        resultValue.Append(valueTable33DS);

                        // Table56
                        var table56DS = new Table56();
                        string valueTable56DS = SetTableProperties(table56DS);
                        resultValue.Append(valueTable56DS);

                        // Table66
                        var table66DS = new Table66();
                        string valueTable66DS = SetTableProperties(table66DS);
                        resultValue.Append(valueTable66DS);

                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal("CnTransactionMessage.SetRetailAdditionalDataForDebitSaleAuthCompletion();", ex);

                throw;
            }
        }

        /// <summary>
        /// Prepare bitmap fields for key update request
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        public static CnTransactionEntities MakeKeyUpdateRequest(TransactionRequest transactionRequest)
        {
            return new CnTransactionEntities
            {
                IndustryType = (IndustryType)Enum.Parse(typeof(IndustryType), transactionRequest.IndustryType),
                ProcessingCode = "010000",
                SystemTrace = transactionRequest.Stan,
                NetworkInternationId = "038",//001 standard key update//038
                TerminalId = "01351646",
                MerchantId = "000082004330015"
            };
        }

        /// <summary>
        /// To get Interac Mac key
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        public static CnTransactionEntities MakeDebitKeyRequest(TransactionRequest transactionRequest)
        {
            return new CnTransactionEntities
            {
                IndustryType = (IndustryType)Enum.Parse(typeof(IndustryType), transactionRequest.IndustryType),
                ProcessingCode = SigmaStaticValue.ProcessingCodeForCreditSale,
                SystemTrace = transactionRequest.Stan,
                NetworkInternationId = SigmaStaticValue.NetworkInternationId,
                TerminalId = transactionRequest.TerminalId,
                MerchantId = transactionRequest.FdMerchantId
            };
        }

        /// <summary>
        /// Prepare additional data for key update request
        /// </summary>
        /// <param name="objMessageType"></param>
        /// <param name="objElements"></param>
        /// <param name="objTransactionEntities"></param>
        /// <returns></returns>
        private Dictionary<Element, string> SetAdditionalDataForKeyUpdate(MessageType objMessageType, Dictionary<Element, string> objElements, CnTransactionEntities objTransactionEntities)
        {
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetAdditionalDataForKeyUpdate() objMessageType:{0};", objMessageType));

            StringBuilder resultValue = new StringBuilder();
            try
            {
                var tableSP = new TableSP_KeyUpdate();
                string tableSPValues = SetTableProperties(tableSP);
                resultValue.Append(tableSPValues);

                if (!string.IsNullOrEmpty(resultValue.ToString()))
                {
                    var bit63length = SetAdditionalTableLength(resultValue.ToString());

                    // Set the length of bitmap 63 and append it with bitmap 63
                    resultValue = resultValue.Insert(0, bit63length);

                    // Add the bit 63 and realated tables
                    objElements.Add(CnBitmapDefinition.FDPrivateUsageData63, resultValue.ToString());

                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetAdditionalDataForKeyUpdate() FDPrivateUsageData63:{0};", resultValue.ToString()));

                    // Set Bit63 data in objTransactionEntities for saving it in Database.
                    objTransactionEntities.FDPrivateUsageData63 = resultValue.ToString();
                }
                else
                {
                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetAdditionalDataForKeyUpdate() ResultValue = <<NULL>>;"));
                }

                return objElements;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal("CnTransactionMessage.SetAdditionalDataForKeyUpdate();", ex);

                throw;
            }
        }

        /// <summary>
        /// Purpose             :   Prepare bitmap fields for cap key download
        /// Function Name       :   PrepareFieldsForCapKeyDownload
        /// Created By          :   Naveen Kumar
        /// Created On          :   19/12/2016
        /// </summary>
        /// <returns>CnTransactionEntities</returns>
        public static CnTransactionEntities PrepareFieldsForCapKeyDownload(TransactionRequest transactionRequest)
        {
            return new CnTransactionEntities
            {
                IndustryType = (IndustryType)Enum.Parse(typeof(IndustryType), transactionRequest.IndustryType),
                ProcessingCode = SigmaStaticValue.ProcessingCodeForCreditSale,
                SystemTrace = transactionRequest.Stan,
                NetworkInternationId = SigmaStaticValue.NetworkInternationId,
                TerminalId = transactionRequest.TerminalId.PadLeft(8, '0'),
                MerchantId = transactionRequest.FdMerchantId.PadLeft(15, '0'),
                IsTORTransaction = transactionRequest.IsTORTransaction
            };
        }

        /// <summary>
        /// Purpose             :   Prepare additional data for cap key
        /// Function Name       :   SetAdditionalDataForCapKey
        /// Created By          :   Naveen Kumar
        /// Created On          :   19/012/2016
        /// </summary>
        /// <returns>string</returns>
        private Dictionary<Element, string> SetAdditionalDataForCapKey(MessageType objMessageType, Dictionary<Element, string> objElements, CnTransactionEntities objTransactionEntities, int i)
        {
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetAdditionalDataForCapKey() objMessageType:{0};", objMessageType));

            StringBuilder resultValue = new StringBuilder();
            try
            {
                if (i < 1)
                {
                    var objTableF1 = new TableF1();
                    string tableF1Values = SetTableProperties(objTableF1);
                    resultValue.Append(tableF1Values);
                }

                var tableFB = new TableFB();
                var seq = Convert.ToString(i + 0001);
                var seqblock = seq.PadLeft(4, SigmaStaticValue.Zero);
                tableFB.ReqFile_Block = seqblock;

                var re = Convert.ToString(i * 256);
                var ReqFileOffset = re.PadLeft(7, SigmaStaticValue.Zero);
                tableFB.Req_FileOffset = ReqFileOffset;

                string tableFBValues = SetTableProperties(tableFB);
                resultValue.Append(tableFBValues);

                if (!string.IsNullOrEmpty(resultValue.ToString()))
                {
                    var bit63length = SetAdditionalTableLength(resultValue.ToString());
                    // Set the length of bitmap 63 and append it with bitmap 63
                    resultValue = resultValue.Insert(0, bit63length);

                    // Add the bit 63 and realated tables
                    objElements.Add(CnBitmapDefinition.FDPrivateUsageData63, resultValue.ToString());

                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetAdditionalDataForCapKey() FDPrivateUsageData63:{0};", resultValue.ToString()));

                    // Set Bit63 data in objTransactionEntities for saving it in Database.
                    objTransactionEntities.FDPrivateUsageData63 = resultValue.ToString();
                }
                else
                {
                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetAdditionalDataForCapKey() ResultValue = <<NULL>>;"));
                }

                return objElements;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal("CnTransactionMessage.SetAdditionalDataForCapKey();", ex);

                throw;
            }
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Purpose             :   To create payload in iso8583 message format
        /// Function Name       :   CreateEMVMessage
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   07/27/2016
        /// Modification Made   :   ****************************
        /// Modified On       :    "MM/DD/YYYY" 
        /// </summary>
        /// <param name="objTransactionEntities"></param>
        /// <returns></returns>
        public string CreateIccMessage(CnTransactionEntities objTransactionEntities, TransactionRequest tr)
        {
            try
            {
                string messageID = string.Empty;
                string dataElements = string.Empty;

                //PAY-13 Set the IsTransArmor flag on the obj, as we should know what the setting is from the TR.
                objTransactionEntities.IsTransArmor = tr.IsTransArmor;

                //Chetu 357
                messageID = FormatFields.FormatMessageType(objTransactionEntities.TransType.ToString(), objTransactionEntities.CardType.ToString(), objTransactionEntities.IsTORTransaction);

                MessageType type = (MessageType)Enum.Parse(typeof(MessageType), objTransactionEntities.CardType.ToString() + objTransactionEntities.TransType.ToString());

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.CreateIccMessage() messageID:{0};dataElements:{1};type:{2};IsTOR:{3};IsTA:{4};transType:{5};EntryMode:{6};", 
                            messageID, dataElements, type, objTransactionEntities.IsTORTransaction, objTransactionEntities.IsTransArmor, objTransactionEntities.TransType, tr.EntryMode));

                if (objTransactionEntities.TransType != TransactionType.Void.ToString() && objTransactionEntities.IsReversalTransaction)
                {
                    if (tr.EntryMode == EntryMode.EmvContact.ToString())
                        type = MessageType.CreditEMVReversal;
                    else if (tr.EntryMode == EntryMode.FSwiped.ToString())
                        type = MessageType.CreditFallbackReversal;
                }
                else if (tr.EntryMode == EntryMode.FSwiped.ToString() && objTransactionEntities.TransType == TransactionType.Sale.ToString())
                {
                    type = MessageType.CreditFallbackSale;
                }

                dataElements = GetMessage(type, objTransactionEntities, 0, tr.EntryMode); //pass payment type with transaction type like CreditSale

                if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                    _logger.LogInfoMessage(string.Format("CnTransactionMessage.CreateIccMessage() messageID:{0};dataElements:{1};", messageID, dataElements));

                return string.Concat(messageID, dataElements);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal("CnTransactionMessage.CreateIccMessage();", ex);

                throw;
            }
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Purpose             :   Pass transaction data in CnTransactionEnities class 
        /// Function Name       :   MakeEMVTransRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   06/06/2016
        /// Modification Made   :   ****************************
        /// Modified On       :    "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>       
        public static CnTransactionEntities MakeEMVTransRequest(TransactionRequest transactionRequest)
        {
            ILogger logger = new Logger();

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                logger.LogInfoMessage(string.Format("CnTransactionMessage.MakeEMVTransRequest() paymentType:{0};cardType:{1};industryType:{2};txnType:{3};", transactionRequest.PaymentType, transactionRequest.CardType, transactionRequest.IndustryType, transactionRequest.TransactionType));

            return new CnTransactionEntities
            {
                CardType = (CardType)Enum.Parse(typeof(CardType), transactionRequest.PaymentType.ToString()),
                TransType = transactionRequest.TransactionType,
                CardIssuer = (CardIssuer)Enum.Parse(typeof(CardIssuer), transactionRequest.CardType.ToString()),
                IndustryType = (IndustryType)Enum.Parse(typeof(IndustryType), transactionRequest.IndustryType),
                ProcessingCode = FormatFields.PrepareProcessingCode(transactionRequest.PaymentType.ToString(), transactionRequest.TransactionType.ToString(), transactionRequest.EntryMode.ToString()),//mandatory
                AmountOfTransaction = transactionRequest.TransactionAmount,
                TransmissionDateTime = FormatFields.FormatTransDateTime(transactionRequest.TrnmsnDateTime),
                SystemTrace = transactionRequest.Stan,
                TimeLocalTransmission = FormatFields.FormatLocalTime(transactionRequest.LocalDateTime),
                DateLocalTrans = FormatFields.FormatLocalDate(transactionRequest.LocalDateTime),
                MerchantCategoryCode = SigmaStaticValue.MCCForRetail,
                NetworkInternationId = SigmaStaticValue.NetworkInternationId,
                PrimaryAccountNumber = transactionRequest.TransactionType.ToString() == TxnTypeType.Completion.ToString() ? transactionRequest.CardNumber : null,

                PosEntryModePinCapability = transactionRequest.PaymentType.ToString() == CardType.Credit.ToString() ? (transactionRequest.EntryMode == EntryMode.EmvContact.ToString() ? SigmaStaticValue.POSEntryModePinCapabilityForChipNPin : SigmaStaticValue.POSEntryModePinCapabilityForEMVContactless) : SigmaStaticValue.POSEntryModePinCapabilityForSwipedWithPin,

                PosConditionCode = transactionRequest.PaymentType.ToString() == CardType.Credit.ToString() ? SigmaStaticValue.PosConditionCodeZeroZero :
                FormatFields.PreparePosConditionCode(transactionRequest.PaymentType.ToString(), transactionRequest.CardType.ToString(), transactionRequest.PinData),//Mandatory (Mandatory on Request, but Optional on Response)-00 

                AcquirerReference = FormatFields.PrepareAcquirerRef(transactionRequest.TransactionType, transactionRequest.SourceTransaction),

                RetrievalRefNumber = transactionRequest.TransRefNo,
                TerminalId = transactionRequest.TerminalId.PadLeft(8, '0'),
                MerchantId = transactionRequest.FdMerchantId.PadLeft(15, '0'),
                MerchantZipCode = FormatFields.FormatMerchantZipCode(transactionRequest.MerchantZipCode, Convert.ToString(transactionRequest.CardType)),

                AdditionalPosData = transactionRequest.EntryMode == EntryMode.EmvContact.ToString() ? SigmaStaticValue.AdditionalPossDataForEMVContact : SigmaStaticValue.AdditionalPossDataForEMVContactless,

                TransactionCurrencyCode = SigmaStaticValue.CurrencyCode,
                AdditionalResponseData = transactionRequest.AddRespdata,
                EncryptedPinData = FormatFields.FormatEncryptedPinData(Convert.ToString(transactionRequest.PaymentType), Convert.ToString(transactionRequest.CardType), transactionRequest.DebitPin),

                Table14Response = transactionRequest.RespTable14,
                Table49Response = transactionRequest.RespTable49,
                TableVIResponse = transactionRequest.RespTableVI,
                TableSPResponse = transactionRequest.RespTableSP,
                TableDSResponse = transactionRequest.RespTableDS,
                TableMCResponse = transactionRequest.RespTableMC,
                IsTORTransaction = transactionRequest.IsTORTransaction,
                ReversalBit63 = transactionRequest.ResponseBit63,

                IsReversalTransaction = transactionRequest.IsReversal,
                AuthIdentificationResponse = FormatFields.PrepareAuthId(transactionRequest.TransactionType, transactionRequest.AuthIdentResponse),

                Track2Data = FormatFields.FormatTrack2Data(transactionRequest.Track2Data, transactionRequest.TransactionType, transactionRequest.IsTORTransaction),

                ICCData = (transactionRequest.EmvData),
                KeyId = transactionRequest.KeyId,
                KeySerialNumber = transactionRequest.KeySerialNumber,
                CardSequenceNumber = transactionRequest.CardSeq,

                //Chetu 357
                Track3Data = transactionRequest.Track3Data,
                TokenType = transactionRequest.TokenType
            };
        }

        /// <summary>
        /// Purpose       : Pass transaction data in CnTransactionEnities class from EMV Contactless
        /// Function Name : MakeEMVContactlessTransRequest    
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns>CnTransactionEntities</returns>
        public static CnTransactionEntities MakeEMVContactlessTransRequest(TransactionRequest transactionRequest)
        {
            ILogger logger = new Logger();

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                logger.LogInfoMessage(string.Format("CnTransactionMessage.MakeEMVContactlessTransRequest() paymentType:{0};cardType:{1};industryType:{2};txnType:{3};", transactionRequest.PaymentType, transactionRequest.CardType, transactionRequest.IndustryType, transactionRequest.TransactionType));

            return new CnTransactionEntities
            {
                CardType = (CardType)Enum.Parse(typeof(CardType), transactionRequest.PaymentType.ToString()),
                TransType = transactionRequest.TransactionType,
                CardIssuer = (CardIssuer)Enum.Parse(typeof(CardIssuer), transactionRequest.CardType.ToString()),
                IndustryType = (IndustryType)Enum.Parse(typeof(IndustryType), transactionRequest.IndustryType),
                ProcessingCode = FormatFields.PrepareProcessingCode(transactionRequest.PaymentType.ToString(), transactionRequest.TransactionType.ToString(), transactionRequest.EntryMode.ToString()),
                AmountOfTransaction = transactionRequest.TransactionAmount,
                TransmissionDateTime = FormatFields.FormatTransDateTime(transactionRequest.TrnmsnDateTime),
                SystemTrace = transactionRequest.Stan,
                TimeLocalTransmission = FormatFields.FormatLocalTime(transactionRequest.LocalDateTime),
                DateLocalTrans = FormatFields.FormatLocalDate(transactionRequest.LocalDateTime),
                MerchantCategoryCode = SigmaStaticValue.MCCForRetail,
                NetworkInternationId = SigmaStaticValue.NetworkInternationId,

                PrimaryAccountNumber = transactionRequest.CardNumber,

                PosEntryModePinCapability = SigmaStaticValue.POSEntryModePinCapabilityForEMVContactless,
                PosConditionCode = SigmaStaticValue.PosConditionCodeZeroZero,
                AcquirerReference = FormatFields.PrepareAcquirerRef(transactionRequest.TransactionType, transactionRequest.SourceTransaction),
                RetrievalRefNumber = transactionRequest.TransRefNo,
                TerminalId = transactionRequest.TerminalId.PadLeft(8, '0'),
                MerchantId = transactionRequest.FdMerchantId.PadLeft(15, '0'),
                MerchantZipCode = FormatFields.FormatMerchantZipCode(transactionRequest.MerchantZipCode, Convert.ToString(transactionRequest.CardType)),
                AdditionalPosData = SigmaStaticValue.AdditionalPossDataForEMVContactless,
                TransactionCurrencyCode = SigmaStaticValue.CurrencyCode,
                AdditionalResponseData = transactionRequest.AddRespdata,
                EncryptedPinData = FormatFields.FormatEncryptedPinData(Convert.ToString(transactionRequest.PaymentType), Convert.ToString(transactionRequest.CardType), transactionRequest.DebitPin),
                Table14Response = transactionRequest.RespTable14,
                Table49Response = transactionRequest.RespTable49,
                TableVIResponse = transactionRequest.RespTableVI,
                TableSPResponse = transactionRequest.RespTableSP,
                TableDSResponse = transactionRequest.RespTableDS,
                TableMCResponse = transactionRequest.RespTableMC,
                IsTORTransaction = transactionRequest.IsTORTransaction,
                ReversalBit63 = transactionRequest.ResponseBit63,
                IsReversalTransaction = transactionRequest.IsReversal,
                AuthIdentificationResponse = FormatFields.PrepareAuthId(transactionRequest.TransactionType, transactionRequest.AuthIdentResponse),

                Track2Data = FormatFields.FormatTrack2Data(transactionRequest.Track2Data, transactionRequest.TransactionType, transactionRequest.IsTORTransaction),

                ICCData = transactionRequest.EmvData,
                KeyId = transactionRequest.KeyId,
                KeySerialNumber = transactionRequest.KeySerialNumber,
                CardSequenceNumber = transactionRequest.CardSeq,
                TokenType = transactionRequest.TokenType,
                EntryMode = transactionRequest.EntryMode,
                TerminalLaneNo = transactionRequest.TerminalLaneNo,

                //Chetu 357
                Track3Data = transactionRequest.Track3Data
            };
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Purpose             :   Create Sale txn.
        /// Function Name       :   SetEmvAdditionalData
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   06/06/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ************************** 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private Dictionary<Element, string> SetEmvAdditionalData(MessageType objMessageType, Dictionary<Element, string> objElements, CnTransactionEntities objTransactionEntities)
        {
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
            {
                _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEmvAdditionalData() objMessageType:{0};CardType:{1};CardIssuer:{2};", objMessageType, objTransactionEntities.CardType, objTransactionEntities.CardIssuer));
                _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEmvAdditionalData() ReversalBit63:{0};", objTransactionEntities.ReversalBit63));
            }

            StringBuilder resultValue = new StringBuilder();
            try
            {
                //Process card type Credit and Debit
                switch (objTransactionEntities.CardType)
                {
                    case CardType.Credit:
                        switch (objMessageType)
                        {
                            // Credit Sale
                            case MessageType.CreditSale:
                            case MessageType.CreditAuthorization:
                            case MessageType.CreditCompletion:
                                if (objTransactionEntities.EntryMode == EntryMode.EmvContactless.ToString())
                                    SetEmvContactlessAdditionalDataForCreditSale(resultValue, objTransactionEntities);
                                else
                                    SetEmvAdditionalDataForCreditSale(resultValue, objTransactionEntities);
                                break;          
                                                  
                            case MessageType.CreditFallbackSale:
                                SetEmvAdditionalDataForCreditSale(resultValue, objTransactionEntities);
                                break;

                            // Credit Full and Partial Reversal
                            case MessageType.CreditPartialReversal:
                            case MessageType.CreditVoid:
                                SetEmvAdditionalDataForCreditReversalVoid(resultValue, objTransactionEntities);
                                break;

                            case MessageType.CreditEMVReversal:     //PAY-13 Adding handling of EMV reversal here as well, as it was never being touched previously.
                                SetEmvAdditionalDataForCreditReversalVoid(resultValue, objTransactionEntities);
                                break;

                            default:
                                break;
                        }

                        break;

                    case CardType.Debit:
                        switch (objMessageType)
                        {
                            // Debit Sale
                            case MessageType.DebitSale:
                            case MessageType.DebitAuthorization:
                            case MessageType.DebitCompletion:
                            case MessageType.DebitRefund:
                                if (objTransactionEntities.EntryMode == EntryMode.EmvContactless.ToString())
                                    SetEmvAdditionalDataForInteracContactlessSale(resultValue, objTransactionEntities);
                                else
                                    SetEmvAdditionalDataForInteracSale(resultValue, objTransactionEntities);
                                break;

                            // Credit Full and Partial Reversal
                            case MessageType.DebitReversal:
                            case MessageType.DebitVoid:
                                SetEmvAdditionalDataForInteracReversalVoid(resultValue, objTransactionEntities);
                                break;

                            case MessageType.CreditEMVReversal:     //PAY-13 Adding handling of EMV reversal here as well, as it was never being touched previously.
                                SetEmvAdditionalDataForInteracSale(resultValue, objTransactionEntities);
                                break;

                            default:
                                break;
                        }

                        break;

                    default:
                        break;
                }

                if (!string.IsNullOrEmpty(resultValue.ToString()))
                {
                    var bit63length = SetAdditionalTableLength(resultValue.ToString());

                    // Set the length of bitmap 63 and append it with bitmap 63
                    resultValue = resultValue.Insert(0, bit63length);

                    // Add the bit 63 and realated tables
                    objElements.Add(CnBitmapDefinition.FDPrivateUsageData63, resultValue.ToString());

                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEmvAdditionalData() FDPrivateUsageData63:{0};", resultValue.ToString()));

                    // Set Bit63 data in objTransactionEntities for saving it in Database.
                    objTransactionEntities.FDPrivateUsageData63 = resultValue.ToString();
                }
                else
                {
                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEmvAdditionalData() ResultValue = <<NULL>>;"));
                }

                return objElements;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(string.Format("CnTransactionMessage.SetEmvAdditionalData() Ex:{0};", ex.ToString()), ex);
                throw;
            }
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Purpose             :   Prepare bitmap fields for EMV credit sale transactions
        /// Function Name       :   SetEmvAdditionalDataForCreditSale
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   07/8/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ************************** 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private void SetEmvAdditionalDataForCreditSale(StringBuilder resultValue, CnTransactionEntities objTransactionEntities)
        {
            try
            {
                string authorizedAmount = FormatFields.FormatAmount(Convert.ToDouble(objTransactionEntities.AmountOfTransaction));

                switch (objTransactionEntities.CardIssuer)
                {
                    // Set additional data for MasterCard Issuer
                    case CardIssuer.MasterCard:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            var strBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                            strBuilder.Remove(56, 12);
                            strBuilder.Insert(56, authorizedAmount);
                            objTransactionEntities.Table14Response = strBuilder.ToString();
                            resultValue.Append(objTransactionEntities.Table14Response);
                        }
                        else
                        {
                            var objMCTable14 = new Table14AdditionalMasterData();
                            string valueMCTable14 = SetTableProperties(objMCTable14);
                            resultValue.Append(valueMCTable14);
                        }

                        // Table68
                        var objMCTable68 = new Table68();
                        string valueMCTable68 = SetTableProperties(objMCTable68);
                        resultValue.Append(valueMCTable68);

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objMCTable69 = new Table69();
                            string valueMCTable69 = SetTableProperties(objMCTable69);
                            resultValue.Append(valueMCTable69);
                        }

                        // TableMC
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                resultValue.Append(objTransactionEntities.TableMCResponse);
                            }
                            else
                            {
                                var objTablesMC = new TableMC();
                                string valueTableMC = SetTableProperties(objTablesMC);
                                resultValue.Append(valueTableMC);
                            }
                        }

                        //Chetu 357
                        if (objTransactionEntities.IsTransArmor)
                        {
                            // Transarmor
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }
                                tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objMCTableSp = new TableSP_Retail();

                                // Better handling of NULL vales for these properties.
                                objMCTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                                objMCTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));

                                objMCTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objMCTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueMCTableSp = SetTableProperties(objMCTableSp);
                                resultValue.Append(valueMCTableSp);
                            }
                        }

                        // TableEV
                        if (objTransactionEntities.EntryMode != EntryMode.FSwiped.ToString())
                        {
                            var objMCTableEV = new TableEV();
                            string valueMCTableEV = SetTableProperties(objMCTableEV);
                            resultValue.Append(valueMCTableEV);
                        }

                        // TableSD
                        var objMCTableSD = new TableSD();
                        string valueMCTableSD = SetTableProperties(objMCTableSD);
                        resultValue.Append(valueMCTableSD);

                        break;

                    // Set additional data for Visa Issuer
                    case CardIssuer.Visa:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                            aStringBuilder.Remove(32, 12);
                            aStringBuilder.Insert(32, authorizedAmount);
                            aStringBuilder.Remove(44, 12);
                            aStringBuilder.Insert(44, authorizedAmount);
                            objTransactionEntities.Table14Response = aStringBuilder.ToString();
                            resultValue.Append(objTransactionEntities.Table14Response);
                        }
                        else
                        {
                            var objVisaTable14 = new Table14AdditionalVisaData();
                            string valueVisaTable14 = SetTableProperties(objVisaTable14);
                            resultValue.Append(valueVisaTable14);
                        }

                        // Table68
                        var objVITable68 = new Table68();
                        string valueVITable68 = SetTableProperties(objVITable68);
                        resultValue.Append(valueVITable68);

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objVisaTable69 = new Table69();
                            string valueVisaTable69 = SetTableProperties(objVisaTable69);
                            resultValue.Append(valueVisaTable69);
                        }

                        // TableVI
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                resultValue.Append(objTransactionEntities.TableVIResponse);
                            }
                            else
                            {
                                var objVisaTableVI = new TableVI();
                                string valueVisaTableVI = SetTableProperties(objVisaTableVI);
                                resultValue.Append(valueVisaTableVI);
                            }
                        }

                        //Chetu 357
                        if (objTransactionEntities.IsTransArmor)
                        {
                            // Transarmor
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }
                                tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objVITableSp = new TableSP_Retail();
                                objVITableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                                objVITableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));
                                objVITableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objVITableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueVITableSp = SetTableProperties(objVITableSp);
                                resultValue.Append(valueVITableSp);
                            }
                        }

                        // TableEV
                        if (objTransactionEntities.EntryMode != EntryMode.FSwiped.ToString())
                        {
                            var objVITableEV = new TableEV();
                            string valueVITableEV = SetTableProperties(objVITableEV);
                            resultValue.Append(valueVITableEV);
                        }

                        // TableSD
                        var objVITableSD = new TableSD();
                        string valueVITableSD = SetTableProperties(objVITableSD);
                        resultValue.Append(valueVITableSD);

                        break;

                    // Set additional data for American Express Issuer
                    case CardIssuer.Amex:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            resultValue.Append(objTransactionEntities.Table14Response);
                        }
                        else
                        {
                            var objAmexTable14 = new Table14AMEXData();
                            string valueAmexTable14 = SetTableProperties(objAmexTable14);
                            resultValue.Append(valueAmexTable14);
                        }

                        // Table68
                        var objAmexTable68 = new Table68();
                        string valueAmexTable68 = SetTableProperties(objAmexTable68);
                        resultValue.Append(valueAmexTable68);

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objAmexTable69 = new Table69();
                            string valueAmexTable69 = SetTableProperties(objAmexTable69);
                            resultValue.Append(valueAmexTable69);
                        }

                        //Chetu 357
                        if (objTransactionEntities.IsTransArmor)
                        {
                            // Transarmor
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }
                                tableValues = tableValues + PaymentAPIResources.TableSP07015 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objAmexTableSp = new TableSP_Retail();
                                objAmexTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                                objAmexTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));
                                objAmexTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objAmexTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueAmexTableSp = SetTableProperties(objAmexTableSp);
                                resultValue.Append(valueAmexTableSp);
                            }
                        }

                        // TableEV
                        if (objTransactionEntities.EntryMode != EntryMode.FSwiped.ToString())
                        {
                            var objAmexTableEV = new TableEV();
                            string valueAmexTableEV = SetTableProperties(objAmexTableEV);
                            resultValue.Append(valueAmexTableEV);
                        }

                        // TableSD
                        var objAmexTableSD = new TableSD();
                        string valueAmexTableSD = SetTableProperties(objAmexTableSD);
                        resultValue.Append(valueAmexTableSD);

                        break;

                    // Set additional data for Discover
                    case CardIssuer.Discover:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            if (objTransactionEntities.CardIssuer.ToString() == CardTypeType.Discover.ToString() || objTransactionEntities.CardIssuer.ToString() == CardTypeType.Diners.ToString())
                            {
                                var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                                aStringBuilder.Remove(52, 12);
                                aStringBuilder.Insert(52, authorizedAmount);
                                objTransactionEntities.Table14Response = aStringBuilder.ToString();
                                resultValue.Append(objTransactionEntities.Table14Response);
                            }
                            else
                            {
                                var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                                aStringBuilder.Remove(82, 12);
                                aStringBuilder.Insert(82, authorizedAmount);
                                objTransactionEntities.Table14Response = aStringBuilder.ToString();
                                resultValue.Append(objTransactionEntities.Table14Response);
                            }
                        }
                        else
                        {
                            var objDiscoverTable14 = new Table14AdditionalDiscoverData();
                            string valueDiscoverTable14 = SetTableProperties(objDiscoverTable14);
                            resultValue.Append(valueDiscoverTable14);
                        }

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objDiscoverTable69 = new Table69();
                            string valueDiscoverTable69 = SetTableProperties(objDiscoverTable69);
                            resultValue.Append(valueDiscoverTable69);
                        }

                        // TableDS
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString() && objTransactionEntities.CardIssuer.ToString() != CardTypeType.JCB.ToString())
                            {
                                resultValue.Append(objTransactionEntities.TableDSResponse);
                            }
                            else if (objTransactionEntities.TransType != TransactionType.Completion.ToString())
                            {
                                var objTableDS = new TableDS();
                                string valueTableDS = SetTableProperties(objTableDS);
                                resultValue.Append(valueTableDS);
                            }
                        }

                        //Chetu 357
                        if (objTransactionEntities.IsTransArmor)
                        {
                            // Transarmor
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }

                                if (objTransactionEntities.CardIssuer.ToString() == CardIssuer.Diners.ToString())
                                    tableValues = tableValues + PaymentAPIResources.TableSP07014 + objTransactionEntities.TableSPResponse;
                                else
                                    tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;

                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objDSTableSp = new TableSP_Retail();
                                objDSTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                                objDSTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));
                                objDSTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objDSTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueDSTableSp = SetTableProperties(objDSTableSp);
                                resultValue.Append(valueDSTableSp);
                            }
                        }

                        // TableEV
                        if (objTransactionEntities.EntryMode != EntryMode.FSwiped.ToString())
                        {
                            var objDSTableEV = new TableEV();
                            string valueDSTableEV = SetTableProperties(objDSTableEV);
                            resultValue.Append(valueDSTableEV);
                        }

                        // TableSD
                        var objDSTableSD = new TableSD();
                        string valueDSTableSD = SetTableProperties(objDSTableSD);
                        resultValue.Append(valueDSTableSD);

                        break;

                    // Set additional data for Diners and JCB
                    case CardIssuer.Diners:
                    case CardIssuer.JCB:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            if (objTransactionEntities.CardIssuer.ToString() == CardTypeType.Discover.ToString() || objTransactionEntities.CardIssuer.ToString() == CardTypeType.Diners.ToString())
                            {
                                var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                                aStringBuilder.Remove(52, 12);
                                aStringBuilder.Insert(52, authorizedAmount);
                                objTransactionEntities.Table14Response = aStringBuilder.ToString();
                                resultValue.Append(objTransactionEntities.Table14Response);
                            }
                            else
                            {
                                var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                                aStringBuilder.Remove(82, 12);
                                aStringBuilder.Insert(82, authorizedAmount);
                                objTransactionEntities.Table14Response = aStringBuilder.ToString();
                                resultValue.Append(objTransactionEntities.Table14Response);
                            }
                        }
                        else
                        {
                            var objDiscoverTable14 = new Table14AdditionalDiscoverData();
                            string valueDiscoverTable14 = SetTableProperties(objDiscoverTable14);
                            resultValue.Append(valueDiscoverTable14);
                        }

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objDiscoverTable69 = new Table69();
                            string valueDiscoverTable69 = SetTableProperties(objDiscoverTable69);
                            resultValue.Append(valueDiscoverTable69);
                        }

                        //Chetu 357
                        if (objTransactionEntities.IsTransArmor)
                        {
                            // Transarmor
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }

                                if (objTransactionEntities.CardIssuer.ToString() == CardIssuer.Diners.ToString())
                                    tableValues = tableValues + PaymentAPIResources.TableSP07014 + objTransactionEntities.TableSPResponse;
                                else
                                    tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;

                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objDSTableSp = new TableSP_Retail();
                                objDSTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                                objDSTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));
                                objDSTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objDSTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueDSTableSp = SetTableProperties(objDSTableSp);
                                resultValue.Append(valueDSTableSp);
                            }
                        }

                        // TableEV
                        if (objTransactionEntities.EntryMode != EntryMode.FSwiped.ToString())
                        {
                            var objDiscoverTableEV = new TableEV();
                            string valueDiscoverTableEV = SetTableProperties(objDiscoverTableEV);
                            resultValue.Append(valueDiscoverTableEV);
                        }

                        // TableSD
                        var objDnTableSD = new TableSD();
                        string valueDnTableSD = SetTableProperties(objDnTableSD);
                        resultValue.Append(valueDnTableSD);

                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(string.Format("CnTransactionMessage.SetEmvAdditionalDataForCreditSale() Ex:{0};", ex.ToString()), ex);
                throw;
            }
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Purpose             :   Create reversal txn.
        /// Function Name       :   SetEmvAdditionalDataForCreditReversalVoid
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   07/01/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ************************** 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private void SetEmvAdditionalDataForCreditReversalVoid(StringBuilder resultValue, CnTransactionEntities objTransactionEntities)
        {
            ILogger logger = new Logger();

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
            {
                logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEmvAdditionalDataForCreditReversalVoid() EntryMode:{0};IsTA:{1};IsTOR:{2};", objTransactionEntities.EntryMode, objTransactionEntities.IsTransArmor, objTransactionEntities.IsTORTransaction));
                logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEmvAdditionalDataForCreditReversalVoid() ReversalBit63:{0};", objTransactionEntities.ReversalBit63));
            }

            double num = Convert.ToDouble(objTransactionEntities.MaxAmount);
            string amt = num.ToString("0.00").Replace(".", string.Empty);
            var TxnAmt = Convert.ToString(amt);
            TxnAmt = TxnAmt.PadLeft(12, '0');

            try
            {
                string authorizedAmount = FormatFields.FormatAmount(Convert.ToDouble(objTransactionEntities.AmountOfTransaction));

                switch (objTransactionEntities.CardIssuer)
                {
                    // Set additional data for MasterCard Issuer
                    case CardIssuer.MasterCard:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            var strBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                            strBuilder.Remove(56, 12);
                            strBuilder.Insert(56, authorizedAmount);
                            objTransactionEntities.Table14Response = strBuilder.ToString();
                            resultValue.Append(objTransactionEntities.Table14Response);
                        }
                        else
                        {
                            var objMCTable14 = new Table14AdditionalMasterData();
                            string valueMCTable14 = SetTableProperties(objMCTable14);
                            resultValue.Append(valueMCTable14);
                        }

                        // Table68
                        var objMCTable68 = new Table68();
                        string valueMCTable68 = SetTableProperties(objMCTable68);
                        resultValue.Append(valueMCTable68);

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objMCTable69 = new Table69();
                            string valueMCTable69 = SetTableProperties(objMCTable69);
                            resultValue.Append(valueMCTable69);
                        }

                        // TableMC
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                resultValue.Append(objTransactionEntities.TableMCResponse);
                            }
                            else
                            {
                                var objTablesMC = new TableMC();
                                string valueTableMC = SetTableProperties(objTablesMC);
                                resultValue.Append(valueTableMC);
                            }
                        }

                        //Chetu 357
                        if (objTransactionEntities.IsTransArmor)
                        {
                            // Transarmor
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }
                                tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objMCTableSp = new TableSP_Retail();

                                // Better handling of NULL vales for these properties.
                                objMCTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                                objMCTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));

                                objMCTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objMCTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueMCTableSp = SetTableProperties(objMCTableSp);
                                resultValue.Append(valueMCTableSp);
                            }
                        }

                        // TableEV
                        if (objTransactionEntities.EntryMode != EntryMode.FSwiped.ToString())
                        {
                            var objMCTableEV = new TableEV();
                            string valueMCTableEV = SetTableProperties(objMCTableEV);
                            resultValue.Append(valueMCTableEV);
                        }

                        // TableSD
                        var objMCTableSD = new TableSD();
                        string valueMCTableSD = SetTableProperties(objMCTableSD);
                        resultValue.Append(valueMCTableSD);

                        break;

                    // Set additional data for Visa Issuer
                    case CardIssuer.Visa:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                            aStringBuilder.Remove(32, 12);
                            aStringBuilder.Insert(32, authorizedAmount);
                            aStringBuilder.Remove(44, 12);
                            aStringBuilder.Insert(44, authorizedAmount);
                            objTransactionEntities.Table14Response = aStringBuilder.ToString();
                            resultValue.Append(objTransactionEntities.Table14Response);
                        }
                        else
                        {
                            var objVisaTable14 = new Table14AdditionalVisaData();
                            string valueVisaTable14 = SetTableProperties(objVisaTable14);
                            resultValue.Append(valueVisaTable14);
                        }

                        // Table68
                        var objVITable68 = new Table68();
                        string valueVITable68 = SetTableProperties(objVITable68);
                        resultValue.Append(valueVITable68);

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objVisaTable69 = new Table69();
                            string valueVisaTable69 = SetTableProperties(objVisaTable69);
                            resultValue.Append(valueVisaTable69);
                        }

                        // TableVI
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                resultValue.Append(objTransactionEntities.TableVIResponse);
                            }
                            else
                            {
                                var objVisaTableVI = new TableVI();
                                string valueVisaTableVI = SetTableProperties(objVisaTableVI);
                                resultValue.Append(valueVisaTableVI);
                            }
                        }

                        //Chetu 357
                        if (objTransactionEntities.IsTransArmor)
                        {
                            // Transarmor
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }
                                tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objVITableSp = new TableSP_Retail();
                                objVITableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                                objVITableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));
                                objVITableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objVITableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueVITableSp = SetTableProperties(objVITableSp);
                                resultValue.Append(valueVITableSp);
                            }
                        }

                        // TableEV
                        if (objTransactionEntities.EntryMode != EntryMode.FSwiped.ToString())
                        {
                            var objVITableEV = new TableEV();
                            string valueVITableEV = SetTableProperties(objVITableEV);
                            resultValue.Append(valueVITableEV);
                        }

                        // TableSD
                        var objVITableSD = new TableSD();
                        string valueVITableSD = SetTableProperties(objVITableSD);
                        resultValue.Append(valueVITableSD);

                        break;

                    // Set additional data for American Express Issuer
                    case CardIssuer.Amex:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            resultValue.Append(objTransactionEntities.Table14Response);
                        }
                        else
                        {
                            var objAmexTable14 = new Table14AMEXData();
                            string valueAmexTable14 = SetTableProperties(objAmexTable14);
                            resultValue.Append(valueAmexTable14);
                        }

                        // Table68
                        var objAmexTable68 = new Table68();
                        string valueAmexTable68 = SetTableProperties(objAmexTable68);
                        resultValue.Append(valueAmexTable68);

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objAmexTable69 = new Table69();
                            string valueAmexTable69 = SetTableProperties(objAmexTable69);
                            resultValue.Append(valueAmexTable69);
                        }

                        //Chetu 357
                        if (objTransactionEntities.IsTransArmor)
                        {
                            // Transarmor
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }
                                tableValues = tableValues + PaymentAPIResources.TableSP07015 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objAmexTableSp = new TableSP_Retail();
                                objAmexTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                                objAmexTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));
                                objAmexTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objAmexTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueAmexTableSp = SetTableProperties(objAmexTableSp);
                                resultValue.Append(valueAmexTableSp);
                            }
                        }

                        // TableEV
                        if (objTransactionEntities.EntryMode != EntryMode.FSwiped.ToString())
                        {
                            var objAmexTableEV = new TableEV();
                            string valueAmexTableEV = SetTableProperties(objAmexTableEV);
                            resultValue.Append(valueAmexTableEV);
                        }

                        // TableSD
                        var objAmexTableSD = new TableSD();
                        string valueAmexTableSD = SetTableProperties(objAmexTableSD);
                        resultValue.Append(valueAmexTableSD);

                        break;

                    // Set additional data for Discover
                    case CardIssuer.Discover:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            if (objTransactionEntities.CardIssuer.ToString() == CardTypeType.Discover.ToString() || objTransactionEntities.CardIssuer.ToString() == CardTypeType.Diners.ToString())
                            {
                                var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                                aStringBuilder.Remove(52, 12);
                                aStringBuilder.Insert(52, authorizedAmount);
                                objTransactionEntities.Table14Response = aStringBuilder.ToString();
                                resultValue.Append(objTransactionEntities.Table14Response);
                            }
                            else
                            {
                                var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                                aStringBuilder.Remove(82, 12);
                                aStringBuilder.Insert(82, authorizedAmount);
                                objTransactionEntities.Table14Response = aStringBuilder.ToString();
                                resultValue.Append(objTransactionEntities.Table14Response);
                            }
                        }
                        else
                        {
                            var objDiscoverTable14 = new Table14AdditionalDiscoverData();
                            string valueDiscoverTable14 = SetTableProperties(objDiscoverTable14);
                            resultValue.Append(valueDiscoverTable14);
                        }

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objDiscoverTable69 = new Table69();
                            string valueDiscoverTable69 = SetTableProperties(objDiscoverTable69);
                            resultValue.Append(valueDiscoverTable69);
                        }

                        // TableDS
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString() && objTransactionEntities.CardIssuer.ToString() != CardTypeType.JCB.ToString())
                            {
                                resultValue.Append(objTransactionEntities.TableDSResponse);
                            }
                            else if (objTransactionEntities.TransType != TransactionType.Completion.ToString())
                            {
                                var objTableDS = new TableDS();
                                string valueTableDS = SetTableProperties(objTableDS);
                                resultValue.Append(valueTableDS);
                            }
                        }

                        //Chetu 357
                        if (objTransactionEntities.IsTransArmor)
                        {
                            // Transarmor
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }

                                if (objTransactionEntities.CardIssuer.ToString() == CardIssuer.Diners.ToString())
                                    tableValues = tableValues + PaymentAPIResources.TableSP07014 + objTransactionEntities.TableSPResponse;
                                else
                                    tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;

                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objDSTableSp = new TableSP_Retail();
                                objDSTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                                objDSTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));
                                objDSTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objDSTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueDSTableSp = SetTableProperties(objDSTableSp);
                                resultValue.Append(valueDSTableSp);
                            }
                        }

                        // TableEV
                        if (objTransactionEntities.EntryMode != EntryMode.FSwiped.ToString())
                        {
                            var objDSTableEV = new TableEV();
                            string valueDSTableEV = SetTableProperties(objDSTableEV);
                            resultValue.Append(valueDSTableEV);
                        }

                        // TableSD
                        var objDSTableSD = new TableSD();
                        string valueDSTableSD = SetTableProperties(objDSTableSD);
                        resultValue.Append(valueDSTableSD);

                        break;

                    // Set additional data for Diners and JCB
                    case CardIssuer.Diners:
                    case CardIssuer.JCB:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            if (objTransactionEntities.CardIssuer.ToString() == CardTypeType.Discover.ToString() || objTransactionEntities.CardIssuer.ToString() == CardTypeType.Diners.ToString())
                            {
                                var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                                aStringBuilder.Remove(52, 12);
                                aStringBuilder.Insert(52, authorizedAmount);
                                objTransactionEntities.Table14Response = aStringBuilder.ToString();
                                resultValue.Append(objTransactionEntities.Table14Response);
                            }
                            else
                            {
                                var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                                aStringBuilder.Remove(82, 12);
                                aStringBuilder.Insert(82, authorizedAmount);
                                objTransactionEntities.Table14Response = aStringBuilder.ToString();
                                resultValue.Append(objTransactionEntities.Table14Response);
                            }
                        }
                        else
                        {
                            var objDiscoverTable14 = new Table14AdditionalDiscoverData();
                            string valueDiscoverTable14 = SetTableProperties(objDiscoverTable14);
                            resultValue.Append(valueDiscoverTable14);
                        }

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objDiscoverTable69 = new Table69();
                            string valueDiscoverTable69 = SetTableProperties(objDiscoverTable69);
                            resultValue.Append(valueDiscoverTable69);
                        }

                        //Chetu 357
                        if (objTransactionEntities.IsTransArmor)
                        {
                            // Transarmor
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }

                                if (objTransactionEntities.CardIssuer.ToString() == CardIssuer.Diners.ToString())
                                {
                                    tableValues = tableValues + PaymentAPIResources.TableSP07014 + objTransactionEntities.TableSPResponse;
                                }
                                else
                                {
                                    tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;
                                }
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objDSTableSp = new TableSP_Retail();
                                objDSTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                                objDSTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));
                                objDSTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objDSTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueDSTableSp = SetTableProperties(objDSTableSp);
                                resultValue.Append(valueDSTableSp);
                            }
                        }

                        // TableEV
                        if (objTransactionEntities.EntryMode != EntryMode.FSwiped.ToString())
                        {
                            var objDiscoverTableEV = new TableEV();
                            string valueDiscoverTableEV = SetTableProperties(objDiscoverTableEV);
                            resultValue.Append(valueDiscoverTableEV);
                        }

                        // TableSD
                        var objDnTableSD = new TableSD();
                        string valueDnTableSD = SetTableProperties(objDnTableSD);
                        resultValue.Append(valueDnTableSD);

                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(string.Format("CnTransactionMessage.SetEmvAdditionalDataForCreditReversalVoid() Ex:{0};", ex.ToString()), ex);
                throw;
            }
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Preapre bitmap fields for EMV fallback transaction request
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        public static CnTransactionEntities MakeFallbackCreditTansRequest(TransactionRequest transactionRequest)
        {
            ILogger logger = new Logger();

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                logger.LogInfoMessage(string.Format("CnTransactionMessage.MakeFallbackCreditTransRequest() paymentType:{0};cardType:{1};industryType:{2};txnType:{3};", transactionRequest.PaymentType, transactionRequest.CardType, transactionRequest.IndustryType, transactionRequest.TransactionType));

            return new CnTransactionEntities
            {
                CardType = (CardType)Enum.Parse(typeof(CardType), transactionRequest.PaymentType.ToString()),
                TransType = transactionRequest.TransactionType,
                CardIssuer = (CardIssuer)Enum.Parse(typeof(CardIssuer), transactionRequest.CardType.ToString()),
                IndustryType = (IndustryType)Enum.Parse(typeof(IndustryType), transactionRequest.IndustryType),
                ProcessingCode = FormatFields.PrepareProcessingCode(transactionRequest.PaymentType.ToString(), transactionRequest.TransactionType.ToString(), transactionRequest.EntryMode.ToString()),
                AmountOfTransaction = transactionRequest.TransactionAmount,
                TransmissionDateTime = FormatFields.FormatTransDateTime(transactionRequest.TrnmsnDateTime),
                SystemTrace = transactionRequest.Stan,
                PrimaryAccountNumber = transactionRequest.TransactionType.ToString() == TxnTypeType.Completion.ToString() ? transactionRequest.CardNumber : null,
                TimeLocalTransmission = FormatFields.FormatLocalTime(transactionRequest.LocalDateTime),
                DateLocalTrans = FormatFields.FormatLocalDate(transactionRequest.LocalDateTime),
                MerchantCategoryCode = SigmaStaticValue.MCCForRetail,
                PosEntryModePinCapability = SigmaStaticValue.POSEntryModePinCapabilityForFallback,
                NetworkInternationId = SigmaStaticValue.NetworkInternationId,
                PosConditionCode = SigmaStaticValue.PosConditionCodeZeroZero,
                AcquirerReference = FormatFields.PrepareAcquirerRef(transactionRequest.TransactionType, transactionRequest.SourceTransaction),
                AcquiringId = SigmaStaticValue.AcquiringId,
                KeyId = transactionRequest.KeyId,

                Track2Data = FormatFields.FormatTrack2Data(transactionRequest.Track2Data, transactionRequest.TransactionType, transactionRequest.IsTORTransaction),

                RetrievalRefNumber = transactionRequest.TransRefNo,
                TerminalId = transactionRequest.TerminalId.PadLeft(8, '0'),
                MerchantId = transactionRequest.FdMerchantId.PadLeft(15, '0'),
                TransactionCurrencyCode = SigmaStaticValue.CurrencyCode,
                MerchantZipCode = FormatFields.FormatMerchantZipCode(transactionRequest.MerchantZipCode, Convert.ToString(transactionRequest.CardType)),
                AdditionalPosData = SigmaStaticValue.AdditionalPossDataForRetail,
                AdditionalResponseData = transactionRequest.AddRespdata,
                EncryptedPinData = FormatFields.FormatEncryptedPinData(Convert.ToString(transactionRequest.PaymentType), Convert.ToString(transactionRequest.CardType), transactionRequest.DebitPin),
                IsReversalTransaction = transactionRequest.IsReversal,
                AuthIdentificationResponse = FormatFields.PrepareAuthId(transactionRequest.TransactionType, transactionRequest.AuthIdentResponse),
                EntryMode = transactionRequest.EntryMode,
                ServiceCode = transactionRequest.ServiceCode,

                //Chetu 357
                Track3Data = transactionRequest.Track3Data,
                KeySerialNumber = transactionRequest.KeySerialNumber,
                TokenType = transactionRequest.TokenType
            };
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Purpose             :   Create debit txn.
        /// Function Name       :   SetEmvAdditionalDataForDebitSale
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   08/25/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ************************** 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private static void SetEmvAdditionalDataForInteracSale(StringBuilder resultValue, CnTransactionEntities objTransactionEntities)
        {
            ILogger logger = new Logger();

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
            {
                logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEmvAdditionalDataForInteracSale() EntryMode:{0};IsTA:{1};IsTOR:{2};", objTransactionEntities.EntryMode, objTransactionEntities.IsTransArmor, objTransactionEntities.IsTORTransaction));
                logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEmvAdditionalDataForInteracSale() ReversalBit63:{0};", objTransactionEntities.ReversalBit63));
            }

            try
            {
                // Set additional data for Interac Card

                // Table66
                var objTable66 = new Table66();
                string valueTable66 = SetTableProperties(objTable66);
                resultValue.Append(valueTable66);

                //Table32
                var objTable32 = new Table32();
                objTable32.MAC = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.MacData));
                objTable32.MsgAuthenticate = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.CheckDigit));
                string valueTable32 = SetTableProperties(objTable32);
                resultValue.Append(valueTable32);

                // Table38
                if (objTransactionEntities.IsTORTransaction)
                {
                    var objTable38 = new Table38();
                    objTable38.ReversalReasonCode = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(SigmaStaticValue.ReversalReasonTimeout));
                    string valueTable38 = SetTableProperties(objTable38);
                    resultValue.Append(valueTable38);
                }

                //TableEV
                if (objTransactionEntities.EntryMode != EntryMode.FSwiped.ToString())
                {
                    var objTableEV = new TableEV();
                    string valueTableEV = SetTableProperties(objTableEV);
                    resultValue.Append(valueTableEV);
                }

                //Chetu 357
                if (objTransactionEntities.IsTransArmor)
                {
                    // Transarmor
                    if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                    {
                        var tableToFill = new TableSP(objTransactionEntities.TokenType);
                        var tableType = tableToFill.GetType();
                        var objProperties = tableType.GetProperties();
                        string tableValues = string.Empty;
                        foreach (var item in objProperties)
                        {
                            var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                            if (!String.IsNullOrEmpty(propertyValue))
                                tableValues += propertyValue;
                        }

                        if (objTransactionEntities.CardIssuer.ToString() == CardIssuer.Diners.ToString())
                            tableValues = tableValues + PaymentAPIResources.TableSP07014 + objTransactionEntities.TableSPResponse;
                        else
                            tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;

                        tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                        resultValue.Append(tableValues);
                    }
                    else
                    {
                        var objTableSp = new TableSP_Retail();
                        objTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                        objTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));
                        objTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                        objTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                        string valueTableSp = SetTableProperties(objTableSp);
                        resultValue.Append(valueTableSp);
                    }
                }

                //TableSD
                var objTableSD = new TableSD();
                string valueTableSD = SetTableProperties(objTableSD);
                resultValue.Append(valueTableSD);
            }
            catch (Exception ex)
            {
                logger.LogInfoFatal(string.Format("CnTransactionMessage.SetEmvAdditionalDataForInteracSale() Ex:{0};", ex.ToString()), ex);

                throw;
            }
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Pass transaction data in CnTransactionEnities class for Interac
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns>CnTransactionEntities</returns>
        public static CnTransactionEntities MakeInteracTransRequest(TransactionRequest transactionRequest)
        {
            ILogger logger = new Logger();

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                logger.LogInfoMessage(string.Format("CnTransactionMessage.MakeInteracTransRequest() paymentType:{0};cardType:{1};industryType:{2};txnType:{3};", transactionRequest.PaymentType, transactionRequest.CardType, transactionRequest.IndustryType, transactionRequest.TransactionType));

            return new CnTransactionEntities
            {
                CardType = (CardType)Enum.Parse(typeof(CardType), transactionRequest.PaymentType.ToString()),
                TransType = transactionRequest.TransactionType,
                CardIssuer = (CardIssuer)Enum.Parse(typeof(CardIssuer), transactionRequest.CardType.ToString()),
                IndustryType = (IndustryType)Enum.Parse(typeof(IndustryType), transactionRequest.IndustryType),

                ProcessingCode = FormatFields.GetInteracProcessingCode(transactionRequest.TransactionType, transactionRequest.AccountType), // value based on emv tag D1000. 0-checking, 1-saving

                AmountOfTransaction = transactionRequest.TransactionAmount,
                TransmissionDateTime = FormatFields.FormatTransDateTime(transactionRequest.TrnmsnDateTime),
                SystemTrace = transactionRequest.Stan,
                TimeLocalTransmission = FormatFields.FormatLocalTime(transactionRequest.LocalDateTime),
                DateLocalTrans = FormatFields.FormatLocalDate(transactionRequest.LocalDateTime),
                MerchantCategoryCode = SigmaStaticValue.MCCForRetail,
                NetworkInternationId = SigmaStaticValue.NetworkInternationId,
                PrimaryAccountNumber = transactionRequest.CardNumber,

                PosEntryModePinCapability = SigmaStaticValue.POSEntryModePinCapabilityForChipNPin,

                PosConditionCode = SigmaStaticValue.PosConditionCodeZeroZero,

                AcquirerReference = FormatFields.PrepareAcquirerRef(transactionRequest.TransactionType, transactionRequest.SourceTransaction),
                RetrievalRefNumber = transactionRequest.TransRefNo,
                TerminalId = transactionRequest.TerminalId.PadLeft(8, '0'),
                MerchantId = transactionRequest.FdMerchantId.PadLeft(15, '0'),
                MerchantZipCode = FormatFields.FormatMerchantZipCode(transactionRequest.MerchantZipCode, Convert.ToString(transactionRequest.CardType)),

                AdditionalPosData = transactionRequest.EntryMode == EntryMode.EmvContact.ToString() ? SigmaStaticValue.AdditionalPossDataForEMVContact : SigmaStaticValue.AdditionalPossDataForEMVContactless,

                TransactionCurrencyCode = SigmaStaticValue.CurrencyCode,
                AdditionalResponseData = transactionRequest.AddRespdata,
                EncryptedPinData = FormatFields.FormatEncryptedPinData(Convert.ToString(transactionRequest.PaymentType), Convert.ToString(transactionRequest.CardType), transactionRequest.DebitPin),

                Table14Response = transactionRequest.RespTable14,
                Table49Response = transactionRequest.RespTable49,
                TableVIResponse = transactionRequest.RespTableVI,
                TableSPResponse = transactionRequest.RespTableSP,
                TableDSResponse = transactionRequest.RespTableDS,
                TableMCResponse = transactionRequest.RespTableMC,
                IsTORTransaction = transactionRequest.IsTORTransaction,
                ReversalBit63 = transactionRequest.ResponseBit63,

                IsReversalTransaction = transactionRequest.IsReversal,
                AuthIdentificationResponse = FormatFields.PrepareAuthId(transactionRequest.TransactionType, transactionRequest.AuthIdentResponse),

                Track2Data = FormatFields.FormatTrack2Data(transactionRequest.Track2Data, transactionRequest.TransactionType, transactionRequest.IsTORTransaction),

                ICCData = transactionRequest.EmvData,
                KeyId = transactionRequest.KeyId,
                KeySerialNumber = transactionRequest.KeySerialNumber,
                CardSequenceNumber = transactionRequest.CardSeq,
                CardExpirationDate = transactionRequest.ExpiryDate.Substring(2, 4),

                //Chetu 357
                Track3Data = transactionRequest.Track3Data,
                TokenType = transactionRequest.TokenType
            };
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Set additional data for Debit key update request
        /// </summary>
        /// <param name="objElements"></param>
        /// <param name="objTransactionEntities"></param>
        /// <returns></returns>
        private Dictionary<Element, string> SetAdditionalDataForNewKeyRequest(Dictionary<Element, string> objElements, CnTransactionEntities objTransactionEntities)
        {
            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetAdditionalDataForNewKeyRequest();"));

            StringBuilder resultValue = new StringBuilder();

            try
            {
                // Set additional data for new key request
                var table34 = new Table34();
                string table34Values = SetTableProperties(table34);
                resultValue.Append(table34Values);

                if (!string.IsNullOrEmpty(resultValue.ToString()))
                {
                    var bit63length = SetAdditionalTableLength(resultValue.ToString());

                    // Set the length of bitmap 63 and append it with bitmap 63
                    resultValue = resultValue.Insert(0, bit63length);

                    // Add the bit 63 and realated tables
                    objElements.Add(CnBitmapDefinition.FDPrivateUsageData63, resultValue.ToString());

                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetAdditionalDataForNewKeyRequest() FDPrivateUsageData63:{0};", resultValue.ToString()));

                    // Set Bit63 data in objTransactionEntities for saving it in Database.
                    objTransactionEntities.FDPrivateUsageData63 = resultValue.ToString();
                }
                else
                {
                    if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                        _logger.LogInfoMessage(string.Format("CnTransactionMessage.SetAdditionalDataForNewKeyRequest() ResultValue = <<NULL>>;"));
                }

                return objElements;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal("CnTransactionMessage.SetAdditionalDataForNewKeyRequest();", ex);

                throw;
            }
        }

        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Set Interac Debit reversal/void txn additional data.
        /// </summary>
        /// <param name="resultValue"></param>
        /// <param name="objTransactionEntities"></param>
        private static void SetEmvAdditionalDataForInteracReversalVoid(StringBuilder resultValue, CnTransactionEntities objTransactionEntities)
        {
            ILogger logger = new Logger();

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
            {
                logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEmvAdditionalDataForInteracReversalVoid() EntryMode:{0};IsTA:{1};IsTOR:{2};", objTransactionEntities.EntryMode, objTransactionEntities.IsTransArmor, objTransactionEntities.IsTORTransaction));
                logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEmvAdditionalDataForInteracReversalVoid() ReversalBit63:{0};", objTransactionEntities.ReversalBit63));
            }

            try
            {
                // Set additional data for Interac Card

                // Table32
                var objTable32 = new Table32();
                objTable32.MAC = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.MacData));
                objTable32.MsgAuthenticate = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.CheckDigit));
                string valueTable32 = SetTableProperties(objTable32);
                resultValue.Append(valueTable32);

                // Table38
                var objTable38 = new Table38();

                //Chetu 357
                string reversalRegionCode = SigmaStaticValue.ReversalReasonZeroZero; // As suggested by First Data at time of TOR testing 

                if (objTransactionEntities.IsMAcVerificationFailed)
                    reversalRegionCode = SigmaStaticValue.ReversalReasonMACVerification;

                if (objTransactionEntities.IsTORTransaction)
                    reversalRegionCode = SigmaStaticValue.ReversalReasonTimeout;

                objTable38.ReversalReasonCode = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(reversalRegionCode));
                string valueTable38 = SetTableProperties(objTable38);
                resultValue.Append(valueTable38);

                //Chetu 357
                if (objTransactionEntities.IsTransArmor)
                {
                    // TransArmor
                    var bit63Data = CommonFunctions.GetTableSPDetails(objTransactionEntities.ReversalBit63);

                    var objTable14SP = new TableSP_Reversal();

                    objTable14SP.Tag06Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));

                    // Ensure we have a "07" element in the collection before trying to use it.
                    if ((bit63Data.Count > 0) && (bit63Data["07"] != null))
                    {
                        string tag07val = (bit63Data["07"]);

                        objTable14SP.Tag07Length = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.Length.ToString().PadLeft(3, '0')));
                        objTable14SP.Tag07Value = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(tag07val.ToString()));
                    }

                    string valueTable14SP = SetTableProperties(objTable14SP);
                    resultValue.Append(valueTable14SP);
                }

                // TableEV
                if (objTransactionEntities.EntryMode != EntryMode.FSwiped.ToString())
                {
                    var objMCTableEV = new TableEV();
                    string valueMCTableEV = SetTableProperties(objMCTableEV);
                    resultValue.Append(valueMCTableEV);
                }

                // TableSD
                var objMCTableSD = new TableSD();
                string valueMCTableSD = SetTableProperties(objMCTableSD);
                resultValue.Append(valueMCTableSD);
            }
            catch (Exception ex)
            {
                logger.LogInfoFatal("CnTransactionMessage.SetEmvAdditionalDataForInteracReversalVoid();", ex);

                throw;
            }
        }

        /// <summary>      
        /// Purpose             :   Prepare bitmap fields for EMV Contactless credit sale transactions
        /// Function Name       :   SetEmvContactlessAdditionalDataForCreditSale
        /// Created By          :   Chetu
        /// Created On          :   24/08/2017
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private void SetEmvContactlessAdditionalDataForCreditSale(StringBuilder resultValue, CnTransactionEntities objTransactionEntities)
        {
            ILogger logger = new Logger();

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
            {
                logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEmvContactlessAdditionalDataForCreditSale() EntryMode:{0};IsTA:{1};IsTOR:{2};", objTransactionEntities.EntryMode, objTransactionEntities.IsTransArmor, objTransactionEntities.IsTORTransaction));
                logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEmvContactlessAdditionalDataForCreditSale() ReversalBit63:{0};", objTransactionEntities.ReversalBit63));
            }

            try
            {
                string authorizedAmount = FormatFields.FormatAmount(Convert.ToDouble(objTransactionEntities.AmountOfTransaction));

                switch (objTransactionEntities.CardIssuer)
                {
                    // Set additional data for MasterCard Issuer
                    case CardIssuer.MasterCard:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            var strBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                            strBuilder.Remove(56, 12);
                            strBuilder.Insert(56, authorizedAmount);
                            objTransactionEntities.Table14Response = strBuilder.ToString();
                            resultValue.Append(objTransactionEntities.Table14Response);
                        }
                        else
                        {
                            var objMCTable14 = new Table14AdditionalMasterData();
                            string valueMCTable14 = SetTableProperties(objMCTable14);
                            resultValue.Append(valueMCTable14);
                        }

                        // Table68
                        var objMCTable68 = new Table68();
                        string valueMCTable68 = SetTableProperties(objMCTable68);
                        resultValue.Append(valueMCTable68);

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objMCTable69 = new Table69();
                            string valueMCTable69 = SetTableProperties(objMCTable69);
                            resultValue.Append(valueMCTable69);
                        }

                        // TableMC
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                resultValue.Append(objTransactionEntities.TableMCResponse);
                            }
                            else
                            {
                                var objTablesMC = new TableMC();
                                string valueTableMC = SetTableProperties(objTablesMC);
                                resultValue.Append(valueTableMC);
                            }
                        }

                        //Chetu 357
                        if (objTransactionEntities.IsTransArmor)
                        {
                            // Transarmor
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }
                                tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objMCTableSp = new TableSP_Retail();
                                objMCTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                                objMCTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));
                                objMCTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objMCTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueMCTableSp = SetTableProperties(objMCTableSp);
                                resultValue.Append(valueMCTableSp);
                            }
                        }

                        // TableEV
                        if (objTransactionEntities.EntryMode != EntryMode.FSwiped.ToString())
                        {
                            var objMCTableEV = new TableEV();
                            string valueMCTableEV = SetTableProperties(objMCTableEV);
                            resultValue.Append(valueMCTableEV);
                        }

                        // TableSD
                        var objMCTableSD = new TableSD();

                        //Chetu 357
                        objMCTableSD.TagLN = SigmaStaticValue.TableSDTagLN + objTransactionEntities.TerminalLaneNo.PadLeft(8, '0');

                        string valueMCTableSD = SetTableProperties(objMCTableSD);
                        resultValue.Append(valueMCTableSD);

                        break;

                    // Set additional data for Visa Issuer
                    case CardIssuer.Visa:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                            aStringBuilder.Remove(32, 12);
                            aStringBuilder.Insert(32, authorizedAmount);
                            aStringBuilder.Remove(44, 12);
                            aStringBuilder.Insert(44, authorizedAmount);
                            objTransactionEntities.Table14Response = aStringBuilder.ToString();
                            resultValue.Append(objTransactionEntities.Table14Response);
                        }
                        else
                        {
                            var objVisaTable14 = new Table14AdditionalVisaData();
                            string valueVisaTable14 = SetTableProperties(objVisaTable14);
                            resultValue.Append(valueVisaTable14);
                        }

                        // Table68
                        var objVITable68 = new Table68();
                        string valueVITable68 = SetTableProperties(objVITable68);
                        resultValue.Append(valueVITable68);

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objVisaTable69 = new Table69();
                            string valueVisaTable69 = SetTableProperties(objVisaTable69);
                            resultValue.Append(valueVisaTable69);
                        }

                        // TableVI
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                resultValue.Append(objTransactionEntities.TableVIResponse);
                            }
                            else
                            {
                                var objVisaTableVI = new TableVI();
                                string valueVisaTableVI = SetTableProperties(objVisaTableVI);
                                resultValue.Append(valueVisaTableVI);
                            }
                        }

                        //Chetu 357
                        // Transarmor
                        if (objTransactionEntities.IsTransArmor)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }
                                tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objMCTableSp = new TableSP_Retail();
                                objMCTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                                objMCTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));
                                objMCTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objMCTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;

                                string valueMCTableSp = SetTableProperties(objMCTableSp);
                                resultValue.Append(valueMCTableSp);
                            }
                        }

                        // TableEV
                        if (objTransactionEntities.EntryMode != EntryMode.FSwiped.ToString())
                        {
                            var objVITableEV = new TableEV();
                            string valueVITableEV = SetTableProperties(objVITableEV);
                            resultValue.Append(valueVITableEV);
                        }

                        // TableSD
                        var objVITableSD = new TableSD();
                        string valueVITableSD = SetTableProperties(objVITableSD);
                        resultValue.Append(valueVITableSD);

                        break;

                    // Set additional data for American Express Issuer
                    case CardIssuer.Amex:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            resultValue.Append(objTransactionEntities.Table14Response);
                        }
                        else
                        {
                            var objAmexTable14 = new Table14AMEXData();
                            string valueAmexTable14 = SetTableProperties(objAmexTable14);
                            resultValue.Append(valueAmexTable14);
                        }

                        // Table68
                        var objAmexTable68 = new Table68();
                        string valueAmexTable68 = SetTableProperties(objAmexTable68);
                        resultValue.Append(valueAmexTable68);

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objAmexTable69 = new Table69();
                            string valueAmexTable69 = SetTableProperties(objAmexTable69);
                            resultValue.Append(valueAmexTable69);
                        }

                        //Chetu 357
                        // Transarmor
                        if (objTransactionEntities.IsTransArmor)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }
                                tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objMCTableSp = new TableSP_Retail();
                                objMCTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                                objMCTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));
                                objMCTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objMCTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueMCTableSp = SetTableProperties(objMCTableSp);
                                resultValue.Append(valueMCTableSp);
                            }
                        }

                        // TableEV
                        if (objTransactionEntities.EntryMode != EntryMode.FSwiped.ToString())
                        {
                            var objAmexTableEV = new TableEV();
                            string valueAmexTableEV = SetTableProperties(objAmexTableEV);
                            resultValue.Append(valueAmexTableEV);
                        }

                        // TableSD
                        var objAmexTableSD = new TableSD();
                        string valueAmexTableSD = SetTableProperties(objAmexTableSD);
                        resultValue.Append(valueAmexTableSD);

                        break;

                    // Set additional data for Discover
                    case CardIssuer.Discover:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            if (objTransactionEntities.CardIssuer.ToString() == CardTypeType.Discover.ToString() || objTransactionEntities.CardIssuer.ToString() == CardTypeType.Diners.ToString())
                            {
                                var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                                aStringBuilder.Remove(52, 12);
                                aStringBuilder.Insert(52, authorizedAmount);
                                objTransactionEntities.Table14Response = aStringBuilder.ToString();
                                resultValue.Append(objTransactionEntities.Table14Response);
                            }
                            else
                            {
                                var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                                aStringBuilder.Remove(82, 12);
                                aStringBuilder.Insert(82, authorizedAmount);
                                objTransactionEntities.Table14Response = aStringBuilder.ToString();
                                resultValue.Append(objTransactionEntities.Table14Response);
                            }
                        }
                        else
                        {
                            var objDiscoverTable14 = new Table14AdditionalDiscoverData();
                            string valueDiscoverTable14 = SetTableProperties(objDiscoverTable14);
                            resultValue.Append(valueDiscoverTable14);
                        }

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objDiscoverTable69 = new Table69();
                            string valueDiscoverTable69 = SetTableProperties(objDiscoverTable69);
                            resultValue.Append(valueDiscoverTable69);
                        }

                        // TableDS
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString() && objTransactionEntities.CardIssuer.ToString() != CardTypeType.JCB.ToString())
                            {
                                resultValue.Append(objTransactionEntities.TableDSResponse);
                            }
                            else if (objTransactionEntities.TransType != TransactionType.Completion.ToString())
                            {
                                var objTableDS = new TableDS();
                                string valueTableDS = SetTableProperties(objTableDS);
                                resultValue.Append(valueTableDS);
                            }
                        }

                        //Chetu 357
                        // Transarmor
                        if (objTransactionEntities.IsTransArmor)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }
                                tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objMCTableSp = new TableSP_Retail();
                                objMCTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                                objMCTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));
                                objMCTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objMCTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueMCTableSp = SetTableProperties(objMCTableSp);
                                resultValue.Append(valueMCTableSp);
                            }
                        }

                        // TableEV
                        if (objTransactionEntities.EntryMode != EntryMode.FSwiped.ToString())
                        {
                            var objDSTableEV = new TableEV();
                            string valueDSTableEV = SetTableProperties(objDSTableEV);
                            resultValue.Append(valueDSTableEV);
                        }

                        // TableSD
                        var objDSTableSD = new TableSD();
                        string valueDSTableSD = SetTableProperties(objDSTableSD);
                        resultValue.Append(valueDSTableSD);

                        break;

                    // Set additional data for Diners and JCB
                    case CardIssuer.Diners:
                    case CardIssuer.JCB:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            if (objTransactionEntities.CardIssuer.ToString() == CardTypeType.Discover.ToString() || objTransactionEntities.CardIssuer.ToString() == CardTypeType.Diners.ToString())
                            {
                                var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                                aStringBuilder.Remove(52, 12);
                                aStringBuilder.Insert(52, authorizedAmount);
                                objTransactionEntities.Table14Response = aStringBuilder.ToString();
                                resultValue.Append(objTransactionEntities.Table14Response);
                            }
                            else
                            {
                                var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                                aStringBuilder.Remove(82, 12);
                                aStringBuilder.Insert(82, authorizedAmount);
                                objTransactionEntities.Table14Response = aStringBuilder.ToString();
                                resultValue.Append(objTransactionEntities.Table14Response);
                            }
                        }
                        else
                        {
                            var objDiscoverTable14 = new Table14AdditionalDiscoverData();
                            string valueDiscoverTable14 = SetTableProperties(objDiscoverTable14);
                            resultValue.Append(valueDiscoverTable14);
                        }

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objDiscoverTable69 = new Table69();
                            string valueDiscoverTable69 = SetTableProperties(objDiscoverTable69);
                            resultValue.Append(valueDiscoverTable69);
                        }

                        //Chetu 357
                        // Transarmor
                        if (objTransactionEntities.IsTransArmor)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }
                                tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objMCTableSp = new TableSP_Retail();
                                objMCTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                                objMCTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));
                                objMCTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objMCTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueMCTableSp = SetTableProperties(objMCTableSp);
                                resultValue.Append(valueMCTableSp);
                            }
                        }

                        // TableEV
                        if (objTransactionEntities.EntryMode != EntryMode.FSwiped.ToString())
                        {
                            var objDiscoverTableEV = new TableEV();
                            string valueDiscoverTableEV = SetTableProperties(objDiscoverTableEV);
                            resultValue.Append(valueDiscoverTableEV);
                        }

                        // TableSD
                        var objDnTableSD = new TableSD();
                        string valueDnTableSD = SetTableProperties(objDnTableSD);
                        resultValue.Append(valueDnTableSD);

                        break;

                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal("CnTransactionMessage.SetEmvContactlessAdditionalDataForCreditSale();", ex);

                throw;
            }
        }

        //Chetu 357
        /// <summary>
        /// Used to make transaction request data for MSR Contactless transaction
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        public static CnTransactionEntities MakeMSRContactlessTransRequest(TransactionRequest transactionRequest)
        {
            ILogger logger = new Logger();

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                logger.LogInfoMessage(string.Format("CnTransactionMessage.MakeMSTContactlessTransRequest() paymentType:{0};cardType:{1};industryType:{2};txnType:{3};", transactionRequest.PaymentType, transactionRequest.CardType, transactionRequest.IndustryType, transactionRequest.TransactionType));

            return new CnTransactionEntities
            {
                CardType = (CardType)Enum.Parse(typeof(CardType), transactionRequest.PaymentType.ToString()),
                TransType = transactionRequest.TransactionType,
                CardIssuer = (CardIssuer)Enum.Parse(typeof(CardIssuer), transactionRequest.CardType.ToString()),
                IndustryType = (IndustryType)Enum.Parse(typeof(IndustryType), transactionRequest.IndustryType),
                ProcessingCode = FormatFields.PrepareProcessingCode(transactionRequest.PaymentType.ToString(), transactionRequest.TransactionType.ToString(), transactionRequest.EntryMode.ToString()),
                PrimaryAccountNumber = transactionRequest.TransType.ToString() == TxnTypeType.Completion.ToString() ? transactionRequest.CardNumber : null,
                AmountOfTransaction = transactionRequest.TransactionAmount,
                TransmissionDateTime = FormatFields.FormatTransDateTime(transactionRequest.TrnmsnDateTime),
                TimeLocalTransmission = FormatFields.FormatLocalTime(transactionRequest.LocalDateTime),
                DateLocalTrans = FormatFields.FormatLocalDate(transactionRequest.LocalDateTime),
                CardExpirationDate = FormatFields.FormatExpiryDate(transactionRequest.ExpiryDate, transactionRequest.TransactionType, transactionRequest.IndustryType),
                MerchantCategoryCode = SigmaStaticValue.MCCForRetail,
                PosEntryModePinCapability = FormatFields.FormatPosEntryPinCapability(transactionRequest.EntryMode),
                NetworkInternationId = SigmaStaticValue.NetworkInternationId,
                PosConditionCode = SigmaStaticValue.PosConditionCodeZeroZero,
                AcquirerReference = FormatFields.PrepareAcquirerRef(transactionRequest.TransactionType, transactionRequest.SourceTransaction),
                RetrievalRefNumber = transactionRequest.TransRefNo,
                AuthIdentificationResponse = FormatFields.PrepareAuthId(transactionRequest.TransactionType, transactionRequest.AuthIdentResponse),
                ResponseCode = transactionRequest.ResponseCode,
                TerminalId = transactionRequest.TerminalId.PadLeft(8, '0'),
                MerchantId = transactionRequest.FdMerchantId.PadLeft(15, '0'),
                AdditionalResponseData = transactionRequest.AddRespdata,
                TransactionCurrencyCode = SigmaStaticValue.CurrencyCode,
                EncryptedPinData = FormatFields.FormatEncryptedPinData(Convert.ToString(transactionRequest.PaymentType), Convert.ToString(transactionRequest.CardType), transactionRequest.DebitPin),
                MerchantZipCode = SigmaStaticValue.MerchantZipCode,
                AdditionalPosData = SigmaStaticValue.AdditionalPossDataForMSRContactless,
                SystemTrace = transactionRequest.Stan,
                Table14Response = transactionRequest.RespTable14,
                Table49Response = transactionRequest.RespTable49,
                TableVIResponse = transactionRequest.RespTableVI,
                TableSPResponse = transactionRequest.RespTableSP,
                TableDSResponse = transactionRequest.RespTableDS,
                TableMCResponse = transactionRequest.RespTableMC,
                IsTORTransaction = transactionRequest.IsTORTransaction,
                ReversalBit63 = transactionRequest.ResponseBit63,
                TokenType = transactionRequest.TokenType,
                Token = transactionRequest.RCCIToken,
                SourceTransaction = transactionRequest.SourceTransaction,

                //Chetu 357
                Track2Data = FormatFields.FormatTrack2Data(transactionRequest.Track2Data, transactionRequest.TransactionType, transactionRequest.IsTORTransaction),

                Track3Data = transactionRequest.Track3Data,
                KeyId = transactionRequest.KeyId,
                KeySerialNumber = transactionRequest.KeySerialNumber,
                EntryMode = transactionRequest.EntryMode,
                ICCData = transactionRequest.CardType == CardTypeType.MasterCard ? FormatFields.GetTag9F6EValue(transactionRequest.DeviceType) : string.Empty
            };
        }

        /// <summary>
        /// This method sets additional data for MSR Contactless transaction.
        /// </summary>
        /// <param name="resultValue"></param>
        /// <param name="objTransactionEntities"></param>
        /// <CreatedBy>Chetu</CreatedBy>
        private void SetMSRContactlessAdditionalDataForCreditSaleAuthCompletion(StringBuilder resultValue, CnTransactionEntities objTransactionEntities)
        {
            try
            {
                string authorizedAmount = FormatFields.FormatAmount(Convert.ToDouble(objTransactionEntities.AmountOfTransaction));

                switch (objTransactionEntities.CardIssuer)
                {
                    // Set additional data for MasterCard Issuer
                    case CardIssuer.MasterCard:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            var strBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                            strBuilder.Remove(54, 12);
                            strBuilder.Insert(54, authorizedAmount);
                            objTransactionEntities.Table14Response = strBuilder.ToString();
                            resultValue.Append(objTransactionEntities.Table14Response);
                        }
                        else
                        {
                            var objMCTable14 = new Table14AdditionalMasterData();
                            string valueMCTable14 = SetTableProperties(objMCTable14);
                            resultValue.Append(valueMCTable14);
                        }
                        
                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objMCTable69 = new Table69();
                            string valueMCTable69 = SetTableProperties(objMCTable69);
                            resultValue.Append(valueMCTable69);
                        }
                        
                        // TableMC
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                resultValue.Append(objTransactionEntities.TableMCResponse);
                            }
                            else
                            {
                                var objTablesMC = new TableMC();
                                string valueTableMC = SetTableProperties(objTablesMC);
                                resultValue.Append(valueTableMC);
                            }
                        }
                        
                        // Table36
                        if (objTransactionEntities.TransType != TransactionType.Authorization.ToString() && !objTransactionEntities.IsTORTransaction)
                        {
                            var objMCTables36 = new Table36();
                            objMCTables36.OrderNo = objTransactionEntities.RetrievalRefNumber + PaymentAPIResources.ThreeSpaces;
                            objMCTables36.OrderNo = CnGenericHandler.ConvertToHex(objMCTables36.OrderNo);
                            objMCTables36.OrderNo = CnTransactionMessage.ApplyDatawireFormat(objMCTables36.OrderNo);
                            string valueMCTables36 = SetTableProperties(objMCTables36);
                            resultValue.Append(valueMCTables36);
                        }

                        //Chetu 357
                        if (objTransactionEntities.IsTransArmor)
                        {
                            // Transarmor
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }
                                tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objMCTableSp = new TableSP_Retail();
                                objMCTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                                objMCTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));
                                objMCTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objMCTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueMCTableSp = SetTableProperties(objMCTableSp);
                                resultValue.Append(valueMCTableSp);
                            }
                        }

                        // TableSD
                        var objMCTableSD = new TableSD();
                        string valueMCTableSD = SetTableProperties(objMCTableSD);
                        resultValue.Append(valueMCTableSD);

                        break;

                    // Set additional data for Visa Issuer
                    case CardIssuer.Visa:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                            aStringBuilder.Remove(32, 12);
                            aStringBuilder.Insert(32, authorizedAmount);
                            aStringBuilder.Remove(44, 12);
                            aStringBuilder.Insert(44, authorizedAmount);
                            objTransactionEntities.Table14Response = aStringBuilder.ToString();
                            resultValue.Append(objTransactionEntities.Table14Response);
                        }
                        else
                        {
                            var objVisaTable14 = new Table14AdditionalVisaData();
                            string valueVisaTable14 = SetTableProperties(objVisaTable14);
                            resultValue.Append(valueVisaTable14);
                        }
                        
                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objVisaTable69 = new Table69();
                            string valueVisaTable69 = SetTableProperties(objVisaTable69);
                            resultValue.Append(valueVisaTable69);
                        }

                        // TableVI
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                resultValue.Append(objTransactionEntities.TableVIResponse);
                            }
                            else
                            {
                                var objVisaTableVI = new TableVI();
                                string valueVisaTableVI = SetTableProperties(objVisaTableVI);
                                resultValue.Append(valueVisaTableVI);
                            }
                        }
                        
                        // Table36
                        if (objTransactionEntities.TransType != TransactionType.Authorization.ToString() && !objTransactionEntities.IsTORTransaction)
                        {
                            var objVITables36 = new Table36();
                            objVITables36.OrderNo = objTransactionEntities.RetrievalRefNumber + PaymentAPIResources.ThreeSpaces;
                            objVITables36.OrderNo = CnGenericHandler.ConvertToHex(objVITables36.OrderNo);
                            objVITables36.OrderNo = CnTransactionMessage.ApplyDatawireFormat(objVITables36.OrderNo);
                            objVITables36.EcommUrl = PaymentAPIResources.VIEcommUrl;
                            string valueVITable36 = SetTableProperties(objVITables36);
                            resultValue.Append(valueVITable36);
                        }

                        //Chetu 357
                        if (objTransactionEntities.IsTransArmor)
                        {
                            // Transarmor
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }
                                tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objMCTableSp = new TableSP_Retail();
                                objMCTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                                objMCTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));
                                objMCTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objMCTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueMCTableSp = SetTableProperties(objMCTableSp);
                                resultValue.Append(valueMCTableSp);
                            }
                        }

                        // TableSD
                        var objVITableSD = new TableSD();
                        string valueVITableSD = SetTableProperties(objVITableSD);
                        resultValue.Append(valueVITableSD);

                        break;

                    // Set additional data for American Express Issuer
                    case CardIssuer.Amex:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            resultValue.Append(objTransactionEntities.Table14Response);
                        }
                        else
                        {
                            var objAmexTable14 = new Table14AMEXData();
                            string valueAmexTable14 = SetTableProperties(objAmexTable14);
                            resultValue.Append(valueAmexTable14);
                        }
                        
                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objAmexTable69 = new Table69();
                            string valueAmexTable69 = SetTableProperties(objAmexTable69);
                            resultValue.Append(valueAmexTable69);
                        }
                        
                        // Table36
                        if (objTransactionEntities.TransType != TransactionType.Authorization.ToString() && !objTransactionEntities.IsTORTransaction)
                        {
                            var objAmexTables36 = new Table36();
                            objAmexTables36.OrderNo = objTransactionEntities.RetrievalRefNumber + PaymentAPIResources.ThreeSpaces;
                            objAmexTables36.OrderNo = CnGenericHandler.ConvertToHex(objAmexTables36.OrderNo);
                            objAmexTables36.OrderNo = CnTransactionMessage.ApplyDatawireFormat(objAmexTables36.OrderNo);
                            string valueAmexTables36 = SetTableProperties(objAmexTables36);
                            resultValue.Append(valueAmexTables36);
                        }

                        //Chetu 357
                        if (objTransactionEntities.IsTransArmor)
                        {
                            // Transarmor
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }
                                tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objMCTableSp = new TableSP_Retail();
                                objMCTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                                objMCTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));
                                objMCTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objMCTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueMCTableSp = SetTableProperties(objMCTableSp);
                                resultValue.Append(valueMCTableSp);
                            }
                        }

                        // TableSD
                        var objAmexTableSD = new TableSD();
                        string valueAmexTableSD = SetTableProperties(objAmexTableSD);
                        resultValue.Append(valueAmexTableSD);

                        break;

                    // Set additional data for Discover,Diners and JCB
                    case CardIssuer.Discover:
                    case CardIssuer.Diners:
                    case CardIssuer.JCB:

                        // Table14
                        if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                        {
                            if (objTransactionEntities.CardIssuer.ToString() == CardTypeType.Discover.ToString() || objTransactionEntities.CardIssuer.ToString() == CardTypeType.Diners.ToString())
                            {
                                var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                                aStringBuilder.Remove(52, 12);
                                aStringBuilder.Insert(52, authorizedAmount);
                                objTransactionEntities.Table14Response = aStringBuilder.ToString();
                                resultValue.Append(objTransactionEntities.Table14Response);
                            }
                            else
                            {
                                var aStringBuilder = new StringBuilder(objTransactionEntities.Table14Response);
                                aStringBuilder.Remove(82, 12);
                                aStringBuilder.Insert(82, authorizedAmount);
                                objTransactionEntities.Table14Response = aStringBuilder.ToString();
                                resultValue.Append(objTransactionEntities.Table14Response);
                            }
                        }
                        else
                        {
                            var objDiscoverTable14 = new Table14AdditionalDiscoverData();
                            string valueDiscoverTable14 = SetTableProperties(objDiscoverTable14);
                            resultValue.Append(valueDiscoverTable14);
                        }

                        // Table69
                        if (!objTransactionEntities.IsTORTransaction && objTransactionEntities.TransType != TransactionType.Completion.ToString())
                        {
                            var objDiscoverTable69 = new Table69();
                            string valueDiscoverTable69 = SetTableProperties(objDiscoverTable69);
                            resultValue.Append(valueDiscoverTable69);
                        }

                        // TableDS
                        if (!objTransactionEntities.IsTORTransaction)
                        {
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString() && objTransactionEntities.CardIssuer.ToString() != CardTypeType.JCB.ToString())
                            {
                                resultValue.Append(objTransactionEntities.TableDSResponse);
                            }
                            else if (objTransactionEntities.TransType != TransactionType.Completion.ToString() && objTransactionEntities.CardIssuer.ToString() != CardTypeType.JCB.ToString())
                            {
                                var objTableDS = new TableDS();
                                string valueTableDS = SetTableProperties(objTableDS);
                                resultValue.Append(valueTableDS);
                            }
                        }

                        // Table36
                        if (objTransactionEntities.TransType != TransactionType.Authorization.ToString() && !objTransactionEntities.IsTORTransaction)
                        {
                            var objDSTables36 = new Table36();
                            objDSTables36.OrderNo = objTransactionEntities.RetrievalRefNumber + PaymentAPIResources.ThreeSpaces;
                            objDSTables36.OrderNo = CnGenericHandler.ConvertToHex(objDSTables36.OrderNo);
                            objDSTables36.OrderNo = CnTransactionMessage.ApplyDatawireFormat(objDSTables36.OrderNo);
                            string valueDSTables36 = SetTableProperties(objDSTables36);
                            resultValue.Append(valueDSTables36);
                        }

                        //Chetu 357
                        if (objTransactionEntities.IsTransArmor)
                        {
                            // Transarmor
                            if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                            {
                                var tableToFill = new TableSP(objTransactionEntities.TokenType);
                                var tableType = tableToFill.GetType();
                                var objProperties = tableType.GetProperties();
                                string tableValues = string.Empty;
                                foreach (var item in objProperties)
                                {
                                    var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                                    if (!String.IsNullOrEmpty(propertyValue))
                                        tableValues += propertyValue;
                                }
                                tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;
                                tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                                resultValue.Append(tableValues);
                            }
                            else
                            {
                                var objMCTableSp = new TableSP_Retail();
                                objMCTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                                objMCTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));
                                objMCTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                                objMCTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                                string valueMCTableSp = SetTableProperties(objMCTableSp);
                                resultValue.Append(valueMCTableSp);
                            }
                        }

                        // TableSD
                        var objDSTableSD = new TableSD();
                        string valueDSTableSD = SetTableProperties(objDSTableSD);
                        resultValue.Append(valueDSTableSD);

                        break;

                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal("CnTransactionMessage.SetMSRContactlessAdditionalDataForCreditSaleAuthCompletion();", ex);

                throw;
            }
        }

        /// <summary>        
        /// Purpose             :   This method sets additional data of Interac Contactless transaction.
        /// Function Name       :   SetEmvAdditionalDataForInteracContactlessSale
        /// Created By          :   Chetu          
        /// <param name="resultValue"></param>
        /// <param name="objTransactionEntities"></param>
        private static void SetEmvAdditionalDataForInteracContactlessSale(StringBuilder resultValue, CnTransactionEntities objTransactionEntities)
        {
            ILogger logger = new Logger();

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
            {
                logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEmvAdditionalDataForInteracContactlessSale() EntryMode:{0};IsTA:{1};IsTOR:{2};", objTransactionEntities.EntryMode, objTransactionEntities.IsTransArmor, objTransactionEntities.IsTORTransaction));
                logger.LogInfoMessage(string.Format("CnTransactionMessage.SetEmvAdditionalDataForInteracContactlessSale() ReversalBit63:{0};", objTransactionEntities.ReversalBit63));
            }

            try
            {
                // Table32
                var objTable32 = new Table32();
                objTable32.MAC = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.MacData));
                objTable32.MsgAuthenticate = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.CheckDigit));
                string valueTable32 = SetTableProperties(objTable32);
                resultValue.Append(valueTable32);

                // Table66
                var obj66 = new Table66();
                string valueMCTable66 = SetTableProperties(obj66);
                resultValue.Append(valueMCTable66);

                // TableEV
                if (objTransactionEntities.EntryMode != EntryMode.FSwiped.ToString())
                {
                    var objMCTableEV = new TableEV();
                    string valueMCTableEV = SetTableProperties(objMCTableEV);
                    resultValue.Append(valueMCTableEV);
                }

                //Chetu 357
                if (objTransactionEntities.IsTransArmor)
                {
                    // Transarmor
                    if (objTransactionEntities.TransType == TransactionType.Completion.ToString())
                    {
                        var tableToFill = new TableSP(objTransactionEntities.TokenType);
                        var tableType = tableToFill.GetType();
                        var objProperties = tableType.GetProperties();
                        string tableValues = string.Empty;
                        foreach (var item in objProperties)
                        {
                            var propertyValue = Convert.ToString(item.GetValue(tableToFill));
                            if (!String.IsNullOrEmpty(propertyValue))
                                tableValues += propertyValue;
                        }

                        if (objTransactionEntities.CardIssuer.ToString() == CardIssuer.Diners.ToString())
                            tableValues = tableValues + PaymentAPIResources.TableSP07014 + objTransactionEntities.TableSPResponse;
                        else
                            tableValues = tableValues + PaymentAPIResources.TableSP07016 + objTransactionEntities.TableSPResponse;

                        tableValues = SetAdditionalTableLength(tableValues) + tableValues;
                        resultValue.Append(tableValues);
                    }
                    else
                    {
                        var objTableSp = new TableSP_Retail();
                        objTableSp.TokenTypeValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.TokenType == null ? string.Empty : objTransactionEntities.TokenType));
                        objTableSp.KeyIdTagValue = ApplyDatawireFormat(CnGenericHandler.ConvertToHex(objTransactionEntities.KeyId == null ? string.Empty : objTransactionEntities.KeyId));
                        objTableSp.EncryptionBlockTagDataLength = FormatFields.FormatTrack3DataLength(objTransactionEntities.Track3Data);
                        objTableSp.EncryptionBlockTagValue = objTransactionEntities.Track3Data;
                        string valueTableSp = SetTableProperties(objTableSp);
                        resultValue.Append(valueTableSp);
                    }
                }

                // TableSD
                var objMCTableSD = new TableSD();
                string valueMCTableSD = SetTableProperties(objMCTableSD);
                resultValue.Append(valueMCTableSD);
            }
            catch (Exception ex)
            {
                logger.LogInfoFatal("CnTransactionMessage.SetEmvAdditionalDataForInteracContactlessSale();", ex);

                throw;
            }
        }

        /// <summary>
        /// Pass transaction data in CnTransactionEnities class for Interac Contactless
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <CreatedBy>Chetu</CreatedBy>
        /// <returns>CnTransactionEntities</returns>
        public static CnTransactionEntities MakeInteracContactlessTransRequest(TransactionRequest transactionRequest)
        {
            ILogger logger = new Logger();

            if (ConfigurationManager.AppSettings["IncludeDebug"] == "Y")
                logger.LogInfoMessage(string.Format("CnTransactionMessage.MakeInteracContactlessTransRequest() paymentType:{0};cardType:{1};industryType:{2};txnType:{3};", transactionRequest.PaymentType, transactionRequest.CardType, transactionRequest.IndustryType, transactionRequest.TransactionType));

            return new CnTransactionEntities
            {
                CardType = (CardType)Enum.Parse(typeof(CardType), transactionRequest.PaymentType.ToString()),
                TransType = transactionRequest.TransactionType,
                CardIssuer = (CardIssuer)Enum.Parse(typeof(CardIssuer), transactionRequest.CardType.ToString()),
                IndustryType = (IndustryType)Enum.Parse(typeof(IndustryType), transactionRequest.IndustryType),

                ProcessingCode = FormatFields.GetInteracProcessingCode(transactionRequest.TransactionType, transactionRequest.AccountType != string.Empty ? transactionRequest.AccountType : AccountType.Saving.ToString()),

                AmountOfTransaction = transactionRequest.TransactionAmount,
                TransmissionDateTime = FormatFields.FormatTransDateTime(transactionRequest.TrnmsnDateTime),
                TimeLocalTransmission = FormatFields.FormatLocalTime(transactionRequest.LocalDateTime),
                DateLocalTrans = FormatFields.FormatLocalDate(transactionRequest.LocalDateTime),
                MerchantCategoryCode = SigmaStaticValue.MCCForRetail,
                NetworkInternationId = SigmaStaticValue.NetworkInternationId,

                PrimaryAccountNumber = transactionRequest.CardNumber,

                PosEntryModePinCapability = SigmaStaticValue.POSEntryModePinCapabilityForEMVContactless,
                PosConditionCode = SigmaStaticValue.PosConditionCodeZeroZero,
                AcquirerReference = FormatFields.PrepareAcquirerRef(transactionRequest.TransactionType, transactionRequest.SourceTransaction),
                RetrievalRefNumber = transactionRequest.TransRefNo,
                SystemTrace = transactionRequest.Stan,
                TerminalId = transactionRequest.TerminalId.PadLeft(8, '0'),
                MerchantId = transactionRequest.FdMerchantId.PadLeft(15, '0'),
                MerchantZipCode = SigmaStaticValue.MerchantZipCode,
                AdditionalPosData = SigmaStaticValue.AdditionalPossDataForEMVContactless,
                TransactionCurrencyCode = SigmaStaticValue.CurrencyCode,
                AdditionalResponseData = transactionRequest.AddRespdata,
                EncryptedPinData = FormatFields.FormatEncryptedPinData(Convert.ToString(transactionRequest.PaymentType), Convert.ToString(transactionRequest.CardType), transactionRequest.DebitPin),
                Table14Response = transactionRequest.RespTable14,
                Table49Response = transactionRequest.RespTable49,
                TableVIResponse = transactionRequest.RespTableVI,
                TableSPResponse = transactionRequest.RespTableSP,
                TableDSResponse = transactionRequest.RespTableDS,
                TableMCResponse = transactionRequest.RespTableMC,
                IsTORTransaction = transactionRequest.IsTORTransaction,
                ReversalBit63 = transactionRequest.ResponseBit63,
                IsReversalTransaction = transactionRequest.IsReversal,
                AuthIdentificationResponse = FormatFields.PrepareAuthId(transactionRequest.TransactionType, transactionRequest.AuthIdentResponse),

                Track2Data = FormatFields.FormatTrack2Data(transactionRequest.Track2Data, transactionRequest.TransactionType, transactionRequest.IsTORTransaction),

                ICCData = transactionRequest.EmvData,
                KeyId = transactionRequest.KeyId,
                KeySerialNumber = transactionRequest.KeySerialNumber,
                CardSequenceNumber = transactionRequest.CardSeq,
                CardExpirationDate = transactionRequest.ExpiryDate.Substring(2, 4),
                MacData = transactionRequest.MacData,
                CheckDigit = transactionRequest.CheckDigit,
                EntryMode = transactionRequest.EntryMode,

                //Chetu 357
                Track3Data = transactionRequest.Track3Data,
                TokenType = transactionRequest.TokenType
            };
        }

    }
}
