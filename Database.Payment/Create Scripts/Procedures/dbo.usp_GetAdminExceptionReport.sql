IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAdminExceptionReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_GetAdminExceptionReport]
GO

CREATE PROCEDURE dbo.usp_GetAdminExceptionReport
-- =============================================
-- Author:	<Naveen Kumar>
-- Create date: <10-03-2016>
-- Description:	<Get admin exception report>
-- =============================================
(
	@DateFrom DATETIME=NULL,
	@DateTo DATETIME=NULL
)
AS
BEGIN
	BEGIN TRY
	  -- SET NOCOUNT ON added to prevent extra result sets from
	  SET NOCOUNT ON;
	  SET ANSI_WARNINGS OFF
	  DECLARE @Merchant NVARCHAR(100),
		   @Fleet NVARCHAR(100),
		   @ExpFleetTrans INT,
		   @ActFleetTrans INT,
		   @ExpCar INT,
		   @ActCar INT,
		   @ExpValue DECIMAL(18,2),
		   @ActValue DECIMAL(18,2),
		   @Case VARCHAR(100)
	  DECLARE @TableData TABLE
	   (
		 Merchant NVARCHAR(70),
		 Fleet NVARCHAR(70),
		 ExpFleetTrans INT,
		 ActFleetTrans INT,
		 ExpCar INT,
		 ActCar INT,
		 ExpValue NVARCHAR(40),
		 ActValue NVARCHAR(40),
		 ExpCase NVARCHAR(100)
	   )
	   --make comment 
	   --SELECT 'Merchant' AS 'MerchantName', 'FleetName' AS 'FleetName',1 AS 'ExpFleetTrans',1 AS 'ActFleetTrans', 1 AS 'ExpCar', 1 AS 'ActCar', '$1.25' AS 'ExpValue', '$1.25' AS 'ActValue', 'ExpCase' AS 'ExpCase' 
   			 --till here
	  SELECT COALESCE((SELECT Company FROM Merchant M WHERE M.MerchantID = Trans.MerchantId),'') 'Merchant',
			 COALESCE((SELECT [FleetName] FROM Fleet F WHERE F.FleetID = Fl.FleetID),'')  'Fleet',
			 COALESCE(Fl.FleetTransaction,0) 'ExpFleetTrans',COALESCE(Trans.FTransCount,0) 'ActFleetTrans',
			 COALESCE(Fl.CarTransaction,0) 'ExpCar',COALESCE(Trans.VTransCount,0) 'ActCar',
			 COALESCE(Fl.TransactionValue,0.00) 'ExpValue',COALESCE(Trans.Amount,0.00)  'ActValue' INTO #Temp
	  FROM
	   (
		 SELECT fk_MerchantID MerchantID, FleetID, FleetName, FleetTransaction, CarTransaction, CAST(TransactionValue  AS DECIMAL(18,2)) TransactionValue
		 FROM Fleet F WHERE  F.IsActive = 1 
		 AND ( F.FleetTransaction IS NOT NULL OR  F.CarTransaction IS NOT NULL OR  F.TransactionValue IS NOT NULL) 
	   ) Fl
	  INNER JOIN
	  (
		SELECT V.MerchantId,V.FleetID,SUM(V.TransCount) FTransCount,Count(V.TransCount) VTransCount,SUM(V.FareValue) FareValue,SUM(V.Tip) AS 'Tip'
		,SUM(V.Surcharge) AS 'Surcharge',SUM(V.Amount) AS 'Amount',SUM(V.Fee) AS 'Fee',SUM(V.TechFee) AS 'TechFee',SUM(V.AmountOwing) AS 'AmountOwing' FROM 
		 (
			SELECT COALESCE(T.MerchantId,0) AS 'MerchantId'
			,COALESCE((SELECT TOP 1 fk_FleetID FROM Vehicle V WHERE V.fk_FleetId=T.fk_FleetId),'') AS FleetID
			,COALESCE(T.VehicleNo,'') AS 'VehicleNo'
			,COUNT(T.VehicleNo) AS TransCount
			,SUM(T.FareValue) AS 'FareValue'
			,SUM(T.Tip) AS 'Tip'
			,SUM(T.Surcharge) AS 'Surcharge'
			,cast(SUM(T.Amount) as decimal(18,2)) AS 'Amount'
			,COALESCE(SUM(T.Fee),0)+COALESCE(SUM(T.FleetFee),0) AS 'Fee'
			,SUM(T.TechFee) AS 'TechFee'
			,COALESCE(SUM(T.Amount),0) - COALESCE(SUM(T.FleetFee),0)-COALESCE(SUM(T.Fee),0) AS 'AmountOwing'
			FROM MTDTransaction T
			WHERE T.ResponseCode IN ('000', '00', '002') 
				  AND T.TxnType IN ('Sale','Completion')
				  AND CAST(T.TxnDate AS DATE)>=@DateFrom 
				  AND CAST(T.TxnDate AS DATE)<@DateTo 
				  AND IsRefunded=0 AND IsVoided=0
			GROUP BY T.VehicleNo,T.MerchantId,T.fk_FleetId
		) V GROUP BY V.MerchantId,V.FleetID
	  ) Trans ON (fl.FleetID = Trans.FleetID)
	  WHERE Fl.FleetTransaction > Trans.FTransCount OR Fl.CarTransaction > Trans.VTransCount OR Fl.TransactionValue > Trans.Amount
	   --Declaring cursor for fetching exception data row one by one from temp table
	  DECLARE MerExp_Cursor CURSOR FOR 
	  SELECT Merchant,Fleet,ExpFleetTrans,ActFleetTrans,ExpCar,ActCar,ExpValue,ActValue FROM #Temp
	  OPEN MerExp_Cursor 
	  FETCH NEXT FROM MerExp_Cursor  INTO @Merchant,@Fleet,@ExpFleetTrans,@ActFleetTrans,@ExpCar,@ActCar,@ExpValue,@ActValue
	  WHILE @@FETCH_STATUS = 0 
		BEGIN  
		  SET @Case=''
		  IF(@ExpFleetTrans>@ActFleetTrans)
			BEGIN
			 SET @Case='UnExp Trans'
			END
		   IF(@ExpCar>@ActCar) 
			BEGIN 
			 IF(@Case<>'') 
			   BEGIN
				 SET @Case=@Case+','+ 'UnExp Cars'
				END
			 ELSE 
			  BEGIN
				SET @Case=@Case+ 'UnExp Cars'
			   END
			END
		   IF(@ExpValue>@ActValue)
			BEGIN
			 IF(@Case<>'') 
			  BEGIN
			   SET @Case=@Case+','+'UnExp Trans Value'
			  END
			 ELSE 
			  BEGIN
			   SET @Case=@Case+'UnExp Trans Value'
			  END
			END
		--Set data in xml format
		INSERT INTO @TableData(Merchant,Fleet,ExpFleetTrans,ActFleetTrans,ExpCar,ActCar,ExpValue,ActValue,ExpCase)
		SELECT COALESCE(@Merchant,''),COALESCE(@Fleet,''),COALESCE(@ExpFleetTrans,0),
								  COALESCE(@ActFleetTrans,0),COALESCE(@ExpCar,0),COALESCE(@ActCar,0), CONCAT('$',COALESCE(@ExpValue,0.00)),
								  CONCAT('$',COALESCE(@ActValue,0.00)),COALESCE(@Case,'')
		FETCH NEXT FROM MerExp_Cursor  INTO @Merchant,@Fleet,@ExpFleetTrans,@ActFleetTrans,@ExpCar,@ActCar,@ExpValue,@ActValue
	   END   
	  CLOSE MerExp_Cursor    
	  DEALLOCATE MerExp_Cursor 
	  DROP TABLE #Temp
	  SELECT Merchant AS 'MerchantName',
			 Fleet AS 'FleetName',
			 ExpFleetTrans AS 'ExpFleetTrans',
			 ActFleetTrans AS 'ActFleetTrans',
			 ExpCar AS 'ExpCar',
			 ActCar AS 'ActCar',
			 ExpValue AS 'ExpValue',
			 ActValue AS 'ActValue',
			 ExpCase AS 'ExpCase' 
			 FROM @TableData
	END TRY

	--use catch to log error in the table  ProcErrorHandler
	BEGIN CATCH
			INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
			SELECT ERROR_NUMBER()
				  ,ERROR_SEVERITY()
				  ,ERROR_STATE()
				  ,ERROR_PROCEDURE()
				  ,ERROR_LINE()
				  ,ERROR_MESSAGE()
				  ,SUSER_SNAME()
				  ,HOST_NAME()
				  ,GETDATE()
	END CATCH
END
GO
