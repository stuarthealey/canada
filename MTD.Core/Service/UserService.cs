﻿using System.Collections.Generic;
using Ninject;

using MTD.Core.Extension;
using MTD.Core.DataTransferObjects;
using MTD.Core.Factory.Interface;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Core.Service
{
    class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserFactory _userFactory;

        public UserService([Named("UserRepository")] IUserRepository userRepository, [Named("UserFactory")] IUserFactory userFactory) //("MerchantRepository") maped in class NinjectObjectMapper in MTD.Core.Infrastructure same for ("MerchantFactory")
        {
            _userRepository = userRepository; //need to initilize object only once due to ninject
            _userFactory = userFactory;
        }

        /// <summary>
        /// Purpose             :   To Add the Admin User
        /// Function Name       :   Add
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/25/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userDto"></param>
        /// <returns></returns>
        public bool Add(UserDto userDto) // for adding user under merchant
        {
            try
            {
                User user = _userFactory.CreateUser(userDto);
                return _userRepository.Add(user);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get User Roles
        /// Function Name       :   GetUserRoles
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/25/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<RoleDto> GetUserRoles(int userId) // for accessing roles of user
        {
            try
            {
                IEnumerable<Role> roles = _userRepository.GetUsersRole(userId);
                IEnumerable<RoleDto> roleDto = roles.ToDTOList();
                return roleDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Users Fleet
        /// Function Name       :   GetUsersFleet
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/26/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<UserFleetDto> GetUsersFleet(int userId) // for accessing roles of user
        {
            try
            {
                IEnumerable<UserFleet> fleets = _userRepository.GetUsersFleet(userId);
                IEnumerable<UserFleetDto> fleetDto = fleets.ToDtoUserFleetList();
                return fleetDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get the User List
        /// Function Name       :   UserList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/26/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchanId"></param>
        /// <returns></returns>
        public IEnumerable<UserDto> UserList(int merchanId, string name, int logOnByUserId, int logOnByUserType)// get list of users
        {
            try
            {
                IEnumerable<User> userlist = _userRepository.UserList(merchanId, name, logOnByUserId, logOnByUserType);
                var userDtoList = userlist.ToDTOList();
                return userDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update the exixting user
        /// Function Name       :   Update
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/26/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userDto"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool Update(UserDto userDto, int userId) // updating user
        {
            try
            {
                User user = _userFactory.CreateUser(userDto);
                return _userRepository.Update(user, userId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update the user Profile
        /// Function Name       :   UpdateUserProfile
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/29/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userDto"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool UpdateUserProfile(UserDto userDto, int userId) // updating user
        {
            try
            {
                User user = _userFactory.CreateUser(userDto);
                return _userRepository.UpdateUserProfile(user, userId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get the User
        /// Function Name       :   GetUser
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/29/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public UserDto GetUserUpdate(int userId, int merchantId) // get user on the basis of id
        {
            try
            {
                User obj = _userRepository.GetUserUpdate(userId, merchantId);
                UserDto userDto = obj.ToDtoUser();
                return userDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Delete User
        /// Function Name       :   DeleteUser
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/29/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        public void DeleteUser(int userId) // deleting user
        {
            try
            {
                _userRepository.DeleteUser(userId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Fleet List
        /// Function Name       :   FleetList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/30/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<FleetDto> FleetList(int merchantId)// get list of fleet
        {
            try
            {
                IEnumerable<Fleet> fleetlist = _userRepository.FleetList(merchantId);
                var fleetDtoList = fleetlist.ToDTOList();
                return fleetDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<FleetDto> UserFleetLists(int userId)// get list of fleet
        {
            try
            {
                IEnumerable<Fleet> fleetlist = _userRepository.UserFleetLists(userId);
                var fleetDtoList = fleetlist.ToDTOList();
                return fleetDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Users List
        /// Function Name       :   UserListByFilter
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/30/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="filterSearch"></param>
        /// <returns></returns>
        public IEnumerable<UserDto> UserListByFilter(FilterSearchParametersDto filterSearch, int logOnByUserId, int logOnByUserType)// get list of users
        {
            try
            {
                FilterSearchParameters filterSearchClass = filterSearch.SearchParameters();
                IEnumerable<User> userlist = _userRepository.UserListByFilter(filterSearchClass, logOnByUserId, logOnByUserType);
                var userDtoList = userlist.ToDTOList();
                return userDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get User
        /// Function Name       :   GetUser
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/31/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public UserDto GetUser(int userId, int val) // get user on the basis of id
        {
            User obj = _userRepository.GetUser(userId, val);
            UserDto userDto = obj.ToDtoUser();
            return userDto;
        }

        /// <summary>
        /// Purpose             :   To Get the User
        /// Function Name       :   GetUser
        /// Created By          :   Asif Shafeeque
        /// Created On          :   12/29/2014
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDto GetUser(int userId) // get user on the basis of id
        {
            try
            {
                User obj = _userRepository.GetUser(userId);
                UserDto userDto = obj.ToDtoUser();
                return userDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             : for changing status for first login
        /// Function Name       :  ChangeStatus
        /// Created By          :  Asif Shafeeque
        /// Created On          :  8/17/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"ary>
        /// <param name="UserId"></param>
        public void ChangeStatus(int UserId)
        {
            try
            {
                _userRepository.ChangeStatus(UserId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public IEnumerable<FleetListDto> UserFleetList(int UserId)
        {
            try
            {
                IEnumerable<FleetList> fleetList = _userRepository.UserFleetList(UserId);
                IEnumerable<FleetListDto> fleet = fleetList.ToDTOList();
                return fleet;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Fleet List per PayerType
        /// Function Name       :   PayerFleetList
        /// Created By          :   Shishir Saunakia
        /// Created On          :   10/12/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        ///<param name="PayerId"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<FleetDto> PayerFleetList(int PayerId, int merchantId)// get list of fleet
        {
            try
            {
                IEnumerable<Fleet> fleetlist = _userRepository.PayerFleetList(PayerId, merchantId);
                var fleetDtoList = fleetlist.ToDTOList();
                return fleetDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fleets"></param>
        /// <returns></returns>
        public List<int> GetAccesibleUserFleet(List<int> fleets)
        {
            return _userRepository.GetAccesibleUserFleet(fleets);
        }

        /// <summary>
        /// Purpose             :   To get Fleet List per PayerType
        /// Function Name       :   PayerFleetList
        /// Created By          :   Shishir Saunakia
        /// Created On          :   10/12/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        ///<param name="PayerId"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<UserFleetDto> GetUsersPayerFleet(int userId, int PayerId) // for accessing roles of user
        {
            try
            {
                IEnumerable<UserFleet> fleets = _userRepository.GetUsersPayerFleet(userId, PayerId);
                IEnumerable<UserFleetDto> fleetDto = fleets.ToDtoUserFleetList();
                return fleetDto;
            }
            catch
            {
                throw;
            }
        }
    }
}
