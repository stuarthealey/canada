﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table68DCICashback
    {
        #region Constants
        private const string tblId = "|36|38";
        private const string version = "|32";//Version 2
        #endregion

        #region Variables
        private string defaultBalInfoCapability = "|31";//Balance information not supported in responses.
        private string defaultPartialAuthAppCapability = "|31";//Partial authorization not supported.
        private string defaultFiller = "|20|20|20|20|20|20|20|20";
        private string defaultNumberofAccountInformationRecords = "|31";
        private string defaultAccountType = "30";
        private string defaultAmountType = "40";
        private string defaultSignAmount = "C";
        private string _defaultCurrencyCode = "0124";
        #endregion

        /// <summary>
        /// ID of Table
        /// default X'36 38' in Request
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        /// <summary>
        /// Version of the table
        /// default X'32' in Request for Version 2
        /// </summary>
        public string Version
        {
            get { return version; }
        }

        /// <summary>
        /// POS device’s capability to handle balance information in response messages.
        /// default X'30' in Request 
        /// X'30' — Balance information not supported in responses 
        /// X'31' — Balance information is supported in responses
        /// </summary>
        public string BalanceInfoCapability
        {
            get { return defaultBalInfoCapability; }
            set { defaultBalInfoCapability = value; }
        }

        /// <summary>
        /// POS device’s capability to accept response messages
        /// in which only a portion of the requested amount was approved.
        /// default X'31' in Request 
        /// X'30' — Partial authorization approvals are not supported. 
        /// X'31' — Partial authorization approvals are supported.
        /// </summary>    
        public string PartialAuthApprovalCapability
        {
            get { return defaultPartialAuthAppCapability; }
            set { defaultPartialAuthAppCapability = value; }
        }
        public string Filler
        {
            get { return defaultFiller; }
            set { defaultFiller = value; }
        }
        /// <summary>
        /// This value indicates the number of account information records being provided.
        /// </summary>
        public string NumberofAccountInformationRecords
        {
            set { defaultNumberofAccountInformationRecords = value; }
            get { return defaultNumberofAccountInformationRecords; }
        }

        public string AccountType
        {
            set { defaultAccountType = value; }
            get { return defaultAccountType; }
        }
        public string AmountType
        {
            set { defaultAmountType = value; }
            get { return defaultAmountType; }
        }
        public string CurrencyCode
        {
            set { _defaultCurrencyCode = value; }
            get { return _defaultCurrencyCode; }
        }
        public string SignAmount
        {
            set { defaultSignAmount = value; }
            get { return defaultSignAmount; }
        }
        /// <summary>
        /// 12 digits, with implied decimal relative to the currency code.
        /// In Auto-Substantiation transactions, this value reflects
        /// the qualified amount of the total purchase.
        /// </summary>
        public string Amount { get; set; }
    }
}