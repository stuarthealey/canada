﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MTData.Utility;
using MTData.WebAPI.CanadaCommon;
using MTData.WebAPI.Resources;

namespace MTData.WebAPI.CanadaCert
{
    public class ResponseHandler
    {
        ///// <summary>
        ///// Set bit-63 responded tables data.
        ///// </summary>
        ///// <param name="bit63Data"></param>
        ///// <returns>Dictionary<string, string></returns>
        ///// <CreatedBy>Naveen Kumar</CreatedBy>
        //public static Dictionary<string, string> SetBit63TablesResponse(string bit63Data)
        //{
        //    Dictionary<string, string> responseTableDict = new Dictionary<string, string>();
        //    try
        //    {
        //        //Initially assign whole bit63Data as remainingBitData
        //        var remainingBitData = bit63Data;
        //        while (!string.IsNullOrEmpty(remainingBitData))
        //        {
        //            //Find out the length Indicator which is of 4 digits from starting.
        //            var lengthIndic = remainingBitData.Substring(0, 4).TrimStart(SigmaStaticValue.Zero);
        //            int tblSize = 0;
        //            if (!string.IsNullOrEmpty(lengthIndic))
        //                tblSize = Convert.ToInt32(lengthIndic) * 2;//Table size is twice the length indicator as the data is in Hex.
        //
        //            //Set the table data except table Id.
        //            var changeableTableData = remainingBitData.Substring(4, tblSize);
        //
        //            //Retrieve table Id which is of 4 digits(In Hex) and convert to ASCII for actual result.
        //            var tableId = CnGenericHandler.ConvertHexToAscii(changeableTableData.Substring(0, 4));
        //
        //            //Add Table Id and corresponding table data in the Dictionary. 
        //            responseTableDict.Add(tableId, changeableTableData.Substring(4));
        //
        //            //Set the remaining bit data which is not added in the above Dictionary. 
        //            remainingBitData = remainingBitData.Substring(tblSize + 4);//Added 4 is the length indicator.
        //        }
        //
        //        return responseTableDict;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        /// <summary>
        /// Purpose             :   Set table response
        /// Function Name       :   SetTablesResponse
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   05/3/2016
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   05/16/2016
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static Dictionary<string, string> SetTablesResponse(string bit63Data)
        {
            Dictionary<string, string> responseTableDict = new Dictionary<string, string>();
            try
            {
                var remainingBitData = bit63Data;
                while (!string.IsNullOrEmpty(remainingBitData))
                {
                    var lengthIndic = remainingBitData.Substring(0, 4).TrimStart(SigmaStaticValue.Zero);
                    int tblSize = 0;
                    if (!string.IsNullOrEmpty(lengthIndic))
                        tblSize = Convert.ToInt32(lengthIndic) * 2;

                    var changeableTableData = remainingBitData.Substring(0, tblSize);

                    var tableId = CnGenericHandler.ConvertHexToAscii(changeableTableData.Substring(8, 4));

                    responseTableDict.Add(tableId, changeableTableData.Substring(4));

                    remainingBitData = remainingBitData.Substring(tblSize);
                }

                return responseTableDict;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To parse the canada response
        /// Function Name       :   ParseAndSaveResponse
        /// Created By          :   Naveen Kumar
        /// Created On          :   22/04/2016
        /// Modification Made   :   ****************************
        /// Modified On       :   30/05/2016
        /// </summary>
        /// <param name="cnTrans"></param>
        /// <param name="transId"></param>
        /// <returns>Iso8583Response object</returns>
        public static Iso8583Response ParseAndSaveResponse(CnTransactionEntities cnTrans, int transId)
        {
            ILogger logger = new Logger();
            StringBuilder log = new StringBuilder();

            log.AppendLine(string.Format("ResponseHandler.ParseAndSaveResponse(). transId:{0};CompleteResponse:{1};TransType:{2};IsCapKey:{3};IsTOR:{4};",
                transId, cnTrans.CompleteResponse, cnTrans.TransType, cnTrans.IsCapKey, cnTrans.IsTORTransaction));

            try
            {
                Iso8583Response objIso8583Response = new Iso8583Response();
                if (!string.IsNullOrEmpty(cnTrans.CompleteResponse))
                {
                    Response objResponse = Serialization.Deserialize<Response>(cnTrans.CompleteResponse);
                    if (!object.Equals(objResponse.StatusTypeField, null) && !object.Equals(objResponse.StatusTypeField.StatusCode, null))
                    {
                        log.AppendLine(string.Format("StatusTypeField.StatusCode:{0};StatusTypeField.Value:{1};", objResponse.StatusTypeField.StatusCode, objResponse.StatusTypeField.Value));

                        if (!object.Equals(objResponse.TransactionResponseField, null) && !object.Equals(objResponse.TransactionResponseField.ReturnCode, null))
                        {
                            log.AppendLine(string.Format("TransactionResponseField.ReturnCode:{0};", objResponse.TransactionResponseField.ReturnCode));

                            objIso8583Response.ReturnCode = objResponse.TransactionResponseField.ReturnCode;

                            if (!string.IsNullOrEmpty(objResponse.TransactionResponseField.Payload))
                            {
                                log.AppendLine(string.Format("TransactionResponseField.Payload:{0};", objResponse.TransactionResponseField.Payload));

                                objIso8583Response = FillIso8583Response(objResponse.TransactionResponseField.Payload, cnTrans.IsCapKey);
                            }
                            else
                            {
                                log.Append("objResponse.TransactionResponseField.Payload == <NULL>");
                            }
                        }

                        objIso8583Response.StatusCode = objResponse.StatusTypeField.StatusCode;
                    }
                    else
                    {
                        log.Append("objResponse.StatusTypeField == <NULL> or objResponse.StatusTypeField.StatusCode == <NULL>");
                    }
                }

                objIso8583Response.CardType = cnTrans.CardIssuer.ToString();
                objIso8583Response.PaymentType = cnTrans.CardType.ToString();
                objIso8583Response.TransactionType = cnTrans.TransType;
                objIso8583Response.CompleteResponse = cnTrans.CompleteResponse;
                objIso8583Response.TransactionId = transId;
                
                if (objIso8583Response.ResponseCode == PaymentAPIResources.ResponseCode00)
                    objIso8583Response.Message = PaymentAPIResources.Approval;

                objIso8583Response.IsTORTransaction = cnTrans.IsTORTransaction;

                log.AppendLine(string.Format("objIso8583Response.ResponseCode:{0};objIso8583Response.Message:{1};", objIso8583Response.ResponseCode, objIso8583Response.Message));
                logger.LogInfoMessage(log.ToString());

                return objIso8583Response;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieve ISO8583 response and set the responded Bitmap values.
        /// </summary>
        /// <param name="payloadData"></param>
        /// <returns>objResponse</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        private static Iso8583Response FillIso8583Response(string payloadData, bool isCapkey = false)
        {
            ILogger logger = new Logger();
            StringBuilder log = new StringBuilder();

            log.AppendLine(string.Format("ResponseHandler.FillIso8583Response(). payloadData:{0};isCapKey:{1};", payloadData, isCapkey));

            Iso8583Response objResponse;
            try
            {
                // Format the payload in Hex form.
                var payLoadInHex = FormatPayLoad(payloadData);

                log.AppendLine(string.Format("payLoadInHex:{0};", payLoadInHex));

                // Remove Message ID from Payload.
                var payloadWithoutMsgID = payLoadInHex.Substring(4);

                log.AppendLine(string.Format("payloadWithoutMsgID:{0};", payloadWithoutMsgID));

                // Get the Bitmap
                var bitmap = GetBitmap(payloadWithoutMsgID);

                log.AppendLine(string.Format("bitmap:{0};", bitmap));

                // Convert Bitmap from hex to Binary
                var binaryBitmap = bitmap.Aggregate(new StringBuilder(), (builder, c) =>
                                builder.Append(Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, SigmaStaticValue.Zero))).ToString();

                log.AppendLine(string.Format("binaryBitmap:{0};", binaryBitmap));

                // To get Binary bitmap into char array 
                char[] bitarr = binaryBitmap.ToArray();
                
                // To get the ID of Elements set as per Bitmap
                var elementIndices = bitarr.Select((val, indx) => new { val, indx })
                                    .Where(v => v.val.Equals('1'))
                                    .Select(i => i.indx + 1).ToArray();

                log.AppendLine(string.Format("elementIndices.Length:{0};", elementIndices.Length));

                if (elementIndices.Length > 0)
                {
                    foreach (int e in elementIndices)
                    {
                        log.AppendLine(string.Format("  element:{0};", e));
                    }
                }

                // Remove the bitmap from the payload.
                var payloadWithoutBitmap = payloadWithoutMsgID.Substring(bitmap.Length);

                log.AppendLine(string.Format("payloadWithoutBitmap:{0};", payloadWithoutBitmap));

                objResponse = new Iso8583Response()
                {
                    ResponseMessageID = payLoadInHex.Substring(0, 4),
                    Bitmap = bitmap
                };

                log.AppendLine(string.Format("objResponse.ResponseMessageID:{0};objResponse.Bitmap:{1};", objResponse.ResponseMessageID, objResponse.Bitmap));

                objResponse = SetResponseValues(objResponse, elementIndices, payloadWithoutBitmap, isCapkey);

                logger.LogInfoMessage(log.ToString());
            }
            catch
            {
                throw;
            }

            return objResponse;
        }

        /// <summary>
        /// Set the payload in Hexadecimal format.
        /// </summary>
        /// <param name="Payload"></param>
        /// <returns>string result</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        private static string FormatPayLoad(string Payload)
        {
            var result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(Payload))
                {
                    var payloadArr = Payload.Split(SigmaStaticValue.Pipe);

                    for (int i = 0; i < payloadArr.Length; i++)
                    {
                        var item = payloadArr[i].ToString();
                        if (item.Length > 2)
                        {
                            var ASCIIItem = item.Substring(2);// first two char will be hex in Elements list.
                            payloadArr[i] = item.Substring(0, 2) + CnGenericHandler.ConvertToHex(ASCIIItem); //Append the Converted ASCII To Hex value to item                     
                        }
                    }

                    result = string.Join(string.Empty, payloadArr);
                }
            }
            catch
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Check the presence of primary/secondary bitmap.
        /// Then retrieve the bitmap from responded payload.
        /// </summary>
        /// <param name="hexPayloadText"></param>
        /// <returns>string result</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        private static string GetBitmap(string hexPayloadText)
        {
            string result = string.Empty;
            try
            {
                var initialBitmapBit = hexPayloadText.Substring(0, 1);
                var initialBinaryBit = Convert.ToString(Convert.ToInt32(initialBitmapBit, 16), 2).PadLeft(4, SigmaStaticValue.Zero);

                if (initialBinaryBit.StartsWith(Convert.ToString(SigmaStaticValue.Zero)))
                    result = hexPayloadText.Substring(0, 16);
                else
                    result = hexPayloadText.Substring(0, 32);
            }
            catch
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Set the responded Bitmap value from the payload.
        /// </summary>
        /// <param name="objResponse"></param>
        /// <param name="elementIndices"></param>
        /// <param name="payload"></param>
        /// <returns>Iso8583Response objResponse</returns>
        /// <CreatedBy>Naveen Kumar</CreatedBy>
        private static Iso8583Response SetResponseValues(Iso8583Response objResponse, int[] elementIndices, string payload, bool isCapKey = false)
        {
            ILogger logger = new Logger();
            StringBuilder log = new StringBuilder();

            log.AppendLine(string.Format("ResponseHandler.SetResponseValues(). payload:{0};isCapKey:{1};", payload, isCapKey));

            try
            {
                var objResponseProperties = objResponse.GetType().GetProperties();
                foreach (var item in objResponseProperties)
                {
                    log.AppendLine(string.Format("item:{0};", item.Name));

                    // Get bitmap properties by name in response class
                    var objElementProperties = typeof(CnBitmapDefinition).GetProperty(item.Name);
                    if (objElementProperties != null)
                    {
                        var objElementValue = (Element)objElementProperties.GetValue(null);

                        log.AppendLine(string.Format("objElementValue.ElementID:{0};", objElementValue.ElementID));

                        if (elementIndices.Contains(objElementValue.ElementID))
                        {
                            log.AppendLine(string.Format("elementIndices contains ElementID:{0};LengthType:{1};LengthMax:{2};LengthIndicator:{3};",
                                objElementValue.ElementID, objElementValue.LengthType, objElementValue.LengthMax, objElementValue.LentghIndicator));

                            int elementSize = 0;    // To store the length of each bitmap data.
                            if (objElementValue.LengthType.Equals(LengthType.Fixed))
                            {
                                // If length is odd number, make it even as 0 was added to make a full byte.
                                if (objElementValue.LengthMax % 2 == 0)
                                    elementSize = objElementValue.LengthMax; // Set the length of the Element's data.
                                else
                                    elementSize = objElementValue.LengthMax + 1;
                              
                                if (objElementValue.Format.Equals(FormatType.ASCII))
                                    elementSize = objElementValue.LengthMax * 2;
                            }
                            else if (objElementValue.LengthType.Equals(LengthType.Variable))
                            {
                                int indicatorSize = objElementValue.LentghIndicator * 2;    // Set the length indicator size.

                                if (objElementValue.ElementID.Equals(63))
                                {
                                    var lengthIndicator = payload.Substring(0, indicatorSize).TrimStart(SigmaStaticValue.Zero);
                                    if (!string.IsNullOrEmpty(lengthIndicator))
                                        elementSize = Convert.ToInt32(lengthIndicator) * 2;
                                }
                                else
                                {
                                    // Changes done for EMV Contact certification of Canada
                                    if (objElementValue.ElementID.Equals(55))
                                    {
                                        var lengthIndicator = payload.Substring(0, indicatorSize).TrimStart(SigmaStaticValue.Zero);
                                        if (!string.IsNullOrEmpty(lengthIndicator))
                                            elementSize = Convert.ToInt32(lengthIndicator) * 2;
                                    }
                                    else
                                    {
                                        var lengthIndicator1 = payload.Substring(0, indicatorSize).TrimStart(SigmaStaticValue.Zero);

                                        if (!string.IsNullOrEmpty(lengthIndicator1))
                                        {
                                            if (!objElementValue.Format.Equals(FormatType.BCD) && !objElementValue.Format.Equals(FormatType.HD))
                                                elementSize = Convert.ToInt32(lengthIndicator1) * 2;
                                            else
                                                elementSize = Convert.ToInt32(lengthIndicator1);
                                        }
                                    }

                                    payload = payload.Substring(indicatorSize);
                                }
                            }

                            log.AppendLine(string.Format("payload:{0};elementSize:{1};", payload, elementSize));

                            var resultValue = payload.Substring(0, elementSize);

                            log.AppendLine(string.Format("resultValue:{0};", resultValue));

                            // For CapKey
                            if (objElementValue.ElementID.Equals(63) && isCapKey)
                            {
                                resultValue = payload.Substring(0, elementSize + 4);

                                log.AppendLine(string.Format("ElementID=63;resultValue:{0};", resultValue));
                            }

                            if (objElementValue.Format.Equals(FormatType.ASCII) || objElementValue.Format.Equals(FormatType.HD))
                            {
                                resultValue = CnGenericHandler.ConvertHexToAscii(resultValue);

                                log.AppendLine(string.Format("ASCII or HD;resultValue:{0};", resultValue));
                            }

                            item.SetValue(objResponse, resultValue);

                            payload = payload.Substring(elementSize);
                        }
                        else
                        {
                            log.AppendLine(string.Format("elementIndices DOES NOT contain ElementID:{0};", objElementValue.ElementID));
                        }
                    }
                    else
                    {
                        log.AppendLine("objElementProperties == <NULL>");
                    }
                }

                logger.LogInfoMessage(log.ToString());
            }
            catch
            {
                throw;
            }

            return objResponse;
        }

        ///// <summary>
        ///// Get the amount in double format from Datawire format.
        ///// </summary>
        ///// <param name="amount"></param>
        ///// <returns>double result</returns>
        ///// <CreatedBy>Naveen Kumar</CreatedBy>
        //private static double ConvertAmountToDouble(string amount)
        //{
        //    try
        //    {
        //        var result = 0.00;
        //        if (!string.IsNullOrEmpty(amount) && amount.Length > 2)
        //        {
        //            result = Convert.ToDouble(amount.Insert((amount.Length - 2), SigmaStaticValue.Dot));
        //        }
        //        return result;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
       
    }
}