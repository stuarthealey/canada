﻿using MTD.Core.DataTransferObjects;
using MTD.Core.Factory.Interface;
using MTD.Data.Data;


namespace MTD.Core.Factory
{
    public class AdminUserFactory : IAdminUserFactory
    {
        public User CreateAdminUser(UserDto adminUserDto)
        {
            User adminUser = new User
            {
                FName = adminUserDto.FName,
                LName = adminUserDto.LName,
                Email = adminUserDto.Email,
                Phone = adminUserDto.Phone,
                fk_Country = adminUserDto.fk_Country,
                fk_State = adminUserDto.fk_State,
                fk_City = adminUserDto.fk_City,
                Address = adminUserDto.Address,
                PCode = adminUserDto.PCode,
                IsActive = adminUserDto.IsActive,
                IsLockedOut = adminUserDto.IsLockedOut,
                ModifiedBy = adminUserDto.ModifiedBy,
                CreatedBy = adminUserDto.CreatedBy
            };
            return adminUser;
        }
    }
}




