﻿using MTData.Web.ViewData;

namespace MTData.Web.Models
{
    public class PaymentRequest
    {
        public Header Header { get; set; }

        public TransactionDetail TransactionDetail { get; set; }
    }
}