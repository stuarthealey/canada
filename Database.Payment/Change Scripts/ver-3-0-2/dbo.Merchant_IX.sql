-- Create indexes on Merchant for performance.
--============================================

IF NOT EXISTS (SELECT * FROM sysindexes si INNER JOIN sysobjects so ON so.id = si.id WHERE si.name = 'IX_MerchantActive' AND OBJECTPROPERTY(so.id, N'IsUserTable') = 1 AND so.name = 'Merchant')
BEGIN
	PRINT 'Creating Index IX_MerchantActive on Merchant'

	CREATE NONCLUSTERED INDEX IX_MerchantActive ON dbo.Merchant (IsActive, MerchantID)
		WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
ELSE
BEGIN
	PRINT 'Index IX_MerchantActive on Merchant already exists'
END
