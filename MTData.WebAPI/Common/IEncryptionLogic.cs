﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace MTData.WebAPI.Common
{
    /// <summary>
    /// 
    /// </summary>
    public interface IEncryptionLogic
    {
        string Encrypt(string plainText);

        bool TryDecrypt(string base64CipherText, out string plainText);
    }

}



