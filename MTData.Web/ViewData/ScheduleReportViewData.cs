﻿using MTData.Web.Resource;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace MTData.Web.ViewData
{
    public class ScheduleReportViewData
    {
        public int ScheduleID { get; set; }
        [Required(ErrorMessageResourceType = typeof(MtdataResource), ErrorMessageResourceName = "Please_enter_date_from")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime DateFrom { get; set; }
        [Required(ErrorMessageResourceType = typeof(MtdataResource), ErrorMessageResourceName = "Please_enter_date_to")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime? DateTo { get; set; }
        [Required(ErrorMessageResourceType = typeof(MtdataResource), ErrorMessageResourceName = "Please_select_frequency")]
        public Nullable<byte> Frequency { get; set; }
        [Required(ErrorMessageResourceType = typeof(MtdataResource), ErrorMessageResourceName = "Please_select_the_frequency_type")]
        public Nullable<byte> FrequencyType { get; set; }
        [Required(ErrorMessageResourceType = typeof(MtdataResource), ErrorMessageResourceName = "Please_enter_the_start_time")]
        public TimeSpan? StartTime { get; set; }
        [Required(ErrorMessageResourceType = typeof(MtdataResource), ErrorMessageResourceName = "Please_select_a_timezone")]
        public string TimeZone { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string WeekName { get; set; }
        public string FrequencyName { get; set; }
        public Nullable<int> WeekDay { get; set; }
        public Nullable<int> MonthDay { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Nullable<bool> IsCreated { get; set; }
        [Required(ErrorMessageResourceType = typeof(MtdataResource), ErrorMessageResourceName = "Please_select_a_report_name")]
        public Nullable<int> fk_ReportID { get; set; }
        public string DisplayName { get; set; }
        public IEnumerable<WeekDayViewData> DayList { get; set; }
        public IEnumerable<MerchantListViewData> MerchantList { get; set; }
        public IEnumerable<MerchantListViewData> AdminList { get; set; }
        public IEnumerable<ReportListViewData> ReportList { get; set; }
        public IEnumerable<MonthDayViewData> MonthDayList { get; set; }
        public IEnumerable<TimeZoneViewData> TimeZones { get; set; }
        public IEnumerable<FrequencyListViewData> FrequencyList { get; set; }
        public IEnumerable<FrequencyTypeViewData> FrequencyTyeList { get; set; }
        public Nullable<System.TimeSpan> DisplayStartTime { get; set; }
        public Nullable<System.DateTime> NextScheduleDateTime { get; set; }
        public int? fk_MerchantId { get; set; }
        public int UserType { get; set; }
        public Nullable<int> CorporateUserId { get; set; }
        public bool IsReport { get; set; }
    }
}