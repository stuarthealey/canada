﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MTData.Web.ViewData
{
    public class GetAdminWeeklySummaryCountViewData
    {
        public Nullable<long> Sr_No { get; set; }
        public string FleetName { get; set; }
        public Nullable<int> TransCountThisWeek { get; set; }
        public Nullable<int> TransCountWeekFirst { get; set; }
        public Nullable<int> TransCountWeekSecond { get; set; }
        public Nullable<int> TransCountWeekThird { get; set; }
        public Nullable<int> TransCountWeekFourth { get; set; }
        public Nullable<int> TransCountWeekFifth { get; set; }
        public Nullable<int> TransCountWeekSixth { get; set; }
    }
}