﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.WebAPI.Models;
using Ninject;

namespace MTData.WebAPI.Controllers
{
    public class GetTCCashieringController : ApiController
    {
        private readonly ITransactionService _transactionService;

        private readonly StringBuilder _logMessage;
        private StringBuilder _logTracking;
        readonly ILogger _logger = new Logger();
        public GetTCCashieringController(
            [Named("TransactionService")] ITransactionService transactionService,
            StringBuilder logMessage, StringBuilder logTracking)
        {
            _transactionService = transactionService;
            _logMessage = logMessage;
            _logTracking = logTracking;
        }

        /// <summary>
        /// Created By : Umesh K3
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult GetTCCashieringData(HttpRequestMessage request)
        {
            try
            {
                IEnumerable<DownloadDriverDataDto> dData = new List<DownloadDriverDataDto>();
                DownloadParametersDto dParam = new DownloadParametersDto();
                dParam.DriverId = "99999";
                dData = _transactionService.DownloadDriverData(dParam);
                return Ok(dData);
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
                return Ok();
            }
        }

    }
}
