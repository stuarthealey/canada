IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FDMerchant]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_FDMerchant]
GO

CREATE PROCEDURE dbo.usp_FDMerchant
(
	@merchantId INT = NULL
)
AS
BEGIN
	SELECT * 
	FROM dbo.FDMerchant fd 
		INNER JOIN dbo.Merchant m ON fd.FDId = m.Fk_FDId 
	WHERE MerchantID = @merchantId
END
GO
