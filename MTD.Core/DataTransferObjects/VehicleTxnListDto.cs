﻿namespace MTD.Core.DataTransferObjects
{
   public class VehicleTxnListDto
    {
        public string VehicleText { get; set; }
        public int FleetId { get; set; }
    }
}
