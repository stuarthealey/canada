﻿using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class CcNoPrefix
    {
        private decimal? surchargeAmount;

        [DataMember]
        public decimal? SurchargeAmount
        {
            get { return this.surchargeAmount ?? default(decimal); }
            set { this.surchargeAmount = value; }
        }

        private decimal? surchargeMaxCap;

        [DataMember]
        public decimal? SurchargeMaxCap
        {
            get { return this.surchargeMaxCap ?? default(decimal); }
            set { this.surchargeMaxCap = value; }
        }
            
    }
}