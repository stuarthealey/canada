IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FdTerminal]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_FdTerminal]
GO

CREATE PROCEDURE dbo.usp_FdTerminal
(
	@deviceId VARCHAR(400)
)
AS
BEGIN
	SELECT * 
	FROM dbo.Terminal 
	WHERE MacAdd = @deviceId 
	  AND IsActive = 1
END
GO
