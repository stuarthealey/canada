﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table48
    {
        #region Private Constants and Variables
        #region Constants
        private const string tblId = "|34|38";
        #endregion

        #region Variables
        //The terminal will prompt for the sales tax when Visa indicates that the card is a commercial card.
        //Business, corporate and purchasing cards are all a subset of commercial cards.
        private string defaultTaxAmountCapable = "T";//T is suggested by FirstData as commercial cards will be used.
        #endregion
        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'34 38' Table 48
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        /// <summary>
        /// gets or sets the value of TaxAmountCapable
        /// </summary>
        public string TaxAmountCapable
        {
            get { return defaultTaxAmountCapable; }
            set { defaultTaxAmountCapable = value; }
        }
        #endregion
    }
}