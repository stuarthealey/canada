IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FirstDataDetails]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_FirstDataDetails]
GO

CREATE PROCEDURE dbo.usp_FirstDataDetails
-- =============================================
-- Author:		<Chetu>
-- Create date: <15-06-2015>
-- Description:	<First data details>
-- =============================================
--	S.Healey	2016-11-03		- T#6488 Additional paramater for @IncludeDatawire, to handle UK region.
(
	@deviceId VARCHAR(100) = NULL,
	@driverNumber VARCHAR(100) = NULL,
	@token VARCHAR(MAX) = NULL,
	@fleetId INT = NULL,
	@isDispatchRequest BIT = NULL,
	@IncludeDatawire BIT = 1
)
AS
BEGIN
	IF (@fleetId IS NOT NULL) AND (@token IS NOT NULL) -- Driver Validation with FleetID and TOken,driverNo
	BEGIN
		IF EXISTS (SELECT 1 FROM dbo.DriverLogin WHERE DriverNo = @driverNumber AND FleetId = @fleetId AND token = @token AND IsDispatchRequest = @isDispatchRequest)
		BEGIN
			EXEC dbo.usp_GetFirstDataValue @deviceId = @deviceId, @ResultType = '1', @IncludeDatawire = @IncludeDatawire
		END
	END
	ELSE IF ((@fleetId IS NULL) AND (@token IS NOT NULL)) --Validating driver by  DriverNo and Token [Backward compatibility]
	BEGIN
		IF EXISTS (SELECT 1 FROM dbo.DriverLogin WHERE DriverNo = @driverNumber AND token=@token AND IsDispatchRequest = 0)
		BEGIN
			EXEC dbo.usp_GetFirstDataValue @deviceId = @deviceId, @ResultType = '1', @IncludeDatawire = @IncludeDatawire
		END
	END
	ELSE IF (@token IS NULL)
	BEGIN
		EXEC dbo.usp_GetFirstDataValue @deviceId = @deviceId, @ResultType = '1', @IncludeDatawire = @IncludeDatawire
	END
	ELSE
	BEGIN
		EXEC dbo.usp_GetFirstDataValue @deviceId = @deviceId, @ResultType = '0', @IncludeDatawire = @IncludeDatawire -- returns only schema
	END
END
GO
