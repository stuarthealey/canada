﻿using System;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

/* The below code will prepare Debit Sale transaction request object populating various 
 * transaction parameters. The parameter values used below are test data and should not used for
 * actual real-time authorization. 
 * */
namespace GlobalMessageFormatter
{
    public class PinLessDebit
    {
        GMFMessageVariants gmfMsgVar = new GMFMessageVariants();
        PinlessDebitRequestDetails debitReq = new PinlessDebitRequestDetails();

        public PinLessDebit()
        {
            /* Based on the GMF Specification UMF_Schema_V1.1.14.xsd, fields that are mandatory or related to 
               this transaction should be populated.*/

            #region Common Group
            /* Populate values for Common Group */
            CommonGrp cmnGrp = new CommonGrp();

            /* The payment type of the transaction. */
            cmnGrp.PymtType = PymtTypeType.PLDebit;
            cmnGrp.PymtTypeSpecified = true;

            /* The type of transaction being performed. */
            cmnGrp.TxnType = TxnTypeType.Sale;
            cmnGrp.TxnTypeSpecified = true;

            /* The local date and time in which the transaction was performed. */
            cmnGrp.LocalDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");

            /* The transmission date and time of the transaction (in GMT/UCT). */
            cmnGrp.TrnmsnDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");

            /* A number assigned by the merchant to uniquely reference the transaction. 
             * This number must be unique within a day per Merchant ID per Terminal ID. */
            cmnGrp.STAN = "611489";

            /* A number assigned by the merchant to uniquely reference a set of transactions. 
             * sThis number must be unique within a day for a given Merchant ID/ Terminal ID. */
            cmnGrp.RefNum = "813927000521";

            /* Order number of the transaction
             */
            cmnGrp.OrderNum = "249429379225";

            /* An ID assigned by First Data, for the Third Party Processor or 
             * Software Vendor that generated the transaction. */
            cmnGrp.TPPID = "RMT001";            //This is dummy value. Please use the actual value

            /* A unique ID assigned to a terminal. */
            cmnGrp.TermID = "00000001";	//This is dummy value. Please use the actual value

            /* A unique ID assigned by First Data, to identify the Merchant. */
            cmnGrp.MerchID = "9058";     //This is dummy value. Please use the actual value       

            /* An identifier used to indicate the terminal’s account number entry mode 
             * and authentication capability via the Point-of-Service. */
            cmnGrp.POSEntryMode = "010";

            /* An identifier used to indicate the authorization conditions at the Point-of-Service (POS). */
            cmnGrp.POSCondCode = POSCondCodeType.Item59;
            cmnGrp.POSCondCodeSpecified = true;

            /* An identifier used to describe the type of terminal being used for the transaction. */
            cmnGrp.TermCatCode = TermCatCodeType.Item00;
            cmnGrp.TermCatCodeSpecified = true;

            /* An identifier used to indicate the entry mode capability of the terminal. */
            cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item00;
            cmnGrp.TermEntryCapabltSpecified = true;

            /* The amount of the transaction. This may be an authorization amount, 
             * adjustment amount or a reversal amount based on the type of transaction. 
             * It is inclusive of all additional amounts. 
             * It is submitted in the currency represented by the Transaction Currency field.  
             * The field is overwritten in the response for a partial authorization. */
            cmnGrp.TxnAmt = "000000002400";

            /* The numeric currency of the Transaction Amount. */
            cmnGrp.TxnCrncy = "840";

            /* An indicator that describes the location of the terminal. */
            cmnGrp.TermLocInd = TermLocIndType.Item1;
            cmnGrp.TermLocIndSpecified = true;

            /* Indicates whether or not the terminal has the capability to capture the card data. */
            cmnGrp.CardCaptCap = CardCaptCapType.Item0;
            cmnGrp.CardCaptCapSpecified = true;

            /* Indicates Group ID. */
            cmnGrp.GroupID = "20001"; //Group ID value will be assigned by First Data.	//This is dummy value. Please use the actual value

            debitReq.CommonGrp = cmnGrp;
            #endregion

            #region Card Group
            /* Populate values for Card Group */
            CardGrp crdGrp = new CardGrp();
            /*Track 2 data*/
            crdGrp.AcctNum = "4017779991111115";
            crdGrp.CardExpiryDate = "20160430";
            //crdGrp.Track2Data = "4017779999999011=16041011000013345678";
            debitReq.CardGrp = crdGrp;
            #endregion

            //#region PIN Group
            ///* Populate values for PIN Group */
            //PINGrp pinGroup = new PINGrp();

            ///* The PIN Data for the Debit or EBT transaction being submitted.
            // * HEXADecimal value need to be entered. */
            //pinGroup.PINData = "7C253F3622D7732D";

            ///* Provides the initialization vector for DUKPT PIN Debit and EBT transactions. */
            //pinGroup.KeySerialNumData = "F876543210000CC0000B";

            //debitReq.PINGrp = pinGroup;
            //#endregion

            //#region Additional Amount Group
            ///*  Populate values for Additional Amount Group */
            //AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();

            ///* An identifier used to indicate whether or not the 
            // * terminal/software can support partial authorization approvals.  */
            //addAmtGrp.PartAuthrztnApprvlCapablt = PartAuthrztnApprvlCapabltType.Item1;
            //addAmtGrp.PartAuthrztnApprvlCapabltSpecified = true;

            ///* Creating a generic array of Additional Amount 
            // * Group type to sent the data to as an array */
            //AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[2];
            //addAmtGrpArr[0] = addAmtGrp;

            //debitReq.AddtlAmtGrp = addAmtGrpArr;
            //#endregion

            #region Ecommerce Group

            EcommGrp ecomgrp = new EcommGrp();
            ecomgrp.EcommTxnInd = EcommTxnIndType.Item03;
            ecomgrp.EcommTxnIndSpecified = true;

            ecomgrp.EcommURL = "www.ex.com";
            debitReq.EcommGrp = ecomgrp;

            #endregion


            /* Add the data populated object to GMF message variant object */
            gmfMsgVar.Item = debitReq;
        }

        /* Generate Client Ref Number in the format <STAN>|<TPPID>, right justified and left padded with "0" */
        public string GetClientRef()
        {
            string clientRef = string.Empty;

            PinlessDebitRequestDetails debitReq = gmfMsgVar.Item as PinlessDebitRequestDetails;
            clientRef = debitReq.CommonGrp.STAN + "|" + debitReq.CommonGrp.TPPID;
            clientRef = "00" + clientRef;

            return clientRef;
        }

        /* The method will convert the GMF transaction object into an XML string */
        public String GetXMLData()
        {
            string xmlString = null;
            MemoryStream memoryStream = new MemoryStream();
            XmlSerializer xs = new XmlSerializer(gmfMsgVar.GetType());
            XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, System.Text.Encoding.UTF8);
            xs.Serialize(xmlTextWriter, gmfMsgVar);
            memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            xmlString = encoding.GetString(memoryStream.ToArray());
            xmlString = xmlString.Substring(1, xmlString.Length - 1);
            return xmlString;
        }
    }
}
