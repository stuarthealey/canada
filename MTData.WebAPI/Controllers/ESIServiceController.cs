﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using MTD.Core.Service.Interface;
using MTData.Utility;
using MTData.WebAPI.Common;
using MTData.WebAPI.ESIModels;
using MTData.WebAPI.Models;
using Ninject;

namespace MTData.WebAPI.Controllers
{
    public  class ESIWebServiceController : BaseController
    {
        private readonly ITransactionService _transactionService;
        private readonly ITransactionServiceUK _transactionServiceUk;
        private LocalVariables objLocalVars = new LocalVariables();
        private readonly StringBuilder _logMessage;
        private readonly ICommonService _commonService;
        private StringBuilder _logTracking;
        readonly ILogger _logger = new Logger();

        

        public ESIWebServiceController(
           [Named("TransactionService")] ITransactionService transactionService, [Named("TerminalService")] ITerminalService terminalService, StringBuilder logMessage, StringBuilder logTracking,
            [Named("UkTxnService")] ITransactionServiceUK transactionServiceUK,
             [Named("CommonService")] ICommonService commonService
            )
            : base(terminalService, logMessage)
        {
            _transactionService = transactionService;
            _logMessage = logMessage;
            _logTracking = logTracking;
            _transactionServiceUk = transactionServiceUK;
            _commonService = commonService;
        }

        public IHttpActionResult Authenticate(HttpRequestMessage request,string systemId, string userName, string userPwd)
        {
           
            try
            {
                ESIAuthenticateResult loginResult = new ESIAuthenticateResult();
                loginResult.Error = Resources.EsiService.None;
                loginResult.Succeeded = true;
                loginResult.Token = "C2skABqIu2PqwBSxiSnWheWTt40=";
                return Ok(loginResult);
            }
            catch (Exception ex)
            {
               // _logger.LogInfoFatel(_logMessage.ToString(), ex);
                //response.Header = CommonFunctions.FailedResponse(PaymentAPIResources.Failed, objLocalVars._deviceId, objLocalVars._requestId, status: "Failed");
                return Ok();
            }
            return Ok();
        }
    }
}
