﻿using System;
using System.Collections.Generic;

namespace MTD.Core.DataTransferObjects
{
   public class RecipientDto
   {
       public int RecipientID { get; set; }
       public int? fk_MerchantID { get; set; }
       public int? fk_UserID { get; set; }
       public bool IsRecipient { get; set; }
       public int ReportId { get; set; }
       public string Company { get; set; }
       public string ModifiedBy { get; set; }
       public DateTime? ModifiedDate { get; set; }
       public IEnumerable<MerchantListDto> MerchantList { get; set; }
    }
}
