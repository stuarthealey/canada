﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MTD.Data.Data
{
  public class ReceiptFormat
    {
        public int FleetID { get; set; }
        public int fk_MerchantID { get; set; }
        public string FleetName { get; set; }
        public string Description { get; set; }

       public IEnumerable<FleetReceipt> fleetReceipt { get; set; }
        public IEnumerable<ReceiptMaster> recceiptMaster { get; set; }
        public int FleetReceiptID { get; set; }
        public int fk_FleetID { get; set; }
        public int fk_FieldID { get; set; }
        public string fk_FieldText { get; set; }
        public int Postion { get; set; }
        public Nullable<bool> IsShow { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public IEnumerable<Fleet> fleetlist { get; set; }
      
    }
}
