﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MTData.Web.ViewData
{
    public class MerchantUserViewData
    {
        //Properties of User
        public int UserID { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [StringLength(50, ErrorMessage = "Up to 50 characters are allowed")]
        [Display(Name = "First Name")]
        public string FName { get; set; }
        
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Last Name")]
        [StringLength(50, ErrorMessage = "Up to 50 characters are allowed")]
        public string LName { get; set; }
        
        [Required]
        [Display(Name = "Email")]
        [StringLength(40, ErrorMessage = "Up to 40 characters are allowed")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Email is not valid")]
        public string Email { get; set; }
        
        [Required]
        [DataType(DataType.PhoneNumber)]
        [StringLength(18, ErrorMessage = "Up to 18 numbers are allowed")]
        [MinLength(6, ErrorMessage = "Phone  must be minimum length of 6")]
        [Display(Name = "Phone")]
        public string Phone { get; set; }
        
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Country")]
        [StringLength(30, ErrorMessage = "Up to 50 characters are allowed")]
        public string Country { get; set; }
        
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "State")]
        [StringLength(30, ErrorMessage = "Up to 50 characters are allowed")]        
        public string State { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Up to 50 characters are allowed")]
        [Display(Name = "City")]
        [DataType(DataType.Text)]
        public string City { get; set; }
        
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Address")]
        [StringLength(50, ErrorMessage = "Up to 50 characters are allowed")]
        public string Address { get; set; }
        
        public int isPersonalUpdate { get; set; }
        public string PCode { get; set; }

        public TCViewData TCondition { get; set; }
        public string ConfirmPCode { get; set; }
        public IEnumerable<FDMerchantListViewData> FdMerchantList { get; set; }
        public int fk_UserTypeID { get; set; }
        public Nullable<int> fk_MerchantID { get; set; }
        public Nullable<int> DriverTaxNo { get; set; }
        public bool IsTest { get; set; }
        public Nullable<bool> IsActivated { get; set; }
        public bool IsLockedOut { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        
        public int MerchantID { get; set; }
        
        [Required]
        [DataType(DataType.Text)]
        [StringLength(50, ErrorMessage = "Up to 50 characters are allowed")]
        [Display(Name = "Company")]
        public string Company { get; set; }
        
        public Nullable<decimal> SetupFee { get; set; }
        public Nullable<decimal> MonthlyFee { get; set; }
        
        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Disclaimer")]
        public string Disclaimer { get; set; }
        
        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Terms and Conditions")]
        public string TermCondition { get; set; }
        
        [Required]
        [Range(typeof(decimal), "0", "100")]
        [Display(Name = "State Tax Rate")]
        public Nullable<decimal> StateTaxRate { get; set; }
        
        public Nullable<decimal> StateTaxMax { get; set; }
        
        [Required]
        [Range(typeof(decimal), "0", "100")]
        [Display(Name = "Federal Tax Rate")]
        public Nullable<decimal> FederalTaxRate { get; set; }
        
        public Nullable<decimal> FederalTaxMax { get; set; }
        
        [Required]
        [Display(Name = "Tip low")]
        public Nullable<decimal> TipPerLow { get; set; }
        
        [Required]
        [Display(Name = "Tip medium")]
        public Nullable<decimal> TipPerMedium { get; set; }
        
        [Required]
        [Display(Name = "Tip high")]
        public Nullable<decimal> TipPerHigh { get; set; }
        
        public bool IsActive { get; set; }
        public Nullable<bool> IsAllowSale { get; set; }
        public Nullable<bool> IsAllowPreauth { get; set; }
        public Nullable<bool> IsAllowCapture { get; set; }
        public Nullable<bool> IsAllowRefund { get; set; }
        public Nullable<bool> IsAllowVoid { get; set; }
        public Nullable<int> Fk_FDId { get; set; }
        
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Company Tax Number")]
        [StringLength(18, ErrorMessage = "Up to 18 characters are allowed")]
        public string CompanyTaxNumber { get; set; }
        
        public string RolesAssigned { get; set; }
        public IEnumerable<RoleViewData> UserRoles { get; set; }
        public List<int> UserRoleIdList { get; set; }
        public IEnumerable<CountryViewData> CountryList { get; set; }
        public IEnumerable<StateViewData> StateList { get; set; }
        public IEnumerable<CityViewData> CityList { get; set; }
        public bool IsTaxInclusive { get; set; }
        public string TokenType { get; set; }
        public string MCCode { get; set; }
        public string EncryptionKey { get; set; }
        public bool IsOtherDetailSaved { get; set; }
        public bool IsFirstTime = false;
        public string DisclaimerPlainText { get; set; }
        public string TermConditionPlainText { get; set; }
        public bool IsJobNoRequired { get; set; }
        public string TimeZone { get; set; }
        public IEnumerable<TimeZoneViewData> TimeZones { get; set; }
        public string ZipCode { get; set; }

        public bool UseTransArmor { get; set; }      //PAY-13
    }

    class UserRoles
    {
        public string RoleName { get; set; }
        public int RoleId { get; set; }
    }
}