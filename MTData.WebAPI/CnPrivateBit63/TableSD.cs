﻿namespace MTData.WebAPI.CnPrivateBit63
{
    public class TableSD
    {
        #region Constants

        private const string tblId = "SD";
        private const string tagTC = "TC";
        private const string tagTCDataLength = "015";
        private const string terminalTypeCapabilty = "700111100000000";

        private string tagLN = string.Empty;

        #endregion

        #region Variables

        private string laneNumber = string.Empty;
        
        #endregion

        #region Public Properties
        
        /// <summary>
        /// ID of Table
        /// default SD in Request
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        public string TagTC
        {
            get { return tagTC; }
        }

        public string TagTCDataLength
        {
            get { return tagTCDataLength; }
        }

        public string TerminalTypeCapabilty
        {
            get { return terminalTypeCapabilty; }
        }

        public string TagLN
        {
            get { return tagLN; }
            set { tagLN = value; }
        }

        #endregion
    }
}