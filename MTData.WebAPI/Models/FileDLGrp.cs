﻿namespace MTData.WebAPI.Models
{
    public class FileDLGrp
    {
        public string FunCode { get; set; }
        public string CurrFileCreationDt { get; set; }
        public string FileSize { get; set; }
        public string FileCRC16 { get; set; }
        public string ReqFileOffset { get; set; }
        public string FBData { get; set; }
        public string NextFileDLOffset { get; set; }
        public string FBSeq { get; set; }
    }
}