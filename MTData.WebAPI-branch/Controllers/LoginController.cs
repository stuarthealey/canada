﻿using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using MTData.Utility.Extension;
using MTData.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using MTD.Core.DataTransferObjects;
using Ninject;
using MTD.Core.Service.Interface;
using MTData.Utility.Email;
using MTData.Utility.Logging;
using MTData.Utility.Logging.InLogger;
namespace MTData.WebAPI.Controllers
{
    public class LoginController : ApiController
    {
        readonly ILogger _logger = new Logger();
        private readonly IDriverService _driverService;
        private readonly ITerminalService _terminalService;
        private readonly IFleetService _fleetService;
        private readonly IMerchantService _merchantService;
        private readonly IUserService _userService;
        private readonly IReceiptService _receiptService;
        private readonly IVehicleService _vehicleService;
        private readonly StringBuilder _logMessage;

        public LoginController([Named("DriverService")] IDriverService driverService, [Named("TerminalService")] ITerminalService terminalService,
            [Named("FleetService")] IFleetService fleetService, [Named("MerchantService")] IMerchantService merchantService, [Named("UserService")] IUserService userService,
            [Named("ReceiptService")] IReceiptService receiptService, [Named("VehicleService")] IVehicleService vehicleService, StringBuilder logMessage)
        {
            _driverService = driverService;
            _terminalService = terminalService;
            _fleetService = fleetService;
            _merchantService = merchantService;
            _userService = userService;
            _receiptService = receiptService;
            _vehicleService = vehicleService;
            _logMessage = logMessage;
        }

        /// <summary>
        /// Purpose             :   Android login and sending receipt information
        /// Function Name       :   SignIn
        /// Created By          :   Salil Gupta
        /// Created On          :   12/15/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/01/2015 "MM/DD/YYYY"
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult SignIn(HttpRequestMessage request)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                               MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            _logMessage.Append("creating an object of LoginHeader Class");
            LoginHeader mHeader = new LoginHeader();
            _logMessage.Append("object created succussfgully of LoginHeader Class as mHeader");
            _logMessage.Append("creating an object of LoginReceipt Class");
            LoginReceipt mReceipt = new LoginReceipt();
            _logMessage.Append("object created succussfgully of LoginReceipt Class as mReceipt");
            _logMessage.Append("creating an object of LoginCompany Class");
            LoginCompany mCompany = new LoginCompany();
            _logMessage.Append("object created succussfgully of LoginCompany Class as mCompany");
            _logMessage.Append("creating an object of LoginTips Class");
            LoginTips mTips = new LoginTips();
            _logMessage.Append("object created succussfgully of LoginTips Class as mTips");
            _logMessage.Append("creating an object of LoginSurcharge Class");
            LoginSurcharge mSurcharge = new LoginSurcharge();
            _logMessage.Append("object created succussfgully of LoginSurcharge Class as mSurcharge");
            _logMessage.Append("creating an object of CCNoPrefix Class");
            CcNoPrefix mCcNoPrefix = new CcNoPrefix();
            _logMessage.Append("object created succussfgully of mCcNoPrefix Class as mCcNoPrefix");
            _logMessage.Append("creating an object of LoginTaxes Class");
            LoginTaxes mTaxes = new LoginTaxes();
            _logMessage.Append("object created succussfgully of LoginTaxes Class as mTaxes");
            _logMessage.Append("creating an object of LoginResponse Class");
            LoginResponse response = new LoginResponse();
            _logMessage.Append("object created succussfgully of LoginResponse Class as response");
            try
            {
                _logMessage.Append("creating an object of XmlDocument Class");
                XmlDocument xmlDoc = new XmlDocument();
               _logMessage.Append("calling load method of xmlDoc class to get xml request from android device and load");
                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);
               _logMessage.Append("load method of xmlDoc class executed successfully");
               _logMessage.Append("calling SelectSingleNode method of xmlDoc class");
               var selectSingleNode = xmlDoc.SelectSingleNode("LoginRequest");

               _logMessage.Append("checking if parent node of requet xml is not null");
               if (selectSingleNode == null)
               {
                   mHeader.Message = "Invalid request format";
                   mHeader.Status = "Failed";
                   response.Header = mHeader;
                   return Ok(response);
               }
               _logMessage.Append("parent node of requet xml is not null");

               string requestId = xmlDoc.SelectSingleNode("LoginRequest/Header/RequestId").InnerText;
               string deviceId = xmlDoc.SelectSingleNode("LoginRequest/Header/DeviceID").InnerText;
               string driverId = xmlDoc.SelectSingleNode("LoginRequest/Login/DriverNumber").InnerText;
                string driverPin = xmlDoc.SelectSingleNode("LoginRequest/Login/Pin").InnerText;
               _logMessage.Append("Calling GetDriverDto method of _driverService class to get driver with driver id and pin are"+driverId+ "  and "+driverPin );
               DriverDto driver = _driverService.GetDriverDto(driverId, driverPin);
               _logMessage.Append("GetDriverDto method of _driverService executed successfully");
               _logMessage.Append("Calling GetTerminalDto method of _terminalService class to get driver with deviceId " + deviceId);
               TerminalDto terminal = _terminalService.GetTerminalDto(deviceId);
               _logMessage.Append("GetTerminalDto method of _terminalService executed successfully");
               if (driver == null || terminal == null)
               {
                   if (driver == null)
                   {
                       mHeader.Status = "Failed";
                       mHeader.DeviceId = deviceId;
                       mHeader.RequestId = requestId;
                       mHeader.Message = "Invalid login details, please try again.";
                       response.Header = mHeader;
                       return Ok(response);
                   }
                   _logMessage.Append("driver is not null");
                   mHeader.Status = "Failed";
                   mHeader.DeviceId = deviceId;
                   mHeader.RequestId = requestId;
                   mHeader.Message = "Invalid terminal configuration, please contact support.";
                   response.Header = mHeader;
                   return Ok(response);
               }
               int fleetId = driver.fk_FleetID;
               int mid = terminal.fk_MerchantID;
               int terminalId = terminal.TerminalID;
               _logMessage.Append("Calling GetVehicleByTerminal method of _vehicleService class to get terminal with terminalId " + terminalId);
               VehicleDto vehicle = _vehicleService.GetVehicleByTerminal(terminalId);
               _logMessage.Append("GetVehicleByTerminal method of _terminalService executed successfully");
            
               if (vehicle == null)
               {
                   mHeader.Status = "Failed";
                   mHeader.DeviceId = deviceId;
                   mHeader.RequestId = requestId;
                   mHeader.Message = "Invalid driver configuration, please contact support.";
                   response.Header = mHeader;
                   return Ok(response);
               }

               _logMessage.Append("vehicle is not null");
               _logMessage.Append("Calling GetMerchant method of _merchantService class to get terminal with mid " + mid);
               MerchantDto merchant = _merchantService.GetMerchant(mid);
               _logMessage.Append("GetMerchant method of _merchantService executed successfully");

              
               if (merchant == null)
               {
                   mHeader.Status = "Failed";
                   mHeader.DeviceId = deviceId;
                   mHeader.RequestId = requestId;
                   mHeader.Message = "Invalid driver configuration, please contact support.";
                   response.Header = mHeader;
                   return Ok(response);
               }
               _logMessage.Append("merchant is not null");
               _logMessage.Append("Calling GetUser method of _userService class to get terminal with mid " + mid+ " and 2");
               UserDto user = _userService.GetUser(mid, 2);
               _logMessage.Append("GetUser method of _userService executed successfully");

              
               if (user == null)
               {
                   mHeader.Status = "Failed";
                   mHeader.DeviceId = deviceId;
                   mHeader.RequestId = requestId;
                   mHeader.Message = "Invalid driver configuration, please contact support.";
                   response.Header = mHeader;
                   return Ok(response);
               }
               _logMessage.Append("user is not null");

               _logMessage.Append("Calling GetFleet method of _fleetService class to get fleet with fleetId " + fleetId);
               FleetDto fleet = _fleetService.GetFleet(fleetId);
               _logMessage.Append("GetFleet method of _fleetService executed successfully");
             
               if (fleet == null)
               {
                   mHeader.Status = "Failed";
                   mHeader.DeviceId = deviceId;
                   mHeader.RequestId = requestId;
                   mHeader.Message = "Invalid driver configuration, please contact support.";
                   response.Header = mHeader;
                   return Ok(response);
               }
               _logMessage.Append("fleet is not null");

               _logMessage.Append("Calling ReceiptByfleetId method of _receiptService class to get fleet with fleetId " + fleetId);
               List<ReceiptListDto> receipt = _receiptService.ReceiptByfleetId(fleetId).ToList();
               _logMessage.Append("ReceiptByfleetId method of _receiptService executed successfully");

               _logMessage.Append("making list of receiptInformation and getting information from recipet");
               List<ReceiptInfo> receiptInformation = receipt.Select(t => new ReceiptInfo
                {
                   Order = t.Postion, Show = t.IsShow, Name = t.FieldName.Trim(), Value = t.fk_FieldText.Trim()
               }).ToList();
               _logMessage.Append("making list of receiptInformation and getting information from recipet completed");
               
                //Creating token for android device
               string token=StringExtension.GenerateToken(driverId,driverPin);
               response.Token = token;
               mReceipt.ReceiptField = receiptInformation;
                    //request Header information
               mHeader.DeviceId = deviceId;
               mHeader.RequestId = requestId;
               mHeader.Status = "Success";
               mHeader.Message = "Successfully logged in";
               mCompany.Fleet = fleet.FleetName;
               mCompany.Vehicle = vehicle.VehicleNumber;
               mCompany.Driver = driver.DriverNo;
               mCompany.DriversTaxNumber = driver.DriverTaxNo;
               //mCompany.companyTaxNumber = merchant.CompanyTaxNumber;
               mCompany.CompanyName = merchant.Company;
               mCompany.CompanyAddress = user.Address;
               mCompany.CompanyPhoneNumber = user.Phone;
               mTips.TipPercentageLow = merchant.TipPerLow;
               mTips.TipPercentageMedium = merchant.TipPerMedium;
               mTips.TipPercentageHigh = merchant.TipPerHigh;
               mTaxes.FederalTax = merchant.FederalTaxRate;
               mTaxes.StateTax = merchant.StateTaxRate;
               response.Disclaimer = merchant.Disclaimer;
               response.TermsConditions = merchant.TermCondition;
              // response.IsTaxIncluded = merchant.IsTaxInclusive;
               response.Header = mHeader;
               response.ReceiptFormat = mReceipt;
               response.CompanyDetails = mCompany;
               response.Tips = mTips;
               mSurcharge.CardNumberPrefix = mCcNoPrefix;
               response.Surcharge = mSurcharge;
               response.Taxes = mTaxes;
               response.FleetId = Convert.ToString(fleetId);
               _logMessage.Append("returing response");
               return Ok(response);
            }
            catch (Exception ex)
            {
                //logger.LogException(ex);
                mHeader.Status = "Failed";
                mHeader.DeviceId = mHeader.DeviceId;
                mHeader.RequestId = mHeader.RequestId;
                mHeader.Message = ex.Message;
                response.Header = mHeader;
                _logger.LogInfoFatel(_logMessage.ToString(),ex);
                return Ok(response);

            }
        }

        /// <summary>
        /// Purpose             :   Sending Email to merchant customer
        /// Function Name       :   Email
        /// Created By          :   Salil Gupta
        /// Created On          :   01/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/01/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult Email(HttpRequestMessage request)
        {
            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                             MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            _logMessage.Append("creating an object of LoginHeader Class");
            LoginHeader mHeader = new LoginHeader();
            _logMessage.Append("object created succussfgully of LoginHeader Class as mHeader");

            _logMessage.Append("creating an object of MailResponse Class");
            var response = new MailResponse();
            _logMessage.Append("object created succussfgully of MailResponse Class as response");
           
            try
            {
                var xmlDoc = new XmlDocument();
                _logMessage.Append("calling load method of xmlDoc class to get xml request from android device and load");
                xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);
                _logMessage.Append("load method of xmlDoc class executed successfully");
                _logMessage.Append("calling SelectSingleNode method of xmlDoc class");
                _logMessage.Append("calling SelectSingleNode method of xmlDoc class");
                var selectSingleNode = xmlDoc.SelectSingleNode("MailRequest");
                _logMessage.Append("checking if parent node of requet xml is not null");
             
                if (selectSingleNode == null)
                {
                    mHeader.Message = "Invalid request format";
                    mHeader.Status = "Failed";
                    response.Header = mHeader;
                    return Ok(response);
                }
                _logMessage.Append("parent node of requet xml is not null");
                _logMessage.Append("recieving transaction information from android");
                string requestId = xmlDoc.SelectSingleNode("MailRequest/Header/RequestId").InnerText;
                string deviceId = xmlDoc.SelectSingleNode("MailRequest/Header/DeviceID").InnerText;
                string driverId = xmlDoc.SelectSingleNode("MailRequest/Header/DriverId").InnerText;
                string cMail = xmlDoc.SelectSingleNode("MailRequest/Header/CustomerMailId").InnerText;
                string cToken = xmlDoc.SelectSingleNode("MailRequest/Header/Token").InnerText;
                string cName = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/CompName").InnerText;
                string cPhone = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/CompPhone").InnerText;
                string cTaxNo = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/CompTaxNumber").InnerText;
                string cAddress = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/CompanyAddress").InnerText;
                string destAdd = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/DestAdd").InnerText;
                string disclaimer = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/Disclaimer").InnerText;
                string driverTaxNo = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/DriverTaxNo").InnerText;
                string extra = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/Extra").InnerText;
                string fare = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/Fare").InnerText;
                string federalTaxAmt = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/FederalTaxAmt").InnerText;
                string federalTaxPer = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/FederalTaxPer").InnerText;
                string flagfall = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/FlagFall").InnerText;
                int fleetId = Convert.ToInt32(xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/Fleet").InnerText);
                string pickAdd = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/PickAdd").InnerText;
                string stateMaxTax = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/StateMaxTax").InnerText;
                string stateTaxPer = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/StateTaxPer").InnerText;
                string subTotal = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/SubTotal").InnerText;
                string surAmt = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/SurAmt").InnerText;
                string surPer = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/SurPer").InnerText;
                string termsAndConditions = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/TermsAndConditions").InnerText;
                string tip = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/Tip").InnerText;
                string tolls = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/Tolls").InnerText;
                string total = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/Total").InnerText;
                string vehicle = xmlDoc.SelectSingleNode("MailRequest/ReceiptFormat/Vehicle").InnerText;
                _logMessage.Append("transaction information from android recieved successfully");
                var tablestring = string.Empty;
                _logMessage.Append("making html table for sending mail");
                tablestring = string.Format("{0}<table style='font-size:14px; font-family:verdana;border:1px black;height:500px;width:800px;'>", tablestring);

                _logMessage.Append("Calling ReceiptToShowByfleetId method of _receiptService class to get fleet with fleetId " + fleetId);

                bool isValid = _terminalService.IsValid(deviceId);
                string[] encArray = cToken.Split(':');
                string encDriverNo = encArray[1];
                string encDriverPin = encArray[2];
                string driverNo = StringExtension.Decrypt(encDriverNo);
                string driverPin = StringExtension.Decrypt(encDriverPin);
                bool isDriverValid = _driverService.IsDriverNoValid(driverNo,driverPin);
                if (cToken == string.Empty || isDriverValid == false || isValid == false)
                {
                    mHeader.Message = "Unauthorized access.";
                    mHeader.Status = "Failed";
                    mHeader.DeviceId = deviceId;
                    mHeader.RequestId = requestId;
                    response.Header = mHeader;
                    return Ok(response);
                }


                List<ReceiptListDto> receipt = _receiptService.ReceiptToShowByfleetId(fleetId).ToList();
                _logMessage.Append("ReceiptToShowByfleetId method of _receiptService executed successfully");

                if (receipt.Count == 0)
                {
                    mHeader.Message = "Fleet not found";
                    mHeader.Status = "Failed";
                    mHeader.DeviceId = deviceId;
                    mHeader.RequestId = requestId;
                    response.Header = mHeader;
                    return Ok(response);
                }
                _logMessage.Append("receipt is not null");
                _logMessage.Append("begin for loop to get reciept information and append data to mail table");
                foreach (ReceiptListDto t in receipt)
                {
                    switch (t.FieldName)
                    {
                        case "Company Name":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, cName);
                            break;
                        case "Company Phone":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, cPhone);
                            break;
                        case "Company Address":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, cAddress);
                            break;
                        case "Company Tax Number":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, cTaxNo);
                            break;
                        case "Driver":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, driverId);
                            break;
                        case "Driver Tax Number":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, driverTaxNo);
                            break;
                        case "Extra":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, extra);
                            break;
                        case "Fare":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, fare);
                            break;
                        case "Federal Tax Amount":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, federalTaxAmt);
                            break;
                        case "Federal Tax Percentage":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, federalTaxPer);
                            break;
                        case "Flagfall":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, flagfall);
                            break;
                        case "Fleet":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, fleetId);
                            break;
                        case "Pick Address":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, pickAdd);
                            break;
                        case "State Tax Amount":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, stateMaxTax);
                            break;
                        case "State Tax Percentage":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, stateTaxPer);
                            break;
                        case "Subtotal":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, subTotal);
                            break;
                        case "Surcharge Amount":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, surAmt);
                            break;
                        case "Surcharge Percentage":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, surPer);
                            break;
                        case "Terms and Conditions":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, termsAndConditions);
                            break;
                        case "Tip":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, tip);
                            break;
                        case "Tolls":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, tolls);
                            break;
                        case "Total":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, total);
                            break;
                        case "Vehicle":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, vehicle);
                            break;
                        case "Disclaimer":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, disclaimer);
                            break;
                        case "Destination Address":
                            tablestring = string.Format("{0}<tr><td>{1}</td><td>{2}</td><td>", tablestring, t.fk_FieldText, destAdd);
                            break;

                            
                    }
                    //tablestring = tablestring + "<tr><td>" + t.fk_FieldText + "</td><td>" + vehicle +
                    //                      "</td><td>";

                    }
                tablestring = tablestring + "</table>";
                _logMessage.Append("mail table created success fully that is " +tablestring);
                var em = new EmailUtil();

                _logMessage.Append("Calling SendMail method of EmailUtil to send mail taking parameters mail, tablestring, cName, Invoice =" + cMail + ", " + tablestring + ", " + cName + ", Invoice");

                bool responseEmail = em.SendMail(cMail, tablestring, cName, cName + " Invoice");
                _logMessage.Append("SendMail method of EmailUtil to send executed successfully response true");
                if (!responseEmail)
                    {
                        mHeader.Status = "Failed";
                    mHeader.DeviceId = deviceId;
                    mHeader.RequestId = requestId;
                    mHeader.Message = "Mail not sent";
                    response.Header = mHeader;
                    return Ok(response);
                }
                //request Header information
                mHeader.DeviceId = deviceId;
                mHeader.RequestId = requestId;
                mHeader.Status = "Success";
                mHeader.Message = "Message sent successfully";
                response.Header = mHeader;
                 _logMessage.Append("SendMail method of EmailUtil to send executed successfully response true");
                return Ok(response);
                    }

            catch (Exception ex)
            {
                mHeader.Status = "Failed";
                mHeader.Message = ex.Message;
                response.Header = mHeader;
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
                return Ok(response);

            }
        }
     
    }
}
