﻿using MTD.Data.Data;

namespace MTD.Data.Repository.Interface
{
    public interface IMemberRepository
  {
    
      User GetPassword(string emailId);  
      SessionMerchantUser CreateLogin(Login login);
      SessionMerchantUser LoginAsAdmin(Login login);
      int UpdatePassword(Login login);
      bool Reset(string email, string newPassword);
      bool PassCodeExpiration(string email);
      int ResetExpiredPassword(Login login);
      bool IsLocked(string email);
      bool CountFailedAttempt(string email);
  }
}
