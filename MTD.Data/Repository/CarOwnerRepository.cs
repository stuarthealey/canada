﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;

using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Data.Repository
{
    public class CarOwnerRepository : BaseRepository, ICarOwnersRepository
    {
        public CarOwnerRepository(IMtDataDevlopmentsEntities dbMtData) : base(dbMtData)
        {
        }

        Regex regex = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        Regex regexNum = new Regex(@"^[0-9]+$");
        Regex alphaNum = new Regex(@"^[a-zA-Z0-9 ]+$");
        Regex alphaNumOnly = new Regex(@"^[a-zA-Z0-9]+$");

        /// <summary>
        /// 
        /// </summary>
        /// <param name="carOwner"></param>
        /// <returns></returns>
        public bool Add(CarOwner carOwner)
        {
            try
            {
                var cOwner = DbMtData.CarOwners.Any(x => x.EmailAddress == carOwner.EmailAddress && (bool)x.IsActive && x.fk_MerchantId == carOwner.fk_MerchantId);
                
                if (cOwner)
                    return false;

                carOwner.IsActive = true;
                carOwner.CreatedDate = DateTime.Now;
                DbMtData.CarOwners.Add(carOwner);
                return Convert.ToBoolean(DbMtData.CommitChanges());
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CarOwner Read(int id)
        {
            try
            {
                CarOwner carOwner = DbMtData.CarOwners.Find(id);
                return carOwner;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CarOwner> CarOwnerList()
        {
            try
            {
                return DbMtData.CarOwners.OrderBy(x => x.OwnerName);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="owner"></param>
        /// <returns></returns>
        public bool Update(CarOwner owner)
        {
            try
            {
                var cOwner = DbMtData.CarOwners.Any(x => x.EmailAddress == owner.EmailAddress && (bool)x.IsActive && x.Id != owner.Id && x.fk_MerchantId == owner.fk_MerchantId);
                if (cOwner)
                    return false;

                CarOwner carOwner = DbMtData.CarOwners.Find(owner.Id);
                if (carOwner != null)
                {
                    carOwner.Address = owner.Address;
                    carOwner.City = owner.City;
                    carOwner.Contact = owner.Contact;
                    carOwner.Country = owner.Country;
                    carOwner.EmailAddress = owner.EmailAddress;
                    carOwner.ModifiedBy = owner.ModifiedBy;
                    carOwner.ModifiedDate = DateTime.Now;
                    carOwner.OwnerName = owner.OwnerName;
                    carOwner.State = owner.State;
                    carOwner.ContactPerson = owner.ContactPerson;
                    carOwner.SageAccount = owner.SageAccount;
                    carOwner.BankName = owner.BankName;
                    carOwner.AccountName = owner.AccountName;
                    carOwner.BSB = owner.BSB;
                    carOwner.Account = owner.Account;
                    DbMtData.CommitChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public bool Remove(int id)
        {
            try
            {
                var vehicelDb = DbMtData.Vehicles.Any(x => x.fk_CarOwenerId == id && (bool)x.IsActive);
                if (vehicelDb)
                    return false;

                var userDb = DbMtData.CarOwners.Find(id);

                userDb.IsActive = false;

                return Convert.ToBoolean(DbMtData.CommitChanges());
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public int GetOwnerCount(string name, int merchantId, string ownerName)
        {
            var ownerList = DbMtData.CarOwners.Where(x => (bool)x.IsActive && x.fk_MerchantId == merchantId).ToList();    
            
            if (!string.IsNullOrEmpty(name))
            {
                ownerList = ownerList.Where(p => p.ContactPerson.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
            }
            
            if (!string.IsNullOrEmpty(ownerName))
            {
                ownerList = ownerList.Where(p => p.OwnerName != null).ToList();
                ownerList = ownerList.Where(p => p.OwnerName.IndexOf(ownerName, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
            }
            
            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(ownerName))
            {
                ownerList = ownerList.Where(p => p.ContactPerson.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0 && p.OwnerName.IndexOf(ownerName, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
            }

            return ownerList.Count();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fsParametes"></param>
        /// <returns></returns>
        public IEnumerable<CarOwner> GetOwnerListByFilter(FilterSearchParameters fsParametes)
        {
            try
            {
                IEnumerable<CarOwner> ownerList = (from owners in DbMtData.CarOwners
                                                   join countries in DbMtData.CountryCodes on owners.Country equals countries.CountryCodesID
                                                   where (bool)owners.IsActive && owners.fk_MerchantId == fsParametes.MerchantId
                                                   select
                                                       new
                                                       {
                                                           CountryCodes = countries,
                                                           CarOwners = owners,
                                                       }).AsEnumerable().Select(objNew =>
                                                            new CarOwner
                                                            {

                                                                Id = objNew.CarOwners.Id,
                                                                OwnerName = objNew.CarOwners.OwnerName ?? string.Empty,
                                                                Contact = objNew.CarOwners.Contact,
                                                                EmailAddress = objNew.CarOwners.EmailAddress,
                                                                Country = objNew.CountryCodes.Title,
                                                                State = objNew.CarOwners.State,
                                                                City = objNew.CarOwners.City,
                                                                IsActive = objNew.CarOwners.IsActive,
                                                                CreatedBy = objNew.CarOwners.CreatedBy,
                                                                CreatedDate = objNew.CarOwners.CreatedDate,
                                                                ModifiedBy = objNew.CarOwners.ModifiedBy,
                                                                ModifiedDate = objNew.CarOwners.ModifiedDate,
                                                                Address = objNew.CarOwners.Address,
                                                                ContactPerson = objNew.CarOwners.ContactPerson,
                                                                fk_MerchantId = objNew.CarOwners.fk_MerchantId,
                                                                OwnerDetail = objNew.CarOwners.OwnerDetail
                                                            });

                ownerList = ownerList.OrderBy(p => p.ContactPerson); //Default!
                
                if (!string.IsNullOrEmpty(fsParametes.Name))
                    ownerList = ownerList.Where(p => p.ContactPerson.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();

                if (!string.IsNullOrEmpty(fsParametes.OwnerName))
                    ownerList = ownerList.Where(p => p.OwnerName.IndexOf(fsParametes.OwnerName, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                
                if (!string.IsNullOrEmpty(fsParametes.Name) && !string.IsNullOrEmpty(fsParametes.OwnerName))
                    ownerList = ownerList.Where(p => p.ContactPerson.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0 && p.OwnerName.IndexOf(fsParametes.OwnerName, StringComparison.OrdinalIgnoreCase) >= 0).ToList();

                if (!string.IsNullOrEmpty(fsParametes.Sorting))
                    ownerList = SortedOwners(fsParametes.Sorting, ownerList).ToList();

                List<CarOwner> mUsers = new List<CarOwner>();
                foreach (var mu in ownerList)
                {
                    mu.Address = mu.Address + ", " + mu.State + ", " + mu.City + ", " + mu.Country;
                    mu.Address = mu.Address.Replace(", , ,", ", ");
                    mu.Address = mu.Address.Replace(", ,", ", ");
                    mUsers.Add(mu);
                }

                return fsParametes.PageSize > 0 ? mUsers.Skip(fsParametes.StartIndex).Take(fsParametes.PageSize).ToList() : mUsers.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<CarOwner> SortedOwners(string sorting, IEnumerable<CarOwner> query)
        {
            switch (sorting)
            {
                case "OwnerName ASC":
                    query = query.OrderBy(p => p.OwnerName);
                    break;
                case "OwnerName DESC":
                    query = query.OrderByDescending(p => p.OwnerName);
                    break;
                case "Contact ASC":
                    query = query.OrderBy(p => p.Contact);
                    break;
                case "Contact DESC":
                    query = query.OrderByDescending(p => p.Contact);
                    break;
                case "EmailAddress ASC":
                    query.OrderBy(p => p.EmailAddress);
                    break;
                case "EmailAddress DESC":
                    query = query.OrderByDescending(p => p.EmailAddress);
                    break;
            }

            return query;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="carOwner"></param>
        /// <returns></returns>
        public int IsValidCarOwner(CarOwner carOwner)
        {
            try
            {
                int response = 0;
                
                if (carOwner.EmailAddress == string.Empty)
                    return 2;
                
                if (carOwner.Address == string.Empty)
                    return 3;
                
                if (carOwner.Country == string.Empty)
                    return 6;
                
                if (!string.IsNullOrEmpty(carOwner.Contact))
                {
                    if (carOwner.Contact.Length < 6)
                        return 7;
                }
                
                if (carOwner.ContactPerson == string.Empty)
                    return 8;
                
                if (!string.IsNullOrEmpty(carOwner.EmailAddress))
                {
                    Match match = regex.Match(carOwner.EmailAddress);
                    if (!match.Success)
                        return 9;
                }
                
                Match exp = alphaNum.Match(carOwner.OwnerName);
                
                if (!string.IsNullOrEmpty(carOwner.OwnerName))
                {
                    if (!exp.Success)
                        return 10;

                    if (carOwner.OwnerName.Length>=100)
                        return 11;
                }

                exp = alphaNum.Match(carOwner.ContactPerson);
                if (!exp.Success)
                    return 12;

                if (!string.IsNullOrEmpty(carOwner.Contact))
                {
                    exp = alphaNumOnly.Match(carOwner.Contact);
                    if (!exp.Success)
                        return 13;

                    if (carOwner.Contact.Length >= 20)
                        return 14;
                }

                if (!string.IsNullOrEmpty(carOwner.State))
                {
                    exp = alphaNum.Match(carOwner.State);
                    if (!exp.Success)
                        return 15;
                 
                    if (carOwner.State.Length >= 40)
                        return 16;
                }

                if (!string.IsNullOrEmpty(carOwner.City))
                {
                    exp = alphaNum.Match(carOwner.City);
                    if (!exp.Success)
                        return 17;

                    if (carOwner.City.Length >= 40)
                        return 18;
                }

                if (!string.IsNullOrEmpty(carOwner.SageAccount))
                {
                    exp = alphaNum.Match(carOwner.SageAccount);
                    if (!exp.Success)
                        return 19;
                    
                    if (carOwner.SageAccount.Length >= 50)
                        return 20;
                }

                if (!string.IsNullOrEmpty(carOwner.BankName))
                {
                    exp = alphaNum.Match(carOwner.BankName);
                    if (!exp.Success)
                        return 21;
                    
                    if (carOwner.BankName.Length >= 50)
                        return 22;
                }

                if (!string.IsNullOrEmpty(carOwner.AccountName))
                {
                    exp = alphaNum.Match(carOwner.AccountName);
                    if (!exp.Success)
                        return 23;
                    
                    if (carOwner.AccountName.Length >= 50)
                        return 24;
                }

                if (!string.IsNullOrEmpty(carOwner.BSB))
                {
                    exp = alphaNum.Match(carOwner.BSB);
                    if (!exp.Success)
                        return 25;
                    
                    if (carOwner.BSB.Length >= 50)
                        return 26;
                }

                if (!string.IsNullOrEmpty(carOwner.Account))
                {
                    exp = alphaNum.Match(carOwner.Account);
                    if (!exp.Success)
                        return 27;
                    
                    if (carOwner.Account.Length >= 50)
                        return 28;
                }

                if (carOwner.ContactPerson.Length >= 100)
                    return 29;

                if (!string.IsNullOrEmpty(carOwner.Country))
                {
                    CountryCode country = (from m in DbMtData.CountryCodes where m.Title == carOwner.Country select m).FirstOrDefault();
                    if (country == null)
                        return 4;// Country does not exists.
                }

                if (!string.IsNullOrEmpty(carOwner.EmailAddress))
                {
                    var IsEmailExist = DbMtData.CarOwners.Any(x => x.EmailAddress == carOwner.EmailAddress && (bool)x.IsActive && x.fk_MerchantId == carOwner.fk_MerchantId);
                    if (IsEmailExist)
                        return 5;// Email does not exists.
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CarOwnerExcel"></param>
        /// <returns></returns>
        public CarOwner CarOwnerExcel(CarOwner carOwnerExcel)
        {
            try
            {
                CountryCode country = (from m in DbMtData.CountryCodes where m.Title == carOwnerExcel.Country select m).FirstOrDefault();
                carOwnerExcel.Country = country.CountryCodesID;

                return carOwnerExcel;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}