﻿using System.Collections.Generic;
using MTD.Data.Data;

namespace MTD.Data.Repository.Interface
{
    public interface ITaxRepository
    {
        bool Add(Merchant taxMerchant);
        IEnumerable<Merchant> TaxList();
        void Update(Merchant taxMerchant, int merchantId);
        Merchant GetTax(int merchantId);
        void DeleteTax(int merchantId);
        DefaultSetting GetDefaultTax();
    }
}
