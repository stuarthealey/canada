﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MTData.Web.ViewData
{
    public class DriverRecListViewData
    {
     
        public Nullable<int> fk_FleetId { get; set; }
        public Nullable<int> UserType { get; set; }
        public string ModifiedBy { get; set; }
        public IEnumerable<DriverRecipientViewdata> UserList { get; set; }
        public IEnumerable<FleetViewData> fleetlist { get; set; }
        public string loggedUser { get; set; }
       
    }

   
}