﻿using System.Collections.Generic;
using AutoMapper;

using MTD.Core.DataTransferObjects;
using MTData.Web.Models;
using MTData.Web.ViewData;

namespace MTData.Web
{
    public static class ViewDataExtension
    {
        static ViewDataExtension()
        {
            Mapper.CreateMap<DrivernamelistDto, DrivernamelistViewData>();
            Mapper.CreateMap<MerchantDto, MerchantViewData>();
            Mapper.CreateMap<MerchantViewData, MerchantDto>();
            Mapper.CreateMap<MerchantUserViewData, MerchantUserDto>().ForMember(x => x.GropuId, d => d.Ignore()).ForMember(x => x.RapidConnect, d => d.Ignore()).ForMember(x => x.ProjectType, d => d.Ignore()).ForMember(x => x.ProjectId, d => d.Ignore()).ForMember(x => x.EcommProjectId, d => d.Ignore());
            Mapper.CreateMap<FDMerchantListDto, FDMerchantListViewData>();
            Mapper.CreateMap<VehicleTxnListDto, VehicleTxnListViewData>();
            Mapper.CreateMap<DriverTxnListDto, DriverTxnListViewData>();
            Mapper.CreateMap<TxnTaxSurchargesDto, TxnTaxSurchargesViewData>();
            Mapper.CreateMap<UserDto, UserViewData>().ForMember(result => result.IsMerAdd, dest => dest.Ignore()).ForMember(result => result.IsChkMerAdd, dest => dest.Ignore());
            Mapper.CreateMap<GatewayViewData, GatewayDto>();
            Mapper.CreateMap<GatewayDto, GatewayViewData>().ForMember(s => s.TimeZones, d => d.Ignore()).ForMember(s => s.GatewayCurrencyList, d => d.Ignore());
            Mapper.CreateMap<UserViewData, UserDto>();

            Mapper.CreateMap<MerchantUserDto, MerchantUserViewData>().ForMember(result => result.CountryList, dest => dest.Ignore()).ForMember(result => result.IsOtherDetailSaved, dest => dest.Ignore()).ForMember(result => result.IsFirstTime, dest => dest.Ignore()).ForMember(x => x.TimeZones, d => d.Ignore())
                .ForMember(result => result.StateList, dest => dest.Ignore()).ForMember(result => result.ConfirmPCode, dest => dest.Ignore()).ForMember(result => result.FdMerchantList, dest => dest.Ignore())
                .ForMember(result => result.CityList, dest => dest.Ignore());
            
            Mapper.CreateMap<UserViewData, UserDto>().ForMember(result => result.ConfirmPCode, dest => dest.Ignore());
            
            Mapper.CreateMap<UserDto, UserViewData>().ForMember(result => result.CountryList, dest => dest.Ignore()).ForMember(result => result.MerchantList, dest => dest.Ignore())
                .ForMember(result => result.StateList, dest => dest.Ignore())
                .ForMember(result => result.CityList, dest => dest.Ignore());
            
            Mapper.CreateMap<RoleDto, RoleViewData>();
            Mapper.CreateMap<CountryStateDto, StateViewData>();
            
            Mapper.CreateMap<FleetViewData, FleetDto>().ForMember(s => s.IsCardSwiped, d => d.Ignore()).ForMember(s => s.IsContactless, d => d.Ignore()).ForMember(s => s.IsChipAndPin, d => d.Ignore()).ForMember(s => s.IsAssigned, d => d.Ignore())
                .ForMember(s => s.IsReceipt, d => d.Ignore()).ForMember(s => s.IsAssigned, d => d.Ignore()).ForMember(s => s.CurrencyCode, d => d.Ignore());
            
            Mapper.CreateMap<DriverViewData, DriverDto>();
            
            Mapper.CreateMap<DriverDto, DriverViewData>().ForMember(result => result.CountryList, dest => dest.Ignore())
                .ForMember(result => result.StateList, dest => dest.Ignore())
                .ForMember(result => result.CityList, dest => dest.Ignore()).ForMember(result => result.FleetList, dest => dest.Ignore()).ForMember(r => r.uploadFile, dest => dest.Ignore())
                .ForMember(r => r.driverListViewData, dest => dest.Ignore())
                .ForMember(r => r.IsDriverError, dest => dest.Ignore());
            
            Mapper.CreateMap<TerminalDto, TerminalViewData>().ForMember(r => r.TermId, dest => dest.Ignore()).ForMember(r => r.RegisteredTerminalList, dest => dest.Ignore()).ForMember(r => r.DeviceList, dest => dest.Ignore())
                .ForMember(r => r.uploadFile, dest => dest.Ignore())
                .ForMember(r => r.TerminalExcelUploadViewData, dest => dest.Ignore())
                .ForMember(r => r.IsTerminalError, dest => dest.Ignore()).ForMember(r => r.Contact, dest => dest.Ignore()).ForMember(r => r.ContactLess, dest => dest.Ignore());
            Mapper.CreateMap<MtdTransactionsViewData, MTDTransactionDto>().ForMember(source => source.IsChargeBackAllow, dest => dest.Ignore()).ForMember(source => source.ResponseBit63, dest => dest.Ignore());
            Mapper.CreateMap<MTDTransactionDto, MtdTransactionsViewData>();
            Mapper.CreateMap<CityDto, CityViewData>();
            Mapper.CreateMap<CountryViewData, CountryCodeDto>();
            Mapper.CreateMap<CountryCodeDto, CountryViewData>();
            Mapper.CreateMap<TerminalViewData, TerminalDto>();
            Mapper.CreateMap<FleetDto, FleetViewData>().ForMember(result => result.currencyList, dest => dest.Ignore()).ForMember(result => result.fleetlist, dest => dest.Ignore());
            
            Mapper.CreateMap<UserDto, UserViewData>().ForMember(result => result.UserTypeList, dest => dest.Ignore()).ForMember(result => result.merchantRoles, dest => dest.Ignore()).ForMember(result => result.FleetAssigned, dest => dest.Ignore())
                .ForMember(result => result.UserRoles, dest => dest.Ignore()).ForMember(result => result.UserRoleIdList, dest => dest.Ignore());
            
            Mapper.CreateMap<VehicleDto, VehicleViewData>().ForMember(result => result.FleetList, dest => dest.Ignore()).ForMember(result => result.CarOwnerList, dest => dest.Ignore())
                .ForMember(result => result.TerminalList, dest => dest.Ignore()).ForMember(result => result.uploadFile, dest => dest.Ignore()).ForMember(result => result.VehicleListViewData, dest => dest.Ignore()).ForMember(result => result.IsVehicleError, dest => dest.Ignore());
            
            Mapper.CreateMap<VehicleViewData, VehicleDto>();
            Mapper.CreateMap<TerminalListDto, TerminalListViewData>();
            Mapper.CreateMap<FleetListDto, FleetListViewData>();
            Mapper.CreateMap<TaxDto, TaxViewData>().ForMember(result => result.MerchantList, dest => dest.Ignore()).ForMember(result => result.MerchantName, dest => dest.Ignore());
            Mapper.CreateMap<TaxViewData, TaxDto>();
            Mapper.CreateMap<MerchantListDto, MerchantListViewData>();
            Mapper.CreateMap<DefaultTaxDto, DefaultTaxViewData>();
            
            Mapper.CreateMap<SurchargeDto, SurchargeViewData>()
                  .ForMember(result => result.MerchantList, dest => dest.Ignore()).ForMember(result => result.TechFee, dest => dest.Ignore())
                  .ForMember(result => result.GatewayList, dest => dest.Ignore()).ForMember(result => result.TechnologyFeeList, dest => dest.Ignore())
                  .ForMember(result => result.TypeList, dest => dest.Ignore()).ForMember(result => result.Surcharge, dest => dest.Ignore()).ForMember(result => result.SurchargePercentage, dest => dest.Ignore()).ForMember(result => result.MerchantName, dest => dest.Ignore())
                  .ForMember(result => result.BookingFee, dest => dest.Ignore()).ForMember(result => result.FeePercentage, dest => dest.Ignore()).ForMember(result => result.Fleets, dest => dest.Ignore());
            
            Mapper.CreateMap<SurchargeViewData, SurchargeDto>();
            Mapper.CreateMap<GatewayListDto, GatewayListViewData>();
            Mapper.CreateMap<MerchantUserDto, MerchantUserViewData>()
                .ForMember(result => result.UserRoles, dest => dest.Ignore()
                ).ForMember(result => result.UserRoleIdList, dest => dest.Ignore()).ForMember(result => result.TCondition, dest => dest.Ignore()).ForMember(result => result.isPersonalUpdate, dest => dest.Ignore());

            Mapper.CreateMap<MerchantListViewData, AdminRecipienMastertListDto>();
            Mapper.CreateMap<ManageRecieptDto, ManageRecieptViewData>();
            Mapper.CreateMap<ManageRecieptViewData, ManageRecieptDto>();
            Mapper.CreateMap<SearchTransactionDto, SearchTransactionViewData>().ForMember(result => result.TechFee, dest => dest.Ignore()).ForMember(result => result.Surcharge, dest => dest.Ignore());
            Mapper.CreateMap<ManyTxnClass, ManyTxnClassDto>();
            Mapper.CreateMap<UserFleetDto, UserFleetViewData>();
            Mapper.CreateMap<UserDto, UserViewData>().ForMember(result => result.fleetlist, dest => dest.Ignore());
            Mapper.CreateMap<ManageRecieptDto, ManageRecieptViewData>().ForMember(result => result.fleetlist, dest => dest.Ignore());
            Mapper.CreateMap<UserDto, UserViewData>().ForMember(result => result.userFleetList, dest => dest.Ignore()).ForMember(result => result.UserMerchantList, dest => dest.Ignore());
            Mapper.CreateMap<CurrencyDto, CurrencyViewData>().ForMember(result => result.currency, dest => dest.Ignore());
            Mapper.CreateMap<MerchantGatewayViewData, MerchantGatewayDto>();
            Mapper.CreateMap<DefaultSettingDto, DefaultSettingViewData>().ForMember(result => result.GatewayList, dest => dest.Ignore()).ForMember(result => result.TypeList, dest => dest.Ignore()).ForMember(result => result.TechnologyFeeList, dest => dest.Ignore()).ForMember(result => result.IsNew, dest => dest.Ignore()).ForMember(result => result.FeePercentage, dest => dest.Ignore()).ForMember(result => result.Surcharge, dest => dest.Ignore()).ForMember(result => result.BookingFee, dest => dest.Ignore()).ForMember(result => result.TechFee, dest => dest.Ignore()).ForMember(result => result.SurPercentage, dest => dest.Ignore()).ForMember(result => result.SurType, dest => dest.Ignore()).ForMember(result => result.FeeType, dest => dest.Ignore());
            Mapper.CreateMap<DefaultSettingViewData, DefaultSettingDto>();
            Mapper.CreateMap<MerchantGatewayDto, MerchantGatewayViewData>().ForMember(result => result.GatewayList, dest => dest.Ignore()).ForMember(result => result.TypeList, dest => dest.Ignore()).ForMember(result => result.GatewayType, dest => dest.Ignore());
            Mapper.CreateMap<BookingFeeViewData, BookigFeeDto>();
            Mapper.CreateMap<BookigFeeDto, BookingFeeViewData>().ForMember(result => result.GatewayList, dest => dest.Ignore()).ForMember(result => result.MerchantList, dest => dest.Ignore()).ForMember(result => result.TypeList, dest => dest.Ignore()).ForMember(result => result.BookingFee, dest => dest.Ignore()).ForMember(result => result.FeePercentage, dest => dest.Ignore()).ForMember(result => result.Company, dest => dest.Ignore()).ForMember(result => result.MerchantName, dest => dest.Ignore());
            Mapper.CreateMap<TCDto, TCViewData>();
            Mapper.CreateMap<PrefixViewData, PrefixDto>();
            Mapper.CreateMap<PrefixDto, PrefixViewData>().ForMember(result => result.GatewayList, dest => dest.Ignore());
            Mapper.CreateMap<MTDTransactionDto, SearchTransactionViewData>();
            Mapper.CreateMap<DriverTransactionResult, DriverTransactionViewData>();
            Mapper.CreateMap<ReceiptMasterDto, ReceiptMasterViewData>();
            Mapper.CreateMap<ReceiptMasterViewData, ReceiptMasterDto>();
            Mapper.CreateMap<ReceiptFormatViewData, ReceiptFormatDto>();
            Mapper.CreateMap<ReceiptFormatDto, ReceiptFormatViewData>();
            Mapper.CreateMap<ReceiptFormatViewData, ReceiptFormatDto>();
            Mapper.CreateMap<FleetReceiptDto, FleetReceiptViewData>();
            Mapper.CreateMap<FleetReceiptViewData, FleetReceiptDto>();
            Mapper.CreateMap<ScheduleReportViewData, ScheduleReportDto>().ForMember(result => result.TimeInString, dest => dest.Ignore()).ForMember(result => result.CorporateUserId, dest => dest.Ignore());
            
            Mapper.CreateMap<ScheduleReportDto, ScheduleReportViewData>().ForMember(result => result.AdminList, dest => dest.Ignore()).ForMember(result => result.FrequencyTyeList, dest => dest.Ignore()).ForMember(result => result.FrequencyList, dest => dest.Ignore()).ForMember(result => result.TimeZones, dest => dest.Ignore()).ForMember(result => result.FrequencyName, dest => dest.Ignore()).ForMember(result => result.DayList, dest => dest.Ignore()).ForMember(result => result.MonthDayList, dest => dest.Ignore()).ForMember(result => result.WeekName, dest => dest.Ignore()).ForMember(result => result.UserType, dest => dest.Ignore()).ForMember(result => result.IsReport, dest => dest.Ignore())
                .ForMember(result => result.ReportList, dest => dest.Ignore()).ForMember(result => result.MerchantList, dest => dest.Ignore());
            
            Mapper.CreateMap<RecipientDto, RecipientViewData>().ForMember(result => result.MerchantList, dest => dest.Ignore()).ForMember(result => result.ReportList, dest => dest.Ignore()).ForMember(result => result.chkAll, dest => dest.Ignore());
            Mapper.CreateMap<RecipientViewData, RecipientDto>();
            Mapper.CreateMap<MerchantListViewData, MerchantListDto>();
            Mapper.CreateMap<DrivernamelistDto, DrivernamelistViewData>();
            Mapper.CreateMap<RapidConnectTerminalDto, RapidConnectViewData>();
            Mapper.CreateMap<RapidConnectViewData, RapidConnectTerminalDto>();
            Mapper.CreateMap<FDMerchantViewData, fDMerchantDto>();//by asif added on 4/17/2015
            Mapper.CreateMap<FDMerchantListDto, FDMerchantListViewData>();//by asif added on 4/17/2015
            Mapper.CreateMap<fDMerchantDto, FDMerchantViewData>();//by Naveen added on 4/17/2015          
            Mapper.CreateMap<DeviceNameListDto, DeviceNameListViewData>();
            Mapper.CreateMap<DeviceNameListViewData, DeviceNameListDto>();
            Mapper.CreateMap<FleetSettingDto, FleetSettingViewData>();//by asif added on 06/06/2015
            Mapper.CreateMap<FleetSettingViewData, FleetSettingDto>().ForMember(s => s.CurrencyCode, d => d.Ignore());//by asif added on 06/06/2015
            Mapper.CreateMap<AdminRecipienMastertListDto, AdminRecipientMasterViewData>();
            Mapper.CreateMap<AdminRecipientMasterViewData, AdminRecipienMastertListDto>();
            Mapper.CreateMap<AdminRecipientViewData, AdminRecipientListDto>();
            Mapper.CreateMap<FleetReceiptViewData, ReceiptMasterViewData>();//by asif added on 06/06/2015
            Mapper.CreateMap<ReportListDto, ReportListViewData>();
            
            //Fro upload Terminal device List
            Mapper.CreateMap<TerminalExcelUploadViewData, TerminalExcelUploadDto>();

            Mapper.CreateMap<TerminalExcelUploadDto, TerminalExcelUploadViewData>();
            Mapper.CreateMap<DriverRecipientDto, DriverRecipientViewdata>();
            Mapper.CreateMap<DriverRecipientViewdata, DriverRecipientDto>();
            Mapper.CreateMap<DriverRecListViewData, DriverRecListDto>();

            Mapper.CreateMap<CarListDto, CarListViewData>();
            Mapper.CreateMap<DriverListsDto, DriverListViewData>();
            Mapper.CreateMap<FleetListViewData, FleetListDto>();
            Mapper.CreateMap<CarOwnerViewData, CarOwnersDto>();
            Mapper.CreateMap<PageSizeSearchViewData, PageSizeSearchDto>();
            Mapper.CreateMap<CarOwnersDto, CarOwnerViewData>().ForMember(s => s.IsCarOwnerError, d => d.Ignore()).ForMember(s => s.CountryList, d => d.Ignore()).ForMember(s => s.CarOwnerListViewData, d => d.Ignore());
            Mapper.CreateMap<AllMerchantsDto, AllMerchantsViewData>();
            Mapper.CreateMap<TransactionCountSlabDto, TransactionCountSlabViewData>();
            Mapper.CreateMap<TransactionCountSlabViewData, TransactionCountSlabDto>();
            Mapper.CreateMap<GetAdminSummaryReportDto, GetAdminSummaryReportViewData>();
            Mapper.CreateMap<GetCardTypeReportDto, GetCardTypeReportViewData>();
            Mapper.CreateMap<GetAdminExceptionReportDto, GetAdminExceptionReportViewData>();
            Mapper.CreateMap<GetTotalTransByVehicleReportDto, GetTotalTransByVehicleReportViewData>();

            Mapper.CreateMap<GetMerchantSummaryReportDto, GetMerchantSummaryReportViewData>();
            Mapper.CreateMap<GetDetailReportDto, GetDetailReportViewData>();

            Mapper.CreateMap<GetMerchantExceptionReportDto,  GetMerchantExceptionReportViewData>();
            Mapper.CreateMap<GetMerchantTotalTransByVehicleReportDto, GetMerchantTotalTransByVehicleReportViewData>();
            Mapper.CreateMap<GetMerchantCardTypeReportDto, GetMerchantCardTypeReportViewData>();

            Mapper.AssertConfigurationIsValid();
        }

        public static IEnumerable<FleetReceiptViewData> ToViewDataList(this IEnumerable<FleetReceiptDto> fleetReceipt)
        {
            return Mapper.Map<IEnumerable<FleetReceiptDto>, IEnumerable<FleetReceiptViewData>>(fleetReceipt);
        }

        public static IEnumerable<FleetReceiptDto> ToDtoList(this IEnumerable<FleetReceiptViewData> receiptView)
        {
            return Mapper.Map<IEnumerable<FleetReceiptViewData>, IEnumerable<FleetReceiptDto>>(receiptView);
        }

        public static TxnTaxSurchargesViewData ToDTO(this TxnTaxSurchargesDto receiptView)
        {
            return Mapper.Map<TxnTaxSurchargesDto, TxnTaxSurchargesViewData>(receiptView);
        }

        public static ReceiptFormatDto ToDTO(this ReceiptFormatViewData receiptView)
        {
            return Mapper.Map<ReceiptFormatViewData, ReceiptFormatDto>(receiptView);
        }

        public static IEnumerable<ReceiptFormatDto> ToReceiptDtoList(this IEnumerable<ReceiptFormatViewData> receiptView)
        {
            return Mapper.Map<IEnumerable<ReceiptFormatViewData>, IEnumerable<ReceiptFormatDto>>(receiptView);
        }

        public static IEnumerable<ReceiptMasterViewData> MasterToViewDataList(this IEnumerable<ReceiptMasterDto> recMaster)
        {
            return Mapper.Map<IEnumerable<ReceiptMasterDto>, IEnumerable<ReceiptMasterViewData>>(recMaster);
        }

        public static ReceiptMasterDto ToReceiptMastertDto(this ReceiptMasterViewData defaultViewData)
        {
            return Mapper.Map<ReceiptMasterViewData, ReceiptMasterDto>(defaultViewData);
        }

        public static IEnumerable<ReceiptFormatViewData> ToViewDataList(this IEnumerable<ReceiptFormatDto> receipt)
        {
            return Mapper.Map<IEnumerable<ReceiptFormatDto>, IEnumerable<ReceiptFormatViewData>>(receipt);
        }

        public static MerchantViewData ToViewData(this MerchantDto merchantDto)
        {
            return Mapper.Map<MerchantDto, MerchantViewData>(merchantDto);
        }

        public static MerchantDto ToDTO(this MerchantViewData merchantView)
        {
            return Mapper.Map<MerchantViewData, MerchantDto>(merchantView);
        }

        public static IEnumerable<MerchantViewData> ToViewDataList(this IEnumerable<MerchantDto> merchant)
        {
            return Mapper.Map<IEnumerable<MerchantDto>, IEnumerable<MerchantViewData>>(merchant);
        }

        public static UserViewData ToViewData(this UserDto merchantDto)
        {
            return Mapper.Map<UserDto, UserViewData>(merchantDto);
        }

        public static MerchantUserViewData ToViewData(this MerchantUserDto merchantDto)
        {
            return Mapper.Map<MerchantUserDto, MerchantUserViewData>(merchantDto);
        }

        public static UserDto ToDTO(this UserViewData userView)
        {
            return Mapper.Map<UserViewData, UserDto>(userView);
        }

        public static MerchantUserDto ToDTO(this MerchantUserViewData merchantView)
        {
            return Mapper.Map<MerchantUserViewData, MerchantUserDto>(merchantView);
        }

        public static IEnumerable<UserViewData> ToViewDataList(this IEnumerable<UserDto> merchant)
        {
            return Mapper.Map<IEnumerable<UserDto>, IEnumerable<UserViewData>>(merchant);
        }

        public static IEnumerable<MerchantUserViewData> ToViewDataList(this IEnumerable<MerchantUserDto> merchant)
        {
            return Mapper.Map<IEnumerable<MerchantUserDto>, IEnumerable<MerchantUserViewData>>(merchant);
        }

        public static UserViewData ToViewAdminData(this UserDto userDto)
        {
            return Mapper.Map<UserDto, UserViewData>(userDto);
        }

        public static UserDto ToAdminDto(this UserViewData userView)
        {
            return Mapper.Map<UserViewData, UserDto>(userView);
        }

        public static IEnumerable<UserViewData> ToAdminViewDataList(this IEnumerable<UserDto> user)
        {
            return Mapper.Map<IEnumerable<UserDto>, IEnumerable<UserViewData>>(user);
        }

        public static GatewayDto ToGatewayDto(this GatewayViewData gatewayView)
        {
            return Mapper.Map<GatewayViewData, GatewayDto>(gatewayView);
        }

        public static IEnumerable<GatewayDto> ToGatewayDtoList(this IEnumerable<GatewayViewData> gatewayView)
        {
            return Mapper.Map<IEnumerable<GatewayViewData>, IEnumerable<GatewayDto>>(gatewayView);
        }

        public static IEnumerable<GatewayViewData> ToGatewayViewDataList(this IEnumerable<GatewayDto> gatewayView)
        {
            return Mapper.Map<IEnumerable<GatewayDto>, IEnumerable<GatewayViewData>>(gatewayView);
        }

        public static GatewayViewData ToGatewayViewData(this GatewayDto gatewayView)
        {
            return Mapper.Map<GatewayDto, GatewayViewData>(gatewayView);
        }

        public static IEnumerable<RoleViewData> ToViewDataList(this IEnumerable<RoleDto> merchant)
        {
            return Mapper.Map<IEnumerable<RoleDto>, IEnumerable<RoleViewData>>(merchant);
        }

        public static FleetDto ToDTO(this FleetViewData merchantView)
        {
            return Mapper.Map<FleetViewData, FleetDto>(merchantView);
        }

        public static FleetViewData ToViewData(this FleetDto fleetDto)
        {
            return Mapper.Map<FleetDto, FleetViewData>(fleetDto);
        }

        public static IEnumerable<FleetViewData> ToViewDataList(this IEnumerable<FleetDto> fleet)
        {
            return Mapper.Map<IEnumerable<FleetDto>, IEnumerable<FleetViewData>>(fleet);
        }

        public static DriverDto ToDriverDto(this DriverViewData driverViewData)
        {
            return Mapper.Map<DriverViewData, DriverDto>(driverViewData);
        }

        public static TerminalDto ToTerminalDto(this TerminalViewData terminalViewData)
        {
            return Mapper.Map<TerminalViewData, TerminalDto>(terminalViewData);
        }

        public static DriverViewData ToDriverViewData(this DriverDto driverDto)
        {
            return Mapper.Map<DriverDto, DriverViewData>(driverDto);
        }

        public static IEnumerable<DriverViewData> ToDriverViewDataList(this IEnumerable<DriverDto> driverDto)
        {
            return Mapper.Map<IEnumerable<DriverDto>, IEnumerable<DriverViewData>>(driverDto);
        }

        public static TerminalViewData ToTerminalViewData(this TerminalDto terminalDto)
        {
            return Mapper.Map<TerminalDto, TerminalViewData>(terminalDto);
        }

        public static IEnumerable<TerminalViewData> ToTerminalViewDataList(this IEnumerable<TerminalDto> terminalDto)
        {
            return Mapper.Map<IEnumerable<TerminalDto>, IEnumerable<TerminalViewData>>(terminalDto);
        }

        public static IEnumerable<CountryViewData> ToCountryViewDataList(this IEnumerable<CountryCodeDto> countryDto)
        {
            return Mapper.Map<IEnumerable<CountryCodeDto>, IEnumerable<CountryViewData>>(countryDto);
        }

        public static IEnumerable<StateViewData> ToStateViewDataList(this IEnumerable<CountryStateDto> stateDto)
        {
            return Mapper.Map<IEnumerable<CountryStateDto>, IEnumerable<StateViewData>>(stateDto);
        }

        public static IEnumerable<CityViewData> ToCityViewDataList(this IEnumerable<CityDto> cityDto)
        {
            return Mapper.Map<IEnumerable<CityDto>, IEnumerable<CityViewData>>(cityDto);
        }

        public static IEnumerable<UserFleetViewData> ToViewDataList(this IEnumerable<UserFleetDto> userFleet)
        {
            return Mapper.Map<IEnumerable<UserFleetDto>, IEnumerable<UserFleetViewData>>(userFleet);
        }

        public static VehicleDto ToVehicleDto(this VehicleViewData vehicleViewData)
        {
            return Mapper.Map<VehicleViewData, VehicleDto>(vehicleViewData);
        }

        public static VehicleViewData ToVehicleViewData(this VehicleDto vehicleDto)
        {
            return Mapper.Map<VehicleDto, VehicleViewData>(vehicleDto);
        }

        public static IEnumerable<VehicleViewData> ToVehicleViewDataList(this IEnumerable<VehicleDto> vehicleDto)
        {
            return Mapper.Map<IEnumerable<VehicleDto>, IEnumerable<VehicleViewData>>(vehicleDto);
        }

        public static IEnumerable<FleetListViewData> ToFleetViewDataSelectList(this IEnumerable<FleetListDto> fleetList)
        {
            return Mapper.Map<IEnumerable<FleetListDto>, IEnumerable<FleetListViewData>>(fleetList);
        }

        public static IEnumerable<TerminalListViewData> ToTerminalViewDataSelectList(this IEnumerable<TerminalListDto> terminalList)
        {
            return Mapper.Map<IEnumerable<TerminalListDto>, IEnumerable<TerminalListViewData>>(terminalList);
        }

        public static TaxDto ToTaxDto(this TaxViewData taxViewData)
        {
            return Mapper.Map<TaxViewData, TaxDto>(taxViewData);
        }

        public static TaxViewData ToTaxViewData(this TaxDto taxDto)
        {
            return Mapper.Map<TaxDto, TaxViewData>(taxDto);
        }

        public static IEnumerable<TaxViewData> ToTaxViewDataList(this IEnumerable<TaxDto> taxDto)
        {
            return Mapper.Map<IEnumerable<TaxDto>, IEnumerable<TaxViewData>>(taxDto);
        }

        public static IEnumerable<MerchantListViewData> ToMerchantViewDataSelectList(this IEnumerable<MerchantListDto> merchantList)
        {
            return Mapper.Map<IEnumerable<MerchantListDto>, IEnumerable<MerchantListViewData>>(merchantList);
        }

        public static IEnumerable<MerchantListViewData> ToMerchantViewDataSelectList(this IEnumerable<AdminRecipienMastertListDto> merchantList)
        {
            return Mapper.Map<IEnumerable<AdminRecipienMastertListDto>, IEnumerable<MerchantListViewData>>(merchantList);
        }

        public static DefaultTaxViewData ToDefaultTaxViewData(this DefaultTaxDto defaultTaxDto)
        {
            return Mapper.Map<DefaultTaxDto, DefaultTaxViewData>(defaultTaxDto);
        }

        public static DefaultTaxDto TodDefaultTaxDto(this DefaultTaxViewData defaultTaxView)
        {
            return Mapper.Map<DefaultTaxViewData, DefaultTaxDto>(defaultTaxView);
        }

        public static SurchargeDto ToSurchareDto(this SurchargeViewData surcharge)
        {
            return Mapper.Map<SurchargeViewData, SurchargeDto>(surcharge);
        }

        public static SurchargeViewData ToSurchargeViewData(this SurchargeDto surchargeDto)
        {
            return Mapper.Map<SurchargeDto, SurchargeViewData>(surchargeDto);
        }

        public static IEnumerable<SurchargeViewData> ToSurchargeViewDataList(this IEnumerable<SurchargeDto> surcharge)
        {
            return Mapper.Map<IEnumerable<SurchargeDto>, IEnumerable<SurchargeViewData>>(surcharge);
        }

        public static IEnumerable<GatewayListViewData> ToGatewayViewDataList(this IEnumerable<GatewayListDto> gatewayList)
        {
            return Mapper.Map<IEnumerable<GatewayListDto>, IEnumerable<GatewayListViewData>>(gatewayList);
        }

        public static MtdTransactionsViewData ToViewData(this MTDTransactionDto transactionDto)
        {
            return Mapper.Map<MTDTransactionDto, MtdTransactionsViewData>(transactionDto);
        }

        public static MTDTransactionDto ToDTO(this MtdTransactionsViewData transactionView)
        {
            return Mapper.Map<MtdTransactionsViewData, MTDTransactionDto>(transactionView);
        }

        public static ManageRecieptDto ToRecieptDto(this ManageRecieptViewData recieptview)
        {
            return Mapper.Map<ManageRecieptViewData, ManageRecieptDto>(recieptview);
        }

        public static IEnumerable<ManageRecieptViewData> ToViewDataList(this IEnumerable<ManageRecieptDto> recieptview)
        {
            return Mapper.Map<IEnumerable<ManageRecieptDto>, IEnumerable<ManageRecieptViewData>>(recieptview);
        }

        public static IEnumerable<CurrencyViewData> ToCurrencyViewDataList(this IEnumerable<CurrencyDto> currency)
        {
            return Mapper.Map<IEnumerable<CurrencyDto>, IEnumerable<CurrencyViewData>>(currency);
        }

        public static MerchantGatewayDto ToMerchantGatewayDto(this MerchantGatewayViewData gatewayViewData)
        {
            return Mapper.Map<MerchantGatewayViewData, MerchantGatewayDto>(gatewayViewData);
        }

        public static MerchantGatewayViewData ToMerchantGatewayViewData(this MerchantGatewayDto gatewayDto)
        {
            return Mapper.Map<MerchantGatewayDto, MerchantGatewayViewData>(gatewayDto);
        }

        public static IEnumerable<MerchantGatewayViewData> ToMerchantGatewayViewDataList(this IEnumerable<MerchantGatewayDto> gatewayDto)
        {
            return Mapper.Map<IEnumerable<MerchantGatewayDto>, IEnumerable<MerchantGatewayViewData>>(gatewayDto);
        }

        public static BookigFeeDto ToFeeDto(this BookingFeeViewData fee)
        {
            return Mapper.Map<BookingFeeViewData, BookigFeeDto>(fee);
        }

        public static IEnumerable<BookingFeeViewData> ToFeeViewDataList(this IEnumerable<BookigFeeDto> fee)
        {
            return Mapper.Map<IEnumerable<BookigFeeDto>, IEnumerable<BookingFeeViewData>>(fee);
        }

        public static BookingFeeViewData ToFeeViewData(this BookigFeeDto feeDto)
        {
            return Mapper.Map<BookigFeeDto, BookingFeeViewData>(feeDto);
        }

        public static DefaultSettingDto ToDefaultDto(this DefaultSettingViewData defaultViewData)
        {
            return Mapper.Map<DefaultSettingViewData, DefaultSettingDto>(defaultViewData);
        }

        public static DefaultSettingViewData ToDefaultViewData(this DefaultSettingDto defaultDto)
        {
            return Mapper.Map<DefaultSettingDto, DefaultSettingViewData>(defaultDto);
        }

        public static IEnumerable<DefaultSettingViewData> ToDefaultViewDataList(this IEnumerable<DefaultSettingDto> defaultDto)
        {
            return Mapper.Map<IEnumerable<DefaultSettingDto>, IEnumerable<DefaultSettingViewData>>(defaultDto);
        }

        public static ManageRecieptViewData ToreceiptViewData(this ManageRecieptDto recpt)
        {
            return Mapper.Map<ManageRecieptDto, ManageRecieptViewData>(recpt);
        }

        public static IEnumerable<SearchTransactionViewData> ToViewData(this IEnumerable<MTDTransactionDto> transactionDto)
        {
            return Mapper.Map<IEnumerable<MTDTransactionDto>, IEnumerable<SearchTransactionViewData>>(transactionDto);
        }

        public static TCViewData ToTermViewData(this TCDto termDto)
        {
            return Mapper.Map<TCDto, TCViewData>(termDto);
        }

        public static PrefixDto ToPrefixDto(this PrefixViewData prefixViewData)
        {
            return Mapper.Map<PrefixViewData, PrefixDto>(prefixViewData);
        }

        public static PrefixViewData ToPrefixViewData(this PrefixDto prefixDto)
        {
            return Mapper.Map<PrefixDto, PrefixViewData>(prefixDto);
        }

        public static IEnumerable<PrefixViewData> ToPrefixViewDataList(this IEnumerable<PrefixDto> prefixDto)
        {
            return Mapper.Map<IEnumerable<PrefixDto>, IEnumerable<PrefixViewData>>(prefixDto);
        }

        public static IEnumerable<DriverTransactionViewData> ToDriverViewData(this IEnumerable<DriverTransactionResult> transactionDto)
        {
            return Mapper.Map<IEnumerable<DriverTransactionResult>, IEnumerable<DriverTransactionViewData>>(transactionDto);
        }

        public static ScheduleReportDto ToScheduleDto(this ScheduleReportViewData scheduleViewData)
        {
            return Mapper.Map<ScheduleReportViewData, ScheduleReportDto>(scheduleViewData);
        }

        public static ScheduleReportViewData ToScheduleViewData(this ScheduleReportDto scheduleDto)
        {
            return Mapper.Map<ScheduleReportDto, ScheduleReportViewData>(scheduleDto);
        }

        public static RecipientDto ToRecipientDto(this RecipientViewData recipientViewData)
        {
            return Mapper.Map<RecipientViewData, RecipientDto>(recipientViewData);
        }

        public static RecipientViewData ToRecipientViewData(this RecipientDto recipientDto)
        {
            return Mapper.Map<RecipientDto, RecipientViewData>(recipientDto);
        }

        public static MerchantListDto ToMerchantListDto(this MerchantListViewData merchantViewData)
        {
            return Mapper.Map<MerchantListViewData, MerchantListDto>(merchantViewData);
        }

        public static IEnumerable<DrivernamelistViewData> ToViewDataList(this IEnumerable<DrivernamelistDto> driverlist)
        {
            return Mapper.Map<IEnumerable<DrivernamelistDto>, IEnumerable<DrivernamelistViewData>>(driverlist);
        }

        public static IEnumerable<RapidConnectViewData> ToRegisteredTerminalViewDataList(this IEnumerable<RapidConnectTerminalDto> terminalDto)
        {
            return Mapper.Map<IEnumerable<RapidConnectTerminalDto>, IEnumerable<RapidConnectViewData>>(terminalDto);
        }

        public static RapidConnectTerminalDto ToRcTerminalDto(this RapidConnectViewData rcViewData)
        {
            return Mapper.Map<RapidConnectViewData, RapidConnectTerminalDto>(rcViewData);
        }

        public static fDMerchantDto ToDTO(this FDMerchantViewData fdMerchant)//by asif added on 4/17/2015
        {
            return Mapper.Map<FDMerchantViewData, fDMerchantDto>(fdMerchant);
        }

        public static FDMerchantViewData ToFdMerchantViewData(this fDMerchantDto fdMerchant)//by asif added on 4/17/2015
        {
            return Mapper.Map<fDMerchantDto, FDMerchantViewData>(fdMerchant);
        }

        public static IEnumerable<FDMerchantListViewData> ToViewDataList(this IEnumerable<FDMerchantListDto> fdMerchantList)//by asif added on 4/17/2015
        {
            return Mapper.Map<IEnumerable<FDMerchantListDto>, IEnumerable<FDMerchantListViewData>>(fdMerchantList);
        }

        public static IEnumerable<DeviceNameListViewData> ToDeviceViewData(this IEnumerable<DeviceNameListDto> assignTerminalDto)
        {
            return Mapper.Map<IEnumerable<DeviceNameListDto>, IEnumerable<DeviceNameListViewData>>(assignTerminalDto);
        }

        public static DeviceNameListViewData ToDeviceView(this DeviceNameListDto deviceViewDto)//by asif added on 4/17/2015
        {
            return Mapper.Map<DeviceNameListDto, DeviceNameListViewData>(deviceViewDto);
        }

        public static DeviceNameListDto ToDeviceDto(this DeviceNameListViewData deviceView)//by asif added on 4/17/2015
        {
            return Mapper.Map<DeviceNameListViewData, DeviceNameListDto>(deviceView);
        }

        public static IEnumerable<DriverTxnListViewData> ToDto(this IEnumerable<DriverTxnListDto> dList)
        {
            return Mapper.Map<IEnumerable<DriverTxnListDto>, IEnumerable<DriverTxnListViewData>>(dList);
        }

        public static IEnumerable<VehicleTxnListViewData> ToDto(this IEnumerable<VehicleTxnListDto> vList)
        {
            return Mapper.Map<IEnumerable<VehicleTxnListDto>, IEnumerable<VehicleTxnListViewData>>(vList);
        }

        public static FleetSettingDto toFleetSettingDto(this FleetSettingViewData fleetSetting)//by asif added on 06/06/2015
        {
            return Mapper.Map<FleetSettingViewData, FleetSettingDto>(fleetSetting);
        }

        public static FleetSettingViewData toFleetSettingViewData(this FleetSettingDto fdMerchant)//by asif added on 6/8/2015
        {
            return Mapper.Map<FleetSettingDto, FleetSettingViewData>(fdMerchant);
        }

        public static IEnumerable<FleetViewData> TofleetSettingViewdataList(this IEnumerable<FleetDto> fleetSettingList)//by asif added on 6/8/2015
        {
            return Mapper.Map<IEnumerable<FleetDto>, IEnumerable<FleetViewData>>(fleetSettingList);
        }

        public static IEnumerable<AdminRecipientMasterViewData> ToViewDataList(this IEnumerable<AdminRecipienMastertListDto> merchantList)
        {
            return Mapper.Map<IEnumerable<AdminRecipienMastertListDto>, IEnumerable<AdminRecipientMasterViewData>>(merchantList);
        }

        public static AdminRecipientListDto ToDtoList(this AdminRecipientViewData adminRecipientDto)
        {
            return Mapper.Map<AdminRecipientViewData, AdminRecipientListDto>(adminRecipientDto);
        }

        public static IEnumerable<ReceiptMasterViewData> ToReceipMasterList(this IEnumerable<FleetReceiptViewData> fleetReceipt)
        {
            return Mapper.Map<IEnumerable<FleetReceiptViewData>, IEnumerable<ReceiptMasterViewData>>(fleetReceipt);
        }

        public static IEnumerable<ReportListViewData> ToReportList(this IEnumerable<ReportListDto> report)
        {
            return Mapper.Map<IEnumerable<ReportListDto>, IEnumerable<ReportListViewData>>(report);
        }

        public static IEnumerable<MerchantListDto> ToMerchantListDto(this IEnumerable<MerchantListViewData> merchantViewData)
        {
            return Mapper.Map<IEnumerable<MerchantListViewData>, IEnumerable<MerchantListDto>>(merchantViewData);
        }

        public static IEnumerable<AdminRecipienMastertListDto> ToDtoList(this IEnumerable<MerchantListViewData> adminRecipientDto)
        {
            return Mapper.Map<IEnumerable<MerchantListViewData>, IEnumerable<AdminRecipienMastertListDto>>(adminRecipientDto);
        }

        public static List<TerminalExcelUploadDto> ToTerminalDtoList(this List<TerminalExcelUploadViewData> terminalViewData)
        {
            return Mapper.Map<List<TerminalExcelUploadViewData>, List<TerminalExcelUploadDto>>(terminalViewData);
        }

        public static List<TerminalExcelUploadViewData> ToTerminalViewDataList(this List<TerminalExcelUploadDto> terminalViewData)
        {
            return Mapper.Map<List<TerminalExcelUploadDto>, List<TerminalExcelUploadViewData>>(terminalViewData);
        }

        public static List<DriverDto> ToDriverDtoList(this List<DriverViewData> driverViewData)
        {
            return Mapper.Map<List<DriverViewData>, List<DriverDto>>(driverViewData);
        }

        public static List<DriverViewData> ToDriverViewDataList(this List<DriverDto> driverDto)
        {
            return Mapper.Map<List<DriverDto>, List<DriverViewData>>(driverDto);
        }
        public static IEnumerable<DriverRecipientViewdata> ToDriverViewDataList(this IEnumerable<DriverRecipientDto> driverDto)
        {
            return Mapper.Map<IEnumerable<DriverRecipientDto>, IEnumerable<DriverRecipientViewdata>>(driverDto);
        }

        public static IEnumerable<DriverRecipientDto> TorecipientDtoList(this IEnumerable<DriverRecipientViewdata> recipient)
        {
            return Mapper.Map<IEnumerable<DriverRecipientViewdata>, IEnumerable<DriverRecipientDto>>(recipient);
        }

        public static DriverRecListDto toDto(this DriverRecListViewData DriverRecipient)//by asif added on 06/06/2015
        {
            return Mapper.Map<DriverRecListViewData, DriverRecListDto>(DriverRecipient);
        }

        public static IEnumerable<CarListViewData> ToViewDataList(this IEnumerable<CarListDto> carList)
        {
            return Mapper.Map<IEnumerable<CarListDto>, IEnumerable<CarListViewData>>(carList);
        }

        public static IEnumerable<DriverListViewData> ToViewDataList(this IEnumerable<DriverListsDto> driverList)
        {
            return Mapper.Map<IEnumerable<DriverListsDto>, IEnumerable<DriverListViewData>>(driverList);
        }

        public static IEnumerable<FleetListViewData> ToViewDataList(this IEnumerable<FleetListDto> fleetList)
        {
            return Mapper.Map<IEnumerable<FleetListDto>, IEnumerable<FleetListViewData>>(fleetList);
        }

        public static List<VehicleDto> ToDtoList(this List<VehicleViewData> VehicelViewData)
        {
            return Mapper.Map<List<VehicleViewData>, List<VehicleDto>>(VehicelViewData);
        }

        public static List<VehicleViewData> ToDriverViewDataList(this List<VehicleDto> driverDto)
        {
            return Mapper.Map<List<VehicleDto>, List<VehicleViewData>>(driverDto);
        }

        public static CarOwnerViewData ToCarOwner(this CarOwnersDto carOwners)
        {
            return Mapper.Map<CarOwnersDto, CarOwnerViewData>(carOwners);
        }

        public static CarOwnersDto ToCarOwner(this CarOwnerViewData carOwners)
        {
            return Mapper.Map<CarOwnerViewData, CarOwnersDto>(carOwners);
        }

        public static List<CarOwnerViewData> ToCarOwnerList(this IEnumerable<CarOwnersDto> carOwners)
        {
            return Mapper.Map<IEnumerable<CarOwnersDto>, List<CarOwnerViewData>>(carOwners);
        }
        public static List<CarOwnersDto> ToDtoList(this List<CarOwnerViewData> CarOwners)
        {
            return Mapper.Map<List<CarOwnerViewData>, List<CarOwnersDto>>(CarOwners);
        }

        public static ManyTxnClassDto[] ToDTO(this ManyTxnClass[] transactionView)
        {
            return Mapper.Map<ManyTxnClass[], ManyTxnClassDto[]>(transactionView);
        }

        public static PageSizeSearchDto ToPageSizeSearch(this PageSizeSearchViewData carOwners)
        {
            return Mapper.Map<PageSizeSearchViewData, PageSizeSearchDto>(carOwners);
        }

        public static IEnumerable<AllMerchantsViewData> ToViewDataList(this IEnumerable<AllMerchantsDto> merchants)
        {
            return Mapper.Map<IEnumerable<AllMerchantsDto>, IEnumerable<AllMerchantsViewData>>(merchants);
        }

        public static TransactionCountSlabViewData ToViewData(this TransactionCountSlabDto txnSlab)
        {
            return Mapper.Map<TransactionCountSlabDto, TransactionCountSlabViewData>(txnSlab);
        }

        public static TransactionCountSlabDto ToDto(this TransactionCountSlabViewData txnSlab)
        {
            return Mapper.Map<TransactionCountSlabViewData, TransactionCountSlabDto>(txnSlab);
        }

        //Report on demand
        public static IEnumerable<GetAdminSummaryReportViewData> ToAdminSummaryList(this IEnumerable<GetAdminSummaryReportDto> summaryData)
        {
            return Mapper.Map<IEnumerable<GetAdminSummaryReportDto>, IEnumerable<GetAdminSummaryReportViewData>>(summaryData);
        }

        public static IEnumerable<GetCardTypeReportViewData> ToCardTypeList(this IEnumerable<GetCardTypeReportDto> cardData)
        {
            return Mapper.Map<IEnumerable<GetCardTypeReportDto>, IEnumerable<GetCardTypeReportViewData>>(cardData);
        }

        public static IEnumerable<GetMerchantCardTypeReportViewData> ToMerchantCardTypeList(this IEnumerable<GetMerchantCardTypeReportDto> cardData)
        {
            return Mapper.Map<IEnumerable<GetMerchantCardTypeReportDto>, IEnumerable<GetMerchantCardTypeReportViewData>>(cardData);
        }

        public static IEnumerable<GetAdminExceptionReportViewData> ToExceptionList(this IEnumerable<GetAdminExceptionReportDto> summaryData)
        {
            return Mapper.Map<IEnumerable<GetAdminExceptionReportDto>, IEnumerable<GetAdminExceptionReportViewData>>(summaryData);
        }

        public static IEnumerable<GetTotalTransByVehicleReportViewData> ToVehicleReportList(this IEnumerable<GetTotalTransByVehicleReportDto> cardData)
        {
            return Mapper.Map<IEnumerable<GetTotalTransByVehicleReportDto>, IEnumerable<GetTotalTransByVehicleReportViewData>>(cardData);
        }
         
        public static IEnumerable<GetMerchantSummaryReportViewData> ToSummaryList(this IEnumerable<GetMerchantSummaryReportDto> cardData)
        {
            return Mapper.Map<IEnumerable<GetMerchantSummaryReportDto>, IEnumerable<GetMerchantSummaryReportViewData>>(cardData);
        }

        public static IEnumerable<GetDetailReportViewData> ToDetailList(this IEnumerable<GetDetailReportDto> summaryData)
        {
            return Mapper.Map<IEnumerable<GetDetailReportDto>, IEnumerable<GetDetailReportViewData>>(summaryData);
        }

        public static IEnumerable<GetMerchantExceptionReportViewData> ToMerchantSummaryExceptionList(this IEnumerable<GetMerchantExceptionReportDto> cardData)
        {
            return Mapper.Map<IEnumerable<GetMerchantExceptionReportDto>, IEnumerable<GetMerchantExceptionReportViewData>>(cardData);
        }

        public static IEnumerable<GetTotalTransByVehicleReportViewData> ToMerchantTotalTransList(this IEnumerable<GetTotalTransByVehicleReportDto> summaryData)
        {
            return Mapper.Map<IEnumerable<GetTotalTransByVehicleReportDto>, IEnumerable<GetTotalTransByVehicleReportViewData>>(summaryData);
        }

        public static IEnumerable<GetMerchantTotalTransByVehicleReportViewData> ToCorpTotalTransList(this IEnumerable<GetTotalTransByVehicleReportDto> summaryData)
        {
            return Mapper.Map<IEnumerable<GetTotalTransByVehicleReportDto>, IEnumerable<GetMerchantTotalTransByVehicleReportViewData>>(summaryData);
        }
        public static IEnumerable<GetMerchantTotalTransByVehicleReportViewData> ToMerchantTotalTransListView(this IEnumerable<GetMerchantTotalTransByVehicleReportDto> summaryData)
        {
            return Mapper.Map<IEnumerable<GetMerchantTotalTransByVehicleReportDto>, IEnumerable<GetMerchantTotalTransByVehicleReportViewData>>(summaryData);
        }

        public static IEnumerable<GetMerchantTotalTransByVehicleReportViewData> ToMerchantTotalTransList(this IEnumerable<GetMerchantTotalTransByVehicleReportDto> summaryData)
        {
            return Mapper.Map<IEnumerable<GetMerchantTotalTransByVehicleReportDto>, IEnumerable<GetMerchantTotalTransByVehicleReportViewData>>(summaryData);
        }

    }
}