﻿using System;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Xml;
using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Utility.Logging;
using MTData.Utility.Logging.InLogger;
using MTData.WebAPI.Models;
using Ninject;


namespace MTData.WebAPI.Controllers
{
    public class PaymentController : TransactionRequestController
    {
        private readonly ITransactionService _transactionService;
        private readonly ITerminalService _terminalService;
        private readonly IMerchantService _merchantService;
        private string _trackData;
        private string _deviceId;
        private PymtTypeType _paytype;
        private string _cardNumber;
        private string _tType;
        private TxnTypeType _tranType;
        private string _expiryDate;
        private CCVIndType _cvvInd;
        private string _cvv;
        private string _industryType;
        private string _amount;
        private string _driverNumber;
        private string _vehicleNumber;
        private string _rapidConnectAuthId;
        private string _cardPin;
        private string _cardSerial;
        private string _addAmount;
        private bool _isRegistered;
        private readonly StringBuilder _logMessage;
        readonly ILogger _logger = new Logger();
        public PaymentController(
            [Named("TransactionService")] ITransactionService transactionService, 
            [Named("MerchantService")] IMerchantService merchantService,
            [Named("TerminalService")] ITerminalService terminalService, StringBuilder logMessage)
            : base(terminalService, logMessage)
        {
            _merchantService = merchantService;
            _transactionService = transactionService;
            _terminalService = terminalService;
            _logMessage = logMessage;
            /*
            _driverService = driverService;
            _fleetService = fleetService;
            _surchargeService = surchargeService;
            _userService = userService;
            _receiptService = receiptService;
            _vehicleService = vehicleService;
            _logMessage = logMessage;
             */
        }

        fDMerchantDto _fdMerchantobj;
        DatawireDto _datawireObj;
        /// <summary>
        /// Purpose             :   Sending Credit and debit card transaction to rapid connect
        /// Function Name       :   SendPayment
        /// Created By          :   Salil Gupta
        /// Created On          :   02/27/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IHttpActionResult SendPayment(HttpRequestMessage request)
        {
            /*Transaction request in xml format */
            /*Transaction response in xml format received from Datawire */
            TerminalDto terminal;
            string clientRef;
            try
            {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);
            var xmlNode = xmlDoc.SelectSingleNode("TransactionRequest/DeviceId");
            if (xmlNode != null)
                _deviceId = xmlNode.InnerText;

            var paymentType = xmlDoc.SelectSingleNode("TransactionRequest/PaymentType");
            if (paymentType != null)
                    _paytype = (PymtTypeType) Enum.Parse(typeof (PymtTypeType), paymentType.InnerText);

            var trackData = xmlDoc.SelectSingleNode("TransactionRequest/TrackData");
            if (trackData != null)
                _trackData = trackData.InnerText;

            var cNumber = xmlDoc.SelectSingleNode("TransactionRequest/CardNumber");
            if (cNumber != null)
                _cardNumber = cNumber.InnerText;

            var transType = xmlDoc.SelectSingleNode("TransactionRequest/TransactionType");
            if (transType != null)
                _tType = transType.InnerText;

            if (_tType != "Void")
            {
                    if (transType != null)
                        _tranType = (TxnTypeType) Enum.Parse(typeof (TxnTypeType), transType.InnerText);
            }

            var eDate = xmlDoc.SelectSingleNode("TransactionRequest/ExpiryDate");
            if (eDate != null)
                _expiryDate = eDate.InnerText;

            var cvvInd = xmlDoc.SelectSingleNode("TransactionRequest/CCVInd");
            if (cvvInd != null)
                    _cvvInd = (CCVIndType) Enum.Parse(typeof (CCVIndType), cvvInd.InnerText);

            var cvNumber = xmlDoc.SelectSingleNode("TransactionRequest/CCVData");
            if (cvNumber != null)
                _cvv = cvNumber.InnerText;

            var indType = xmlDoc.SelectSingleNode("TransactionRequest/IndustryType");
            if (indType != null)
                _industryType = indType.InnerText;

            var singleNode = xmlDoc.SelectSingleNode("TransactionRequest/TransactionAmount");
            if (singleNode != null)
                _amount = singleNode.InnerText;

            var singleNodeDriver = xmlDoc.SelectSingleNode("TransactionRequest/DriverNumber");
            if (singleNodeDriver != null)
                _driverNumber = singleNodeDriver.InnerText;

            var singleNodeVehicleNo = xmlDoc.SelectSingleNode("TransactionRequest/VehicleNumber");
            if (singleNodeVehicleNo != null)
                _vehicleNumber = singleNodeVehicleNo.InnerText;

            var rapidConnectAuthId = xmlDoc.SelectSingleNode("TransactionRequest/RapidConnectAuthId");
            if (rapidConnectAuthId != null)
                _rapidConnectAuthId = rapidConnectAuthId.InnerText;
            //Debit
            var singleNodePinData = xmlDoc.SelectSingleNode("TransactionRequest/PinData");
            if (singleNodePinData != null)
                _cardPin = singleNodePinData.InnerText;

            var singleNodeKeySerNumData = xmlDoc.SelectSingleNode("TransactionRequest/KeySerNumData");
            if (singleNodeKeySerNumData != null)
                _cardSerial = singleNodeKeySerNumData.InnerText;

            var singleNodeKeyAddAmt = xmlDoc.SelectSingleNode("TransactionRequest/AddAmt");
            if (singleNodeKeyAddAmt != null)
                _addAmount = singleNodeKeyAddAmt.InnerText;
                
                terminal = _terminalService.GetTerminalDto(_deviceId);
            //_terminalObj = _merchantService.TerminalDetails(_deviceId);
            _fdMerchantobj = _merchantService.GetFDMerchant(terminal.fk_MerchantID);
            _datawireObj = _merchantService.GetDataWireDetails(terminal.fk_MerchantID, _deviceId);
                _isRegistered = _terminalService.IsRegistered(terminal.fk_MerchantID, _deviceId);


            /* Generate formatted XML data from above auth transaction request.*/
            TransactionRequest tr = new TransactionRequest
            {
                    FdMerchantId = Convert.ToString(_fdMerchantobj.FDMerchantID), //MTData
                MtdMerchantId = terminal.fk_MerchantID,
                SerialNumber = terminal.SerialNo,
                TerminalID = _datawireObj.RCTerminalId,
                Track2Data = _trackData,
                TransactionType = _tType,
                    TransType = _tranType,
                PaymentType = _paytype,
                TransactionAmount = _amount,
                CardNumber = _cardNumber,
                //CardType = _cardType,
                ExpiryDate = _expiryDate,
                CardCVV = _cvv,
                CCVIndType = _cvvInd,
                IndustryType = _industryType,
                TppId = _fdMerchantobj.ProjectId,
                GroupId = Convert.ToString(_fdMerchantobj.GroupId),
                Did = _datawireObj.DID,
                App = _fdMerchantobj.App,
                ServiceId = Convert.ToString(_fdMerchantobj.ServiceId),
                ServiceUrl = _fdMerchantobj.ServiceUrl,
                DriverNumber = _driverNumber,
                VehicleNumber = _vehicleNumber,
                RapidConnectAuthId = _rapidConnectAuthId,
                PinData = _cardPin,
                KeySerNumData = _cardSerial,
                AdditionalAmout = _addAmount,
                    
            };
                CreateTransactionRequest(tr);


            /* Generate Client Ref Number in the format <STAN>|<TPPID>, right justified and left padded with "0" */

                clientRef = GetClientRef();

                if (!_isRegistered)
            {
                    SendRegistrationRequest(clientRef, _fdMerchantobj, _datawireObj.RCTerminalId);
            }

            if (clientRef != null)
            {

                string xmlSerializedTransReq = GetXmlData();
                //Save transaction into MTDTransaction Table
                CommonFunctions cf = new CommonFunctions();
                MTDTransactionDto transactionDto = cf.XmlToTransactionDto(xmlSerializedTransReq, tr);
                _transactionService.Add(transactionDto);
                _terminalService.UpdateStan(transactionDto.Stan, transactionDto.TransRefNo, tr.SerialNumber);
                //end Save transaction into MTDTransaction Table
                    /* Send data using SOAP protocol to Datawire*/
                    string xmlSerializedTransResp = new SoapHandler().SendMessage(xmlSerializedTransReq, clientRef, tr);
                    //update response of transaction
                    MTDTransactionDto responsetransactionDto = cf.XmlResponseToTransactionDto(xmlSerializedTransResp);
                    _transactionService.Update(responsetransactionDto);
                    if (xmlSerializedTransResp == null)
                    {
                        tr.TransactionType = "Timeout";
                        CreateTransactionRequest(tr);
                        string xmlSerializedTimeoutReq = GetXmlData();
                        //Save transaction into MTDTransaction Table
                        CommonFunctions cftimeout = new CommonFunctions();
                        MTDTransactionDto transactiontimeoutDto = cftimeout.XmlToTransactionDto(xmlSerializedTimeoutReq, tr);
                        _transactionService.Add(transactiontimeoutDto);
                        _terminalService.UpdateStan(transactionDto.Stan, transactionDto.TransRefNo, tr.SerialNumber);
                        //end Save transaction into MTDTransaction Table
                /* Send data using SOAP protocol to Datawire*/
                        xmlSerializedTransResp = new SoapHandler().SendMessage(xmlSerializedTransReq, clientRef, tr);
                //update response of transaction
                        MTDTransactionDto responsetimeoutDto = cf.XmlResponseToTransactionDto(xmlSerializedTransResp);
                        _transactionService.Update(responsetimeoutDto);
                    }
                //end of update 
                    return Ok(xmlSerializedTransResp);
            }
        }
            catch
                (Exception ex)
        {

                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return Ok();
        }
    }
}