﻿namespace MTData.WebAPI.Models
{
    public class VisaGrp
    {
        public string CardLevelResult { get; set; }
        public string SourceReasonCode { get; set; }
        public string ACI { get; set; }
        public string TransID { get; set; }
        
    }
}