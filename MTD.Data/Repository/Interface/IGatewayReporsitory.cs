﻿using System.Collections.Generic;
using MTD.Data.Data;

namespace MTD.Data.Repository.Interface
{
  public  interface IGatewayReporsitory
    {
      int Create(Gateway gateway);
      bool Update(Gateway gateway);
      IEnumerable<Gateway> GatewaysList();
      void DeleteGateway(int gatewayId);
      Gateway GetGateway(int gatewayId);
      IEnumerable<Currency> GetGatewayCurrencyList();
      IEnumerable<Gateway> GetGatewayByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting);
      int GatewaysListCount(string name);
    }
}
