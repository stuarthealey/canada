﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTDataDispatchSystemSync.Models
{
    public class NextOfKinAddress
    {
        public int UnitAndNumber { get; set; }
        public string StreetAndDesignation { get; set; }
        public string Suburb { get; set; }
        public int PostCode { get; set; }
        public string StateRegion { get; set; }
    }
}