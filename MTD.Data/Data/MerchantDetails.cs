﻿using System;
namespace MTD.Data.Data
{
  public  class MerchantDetails
    {
        public int GatewayId { get; set; }
        public string Name { get; set; }
        public bool? IsActive { get; set; }
        public string GatewayUrl { get; set; }
        public string GatewayOffset { get; set; }
        public bool? IsSaleSupported { get; set; }
        public bool? IsPreauthSupported { get; set; }
        public bool? IsCaptureSupported { get; set; }
        public bool? IsCancelSupported { get; set; }
        public bool? IsRefundSupported { get; set; }
        public bool IsDefault { get; set; }
        public string GatewayUserName { get; set; }
        public string GatewayPCode { get; set; }
        public string GatewayCurrency { get; set; }
        //MerchantGateway
        public int MerchantGatewayID { get; set; }
        public Nullable<int> fk_MerchantID { get; set; }
        public int fk_GatewayID { get; set; }
        public string MerchantGatewayUserName { get; set; }
        public string MerchantGatewayPCode { get; set; }
        public Nullable<bool> MerchantGatewayIsActive { get; set; }
        public string MerchantGatewayName { get; set; }
        public string MerchantGatewayCompany { get; set; }
        public Nullable<bool> IsMtdataGateway { get; set; }
        public string Type { get; set; }
        //TerminalDto
        public int TerminalId { get; set; }
        public string SerialNo { get; set; }
        public string DeviceName { get; set; }
        public string MacAdd { get; set; }
        public string SoftVersion { get; set; }
        public string Description { get; set; }
        public int fk_MerchantId { get; set; }
        public bool TerminalIsActive { get; set; }
        public Nullable<bool> IsTerminalAssigned { get; set; }
        //MerchantDto
        public int MerchantID { get; set; }
        public string Company { get; set; }
        public Nullable<decimal> SetupFee { get; set; }
        public Nullable<decimal> MonthlyFee { get; set; }
        public string Disclaimer { get; set; }
        public string TermCondition { get; set; }
        public Nullable<decimal> StateTaxRate { get; set; }
        public Nullable<decimal> FederalTaxRate { get; set; }
        public Nullable<decimal> TipPerLow { get; set; }
        public Nullable<decimal> TipPerMedium { get; set; }
        public Nullable<decimal> TipPerHigh { get; set; }
        public bool MerchantIsActive { get; set; }
        public Nullable<bool> IsAllowSale { get; set; }
        public Nullable<bool> IsAllowPreauth { get; set; }
        public Nullable<bool> IsAllowCapture { get; set; }
        public Nullable<bool> IsAllowRefund { get; set; }
        public Nullable<bool> IsAllowVoid { get; set; }
        public bool IsTaxInclusive { get; set; }
        public decimal Surcharge { get; set; }
        public string CompanyTaxNumber { get; set; }

        public bool UseTransArmor { get; set; }     //PAY-13
    }
}
