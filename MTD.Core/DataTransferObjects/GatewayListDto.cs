﻿namespace MTD.Core.DataTransferObjects
{
   public  class GatewayListDto
    {
        public int GatewayID { get; set; }
        public string Name { get; set; }
    }
}
