﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace MTData.WebAPI.Models
{
    public class CapkData
    {
        public string Request { get; set; }
        public string Response { get; set; }
        public string CapK { get; set; }
        public XmlDocument XMLFileData { get; set; }
        public bool Status { get; set; }
        public string CurrFileCreationDt { get; set; }
        public string FileType { get; set; }
        public string FunCode { get; set; }
        public string FileSize { get; set; }
        public string FileCRC16 { get; set; }
    }
}