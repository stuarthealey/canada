﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class TableSDPinLessPOS
    {
        #region Private Constants and Variables
        #region Constants
        private const string tblId = "SD";
        private const string tag = "OC";
        private const string dataLength = "015";
        #endregion
        #region Variables
        private string otherTerminalCapabilities = "|31|30|30|30|30|30|30|30|30|30|30|30|30|30|30";
        #endregion
        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default SD in Request
        /// </summary>
        public static string TableID
        {
            get { return tblId; }
        }
        /// <summary>
        /// Return Tag 'OC' for PinLess POS Debit.
        /// </summary>
        public static string Tag
        {
            get { return tag; }
        }
        /// <summary>
        /// Retrun fixed data length as '015'.
        /// </summary>
        public static string DataLength
        {
            get { return dataLength; }
        }
        /// <summary>
        /// Return Other Terminal Capabilities of length 15.
        /// </summary>
        public string OtherTerminalCapabilities
        {
            get { return otherTerminalCapabilities; }
            set { otherTerminalCapabilities = value; }
        }
        #endregion
    }
}