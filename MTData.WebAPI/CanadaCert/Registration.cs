﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MTData.WebAPI.CanadaCert

{
    /// <summary>
    /// This class is used for sending merchant registration packet for the
    /// request class that can be used as the property in Request class for registration 
    /// of merchant with require property for datawire server
    /// </summary>
    [System.SerializableAttribute()]
    public class Registration
    {
        public string ServiceID { get; set; }
    }
}
