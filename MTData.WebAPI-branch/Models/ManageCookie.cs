﻿using System;
using System.Net.Http.Headers;
using System.Text;

namespace MTData.WebAPI.Models
{
    public class ManageCookie
    {
        public static string CookieValue { get; set; }
        public static string GenerateToken(string driverNo)
        {
            CryptLib _crypt = new CryptLib();
            string key = CryptLib.GetHashSha256("d985ca5f92e3ed87a71753bbc4fbf5c9", 31);
            string userIdEncrypt = _crypt.Encrypt(driverNo, key, "kFl18ZGH+9CMUtw=");
            string ranNumber = CryptLib.GenerateRandomIv(16);
            string tokenEncrypt = _crypt.Encrypt(ranNumber, key, "kFl18ZGH+9CMUtw=");
            string encryptedDriver = EncryptDriverWithToken(driverNo);
            string token = userIdEncrypt + ":" + encryptedDriver + ":" + tokenEncrypt;
            return token;
        }
        public static string CreateCookie(string token)
        {
            CookieHeaderValue cookie=new CookieHeaderValue("driverToken",token);
            cookie.Expires = DateTime.Now.AddDays(1);
            CookieValue = cookie.ToString();
            return CookieValue;
        }

        public static string EncryptDriverWithToken(string driverNo)
        {
            byte[] encData_byte;
            encData_byte = Encoding.UTF8.GetBytes(driverNo);
            string encodedData = Convert.ToBase64String(encData_byte);
            return encodedData;
           
        }
        public static string DecryptDriverFromToken(string token)
        {
            
            string[] tokenArray = token.Split(':');
            string encryptedDriverNo = tokenArray[1];
            UTF8Encoding encoder = new System.Text.UTF8Encoding();
            Decoder utf8Decode = encoder.GetDecoder();
            byte[] todecodeByte = Convert.FromBase64String(encryptedDriverNo);
            int charCount = utf8Decode.GetCharCount(todecodeByte, 0, todecodeByte.Length);
            char[] decodedChar = new char[charCount];
            utf8Decode.GetChars(todecodeByte, 0, todecodeByte.Length, decodedChar, 0);
            string result = new String(decodedChar);
            return result;


        }

     }
}