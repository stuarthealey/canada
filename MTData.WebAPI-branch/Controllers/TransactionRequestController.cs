﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web.Http;
using System.Xml;
using System.Xml.Serialization;
using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.WebAPI.Models;
using MTData.WebAPI.SrsService;
using Ninject;
using MTData.Utility.Logging;
using MTData.Utility.Logging.InLogger;
namespace MTData.WebAPI.Controllers
{

    public class TransactionRequestController : BaseController
    {
        protected string IndustryType;
        protected string PaymentType;
        protected string TransactionType;
        private static ITerminalService _terminalService;
        private readonly StringBuilder _logMessage;
        readonly ILogger _logger = new Logger();
        public TransactionRequestController([Named("TerminalService")] ITerminalService terminalService, StringBuilder logMessage)
            : base(terminalService, logMessage)
        {
            _terminalService = terminalService;
            _logMessage = logMessage;
        }

        private readonly GMFMessageVariants _gmfMsgVar = new GMFMessageVariants();
        private readonly CreditRequestDetails _creditReq = new CreditRequestDetails();                      // Credit request for Ecomm/retail transaction
        private readonly PinlessDebitRequestDetails _pinlessDebit = new PinlessDebitRequestDetails();       // Debit request for Ecomm transaction
        private readonly DebitRequestDetails _debitReq = new DebitRequestDetails();                         // Debit request for Retail transaction
        private readonly VoidTOReversalRequestDetails _revDetails = new VoidTOReversalRequestDetails();     // Void TO Reversal Request
        private readonly TARequestDetails _taRequest = new TARequestDetails();  
        /// <summary>
        /// Purpose             :   Finding credit and debit card request of Retail and ecommerce domain for First Data. Transaction Implemented Authorization, Completion, Sale, Refund, Void.
        /// Function Name       :   CreateTransactionRequest
        /// Created By          :   Salil Gupta
        /// Created On          :   03/10/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        public IHttpActionResult CreateTransactionRequest(TransactionRequest transactionRequest)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                              MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                string industryType = transactionRequest.IndustryType;
                string paymentType = transactionRequest.PaymentType.ToString();
                string transactionType = (transactionRequest.TransactionType == "Void" || transactionRequest.TransactionType == "Timeout")
                    ? "Void"
                    : transactionRequest.TransType.ToString();
                switch (industryType)
                {
                    case "ecommerce":
                        if (paymentType == "Credit")
                        {
                            switch (transactionType)
                            {
                                case "Authorization":
                                    CreditRequestDetails creditRequestAuthorization = CreateRequest(transactionRequest);
                                    return Ok(creditRequestAuthorization);
                                case "Completion":
                                    CreditRequestDetails creditRequestCompletion = CreateRequest(transactionRequest);
                                    return Ok(creditRequestCompletion);
                                case "Sale":
                                    CreditRequestDetails creditRequestSale = CreateRequest(transactionRequest);
                                    return Ok(creditRequestSale);
                                case "Refund":
                                    CreditRequestDetails creditRequestRefund = CreateRequest(transactionRequest);
                                    return Ok(creditRequestRefund);
                                case "Void":
                                    VoidTOReversalRequestDetails creditRequestVerification =
                                        ReversalRequest(transactionRequest);
                                    return Ok(creditRequestVerification);
                                case "TATokenRequest":
                                    TARequestDetails creditTaRequest = TaRequest(transactionRequest);
                                    return Ok(creditTaRequest);
                            }
                        }
                        if (paymentType == "Debit")
                        {
                            switch (transactionType)
                            {
                                case "Sale":
                                    PinlessDebitRequestDetails debitSale = CreatePinlessDebitRequest(transactionRequest);
                                    return Ok(debitSale);
                                case "Refund":
                                    PinlessDebitRequestDetails debitRefund =
                                        CreatePinlessDebitRequest(transactionRequest);
                                    return Ok(debitRefund);
                            }
                        }
                        break;
                    case "Retail":
                        if (paymentType == "Credit")
                        {
                                switch (transactionType)
                                {
                                    case "Authorization":
                                    CreditRequestDetails creditRequestAuthorization = CreateRequest(transactionRequest);
                                    return Ok(creditRequestAuthorization);
                                    case "Completion":
                                        CreditRequestDetails creditRequestCompletion = CreateRequest(transactionRequest);
                                        return Ok(creditRequestCompletion);
                                    case "Sale":
                                    CreditRequestDetails creditRequestSale = CreateRequest(transactionRequest);
                                    return Ok(creditRequestSale);
                                    case "Refund":
                                        CreditRequestDetails creditRequestRefund = CreateRequest(transactionRequest);
                                        return Ok(creditRequestRefund);
                                case "Void":
                                    VoidTOReversalRequestDetails creditRequestVerification =
                                        ReversalRequest(transactionRequest);
                                    return Ok(creditRequestVerification);
                                case "TATokenRequest":
                                    TARequestDetails creditTaRequest = TaRequest(transactionRequest);
                                    return Ok(creditTaRequest);
                                }
                            }
                        if (paymentType == "Debit")
                        {
                            switch (transactionType)
                            {
                                case "Sale":
                                    DebitRequestDetails debitSale = CreatePinDebitRequest(transactionRequest);
                                    return Ok(debitSale);
                                case "Refund":
                                    DebitRequestDetails debitRefund = CreatePinDebitRequest(transactionRequest);
                                    return Ok(debitRefund);
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return Ok();
        }
        /// <summary>
        /// Purpose             :   Creating credit card request
        /// Function Name       :   CreateRequest
        /// Created By          :   Salil Gupta
        /// Created On          :   03/16/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        private CreditRequestDetails CreateRequest(TransactionRequest transactionRequest)
        {
            /* Based on the GMF Specification UMF_Schema_V1.1.14.xsd, fields that are mandatory or related to 
                   this transaction should be populated.*/
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                              MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                MTDTransactionDto mtdTransaction = null;
                if (!string.IsNullOrEmpty(transactionRequest.RapidConnectAuthId))
                {
                     mtdTransaction = _terminalService.Completion(
                        transactionRequest.RapidConnectAuthId,
                   transactionRequest.SerialNumber, transactionRequest.MtdMerchantId);
                }

                #region Common Group

                CommonGrp cmnGrp = new CommonGrp();

                /* The payment type of the transaction. */
                PymtTypeType typePayment = transactionRequest.PaymentType;
                cmnGrp.PymtType = typePayment;
                cmnGrp.PymtTypeSpecified = true;
                /* merchant category code. */
                cmnGrp.MerchCatCode = ConfigurationManager.AppSettings["MerchantCategoryCode"];
                /* The type of transaction being performed. */
                cmnGrp.TxnType = transactionRequest.TransType;
                cmnGrp.TxnTypeSpecified = true;
                /* The local date and time in which the transaction was performed. */
                cmnGrp.LocalDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                /* The transmission date and time of the transaction (in GMT/UCT). */
                cmnGrp.TrnmsnDateTime = DateTime.UtcNow.ToString("yyyyMMddHHmmss");
                /* A number assigned by the merchant to uniquely reference the transaction. 
                 * This number must be unique within a day per Merchant ID per Terminal ID. */
                cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.SerialNumber);

                /* A number assigned by the merchant to uniquely reference a set of transactions. */
                //cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MerchantID, transactionRequest.TerminalID);//"480061115979";
                cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.SerialNumber);
                //"480061115979";


                /* A number assigned by the merchant to uniquely reference a transaction order sequence. */
                cmnGrp.OrderNum = cmnGrp.RefNum; //"162091864975";


                /* An ID assigned by First Data, for the Third Party Processor or 
                 * Software Vendor that generated the transaction. */
                cmnGrp.TPPID = transactionRequest.TppId;

                /* A unique ID assigned to a terminal. */
                cmnGrp.TermID = transactionRequest.TerminalID;

                //get terminal id from transaction request

                /* A unique ID assigned by First Data, to identify the Merchant. */
                cmnGrp.MerchID = transactionRequest.FdMerchantId;

                cmnGrp.GroupID = transactionRequest.GroupId;
                //Group ID value will be assigned by First Data. 
                //get terminal id from transaction request
                if (string.Equals(transactionRequest.IndustryType, "Ecommerce", StringComparison.CurrentCultureIgnoreCase))
                {
                    cmnGrp.POSEntryMode = "010";
                    cmnGrp.POSCondCode = POSCondCodeType.Item59;
                    cmnGrp.POSCondCodeSpecified = true;
                    cmnGrp.TermCatCode = TermCatCodeType.Item00;
                    cmnGrp.TermCatCodeSpecified = true;
                    cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item10;
                    cmnGrp.TermEntryCapabltSpecified = true;
                    cmnGrp.TermLocInd = TermLocIndType.Item1;
                    cmnGrp.TermLocIndSpecified = true;
                    cmnGrp.CardCaptCap = CardCaptCapType.Item0;
                    cmnGrp.CardCaptCapSpecified = true;

                    EcommGrp ecomgrp = new EcommGrp();
                    ecomgrp.EcommTxnInd = EcommTxnIndType.Item03;
                    ecomgrp.EcommTxnIndSpecified = true;
                    if (!string.Equals(cmnGrp.TxnType.ToString(), "Authorization", StringComparison.CurrentCultureIgnoreCase))
                        ecomgrp.EcommURL = "www.ex.com";
                    _creditReq.EcommGrp = ecomgrp;
                }



                if (string.Equals(transactionRequest.IndustryType, "retail", StringComparison.CurrentCultureIgnoreCase))
                {
                    cmnGrp.POSEntryMode = "901";
                    cmnGrp.POSCondCode = POSCondCodeType.Item00;
                    cmnGrp.POSCondCodeSpecified = true;
                    // cmnGrp.TermCatCode = TermCatCodeType.Item01;
                    cmnGrp.TermCatCode = TermCatCodeType.Item00;
                    cmnGrp.TermCatCodeSpecified = true;
                    cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item01;
                    cmnGrp.TermEntryCapabltSpecified = true;
                    cmnGrp.TermLocInd = TermLocIndType.Item0;
                    cmnGrp.TermLocIndSpecified = true;
                    cmnGrp.CardCaptCap = CardCaptCapType.Item1;
                    cmnGrp.CardCaptCapSpecified = true;

                }

                /* The amount of the transaction. This may be an authorization amount, 
                 * adjustment amount or a reversal amount based on the type of transaction. 
                 * It is inclusive of all additional amounts. 
                 * It is submitted in the currency represented by the Transaction Currency field.  
                 * The field is overwritten in the response for a partial authorization. */
                cmnGrp.TxnAmt = transactionRequest.TransactionAmount;

                /* The numeric currency of the Transaction Amount. */
                cmnGrp.TxnCrncy = "840";

                /* An indicator that describes the location of the terminal. */


                /* Indicates Group ID. */


                _creditReq.CommonGrp = cmnGrp;

                #endregion

                #region Card Group

                /* Populate values for Card Group */
                CardGrp cardGrp = new CardGrp();

                if (!(string.Equals(transactionRequest.IndustryType, "retail", StringComparison.CurrentCultureIgnoreCase) && cmnGrp.TxnType.ToString() != "Completion"))
                {
                    cardGrp.AcctNum = transactionRequest.CardNumber;
                    cardGrp.CardExpiryDate = transactionRequest.ExpiryDate;

                }
                else
                {
                    cardGrp.Track2Data = transactionRequest.Track2Data;
                }

                cardGrp.CardType = CreditCardType.GetCardType(transactionRequest.CardNumber);
                cardGrp.CardTypeSpecified = true;

                if (!string.Equals(cmnGrp.TxnType.ToString(), "completion", StringComparison.OrdinalIgnoreCase) && !string.Equals(cmnGrp.TxnType.ToString(), "Refund", StringComparison.OrdinalIgnoreCase))
                {
                    cardGrp.CCVInd = CCVIndType.Prvded;
                    cardGrp.CCVIndSpecified = true;
                    cardGrp.CCVData = transactionRequest.CardCVV; //card cvv
                }
                _creditReq.CardGrp = cardGrp;

                #endregion

                if (!string.Equals(cmnGrp.TxnType.ToString(), "completion", StringComparison.OrdinalIgnoreCase) && !string.Equals(cmnGrp.TxnType.ToString(), "Refund", StringComparison.OrdinalIgnoreCase))
                {
                    #region Additional Amount Group

                    AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();

                    addAmtGrp.PartAuthrztnApprvlCapablt = PartAuthrztnApprvlCapabltType.Item1;
                    addAmtGrp.PartAuthrztnApprvlCapabltSpecified = true;

                    AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[1];
                    addAmtGrpArr[0] = addAmtGrp;

                    _creditReq.AddtlAmtGrp = addAmtGrpArr;

                    #endregion
                }

                // Only for Visa Card

                #region Visa Group

                if (string.Equals(cardGrp.CardType.ToString(), "Visa", StringComparison.OrdinalIgnoreCase) && !string.Equals(cmnGrp.TxnType.ToString(), "Refund", StringComparison.OrdinalIgnoreCase))
                {
                    VisaGrp visaGrp = new VisaGrp();
                    visaGrp.VisaBID = "56412";
                    visaGrp.VisaAUAR = "000000000000";
                    if (!string.Equals(cmnGrp.TxnType.ToString(), "completion", StringComparison.OrdinalIgnoreCase))
                    {
                        visaGrp.ACI = ACIType.Y;
                        visaGrp.ACISpecified = true;
                        visaGrp.TaxAmtCapablt = TaxAmtCapabltType.Item1;
                        visaGrp.TaxAmtCapabltSpecified = true;
                    }
                    else
                    {
                        visaGrp.ACI = ACIType.T;
                        visaGrp.ACISpecified = true;
                    }
                    _creditReq.Item = visaGrp;
                }

                #endregion

                #region AmexGrp Group
                if (string.Equals(cardGrp.CardType.ToString(), "Amex", StringComparison.OrdinalIgnoreCase) && string.Equals(cmnGrp.TxnType.ToString(), "completion", StringComparison.OrdinalIgnoreCase))
                {

                   
                    AmexGrp amexgrp = new AmexGrp();
                    // amexgrp.AmExPOSData ="";
                    amexgrp.AmExTranID = mtdTransaction.GatewayTxnId;
                    _creditReq.Item = amexgrp;
                }
                #endregion

                #region Discover Group
                if ((string.Equals(cardGrp.CardType.ToString(), "Diners", StringComparison.OrdinalIgnoreCase) || string.Equals(cardGrp.CardType.ToString(), "Discover", StringComparison.OrdinalIgnoreCase) || string.Equals(cardGrp.CardType.ToString(), "JCB", StringComparison.OrdinalIgnoreCase)) && string.Equals(cmnGrp.TxnType.ToString(), "completion", StringComparison.OrdinalIgnoreCase))
                {

                    DSGrp dsGrp = new DSGrp();
                    dsGrp.DiscNRID = mtdTransaction.GatewayTxnId;
                    _creditReq.Item = dsGrp;
                }
                #endregion

                #region Customer info Group
                if (!string.Equals(cmnGrp.TxnType.ToString(), "Refund", StringComparison.OrdinalIgnoreCase))
                {
                    CustInfoGrp custinfo = new CustInfoGrp();
                    custinfo.AVSBillingAddr = "1307 Broad Hollow Road";
                    custinfo.AVSBillingPostalCode = "11747";
                    _creditReq.CustInfoGrp = custinfo;
                }
                #endregion

                //  this group required for visa completion trax
                #region addAm Group

                if (string.Equals(cmnGrp.TxnType.ToString(), "completion", StringComparison.OrdinalIgnoreCase))
                {
                    AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();
                    addAmtGrp.AddAmt = transactionRequest.TransactionAmount;
                    addAmtGrp.AddAmtCrncy = "840";
                        addAmtGrp.AddAmtType = AddAmtTypeType.FirstAuthAmt;
                        addAmtGrp.AddAmtTypeSpecified = true;

                    AddtlAmtGrp addAmtGrp1 = new AddtlAmtGrp();
                    addAmtGrp1.AddAmt = transactionRequest.TransactionAmount;
                    addAmtGrp1.AddAmtCrncy = "840";
                    addAmtGrp1.AddAmtType = AddAmtTypeType.TotalAuthAmt;
                    addAmtGrp1.AddAmtTypeSpecified = true;


                    AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[2];
                    addAmtGrpArr[0] = addAmtGrp;
                    addAmtGrpArr[1] = addAmtGrp1;
                    _creditReq.AddtlAmtGrp = addAmtGrpArr;

                    OrigAuthGrp orgAuthgrp = new OrigAuthGrp();
                    orgAuthgrp.OrigAuthID = mtdTransaction.AuthId; // "OK3542";

                    orgAuthgrp.OrigLocalDateTime = mtdTransaction.LocalDateTime;
                    orgAuthgrp.OrigTranDateTime = mtdTransaction.TransmissionDateTime;

                    orgAuthgrp.OrigSTAN = Convert.ToString(mtdTransaction.Stan); //"000001";
                    orgAuthgrp.OrigRespCode = Convert.ToString(mtdTransaction.ResponseCode); //"002";
                    _creditReq.OrigAuthGrp = orgAuthgrp;
                   
                }
                #endregion
                /* Add the data populated object to GMF message variant object */
                _gmfMsgVar.Item = _creditReq;
                return _creditReq;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;

        }
        /// <summary>
        /// Purpose             :   Creating pinless debit request
        /// Function Name       :   CreatePinlessDebitRequest
        /// Created By          :   Umesh
        /// Created On          :   03/20/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        private PinlessDebitRequestDetails CreatePinlessDebitRequest(TransactionRequest transactionRequest)
        {
            /* Based on the GMF Specification UMF_Schema_V1.1.14.xsd, fields that are mandatory or related to 
               this transaction should be populated.*/
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                              MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            #region Common Group

            /* Populate values for Common Group */
            CommonGrp cmnGrp = new CommonGrp();

            /* The payment type of the transaction. */
            //PymtTypeType typePayment = transactionRequest.PaymentType;
            cmnGrp.PymtType = PymtTypeType.PLDebit;
            //cmnGrp.PymtType = PymtTypeType.PLDebit;
            cmnGrp.PymtTypeSpecified = true;

            /* The type of transaction being performed. */
            cmnGrp.TxnType = transactionRequest.TransactionType == "Void" ? TxnTypeType.Sale : transactionRequest.TransType;
            cmnGrp.TxnTypeSpecified = true;

            /* The local date and time in which the transaction was performed. */
            cmnGrp.LocalDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");

            /* The transmission date and time of the transaction (in GMT/UCT). */
            cmnGrp.TrnmsnDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");

            /* A number assigned by the merchant to uniquely reference the transaction. 
             * This number must be unique within a day per Merchant ID per Terminal ID. */
            //cmnGrp.STAN = "611489";

            cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.SerialNumber);

            /* A number assigned by the merchant to uniquely reference a set of transactions. 
             * sThis number must be unique within a day for a given Merchant ID/ Terminal ID. */
            // cmnGrp.RefNum = "813927000521";
            cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.SerialNumber);
            /* Order number of the transaction
             */
            // cmnGrp.OrderNum = "249429379225";
            cmnGrp.OrderNum = cmnGrp.RefNum;
            /* An ID assigned by First Data, for the Third Party Processor or 
             * Software Vendor that generated the transaction. */
            cmnGrp.TPPID = transactionRequest.TppId;

            /* A unique ID assigned to a terminal. */
            cmnGrp.TermID = transactionRequest.TerminalID;

            /* A unique ID assigned by First Data, to identify the Merchant. */
            cmnGrp.MerchID = transactionRequest.FdMerchantId;


            /* An identifier used to indicate the terminal’s account number entry mode 
             * and authentication capability via the Point-of-Service. */
            cmnGrp.POSEntryMode = "010";

            /* An identifier used to indicate the authorization conditions at the Point-of-Service (POS). */
            cmnGrp.POSCondCode = POSCondCodeType.Item59;
            cmnGrp.POSCondCodeSpecified = true;

            /* An identifier used to describe the type of terminal being used for the transaction. */
            cmnGrp.TermCatCode = TermCatCodeType.Item00;
            cmnGrp.TermCatCodeSpecified = true;

            /* An identifier used to indicate the entry mode capability of the terminal. */
            cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item00;
            cmnGrp.TermEntryCapabltSpecified = true;

            /* The amount of the transaction. This may be an authorization amount, 
             * adjustment amount or a reversal amount based on the type of transaction. 
             * It is inclusive of all additional amounts. 
             * It is submitted in the currency represented by the Transaction Currency field.  
             * The field is overwritten in the response for a partial authorization. */
            cmnGrp.TxnAmt = transactionRequest.TransactionAmount;


            /* The numeric currency of the Transaction Amount. */
            cmnGrp.TxnCrncy = "840";

            /* An indicator that describes the location of the terminal. */
            cmnGrp.TermLocInd = TermLocIndType.Item1;
            cmnGrp.TermLocIndSpecified = true;

            /* Indicates whether or not the terminal has the capability to capture the card data. */
            cmnGrp.CardCaptCap = CardCaptCapType.Item0;
            cmnGrp.CardCaptCapSpecified = true;

            /* Indicates Group ID. */
            cmnGrp.GroupID = transactionRequest.GroupId;
            //Group ID value will be assigned by First Data.  //This is dummy value. Please use the actual value

            _pinlessDebit.CommonGrp = cmnGrp;

            #endregion

            #region Card Group

            /* Populate values for Card Group */
            CardGrp crdGrp = new CardGrp();
            /*Track 2 data*/
            crdGrp.AcctNum = transactionRequest.CardNumber;
            crdGrp.CardExpiryDate = transactionRequest.ExpiryDate;
            //crdGrp.Track2Data = "4017779999999011=16041011000013345678";
            _pinlessDebit.CardGrp = crdGrp;

            #endregion


            #region Ecommerce Group

            EcommGrp ecomgrp = new EcommGrp();
            ecomgrp.EcommTxnInd = EcommTxnIndType.Item03;
            ecomgrp.EcommTxnIndSpecified = true;

            ecomgrp.EcommURL = "www.ex.com";
            _pinlessDebit.EcommGrp = ecomgrp;

            #endregion


            /* Add the data populated object to GMF message variant object */
            _gmfMsgVar.Item = _pinlessDebit;
            return _pinlessDebit;


            }
            catch (Exception ex)
            {

                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;
        }
        /// <summary>
        /// Purpose             :   Creating pin debit request
        /// Function Name       :   CreatePinDebitRequest
        /// Created By          :   Umesh
        /// Created On          :   03/20/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY"  
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        private DebitRequestDetails CreatePinDebitRequest(TransactionRequest transactionRequest)
        {
            /* Based on the GMF Specification UMF_Schema_V1.1.14.xsd, fields that are mandatory or related to 
               this transaction should be populated.*/
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                              MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            #region Common Group
            /* Populate values for Common Group */
            CommonGrp cmnGrp = new CommonGrp();

            /* The payment type of the transaction. */
            cmnGrp.PymtType = PymtTypeType.Debit;
            cmnGrp.PymtTypeSpecified = true;

            /* The type of transaction being performed. */
            cmnGrp.TxnType = transactionRequest.TransactionType == "Void" ? TxnTypeType.Sale : transactionRequest.TransType;
            cmnGrp.TxnTypeSpecified = true;

            /* The local date and time in which the transaction was performed. */
            cmnGrp.LocalDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");

            /* The transmission date and time of the transaction (in GMT/UCT). */
            cmnGrp.TrnmsnDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");

            /* A number assigned by the merchant to uniquely reference the transaction. 
             * This number must be unique within a day per Merchant ID per Terminal ID. */
            cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.SerialNumber);

            /* A number assigned by the merchant to uniquely reference a set of transactions. 
             * sThis number must be unique within a day for a given Merchant ID/ Terminal ID. */
            // cmnGrp.RefNum = "813927000521";
            cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.SerialNumber);
            /* Order number of the transaction
             */
            // cmnGrp.OrderNum = "249429379225";
            cmnGrp.OrderNum = cmnGrp.RefNum;
            /* An ID assigned by First Data, for the Third Party Processor or 
             * Software Vendor that generated the transaction. */
            cmnGrp.TPPID = transactionRequest.TppId;

            /* A unique ID assigned to a terminal. */
            cmnGrp.TermID = transactionRequest.TerminalID;

            /* A unique ID assigned by First Data, to identify the Merchant. */
            cmnGrp.MerchID = transactionRequest.FdMerchantId;


            /* An identifier used to indicate the terminal’s account number entry mode 
             * and authentication capability via the Point-of-Service. */
            cmnGrp.POSEntryMode = "911";

            /* An identifier used to indicate the authorization conditions at the Point-of-Service (POS). */
            cmnGrp.POSCondCode = POSCondCodeType.Item00;
            cmnGrp.POSCondCodeSpecified = true;

            /* An identifier used to describe the type of terminal being used for the transaction. */
            cmnGrp.TermCatCode = TermCatCodeType.Item00;
            cmnGrp.TermCatCodeSpecified = true;

            /* An identifier used to indicate the entry mode capability of the terminal. */
            cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item01;
            cmnGrp.TermEntryCapabltSpecified = true;

            /* The amount of the transaction. This may be an authorization amount, 
             * adjustment amount or a reversal amount based on the type of transaction. 
             * It is inclusive of all additional amounts. 
             * It is submitted in the currency represented by the Transaction Currency field.  
             * The field is overwritten in the response for a partial authorization. */
            cmnGrp.TxnAmt = transactionRequest.TransactionAmount;

            /* The numeric currency of the Transaction Amount. */
            cmnGrp.TxnCrncy = "840";

            /* An indicator that describes the location of the terminal. */
            cmnGrp.TermLocInd = TermLocIndType.Item0;
            cmnGrp.TermLocIndSpecified = true;

            /* Indicates whether or not the terminal has the capability to capture the card data. */
            cmnGrp.CardCaptCap = CardCaptCapType.Item1;
            cmnGrp.CardCaptCapSpecified = true;

            /* Indicates Group ID. */
            cmnGrp.GroupID = transactionRequest.GroupId; //Group ID value will be assigned by First Data.  //This is dummy value. Please use the actual value

            _debitReq.CommonGrp = cmnGrp;
            #endregion

            #region Card Group
            /* Populate values for Card Group */
            CardGrp crdGrp = new CardGrp();
            /*Track 2 data*/
            //crdGrp.AcctNum = "4017779991111115";
            //crdGrp.CardExpiryDate = "20160430";
            crdGrp.Track2Data = "4017779991111115=18121010000000001";  // remove hard code
            //crdGrp.Track2Data = transactionRequest.Track2Data;
            _debitReq.CardGrp = crdGrp;
            #endregion

            #region PIN Group
            /* Populate values for PIN Group */
            PINGrp pinGroup = new PINGrp();

            /* The PIN Data for the Debit or EBT transaction being submitted.
             * HEXADecimal value need to be entered. */
            pinGroup.PINData = "3AA535C37D91C531";
            //pinGroup.PINData = transactionRequest.PinData; // remove hard code

            /* Provides the initialization vector for DUKPT PIN Debit and EBT transactions. */
            pinGroup.KeySerialNumData = "F876543210A004200171"; // remove hard code
            //pinGroup.KeySerialNumData = transactionRequest.KeySerNumData;

            _debitReq.PINGrp = pinGroup;
            #endregion

            #region Additional Amount Group

                if (string.Equals(cmnGrp.TxnType.ToString(), "Sale", StringComparison.OrdinalIgnoreCase))
                {
                    /*  Populate values for Additional Amount Group */
                    AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();

                    /* An identifier used to indicate whether or not the 
             * terminal/software can support partial authorization approvals.  */
                    addAmtGrp.PartAuthrztnApprvlCapablt = PartAuthrztnApprvlCapabltType.Item1;
                    addAmtGrp.PartAuthrztnApprvlCapabltSpecified = true;

                    /* Creating a generic array of Additional Amount 
             * Group type to sent the data to as an array */
                    AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[1];
                    addAmtGrpArr[0] = addAmtGrp;

                    _debitReq.AddtlAmtGrp = addAmtGrpArr;
                }

                #endregion

            /* Add the data populated object to GMF message variant object */
            _gmfMsgVar.Item = _debitReq;
            return _debitReq;
            }
            catch (Exception ex)
            {

                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
           return null;
        }
        /// <summary>
        /// Purpose             :   Creating Reversal Void tarnsaction request
        /// Function Name       :   ReversalRequest
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   03/20/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        public VoidTOReversalRequestDetails ReversalRequest(TransactionRequest transactionRequest)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                              MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            #region Common Group
                MTDTransactionDto mtdTransaction = null;
                if (!string.IsNullOrEmpty(transactionRequest.RapidConnectAuthId))
                {
                    mtdTransaction = _terminalService.Completion(
                       transactionRequest.RapidConnectAuthId,
                   transactionRequest.SerialNumber, transactionRequest.MtdMerchantId);
                }
            /* Populate values for Common Group */
            CommonGrp cmnGrp = new CommonGrp();

            /* The payment type of the transaction. */
            PymtTypeType typePayment = transactionRequest.PaymentType;
            cmnGrp.PymtType = typePayment;
            cmnGrp.PymtTypeSpecified = true;
            cmnGrp.ReversalInd = transactionRequest.TransactionType == "Void" ? ReversalIndType.Void : ReversalIndType.Timeout;
            cmnGrp.ReversalIndSpecified = true;

            //cmnGrp.MerchCatCode = "5965";
            /* The type of transaction being performed. */
            if (mtdTransaction.TxnType == "Sale")
                cmnGrp.TxnType = TxnTypeType.Sale;
            if (mtdTransaction.TxnType == "Refund")
                cmnGrp.TxnType = TxnTypeType.Refund;
            if (mtdTransaction.TxnType == "Authorization")
                cmnGrp.TxnType = TxnTypeType.Authorization;
            if (mtdTransaction.TxnType == "Completion")
                cmnGrp.TxnType = TxnTypeType.Completion;
            cmnGrp.TxnTypeSpecified = true;

            /* The local date and time in which the transaction was performed. */
            cmnGrp.LocalDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");

            /* The transmission date and time of the transaction (in GMT/UCT). */
            cmnGrp.TrnmsnDateTime = DateTime.UtcNow.ToString("yyyyMMddHHmmss");

            /* A number assigned by the merchant to uniquely reference the transaction. 
             * This number must be unique within a day per Merchant ID per Terminal ID. */
            cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalID);

            /* A number assigned by the merchant to uniquely reference a set of transactions. */
            cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalID);
            //"480061115979";

            /* A number assigned by the merchant to uniquely reference a transaction order sequence. */
            cmnGrp.OrderNum = cmnGrp.RefNum; //"162091864975";

            /* An ID assigned by First Data, for the Third Party Processor or 
             * Software Vendor that generated the transaction. */
            cmnGrp.TPPID = transactionRequest.TppId;

            /* A unique ID assigned to a terminal. */
            cmnGrp.TermID = transactionRequest.TerminalID;

            /* A unique ID assigned by First Data, to identify the Merchant. */
            cmnGrp.MerchID = transactionRequest.FdMerchantId;

            if (string.Equals(transactionRequest.IndustryType, "Ecommerce", StringComparison.CurrentCultureIgnoreCase))
            {
                cmnGrp.POSEntryMode = "010";
                cmnGrp.POSCondCode = POSCondCodeType.Item59;
                cmnGrp.POSCondCodeSpecified = true;
                cmnGrp.TermCatCode = TermCatCodeType.Item00;
                cmnGrp.TermCatCodeSpecified = true;
                cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item10;
                cmnGrp.TermEntryCapabltSpecified = true;
                cmnGrp.TermLocInd = TermLocIndType.Item1;
                cmnGrp.TermLocIndSpecified = true;
                cmnGrp.CardCaptCap = CardCaptCapType.Item0;
                cmnGrp.CardCaptCapSpecified = true;

                EcommGrp ecomgrp = new EcommGrp();
                ecomgrp.EcommTxnInd = EcommTxnIndType.Item03;
                ecomgrp.EcommTxnIndSpecified = true;
                
                _revDetails.EcommGrp = ecomgrp;
            }

            /* An identifier used to indicate the terminal’s account number entry mode 
             * and authentication capability via the Point-of-Service. */

            if (string.Equals(transactionRequest.IndustryType, "Retail", StringComparison.CurrentCultureIgnoreCase))
            {
                cmnGrp.POSEntryMode = "901";
                cmnGrp.POSCondCode = POSCondCodeType.Item00;
                cmnGrp.POSCondCodeSpecified = true;
                cmnGrp.TermCatCode = TermCatCodeType.Item00;
                cmnGrp.TermCatCodeSpecified = true;
                cmnGrp.TermEntryCapablt = TermEntryCapabltType.Item00;
                cmnGrp.TermEntryCapabltSpecified = true;
                cmnGrp.TermLocInd = TermLocIndType.Item1;
                cmnGrp.TermLocIndSpecified = true;
                /* Indicates whether or not the terminal has the capability to capture the card data. */
                cmnGrp.CardCaptCap = CardCaptCapType.Item1;
                cmnGrp.CardCaptCapSpecified = true;
            }
            /* The amount of the transaction. This may be an authorization amount, 
             * adjustment amount or a reversal amount based on the type of transaction. 
             * It is inclusive of all additional amounts. 
             * It is submitted in the currency represented by the Transaction Currency field.  
             * The field is overwritten in the response for a partial authorization. */
            cmnGrp.TxnAmt = transactionRequest.TransactionAmount;

            /* The numeric currency of the Transaction Amount. */
            cmnGrp.TxnCrncy = "840";

            /* An indicator that describes the location of the terminal. */


            /* Indicates Group ID. */
            cmnGrp.GroupID = transactionRequest.GroupId;
            _revDetails.CommonGrp = cmnGrp;
            #endregion
            /* Populate values for Card Group */
            #region Card Group
            CardGrp cardGrp = new CardGrp();
            
            cardGrp.AcctNum = transactionRequest.CardNumber;
            cardGrp.CardExpiryDate = transactionRequest.ExpiryDate;
            cardGrp.CardType = CreditCardType.GetCardType(transactionRequest.CardNumber);
            cardGrp.CardTypeSpecified = true;
           
            
            _revDetails.CardGrp = cardGrp;
            #endregion

            #region Additional Amount Group
            AddtlAmtGrp addAmtGrp = new AddtlAmtGrp();
            addAmtGrp.AddAmt = transactionRequest.TransactionAmount;
            addAmtGrp.AddAmtCrncy = "840";
            addAmtGrp.AddAmtType = AddAmtTypeType.TotalAuthAmt;
            addAmtGrp.AddAmtTypeSpecified = true;
            AddtlAmtGrp[] addAmtGrpArr = new AddtlAmtGrp[1];
            addAmtGrpArr[0] = addAmtGrp;
            _revDetails.AddtlAmtGrp = addAmtGrpArr;
            #endregion

            //#region AmexGrp Group
            //if (cardGrp.CardType.ToString().Equals("Amex"))
            //{
            //    AmexGrp amexgrp = new AmexGrp();
            //    amexgrp.AmExTranID = mtdTransaction.GatewayTxnId;
            //    _revDetails.Item = amexgrp;
            //}
            //#endregion

            #region Discover Group
            if (cardGrp.CardType.ToString().Equals("Discover"))
            {
                DSGrp dsGrp = new DSGrp();

                dsGrp.DiscNRID = mtdTransaction.GatewayTxnId;
                _revDetails.Item = dsGrp;
            }
            #endregion

            #region Visa Group
            if (cardGrp.CardType.ToString().Equals("Visa"))
            {
                VisaGrp visaGrp = new VisaGrp();
                visaGrp.ACI = ACIType.T;
                visaGrp.ACISpecified = true;
                _revDetails.Item = visaGrp;
            }
            #endregion

            //  this group required for visa completion trax
            #region originalAuth Group

                OrigAuthGrp orgAuthgrp = new OrigAuthGrp();
                orgAuthgrp.OrigAuthID = mtdTransaction.AuthId;
                orgAuthgrp.OrigLocalDateTime = mtdTransaction.LocalDateTime;
                orgAuthgrp.OrigTranDateTime = mtdTransaction.TransmissionDateTime;
                orgAuthgrp.OrigSTAN = mtdTransaction.Stan;
                orgAuthgrp.OrigRespCode = mtdTransaction.ResponseCode;
                _revDetails.OrigAuthGrp = orgAuthgrp;
            

            #endregion

            /* Add the data populated object to GMF message variant object */
            _gmfMsgVar.Item = _revDetails;
            return _revDetails;


            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;
        }

        public TARequestDetails TaRequest(TransactionRequest transactionRequest)
        {

            #region Common Group

            /* Populate values for Common Group */
            CommonGrp cmnGrp = new CommonGrp();

            /* The payment type of the transaction. */
            PymtTypeType typePayment = transactionRequest.PaymentType;
            cmnGrp.PymtType = typePayment;
            cmnGrp.PymtTypeSpecified = true;

            //cmnGrp.MerchCatCode = "5965";
            /* The type of transaction being performed. */
            cmnGrp.TxnType = TxnTypeType.TATokenRequest;
            cmnGrp.TxnTypeSpecified = true;

            /* The local date and time in which the transaction was performed. */
            cmnGrp.LocalDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");

            /* The transmission date and time of the transaction (in GMT/UCT). */
            cmnGrp.TrnmsnDateTime = DateTime.UtcNow.ToString("yyyyMMddHHmmss");

            /* A number assigned by the merchant to uniquely reference the transaction. 
             * This number must be unique within a day per Merchant ID per Terminal ID. */
            cmnGrp.STAN = GetSystemTraceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalID);

            /* A number assigned by the merchant to uniquely reference a set of transactions. */
            cmnGrp.RefNum = GetReferenceNumber(transactionRequest.MtdMerchantId, transactionRequest.TerminalID);
            //"480061115979";

            /* A number assigned by the merchant to uniquely reference a transaction order sequence. */
            //cmnGrp.OrderNum = cmnGrp.RefNum; //"162091864975";

            /* An ID assigned by First Data, for the Third Party Processor or 
             * Software Vendor that generated the transaction. */
            cmnGrp.TPPID = transactionRequest.TppId;

            /* A unique ID assigned to a terminal. */
            cmnGrp.TermID = transactionRequest.TerminalID;

            /* A unique ID assigned by First Data, to identify the Merchant. */
            cmnGrp.MerchID = transactionRequest.FdMerchantId;

            /* Indicates Group ID. */
            cmnGrp.GroupID = transactionRequest.GroupId;
            _taRequest.CommonGrp = cmnGrp;

            #endregion

            #region Card Group

            /* Populate values for Card Group */
            CardGrp cardGrp = new CardGrp();

            cardGrp.AcctNum = transactionRequest.CardNumber;

            _taRequest.CardGrp = cardGrp;

            #endregion

            #region TransArmor group

            /* Populate values for Card Group */
            TAGrp taGrp = new TAGrp();

            taGrp.SctyLvl = SctyLvlType.Tknizatn;
            taGrp.SctyLvlSpecified = true;

            taGrp.TknType = "1174";

            _taRequest.TAGrp = taGrp;

            #endregion


            /* Add the data populated object to GMF message variant object */
            _gmfMsgVar.Item = _taRequest;

            return _taRequest;

        }

        /*  */
        /// <summary>
        /// Purpose             :   Generate Client Ref Number in the format STAN|TPPID, right justified and left padded with "0"
        /// Function Name       :   GetClientRef
        /// Created By          :   Umesh
        /// Created On          :   03/23/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY"  
        /// </summary>
        /// <returns></returns>
        public string GetClientRef()
        {
            CreditRequestDetails req = _gmfMsgVar.Item as CreditRequestDetails;
            if (req != null)
            {
                string clientRef = req.CommonGrp.STAN + "|" + req.CommonGrp.TPPID;
                clientRef = "00" + clientRef;
                return clientRef;
            }
            return "";
        }

        
        /// <summary>
        /// Purpose             :   The method will convert the GMF transaction object into an XML string
        /// Function Name       :   GetXmlData
        /// Created By          :   Umesh
        /// Created On          :   03/24/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY"  
        /// </summary>
        /// <returns></returns>
        public String GetXmlData()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                              MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
            MemoryStream memoryStream = new MemoryStream();
            XmlSerializer xs = new XmlSerializer(_gmfMsgVar.GetType());
            XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, System.Text.Encoding.UTF8);
            xs.Serialize(xmlTextWriter, _gmfMsgVar);
            memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
            var encoding = new System.Text.UTF8Encoding();
            string xmlString = encoding.GetString(memoryStream.ToArray());
            memoryStream.Dispose();
            xmlString = xmlString.Substring(1, xmlString.Length - 1);
            return xmlString;
            }
            catch (Exception ex)
            {

                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :   Sending request to register the terminal with datawire
        /// Function Name       :   SendRegistrationRequest
        /// Created By          :   Salil Gupta
        /// Created On          :   03/25/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY"
        /// </summary>
        /// <param name="clientRef"></param>
        /// <param name="fdMerchantobj"></param>
        /// <param name="tId"></param>
        /// <returns></returns>
        public RespClientIDType SendRegistrationRequest(string clientRef, fDMerchantDto fdMerchantobj, string tId)
        {
            RespClientIDType response = new RespClientIDType();
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                              MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                ResponseType res;
                using (srsPortTypeClient client = new srsPortTypeClient())
                {
                    ReqClientIDType reqClientIdType = new ReqClientIDType();
                    reqClientIdType.App = fdMerchantobj.App;
                    reqClientIdType.DID = "";
                    reqClientIdType.Auth = fdMerchantobj.GroupId + fdMerchantobj.FDMerchantID + "|" + tId;
                    reqClientIdType.ClientRef = GetClientRef(); // clientRef;
                    RegistrationType registrationType = new RegistrationType();
                    registrationType.ServiceID = Convert.ToString(fdMerchantobj.ServiceId);
                    RequestType requestType = new RequestType();
                    requestType.ReqClientID = reqClientIdType;
                    requestType.Item = registrationType;
                    SrsOperationRequest operationRequest = new SrsOperationRequest();
                    operationRequest.Request = requestType;
                    res = client.SrsOperation(requestType);
                }

                if (res.Status.StatusCode.ToString() == "OK")
                {
                    ResponseType activeResponse = SendActivationRequest(res.RespClientID.ClientRef, res.RespClientID.DID, fdMerchantobj, tId);
                    if (activeResponse.Status.StatusCode.ToString() == "OK")
                    {
                        try
                        {
                            response = res.Item as RespClientIDType;
                            return response;
                        }
                        catch (Exception ex)
                        {
                            _logger.LogInfoFatel(_logMessage.ToString(), ex);
                        }
                    }
                }
                else if (res.Status.StatusCode.ToString() == "Retry")
                {
                    SendRegistrationRequest(clientRef, fdMerchantobj, tId);
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;
        }

        /// <summary>
        /// Purpose             :   Sending request to register the terminal with datawire
        /// Function Name       :   SendActivationRequest
        /// Created By          :   Salil Gupta
        /// Created On          :   03/25/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   04/02/2015 "MM/DD/YYYY" 
        /// </summary>
        /// <param name="clientRef"></param>
        /// <param name="dId"></param>
        /// <param name="fdMerchantobj"></param>
        /// <param name="tId"></param>
        /// <returns></returns>
        private ResponseType SendActivationRequest(string clientRef, string dId, fDMerchantDto fdMerchantobj, string tId)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " +
                              MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture,
                    " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                ResponseType response;
                using (srsPortTypeClient client = new srsPortTypeClient())
                {
                    ReqClientIDType reqClientIdType = new ReqClientIDType();
                    reqClientIdType.App = fdMerchantobj.App; //"RAPIDCONNECTSRS";
                    reqClientIdType.DID = dId;
                    reqClientIdType.Auth = fdMerchantobj.GroupId + fdMerchantobj.FDMerchantID + "|" + tId;
                        // "10001RCTST0000008087|00000002";
                    reqClientIdType.ClientRef = clientRef;
                    ActivationType activationType = new ActivationType();
                    activationType.ServiceID = Convert.ToString(fdMerchantobj.ServiceId);
                    RequestType requestType = new RequestType();
                    requestType.ReqClientID = reqClientIdType;
                    requestType.Item = activationType;
                    SrsOperationRequest operationRequest = new SrsOperationRequest();
                    operationRequest.Request = requestType;
                    //Firstdata.RapidConnect.Datawire.Soap.rcService rc = new rcService();
                    //rc.Url = "https://stagingsupport.datawire.net/rc/srssoap/";
                    response = client.SrsOperation(operationRequest.Request);
                }
                return response;


            }
            catch (Exception ex)
            {
                _logger.LogInfoFatel(_logMessage.ToString(), ex);
            }
            return null;
        }

    }
}

