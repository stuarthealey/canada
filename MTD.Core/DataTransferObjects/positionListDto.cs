﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
   public class PositionListDto
    {
        public string Field { get; set; }
        public int? value { get; set; }
        public bool isChecked { get; set; }
    }
}
