﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class TAKeyResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public ResponseHeader Header { get; set; }
    }
}