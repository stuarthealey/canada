﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table48Bit48
    {
        #region Private Constants and Variables
        
        #region Constants
        
        private const string _lengthAttribute = "0031";

        #endregion

        #region Variables
        
        private string _transCode = "3939";
        private string _zipCode = "4c3457354134202020";
        private string _streetAddress = "3120576573746d696e7374657220202020202020";

        #endregion
        
        #endregion

        #region Public Properties

        public string lengthAttribute
        {
            get { return _lengthAttribute; }
        }

        public string transCode
        {
            get { return _transCode; }
            set { _transCode = value; }
        }

        public string streetAddress
        {
            get { return _streetAddress; }
            set { _streetAddress = value; }
        }

        public string ZipCode
        {
            get { return _zipCode; }
            set { _zipCode = value; }
        }

        #endregion
    }
}