﻿using System;
using System.Configuration;
using System.Web.Configuration;
using System.Web.Mvc;

namespace MTData.WebAPI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            bool doEncrypt = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["isEncrypt"]);
            if (doEncrypt)
                Encrypt();
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        [NonAction]
        public void Encrypt()
        {
            ProtectSection("connectionStrings", "RSAProtectedConfigurationProvider");
            ProtectSection("appSettings", "RSAProtectedConfigurationProvider");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sectionName"></param>
        /// <param name="provider"></param>
        [NonAction]
        private void ProtectSection(string sectionName, string provider)
        {
            Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
            ConfigurationSection section = config.GetSection(sectionName);
            if (section != null && !section.SectionInformation.IsProtected)
            {
                section.SectionInformation.ProtectSection(provider);
                config.Save();
            }
        }
    }
}
