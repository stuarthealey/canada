﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace MTData.Web.ViewData
{
    public class TerminalViewData
    {
        public int TerminalID { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "Please enter the serial number")]
        [Display(Name = "Serial number")]
        [RegularExpression(@"^[A-Za-z,0-9, - , : , .]+$", ErrorMessage = "Serial number  is not valid")]
        public string SerialNo { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "Please select device name")]
        [Display(Name = "Device name")]
        [RegularExpression(@"^[A-Za-z,0-9, - , : , .]+$", ErrorMessage = "Device name  is not valid")]
        public string DeviceName { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "Please enter the mac address")]
        [Display(Name = "Mac address")]
        [RegularExpression(@"^[A-Za-z,0-9, - , : , .]+$", ErrorMessage = "Mac address  is not valid")]
        public string MacAdd { get; set; }

        public string Device { get; set; }
        public string SoftVersion { get; set; }
        public string Description { get; set; }
        public int fk_MerchantID { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Nullable<bool> IsTerminalAssigned { get; set; }
        public string Stan { get; set; }
        public string RefNumber { get; set; }
        public DateTime? TransactionDate { get; set; }
        public Nullable<int> DatawireId { get; set; }
        public string TermId { get; set; }
        public IEnumerable<RapidConnectViewData> RegisteredTerminalList { get; set; }
        public IEnumerable<DeviceNameListViewData> DeviceList { get; set; }
        //Ravit : Upload Excel file when create Terminal device
        public HttpPostedFileBase uploadFile { get; set; }
        public string Contact { get; set; }
        public string ContactLess { get; set; }


        public List<TerminalExcelUploadViewData> TerminalExcelUploadViewData { get; set; }
        public decimal? FileVersion { get; set; }
        public decimal? TAKeyFileVersion { get; set; }
        public bool IsTerminalError { get; set; }
        public Nullable<bool> IsContactLess { get; set; }
        //public Nullable<decimal> FileVersion { get; set; }
        //public Nullable<decimal> TAKeyFileVersion { get; set; }

    }
}