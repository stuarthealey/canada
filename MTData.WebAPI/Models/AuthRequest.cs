﻿using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class AuthRequest
    {
        public string DriverNumber { get; set; }
        public string FleetId { get; set; }
        public int TransId { get; set; } 
        public string Token { get; set; }
        public string DeviceId { get; set; }

    }
}