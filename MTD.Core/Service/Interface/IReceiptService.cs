﻿using System.Collections.Generic;
using MTD.Core.DataTransferObjects;


namespace MTD.Core.Service.Interface
{
   public interface IReceiptService
    {
       IEnumerable<ReceiptListDto> ReceiptByfleetId(int fleetId);

       IEnumerable<ReceiptListDto> ReceiptToShowByfleetId(int fleetId);

       IEnumerable<ReceiptFormatDto> ReceiptList(int merchantId,string name,int logOnByUserId,int logOnByUserType);

        IEnumerable<FleetReceiptDto> GetReceiptFleet(int fleetId);

       bool UpdateReceiptMaster(ReceiptMasterDto receiptMasterDto);

       IEnumerable<FleetDto> GetUserFleet(int merchantId);

       IEnumerable<ReceiptMasterDto> GetReceiptFields();

       bool Update(ReceiptFormatDto fleetDto, int fleetId);

       void DeleteReceipt(int fleetId);

       void DeleteReceipt(int fleetId, int merchantId);

       void DeleteReceiptField(int fieldId);

       void AddReciept(ReceiptFormatDto recieptDto);

        void AddRecieptField(string field, ReceiptMasterDto receiptMasterDto);

       bool IsExistFleet(int fleetId);

        bool IsExistFleetForUpdate(int fleetId, int receiptId);

       bool IsFieldExist(int fieldId);

       IEnumerable<ReceiptFormatDto> ReceiptListByFilter(FilterSearchParametersDto filterSearch, int logOnByUserId, int logOnByUserType);

        IEnumerable<FleetDto> GetFleetList(int merchantId);

    }
}
