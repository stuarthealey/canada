﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace MTData.WebAPI.Common
{
    /// <summary>
    /// 
    /// </summary>
    public class RSAEncryptionLogic : IEncryptionLogic
    {
        #region Definitions

        public enum RSAKeySize
        {
            RSA2048 = 2048,
            RSA4096 = 4096
        }

        #endregion

        #region Singleton Accessors

        private static readonly RSAEncryptionLogic _Instance = new RSAEncryptionLogic();

        public static RSAEncryptionLogic Instance
        {
            get { return _Instance; }
        }

        #endregion

        #region Attributes

        private RSACryptoServiceProvider _RsaProvider;

        #endregion

        #region Public Accessors

        //public int KeySize { get; private set; }

        //public int Exponent { get; private set; } // Typically will be 65537 (0x10001) (most commonly used prime for RSA exponent)

        #endregion

        #region Constructors

        private RSAEncryptionLogic()
        {
            _RsaProvider = new RSACryptoServiceProvider();

            try
            {
                string RSAKeyPath = System.Configuration.ConfigurationManager.AppSettings["RSAKeyPath"];
                string RSAKeyXml;

                using (StreamReader streamReader = new StreamReader(RSAKeyPath))
                {
                    RSAKeyXml = streamReader.ReadToEnd();
                }

                _RsaProvider.FromXmlString(RSAKeyXml);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        #endregion


        #region Public Methods

        public string GenerateNewKey(RSAKeySize keySize)
        {
            using (RSACryptoServiceProvider rsaProvider = new RSACryptoServiceProvider((int)keySize))
            {
                return rsaProvider.ToXmlString(true);
            }
        }

        public string ExportPublicKey()
        {
            return _RsaProvider.ToXmlString(false);
        }

        public string Encrypt(string plainText)
        {
            string cypherText = null;

            if (plainText != null)
            {
                byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

                byte[] cypherTextBytes = _RsaProvider.Encrypt(plainTextBytes, true);

                cypherText = Convert.ToBase64String(cypherTextBytes);
            }

            return cypherText;
        }

        public bool TryDecrypt(string base64CipherText, out string plainText)
        {
            bool result;

            try
            {
                byte[] cipherTextBytes = Convert.FromBase64String(base64CipherText);

                byte[] plainTextBytes = _RsaProvider.Decrypt(cipherTextBytes, true);

                plainText = Encoding.UTF8.GetString(plainTextBytes);
                result = true;
            }
            catch
            {
                plainText = null;
                result = false;
            }

            return result;
        }

        #endregion
    }

}



