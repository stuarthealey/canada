﻿using System.Collections.Generic;
using MTD.Core.DataTransferObjects;

namespace MTD.Core.Service.Interface
{
    public interface IPrefixService
    {
        bool Add(PrefixDto prefixDto);

        void Update(PrefixDto prefixDto, int prefixId);

        IEnumerable<PrefixDto> GetPrefixDtoList(int merchantId,string name);

        PrefixDto GetPrefixDto(int prefixId);

        void DeletePrefix(int prefixId);

        IEnumerable<PrefixDto> PrefixListByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting,int merchantId);

    }
}
