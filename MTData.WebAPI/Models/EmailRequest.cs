﻿namespace MTData.WebAPI.Models
{
    public class EmailRequest
    {
        public string RequestId { get; set; }
        public string DeviceId { get; set; }
        public string DriverId { get; set; }
        public string CMail { get; set; }
        public string CToken { get; set; }
        public string CName { get; set; }
        public string CPhone { get; set; }
        public string CTaxNo { get; set; }
        public string CAddress { get; set; }
        public string DestAdd { get; set; }
        public string Disclaimer { get; set; }
        public string DriverTaxNo { get; set; }
        public string Extra { get; set; }
        public string Fare { get; set; }
        public string FederalTaxAmt { get; set; }
        public string FederalTaxPer { get; set; }
        public string Flagfall { get; set; }
        public int FleetId { get; set; }
        public string PickAdd { get; set; }
        public string StateMaxTax { get; set; }
        public string StateTaxPer { get; set; }
        public string SubTotal { get; set; }
        public string SurAmt { get; set; }
        public string SurPer { get; set; }
        public string TermsAndConditions { get; set; }
        public string Tip { get; set; }
        public string Tolls { get; set; }
        public string Total { get; set; }
        public string Vehicle { get; set; }
        public string PlateNumber { get; set; }
        public string CreditCardNumber { get; set; }

    }
}