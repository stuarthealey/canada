﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Data.Repository
{
    public class DriverRepository : BaseRepository, IDriverRepository
    {
        public DriverRepository(IMtDataDevlopmentsEntities dbMtData) : base(dbMtData)
        {
        }

        Regex regex = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        Regex regexNum = new Regex(@"^[0-9]+$");
        Regex alphaNum = new Regex(@"^[a-zA-Z0-9 ]+$");
        Regex alphaNumOnly = new Regex(@"^[a-zA-Z0-9]+$");

        /// <summary>
        /// Purpose             :   To check driver emailId exist
        /// Function Name       :   IsEmailExist
        /// Created By          :   Naveen Kumar
        /// Created On          :   1/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="emailId"></param>
        /// <param name="fleetId"></param>
        /// <param name="driverId"></param>
        /// <returns></returns>
        public bool IsEmailExist(string emailId, int fleetId, int driverId)
        {
            return DbMtData.Drivers.Any(m => m.Email == emailId && m.fk_FleetID == fleetId && m.IsActive && m.DriverID != driverId);
        }

        /// <summary>
        /// Purpose             :   To add driver
        /// Function Name       :   Add
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public bool Add(Driver driver)
        {
            try
            {
                bool isExist = DbMtData.Drivers.Any(m => m.DriverNo == driver.DriverNo && m.fk_FleetID == driver.fk_FleetID && m.IsActive);
                if (isExist)
                {
                    return true;
                }

                driver.IsActive = true;
                driver.IsLocked = false;
                driver.CreatedDate = DateTime.Now;
                DbMtData.Drivers.Add(driver);
                DbMtData.CommitChanges();
            }
            catch (Exception)
            {
                throw;
            }
            return false;
        }

        /// <summary>
        /// Purpose             :   To get driver baesd on Id
        /// Function Name       :   GetDriver
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverId"></param>
        /// <returns></returns>
        public Driver GetDriver(int driverId, int merchantId)
        {
            try
            {
                var driverDb = (from driver in DbMtData.Drivers
                                join fleet in DbMtData.Fleets on driver.fk_FleetID equals fleet.FleetID into result
                                from resTemp in result
                                where driver.DriverID == driverId && resTemp.fk_MerchantID == merchantId
                                select new { driver }).AsEnumerable().Select(objNew => new Driver
                                {
                                    DriverID = objNew.driver.DriverID,
                                    DriverNo = objNew.driver.DriverNo,
                                    DriverTaxNo = objNew.driver.DriverTaxNo,
                                    FName = objNew.driver.FName,
                                    LName = objNew.driver.LName,
                                    Email = objNew.driver.Email,
                                    Phone = objNew.driver.Phone,
                                    fk_Country = objNew.driver.fk_Country,
                                    Fk_State = objNew.driver.Fk_State,
                                    Fk_City = objNew.driver.Fk_City,
                                    Address = objNew.driver.Address,
                                    PIN = objNew.driver.PIN,
                                    fk_FleetID = objNew.driver.fk_FleetID,
                                    IsLocked = objNew.driver.IsLocked,
                                    CreatedBy = objNew.driver.CreatedBy,
                                    CreatedDate = objNew.driver.CreatedDate,
                                    ModifiedBy = objNew.driver.ModifiedBy,
                                    ModifiedDate = objNew.driver.ModifiedDate,
                                    SageAccount = objNew.driver.SageAccount,
                                    BankName = objNew.driver.BankName,
                                    AccountName = objNew.driver.AccountName,
                                    BSB = objNew.driver.BSB,
                                    Account = objNew.driver.Account,
                                }).FirstOrDefault();
                return driverDb;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To update driver
        /// Function Name       :   Update
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/05/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="driverId"></param>
        /// <returns></returns>
        public bool Update(Driver driver, int driverId)
        {
            try
            {
                bool isExist = DbMtData.Drivers.Any(m => m.DriverNo == driver.DriverNo && m.fk_FleetID == driver.fk_FleetID && m.IsActive && m.DriverID != driverId);
                if (isExist)
                {
                    return true;
                }

                Driver driverDb = DbMtData.Drivers.Find(driverId);
                driverDb.FName = driver.FName;
                driverDb.LName = driver.LName;
                driverDb.Phone = driver.Phone;
                driverDb.DriverNo = driver.DriverNo;
                driverDb.DriverTaxNo = driver.DriverTaxNo;
                driverDb.Email = driver.Email;
                driverDb.fk_FleetID = driver.fk_FleetID;
                driverDb.fk_Country = driver.fk_Country;
                driverDb.Fk_State = driver.Fk_State;
                driverDb.Fk_City = driver.Fk_City;
                driverDb.Address = driver.Address;
                driverDb.ModifiedBy = driver.ModifiedBy;
                driverDb.SageAccount = driver.SageAccount;
                driverDb.BankName = driver.BankName;
                driverDb.AccountName = driver.AccountName;
                driverDb.BSB = driver.BSB;
                driverDb.Account = driver.Account;
                driverDb.ModifiedDate = DateTime.Now;
            }
            catch (Exception)
            {
                throw;
            }

            DbMtData.CommitChanges();
            return false;
        }

        /// <summary>
        /// Purpose             :   To delete driver
        /// Function Name       :   DeleteDriver
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/06/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverId"></param>
        public void DeleteDriver(int driverId)
        {
            try
            {
                Driver driverDb = DbMtData.Drivers.Find(driverId);
                driverDb.IsActive = false;
                DbMtData.CommitChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get the list of diver
        /// Function Name       :   DriverList
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/06/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<Driver> DriverList(FilterSearchParameters filterSearch)
        {
            try
            {
                IEnumerable<Driver> driversList = (from drivers in DbMtData.Drivers
                                                   join countries in DbMtData.CountryCodes on drivers.fk_Country equals countries.CountryCodesID
                                                   join n in DbMtData.Fleets on drivers.fk_FleetID equals n.FleetID
                                                   where drivers.IsActive && n.fk_MerchantID == filterSearch.MerchantId
                                                   select new { Drivers = drivers, CountryCodes = countries, n.FleetName }).AsEnumerable().Select(objNew =>
                                                            new Driver
                                                            {
                                                                FName = objNew.Drivers.FName,
                                                                LName = objNew.Drivers.LName,
                                                                Phone = objNew.Drivers.Phone,
                                                                Email = objNew.Drivers.Email,
                                                                DriverNo = objNew.Drivers.DriverNo,
                                                                DriverTaxNo = objNew.Drivers.DriverTaxNo,
                                                                fk_Country = objNew.CountryCodes.Title,
                                                                GridState = objNew.Drivers.Fk_State,
                                                                GridCity = objNew.Drivers.Fk_City,
                                                                Address = objNew.Drivers.Address,
                                                                IsActive = objNew.Drivers.IsActive,
                                                                DriverID = objNew.Drivers.DriverID,
                                                                FleetName = objNew.FleetName,
                                                                fk_FleetID = objNew.Drivers.fk_FleetID

                                                            }).ToList();

                if (filterSearch.LogOnByUserType == 5)
                {
                    List<int> fleetListAccessed = GetAccesibleFleet(filterSearch.LogOnByUserId, filterSearch.MerchantId);
                    List<Driver> driverViewResult = driversList.Where(m => fleetListAccessed.Contains((int)m.fk_FleetID)).Select(m => m).Distinct().ToList();
                    driversList = driverViewResult.AsEnumerable();
                }

                if (!string.IsNullOrEmpty(filterSearch.Name) && string.IsNullOrEmpty(filterSearch.DriverNo))
                {
                    driversList = driversList.Where(p => p.FName.IndexOf(filterSearch.Name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }

                if (!string.IsNullOrEmpty(filterSearch.DriverNo) && string.IsNullOrEmpty(filterSearch.Name))
                {
                    driversList = driversList.Where(p => p.DriverNo.IndexOf(filterSearch.DriverNo, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }

                if (!string.IsNullOrEmpty(filterSearch.Name) && !string.IsNullOrEmpty(filterSearch.DriverNo))
                {
                    driversList = driversList.Where(p => p.FName.IndexOf(filterSearch.Name, StringComparison.OrdinalIgnoreCase) >= 0 && p.DriverNo.IndexOf(filterSearch.DriverNo, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }

                return driversList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get list of driver
        /// Function Name       :   DrivGetDeriverListByFiltererList
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/06/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fsParametes"></param>
        /// <returns></returns>
        public IEnumerable<Driver> DrivGetDeriverListByFiltererList(FilterSearchParameters fsParametes)
        {
            try
            {
                IEnumerable<Driver> driversList = (from drivers in DbMtData.Drivers
                                                   join countries in DbMtData.CountryCodes on drivers.fk_Country equals countries.CountryCodesID
                                                   join n in DbMtData.Fleets on drivers.fk_FleetID equals n.FleetID
                                                   where drivers.IsActive && n.fk_MerchantID == fsParametes.MerchantId
                                                   select
                                                       new
                                                       {
                                                           Drivers = drivers,
                                                           CountryCodes = countries,
                                                           n.FleetName,
                                                           n.FleetID
                                                       }).AsEnumerable().Select(objNew =>
                                                                new Driver
                                                                {
                                                                    FName = objNew.Drivers.FName + " " + objNew.Drivers.LName,
                                                                    LName = objNew.Drivers.LName,
                                                                    Phone = objNew.Drivers.Phone,
                                                                    Email = objNew.Drivers.Email,
                                                                    DriverNo = objNew.Drivers.DriverNo,
                                                                    DriverTaxNo = objNew.Drivers.DriverTaxNo,
                                                                    Address = objNew.Drivers.Address + ", " + objNew.Drivers.Fk_City + ", " + objNew.Drivers.Fk_State + ", " + objNew.CountryCodes.Title,
                                                                    IsActive = objNew.Drivers.IsActive,
                                                                    DriverID = objNew.Drivers.DriverID,
                                                                    FleetName = objNew.FleetName,
                                                                    fk_FleetID = objNew.FleetID

                                                                });

                var userFleet = fsParametes.UserFleet;
                if (fsParametes.UserType == 4)
                {
                    driversList = driversList.Where(x => userFleet.Select(y => y.fk_FleetID).Contains(x.fk_FleetID)).ToList();
                }

                if (fsParametes.LogOnByUserType == 5)
                {
                    List<int> fleetListAccessed = GetAccesibleFleet(fsParametes.LogOnByUserId, fsParametes.MerchantId);
                    List<Driver> driverViewResult = driversList.Where(m => fleetListAccessed.Contains((int)m.fk_FleetID)).Select(m => m).Distinct().ToList();
                    driversList = driverViewResult.AsEnumerable();
                }
                
                var query = from m in driversList where (m.IsActive) select m;
                query = query.OrderBy(p => p.FName); //Default!
                
                if (!string.IsNullOrEmpty(fsParametes.Name) && string.IsNullOrEmpty(fsParametes.DriverNo))
                {
                    query = query.Where(p => p.FName.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                
                if (!string.IsNullOrEmpty(fsParametes.DriverNo) && string.IsNullOrEmpty(fsParametes.Name))
                {
                    query = query.Where(p => p.DriverNo.IndexOf(fsParametes.DriverNo, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                
                if (!string.IsNullOrEmpty(fsParametes.Name) && !string.IsNullOrEmpty(fsParametes.DriverNo))
                {
                    query = query.Where(p => p.FName.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0 && p.DriverNo.IndexOf(fsParametes.DriverNo, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }

                if (!string.IsNullOrEmpty(fsParametes.Sorting))
                {
                    query = SortedDriver(fsParametes.Sorting, query).ToList();
                }

                List<Driver> mUsers = new List<Driver>();
                foreach (var mu in query)
                {
                    mu.Address = mu.Address.Replace(", , ,", ", ");
                    mu.Address = mu.Address.Replace(", ,", ", ");
                    mUsers.Add(mu);
                }

                return fsParametes.PageSize > 0
                    ? mUsers.Skip(fsParametes.StartIndex).Take(fsParametes.PageSize).ToList() //Paging
                    : mUsers.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get driver based on Pin
        /// Function Name       :   GetDriverDto
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/06/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverNo"></param>
        /// <param name="driverPin"></param>
        /// <returns></returns>
        public Driver GetDriverDto(string driverNo, string driverPin)
        {
            var sDriver = DbMtData.Drivers.FirstOrDefault(a => a.DriverNo.Equals(driverNo) && a.PIN.Equals(driverPin) && a.IsActive);
            return sDriver;
        }

        /// <summary>
        /// Purpose             :   To check driver is valid or not
        /// Function Name       :   IsDriverNoValid
        /// Created By          :   Naveen Kumar
        /// Created On          :   01/06/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="driverNo"></param>
        /// <param name="driverPin"></param>
        /// <returns></returns>
        public bool IsDriverNoValid(string driverNo, string driverPin)
        {
            return DbMtData.Drivers.Any(m => m.DriverNo == driverNo && m.PIN == driverPin && m.IsActive);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driverNo"></param>
        /// <returns></returns>
        public Driver GetDriverDto(string driverNo)
        {
            var sDriver = DbMtData.Drivers.FirstOrDefault(a => a.DriverNo.Equals(driverNo) && a.IsActive);
            return sDriver;
        }

        /// <summary>
        /// Purpose             :   To check driver driverno exist
        /// Function Name       :   IsEmailExist
        /// Created By          :   Ravit Chaudhary
        /// Created On          :   06/20/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="emailId"></param>
        /// <returns></returns>
        public int IsDriverNoExist(Driver objDriver, int merchantId, int userId, int userType)
        {
            try
            {
                bool driverDb = true;
                int response = -1;

                if (objDriver.FName == string.Empty)
                    return 8; //First name is required
                
                if (objDriver.LName == string.Empty)
                    return 9; //Last name is required
                
                if (objDriver.Phone == string.Empty)
                    return 10; //Phone is required
                
                if (objDriver.DriverNo == string.Empty)
                    return 11; //Driver number is required
                
                if (objDriver.PIN == string.Empty)
                    return 12; //PIN is required
                
                if (objDriver.DriverTaxNo == string.Empty)
                    return 13; //Driver tax number is required
                
                if (objDriver.FleetName == string.Empty)
                    return 14; //Fleet is required
                
                if (objDriver.fk_Country == string.Empty)
                    return 15; //Country is required
                
                if (objDriver.Address == string.Empty)
                    return 16; //Address is required
                
                if (objDriver.PIN.Length > 10)
                    return 19; //PIN must be maximum length of 10
                
                if (objDriver.PIN.Length < 4)
                    return 20; //PIN must be minimum length of 4
                
                if (objDriver.FName.Length >= 50)
                    return 22; //First Name must not exceed the length of 50
                
                if (objDriver.LName.Length >= 50)
                    return 23; //Last Name must not exceed the length of 50
                
                if (objDriver.Email.Length >= 50)
                    return 24; //Email must not exceed the length of 50
                
                if (objDriver.fk_Country.Length >= 50)
                    return 25; //Country must not exceed the length of 50
                
                if (objDriver.Fk_State.Length >= 50)
                    return 26; //State must not exceed the length of 50
                
                if (objDriver.Fk_City.Length >= 50)
                    return 27; //City must not exceed the length of 50
                
                if (objDriver.Address.Length >= 50)
                    return 28;
                
                if (objDriver.SageAccount.Length >= 50)
                    return 29; //Sage Account must not exceed the length of 50
                
                if (objDriver.BankName.Length >= 50)
                    return 30; //Bank Name must not exceed the length of 50
                
                if (objDriver.AccountName.Length >= 50)
                    return 31; //Account Name must not exceed the length of 50
                
                if (objDriver.BSB.Length >= 50)
                    return 32; //BSB must not exceed the length of 50
                
                if (objDriver.Account.Length >= 50)
                    return 33; //Account must not exceed the length of 50

                Match exp = alphaNum.Match(objDriver.FName);
                if (!exp.Success)
                    return 34; //Invalid First Name
                
                exp = alphaNum.Match(objDriver.LName);
                if (!exp.Success)
                    return 35; //Invalid Last Name
                
                exp = alphaNumOnly.Match(objDriver.DriverNo);
                if (!exp.Success)
                    return 36; //Invalid Driver Number
                
                exp = alphaNumOnly.Match(objDriver.DriverTaxNo);
                if (!exp.Success)
                    return 37; //Invalid Driver Tax Number
                
                if (!string.IsNullOrEmpty(objDriver.Fk_State))
                {
                    exp = alphaNum.Match(objDriver.Fk_State);
                    if (!exp.Success)
                        return 38; //Invalid State
                }

                if (!string.IsNullOrEmpty(objDriver.Fk_City))
                {
                    exp = alphaNum.Match(objDriver.Fk_City);
                    if (!exp.Success)
                        return 39; //Invalid City
                }
                
                if (!string.IsNullOrEmpty(objDriver.SageAccount))
                {
                    exp = alphaNum.Match(objDriver.SageAccount);
                    if (!exp.Success)
                        return 40; //Invalid Sage Account
                }
                
                if (!string.IsNullOrEmpty(objDriver.BankName))
                {
                    exp = alphaNum.Match(objDriver.BankName);
                    if (!exp.Success)
                        return 41; //Invalid Bank Name
                }
                
                if (!string.IsNullOrEmpty(objDriver.AccountName))
                {
                    exp = alphaNum.Match(objDriver.AccountName);
                    if (!exp.Success)
                        return 42; //Invalid Account Name
                }
                
                if (!string.IsNullOrEmpty(objDriver.BSB))
                {
                    exp = alphaNum.Match(objDriver.BSB);
                    if (!exp.Success)
                        return 43; //Invalid BSB
                }
                
                if (!string.IsNullOrEmpty(objDriver.Account))
                {
                    exp = alphaNum.Match(objDriver.Account);
                    if (!exp.Success)
                        return 44; //Invalid  Account
                }

                Match matchNum = regexNum.Match(objDriver.PIN);
                if (!matchNum.Success)
                    return 21; //PIN must be numeric
                
                matchNum = regexNum.Match(objDriver.Phone);
                if (!matchNum.Success)
                    return 45; //Phone number must be numeric

                if (!string.IsNullOrEmpty(objDriver.Email))
                {
                    Match match = regex.Match(objDriver.Email);
                    if (!match.Success)
                        return 17; //Invalid Email Id
                }

                if (objDriver.Phone.Length < 6)
                    return 18; //Phone must be minimum length of 6

                var fleet = DbMtData.Fleets.Where(x => (x.fk_MerchantID == merchantId && x.FleetName == objDriver.FleetName && (bool)x.IsActive)).FirstOrDefault();
                if (fleet == null)
                {
                    response = 2;
                    return response;// fleet is not assigned to the merchant.
                }

                CountryCode country = (from m in DbMtData.CountryCodes where m.Title == objDriver.fk_Country select m).FirstOrDefault();
                if (country == null)
                {
                    return 6;// Country does not exists.
                }

                if (userType == 4)
                {
                    var isExist = DbMtData.UserFleets.Any(x => x.fk_UserID == userId && x.fk_FleetID == fleet.FleetID);
                    if (isExist)
                    {
                        if (objDriver.Email != string.Empty)
                        {
                            if (fleet != null)
                            {
                                driverDb = DbMtData.Drivers.Any(m => (((m.fk_FleetID == fleet.FleetID) && m.Email == objDriver.Email) && m.IsActive));
                                if (driverDb)
                                {
                                    response = 3;
                                    return response;// EmaiID is already exists.
                                }
                            }
                        }

                        if (fleet != null)
                        {
                            driverDb = DbMtData.Drivers.Any(m => ((m.DriverNo == objDriver.DriverNo && m.fk_FleetID == fleet.FleetID) && m.IsActive));
                            if (driverDb)
                            {
                                response = 4;
                                return response;// Driver Number is already exists.
                            }
                        }

                        if (!driverDb)
                        {
                            response = 0;
                            return response;// valid driver to add.
                        }

                        return 0;
                    }
                    else
                    {
                        response = 5;
                        return response;// fleet is not assigned to the user.
                    }
                }

                if (objDriver.Email != string.Empty)
                {
                    if (fleet != null)
                    {
                        driverDb = DbMtData.Drivers.Any(m => (((m.fk_FleetID == fleet.FleetID) && m.Email == objDriver.Email) && m.IsActive));
                        if (driverDb)
                        {
                            response = 3;
                            return response;// EmailId is already exists same fleet.
                        }
                    }
                }

                if (fleet != null)
                {
                    driverDb = DbMtData.Drivers.Any(m => ((m.DriverNo == objDriver.DriverNo && m.fk_FleetID == fleet.FleetID) && m.IsActive));
                    if (driverDb)
                    {
                        response = 4;
                        return response;// Driver Number is already exists.
                    }
                }

                if (!driverDb)
                {
                    response = 0;
                    return response;// valid driver to add.
                }

                return 0;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDriver"></param>
        /// <param name="merchantId"></param>
        public bool AddDriverList(Driver objDriver, int merchantId)
        {
            try
            {
                if (objDriver != null)
                {
                    Fleet fleet = (from pt in DbMtData.Fleets where pt.FleetName == objDriver.FleetName && pt.fk_MerchantID == merchantId && (bool)pt.IsActive select pt).FirstOrDefault();
                    CountryCode country = (from m in DbMtData.CountryCodes where m.Title == objDriver.fk_Country select m).FirstOrDefault();

                    if (country != null && fleet != null)
                    {
                        objDriver.DriverNo = objDriver.DriverNo;
                        objDriver.DriverTaxNo = objDriver.DriverTaxNo;
                        objDriver.FName = objDriver.FName;
                        objDriver.LName = objDriver.LName;
                        objDriver.Email = objDriver.Email;
                        objDriver.Phone = objDriver.Phone;
                        objDriver.fk_Country = country.CountryCodesID;
                        objDriver.Fk_State = objDriver.Fk_State;
                        objDriver.Fk_City = objDriver.Fk_City;
                        objDriver.Address = objDriver.Address;
                        objDriver.PIN = objDriver.PIN;
                        objDriver.fk_FleetID = fleet.FleetID;
                        objDriver.IsLocked = false;
                        objDriver.IsActive = true;
                        objDriver.CreatedBy = objDriver.CreatedBy;
                        objDriver.CreatedDate = DateTime.Now;
                        objDriver.ModifiedBy = objDriver.ModifiedBy;
                        objDriver.SageAccount = objDriver.SageAccount;
                        objDriver.BankName = objDriver.BankName;
                        objDriver.AccountName = objDriver.AccountName;
                        objDriver.BSB = objDriver.BSB;
                        objDriver.Account = objDriver.Account;
                        DbMtData.Drivers.Add(objDriver);

                        return Convert.ToBoolean(DbMtData.CommitChanges());
                    }
                }

                return false;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driverId"></param>
        /// <param name="Pin"></param>
        /// <returns></returns>
        public bool ResetPin(int driverId, string Pin)
        {
            try
            {
                var driverDb = DbMtData.Drivers.Find(driverId);
                if (driverDb != null)
                {
                    driverDb.PIN = Pin;
                    DbMtData.CommitChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public IEnumerable<DriverReportRecipient> GetDriverReceipent(int merchantId, int fleetId)
        {
            try
            {
                var driverRecipient = (from drivers in DbMtData.Drivers
                                       join fleet in DbMtData.Fleets on drivers.fk_FleetID equals fleet.FleetID
                                       join merchant in DbMtData.Merchants on fleet.fk_MerchantID equals merchant.MerchantID
                                       join driverRec in DbMtData.DriverReportRecipients on drivers.DriverID equals driverRec.fk_DriverId
                                       into t
                                       from driverRec in t.DefaultIfEmpty()
                                       where drivers.fk_FleetID == fleetId && (bool)drivers.IsActive
                                       select new
                                       {
                                           drivers.DriverID,
                                           drivers.DriverNo,
                                           drivers.FName,
                                           drivers.LName,
                                           driverRec.IsRecipient,
                                           driverRec.ModifiedBy,
                                           driverRec.IsDaily,
                                           driverRec.IsWeekly,
                                           driverRec.IsMonthly,
                                           drivers.Email,
                                           drivers.fk_FleetID,
                                           merchant.Company,
                                           driverRec.IsOwnerRecipient
                                       }).Distinct().AsEnumerable().Select(objNew =>
                                                new DriverReportRecipient
                                                {
                                                    DriverID = objNew.DriverID,
                                                    DriverNo = objNew.DriverNo,
                                                    FName = objNew.FName,
                                                    LName = objNew.LName,
                                                    IsRecipient = objNew.IsRecipient,
                                                    ModifiedBy = objNew.ModifiedBy,
                                                    Email = objNew.Email,
                                                    Company = objNew.Company,
                                                    IsDaily = objNew.IsDaily,
                                                    IsWeekly = objNew.IsWeekly,
                                                    IsMonthly = objNew.IsMonthly,
                                                    fk_FleetId = objNew.fk_FleetID,
                                                    IsOwnerRecipient = objNew.IsOwnerRecipient
                                                });

                var a = driverRecipient.Distinct();
                return driverRecipient.Distinct();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driverRecipient"></param>
        /// <param name="fleetId"></param>
        /// <param name="userType"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public bool UpdateDriverRecipient(DriverRecList driverRecipient, int fleetId, int userType, int merchantId)
        {
            DriverReportRecipient[] driverRecipientObj = new DriverReportRecipient[driverRecipient.UserList.Count()];
            try
            {
                if (userType == 1 || userType == 5)
                {
                    int countRec = 0;
                    foreach (var item in driverRecipient.UserList)
                    {
                        driverRecipientObj[countRec] = new DriverReportRecipient();
                        driverRecipientObj[countRec] = (from m in DbMtData.DriverReportRecipients where m.fk_DriverId == item.DriverID && m.fk_FleetId == item.fk_FleetId select m).FirstOrDefault();
                        if (driverRecipientObj[countRec] != null)
                        {
                            if ((Convert.ToBoolean(item.IsWeekly) || Convert.ToBoolean(item.IsDaily) || Convert.ToBoolean(item.IsOwnerRecipient) || Convert.ToBoolean(item.IsMonthly)) && Convert.ToBoolean(item.IsRecipient) == false)
                                item.IsRecipient = true;

                            driverRecipientObj[countRec].fk_DriverId = item.DriverID;
                            driverRecipientObj[countRec].fk_FleetId = item.fk_FleetId;
                            driverRecipientObj[countRec].IsRecipient = item.IsRecipient;
                            driverRecipientObj[countRec].UserType = userType;
                            driverRecipientObj[countRec].ModifiedBy = driverRecipient.ModifiedBy;
                            driverRecipientObj[countRec].CreatedBy = driverRecipient.ModifiedBy;
                            driverRecipientObj[countRec].CreatedDate = DateTime.Now;
                            driverRecipientObj[countRec].ModifiedDate = DateTime.Now;
                            driverRecipientObj[countRec].IsDaily = item.IsDaily;
                            driverRecipientObj[countRec].IsMonthly = item.IsMonthly;
                            driverRecipientObj[countRec].IsWeekly = item.IsWeekly;
                            driverRecipientObj[countRec].IsOwnerRecipient = item.IsOwnerRecipient;

                            DbMtData.CommitChanges();
                        }
                        else
                        {
                            if ((Convert.ToBoolean(item.IsWeekly) || Convert.ToBoolean(item.IsDaily) || Convert.ToBoolean(item.IsOwnerRecipient) || Convert.ToBoolean(item.IsMonthly)) && Convert.ToBoolean(item.IsRecipient) == false)
                                item.IsRecipient = true;

                            driverRecipientObj[countRec] = new DriverReportRecipient();

                            if ((Convert.ToBoolean(item.IsWeekly) || Convert.ToBoolean(item.IsDaily) || Convert.ToBoolean(item.IsOwnerRecipient) || Convert.ToBoolean(item.IsMonthly)) && Convert.ToBoolean(item.IsRecipient) == false)
                                item.IsRecipient = true;

                            driverRecipientObj[countRec].fk_DriverId = item.DriverID;
                            driverRecipientObj[countRec].fk_FleetId = item.fk_FleetId;
                            driverRecipientObj[countRec].IsRecipient = item.IsRecipient;
                            driverRecipientObj[countRec].UserType = userType;
                            driverRecipientObj[countRec].ModifiedBy = driverRecipient.ModifiedBy;
                            driverRecipientObj[countRec].CreatedBy = driverRecipient.ModifiedBy;
                            driverRecipientObj[countRec].CreatedDate = DateTime.Now;
                            driverRecipientObj[countRec].ModifiedDate = DateTime.Now;
                            driverRecipientObj[countRec].IsDaily = item.IsDaily;
                            driverRecipientObj[countRec].IsMonthly = item.IsMonthly;
                            driverRecipientObj[countRec].IsWeekly = item.IsWeekly;
                            driverRecipientObj[countRec].IsOwnerRecipient = item.IsOwnerRecipient;

                            DbMtData.DriverReportRecipients.Add(driverRecipientObj[countRec]);
                            DbMtData.CommitChanges();
                        }

                        countRec++;
                    }

                    return true;
                }

                int count = 0;
                foreach (var item in driverRecipient.UserList)
                {
                    driverRecipientObj[count] = new DriverReportRecipient();
                    driverRecipientObj[count] = (from m in DbMtData.DriverReportRecipients where m.fk_DriverId == item.DriverID && m.fk_FleetId == fleetId select m).FirstOrDefault();
                    if (driverRecipientObj[count] != null)
                    {
                        driverRecipientObj[count].fk_DriverId = item.DriverID;
                        driverRecipientObj[count].fk_FleetId = fleetId;

                        if (item.IsRecipient == null || !(bool)item.IsRecipient)
                            driverRecipientObj[count].IsRecipient = false;
                        else
                            driverRecipientObj[count].IsRecipient = item.IsRecipient;

                        driverRecipientObj[count].UserType = userType;
                        driverRecipientObj[count].ModifiedBy = driverRecipient.ModifiedBy;
                        driverRecipientObj[count].CreatedBy = driverRecipient.ModifiedBy;
                        driverRecipientObj[count].CreatedDate = DateTime.Now;
                        driverRecipientObj[count].ModifiedDate = DateTime.Now;

                        if ((Convert.ToBoolean(item.IsWeekly) || Convert.ToBoolean(item.IsDaily) || Convert.ToBoolean(item.IsOwnerRecipient) || Convert.ToBoolean(item.IsMonthly)) && Convert.ToBoolean(item.IsRecipient) == false)
                        {
                            item.IsRecipient = true;
                            driverRecipientObj[count].IsRecipient = true;
                        }

                        if (item.IsOwnerRecipient != null && Convert.ToBoolean(item.IsRecipient))
                            driverRecipientObj[count].IsOwnerRecipient = true;
                        else
                            driverRecipientObj[count].IsOwnerRecipient = false;

                        if (item.IsDaily != null && Convert.ToBoolean(item.IsRecipient))
                            driverRecipientObj[count].IsDaily = true;
                        else
                            driverRecipientObj[count].IsDaily = false;

                        if (item.IsMonthly != null && Convert.ToBoolean(item.IsRecipient))
                            driverRecipientObj[count].IsMonthly = true;
                        else
                            driverRecipientObj[count].IsMonthly = false;

                        if (item.IsWeekly != null && Convert.ToBoolean(item.IsRecipient))
                            driverRecipientObj[count].IsWeekly = true;
                        else
                            driverRecipientObj[count].IsWeekly = false;

                        DbMtData.CommitChanges();
                    }
                    else
                    {
                        driverRecipientObj[count] = new DriverReportRecipient();
                        driverRecipientObj[count].fk_DriverId = item.DriverID;
                        driverRecipientObj[count].fk_FleetId = fleetId;

                        if (item.IsRecipient == null || !(bool)item.IsRecipient)
                            driverRecipientObj[count].IsRecipient = false;
                        else
                            driverRecipientObj[count].IsRecipient = item.IsRecipient;

                        driverRecipientObj[count].UserType = userType;
                        driverRecipientObj[count].ModifiedBy = driverRecipient.ModifiedBy;
                        driverRecipientObj[count].CreatedBy = driverRecipient.ModifiedBy;
                        driverRecipientObj[count].CreatedDate = DateTime.Now;
                        driverRecipientObj[count].ModifiedDate = DateTime.Now;

                        if ((Convert.ToBoolean(item.IsWeekly) || Convert.ToBoolean(item.IsDaily) || Convert.ToBoolean(item.IsOwnerRecipient) || Convert.ToBoolean(item.IsMonthly)) && Convert.ToBoolean(item.IsRecipient) == false)
                        {
                            item.IsRecipient = true;
                            driverRecipientObj[count].IsRecipient = true;
                        }

                        if (item.IsOwnerRecipient != null && Convert.ToBoolean(item.IsRecipient))
                            driverRecipientObj[count].IsOwnerRecipient = true;
                        else
                            driverRecipientObj[count].IsOwnerRecipient = false;

                        if (item.IsDaily != null && Convert.ToBoolean(item.IsRecipient))
                            driverRecipientObj[count].IsDaily = true;
                        else
                            driverRecipientObj[count].IsDaily = false;

                        if (item.IsMonthly != null && Convert.ToBoolean(item.IsRecipient))
                            driverRecipientObj[count].IsMonthly = true;
                        else
                            driverRecipientObj[count].IsMonthly = false;

                        if (item.IsWeekly != null && Convert.ToBoolean(item.IsRecipient))
                            driverRecipientObj[count].IsWeekly = true;
                        else
                            driverRecipientObj[count].IsWeekly = false;

                        DbMtData.DriverReportRecipients.Add(driverRecipientObj[count]);
                        DbMtData.CommitChanges();
                    }

                    count++;
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<Driver> SortedDriver(string sorting, IEnumerable<Driver> query)
        {
            switch (sorting)
            {
                case "FName ASC":
                    query = query.OrderBy(p => p.FName);
                    break;
                case "FName DESC":
                    query = query.OrderByDescending(p => p.FName);
                    break;
                case "Phone ASC":
                    query = query.OrderBy(p => p.Phone);
                    break;
                case "Phone DESC":
                    query = query.OrderByDescending(p => p.Phone);
                    break;
                case "Email ASC":
                    query.OrderBy(p => p.Email);
                    break;
                case "Email DESC":
                    query = query.OrderByDescending(p => p.Email);
                    break;
                case "FleetName ASC":
                    query = query.OrderBy(p => p.FleetName);
                    break;
                case "FleetName DESC":
                    query = query.OrderByDescending(p => p.FleetName);
                    break;
            }

            return query;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public string GetDriverCompany(int merchantId)
        {
            try
            {
                var Company = (from m in DbMtData.Merchants where m.MerchantID == merchantId select m.Company).FirstOrDefault();
                return Company;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            : to  add schedule report of driver
        /// Function Name       :   ScheduleDriverReport
        /// Created By          :  Asf Shafeeque
        /// Created On          :  7/27/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="scheduleReport"></param>
        /// <returns></returns>
        public bool ScheduleDriverReport(ScheduleReport scheduleReport)
        {
            try
            {
                var dbRecord = DbMtData.ScheduleReports.Where(x => x.fk_ReportID == scheduleReport.fk_ReportID).FirstOrDefault();
                var dbReport = DbMtData.Reports.Where(x => x.ReportId == scheduleReport.fk_ReportID).FirstOrDefault();
                ScheduleReport dbSchedule = new ScheduleReport();
                dbSchedule.DateFrom = scheduleReport.DateFrom;
                dbSchedule.DateTo = scheduleReport.DateTo;
                dbSchedule.Frequency = scheduleReport.Frequency;
                dbSchedule.FrequencyType = scheduleReport.FrequencyType;
                dbSchedule.WeekDay = scheduleReport.WeekDay;
                dbSchedule.MonthDay = scheduleReport.MonthDay;
                dbSchedule.StartTime = scheduleReport.StartTime;
                dbSchedule.TimeZone = scheduleReport.TimeZone;
                dbSchedule.CreatedBy = scheduleReport.CreatedBy;
                dbSchedule.ModifiedBy = scheduleReport.ModifiedBy;
                dbSchedule.CreatedDate = DateTime.Now;
                dbSchedule.ModifiedDate = DateTime.Now;
                dbSchedule.fk_ReportID = scheduleReport.fk_ReportID;
                dbSchedule.ScheduleID = (int)scheduleReport.fk_ReportID;
                DbMtData.ScheduleReports.Add(dbSchedule);
                dbReport.IsCreated = true;
                return Convert.ToBoolean(DbMtData.CommitChanges());
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            : to  get driver report list
        /// Function Name       :   DriverReportList
        /// Created By          :  Asf Shafeeque
        /// Created On          :  7/27/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Report> DriverReportList()
        {
            try
            {
                IEnumerable<Report> query = DbMtData.Reports.Where(x => x.ReportId > 3 && !(bool)x.IsCreated);
                return query;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <param name="sorting"></param>
        /// <returns></returns>
        public IEnumerable<ScheduleReport> ScheduleReportListByFilter(string name, int startIndex, int count, string sorting)
        {
            try
            {
                IEnumerable<ScheduleReport> query = (from m in DbMtData.ScheduleReports
                                                     join n in DbMtData.Reports on m.fk_ReportID
                                                         equals n.ReportId
                                                     where m.ScheduleID > 3
                                                     select new
                                                     {
                                                         m.DateFrom,
                                                         m.DateTo,
                                                         m.Frequency,
                                                         m.FrequencyType,
                                                         m.WeekDay,
                                                         m.MonthDay,
                                                         m.StartTime,
                                                         m
                                                         .TimeZone,
                                                         m
                                                         .ScheduleID,
                                                         m
                                                         .IsCreated,
                                                         m.fk_ReportID,
                                                         n.DisplayName
                                                     }).AsEnumerable().Select(objNew => new
                                                    ScheduleReport
                                                     {
                                                         DateFrom = objNew.DateFrom,
                                                         DateTo = objNew.DateTo,
                                                         Frequency = objNew.Frequency,
                                                         FrequencyType = objNew.FrequencyType,
                                                         WeekDay = objNew.WeekDay,
                                                         MonthDay = objNew.MonthDay,
                                                         StartTime = objNew.StartTime,
                                                         TimeZone = objNew.TimeZone,
                                                         ScheduleID = objNew.ScheduleID,
                                                         IsCreated = objNew.IsCreated,
                                                         fk_ReportID = objNew.fk_ReportID,
                                                         DisplayName = objNew.DisplayName
                                                     }).ToList();

                if (!string.IsNullOrEmpty(name))
                {
                    query = query.Where(p => p.DisplayName.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0);
                }

                if (!string.IsNullOrEmpty(sorting))
                {
                    switch (sorting)
                    {
                        case "DisplayName ASC":
                            query = query.OrderBy(p => p.DisplayName);
                            break;
                        case "DisplayName DESC":
                            query = query.OrderByDescending(p => p.DisplayName);
                            break;
                        case "DateFrom ASC":
                            query = query.OrderBy(p => p.DateFrom);
                            break;
                        case "DateFrom DESC":
                            query = query.OrderByDescending(p => p.DateFrom);
                            break;
                        case "DateTo ASC":
                            query = query.OrderBy(p => p.DateTo);
                            break;
                        case "DateTo DESC":
                            query = query.OrderByDescending(p => p.DateTo);
                            break;
                        case "FrequencyName ASC":
                            query = query.OrderBy(p => p.Frequency);
                            break;
                        case "FrequencyName DESC":
                            query = query.OrderByDescending(p => p.Frequency);
                            break;
                    }
                }

                return query;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scheduleId"></param>
        /// <returns></returns>
        public ScheduleReport GetScheduleReport(int scheduleId)
        {
            ScheduleReport scheduleDb = DbMtData.ScheduleReports.FirstOrDefault(x => x.ScheduleID == scheduleId);
            return scheduleDb;
        }

        /// <summary>
        ///  Purpose            : to  update scheduled driver report 
        /// Function Name       :   ScheduleDriverReportUpdate
        /// Created By          :  Asf Shafeeque
        /// Created On          :  7/27/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="scheduleReport"></param>
        /// <returns></returns>
        public bool ScheduleDriverReportUpdate(ScheduleReport scheduleReport)
        {
            try
            {
                var dbRecord = DbMtData.ScheduleReports.Where(x => x.fk_ReportID == scheduleReport.fk_ReportID).FirstOrDefault();
                var dbReport = DbMtData.Reports.Where(x => x.ReportId == scheduleReport.fk_ReportID).FirstOrDefault();
                if (dbRecord != null)//update
                {
                    dbRecord.DateFrom = scheduleReport.DateFrom;
                    dbRecord.DateTo = scheduleReport.DateTo;
                    dbRecord.Frequency = scheduleReport.Frequency;
                    dbRecord.FrequencyType = scheduleReport.FrequencyType;
                    dbRecord.WeekDay = scheduleReport.WeekDay;
                    dbRecord.MonthDay = scheduleReport.MonthDay;
                    dbRecord.StartTime = scheduleReport.StartTime;
                    dbRecord.TimeZone = scheduleReport.TimeZone;
                    dbRecord.CreatedBy = scheduleReport.CreatedBy;
                    dbRecord.ModifiedBy = scheduleReport.ModifiedBy;
                    dbRecord.CreatedDate = DateTime.Now;
                    dbRecord.ModifiedDate = DateTime.Now;
                    dbRecord.ScheduleID = (int)scheduleReport.fk_ReportID;
                    dbReport.IsCreated = true;
                    return Convert.ToBoolean(DbMtData.CommitChanges());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return false;
        }

        /// <summary>
        /// Purpose             :   to get the list of all fleet
        /// Function Name       :   AllFleets
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/25/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<Fleet> AllFleets()
        {
            try
            {
                IEnumerable<Fleet> fleetList = (from fleet in DbMtData.Fleets
                                                 join user in DbMtData.Users on fleet.fk_MerchantID equals user.fk_MerchantID
                                                 where fleet.IsActive && (bool)user.IsActive && user.fk_UserTypeID == 2
                                                 select new { fleet.FleetID, fleet.FleetName }).AsEnumerable().Select(objNew =>
                                                        new Fleet
                                                        {
                                                            FleetID = objNew.FleetID,
                                                            FleetName = objNew.FleetName
                                                        }).ToList().Distinct();

                return fleetList.OrderBy(x => x.FleetName);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   to get the list driver report list
        /// Function Name       :   ScheduleDriverReportList
        /// Created By          :   Asif Shafeeque 
        /// Created On          :   12/25/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ScheduleReport> ScheduleDriverReportList()
        {
            try
            {
                IEnumerable<ScheduleReport> query = DbMtData.ScheduleReports.Where(x => x.fk_ReportID > 3);
                return query;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driverRecipient"></param>
        /// <param name="fleetId"></param>
        /// <param name="userType"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public bool MerchantDriverRecipient(DriverRecList driverRecipient, int fleetId, int userType, int merchantId)
        {
            DriverReportRecipient[] driverRecipientObj = new DriverReportRecipient[driverRecipient.UserList.Count()];
            try
            {
                if (userType == 1)
                {
                    int countRec = 0;
                    foreach (var item in driverRecipient.UserList)
                    {
                        driverRecipientObj[countRec] = new DriverReportRecipient();
                        driverRecipientObj[countRec] = (from m in DbMtData.DriverReportRecipients where m.fk_DriverId == item.DriverID && m.fk_FleetId == item.fk_FleetId select m).FirstOrDefault();

                        if (driverRecipientObj[countRec] != null)
                        {
                            if ((Convert.ToBoolean(item.IsWeekly) || Convert.ToBoolean(item.IsDaily) || Convert.ToBoolean(item.IsOwnerRecipient) || Convert.ToBoolean(item.IsMonthly)) && Convert.ToBoolean(item.IsRecipient) == false)
                                item.IsRecipient = true;

                            driverRecipientObj[countRec].fk_DriverId = item.DriverID;
                            driverRecipientObj[countRec].fk_FleetId = item.fk_FleetId;
                            driverRecipientObj[countRec].IsRecipient = item.IsRecipient;
                            driverRecipientObj[countRec].UserType = userType;
                            driverRecipientObj[countRec].ModifiedBy = driverRecipient.ModifiedBy;
                            driverRecipientObj[countRec].CreatedBy = driverRecipient.ModifiedBy;
                            driverRecipientObj[countRec].CreatedDate = DateTime.Now;
                            driverRecipientObj[countRec].ModifiedDate = DateTime.Now;
                            driverRecipientObj[countRec].IsDaily = item.IsDaily;
                            driverRecipientObj[countRec].IsMonthly = item.IsMonthly;
                            driverRecipientObj[countRec].IsWeekly = item.IsWeekly;
                            driverRecipientObj[countRec].IsOwnerRecipient = item.IsOwnerRecipient;

                            DbMtData.CommitChanges();
                        }
                        else
                        {
                            if ((Convert.ToBoolean(item.IsWeekly) || Convert.ToBoolean(item.IsDaily) || Convert.ToBoolean(item.IsOwnerRecipient) || Convert.ToBoolean(item.IsMonthly)) && Convert.ToBoolean(item.IsRecipient) == false)
                                item.IsRecipient = true;

                            driverRecipientObj[countRec] = new DriverReportRecipient();
                            driverRecipientObj[countRec].fk_DriverId = item.DriverID;
                            driverRecipientObj[countRec].fk_FleetId = item.fk_FleetId;
                            driverRecipientObj[countRec].IsRecipient = item.IsRecipient;
                            driverRecipientObj[countRec].UserType = userType;
                            driverRecipientObj[countRec].ModifiedBy = driverRecipient.ModifiedBy;
                            driverRecipientObj[countRec].CreatedBy = driverRecipient.ModifiedBy;
                            driverRecipientObj[countRec].CreatedDate = DateTime.Now;
                            driverRecipientObj[countRec].ModifiedDate = DateTime.Now;
                            driverRecipientObj[countRec].IsDaily = item.IsDaily;
                            driverRecipientObj[countRec].IsMonthly = item.IsMonthly;
                            driverRecipientObj[countRec].IsWeekly = item.IsWeekly;
                            driverRecipientObj[countRec].IsOwnerRecipient = item.IsOwnerRecipient;

                            DbMtData.DriverReportRecipients.Add(driverRecipientObj[countRec]);
                            DbMtData.CommitChanges();
                        }
                        countRec++;
                    }

                    return true;
                }

                int count = 0;
                foreach (var item in driverRecipient.UserList)
                {
                    driverRecipientObj[count] = new DriverReportRecipient();
                    driverRecipientObj[count] = (from m in DbMtData.DriverReportRecipients where m.fk_DriverId == item.DriverID && m.fk_FleetId == fleetId select m).FirstOrDefault();
                    if (driverRecipientObj[count] != null)
                    {
                        driverRecipientObj[count].fk_DriverId = item.DriverID;
                        driverRecipientObj[count].fk_FleetId = fleetId;

                        if (item.IsRecipient == null || !(bool)item.IsRecipient)
                            driverRecipientObj[count].IsRecipient = false;
                        else
                            driverRecipientObj[count].IsRecipient = item.IsRecipient;

                        driverRecipientObj[count].UserType = userType;
                        driverRecipientObj[count].ModifiedBy = driverRecipient.ModifiedBy;
                        driverRecipientObj[count].CreatedBy = driverRecipient.ModifiedBy;
                        driverRecipientObj[count].CreatedDate = DateTime.Now;
                        driverRecipientObj[count].ModifiedDate = DateTime.Now;

                        if ((Convert.ToBoolean(item.IsWeekly) || Convert.ToBoolean(item.IsDaily) || Convert.ToBoolean(item.IsOwnerRecipient) || Convert.ToBoolean(item.IsMonthly)) && Convert.ToBoolean(item.IsRecipient) == false)
                        {
                            item.IsRecipient = true;
                            driverRecipientObj[count].IsRecipient = true;
                        }

                        if (item.IsOwnerRecipient != null && Convert.ToBoolean(item.IsRecipient))
                            driverRecipientObj[count].IsOwnerRecipient = true;
                        else
                            driverRecipientObj[count].IsOwnerRecipient = false;

                        if (item.IsDaily != null && Convert.ToBoolean(item.IsRecipient))
                            driverRecipientObj[count].IsDaily = true;
                        else
                            driverRecipientObj[count].IsDaily = false;

                        if (item.IsMonthly != null && Convert.ToBoolean(item.IsRecipient))
                            driverRecipientObj[count].IsMonthly = true;
                        else
                            driverRecipientObj[count].IsMonthly = false;

                        if (item.IsWeekly != null && Convert.ToBoolean(item.IsRecipient))
                            driverRecipientObj[count].IsWeekly = true;
                        else
                            driverRecipientObj[count].IsWeekly = false;
                        DbMtData.CommitChanges();
                    }
                    else
                    {
                        driverRecipientObj[count] = new DriverReportRecipient();
                        driverRecipientObj[count].fk_DriverId = item.DriverID;
                        driverRecipientObj[count].fk_FleetId = fleetId;

                        if (item.IsRecipient == null || !(bool)item.IsRecipient)
                            driverRecipientObj[count].IsRecipient = false;
                        else
                            driverRecipientObj[count].IsRecipient = item.IsRecipient;

                        driverRecipientObj[count].UserType = userType;
                        driverRecipientObj[count].ModifiedBy = driverRecipient.ModifiedBy;
                        driverRecipientObj[count].CreatedBy = driverRecipient.ModifiedBy;
                        driverRecipientObj[count].CreatedDate = DateTime.Now;
                        driverRecipientObj[count].ModifiedDate = DateTime.Now;

                        if ((Convert.ToBoolean(item.IsWeekly) || Convert.ToBoolean(item.IsDaily) || Convert.ToBoolean(item.IsOwnerRecipient) || Convert.ToBoolean(item.IsMonthly)) && Convert.ToBoolean(item.IsRecipient) == false)
                        {
                            item.IsRecipient = true;
                            driverRecipientObj[count].IsRecipient = true;
                        }

                        if (item.IsOwnerRecipient != null && Convert.ToBoolean(item.IsRecipient))
                            driverRecipientObj[count].IsOwnerRecipient = true;
                        else
                            driverRecipientObj[count].IsOwnerRecipient = false;

                        if (item.IsDaily != null && Convert.ToBoolean(item.IsRecipient))
                            driverRecipientObj[count].IsDaily = true;
                        else
                            driverRecipientObj[count].IsDaily = false;

                        if (item.IsMonthly != null && Convert.ToBoolean(item.IsRecipient))
                            driverRecipientObj[count].IsMonthly = true;
                        else
                            driverRecipientObj[count].IsMonthly = false;

                        if (item.IsWeekly != null && Convert.ToBoolean(item.IsRecipient))
                            driverRecipientObj[count].IsWeekly = true;
                        else
                            driverRecipientObj[count].IsWeekly = false;

                        DbMtData.DriverReportRecipients.Add(driverRecipientObj[count]);
                        DbMtData.CommitChanges();
                    }

                    count++;
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DriverReportRecipient> GetDriverReceipent()
        {
            try
            {
                var driverRecipient = (from drivers in DbMtData.Drivers
                                       join fleet in DbMtData.Fleets on drivers.fk_FleetID equals fleet.FleetID
                                       join user in DbMtData.Users on fleet.fk_MerchantID equals user.fk_MerchantID
                                       join merchant in DbMtData.Merchants on fleet.fk_MerchantID equals merchant.MerchantID
                                       join driverRec in DbMtData.DriverReportRecipients on
                                       drivers.DriverID equals driverRec.fk_DriverId
                                        into t
                                       from driverRec in t.DefaultIfEmpty()
                                       where (bool)drivers.IsActive && (bool)user.IsActive && user.fk_UserTypeID == 2
                                       select new
                                       {
                                           drivers.DriverID,
                                           drivers.DriverNo,
                                           drivers.FName,
                                           drivers.LName,
                                           driverRec.IsRecipient,
                                           driverRec.ModifiedBy,
                                           driverRec.IsDaily,
                                           driverRec.IsWeekly,
                                           driverRec.IsMonthly,
                                           drivers.Email,
                                           drivers.fk_FleetID,
                                           merchant.Company,
                                           driverRec.IsOwnerRecipient
                                       }).Distinct().AsEnumerable().Select(objNew =>

                     new DriverReportRecipient
                     {
                         DriverID = objNew.DriverID,
                         DriverNo = objNew.DriverNo,
                         FName = objNew.FName,
                         LName = objNew.LName,
                         IsRecipient = objNew.IsRecipient,
                         ModifiedBy = objNew.ModifiedBy,
                         Email = objNew.Email,
                         Company = objNew.Company,
                         IsDaily = objNew.IsDaily,
                         IsWeekly = objNew.IsWeekly,
                         IsMonthly = objNew.IsMonthly,
                         fk_FleetId = objNew.fk_FleetID,
                         IsOwnerRecipient = objNew.IsOwnerRecipient
                     });

                var a = driverRecipient.Distinct();
                return driverRecipient.Distinct();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<DriverReportRecipient> GetMerchantDriverReceipent(int merchantId)
        {
            try
            {
                var driverRecipient = (from drivers in DbMtData.Drivers
                                       join fleet in DbMtData.Fleets on drivers.fk_FleetID equals fleet.FleetID
                                       join merchant in DbMtData.Merchants on fleet.fk_MerchantID equals merchant.MerchantID
                                       join driverRec in DbMtData.DriverReportRecipients on
                                       drivers.DriverID equals driverRec.fk_DriverId
                                        into t
                                       from driverRec in t.DefaultIfEmpty()
                                       where (bool)drivers.IsActive && merchant.MerchantID == merchantId
                                       select new
                                       {
                                           drivers.DriverID,
                                           drivers.DriverNo,
                                           drivers.FName,
                                           drivers.LName,
                                           driverRec.IsRecipient,
                                           driverRec.ModifiedBy,
                                           driverRec.IsDaily,
                                           driverRec.IsWeekly,
                                           driverRec.IsMonthly,
                                           drivers.Email,
                                           drivers.fk_FleetID,
                                           driverRec.IsOwnerRecipient,
                                           merchant.Company
                                       }).Distinct().AsEnumerable().Select(objNew =>
                     new DriverReportRecipient
                     {
                         DriverID = objNew.DriverID,
                         DriverNo = objNew.DriverNo,
                         FName = objNew.FName,
                         LName = objNew.LName,
                         IsRecipient = objNew.IsRecipient,
                         ModifiedBy = objNew.ModifiedBy,
                         Email = objNew.Email,
                         Company = objNew.Company,
                         IsDaily = objNew.IsDaily,
                         IsWeekly = objNew.IsWeekly,
                         IsMonthly = objNew.IsMonthly,
                         fk_FleetId = objNew.fk_FleetID,
                         IsOwnerRecipient = objNew.IsOwnerRecipient
                     });
                var a = driverRecipient.Distinct();
                return driverRecipient.Distinct();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driverNo"></param>
        /// <param name="fleetId"></param>
        /// <returns></returns>
        public Driver GetDriverDto(string driverNo, int fleetIds)
        {
            Driver sDriver = new Driver();
            var flDb = DbMtData.Fleets.Where(x => x.FleetID == fleetIds).FirstOrDefault();

            if (flDb != null)
            {
                int? fleetId = flDb.FleetID;
                if (fleetId > 0)
                {
                    if (sDriver != null)
                    {
                        sDriver = DbMtData.Drivers.FirstOrDefault(a => a.DriverNo.Equals(driverNo) && a.fk_FleetID == fleetId && a.IsActive);
                        sDriver.fk_FleetID = fleetIds;
                        return sDriver;
                    }
                }
            }

            return sDriver;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="userId"></param>
        /// <param name="userFleet"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public IEnumerable<Driver> UserDriverList(FilterSearchParameters filterSearch)
        {
            try
            {
                IEnumerable<Driver> driversList = (from drivers in DbMtData.Drivers
                                                   join countries in DbMtData.CountryCodes on drivers.fk_Country equals countries.CountryCodesID
                                                   join n in DbMtData.Fleets on drivers.fk_FleetID equals n.FleetID
                                                   where drivers.IsActive && n.fk_MerchantID == filterSearch.MerchantId
                                                   select new { Drivers = drivers, CountryCodes = countries, n.FleetName }).AsEnumerable().Select(objNew =>
                                                        new Driver
                                                        {
                                                            FName = objNew.Drivers.FName,
                                                            LName = objNew.Drivers.LName,
                                                            Phone = objNew.Drivers.Phone,
                                                            Email = objNew.Drivers.Email,
                                                            DriverNo = objNew.Drivers.DriverNo,
                                                            DriverTaxNo = objNew.Drivers.DriverTaxNo,
                                                            fk_Country = objNew.CountryCodes.Title,
                                                            GridState = objNew.Drivers.Fk_State,
                                                            GridCity = objNew.Drivers.Fk_City,
                                                            Address = objNew.Drivers.Address,
                                                            IsActive = objNew.Drivers.IsActive,
                                                            DriverID = objNew.Drivers.DriverID,
                                                            FleetName = objNew.FleetName,
                                                            fk_FleetID = objNew.Drivers.fk_FleetID

                                                        }).ToList();

                driversList = driversList.Where(x => filterSearch.UserFleet.Select(y => y.fk_FleetID).Contains(x.fk_FleetID)).ToList();
                if (filterSearch.LogOnByUserType == 5)
                {
                    List<int> fleetListAccessed = GetAccesibleFleet(filterSearch.LogOnByUserId, filterSearch.MerchantId);
                    List<Driver> driverViewResult = driversList.Where(m => fleetListAccessed.Contains((int)m.fk_FleetID)).Select(m => m).Distinct().ToList();
                    driversList = driverViewResult.AsEnumerable();
                }

                if (!string.IsNullOrEmpty(filterSearch.Name) && string.IsNullOrEmpty(filterSearch.DriverNo))
                {
                    driversList = driversList.Where(p => p.FName.IndexOf(filterSearch.Name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }

                if (!string.IsNullOrEmpty(filterSearch.DriverNo) && string.IsNullOrEmpty(filterSearch.Name))
                {
                    driversList = driversList.Where(p => p.DriverNo.IndexOf(filterSearch.DriverNo, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }

                if (!string.IsNullOrEmpty(filterSearch.Name) && !string.IsNullOrEmpty(filterSearch.DriverNo))
                {
                    driversList = driversList.Where(p => p.FName.IndexOf(filterSearch.Name, StringComparison.OrdinalIgnoreCase) >= 0 && p.DriverNo.IndexOf(filterSearch.DriverNo, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }

                return driversList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get driver login data
        /// </summary>
        /// <param name="DeviceId"></param>
        /// <param name="DriverId"></param>
        /// <param name="UserType"></param>
        /// <returns></returns>
        public IEnumerable<usp_GetLoginData_Result> GetDriverLoginDto(string deviceId, string driverId, int userType)
        {
            var rslt = DbMtData.usp_GetLoginData(deviceId, driverId, userType);
            return rslt;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="hashPin"></param>
        /// <returns></returns>
        public bool UpdatePin(int id, string hashPin)
        {
            try
            {
                var driver = DbMtData.Drivers.Where(x => x.DriverID == id).FirstOrDefault();
                if (driver != null)
                {
                    driver.PIN = hashPin;
                    return Convert.ToBoolean(DbMtData.CommitChanges());
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }

        /// <summary>
        /// Purpose             :   to get the accessible fleets 
        /// Function Name       :   GetAccesibleFleet
        /// Created By          :   Naveen Kumar
        /// Created On          :   10/15/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public List<int> GetAccesibleFleet(int logOnByUserId, int merchantId)
        {
            List<int> accessibleFleets = (from m in DbMtData.UserFleets
                                          where m.fk_UserID == logOnByUserId && m.fk_MerchantId == merchantId
                                          && m.fk_UserTypeId == 5 && m.IsActive == true
                                          select (int)m.fk_FleetID).Distinct().ToList();
            return accessibleFleets;
        }


        public bool IsExist(int driverID)
        {
            return DbMtData.Drivers.Any(x => x.DriverID == driverID);
        }

        public bool IsDupligate(int fleetId, string driverId)
        {
            bool isFound = DbMtData.Fleets.Any(x => x.DispatchFleetID == fleetId);
            if (isFound)
            {
                int FleetfleetId = DbMtData.Fleets.Where(x => x.DispatchFleetID == fleetId).FirstOrDefault().FleetID;
                return DbMtData.Drivers.Any(x => x.fk_FleetID == FleetfleetId && x.DriverNo == driverId);
            }
            return false;
        }

        public Driver AddDispatchDriver(Driver driver)
        {
            try
            {
                Driver returnResult = new Driver();
                int? fleetId = DbMtData.Fleets.Where(x => x.DispatchFleetID == driver.fk_FleetID).FirstOrDefault().FleetID;
                if (driver.DriverID > 0)
                {
                    Driver dDispatch = DbMtData.Drivers.Where(x => x.DriverID == driver.DriverID).FirstOrDefault();

                    if (dDispatch.DriverNo != driver.DriverNo)
                    {
                        returnResult.DriverNo = driver.DriverNo;
                        dDispatch.DriverNo = driver.DriverNo;
                    }
                    else
                        returnResult.DriverNo = string.Empty;

                    if (dDispatch.Fk_City != driver.Fk_City)
                    {
                        returnResult.Fk_City = driver.Fk_City;
                        dDispatch.Fk_City = driver.Fk_City;
                    }
                    else
                        returnResult.Fk_City = string.Empty;
                    
                    if (dDispatch.FName != driver.FName) 
                    {
                        returnResult.FName = driver.FName;
                        dDispatch.FName = driver.FName;
                    }
                    else
                        returnResult.FName = string.Empty;

                    if (dDispatch.Email != driver.Email) 
                    {
                        returnResult.Email = driver.Email;
                        dDispatch.Email = driver.Email;
                    }
                    else
                        returnResult.Email = string.Empty;

                    if (dDispatch.PIN != driver.PIN) 
                    {
                        returnResult.PIN = driver.PIN;
                        dDispatch.PIN = driver.PIN;
                    }
                    else
                        returnResult.PIN = string.Empty;

                    if (dDispatch.Phone != driver.Phone) 
                    {
                        returnResult.Phone = driver.Phone;
                        dDispatch.Phone = driver.Phone;
                    }
                    else
                        returnResult.Phone = string.Empty;

                    if (dDispatch.IsActive != driver.IsActive) 
                    {
                        returnResult.IsActive = driver.IsActive;
                        dDispatch.IsActive = driver.IsActive;
                    }
                    else
                        returnResult.IsActive = false;

                    DbMtData.CommitChanges();

                    return returnResult;
                }
                else
                {
                    driver.fk_FleetID = Convert.ToInt32(fleetId); 
                    driver.IsActive = true;
                    driver.IsLocked = false;
                    driver.CreatedDate = DateTime.Now;
                    DbMtData.Drivers.Add(driver);
                    DbMtData.CommitChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return driver;
        }
    }
}

