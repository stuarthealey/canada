﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
   public  class RapidConnectTerminalDto
    {
        public int ID { get; set; }
        public string DID { get; set; }
        public string DatewireXml { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<int> MerchantId { get; set; }
        public string URL { get; set; }
        public string RCTerminalId { get; set; }
    }
}
