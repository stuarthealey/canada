﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MTData.Web.ViewData
{
    public class GatewayViewData
    {
        public int GatewayID { get; set; }
        [Required(ErrorMessage = "Please enter gateway name")]
        public string Name { get; set; }
        public bool IsActive { get; set; }
        [Required(ErrorMessage = "Please enter gateway url")]
        [Url(ErrorMessage = "Please enter valid url")]
        public string GatewayUrl { get; set; }
        [Required(ErrorMessage = "Please select gateway offset")]
        public string GatewayOffset { get; set; }
        [Required(ErrorMessage = "Sale supported is required")]
        public bool IsSaleSupported { get; set; }
        [Required(ErrorMessage = "Preauth supported is required")]
        public bool IsPreauthSupported { get; set; }
        [Required(ErrorMessage = "Capture supported is required")]
        public bool IsCaptureSupported { get; set; }
        [Required(ErrorMessage = "Cancel supported is required")]
        public bool IsCancelSupported { get; set; }
        [Required(ErrorMessage = "Refund supported is required")]
        public bool IsRefundSupported { get; set; }
        public IEnumerable<TimeZoneViewData> TimeZones { get; set; }
        public IEnumerable<CurrencyViewData> GatewayCurrencyList { get; set; }
        public string GatewayCurrency { get; set; }
        public bool IsDefault { get; set; }
        [Required(ErrorMessage = "Please enter user name")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Please enter password")]
        public string PCode { get; set; }
    }
}