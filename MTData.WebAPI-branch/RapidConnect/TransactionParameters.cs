﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTData.Utility.RapidConnect
{
    public sealed class TransactionParameters
    {
        GMFMessageVariants gmfMsgVar = new GMFMessageVariants();
        CreditRequestDetails creditReq = new CreditRequestDetails();

        public string GetClientRef()
        {
            string clientRef = string.Empty;

            CreditRequestDetails creditReq = gmfMsgVar.Item as CreditRequestDetails;
            clientRef = creditReq.CommonGrp.STAN + "|" + creditReq.CommonGrp.TPPID;
            clientRef = "00" + clientRef;

            return clientRef;
        }

    }
}
