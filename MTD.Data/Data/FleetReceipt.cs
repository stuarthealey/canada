//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MTD.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class FleetReceipt
    {
        public int FleetReceiptID { get; set; }
        public Nullable<int> fk_FleetID { get; set; }
        public int fk_FieldID { get; set; }
        public string fk_FieldText { get; set; }
        public int Postion { get; set; }
        public Nullable<bool> IsShow { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual Fleet Fleet { get; set; }
        public virtual ReceiptMaster ReceiptMaster { get; set; }
    }
}
