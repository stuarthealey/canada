
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MerchantSummaryReportScheduler]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_MerchantSummaryReportScheduler]
GO

/****** Object:  StoredProcedure [dbo].[usp_MerchantSummaryReportScheduler]    Script Date: 06/01/2015 10:59:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <09-06-2015>
-- Description:	<Prepare and Send summary reports to merchants>
-- =============================================
CREATE PROCEDURE [dbo].[usp_MerchantSummaryReportScheduler]
AS
BEGIN TRY
SET NOCOUNT ON;
SET ANSI_WARNINGS OFF
DECLARE @Res INT,
  @MerchantId INT,
  @To VARCHAR(50),
  @Body NVARCHAR(MAX),
  @Error VARCHAR(200),
  @MailId INT,
  @Valid INT,
  @MsgXml NVARCHAR(MAX),
  @XmlData NVARCHAR(MAX),
  @MerchantName NVARCHAR(50),
  @Company NVARCHAR(100),
  @ReportType INT,
  @RepType VARCHAR(50),
  @RepHeading VARCHAR(200),
  @FDate DATETIME,
  @TDate DATETIME,
  @TotalTransaction INT,
  @TotalActualFare NVARCHAR(30),
  @TotalTips NVARCHAR(30),
  @TotalFareAmount NVARCHAR(30),
  @TotalTechFee NVARCHAR(30),
  @TotalSurcharge NVARCHAR(30),
  @TotalFee   NVARCHAR(30),
  @NoChargeBack INT,
  @ChargeBackValue NVARCHAR(25),
  @NoVoid INT,
  @VoidValue NVARCHAR(25),
  @AmountOwing  NVARCHAR(30),
  @NextScheduleDateTime DATETIME,
  @ReportId INT,
  @NextAdminScheduleDateTime DATETIME,
  @Frequency INT,
  @FrequencyType INT,
  @ScheduleId INT
  DECLARE @CursorData TABLE
    (
      MerchantId INT,
	  Email NVARCHAR(80),
	  Company NVARCHAR(80),
	  ReportId  INT NULL
    )
  SELECT @NextScheduleDateTime=NextScheduleDateTime FROM ScheduleReport WHERE fk_MerchantId IS NULL AND fk_ReportID=1 AND IsCreated=1
  INSERT INTO @CursorData(MerchantId,Email,Company,ReportId)
  SELECT distinct m.MerchantID,u.Email,m.Company,NULLIF(1,sc.fk_ReportID)
  FROM Merchant  m INNER JOIN recipient r ON m.MerchantID=r.fk_MerchantID 
  INNER JOIN  Users u ON m.MerchantID=u.fk_MerchantID 
  LEFT JOIN ScheduleReport sc on sc.fk_MerchantId=m.MerchantID
  WHERE r.IsRecipient=1 AND r.fk_UserTypeId=2 AND r.fk_ReportId=1
  AND m.IsActive=1 AND u.fk_UserTypeID=2 
  AND ((sc.fk_ReportID=1 AND sc.IsCreated=1 AND sc.NextScheduleDateTime>=DATEADD(SECOND,-900,GETDATE()) AND sc.NextScheduleDateTime<GETDATE())
  OR (sc.fk_ReportID IS NULL AND @NextScheduleDateTime>=DATEADD(SECOND,-900,GETDATE()) AND @NextScheduleDateTime<GETDATE())
  OR (sc.fk_ReportID IN (2,3) AND 1<>(SELECT COUNT(*) FROM ScheduleReport WHERE fk_MerchantId=sc.fk_MerchantId AND IsCreated=1 AND fk_ReportID=1) 
  AND @NextScheduleDateTime>=DATEADD(SECOND,-900,GETDATE()) AND @NextScheduleDateTime<GETDATE()))
 --Declaring cursor for fetching merchant recipients one by one for summary report
  DECLARE Queue_Cursor CURSOR FOR 
  SELECT MerchantId,Email,Company,ReportId FROM @CursorData
  OPEN Queue_Cursor 
  FETCH NEXT FROM Queue_Cursor INTO @MerchantId,@To,@Company,@ReportId 
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
  SET @XmlData=''
  SELECT @MerchantName=FName FROM Users WHERE fk_MerchantID=@MerchantId AND fk_UserTypeID=2
  --Calling store procedure SummaryReport to get the data in xml format
  EXEC usp_MerchantSummaryReport @MerchantId,@ReportId,@XmlData OUTPUT,@ReportType OUTPUT,@FDate OUTPUT,@TDate OUTPUT,
  @TotalTransaction OUTPUT, @TotalActualFare OUTPUT,@TotalTips OUTPUT,@TotalFareAmount OUTPUT,
  @TotalSurcharge OUTPUT,@TotalFee  OUTPUT,@NoChargeBack OUTPUT, @ChargeBackValue OUTPUT,
  @NoVoid OUTPUT, @VoidValue OUTPUT,@TotalTechFee OUTPUT,@AmountOwing OUTPUT
  SET  @RepType=CASE @ReportType WHEN 1 THEN 'Daily Summarized Transaction Report' WHEN 2 THEN 'Weekly Summarized Transaction Report'
  ELSE 'Monthly Summarized Transaction Report' END
  SET @RepHeading=@RepType+' '+'(Dated from'+' '+ CONVERT(VARCHAR(11),@FDate,106)+' '+'to'+' '+CONVERT(VARCHAR(11),@TDate,106)+')'
  IF(@XmlData IS NULL OR @XmlData='') BEGIN 
  SET @Valid=0
  INSERT INTO MailMessage(RepUserId,[Status],SendTo,Body,RepUserTypeId,[Date],ReportType) VALUES(@MerchantId,-1,@To,'No records in database',2,GETDATE(),'MerchantSummaryReport')
  END
  ELSE BEGIN
  SET @Valid=1
  END
  IF(@Valid=1) BEGIN
  SET @MsgXml=CAST((SELECT CONCAT('Dear',' ',@MerchantName,',') AS 'p' FOR XML PATH('p'), ELEMENTS ) AS NVARCHAR(MAX))
  --Set the xml data in the html format 
  SET @Body ='<html>
  <style>td{text-align:left;font-family:verdana;font-size:12px} p{font-family:verdana;font-size:13px}
  span{font-family:verdana;font-size:13px}</style>
  <body><p>Listed below is the transaction report containing summarized details of transactions.</p>
  <H5 style="color:black;text-align:center;font-size:11px;font-family:verdana"> Merchant  :  '+@Company+'</H5>
  <H5 style="color:grey;text-align:center;font-size:11px;font-family:verdana">'+@RepHeading+'</H5>
  <table border = 1 cellpadding="0" cellspacing="0" style="background-color:white;border-collapse:collapse"> <tr>
  <th style="color:grey;font-size:13px;width:90px;text-align:left">Sr.No</th>
  <th style="color:grey;font-size:13px;width:600px;text-align:left">Fleet</th>
  <th style="color:grey;font-size:13px;width:600px;text-align:left">Car Number</th>
  <th style="color:grey;font-size:13px;width:140px;text-align:left">No of Trans</th>
  <th style="color:grey;font-size:13px;width:150px;text-align:left">Fare Amt</th>
  <th style="color:grey;font-size:13px;width:100px;height:19px;text-align:left">Tips</th>
  <th style="color:grey;font-size:13px;width:120px;text-align:left">Surcharges</th>
  <th style="color:grey;font-size:13px;width:100px;text-align:left">Tech Fee</th>
  <th style="color:grey;font-size:13px;width:100px;text-align:left">No of Chargebacks</th>
  <th style="color:grey;font-size:13px;width:100px;text-align:left">Chargeback Value</th>
  <th style="color:grey;font-size:13px;width:200px;text-align:left">No of void</th>
  <th style="color:grey;font-size:13px;width:130px;text-align:left">Void value</th>
  <th style="color:grey;font-size:13px;width:150px;text-align:left">Total Amount</th>
  <th style="color:grey;font-size:13px;width:150px;text-align:left">Fees</th>
  <th style="color:grey;font-size:13px;width:100px;text-align:left">Amount Owing</th></tr>'    
  SET @Body = @MsgXml+@Body + @XmlData +'<tfoot><tr style="height:25px;font-weight:bold">
  <td colspan=3 style="font-size:11px;text-align:center">
  Total</td><td style="font-size:11px">'+CAST(@TotalTransaction AS NVARCHAR(50))+'</td>
  <td style="font-size:11px">'+@TotalActualFare+'</td>
  <td style="font-size:11px">'+@TotalTips+'</td>
  <td style="font-size:11px">'+@TotalSurcharge+'</td>
  <td style="font-size:11px">'+@TotalTechFee+'</td>
  <td style="font-size:11px">'+CAST(@NoChargeBack AS NVARCHAR(50))+'</td>
  <td style="font-size:11px">'+@ChargeBackValue+'</td>
  <td style="font-size:11px">'+CAST(@NoVoid AS NVARCHAR(50))+'</td>
  <td style="font-size:11px">'+@VoidValue+'</td>
  <td style="font-size:11px">'+@totalFareAmount+'</td>
  <td style="font-size:11px">'+@TotalFee+'</td>
  <td style="font-size:11px">'+@AmountOwing+'</td></tr></tfoot></table><br/><br/>
  <span>Thanks & Regards,<br/><br/>MTData Developments Pty Ltd.<br/>www.mtdata.com<span>
  <h4 style="color:grey;font-size:12px;font-family:verdana">MTData-All rights reserved</h4>
  <p style="font-size:11px;color:black">Note:This is an automated message. Do not Reply or Reply All
  to this message as replies are not being accepted.</p> </body></html>'
  INSERT INTO MailMessage(RepUserId,[Status],SendTo,Body,[RepUserTypeId],[Date],[ReportType]) VALUES(@MerchantId,0,@To,@Body,2,GETDATE(),'MerchantSummaryReport')
  SET @MailId=SCOPE_IDENTITY()
  --Execute the sp_send_dbmail to send the summary report in html format 
  EXEC @Res=msdb.dbo.sp_send_dbmail
  @profile_name = 'MTData',
  @recipients =@To,
  @body =@Body,
  @subject = 'Merchant Summarized Transaction Report',
  @body_format ='HTML'
  IF (@Res != 0)
  BEGIN
     SET @Error =@@ERROR
     UPDATE MailMessage set Error=@Error,[Date]=GETDATE() where MailId=@MailId
  END;
  ELSE
  BEGIN
     UPDATE MailMessage set [Status]=1,[Date]=GETDATE() where MailId=@MailId
  END
  END
  FETCH NEXT FROM Queue_Cursor INTO @MerchantId,@To,@Company,@ReportId
  END   
  CLOSE Queue_Cursor   
  DEALLOCATE Queue_Cursor
  SELECT  @ScheduleId=ScheduleID,
	      @Frequency=Frequency,
          @FrequencyType=FrequencyType,
	      @NextAdminScheduleDateTime=NextScheduleDateTime		 
    FROM ScheduleReport 
    WHERE fk_ReportId=1 AND IsCreated=1 AND fk_MerchantId IS NULL
	IF(@NextAdminScheduleDateTime>=DATEADD(SECOND,-900,GETDATE()) AND @NextAdminScheduleDateTime<GETDATE())
	BEGIN
    IF(@Frequency=1)
    BEGIN
		SET @NextAdminScheduleDateTime=@NextAdminScheduleDateTime+@FrequencyType
    END
    ELSE IF(@Frequency=2)
    BEGIN
		SET @NextAdminScheduleDateTime=DATEADD(WK,@FrequencyType,@NextAdminScheduleDateTime)
    END
    ELSE IF(@Frequency=3)
    BEGIN
		SET @NextAdminScheduleDateTime=DATEADD(MONTH,@FrequencyType,@NextAdminScheduleDateTime)
    END  
	UPDATE ScheduleReport SET NextScheduleDateTime=@NextAdminScheduleDateTime WHERE ScheduleID=@ScheduleId
	END
END TRY
--use catch to log error in the table  ProcErrorHandler
BEGIN CATCH
       INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
       SELECT ERROR_NUMBER()
	         ,ERROR_SEVERITY()
			 ,ERROR_STATE()
			 ,ERROR_PROCEDURE()
			 ,ERROR_LINE()
			 ,ERROR_MESSAGE()
			 ,SUSER_SNAME()
			 ,HOST_NAME()
			 ,GETDATE()
END CATCH

GO


