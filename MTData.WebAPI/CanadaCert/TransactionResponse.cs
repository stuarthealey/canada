﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CanadaCert
{
    public class TransactionResponce
    {
        public string ReturnCode { get; set; }

        public string Payload { get; set; }
    }
}