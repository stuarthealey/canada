﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CapKeyClient
{
    class Program
    {
        static void Main(string[] args)
        {
            SubmitRequest();
        }

        public static void SubmitRequest()
        {
            StringBuilder sbLog = new StringBuilder();
            sbLog.AppendLine("[" + DateTime.Now.ToString("yyyyMMddHHmmss") + "] " + "\n\nCapKey process start...");

            string apiURL = ConfigurationManager.AppSettings["ApiURL"];
            sbLog.AppendLine("[" + DateTime.Now.ToString("yyyyMMddHHmmss") + "] " + "ApiURL: " + apiURL);

            sbLog.AppendLine("[" + DateTime.Now.ToString("yyyyMMddHHmmss") + "] " + "Preparing request to call api...");
            var wReq = (HttpWebRequest)WebRequest.Create(apiURL);
            wReq.ContentType = "text/xml; charset=utf-8";
            wReq.Timeout = 120 * 1000;
            wReq.Method = "POST";
            try
            {
                sbLog.AppendLine("[" + DateTime.Now.ToString("yyyyMMddHHmmss") + "] " + "Writing web request...");
                var sReq = new StreamWriter(wReq.GetRequestStream());
                sReq.Flush();
                sReq.Close();

                sbLog.AppendLine("[" + DateTime.Now.ToString("yyyyMMddHHmmss") + "] " + "Calling api...");
                var wResp = (HttpWebResponse)wReq.GetResponse();

                sbLog.AppendLine("[" + DateTime.Now.ToString("yyyyMMddHHmmss") + "] " + "API call was successfull. Process Complete.");
                Logger.WriteLog(sbLog.ToString());
            }
            catch (Exception ex)
            {
                sbLog.AppendLine("[" + DateTime.Now.ToString("yyyyMMddHHmmss") + "] " + "Exception occurred:\n" + ex.ToString());
                Logger.WriteLog(sbLog.ToString());
            }           
        }
    }
}
