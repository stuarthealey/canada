﻿using System;
using System.Collections.Generic;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Extension;
using MTD.Core.Factory.Interface;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Core.Service
{
    public class MerchantService : IMerchantService
    {
        private readonly IMerchantRepository _merchantRepository;
        private readonly IMerchantFactory _merchantFactory;

        public MerchantService([Named("MerchantRepository")] IMerchantRepository marchentRepository, [Named("MerchantFactory")] IMerchantFactory merchantFactory) //("MerchantRepository") maped in class NinjectObjectMapper in MTD.Core.Infrastructure same for ("MerchantFactory")
        {
            _merchantRepository = marchentRepository; //need to initilize object only once due to ninject
            _merchantFactory = merchantFactory;
        }

        /// <summary>
        /// Purpose             :   To Add the Merchant User
        /// Function Name       :   Add
        /// Created By          :   Umesh Kumar
        /// Created On          :   01/29/2015
        /// Modifications Made   :   ******************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantUserDto"></param>
        /// <returns></returns>
        public bool Add(MerchantUserDto merchantUserDto)
        {
            try
            {
                MerchantUser merchantuser = _merchantFactory.ToMerchantUser(merchantUserDto);
                return _merchantRepository.Add(merchantuser); // add method exist in MTD.Data.Repository to save a new merchant into database Merchant
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update the Merchant User 
        /// Function Name       :   Update
        /// Created By          :   Umesh Kumar
        /// Created On          :   01/29/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantUserDto"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public int Update(MerchantUserDto merchantUserDto, int merchantId)
        {
            try
            {
                MerchantUser merchantuser = _merchantFactory.ToMerchantUser(merchantUserDto);
                return _merchantRepository.Update(merchantuser, merchantId); // add method exist in MTD.Data.Repository to save a new merchant into database Merchant
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Merchant
        /// Function Name       :   GetMerchant
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/29/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public MerchantUserDto GetMerchant(int merchantId, int userId)
        {
            try
            {
                MerchantUser obj = _merchantRepository.GetMerchant(merchantId, userId);
                MerchantUserDto merchantDto = obj.ToDtoMerchantUser();
                return merchantDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public MerchantUserDto GetMerchantViewAs(int merchantId, int userId)
        {
            try
            {
                MerchantUser obj = _merchantRepository.GetMerchantViewAs(merchantId, userId);
                MerchantUserDto merchantDto = obj.ToDtoMerchantUser();
                return merchantDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Delete the Merchant
        /// Function Name       :   DeleteMerchant
        /// Created By          :   salil Gupta
        /// Created On          :   01/29/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        public void DeleteMerchant(int merchantId)
        {
            try
            {
                _merchantRepository.DeleteMerchant(merchantId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Merchants list
        /// Function Name       :   GetMerchants
        /// Created By          :   Asif Shafeeque
        /// Created On          :   01/30/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Merchant> GetMerchants()
        {
            try
            {
                return _merchantRepository.GetMerchants();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get the Merchant List
        /// Function Name       :   MerchantList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/02/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MerchantUserDto> MerchantList(string name)
        {
            try
            {
                IEnumerable<MerchantUser> merchantList = _merchantRepository.MerchantList(name);
                var merchantDtoList = merchantList.ToDTOList();
                return merchantDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get the User Roles
        /// Function Name       :   GetUserRoles
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/02/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<RoleDto> GetUserRoles() // for accessing roles of user
        {
            try
            {
                IEnumerable<Role> roles = _merchantRepository.GetUsersRole();
                IEnumerable<RoleDto> roleDto = roles.ToDTOList();
                return roleDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Add new Reciept
        /// Function Name       :   AddReciept
        /// Created By          :   Salil Gupta
        /// Created On          :   02/02/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="recieptDto"></param>
        public void AddReciept(ManageRecieptDto recieptDto) // for accessing roles of user
        {
            try
            {
                ManageReciept recipt = _merchantFactory.ToManageReciept(recieptDto);
                _merchantRepository.AddReciept(recipt);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Merchant List
        /// Function Name       :   GetMerchantByFilter
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   02/02/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public IEnumerable<MerchantUserDto> GetMerchantByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting)
        {
            try
            {
                var merchantList = _merchantRepository.GetMerchantByFilter(name, jtStartIndex, jtPageSize, jtSorting);
                var merchantDtoList = merchantList.ToDTOList();
                return merchantDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get the Merchant
        /// Function Name       :   GetMerchant
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/02/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public MerchantDto GetMerchant(int merchantId)
        {
            Merchant obj = _merchantRepository.GetMerchant(merchantId);
            MerchantDto merchantDto = obj.ToDtoMerchant();
            return merchantDto;
        }

        /// <summary>
        /// Purpose             :   To Get FDMerchant
        /// Function Name       :   GetFDMerchant
        /// Created By          :   Salil Gupta
        /// Created On          :   14/03/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public fDMerchantDto GetFDMerchant(int merchantId)
        {
            try
            {
                FDMerchant fdMerchant = _merchantRepository.GetFDMerchant(merchantId);
                return fdMerchant.TofdMerchantDto();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public fDMerchantDto FdMerchantExist(int fkDId)
        {
            try
            {
                FDMerchant fdMerchant = _merchantRepository.FdMerchantExist(fkDId);
                return fdMerchant.TofdMerchantDto();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get DataWire Details
        /// Function Name       :   GetDataWireDetails
        /// Created By          :   Salil Gupta
        /// Created On          :   14/03/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="serialNo"></param>
        /// <returns></returns>
        public DatawireDto GetDataWireDetails(int merchantId, int id)
        {
            try
            {
                Datawire datawire = _merchantRepository.GetDataWireDetails(merchantId, id);
                return datawire.TodatawireDto();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To TerminalDetails
        /// Function Name       :   GetFDMerchant
        /// Created By          :   Naveen Kumar
        /// Created On          :   14/02/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="serialNo"></param>
        /// <returns></returns>
        public TerminalDto TerminalDetails(string serialNo)
        {
            try
            {
                Terminal terminal = _merchantRepository.TerminalDetails(serialNo);
                return terminal.ToTerminalDto();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Delete Fd Merchant
        /// Function Name       :   DeleteFdMerchant
        /// Created By          :   Umesh Kumar
        /// Created On          :   14/02/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fdMerchantId"></param>
        /// <returns></returns>
        public bool DeleteFdMerchant(int fdMerchantId)
        {
            try
            {
                return _merchantRepository.DeleteFdMerchant(fdMerchantId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To FD Merchant Update
        /// Function Name       :   GetFDMerchantUpdate
        /// Created By          :   Naveen Kumar
        /// Created On          :   14/02/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fdMerchantId"></param>
        /// <returns></returns>
        public fDMerchantDto GetFDMerchantUpdate(int fdMerchantId)
        {
            try
            {
                FDMerchant obj = _merchantRepository.GetFdMerchantUpdate(fdMerchantId);
                fDMerchantDto merchantDto = obj.TofdMerchantDto();
                return merchantDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Fd Merchant Update
        /// Function Name       :   FdMerchantUpdate
        /// Created By          :   Naveen Kumar
        /// Created On          :   14/02/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fDMerchantDto"></param>
        /// <param name="fdMerchantId"></param>
        /// <returns></returns>
        public void FdMerchantUpdate(fDMerchantDto fDMerchantDto, int fdMerchantId)
        {
            try
            {
                FDMerchant fdMerchant = fDMerchantDto.ToFdMerchant();
                _merchantRepository.UpdateFdMerchant(fdMerchant, fdMerchantId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Fd Merchant For Update
        /// Function Name       :   GetFdMerchantForUpdate
        /// Created By          :   Umesh Kumar
        /// Created On          :   14/02/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fdId"></param>
        /// <returns></returns>
        public fDMerchantDto GetFdMerchantForUpdate(int fdId)
        {
            var fdMerchant = _merchantRepository.GetFdMerchantForUpdate(fdId);
            var fdMerchantDto = fdMerchant.TofdMerchantDto();
            return fdMerchantDto;
        }

        /// <summary>
        /// Purpose             :   To Get FD Merchant List
        /// Function Name       :   GetFDMerchantList
        /// Created By          :   Umesh Kumar
        /// Created On          :   14/02/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<FDMerchantListDto> GetFDMerchantList()
        {
            try
            {
                IEnumerable<FDMerchant> fdMerchantList = _merchantRepository.GetFDMerchantList();
                IEnumerable<FDMerchantListDto> fdList = fdMerchantList.ToFdMerchantListDTO();
                return fdList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get FD Merchant List
        /// Function Name       :   GetFDMerchantList
        /// Created By          :   Umesh Kumar
        /// Created On          :   14/04/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fDMerchantDto"></param>
        /// <returns></returns>
        public bool FdMerchantAdd(fDMerchantDto fDMerchantDto)
        {
            try
            {
                var fdmerchant = new FDMerchant
                {
                    FDMerchantID = fDMerchantDto.FDMerchantID,
                    GroupId = fDMerchantDto.GroupId,
                    ServiceId = fDMerchantDto.ServiceId,
                    ProjectId = fDMerchantDto.ProjectId,
                    App = fDMerchantDto.App,
                    ServiceUrl = fDMerchantDto.ServiceUrl,
                    CreatedBy = fDMerchantDto.CreatedBy
                };

                return _merchantRepository.FdMerchantAdd(fdmerchant);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Add Fd Merchant
        /// Function Name       :   AddFdMerchant
        /// Created By          :   Umesh Kumar
        /// Created On          :   14/04/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="fDMerchantDto"></param>
        /// <returns></returns>
        public int AddFdMerchant(fDMerchantDto fDMerchantDto)
        {
            try
            {
                var fdmerchant = new FDMerchant
                {
                    FDMerchantID = fDMerchantDto.FDMerchantID,
                    GroupId = fDMerchantDto.GroupId,
                    ServiceId = fDMerchantDto.ServiceId,
                    ProjectId = fDMerchantDto.ProjectId,
                    App = fDMerchantDto.App,
                    ServiceUrl = fDMerchantDto.ServiceUrl,
                    CreatedBy = fDMerchantDto.CreatedBy,
                    MCCode = fDMerchantDto.MCCode,
                    TokenType = fDMerchantDto.TokenType,
                    ProjectType = fDMerchantDto.ProjectType,
                    EcommProjectId = fDMerchantDto.EcommProjectId
                };

                return _merchantRepository.AddFdMerchant(fdmerchant);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Fd Merchant List
        /// Function Name       :   GetFdMerchantListByFilter
        /// Created By          :   Umesh Kumar
        /// Created On          :   14/04/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public IEnumerable<fDMerchantDto> GetFdMerchantListByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting)
        {
            try
            {
                var fdMerchantList = _merchantRepository.GetFdMerchantByFilter(name, jtStartIndex, jtPageSize, jtSorting);
                var merchantDtoList = fdMerchantList.ToFdDTOList();
                return merchantDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Merchant Id
        /// Function Name       :   GetMerchantId
        /// Created By          :   Umesh Kumar
        /// Created On          :   14/05/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public int GetMerchantId(string email)
        {
            return _merchantRepository.GetMerchantId(email);
        }

        /// <summary>
        /// Purpose             :   To Check Company Exsit
        /// Function Name       :   CompanyExsit
        /// Created By          :   Umesh Kumar
        /// Created On          :   14/05/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public bool CompanyExsit(string company)
        {
            return _merchantRepository.CompanyExsit(company);
        }

        /// <summary>
        /// Purpose             :   To Check Email Exist
        /// Function Name       :   EmailExist
        /// Created By          :   Umesh Kumar
        /// Created On          :   14/05/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool EmailExist(string email)
        {
            return _merchantRepository.EmailExist(email);
        }

        /// <summary>
        /// Purpose             :   To Update Password
        /// Function Name       :   UpdatePassword
        /// Created By          :   Umesh Kumar
        /// Created On          :   14/06/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public bool UpdatePassword(int userId, string newPassword)
        {
            return _merchantRepository.UpdatePassword(userId, newPassword);
        }

        /// <summary>
        /// Purpose             :   To Get Email
        /// Function Name       :   GetEmail
        /// Created By          :   Umesh Kumar
        /// Created On          :   14/06/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetEmail(int userId)
        {
            return _merchantRepository.GetEmail(userId);
        }

        /// <summary>
        /// Purpose             :   To Get User Receipent
        /// Function Name       :   GetEmail
        /// Created By          :   Asif
        /// Created On          :   14/02/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<MerchantListDto> GetUserReceipent(int merchantId)
        {
            try
            {
                var recipientList = _merchantRepository.GetUserReceipent(merchantId);
                var recipientListDto = recipientList.ToDtoRecipientList();
                return recipientListDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update User Recipient
        /// Function Name       :   UpdateUserRecipient
        /// Created By          :   Asif Shafeeque
        /// Created On          :   06/05/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="recipientDto"></param>
        /// <returns></returns>
        public bool UpdateUserRecipient(RecipientDto recipientDto)
        {
            try
            {
                RecipientMaster recipientMaster = recipientDto.ToRecipientMaster();
                return _merchantRepository.UpdateUserRecipient(recipientMaster);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get ReportList
        /// Function Name       :   GetReportList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   06/05/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ReportListDto> GetReportList()
        {
            try
            {
                var reportList = _merchantRepository.GetReportList();
                return reportList.ToReportList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get ReportList
        /// Function Name       :   GetReportList
        /// Created By          :   Asif Shafeeque
        /// Created On          :   06/15/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="reportId"></param>
        /// <returns></returns>
        public IEnumerable<MerchantListDto> GetUserReceipent(int merchantId, int reportId)
        {
            try
            {
                var recipientList = _merchantRepository.GetUserReceipent(merchantId, reportId);
                var recipientListDto = recipientList.ToDtoRecipientList();
                return recipientListDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public bool CompanyExsit(string company, int merchantId)
        {
            return _merchantRepository.CompanyExsit(company, merchantId);
        }

        /// <summary>
        /// Purpose             :   To Update Password
        /// Function Name       :   UpdatePassword
        /// Created By          :   Umesh Kumar
        /// Created On          :   20/11/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public bool UpdateTCPassword(int merchantId, string userName, string newPassword)
        {
            return _merchantRepository.UpdateTCPassword(merchantId, userName, newPassword);
        }

        /// <summary>
        /// Purpose             :   To Update Password
        /// Function Name       :   UpdatePassword
        /// Created By          :   Umesh Kumar
        /// Created On          :   20/11/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public string IsAllReadySet(int merchantId)
        {
            return _merchantRepository.IsAllReadySet(merchantId);
        }

        public bool IsUserAvailable(string userName)
        {
            return _merchantRepository.IsUserAvailable(userName);
        }

        public string GetMerchantZipCode(int merchantId)
        {
            return _merchantRepository.GetMerchantZipCode(merchantId);
        }

        //PAY-13
        /// <summary>
        /// Get the IsTransArmor value from the Merchant.
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public bool IsTransArmor(int merchantId)
        {
            return _merchantRepository.IsTransArmor(merchantId);
        }

    }
}
