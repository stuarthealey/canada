﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Data.Repository
{
    public class VehicleRepository : BaseRepository, IVehicleRepository
    {
        public VehicleRepository(IMtDataDevlopmentsEntities dbMtData) : base(dbMtData)
        {
        }

        Regex alphaNum = new Regex(@"^[a-zA-Z0-9 ]+$");

        /// <summary>
        ///  Purpose            :  to add vehicle
        /// Function Name       :   Add
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/20/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"  
        /// </summary>
        /// <param name="vehicle"></param>
        public int Add(Vehicle vehicle)
        {
            try
            {
                var vehicleDb = DbMtData.Vehicles.FirstOrDefault(m => (m.VehicleNumber == vehicle.VehicleNumber || m.VehicleRegNo == vehicle.VehicleRegNo || m.PlateNumber == vehicle.PlateNumber) && m.fk_FleetID == vehicle.fk_FleetID && m.IsActive);
                if (vehicleDb != null)
                {
                    if (vehicleDb.VehicleNumber == vehicle.VehicleNumber && vehicleDb.fk_FleetID == vehicle.fk_FleetID)
                        return 1;
                    if (vehicleDb.VehicleRegNo == vehicle.VehicleRegNo && vehicleDb.fk_FleetID == vehicle.fk_FleetID)
                        return 2;
                    if (vehicleDb.PlateNumber == vehicle.PlateNumber && vehicleDb.fk_FleetID == vehicle.fk_FleetID)
                        return 3;
                }

                vehicle.IsActive = true;
                vehicle.CreatedDate = DateTime.Now;
                DbMtData.Vehicles.Add(vehicle);
                DbMtData.CommitChanges();

                var terminal = (from m in DbMtData.Vehicles
                                join n in DbMtData.Terminals on vehicle.fk_TerminalID equals n.TerminalID
                                select n).FirstOrDefault();
                
                if (terminal != null)
                {
                    int terminalId = terminal.TerminalID;
                    Terminal terminalDb = DbMtData.Terminals.Find(terminalId);
                    terminalDb.IsTerminalAssigned = true;
                    DbMtData.CommitChanges();

                }

                return 0;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :  to get vehicle
        /// Function Name       :   GetVehicle
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/24/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        public Vehicle GetVehicle(int vehicleId)
        {
            try
            {
                Vehicle vehicleDb = DbMtData.Vehicles.Find(vehicleId);
                return vehicleDb;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :  Fetching the Vehicle on the basis of its ID from the database.
        /// Function Name       :   GetVehicleByTerminal
        /// Created By          :   Naveen Kumar
        /// Created On          :  12/24/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"  
        /// </summary>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        public Vehicle GetVehicleByTerminal(int terminalId)
        {
            var sVehicle = DbMtData.Vehicles.FirstOrDefault(a => a.fk_TerminalID == terminalId && a.IsActive);
            return sVehicle;
        }

        /// <summary>
        ///  Purpose            :  to get vehicle from terminal
        /// Function Name       :   Update
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/24/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="vehicleId"></param>
        /// <param name="terminalId"></param>
        public int Update(Vehicle vehicle, int vehicleId, int terminalId)
        {
            try
            {
                var vehicleDB = DbMtData.Vehicles.FirstOrDefault(m => ((m.VehicleNumber == vehicle.VehicleNumber || m.VehicleRegNo == vehicle.VehicleRegNo || m.PlateNumber == vehicle.PlateNumber) && m.IsActive && m.VehicleID != vehicleId && m.fk_FleetID == vehicle.fk_FleetID));
                if (vehicleDB != null)
                {
                    if (vehicleDB.VehicleNumber == vehicle.VehicleNumber)
                        return 1;
                    if (vehicleDB.VehicleRegNo == vehicle.VehicleRegNo)
                        return 2;
                    if (vehicleDB.PlateNumber == vehicle.PlateNumber)
                        return 3;
                }

                Vehicle vehicleDb = DbMtData.Vehicles.Find(vehicleId);
                vehicleDb.VehicleNumber = vehicle.VehicleNumber;
                vehicleDb.VehicleRegNo = vehicle.VehicleRegNo;
                vehicleDb.PlateNumber = vehicle.PlateNumber;
                vehicleDb.fk_FleetID = vehicle.fk_FleetID;
                vehicleDb.fk_TerminalID = vehicle.fk_TerminalID;
                vehicleDb.ModifiedDate = DateTime.Now;
                vehicleDb.ModifiedBy = vehicle.ModifiedBy;
                vehicleDb.Description = vehicle.Description;
                vehicleDb.fk_CarOwenerId = vehicle.fk_CarOwenerId;
                
                if (terminalId != vehicle.fk_TerminalID)
                {
                    if (vehicle.fk_TerminalID != null)
                    {
                        Terminal updatedTerminal = DbMtData.Terminals.Find(vehicle.fk_TerminalID);
                        updatedTerminal.IsTerminalAssigned = true;
                    }
                    if (terminalId != 0)
                    {
                        Terminal terminal = DbMtData.Terminals.Find(terminalId);
                        terminal.IsTerminalAssigned = false;
                    }
                }
            }
            catch
            {
                throw;
            }

            DbMtData.CommitChanges();
            return 0;
        }

        /// <summary>
        ///  Purpose            :  to delete the vehicle.
        /// Function Name       :   DeleteVehicle
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/25/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="vehicleId"></param>
        public void DeleteVehicle(int vehicleId)
        {
            try
            {
                Vehicle vehicleDb = DbMtData.Vehicles.Find(vehicleId);
                Terminal terminalDb = (from m in DbMtData.Terminals where m.TerminalID == vehicleDb.fk_TerminalID select m).FirstOrDefault();
                
                if (terminalDb != null) terminalDb.IsTerminalAssigned = false;
                vehicleDb.IsActive = false;
                DbMtData.CommitChanges();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :  to get the list of vehicle
        /// Function Name       :   VehicleList
        /// Created By          :   Naveen Kumar
        /// Created On          :  12/26/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<Vehicle> VehicleList(int merchantId)
        {
            try
            {
                var vehicleList = (from m in DbMtData.Vehicles
                                   join n in DbMtData.Fleets on m.fk_FleetID
                                       equals n.FleetID
                                   join k in DbMtData.Terminals on m.fk_TerminalID
                                       equals k.TerminalID
                                   where m.IsActive && n.fk_MerchantID == merchantId
                                   select
                                       new { m.VehicleID, m.VehicleNumber, m.VehicleRegNo, m.PlateNumber, n.FleetName, k.DeviceName }).AsEnumerable()
                    .Select
                    (objNew => new Vehicle
                    {
                        VehicleID = objNew.VehicleID,
                        VehicleRegNo = objNew.VehicleRegNo,
                        VehicleNumber = objNew.VehicleNumber,
                        PlateNumber = objNew.PlateNumber,
                        FleetName = objNew.FleetName,
                        DeviceName = objNew.DeviceName
                    }).ToList();

                return vehicleList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        ///  Purpose            :  to get the vehicle filtered list 
        /// Function Name       :   GetVehileListByFilter
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/29/2014
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <param name="sorting"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<Vehicle> GetVehileListByFilter(FilterSearchParameters fSearch)
        {
            try
            {
                var vehicleList = GetVehileList(fSearch);

                if (!string.IsNullOrEmpty(fSearch.Sorting))
                {
                    vehicleList = SortedVehicle(fSearch.Sorting, vehicleList);
                }

                return fSearch.PageSize > 0
                     ? vehicleList.Skip(fSearch.StartIndex).Take(fSearch.PageSize).ToList() //Paging
                     : vehicleList.ToList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get the vehicle list by merchant id. 
        /// Function Name       :   GetVehileList
        /// Created By          :   Sunil Singh
        /// Created On          :   06/25/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY" 
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public IEnumerable<Vehicle> GetVehileList(FilterSearchParameters fSearch)
        {
            try
            {
                var vehicleList = (from m in DbMtData.Vehicles
                                   join n in DbMtData.Fleets on m.fk_FleetID equals n.FleetID
                                   join owner in DbMtData.CarOwners on m.fk_CarOwenerId equals owner.Id into carlist
                                   from owner in carlist.DefaultIfEmpty()
                                   join k in DbMtData.Terminals on m.fk_TerminalID equals k.TerminalID into res
                                   from y1 in res.DefaultIfEmpty()
                                   where m.IsActive && n.fk_MerchantID == fSearch.MerchantId
                                   select
                        new { m.VehicleID, m.VehicleNumber, m.VehicleRegNo, m.PlateNumber, n.FleetName, y1.SerialNo, n.fk_MerchantID, m.fk_FleetID, y1.MacAdd, owner.OwnerName })
                    .AsEnumerable()
                    .Select
                    (objNew => new Vehicle
                    {
                        VehicleID = objNew.VehicleID,
                        VehicleNumber = objNew.VehicleNumber,
                        PlateNumber = objNew.PlateNumber,
                        FleetName = objNew.FleetName,
                        DeviceName = objNew.MacAdd,
                        VehicleRegNo = objNew.VehicleRegNo,
                        fk_FleetID = objNew.fk_FleetID,
                        CarOwnerName = objNew.OwnerName
                    });
                if (fSearch.LogOnByUserType == 5)
                {
                    List<int> fleetListAccessed = GetAccesibleFleet(fSearch.LogOnByUserId, fSearch.MerchantId);
                    List<Vehicle> fleetViewResult = vehicleList.Where(m => fleetListAccessed.Contains((int)m.fk_FleetID)).Select(m => m).Distinct().ToList();
                    vehicleList = fleetViewResult.AsEnumerable();
                }
                vehicleList = vehicleList.OrderBy(p => p.VehicleNumber); //Default!
                if (!string.IsNullOrEmpty(fSearch.Name))
                {
                    vehicleList = vehicleList.Where(p => p.VehicleNumber.IndexOf(fSearch.Name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                if (!string.IsNullOrEmpty(fSearch.SerialNumber))
                {
                    vehicleList = vehicleList.Where(x => x.DeviceName != null);
                    vehicleList = vehicleList.Where(p => p.DeviceName.IndexOf(fSearch.SerialNumber, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                if (!string.IsNullOrEmpty(fSearch.OwnerName))
                {
                    vehicleList = vehicleList.Where(x => x.CarOwnerName != null);
                    vehicleList = vehicleList.Where(p => p.CarOwnerName.IndexOf(fSearch.OwnerName, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                if (!string.IsNullOrEmpty(fSearch.Name) && !string.IsNullOrEmpty(fSearch.OwnerName) && !string.IsNullOrEmpty(fSearch.SerialNumber))
                {
                    vehicleList = vehicleList.Where(p => p.VehicleNumber.IndexOf(fSearch.Name, StringComparison.OrdinalIgnoreCase) >= 0 && p.CarOwnerName.IndexOf(fSearch.OwnerName, StringComparison.OrdinalIgnoreCase) >= 0 && p.DeviceName.IndexOf(fSearch.SerialNumber, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                return vehicleList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sorting"></param>
        /// <param name="vehicleList"></param>
        /// <returns></returns>
        private IEnumerable<Vehicle> SortedVehicle(string sorting, IEnumerable<Vehicle> vehicleList)
        {
            switch (sorting)
            {
                case "VehicleNumber ASC":
                    vehicleList = vehicleList.OrderBy(p => p.VehicleNumber);
                    break;
                case "VehicleNumber DESC":
                    vehicleList = vehicleList.OrderByDescending(p => p.VehicleNumber);
                    break;
                case "VehicleRegNo ASC":
                    vehicleList = vehicleList.OrderBy(p => p.VehicleRegNo);
                    break;
                case "VehicleRegNo DESC":
                    vehicleList = vehicleList.OrderByDescending(p => p.VehicleRegNo);
                    break;
                case "PlateNumber ASC":
                    vehicleList = vehicleList.OrderBy(p => p.PlateNumber);
                    break;
                case "PlateNumber DESC":
                    vehicleList = vehicleList.OrderByDescending(p => p.PlateNumber);
                    break;
                case "FleetName ASC":
                    vehicleList = vehicleList.OrderBy(p => p.FleetName);
                    break;
                case "FleetName DESC":
                    vehicleList = vehicleList.OrderByDescending(p => p.FleetName);
                    break;
            }
            return vehicleList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public int IsValidVehicle(Vehicle vehicle, int merchantId, int userId, int userType)
        {
            try
            {
                int response = 7;
                if (vehicle.VehicleNumber == string.Empty)
                    return 9;
                if (vehicle.VehicleRegNo == string.Empty)
                    return 10;
                if (vehicle.PlateNumber == string.Empty)
                    return 11;
                if (vehicle.FleetName == string.Empty)
                    return 12;
                if (vehicle.Description.Length >= 200)
                    return 14;
                if (vehicle.VehicleNumber.Length >= 100)
                    return 15;
                if (vehicle.VehicleRegNo.Length >= 100)
                    return 16;
                if (vehicle.PlateNumber.Length >= 100)
                    return 17;
                Match exp = alphaNum.Match(vehicle.VehicleNumber);
                if (!exp.Success)
                    return 18;
                exp = alphaNum.Match(vehicle.VehicleRegNo);
                if (!exp.Success)
                    return 19;
                exp = alphaNum.Match(vehicle.PlateNumber);
                if (!exp.Success)
                    return 20;
                if (!string.IsNullOrEmpty(vehicle.CarOwnerEmail))
                {
                    var carOwner = DbMtData.CarOwners.Any(x => x.EmailAddress == vehicle.CarOwnerEmail && (bool)x.IsActive && x.fk_MerchantId == merchantId);
                    if (!carOwner)
                        return 13; // EmailId does'nt exist for Car Owner
                }

                var fleet = DbMtData.Fleets.Where(x => (x.fk_MerchantID == merchantId && x.FleetName == vehicle.FleetName && x.IsActive)).FirstOrDefault();
                if (fleet == null)
                {
                    response = 5;// fleet is not assigned to that merchant.
                    return response;
                }

                if (userType == 4)
                {
                    var isExist = DbMtData.UserFleets.Any(x => x.fk_UserID == userId && x.fk_FleetID == fleet.FleetID);
                    if (isExist)
                    {
                        if (vehicle.DeviceName != string.Empty)
                        {
                            var terminaldb = DbMtData.Terminals.Where(x => (x.fk_MerchantID == merchantId && x.MacAdd == vehicle.DeviceName && x.IsActive && !(bool)x.IsTerminalAssigned)).FirstOrDefault();
                            if (terminaldb == null)
                            {
                                response = 6; return response;
                            }
                            if (terminaldb != null && fleet != null)
                            {
                                response = 7; return response;
                            }
                            else
                            {
                                response = 6; return response;
                            }
                        }
                    }
                    else
                    {
                        response = 8;//fleet is not assigned to that user.
                        return response;
                    }
                }

                if (vehicle.DeviceName != string.Empty)
                {
                    var terminal = DbMtData.Terminals.Where(x => (x.fk_MerchantID == merchantId && x.MacAdd == vehicle.DeviceName && x.IsActive && !(bool)x.IsTerminalAssigned)).FirstOrDefault();
                    if (terminal != null && fleet != null)
                        response = 7;
                    else
                        response = 6;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public Vehicle VehicelExcel(Vehicle vehicle, int merchantId)
        {
            try
            {
                var fleet = DbMtData.Fleets.Where(x => (x.fk_MerchantID == merchantId && x.FleetName == vehicle.FleetName && x.IsActive)).FirstOrDefault();
                var terminal = DbMtData.Terminals.Where(x => (x.fk_MerchantID == merchantId && x.MacAdd == vehicle.DeviceName && x.IsActive)).FirstOrDefault();
                if (!string.IsNullOrEmpty(vehicle.CarOwnerEmail))
                {
                    var carOwner = DbMtData.CarOwners.Where(x => x.EmailAddress == vehicle.CarOwnerEmail && (bool)x.IsActive && x.fk_MerchantId == merchantId).FirstOrDefault();
                    if (carOwner != null)
                        vehicle.fk_CarOwenerId = carOwner.Id;
                }
                if (terminal != null)
                    vehicle.fk_TerminalID = (int)terminal.TerminalID;
                if (fleet != null)
                    vehicle.fk_FleetID = (int)fleet.FleetID;
                return vehicle;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fsParametes"></param>
        /// <returns></returns>
        public IEnumerable<Vehicle> UserVehicleList(FilterSearchParameters fsParametes)
        {
            try
            {
                var vehicleList = (from m in DbMtData.Vehicles
                                   join n in DbMtData.Fleets on m.fk_FleetID equals n.FleetID
                                   join owner in DbMtData.CarOwners on m.fk_CarOwenerId equals owner.Id into carlist
                                   from owner in carlist.DefaultIfEmpty()
                                   join k in DbMtData.Terminals on m.fk_TerminalID equals k.TerminalID into res
                                   from y1 in res.DefaultIfEmpty()
                                   where m.IsActive && n.fk_MerchantID == fsParametes.MerchantId
                                   select
                        new { m.VehicleID, m.VehicleNumber, m.VehicleRegNo, m.PlateNumber, n.FleetName, y1.SerialNo, n.fk_MerchantID, m.fk_FleetID, owner.OwnerName })
                   .AsEnumerable()
                   .Select
                   (objNew => new Vehicle
                   {
                       VehicleID = objNew.VehicleID,
                       VehicleNumber = objNew.VehicleNumber,
                       PlateNumber = objNew.PlateNumber,
                       FleetName = objNew.FleetName,
                       DeviceName = objNew.SerialNo,
                       VehicleRegNo = objNew.VehicleRegNo,
                       fk_FleetID = objNew.fk_FleetID,
                       CarOwnerName = objNew.OwnerName
                   });

                var userfleet = fsParametes.UserFleet;
                vehicleList = vehicleList.Where(x => userfleet.Select(y => y.fk_FleetID).Contains(x.fk_FleetID)).ToList();
                
                if (fsParametes.LogOnByUserType == 5)
                {
                    List<int> fleetListAccessed = GetAccesibleFleet(fsParametes.LogOnByUserId, fsParametes.MerchantId);
                    List<Vehicle> fleetViewResult = vehicleList.Where(m => fleetListAccessed.Contains((int)m.fk_FleetID)).Select(m => m).Distinct().ToList();
                    vehicleList = fleetViewResult.AsEnumerable();
                }
                if (!string.IsNullOrEmpty(fsParametes.Name))
                {
                    vehicleList = vehicleList.Where(p => p.VehicleNumber.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                if (!string.IsNullOrEmpty(fsParametes.OwnerName))
                {
                    vehicleList = vehicleList.Where(x => x.CarOwnerName != null);
                    vehicleList = vehicleList.Where(p => p.CarOwnerName.IndexOf(fsParametes.OwnerName, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                if (!string.IsNullOrEmpty(fsParametes.SerialNumber))
                {
                    vehicleList = vehicleList.Where(x => x.DeviceName != null);
                    vehicleList = vehicleList.Where(p => p.DeviceName.IndexOf(fsParametes.SerialNumber, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                if (!string.IsNullOrEmpty(fsParametes.Name) && !string.IsNullOrEmpty(fsParametes.OwnerName) && !string.IsNullOrEmpty(fsParametes.SerialNumber))
                {
                    vehicleList = vehicleList.Where(p => p.VehicleNumber.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0 && p.CarOwnerName.IndexOf(fsParametes.OwnerName, StringComparison.OrdinalIgnoreCase) >= 0 && p.DeviceName.IndexOf(fsParametes.SerialNumber, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }

                if (!string.IsNullOrEmpty(fsParametes.Sorting))
                {
                    vehicleList = SortedVehicle(fsParametes.Sorting, vehicleList).ToList();
                }

                return fsParametes.PageSize > 0
                     ? vehicleList.Skip(fsParametes.StartIndex).Take(fsParametes.PageSize).ToList() //Paging
                     : vehicleList.ToList();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userFleet"></param>
        /// <param name="merchantId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public int UserVehicleCount(FilterSearchParameters fsParametes)
        {
            try
            {
                var vehicleList = (from m in DbMtData.Vehicles
                                   join n in DbMtData.Fleets on m.fk_FleetID equals n.FleetID
                                   join owner in DbMtData.CarOwners on m.fk_CarOwenerId equals owner.Id into carlist
                                   from owner in carlist.DefaultIfEmpty()
                                   join k in DbMtData.Terminals on m.fk_TerminalID equals k.TerminalID into res
                                   from y1 in res.DefaultIfEmpty()
                                   where m.IsActive && n.fk_MerchantID == fsParametes.MerchantId
                                   select
                        new { m.VehicleID, m.VehicleNumber, m.VehicleRegNo, m.PlateNumber, n.FleetName, y1.SerialNo, n.fk_MerchantID, m.fk_FleetID, owner.OwnerName })
                   .AsEnumerable()
                   .Select
                   (objNew => new Vehicle
                   {
                       VehicleID = objNew.VehicleID,
                       VehicleNumber = objNew.VehicleNumber,
                       PlateNumber = objNew.PlateNumber,
                       FleetName = objNew.FleetName,
                       DeviceName = objNew.SerialNo,
                       VehicleRegNo = objNew.VehicleRegNo,
                       fk_FleetID = objNew.fk_FleetID,
                       CarOwnerName = objNew.OwnerName
                   });

                vehicleList = vehicleList.Where(x => fsParametes.UserFleet.Select(y => y.fk_FleetID).Contains(x.fk_FleetID)).ToList();
                if (fsParametes.LogOnByUserType == 5)
                {
                    List<int> fleetListAccessed = GetAccesibleFleet(fsParametes.LogOnByUserId, fsParametes.MerchantId);
                    List<Vehicle> fleetViewResult = vehicleList.Where(m => fleetListAccessed.Contains((int)m.fk_FleetID)).Select(m => m).Distinct().ToList();
                    vehicleList = fleetViewResult.AsEnumerable();
                }
                if (!string.IsNullOrEmpty(fsParametes.Name))
                {
                    vehicleList = vehicleList.Where(p => p.VehicleNumber.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                if (!string.IsNullOrEmpty(fsParametes.OwnerName))
                {
                    vehicleList = vehicleList.Where(x => x.CarOwnerName != null);
                    vehicleList = vehicleList.Where(p => p.CarOwnerName.IndexOf(fsParametes.OwnerName, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                if (!string.IsNullOrEmpty(fsParametes.SerialNumber))
                {
                    vehicleList = vehicleList.Where(x => x.DeviceName != null);
                    vehicleList = vehicleList.Where(p => p.DeviceName.IndexOf(fsParametes.SerialNumber, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                if (!string.IsNullOrEmpty(fsParametes.Name) && !string.IsNullOrEmpty(fsParametes.OwnerName) && !string.IsNullOrEmpty(fsParametes.SerialNumber))
                {
                    vehicleList = vehicleList.Where(p => p.VehicleNumber.IndexOf(fsParametes.Name, StringComparison.OrdinalIgnoreCase) >= 0 && p.CarOwnerName.IndexOf(fsParametes.OwnerName, StringComparison.OrdinalIgnoreCase) >= 0 && p.DeviceName.IndexOf(fsParametes.SerialNumber, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                }
                return vehicleList.Count();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CarOwner> CarOwnerList(int merchantId)
        {
            try
            {
                var carOwnerList = (from m in DbMtData.CarOwners where m.fk_MerchantId == merchantId && (bool)m.IsActive select new { m.Id, m.OwnerName, m.Contact, m.EmailAddress })
                        .AsEnumerable()
                        .Select
                        (objNew => new CarOwner
                        {
                            Id = objNew.Id,
                            OwnerName = objNew.OwnerName,
                            Contact = objNew.Contact,
                            EmailAddress = objNew.EmailAddress,
                            OwnerDetail = objNew.EmailAddress + ": " + objNew.OwnerName + ": " + objNew.Contact
                        });
                return carOwnerList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CarOwner> PayCarOwnerList(int FleetId)
        {
            try
            {
                var PayCarOwnerList = (from m in DbMtData.CarOwners join V in DbMtData.Vehicles on m.Id equals V.fk_CarOwenerId where V.fk_FleetID == FleetId && (bool)m.IsActive select new { m.Id, m.OwnerName })
                        .AsEnumerable()
                        .Select
                        (objNew => new CarOwner
                        {
                            Id = objNew.Id,
                            OwnerName = objNew.OwnerName
                        });
                return PayCarOwnerList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   to get the accessible fleets 
        /// Function Name       :   GetAccesibleFleet
        /// Created By          :   Naveen Kumar
        /// Created On          :   10/15/2015
        /// Modificatios Made   :   ****************************
        /// Modidified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public List<int> GetAccesibleFleet(int logOnByUserId, int merchantId)
        {
            List<int> accessibleFleets = (from m in DbMtData.UserFleets
                                          where m.fk_UserID == logOnByUserId && m.fk_MerchantId == merchantId
                                          && m.fk_UserTypeId == 5 && m.IsActive == true
                                          select (int)m.fk_FleetID).Distinct().ToList();
            return accessibleFleets;
        }

        public Vehicle VehicleListInFleet(string token, string carNumber, int fleetID)
        {
            Vehicle vehileList = new Vehicle();
            int? fleetId = 0;
            var dbDuplicate = DbMtData.Fleets.Where(x => x.DispatchFleetID == fleetID).FirstOrDefault();
            if (dbDuplicate != null)
            {
                fleetId = dbDuplicate.FleetID;
            }
            if (fleetId > 0)
            {
                vehileList = DbMtData.Vehicles.Where(x => x.VehicleNumber == carNumber && x.fk_FleetID == fleetId && x.IsActive).FirstOrDefault();
                vehileList.fk_FleetID = fleetID;
                return vehileList;
            }
            return vehileList;
        }

        public bool IsDuplicateCarNumber(int fleetId, string CarNumber)
        {
            var dbDuplicate = DbMtData.Fleets.Where(x => x.DispatchFleetID == fleetId).FirstOrDefault();
            if (dbDuplicate != null)
            {
                int fleetIds = dbDuplicate.FleetID;
                if (fleetIds > 0)
                {
                    return DbMtData.Vehicles.Any(x => x.fk_FleetID == fleetIds && x.VehicleNumber == CarNumber);
                }
            }

            return false;
        }

        public bool FindFleet(int fleetId)
        {
            return DbMtData.Fleets.Any(x => x.DispatchFleetID == fleetId);
        }

        public Vehicle AddDespetchVehicle(Vehicle vehicle, string token)
        {
            Vehicle returnVehicle = new Vehicle();
            try
            {
                int bkFleet = vehicle.fk_FleetID;
                int? fleetId = DbMtData.Fleets.Where(x => x.DispatchFleetID == vehicle.fk_FleetID).FirstOrDefault().FleetID;
                int vehId = vehicle.VehicleID;

                if (vehId > 0)
                {
                    Vehicle veh = DbMtData.Vehicles.Find(vehId);
                    if (veh != null)
                    {

                        if (veh.VehicleNumber != vehicle.VehicleNumber)
                        {
                            returnVehicle.VehicleNumber = vehicle.VehicleNumber;
                            veh.VehicleNumber = vehicle.VehicleNumber;
                        }
                        else
                            returnVehicle.VehicleNumber = string.Empty;

                        if (veh.VehicleRegNo != vehicle.VehicleRegNo)
                        {
                            returnVehicle.VehicleRegNo = vehicle.VehicleRegNo;
                            veh.VehicleRegNo = vehicle.VehicleRegNo;
                        }
                        else
                            returnVehicle.VehicleRegNo = string.Empty;
                        if (veh.PlateNumber != vehicle.PlateNumber)
                        {
                            returnVehicle.PlateNumber = vehicle.PlateNumber;
                            veh.PlateNumber = vehicle.PlateNumber;
                        }
                        else
                            returnVehicle.PlateNumber = string.Empty;
                        if (veh.IsActive != vehicle.IsActive)
                        {
                            returnVehicle.IsActive = vehicle.IsActive;
                            veh.IsActive = vehicle.IsActive;
                        }
                        DbMtData.CommitChanges();
                    }
                }
                else
                {
                    vehicle.fk_FleetID = Convert.ToInt32(fleetId);
                    vehicle.IsActive = true;
                    vehicle.CreatedDate = DateTime.Now;
                    DbMtData.Vehicles.Add(vehicle);
                    DbMtData.CommitChanges();
                    var terminal = (from m in DbMtData.Vehicles
                                    join n in DbMtData.Terminals on vehicle.fk_TerminalID equals n.TerminalID
                                    select n).FirstOrDefault();
                    if (terminal != null)
                    {
                        int terminalId = terminal.TerminalID;
                        Terminal terminalDb = DbMtData.Terminals.Find(terminalId);
                        terminalDb.IsTerminalAssigned = true;
                        DbMtData.CommitChanges();

                    }
                    vehicle.fk_FleetID = bkFleet;
                    return vehicle;
                }
            }
            catch
            {
                throw;
            }

            return returnVehicle;
        }

    }
}
