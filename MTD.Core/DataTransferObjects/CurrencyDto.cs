﻿namespace MTD.Core.DataTransferObjects
{
    public class CurrencyDto
    {
        public int CurrencyCodeID { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencyAbv { get; set; }
        public string CurrencySymbol { get; set; }
        public string currency
        {
            get
            {
                return CurrencyAbv + "(" + CurrencySymbol + ")";
            }
        }
    }
}
