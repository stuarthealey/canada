﻿namespace MTD.Core.DataTransferObjects
{
   public  class TerminalListDto
    {
        public int TerminalID { get; set; }
        public string MacAdd { get; set; }

    }
}
