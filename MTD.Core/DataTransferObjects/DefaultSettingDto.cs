﻿using System;

namespace MTD.Core.DataTransferObjects
{
    public class DefaultSettingDto
    {
        public int DefaultID { get; set; }
        public string DefaultName { get; set; }
        public Nullable<decimal> StateTaxRate { get; set; }
        public Nullable<decimal> FederalStateRate { get; set; }
        public Nullable<byte> SurchargeType { get; set; }
        public Nullable<decimal> SurchargeFixed { get; set; }
        public Nullable<decimal> SurchargePer { get; set; }
        public Nullable<decimal> SurchargeMaxCap { get; set; }
        public Nullable<byte> BookingFeeType { get; set; }
        public Nullable<decimal> BookingFeeFixed { get; set; }
        public Nullable<decimal> BookingFeePer { get; set; }
        public Nullable<decimal> BookingMaxCap { get; set; }
        public string TermCondition { get; set; }
        public string Disclaimer { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<decimal> TipLow { get; set; }
        public Nullable<decimal> TipMedium { get; set; }
        public Nullable<decimal> TipHigh { get; set; }
        public Nullable<byte> TechFeeType { get; set; }
        public Nullable<decimal> TechFeeFixed { get; set; }
        public Nullable<decimal> TechFeePer { get; set; }
        public Nullable<decimal> TechFeeMaxCap { get; set; }
        public Nullable<int> TransCountSlablt1 { get; set; }
        public Nullable<int> TransCountSlabGt2 { get; set; }
        public Nullable<int> TransCountSlablt2 { get; set; }
        public Nullable<int> TransCountSlabGt3 { get; set; }
        public Nullable<int> TransCountSlablt3 { get; set; }
        public Nullable<int> TransCountSlabGt4 { get; set; }
    }
}
