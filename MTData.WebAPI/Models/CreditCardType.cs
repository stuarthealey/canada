﻿using System;

namespace MTData.WebAPI.Models
{
    public static class CreditCardType
    {
        /// <summary>
        /// Changes done for EMV Contact certification of Canada
        /// Get card type based on the card number
        /// </summary>
        /// <param name="cardNumber"></param>
        /// <returns></returns>
        public static CardTypeType GetCardType(string cardNumber)
        {
            int diners = Convert.ToInt32(cardNumber.Substring(0, 7));
            int discover = Convert.ToInt32(cardNumber.Substring(0, 8));
            int jcb = Convert.ToInt32(cardNumber.Substring(0, 6));
            int masterCard = Convert.ToInt32(cardNumber.Substring(0, 6));

            CardTypeType result = CardTypeType.Visa;        

            //Set card type on the basis of Initial Digits of Card Number.
            if (cardNumber.StartsWith("4"))
            {
                result = CardTypeType.Visa;
            }
            else if (cardNumber.StartsWith("5"))
            {
                result = CardTypeType.MasterCard;
            }
            else if (masterCard >= 222100 && masterCard <= 272099) // modified by asif on 09/26/2016 for MasterCard 2'series bin
            {
                result = CardTypeType.MasterCard;
            }
            else if (cardNumber.StartsWith("67"))
            {
                result = CardTypeType.MasterCard;
            }
            else if (cardNumber.StartsWith("37") || cardNumber.StartsWith("34"))
            {
                result = CardTypeType.Amex;
            }
            else if ((diners >= 3600000 && diners <= 3699999) ||
                     (diners >= 3000000 && diners <= 3059999) ||
                     (diners >= 3095000 && diners <= 3095999) ||
                     (diners >= 3800000 && diners <= 3999999))
            {
                result = CardTypeType.Diners;
            }
            else if ((discover >= 60110000 && discover <= 60110399) ||
                     (discover >= 60110500 && discover <= 60110999) ||
                     (discover >= 60112000 && discover <= 60114999) ||
                     (discover >= 60117400 && discover <= 60117499) ||
                     (discover >= 60117700 && discover <= 60117999) ||
                     (discover >= 60118600 && discover <= 60119999) ||
                     (discover >= 62212600 && discover <= 62292599) ||
                     (discover >= 62400000 && discover <= 62699999) ||
                     (discover >= 62820000 && discover <= 62889999) ||
                     (discover >= 64400000 && discover <= 65059999) ||
                     (discover >= 65061100 && discover <= 65999999))
            {
                result = CardTypeType.Discover;
            }
            else if (jcb >= 352800 && jcb <= 358999)
            {
                result = CardTypeType.JCB;
            }

            return result;
        }
    }
}
