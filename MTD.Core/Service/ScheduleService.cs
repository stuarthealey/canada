﻿using System;
using System.Collections.Generic;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Extension;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Core.Service
{
    public class ScheduleService : IScheduleService
    {
        private readonly IScheduleRepository _scheduleRepository;

        public ScheduleService([Named("ScheduleRepository")] IScheduleRepository scheduleRepository)
        {
            _scheduleRepository = scheduleRepository;

        }

        /// <summary>
        /// Purpose             :   To Add Schedule Report
        /// Function Name       :   Add
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/23/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="scheduleDto"></param>
        public void Add(ScheduleReportDto scheduleDto)
        {
            try
            {
                ScheduleReport scheduleReport = scheduleDto.ToScheduleReport();
                _scheduleRepository.Add(scheduleReport);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Get Schedule 
        ///  Function Name       :   GetScheduleDto
        /// Created By          :   Madhuri Tanwar
        /// Created On          :   02/23/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public ScheduleReportDto GetScheduleDto(int scheduleId)
        {
            try
            {
                var scheduleReport = _scheduleRepository.GetScheduleReport(scheduleId);
                var scheduleReportDto = scheduleReport.ToScheduleReportDto();
                return scheduleReportDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update Schedule Report
        /// Function Name       :   GetMerchantList
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/24/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="scheduleDto"></param>
        public void Update(ScheduleReportDto scheduleDto)
        {
            try
            {
                ScheduleReport scheduleReport = scheduleDto.ToScheduleReport();
                _scheduleRepository.Update(scheduleReport);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update Recipient
        /// Function Name       :   UpdateRecipient
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/24/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="recipientDto"></param>
        /// <returns></returns>
        public bool UpdateRecipient(RecipientDto recipientDto)
        {
            try
            {
                RecipientMaster recipientMaster = recipientDto.ToRecipientMaster();
                return _scheduleRepository.UpdateRecipient(recipientMaster);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update AdminRecipient
        /// Function Name       :   UpdateAdminRecipient
        /// Created By          :   Ravit Chaudhary
        /// Created On          :   06/05/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="adminRecipientDto"></param>
        /// <returns></returns>
        public bool UpdateAdminRecipient(AdminRecipientListDto adminRecipientDto)
        {
            try
            {
                AdminRecipientList adminRecipientUser = adminRecipientDto.ToAdminRecipientMaster();
                return _scheduleRepository.UpdateAdminRecipient(adminRecipientUser);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Schedule Report List
        /// Function Name       :   UpdateAdminRecipient
        /// Created By          :   Naveen Kumar
        /// Created On          :   06/20/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ScheduleReportDto> ScheduleReportList()
        {
            try
            {
                var userList = _scheduleRepository.ScheduleReportList();
                var adminDtoList = userList.ToScheduleList();
                return adminDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Schedule Report List
        /// Function Name       :   ScheduleReportListByFilter
        /// Created By          :   Naveen Kumar
        /// Created On          :   06/20/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public IEnumerable<ScheduleReportDto> ScheduleReportListByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting)
        {
            try
            {
                var merchantList = _scheduleRepository.ScheduleReportListByFilter(name, jtStartIndex, jtPageSize, jtSorting);
                var merchantDtoList = merchantList.ToScheduleList();
                return merchantDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get Schedule Report List
        /// Function Name       :   ScheduleReportListByFilter
        /// Created By          :   Naveen Kumar
        /// Created On          :   06/20/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public IEnumerable<ScheduleReportDto> ScheduleReportListByFilter(FilterSearchParametersDto filterSearchParameterDto, string jtSorting)
        {
            try
            {
                var merchantList = _scheduleRepository.ScheduleReportListByFilter(filterSearchParameterDto.SearchParameters(), jtSorting); 
                var merchantDtoList = merchantList.ToScheduleList();
                return merchantDtoList;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To get ReportList
        /// Function Name       :   ReportList
        /// Created By          :   Umesh Kumar
        /// Created On          :   06/22/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ReportListDto> ReportList()
        {
            var reportList = _scheduleRepository.ReportList();
            var reportDtoList = reportList.ToReportList();
            return reportDtoList;
        }

        /// <summary>
        /// Purpose             :   To get ReportList
        /// Function Name       :   ReportList
        /// Created By          :   Umesh Kumar
        /// Created On          :   06/22/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ReportListDto> ReportList(int merchantId)
        {
            var reportList = _scheduleRepository.ReportList(merchantId);
            var reportDtoList = reportList.ToReportList();
            return reportDtoList;
        }

        /// <summary>
        /// Purpose             :   To Get Report List in ReadOnly mode
        /// Function Name       :   ReportListReadOnly
        /// Created By          :   Umesh Kumar
        /// Created On          :   06/22/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ReportListDto> ReportListReadOnly()
        {
            var reportList = _scheduleRepository.ReportListReadOnly();
            var reportDtoList = reportList.ToReportList();
            return reportDtoList;
        }
        
        public void Delete(int ScheduleID)
        {
            try
            {
                _scheduleRepository.Delete(ScheduleID);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recipientDto"></param>
        /// <param name="corporateUserId"></param>
        /// <param name="modifiedBy"></param>
        public void UpdateCorporateUserRecipient(RecipientDto recipientDto,int corporateUserId,string modifiedBy)
        {
            try
            {
                RecipientMaster recipientMaster = recipientDto.ToRecipientMaster();
                 _scheduleRepository.UpdateCorporateUserRecipient(recipientMaster,corporateUserId,modifiedBy);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scheduleDto"></param>
        public void AddCorporateUser(ScheduleReportDto scheduleDto)
        {
            try
            {
                ScheduleReport scheduleReport = scheduleDto.ToScheduleReport();
                _scheduleRepository.AddCorporateUser(scheduleReport);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scheduleDto"></param>
        public void UpdateCorporate(ScheduleReportDto scheduleDto)
        {
            try
            {
                ScheduleReport scheduleReport = scheduleDto.ToScheduleReport();
                _scheduleRepository.UpdateCorporate(scheduleReport);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="corporateId"></param>
        /// <returns></returns>
        public IEnumerable<ReportListDto> CorporateReportList(int corporateId)
        {
            var reportList = _scheduleRepository.CorporateReportList(corporateId);
            var reportDtoList = reportList.ToReportList();
            return reportDtoList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scheduleId"></param>
        public void DeleteCorporate(int scheduleId)
        {
            _scheduleRepository.DeleteCorporate(scheduleId);
        }


        /// <summary>
        /// Purpose             :   To get ReportList
        /// Function Name       :   ReportList
        /// Created By          :   Umesh Kumar
        /// Created On          :   06/22/2015
        /// Modifications Made  :   ****************************
        /// Modified On         :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ReportListDto> ReportList(int to, int from)
        {
            var reportList = _scheduleRepository.ReportList(to, from);
            var reportDtoList = reportList.ToReportList();
            return reportDtoList;
        }


        public IEnumerable<GetAdminWeeklySummaryCountDto> WeeklySummary(FilterSearchParametersDto filterSearchParameterDto)
        {
            try
            {
                var fs = filterSearchParameterDto.SearchParameters();
                var weeklyReport = _scheduleRepository.WeeklySummary(fs);
                var weekDtoList = weeklyReport.ToWeeklyList();
                return weekDtoList;
            }
            catch
            {
                throw;
            }

        }

        public int WeeklySummaryCount(FilterSearchParametersDto filterSearchParameterDto)
        {
            var fs = filterSearchParameterDto.SearchParameters();
            return _scheduleRepository.WeeklySummaryCount(fs);
        }

        public IEnumerable<WeeklyDollarVolumeDto> WeeklyDollarVolumeReport(FilterSearchParametersDto filterSearchParameterDto)
        {
            try
            {
                var fs = filterSearchParameterDto.SearchParameters();
                var weeklyReport = _scheduleRepository.WeeklyDollarVolumeReport(fs);
                var weekDtoList = weeklyReport.ToWeeklyDollarVolumeList();
                return weekDtoList;
            }
            catch
            {
                throw;
            }

        }

        public int WeeklyDollarVolumeReportCount(FilterSearchParametersDto filterSearchParameterDto)
        {
            var fs = filterSearchParameterDto.SearchParameters();
            return _scheduleRepository.WeeklyDollarVolumeReportCount(fs);
        }

    }
}
