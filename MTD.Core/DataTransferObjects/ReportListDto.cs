﻿namespace MTD.Core.DataTransferObjects
{
   public class ReportListDto
   {
       public int ReportId { get; set; }
       public string Name { get; set; }
       public string DisplayName { get; set; }
    }
}
