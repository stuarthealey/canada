﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    class Table33
    {
        #region Private Constants and Variables

        #region Constants
        private const string tblId = "|33|33";
        private const string smidCharOne = "F";//It is char one of SMID – Security Management Information Data.
        #endregion

        #region Variables
        //All these variables belongs to SMID – Security Management Information Data.
        private string baseDerivationKeyID = string.Empty;
        private string deviceID = string.Empty;
        private string transactionCounter = string.Empty;
        #endregion

        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'33 33' Table 33
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        /// <summary>
        /// It is the first field of Key Serial Number Data (or SMID – Security Management Information Data)
        /// It's length is 1 and value should be "F".
        /// </summary>
        public string SMIDCharOne
        {
            get { return smidCharOne; }
        }

        /// <summary>
        /// It is the second field of Key Serial Number Data (or SMID – Security Management Information Data)
        /// It's length is 9(If fewer than nine positions, pad on LEFT with ‘F’s.).
        /// </summary>
        public string BaseDerivationKeyID
        {
            get { return baseDerivationKeyID; }
            set { baseDerivationKeyID = value; }
        }

        /// <summary>
        /// It is the third field of Key Serial Number Data (or SMID – Security Management Information Data)
        /// It's length is 5.
        /// </summary>
        public string DeviceID
        {
            get { return deviceID; }
            set { deviceID = value; }
        }

        /// <summary>
        /// It is the fourth field of Key Serial Number Data (or SMID – Security Management Information Data)
        /// It's length is 5.
        /// </summary>
        public string TransactionCounter
        {
            get { return transactionCounter; }
            set { transactionCounter = value; }
        }
        #endregion
    }
}