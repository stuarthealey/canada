﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.Web.ViewData
{
    public class GetCardTypeReportViewData
    {
        public Nullable<long> SrNo { get; set; }
        public string MerchantName { get; set; }
        public string FleetName { get; set; }
        public Nullable<int> VISATxn { get; set; }
        public string VISA { get; set; }
        public Nullable<int> MasterTxn { get; set; }
        public string MasterCard { get; set; }
        public Nullable<int> AmexTxn { get; set; }
        public string AmericanExpress { get; set; }
        public Nullable<int> DisTxn { get; set; }
        public string Discover { get; set; }
        public Nullable<int> JCBTxn { get; set; }
        public string JCB { get; set; }
        public Nullable<int> DinerTxn { get; set; }
        public string DinersClub { get; set; }
    }
}