
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_AdminSummaryReportScheduler]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_AdminSummaryReportScheduler]
GO

/****** Object:  StoredProcedure [dbo].[usp_AdminSummaryReportScheduler]    Script Date: 11/2/2015 10:59:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <12-06-2015>
-- Description:	<Prepare and Send summary reports to admin users>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AdminSummaryReportScheduler]
AS
BEGIN TRY
 SET NOCOUNT ON;
 SET ANSI_WARNINGS OFF
 DECLARE @Res INT,
 @UserId INT,
 @To NVARCHAR(50),
 @Body NVARCHAR(MAX),
 @Error NVARCHAR(200),
 @MailId INT,
 @Valid INT,
 @MsgXml NVARCHAR(40),
 @XmlData NVARCHAR(MAX),
 @UserName NVARCHAR(45),
 @ReportType INT,
 @RepType NVARCHAR(50),
 @RepHeading NVARCHAR(200),
 @FDate DATETIME,
 @TDate DATETIME,
 @TotalTransaction INT,
 @TotalActualFare NVARCHAR(30),
 @TotalTips NVARCHAR(30),
 @TotalFareAmount NVARCHAR(30),
 @TotalSurcharge NVARCHAR(30),
 @TotalFee  NVARCHAR(30),
 @NoChargeBack INT,
 @ChargeBackValue NVARCHAR(25),
 @NoVoid INT,
 @VoidValue NVARCHAR(25),
 @AmountOwing  NVARCHAR(30),
 @TotalTechFee NVARCHAR(30)
 --Declaring cursor for fetching admin recipients one by one from database
 DECLARE Queue_Cursor CURSOR FOR 
 SELECT u.UserID,u.Email FROM Users u INNER JOIN Recipient r ON u.UserID=r.fk_UserId WHERE r.IsRecipient=1 AND r.fk_UserTypeId=1 
 AND  r.fk_ReportId=1 AND u.IsActive=1 AND u.fk_UserTypeID=1 
 OPEN Queue_Cursor
 FETCH NEXT FROM Queue_Cursor INTO @UserId,@To
 WHILE @@FETCH_STATUS = 0   
 BEGIN   
 SET @XmlData=''
 SELECT @UserName=FName FROM Users WHERE UserID=@UserId 
--Calling store procedure AdminSummaryReport to get the data in xml format
 EXEC usp_AdminSummaryReport @XmlData OUTPUT,
 @ReportType OUTPUT,@FDate OUTPUT,@TDate OUTPUT,
 @TotalTransaction OUTPUT, @TotalActualFare OUTPUT,
 @TotalTips OUTPUT,@TotalFareAmount OUTPUT,
 @TotalSurcharge OUTPUT,@TotalFee  OUTPUT,
 @NoChargeBack OUTPUT,@ChargeBackValue OUTPUT,
 @NoVoid OUTPUT,@VoidValue OUTPUT,@TotalTechFee OUTPUT,@AmountOwing OUTPUT
 SET  @RepType=CASE @ReportType WHEN 1 THEN 'Daily Summarized Transaction Report'
 WHEN 2 THEN 'Weekly Summarized Transaction Report' ELSE 'Monthly Summarized Transaction Report' END
 SET @RepHeading=@RepType+' '+'(Dated from'+' '+ CONVERT(VARCHAR(11),@FDate,106)+' '+'to'+' '+CONVERT(VARCHAR(11),@TDate,106)+')'
 IF(@XmlData IS NULL OR @XmlData='') BEGIN
 SET @Valid=0
 INSERT INTO MailMessage(RepUserId,[Status],SendTo,Body,RepUserTypeId,[Date],ReportType) VALUES(@UserId,-1,@To,'No records in database',1,GETDATE(),'AdminSummaryReport')
 END
 ELSE BEGIN
 SET @Valid=1
 END
 IF(@Valid=1) BEGIN
 SET @MsgXml=CAST((SELECT CONCAT('Dear',' ',@UserName,',') FOR XML PATH('p'), ELEMENTS ) AS NVARCHAR(MAX))
 --Set the xml data in the html format 
 SET @Body ='<html><style>td{text-align:left;font-family:verdana;font-size:12px}
 p{font-family:verdana;font-size:13px}span{font-family:verdana;font-size:13px}</style>
 <body><p>Listed below is the transaction report containing summarized details of transactions corresponding to all merchants.</p>
 <H5 style="color:grey;text-align:center;font-size:11px;font-family:verdana">'+@RepHeading+'</H5>
 <table border = 1 cellpadding="0" cellspacing="0" style="background-color:white;border-collapse:collapse"> <tr>
 <th style="color:grey;font-size:13px;width:90px;text-align:left">Sr.No</th>
 <th style="color:grey;font-size:13px;width:600px;text-align:left">Merchant</th>
 <th style="color:grey;font-size:13px;width:600px;text-align:left">Fleet</th>
 <th style="color:grey;font-size:13px;width:140px;text-align:left">No of Trans</th>
 <th style="color:grey;font-size:13px;width:150px;text-align:left">Fare Amt</th>
 <th style="color:grey;font-size:13px;width:100px;height:19px;left-align:center">Tips</th>
 <th style="color:grey;font-size:13px;width:120px;text-align:left">Surcharges</th>
 <th style="color:grey;font-size:13px;width:100px;text-align:left">Fees</th>
 <th style="color:grey;font-size:13px;width:100px;text-align:left">No Chargebacks</th>
 <th style="color:grey;font-size:13px;width:100px;text-align:left">Chargeback Value</th>
 <th style="color:grey;font-size:13px;width:130px;text-align:left">No of void</th>
 <th style="color:grey;font-size:13px;width:100px;text-align:left">void Value</th>
 <th style="color:grey;font-size:13px;width:150px;text-align:left">Total Amount</th>
 <th style="color:grey;font-size:13px;width:150px;text-align:left">Tech Fee</th>
 <th style="color:grey;font-size:13px;width:100px;text-align:left">Amount Owing</th></tr>'    
 SET @Body = @MsgXml+@Body + @XmlData +'<tfoot><tr style="height:25px;font-weight:bold">
 <td colspan=3 style="font-size:11px;text-align:center">Total</td>
 <td style="font-size:11px">' +CAST(@TotalTransaction AS NVARCHAR(50))+'</td>
 <td style="font-size:11px">'+@TotalActualFare+'</td>
 <td style="font-size:11px">'+@TotalTips+'</td>
 <td style="font-size:11px">'+@TotalSurcharge+'</td>
 <td style="font-size:11px">'+@TotalFee+'</td>
 <td style="font-size:11px">'+CAST(@NoChargeBack AS NVARCHAR(50))+'</td>
 <td style="font-size:11px">'+@ChargeBackValue+'</td>
  <td style="font-size:11px">'+CAST(@NoVoid AS NVARCHAR(50))+'</td>
 <td style="font-size:11px">'+@VoidValue+'</td>
 <td style="font-size:11px">'+@TotalFareAmount+'</td>
 <td style="font-size:11px">'+@TotalTechFee+'</td>
 <td style="font-size:11px">'+@AmountOwing+'</td>
 </tr></tfoot></table><br/><br/><span>Thanks & Regards,<br/><br/>MTData Developments Pty Ltd.<br/>www.mtdata.com<span>
 <h4 style="color:grey;font-size:12px;font-family:verdana">MTData-All rights reserved</h4>
 <p style="font-size:11px;color:black">Note:This is an automated message. Do not Reply or Reply All to this message
  as replies are not being accepted.</p></body></html>'
 INSERT INTO MailMessage(RepUserId,[Status],SendTo,Body,RepUserTypeId,[Date],ReportType) VALUES(@UserId,0,@To,@Body,1,GETDATE(),'AdminSummaryReport')
 SET @MailId=@@IDENTITY
 --Execute the sp_send_dbmail to send the summary report in html format 
 EXEC @Res=msdb.dbo.sp_send_dbmail
 @profile_name = 'MTData',
 @recipients=@To,
 @body =@Body,
 @subject = 'Admin Summarized Transaction Report',
 @body_format ='HTML'
 IF (@Res != 0)
 BEGIN
    SET @Error =@@ERROR
    UPDATE MailMessage SET Error=@Error,[Date]=GETDATE() WHERE MailId=@MailId
  END;
 ELSE
 BEGIN
    UPDATE MailMessage SET [Status]=1,[Date]=GETDATE() WHERE MailId=@MailId
 END
 END
  FETCH NEXT FROM Queue_Cursor INTO @UserId,@To
  END   
  CLOSE Queue_Cursor   
  DEALLOCATE Queue_Cursor
END TRY
--use catch to log error with details in ProcErrorHandler table
BEGIN CATCH
     INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
     SELECT ERROR_NUMBER()
	       ,ERROR_SEVERITY()
		   ,ERROR_STATE()
		   ,ERROR_PROCEDURE()
		   ,ERROR_LINE()
		   ,ERROR_MESSAGE()
		   ,SUSER_SNAME()
		   ,HOST_NAME()
		   ,GETDATE()
END CATCH
GO

