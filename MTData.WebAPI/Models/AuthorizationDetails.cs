﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    [XmlSerializerFormat]
    public class AuthorizationDetails
    {

        private string _type;

        [DataMember]
        public string PaymentType
        {
            get { return string.IsNullOrEmpty(this._type) ? string.Empty : this._type; }
            set { this._type = value; }
        }


        private string _cType;

        [DataMember]
        public string CardType
        {
            get { return string.IsNullOrEmpty(this._cType) ? string.Empty : this._cType; }
            set { this._cType = value; }
        }

        private string _mode;

        [DataMember]
        public string EntryMode
        {
            get { return string.IsNullOrEmpty(this._mode) ? string.Empty : this._mode; }
            set { this._mode = value; }
        }

        private string _transType;

        [DataMember]
        public string TransactionType
        {
            get { return string.IsNullOrEmpty(this._transType) ? string.Empty : this._transType; }
            set { this._transType = value; }
        }

        private string _transAmount;

        [DataMember]
        public string TransactionAmount
        {
            get { return string.IsNullOrEmpty(this._transAmount) ? string.Empty : this._transAmount; }
            set { this._transAmount = value; }
        }

        private string _industry;

        [DataMember]
        public string IndustryType
        {
            get { return string.IsNullOrEmpty(this._industry) ? string.Empty : this._industry; }
            set { this._industry = value; }
        }

        private string _driver;

        [DataMember]
        public string DriverNumber
        {
            get { return string.IsNullOrEmpty(this._driver) ? string.Empty : this._driver; }
            set { this._driver = value; }
        }

        private string _vehicle;

        [DataMember]
        public string VehicleNumber
        {
            get { return string.IsNullOrEmpty(this._vehicle) ? string.Empty : this._vehicle; }
            set { this._vehicle = value; }
        }

        private string _created;

        [DataMember]
        public string CreatedBy
        {
            get { return string.IsNullOrEmpty(this._created) ? string.Empty : this._created; }
            set { this._created = value; }
        }

        private string _address;

        [DataMember]
        public string StreetAddress
        {
            get { return string.IsNullOrEmpty(this._address) ? string.Empty : this._address; }
            set { this._address = value; }
        }

       

        private string _authcode;

        [DataMember]
        public string AuthId
        {
            get { return string.IsNullOrEmpty(this._authcode) ? string.Empty : this._authcode; }
            set { this._authcode = value; }
        }

        private string _transId;

        [DataMember]
        public string TransactionId
        {
            get { return string.IsNullOrEmpty(this._transId) ? string.Empty : this._transId; }
            set { this._transId = value; }
        }

        private string _rescode;

        [DataMember]
        public string RespCode
        {
            get { return string.IsNullOrEmpty(this._rescode) ? string.Empty : this._rescode; }
            set { this._rescode = value; }
        }

      
        private string _ccnumber;

        [DataMember]
        public string CardNumber
        {
            get { return string.IsNullOrEmpty(this._ccnumber) ? string.Empty : this._ccnumber; }
            set { this._ccnumber = value; }
        }     
    }
}