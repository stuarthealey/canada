﻿using System.Collections.Generic;
using MTD.Data.Data;

namespace MTD.Data.Repository.Interface
{
    public interface IPrefixRepository
    {
        bool Add(CCPrefix prefix);
        IEnumerable<CCPrefix> prefixList(int merchantId, string name);
        void Update(CCPrefix prefix, int prefixId);
        CCPrefix GetPrefix(int prefixId);
        void DeletePrefix(int prefixId);
         IEnumerable<CCPrefix> PrefixListByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting,int merchantId);
    }
}
