﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.Models
{
    public static class ServiceURL
    {
        public static string SrsServiceUrl { get; set; }
        public static string RcSoapServiceUrl { get; set; }
    }
}