﻿using System.Runtime.Serialization;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    public class LoginTaxes
    {
        //[DataMember(EmitDefaultValue = false)]
        //public decimal? StateTax{get;set;}

        private decimal? stateTax;

        [DataMember]
        public decimal? StateTax
        {
            get { return this.stateTax ?? default(decimal); }
            set { this.stateTax = value; }
        }

        //[DataMember(EmitDefaultValue = false)]
        //public decimal? FederalTax{get;set;}

        private decimal? federalTax;

        [DataMember]
        public decimal? FederalTax
        {
            get { return this.federalTax ?? default(decimal); }
            set { this.federalTax = value; }
        }
    }
}