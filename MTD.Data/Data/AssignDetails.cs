﻿namespace MTD.Data.Data
{
    public static class AssignDetails
    {
        public static SessionMerchantUser AssignProperties(Gateway gateway, Merchant_Gateway merchantGateway,
            Terminal terminal, Merchant merchant, User user)
        {
            var merchantDetails = new SessionMerchantUser();
            var md = new MerchantDetails();
            if (gateway != null)
            {
                md.GatewayId = gateway.GatewayID;
                md.Name = gateway.Name;
                md.IsActive = gateway.IsActive;
                md.GatewayUrl = gateway.GatewayUrl;
                md.GatewayOffset = gateway.GatewayOffset;
                md.IsSaleSupported = gateway.IsSaleSupported;
                md.IsPreauthSupported = gateway.IsPreauthSupported;
                md.IsCaptureSupported = gateway.IsCaptureSupported;
                md.IsCancelSupported = gateway.IsCancelSupported;
                md.IsRefundSupported = gateway.IsRefundSupported;
                md.IsDefault = gateway.IsDefault;
                md.GatewayUserName = gateway.UserName;
                md.GatewayPCode = gateway.PCode;
                md.GatewayCurrency = gateway.GatewayCurrency;
            }

            if (merchantGateway != null)
            {
                md.MerchantGatewayID = merchantGateway.MerchantGatewayID;
                md.fk_MerchantID = merchantGateway.fk_MerchantID;
                md.fk_GatewayID = merchantGateway.fk_GatewayID;
                md.MerchantGatewayUserName = merchantGateway.UserName;
                md.MerchantGatewayPCode = merchantGateway.PCode;
                md.MerchantGatewayIsActive = merchantGateway.IsActive;
                md.MerchantGatewayName = merchantGateway.Name;
                md.MerchantGatewayCompany = merchantGateway.Company;
                md.IsMtdataGateway = merchantGateway.IsMtdataGateway;
                md.Type = merchantGateway.Type;
            }

            if (terminal != null)
            {
                md.TerminalId = terminal.TerminalID;
                md.SerialNo = terminal.SerialNo;
                md.DeviceName = terminal.DeviceName;
                md.MacAdd = terminal.MacAdd;
                md.SoftVersion = terminal.SoftVersion;
                md.Description = terminal.Description;
                md.fk_MerchantId = terminal.fk_MerchantID;
                md.TerminalIsActive = terminal.IsActive;
                md.IsTerminalAssigned = terminal.IsTerminalAssigned;
            }

            if (merchant != null)
            {
                md.MerchantID = merchant.MerchantID;
                md.Company = merchant.Company;
                md.SetupFee = merchant.SetupFee;
                md.MonthlyFee = merchant.MonthlyFee;
                md.Disclaimer = merchant.Disclaimer;
                md.TermCondition = merchant.TermCondition;
                md.StateTaxRate = merchant.StateTaxRate;
                md.FederalTaxRate = merchant.FederalTaxRate;
                md.TipPerLow = merchant.TipPerLow;
                md.TipPerMedium = merchant.TipPerMedium;
                md.TipPerHigh = merchant.TipPerHigh;
                md.MerchantIsActive = merchant.IsActive;
                md.IsAllowSale = merchant.IsAllowSale;
                md.IsAllowPreauth = merchant.IsAllowPreauth;
                md.IsAllowCapture = merchant.IsAllowCapture;
                md.IsAllowRefund = merchant.IsAllowRefund;
                md.IsAllowVoid = merchant.IsAllowVoid;
                md.Surcharge =(decimal) 12.3;//hard code
                md.IsTaxInclusive =(bool) merchant.IsTaxInclusive;
                md.CompanyTaxNumber = merchant.CompanyTaxNumber;
                md.UseTransArmor = merchant.UseTransArmor.HasValue ? merchant.UseTransArmor.Value : false;      //PAY-13
            }

            merchantDetails.SessionUser = user;
            merchantDetails.SessionMerchant = md;
            return merchantDetails;
        }
    }
}
