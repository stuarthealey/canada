﻿using System;

namespace MTData.Web.ViewData
{
    public class FDMerchantViewData
    {
        public int FDId { get; set; }      
        public string FDMerchantID { get; set; }
        public string GroupId { get; set; }
        public string ServiceId { get; set; }
        public string ProjectId { get; set; }
        public string App { get; set; }
        public string ServiceUrl { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsAssigned { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<byte> ProjectType { get; set; }
        public string EcommProjectId { get; set; }
        public string TokenType { get; set; }
        public string MCCode { get; set; }
        public string EncryptionKey { get; set; }
    }
}