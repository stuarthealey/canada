
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MerchantSummaryReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_MerchantSummaryReport]
GO

/****** Object:  StoredProcedure [dbo].[usp_MerchantSummaryReport]    Script Date: 06/01/2015 10:59:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Naveen Kumar>
-- Create date: <08-06-2015>
-- Description:	<Prepare transaction summary reports for merchants>
-- =============================================
CREATE PROCEDURE [dbo].[usp_MerchantSummaryReport]
( 
  @MerchantId INT,
  @ReportId INT,
  @XmlData NVARCHAR(MAX) OUT,
  @ReportType INT OUT,
  @FDate DATE OUT,
  @TDate DATE OUT,
  @TotalTransaction INT OUT,
  @TotalActualFare NVARCHAR(35) OUT,
  @TotalTips NVARCHAR(35) OUT,
  @TotalFareAmount NVARCHAR(35) OUT,
  @TotalSurcharge NVARCHAR(35) OUT,
  @TotalFee NVARCHAR(35) OUT,
  @NoChargeBack INT OUT,
  @ChargeBackValue NVARCHAR(35) OUT,
  @NoVoid INT OUT,
  @VoidValue NVARCHAR(35) OUT,
  @TotalTechFee NVARCHAR(35) OUT,
  @AmountOwing  NVARCHAR(35) OUT
)
AS
BEGIN TRY
    SET ANSI_WARNINGS OFF
    SET NOCOUNT ON;
	DECLARE @NextScheduleDateTime DATETIME,
	        @ScheduleId INT
    DECLARE  @Frequency INT,
             @FrequencyType INT,
             @DateFrom DATE,
             @DateTo DATE
     DECLARE @InnerTable TABLE
      (
        Id INT, 
	    FareValue MONEY,
	    Tip MONEY, 
	    Amount MONEY, 
	    Surcharge MONEY, 
	    Fee MONEY,
	    VehicleNo NVARCHAR(70), 
	    TxnType NVARCHAR(25),
	    ResponseCode NVARCHAR(30),
	    fk_FleetId INT,
		IsRefunded BIT,
		IsVoided BIT,
		IsCompleted BIT,
		TechFee MONEY,
		FleetFee MONEY
      )  
     DECLARE @OuterTable TABLE
      (
         Id  INT, 
         FareValue MONEY, 
         Tip MONEY,
         Amount MONEY,
         Surcharge MONEY, 
         Fee MONEY, 
         TxnType NVARCHAR(25),
         ResponseCode NVARCHAR(30),
	     fk_FleetId INT,
		 TechFee MONEY,
		 IsRefunded BIT,
		 IsVoided BIT,
		 IsCompleted BIT,
		 FleetFee MONEY	
       )   
	 IF(@ReportId=1)
	 BEGIN
	 SELECT  @ScheduleId=ScheduleID,
	         @Frequency=Frequency,
             @FrequencyType=FrequencyType,
			 @NextScheduleDateTime=NextScheduleDateTime		 
     FROM ScheduleReport 
     WHERE fk_ReportId=1 AND IsCreated=1 AND fk_MerchantId IS NULL
	 END
	 IF(@ReportId IS NULL)
	 BEGIN
     SELECT  @ScheduleId=ScheduleID,
	         @Frequency=Frequency,
             @FrequencyType=FrequencyType,
			 @NextScheduleDateTime=NextScheduleDateTime		 
     FROM ScheduleReport 
     WHERE fk_ReportId=1 AND IsCreated=1 AND fk_MerchantId=@MerchantId
	 END
     IF(@Frequency=1)
     BEGIN
        SET @DateFrom=GETDATE()-@FrequencyType
        SET @DateTo=GETDATE()
		SET @NextScheduleDateTime=@NextScheduleDateTime+@FrequencyType
     END
     ELSE IF(@Frequency=2)
     BEGIN
        SET @DateFrom = DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(WK,-@FrequencyType,GETDATE())), 0)-1
        SET @DateTo= DATEADD(WEEK, DATEDIFF(WEEK, 0, GETDATE()), 0)-1
		SET @NextScheduleDateTime=DATEADD(WK,@FrequencyType,@NextScheduleDateTime)
     END
     ELSE IF(@Frequency=3)
     BEGIN
        SET @DateFrom =DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH,-@FrequencyType,GETDATE())), 0)
        SET @DateTo=DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) 
		SET @NextScheduleDateTime=DATEADD(MONTH,@FrequencyType,@NextScheduleDateTime)
     END  
	 ELSE
	 BEGIN
	    RETURN(-1) 
	 END 
	 IF(@ReportId IS NULL)
	 BEGIN
	    UPDATE ScheduleReport SET NextScheduleDateTime=@NextScheduleDateTime WHERE ScheduleID=@ScheduleId
	 END
    --Insert data into table variable InnnerTable for reuse                      
     INSERT INTO @InnerTable(Id,FareValue,Tip,Amount,Surcharge,Fee,VehicleNo,TxnType,ResponseCode,fk_FleetId,IsRefunded,IsVoided,IsCompleted,TechFee,FleetFee)                   
     SELECT     mt.Id
               ,mt.FareValue
		       ,mt.Tip
		       ,mt.Amount
		       ,mt.Surcharge
		       ,mt.Fee
		       ,mt.VehicleNo
		       ,mt.TxnType
		       ,mt.ResponseCode
		       ,mt.fk_FleetId 
			   ,mt.IsRefunded
			   ,mt.IsVoided
			   ,mt.IsCompleted
			   ,mt.TechFee
			   ,mt.FleetFee
      FROM 
	    (
		 SELECT Id,FareValue,Tip,Amount,Surcharge,Fee,VehicleNo,TxnType,ResponseCode,fk_FleetId,TxnDate,IsRefunded,IsVoided,IsCompleted,TechFee,FleetFee
		 FROM MTDTransaction
	    ) mt
       JOIN 
	    (
		  SELECT VehicleNumber,fk_FleetID FROM Vehicle WHERE IsActive=1
		) vh 
     ON mt.VehicleNo=vh.VehicleNumber 
     WHERE CAST(mt.TxnDate AS DATE)>=@DateFrom 
	       AND CAST(mt.TxnDate AS DATE)<@DateTo    
           AND mt.fk_FleetId=vh.fk_FleetID
     --Insert data into table variable OuterTable for reuse    
     INSERT INTO @OuterTable(Id,FareValue,Tip,Amount,Surcharge,Fee,TxnType,ResponseCode,fk_FleetId,TechFee,IsRefunded,IsVoided,IsCompleted,FleetFee) 
     SELECT    t.Id
              ,t.FareValue
		      ,t.Tip
			  ,t.Amount
		      ,t.Surcharge
		      ,t.Fee
		      ,t.TxnType
		      ,t.ResponseCode
		      ,t.fk_FleetId
			  ,t.TechFee
			  ,t.IsRefunded
			  ,t.IsVoided
			  ,t.IsCompleted
			  ,t.FleetFee
     FROM  MTDTransaction t 
     WHERE t.MerchantID=@MerchantId 
     AND CAST(t.TxnDate AS DATE)>=@DateFrom 
	 AND CAST(t.TxnDate AS DATE)<@DateTo   
     --Prepare data for summary report in xml format                            
     SET @XmlData=CAST((SELECT   ROW_NUMBER() OVER (ORDER BY FleetName) AS 'td',''
                                ,ISNULL(v.FleetName,'')  AS 'td',''
							    ,ISNULL(v.VehicleNumber,'')  AS 'td',''
							    ,(SELECT ISNULL(COUNT(Id),0) FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID  AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND it.IsVoided=0 AND it.ResponseCode IN ('000','002')) AS 'td',''
							    ,CONCAT('$',(SELECT ISNULL(SUM(FareValue),0) FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002'))) AS 'td',''
                                ,CONCAT('$',(SELECT ISNULL(SUM(Tip),0) FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND  it.ResponseCode IN ('000','002'))) AS 'td',''
								,CONCAT('$',(SELECT ISNULL(SUM(surcharge),0)FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002'))) AS 'td',''						
								,CONCAT('$',(SELECT ISNULL(SUM(TechFee),0)FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002'))) AS 'td',''	              
							    ,(SELECT ISNULL(COUNT(id),0) FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID   AND it.TxnType='Refund' AND it.ResponseCode='000') AS 'td',''
							    ,CONCAT('$',(SELECT ISNULL(SUM(Amount),0) FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID   AND it.TxnType='Refund' AND it.ResponseCode='000')) AS 'td',''
							    ,(SELECT ISNULL(COUNT(id),0) FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID  AND it.TxnType='Void' AND it.ResponseCode='000') AS 'td',''
							    ,CONCAT('$',(SELECT ISNULL(SUM(Amount),0) FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID   AND it.TxnType='Void' AND it.ResponseCode='000')) AS 'td',''
								,CONCAT('$',(SELECT ISNULL(SUM(Amount),0) FROM  @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND IT.IsVoided=0 AND it.ResponseCode IN ('000','002'))) AS 'td',''  
								,CONCAT('$',(SELECT ISNULL(SUM(Fee),0)+ISNULL(SUM(FleetFee),0) FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002'))) AS 'td',''	              
							    ,CONCAT('$',(SELECT ISNULL(SUM(Amount),0) FROM  @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID AND it.TxnType IN ('Sale','Completion') AND it.IsRefunded=0 AND IT.IsVoided=0 AND it.ResponseCode IN ('000','002'))-
								 (SELECT ISNULL(SUM(Fee),0)+ISNULL(SUM(FleetFee),0) FROM @InnerTable it WHERE  it.VehicleNo=v.VehicleNumber AND it.fk_FleetId=v.fk_FleetID AND ((it.TxnType='Sale' AND it.IsRefunded=0 AND it.IsVoided=0) OR (it.TxnType='Authorization' AND it.IsCompleted=1)) AND it.ResponseCode IN ('000','002'))) AS 'td'
                        FROM MTDTransaction m 
					     INNER JOIN 
						    (
							 SELECT v.VehicleNumber AS 'VehicleNumber'
							         ,v.fk_FleetID AS 'fk_FleetID'
									 ,f.FleetID AS 'FleetID'
									 ,f.FleetName AS 'FleetName'
									 ,mer.Company AS 'Company' 									
							  FROM 
							    (
								 SELECT VehicleNumber,fk_FleetID FROM Vehicle WHERE IsActive=1
							    ) v  
							  INNER JOIN 
							    (
								 SELECT FleetID,FleetName,fk_MerchantID FROM Fleet WHERE IsActive=1
							    ) f
							    ON v.fk_FleetID=f.FleetID
							  INNER JOIN
							    (
								 SELECT MerchantID,Company FROM Merchant WHERE IsActive=1
							    ) mer
                                ON f.fk_MerchantID=mer.MerchantID WHERE mer.MerchantID=@MerchantId
						   ) v
						   ON m.VehicleNo=v.VehicleNumber 
						   WHERE m.ResponseCode IN ('000','002') 
						   AND CAST(m.TxnDate AS DATE)>=@DateFrom 
						   AND CAST(m.TxnDate AS DATE)<@DateTo 
						   AND m.fk_FleetId=v.fk_FleetID
						   AND m.Id NOT IN (SELECT Id FROM dbo.MTDTransaction WHERE m.TxnType='Authorization' AND m.IsCompleted=0)
			            GROUP BY v.FleetName,v.FleetID,v.VehicleNumber,v.Company,v.fk_FleetID                        					   					        											    
					    ORDER BY FleetName
                        FOR XML PATH('tr'), ELEMENTS ) AS NVARCHAR(MAX))
     SELECT  @TotalTransaction=ISNULL(COUNT(ot.Id),0) 
     FROM    @OuterTable ot 
     WHERE   ot.TxnType IN ('Sale','Completion') 
	         AND ot.ResponseCode IN ('000','002')
			 AND ot.IsRefunded=0 
			 AND ot.IsVoided=0
     SELECT  @TotalActualFare=CONCAT('$',CAST(ISNULL(SUM(ot.FareValue),0) AS DECIMAL(16,2)))
            ,@totalTips=CONCAT('$',CAST(ISNULL(SUM(ot.Tip),0) AS DECIMAL(16,2)))
		    ,@TotalSurcharge=CONCAT('$',CAST(ISNULL(SUM(ot.Surcharge),0) AS DECIMAL(16,2)))
		    ,@TotalFee=CAST(ISNULL(SUM(ot.Fee),0)+ISNULL(SUM(ot.FleetFee),0) AS DECIMAL(16,2))
			,@TotalTechFee=CAST(ISNULL(SUM(ot.TechFee),0) AS DECIMAL(16,2))			 
     FROM    @OuterTable ot 
     WHERE   ot.ResponseCode IN ('000','002') 
	         AND ((ot.TxnType='Sale' AND ot.IsRefunded=0 AND ot.IsVoided=0) 
			 OR (ot.TxnType='Authorization' AND ot.IsCompleted=1))  
	 SELECT  @TotalFareAmount=CONCAT('$',CAST(ISNULL(SUM(ot.Amount),0) AS DECIMAL(16,2))),
	         @AmountOwing=CONCAT('$',CAST(ISNULL(SUM(ot.Amount),0) AS DECIMAL(18,2))-CAST(@TotalFee AS DECIMAL(18,2)))
     FROM    @OuterTable ot
	 WHERE   ot.ResponseCode IN ('000','002') AND ot.TxnType IN ('Sale','Completion') AND ot.IsRefunded=0 AND ot.IsVoided=0 
     SELECT  @NoChargeBack=ISNULL(COUNT(ot.Id),0)
            ,@ChargeBackValue=CONCAT('$',CAST(ISNULL(SUM(ot.Amount),0) AS DECIMAL(18,2))) 
     FROM    @OuterTable ot 
     WHERE   ot.TxnType ='Refund'AND ot.ResponseCode='000'
     SELECT  @NoVoid=ISNULL(COUNT(Id),0)
            ,@VoidValue=CONCAT('$',CAST(ISNULL(SUM(Amount),0) AS DECIMAL(18,2))) 
     FROM    @OuterTable ot 
     WHERE   ot.TxnType ='Void'AND ot.ResponseCode='000'
     SET     @ReportType=@Frequency
     SET     @FDate=@DateFrom
     SET     @TDate= DATEADD(DAY,-1,@DateTo)
	 SET     @TotalTechFee=CONCAT('$',@TotalTechFee)
     SELECT  @XmlData
	        ,@ReportType
            ,@FDate
			,@TDate
	        ,@TotalTransaction
	        ,@TotalActualFare
	        ,@TotalTips
	        ,@TotalFareAmount
	        ,@TotalSurcharge
	        ,@TotalFee
	        ,@NoChargeBack
	        ,@ChargeBackValue
	        ,@NoVoid
	        ,@VoidValue
	        ,@AmountOwing
			,@TotalTechFee
END TRY
--use catch to log error with details in ProcErrorHandler table
BEGIN CATCH
     INSERT INTO ProcErrorHandler(ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,UserName,HostName,[TimeStamp])
     SELECT ERROR_NUMBER()
	       ,ERROR_SEVERITY()
		   ,ERROR_STATE()
		   ,ERROR_PROCEDURE()
		   ,ERROR_LINE()
		   ,ERROR_MESSAGE()
		   ,SUSER_SNAME()
		   ,HOST_NAME()
		   ,GETDATE()
END CATCH
GO

