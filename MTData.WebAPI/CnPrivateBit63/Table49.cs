﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table49
    {
        #region Private Constants and Variables
        #region Constants
        private const string tblId = "|34|39";
        #endregion
        #region Variables
        private string defaultPresenceIndicator = "|31";//1-Value is present. 0-Value is not present.
        private string defaultCardCodeValue = "|20|20|20|20";//if PresenceIndicator=1 then CVV should be assigned.
        #endregion
        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'34 39' in Request
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        /// <summary>
        /// Presence Indicator
        /// used to indicate the existence of the Card Code Value
        /// </summary>
        public string PresenceIndicator
        {
            get { return defaultPresenceIndicator; }
            set { defaultPresenceIndicator = value; }
        }

        /// <summary>
        /// Card Code Value
        /// Used to get and set the Card Code Value
        /// </summary>
        public string CardCodeValue
        {
            get { return defaultCardCodeValue; }
            set { defaultCardCodeValue = value; }
        }
        #endregion
    }
}