//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MTD.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class CCPrefix
    {
        public int PrefixID { get; set; }
        public int CCPrefix1 { get; set; }
        public int fk_GatewayID { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string Name { get; set; }
        
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> fk_MerchantId { get; set; }
    
        public virtual Gateway Gateway { get; set; }
        public virtual Merchant Merchant { get; set; }
    }
}
