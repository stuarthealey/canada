﻿using System.Collections.Generic;
using MTD.Core.DataTransferObjects;

namespace MTD.Core.Service.Interface
{
    public interface ITaxService
    {
        bool Add(TaxDto taxDto);
        
        void Update(TaxDto taxDto, int merchantId);
        
        IEnumerable<TaxDto> GetTaxDtoList();
        
        TaxDto GetTaxDto(int merchantId);
        
        void DeleteTax(int merchantId);
        
        DefaultTaxDto GetDefaultTaxDto();
    
    }
}
