﻿using MTD.Core.DataTransferObjects;

namespace MTD.Core.Service.Interface
{
    public interface IDefaultService
    {
        void Update(DefaultSettingDto defaultDto, int defaultId);

        DefaultSettingDto GetDefaultDto(int defaultId);

        TransactionCountSlabDto GetTransactionCountSlab(int merchantId);

        TransactionCountSlabDto GetTransactionCountSlabCorp(int userId);

        void Update(TransactionCountSlabDto slabDto, int merchantId);

        void UpdateCorp(TransactionCountSlabDto slabDto, int userId);
    }
}
