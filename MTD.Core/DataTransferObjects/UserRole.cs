﻿using System;
using MTD.Data.Data;

namespace MTD.Core.DataTransferObjects
{
    public class UserRole
    {
        public int UserRoleID { get; set; }
        public int fk_UserID { get; set; }
        public int fk_RoleID { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public virtual Data.Data.Role Role { get; set; }
        public virtual User User { get; set; }
    }
}