﻿namespace MTData.Web.ViewData
{
    public class ReportListViewData
    {
        public int ReportId { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
    }
}