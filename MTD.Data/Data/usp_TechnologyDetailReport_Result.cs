//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MTD.Data.Data
{
    using System;
    
    public partial class usp_TechnologyDetailReport_Result
    {
        public string Merchant { get; set; }
        public Nullable<int> fk_FleetId { get; set; }
        public string FleetName { get; set; }
        public Nullable<byte> PayType { get; set; }
        public int TransId { get; set; }
        public Nullable<System.DateTime> TxnDate { get; set; }
        public string StringDateTime { get; set; }
        public string VehicleNo { get; set; }
        public string DriverNo { get; set; }
        public string TxnType { get; set; }
        public string Amount { get; set; }
        public string CardType { get; set; }
        public string TechFee { get; set; }
        public string FleetFee { get; set; }
        public string Surcharge { get; set; }
        public string Booking_Fee { get; set; }
        public string AuthId { get; set; }
    }
}
