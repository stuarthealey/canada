﻿namespace MTData.Web.ViewData
{
    public class StateViewData
    {
        public string StatesID { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public string fk_CountryCodesID { get; set; }
    }
}