﻿using System;
namespace MTData.Web.ViewData
{
    public class UserFleetViewData
    {
    
        public int fk_FleetID { get; set; }
        public int fk_UserID { get; set; }
        public Nullable<int> fk_MerchantId { get; set; }
        public Nullable<int> fk_UserTypeId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string FleetName { get; set; }
    }
}