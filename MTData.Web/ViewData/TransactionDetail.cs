﻿namespace MTData.Web.ViewData
{
    public class TransactionDetail
    {
        public string PaymentType { get; set; }
        public string TransNote { get; set; }
        public string TerminalId{ get; set; }
        public string CardType { get; set; }
        public string EntryMode { get; set; }
        public string CardNumber { get; set; }
        public string ExpiryDate { get; set; }
        public string CcvData { get; set; }
        public string TransactionType { get; set; }
        public string TransactionAmount { get; set; }
        public string IndustryType { get; set; }
        public string VehicleNumber { get; set; }
        public string RapidConnectAuthId { get; set; }
        public string CreatedBy { get; set; }
        public string StreetAddress { get; set; }
        public string PickUpLat { get; set; }
        public string PickUpLng { get; set; }
        public string PickUpAdd { get; set; }
        public string DropOffLat { get; set; }
        public string DropOffLng { get; set; }
        public string DropOffAdd { get; set; }
        public string ZipCode { get; set; }
        public string DeviceId { get; set; }
        public string DriverNumber { get; set; }
        public string JobNumber { get; set; }
        public string FirstFourDigits { get; set; }
        public string LastFourDigits { get; set; }
        public string Fare { get; set; }
        public string Tip { get; set; }
        public string Taxes { get; set; }
        public string Surcharge { get; set; }
        public string Fee { get; set; }
        public string Tolls { get; set; }
        public string FlagFall { get; set; }
        public string Extras { get; set; }
        public string GateFee { get; set; }
        public string Others { get; set; }
        public string Track1Data { get; set; }
        public string Track2Data { get; set; }
        public string Track3Data { get; set; }
        public string KeyId { get; set; }
        public string CurrencyCode { get; set; }
        public string PinData { get; set; }
        public string FleetId { get; set; }
        public string EmvData { get; set; }
        public bool EmvRefundVoid { get; set; } //to identify emv refund/void request from portal
    }
}