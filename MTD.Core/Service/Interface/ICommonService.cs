﻿using System;
using System.Collections.Generic;

using MTD.Core.DataTransferObjects;

namespace MTD.Core.Service.Interface
{
    public interface ICommonService
    {
        IEnumerable<CountryCodeDto> GetCountryList();

        IEnumerable<CountryStateDto> GetStateList(string stateId, string mtch);

        IEnumerable<CityDto> GetCityList(string mtch);

        IEnumerable<FleetListDto> GetFleetList(int merchantId);

        IEnumerable<TerminalListDto> GetTerminalList(int merchantId);

        IEnumerable<MerchantListDto> GetMerchantList();

        IEnumerable<GatewayListDto> GetGatewayList();

        bool IsExist(string emailId);

        IEnumerable<MerchantListDto> GetTaxMerchantList();

        TCDto GetTermDto(int termId);

        bool IsLockedMember(int id, bool status);

        IEnumerable<MerchantListDto> GetRecipientList();

        IEnumerable<RapidConnectTerminalDto> GetRegisteredTerminalList(int merchantId); 

        IEnumerable<MerchantListDto> GetRecipientList(int reportId, int userType); 

        IEnumerable<MerchantListDto> GetRecipientListDistinct();

        IEnumerable<MerchantListDto> GetAdminRecipientListDistinct();

        void SaveLogInDetails(string driverId, string vehicleId, string token, bool isDispatchRequest, int fleetId);

        int SaveLogOutDetails(string driverNo, bool isDispatchRequest,int fleetId);

        bool IsTokenValid(string driverNo, string token);

        FirstDataDetailsResultDto GetFirstDataDetails(string deviceId, string driverNo, string token, int fleetId, string requestType);

        int OnDemandReport(int driverId, int reportType, DateTime reportDate);

        IEnumerable<DriverListsDto> GetDrivers(int fleetId);

        IEnumerable<CarListDto> GetCars(int fleetId);

        bool IsValidateTokenMacAdd(string macAdd,string driverNo, string token,int fleetId);

        Tuple<string, string> GetCardAndPlate(int requestId);

        SurchargeFleetFeeDto SurchargeFees(int fleetId);

        IEnumerable<MerchantListDto> GetCorporateUserList(int corporateId);

        IEnumerable<MerchantListDto> GetUpdatedCorporateUserList(int reportId, int corporateUserId);

        string MerchantTimeZone(int merchantId);

        IEnumerable<MerchantListDto> WeeklyReportRecipients(int reportId);
    }
}
