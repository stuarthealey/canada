﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Service.Interface;
using MTData.Web.Resource;
using MTData.Web.ViewData;
using MTData.Utility;

namespace MTData.Web.Controllers
{
    public class AdminController : Controller
    {
        readonly ILogger _logger = new Logger();
        private UserViewData _objUserViewData;
        private readonly ICommonService _commonService;
        private readonly IAdminUserService _adminUserService;
        private StringBuilder _logMessage;
        private StringBuilder _logTracking;

        public AdminController([Named("AdminUserService")] IAdminUserService adminUserService, [Named("CommonService")] ICommonService commonService, StringBuilder logMessage, StringBuilder logTracking)
        {
            _commonService = commonService;
            _adminUserService = adminUserService;
            _logMessage = logMessage;
            _logTracking = logTracking;
        }

        /// <summary>
        /// Purpose             :   To get the list of admin users
        /// Function Name       :   Index
        /// Created By          :   Naveen Kumar
        /// Created On          :   17/12/2014
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            if ((SessionDto)Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                return View();
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   To create admin user
        /// Function Name       :   Create
        /// Created By          :  Naveen Kumar
        /// Created On          :   22/12/2014
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("using object initilizar for UserViewData and getting list of roles, counrtylist, statelist and citylist");

                _objUserViewData = new UserViewData
                {
                    CountryList = CountryList(),
                    fk_Country = MtdataResource.Australia //"AU"
                }; _logMessage.Append("properties to UserViewData has assigned successfully returning to view");

                return View(_objUserViewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   To create admin user
        /// Function Name       :   Create
        /// Created By          :   Naveen Kumar
        /// Created On          :   26/12/2014
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userView"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(UserViewData userView)
        {
            if (Session["User"] == null) return RedirectToAction("Index", "Member");
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1) return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("using object initilizar for UserViewData and getting list of roles, counrtylist, statelist and citylist");

                _objUserViewData = new UserViewData{CountryList = CountryList()};

                _logMessage.Append("properties to UserViewData has assigned successfully");

                if (ModelState.IsValid)
                {
                    _logMessage.Append("calling ToAdminDto method to map UserDto to UserViewData and retrun UserDto");

                    var userDto = userView.ToAdminDto();
                    string cPCode = Membership.GeneratePassword(10, 0);

                    _logMessage.Append("ToDTO method executed successfully");

                    userDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                    userDto.ModifiedBy = (((SessionDto)Session["User"]).SessionUser.Email);
                    userDto.PCode = cPCode.Hash();

                    _logMessage.Append("Calling Add method of adminUserService class takes MerchantUserDto, MerchantUserDto properties Frist Name= " + userDto.FName + ", Last Name= " + userDto.LName + ", Phone= " + userDto.Phone + ", Email= " + userDto.Email + ", Country= " + userDto.fk_Country + ", State= " + userDto.fk_State + ", City= " + userDto.fk_City + ", Address= " + userDto.Address + ", RolesAssigned= " + userDto.RolesAssigned + "+");

                    bool isExist = _adminUserService.Add(userDto);
                    if (isExist)
                    {
                        ViewBag.Message = "Account for '" + userDto.Email + "' already exist please contact to your administrator"; //"Email address already exists";
                        return View(_objUserViewData);
                    }

                    try
                    {
                        _logMessage.Append("Mail Sending begin and reading file ~/HtmlTemplate/confirmMail.html");

                        var filename = System.Web.HttpContext.Current.Server.MapPath("~/HtmlTemplate/confirmMail.html");
                        using (var myFile = new System.IO.StreamReader(filename))
                        {
                            string msgfile = myFile.ReadToEnd();
                            var msgBody = new StringBuilder(msgfile);
                            myFile.Close();

                            string cMail = userView.Email;
                            string cName = "MTI Admin";
                            string cuserName = userView.FName;
                            string appUrl = System.Configuration.ConfigurationManager.AppSettings["ApplicationURL"];

                            msgBody.Replace("[##cMail##]", cMail);
                            msgBody.Replace("[##cName##]", cName);
                            msgBody.Replace("[##cPassword##]", cPCode);
                            msgBody.Replace("[##AppURL##]", appUrl);
                            msgBody.Replace("[##cuserName##]", cuserName);

                            var sendEmail = new EmailUtil();

                            _logMessage.Append("calling SendMail method of utility class");

                            sendEmail.SendMail(cMail, msgBody.ToString(), cName, "Registered Admin");
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogInfoFatal(_logMessage.ToString(), ex);
                    }

                    _logTracking.Append("New Admin User " + userView.Email + " was Creadted by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                    _logger.LogInfoMessage(_logTracking.ToString());
                    _logMessage.Append("Add method of adminUserService class executed successfully");

                    return Redirect("Index");
                }

                return View(_objUserViewData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View(_objUserViewData);
        }

        /// <summary>
        /// Purpose             :   To edit the details of admin user
        /// Function Name       :   Update
        /// Created By          :   Naveen Kumar
        /// Created On          :   31/12/2014
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="adminUserId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Update(int adminUserId)
        {
            if (Session["User"] == null) 
                return RedirectToAction("Index", "Member");

            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || ((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1)
                return RedirectToAction("index", "Member");

            _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
            _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

            try
            {
                _logMessage.Append("calling GetAdminUserDto method of _adminUserService takes parameter adminUserId= " + adminUserId);

                var adminUserDto = _adminUserService.GetAdminUserDto(adminUserId);
                UserViewData mvData = adminUserDto.ToViewData();
                var countryDtoList = _commonService.GetCountryList();

                mvData.CountryList = countryDtoList.ToCountryViewDataList();

                _logMessage.Append("calling GetStateList of _commonService to populate state dropdown taking country id= " + mvData.fk_Country);

                ViewData.Model = mvData;

                return View(mvData);
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return View();
        }

        /// <summary>
        /// Purpose             :   To update details of admin user
        /// Function Name       :   Update
        /// Created By          :   Naveen Kumar
        /// Created On          :   05/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userView"></param>
        /// <param name="adminUserId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(UserViewData userView, int adminUserId)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null || (((SessionDto)Session["User"]).SessionUser.fk_UserTypeID != 1))
                return RedirectToAction("index", "Member");

            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                if (!ModelState.IsValid)
                {
                    _logMessage.Append("Creating object initilizar for MerchantUserViewData and getting list of roles, counrtylist, statelist and citylist");

                    _objUserViewData = new UserViewData{CountryList = CountryList()};

                    return View(_objUserViewData);
                }

                UserDto adminUserDto = ViewDataExtention.CreateAdminUserDto(userView);

                _logMessage.Append("CreateAdminUserDto method executed successfully");

                adminUserDto.CreatedBy = (((SessionDto)Session["User"]).SessionUser.Email);

                _logMessage.Append(
                    "Calling Update method of adminUserService class takes AdminUserDto, AdminUserDto properties  are Frist Name= " +
                    adminUserDto.FName + ", Last Name= " + adminUserDto.LName + ", Phone= " + adminUserDto.Phone +
                    ", Email= " + adminUserDto.Email + ", Country= " + adminUserDto.fk_Country + ", State= " +
                    adminUserDto.fk_State + ", City= " + adminUserDto.fk_City + ", Address= " + adminUserDto.Address);

                bool isExist = _adminUserService.Update(adminUserDto, adminUserId);
                if (!isExist)
                {
                    ViewBag.Message = MtdataResource.Email_already_exist;  //"Email address already exists";

                    _objUserViewData = new UserViewData{CountryList = CountryList()};

                    return View(_objUserViewData);
                }

                _logTracking.Append("Admin User Id " + adminUserId + " was Update by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                _logMessage.Append("Update method of adminUserService class executed successfully");

                return Redirect("Index");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return Redirect("Index");
        }

        /// <summary>
        /// Purpose             :   To filter the list of admin users
        /// Function Name       :   AdminUserListByFiter
        /// Created By          :   Naveen Kumar
        /// Created On          :   12/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="name"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <returns></returns>
        public JsonResult AdminUserListByFiter(string name = null, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetAdminUserListByFilter method of adminUserService class to get Admin Users List");

                name = name.Trim();

                int adminUserCount;
                var adminUser = _adminUserService.GetAdminUserListByFilter(name, jtStartIndex, jtPageSize, jtSorting);

                _logMessage.Append("GetAdminUserListByFilter of adminUserService executed successfully");

                if (string.IsNullOrEmpty(name))
                {
                    _logMessage.Append("Calling AdminUserListCount method of adminUserService class to count adminUsers");

                    adminUserCount = _adminUserService.GetAdminUserList(name).ToList().Count;

                    _logMessage.Append("AdminUserListCount method of adminUserService executed successfully result = " + adminUserCount);

                    return Json(new { Result = "OK", Records = adminUser, TotalRecordCount = adminUserCount });
                }

                var userDtos = adminUser as UserDto[] ?? adminUser.ToArray();
                adminUserCount = _adminUserService.GetAdminUserList(name).ToList().Count;

                return Json(new { Result = "OK", Records = userDtos, TotalRecordCount = adminUserCount });
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
                return Json(new { Result = "ERROR", ex.Message });
            }
        }

        /// <summary>
        /// Purpose             :   To delete the admin user
        /// Function Name       :   Delete
        /// Created By          :   Naveen Kumar
        /// Created On          :   13/01/2015
        /// Modification Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(int userId)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) return Json(new { Result = "No", Message = "Not Authorized" });
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));

                var sessionData = ((SessionDto)Session["User"]);
                if (sessionData.SessionUser.UserID == userId)
                {
                    var msg = "Can't delete currently logged in user";
                    return Json(new { Result = "No", Message = msg });
                }

                _logMessage.Append("Calling DeleteAdminUser method of adminUserService class takes UserId= " + userId);

                _adminUserService.DeleteAdminUser(userId);

                _logMessage.Append("DeleteAdminUser method of adminUserService class executed successfully");
                _logTracking.Append("Admin User Id " + userId + " was Deleted by " + sessionData.SessionUser.Email + "at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());
                
                return Json(new { Result = "OK" });
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return Json(new { Result = "No" });
        }

        /// <summary>
        /// Purpose             :   To lock the admin user
        /// Function Name       :   Islocked
        /// Created By          :   Naveen Kumar
        /// Created On          :   14/01/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Islocked(int id, bool status)
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("using object initilizar for UserViewData and getting list of roles, counrtylist, statelist and citylist");
                _logMessage.Append("calling IsLockedMember method of _commonService with parameters id and status " + id + " and " + status);

                _commonService.IsLockedMember(id, status);

                _logMessage.Append("IsLockedMember method of _commonService executed successfully returning to view");

                if (status)
                    _logTracking.Append("Admin User Id " + id + " was Locked by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());

                _logTracking.Append("Admin User Id " + id + " was Unlocked by " + (((SessionDto)Session["User"]).SessionUser.Email) + "at " + System.DateTime.Now.ToUniversalTime());
                _logger.LogInfoMessage(_logTracking.ToString());

                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), exception);
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public IEnumerable<CountryViewData> CountryList()
        {
            try
            {
                _logMessage.Append("Controller Name:-" + GetType().Name + "; Method Name :- " + MethodBase.GetCurrentMethod().Name + ";");
                _logMessage.Append(String.Format(CultureInfo.InvariantCulture, " Date:-" + DateTime.Now.ToShortDateString() + "; Time:- " + DateTime.Now.ToShortTimeString()));
                _logMessage.Append("Calling GetCountryList method of _commonService class to get countries");

                var countryDtoList = _commonService.GetCountryList();
                var countryViewDataList = countryDtoList.ToCountryViewDataList();

                List<CountryViewData> objcountrylist = countryViewDataList.ToList();

                _logMessage.Append("convert countryViewDataList into objcountrylist successful");

                var li = new List<SelectListItem> { new SelectListItem { Text = MtdataResource.Please_Select_a_Country, Value = "0" } };

                li.AddRange(objcountrylist.Select(item => new SelectListItem { Text = item.Title, Value = Convert.ToString(item.CountryCodesID) }));

                _logMessage.Append("returning country list");

                return objcountrylist;
            }
            catch (Exception ex)
            {
                _logger.LogInfoFatal(_logMessage.ToString(), ex);
            }

            return null;
        }

    }
}