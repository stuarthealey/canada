﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
    public class MtdtxnSemiUKDto
    {
        public int Id { get; set; }
        public string Revision { get; set; }
        public string SessionId { get; set; }
        public string MessageNumber { get; set; }
        public string TransactionStatus { get; set; }
        public string EntryMethod { get; set; }
        public string ReceiptNumber { get; set; }
        public string AcquirerMerchantID { get; set; }
        public string DateTime { get; set; }
        public string Currency { get; set; }
        public string CardSchemeName { get; set; }
        public string PAN { get; set; }
        public string ExpiryDate { get; set; }
        public string GemsReceiptID { get; set; }
        public string AuthorizationCode { get; set; }
        public string AcquirerResponseCode { get; set; }
        public string Reference { get; set; }
        public string MerchantName { get; set; }
        public string MerchantAddress1 { get; set; }
        public string MerchantAddress2 { get; set; }
        public string AID { get; set; }
        public string PANSequenceNum { get; set; }
        public string StartDate { get; set; }
        public string TerminalIdentity { get; set; }
        public Nullable<decimal> TransactionAmount { get; set; }
        public Nullable<bool> IsDCCTxn { get; set; }
        public Nullable<bool> IsLoyaltyTxn { get; set; }
        public Nullable<decimal> DCCAmount { get; set; }
        public Nullable<decimal> DonationAmount { get; set; }
        public Nullable<decimal> RedeemedAmount { get; set; }
        public string DCCCurrency { get; set; }
        public string FXRateApplied { get; set; }
        public Nullable<bool> FXExponentApplied { get; set; }
        public Nullable<int> DCCCurrencyExponent { get; set; }
        public string MessageHost { get; set; }
        public string TransactionType { get; set; }
        public Nullable<decimal> GratuityAmount { get; set; }
        public Nullable<decimal> CashAmount { get; set; }
        public Nullable<decimal> TotalTransactionAmount { get; set; }
        public string ICCApplicationFileName { get; set; }
        public string ICCApplicationPreferredName { get; set; }
        public Nullable<int> eCardVerificationMethod { get; set; }
        public string TransactionID { get; set; }
        public string RequestXml { get; set; }
        public Nullable<decimal> Surcharge { get; set; }
        //MoreFields
        public string TransNote { get; set; }
        public string CardType { get; set; }
        public string ZipCode { get; set; }
        public string StreetAddress { get; set; }
        public string RapidConnectAuthId { get; set; }
        public string PickUpAdd { get; set; }
        public string JobNumber { get; set; }
        public string PaymentType { get; set; }
        
        public Nullable<double> PickUpLat { get; set; }
        public Nullable<double> PickUpLng { get; set; }
        public Nullable<decimal> Others { get; set; }
        public Nullable<decimal> FlagFall { get; set; }
        public Nullable<decimal> Fee { get; set; }
        public Nullable<decimal> Fare { get; set; }
        public Nullable<decimal> Extras { get; set; }
        public Nullable<decimal> Tolls { get; set; }
        public Nullable<decimal> Taxes { get; set; }        
    }
}
