﻿using Ninject;

using MTD.Core.DataTransferObjects;
using MTD.Core.Extension;
using MTD.Core.Service.Interface;
using MTD.Data.Data;
using MTD.Data.Repository.Interface;

namespace MTD.Core.Service
{
    public class DefaultService : IDefaultService
    {
        private readonly IDefaultRepository _defaultRepository;

        public DefaultService([Named("DefaultRepository")] IDefaultRepository defaultRepository)
        {
            _defaultRepository = defaultRepository;
        }

        /// <summary>
        /// Purpose             :   To get Default Setting
        /// Function Name       :   GetAdminUserListByFilter
        /// Created By          :   Asif Shafeeque
        /// Created On          :   02/11/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="defaultId"></param>
        /// <returns></returns>
        public DefaultSettingDto GetDefaultDto(int defaultId)
        {
            try
            {
                var defaultSetting = _defaultRepository.GetDefault(defaultId);
                var defaultDto = defaultSetting.ToDefaultDto();
                return defaultDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Purpose             :   To Update Default Setting
        /// Function Name       :   Update
        /// Created By          :   Umesh Kumar
        /// Created On          :   02/11/2015
        /// Modifications Made   :   ****************************
        /// Modified On       :   ##/##/#### "MM/DD/YYYY"
        /// </summary>
        /// <param name="defaultDto"></param>
        /// <param name="defaultId"></param>
        public void Update(DefaultSettingDto defaultDto, int defaultId)
        {
            try
            {
                DefaultSetting defaultSetting = defaultDto.ToDefaultSetting();
                _defaultRepository.Update(defaultSetting, defaultId);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="defaultId"></param>
        /// <returns></returns>
        public TransactionCountSlabDto GetTransactionCountSlab(int merchantId)
        {
            try
            {
                TransactionCountSlab defaultSetting = _defaultRepository.GetTransactionCountSlab(merchantId);
                var defaultDto = defaultSetting.ToDefaultDto();
                return defaultDto;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public TransactionCountSlabDto GetTransactionCountSlabCorp(int userId)
        {
            try
            {
                TransactionCountSlab defaultSetting = _defaultRepository.GetTransactionCountSlabCorp(userId);
                var defaultDto = defaultSetting.ToDefaultDto();
                return defaultDto;
            }
            catch
            {
                throw;
            }
        }

        public void Update(TransactionCountSlabDto slabDto, int merchantId)
        {
            try
            {
                TransactionCountSlab slab = slabDto.ToDTO();
                _defaultRepository.Update(slab, merchantId);
            }
            catch
            {
                throw;
            }
        }

        public void UpdateCorp(TransactionCountSlabDto slabDto, int userId)
        {
            try
            {
                TransactionCountSlab slab = slabDto.ToDTO();
                _defaultRepository.UpdateCorp(slab, userId);
            }
            catch
            {
                throw;
            }
        }
    }
}
