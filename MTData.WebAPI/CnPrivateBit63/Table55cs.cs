﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table55
    {
        #region Private Constants and Variables
        #region Constants
        private const string tblId = "|35|35";
        private const string twoSpaces = "|20|20";
        #endregion
        #region Variables
        //Flag 1 Indicates that the merchant is able to accept the Merchant Advice Code at the Point of Service.
        private string defaultFlag = "1";
        #endregion
        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'35 35' in Request
        /// </summary>
        public string TableID
        {
            get { return tblId; }
        }

        /// <summary>
        /// Flag
        /// Used to Indicates that the merchant is able to accept the 
        /// Merchant Advice Code at the Point of Service.
        /// </summary>
        public string Flag
        {
            get { return defaultFlag; }
            set { defaultFlag = value; }
        }

        /// <summary>
        /// Indicator
        /// Provides the Merchant Advice Code.
        /// </summary>
        public string Indicator
        {
            get { return twoSpaces; }
        }
        #endregion
    }
}