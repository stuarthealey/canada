IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ExternalUser')
	CREATE TABLE [dbo].[ExternalUser](
		[MerchantId] [int] NULL,
		[UserName] [nvarchar](50) NULL,
		[PCode] [nvarchar](500) NULL,
		[Id] [int] IDENTITY(1,1) NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_NAME ='uk_UserName_ExternalUser')
	ALTER Table ExternalUser ADD CONSTRAINT uk_UserName_ExternalUser UNIQUE(UserName)
GO
 