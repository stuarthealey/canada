﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web;

namespace MTData.WebAPI.Models
{
    [DataContract(Namespace = "")]
    [XmlSerializerFormat]
    public class MACVerification
    {
        private string _macData;
        [DataMember]
        public string MacData
        {
            get { return string.IsNullOrEmpty(this._macData) ? string.Empty : this._macData; }
            set { this._macData = value; }
        }

        private string _checkDigit;
        [DataMember]
        public string CheckDigit
        {
            get { return string.IsNullOrEmpty(this._checkDigit) ? string.Empty : this._checkDigit; }
            set { this._checkDigit = value; }
        }

        private string _tMac;
        [DataMember]
        public string TMAC
        {
            get { return string.IsNullOrEmpty(this._tMac) ? string.Empty : this._tMac; }
            set { this._tMac = value; }
        }

        private string _tKpe;
        [DataMember]
        public string TKPE
        {
            get { return string.IsNullOrEmpty(this._tKpe) ? string.Empty : this._tKpe; }
            set { this._tKpe = value; }
        }

        private string _tKme;
        [DataMember]
        public string TKME
        {
            get { return string.IsNullOrEmpty(this._tKme) ? string.Empty : this._tKme; }
            set { this._tKme = value; }
        }

        private string _stan;
        [DataMember]
        public string STAN
        {
            get { return string.IsNullOrEmpty(this._stan) ? string.Empty : this._stan; }
            set { this._stan = value; }
        }
    }
}
