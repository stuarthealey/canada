﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Data.Data
{  
    public class DriverLoginData
    {
        public string Error { get; set; }
        public Nullable<int> TerminalID { get; set; }
        public Nullable<int> fk_MerchantID { get; set; }
        public Nullable<int> fleetId { get; set; }
        public string VehicleNumber { get; set; }
        public string PIN { get; set; }
        public string DriverNo { get; set; }
        public string DriverTaxNo { get; set; }
        public string FleetName { get; set; }
        public Nullable<bool> IsCardSwiped { get; set; }
        public Nullable<bool> IsContactless { get; set; }
        public Nullable<bool> IsChipAndPin { get; set; }
        public Nullable<bool> IsShowTip { get; set; }
        public string CurrencyCode { get; set; }
        public string CompanyTaxNumber { get; set; }
        public string Company { get; set; }
        public Nullable<decimal> TipPerLow { get; set; }
        public Nullable<decimal> TipPerMedium { get; set; }
        public Nullable<decimal> TipPerHigh { get; set; }
        public Nullable<decimal> FederalTaxRate { get; set; }
        public Nullable<decimal> StateTaxRate { get; set; }
        public string Disclaimer { get; set; }
        public string TermCondition { get; set; }
        public Nullable<bool> IsTaxInclusive { get; set; }
        public string Address { get; set; }
        public string fk_City { get; set; }
        public string fk_State { get; set; }
        public string fk_Country { get; set; }
    }
}
