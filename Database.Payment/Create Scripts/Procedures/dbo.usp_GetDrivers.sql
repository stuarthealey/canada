IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetDrivers]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[usp_GetDrivers]
GO

CREATE PROCEDURE dbo.usp_GetDrivers
(
	@MerchantId INT
)
AS
BEGIN
	SELECT * 
	FROM dbo.Driver
	WHERE fk_FleetID IN (SELECT F.FleetID FROM dbo.Fleet f WHERE f.fk_MerchantID =  @MerchantId)
	  AND IsActive = 1
END
GO
