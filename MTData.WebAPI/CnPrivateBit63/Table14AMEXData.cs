﻿using System;

namespace MTData.WebAPI.CnPrivateBit63
{
    public class Table14AMEXData
    {
        #region Private Constants and Variables

        #region Constants
        private const string tblId = "|31|34";
        private const string filler6Space = "|20|20|20|20|20|20";
        private const string filler12Space = "|20|20|20|20|20|20|20|20|20|20|20|20";
        #endregion

        #region Variables
        private string defaultAmexIndicator = "|58"; // |58 represents hex of X
        private string defaultTranID = "|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20"; // |20 represents hex of one blank space
        private string defaultPosData = "|20|20|20|20|20|20|20|20|20|20|20|20";
        private string defaultSellerID = string.Empty;//Not Assigned.
        #endregion

        #endregion

        #region Public Properties
        /// <summary>
        /// ID of Table
        /// default X'31 34' in Request
        /// </summary>
        public string TableID
        {
            // |31 and |34 represent hex of numeric 1 and 4
            get { return tblId; }
        }

        /// <summary>
        /// American Express Indicator
        /// default X is set in request
        /// </summary>
        public string AmexIndicator
        {
            get { return defaultAmexIndicator; }
            set { defaultAmexIndicator = value; }
        }

        /// <summary>
        /// Tran ID (Provided by Issuer)
        /// default 15 Spaces(1 space= X'20') in Request 
        /// </summary>
        public string TranID
        {
            get { return defaultTranID; }
            set { defaultTranID = value; }
        }

        /// <summary>
        /// Filler. contains 6 Spaces(1 space= X'20') in Request 
        /// </summary>
        public string Filler6Space
        {
            get { return filler6Space; }
        }

        /// <summary>
        /// POS Data
        /// default 12 Spaces(1 space= X'20') in Request 
        /// </summary>
        public string POSData
        {
            get { return defaultPosData; }
            set { defaultPosData = value; }
        }

        /// <summary>
        /// Filler. contains 12 Spaces(1 space= X'20') in Request 
        /// </summary>
        public string Filler12Space
        {
            get { return filler12Space; }
        }

        /// <summary>
        /// Merchant assigned Seller/Vendor unique numeric identifier.
        /// default 20 Spaces(1 space= X'20') in Request.
        /// The Seller ID is mandatory to be used only by aggregators.
        /// Space filled for non seller ID transaction by Aggregators.
        /// </summary>
        public string SellerID
        {
            get { return defaultSellerID; }
            set { defaultSellerID = SpacePadRightinValue(value, 20); }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Pad Right with spaces if Value is less then its size.
        /// </summary>
        /// <param name="value">string value of the Field</param>
        /// <param name="ilength">Length of the Field</param>
        /// <returns>Field Value with Padded Spaces if needed.</returns>
        private string SpacePadRightinValue(string value, int ilength)
        {
            var result = Convert.ToString(value).PadRight(ilength, Convert.ToChar(" "));
            result = result.Replace(" ", "PI20");//sigma static value enum should be used
            return result;
        }
 
        #endregion
    }
}