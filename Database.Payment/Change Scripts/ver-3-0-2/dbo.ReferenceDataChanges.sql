-- Update version number
------------------------
UPDATE dbo.SystemConfig
SET Version = '3.0.2.7'
WHERE SystemConfigID = (SELECT MAX(SystemConfigID) FROM dbo.SystemConfig)