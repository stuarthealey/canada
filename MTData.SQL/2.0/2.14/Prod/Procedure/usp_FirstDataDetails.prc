IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FirstDataDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FirstDataDetails]
GO

/****** Object:  StoredProcedure [dbo].[usp_FirstDataDetails]    Script Date: 11/3/2015 9:26:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[usp_FirstDataDetails]

(
       @deviceId varchar(100)=null,
       @driverNumber varchar(100)=null,
       @token varchar(max)=null,
	   @fleetId INT=null,
	   @isDispatchRequest BIT=null
)
as
begin
if (@fleetId IS NOT NULL) AND (@token IS NOT NULL)-- Driver Validation with FleetID and TOken,driverNo
BEGIN
if exists (select 1 from DriverLogin where DriverNo = @driverNumber and FleetId=@fleetId and token=@token and IsDispatchRequest = @isDispatchRequest)

		   begin
				  exec [usp_GetFirstDataValue] @deviceId,'1'
		   end
END
ELSE if (@fleetId IS NULL) AND (@token IS NOT NULL)--Validating driver by  DriverNo and Token [Backward compatibility]
BEGIN
	if exists (select 1 from DriverLogin where DriverNo = @driverNumber and token=@token and IsDispatchRequest = 0)

		   begin
				   exec [usp_GetFirstDataValue] @deviceId,'1'
		   end
END
ELSE if (@token IS NULL)
       begin
        exec [usp_GetFirstDataValue] @deviceId,'1'
       end

else
       begin
        exec [usp_GetFirstDataValue] @deviceId,'0'-- returns only schema
       end
end

GO


