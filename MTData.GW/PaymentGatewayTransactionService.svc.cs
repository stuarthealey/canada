﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml.Serialization;
using MTData.Utility;
using MTData.WCFGW.Model;
using System.Xml;
using System.Xml.Linq;
using System.Text.RegularExpressions;

namespace MTData.WCFGW
{
    public class PaymentGatewayTransactionService : IPaymentGatewayTransactionService
    {
        ILogger _logger = new Logger();
        StringBuilder _logMessage = new StringBuilder();

        public string BasicAuthorisation(string requestPacket)
        {
            Model.BasicAuthoriseRequest request = null;
            Model.BasicAuthoriseResponse response = null;
            try
            {
                if (requestPacket == null)
                    throw new ArgumentNullException("requestPacket is NULL");             
                request = FromXmlString(requestPacket.Trim());

                if (request == null)
                    throw new ArgumentNullException("requestPacket did not parse properly");
                if (request.AuthenticationHeader == null)
                    throw new ArgumentNullException("request.AuthenticationHeader did not parse properly");
                if (request.BasicAuthorise == null)
                    throw new ArgumentNullException("request.BasicAuthorise did not parse properly");

                //=================================================================================//

                _logMessage.AppendLine("BasicAuthorisation: process start. calling Web API.");
                var resp = ServiceHelper.RequestAPI(requestPacket, "Payment_ApiUrl".AppSetting());

                _logMessage.AppendLine("BasicAuthorisation: parsing response.");
                response = (BasicAuthoriseResponse)ServiceHelper.XmlToObject(resp, typeof(BasicAuthoriseResponse));

                _logMessage.AppendLine("BasicAuthorisation: process complete.");
                _logger.LogInfoMessage(_logMessage.ToString());

                return ToXmlString(response); ;

                //=================================================================================//
            }
            catch (Exception ex)
            {
                response = new Model.BasicAuthoriseResponse()
                {
                    BasicAuthoriseResult = new Model.BasicAuthoriseResult()
                    {
                        JobId = (request != null && request.BasicAuthorise != null) ? request.BasicAuthorise.jobId : 0,
                        ErrorMessage = ex.Message,
                        BalanceAmt = (request != null && request.BasicAuthorise != null) ? request.BasicAuthorise.paymentAmt : 0
                    }
                };
            }
            if (response != null)
                return ToXmlString(response);
            else
                return "";
        }

        private string ToXmlString(Model.BasicAuthoriseResponse response)
        {
            using (var memoryStream = new MemoryStream())
            {
                var xmlSerializer = new XmlSerializer(typeof(Model.BasicAuthoriseResponse), "http://services.mtdata.com/payment");
                XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
                xmlSerializer.Serialize(memoryStream, response, namespaces);

                return Encoding.UTF8.GetString(memoryStream.ToArray());
            }
        }

        private Model.BasicAuthoriseRequest FromXmlString(string xmlRequest)
        {
            using (MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlRequest)))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Model.BasicAuthoriseRequest), "http://services.mtdata.com/payment");
                return (Model.BasicAuthoriseRequest)xmlSerializer.Deserialize(memoryStream);
            }
        }
    }
}
