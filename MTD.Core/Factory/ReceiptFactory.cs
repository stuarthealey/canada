﻿using MTD.Core.DataTransferObjects;
using MTD.Core.Factory.Interface;
using MTD.Data.Data;

namespace MTD.Core.Factory
{
   public class ReceiptFactory:IReceiptFactory
    {
       public ReceiptFormat CreateReceipt(ReceiptFormatDto receiptDto)
       {
           ReceiptFormat receipt = new ReceiptFormat
           {
               FleetID = receiptDto.FleetID,
               fk_MerchantID = receiptDto.FleetID,
               FleetName = receiptDto.FleetName,
               Description = receiptDto.Description,
               //recceiptMaster = receiptDto.recceiptMaster,
               FleetReceiptID = receiptDto.FleetReceiptID,
               fk_FleetID = receiptDto.fk_FleetID,
               fk_FieldID = receiptDto.fk_FieldID,
               Postion = receiptDto.Postion,
               IsShow = receiptDto.IsShow,
               CreatedBy = receiptDto.CreatedBy,
               ModifiedDate = receiptDto.ModifiedDate,
               ModifiedBy = receiptDto.ModifiedBy,
        
           };
           return receipt;
       }
    }
}
