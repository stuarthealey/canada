﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTD.Core.DataTransferObjects
{
    public class MerchantListIDDto
    {
        public int fk_MerchantID { get; set; }
    }
}
