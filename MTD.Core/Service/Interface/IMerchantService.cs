﻿using System.Collections.Generic;

using MTD.Core.DataTransferObjects;

namespace MTD.Core.Service.Interface
{
    public interface IMerchantService
    {
        bool Add(MerchantUserDto merchant);

        int Update(MerchantUserDto merchant, int merchantId);

        MerchantUserDto GetMerchant(int merchantId, int userId);

        MerchantUserDto GetMerchantViewAs(int merchantId, int userId);

        void DeleteMerchant(int merchantId);

        IEnumerable<MerchantUserDto> MerchantList(string name);

        IEnumerable<RoleDto> GetUserRoles();

        void AddReciept(ManageRecieptDto recieptDto);

        MerchantDto GetMerchant(int merchantId);

        IEnumerable<MerchantUserDto> GetMerchantByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting);

        DatawireDto GetDataWireDetails(int merchantId, int id);

        TerminalDto TerminalDetails(string serialNo);

        bool FdMerchantAdd(fDMerchantDto fDMerchant);
        
        IEnumerable<FDMerchantListDto> GetFDMerchantList();
        
        IEnumerable<fDMerchantDto> GetFdMerchantListByFilter(string name, int jtStartIndex, int jtPageSize, string jtSorting);
        
        bool DeleteFdMerchant(int fdMerchantId);
        
        fDMerchantDto GetFDMerchantUpdate(int fdMerchantId);
        
        void FdMerchantUpdate(fDMerchantDto fdMerchant, int fdMerchantId);
        
        fDMerchantDto GetFdMerchantForUpdate(int fdId);
        
        fDMerchantDto GetFDMerchant(int merchantId);
        
        int GetMerchantId(string email);
        
        int AddFdMerchant(fDMerchantDto fDMerchant);
        
        bool CompanyExsit(string company);

        bool EmailExist(string email);

        bool UpdatePassword(int userId, string newPassword);

        bool UpdateTCPassword(int merchantId,string userName, string newPassword);

        string IsAllReadySet(int merchantId);

        string GetEmail(int userId);
        
        IEnumerable<MerchantListDto> GetUserReceipent(int merchantId);
        
        bool UpdateUserRecipient(RecipientDto recipientDto);

        fDMerchantDto FdMerchantExist(int fkDId);
        
        IEnumerable<ReportListDto> GetReportList();

        IEnumerable<MerchantListDto> GetUserReceipent(int merchantId, int reportId);

        bool CompanyExsit(string company, int merchantId);

        bool IsUserAvailable(string userName);

        string GetMerchantZipCode(int merchantId);

        //PAY-13 Trans Armor flag set from Merchant UseTransArmor setting.
        bool IsTransArmor(int merchantId);
    }
}
