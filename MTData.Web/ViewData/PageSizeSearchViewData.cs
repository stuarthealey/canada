﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTData.Web.ViewData
{
    public class PageSizeSearchViewData
    {
        public string Name { get; set; }
        public int JtStartIndex { get; set; }
        public int JtPageSize { get; set; }
        public string JtSorting { get; set; }
        public int UserId { get; set; }
        public int UserType { get; set; }
        public int fk_userTypeId { get; set; }
    }
}