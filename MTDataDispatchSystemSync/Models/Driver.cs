﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web;
using System.Xml.Serialization;

namespace MTDataDispatchSystemSync.Models
{
    public class Driver
    {
        public int DriverID { get; set; }
        public string DriverNumber { get; set; }
        public string City { get; set; }
        public int DriverPIN { get; set; }
        public string DriverName { get; set; }
        public string DriverSurname { get; set; }
        public string DisplayName { get; set; }
        public List<Address> Address { get; set; }
        public string Comments { get; set; }
        public string DateOfBirth { get; set; }
        public string BirthCountry { get; set; }
        public string DriverLicenceCountry { get; set; }
        public string DriverLicenceState { get; set; }
        public string DriverLicence { get; set; }
        public string DriverLicenceExpiryDate { get; set; }
        public string Email { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string ValidationReason { get; set; }
        public string InhibitedReason { get; set; }
        public bool IsActive { get; set; }
        public string StartDate { get; set; }
        public string TaxiLicence { get; set; }
        public string TaxNumber { get; set; }
        public string PoliceCheck { get; set; }
        public string Medical { get; set; }
        public string DefensiveDriving { get; set; }
        public string FindgerPrintExpiry { get; set; }
        public bool SMSCapable { get; set; }
        public bool OwnerOp { get; set; }
        public string DriverHeight { get; set; }
        public string Weight { get; set; }
        public string Sex { get; set; }
        public string HairColor { get; set; }
        public string EyeColor { get; set; }
        public string Ethnicity { get; set; }
        public List<Lease> Lease { get; set; }
        public string NextOfKinName { get; set; }
        public string NextOfKinSurName { get; set; }
        public string NextOfKinRelationship { get; set; }
        public string NextOfKinPhone { get; set; }
        public string NextOfKinMobile { get; set; }
        public List<NextOfKinAddress> NextOfKinAddress { get; set; }
        public string SSN { get; set; }
        public string LastLogonTime { get; set; }
        public string LastLogoffTime { get; set; }
        public List<Fleets> DriverFleets { get; set; }
        public List<Condition> DriverConditions { get; set; }
        public List<ExpiryDateValueArray> ExpiryDateValueArray { get; set; }
    }
}