-- Create indexes on Vehicle for performance.
--===========================================

IF NOT EXISTS (SELECT * FROM sysindexes si INNER JOIN sysobjects so ON so.id = si.id WHERE si.name = 'IX_VehicleNumberFleet' AND OBJECTPROPERTY(so.id, N'IsUserTable') = 1 AND so.name = 'Vehicle')
BEGIN
	PRINT 'Creating Index IX_VehicleNumberFleet on Vehicle'

	CREATE NONCLUSTERED INDEX IX_VehicleNumberFleet ON dbo.Vehicle (VehicleNumber, fk_FleetID)
		WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
ELSE
BEGIN
	PRINT 'Index IX_VehicleNumberFleet on Vehicle already exists'
END
