﻿using MTData.WebAPI.CanadaCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MTData.WebAPI.CanadaCert
{
    public class CnTransactionRequest : ICloneable
    {
        #region Public properties
        /// <summary>
        /// gets or sets the Token value
        /// Token uniquely identifies a Merchant
        /// </summary>
        [DataMember]
        public long Token { get; set; }

        /// <summary>
        /// Get and Set the transaction type like Sale, Void etc.
        /// </summary>
        [DataMember]
        public TransactionType TransactionType { get; set; }

        /// <summary>
        /// set and get Card Number 
        /// </summary>
        [DataMember]
        public string CardNumber { get; set; }

        /// <summary>
        /// set and get transaction Amount
        /// </summary>
        [DataMember]
        public decimal TransactionAmount { get; set; }

        /// <summary>
        /// set and get Card ExpiryDate
        /// </summary>
        [DataMember]
        public string CardExpiryDate { get; set; }

        /// <summary>
        /// set and get Card Verification Value
        /// </summary>
        [DataMember]
        public string CardCVV { get; set; }

        /// <summary>
        /// gets and sets the Track1 Data from card
        /// </summary>
        [DataMember]
        public string Track1Data { get; set; }

        /// <summary>
        /// gets and sets the Track2 Data from card
        /// </summary>
        [DataMember]
        public string Track2Data { get; set; }

        /// <summary>
        /// get or sets the Zip Code for the request.
        /// </summary>
        [DataMember]
        public string ZipCode { get; set; }
        /// <summary>
        /// get or sets the Street address for the request.
        /// </summary>
        [DataMember]
        public string StreetAddress { get; set; }

        /// <summary>
        /// get or set the Transaction Id of original sale transaction.
        /// </summary>
        [DataMember]
        public long TransactionId { get; set; }

        /// <summary>
        /// Unique ID from end user 
        /// </summary>
        [DataMember]
        public string InvoiceNo { get; set; }

        /// <summary>
        /// Get or set the Authcode
        /// </summary>
        [DataMember]
        public string Authcode { get; set; }

        /// <summary>
        /// Get or set the types of Bill Payment.
        /// </summary>
        [DataMember]
        public BillPaymentType BillPaymentType { get; set; }

        //Added for testing purpose only.
        [DataMember]
        public BillPaymentMode BillPaymentMode { get; set; }

        /// <summary>
        /// Added to accept the tranasction source key from different merchant
        /// </summary>
        [DataMember]
        public string TransactionSourceKey { get; set; }

        /// <summary>
        /// Added to accept the encrypted pin data for pin transactions.
        /// </summary>
        [DataMember]
        public string EncryptedPinData { get; set; }

        /// <summary>
        /// Set and get whether cash back transaction or not.
        /// </summary>
        [DataMember]
        public bool IsCashBack { get; set; }

        /// <summary>
        /// Set and get cash back amount.
        /// </summary>
        [DataMember]
        public decimal CashBackAmount { get; set; }

        /// <summary>
        /// set or get the terminal lane number 
        /// </summary>
        [DataMember]
        public string TerminalLaneNumer { get; set; }

        /// <summary>
        /// Set and get whether pin less debit transaction or not.
        /// </summary>
        [DataMember]
        public bool IsPinLessDebit { get; set; }

        [DataMember]
        public string Payload { get; set; }

        [DataMember]
        public string ServiceID { get; set; }

        #endregion

        #region Other Properties
        /// <summary>
        /// Get and set the SystemTrace number.
        /// </summary>
        public string SystemTrace { get; set; }

        public string ClientReference { get; set; }

        public string ReferenceNumber { get; set; }

        public string MerchantId { get; set; }

        public string TerminalId { get; set; }

        public string MerchantZipCode { get; set; }

        /// <summary>
        /// set and get the CardType like Credit, Debit etc
        /// </summary>
        public CardType CardType { get; set; }

        public CardIssuer CardIssuer { get; set; }

        public IndustryType Industry { get; set; }

        public string TransactionCounter { get; set; }

        public string MerchantCategoryCode { get; set; }

        public bool IsSwiped { get; set; }

        //public string POSEntryMode { get; set; }
        #endregion

        #region Member function
        public object Clone()
        {
            return this.MemberwiseClone();
        }
        #endregion//end member function
    }
}