﻿using System.Collections.Generic;
using MTD.Core.DataTransferObjects;

namespace MTD.Core.Service.Interface
{
    public interface IBookingFeeService
    {

        void Add(BookigFeeDto feeDto);

        void Update(BookigFeeDto feeDto, int bookingFeeId);
        
        IEnumerable<BookigFeeDto> GetFeeDtoList();
        
        BookigFeeDto GetFeeDto(int bookingFeeId);
        
        void DeleteFee(int bookingFeeId);

        IEnumerable<BookigFeeDto> FeeListByFiter(FilterSearchParametersDto searchParameters);  
    
    }
}
